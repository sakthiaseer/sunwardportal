﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SalesReport
{
    class Program
    {
        static void Main(string[] args)
        {
           
            SyncSalesReport();
        }
        static void SyncSalesReport()
        {
             var appSettings = ConfigurationManager.AppSettings;
             string filePath = @""+ appSettings.Get("FilePath");
            if (File.Exists(filePath))
            {
                Application xlApp = new Application();
                Workbook xlWorkBook = xlApp.Workbooks.Open(filePath);
                Worksheet xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Range xlRange = xlWorkSheet.UsedRange;
                int totalRows = xlRange.Rows.Count;
                using (var context = new CRT_SWTMSEntities())
                {
                    var historicalDetails = context.HistoricalDetails.Select(s=>new { s.Invoice,s.CustNo,s.Item}).ToList();
                    List<HistoricalDetail> addHistoricalDetails = new List<HistoricalDetail>();
                    for (int rowCount = 2; rowCount <= totalRows; rowCount++)
                    {
                        if (rowCount == 2)
                        {
                            Console.WriteLine("Start");
                        }
                        if (rowCount == totalRows)
                        {
                            Console.WriteLine("Completed");
                        }
                        var invoiceNo = ((xlRange.Cells[rowCount, 2] as Range).Text);
                        if (!string.IsNullOrEmpty(invoiceNo))
                        {
                            var custNo = ((xlRange.Cells[rowCount, 3] as Range).Text);
                            var item = ((xlRange.Cells[rowCount, 7] as Range).Text);
                            var dt = ((xlRange.Cells[rowCount, 1] as Range).Text);
                            DateTime? dateTime = null;
                            if (!string.IsNullOrEmpty(dt))
                            {
                                string s2 = "/";
                                if (dt.Contains(s2))
                                {
                                    string[] dateString = dt.Split('/');
                                    if (dateString.Count() > 2)
                                    {
                                        DateTime d = Convert.ToDateTime(dateString[1] + "/" + dateString[0] + "/" + dateString[2]);
                                        dateTime = d;
                                    }
                                }
                                else
                                {
                                    string[] dateStrings = dt.Split('-');
                                    if (dateStrings.Count() > 2)
                                    {
                                        DateTime d = Convert.ToDateTime(dateStrings[1] + "/" + dateStrings[0] + "/" + dateStrings[2]);
                                        dateTime = d;
                                    }
                                    else
                                    {
                                        double d = double.Parse(dt);
                                        DateTime conv = DateTime.FromOADate(d);
                                        dateTime = conv;
                                    }
                                }
                            }
                            var expiryDt = ((xlRange.Cells[rowCount, 12] as Range).Text);
                            DateTime? expiryDate = null;
                            if (!string.IsNullOrEmpty(expiryDt))
                            {
                                string s2 = "/";
                                if (expiryDt.Contains(s2))
                                {
                                    string[] dateString = expiryDt.Split('/');
                                    if (dateString.Count() > 2)
                                    {
                                        DateTime d = Convert.ToDateTime(dateString[1] + "/" + dateString[0] + "/" + dateString[2]);
                                        expiryDate = d;
                                    }
                                }
                                else
                                {
                                    string[] dateStrings = expiryDt.Split('-');
                                    if (dateStrings.Count() > 2)
                                    {
                                        DateTime d = Convert.ToDateTime(dateStrings[1] + "/" + dateStrings[0] + "/" + dateStrings[2]);
                                        expiryDate = d;
                                    }
                                    else
                                    {
                                        double ds = double.Parse(dt);
                                        DateTime conv = DateTime.FromOADate(ds);
                                        expiryDate = conv;
                                    }
                                }
                            }

                            var counts = historicalDetails.Where(w => w.Item == item && w.CustNo == custNo && w.Item == item).Count();
                            if (counts <= 0)
                            {
                                HistoricalDetail historicalDetail = new HistoricalDetail
                                {
                                    NavCustomerID = long.Parse(appSettings.Get("NavCustomerId")),
                                    AddedByUserID = 1,
                                    AddedDate = DateTime.Now,
                                    StatusCodeID = 1,
                                    Date = dateTime,
                                    Invoice = invoiceNo,
                                    CustNo = custNo,
                                    Company = ((xlRange.Cells[rowCount, 4] as Range).Text),
                                    BranchCode = ((xlRange.Cells[rowCount, 5] as Range).Text),
                                    Branch = ((xlRange.Cells[rowCount, 6] as Range).Text),
                                    Item = item,
                                    Description = ((xlRange.Cells[rowCount, 8] as Range).Text),
                                    Po = ((xlRange.Cells[rowCount, 9] as Range).Text),
                                    TranType = ((xlRange.Cells[rowCount, 10] as Range).Text),
                                    Batch = ((xlRange.Cells[rowCount, 11] as Range).Text),
                                    Expiry = expiryDate,
                                    Ref = ((xlRange.Cells[rowCount, 13] as Range).Text),
                                    Price = decimal.Parse((xlRange.Cells[rowCount, 14] as Range).Text),
                                    Discount = decimal.Parse((xlRange.Cells[rowCount, 15] as Range).Text),
                                    Qty = decimal.Parse((xlRange.Cells[rowCount, 16] as Range).Text),
                                    Bouns = decimal.Parse((xlRange.Cells[rowCount, 17] as Range).Text),
                                    Nett = decimal.Parse((xlRange.Cells[rowCount, 18] as Range).Text),
                                    Gross = decimal.Parse((xlRange.Cells[rowCount, 19] as Range).Text),
                                };
                                Console.WriteLine(invoiceNo + "..");
                               // context.HistoricalDetails.Add(historicalDetail);
                                //addHistoricalDetails.Add(historicalDetail);
                            }
                            else
                            {
                                Console.WriteLine(invoiceNo + ".. Exits");
                            }
                        }
                    }
                    context.SaveChanges();
                    //context.BulkInsert(addHistoricalDetails);
                }
                xlWorkBook.Close();
                xlApp.Quit();
                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("File Not Exits");
            }
        }
    }
}
