﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToExcelFile = ""
    + @"C:\Users\Sathish\Downloads\DMS20220902.xlsx";
            string sheetName = "2022 standalone";
            List<ProfileUpdateModel> itemMasterModels = new List<ProfileUpdateModel>();
            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var navItemMasterModels = (from a in excelFile.Worksheet<ProfileUpdateModel>(sheetName) select a).ToList();
            using (var context = new SW_LIVEEntities())
            {
                navItemMasterModels.ForEach(f =>
                {
                    var documentID = context.Documents.Where(d => d.ProfileNo == f.ProfileNoOld).FirstOrDefault()?.DocumentID;
                    if(documentID>0 && f.ProfileNoNew!=null)
                    {
                        var query1 = string.Format("Update Documents Set ProfileNo='{1}'  Where DocumentId= {0}", documentID, f.ProfileNoNew);
                        var rowaffected1 = context.Database.ExecuteSqlCommand(query1);
                       
                    }
                });
               // context.Documents
            }
        }
    }
    class ProfileUpdateModel
    {
        public long DocumentID { get; set; }

        public string ProfileNoOld { get; set; }
        public string ProfileNoNew { get; set; }
       


    }
}
