﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class VendorListModel : BaseModel
    {
        public long VendorsListID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string VendorNo { get; set; }
        public long? OfficeAddressID { get; set; }
        public long? SiteAddressID { get; set; }
        public List<ICTCertificateModel> CertificateList { get; set; }
        public List<ICTContactDetailsModel> ContactList { get; set; }
        public AddressModel OfficeAddress { get; set; }
        public AddressModel SiteAddress { get; set; }
        public VendorInvoiceModel VendorListInvoice { get; set; }
        public VendorPaymentModel VenorListPayment { get; set; }
        public List<long?> SourceListIDs { get; set; }









    }
}
