﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeLeaveInformationModel : BaseModel
    {
        public long EmployeeLeaveInformatonId { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public int? NonPresenceId { get; set; }
        public string EmployeeName { get; set; }
        public string NonPresence { get; set; }
        public string Remarks { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string Designation { get; set; }
        public string DepartmentDesignation { get; set; }
    }
}
