﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommitmentOrdersModel: BaseModel
    {
        public long CommitmentOrdersId { get; set; }
        public long? CompanyId { get; set; }
        public string SwreferenceNo { get; set; }
        public DateTime? Date { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerRefNo { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string Product { get; set; }
       

    }
}
