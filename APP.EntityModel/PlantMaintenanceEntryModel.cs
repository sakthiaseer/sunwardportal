﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlantMaintenanceEntryModel : BaseModel
    {
        public long PlantEntryId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public long? ZoneId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public string CompanyName { get; set; }
        public string SiteName { get; set; }
        public string ZoneName { get; set; }
        public string LocationName { get; set; }
        public string AreaName { get; set; }
        public string SpecificAreaName { get; set; }
        
    }
}
