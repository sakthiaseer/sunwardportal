﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TeamMasterModel : BaseModel
    {
        public long TeamMasterID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<long>  MemberIds { get; set; }

    }
}
