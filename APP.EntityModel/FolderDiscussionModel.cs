﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FolderDiscussionModel : BaseModel
    {
        public long DiscussionNotesID { get; set; }
        public long? FolderID { get; set; }
        public long? ParentDiscussionNotesID { get; set; }
        public string DiscussionNotes { get; set; }
        public long? DiscussedBy { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsEdited { get; set; }
        public long? EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }
        public bool? IsDocument { get; set; }
        public long? DocumentID { get; set; }
        public int? StatusCodeId { get; set; }
        public string Status { get; set; }
        public string FileName { get; set; }

        public List<FolderDiscussionModel> FolderDiscussionModels { get; set; }

        public List<long> DiscussionUserID { get; set; }
        public string DiscussionSubject { get; set; }
        public Guid? SessionID { get; set; }
        public FolderDiscussionAttachmentModel FolderDiscussionAttachemntsModels { get; set; }
        public bool ViewReply { get; set; } = false;
        public string ReplyLabel { get; set; } = "view replies";
    }
}
