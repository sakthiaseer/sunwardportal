﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ManpowerInformationLineModel : BaseModel
    {
        public long ManpowerInformationLineID { get; set; }
        public long? ManpowerInformationID { get; set; }
        public string OperationNo { get; set; }
        public bool? IsOperatorSkilled { get; set; }
        public bool? IsQCApproval { get; set; }
        public bool? IsPICInPerson { get; set; }
        public string OperationFromNo { get; set; }
        public string OperationToNo { get; set; }
        public string AddedByHuman { get; set; }
        public string MaxNumberOfHuman { get; set; }


    }
}
