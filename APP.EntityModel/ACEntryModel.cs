﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACEntryModel : BaseModel
    {
        public long ACEntryId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CustomerName { get; set; }
        public long? DocumentId { get; set; }
        public string FileName { get; set; }
        public Guid? SessionID { get; set; }
        public decimal? Quantity { get; set; }
        public string Remark { get; set; }
        public string Version { get; set; }

        public Guid? SessionId { get; set; }

        public List<ACEntryLinesModel> Acentrylines { get; set; }

    }
}
