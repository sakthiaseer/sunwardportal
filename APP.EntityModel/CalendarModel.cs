﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CalendarModel : BaseModel
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string cssClass { get; set; }
        public string Title { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public long SourceId { get; set; }
    }
}
