﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionSimulationGroupingModel : BaseModel
    {
        public long ProductionSimulationGroupingId { get; set; }
        public long? ProductionSimulationId { get; set; }
        public string ProdOrderNo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string PackSize { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public decimal? PerQuantity { get; set; }
        public string PerQtyUom { get; set; }
        public string BatchNo { get; set; }
        public DateTime? StartingDate { get; set; }
        public long? CompanyId { get; set; }
        public decimal? PlannedQty { get; set; }
        public decimal? OutputQty { get; set; }
        public bool? IsOutput { get; set; }
        public DateTime? ProcessDate { get; set; }
        public string RePlanRefNo { get; set; }
        public string BatchSize { get; set; }
        public bool? IsBmrticket { get; set; }
        public string Description2 { get; set; }
        public long? TypeOfProdOrderId { get; set; }
        public long? SellingStatusId { get; set; }
        public string PortalStatusNo { get; set; }
        public long? ProfileID { get; set; }
        public string TypeOfProdOrder { get; set; }
        public string SellingStatus { get; set; }
        public string GenericCode { get; set; }
        public string InternalRef { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string ShelfLife { get; set; }
        public string RecipeNo { get; set; }
        public string MachineCenterName { get; set; }
        public string LocationCode { get; set; }
        public string Substatus { get; set; }
        public string Remarks { get; set; }
        public long? MethodCodeId { get; set; }
        public string Dispense { get; set; }

        public string MethodCode { get; set; }
    }
}
