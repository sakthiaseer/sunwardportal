﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppTranStockModel : BaseModel
    {
        public int Id { get; set; }
        public string RequisitionNo { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public string TransferNo { get; set; }
    }

    public class AppTranStockLineModel : BaseModel
    {
        public int Id { get; set; }
        public int? AppTranStockId { get; set; }
        public string ScanActionStatus { get; set; }
        public string DocRequisitionNo { get; set; }
        public string ActionStatus { get; set; }
        public string LabelNo { get; set; }
        public string ItemNo { get; set; }
        public string LotNo { get; set; }
        public decimal? Qty { get; set; }
    }
}
