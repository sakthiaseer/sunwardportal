﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyListingCompanyTypeModel
    {
        public long CompanyListingCompanyTypeID { get; set; }
        public long? CompanyListingID { get; set; }
        public long? CompanyTypeID { get; set; }
        public string CompanyTypeName { get; set; }
        public string CompanyName { get; set; }
    }
}
