﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationAbbreviationModel:BaseModel
    {
        public long ApplicationAbbreviationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Definition { get; set; }
        public long? DeparmentId { get; set; }

        public string DepartmentName { get; set; }
    }
}
