﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPProductLineModel : BaseModel
    {
        public long FPProductLineID {get; set;}
        public long? DatabaseID { get; set; }
        public long? NavisionID { get; set; }
        public long? FPProductID { get; set; }
        public string Database { get; set; }
        public string Navision { get; set; }

    }
}
