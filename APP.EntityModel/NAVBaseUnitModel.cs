﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVBaseUnitModel : BaseModel
    {
        public long NavBaseUnitId { get; set; }
        public long? CompanyId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
