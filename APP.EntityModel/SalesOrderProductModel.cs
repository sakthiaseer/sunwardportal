﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderProductModel:BaseModel
    {
        public long SalesOrderProductId { get; set; }
        public long? SalesOrderId { get; set; }
        public long? CrossReferenceProductId { get; set; }
        public string ProductNo { get; set; }
        public decimal? BalanceQty { get; set; }
        public string SpecialRemarks { get; set; }
        public decimal? QtyOrder { get; set; }
        public long? Uom { get; set; }
        public string UomName { get; set; }
        public DateTime? RequestShipmentDate { get; set; }
        public DateTime? TenderPeriodEnds { get; set; }
        public decimal? QtyIntend { get; set; }
        public decimal? Kivqty { get; set; }
        public long? PurchaseItemId { get; set; }
        public string Price { get; set; }
        public string Description { get; set; }
        public string ContractNo { get; set; }
        public long? SalesOrderEntryId { get; set; } 
        public string Ponumber { get; set; }

    }
}
