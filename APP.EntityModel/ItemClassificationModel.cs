﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemClassificationModel : BaseModel
    {
        public long ItemClassificationID { get; set; }
        public long? ParentID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FormTitle { get; set; }
        public bool? IsForm { get; set; }
        public long? FormId { get; set; }
        public long? ProfileID { get; set; }
        public string ProfileName { get; set; }
        public string FormName { get; set; }
        public long? MainClassificationID { get; set; }

        public string ProfileReferenceNo { get; set; }

        public long? ItemClassificationAccessID { get; set; }

        public long? UserID { get; set; }
        public long? UserGroupID { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public long? RoleID { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsDelete { get; set; }
        public int? ClassificationTypeId { get; set; }

        public string ClassificationLinkType { get; set; }
        public List<ItemClassificationModel> Children { get; set; }
        public List<long?> ItemClassificationIDs { get; set; }

        public List<long?> itemClassificationFormIds { get; set; }
        public bool? IsApprovalForm { get; set; }
        public ItemClassificationPermissionModel ItemClassificationPermissionModel { get; set; }
        public string PathName { get; set; }
        public List<string> PathNames { get; set; } = new List<string>();
    }
}
