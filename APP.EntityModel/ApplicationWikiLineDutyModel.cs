﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationWikiLineDutyModel:BaseModel
    {
        public long ApplicationWikiLineDutyId { get; set; }
        public long? ApplicationWikiLineId { get; set; }
        public long? DutyNo { get; set; }
        public long? EmployeeId { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationNumber { get; set; }
        public string PlantCompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string EmployeeName { get; set; }
        public string DutyNoName { get; set; }
        public string Description { get; set; }
        public List<long?> EmployeeIDs { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public string Type { get; set; }
        public List<WikiResponsibilityModel> wikiResponsibilityModels { get; set; }
    }
}
