﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentFolderModel : BaseModel
    {
        public long DocumentFolderID { get; set; }
        public long FolderID { get; set; }
        public long DocumentID { get; set; }
        public string Document { get; set; }
        public string FolderTitle { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long? FileSize { get; set; }
        public Guid? SessionID { get; set; }
        public bool? IsMajorChange { get; set; }
        public bool? IsNoChange { get; set; }
        public bool? IsReleaseVersion { get; set; }
        public List<long> ReadonlyUserID { get; set; }
        public List<long> ReadWriteUserID { get; set; }
        public long? SelectedFolderID { get; set; }
        public string ActualVersionNo { get; set; }
        public string DraftingVersionNo { get; set; }
        public string OverwriteVersionNo { get; set; }
        public bool? IsOverwrite { get; set; }

        public string Description { get; set; }
        public string Link { get; set; }

    }
}
