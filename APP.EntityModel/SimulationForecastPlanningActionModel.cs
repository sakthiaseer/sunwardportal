﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SimulationForecastPlanningActionModel
    {
        public long ProductionSimulationId { get; set; }
        public string GenericCode { get; set; }
        public string ProdOrderNo { get; set; }
        public string BatchNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string InternalRef { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string ShelfLife { get; set; }
        public string BatchSize { get; set; }
        public string RecipeNo { get; set; }
        public string MachineCenterName { get; set; }
        public string LocationCode { get; set; }
        public string Substatus { get; set; }
        public string Remarks { get; set; }
    }
}
