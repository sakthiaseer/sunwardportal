﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class FpdrugClassificationLineModel:BaseModel
    {
        public long FpdrugClassificationLineId { get; set; }
        public long? FpdrugClassificationId { get; set; }
        public int? PurposeId { get; set; }
        public long? RequirementId { get; set; }
        public string TimeRequire { get; set; }
        public long? CalculationPointId { get; set; }
        public string WiLink { get; set; }
        public string PurposeName { get; set; }
        public string RequirementName { get; set; }
        public string CalculationPoint { get; set; }
    }
}
