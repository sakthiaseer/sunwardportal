﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesBorrowHeaderModel : BaseModel
    {
        public long SalesBorrowHeaderId { get; set; }
        public long? OnBehalfId { get; set; }
        public long? BorrowFromId { get; set; }
        public long? CustomerToSendId { get; set; }
        public string PurposeOfBorrow { get; set; }
        public bool? IsReturn { get; set; }

        public string OnBehalf { get; set; }
        public string BorrowFrom { get; set; }
        public string CustomerToSend { get; set; }




    }
}
