﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppDispenserDispensingDrumDetailModel
    {
        public long DispenserDispensingDrumDetailsId { get; set; }
        public long DispenserDispensingLineId { get; set; }
        public string MaterialNo { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public decimal? Weight { get; set; }
        public string WeighingPhoto { get; set; }
        public byte[] WeighingPhotoStream { get; set; }
    }
}
