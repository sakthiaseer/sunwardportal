﻿using System;
using System.Collections.Generic;



namespace APP.EntityModels
{
    public class WikiListModel
    {
        public string ShortDescription { get; set; }
        public long PageID { get; set; }
        public string Title { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public bool RequiredPermission { get; set; }
        public DateTime? AddedDate { get; set; }
        public bool? IsPublishToAll { get; set; }
        public long? OwnerID { get; set; }
        public List<string> PlantCode { get; set; }
        public string WikiCompany { get; set; }
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
    }


    public class WikiDetailModel
    {
        public long PageID { get; set; }
        public long? CategoryId { get; set; }
        public long? DepartmentId { get; set; }
        public long? LanguageId { get; set; }
        public long? ParentPageId { get; set; }
        public long? GroupId { get; set; }
        public long? SectionId { get; set; }
        public string Title { get; set; }
        public string PageContent { get; set; }
        public string EditContent { get; set; }
        public string ShortDescription { get; set; }
        public string Notes { get; set; }
        public long? WikiSearchId { get; set; }
        public string SessionId { get; set; }
        public string SearchDocument { get; set; }
        public string WorkingDocument { get; set; }
        public bool? NoTransalation { get; set; }
        public bool? IsChinese { get; set; }
        public string ProposedTag { get; set; }
        public bool? IsMalay { get; set; }
        public int? MethodOfContent { get; set; }
        public bool? IsChangeApproval { get; set; }
        public bool? IsGmprelated { get; set; }
        public bool? SmecommentRequired { get; set; }
        public DateTime? SmedueDate { get; set; }
        public long? PrimaryApproverId { get; set; }
        public long? NextApproverId { get; set; }
        public bool? ApproveMySelf { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public long? TableId { get; set; }
        public long? WorkFlowId { get; set; }
        public int? VersionType { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public bool? UnderConstruction { get; set; }
        public bool? NeedFurtherReview { get; set; }
        public string SameControlStatement { get; set; }
        public string ControlStatement { get; set; }
        public long? OwnerId { get; set; }
        public long? Assignee { get; set; }
        public string ReviewComment { get; set; }
        public DateTime? PublishDate { get; set; }

        public DateTime? StartReviewDate { get; set; }
        public bool? ContentApproved { get; set; }
        public bool? Comment { get; set; }
        public string CommentDocument { get; set; }
        public string IssueNo { get; set; }
        public string RevisionNo { get; set; }
        public string ChangeControlNo { get; set; }

        public DateTime? ImplementationDate { get; set; }
        public string GroupReference { get; set; }
        public string NextAction { get; set; }
        public bool? IsPublishToAll { get; set; }
        public bool? IsTestPage { get; set; }
        public bool? IsReturned { get; set; }
        public long? SmechairPerson { get; set; }
        public bool? ApproveEditPage { get; set; }
        public string ArchiveReason { get; set; }
        public int? TypeOfRightId { get; set; }
        public int StatusCodeId { get; set; }
        public int? SecondaryStatusId { get; set; }
        public long AddedByUserId { get; set; }

        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }



        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
        public string CompanyName { get; set; }
        public string DocumentType { get; set; }
        public string Topic { get; set; }
        public string Owner { get; set; }
        public string TrainingMethod { get; set; }

        public bool RequiredPermission { get; set; }
        public bool PrintPermission { get; set; }
        public bool IsMorePage { get; set; }

        public int TotalLikes { get; set; }
        public int TotalComments { get; set; }

        public List<WikiCommentModel> Comments { get; set; }
    }

    public class WikiCommentModel
    {
        public long CommentID { get; set; }
        public long WikiPageID { get; set; }
        public long? AddedByUserID { get; set; }
        public string Comment { get; set; }
        public string AddedByUser { get; set; }
        public DateTime AddedDate { get; set; }
        public bool? IsEdit { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class WikiAccessRightModel
    {
        public long WikiAccessRightID { get; set; }
        public Nullable<long> GroupID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<long> PageID { get; set; }
        public string Group { get; set; }
        public List<string> User { get; set; }
        public string UserListID { get; set; }
        public int? TypeOfRights { get; set; }
        public string Type { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string SessionID { get; set; }
        public int? Duty { get; set; }
        public Nullable<bool> IsPrintable { get; set; }
        public Nullable<bool> IsEdit { get; set; }
        public bool IsTemp { get; set; }
        public bool IsSelect { get; set; }
        public string StrAction { get; set; }

        public long StatusCodeID { get; set; }
    }

    public class LinkPageModel
    {
        public long PageID { get; set; }
        public string Title { get; set; }
        public bool RequiredPermission { get; set; }
        public int Status { get; set; }
        public long PageLinkConditionID { get; set; }
        public string Code { get; set; }
        public string LinkCondition { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    public class PageConditionModel
    {
        public long PageLinkConditionId { get; set; }
        public string Code { get; set; }
        public string LinkCondition { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }


    public class DocumentModel
    {
        public long DocumentID { get; set; }
        public string LinkFileName { get; set; }
        public string UserAccess { get; set; }
        public bool? IsSpecialFile { get; set; }
        public List<string> UserAccessID { get; set; }
        public bool HasAccess { get; set; }

    }


    public class WikiSearchModel
    {
        public string Title { get; set; }

        public long? DocumentTypeID { get; set; }
        public long? CategoryID { get; set; }
        public long? DepartmentID { get; set; }
        public long? CompanyID { get; set; }
        public long? LanguageID { get; set; }
        public string DocumentNo { get; set; }
        public string ShortDesc { get; set; }
        public long? TagID { get; set; }
        public long? TopicID { get; set; }
        public long? OwnerID { get; set; }
        public long? TrainerID { get; set; }
        public long? PriApprovalID { get; set; }
        public long? NextApprovalID { get; set; }
        public long? TrainingMethodID { get; set; }
        public long? StatusID { get; set; }
        public long? SecondStatusID { get; set; }
        public bool? NoTranslate { get; set; }
        public bool? IsChinese { get; set; }
        public bool? IsMalay { get; set; }
        public bool? IsGMP { get; set; }
        public bool? IsSME { get; set; }
        public bool? IsTest { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ImplementDate { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public DateTime? ReviewDate { get; set; }
        public DateTime? ExpectedDate { get; set; }

        public long ByUserID { get; set; }


    }

}
