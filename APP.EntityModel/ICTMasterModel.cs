﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ICTMasterModel : BaseModel
    {
        public long ICTMasterID { get; set; }
        public long? CompanyID { get; set; }
        public long? PlantID { get; set; }
        public long? ParentICTID { get; set; }
        
        public int MasterType { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public long? LayoutPlanID { get; set; }
        public long? SiteID { get; set; }
        public long? LocationID { get; set; }
        public long? ZoneID { get; set; }
        public long? AreaID { get; set; }

        public string VersionNo { get; set; }
        public DateTime EffectiveDate { get; set; }
        public List<ICTLayoutPlanTypesModel> LayoutPlanTypes { get; set; }
        public bool IsAllLoction { get; set; }
        public bool IsSpecificAreaLoction { get; set; }
        public List<long?> ICTMasterIDs { get; set; }
        public int SelectMasterType { get; set; }
        public string Site { get; set; }
        public string Zone { get; set; }
        public string Area { get; set; }
        public string Location { get; set; }

        public string SiteName { get; set; }
        public string ZoneName { get; set; }
        public string AreaName { get; set; }
        public string LocationName { get; set; }
        public string SpecificAreaName { get; set; }
        public string LocationDropDown { get; set; }
        public string LocationDescription { get; set; }

    }

    public class ICTMobileMaster
    {
        public long ICTMasterID { get; set; }
        public long? CompanyID { get; set; }
        public long? ParentICTID { get; set; }
        public int MasterType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
