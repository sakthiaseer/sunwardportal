﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FulfillmentSearchParam
    {

        public string Search { get; set; }
        public string Company { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime FilterByDate { get; set; }
        public int? ReportOptionID { get; set; }
        public long? UserId { get; set; }

        public bool IsPromised { get; set; }
        public bool IsNegativeStock { get; set; }

        public List<long?> MethodCodeIds { get; set; }

    }
}
