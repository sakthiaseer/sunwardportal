﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ShortcutModel : BaseModel
    {
        public long ShortCutID { get; set; }
        public string Name { get; set; }
        public int? StatusCodeId { get; set; }
    }
}
