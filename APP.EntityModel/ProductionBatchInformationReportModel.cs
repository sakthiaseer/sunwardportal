﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchInformationReportModel
    {
        public long ProductionBatchInformationID { get; set; }
        public long ProductionBatchInformationLineID { get; set; }
        public string ProductDescription { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? DisposalDate { get; set; }
        public string QRCodeData { get; set; }
        public long? QtyPack { get; set; }
        public string QtyPackUnits { get; set; }
        public string CompanyName { get; set; }
        public string TicketNo { get; set; }
        public string ProductionOrderNo { get; set; }
        public string UnitsofBatchSize { get; set; }
        public long? ItemId { get; set; }
        public string ProductCode { get; set; }
    }
}
