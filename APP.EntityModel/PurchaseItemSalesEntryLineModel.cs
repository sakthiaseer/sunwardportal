﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PurchaseItemSalesEntryLineModel : BaseModel
    {
        public long PurchaseItemSalesEntryLineID { get; set; }
        public string QuotationNo { get; set; }
        public long? SalesOrderEntryID { get; set; }
        public long? SoByCustomersId { get; set; }
        public int? OrderQty { get; set; }
        public long? OrderCurrencyID { get; set; }
        public decimal? SellingPrice { get; set; }
        public string FOC { get; set; }
        public string SchdAgreementNo { get; set; }
        public string TenderAgencyNo { get; set; }
        public long? ShipToAddressID { get; set; }
        public long? UOMID { get; set; }
        public long? UOMfirstID { get; set; }
        public long? UOMsecondID { get; set; }
        public bool? IsAllowExtension { get; set; }
        public int? ExtensionMonth { get; set; }
        public int? ExtensionQuantity { get; set; }
        public bool? IsLotDeliveries { get; set; }
        public DateTime? DeliveryDateOfOrder { get; set; }
        public long? ItemId { get; set; }
        public decimal? OptionalQty { get; set; }
        public string PricePerQty { get; set; }
        public bool? IsFOC { get; set; }
        public string ProductNO { get; set; }
        public string OrderCurrency { get; set; }
        public string UOM { get; set; }
        public string Description { get; set; }

        public string ProductGroupCode { get; set; }
        public string ProductGroupUom { get; set; }


    }
}
