﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavItemBatchNoModel : BaseModel
    {
        public long NavItemBatchNoID { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string BatchNo { get; set; }
        public string BatchSize { get; set; }
        public string Description { get; set; }


    }
}
