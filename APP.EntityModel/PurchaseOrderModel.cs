﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PurchaseOrderModel:BaseModel
    {
        public long PurchaseOrderId { get; set; }
        public int? TypeOfPoId { get; set; }
        public long? TypeId { get; set; }
        public DateTime? DateOfPo { get; set; }
        public long? FromCompanyId { get; set; }
        public long? VendorNameId { get; set; }
        public string VersionNo { get; set; }
        public string Ponumber { get; set; }
        public string TypeOfPo { get; set; }
        public string FromCompany { get; set; }
        public string VendorName { get; set; }

            public Guid? SessionId { get; set; }

        
    }
}
