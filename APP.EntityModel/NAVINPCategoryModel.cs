﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVINPCategoryModel
    {
        public long NAVINPCategoryID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
