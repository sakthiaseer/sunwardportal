﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlannerCommentModel
    {
        public long InpreportId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public string ItemCategory { get; set; }
        public string Comments { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Company { get; set; }
    }
}
