﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationUserRoleModel
    {
        public long UserRoleID { get; set; }
        public long? UserID { get; set; }
        public long? RoleID { get; set; }
    }
}
