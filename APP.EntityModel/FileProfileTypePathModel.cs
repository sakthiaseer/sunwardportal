﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FileProfileTypePathModel
    {
       
            public long FileProfileTypeId { get; set; }
            public string Name { get; set; }
            public List<string> PathNames { get; set; }
        public long? ParentId { get; set; }
        public bool? IsExpiryDate { get; set; }
        public string ProfileName { get; set; }
        public bool? IsAllowMobileUpload { get; set; }
        public bool? IsDocumentAccess { get; set; }
        public DocumentPermissionModel DocumentPermissionData { get; set; }

    }
}
