﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityRoutineAppLineModel:BaseModel
    {
        public long ProductionActivityRoutineAppLineId { get; set; }
        public long? ProductionActivityRoutineAppId { get; set; }
        public string ActionDropdown { get; set; }
        public long? ProdActivityActionId { get; set; }
        public long? ProdActivityCategoryId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public bool? IsTemplateUpload { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public long? ProductActivityCaseLineId { get; set; }
        public long? NavprodOrderLineId { get; set; }
        public string Comment { get; set; }
        public bool? QaCheck { get; set; }
        public bool? IsOthersOptions { get; set; }
        public long? ProdActivityResultId { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? ProdActivityCategoryChildId { get; set; }
        public long? ProdActivityActionChildD { get; set; }
        public string TopicId { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public string ManufacturingProcessChild { get; set; }
        public string ProdActivityCategoryChild { get; set; }
        public string ProdActivityActionChild { get; set; }
    }
}
