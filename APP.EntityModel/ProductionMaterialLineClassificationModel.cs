﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMaterialLineClassificationModel:BaseModel
    {
        public long MaterialClassificationId { get; set; }
        public string NavisionType { get; set; }
        public long? DrugClassificationId { get; set; }
        public long? ProductionMaterialLineId { get; set; }
    }
}
