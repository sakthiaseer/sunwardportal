﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistributorReplenishmentLineModel : BaseModel
    {
        public long DistributorReplenishmentLineId { get; set; }
        public long? DistributorReplenishmentId { get; set; }
        public decimal? ReplenishmentTiming { get; set; }
        public bool? NeedVersionChecking { get; set; }
        public string NeedVersionCheckingFlag { get; set; }
        public decimal? MaxHoldingStock { get; set; }
        public decimal? NoOfMonthToReplenish { get; set; }
        public bool? IsExceptionalItem { get; set; }
        public long? ProductNameId { get; set; }
        public string ProductName { get; set; }

    
    }
}
