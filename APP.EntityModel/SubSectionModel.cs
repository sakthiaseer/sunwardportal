﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SubSectionModel : BaseModel
    {
        public long SubSectionID { get; set; }
        public long? SectionID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public string SectionName { get; set; }
        public string DepartmentName { get; set; }
        public string DivisionName { get; set; }
        public string CompanyDivisionName { get; set; }
        public string ProfileCode { get; set; }
    }
}
