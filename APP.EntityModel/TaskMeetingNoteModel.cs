﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskMeetingNoteModel : BaseModel
    {
        public long? TaskMeetingNoteID { get; set; }
        public DateTime? MeetingDate { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? TaskAttachmentID { get; set; }
        public long? AddedByUserID { get; set; }
        public string MeetingNotes { get; set; }
       







    }
}
