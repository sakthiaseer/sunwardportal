﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MediaModel
    {
        public long VideoId { get; set; }
        public string Thumb { get; set; }
        public string Src { get; set; }
        public string SrcType { get; set; }
        public string Type { get; set; }
        public string Caption { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
