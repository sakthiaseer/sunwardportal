﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesEntryContractModel
    {
        public long SalesOrderID { get; set; }
        public string ContractNo { get; set; }
        public string OrderByName { get; set; }
        public string CustomerName { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public List<SalesEntryProduct> SalesEntryProducts { get; set; } = new List<SalesEntryProduct>();

    }
    public class SalesEntryProduct
    {
        public long PurchaseItemSalesEntryLineID { get; set; }
        public long SalesOrderID { get; set; }
        public long ItemID { get; set; }
        public string ProductName { get; set; }
        public string BUOM { get; set; }
        public string PUOM { get; set; }
        public string PackUom { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long? OrderCurrencyId { get; set; }
        public string OrderCurrency { get; set; }
        public decimal? SellingPrice { get; set; }
    }
}
