﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GroupSimulationModel:BaseModel
    {
        public long? SimulationForForecastPlanningId { get; set; }
        public long? ItemId { get; set; }
        public long? MethodCodeId { get; set; }
        public string MethodCode { get; set; }
        public long? TypeOfProdOrderId { get; set; }
        public long? SellingStatusId { get; set; }
        public List<ApproveReceipeItemsModel> ApproveReceipeItems { get; set; }
    }
    public class ApproveReceipeItemsModel
    {
        public string BatchSize { get; set; }
        public string Remarks { get; set; }
        public string ReceipeNo { get; set; }
        public string Machine { get; set; }
    }
}
