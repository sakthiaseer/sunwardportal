using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemClassificationTransPortModel : BaseModel
    {
        public long TransportId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileID { get; set; }
        public long? CompanyId { get; set; }
        public string NameOfTransportation { get; set; }
        public string Purpose { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string TransportCompanyName { get; set; }
        public string ProfileName { get; set; }
    }
}
