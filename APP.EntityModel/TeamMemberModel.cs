﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TeamMemberModel : BaseModel
    {
        public long TeamMemberID { get; set; }
        public long? MemberID { get; set; }
        public long? TeamID { get; set; }
        public string TeamName { get; set; }
        public string Description { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
