﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class QuotationHistoryModel : BaseModel
    {
        public long QuotationHistoryId { get; set; }
        public long? CompanyId { get; set; }
        public string SwreferenceNo { get; set; }
        public DateTime? Date { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerRefNo { get; set; }
        public string CustomerName {get;set;}

         public Guid? SessionId { get; set; }

        public string Company { get; set; }
        public string Product { get; set; }
        public long? ProductId { get; set; }
        public decimal? Qty { get; set; }
        public long? Uomid { get; set; }
        public string UOM { get; set; }
        public string OfferCurrency { get; set; }
        public decimal? OfferPrice { get; set; }
        public long? OfferUomid { get; set; }
        public long? OfferCurrencyId { get; set; }
        public long? QuotationHistorylineId { get; set; }
    }

    public class QuotationHistoryLineModel : BaseModel
    {
        public long QuotationHistoryLineId { get; set; }
        public long? QutationHistoryId { get; set; }
        public string Source { get; set; }
        public decimal? Quantity { get; set; }
        public long? Uomid { get; set; }
        public string UOM { get; set; }
        public long? PackingId { get; set; }
        public string Packing { get; set; }
        public long? OfferCurrencyId { get; set; }
        public string OfferCurrency { get; set; }
        public decimal? OfferPrice { get; set; }
        public long? OfferUomid { get; set; }
        public string OfferUOM { get; set; }
        public decimal? Focqty { get; set; }
        public long? ShippingTermsId { get; set; }
        public string ShippingTerms { get; set; }
        public bool? IsTenderExceed { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsAwarded { get; set; }

        public long? ProductId { get; set; }
        public string ProductName { get; set; }

        public string Productdescription {get;set;}
        public string Remarks { get; set; }

    }

    public class QuotationHistoryDocumentModel : BaseModel
    {
        public long QuotationHistoryDocumentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool IsImage {get;set;}     

        public string DocumentType { get; set; }
        public bool Uploaded { get; set; }     
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public string ProfileNo { get; set; }
        public DocumentPermissionModel DocumentPermissionModel { get; set; }
    }

    public class QuotationHistoryReportModel
    {
        public long? QuotationHistoryId { get; set; }
        public long? ProductId { get; set; }
        public long? QuotationHistoryLineId { get; set; }
        public DateTime? DateOfOffer { get; set; }
        public string Company { get; set; }
        public string SwreferenceNo { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public string UOM { get; set; }
        public bool? IsAwarded { get; set; }
        public string OfferCurrency { get; set; }
        public decimal? OfferPrice { get; set; }
        public Guid? SessionId { get; set; }
        public string CustomerRefNo { get; set; }


    }
}
