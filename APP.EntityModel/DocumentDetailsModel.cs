﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentDetailsModel
    {
        public long? DocumentID { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}
