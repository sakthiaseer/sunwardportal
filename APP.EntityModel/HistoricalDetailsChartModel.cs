﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HistoricalDetailsChartModel
    {
        public List<string> Yeardate { get; set; }
        public List<decimal> Yearsales { get; set; }
        public List<string> Monthdate { get; set; }
        public List<decimal> Monthsales { get; set; }
        public SortedList Monthwisesales { get; set; }
    }
}
