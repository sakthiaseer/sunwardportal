﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationFormSearchListModel
    {
        public List<ApplicationFormSearchModel> ApplicationFormSearchList { get; set; }
    }
}
