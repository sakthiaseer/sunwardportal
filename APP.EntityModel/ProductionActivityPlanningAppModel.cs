﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityPlanningAppModel:BaseModel
    {
        public long ProductionActivityPlanningAppId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProdActivityActionId { get; set; }
        public string ActionDropdown { get; set; }
        public long? ProdActivityCategoryId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public bool? IsTemplateUpload { get; set; }
        public string ProdOrderNo { get; set; }
        public string ProdActivityAction { get; set; }
        public string ProdActivityCategory { get; set; }
        public string ManufacturingProcess { get; set; }
        public string IsTemplateUploadFlag { get; set; }
        public long? ProductActivityCaseLineId { get; set; }
        public string NameOfTemplate { get; set; }
        public long? DocumentId { get; set; }
        public string Comment { get; set; }
        public long? NavprodOrderLineId { get; set; }
        public Guid? LineSessionId { get; set; }
        public string LineComment { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public string Link { get; set; }
        public bool? QaCheck { get; set; } = false;
        public string RePlanRefNo { get; set; }
        public int? OrderLineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public string BatchNo { get; set; }
        public long? LocationToSaveId { get; set; }
        public bool? IsOthersOptions { get; set; }
        public long? DocumentParentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public bool? IsLocked { get; set; }
        public long? LockedByUserId { get; set; }
        public string LockedByUser { get; set; }
        public long? NotifyCount { get; set; }
        public long? ProdActivityResultId { get; set; }
        public string ProdActivityResult { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TopicId { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? ProdActivityCategoryChildId { get; set; }
        public long? ProdActivityActionChildD { get; set; }
        public string ManufacturingProcessChild { get; set; }
        public string ProdActivityCategoryChild { get; set; }
        public string ProdActivityActionChild { get; set; }
        public DocumentPermissionModel DocumentPermissionData { get; set; }
        public string ProdOrderNoDesc { get; set; }
        public string Type { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public string QaCheckUser { get; set; }
        public string FilePath { get; set; }
        public List<ProductionActivityPlanningAppLineQaCheckerModel> ProductionActivityPlanningAppLineQaCheckerModels { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public int? SupportDocCount { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public List<long> ResponsibilityUsers { get; set; }
        public string TicketNo { get; set; }
    }
}
