﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentRolePermissionModel : BaseModel
    {
        public long DocumentRolePermissionID { get; set; }
        public long? DocumentID { get; set; }
        public long? DocumentPermissionID { get; set; }
        public long? DocumentRoleID { get; set; }
        public string FileName { get; set; }
        public string PermissionName { get; set; }
        public string DocumentRoleName { get; set; }

        //Document Permission
        public bool? IsRead { get; set; }
        public bool? IsCreateFolder { get; set; }
        public bool? IsCreateDocument { get; set; }
        public bool? IsSetAlert { get; set; }
        public bool? IsEditIndex { get; set; }
        public bool? IsRename { get; set; }
        public bool? IsUpdateDocument { get; set; }
        public bool? IsCopy { get; set; }
        public bool? IsMove { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsRelationship { get; set; }
        public bool? IsListVersion { get; set; }
        public bool? IsInvitation { get; set; }
        public bool? IsSendEmail { get; set; }
        public bool? IsDiscussion { get; set; }
        public bool? IsAccessControl { get; set; }
        public bool? IsAuditTrail { get; set; }
        public bool? IsRequired { get; set; }
    }
}
