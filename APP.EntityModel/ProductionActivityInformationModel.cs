﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityInformationModel:BaseModel
    {
        public long ProductionActivityInformationId { get; set; }
        public long? CountryId { get; set; }
        public long? TicketNo { get; set; }
        public long? BatchNo { get; set; }
        public long? ProcessId { get; set; }
        public long? UploadDocumentId { get; set; }
        public string Country { get; set; }
        public string TicketNos { get; set; }
        public string BatchNos { get; set; }
        public string Process { get; set; }
        public string UploadDocumentName { get; set; }
    }
}
