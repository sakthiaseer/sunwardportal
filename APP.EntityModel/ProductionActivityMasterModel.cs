﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityMasterModel:BaseModel
    {
        public long ProductionActivityMasterId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? CategoryActionId { get; set; }
        public long? ActionId { get; set; }
        public string Reference { get; set; }
        public string CheckerNotes { get; set; }
        public string Upload { get; set; }
        public string ManufacturingProcess { get; set; }
        public string CategoryAction { get; set; }
        public string Action { get; set; }
    }
}
