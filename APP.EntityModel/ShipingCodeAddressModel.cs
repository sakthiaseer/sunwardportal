﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ShipingCodeAddressModel
    {
        public long? ShipingCodeAddressId { get; set; }
        public string ShipingCodeType { get; set; }
        public string AddressType { get; set; }
        public string ShipingAddress { get; set; }
        public string LocationNo { get; set; }
        public string LocationNoName { get; set; }
        public string LocationName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }

    }
}
