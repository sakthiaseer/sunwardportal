﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPProductNavisionLineModel : BaseModel
    {
        public long FPProductNavisionLineID { get; set; }
        public long? DatabaseID { get; set; }
        public long? NavisionID { get; set; }
        public string LinkProfileRefNo { get; set; }
        public string MasterProfileRefNo { get; set; }
        public string ProfileRefNo { get; set; }
        public string Database { get; set; }
        public string Navision { get; set; }

    }
}
