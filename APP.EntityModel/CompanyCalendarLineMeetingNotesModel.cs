﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyCalendarLineMeetingNotesModel:BaseModel
    {
        public long CompanyCalendarLineMeetingNotesId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string MeetingData { get; set; }
        public string Subject { get; set; }
        public bool? IsTitleFlag { get; set; }
    }
}
