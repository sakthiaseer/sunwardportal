﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemClassificationHeaderModel : BaseModel
    {
        public long ItemClassificationHeaderID { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileID { get; set; }
        public string MaterialNo { get; set; }
        public string MaterialName { get; set; }
        public long? MaterialID { get; set; }
        public string AlsoKnownAs { get; set; }
        public long? UOMID { get; set; }

        public string ProfileName { get; set; }
        public string UOM { get; set; }
        public string FPMaterial { get; set; }
        public string LinkProfileReferenceNo { get; set; }
    }
}
