﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TenderPeriodPricingModel:BaseModel
    {
        public long TenderPeriodPricingId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public string ContractNo { get; set; }
        public string ContractNoName { get; set; }
        public DateTime? EffectiveMonth { get; set; }
        public string CustomerName { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
    }
}
