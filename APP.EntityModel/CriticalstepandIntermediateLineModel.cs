﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CriticalstepandIntermediateLineModel : BaseModel
    {
      public long  CriticalstepandIntermediateLineID { get; set; }
      public long? CriticalstepandIntermediateID { get; set; }
        public long? TestID { get; set; }
        public long? TestStageID { get; set; }
        public List<long?>  SamplingFrequencyIDs { get; set; }
        public decimal? SampleFrequencyValue { get; set; }
        public long? UnitsID { get; set; }
        public long? SpecificationLimitsID { get; set; }

        public List<ApplicationMasterDetailModel> ApplicationMasterList { get; set; }
        public List<CriticalSamplingFrequencyModel> SamplingFrequncyList { get; set; }
       
        public decimal? BatchSizeValue { get; set; }
        public decimal? TimelineValue { get; set; }
        public decimal? TimepointValue { get; set; }
        public string Test { get; set; }
        public string TestStage { get; set; }
       
        public string SamplingFrequency { get; set; }
        public string Units { get; set; }
        public string SpecificationLimits { get; set; }


    }
}
