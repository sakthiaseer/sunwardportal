using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CurrencyModel
    {      
       
        public long CurrencyId { get; set; }        
        public string CurrencyName { get; set; }
        public long ApplicationMasterID {get;set;}
       
    }
}
