﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationMasterChildModel:BaseModel
    {
        public long ApplicationMasterChildId { get; set; }
        public long? ApplicationMasterParentId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public string ApplicationNameList { get; set; }
        public string DisplayName { get; set; }
    }
}
