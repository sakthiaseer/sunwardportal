﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InstructionTypeModel : BaseModel
    {
        public long InstructionTypeID { get; set; }
        public string DocumentName { get; set; }
        public int? TypeID { get; set; }
        public string No { get; set; }
        public string InstructionNo { get; set; }
        public string VersionNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string Link { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
        public long? CalibrationServiceInformationID { get; set; }
    }
}
