﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationFormModel : BaseModel
    {
        public long FormID { get; set; }
        //public long ParentID { get; set; }
        public string Name { get; set; }
        public string PathURL { get; set; }
        public string ModuleName { get; set; }
        public string FormType { get; set; }
        public string TableName { get; set; }
        public long? ProfileID { get; set; }
        public long? FileProfileTypeID { get; set; }
        public string ProfileName { get; set; }
        public int? ClassificationTypeId { get; set; }
        public string ClassificationType { get; set; }
        public string ScreenId { get; set; }
        public long? PersonInchargeId { get; set; }
        public List<long> ApplicationFormSubIDs { get; set; }
    }
}
