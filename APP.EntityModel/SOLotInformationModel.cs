﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SOLotInformationModel : BaseModel
    {
        public long SolotInformationId { get; set; }
        public long? SowithoutBlanketOrderId { get; set; }
        public decimal? OrderQty { get; set; }
        public long? OrderCurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public long? PriceUnitId { get; set; }
        public bool? IsFoc { get; set; }
        public string Foc { get; set; }
        public DateTime? RequestShipmentDate { get; set; }

        public string OrderCurrency { get; set; }
        public string PriceUnit { get; set; }
        public string UOM { get; set; }
    }
}
