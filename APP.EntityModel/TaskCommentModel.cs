﻿using System;
using System.Collections.Generic;

namespace APP.EntityModel
{
    public class TaskCommentModel : BaseModel
    {
        public long TaskCommentID { get; set; }
        public long? TaskMasterID { get; set; }
        public long? MainTaskId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }

        public string CommentedByName { get; set; }
        // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy HH:mm:ss tt}")]
        public DateTime? CommentedDate { get; set; }
        public long? ParentCommentId { get; set; }
        public bool? OverDueAllowed { get; set; }
        public List<TaskCommentModel> TaskCommentModels { get; set; }
        public CommentAttachmentModel CommentAttachmentModel { get; set; }
        public TaskMasterModel TaskMasterModel { get; set; }
        public string CssClass { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsPin { get; set; }
        public bool? IsQuote { get; set; }
        public long? QuoteCommentId { get; set; }
        public long? PinnedBy { get; set; }
        public string PinnedByUser { get; set; }
        public bool? IsEdited { get; set; }
        public long? EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }
        public bool? IsDocument { get; set; }
        public long? DocumentId { get; set; }
        public Guid? SessionID { get; set; }
        public string TaskSubject { get; set; }
        public string TaskName { get; set; }
        public string AssignedType{ get; set; }
        public long? TaskId { get; set; }       
        public List<long> MessageAssignedTos { get; set; }
        public List<string> AssignedToUsers { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? MyDueDate { get; set; }
        public DateTime? SubjectDueDate { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public int? SubCommentStatusCodeID { get; set; }
        public bool ViewReply { get; set; } = false;
        public string ReplyLabel { get; set; } = "view replies";
        public string SubjectStatus { get; set; }

        public DateTime? LastCommentedDate { get; set; }
        public string LastCommentedBy { get; set; }
        public string QuoteCommentUser { get; set; }
        public DateTime?  QuoteCommentDate { get; set; }
        public string QuoteComment { get; set; }
        public string DueIn { get; set; }
        public string Baseurl { get; set; }
        public bool? IsNoDueDate { get; set; }
        public bool? IsClosed { get; set; } = false;
        public InviteUserModel InviteUserData { get; set; }
        public List<long?> MessageAssignedToIds { get; set; }
    }
}
