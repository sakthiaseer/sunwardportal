﻿using System;

namespace APP.EntityModel
{
    public class ChatUserModel : BaseModel
    {
        public long ChatUserId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public Guid? SessionId { get; set; }
        public string ConnectionId { get; set; }
        public bool? IsOnline { get; set; } 
        public string ImageURL { get; set; }
        public string LastMessage { get; set; }
        public DateTime? LastChattedDate { get; set; }
    }
}
