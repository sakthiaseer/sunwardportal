﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchInformationLineModel : BaseModel
    {
        public long ProductionBatchInformationLineID { get; set; }
        public long? ProductionBatchInformationID { get; set; }
        public long? ProductCodeID { get; set; }
        public string BatchNo { get; set; }
        public long? ItemId { get; set; }
        public long? QtyPack { get; set; }
        public long? QtyPackUnitsID { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string ProductCode { get; set;}
        public string Product { get; set; }
        public string PackingUnits { get; set; }
        public string ProductCodeName { get; set; }
        public string ProductUOM { get; set; }


    }
}
