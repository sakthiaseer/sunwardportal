﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyHolidaysModel :BaseModel
    {
       
        public long CompanyHolidayID { get; set; }
        public int? CalendarYear { get; set; }
        public long? PlantID { get; set; }
        public long? StateID { get; set; }
        public long? CountryID { get; set; }
        public string State { get; set; }
        public decimal? NoOfHolidays { get; set; }
        public string Reference { get; set; }

        public ICollection<CompanyHolidayDetailModel> CompanyHolidayDetail { get; set; }
    }
}
