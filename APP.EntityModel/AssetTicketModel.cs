﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AssetTicketModel:BaseModel
    {
        public long AssetTicketId { get; set; }
        public long? AssetId { get; set; }
        public int? UrgencyStatus { get; set; }
        public int? ImpactStatus { get; set; }
        public int? PriorityStatus { get; set; }
        public string Description { get; set; }
        public string AssetName { get; set; }
        public string UrgencyStatusName { get; set; }
        public string ImpactStatusName { get; set; }
        public string PriorityStatusName { get; set; }
    }
}
