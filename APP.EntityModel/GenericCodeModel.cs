﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GenericCodeModel
    {
        public long GenericCodeId { get; set; }
        public string Code { get; set; }
        public string UOM { get; set; }
        public string CodeUOM { get; set; }
        public string Description { get; set; }
    }
}
