﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentUserRoleModel
    {
        public long DocumentUserRoleID { get; set; }
        public long? UserID { get; set; }
        public long? RoleID { get; set; }
        public long? PreviousRoleID { get; set; }
        public long? FolderID { get; set; }
        public long? DocumentID { get; set; }
        public long? UserGroupID { get; set; }
        public bool? IsFolderLevel { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? FolderAddedByUserID { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> DocumetUserRoleIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public bool IsSelected { get; set; } = false;
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string UserGroupName { get; set; }
        public string FileProfileTypeName { get; set; }
        public string FileName { get; set; }
        public string FolderName { get; set; }
        public List<long?> FileProfileTypeIds { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }

        public long? LevelID { get; set; }
        public List<long?> DocumentIds { get; set; }

    }
}
