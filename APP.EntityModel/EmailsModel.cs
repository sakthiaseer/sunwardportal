﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmailsModel:BaseModel
    {
        public long EmailsId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string FromEmails { get; set; }
        public string ToMails { get; set; }
        public string Ccmails { get; set; }
        public string Bccmails { get; set; }
        public bool isAttachment { get; set; } = false;
        public string FormId { get; set; }
        public List<PortalEmailAttachmentModel> emailAttachmentModels { get; set; }
    }
    public class PortalEmailAttachmentModel
    {
        public string FileName { get; set; }
        public string DocumentName { get; set; }
        public string DisplayName { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }
        public int? DocumentType { get; set; }
        public byte[] FileData { get; set; }
        public int? FileIndex { get; set; }
        public long? FileSize { get; set; }
    }
}
