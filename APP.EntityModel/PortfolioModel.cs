﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PortfolioModel : BaseModel
    {
        public long PortfolioId { get; set; }
       
        public long? OnBehalfId { get; set; }
        public long? PortfolioTypeId { get; set; }
        public long? CompanyId { get; set; }
        public string OnBehalfUser { get; set; }
        public string PortfolioType { get; set; }
        public List<long?> OnBehalfIds { get; set; }

    }
}
