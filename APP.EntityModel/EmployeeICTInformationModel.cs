﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeICTInformationModel : BaseModel
    {
        public long EmployeeICTInformationID { get; set; }
        public long? EmployeeID { get; set; }
        public long? SoftwareID { get; set; }
        public string LoginID { get; set; }
        public string Password { get; set; }
        public long? RoleID { get; set; }
        public List<long?> RoleIDs { get; set; }
        public string Software { get; set; }
        public string Role { get; set; }

        public bool? IsPortal { get; set; }
    }
}
