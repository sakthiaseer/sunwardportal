using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProdMonthAndYearModel : BaseModel
    {
        public string MonthId { get; set; }
        public string MonthName { get; set; }
    }
}
