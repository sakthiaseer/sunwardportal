﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyCalendarModel : BaseModel
    {
        public long CompanyCalendarId { get; set; }
        public DateTime? CalendarEventDate { get; set; }
        public long? CalenderEventId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public string CalenderEventName { get; set; }
        public string SiteName { get; set; }
        public string Title { get; set; }
        public DateTime? Start { get; set; }
        public long? UserId { get; set; }
        public bool? RequireAssistance { get; set; }
        public string RequireAssistanceFlag { get; set; }
        public string UserName { get; set; }
        public string TypeOfEventName { get; set; }
        public string Subject { get; set; }
        public TimeSpan? FromTime { get; set; }
        public TimeSpan? ToTime { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? EventDate { get; set; }
        public string TypeOfServiceName { get; set; }
        public long? CalenderStatusId { get; set; }
        public string CalenderStatus { get; set; }
        public long? OwnerId { get; set; }
        public string OwnerName { get; set; }
        public long? OnBehalfId { get; set; }
        public string OnBehalfName { get; set; }
        public long? AssistanceFromId { get; set; }
        public string AssistanceFrom { get; set; }
        public List<long?> AssistanceFromIds { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string Notes { get; set; }
        public long? CompanyCalendarIds { get; set; }
        public long? CompanyCalendarLineNotesId { get; set; }
        public long? TypeOfServiceId { get; set; }
        public bool? IsRead { get; set; }
    }
}
