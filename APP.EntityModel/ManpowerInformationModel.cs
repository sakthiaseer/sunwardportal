﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ManpowerInformationModel : BaseModel
    {
        public long ManpowerInformationID { get; set; }
        public string ProcessFromNo { get; set; }
        public string ProcessToNo { get; set; }
        public long? ManpowerNoID { get; set; }
        public long? StandardManufacturingProcessId { get; set; }

        public string ManpowerNo { get; set; }
    }
}
