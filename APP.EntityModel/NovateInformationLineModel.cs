﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NovateInformationLineModel : BaseModel
    {
        public long NovateInformationLineId { get; set; }
        public long? TenderInformationId { get; set; }
        public long? NovateFromId { get; set; }
        public string NovateFrom { get; set; }
        public long? NovateToId { get; set; }
        public string NovateTo { get; set; }
        public decimal? Quantity { get; set; }
    }
}
