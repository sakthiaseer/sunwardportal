﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SectionModel : BaseModel
    {
        public long SectionID { get; set; }
        public long? DepartmentID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool? IsWiki { get; set; }
        public bool? IsTestPaper { get; set; }
        public int? HeadCount { get; set; }        
        public string DepartmentName { get; set; }
        public string DivisionName { get; set; }
        public string CompanyDivisionName { get; set; }
        public string ProfileCode { get; set; }
    }
}
