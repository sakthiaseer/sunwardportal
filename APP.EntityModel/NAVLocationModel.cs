﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVLocationModel : BaseModel
    {
        public long NAVLocationId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long? PlantID { get; set; }
        public long? ItemID { get; set; }
        public string ItemNo { get; set; }


    }
}