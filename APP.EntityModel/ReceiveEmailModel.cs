﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ReceiveEmailModel:BaseModel
    {
        public long ReceiveEmailId { get; set; }
        public string MessageId { get; set; }
        public string Subject { get; set; }
        public int? EmailSalesStatusId { get; set; }
        public bool? IsAcknowledgement { get; set; }
        public string EmailSalesStatusName { get; set; }
        public string Sonumber { get; set; }
        public string IsAcknowledgementFlag { get; set; }
        public string NotRelatedDescription { get; set; }
        public bool? SameAsSalesOrderCompleteProcess { get; set; }
        public string SameAsSalesOrderCompleteProcessFlag { get; set; }
        public long? SalesOrderId { get; set; }
        public string HtmlFileName { get; set; }
        public string Description { get; set; }
        public long? EmailCategoryId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }
        public long? FileProfileTypeId { get; set; }
    }
}
