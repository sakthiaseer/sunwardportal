﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ICBMPLineModel : BaseModel
    {
        public long ICBMPLineID { get; set; }
        public long? ICBMPID { get; set; }
        public long? ManufacturingStepsID { get; set; }
        public long? MachineGroupingID { get; set; }
        public long? MachineID { get; set; }
        public string WILink { get; set; }

        public List<long?> DetermineFactorIDs { get; set; }
    }
}
