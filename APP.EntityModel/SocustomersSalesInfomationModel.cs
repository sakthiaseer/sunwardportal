﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SocustomersSalesInfomationModel:BaseModel
    {
        public long SocustomersSalesInfomationId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? PaymentModeId { get; set; }
        public long? ActivationOfOrderId { get; set; }
        public string PaymentModeName { get; set; }
        public string ActivationOfOrderName { get; set; }
    }
}
