﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class APPDispenserDispensingModel : BaseModel
    {
        public long DispenserDispensingId { get; set; }
        public string WorkOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public int? TotalItem { get; set; }
        public int? WeighingMaterial { get; set; }
        public List<APPDispenserDispensingLineModel> DispensingLines { get; set; }
    }
}
