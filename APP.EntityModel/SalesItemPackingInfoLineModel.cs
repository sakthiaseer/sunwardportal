﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesItemPackingInfoLineModel : BaseModel
    {
        public long SalesItemPackingInfoLineId { get; set; }
        public long? SalesItemPackingInfoId { get; set; }
        public int? SalesFactor { get; set; }
        public long? SalesPerPack { get; set; }
        public string Fpname { get; set; }
        public string SalesPerPackName { get; set; }
        public List<long?> PackageRequirementIds { get; set; }
    }
}
