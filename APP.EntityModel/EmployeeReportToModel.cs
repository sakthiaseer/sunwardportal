﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeReportToModel
    {
        public long EmployeeReportToID { get; set; }
        public long EmployeeID { get; set; }
        public long ReportToID { get; set; }
    }
}
