﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentProfileItem
    {
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string Abbreviation1 { get; set; }
        public bool IsCompany { get; set; }
        public bool IsDepartment { get; set; }
        public bool IsSection { get; set; }
        public bool IsSubSection { get; set; }
        public int? StatusCodeID { get; set; }
    }
}
