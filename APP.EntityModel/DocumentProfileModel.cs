﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentProfileModel : BaseModel
    {
        public long DocumentProfileID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? AutoGenerateReferenceNo { get; set; }
        public int? RunningReferenceNo { get; set; }
    }
}
