﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SellingPricingTiersModel
    {
        public long SellingPricingTiersID { get; set; }
        public long? SellingPriceInfoID { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? TotalQtyBouns { get; set; }
        public decimal? TotalQtyBounsPrice { get; set; }
        public long? PricingMethodId { get; set; }
        public long? SellingCurrencyId { get; set; }
        public string SellingCurrency { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? MinQty { get; set; }
    }
}
