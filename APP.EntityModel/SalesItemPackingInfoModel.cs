﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesItemPackingInfoModel : BaseModel
    {
        public long SalesItemPackingInfoId { get; set; }
        public long? ManufacturingSiteID { get; set; }
        public long? FinishProductGeneralInfoID { get; set; }
        public long? FinishProductGeneralInfoLineID { get; set; }
        public string ManufacturingSite { get; set; }
        public string RegisterCountry { get; set; }
        public string ProductionRegistrationHolder { get; set; }
        public string ProductName { get; set; }
        public string ProductOwner { get; set; }
        public string PackagingType { get; set; }
        public decimal? SmallestPackQty { get; set; }
        public string SmallestQtyunit { get; set; }
        public string SmallestQtyperPack { get; set; }
        public string RegistrationFactor { get; set; }
        public string RegistrationPerPack { get; set; }
        public bool? SalesInformation { get; set; }
        public bool? IsSalesPackRegistration { get; set; }
        public int? SalesFactor { get; set; }
        public string SalePerPack { get; set; }
        public long? Fpno { get; set; }
        public string FpnoName { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string Fpname { get; set; }
        public bool? MasterPackagingStatus { get; set; }
        public string IsSalesPackRegistrationFlag { get; set; }
        public string MasterPackagingStatusFlag { get; set; }
        public string SalesInformationFlag { get; set; }
        public List<long?> PackageRequirementIds { get; set; }
    }
}
