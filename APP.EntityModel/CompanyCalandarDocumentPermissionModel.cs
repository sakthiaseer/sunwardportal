﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyCalandarDocumentPermissionModel:BaseModel
    {
        public long CompanyCalandarDocumentPermissionId { get; set; }
        public long? DocumentId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public List<long?> RestrictUserIds { get; set; }
    }
}
