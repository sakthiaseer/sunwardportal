﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LayoutPlanTypeModel : BaseModel
    {
        public long LayoutPlanTypeID { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }
        public long? LayoutTypeID { get; set; }
        public string VersionNo { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Guid? SessionID { get; set; }

        public string LayoutTypeName { get; set; }
    }
}
