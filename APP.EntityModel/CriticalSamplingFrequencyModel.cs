﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CriticalSamplingFrequencyModel
    {
        public long CriticalSamplingFrequencyID { get; set; }
        public long? SamplingFrequencyID { get; set; }
        public decimal? SampleFrequencyValue { get; set; }

        public long? CriticalstepandIntermediateLineID { get; set; }

    }
}
