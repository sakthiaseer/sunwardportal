﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyCalendarLineModel : BaseModel
    {
        public long CompanyCalendarLineId { get; set; }
        public long? CompanyCalendarId { get; set; }
        public DateTime? EventDate { get; set; }
        public long? VendorId { get; set; }
        public long? TypeOfServiceId { get; set; }
        public long? MachineId { get; set; }
        public string Notes { get; set; }
        public decimal? NoOfHoursRequire { get; set; }
        public string VendorName { get; set; }
        public string TypeOfServiceName { get; set; }
        public string MachineName { get; set; }
        public List<long?> LocationIds { get; set; }
        public string LocationName { get; set; }
        public DateTime? CalendarEventDate { get; set; }
        public long? CalenderEventId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public string CalenderEventName { get; set; }
        public string SiteName { get; set; }
        public long Id { get; set; }
        public string Title { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string cssClass { get; set; }
        public bool IsYearly { get; set; }
        public bool IsService { get; set; }
        public bool IsMachine { get; set; }
        public bool IsLocation { get; set; }
        public string Name { get; set; }
        public string AllEventFlag { get; set; }
        public TimeSpan? FromTime { get; set; }
        public TimeSpan? ToTime { get; set; }
        public string LinkDocComment { get; set; }
        public bool IsLinkDocument { get; set; }
        public long? TypeOfEventId { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? RequireAssistance { get; set; }
        public string RequireAssistanceFlag { get; set; }
        public string TypeOfEventName { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public long? CalenderStatusId { get; set; }
        public string CalenderStatus { get; set; }
        public List<long?> ParticipantsIds { get; set; }
        public List<long?> InformationIds { get; set; }
        public string Participants { get; set; }
        public string Information { get; set; }
        public long? OwnerId { get; set; }
        public string OwnerName { get; set; }
        public long? OnBehalfId { get; set; }
        public string OnBehalfName { get; set; }
        public long? AssistanceFromId { get; set; }
        public string AssistanceFrom { get; set; }
        public long? RequestorId { get; set; }
        public string RequestorName { get; set; }
        public bool? Tentative { get; set; }
        public List<long?> AssistanceFromIds { get; set; }
        public List<CompanyCalendarLineNotesModel> NotesItems { get; set; }
        public List<CompanyCalendarLineLinkModel> LinkItems { get; set; }
        public List<ApplicationUserModel> ApplicationUsers { get; set; }
        public List<long?> HeadersUserIds { get; set; }
        public long? CalanderParentId { get; set; }
    }
    public class CompanyCalendarLineNotesModel : BaseModel
    {
        public long CompanyCalendarLineNotesId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string NotesDetails { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public long? CalendarNotesId { get; set; }
        public bool? IsUrgent { get; set; }
        public bool? IsQuote { get; set; }
        public long? UserId { get; set; }
        public bool? IsShowInfoAllUser { get; set; }
        public List<long?> UserIds { get; set; }
        public string Users { get; set; }
    }
    public partial class CompanyCalendarLineLinkModel : BaseModel
    {
        public long CompanyCalendarLineLinkId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string UrlLink { get; set; }
        public string UrlText { get; set; }
        public List<long?> RestrictUserIds { get; set; }
        public string RestrictUsers { get; set; }
        public bool? IsPrint { get; set; }
        public List<long?> PrintUserIds { get; set; }
        public string PrintUsers { get; set; }
        public string Type { get; set; }
        public long? CompanyCalendarLineTypeId { get; set; }
        public string Description { get; set; }

    }
}
