﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationWikiLineModel : BaseModel
    {
        public long ApplicationWikiLineId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? DutyId { get; set; }
        public string Responsibility { get; set; }
        public long? PageLink { get; set; }
        public long? FunctionLink { get; set; }
        public string DutyName { get; set; }
        public string PageLinkName { get; set; }
        public string FunctionLinkName { get; set; }
        public List<ApplicationWikiLineDutyModel> ApplicationWikiLineDutys { get; set; }
        public List<ApplicationWikiRecurrenceModel> ApplicationWikiRecurrences { get; set; }
        public bool? NotificationAdvice { get; set; }
        public int? NotificationAdviceTypeId { get; set; }
        public int? RepeatId { get; set; }
        public int? CustomId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Monthly { get; set; }
        public int? Yearly { get; set; }
        public string EventDescription { get; set; }
        public bool? DaysOfWeek { get; set; }
        public string NotificationAdviceFlag { get; set; }
        public List<int?> WeeklyIds { get; set; }
        public int? NotificationStatusId { get; set; }
        public Guid? SessionId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }
        public List<long?> NotifyIds { get; set; }
        public DateTime? NotifyEndDate { get; set; }
        public bool? IsAllowDocAccess { get; set; }
        public List<int?> DaysOfWeekIds { get; set; }
        public string NotificationAdviceType { get; set; }
    }
}
