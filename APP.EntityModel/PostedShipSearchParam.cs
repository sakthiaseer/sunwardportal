﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class PostedShipSearchParam
    {
        public string Search { get; set; }
        public string Company { get; set; }
        public long? CompanyId { get; set; }
        public DateTime StockMonth { get; set; }
        public DateTime FromPostingDate { get; set; }
        public DateTime ToPostingDate { get; set; }
        public string CustomerId { get; set; }
        public long? UserId { get; set; }
    }
}
