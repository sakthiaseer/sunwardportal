﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingUnitBoxModel : BaseModel
    {
        public long UnitBoxId { get; set; }
        public bool? IsPrinted { get; set; }
        public string Printed { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? BgPantoneColorId { get; set; }
        public long? BgColorId { get; set; }
        public long? LetteringId { get; set; }
        public decimal? SizeLength { get; set; }
        public decimal? SizeWidth { get; set; }
        public decimal? SizeHeight { get; set; }
        public long? PackagingMaterialId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PackingUnitId { get; set; }

        public string PantoneColor { get; set; }
        public string Color { get; set; }
        public string Lettering { get; set; }
        public string BgPantoneColor { get; set; }
        public string BgColor { get; set; }

        public string PackagingMaterial { get; set; }
        public string PackagingItem { get; set; }
        public string PackagingUnit { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public decimal? Grammage { get; set; }
        public int? FinishingId { get; set; }
        public string FinishingName { get; set; }
        public int? UnitBoxTypeID { get; set; }

         public bool? IsVersion { get; set; }
    }

}
