﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemCostLineModel : BaseModel
    {
        public long ItemCostLineId { get; set; }
        public long? ItemCostId { get; set; }
        public long? ItemId { get; set; }
        public string Item { get; set; }
        public string CostMethod { get; set; }
        public string CostCurrency { get; set; }
        public decimal? Cost { get; set; }
        public string Uom { get; set; }
        public string ReasonForChange { get; set; }
    }
}
