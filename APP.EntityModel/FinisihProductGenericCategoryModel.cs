﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinisihProductGenericCategoryModel:BaseModel
    {
        public long GenericId { get; set; }
        public long? FinishProductId { get; set; }
        public long? GenericCategoryId { get; set; }
        public string ProductName { get; set; }
    }
}
