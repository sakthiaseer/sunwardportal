﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppTranLocationModel : BaseModel
    {
        public long Id { get; set; }
        public string LocationTo { get; set; }
        public string LocationFrom { get; set; }
        public string RequisitionNo { get; set; }
        public string TransferNo { get; set; }
    }

    public class AppTranLocationLineModel : BaseModel
    {
        public long Id { get; set; }
        public long? AppTranLfromLtoId { get; set; }
        public string ItemNo { get; set; }
        public string LotNo { get; set; }
        public decimal? Qty { get; set; }
        public int? TotalLableQty { get; set; }
    }
}
