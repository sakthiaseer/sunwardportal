﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackingInformationLineModel:BaseModel
    {
        public long PackingInformationLineId { get; set; }
        public long? PackingInformationId { get; set; }
        public long? PackagingItemCategoryId { get; set; }
        public long? PackagingItemNameId { get; set; }
        public long? UomId { get; set; }
        public decimal? NoOfUnits { get; set; }
        public string Link { get; set; }
        public long? PackagingMethodId { get; set; }
        public string NameOfPackingMethod { get; set; }
        public decimal? UnitsPackingContent { get; set; }
        public string PackagingItemCategoryName { get; set; }
        public string PackagingItemName { get; set; }
        public string UomName { get; set; }
        public string PackagingMethodName { get; set; }
    }
}
