﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class WorkOrderCommentUserModel
    {
        public long WorkOrderCommentUserId { get; set; }
        public long? WorkOrderCommentId { get; set; }
        public long? UserId { get; set; }
        public bool? IsAssignTo { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }
        public string Status { get; set; }
        public bool? IsClosed { get; set; }
    }
}
