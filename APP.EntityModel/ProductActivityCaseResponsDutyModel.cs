﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductActivityCaseResponsDutyModel:BaseModel
    {
        public long ProductActivityCaseResponsDutyId { get; set; }
        public long? ProductActivityCaseResponsId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? DutyNo { get; set; }
        public long? EmployeeId { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public long? DesignationId { get; set; }
        public string DesignationNumber { get; set; }
        public string PlantCompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string EmployeeName { get; set; }
        public string DutyNoName { get; set; }
        public string Description { get; set; }
        public List<long?> EmployeeIDs { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public string Type { get; set; }
        public ProductActivityPermissionModel productActivityPermissionModel { get; set; }
        public List<ProductActivityCaseResponsResponsibleModel> wikiResponsibilityModels { get; set; }
    }
}
