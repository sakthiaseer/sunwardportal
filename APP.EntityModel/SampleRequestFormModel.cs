﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SampleRequestFormModel : BaseModel
    {
        public long SampleRequestFormId { get; set; }
        public long? ProfileId { get; set; }
        public long? TypeOfRequestId { get; set; }
        public string ProfileNo { get; set; }
        public long? CompanyId { get; set; }
        public string Company { get; set; }
        public long? PersonId { get; set; }
        public string Person { get; set; }
        public long? PurposeSampleRequestId { get; set; }
        public string PurposeSampleRequest { get; set; }
        public string AdditionalRequirement { get; set; }
        public DateTime? DueDate { get; set; }
        public string ScreenId { get; set; }
        public long? NavLocationId { get; set; }
        public long? InventoryTypeId { get; set; }
        public string InventoryType { get; set; }
        public string TypeOfRequest { get; set; }
    }
}
