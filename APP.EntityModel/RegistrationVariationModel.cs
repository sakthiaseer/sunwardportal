﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class RegistrationVariationModel:BaseModel
    {
        public long RegistrationVariationId { get; set; }
        public long? RegistrationCountryId { get; set; }
        public long? RegistrationClassificationId { get; set; }
        public long? RegistrationVariationCodeId { get; set; }
        public List<long?> RegistrationVariationTypeId { get; set; }
        public long? RegistrationCategory { get; set; }
       
        public string RegistrationCountryName { get; set; }
        public string RegistrationClassificationName { get; set; }
        public string RegistrationVariationCodeName { get; set; }
        public string VariationTitle { get; set; }
        public int? StatustCodeId { get; set; }
        public string RegistrationCategoryName { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
        public string VariationCodeandCountry { get; set; }
        public string RegisterationName { get; set; }
        public string RegistrationVariationTypeName { get; set; }
    }
}
