﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PacketInformationLineModel : BaseModel
    {
        public long PacketInformationId { get; set; }
        public long? TenderInformationId { get; set; }
        public DateTime? PacketInformationDate { get; set; }
        public decimal? QtyForPacket { get; set; }
    }
}
