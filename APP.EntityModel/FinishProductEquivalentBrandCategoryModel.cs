﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductEquivalentBrandCategoryModel:BaseModel
    {
        public long EquivalentBrandId { get; set; }
        public long? FinishProductId { get; set; }
        public long? EquivalentBrandCategoryId { get; set; }
        public string ProductName { get; set; }
    }
}
