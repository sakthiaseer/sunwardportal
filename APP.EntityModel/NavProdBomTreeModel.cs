﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavProdBomTreeModel
    {
        public long NavprodBomtreeId { get; set; }
        public string ParentItemNo { get; set; }
        public string ParentRecipeNo { get; set; }
        public string ItemNo { get; set; }
        public string RecipeNo { get; set; }
        public string Description { get; set; }
        public string Uom { get; set; }
        public string BatchNo { get; set; }
        public string LeadTime { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public string MainParentItemNo { get; set; }
        public decimal? Quantity { get; set; }
        public List<NavProdBomTreeModel> Children { get; set; } = new List<NavProdBomTreeModel>();
    }
}
