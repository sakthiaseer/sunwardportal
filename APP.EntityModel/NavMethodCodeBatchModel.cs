        
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavMethodCodeBatchModel :BaseModel
    {
        public long MethodCodeBatchId { get; set; }
        public long NavMethodCodeId { get; set; }
        public long? BatchSize { get; set; }
        public long? DefaultBatchSize { get; set; }
        public decimal? BatchUnitSize { get; set; }
        public string BatchSizeValue { get; set; }
    }
}

        // public long MethodCodeBatchId { get; set; }
        // public long NavMethodCodeId { get; set; }
        // public long? BatchSize { get; set; }
        // public long? DefaultBatchSize { get; set; }
        // public decimal? BatchUnitSize { get; set; }