﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductGeneralInfoModel : BaseModel
    {
        public long FinishProductGeneralInfoID { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? FinishProductGeneralInfoLineId { get; set; }
        public long? FinishProductID { get; set; }
        public long? RegisterCountry { get; set; }
        public string ProductName { get; set; }
        public string RegisterCountryProductName { get; set; }
        public long? RegisterProductOwnerID { get; set; }
        public long? DrugClassificationID { get; set; }

        public long?  RegistrationDocumentID { get; set; }
        public string CallNo { get; set; }
        public string RegistrationReferenceNo { get; set; }
        public string RegistrationNo { get; set; }
        public DateTime? RegistrationDateOfSubmission { get; set; }
        public DateTime? RegistrationDateOfIssue { get; set; }
        public DateTime? RegistrationDateOfExpiry { get; set; }
        public DateTime? RegistrationDateOfApproval { get; set; }
        public Guid? SessionID { get; set; }
        public string CountryName { get; set; }

        public string linkComment { get; set; }
        public string LinkDmscomment { get; set; }
        public string ProductOwner { get; set; }
        public string DrugClassification { get; set; }

        public string RegistrationDocument { get; set; }

        public long? ProductRegistrationHolderId { get; set; }
        public long? ProductionRegistrationHolderId { get; set; }
        public string ProductionRegistrationHolderName { get; set; }
        public long? PrhspecificProductId { get; set; }
        public string ProductRegistrationHolderName { get; set; }
        public string PrhspecificProductName { get; set; }
        public string ManufacturingSite { get;set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
       
        public long? RegistrationProductCategoryId { get; set; }
        public long? RegistrationCategoryClassId { get; set; }
       
        public string RegistrationProductCategoryName { get; set; }
        public string RegistrationCategoryClassName { get; set; }
        public long DosageFormId { get; set; }
        public string DosageFormName { get; set; }

        public string FPProductName { get; set; }

        public bool IsMaster { get; set; }

        public string Remarks { get; set; }
        public string Status { get; set; }
        public int? StatusId { get; set; }
    }
}
