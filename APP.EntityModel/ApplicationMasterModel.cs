﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationMasterModel
    {
        public long ApplicationMasterId { get; set; }
        public string ApplicationMasterName { get; set; }
        public string ApplicationMasterDescription { get; set; }
    }
}
