﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesItemPackingInfoItemsModel
    {
        public long? ManufacturingSiteID { get; set; }
        public long? FinishProductID { get; set; }
        public long? RegisterCountry { get; set; }
        public long? RegisterProductOwnerID { get; set; }
        public long? FinishProductGeneralInfoID { get; set; }
        public long? ProductRegistrationHolderId { get; set; }
        public string ManufacturingSite { get; set; }
        public decimal? PackQty { get; set; }
        public long? PackQtyunitId { get; set; }
        public long? PerPackId { get; set; }
        public decimal? FactorOfSmallestProductionPack { get; set; }
        public long? SalesPerPackId { get; set; }
        public string RegisterCountryName { get; set; }
        public string ProductionRegistrationHolder { get; set; }
        public string ProductName { get; set; }
        public string ProductOwner { get; set; }
        public string PackagingType { get; set; }
        public string PackQtyUnit { get; set; }
        public string PerPackName { get; set; }
        public string SalesPerPackName { get; set; }
    }
}
