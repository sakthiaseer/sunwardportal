﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionColorModel : BaseModel
    {
        public long ProductionColorID { get; set; }
        public long? ItemNo { get; set; }
        public long? ColorID { get; set; }
        public long? GenericName { get; set; }
        public long? ColorIndexNumberID { get; set; }

        public string Item { get; set; }
        public string Color { get; set; }
        public string GenericMasterName { get; set; }
        public string ColorIndexNumber { get; set; }
        public decimal? DyeContent { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }

        public string MasterProfileReferenceNo { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }

        public long SpecNoID { get; set; }
        public string SpecNos { get; set; }
        public string SpecificationNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string PurposeName { get; set; }
        public string ProcessUseSpecName { get; set; }
        public string RndpurposeName { get; set; }
        public string StudyLink { get; set; }
    }
}
