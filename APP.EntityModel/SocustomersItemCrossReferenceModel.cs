﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SocustomersItemCrossReferenceModel:BaseModel
    {
        public long SocustomersItemCrossReferenceId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string No { get; set; }
        public long? NavItemId { get; set; }
        public string CustomerReferenceNo { get; set; }
        public string CustomerReferenceNo2 { get; set; }
        public string Description { get; set; }
        public long? CustomerPackingUomId { get; set; }
        public long? PerUomId { get; set; }
        public long? PurchasePerUomId { get; set; }
        public DateTime? ShelfLifeDate { get; set; }
        public string CustomerPackingUomName { get; set; }
        public string PerUomName { get; set; }
        public string PurchasePerUomName { get; set; }
        public string MinShelfLife { get; set; }
        public List<long?> TypeOfPermitIds { get; set; }
        public string ProductNo { get; set; }
        public long? CompanyListingID { get; set; }
        public string CompanyListingName { get; set; }
        public bool? IsSameAsSunward { get; set; }

        public string IsSameAsSunwardFlag { get; set; }

        public List<SobyCustomerSunwardEquivalentModel> SobyCustomerSunwardEquivalentItems { get; set; }
        public string NavItemName { get; set; }
    }
}
