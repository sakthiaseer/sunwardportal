using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingItemHistoryModel : BaseModel
    {
        public long PackagingItemHistoryId { get; set; }
        public long? CategoryId { get; set; }
        public long? ItemId { get; set; }
        public string CategoryName { get; set; }
        public string ItemName { get; set; }

    }
}
