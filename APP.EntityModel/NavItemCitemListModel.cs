﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavItemCitemListModel
    {
        public long NavItemCitemID { get; set; }
        public long? NavItemID { get; set; }
        public long? NavItemCustomerItemID { get; set; }
    }
}
