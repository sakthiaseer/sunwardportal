﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApptransferRelcassLinesSearchModel
    {
        //Changes Done by Aravinth 18JUL2019 Start
        public long ApptransferRelcassLineID { get; set; }
        public long? TransferRelcassID { get; set; }
        public string DrumNo { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public decimal? Quantity { get; set; }
        public string TransferRelcass { get; set; }

        //Changes Done by Aravinth 18JUL2019 End
    }
}
