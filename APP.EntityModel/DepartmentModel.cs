﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DepartmentModel : BaseModel
    {
        public long DepartmentId { get; set; }
        public long? CompanyId { get; set; }
        public long? DivisionID { get; set; }
        public long? Hodid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public string CompanyNamee { get; set; }
        public string HodName { get; set; }
        public string DivisionName { get; set; }
        public string ProfileCode { get; set; }

        public string CompanyDivisionName { get; set; }
    }
}
