﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HRExternalPersonalModel : BaseModel
    {
        public long HrexternalPersonalId { get; set; }
        public long? CompanyListingId { get; set; }
        public long? PurposeForExternalPersonalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Gender { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Icinformation { get; set; }
        public string HandphoneNo { get; set; }
        public string CompanyListingName { get; set; }
        public string PurposeForExternalPersonal { get; set; }
    }
}
