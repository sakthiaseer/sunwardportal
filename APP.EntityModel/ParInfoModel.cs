﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ParInfoModel
    {
        public DateTime? CompletionDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string DocumentNo { get; set; }
        public string SourceName { get; set; }
        public string LocationCode { get; set; }
        public string BatchNo { get; set; }
        public decimal? Demand { get; set; }
        public decimal? Supply { get; set; }
        public string ShippingAdvice { get; set; }
        public decimal? Balance { get; set; }
        public string ItemNo { get; set; }
        public decimal? Quantity { get; set; }
    }
}
