using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BompackingMasterModel : BaseModel
    {
         public long BompackingId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public int? DosageFormId { get; set; }
        public decimal? PackQty { get; set; }
        public long? PackQtyunitId { get; set; }
        public long? PerPackId { get; set; }
        public long? SalesPerPackId { get; set; }
        public long? BottleTypeId { get; set; }
        public string BompackingName { get; set; }
        public string Description { get; set; }
        public decimal? VesionNo { get; set; }
        public decimal? BlisterStripSizeLength { get; set; }
        public decimal? BlisterStripSizeWidth { get; set; }
        public decimal? BsalesPackQty { get; set; }
        public long? BsalesPackUnitId { get; set; }
        public decimal? CcapsuleSize { get; set; }
        public decimal? TbtabletSizeLength { get; set; }
        public decimal? TbtableSizeWidth { get; set; }

        public string DosageFormName {get;set;}

       public List<BompackingMasterLineModel> BompackingMasterLine { get; set; }   
       public List<long?> CountryId { get; set; } = new List<long?>();
    }
}
