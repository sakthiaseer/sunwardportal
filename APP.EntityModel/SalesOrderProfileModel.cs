﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderProfileModel : BaseModel
    {
        public long SalesOrderProfileID { get; set; }
        public int? OrderByID { get; set; }
        public long? ProfileID { get; set; }
        public long? TenderProfileID { get; set; }
        public long? CustomerProfileID { get; set; }
        public long? BlanketOrderProfileID { get; set; }
        public string ProfileName { get; set; }
        public string OrderBy { get; set; }
    }
}
