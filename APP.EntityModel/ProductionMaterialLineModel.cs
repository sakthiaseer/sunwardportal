﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMaterialLineModel : BaseModel
    {
        public long ProductionMaterialLineId { get; set; }
        public long? ProductionMaterialId { get; set; }
        public int? SaltOrBaseId { get; set; }
        public decimal? XofGeneralItemName { get; set; }
        public bool? IsWaterMalecule { get; set; }
        public long? HydrationId { get; set; }
        public int? MicronisedOrMeshId { get; set; }
        public decimal? ParticleSize { get; set; }
        public decimal? MeshNo { get; set; }
        public bool? IsDensity { get; set; }
        public int? Density { get; set; }
        public decimal? DyeContentFrom { get; set; }
        public decimal? DyeContentTo { get; set; }
        public long? DatabaseRequireId { get; set; }
        public string SuggestNameBySystem { get; set; }
        public long? NavisionSinid { get; set; }
        public long? ItemCategorySinid { get; set; }
        public long? NavisionMalId { get; set; }
        public long? ItemCategorMalId { get; set; }
        public bool? IsChangeNavisonToPropose { get; set; }
        public string IsWaterMaleculeFlag { get; set; }
        public string IsDensityFlag { get; set; }
        public string SaltOrBaseName { get; set; }
        public string MicronisedOrMeshName { get; set; }
        public string HydrationName {get;set;}
        public string DensityName { get; set; }
        public List<long> MaterialClassificationSIGId { get; set; }
        public List<long> MaterialClassificationMALId { get; set; }
        public string IsChangeNavisonToProposeFlag { get; set; }
    }
}
