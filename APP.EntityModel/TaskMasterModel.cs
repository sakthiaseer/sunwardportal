﻿using System;
//using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskMasterModel :BaseModel
    {
        public long TaskID { get; set; }
        public long? ParentTaskID { get; set; }
        public long? MainTaskId { get; set; }
        public string Title { get; set; }
        public List<long?> AssignedTo { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NewDueDate { get; set; }
        public string Description { get; set; }
        public long? ProjectID { get; set; }
        public string UserName { get; set; }
        public List<string> UserNames { get; set; }
        public string ProjectName { get; set; }
        public bool? Expander { get; set; } = true;
        public Guid? SessionID { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? End { get; set; }
        public long Id { get; set; }
        public string cssClass { get; set; }

        public List<long> TagIds { get; set; }
        public List<long?> AssignedCC { get; set; }
        public List<long?>  AssignedRights { get; set; }
        public long? OnBehalfID { get; set; }
        public long TaskLinkID { get; set; }
        public string TaskLinkDescription { get; set; }
        public long? LinkedTaskID { get; set; }
        public long? OwnerID { get; set; }

        public List<long> TaskLinkedIds { get; set; }
        public List<long> TaskMergedIds { get; set; }
        public List<long> AttachmentIds { get; set; }

        public List<long> Attachments { get; set; }

        public string OnBehalfName { get; set; }
        
        public string FollowUp { get; set; }
        public string OverDueAllowed { get; set; }

        public List<string> TagNames { get; set; }
        public int? OrderNo { get; set; }
        public DateTime DiscussionDate { get; set; } 
        public bool? IsRead { get; set; }
        public bool IsUrgent { get; set; }
        public bool? IsAutoClose { get; set; }
        public string Link { get; set; }
        public string Version { get; set; }

        public int WorkTypeID { get; set; }
        public string WorkType { get; set; }

        public int RequestTypeID { get; set; }

        public string RequestType { get; set; }
        public string Name { get; set; }       

        public bool HasPermission { get; set; }
        public List<TaskMasterModel> Children { get; set; }
        public DateTime? RemainderDate { get; set; }
        public DateTime? AssignedDate { get; set; }
        public TaskNotesModel TaskNote { get; set; }

        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
        public List<TaskCommentModel> TaskMessages { get; set; }


        public List<long> ReadonlyUserID { get; set; }

        public List<long> ReadWriteUserID { get; set; }


        public List<string> DocSessionID { get; set; }

        public long? TaskAttachmentId { get; set; }
        public long? DocumentID { get; set; }
        public long? TaskCommentId { get; set; }
        public int? TaskLevel { get; set; }
        public string TaskNumber { get; set; }
        public bool? IsNoDueDate { get; set; }

        public bool UnreadMessage { get; set; }

        public string TreeLevel { get; set; }
        public string cssTitle { get; set; }
        public List<string> PersonaltagIds { get; set; }
        public bool? IsNotifyByMessage { get; set; }
        public bool? IsEmail { get; set; }
        public string AssignToNames { get; set; }
        public string AssignCCNames { get; set; }
        public string DocumentDescription { get; set; }
        public Guid? VersionSessionId { get; set; }
        public List<TaskMasterDescriptionVersionModel> TaskMasterDescriptionVersionList { get; set; }
        public bool? IsAllowEditContent { get; set; }
        public bool? IsAssignToLogin { get; set; }
        public bool? IsAssignCCLogin { get; set; }
        public bool? IsOwnerOnbehalfLogin { get; set; }
        public bool? IsOwnerLogin { get; set; }
        public bool? IsOnbehalfLogin { get; set; }
        public bool? IsCheckedReadUnRead { get; set; }
        public List<long?> TaskIds { get; set; }
        public long? SourceId { get; set; }
        public string TaskNotes { get; set; }
    }
}
