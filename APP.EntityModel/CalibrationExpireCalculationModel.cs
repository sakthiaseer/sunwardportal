﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CalibrationExpireCalculationModel
    {
        public long CalibrationExpireCalculationID { get; set; }
        public int? Months { get; set; }
        public long? CalibrationServiceInformationID { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
    }
}
