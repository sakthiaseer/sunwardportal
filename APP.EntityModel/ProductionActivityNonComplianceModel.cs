﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityNonComplianceModel:BaseModel
    {
        public long ProductionActivityNonComplianceId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? IpirReportId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public string Type { get; set; }
        public List<long?> UserIDs { get; set; }
        public string ActionType { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public string Notes { get; set; }
        public List<ProductionActivityNonComplianceUserModel> productionActivityNonComplianceUserModels { get; set; }
    }
    public class ProductionActivityNonComplianceUserModel
    {
        public long ProductionActivityNonComplianceUserId { get; set; }
        public long? ProductionActivityNonComplianceId { get; set; }
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
    }
}
