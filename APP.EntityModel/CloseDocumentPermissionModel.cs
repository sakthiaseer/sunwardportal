﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CloseDocumentPermissionModel
    {
        public long CloseDocumentPermissionId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public bool? IsCloseDocumentPermission { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> DocumetUserRoleIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public bool IsSelected { get; set; } = false;
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string UserGroupName { get; set; }
        public string FileProfileTypeName { get; set; }
        public List<long?> FileProfileTypeIds { get; set; }
    }
}
