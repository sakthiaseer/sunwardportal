﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductActivityCaseResponsResponsibleModel
    {
        public long WikiResponsibilityID { get; set; }
        public long? EmployeeID { get; set; }
        public long? UserID { get; set; }
        public long? UserGroupID { get; set; }

        public List<long?> EmployeeIDs { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public long? ProductActivityCaseResponsDutyId { get; set; }

        public string EmployeeName { get; set; }
        public string UserGroupName { get; set; }
        public string DesignationNumber { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string PlantCompanyName { get; set; }
        public bool? isSelected { get; set; }
        public int? HeadCount { get; set; }
        public ProductActivityPermissionModel productActivityPermissionModel { get; set; }
        public List<long?> WikiResponsibilityIDs { get; set; }
        public  List<string> PermissionList { get; set; }

        public  string Permissions { get; set; }
    }
}
