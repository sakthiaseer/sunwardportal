﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateNotifyModel
    {
        public long JobProgressTemplateNotifyId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public string UserType { get; set; }
        public long? EmployeeId { get; set; }
        public long? UserGroupId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public int? HeadCount { get; set; }
        public bool? isSelected { get; set; }
        public List<long?> JobProgressTemplateNotifyIds { get; set; }
    }
}
