﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BomProductionGroup:BaseModel
    {
        public long BomproductionGroupId { get; set; }
        public long? ManufacturingRecipeId { get; set; }
        public long? FpexcipientId { get; set; }
        public long? ManufacturingSiteID { get; set; }
        public string ManufacturingSite { get; set; }
        public string RegisterCountry { get; set; }
        public string PRHSpecificName { get; set; }
        public string RegisterHolderName { get; set; }
        public string ProductOwner { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string PRHSpecificProductName { get; set; }

    }
}
