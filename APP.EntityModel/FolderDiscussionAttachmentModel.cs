﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class FolderDiscussionAttachmentModel: BaseModel
    {
        public long FolderDiscussionAttachmentId { get; set; }
        public long? FolderDiscussionId { get; set; }
        public long? DocumentId { get; set; }
        public string FileName { get; set; }
        public long? UserId { get; set; }
        public List<string> FileNames { get; set; }
        public List<long?> DocumentIdList { get; set; }

        public List<DocumentsModel> FileDocumentList { get; set; }

        public string ContentType { get; set; }
        public List<string> ContentTypes { get; set; }
    }
}
