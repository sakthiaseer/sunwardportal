﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityClassificationModel:BaseModel
    {
        public long ProductionActivityClassificationId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? ReplanNo { get; set; }
        public string ItemDescription { get; set; }
        public string BatchNo { get; set; }
        public string BatchSize { get; set; }
        public string Uom { get; set; }
        public string Pv { get; set; }
        public string ProfileName { get; set; }
    }
}
