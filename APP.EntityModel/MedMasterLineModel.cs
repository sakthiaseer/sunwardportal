﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MedMasterLineModel : BaseModel
    {
        public long MedMasterLineId { get; set; }
        public long? MedMasterId { get; set; }
        public long? ItemId { get; set; }
        public string SerialNo { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? LocationId { get; set; }
        public bool? IsFixPartMed { get; set; }
        public bool? IsDedicatedUsage { get; set; }
        public int? DedicatedUsageId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public long? DedicatedUsageLocationId { get; set; }
        public long? DedicatedUsageAreaId { get; set; }
        public long? DedicatedUsagespecificAreaId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string Item { get; set; }
        public string DedicatedUsage { get; set; }
        public string IsFixPartMedFlag { get; set; }
        public string IsDedicatedUsageFlag { get; set; }
        public string InventoryType { get; set; }
        public string Location { get; set; }
        public string Area { get; set; }
        public string SpecificArea { get; set; }
        public string DedicatedUsageLocation { get; set; }
        public string DedicatedUsageArea { get; set; }
        public string DedicatedUsagespecificArea { get; set; }
        public string ManufacturingProcess { get; set; }
        public List<long?> MachineIds { get; set; }
    }
}
