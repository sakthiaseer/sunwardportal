﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BmrmovementModel:BaseModel
    {
        public long BmrmovementId { get; set; }
        public bool? ShipReceivedStatus { get; set; }
        public long? ShipToId { get; set; }
        public long? BmrstatusId { get; set; }
        public string ShipTo { get; set; }
        public string ShippedBy { get; set; }
        public string Bmrstatus { get; set; }
        public List<BmrmovementLineModel> BmrmovementLines { get; set; } = new List<BmrmovementLineModel>();
    }
}
