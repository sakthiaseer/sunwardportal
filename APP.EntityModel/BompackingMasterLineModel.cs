using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BompackingMasterLineModel : BaseModel
    {
        
        public long BompackingMasterLineId { get; set; }
        public long? BompackingMasterId { get; set; }
        public long? SetTypeId { get; set; }
        public string No { get; set; }
        public string ItemName { get; set; }
        public int? CalculationMethodId { get; set; }
        public decimal? Qty { get; set; }
        public long? Uomid { get; set; }
        public string Bominformation { get; set; }

        public string SetTypeName{get;set;}

        
        public string CalulationName {get;set;}

       
      

    
       
    }
}
