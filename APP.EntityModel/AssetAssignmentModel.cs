﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AssetAssignmentModel : BaseModel
    {
        public long AssetAssignId { get; set; }
        public long? AssetId { get; set; }
        public long? AssignedTo { get; set; }
        public string AssignedToName { get; set; }
        public DateTime? AssignedOn { get; set; }
        public int? AssetStatus { get; set; }
        public long? AssetManagedBy { get; set; }
        public string ManagedBy { get; set; }
        public bool? IsLatest { get; set; }

        public string AssetStatusName { get; set; }
    }
}
