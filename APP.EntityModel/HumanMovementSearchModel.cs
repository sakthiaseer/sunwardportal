﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HumanMovementSearchModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string IsSwstaff { get; set; }
        public List<long?> CompanyId { get; set; }
        public List<string> LocationId { get; set; }
        public string Temperature { get; set; }
        public string Questionnaire { get; set; }
        public bool? AllLocation { get; set; }
        public bool? AllVisitorCompany { get; set; }
        public long? PlantId { get; set; }
        public int? MovementStatusId { get; set; }
        public string TesKitResult { get; set; }
        public bool? IsDuplicate { get; set; }
    }
}
