﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApptransferRelcassEntryModel : BaseModel
    {
        public long TransferReclassID { get; set; }
        public long? TransferFromLocationID { get; set; }
        public long? TransferFromAreaID { get; set; }
        public long? TransferFromSpecificAreaID { get; set; }
        public long? TransferToLocationID { get; set; }
        public long? TransferToAreaID { get; set; }
        public long? TransferToSpecificAreaID { get; set; }
        public int? TransferTypeID { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public bool PostedtoNAV { get; set; }

        //Changes Done by Aravinth 18JUL2019 Start

        public string TransferFromLocationName { get; set; }
        public string TransferFromAreaName { get; set; }
        public string TransferFromSpecificAreaName { get; set; }
        public string TransferToLocationName { get; set; }
        public string TransferToAreaName { get; set; }
        public string TransferToSpecificAreaName { get; set; }
        public string TransferTypeName { get; set; }

        public string TransferOrderNo { get; set; }
        public long? ReceiveLocationId { get; set; }
        public string ReceiveLocation { get; set; }

        public List<ApptransferRelcassLinesModel> ApptransferRelcassLinesDetailModels { get; set; } = new List<ApptransferRelcassLinesModel>();

        //Changes Done by Aravinth 18JUL2019 End
    }
}
