﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackagingBottleInsertModel:BaseModel
    {
        public long BottleInsertSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public string Code { get; set; }
        public long? PackagingMaterialColorId { get; set; }
        public long? PackagingTypeId { get; set; }
        public long? PackagingMaterialShapeId { get; set; }
        public decimal? Measurement { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PackagingItemSetInfoName { get; set; }
        public string PackagingMaterialColorName { get; set; }
        public string PackagingTypeName { get; set; }
        public string PackagingMaterialShapeName { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public bool? FormTab { get; set; }
        public bool? VersionControl { get; set; }
        public List<long?> UseforItemIds { get; set; }

    }
}
