﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OrderRequirementLineSplitModel:BaseModel
    {
        public long OrderRequirementLineSplitId { get; set; }
        public long? OrderRequirementLineId { get; set; }
        public decimal? ProductQty { get; set; }
        public long? SplitProductId { get; set; }
        public decimal? SplitProductQty { get; set; }
        public string Remarks { get; set; }
        public string SplitProductName { get; set; }
        public bool IsNavSync { get; set; }
    }
}
