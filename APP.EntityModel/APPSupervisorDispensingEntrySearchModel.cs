﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class APPSupervisorDispensingEntrySearchModel
    {
        public long SupervisorDispensingId { get; set; }
        public string WorkOrderNo { get; set; }
        public string ItemName { get; set; }
        public string DrumNo { get; set; }
    }
}
