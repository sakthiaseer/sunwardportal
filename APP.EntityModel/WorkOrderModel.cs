﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class WorkOrderModel : BaseModel
    {
        public long WorkOrderId { get; set; }
        public int? AssignmentId { get; set; }
        public string AssignmentName { get; set; }
        public long? RequirementId { get; set; }
        public string RequirementName { get; set; }
        public long? ModuleId { get; set; }
        public string ModuleName { get; set; }
        public long? AssignTo { get; set; }
        public int? TypeId { get; set; }
        public long? PermissionID { get; set; }
        public long? UserGroupId { get; set; }
        public List<long?> UserGroupUserIds { get; set; }
        public long? NavisionModuleId { get; set; }
        public string NavisionModule { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }

    }

    public class WorkOrderLineModel : BaseModel
    {
        public long WorkOrderLineId { get; set; }
        public long? WorkOrderId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public int? PriorityId { get; set; }
        public string PriorityName { get; set; }
        public int? SunwardStatusId { get; set; }
        public string SunwardStatusName { get; set; }
        public DateTime? StatusUpdateDate { get; set; }
        public DateTime? ExpectedReleaseDate { get; set; }
        public string TaskLink { get; set; }
        public string ResultLink { get; set; }
        public int? CrtstatusId { get; set; }
        public string CrtStatusName { get; set; }
        public string LastIndex { get; set; }
        public long? RequirementId { get; set; }
        public string RequirementName { get; set; }
        public string WorkOrderProfileNo { get; set; }
    }
}
