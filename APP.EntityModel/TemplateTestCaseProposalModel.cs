﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TemplateTestCaseProposalModel:BaseModel
    {
        public long TemplateTestCaseProposalId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public List<long?> UserIds { get; set; }
        public bool? IsByCaseCompany { get; set; }
        public string IsByCaseCompanyFlag { get; set; }
        public long? CompanyId { get; set; }
        public string ParticipantUsers { get; set; }
        public string UserType { get; set; }
        public long? TemplateCaseFormId { get; set; }
        public long? ReassignUserId { get; set; }
        public string ReassignUser { get; set; }
    }
}
