﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavSaleCategoryModel : BaseModel
    {
        public long SaleCategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string NoSeries { get; set; }
        public long? LocationID { get; set; }
        public string Location { get; set; }
        public string SGNoSeries { get; set; }
    }
}
