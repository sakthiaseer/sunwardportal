﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ActivityInfoModel
    {
        public long ActivityMasterMultipleId { get; set; }
        public long? AcitivityMasterId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }

        public string ActivityMasterNames { get; set; }

        public List<long?> ActivityInfoIds { get; set; }
    }
}
