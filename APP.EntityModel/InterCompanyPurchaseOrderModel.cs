using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InterCompanyPurchaseOrderModel : BaseModel
    {
        public long InterCompanyPurchaseOrderId { get; set; }
        public DateTime? Date { get; set; }
        public long? LoginId { get; set; }
        public long? BuyerCompanyId { get; set; }

        public string LoginName { get; set; }

        public string BuyerCompanyName { get; set; }






    }

    public class InterCompanyPurchaseOrderLineModel : BaseModel
    {

        public long InterCompanyPurchaseOrderLineId { get; set; }
        public long? InterCompanyPurchaseOrderId { get; set; }
        public long? NavItemId { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public DateTime? PromisedDeliveryDate { get; set; }
        public string Location { get; set; }

        public string NavNo { get; set; }

        public string Description {get;set;}

        public string Description2{get;set;}


        public string InternalRef{get;set;}

        public string PackUom {get;set;}

        




    }



}
