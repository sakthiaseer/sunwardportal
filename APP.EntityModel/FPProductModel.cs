﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPProductModel : BaseModel
    {
        public long FPProductID { get; set; }
        public long? FPProductGeneralInfoID { get; set; }
        public List<long?> FinishProductGeneralInforLineIDs { get; set; }

        public bool? IsActiveSalesPack { get; set; } = true;
        public string ProductName { get; set; }
    }
}
