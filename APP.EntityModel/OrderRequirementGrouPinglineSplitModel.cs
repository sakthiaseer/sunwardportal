﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OrderRequirementGrouPinglineSplitModel:BaseModel
    {
        public long OrderRequirementGrouPinglineSplitId { get; set; }
        public long? OrderRequirementGroupingLineId { get; set; }
        public decimal? ProductQty { get; set; }
        public long? SplitProductId { get; set; }
        public decimal? SplitProductQty { get; set; }
        public string Remarks { get; set; }
        public bool? IsNavSync { get; set; }
        public string SplitProductName { get; set; }
    }
}
