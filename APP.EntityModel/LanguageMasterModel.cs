﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LanguageMasterModel
    {
        public long LanguageID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
