﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ActivityEmailTopicModel : BaseModel
    {
        public long ActivityEmailTopicId { get; set; }
        public long? ActivityMasterId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? CategoryActionId { get; set; }
        public string Subject { get; set; }
        public long? ActionId { get; set; }
        public string Comment { get; set; }
        public Guid? DocumentSessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public Guid? EmailTopicSessionId { get; set; }
        public string ActivityType { get; set; }
        public long? FromId { get; set; }
        public string ToIds { get; set; }
        public string CcIds { get; set; }

        public bool? IsDraft { get; set; }
        public string BackUrl { get; set; }
    }
}
