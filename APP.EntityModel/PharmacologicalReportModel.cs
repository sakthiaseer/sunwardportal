﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PharmacologicalReportModel
    {
        public long? FinishProductId { get; set; }
        public long? PharmacologicalpropertiesId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public string ManufacturingSiteName { get; set; }
        public string ProductName { get; set; }
        public string PharmacologicalCategory { get; set; }
        public string ChemicalSubgroup { get; set; }
        public string GenericName { get; set; }
        public string EquvalentBrand { get; set; }
        public string Atccode { get; set; }
        public string DrugClassification { get; set; }
        public string RegistrationProductCategoryName { get; set; }
        public string RegistrationFileName { get; set; }
        public string Gtinno { get; set; }
    }
}
