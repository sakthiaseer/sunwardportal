﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskInviteUserModel:BaseModel
    {
        public long TaskInviteUserId { get; set; }
        public long? TaskId { get; set; }
        public long? InviteUserId { get; set; }
        public DateTime? InvitedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }
        public List<long?> InviteUserIds { get; set; }
        public string Title { get; set; }
        public long? TaskCommentID { get; set; }
        public string Baseurl { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }


    }
}
