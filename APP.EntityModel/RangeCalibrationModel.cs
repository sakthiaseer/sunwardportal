﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RangeCalibrationModel : BaseModel
    {
        public long RangeCalibrationID { get; set; }
        public string ParameterRange { get; set; }
        public string Max { get; set; }
        public string Min { get; set; }
        public string UnitOfMeasure { get; set; }
        public string Remarks { get; set; }
        public long? CalibrationServiceInformationID { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
    }
}
