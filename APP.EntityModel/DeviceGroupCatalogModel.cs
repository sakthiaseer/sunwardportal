﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DeviceGroupCatalogModel : BaseModel
    {
        public long DeviceGroupCatalogID { get; set; }
        public long? DeviceCatalogID { get; set; }
        public long? DeviceGroupID { get; set; }
    }
}
