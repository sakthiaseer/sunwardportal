﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
  public class ApplicationFormSubModel:BaseModel
    {
        public long ApplicationFormSubId { get; set; }
        public long? FormId { get; set; }
        public long? ApplicationFormId { get; set; }
        public string FormName { get; set; }
        public string ModuleName { get; set; }
        public string Path { get; set; }
        public long? ProfileID { get; set; }
        public DocumentPermissionModel DocumentPermissionModels { get; set; }
    }
}
