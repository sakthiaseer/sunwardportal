﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ClassificationProductionModel : BaseModel
    {
        public long ClassificationProductionId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileName { get; set; }
    }
}
