﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskCommentUserModel:BaseModel
    {
        public long TaskCommentUserId { get; set; }
        public long TaskCommentId { get; set; }
        public long TaskMasterId { get; set; }
        public long UserId { get; set; }
        public bool IsRead { get; set; }
        public bool? IsPin { get; set; }
        public long? PinnedBy { get; set; }
        public bool? IsAssignedTo { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsClosed { get; set; }
        public string Title { get; set; }
        public string TaskSubject { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
