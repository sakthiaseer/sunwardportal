﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TemplateTestCaseModel : BaseModel
    {
        public long TemplateTestCaseId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateCode { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public string Instruction { get; set; }
        public string DepartmentName { get; set; }
        public string NameOfTemplate { get; set; }
        public string Subject { get; set; }
        public string Link { get; set; }
        public string Naming { get; set; }
        public string LocationName { get; set; }
        public string AutoNumbering { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public List<TemplateTestCaseLinkModel> templateTestCaseLinkModels { get; set; }
        public List<long?> DepartmentIds { get; set; }
        public bool? IsAutoNumbering { get; set; }
        public long? TemplateProfileId { get; set; }
        public string TemplateProfileName { get; set; }
        public int? VersionNo { get; set; }
        public string SpecificCase { get; set; }
        public List<long?> ProposalUserIds { get; set; }
        public bool? IsTemplateExits { get; set; }
    }
    public class TemplateTestCaseLinkModel:BaseModel
    {
        public long TemplateTestCaseLinkId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public string NameOfTemplate { get; set; }
        public string Subject { get; set; }
        public string Link { get; set; }
        public string Naming { get; set; }
        public string LocationName { get; set; }
        public string AutoNumbering { get; set; }
        public bool? IsAutoNumbering { get; set; }
        public long? TemplateProfileId { get; set; }
        public string TemplateProfileName { get; set; }
        public string DocumentNo{ get; set; }
        public long? LocationToSaveId { get; set; }
        public long? TemplateCaseFormId { get; set; }

    }
    public  class TemplateTestCaseLinkDocModel:BaseModel
    {
        public long TemplateTestCaseLinkDocId { get; set; }
        public long? TemplateTestCaseLinkId { get; set; }
        public string DocumentNo { get; set; }

    }
    public class TemplateTestCaseLinkDocNoModel:BaseModel
    {
        public long? TemplateTestCaseLinkId { get; set; }
        public string DocumentNo { get; set; }
        public string Title { get; set; }
        public long? ProfileID { get; set; }
    }
}
