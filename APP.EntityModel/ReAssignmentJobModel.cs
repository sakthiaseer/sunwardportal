﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ReAssignmentJobModel : BaseModel
    {
        public long ReassignmentJobId { get; set; }
        public long? MobileShiftId { get; set; }
        public long? CompanyDbid { get; set; }
        public long? ProdTaskId { get; set; }
        public long? SpecificProcedureId { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public string CompanyDb { get; set; }
        public string MobileShift { get; set; }
        public string ProdTask { get; set; }
        public string SpecificProcedure { get; set; }
        public List<long?> ReAssignementManpowerMultipleIds { get; set; }
    }
}
