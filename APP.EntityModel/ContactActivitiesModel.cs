﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ContactActivitiesModel : BaseModel
    {
        public long ContactActivitiesId { get; set; }
        public long? ContactId { get; set; }
        public int? Type { get; set; }
        public string Summary { get; set; }
    }
}
