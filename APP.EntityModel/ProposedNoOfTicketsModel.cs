﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProposedNoOfTicketsModel
    {
        public long ProposedItemId { get; set; }
        public long? ItemId { get; set; }
        public decimal? FbStockQty { get; set; }
        public decimal? AdhocStockQty { get; set; }
        public DateTime? StartMonth { get; set; }
        public DateTime? EndMonth { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ReportDate { get; set; }
        public string VersionNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
    }
    public class PostNavProposedNoOfTicketsModel
    {
        public List<ProposedNoOfTicketsModel> proposedNoOfTicketsModels { get; set; }
    }
}
