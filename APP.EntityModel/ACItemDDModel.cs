﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACItemDDModel
    {
        public long? DistACID { get; set; }
        public string Description { get; set; }
    }
}
