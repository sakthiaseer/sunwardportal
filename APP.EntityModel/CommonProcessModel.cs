﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonProcessModel:BaseModel
    {
        public long CommonProcessId { get; set; }
        public List<long?> DosageFormIds { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string DosageFormName { get; set; }
        public string ManufacturingProcessName { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
    }
}
