﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public  class FulfillmentofAdhocModel : BaseModel
    {
        public string MethodCode { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public string ItemNo { get; set; }
        public string InternalRef { get; set; }
        public string BUOM { get; set; }
        public decimal? StockBalance { get; set; }
        public decimal? ProductionOrder { get; set; }
        public DateTime? Date { get; set; }
        public string DocumentNo { get; set; }
        public string SourceName { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Balance { get; set; }
        public string OrderType { get; set; }
        public bool IsMonthLastRecord { get; set; }
        public bool IsPromiseDate { get; set; }
        //public List<SaleProdOrders> SaleProdOrders { get; set; }
    }

    public class SaleProdOrders
    {
        public DateTime? Date { get; set; }
        public string DocumentNo { get; set; }
        public string SourceName { get; set; }
        public decimal? Quantity { get; set; }
        public string Balance { get; set; }
    }
}
