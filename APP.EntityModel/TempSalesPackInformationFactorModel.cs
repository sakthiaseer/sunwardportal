﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TempSalesPackInformationFactorModel : BaseModel
    {
        public long TempSalesPackInformationFactorID { get; set; }
        public long? TempSalesPackInformationID { get; set; }
        public int? SalesFactor { get; set; }
        public long? ProfileID { get; set; }
        public string ProfileNo { get; set; }
        public string FPName { get; set; }
        public long? ItemID { get; set; }
        public long? QTYPackPerCarton { get; set; }

        public decimal? RegistrationFactor { get; set; }
        public string RegistrationPerPack { get; set; }
        public string PrhspecificProductName { get; set; }
        public decimal? SmallestPackQty { get; set; }
      
        public string SmallestQtyUnit { get; set; }
        public string SmallestPerPack { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ProfileName { get; set; }

        public string QTYPackPerCartonName { get; set; }
    }
}
