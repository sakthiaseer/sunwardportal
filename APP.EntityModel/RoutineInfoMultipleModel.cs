﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RoutineInfoMultipleModel
    {
        public long RoutineInfoMultipleId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public long? RoutineInfoId { get; set; }
    }
}
