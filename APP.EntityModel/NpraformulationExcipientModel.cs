﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NpraformulationExcipientModel:BaseModel
    {
        public long NpraformulationExcipientId { get; set; }
        public long? NpraformulationId { get; set; }
        public long? ExcipientId { get; set; }
        public decimal? Strength { get; set; }
        public long? UnitsId { get; set; }
        public long? FunctionId { get; set; }
        public long? SourceId { get; set; }
        public long? RemarksFunctionId { get; set; }
        public string Remarks { get; set; }
        public string Excipient { get; set; }
        public string Units { get; set; }
        public string Function { get; set; }
        public string Source { get; set; }
        public string RemarksFunction { get; set; }
    }
}
