﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CustomerAcceptanceConfirmationModel : BaseModel
    {
        public long CustomerAcceptanceConfirmationID { get; set; }
        public bool? IsCustomerName { get; set; }
        public bool? IsContractNo { get; set; }
        public bool? IsProductName { get; set; }
        public bool? IsUom { get; set; }
        public bool? IsTenderExtensionFrom { get; set; }
        public bool? IsTenderExtensionTo { get; set; }
        public bool? IsNoOfLot { get; set; }
        public bool? IsQuotationNo { get; set; }

        public string ConfirmationPONo { get; set; }
        public long? BlanketOrderId { get; set; }

        public string CustomerName { get; set; }
        public string ContractNo { get; set; }
        public string ProductName { get; set; }
        public string Uom { get; set; }
        public string TenderExtensionFrom { get; set; }
        public string TenderExtensionTo { get; set; }
        public string NoOfLot { get; set; }
        public string QuotationNo { get; set; }
    }
}
