﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlanningSomulationModel
    {
        public string MethodeCode { get; set; }
        public string Description { get; set; }
        public string PackSize { get; set; }
        public string Customer { get; set; }
        public decimal ACQty { get; set; }
        public decimal UnitQty { get; set; }
        public decimal ThreeMonthACQty { get; set; }
        public string ProdRecipe { get; set; }
        public string BatchSize { get; set; }
        public string NoofTickets { get; set; }
        public string NoOfDays { get; set; }
        public decimal DistStockBalance { get; set; }
        public decimal NAVStockBalance { get; set; }
        public decimal  StockBalance { get; set; }
    }
}
