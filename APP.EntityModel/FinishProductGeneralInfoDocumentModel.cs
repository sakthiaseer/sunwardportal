﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public  class FinishProductGeneralInfoDocumentModel : BaseModel
    {
        public long FinishProductGeneralInfoDocumentID { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsImage { get; set; }
    }
}
