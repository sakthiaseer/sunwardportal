﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HumanMovementSageModel
    {
        public long SageInformationId { get; set; }
        public long? HumanMovementId { get; set; }
        public string SageId { get; set; }
        public string Name { get; set; }
        public string ScanStatus { get; set; }
        public decimal? Temperature { get; set; }
        public string ActionByPharmacist { get; set; }
        public string Remarks { get; set; }
        public long? EmployeeId { get; set; }

        public string MovementStatus { get; set; }

    }
}
