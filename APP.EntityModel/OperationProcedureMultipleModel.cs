﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OperationProcedureMultipleModel
    {
        public long OperationProcedureMultipleID { get; set; }
        public long? ProductionMachineID { get; set; }
        public long? OperationProcedureID { get; set; }

    }
}
