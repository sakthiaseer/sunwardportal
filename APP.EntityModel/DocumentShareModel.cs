﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentShareModel:BaseModel
    {
        public long DocumentShareId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? DocumentId { get; set; }
        public long? UserId { get; set; }
        public bool? IsShareAnyOne { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<DocumentsModel> SelectedShareItems { get; set; }
    }
}
