﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackagingBottleModel:BaseModel
    {
        public long BottleSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public decimal? Volume { get; set; }
        public string Code { get; set; }
        public long? PackSizeunitsId { get; set; }
        public long? ColorId { get; set; }
        public long? TypeId { get; set; }
        public long? ShapeId { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public  string PackagingItemSetInfoName { get; set; }
        public string PackSizeunitsName { get; set; }
        public string ColorName { get; set; }
        public string TypeName { get; set; }
        public string ShapeName { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public bool? FormTab { get; set; }
        public bool? VersionControl { get; set; }
        public List<long?> UseforItemIds { get; set; }

    }
}
