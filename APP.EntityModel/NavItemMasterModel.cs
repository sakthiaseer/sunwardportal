﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavItemMasterModel
    {
        public long ItemId { get; set; }
        public string No { get; set; }
        public string Company { get; set; }
        public long CompanyId { get; set; }
    }
}
