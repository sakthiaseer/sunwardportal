﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OrderRequirementGroupingLineModel:BaseModel
    {
        public long OrderRequirementGroupingLineId { get; set; }
        public long? ProductionSimulationGroupingId { get; set; }
        public long? ProductId { get; set; }
        public string TicketBatchSizeId { get; set; }
        public decimal? NoOfTicket { get; set; }
        public string Remarks { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public bool? RequireToSplit { get; set; }
        public string TicketBatchSizeName { get; set; }
        public string ProductName { get; set; }
        public string RequireToSplitFlag { get; set; }
        public decimal? ProductQty { get; set; }
        public string UOM { get; set; }
        public long? NavLocationId { get; set; }
        public long? NavUomid { get; set; }
        public string NavLocationName { get; set; }
        public string NavUomName { get; set; }
        public bool IsNavSync { get; set; }
    }
}
