﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AssetDepreciationModel
    {
        public long AssetDepreciationId { get; set; }
        public long AssetMasterId { get; set; }
        public decimal? DepriciationAmount { get; set; }
        public DateTime? DepriciationDate { get; set; }
        public decimal? BookValue { get; set; }
    }
}
