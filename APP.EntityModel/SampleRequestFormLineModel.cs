﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SampleRequestFormLineModel : BaseModel
    {
        public long SampleRequestFormLineId { get; set; }
        public long? SampleRequestFormId { get; set; }
        public long? ItemId { get; set; }
        public string Item { get; set; }
        public string Description { get; set; }
        public string Uom { get; set; }
        public decimal? QtyRequire { get; set; }
        public decimal? QtyIssue { get; set; }
        public string BatchNo { get; set; }
        public DateTime? MfgDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string SpecialNotes { get; set; }
        public string SpecialNotesIssue { get; set; }
        public bool IsRequestForPerson { get; set; }
        public bool? IsSpecificBatch { get; set; }
        public bool? InStockSameItem { get; set; }
        public long? InventoryItemId { get; set; }
        public string Factor { get; set; }
        public decimal? BalanceQty { get; set; }
        public int? HeaderStatus { get; set; }
        public long? NavCompanyId { get; set; }
        public decimal? TotalIssueQty { get; set; }

        public int? IssueStatusId { get; set; }
        public string IssueStatus { get; set; }
        public string NavLocation { get; set; }
        public List<IssueRequestSampleLineModel> IssueRequestSampleLineModels { get; set; }
    }
}
