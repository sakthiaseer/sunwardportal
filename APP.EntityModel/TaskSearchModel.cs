﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
  public  class TaskSearchModel
    {
        public string Title { get; set; }
        public long AssignTypeIds { get; set; }
        public long StatusIds { get; set; }
        public long WorkTypeIds { get; set; }
        public long RequestTypeIds { get; set; }
        public long ProjectIds { get; set; }
        public List<long> TagIds { get; set; }
        public List<string> PersonaltagIds { get; set; }
        public long UserId { get; set; }

        public List<long> TaskStatusIds { get; set; }
        public List<long> TaskAssigneeIds { get; set; }

        public string TaskType { get; set; }

        public string BarType { get; set; }
        public string ClickType { get; set; }
        public List<long> UserIds { get; set; }

        public bool UnreadSubject { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? CreatedToDate { get; set; }
        public bool? IsBefore { get; set; }
        public bool? IsEmail { get; set; }

    }
}
