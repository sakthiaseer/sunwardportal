﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonFieldsProductionMachineModel:BaseModel
    {
        public long CommonFieldsProductionMachineId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public long? MachineGroupingId { get; set; }
        public string MachineNo { get; set; }
        public long? Location { get; set; }
        public string NameOfTheMachine { get; set; }
        public string LocationName { get; set; }
        public string ManufacturingSiteName { get; set; }
        public string ManufacturingProcessName { get; set; }
        public string ManufacturingStepsName { get; set; }
        public string MachineGroupingName { get; set; }
        public string MachineName { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileName { get; set; }
        public string MachinesName { get; set; }
        public string MachineNameList { get; set; }
    }
}
