﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistributorGenericCodeModel
    {
        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }
        public long? CompanyId { get; set; }
    }
}
