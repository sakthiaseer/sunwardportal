﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class ProfileReserveDataModel
    {
        public long? ProfileId { get; set; }
        public long? FileProfileTypeId { get; set; }
    }
}
