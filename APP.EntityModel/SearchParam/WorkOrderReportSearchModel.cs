﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class WorkOrderReportSearchModel
    {
        public int? AssignmentId { get; set; }

        public long? NavisionModuleId { get; set; }

        public long? PermissionID { get; set; }
        public string ProfileNo { get; set; }

        public int? StatusCodeID { get; set; }

        public long? Id { get; set; }
       
        public string Subject { get; set; }
       
        public int? PriorityId { get; set; }
        public long? RequirementId { get; set; }
        public int? SunwardStatusId { get; set; }
        public int? CrtstatusId { get; set; }
    }
}
