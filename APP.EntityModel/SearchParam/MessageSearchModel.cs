﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.SearchParam
{
    public class MessageSearchModel
    {
        public long? UserID { get; set; }
        public long? CommentBy { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusCodeID { get; set; }
    }
}
