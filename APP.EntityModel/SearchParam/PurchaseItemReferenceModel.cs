﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PurchaseItemReferenceModel
    {
        public long? PurchaseItemSalesEntryLineID { get; set; }       
        public long? SalesOrderEntryID { get; set; }
        public long? SoByCustomersId { get; set; }
        public string CustomerName { get; set; }
        public string OrderBy { get; set; }
        public string ProductNo { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
