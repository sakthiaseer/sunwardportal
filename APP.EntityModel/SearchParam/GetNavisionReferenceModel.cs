﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GetNavisionReferenceModel
    {
        public long? Id { get; set; }
        public long? SalesOrderEntryId { get; set; }
    }
}
