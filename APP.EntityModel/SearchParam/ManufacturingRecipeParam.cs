﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.SearchParam
{
    public class ManufacturingRecipeParam
    {
        public long? SiteID { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public string SiteName { get; set; }
        public long? ProfileID { get; set; }
        public long? DosageFormID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? UserID { get; set; }
    }
}
