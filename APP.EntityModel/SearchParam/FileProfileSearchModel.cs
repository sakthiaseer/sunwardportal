﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FileProfileSearchModel
    {
        public long? FileProfileTypeId { get; set; }
        public long? ProfileId { get; set; }
        public string FileName { get; set; }
        public long? UserId { get; set; }
    }
}
