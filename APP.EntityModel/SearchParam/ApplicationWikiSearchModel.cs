﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationWikiSearchModel
    {
        public string Title { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? WikiTypeId { get; set; }
        public long? WikiOwnerId { get; set; }      
        public List<string> FilterFields { get; set; }
        public long? WikiCategoryId { get; set; }
        public long? WikiTopicId { get; set; }
        public long? EmployeeId { get; set; }
        public long? PlantId { get; set; }
        public long? WikiOwnerTypeId { get; set; }
    }
}
