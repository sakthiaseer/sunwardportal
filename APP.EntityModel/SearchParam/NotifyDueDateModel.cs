﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NotifyDueDateModel
    {
        public long? ModifiedByUserID { get; set; }
        public DateTime? DueDate { get; set; }
        public long? NotifyDocumentId { get; set; }
    }
}
