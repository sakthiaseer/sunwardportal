﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GeneralInfoSearchModel
    {
        public long ProductID { get; set; }
        public long OwnerID { get; set; }
        public long HolderID { get; set; }
        public long CountryID { get; set; }
    }
}
