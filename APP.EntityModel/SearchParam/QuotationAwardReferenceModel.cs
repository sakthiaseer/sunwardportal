﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class QuotationAwardReferenceModel 
    {
        public long? ProductId { get; set; }
        public string ProductGroupCode { get; set; }
        public string ProvideQuotation { get; set; }
        public decimal? Qty { get; set; }
        public decimal? MinQty { get; set; }
        public decimal? PricePerUnit { get; set; }
    }
}
