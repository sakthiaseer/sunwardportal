﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderReportSearchModel
    {
        public long? customerID { get; set; }
        public List<long?> ItemIds { get; set; }
        public int? statusCodeID { get; set; }
    }
}
