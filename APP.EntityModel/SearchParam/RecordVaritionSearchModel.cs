﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RecordVaritionSearchModel
    {
        public string RelatedChangeControlNo { get; set; }
        public long? RegistrationHolderId { get; set; }
        public long? RegisterCountryId { get; set; }
        public string ProductName { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public int? SubmissionStatusId { get; set; }
        public long? ProductOwnerId { get; set; }
        public long? VariationCodeId { get; set; }
        public long? VariationStatusId { get; set; }
    }
}
