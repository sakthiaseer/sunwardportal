﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentNoSeriesSearchModel
    {
        public long? ProfileID { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
    }
}
