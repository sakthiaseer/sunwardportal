﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderSearchModel
    {
        public long? SalesOrderEntryId { get; set; }
        public long? CompanyID { get; set; }
        public long? OrderByID { get; set; }
        public long? CustomerID { get; set; }
        public long? TenderAgencyID { get; set; }
        public long? TypeOfOrderID { get; set; }
        public long? ItemCustomerID { get; set; }
        public long? TenderProductNoID { get; set; }
        public long? DistributionCustomerID { get; set; }
        public long? PonumberId { get; set; }
        public long? TypeOfSalesOrdeId { get; set; }
    }
}
