﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.SearchParam
{
    public class TaskCommentSearchModel
    {
        public long? UserID { get; set; }

        public long? TaskID { get; set; }

        public int? StatusCodeID { get; set; }
        public string SearchText { get; set; }

    }
}
