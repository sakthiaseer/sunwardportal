﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessAvailabilityParam
    {
        public long CompanyId { get; set; }
        public List<string> ReplanNos { get; set; }
        public string Process { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
