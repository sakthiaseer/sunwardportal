﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessMachineTimeLineSearch
    {
        public long? ProcessMachineTimeId { get; set; }      
        public long? ItemId { get; set; }
    }
}
