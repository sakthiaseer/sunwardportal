﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.SearchParam
{
    public class SearchProductionSchedulingModel
    {
        public string Search { get; set; }
        public string OsdOrGalenical { get; set; }
        public string OsdOrGalenicalSelect { get; set; }
        public bool OSD { get; set; }
        public bool Galenical { get; set; }
        public string Company { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
