﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class QuotationHistorySearchModel
    {
       
        public long? CompanyId { get; set; }
        public string SwreferenceNo { get; set; }
        public long? CustomerId { get; set; }
        public long? ProductId { get; set; }
        public DateTime? FromMonth { get; set; }
        public DateTime? ToMonth { get; set; }


        //Following Name for Quotation Award Reference
        public long? QuotationhistoryId { get; set; }
        public decimal? Qty { get; set; }
    }
}
