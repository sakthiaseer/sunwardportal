﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GetUserGroupModel
    {
        public long UserGroupID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsTMS { get; set; }

        public List<long?> UserIDs { get; set; }

        public int? StatusCodeID { get; set; }
    }
}
