﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TemplateTestCaseFormModel : BaseModel
    {
        public long TemplateTestCaseFormId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public long? CompanyId { get; set; }
        public string CaseNo { get; set; }
        public string SubjectName { get; set; }
        public string VersionNo { get; set; }
        public string Instruction { get; set; }
        public string TemplateName { get; set; }
        public string TemplateInstruction { get; set; }
        public string DepartmentName { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public string TopicId { get; set; }
        public List<TemplateTestCaseLinkModel> templateTestCaseLinkModels { get; set; }
        public List<TemplateTestCaseProposalModel> TemplateTestCaseProposalModels { get; set; }
        public List<TemplateTestCaseCheckListModel> TemplateTestCaseCheckListModels { get; set; }
    }
    public class TemplateTestCaseFormSearchModel : BaseModel
    {
        public string CaseNo { get; set; }
        public long? CompanyId { get; set; }
        public string SubjectName { get; set; }
        public long? TemplateId { get; set; }
    }
    public class TemplateTestCaseLinkSetModel 
    {
        public long? PreTemplateTestCaseLinkId { get; set; }
        public long? NewTemplateTestCaseLinkId { get; set; }
    }
}
