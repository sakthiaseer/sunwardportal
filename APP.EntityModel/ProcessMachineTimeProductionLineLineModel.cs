﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessMachineTimeProductionLineLineModel:BaseModel
    {
        public long ProcessMachineTimeProductionLineLineId { get; set; }
        public long? ProcessMachineTimeProductionLineId { get; set; }
        public string OperationStepNo { get; set; }
        public string Description { get; set; }
        public string Preference { get; set; }
        public long? ProductionCentreStepsId { get; set; }
        public string ProductionCentreSteps { get; set; }
    }
}
