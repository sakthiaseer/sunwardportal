using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationMasterDetails:BaseModel
    {
        public long Units { get; set; }
        public long Units1 { get; set; }
        public string UnitsName1 { get; set; }
        public long ApplicationMasterID { get; set; }
    }
}
