﻿using System;
//using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace APP.EntityModel
{
    public class PersonalTagsModel
    {

        public long PersonalTagID { get; set; }
        public long? TaskID { get; set; }

        public string Tag { get; set; }

        public List<long> TagIds { get; set; }

        public List<string> Tags { get; set; }

        public long? AddedByUserID { get; set; }

        public DateTime AddedDate { get; set; }

    }
}
