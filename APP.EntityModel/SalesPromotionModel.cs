﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesPromotionModel : BaseModel
    {
        public long SalesPromotionId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProfileId { get; set; }
        public string PromotionNo { get; set; }
        public long? PurposeOfPromotionId { get; set; }
        public string PurposeOfPromotion { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class SalesMainPromotionListModel : BaseModel
    {
        public long SalesMainPromotionListId { get; set; }
        public long? SalesPromotionId { get; set; }
        public long? GenericCodeId { get; set; }
        public bool? IsFocList { get; set; }
        public string ItemUom { get; set; }

        public string GenericCode { get; set; }

    }
}
