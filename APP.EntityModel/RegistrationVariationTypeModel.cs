﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RegistrationVariationTypeModel : BaseModel
    {
        public long RegistrationVariationTypeId { get; set; }
        public long? RegistrationVariationId { get; set; }
        public long VariationTypeId { get; set; }
    }
}
