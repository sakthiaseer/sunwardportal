﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MarginInformationSearchModel
    {
        public DateTime? TillDate { get; set; }
        public long? BusinessCategoryId { get; set; }
        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public long? GenericCodeId { get; set; }
        public long? NavItemId { get; set; }
    }
}
