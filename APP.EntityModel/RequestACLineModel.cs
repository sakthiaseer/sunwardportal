﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RequestACLineModel : BaseModel
    {
        public long RequestAclineId { get; set; }
        public long? RequestAcid { get; set; }
        public long? NavItemId { get; set; }
        public long? DistributorItemId { get; set; }
        public string ShelfLife { get; set; }
        public decimal? Avg12Months { get; set; }
        public decimal? Avg6Months { get; set; }
        public decimal? Avg3Months { get; set; }
        public decimal? CurrentAc { get; set; }
        public decimal? ProposedNewAc { get; set; }
        public decimal? DifferenceInAc { get; set; }
        public decimal? PercentDifference { get; set; }
        public string Comments { get; set; }
        public string ItemName { get; set; }
        public string AcEntryName { get; set; }
        public int? PackSize { get; set; }
        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerName { get; set; }

        public decimal? Months12 { get; set; }
        public decimal? Months6 { get; set; }
        public long? PicId { get; set; }
    }
}
