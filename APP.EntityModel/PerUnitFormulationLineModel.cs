﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PerUnitFormulationLineModel : BaseModel
    {
        public long PerUnitFormulationLineID { get; set; }
        public long? ManufacturingProcessID { get; set; }
        public long? ManufacturingStepsID { get; set; }
        public long? FinishProductExcipintID { get; set; }

        public string ManufacturingSteps { get; set; }
        public string ManufacturingProcess { get; set; }
    }
}
