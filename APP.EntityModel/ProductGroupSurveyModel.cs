﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductGroupSurveyModel : BaseModel
    {
        public long ProductGroupSurveyId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? SurveyDate { get; set; }
        public long? PharmacologicalCategoryId { get; set; }
        public string PharmacologicalCategory { get; set; }
        public string ProductGroupSurveyName { get; set; }

    }
}
