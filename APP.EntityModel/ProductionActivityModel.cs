﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityModel : BaseModel
    {
        public long ProductionActivityId { get; set; }
        public string Company { get; set; }
        public string TicketNo { get; set; }
        public string BatchNo { get; set; }
        public long? ProcessId { get; set; }
        public string ProcessName { get; set; }
        public long? ActivityDocumentId { get; set; }
        public string ActivityDocumentName { get; set; }
        public byte[] UploadPhoto { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public bool? IsFinalProcess { get; set; }
        public bool? IsManualEntry { get; set; }
        public string SubLot { get; set; }
        public string Comment { get; set; }
        public bool? IsNotification { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
    }
}
