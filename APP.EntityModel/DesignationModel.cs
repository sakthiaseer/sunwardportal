﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DesignationModel : BaseModel
    {
        public long DesignationID { get; set; }
        public long? LevelID { get; set; }
        public long? SubSectionTID { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public long? CompanyID { get; set; }
        public string CompanyNamee { get; set; }
        public string LevelName { get; set; }
        public string SubSectionTwoName { get; set; }
        public string SectionName { get; set; }
        public string DepartmentName { get; set; }
        public string DivisionName { get; set; }
        public string SubSectionName { get; set; }
        public string DropDownNames { get; set; }

        public long? DivisionID { get; set; }
        public long? DepartmentID { get; set; }
        public long? SectionID { get; set; }
        public long? SubSectionID { get; set; }

    }
}
