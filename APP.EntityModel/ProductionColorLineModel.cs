﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionColorLineModel : BaseModel
    {
        public long ProductionColorLineID { get; set; }
        public long? ProductionColorID { get; set; }
        public long? DatabseRequireID { get; set; }
        public long? NavisionID { get; set; }
        public string DatabseRequire { get; set; }
        public string Navision { get; set; }
        public string BUOM { get; set; }
    }
}
