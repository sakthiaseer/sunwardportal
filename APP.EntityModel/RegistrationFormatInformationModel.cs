﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RegistrationFormatInformationModel:BaseModel
    {
        public long RegistrationFormatInformationId { get; set; }
        public long? FormatId { get; set; }
        public long? ProductCategoryId { get; set; }
        public long? TypeId { get; set; }
        public string VariationDescription { get; set; }
        public long? VariationOnId { get; set; }
        public long? VariationCategoryId { get; set; }
        public long? VariationCategoryNoId { get; set; }
        public bool? IsCombinationOfOtherFormat { get; set; }
        public string IsCombinationOfOtherFlag { get; set; }
        public string FormatName { get; set; }
        public string ProductCategoryName { get; set; }
        public string TypeName { get; set; }
        public string VariationOnName { get; set; }
        public string VariationCategoryName { get; set; }
        public string VariationCategoryNoName { get; set; }
        public List<long> RegistrationFormatListIds { get; set; }
        public long? VariationCodeId { get; set; }
        public string VariationCodeName { get; set; }
    }
}
