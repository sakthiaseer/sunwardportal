﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackagingNestingModel:BaseModel
    {
        public long NestingSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public decimal? RowMeasurementLength { get; set; }
        public decimal? RowMeasurementWidth { get; set; }
        public decimal? ColumnMeasurementLength { get; set; }
        public decimal? ColumnMeasurementWidth { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PackagingItemSetInfoName { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public bool? FormTab { get; set; }
        public bool? VersionControl { get; set; }
        public List<long?> UseforItemIds { get; set; }


    }
}
