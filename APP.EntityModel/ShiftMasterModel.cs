﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ShiftMasterModel : BaseModel
    {
        public long ShiftID { get; set; }       
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

    }
}
