﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPItemModel
    {
        public DateTime? CompletionDate { get; set; }
        public DateTime? Safetylead { get; set; }
        public DateTime? Shipmentdate { get; set; }
        public string DocumentNo { get; set; }
        public string Description { get; set; }
        public string Sourcename { get; set; }
        public string LocationCode { get; set; }
        public decimal? Demand { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Supply { get; set; }
        public decimal? Rpobalance { get; set; }
        public string Rpono { get; set; }
        public string ItemNo { get; set; }
        public decimal? QuantityBase { get; set; }
        public decimal? Quantity { get; set; }
        public string BatchNo { get; set; }
        public string ShippingAdvice { get; set; }
        public string DocumentType { get; set; }
        public string FilterType { get; set; }
    }
}
