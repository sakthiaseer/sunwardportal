﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavStockBalanceModel : BaseModel
    {
        public long AvnavStockBalID { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public string ItemDecs { get; set; }
        public string Uom { get; set; }
        public decimal? RemainingQty { get; set; }
        public bool? IsNav { get; set; }
        public string PackSize { get; set; }
        public int? Ac { get; set; }

        public string Dist { get; set; }
        public string ItemCode { get; set; }
        public string Packuom { get; set; }

        public string SWItemNo { get; set; }
        public string SWDesc { get; set; }
        public string SWDesc2 { get; set; }
        public long? CompanyId { get; set; }
        public string InternalRefNo { get; set; }
        public string ItemGroup { get; set; }
    }
    public class  StockBalanceSearch  
    {
        public DateTime StkMonth { get; set; }
        public string ItemNo { get; set; }
        public long? CompanyId { get; set; }
        public long? UserId { get; set; }
    }
    }
