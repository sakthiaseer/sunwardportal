﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DeviceCatalogMasterModel : BaseModel
    {
        public long DeviceCatalogMasterID { get; set; }
        public string DeviceName { get; set; }
        public string ModelName { get; set; }
        public long? CalibrationTypeID { get; set; }
        public string CalibrationTypeName { get; set; }
        public long? SourceListID { get; set; }
        public long? DeviceGroupListID { get; set; }        
        public string ManufacturerName { get; set; }
        public string Cataloglink { get; set; }
        public string Status { get; set; }

        public CalibrationExpireCalculationModel CalibrationExpireCal { get; set; }
        public virtual List<DeviceGroupCatalogModel> DeviceGroupCatalogList { get; set; }
        public virtual List<DeviceManufacturerGroupModel> DeviceManufacturerGroupList { get; set; }
        public virtual List<InstructionTypeModel> DeviceInstructionTypeList { get; set; }
        public virtual List<SunwardAssetListModel> SunwardAssetListItesms { get; set; }
        public virtual List<AcceptableCalibrationInformationModel> AcceptableCalibrationInfoList { get; set; }
        public virtual List<CalibrationVendorInformationModel> CalibrationVendorInfoList { get; set; }
        public virtual List<InstructionTypeModel> CalibrationInstructionTypeList { get; set; }
        public virtual List<RangeCalibrationModel> RangeCalibrationList { get; set; }
       

    }
}
