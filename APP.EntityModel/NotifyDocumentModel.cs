﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NotifyDocumentModel : BaseModel
    {
        public long NotifyDocumentId { get; set; }
        public long? UserId { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> CcuserIDs { get; set; }
        
        public List<long?> UserGroupIDs { get; set; }
        public List<long?> CcuserGroupIDs { get; set; }
        
        public string UserName { get; set; }
        public string UserGroupName { get; set; }
        public string CcuserName { get; set; }
        public string CcuserGroupName { get; set; }
        public string Remarks { get; set; }
        public long? DocumentId { get; set; }
        public long? DocumentShareId { get; set; }
        public string DocumentName { get; set; }
        public string ContentType { get; set; }
        public bool? IsUrgent { get; set; }
        public string Type { get; set; }
        public bool IsDownload { get; set; }
        public string DocumentPath { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? ParentId { get; set; }
        public long? MainFileProfileTypeId { get; set; }
        public bool? IsQuote { get; set; }
        public long? QuoteNotifyId { get; set; }
        public string QuoteMessage { get; set; }
        public bool? IsSenderClose { get; set; } = false;
        public long? DocumentParentId { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsDetailNotes { get; set; }
        public bool? IsQuoteDetailNotes { get; set; }
        public bool? IsRead { get; set; }
        public string CssClass { get; set; }
        public long NotifyDocumentUserGroupId { get; set; }
        public long? GUserId { get; set; }
        public long? GUserGroupId { get; set; }
        public bool? GIsClosed { get; set; }
        public bool? GIsCcuser { get; set; }
        public bool? GIsRead { get; set; }
        public long NotifyUserGroupUserId { get; set; }
        public long? UUserId { get; set; }
        public long? UUserGroupId { get; set; }
        public bool? UIsClosed { get; set; }
        public bool? UIsCcuser { get; set; }
        public bool? UIsRead { get; set; }
        public bool? IsHidden { get; set; }
        public string Baseurl { get; set; }
        public string NotifyType { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? IpirReportId { get; set; }
        public long? IpirReportAssignmentId { get; set; }
    }
}
