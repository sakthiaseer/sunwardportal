﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistributorReplenishmentModel:BaseModel
    {
        public long DistributorReplenishmentId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromPeriod { get; set; }
        public DateTime? ToPeriod { get; set; }
        public decimal? ReplenishmentTiming { get; set; }
        public decimal? NoOfMonthToReplenish { get; set; }

        public bool? NeedVersionChecking { get; set; }
        public string NeedVersionCheckingFlag { get; set; }
        public string CompanyListingName { get; set; }
        public string ReferenceInfo { get; set; }
        public long VersionTableId { get; set; }

        public Guid? SessionId { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public decimal? MaxHoldingStock { get; set; }
        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }

        public string DosageForm { get; set; }
        public string DrugClassification { get; set; }
        public List<DistributorReplenishmentLineModel> DistributorReplenishmentLine { get; set; }

    }
}
