﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMethodManufacturingModel
    {
        public long ProductionMethodManufacturingID { get; set; }
        public long? ProductionMethodTemplateID { get; set; }
        public long? ManufacturingSiteID { get; set; }
    }
}
