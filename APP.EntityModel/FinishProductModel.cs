﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductModel : BaseModel
    {
        public long FinishProductId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public string ManufacturingSiteName { get; set; }
        public long? DosageFormId { get; set; }
        public string DosageFormName { get; set; }
        public long? ProductId { get; set; }
        public string RegistrationFileName { get; set; }
        public List<long?> GenericNameId { get; set; }
        public string GenericName { get; set; }
        public string EquvalentBrandName { get; set; }
        public List<long?> ChemicalSubgroupId { get; set; }
        public List<long?> PharmacologicalCategoryId { get; set; }
        public string Atccode { get; set; }
        public string Gtinno { get; set; }
        public string ProductOwner { get; set; }
        public string Country { get; set; }
        public string ProductName { get; set; }
        public string ChemicalSubgroupName { get; set; }
        public List<long?> EquvalentBrandNameId { get; set; }
        public string RegisterationCodeName { get; set; }
        public List<FinishProductLineModel> FinishProductLine { get; set; }
        public List<long> FinishProductLineId { get; set; }
        public long? DrugClassificationID { get; set; }
        public long? RegistrationProductCategory { get; set; }

    }
}
