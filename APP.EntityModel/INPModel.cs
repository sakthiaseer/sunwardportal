﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPModel
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Company { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? QuantityBase { get; set; }
        public decimal? Supply { get; set; }
        public decimal? Demand { get; set; }
        public decimal? Balance { get; set; }
        public string Uom { get; set; }
        public string FilterType { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public decimal? AvailableQty { get; set; }
        public decimal? SaftyStock { get; set; }
        public decimal? QuantityRequired { get; set; }
        public string ItemCategory { get; set; }
        public string DocumentNo { get; set; }
        public string OrderStatus { get; set; }
        //Changes Done by Aravinth
        public long InpreportId { get; set; }

        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public long? ItemId { get; set; }

        //inter company

        public string IcDocumentNo { get; set; }
        public DateTime? IcShipmentDate { get; set; }
        public string IcDescription { get; set; }
        public decimal? IcSupply { get; set; }
        public decimal? IcDemand { get; set; }
        public decimal? IcBalance { get; set; }

        public List<INPModel> InterCompanyList { get; set; }

        public string SupplyTo { get; set; }
    }
}
