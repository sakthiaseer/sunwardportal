﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackageAluminiumFoilLineModel:BaseModel
    {
        public long PackagingAluminiumFoilLineId { get; set; }
        public long? PackagingAluminiumFoilId { get; set; }
        public decimal? RepeatedLength { get; set; }
        public decimal? BlisterWidth { get; set; }
        public decimal? BlisterLength { get; set; }
        public decimal? BlisterGap { get; set; }
        public decimal? BlisterAndFoilGap { get; set; }
    }
}
