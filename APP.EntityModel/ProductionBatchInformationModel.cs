﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchInformationModel : BaseModel
    {
        public long ProductionBatchInformationID { get; set; }
        public long? PlantID { get; set; }
        public string TicketNo { get; set; }
        public DateTime? ManufacturingStartDate { get; set; }
        public long? UnitsofBatchSize { get; set; }
        public string PackingUnits { get; set; }
        public string ProductionOrderNo { get; set; }
        public long? BatchSizeId { get; set; }
        public string BatchSize { get; set; }

        public string BatchNo { get; set; }
        public string ItemNo { get; set; }


    }
}
