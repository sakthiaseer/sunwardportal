﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPManufacturingRecipeModel : BaseModel
    {
        public long FPManufacturingRecipeID { get; set; }
        public long? ManufacturingSiteID { get; set; }
        public long? FinishProductGeneralInfoID { get; set; }
        public bool? IsMaster { get; set; }
        public string Master { get; set; }
        public long? FinishProductId { get; set; }
        public string ManufacturingSite { get; set; }
        public string CustomerProductName { get; set; }
        public string RegisterCountry { get; set; }
        public string PRHSpecificName { get; set; }
        public string RegisterHolderName { get; set; }
        public string ProductName { get; set; }
        public string ProductOwner { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PRHSpecificProductName { get; set; }
    }
}
