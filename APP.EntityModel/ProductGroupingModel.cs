﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductGroupingModel : BaseModel
    {
        public long ProductGroupingId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProfileId { get; set; }
        public string ProductGroupCode { get; set; }
        public string ProductGroupDescription { get; set; }
        public long? ProductNameId { get; set; }
        public string ProductName { get; set; }
        public long? PackingInforId { get; set; }
        public string PackingUnits { get; set; }
        public long? QuotationUnitsId { get; set; }
        public string UOM { get; set; }
        public long? SupplyToId { get; set; }
        public string SupplyTo { get; set; }
    }
}
