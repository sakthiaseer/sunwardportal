﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class NotesModel : BaseModel
    {
        public long NotesId { get; set; }
        public string Notes { get; set; }
        public long? DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
        public string ContentType { get; set; }
        public bool? IsPersonalNotes { get; set; }
    }
}
