﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OutlookEmailModel
    {
        public string Sender { get; set; }
        public DateTime? SentOn { get; set; }
        public string RecipientsTo { get; set; }
        public string RecipientsCc { get; set; }
        public string Subject { get; set; }
        public string BodyHtml { get; set; }
        public long? UserId { get; set; }
        public long? ProfileId { get; set; }
        public long? FolderId { get; set; }
        public List<OutlookEmailAttachmentModel> Attachments { get; set; }
    }
    public class OutlookEmailAttachmentModel
    {
        public string ContentId { get; set; }
        public string Filename { get; set; }
        public byte[] FileData { get; set; }
        public string Size { get; set; }
        public long? Filesize { get; set; }
    }
}
