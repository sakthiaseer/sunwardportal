﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DeviceManufacturerGroupModel : BaseModel
    {
        public long DeviceManufacturerID { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
        public long? ManufacturerSourceListID { get; set; }
    }
}
