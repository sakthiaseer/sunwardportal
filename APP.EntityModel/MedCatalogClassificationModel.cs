﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MedCatalogClassificationModel:BaseModel
    {
        public long MedCatalogId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? GroupId { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryNo { get; set; }
        public string CategoryNos { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LastNo { get; set; }
        public string ProfileName { get; set; }
        public string GroupName { get; set; }
        public string CategoryName { get; set; }
    }
}
