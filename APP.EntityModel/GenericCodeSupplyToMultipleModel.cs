﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GenericCodeSupplyToMultipleModel 
    {
        public long GenericCodeSupplyToMultipleId { get; set; }
        public long? GenericCodeId { get; set; }
        public long? SupplyToId { get; set; }
        public string Description { get; set; }
        public List<NavItemModel> NavItemsList { get; set; }
    }
}
