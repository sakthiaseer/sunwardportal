﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PKGInformationByPackSizeModel : BaseModel
    {
        public long PkginformationByPackSizeId { get; set; }
        public long? PkgregisteredPackingInformationId { get; set; }
        public string ProfileRefNo { get; set; }
        public long? PackSizeId { get; set; }
        public DateTime? DateOfApproval { get; set; }
        public DateTime? DateOfDownload { get; set; }
        public string VersionNo { get; set; }
        public string Link { get; set; }
        public string PackSize { get; set; }
        public long? ProfileID { get; set; }
    }
}
