﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApproveParam
    {
        public string ScreenId { get; set; }
        public Guid? SessionId { get; set; }
    }
}
