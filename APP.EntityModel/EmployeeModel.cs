﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeModel : BaseModel
    {
        public long EmployeeID { get; set; }
        public long? UserID { get; set; }
        public string SageID { get; set; }
        public long? PlantID { get; set; }
        public long? LevelID { get; set; }
        public string Nricno { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Gender { get; set; }
        public string JobTitle { get; set; }
        public string Email { get; set; }
        public int? TypeOfEmployeement { get; set; }
        public long? LanguageID { get; set; }

        public long? CityID { get; set; }
        public long? RegionID { get; set; }
        public string Signature { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? DateOfEmployeement { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public string Extension { get; set; }
        public string SpeedDial { get; set; }
        public string Mobile { get; set; }
        public string SkypeAddress { get; set; }
        public long? ReportID { get; set; }
        public bool? IsActive { get; set; }
        public string UserCode { get; set; }
        public long? DivisionID { get; set; }      
       
        // [Required(AllowEmptyStrings = false, ErrorMessage = "User name is required!.")]

        public string Name { get; set; }
        public string EmployeeNo { get; set; }
        public byte AuthenticationType { get; set; }
        public string LoginID { get; set; }
        public string LoginPassword { get; set; }
        public long? UserGroupID { get; set; }
        public long? RoleID { get; set; }
        public long? SectionID { get; set; }
        public long? SubSectionID { get; set; }
        public long? SubSectionTID { get; set; }
        public long? DesignationID { get; set; }
        public long? DepartmentID { get; set; }
        public byte?[] AcceptanceLetter { get; set; }
        public DateTime? ExpectedJoiningDate { get; set; }
        public long? AcceptanceStatus { get; set; }
        public bool? isEnableLogin { get; set; } = true;
        public string Status { get; set; }
        public DateTime? AcceptanceStatusDate { get; set; }
        //Drop Down Names
        public string UserName { get; set; }
        public string PlantName { get; set; }
        public string LevelName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string LanguageName { get; set; }
        public string SectionName { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTwoName { get; set; }
        public string DivisionName { get; set; }
        public string CityName { get; set; }
        public string RegionName { get; set; }
        public string ReportName { get; set; }
        public int? HeadCount { get; set; }
        public string DropDownName { get; set; }
        public List<long?> ReportToIDs { get; set; }
        public List<EmployeeInformationModel> EmployeeActingList { get; set; }
        public List<EmployeeInformationModel> EmployeeOverseesList { get; set; }
        public DateTime? ResignAgreedLastDate { get; set; }


    }

}
