﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderEntryModel : BaseModel
    {
        public long SalesOrderEntryID { get; set; }
        public long? ProfileID { get; set; }
        public string ProfileName { get; set; }
        public long? CompanyID { get; set; }
        public int? OrderByID { get; set; }
        public long? CustomerID { get; set; }
        public long? TenderAgencyID { get; set; }
        public string CustomerName { get; set; }
        public int? TypeOfOrderID { get; set; }
        public string TagAlongOrderNo { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderPeriodFrom { get; set; }
        public DateTime? OrderPeriodTo { get; set; }
        public bool? IsMultipleQuatationNo { get; set; }
        public bool? IsExtensionInfoMultiple { get; set; }
        public bool? IsAllowExtension { get; set; }
        public bool? IsEachDistribution { get; set; }
        public int? ExtensionMonth { get; set; }
        public int? ExtensionQuantity { get; set; }
        public Guid? SessionID { get; set; }
        public string OrderEntryType { get; set; }

        public string OrderByName { get; set; }
        public string TypeOfOrderName { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? TypeOfSalesOrderId { get; set; }

        public List<TagAlongCustomerModel> TagAlongCustomerList { get; set; }

        public List<SalesEntryProduct> SalesEntryProducts { get; set; }

        public long? TagAlongCustomerID { get; set; }
        public long? TagAlongTenderID { get; set; }
        public string TagAlongCustomerName { get; set; }
        public long? TagAlongTenderNoID { get; set; }
        public bool? IsTagAlongWithTenderAgency { get; set; }
        public bool? IsWithOrderPeriod { get; set; }
        public long? CrossReferenceId { get; set; }
        public long? BlanketOrderNoId { get; set; }
        public DateTime? DateOfOrder { get; set; }

        public string CrossReferenceNo { get; set; }
        public string ItemNo { get; set; }
        public string TagAlongTenderNo { get; set; }

        public string ItemDescription { get; set; }
        public string ItemUom { get; set; }
        public long? BlanketItemId { get; set; }
        public string QuotationNo { get; set; }
        public string Ponumber { get; set; }
        public long? ProductItemId { get; set; }
        public long? NovateProductId { get; set; }

    }
}
