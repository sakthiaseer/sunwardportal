﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class WikiResponsibilityModel
    {
        public long WikiResponsibilityID { get; set; }
        public long? EmployeeID { get; set; }
        public long? UserID { get; set; }
        public long? UserGroupID { get; set; }

        public List<long?> EmployeeIDs { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public long? ApplicationWikiLineDutyID { get; set; }
        public long? CompanyId { get; set; }
        public string EmployeeName { get; set; }
        public string UserGroupName { get; set; }
        public string DesignationNumber { get; set; }        
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public bool? isSelected { get; set; }
        public int? HeadCount { get; set; }

        public List<long?> WikiResponsibilityIDs { get; set; }
    }
}
