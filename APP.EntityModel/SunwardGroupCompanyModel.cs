﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SunwardGroupCompanyModel : BaseModel
    {
        public long SunwardGroupCompanyID { get; set; }
        public string SunwardGroupCompanyName { get; set; }
        public string CompanyCode { get; set; }
        public long? RegisteredCountryID { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public DateTime? CompanyEstablishedDate { get; set; }
        public string GSTNo { get; set; }

        public string RegisteredCountry { get; set; }
    }
}
