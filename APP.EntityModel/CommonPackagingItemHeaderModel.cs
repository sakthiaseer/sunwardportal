﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackagingItemHeaderModel : BaseModel
    {
        public long CommonPackagingItemId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string AlsoKownAs { get; set; }
        public long? UomId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileName { get; set; }
        public string PackagingItemName { get; set; }
        public long? PackagingItemCategoryId { get; set; }
        public string PackagingItemCategoryName { get; set; }
        public string UomName { get; set; }
        public string PackagingMaterialNo { get; set; }
        public string TypeName { get; set; }
    }
}
