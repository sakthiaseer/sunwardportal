﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ContractDistributionSalesEntryLineModel : BaseModel
    {
        public long ContractDistributionSalesEntryLineID { get; set; }
        public long? SOCustomerID { get; set; }
        public long? PurchaseItemSalesEntryLineID { get; set; }
        public decimal? TotalQty { get; set; }
        public long? Uomid { get; set; }
        public int? NoOfLots { get; set; }
        public string PONumber { get; set; }
        public Guid? SessionID { get; set; }
        public string UOM { get; set; }
        public int? PerFrequencyQty { get; set; }
        public long? FrequencyID { get; set; }
        public string CustomerName { get; set; }
        public decimal? SellingPrice { get; set; }

    }
}
