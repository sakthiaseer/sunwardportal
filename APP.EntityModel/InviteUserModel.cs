﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InviteUserModel 
    {
        public long? TaskCommentId { get; set; }
        public long? TaskId { get; set; }
        public DateTime? DueDate { get; set; }
        public string AssignTask { get; set; }
        public List<long?> InviteUsers { get; set; }
        public long? OnBehalfId { get; set; }
        public long? TaskAppointmentId { get; set; }
    }
}
