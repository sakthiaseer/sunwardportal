﻿using System;
using System.Collections.Generic;

namespace APP.EntityModel
{
    public class TaskNotesModel 
    {
        public long TaskNotesID { get; set; }
        public long? TaskID { get; set; }

        public string Notes { get; set; }

        public DateTime? RemainderDate { get; set; }

        public long? TaskUserID { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool? IsNoReminderDate { get; set; }

        // public DateTime? DueDate { get; set; }
        public string Title { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
