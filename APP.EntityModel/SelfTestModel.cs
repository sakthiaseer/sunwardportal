﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SelfTestModel
    {
        public long SelfTestId { get; set; }
        public DateTime? DateAndTime { get; set; }
        public string Time { get; set; }
        public string Language { get; set; }
        public string Location { get; set; }
        public bool? IsSwstaff { get; set; }
        public long? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string SageId { get; set; }
        public string ContactNo { get; set; }
        public string VisitingPurpose { get; set; }
        public string PurposeOfVisit { get; set; }
        public long? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string NameOfCompany { get; set; }
        public string CompanyRelatedpurposeOfVisit { get; set; }
        public bool? HignFever { get; set; }
        public bool? MildFever { get; set; }
        public bool? SoreThroat { get; set; }
        public bool? MuscleJointPain { get; set; }
        public bool? Headache { get; set; }
        public bool? ShortnessOfBread { get; set; }
        public bool? Diarrhea { get; set; }
        public bool? LossOfVoice { get; set; }
        public bool? Phlegm { get; set; }
        public bool? Flu { get; set; }
        public bool? Gathering { get; set; }
        public string GatheringDetails { get; set; }
        public bool? TravellingCountry { get; set; }
        public string TravellingCountryDetails { get; set; }
        public bool? ContactPatients { get; set; }
        public string ContactPatientsDetails { get; set; }
        public decimal? Temperature { get; set; }
        public Guid? SessionId { get; set; }
        public bool? NauseaVomiting { get; set; }
        public bool? Sneezing { get; set; }
        public bool? Fatigue { get; set; }
        public bool? Conjunctivits { get; set; }
        public bool? LossOfTasteSmell { get; set; }
        public bool? LossOfApetite { get; set; }
        public bool? RunnyStuffyNose { get; set; }
        public bool? Chills { get; set; }
        public bool? NoForAllAbove { get; set; }
        public bool? Coughing { get; set; }
        public string IsSwstaffFlag { get; set; }
        public string HignFeverFlag { get; set; }
        public string MildFeverFlag { get; set; }
        public string SoreThroatFlag { get; set; }
        public string MuscleJointPainFlag { get; set; }
        public string HeadacheFlag { get; set; }
        public string ShortnessOfBreadFlag { get; set; }
        public string DiarrheaFlag { get; set; }
        public string LossOfVoiceFlag { get; set; }
        public string PhlegmFlag { get; set; }
        public string FluFlag { get; set; }
        public string GatheringFlag { get; set; }
        public string TravellingCountryFlag { get; set; }
        public string ContactPatientsFlag { get; set; }
        public string NauseaVomitingFlag { get; set; }
        public string SneezingFlag { get; set; }
        public string FatigueFlag { get; set; }
        public string ConjunctivitsFlag { get; set; }
        public string LossOfTasteSmellFlag { get; set; }
        public string LossOfApetiteFlag { get; set; }
        public string RunnyStuffyNoseFlag { get; set; }
        public string ChillsFlag { get; set; }
        public string NoForAllAboveFlag { get; set; }
        public string CoughingFlag { get; set; }
        public List<long> PersonToSeeIds { get; set; }
        public List<long> CompanyRelatedpersonToSeeIds { get; set; }
        public string SunEmployeeName { get; set; }
        public string SymptomsLists { get; set; }
        public string SymptomsListsSeparate { get; set; }
        public string LongQuestions { get; set; }
        public string HumanMovementCompanyRelated { get; set; }
        public string HumanMovementPrivate { get; set; }
        public string Persontosee { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string PlantCode { get; set; }
        public string OtherCompanyName { get; set; }
        public int? MovementStatusId { get; set; }
        public string MovementStatusName { get; set; }
        public string Description { get; set; }
        public string Hrdescription { get; set; }
        public int? SelfTestStatusId { get; set; }
        public DateTime? SelfTestDate { get; set; }
        public string SelfTestKitLotNo { get; set; }
        public string NricNo { get; set; }
        public DateTime? TimeOfSelfTest { get; set; }
        public string TesKitResult { get; set; }
        public long? DocumentID { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string ContentType { get; set; }
        public string ImageType { get; set; }
        public string ImageData { get; set; }
        public bool? IsVoidStaus { get; set; }
    }
    public class SelfTestDocuments
    {
        public List<Guid?> SessionIds { get; set; }
        public Guid? SessionId { get; set; }
        public string ImageData { get; set; }

    }
    public class SelfTestVoidModel
    {
        public List<long?> Ids { get; set; }
    }
}
