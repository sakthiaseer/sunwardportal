﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InterCompanyPricingSummaryModel
    {
        public long? FromCompanyId { get; set; }
        public long? ToCompanyId { get; set; }
        public long? BusinessCategoryId { get; set; }
        public long? CompanyId { get; set; }
        public string SpecificOrderNo { get; set; }
    }
}
