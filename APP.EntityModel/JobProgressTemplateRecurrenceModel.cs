﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateRecurrenceModel:BaseModel
    {
        public long JobProgressTemplateRecurrenceId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public int? TypeId { get; set; }
        public int? RepeatNos { get; set; }
        public bool? Sunday { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturyday { get; set; }
        public int? OccurenceOptionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? NoOfOccurences { get; set; }
        public string TypeName { get; set; }
        public string OccurenceOptionName { get; set; }
        public List<long?> SelectedDay { get; set; }
    }
}
