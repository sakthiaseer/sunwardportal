﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AssetMaintenanceModel : BaseModel
    {
        public long AssetMaintenanceId { get; set; }
        public long? AssetId { get; set; }
        public DateTime? MaintenanceDate { get; set; }
        public DateTime? NextMaintenanceDate { get; set; }
        public string Description { get; set; }
        public long? PerformedBy { get; set; }
        public decimal? Cost { get; set; }
        public int? DownDuration { get; set; }

        public string Performed { get; set; }
    }
}
