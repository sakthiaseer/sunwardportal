﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductGeneraIInfoReportModel : BaseModel
    {
        public long FinishProductGeneralInfoID { get; set; }
        public string ProductName { get; set; }
        public string RegisterCountryProductName { get; set; }
        public string CallNo { get; set; }
        public string RegistrationReferenceNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CountryName { get; set; }
        public string linkComment { get; set; }
        public string LinkDmscomment { get; set; }
        public string ProductOwner { get; set; }
        public string DrugClassification { get; set; }
        public string RegistrationDocument { get; set; }
        public string ProductRegistrationHolderName { get; set; }
        public string ProductionRegistrationHolderName { get; set; }
        public string PrhspecificProductName { get; set; }
        public string ManufacturingSite { get; set; }
        public string RegisterationCodeName { get; set; }
        public string RegistrationProductCategoryName { get; set; }
        public string RegistrationCategoryClassName { get; set; }
        public string DosageFormName { get; set; }
        public string FPProductName { get; set; }
        public bool IsMaster { get; set; }
        public string Master { get; set; }
        public string Remarks { get; set; }
        public long FinishProductGeneralInfoLineID { get; set; }
        public decimal? PerUnitQTY { get; set; }
        public decimal? ShelfLife { get; set; }
        public byte?[] Picture { get; set; }
        public string StorageCondition { get; set; }
        public decimal? PackQty { get; set; }
        public string PackType { get; set; }
        public decimal? EquvalentSmallestQty { get; set; }
        public decimal? FactorOfSmallestProductionPack { get; set; }
        public decimal? Temparature { get; set; }
        public bool? IsProtectFromLight { get; set; }
        public string PackagingType { get; set; }
        public string Size { get; set; }
        public string PackQtyunitName { get; set; }
        public string PerPackName { get; set; }
        public string EquvalentSmallestUnitName { get; set; }
        public string TemparatureConditionName { get; set; }
        public string SalesPerPackName { get; set; }
        public string IsProtectFromLightStatus { get; set; }
        public string RegistrationSalesName { get; set; }
        public string StorageConditionName { get; set; }
        public string RegistrationPackingStatusName { get; set; }

        public string FPProductInfoLines { get; set; }
    }
}
