﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonMasterMouldChangePartsModel : BaseModel
    {
        public long CommonMasterMouldChangePartsId { get; set; }
        public long? MachineGroupingId { get; set; }
        public long? BformingMouldId { get; set; }
        public string BformingMouldName { get; set; }
        public long? BtrackMouldId { get; set; }
        public string BtrackMouldName { get; set; }
        public long? BsealingId { get; set; }
        public string BsealingName { get; set; }
        public long? BcutterId { get; set; }
        public string BcutterName { get; set; }
        public string BnameOfTheMouldSet { get; set; }
        public decimal? PsizeLength { get; set; }
        public decimal? PsizeWidth { get; set; }
        public long? PshapeId { get; set; }
        public string PshapeName { get; set; }
        public long? PmarkingId { get; set; }
        public string PmarkingName { get; set; }
        public string PmarkingOn { get; set; }
        public long? PallowInterchangeMachineId { get; set; }
        public long? PinterCompanyShareId { get; set; }
        public long? PinterCompanyMachineId { get; set; }
        public string PallowInterchangeMachineName { get; set; }
        public string PinterCompanyShareName { get; set; }
        public string PinterCompanyMachineName { get; set; }
        public long? ProfileID { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string ProfileRefernceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string MachineGroupingName { get; set; }
        public decimal? UpperTipSizeStandard { get; set; }
        public decimal? UpperTipSizeVariation { get; set; }
        public decimal? LowerTipSizeStandardValue { get; set; }
        public decimal? LowerTipSizeVariationValue { get; set; }
        public decimal? UpperTipStraigthStandard { get; set; }
        public decimal? UpperTipStraigthVariation { get; set; }
        public decimal? LowerTipStraigthStandardValue { get; set; }
        public decimal? LowerTipStraigthVariationValue { get; set; }
        public decimal? UpperTipDepthStandard { get; set; }
        public decimal? UpperTipDepthVariation { get; set; }
        public decimal? LowerTipDepthStandardValue { get; set; }
        public decimal? LowerTipDepthVariationValue { get; set; }
        public decimal? UpperBarrelDiameterStandard { get; set; }
        public decimal? UpperBarrelDiameterVariation { get; set; }
        public decimal? LowerBarrelDiameterStandardValue { get; set; }
        public decimal? LowerBarrelDiameterVariationValue { get; set; }
        public decimal? UpperOverallLengthStandard { get; set; }
        public decimal? UpperOverallLengthVariation { get; set; }
        public decimal? LowerOverallLengthStandardValue { get; set; }
        public decimal? LowerOverallLengthVariationValue { get; set; }
        public decimal? UpperCriticalLengthStandard { get; set; }
        public decimal? UpperCriticalLengthVariation { get; set; }
        public decimal? LowerCriticalLengthStandardValue { get; set; }
        public decimal? LowerCriticalLengthVariationValue { get; set; }
        public decimal? UpperCupRadiusStandard { get; set; }
        public decimal? UpperCupRadiusVariation { get; set; }
        public decimal? LowerCupRadiusStandardValue { get; set; }
        public decimal? LowerCupRadiusVariationValue { get; set; }
        public decimal? UpperCupDepthStandard { get; set; }
        public decimal? UpperCupDepthVariation { get; set; }
        public decimal? LowerCupDepthStandardValue { get; set; }
        public decimal? LowerCupDepthVariationValue { get; set; }
        public decimal? UpperLandStandard { get; set; }
        public decimal? UpperLandVariation { get; set; }
        public decimal? LowerLandStandardValue { get; set; }
        public decimal? LowerLandVariationValue { get; set; }
        public decimal? UpperBevelEdgeAngelStandard { get; set; }
        public decimal? UpperBevelEdgeAngelVariation { get; set; }
        public decimal? LowerBevelEdgeAngelStandardValue { get; set; }
        public decimal? LowerBevelEdgeAngelVariationValue { get; set; }
        public decimal? UpperBevelEdgeStandard { get; set; }
        public decimal? UpperBevelEdgeVariation { get; set; }
        public decimal? LowerBevelEdgeStandardValue { get; set; }
        public decimal? LowerBevelEdgeVariationValue { get; set; }
        public decimal? UpperWidthOfScoreStandard { get; set; }
        public decimal? UpperWidthOfScoreVariation { get; set; }
        public decimal? LowerWidthOfScoreStandardValue { get; set; }
        public decimal? LowerWidthOfScoreVariationValue { get; set; }
        public decimal? UpperDepthOfScoreStandard { get; set; }
        public decimal? UpperDepthOfScoreVariation { get; set; }
        public decimal? LowerDepthOfScoreStandardValue { get; set; }
        public decimal? LowerDepthOfScoreVariationValue { get; set; }
        public bool? Embossement { get; set; }
        public decimal? BoreIdmm { get; set; }
        public decimal? Odmm { get; set; }
        public decimal? Heightmm { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public int? TypeoftoolingId { get; set; }
        public string Totalnumbers { get; set; }
        public string Seriesno { get; set; }
        public Guid? SessionId { get; set; }
        public string EmbossementFlag { get; set; }
        public string Typeoftooling { get; set; }
        public List<long> PallowInterchangeMachineIds { get; set; }
    }
}
