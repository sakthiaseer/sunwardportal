﻿using System;
using System.Collections.Generic;



namespace APP.EntityModel
{
    public class CommentAttachmentModel : BaseModel
    {
        public int CommentAttachmentId { get; set; }
        public long TaskCommentId { get; set; }
        public long DocumentId { get; set; }
        public string FileName { get; set; }
        public List<string> FileNames { get; set; }
        public List<long> DocumentIdList { get; set; }

        public List<DocumentsModel> FileDocumentList { get; set; }
   
        public string ContentType { get; set; }
        public List<string> ContentTypes { get; set; }
        public long UserId { get; set; }
    }
}
