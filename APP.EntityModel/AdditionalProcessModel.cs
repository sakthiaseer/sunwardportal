﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AdditionalProcessModel : BaseModel
    {
        public long AdditionalProcessID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
