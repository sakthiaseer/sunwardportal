﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FileProfileTypeDynamicFormModel:BaseModel
    {
        public long? FileProfileTypeDynamicFormId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string DynamicFormData { get; set; }
        public string ProfileNo { get; set; }
        public string FileProfileTypeName { get; set; }
        public long? DocumentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Type { get; set; }
    }
    public class FileProfileTypeDynamicFormLoadModel
    {
        public FileProfileTypeDynamicFormModel FileProfileTypeDynamicFormModel { get; set; }
        public List<FileProfileSetupFormModel> FileProfileSetupFormModel { get; set; }
    }
}
