
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesAdhocNovateOrderModel : BaseModel
    {
        public long SalesAdhocNovateOrderId { get; set; }
        public long? SalesAdhocNovateId { get; set; }
        public long? SalesAdhocId { get; set; }


    }

    public class SalesAdhocNovateModel : BaseModel
    {
        public long SalesAdhocNovateId { get; set; }

        public long? SalesOrderEntryId { get; set; }
        public DateTime? Date { get; set; }
        public long? ItemId { get; set; }
        public long? FromCustomerId { get; set; }
        public DateTime? FromStartMonth { get; set; }
        public decimal? FromTotalQty { get; set; }
        public long? ToCustomerId { get; set; }
        public DateTime? ToStartMonth { get; set; }
        public decimal? ToTotalQty { get; set; }
        public decimal? IntoNoOfLots { get; set; }
        public string NewPono { get; set; }
        public string ContractNo { get; set; }
        public Guid? SessionId { get; set; }

        public long? UomId { get; set; }
        public string Uom { get; set; }
        public string Description { get; set; }

        public string FromCustomerName {get;set;}
        public string ToCustomerName{get;set;}
    }

    public class SalesAdhocNovateAttachmentModel : BaseModel
    {
        public long SalesAdhocNovateAttachmentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public bool Uploaded { get; set; }
        public Guid? SessionId { get; set; }
        public bool IsImage { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public string ProfileNo { get; set; }

    }

}

