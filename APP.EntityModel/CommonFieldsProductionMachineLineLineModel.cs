﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonFieldsProductionMachineLineLineModel:BaseModel
    {
        public long CommonFieldsProductionMachineLineLineId { get; set; }
        public long? CommonFieldsProductionMachineLineId { get; set; }
        public DateTime? DateToStartRequalification { get; set; }
        public DateTime? DateCompleteRequalification { get; set; }
        public int? StatusOfRequalificationId { get; set; }
        public string StatusOfRequalification { get; set; }
        public Guid? SessionId { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
    }
}
