﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationFormSearchModel:BaseModel
    {
        public long ApplicationSearchFormId { get; set; }
        public long? MainFormId { get; set; }
        public long? ApplicationFormId { get; set; }
        public long? ParentId { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsVisible { get; set; }
        public long UniqueId { get; set; }
        public string TableName { get; set; }
    }
}
