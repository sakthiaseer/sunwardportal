﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlasticBagModel:BaseModel
    {
        public long PlasticBagID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? TarWeight { get; set; }
    }
}
