﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskLikesModel : BaseModel
    {
        public long TaskLikesID { get; set; }
        public long? TaskMasterID { get; set; }
        public long? LikedBy { get; set; }
        public string LikedUser { get; set; }
    }
}
