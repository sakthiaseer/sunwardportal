﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InventoryTypeOpeningStockBalanceLineModel : BaseModel
    {
        public long OpeningStockLineId { get; set; }
        public long? OpeningStockId { get; set; }
        public long? ItemId { get; set; }
        public string BatchNo { get; set; }
        public decimal? QtyOnHand { get; set; }
        public string ItemNo { get; set; }
    }
}
