﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemModel
    {

        byte[] imageByte;
        public byte[] ImageByte
        {
            get { return imageByte; }
            set { imageByte = value; }
        }

        string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        int id;
        public int Id { get { return id; } set { id = value; } }

        string folderName;
        public string FolderName
        {
            get { return folderName; }
            set { folderName = value; }
        }
    }
}
