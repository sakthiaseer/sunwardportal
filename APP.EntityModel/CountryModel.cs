﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CountryModel :BaseModel
    {
        public long CountryID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        
    }
}
