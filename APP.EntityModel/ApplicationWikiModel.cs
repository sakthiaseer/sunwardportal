﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationWikiModel : BaseModel
    {
        public long ApplicationWikiId { get; set; }
        public DateTime? WikiDate { get; set; }
        public long? WikiEntryById { get; set; }
        public long? WikiTypeId { get; set; }
        public long? WikiOwnerId { get; set; }
        public long? WikiCategoryId { get; set; }
        public long? WikiTopicId { get; set; }
        public string Title { get; set; }
        public int? TranslationRequiredId { get; set; }
        public string Objective { get; set; }
        public string ScopeDesc { get; set; }
        public string PreRequisition { get; set; }
        public string Content { get; set; }
        public string EntryBy { get; set; }
        public string WikiType { get; set; }
        public string WikiOwner { get; set; }
        public string WikiCategory { get; set; }
        public string WikiTopic { get; set; }
        public string TranslationRequired { get; set; }
        public string VersionNo { get; set; }
        public bool? IsMalay { get; set; }
        public bool? IsChinese { get; set; }
        public bool? IsNoTranslation { get; set; }
        public long? DepartmentId { get; set; }
        public long? TaskLinkId { get; set; }
        public long? ProfileNo { get; set; }
        public string DepartmentName { get; set; }
        public string TaskLinkName { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public string WikiReference { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? NewReviewDate { get; set; }
        public long VersionTableId { get; set; }
        public bool? IsVersionData { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsEdits { get; set; }
        public List<ApplicationWikiLineModel> ApplicationWikiLines { get; set; }
        public decimal? EstimationPreparationTimeMonth { get; set; }
        public int? WikiOwnerTypeId { get; set; }
        public long? PlantId { get; set; }
        public long? EmployeeId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileName { get; set; }
        public string WikiOwnerTypeName { get; set; }
        public DocumentNoSeriesModel ProfileNoModel { get; set; }
        public bool? IsContentEntry { get; set; }
        public string TaskLink { get; set; }
        public string IsContentEntryFlag { get; set; }
        public List<long> WikiCategoryIds { get; set; }
        public List<long> WikiTopicIds { get; set; }
        public long? ProfilePlantId { get; set; }
        public long? ProfileDepartmentId { get; set; }
        public long? ProfileSectionId { get; set; }
        public long? ProfileSubSectionId { get; set; }
        public long? ProfileDivisionId { get; set; }
        public bool? DraftStatus { get; set; }
        public string WikiOwnerName { get; set; }
        public string ContentInfo { get; set; }
        public List<AppWikiTaskLinkModel> AppWikiTaskLinkModel { get; set; }
        public bool? IsAllowDocAccess { get; set; }
        public List<DocumentsModel> documentsModels { get; set; }
        public List<long?> AppWikiReleaseDocIds { get; set; }
        public List<ApplicationWikiLineModel> applicationWikiLineModels { get; set; }
    }
}
