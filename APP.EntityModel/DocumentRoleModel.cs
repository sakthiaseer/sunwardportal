﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace APP.EntityModel
{
    public class DocumentRoleModel : BaseModel
    {
        public long DocumentRoleID { get; set; }
        public string DocumentRoleName { get; set; }
        public string DocumentRoleDescription { get; set; }
    }
}
