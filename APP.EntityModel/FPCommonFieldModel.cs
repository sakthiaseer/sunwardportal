﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPCommonFieldModel : BaseModel
    {
        public long FPCommonFieldID { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? FPNumberID { get; set; }
        public string FPNumber { get; set; }
        public long? FinishProductID { get; set; }
        public long? DosageFormID { get; set; }
        public string DosageForm { get; set; }
        public string ProfileMaster { get; set; }

        public string Profile { get; set; }
        public string ProfileName { get; set; }
        public string DropDownName { get; set; }

        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

    }
}
