﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavisionSpecificationModel : BaseModel
    {
        public long NavisionSpecificationId { get; set; }
        public long? MaterialSpecificationId { get; set; }
        public long? DatabaseId { get; set; }
        public long? NavisionId { get; set; }
        public string Database { get; set; }
        public string Navision { get; set; }
        public string NavisionBuom { get; set; }
        public string NavisionStatus { get; set; }
        public string SpecificationName { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string InternalReference { get; set; }
        public string LinkProfileRefNo { get; set; }
        public string MasterProfileRefNo { get; set; }
        public string ProfileRefNo { get; set; }
        public long? ProfileID { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string MaterialSpecificationNo { get; set; }
        public string MaterialSpecificationName { get; set; }
    }
}
