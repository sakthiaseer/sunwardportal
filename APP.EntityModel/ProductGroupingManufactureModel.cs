﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductGroupingManufactureModel : BaseModel
    {
        public long ProductGroupingManufactureId { get; set; }
        public long? ProductGroupingId { get; set; }
        public long? ManufactureById { get; set; }
        public string ManufactureBy { get; set; }
        public long? SupplyToId { get; set; }
        public string  SupplyTo { get; set; }
        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }

        public string DosageFormName { get; set; }
        public string DrugClassificationName { get; set; }
    }
}
