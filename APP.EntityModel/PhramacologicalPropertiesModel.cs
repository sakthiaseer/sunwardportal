﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PhramacologicalPropertiesModel:BaseModel
    {
        public long PharmacologicalpropertiesId { get; set; }
        public long? FinishProductId { get; set; }
        public bool? IsMasterDocuement { get; set; }
        public bool? IsInformationvsMaster { get; set; }
        public string Pharmacodynamics { get; set; }
        public string Pharmacokinetics { get; set; }
        public string Indication { get; set; }
        public string RecommendedDose { get; set; }
        public string RouteOfAdministration { get; set; }
        public string Contraindications { get; set; }
        public string Warningandprecautions { get; set; }
        public string Intractionwithothermedicaments { get; set; }
        public string Pregnancyandlactations { get; set; }
        public string Sideeffects { get; set; }
        public string Symptomsandoverdose { get; set; }
        public string Driveandusemachine { get; set; }
        public string Preclinicalsafetydata { get; set; }
        public string Instructionforuse { get; set; }
        public string Storagecondition { get; set; }
        public string ProductName { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
        public string ManufacturingSite { get; set; }

    }
}
