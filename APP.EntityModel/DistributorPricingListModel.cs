﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistributorPricingListModel
    {
        public decimal? MarginPercentage { get; set; }
        public List<SellingPriceInformations> SellingPriceInformations { get; set; }
    }
    public class SellingPriceInformations
    {
        public string GenericCode { get; set; }
        public string MarginPercentage { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public string Currency { get; set; }
        public decimal? MinSellingPrice { get; set; }
        public decimal? SummerySellingPrice { get; set; }
        public string SummeryCurrency { get; set; }
        public long? CurrencyId { get; set; }
        public string InternalRef { get; set; }
        public string ItemCategoryCode { get; set; }
    }
}
