﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OperationProcedureModel:BaseModel
    {
        public long OperationProcedureId { get; set; }
        public List<long?> DosageFormId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string DosageFormName{ get; set; }
        public string ManufacturingProcessName { get; set; }

    }

    public class OperationProcedureItem 
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public long? ParentId { get; set; }

    }
}
