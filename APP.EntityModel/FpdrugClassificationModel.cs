﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FpdrugClassificationModel:BaseModel
    {
        public long FpdrugClassificationId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? DrugClassificationId { get; set; }
        public long? ProfileId { get; set; }
        public long? CountryId { get; set; }
        public bool? IsInstruction { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string DrugClassification { get; set; }
        public string ProfileName { get; set; }
        public string CountryName { get; set; }
        public string IsInstructionFlag { get; set; }
    }
}
