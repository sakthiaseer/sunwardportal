﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ClassificationBmrLineModel : BaseModel
    {
        public long ClassificationBmrLineId { get; set; }
        public long? ClassificationBmrId { get; set; }
        public string BatchNo { get; set; }
        public string SupplyTo { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public decimal? OutputQty { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string ProductUom { get; set; }
    }
}
