﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NumberSeriesCodeModel
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
