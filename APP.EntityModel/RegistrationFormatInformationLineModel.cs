﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RegistrationFormatInformationLineModel : BaseModel
    {
        public long RegistrationFormatInformationLineId { get; set; }
        public long? RegistrationFormatInformationId { get; set; }
        public long? PartNameId { get; set; }
        public long? SectionNameId { get; set; }
        public string PartName { get; set; }
        public string SectionName { get; set; }
        public string Numbering { get; set; }
        public string Description { get; set; }

        public int? FieldTypeId { get; set; }
        public string FieldType { get; set; }
        public bool? isEdit { get; set; }
        public bool? isDelete { get; set; }
        public RegistrationFormatInformationLineUsersModel RegistrationFormatInformationLineUsersModels { get; set; }
    }
}
