﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackageLeafletModel : BaseModel
    {
        public long LeafletId { get; set; }
        public decimal? LengthSize { get; set; }
        public decimal? WidthSize { get; set; }
        public long? PackingMaterialId { get; set; }
        public long? PackingItemId { get; set; }
        public long? PackingUnitId { get; set; }

        public string PackingMaterial { get; set; }
        public string PackingItem { get; set; }
        public string PackingUnit { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public int? PrintedOnId { get; set; }
        public string PrintedOnType { get; set; }
        public int? LeafletTypeID { get; set; }
         public bool? IsVersion { get; set; }
    }
}
