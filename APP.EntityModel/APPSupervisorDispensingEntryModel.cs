﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class APPSupervisorDispensingEntryModel : BaseModel
    {
        public long SupervisorDispensingId { get; set; }
        public bool PreLineClearance { get; set; }
        public string WorkOrderNo { get; set; }
        public string ProdOrderNo { get; set; }
        public string WeighingMachine { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemName { get; set; }
        public string LotNo { get; set; }
        public string JobNo { get; set; }
        public string UOM { get; set; }
        public string SubLotNo { get; set; }
        public string QCRefNo { get; set; }
        public string DrumNo { get; set; }
        public decimal? DrumWeight { get; set; }
        
        
        
    }
}
