﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CalibrationVendorInformationModel : BaseModel
    {
        public long CalibrationVendorInformationID { get; set; }
        public string VendorName { get; set; }
        public string MethodOfCalibration { get; set; }
        public DateTime? CalibrationLeadTime { get; set; }
        public string MethodOfQuotation { get; set; }
        public string Rating { get; set; }
        public long? CalibrationServiceInformationID { get; set; }
        public int? Status { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
    }
}
