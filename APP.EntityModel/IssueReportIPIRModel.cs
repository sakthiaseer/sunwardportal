﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IssueReportIPIRModel : BaseModel
    {
        public long Ipirid { get; set; }
        public string Location { get; set; }
        public string ProdOrderNo { get; set; }
        public string Description { get; set; }
        public int? BatchSize { get; set; }
        public string Uom { get; set; }
        public string BatchNo { get; set; }
        public string IssueTitle { get; set; }
        public Guid? DocSessionId { get; set; }
        public long? ProfileId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string ProfileNo { get; set; }
        public long? CompanyId { get; set; }
        public List<long> AssignTos { get; set; } = new List<long>();
        public List<long> CCTos { get; set; } = new List<long>();     

    }

    public class IssueReportAssignToModel
    {
        public long IpirassignToId { get; set; }
        public long? Ipirid { get; set; }
        public long? AssignToId { get; set; }
    }

    public class IssueReportCCToModel
    {
        public long IpircctoId { get; set; }
        public long? Ipirid { get; set; }
        public long? CctoId { get; set; }
    }
}
