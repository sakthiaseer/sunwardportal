using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ExchangeRateModel : BaseModel
    {
        public long ExchangeRateId { get; set; }
        public long? PurposeId { get; set; }
        public string Description { get; set; }
        public DateTime? ValidPeriodFrom { get; set; }
        public DateTime? ValidPeriodTo { get; set; }
        public string PurposeName{get;set;}       

        public long VersionTableId { get; set; }
        public string ReferenceInfo { get; set; }
        public bool? IsVersionData { get; set; }

        public List<ExchangeRateLineModel> ExchangeRateLine { get; set; }   
    }
}
