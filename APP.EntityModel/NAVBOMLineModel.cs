﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVBOMLineModel
    {
        public string ProdBomNo { get; set; }
        public string VersionCode { get; set; }
        public int LineNo { get; set; }
        public string Type { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityPer { get; set; }
        public decimal QuantityPerUnit { get; set; }
        public string BatchSize { get; set; }
        public string Name { get; set; }
    }
}
