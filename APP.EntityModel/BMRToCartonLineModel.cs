﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BMRToCartonLineModel : BaseModel
    {
        public long BmrtoCartonLineId { get; set; }
        public long? BmrtoCartonId { get; set; }
        public string Bmrno { get; set; }
        public long? LoginUserId { get; set; }
    }
}
