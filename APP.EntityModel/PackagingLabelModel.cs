﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingLabelModel : BaseModel
    {
        public long PackagingLabelId { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? LetteringId { get; set; }
        public long? BgPantoneColorId { get; set; }
        public long? BgColorId { get; set; }
        public decimal? SizeLength { get; set; }
        public decimal? SizeWidth { get; set; }
        public long? PackagingMaterialId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PackagingUnitId { get; set; }

        public string PantoneColor { get; set; }
        public string Color { get; set; }
        public string Lettering { get; set; }
        public string BgPantoneColor { get; set; }
        public string BgColor { get; set; }
      
        public string PackagingMaterial { get; set; }
        public string PackagingItem { get; set; }
        public string PackagingUnit { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public int? LabelTypeID { get; set; }
         public bool? IsVersion { get; set; }
    }
}
