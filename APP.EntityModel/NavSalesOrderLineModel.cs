﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavSalesOrderLineModel : BaseModel
    {
        public long NavSalesOrderLineId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNo { get; set; }

        public string VersionNo { get; set; }
        public int? OrderLineNo { get; set; }
        public string SelltoCustomerNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public decimal? TotalOrderQuantity { get; set; }
        public decimal? DeliverQuantity { get; set; }
        public decimal? OutstandingQuantity { get; set; }
        public DateTime? PromisedDeliveryDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string UnitofMeasureCode { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncUserId { get; set; }
        public bool? IsNonDeliverSo { get; set; }
        public string Sostatus { get; set; }

        public List<NonDeliverSOModel> NonDeliverSOModels { get; set; }
    }

    public class NonDeliverSOModel : BaseModel
    {
        public long NonDeliverSoid { get; set; }
        public long? NavSalesOrderLineId { get; set; }
        public bool? IsChangeBalanceQty { get; set; }
        public decimal? NewBalanceQty { get; set; }
        public long? ReasonId { get; set; }
        public string ReasonDescription { get; set; }
        public string UOM { get; set; }
        public DateTime? NewShipmentDate { get; set; }
        public bool? IsLatest { get; set; }
    }
}
