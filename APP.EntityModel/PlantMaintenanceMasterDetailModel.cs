﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlantMaintenanceMasterDetailModel
    {
        public long PlantMaintenanceMasterDetailID { get; set; }
        public long? PlantEntryLineID { get; set; }
        public long? PlantMaintenanceEntryMasterID { get; set; }
    }
}
