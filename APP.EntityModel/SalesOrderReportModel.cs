﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesOrderReportModel
    {
        public long? SalesOrderEntryID { get; set; }
        public long? PurchaseItemSalesEntryLineID { get; set; }
        public long? ContractDistributionSalesEntryLineID { get; set; }
        public long? ProjectedDeliverySalesOrderLineID { get; set; }
        public string CustomerName { get; set; }
        public int? NoOfLots { get; set; }
        public string PONumber { get; set; }
        public decimal? TotalQty { get; set; }
        public DateTime? OrderPeriodFrom { get; set; }
        public DateTime? OrderPeriodTo { get; set; }
        public string TenderNo { get; set; }
        public long? FrequencyID { get; set; }
        public string Frequency { get; set; }
        public int? TotalMonth { get; set; }
        public int? PerFrequencyQty { get; set; }
        public DateTime? StartDate { get; set; }

        public decimal PacketQty { get; set; }

        public List<TenderDistributionModel> TenderDistribution { get; set; }
    }

    public class TenderDistributionModel
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int IntMonth { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalQty { get; set; }
        public DateTime StartDate { get; set; }
    }
}
