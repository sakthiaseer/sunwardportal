﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NoticeLineModel : BaseModel
    {
        public long NoticeLineId { get; set; }
        public long? NoticeId { get; set; }
        public string Link { get; set; }
        public string Wilink { get; set; }
        public int? ModuleId { get; set; }
        public int? FrequencyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? MonthlyDay { get; set; }
        public bool? DaysOfWeek { get; set; }
        public long? CompanyId { get; set; }
        public string ModuleName { get; set; }
        public string FrequencyName { get; set; }
        public List<int?> NoticeWeeklyIds { get; set; }
        public List<int?> DaysOfWeekIds { get; set; }
        public string TypeName { get; set; }
        public long? TypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
