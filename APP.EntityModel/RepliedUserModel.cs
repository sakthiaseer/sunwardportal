﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RepliedUserModel
    {
        public long TaskId { get; set; }
        public long UserId { get; set; }
        public bool IsRead { get; set; }
        public string UserName { get; set; }
    }
}
