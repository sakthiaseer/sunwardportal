﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVRecipesModel
    {
        public long ItemRecipeId { get; set; }
        public string ParentItemNo { get; set; }
        public string ItemNo { get; set; }
        public string RecipeNo { get; set; }
        public string RecipeName { get; set; }
        public string Description { get; set; }
        public string BatchSize { get; set; }
        public string Remark { get; set; }
        public decimal UnitQTY { get; set; }
        public string UOM { get; set; }
        public decimal ProductionTime { get; set; }
        public long? CompanyId { get; set; }
        public string DefaultBatch { get; set; }
        public string MachineCenterCode { get; set; }
        public List<NAVRecipesModel> Children { get; set; } = new List<NAVRecipesModel>();
    }
}
