﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RegistrationFormatInformationLineUsersModel:BaseModel
    {
        public long RegistrationFormatInformationLineUsersId { get; set; }
        public long? RegistrationFormatInformationLineId { get; set; }
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> ItemClassificationAccessIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string UserGroupName { get; set; }
    }
}
