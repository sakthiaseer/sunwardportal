﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVManufacturingProcessModel : BaseModel
    {
        public long ManufacturingProcessID { get; set; }
        public long? CompanyId { get; set; }
        public string PigeonHoleVersion { get; set; }
        public string Process { get; set; }
        public bool? NoChangeinDate { get; set; }
        public string ReplanRefNo { get; set; }
        public string ProdOrderNo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public bool? ReleaseFromPlanner { get; set; }
        public string Substatus { get; set; }
        public string Remarks { get; set; }
        public string DescriptionDetails { get; set; }
    }

    public class NavManufacturingProcessItem
    {
        public string Process { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public List<NavManufacturingDateRange> AvailableDates { get; set; }
    }

    public class NavManufacturingDateRange
    {
        public string Start { get; set; }
        public string Date { get; set; }

        public DateTime CurrentDate { get; set; }
        public string End { get; set; }
        public string Title { get; set; }
        public string ItemNo { get; set; }
        public string Process { get; set; }
        public string ItemDetail { get; set; }
        public string CssClass { get; set; }
        public bool Open { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long? CompanyId { get; set; }
    }
}
