﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterInterCompanyPricingModel : BaseModel
    {
        public long MasterInterCompanyPricingId { get; set; }
        public DateTime? ValidityFrom { get; set; }
        public DateTime? ValidityTo { get; set; }
        public bool? IsNeedVersionFunction { get; set; }
        public long? FromCompanyId { get; set; }
        public long? ToCompanyId { get; set; }
        public string FromCompany { get; set; }
        public string ToCompany { get; set; }
        public decimal? InterCompanyMinMargin { get; set; }
        public decimal? InterCompanyProfitSharing { get; set; }
        public long? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public decimal? MinBillingToCustomer { get; set; }
        public Guid? VersionSessionId { get; set; }
        public string ReferenceInfo { get; set; }

        public List<MasterInterCompanyPricingLineModel> MasterInterCompanyPricingLineModels { get; set; }
    }
}
