﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class StartOfDayModel : BaseModel
    {
        public long StartOfDayId { get; set; }
        public long? CompanyDbid { get; set; }
        public string CompanyDb { get; set; }
        public long? WorkingBlockId { get; set; }
        public string WorkingBlock { get; set; }
        public List<long> ManPowerIds { get; set; }
        public List<long> ProdTaskIds { get; set; }

        public string ManPower { get; set; }
        public string ProdTask { get; set; }
    }
    public class StartOfDayManpowerMultipleModel : BaseModel
    {
        public long StartOfDayManpowerMultipleId { get; set; }
        public long? StartOfDayManpowerId { get; set; }
        public long? StartOfDayId { get; set; }
    }
    public class StartOfDayProdTaskMultipleModel : BaseModel
    {
        public long StartOfDayProdTaskMultipleId { get; set; }
        public long? ProdTaskPerformId { get; set; }
        public long? StartOfDayId { get; set; }
    }
}
