﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeResignationModel
    {
        public long EmployeeResignationID { get; set; }
        public long? EmployeeID { get; set; }
        public int? ResignationType { get; set; }
        public byte?[] ResignationLetter { get; set; }
        public DateTime? IntendedLastDate { get; set; }
        public DateTime? AgreedLastDate { get; set; }
        public string ResigneeName { get; set; }
        public string DocumentName { get; set; }
        public long? DesignationID { get; set; }
        public long? DocumentID { get; set; }
        public long? DesignationHCNo { get; set; }
        public long? ApprovedBy { get; set; }
        public int? ApproveStatus { get; set; }
        public Guid? SessionID { get; set; }
    }
}
