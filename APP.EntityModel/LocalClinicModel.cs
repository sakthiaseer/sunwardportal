﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LocalClinicModel : BaseModel
    {
        public long LocalClinicId { get; set; }
        public long? CountryId { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string PostalCode { get; set; }
        public long? SpecialityId { get; set; }
        public long? SalesmanCodeId { get; set; }
        public int? CompanyStatusId { get; set; }
        public int? ToSwstatusId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string SpecialityName { get; set; }
        public string CompanyStatusName { get; set; }
        public string ToSwstatusName { get; set; }
        public string ClinicName { get; set; }
        public string SalesmanCodeName { get; set; }

    }
}
