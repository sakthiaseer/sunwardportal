﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RoutineInfoModel : BaseModel
    {
        public long RoutineInfoId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public string Description { get; set; }

        public List<long?> RoutineInfoIds { get; set; }
     
    }
}
