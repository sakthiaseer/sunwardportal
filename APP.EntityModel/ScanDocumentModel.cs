﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ScanDocumentModel : BaseModel
    {
        public string DocumentName { get; set; }
        public string FolderName { get; set; }
        public long FileProfileTypeID { get; set; }
        public long ProfileId { get; set; }
        public bool IsFileProfileSelect { get; set; }
        public string ProfileTypeName { get; set; }
        public string DocumentPath { get; set; }
        public bool IsProductionDocument { get; set; }
        public long RetakeID { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public List<ItemModel> Items { get; set; } = new List<ItemModel>();
    }
}
