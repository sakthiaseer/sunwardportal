﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductExcipientModel:BaseModel
    {
        public long FinishProductExcipientId { get; set; }
        public long? FinishProductId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public string MasterDocument { get; set; }
        public bool? IsInformaionvsmaster { get; set; }
        public string ProductName { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long ProfileID { get; set; }
        public List<long> BomInformationIds { get; set; }
        public string ManufacturingSite { get; set; }
        public string RegisterCountry { get; set; }
        public string PRHSpecificName { get; set; }
        public string RegisterHolderName { get; set; }
        public string ProductOwner { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public string BOMProductName { get; set; }
    }
}
