﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PermissionListViewModel
    {
        public long? DocumentUserRoleID { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string FileProfileType { get; set; }
        public List<string> PermissionList { get; set; }
        public string Permission { get; set; }
        public long? FileProfileTypeSetAccessId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }
    }
}
