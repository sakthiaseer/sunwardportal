﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public  class UpdateDescriptionModel
    {
        public long TaskAttachmentID { get; set; }
        public string Description { get; set; }
    }
}
