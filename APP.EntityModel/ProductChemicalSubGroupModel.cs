﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductChemicalSubGroupModel
    {
        public long ProductChemicalSubGroupID { get; set; }
        public long? FinishProductID { get; set; }
        public long? ChemicalSubGroupID { get; set; }
    }
}
