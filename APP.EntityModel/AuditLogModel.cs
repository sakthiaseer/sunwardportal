﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class AuditLogModel
    {
        public long AuditLogId { get; set; }
        public string AuditType { get; set; }
        public string TableName { get; set; }
        public bool? IsPrimaryKey { get; set; }
        public string PrimaryKeyName { get; set; }
        public long? PrimaryKeyValue { get; set; }
        public bool? IsForeignKey { get; set; }
        public string ForeignKeyName { get; set; }
        public string ColumnName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public bool? IsModified { get; set; }
        public DateTime? AuditDate { get; set; }
        public long? AuditByUserId { get; set; }
        public string AuditByUser { get; set; }

    }
    public class AuditSearchModel
    {
        public string TableName { get; set; }
        public long? PrimaryKeyValue { get; set; }
        public List<string> DisplayColumns { get; set; }
    }
}
