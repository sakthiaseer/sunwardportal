﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GridColumns
    {
        public string field { get; set; }
        public string headerText { get; set; } 
        public int width { get; set; } = 150;
        public string format { get; set; } = "N2";
        public int minWidth { get; set; } = 10;
        public bool allowEditing { get; set; } = false;

        public List<GridColumns> columns { get; set; }
    }
}
