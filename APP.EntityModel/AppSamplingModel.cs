﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppSamplingModel : BaseModel
    {
        public int SamplingId { get; set; }
        public string ScanDocument { get; set; }
        public string TicketNo { get; set; }
        public string SublotNo { get; set; }
        public string BatchNo { get; set; }
        public string ItemCode { get; set; }
        public string UOM { get; set; }
        public string ItemDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Company { get; set; }
    }
}
