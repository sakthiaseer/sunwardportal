﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavvendorModel:BaseModel
    {
        public long VendorId { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string VendorItemNo { get; set; }
        public string VendorNames { get; set; }
        public long? CompanyId { get; set; }
    }
}
