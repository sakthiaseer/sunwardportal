using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProdRecipeCycleModel
    {
        public long ProdRecipeCycleId { get; set; }
        public long? ProdCycleId { get; set; }
        public string ItemTypeId { get; set; }
        public string ItemType { get; set; }
        public long? DosageFormId { get; set; }
        public string DosageName { get; set; }
        public long? RecipeId { get; set; }
        public string RecipeName { get; set; }
        public long? BatchSizeId { get; set; }
        public int? NoOfTickets { get; set; }
        public string PlanningMonth { get; set; }
        public long? BatchSizeName { get; set; }
        public List<string> Months { get; set; } = new List<string>();

        public int? CycleMethodId { get; set; }
        public DateTime? SpecificMonth { get; set; }
        public string ByMonth { get; set; }
        public DateTime? SpecificMonthOfTheYear { get; set; }
        public bool? MustFollow { get; set; }
        public string Notes { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? MethodCodeId { get; set; }

        public string MethodName { get; set; }
        public long? ResonForChangeId { get; set; }
        public Guid? SessionId { get; set; }
        public string RreasonForChange { get; set; }







    }
}



