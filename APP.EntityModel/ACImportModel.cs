﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACImportModel
    {
        public string DistName { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string QtyOnHand { get; set; }
        public string Location { get; set; }
        public string Month { get; set; }
        public string StockBalDate { get; set; }
    }
    public class HeaderList
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public int ID { get; set; }   
        public List<HeaderList> ColumnList { get; set; }

    }
    public class JsonList
    {
        public string EntityName { get; set; }
        public int? ColumnId { get; set; }
    }
    public class ACImportExcelModel
    {
        public List<Header> PropertyList { get; set; }
        public List<Header> HeaderList { get; set; }
    }
    public class EmpInsertList
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }

    }
}
