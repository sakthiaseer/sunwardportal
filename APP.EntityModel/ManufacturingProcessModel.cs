﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ManufacturingProcessModel:BaseModel
    {
        public long ManufacturingProcessId { get; set; }
        public long? FinishProductId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsmaster { get; set; }
        public long? ManufacturingFlowChartId { get; set; }
        public string ProductName { get; set; }
        public Guid? SessionID { get; set; }
        public Guid? SessionId { get; set; }
        public string ManufacturingFlowChartName { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
    }
}
