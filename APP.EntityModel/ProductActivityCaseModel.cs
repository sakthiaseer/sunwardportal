﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductActivityCaseModel:BaseModel
    {
        public long ProductActivityCaseId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string ManufacturingProcess{ get; set; }
        public string Instruction { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? CategoryActionId { get; set; }
        public long? ActionId { get; set; }
        public int? ProductionTypeId { get; set; }
        public string Reference { get; set; }
        public string CheckerNotes { get; set; }
        public string Upload { get; set; }
        public string CategoryAction { get; set; }
        public string Action { get; set; }
        public string ProductionType { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public string IsLanguage { get; set; }
        public string ReferenceMalay { get; set; }
        public string CheckerNotesMalay { get; set; }
        public string UploadMalay { get; set; }
        public string ReferenceChinese { get; set; }
        public string CheckerNotesChinese { get; set; }
        public string UploadChinese { get; set; }
        public List<long> ResponsibilityUsers { get; set; }
        public List<long?> CompanyIds { get; set; }
        public long? VersionCodeStatusId { get; set; }
        public string VersionNo { get; set; }
        public string VersionCodeStatus { get; set; }
        public List<long?> CategoryActionIds { get; set; }
        public List<long?> ActionIds { get; set; }
        public Guid? VersionSessionId { get; set; }

        public List<ProductActivityCaseLineModel> ProductActivityCaseLineModels { get; set; }

        public List<ProductActivityCaseResponsDutyModel> ProductActivityCaseResponsDutyModels { get; set; } 

        public List<ProductActivityCaseResponsModel> ProductActivityCaseResponsModels { get; set; } 

        public List<long?> CategoryActionMultipleIds { get; set; }
        public List<long?> CompanyMultipleIds { get; set; }

        public bool? IsEnglishLanguage { get; set; }
        public bool? IsMalayLanguage { get; set; }
        public bool? IsChineseLanguage { get; set; }
        public long? ProfileId { get; set; }
        public long? FileProfileTypeId { get; set; }
    }
}
