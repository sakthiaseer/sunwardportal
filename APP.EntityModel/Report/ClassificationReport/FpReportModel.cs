﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ReportModel
    {
        public string HeaderName { get; set; }
        public List<Header> Headers { get; set; } = new List<Header>();
        public List<dynamic> DynamicModels { get; set; } = new List<dynamic>();
    }
}
