﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ClassificationListingModel
    {
        public List<Header> Headers { get; set; } = new List<Header>();
        public Dictionary<string,string> JSONFields { get; set; } = new Dictionary<string,string>();
        public List<dynamic> DynamicListItems { get; set; } = new List<dynamic>();
    }
}
