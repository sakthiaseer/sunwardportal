﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductReportParam
    {
        public List<long?> DosageFormIds { get; set; }
        public List<int?> StatusCodeIds { get; set; }
    }
}
