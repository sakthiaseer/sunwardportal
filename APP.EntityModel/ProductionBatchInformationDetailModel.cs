﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchInformationDetailModel
    {
        public string TicketNo { get; set; }
        public string ProductionOrderNo { get; set; }
        public string ItemName { get; set; }
        public string ManufacturingStartDate { get; set; }
        public string ExpiryDate { get; set; }
        public string BatchSizeName { get; set; }
        public string UnitsofBatchSizeName { get; set; }
        public string BatchNo { get; set; }
        public long? QtyPack { get; set; }
        public string QtyPackUnitsName { get; set; }  
    }
    public partial class ItemHeaderModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Align { get; set; }
        public bool Sortable { get; set; }
    }
    public partial class ProductionBatchInformationDetailList
    {
        public List<ItemHeaderModel> ItemHeaderModels { get; set; } = new List<ItemHeaderModel>();
        public dynamic ItemJsonHeaders { get; set; }
        public List<ProductionBatchInformationDetailModel> ProductionBatchInformationDetail { get; set; }= new List<ProductionBatchInformationDetailModel>();
        
    }
}
