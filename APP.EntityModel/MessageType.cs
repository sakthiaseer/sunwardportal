﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public enum MessageType
    {
        Text = 1,
        Image = 2,
        Audio = 3
    }
}
