﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DisposalItemModel : BaseModel
    {
        public long DisposalItemId { get; set; }
        public long? InventoryTypeId { get; set; }
        public string InventoryType { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string BatchNo { get; set; }
        public decimal? DisposalQty { get; set; }
        public string Remarks { get; set; }
    }
}
