﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationGloAbbrSearchModel
    {
        public long ApplicationGlossaryId { get; set; }
        public long ApplicationAbbreviationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Definition { get; set; }
        public int GlossaryCount { get; set; }
    }
}
