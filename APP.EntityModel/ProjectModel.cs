﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProjectModel : BaseModel
    {
        public long ProjectID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Guid? SessionID { get; set; }

        public long? TeamID { get; set; }
        public string TeamName { get; set; }
       

    }
}
