﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistributorPricingModel : BaseModel
    {
        public long DistributorPricingID { get; set; }
        public long? BusinessCategoryID { get; set; }
        public long? CustomerID { get; set; }
        public string MarginInformation { get; set; }
        public decimal? DistributorPrice { get; set; }
        public string BusinessCategory { get; set; }
        public string Customer { get; set; }

        public string GenericName { get; set; }
        public string NavNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string UOM { get; set; }
        public string Currency { get; set; }
    }
}
