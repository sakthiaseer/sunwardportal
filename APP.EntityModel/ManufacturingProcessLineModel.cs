﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ManufacturingProcessLineModel:BaseModel
    {
        public long ManufacturingProcessLineId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ProcessStageId { get; set; }
        public long? ManufacturingProcessUploadId { get; set; }
        public string Remarks { get; set; }
        public string ProcessStageName { get; set; }
        public Guid? SessionID { get; set; }
        public string ManufacturingProcessUploadName { get; set; }
    }
}
