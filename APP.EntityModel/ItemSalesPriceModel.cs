﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemSalesPriceModel : BaseModel
    {
       public long ItemSalesPriceID { get; set; }
       public long? CustomerId { get; set; }
        public long? ItemId { get; set; }
        public string ItemName { get; set; }
        public long?  CompanyID { get; set; }
        public string CustomerName { get; set; }       
        public decimal? SellingPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime?  EndDate { get; set; }

        public string Description1 { get; set; }
        public string Description2 { get; set; }

    }
}
