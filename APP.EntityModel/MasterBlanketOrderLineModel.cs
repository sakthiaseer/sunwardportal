﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterBlanketOrderLineModel : BaseModel
    {
        public long MasterBlanketOrderLineID { get; set; }
        public long? MasterBlanketOrderId { get; set; }
        public int? NoOfWeeksPerMonth { get; set; }
        public DateTime? WeekOneStartDate { get; set; }
        public long? WeekOneLargestOrder { get; set; }
        public DateTime? WeekTwoStartDate { get; set; }
        public long? WeekTwoLargestOrder { get; set; }
        public DateTime? WeekThreeStartDate { get; set; }
        public long? WeekThreeLargestOrder { get; set; }
        public DateTime? WeekFourStartDate { get; set; }
        public long? WeekFourLargestOrder { get; set; }
        public long? BalanceQtyForCalculate { get; set; }


    }
}
