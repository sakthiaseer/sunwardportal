﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyWorkDayMasterModel : BaseModel
    {
        public long WorkDateID { get; set; }
        public string CodeNO { get; set; }
        public long? CountryID { get; set; }
        public long? StateID { get; set; }

        public long? ShiftID { get; set; }
        public long? WorkdayShiftID { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public bool? IsMon { get; set; }
        public bool? IsTue { get; set; }
        public bool? IsWed { get; set; }
        public bool? IsThur { get; set; }
        public bool? IsFri { get; set; }
        public bool? IsSat { get; set; }
        public bool? IsSun { get; set; }
        public int StatusCodeID { get; set; }
        public long? AddedByUserID { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserID { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
