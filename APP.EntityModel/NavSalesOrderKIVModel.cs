﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavSalesOrderKivModel
    {
        public long SalesOrderKivid { get; set; }
        public long? CompanyId { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public string InternalRef { get; set; }
        public string ExternalDocNo { get; set; }
        public string CategoryCode { get; set; }
        public decimal? ItemTrackingQty { get; set; }
        public string LocationCode { get; set; }
        public decimal? OutstandingQty { get; set; }
        public decimal? Quantity { get; set; }
        public string SellToCustomerNo { get; set; }
        public string SellToCustomerName { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string TenderNo { get; set; }
        public string Uom { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}
