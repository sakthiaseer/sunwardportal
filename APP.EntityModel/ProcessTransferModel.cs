﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessTransferModel : BaseModel
    {
        public long ProcessTransferId { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public string TrolleyPalletNo { get; set; }
        public string TransferAction { get; set; }

    }

    public class ProcessTransferLineModel : BaseModel
    {
        public long ProcessTransferLineId { get; set; }
        public long ProcessTransferId { get; set; }
        public string DrumInfo { get; set; }
        public string ProcessInfo { get; set; }
        public string Weight { get; set; }
        public string LocationTo { get; set; }
    }
}
