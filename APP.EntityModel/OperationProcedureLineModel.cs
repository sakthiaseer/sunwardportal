﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OperationProcedureLineModel:BaseModel
    {
        public long OperationProcedureLineId { get; set; }
        public long? OperationProcedureId { get; set; }
        public long? OprocedureId { get; set; }
        public string OprocedureName { get; set; }
        public string Description { get; set; }
        public string WILink { get; set; }
        public bool? IsUpdateNavision { get; set; }
        public Guid? SessionId { get; set; }
    }
}
