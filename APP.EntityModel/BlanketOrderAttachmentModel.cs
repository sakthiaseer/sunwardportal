﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlanketOrderAttachmentModel:BaseModel
    {
        public long BlanketOrderAttachmentId { get; set; }
        public long? BlanketOrderId { get; set; }
        public string DocumentType { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }


        public bool Uploaded { get; set; }
   
        public bool IsImage { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public string ProfileNo { get; set; }
    }
}
