﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingPvcfoilModel : BaseModel
    {
        public long PvcFoilId { get; set; }
        public long? PvcTypeId { get; set; }
        public decimal? FoilThickness { get; set; }
        public decimal? FoilWidth { get; set; }

        public string PvcType { get; set; }
        public string BlisterType { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public List<long> PvcFoilBlisterTypeIds { get; set; }
        public int? PvcFoilTypeId { get; set; }
        public string PvcFoilTypeName { get; set; }

          public bool? IsVersion { get; set; }
    }
}
