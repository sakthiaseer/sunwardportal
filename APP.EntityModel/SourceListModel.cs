﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SourceListModel : BaseModel
    {
        public long SourceListID { get; set; }       
        public string Name { get; set; }

        public string SourceNo { get; set; }
        public long? OfficeAddressID { get; set; }
        public long? SiteAddressID { get; set; }       
        public List<ICTCertificateModel> CertificateList { get; set; }
        public List<ICTContactDetailsModel> ContactList { get; set; }
        public AddressModel OfficeAddress { get; set; }
        public AddressModel SiteAddress { get; set; }
    }

    public class SourceListMobile
    {
        public long SourceListID { get; set; }
        public string Name { get; set; }
    }
}
