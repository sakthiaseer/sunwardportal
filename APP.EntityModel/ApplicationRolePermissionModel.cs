﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationRolePermissionModel
    {
        public long RolePermissionID { get; set; }
        public long? RoleID { get; set; }
        public long? PermissionID { get; set; }
       public ApplicationPermissionModel PermissionModel { get; set; }
    }
   
}
