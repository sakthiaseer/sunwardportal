﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ExcipientInformationLineByProcessModel : BaseModel
    {
        public long ExcipientInformationLineByProcessID { get; set; }
        public long? PerUnitFormulationLineID { get; set; }
        public long? MaterialNameID { get; set; }
        public long? MaterialFunctionID { get; set; }
        
        public long? DosageInformationDosageUnitsID { get; set; }
        public long? PerDosageID { get; set; }
        
        public long? OverageUnitsID { get; set; }
        public long? OverageInformationDosageUnitsID { get; set; }

        public string MaterialFunction { get; set; }
        public string MaterialName { get; set; }
        public string Dosage { get; set; }
        public string DosageInformationDosageUnits { get; set; }
        public string PerDosage { get; set; }
        public string Overage { get; set; }
        public string OverageUnits { get; set; }
        public string OverageInformationDosageUnits { get; set; }
        public long? NavisionSpecificationId { get; set; }
        public long? TypeOfIngredientId { get; set; }
        public string SpecName { get; set; }
        public string TypeOfIngredientName { get; set; }
    }
}
