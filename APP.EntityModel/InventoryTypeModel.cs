﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InventoryTypeModel : BaseModel
    {
        public long InventoryTypeId { get; set; }
        public long? CompanyId { get; set; }
        public long? OwnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ItemSourceId { get; set; }
        public long? SourceTreeId { get; set; }
        public string Owner { get; set; }
        public string ItemSource { get; set; }
        public string DropDownName { get; set; }
        public long? CountryId { get; set; }
    }
}
