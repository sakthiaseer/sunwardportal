﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterScrapMasterModel : BaseModel
    {
        public long BlisterScrapId { get; set; }
        public string BlisterComponent { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public string Sublot { get; set; }
        public string BatchSize { get; set; }
        public string PackSize { get; set; }
        public string Bomtype { get; set; }
        public string Uom { get; set; }
        public string ScrapUom { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }        
        public long? ModifiedByUserId { get; set; }
    
        public ICollection<BlisterScrapLineModel> BlisterScrapLine { get; set; }
        public ICollection<BlisterAllowableModel> BlisterAllowable { get; set; }
        
    }
}
