﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MobileShiftModel:BaseModel
    {
        public long MobileShiftId { get; set; }
        public long? PlantId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
