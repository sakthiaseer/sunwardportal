﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterScrapLineModel : BaseModel
    {
        public long BlisterScrapLineId { get; set; }
        public long? BlisterScrapId { get; set; }
        public string Type { get; set; }
        public string TypeText { get; set; }
        public string ProdOrderNo { get; set; }
        public string ProdOrderNoText { get; set; }
        public string Description { get; set; }
        public int? CalculationType { get; set; }
        public string CalculationTypeText { get; set; }
        public decimal? AddonQty { get; set; }
        public decimal? QtyPer { get; set; }
        public string Uom { get; set; }
        public string UomText { get; set; }
        public decimal? ScrapPercentage { get; set; }
        public bool? QcInheritance { get; set; }
        public bool? Inheritance { get; set; }
    }
}
