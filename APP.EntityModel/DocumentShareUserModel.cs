﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentShareUserModel:BaseModel
    {
        public long DocumentShareUserId { get; set; }
        public long? DocumentShareId { get; set; }
        public long? UserId { get; set; }
        public bool? IsShareAnyOne { get; set; }
        public long? DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string FileProfileTypeName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
