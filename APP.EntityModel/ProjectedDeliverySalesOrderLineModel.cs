﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProjectedDeliverySalesOrderLineModel : BaseModel
    {
        public long ProjectedDeliverySalesOrderLineID { get; set; }
        public long? ContractDistributionSalesEntryLineID { get; set; }
        public long? FrequencyID { get; set; }
        public int? PerFrequencyQty { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsQtyReference { get; set; }
        public string Frequency { get; set; }


    }
}
