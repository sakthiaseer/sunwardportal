﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GroupPlanningModel
    {
        public string Itemgrouping { get; set; }
        public long GroupPlanningId { get; set; }
        public long? CompanyId { get; set; }
        public long? ItemId { get; set; }
        public string ProductGroupCode { get; set; }
        public DateTime? StartDate { get; set; }
        public string ItemNo { get; set; }
        public bool HasAC { get; set; }
        public bool AddhocOrder { get; set; }
        public string Months { get; set; }

        public string AddhocCust { get; set; }
        public string Description { get; set; }
        public string ProductGroup { get; set; }
        public string ProductDescription { get; set; }
        public string MethodCode { get; set; }
        public long? MethodCodeId { get; set; }
        public long? SalesCategoryId { get; set; }
        public string GenericCode { get; set; }
        public decimal NoOfTickets { get; set; }
        public decimal SupplyQty { get; set; }
        public decimal DemandQty { get; set; }
        public decimal PreMonthHSQty { get; set; }
        public decimal ProjHSQty { get; set; }
        public decimal SgQty { get; set; }
        public decimal JbQty { get; set; }
        public decimal InTransitSgQty { get; set; }
        public decimal InTransitJbQty { get; set; }
        public decimal PackSize { get; set; }
        public decimal Balance { get; set; }
        public decimal Balance1 { get; set; }
        public decimal SupplyWip { get; set; }
        public decimal DemandKiv { get; set; }
        public decimal SupplyProcess { get; set; }
        public decimal SupplyQty1 { get; set; }
        public decimal DemandQty1 { get; set; }
        public decimal PreMonthHSQty1 { get; set; }
        public decimal ProjHSQty1 { get; set; }

        public decimal SupplyQty2 { get; set; }
        public decimal DemandQty2 { get; set; }
        public decimal PreMonthHSQty2 { get; set; }
        public decimal ProjHSQty2 { get; set; }

        public decimal SupplyQty3 { get; set; }
        public decimal DemandQty3 { get; set; }
        public decimal PreMonthHSQty3 { get; set; }
        public decimal ProjHSQty3 { get; set; }
        public decimal SupplyQty4 { get; set; }
        public decimal DemandQty4 { get; set; }
        public decimal PreMonthHSQty4 { get; set; }
        public decimal ProjHSQty4 { get; set; }
        public decimal SupplyQty5 { get; set; }
        public decimal DemandQty5 { get; set; }
        public decimal PreMonthHSQty5 { get; set; }
        public decimal ProjHSQty5 { get; set; }
        public decimal SupplyQty6 { get; set; }
        public decimal DemandQty6 { get; set; }
        public decimal PreMonthHSQty6 { get; set; }
        public decimal ProjHSQty6 { get; set; }
        public decimal DistSum { get; set; }

        public decimal DistKiv { get; set; }
        public decimal DistQty { get; set; }
        public decimal DistBalance { get; set; }
        public string ItemDescription { get; set; }
        public string ItemDescription1 { get; set; }
        public string RecipeNo { get; set; }
        public string BatchSize { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public int? NoOfTicket { get; set; }
        public bool? OrderCreated { get; set; }

        public string ItemDescriptions { get; set; }
        public string ShelfLife { get; set; }
        public decimal NoOfTickets1 { get; set; }
        public decimal NoOfTickets2 { get; set; }
        public decimal NoOfTickets3 { get; set; }
        public decimal NoOfTickets4 { get; set; }
        public decimal NoOfTickets5 { get; set; }
        public decimal NoOfTickets6 { get; set; }
        public decimal StockBalance { get; set; }
        public bool IsGroupPlanning { get; set; }
        public decimal SymlQty { get; set; }
        public List<NAVRecipesModel> ItemRecipeLists { get; set; }
        public List<string> RecipeLists { get; set; }

        public bool IsPSB { get; set; }


        //public decimal NoOfMonth1 { get; set; }
        //public decimal NoOfMonth2 { get; set; }
        //public decimal NoOfMonth3 { get; set; }
        //public decimal NoOfMonth4 { get; set; }
        //public decimal NoOfMonth5 { get; set; }
        //public decimal NoOfMonth6 { get; set; }

        public decimal NewBalance { get; set; }
        public decimal NewBalance1 { get; set; }
        public decimal NewBalance2 { get; set; }
        public decimal NewBalance3 { get; set; }
        public decimal NewBalance4 { get; set; }
        public decimal NewBalance5 { get; set; }

        public decimal NewBalanceMonth { get; set; }
        public decimal NewBalanceMonth1 { get; set; }
        public decimal NewBalanceMonth2 { get; set; }
        public decimal NewBalanceMonth3 { get; set; }
        public decimal NewBalanceMonth4 { get; set; }
        public decimal NewBalanceMonth5 { get; set; }

        public decimal NewBalanceHolding { get; set; }
        public decimal NewBalanceHolding1 { get; set; }
        public decimal NewBalanceHolding2 { get; set; }
        public decimal NewBalanceHolding3 { get; set; }
        public decimal NewBalanceHolding4 { get; set; }
        public decimal NewBalanceHolding5 { get; set; }

        public decimal ahQty { get; set; }
        public decimal ahMonth { get; set; }
        public decimal pdtQty { get; set; }
        public decimal pdtMonth { get; set; }
        public decimal noOfMonth { get; set; }

        public decimal ahQty1 { get; set; }
        public decimal ahMonth1 { get; set; }
        public decimal pdtQty1 { get; set; }
        public decimal pdtMonth1 { get; set; }
        public decimal noOfMonth1 { get; set; }

        public string Months1 { get; set; }
        public decimal ahQty2 { get; set; }
        public decimal ahMonth2 { get; set; }
        public decimal pdtQty2 { get; set; }
        public decimal pdtMonth2 { get; set; }
        public decimal noOfMonth2 { get; set; }
        public string Months2 { get; set; }

        public decimal ahQty3 { get; set; }
        public decimal ahMonth3 { get; set; }
        public decimal pdtQty3 { get; set; }
        public decimal pdtMonth3 { get; set; }
        public decimal noOfMonth3 { get; set; }
        public string Months3 { get; set; }

        public decimal ahQty4 { get; set; }
        public decimal ahMonth4 { get; set; }
        public decimal pdtQty4 { get; set; }
        public decimal pdtMonth4 { get; set; }
        public decimal noOfMonth4 { get; set; }
        public string Months4 { get; set; }

        public decimal ahQty5 { get; set; }
        public decimal ahMonth5 { get; set; }
        public decimal pdtQty5 { get; set; }
        public decimal pdtMonth5 { get; set; }
        public decimal noOfMonth5 { get; set; }
        public string Months5 { get; set; }
    }
}
