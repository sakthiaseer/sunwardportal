﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterManufacturingProcessModel:BaseModel
    {
        public long OperationProcedureId { get; set; }
        public long? DosageFormId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public List<long?> OperationProcedureIds { get; set; }
    }
}
