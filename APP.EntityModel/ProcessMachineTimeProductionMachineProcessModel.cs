﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class ProcessMachineTimeProductionMachineProcessModel:BaseModel
    {
        public long ProcessMachineTimeProductionMachineProcessId { get; set; }
        public long? ProcessMachineTimeProductionLineLineId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public bool? UseAnyMachine { get; set; }
        public string UseAnyMachineFlag { get; set; }
        public long? MachineNameId { get; set; }
        public string ManufacturingProcess { get; set; }
        public string ManufacturingSteps { get; set; }
        public string MachineName { get; set; }
        public long? ClassificationCompanyId { get; set; }
        public string ClassificationCompany { get; set; }
    }
}
