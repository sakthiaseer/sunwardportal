﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPCalendarHeader
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Align { get; set; }
        public bool Sortable { get; set; }
    }
}
