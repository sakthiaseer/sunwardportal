﻿using System;
using System.Collections.Generic;

namespace APP.EntityModel
{
    public class TaskAttachmentModel : BaseModel
    {
        public long TaskAttachmentID { get; set; }
        public long? TaskMasterID { get; set; }
        public long? DocumentID { get; set; }
        public long? PreviousDocumentID { get; set; }
        public string Document { get; set; }
        public string TaskTitle { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long? FileSize { get; set; }
        public Guid? SessionID { get; set; }
        public bool? IsMajorChange { get; set; }
        public bool? IsNoChange { get; set; }
        public bool? IsReleaseVersion { get; set; }
        public bool IsLatest { get; set; }
        public bool IsLocked { get; set; }
        public string VersionNo { get; set; }
        public long? LockedByUserID { get; set; }
        public bool? IsMeetingNotes { get; set; }
        public bool? IsDiscussionNotes { get; set; }
        public bool? IsSubTask { get; set; }
        public long? UploadedByUserID { get; set; }
        public DateTime? LockedDate { get; set; }
        public DateTime? UploadedDate { get; set; }

        public List<long?> ReadWriteUserID { get; set; }
        public List<long?> ReadonlyUserID { get; set; }
        public string ActualVersionNo { get; set; }
        public string OverwriteVersionNo { get; set; }
        public string DraftingVersionNo { get; set; }
        public string DocumentDescription { get; set; }
        public bool? IsOverwrite { get; set; }

        public string CheckInDescription { get; set; }


        public long? ParentTaskID { get; set; }
        public long? MainTaskId { get; set; }
        public string Title { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NewDueDate { get; set; }
        public string Description { get; set; }
        public long? ProjectID { get; set; }
        public string Link { get; set; }

    }
}
