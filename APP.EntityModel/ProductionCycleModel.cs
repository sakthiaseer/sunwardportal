using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionCycleModel : BaseModel
    {
        public long ProdCycleId { get; set; }
        public string FileName { get; set; }
        public decimal? DraftVersion { get; set; }
        public decimal? ReleaseVersion { get; set; }
        public DateTime? ValidPeriodFromDate { get; set; }
        public DateTime? ValidPeriodToDate { get; set; }
        public List<ProdRecipeCycleModel> ProdReceipeLine { get; set; }
        public long? CompanyId { get; set; }
        public long? MasterFileId { get; set; }
        public Guid? VersionSessionId { get; set; }
        public string MasterFileName { get; set; }
        public string ReferenceInfo { get; set; }

    }
}



