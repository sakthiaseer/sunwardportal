﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ICTCertificateModel : BaseModel
    {
        public long ICTCertificateID { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }        
        public int CertificateType { get; set; }
        public bool IsExpired { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public long? VendorsListID { get; set; }
        public long? SourceListID { get; set; }


    }
}
