﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CalibrationServiceInformationModel : BaseModel
    {
        public long CalibrationServiceInformationID { get; set; }
        public int? CalibrationNo { get; set; }
        public string Instrument { get; set; }
        public string Lot { get; set; }
        public string Machine { get; set; }
        public string SerialNo { get; set; }
        public string DeviceID { get; set; }
        public long? ManufacturerID { get; set; }
        public string Model { get; set; }
        public string Range { get; set; }
        public string RangeOfCalibration { get; set; }
        public DateTime? LastCalibrationDate { get; set; }
        public DateTime? NextCalibrationDate { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public bool? Certificate { get; set; }
        public string StickerCode { get; set; }
        public int? ExpireMonths { get; set; }

        public virtual List<AcceptableCalibrationInformationModel> AcceptableCalibrationInfoList { get; set; }
        public virtual List<CalibrationVendorInformationModel> CalibrationVendorInfoList { get; set; }
        public virtual List<InstructionTypeModel> CalibrationInstructionTypeList { get; set; }
        public virtual List<RangeCalibrationModel> RangeCalibrationList { get; set; }
    }
}
