﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppDashboardModel
    {
        public string DashboardType { get; set; }
        public string Name { get; set; }
        public string TotalCount { get; set; }
        public int Value { get; set; }
        public long UserID { get; set; }
    }
    public class AppDashboardPieModel
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
