﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HumanMovementActionModel:BaseModel
    {
        public long HumanMovementActionId { get; set; }
        public long? HumanMovementId { get; set; }
        public long? PharmacistId { get; set; }
        public DateTime? LogInTime { get; set; }
        public long? ActionToTakeId { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string PharmacistName { get; set; }
        public string ActionToTakeName { get; set; }
        public long? HrloginId { get; set; }
        public string Hrdescription { get; set; }
        public long? HractionToTakeId { get; set; }
        public string Hrcomment { get; set; }
        public int? HrstatusCodeId { get; set; }
        public string HrloginName { get; set; }
        public string HractionToTakeName { get; set; }
        public string HrstatusCodeName { get; set; }
        public DateTime? HrLogInTime { get; set; }

    }
}
