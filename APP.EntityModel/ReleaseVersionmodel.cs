﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
  public  class ReleaseVersionModel
    {
        public long VersionId { get; set; }
        public string VersionNo { get; set; }
        public string Description { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? ReleaseType { get; set; }

        public List<ReleaseDetailsModel> ReleaseDetails { get; set; }
    }

    public class ReleaseDetailsModel
    {
        public long ReleaseDetailId { get; set; }
        public long? VersionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
