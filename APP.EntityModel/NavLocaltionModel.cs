﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavLocaltionModel : BaseModel
    {
        public long NavlocationId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long? ItemId { get; set; }
        public long? PlantId { get; set; }
        public int? StatusCodeId { get; set; }     
      
    }
}
