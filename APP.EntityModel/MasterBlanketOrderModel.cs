﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterBlanketOrderModel : BaseModel
    {
        public long MasterBlanketOrderId { get; set; }
        public long? CompanyID { get; set; }
        public DateTime? FromPeriod { get; set; }
        public DateTime? ToPeriod { get; set; }
        public long? CustomerID { get; set; }
        public bool? IsRequireVersionInformation { get; set; }
        public Guid? VersionSessionId { get; set; }
        public string ReferenceInfo { get; set; }
        public List<MasterBlanketOrderLineModel> masterBlanketOrderLineModels { get; set; }
        public string CustomerName { get; set; }

    }
}
