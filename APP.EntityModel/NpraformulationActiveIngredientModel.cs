﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NpraformulationActiveIngredientModel:BaseModel
    {
        public long NpraformulationActiveIngredientId { get; set; }
        public long? NpraformulationId { get; set; }
        public long? ActiveIngredientId { get; set; }
        public long? SaltFormId { get; set; }
        public decimal? Strength { get; set; }
        public long? UnitsId { get; set; }
        public long? StrengthSaltFreeId { get; set; }
        public long? SourceId { get; set; }
        public long? FormOfSubstanceId { get; set; }
        public string Remarks { get; set; }
        public string ActiveIngredient { get; set; }
        public string SaltForm { get; set; }
        public string Units { get; set; }
        public string StrengthSaltFree { get; set; }
        public string Source { get; set; }
        public string FormOfSubstance { get; set; }
    }
}
