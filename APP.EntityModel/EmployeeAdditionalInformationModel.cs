﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeAdditionalInformationModel
    {
        public long EmployeeAdditionalInfotmationId { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? ReportingDate { get; set; }
        public string SageNoSeries { get; set; }
        public string OfficeExtensionNo { get; set; }
        public string SpeedDialNo { get; set; }
        public byte[] Photo { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanySkype { get; set; }
        public string PortalLogin { get; set; }
        public string SageLogin { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeInformation { get; set; }
        public long? DocumentId { get; set; }
        public Guid? SessionId { get; set; }
        public string DocumentName { get; set; }

    }
}
