﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApproveMasterModel : BaseModel
    {
        public long ApproveMasterId { get; set; }
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public int? ApproveLevelId { get; set; }
        public string ApproveLevel { get; set; }
        public int? ApproveTypeId { get; set; }
        public string ApproveType { get; set; }
    }
}
