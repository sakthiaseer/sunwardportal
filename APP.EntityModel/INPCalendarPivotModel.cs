﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPCalendarPivotModel:BaseModel
    {
        public int IntMonth { get; set; }
        public string VersionName { get; set; }
         public DateTime ReportDate { get; set; }
        public long? CompanyId { get; set; }
        public long ItemId { get; set; }
        public bool IsSteroid { get; set; }
        public string GrouoItemNo { get; set; }
        public string ItemNo { get; set; }
        public string DistNo { get; set; }
        public long DistAcId { get; set; }
        public string Description { get; set; }
        public string ItemDescriptions { get; set; }
        public string Remarks { get; set; }
        public string MethodCode { get; set; }
        public long? MethodCodeId { get; set; }
        public decimal? SalesCategoryId { get; set; }
        public string UOM { get; set; }
        public string Packuom { get; set; }
        public decimal Quantity { get; set; }
        public string Month { get; set; }
        public long PackSize { get; set; }
        public string Customer { get; set; }
        public string AddhocCust { get; set; }
        public decimal ACQty { get; set; }
        public decimal? AntahQty { get; set; }
        public decimal? ApexQty { get; set; }
        public decimal? MissQty { get; set; }
        public decimal? PxQty { get; set; }
        public decimal? AlpsQty { get; set; }
        public decimal? POQty { get; set; }
        public decimal? SgtQty { get; set; }
        public decimal? PsbQty { get; set; }
        public decimal? ProdNotStartQty { get; set; }
        public string ProdNotStartTickets { get; set; }
        public string ShelfLife { get; set; }
        public string Itemgrouping { get; set; }
        public decimal? SymlQty { get; set; }
        public decimal AcSum { get; set; }
        public decimal UnitQty { get; set; }
        public decimal ThreeMonthACQty { get; set; }
        public string ProdRecipe { get; set; }
        public string BatchSize { get; set; }
        public string NoofTickets { get; set; }
        public string NoOfDays { get; set; }
        public decimal DistStockBalance { get; set; }
        public decimal ApexStockBalance { get; set; }
        public decimal AntahStockBalance { get; set; }
        public decimal SgTenderStockBalance { get; set; }
        public decimal PsbStockBalance { get; set; }
        public decimal MsbStockBalance { get; set; }

        public decimal PreApexStockBalance { get; set; }
        public decimal PreAntahStockBalance { get; set; }
        public decimal PreSgTenderStockBalance { get; set; }
        public decimal PrePsbStockBalance { get; set; }
        public decimal PreMsbStockBalance { get; set; }
        public decimal PreMyStockBalance { get; set; }
        public decimal PreOtherStoreQty { get; set; }
        public decimal PreStockBalance { get; set; }
        public decimal PreStockHoldingBalance { get; set; }
        public decimal PrewipQty { get; set; }
        public decimal NAVStockBalance { get; set; }
        public decimal SgStockBalance { get; set; }
        public decimal MyStockBalance { get; set; }
        public decimal OtherStoreQty { get; set; }
        public decimal InterCompanyTransitQty { get; set; }
        public decimal WipQty { get; set; }
        public decimal Rework { get; set; }
        public decimal NoOfTicket { get; set; }
        public decimal kivQty { get; set; }
        public decimal SWKIVQty { get; set; }
        public decimal fbStock { get; set; }
        public decimal adhocStock { get; set; }
        public decimal StockBalance { get; set; }
        public decimal ActualStockBalance { get; set; }
        public decimal StockHoldingBalance { get; set; }
        public decimal StockHoldingPackSize { get; set; }
        public decimal Month1 { get; set; }
        public decimal Month2 { get; set; }
        public decimal Month3 { get; set; }
        public decimal Month4 { get; set; }
        public decimal Month5 { get; set; }
        public decimal Month6 { get; set; }
        public decimal Month7 { get; set; }
        public decimal Month8 { get; set; }
        public decimal Month9 { get; set; }
        public decimal Month10 { get; set; }
        public decimal Month11 { get; set; }
        public decimal Month12 { get; set; }

        public decimal QtyMonth1 { get; set; }
        public decimal QtyMonth2 { get; set; }
        public decimal QtyMonth3 { get; set; }
        public decimal QtyMonth4 { get; set; }
        public decimal QtyMonth5 { get; set; }
        public decimal QtyMonth6 { get; set; }
        public decimal QtyMonth7 { get; set; }
        public decimal QtyMonth8 { get; set; }
        public decimal QtyMonth9 { get; set; }
        public decimal QtyMonth10 { get; set; }
        public decimal QtyMonth11 { get; set; }
        public decimal QtyMonth12 { get; set; }

        public decimal BatchSize450 { get; set; }
        public string BatchSize90 { get; set; }
        public decimal Roundup1 { get; set; }
        public decimal Roundup2 { get; set; }
        public decimal PackSize1 { get; set; }
        public decimal PackSize2 { get; set; }
        public DateTime ReportMonth { get; set; }
        public string Replenishment { get; set; }
        public List<string> RecipeLists { get; set; }
        public List<NAVRecipesModel> ItemRecipeLists { get; set; }
        public List<NAVRecipesModel> OrderRecipeLists { get; set; }
        public List<INPCalendarPivotModel> Children { get; set; }
        public int? StatusCodeId { get; set; }

        public decimal DistTotal { get; set; }
        public decimal? DeliverynotReceived { get; set; }
        public List<string> ItemList { get; set; }
        public List<GenericCodeReport> GenericCodeReport { get; set; }
        public string SalesCategory { get; set; }
        public long? LocationID { get; set; }
        public long? GenericCodeID { get; set; }

        public string Ticket1 { get; set; }
        public string Ticket2 { get; set; }
        public string Ticket3 { get; set; }
        public string Ticket4 { get; set; }
        public string Ticket5 { get; set; }
        public string Ticket6 { get; set; }
        public string Ticket7 { get; set; }
        public string Ticket8 { get; set; }
        public string Ticket9 { get; set; }
        public string Ticket10 { get; set; }
        public string Ticket11 { get; set; }
        public string Ticket12 { get; set; }

        public string OutputTicket1 { get; set; }
        public string OutputTicket2 { get; set; }
        public string OutputTicket3 { get; set; }
        public string OutputTicket4 { get; set; }
        public string OutputTicket5 { get; set; }
        public string OutputTicket6 { get; set; }
        public string OutputTicket7 { get; set; }
        public string OutputTicket8 { get; set; }
        public string OutputTicket9 { get; set; }
        public string OutputTicket10 { get; set; }
        public string OutputTicket11 { get; set; }
        public string OutputTicket12 { get; set; }

        public decimal ProductionTicket1 { get; set; }
        public decimal ProductionTicket2 { get; set; }
        public decimal ProductionTicket3 { get; set; }
        public decimal ProductionTicket4 { get; set; }
        public decimal ProductionTicket5 { get; set; }
        public decimal ProductionTicket6 { get; set; }
        public decimal ProductionTicket7 { get; set; }
        public decimal ProductionTicket8 { get; set; }
        public decimal ProductionTicket9 { get; set; }
        public decimal ProductionTicket10 { get; set; }
        public decimal ProductionTicket11 { get; set; }
        public decimal ProductionTicket12 { get; set; }

        public decimal TicketHoldingStock1 { get; set; }
        public decimal TicketHoldingStock2 { get; set; }
        public decimal TicketHoldingStock3 { get; set; }
        public decimal TicketHoldingStock4 { get; set; }
        public decimal TicketHoldingStock5 { get; set; }
        public decimal TicketHoldingStock6 { get; set; }
        public decimal TicketHoldingStock7 { get; set; }
        public decimal TicketHoldingStock8 { get; set; }
        public decimal TicketHoldingStock9 { get; set; }
        public decimal TicketHoldingStock10 { get; set; }
        public decimal TicketHoldingStock11 { get; set; }
        public decimal TicketHoldingStock12 { get; set; }

        public decimal ProjectedHoldingStock1 { get; set; }
        public decimal ProjectedHoldingStock2 { get; set; }
        public decimal ProjectedHoldingStock3 { get; set; }
        public decimal ProjectedHoldingStock4 { get; set; }
        public decimal ProjectedHoldingStock5 { get; set; }
        public decimal ProjectedHoldingStock6 { get; set; }
        public decimal ProjectedHoldingStock7 { get; set; }
        public decimal ProjectedHoldingStock8 { get; set; }
        public decimal ProjectedHoldingStock9 { get; set; }
        public decimal ProjectedHoldingStock10 { get; set; }
        public decimal ProjectedHoldingStock11 { get; set; }
        public decimal ProjectedHoldingStock12 { get; set; }

        public decimal ProjectedHoldingStockQty1 { get; set; }
        public decimal ProjectedHoldingStockQty2 { get; set; }
        public decimal ProjectedHoldingStockQty3 { get; set; }
        public decimal ProjectedHoldingStockQty4 { get; set; }
        public decimal ProjectedHoldingStockQty5 { get; set; }
        public decimal ProjectedHoldingStockQty6 { get; set; }
        public decimal ProjectedHoldingStockQty7 { get; set; }
        public decimal ProjectedHoldingStockQty8 { get; set; }
        public decimal ProjectedHoldingStockQty9 { get; set; }
        public decimal ProjectedHoldingStockQty10 { get; set; }
        public decimal ProjectedHoldingStockQty11 { get; set; }
        public decimal ProjectedHoldingStockQty12 { get; set; }

        public decimal OutputProjectedHoldingStockQty1 { get; set; }
        public decimal OutputProjectedHoldingStockQty2 { get; set; }
        public decimal OutputProjectedHoldingStockQty3 { get; set; }
        public decimal OutputProjectedHoldingStockQty4 { get; set; }
        public decimal OutputProjectedHoldingStockQty5 { get; set; }
        public decimal OutputProjectedHoldingStockQty6 { get; set; }
        public decimal OutputProjectedHoldingStockQty7 { get; set; }
        public decimal OutputProjectedHoldingStockQty8 { get; set; }
        public decimal OutputProjectedHoldingStockQty9 { get; set; }
        public decimal OutputProjectedHoldingStockQty10 { get; set; }
        public decimal OutputProjectedHoldingStockQty11 { get; set; }
        public decimal OutputProjectedHoldingStockQty12 { get; set; }

        public decimal OutputProjectedHoldingStock1 { get; set; }
        public decimal OutputProjectedHoldingStock2 { get; set; }
        public decimal OutputProjectedHoldingStock3 { get; set; }
        public decimal OutputProjectedHoldingStock4 { get; set; }
        public decimal OutputProjectedHoldingStock5 { get; set; }
        public decimal OutputProjectedHoldingStock6 { get; set; }
        public decimal OutputProjectedHoldingStock7 { get; set; }
        public decimal OutputProjectedHoldingStock8 { get; set; }
        public decimal OutputProjectedHoldingStock9 { get; set; }
        public decimal OutputProjectedHoldingStock10 { get; set; }
        public decimal OutputProjectedHoldingStock11 { get; set; }
        public decimal OutputProjectedHoldingStock12 { get; set; }

        public decimal ProductionProjected1 { get; set; }
        public decimal ProductionProjected2 { get; set; }
        public decimal ProductionProjected3 { get; set; }
        public decimal ProductionProjected4 { get; set; }
        public decimal ProductionProjected5 { get; set; }
        public decimal ProductionProjected6 { get; set; }
        public decimal ProductionProjected7 { get; set; }
        public decimal ProductionProjected8 { get; set; }
        public decimal ProductionProjected9 { get; set; }
        public decimal ProductionProjected10 { get; set; }
        public decimal ProductionProjected11 { get; set; }
        public decimal ProductionProjected12 { get; set; }

        public decimal QtyProductionProjected1 { get; set; }
        public decimal QtyProductionProjected2 { get; set; }
        public decimal QtyProductionProjected3 { get; set; }
        public decimal QtyProductionProjected4 { get; set; }
        public decimal QtyProductionProjected5 { get; set; }
        public decimal QtyProductionProjected6 { get; set; }
        public decimal QtyProductionProjected7 { get; set; }
        public decimal QtyProductionProjected8 { get; set; }
        public decimal QtyProductionProjected9 { get; set; }
        public decimal QtyProductionProjected10 { get; set; }
        public decimal QtyProductionProjected11 { get; set; }
        public decimal QtyProductionProjected12 { get; set; }

        public decimal BlanketAddhoc1 { get; set; }
        public decimal BlanketAddhoc2 { get; set; }
        public decimal BlanketAddhoc3 { get; set; }
        public decimal BlanketAddhoc4 { get; set; }
        public decimal BlanketAddhoc5 { get; set; }
        public decimal BlanketAddhoc6 { get; set; }
        public decimal BlanketAddhoc7 { get; set; }
        public decimal BlanketAddhoc8 { get; set; }
        public decimal BlanketAddhoc9 { get; set; }
        public decimal BlanketAddhoc10 { get; set; }
        public decimal BlanketAddhoc11 { get; set; }
        public decimal BlanketAddhoc12 { get; set; }

        public bool isTenderExist { get; set; }
        public decimal TenderSum { get; set; }

        public bool IsTenderExist1 { get; set; }
        public bool IsTenderExist2 { get; set; }
        public bool IsTenderExist3 { get; set; }
        public bool IsTenderExist4 { get; set; }
        public bool IsTenderExist5 { get; set; }
        public bool IsTenderExist6 { get; set; }
        public bool IsTenderExist7 { get; set; }
        public bool IsTenderExist8 { get; set; }
        public bool IsTenderExist9 { get; set; }
        public bool IsTenderExist10 { get; set; }
        public bool IsTenderExist11 { get; set; }
        public bool IsTenderExist12 { get; set; }


        public bool IsAcExist { get; set; }

        public decimal? Qty { get; set; }
        public string Comment { get; set; }
        public int? Week { get; set; }
        public bool? IsApproval { get; set; }

        public decimal? ExistingTicket { get; set; }
        public int TicketMonth { get; set; }
        public string TicketMonthName { get; set; }
        public decimal? FullTicket { get; set; }
        public decimal? SplitTicket { get; set; }
        public decimal? QtyTicket { get; set; }
        public decimal? Amount { get; set; }
        public decimal? ManagerAmount { get; set; }
        public decimal? NewAcMonth { get; set; }

        public decimal? ProdQty1 { get; set; }
        public decimal? ProdQty2 { get; set; }
        public decimal? ProdQty3 { get; set; }
        public decimal? ProdQty4 { get; set; }
        public decimal? ProdQty5 { get; set; }
        public decimal? ProdQty6 { get; set; }
        public decimal? ProdQty7 { get; set; }
        public decimal? ProdQty8 { get; set; }
        public decimal? ProdQty9 { get; set; }
        public decimal? ProdQty10 { get; set; }
        public decimal? ProdQty11 { get; set; }
        public decimal? ProdQty12 { get; set; }

        public bool IsTicketCalculated { get; set; }

        public decimal? ProdFrequency { get; set; }
        public decimal? DistReplenishHs { get; set; }
        public decimal? DistAcmonth { get; set; }
        public decimal? AdhocReplenishHs { get; set; }
        public int? AdhocMonthStandAlone { get; set; }
        public decimal? AdhocPlanQty { get; set; }

        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }

        public decimal ProductionRefresh1 { get; set; }
        public decimal ProductionRefresh2 { get; set; }
        public decimal ProductionRefresh3 { get; set; }
        public decimal ProductionRefresh4 { get; set; }
        public decimal ProductionRefresh5 { get; set; }
        public decimal ProductionRefresh6 { get; set; }
        public decimal ProductionRefresh7 { get; set; }

        public decimal ahQty { get; set; }
        public decimal ahMonth { get; set; }
        public decimal pdtQty { get; set; }
        public decimal pdtMonth { get; set; }
        public decimal noOfMonth { get; set; }
        public decimal adjnoOfTicket { get; set; }
        public decimal addedQty { get; set; }
        public decimal addedMonth { get; set; }

        public decimal groupticketMonth { get; set; }
        public decimal groupticketQty { get; set; }

        public decimal? groupticketMonth1 { get; set; }
        public decimal? groupticketQty1 { get; set; }
        public decimal? groupticketMonth2 { get; set; }
        public decimal? groupticketQty2 { get; set; }
        public decimal? groupticketMonth3 { get; set; }
        public decimal? groupticketQty3 { get; set; }
        public decimal? groupticketMonth4 { get; set; }
        public decimal? groupticketQty4 { get; set; }
        public decimal? groupticketMonth5 { get; set; }
        public decimal? groupticketQty5 { get; set; }
        public decimal? groupticketMonth6 { get; set; }
        public decimal? groupticketQty6 { get; set; }
        public decimal? groupticketMonth7 { get; set; }
        public decimal? groupticketQty7 { get; set; }
        public decimal? groupticketMonth8 { get; set; }
        public decimal? groupticketQty8 { get; set; }
        public decimal? groupticketMonth9 { get; set; }
        public decimal? groupticketQty9 { get; set; }
        public decimal? groupticketMonth10 { get; set; }
        public decimal? groupticketQty10 { get; set; }
        public decimal? groupticketMonth11 { get; set; }
        public decimal? groupticketQty11 { get; set; }
        public decimal? groupticketMonth12 { get; set; }
        public decimal? groupticketQty12 { get; set; }

        public decimal? workingQty1 { get; set; }
        public decimal? workingQty2 { get; set; }
        public decimal? workingQty3 { get; set; }
        public decimal? workingQty4 { get; set; }
        public decimal? workingQty5 { get; set; }
        public decimal? workingQty6 { get; set; }
        public decimal? workingQty7 { get; set; }
        public decimal? workingQty8 { get; set; }
        public decimal? workingQty9 { get; set; }
        public decimal? workingQty10 { get; set; }
        public decimal? workingQty11 { get; set; }
        public decimal? workingQty12 { get; set; }

        public List<ProposedAddhocModel> ProposedAddhocOrders { get; set; }
    }

    public class GenericCodeReport
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string ItemCategory { get; set; }
        public string InternalRefNo { get; set; }
        public string MethodCode { get; set; }
        public string DistItem { get; set; }
        public decimal StockBalance { get; set; }

        public string Whse { get; set; }
        public string LotNumber { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string SumOfOnHandQty { get; set; }

        public string OpeningStockValuecompany { get; set; }
        public string OpeningStockValuesupplyBy { get; set; }
        public string OpeningStockValuedosage { get; set; }
        public string OpeningStockValueclassification { get; set; }
        public string OpeningStockValuetotalValue { get; set; }

        public string NewForcastStockValuecompany { get; set; }
        public string NewForcastStockValuesupplyBy { get; set; }
        public string NewForcastStockValuedosage { get; set; }
        public string NewForcastStockValueclassification { get; set; }
        public string NewForcastStockValuetotalValue { get; set; }
    }

    public class PSBReportVersion
    {
        public string TableName { get; set; }
        public string ScreenId { get; set; }
        public string ReferenceInfo { get; set; }
        public Guid SessionId { get; set; }
        public List<INPCalendarPivotModel> VersionData { get; set; }
        public long PrimaryKey { get; set; }
        public string JsonData { get; set; }
        public long AddedByUserId { get; set; }
    }

    public class ProposedAddhocModel
    {
        public string VersionNo { get; set; }
        public string Itemgrouping { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long CompanyId { get; set; }
        public long ItemId { get; set; }
        public string ItemNo { get; set; }
        public string GrouoItemNo { get; set; }
        public long PackSize { get; set; }
        public decimal WipQty { get; set; }
        public decimal TotalStock { get; set; }
        public decimal fbStock { get; set; }
        public decimal addhocStock { get; set; }
        public string FromMonth { get; set; }
        public string ToMonth { get; set; }
        public decimal TotalAddhocStock { get; set; }
        public decimal TotalPropStock { get; set; }
        public decimal TotalPropFbStock { get; set; }
        public decimal TotalPropAddhocStock { get; set; }

        public string allocateStock { get; set; }
        public decimal adjustStock { get; set; }
    }

    public   class GroupTicketPlaningReportModel
    {
        public long GroupPlaningId { get; set; }
        public long? CompanyId { get; set; }
        public string ItemNo { get; set; }
        public long? ItemId { get; set; }
        public int? ReportYear { get; set; }
        public int? ReportMonth { get; set; }
        public decimal? Ahqty { get; set; }
        public decimal? Ahmonth { get; set; }
        public decimal? PdtMonth { get; set; }
        public decimal? PdtQty { get; set; }
        public decimal? ProjHs { get; set; }
        public decimal? NoOfTickets { get; set; }
        public int? AhnoOfMonths { get; set; }
        public string VersionName { get; set; }
        public Guid? SessionId { get; set; }

        public DateTime ReportDate { get; set; }
        public int IntMonth { get; set; }
    }
}
