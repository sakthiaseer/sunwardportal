﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesTargetByStaffModel:BaseModel
    {
        public long SalesTargetByStaffId { get; set; }
        public long? CompanyId { get; set; }
        public long? SalesGroupId { get; set; }
        public long? LeadById { get; set; }
        public string SalesGroup { get; set; }
        public string LeadBy { get; set; }
        public List<SalesTargetByStaffLineModel> SalesTargetByStaffLineModel { get; set; }
    }
}
