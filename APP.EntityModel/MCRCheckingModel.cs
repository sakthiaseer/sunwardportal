﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MCRCheckingModel
    {
        public string PoNo { get; set; }
        public decimal? PoBalance { get; set; }
        public decimal? Supply { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Demand { get; set; }
        public string LocationCode { get; set; }
        public string SourceName { get; set; }
        public string RefPurchaseOrderNo { get; set; }
        public string ReplanRefNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? SafetyLeadTime { get; set; }
        public DateTime? OrderDate { get; set; }
        public string ItemNo { get; set; }
        public decimal? Quantity { get; set; }
    }
}
    