﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SellingCatalogueSearchModel
    {
        public long? CountryId { get; set; }
        public long? CustomerId { get; set; }
        public long? ProductId { get; set; }
        public DateTime? FromValidity { get; set; }
        public DateTime? ToValidity { get; set; }
    }
}
