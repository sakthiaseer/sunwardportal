﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavMethodCodeLinesModel : BaseModel
    {
        public long MethodCodeLineID { get; set; }
        public long? MethodCodeID { get; set; }
        public long? ItemId { get; set; }
        public string ItemName { get; set; }
        public string MethodCodeName { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string CategoryCode { get; set; }
        public string CompanyName { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public int? PackSize { get; set; }
        public string PackUom { get; set; }
    }
}
