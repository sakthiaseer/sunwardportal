﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PortfolioAttachmentModel
    {
        public long PortfolioAttachmentId { get; set; }
        public long? PortfolioLineId { get; set; }
        public long? DocumentID { get; set; }
        public long? PreviousDocumentId { get; set; }
        public bool? IsLatest { get; set; }
        public bool? IsLocked { get; set; }
        public string VersionNo { get; set; }
        public long? LockedByUserId { get; set; }
        public DateTime? LockedDate { get; set; }
        public long? UploadedByUserId { get; set; }
        public long? AddedByUserID { get; set; }
        public DateTime? UploadedDate { get; set; }

        public string FileName { get; set; }
        public Guid? SessionID { get; set; }
    }
}
