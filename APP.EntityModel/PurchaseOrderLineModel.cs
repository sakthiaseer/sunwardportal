﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PurchaseOrderLineModel:BaseModel
    {
        public long PurchaseOrderLineId { get; set; }
        public long? PurchaseOrderId { get; set; }
        public long? NavItemId { get; set; }
        public decimal? Qty { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? UnitPrice { get; set; }
        public long? DeliveryTypeId { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public long? DeliveryToId { get; set; }
        public string Remarks { get; set; }
        public string CurrencyName { get; set; }
        public string DeliveryType { get; set; }
        public string NavisionItem { get; set; }
        public string PurchaseUOM { get; set; }
        public string NavDescription { get; set; }
    }
}
