﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CityMasterModel : BaseModel
    {
        public long CityID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
