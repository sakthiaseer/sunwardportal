﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IcmasterOperationModel:BaseModel
    {
        public long IcmasterOperationId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public string MasterOperation { get; set; }
        public string WiLink { get; set; }
        public bool? Training { get; set; }
        public string TrainingFlag { get; set; }
        public string ManufacturingStepsName { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileId { get; set; }
    }
}
