﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPCalendarModel
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string MethodCode { get; set; }
        public string UOM { get; set; }
        public string CustomerName { get; set; }
        public decimal Quantity { get; set; }
        public int IntMonth { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? EarlistShipmentDate { get; set; }
        public decimal? Month1 { get; set; }
        public decimal? Month2 { get; set; }
        public decimal? Month3 { get; set; }
        public decimal? Month4 { get; set; }
        public decimal? Month5 { get; set; }
        public decimal? Month6 { get; set; }
        public decimal? Month7 { get; set; }
        public decimal? Month8 { get; set; }
        public decimal? Month9 { get; set; }
        public decimal? Month10 { get; set; }
        public decimal? Month11 { get; set; }
        public decimal? Month12 { get; set; }

        public decimal? Month1Total { get; set; }
        public decimal? Month2Total { get; set; }
        public decimal? Month3Total { get; set; }
        public decimal? Month4Total { get; set; }
        public decimal? Month5Total { get; set; }
        public decimal? Month6Total { get; set; }
        public decimal? Month7Total { get; set; }
        public decimal? Month8Total { get; set; }
        public decimal? Month9Total { get; set; }
        public decimal? Month10Total { get; set; }
        public decimal? Month11Total { get; set; }
        public decimal? Month12Total { get; set; }

        public DateTime? Month1CompletionDate { get; set; }
        public DateTime? Month2CompletionDate { get; set; }
        public DateTime? Month3CompletionDate { get; set; }
        public DateTime? Month4CompletionDate { get; set; }
        public DateTime? Month5CompletionDate { get; set; }
        public DateTime? Month6CompletionDate { get; set; }
        public DateTime? Month7CompletionDate { get; set; }
        public DateTime? Month8CompletionDate { get; set; }
        public DateTime? Month9CompletionDate { get; set; }
        public DateTime? Month10CompletionDate { get; set; }
        public DateTime? Month11CompletionDate { get; set; }
        public DateTime? Month12CompletionDate { get; set; }

        public DateTime? EarlistShipment1 { get; set; }
        public DateTime? EarlistShipment2 { get; set; }
        public DateTime? EarlistShipment3 { get; set; }
        public DateTime? EarlistShipment4 { get; set; }
        public DateTime? EarlistShipment5 { get; set; }
        public DateTime? EarlistShipment6 { get; set; }
        public DateTime? EarlistShipment7 { get; set; }
        public DateTime? EarlistShipment8 { get; set; }
        public DateTime? EarlistShipment9 { get; set; }
        public DateTime? EarlistShipment10 { get; set; }
        public DateTime? EarlistShipment11 { get; set; }
        public DateTime? EarlistShipment12 { get; set; }

        public DateTime? CompletionBefore { get; set; }

        public List<INPCalendarModel> ChildCalendarItems { get; set; } = new List<INPCalendarModel>();
    }
}
