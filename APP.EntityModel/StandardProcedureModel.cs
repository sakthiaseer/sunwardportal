﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class StandardProcedureModel : BaseModel
    {
        public long StandardProcedureID { get; set; }
        public long? ManufacturingSiteID { get; set; }
        public long? NoID { get; set; }
        public long? ICMasterOperationID { get; set; }
        public long? StandardManufacturingProcessId { get; set; }

        public string ManufacturingSite { get; set; }
        public string OperationName { get; set; }
    }
}
