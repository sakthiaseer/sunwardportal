﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskUnReadNotesModel

    {
        public long TaskUnReadNotesId { get; set; }
        public string Notes { get; set; }
        public DateTime? MyDueDate { get; set; }
        public long? UserId { get; set; }
        public long? TaskId { get; set; }
        public long? TaskCommentId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
