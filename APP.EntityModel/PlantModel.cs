﻿using System;

namespace APP.EntityModel
{
    public class PlantModel : BaseModel
    {
        public long PlantID { get; set; }
        public long CompanyID { get; set; }
        public string PlantCode { get; set; }

        public string Description { get; set; }
        public long? RegisteredCountryID { get; set; }
        public string RegistrationNo { get; set; }

        public string RegisteredCountry { get; set; }
        public DateTime? EstablishedDate { get; set; }
        public string GSTNo { get; set; }
        public string NavSoapAddress { get; set; }

        public bool IsLinkNav { get; set; }

        public string NavCompanyName { get; set; }
        public string NavUserName { get; set; }
        public string NavPassword { get; set; }
        public string NavOdataAddress { get; set; }

        public string CompanyName { get; set; }
        public long? CountryId { get; set; }
    }

    public class PlantMobileModel
    {
        public long PlantID { get; set; }
        public string PlantCode { get; set; }
        public string Description { get; set; }
        public string NavCompanyName { get; set; }
        public string NavUserName { get; set; }
        public string NavPassword { get; set; }
        public string NavOdataAddress { get; set; }
        public string NavSoapAddress { get; set; }
    }
}
