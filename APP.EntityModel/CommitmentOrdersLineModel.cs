﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommitmentOrdersLineModel : BaseModel
    {
        public long CommitmentOrdersLineId { get; set; }
        public long? CommitmentOrdersId { get; set; }
        public long? ProductId { get; set; }
        public string Source { get; set; }
        public decimal? Quantity { get; set; }
        public long? Uomid { get; set; }
        public long? PackingId { get; set; }
        public long? ShippingTermsId { get; set; }
        public bool? IsTenderExceed { get; set; }
        public bool? IsPlanned { get; set; }
        public Guid? SessionId { get; set; }
        public string Remarks { get; set; }
        public string ProductName { get; set; }
        public DateTime? CommitmentDate { get; set; }

    }
    public class CommitmentOrdersReportModel
    {
        public long CommitmentOrdersLineId { get; set; }
        public long? CommitmentOrdersId { get; set; }
        public long? ProductId { get; set; }
      
        public DateTime? DateOfOffer { get; set; }
        public string Company { get; set; }
        public string SwreferenceNo { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public string UOM { get; set; }
        public bool? IsPlanned { get; set; }
        public DateTime? CommitmentDate { get; set; }
        public string OfferCurrency { get; set; }
        public decimal? OfferPrice { get; set; }
        public Guid? SessionId { get; set; }
        public string CustomerRefNo { get; set; }


    }
}
