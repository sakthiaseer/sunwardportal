﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LevelMasterModel : BaseModel
    {
        public long LevelID { get; set; }
        public long? CompanyID { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }
        public string CompanyNamee { get; set; }
    }
}
