﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterMouldInformationAttachmentModel
    {
        public List<long> BlisterMouldInformationDocumentId { get; set; }
        public List<string> FileName { get; set; }
        public List<long> DocumentIdList { get; set; }
        public List<string> ContentTypes { get; set; }
    }
}
