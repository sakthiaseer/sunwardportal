﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SunwardAssetListModel : BaseModel
    {
        public long SunwardAssetListID { get; set; }
        public int? No { get; set; }
        public long? CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public long? DeviceCatalogMasterID { get; set; }
        public long? SiteID { get; set; }
        public long? ZoneID { get; set; }
        public long? LocationID { get; set; }
        public long? AreaID { get; set; }
        public long? SpecificAreaID { get; set; }
        public string DeviceModelName { get; set; }
        public string SerialNo { get; set; }
        public string OldID { get; set; }
        public string NewID { get; set; }
        public string PhotoOfDevice { get; set; }
        public int? DeviceStatusID { get; set; }
        public string DevicePhysicalCondition { get; set; }
        public int? CalibrationStatus { get; set; }
        public PurchaseInformationModel PurchaseInformation { get; set; }
        
    }
}
