﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
     public  class PlantMaintenanceEntryMasterModel : BaseModel
    {
        public long PlantMaintenanceEntryMasterID { get; set; }
        public string Name { get; set; }
        public int? MasterType { get; set; }
        public long? PlantEntryLineID { get; set; }

    }
}
