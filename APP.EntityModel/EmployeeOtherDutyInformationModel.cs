﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeOtherDutyInformationModel : BaseModel
    {
        public long EmployeeOtherDutyInformationID { get; set; }
        public long? EmployeeID { get; set; }
        public long? DesignationTypeID { get; set; }
        public long? SubSectionTID { get; set; }

        public long? DesignationID { get; set; }
        public int? HeadCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string DesignationType { get; set; }
        public string SubSectionT { get; set; }

        public string Designation { get; set; }


    }
}
