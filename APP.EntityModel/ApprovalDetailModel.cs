﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApprovalDetailModel : BaseModel
    {
        public long ApprovalId { get; set; }
        public long? ApproverId { get; set; }
        public string Remarks { get; set; }
    }
}
