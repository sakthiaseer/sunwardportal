﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class APPDispenserDispensingLineModel
    {
        public long DispenserDispensingLineId { get; set; }
        public long? DispenserDispensingId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string SubLotNo { get; set; }
        public string JobNo { get; set; }
        public string BagNo { get; set; }
        public decimal? BeforeTareWeight { get; set; }
        public string ImageBeforeTare { get; set; }
        public byte[] BeforeTarePhoto { get; set; }
        public decimal? AfterTareWeight { get; set; }
        public string ImageAfterTare { get; set; }
        public byte[] AfterTarePhoto { get; set; }
        public string ItemNo { get; set; }
        public string LotNo { get; set; }
        public string QCRefNo { get; set; }
        public decimal? Weight { get; set; }
        public string WeighingPhoto { get; set; }
        public byte[] WeighingPhotoStream { get; set; }
        public bool? PrintLabel { get; set; }
        public bool? PostedtoNAV { get; set; }

        public List<AppDispenserDispensingDrumDetailModel> DispenserDispensingDrumDetailModels { get; set; } = new List<AppDispenserDispensingDrumDetailModel>();

    }
}
