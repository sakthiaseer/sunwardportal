﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class NotificationModel
    {
        public long NotificationId { get; set; }
        public int? ModuleId { get; set; }
        public string Notification { get; set; }
        public bool? IsRead { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? NotifiedUserId { get; set; }
        public string NotifiedUser { get; set; }
        public string NotificationType { get; set; }
        public string NotificationTime { get; set; }
    }
}
