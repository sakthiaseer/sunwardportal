﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InsertModel
    {
        public int Id { get; set; }

        public List<int> Ids { get; set; }
        public List<ApplicationPermissionModel> ApplicationPermissionModels { get; set; }
    }
}
