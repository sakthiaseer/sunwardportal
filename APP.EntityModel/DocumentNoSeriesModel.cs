﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentNoSeriesModel : BaseModel
    {
        public long NumberSeriesId { get; set; }
        public long? ProfileID { get; set; }
        public string DocumentNo { get; set; }
        public string VersionNo { get; set; }
        public string Title { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? Implementation { get; set; }
        public string ProfileName { get; set; }
        public DateTime? Date { get; set; }
        public long? RequestorId { get; set; }
        public string RequestorName { get; set; }
        public string Link { get; set; }
        public string CompanyCode { get; set; }
        public string DepartmentName { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public string SectionName { get; set; }
        public string SubSectionName { get; set; }
        public long? PlantID { get; set; }
        public string PlantCode { get; set; }
        public string ReasonToVoid { get; set;}

        public Guid? SessionID { get; set; }

        public Guid? SessionId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? UploadedByUserID { get; set; }
        public long? DocumentID { get; set; }
        public long? DivisionId { get; set; }
        //assign to file name
        public string FileName { get; set; }
        public string Description { get; set; }
        public long? ScreenAutoNumberId { get; set; }
        public List<NumberSeriesCodeModel> Abbreviation1 { get; set; }



    }
}
