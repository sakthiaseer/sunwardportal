﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionOutputModel : BaseModel
    {
        public long ProductionOutputId { get; set; }
        public string LocationName { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public long? ProductionEntryId { get; set; }
        public string ProductionOrderNo { get; set; }
        public string SubLotNo { get; set; }
        public string LotNo { get; set; }
        public string DrumNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public string QCRefNo { get; set; }
        public decimal? OutputQty { get; set; }
        public decimal? NetWeight { get; set; }
        public string Buom { get; set; }
        public bool? IsLotComplete { get; set; }
        public bool? IsPostedToNAV { get; set; }
        public bool? IsProdutionOrderComplete { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
    }

    public class ProductionOutputReportModel : BaseModel
    {
        public long ProductionId { get; set; }
        public string LocationName { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string ItemName { get; set; }
        public long? ProductionEntryId { get; set; }
        public string ProductionOrderNo { get; set; }
        public string ReplanRefNo { get; set; }
        public string SubLotNo { get; set; }
        public string DrumNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public decimal? OutputQty { get; set; }
        public decimal? NetWeight { get; set; }
        public string Buom { get; set; }
        public bool? IsLotComplete { get; set; }
        public bool? IsPostedToNAV { get; set; }
        public bool? IsProdutionOrderComplete { get; set; }
        public DateTime? PlanDate { get; set; }
        public DateTime? PlanTime { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Action { get; set; }
        public bool? IsIPIR { get; set; }
        public string IPIRRemarks { get; set; }
        public List<ProductionOutputReportModel> Children { get; set; } = new List<ProductionOutputReportModel>();
    }
}
