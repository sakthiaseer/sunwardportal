﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationPermissionModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long PermissionID { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public long? ParentID { get; set; }
        public byte? PermissionLevel { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Icon { get; set; }
        public string MenuId { get; set; }
        public string PermissionURL { get; set; }
        public string PermissionGroup { get; set; }
        public string PermissionOrder { get; set; }
        public List<string> PathNames { get; set; } = new List<string>();
        public string PathName { get; set; }
        public bool IsDisplay { get; set; }
        public long IsSelected { get; set; }
        public long? AppplicationPermissionID { get; set; }
        public List<ApplicationPermissionModel> Children { get; set; } = new List<ApplicationPermissionModel>();
        public bool? IsHeader { get; set; }

    }
    public class ApplicationPermissionTreeModel
    {
        public List<ApplicationPermissionModel> TreeItems { get; set; } = new List<ApplicationPermissionModel>();
        public List<ApplicationPermissionModel> FlatItems { get; set; } = new List<ApplicationPermissionModel>();
        public List<long?> Ids { get; set; }

    }
}
