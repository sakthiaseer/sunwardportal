﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ManPowerModel
    {
        public long? EmployeeID { get; set; }
        public string Name { get; set; }
    }
}
