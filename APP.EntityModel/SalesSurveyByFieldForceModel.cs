﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesSurveyByFieldForceModel:BaseModel
    {
        public long SalesSurveyByFieldForceId { get; set; }
        public long? SalesPersonId { get; set; }
        public long? ClinicId { get; set; }
        public string SalesPersonName { get; set; }
        public string ClinicName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string AddressName { get; set; }
        public string AddressDetails { get; set; }
    }
}
