﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace APP.EntityModel
{
    public class DocumentAccessModel : BaseModel
    {

        public long DocumentAccessID { get; set; }
        public long? UserID { get; set; }
        public long? DocumentID { get; set; }
        public List<long?> UserIDs { get; set;}
        public List<long?> assignedRights { get; set; }

    }
}
