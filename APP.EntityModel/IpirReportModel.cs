﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IpirReportModel:BaseModel
    {
        public long IpirReportId { get; set; }
        public string Location { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string ProdOrder { get; set; }
        public string IssueTitle { get; set; }
        public string Description { get; set; }
        public string ManufacturingProcess { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderDescription { get; set; }
        public string ProdOrderData { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public bool? IsLocked { get; set; }
        public long? LockedByUserId { get; set; }
        public string LockedByUser { get; set; }
        public long? NotifyCount { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? DocumentId { get; set; }
        public long? DocumentParentId { get; set; }
        public string TopicId { get; set; }
        public string Subject { get; set; }
        public bool? IsOthersOptions { get; set; }
        public string IpirReportNo { get; set; }
        public long? IpirissueId { get; set; }
        public string IpirissueDescription { get; set; }
        public string Ipirissue { get; set; }
        public string Type { get; set; }
        public string FilePath { get; set; }
        public string ProdOrderNo { get; set; }
        public long? LocationId { get; set; }
        public int? SupportDocCount { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public List<long> ResponsibilityUsers { get; set; }
    }
}
