﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationFormMasterPermissionModel
    {
        public long ApplicationFormMasterPermissionId { get; set; }
        public string ScreenId { get; set; }
        public int? ApplicationFormCodeId { get; set; }
        public long? Id { get; set; }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public string ApplicationFormCodeName { get; set; }
        public long ApplicationCodeId { get; set; }
        public List<ApplicationFormMasterPermissionModel> Children { get; set; }
    }
}
