﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
     public class AppIPIREntryModel : BaseModel
    {
        public long IPIREntryID { get; set; }
        public string IPIRNo { get; set; }
        public int? IPIRUploadType { get; set; }
        public string IPIRUploadName { get; set; }
        public long? AdditionalProcessID { get; set; }
        public string AdditionalProcessCode { get; set; }
        public int? NoOfDrums { get; set; }
        public long? LocationID { get; set; }
        public string LocationName { get; set; }
        public int? NoOfManpower { get; set; }
        public int? NoOfHours { get; set; }
        public long? DocumentID { get; set; }
        public string Document { get; set; }

    }
}
