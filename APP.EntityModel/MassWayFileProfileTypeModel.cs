﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MassWayFileProfileTypeModel : BaseModel
    {
        public long? FileProfileTypeId { get; set; }
        public string MainProfileName { get; set; }
        public string CreateProfileName { get; set; }
        public string MassWayType { get; set; }
    }
}
