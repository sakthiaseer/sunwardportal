﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public  class ApplicationRoleModel : BaseModel
    {
        public long RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
    }
}
