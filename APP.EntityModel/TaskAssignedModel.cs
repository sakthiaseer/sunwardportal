﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskAssignedModel : BaseModel
    {
       

        public long? TaskAssignedID { get; set; }      

        public long? TaskOwnerID { get; set; }
        public long? TaskID { get; set; }
        public long? UserID { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? AssignedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NewDueDate { get; set; }

        public int StatusCodeID { get; set; }










    }
}
