﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GeneralEquivalentNavisionModel : BaseModel
    {
      public long  GeneralEquivalentNavisionID { get; set; }
        public long? DatabaseID { get; set; }
        public long? NavisionID { get; set; }
        public string LinkProfileRefNo { get; set; }
        public string MasterProfileRefNo { get; set; }
        public string ProfileRefNo { get; set; }

        public string Database { get; set; }
        public string Navision { get; set; }
        public string NavisionBuom { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }


    }
}
