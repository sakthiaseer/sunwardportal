﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SellingPriceInformationModel : BaseModel
    {
        public long SellingPriceInformationID { get; set; }
        public long? SellingCatalogueID { get; set; }
        public long? ManufacturingID { get; set; }
        public long? ProductID { get; set; }
        public long? PricingMethodID { get; set; }
        public long? BonusCurrencyID { get; set; }
        public decimal? BonusSellingPrice { get; set; }
        public long? Quantity { get; set; }
        public decimal? Bonus { get; set; }
        public long? MinCurrencyID { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MinQty { get; set; }
        public List<SellingPricingTiersModel> sellingPricingTiersList { get; set; }
        public List<NavItemModel> ProductItemList { get; set; }
        public bool? isBonus { get; set; } = false;
        public bool? isMinQty { get; set; } = false;
        public long? CurrencyId { get; set; }
        public string SellingCatalogue { get; set; }
        public string Manufacturing { get; set; }
        public string ProductName { get; set; }
        public string PricingMethod { get; set; }
        public string Currency { get; set; }
        public string SellingPrice { get; set; }
        public string QuantityName { get; set; }
        public string MinCurrency { get; set; }
        public long? companyListingID { get; set; }
        public string companyListingName { get; set; }
        public decimal? MinSellingPrice { get; set; }
        public long? GenericCodeSupplyToMultipleId { get; set; }

    }
}
