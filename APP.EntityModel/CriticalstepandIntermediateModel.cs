﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CriticalstepandIntermediateModel : BaseModel
    {
        public long CriticalstepandIntermediateID { get; set; }
        public long? FinishProductiID { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsmaster { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }

        public string ProductName { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
    }
}
