﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchParam
    {
        public List<string> BatchNos { get; set; }
        public DateTime? TillDate { get; set; }
    }
}
