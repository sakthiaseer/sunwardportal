﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RefSearchModel
    {
        public string ProfileReferenceNo { get; set; }
        public bool IsHeader { get; set; }
        public int? TypeID { get; set; }
        public string NavType { get; set; }
    }
}
