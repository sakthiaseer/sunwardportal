﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class ApplicationPermissionData
    {
        public List<ApplicationPermissionModel> ApplicationPermissionModels { get; set; } = new List<ApplicationPermissionModel>();
        public List<ApplicationPermissionTree> ApplicationPermissionTreeItems { get; set; } = new List<ApplicationPermissionTree>();
        public List<long> SelectedIds { get; set; } = new List<long>();
    }

    public class ApplicationPermissionTree
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationPermissionTree> Children { get; set; } = new List<ApplicationPermissionTree>();
    }
}
