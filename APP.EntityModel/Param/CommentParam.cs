﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class CommentParam
    {
        public long TaskId { get; set; }

        public long UserId { get; set; }
        public  long TaskCommentId { get; set; }

        public List<long> TaskCommentIds { get; set; }

        public List<long> AssignedUserIds { get; set; }
        public List<long> OwnerUserIds { get; set; }

        public List<long> AssignedCCUserIds { get; set; }

        public List<RepliedUserModel> Assigned { get; set; }

        public List<RepliedUserModel> AssignedCC { get; set; }

        public List<RepliedUserModel> OwnerUsers { get; set; }
    }
}
