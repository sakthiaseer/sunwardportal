﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class StockBalanceModel :BaseModel
    {
        public long ACId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PoQty { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
    }
}
