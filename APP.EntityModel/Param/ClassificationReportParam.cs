﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class ClassificationReportParam
    {
        public long ItemClassificationMasterID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? ItemId { get; set; }
        public long? ItemClassificationTypeID { get; set; }
        public long? ItemClassificationID { get; set; }
        public long? UserId { get; set; }
    }
}
