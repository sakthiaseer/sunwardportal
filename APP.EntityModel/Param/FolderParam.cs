﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class FolderParam
    {
        public long UserId { get; set; }
        public bool ShowOnly { get; set; }
    }
}
