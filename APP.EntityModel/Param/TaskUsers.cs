﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class TaskUsers 
    {
        public List<long?> AssignedToUserIDs { get; set; }
        public long? Onbehalf { get; set; }

        public List<long?> AssignedCCUserIDs { get; set; }
    }
}
