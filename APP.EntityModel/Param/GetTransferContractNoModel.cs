﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GetTransferContractNoModel
    {
        public long? CustomerId { get; set; }
        public long? SalesOrderEntryId { get; set; }
        public long? ProductId { get; set; }
    }
}
