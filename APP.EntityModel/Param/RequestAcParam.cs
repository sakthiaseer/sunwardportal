﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RequestAcParam
    {
        public long? CustomerId { get; set; }
        public long? CompanyId { get; set; }
    }
}
