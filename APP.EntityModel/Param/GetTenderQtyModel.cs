﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GetTenderQtyModel
    {
        public long? ContractId { get; set; }
        public long? CustomerId { get; set; }
        public long? ProductId { get; set; }
    }
}
