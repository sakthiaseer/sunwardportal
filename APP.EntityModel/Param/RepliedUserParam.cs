﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Param
{
    public class RepliedUserParam
    {
        public List<RepliedUserModel> Assigned { get; set; }

        public List<RepliedUserModel> AssignedCC { get; set; }
        public List<RepliedUserModel> Owners { get; set; }
    }
}
