﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ClassificationParam
    {
        public long? ClassificationId { get; set; }
        public int ClassificationTypeId { get; set; }
    }
}
