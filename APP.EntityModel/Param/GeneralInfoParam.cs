﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GeneralInfoParam
    {
        public int? StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public long? FinishProductGeneralInfoID { get; set; }
        public string Remarks { get; set; }
    }
}
