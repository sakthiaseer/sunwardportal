﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PharmacologicalReportParam
    {
        public List<long?> ManufacturingSiteIds { get; set; }
    }
}
