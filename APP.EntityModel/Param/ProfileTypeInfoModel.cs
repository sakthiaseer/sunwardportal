﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class ProfileTypeInfoModel
    {
        public long FileProfileTypeId { get; set; }
        public string ProfileTypeInfo { get; set; }
        public long? ModifiedByUserID { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
