﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace APP.EntityModel
{
    public class TaskMembersModel :BaseModel
    {
      public long  TaskMemberID { get; set; }
        public long? TaskID { get; set; }
        public long? OnBehalfID { get; set; }
        public long? AssignedTo { get; set; }
        public long AssignedFrom { get; set; }
        public long? AssignedCC { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NewDueDate { get; set; }
        public string Title { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }




    }
}
