﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyHolidayDetailModel :BaseModel
    {
        public long CompanyHolidayDetailID { get; set; }
        public long? CompanyHolidayID { get; set; }
        public long? HolidayID { get; set; }
        public int? HolidayTypeID { get; set; }
        public int? AllowEarlyReleaseID { get; set; }
        public DateTime? EarlyRelease { get; set; }
        public DateTime? HolidayDate { get; set; }
        public bool? IsCompulsory { get; set; }
        public bool? IsCompanyChoose { get; set; }
        public string HolidayName { get; set; }
        public string HolidayType { get; set; }
        public string AllowEarlyRelease { get; set; }
         

    }
}
