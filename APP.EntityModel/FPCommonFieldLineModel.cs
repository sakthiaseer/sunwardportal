﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPCommonFieldLineModel : BaseModel
    {
      public long  FPCommonFieldLineID { get; set; }
        public long? FPCommonFiledID { get; set; }
        public long? MaterialNoID { get; set; }
        public string  MaterialName { get; set; }
        public decimal? DosageNo { get; set; }
        public long? DosageUnitID { get; set; }
        public long? PerDosageID { get; set; }
        public string Overage { get; set; }
        public long? OverageUnitsID { get; set; }
        public long? OverageDosageUnitsID { get; set; }

        public string DosageUnit { get; set; }
        public string PerDosage { get; set; }       
        public string OverageUnits { get; set; }
        public string OverageDosageUnits { get; set; }
        


    }
}
