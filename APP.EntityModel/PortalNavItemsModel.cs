﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PortalNavItemsModel : BaseModel
    {
        public long? ItemId { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? ProfileID { get; set;}
        public string No { get; set; }
        public string BatchNo { get; set; }
        public string UOM { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? QuantityOnHand { get; set; }

        public string ItemCategoryCode { get; set; }
        public long? CompanyId { get; set; }
    }
}
