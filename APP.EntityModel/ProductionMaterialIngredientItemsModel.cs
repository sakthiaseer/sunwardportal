﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMaterialIngredientItemsModel:BaseModel
    {
        public long ProductMaterialIngredientId { get; set; }
        public long? IngredientId { get; set; }
        public long? ProductionMaterialId { get; set; }
    }
}
