﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HolidayMasterModel : BaseModel
    {

        public long? HolidayID { get; set; }
        public long? StateID { get; set; }
        public string Name { get; set; }
        public string StateName { get; set; }
        public string Description { get; set; }
    }
}
