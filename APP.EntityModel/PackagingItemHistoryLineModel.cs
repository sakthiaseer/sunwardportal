using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingItemHistoryLineModel : BaseModel
    {
        public long PackagingItemHistoryLineId { get; set; }
        public long? PackagingItemHistoryId { get; set; }
        public long? ItemId { get; set; }
        public Guid? ImageSessionId { get; set; }
        public Guid? FileSessionId { get; set; }

        public string Description1 { get; set; }

        public string Description2 { get; set; }

        public string InternalReferenceNo { get; set; }
        public string PkgNo { get; set; }


    }
}
