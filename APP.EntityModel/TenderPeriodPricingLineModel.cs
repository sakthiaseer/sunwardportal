﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TenderPeriodPricingLineModel:BaseModel
    {
        public long TenderPeriodPricingLineId { get; set; }
        public long? TenderPeriodPricingId { get; set; }
        public long? ProductId { get; set; }
        public long? Currency { get; set; }
        public decimal? Price { get; set; }
        public long? Uomid { get; set; }
        public string ProductName { get; set; }
        public decimal? SellingPrice { get; set; }
        public string Uom { get; set; }
        public string CurrencyName { get; set; }
        public Guid? SessionId { get; set; }
    }
}
