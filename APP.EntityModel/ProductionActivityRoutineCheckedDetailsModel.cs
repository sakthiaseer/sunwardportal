﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityRoutineCheckedDetailsModel : BaseModel
    {
        public long ProductionActivityRoutineCheckedDetailsId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public long? ProductionActivityRoutineAppId { get; set; }
        public int? ActivityInfoId { get; set; }
        public bool? IsCheckNoIssue { get; set; }
        public long? CheckedById { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedComment { get; set; }
        public bool? IsCheckReferSupportDocument { get; set; }
        public byte[] CommentImage { get; set; }
        public string CommentImageType { get; set; }
       
        public Guid? SessionId { get; set; }

        public string ActivityInfoName { get; set; }
        public string CheckedByUserName { get; set; }

        public long? RoutineStatusId { get; set; }
        public long? RoutineResultId { get; set; }

        public string RoutineStatusName { get; set; }
        public string RoutineResultName { get; set; }
    }
}
