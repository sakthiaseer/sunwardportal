﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NotifyLongNotesModel : BaseModel
    {
        public long NotifyLongNotesId { get; set; }
        public long? NotifyDocumentId { get; set; }
        public string DetailNotes { get; set; }
        public string DocumentName { get; set; }
        public string Remarks { get; set; }
        public long? ParentId { get; set; }
        public string Baseurl { get; set; }
    }
}
