﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BmrmovementLineModel:BaseModel
    {
        public long BmrmovementLineId { get; set; }
        public long? BmrmovementId { get; set; }
        public string Bmrname { get; set; }
        public string ItemNo { get; set; }
        public string BatchNo { get; set; }
        public decimal? Qty { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string FPDescription { get; set; }
        public string FPItemNo { get; set; }
        public string UOM { get; set; }
        public string LocationCode { get; set; }
        public string ProductionOrderNo { get; set; }
        public int? LineNo { get; set; }
    }
}
