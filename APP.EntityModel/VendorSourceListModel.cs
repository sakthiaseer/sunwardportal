﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class VendorSourceListModel
    {
        public long VendorSourceListID { get; set; }
        public long? SourceListID { get; set; }
        public long? VendorsListID { get; set; }      

        public List<long?> SourceListIds { get; set; }
    }
}
