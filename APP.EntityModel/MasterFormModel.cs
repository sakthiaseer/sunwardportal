﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterFormModel : BaseModel
    {
        public long MasterFormId { get; set; }
        public string FormName { get; set; }
        public string FormSchema { get; set; }
        public string FormModel { get; set; }

        public string FormItems { get; set; }
    }

    public class MasterFormLineModel : BaseModel
    {
        public long MasterFormDetailId { get; set; }
        public long? MasterFormId { get; set; }
        public string MasterFormData { get; set; }
        public long? UserId { get; set; }
    }
}
