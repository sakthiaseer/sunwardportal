﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackingItemListLineModel
    {
        public long CommonPackingItemListLineId { get; set; }
        public long? PackingInformationLineId { get; set; }
        public long? PackagingItemNameId { get; set; }
        public long? UomId { get; set; }
        public decimal? NoOfUnits { get; set; }
    }
}
