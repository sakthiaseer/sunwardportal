﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class CustomerAcceptanceQtyOrderModel
    {
        public long CustomerAcceptanceQtyOrderID { get; set; }
        public decimal? Qty { get; set; }
        public long? CustomerAcceptanceConfirmationID { get; set; }
    }
}
