﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateLineProcessModel : BaseModel
    {
        public long JobProgressTemplateLineProcessId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public long? JobProgressTemplateLineId { get; set; }
        public string TemplateName { get; set; }
        public string ProfileNo { get; set; }
        public string ActionNo { get; set; }
        public string SectionNo { get; set; }
        public string SectionDescription { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public long? Pia { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Message { get; set; }
        public long? Pic { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public int? SpecificDateOfEachMonth { get; set; }
        public string PersonInAction { get; set; }
        public string PersonInCharge { get; set; }
        public string JobDescription { get; set; }
        public long? AssignedTo { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualCompletionDate { get; set; }
        public string CurrentAction { get; set; }
    }
}
