﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppWikiTaskLinkModel
    {
        public long AppWikiTaskLinkId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public string TaskLink { get; set; }
        public string Subject { get; set; }
    }
}
