﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DeviceGroupListModel : BaseModel
    {
        public long DeviceGroupListID { get; set; }
        public string DeviceGroupName { get; set; }
        public string DeviceGroupNo { get; set; }
        public string Appreviation { get; set; }
        public string UOM { get; set; }
        public bool IsRequire { get; set; }
        public long? CalibrationTypeID { get; set; }
        public string CalibrationName { get; set; }

        public virtual ICollection<DeviceGroupCatalogModel> DeviceGroupCatalog { get; set; }

    }
}
