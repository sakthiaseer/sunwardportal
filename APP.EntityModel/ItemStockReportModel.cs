﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemStockReportModel
    {
        public long ItemStockId { get; set; }
        public long ItemId { get; set; }
        public string ItemNo { get; set; }
        public string VariationCode { get; set; }
        public string Description { get; set; }
        public string ItemCategoryCode { get; set; }
        public string Uom { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public decimal? IssueQuantity { get; set; }
        public decimal? BalanceQuantity { get; set; }
        public List<ItemBatchInfoModel> ItemBatchInfo { get; set; }
    }
    public class ItemStockReportAllModel
    {
        public List<ItemStockReportModel> ItemStockReportModel { get; set; }
        public List<ItemExportStockReportModel> ItemExportStockReportModel { get; set; }
    }
    public class ItemExportStockReportModel
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string ItemCategoryCode { get; set; }
        public string QuantityOnHand { get; set; }
        public string IssueQuantity { get; set; }
        public string BalanceQuantity { get; set; }
        public string Uom { get; set; }
    }
}
