﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackagingCartonModel:BaseModel
    {
        public long CartonSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public decimal? OuterMeasurementLength { get; set; }
        public decimal? OuterMeasurementWidth { get; set; }
        public decimal? OuterMeasurementHeight { get; set; }
        public decimal? InnerMeasurementLength { get; set; }
        public decimal? InnerMeasurementWidth { get; set; }
        public decimal? InnerMeasurementHeight { get; set; }
        public long? TypeWallId { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PackagingItemSetInfoName { get; set; }
        public string TypeWallName { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public bool? FormTab { get; set; }
        public bool? VersionControl { get; set; }
        public List<long?> UseforItemIds { get; set; }

    }
}
