﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class SalesTargetByStaffLineModel:BaseModel
    {
        public long SalesTargetByStaffLineId { get; set; }
        public long? SalesTargetByStaffId { get; set; }
        public long? SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }
        public decimal? Target { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public List<long?> SalesCoverCodeIds { get; set; }
    }
}
