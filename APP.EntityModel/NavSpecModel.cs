﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavSpecModel
    {
        public long NavisionSpecificationId { get; set; }
        public string MaterialSpecificationNo { get; set; }
        public string MaterialSpecificationName { get; set; }
    }
}
