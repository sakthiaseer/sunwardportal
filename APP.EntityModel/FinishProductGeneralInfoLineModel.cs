﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductGeneralInfoLineModel
    {
        public long FinishProductGeneralInfoLineID { get; set; }
        public long? FinishProductGeneralInfoID { get; set; }
        public long? PackTypeID { get; set; }
        public long? PackagingTypeID { get; set; }
        public long? PackSize { get; set; }
        public decimal? PerUnitQTY { get; set; }
        public decimal? ShelfLife { get; set; }
        public byte?[] Picture { get; set; }
        public string StorageCondition { get; set; }
        public decimal? PackQty { get; set; }
        public string PackType { get; set; }
        public long? PackQtyunitId { get; set; }
        public long? PerPackId { get; set; }
        public decimal? EquvalentSmallestQty { get; set; }
        public long? EquvalentSmallestUnitId { get; set; }
        public decimal? FactorOfSmallestProductionPack { get; set; }
        public long? SalesPerPackId { get; set; }
        public long? TemparatureConditionId { get; set; }
        public decimal? Temparature { get; set; }
        public bool? IsProtectFromLight { get; set; }
        public string PackagingType { get; set; }

        public string Size { get; set; }
        public string PackQtyunitName { get; set; }
        public string PerPackName { get; set; }
        public string EquvalentSmallestUnitName { get; set; }
        public string TemparatureConditionName { get; set; }
        public string SalesPerPackName { get; set; }
        public string IsProtectFromLightStatus { get; set; }
        public long? RegistrationSalesId { get; set; }
        public List<long> StorageConditionId { get; set; }
        public int? RegistrationPackingStatusId { get; set; }
        public string RegistrationSalesName { get; set; }
        public string StorageConditionName { get; set; }
        public string RegistrationPackingStatusName { get; set; }
        public string FPProductInfoLines { get; set; }

        public long? CapacityId { get; set; }
        public long? ClosureId { get; set; }
        public long? LinerId { get; set; }
        public string CapacityName { get; set; }
        public string ClosureName { get; set; }
        public string LinerName { get; set; }
    }
}
