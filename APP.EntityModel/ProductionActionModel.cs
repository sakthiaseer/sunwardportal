﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActionModel : BaseModel
    {
        public long ProductionActionID { get; set; }
        public string Name { get; set; }
    }
}
