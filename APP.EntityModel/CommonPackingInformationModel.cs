﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonPackingInformationModel:BaseModel
    {
        public long PackingInformationId { get; set; }
        public long? SetInformationId { get; set; }
        public long? PackagingMethodId { get; set; }
        public string PackagingMethodName { get; set; }
        public List<long> PackagingItemSetInfoId { get; set; }
        public bool set { get; set; }
    }
}
