﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DateRangeModel
    {
        public int intmonth { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ItemNo { get; set; }
        public long? MethodCodeId { get; set; }
        public long? SalesCategoryId { get; set; }
        public string Replenishment { get; set; }
        public bool? IsSteroid { get; set; }
        public DateTime StockMonth { get; set; }
        public long? ChangeMethodCodeId { get; set; }
        public string Receipe { get; set; }
        public string Remarks { get; set; }
        public decimal Roundup2 { get; set; }
        public bool IsUpdate { get; set; }
        public int? Ticketformula { get; set; }
        public int? Ticketvalue { get; set; }

        public long? CompanyId { get; set; }
        public string Ref { get; set; }
        public long? VersionId { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? SelectedMonth { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<long?> NavItemIds { get; set; }

        public bool ShowSmallestUnit { get; set; }

        public bool ShowACOnly { get; set; }

        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }

        public bool isAc { get; set; }

        public string VersionNo { get; set; }
    }
}
