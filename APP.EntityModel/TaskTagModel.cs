﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskTagModel : BaseModel
    {
       public long TaskTagID { get; set; }
       public long TaskMasterID { get; set; }
       public long? TagID { get; set; }
       public string TagName { get; set; }

        public bool IsSelected { get; set; } = false;
        public List<TaskTagModel> TaskTagList { get; set; }
        public string TaskName { get; set; }

    }
}
