﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MasterInterCompanyPricingLineModel : BaseModel
    {
        public long? MasterInterCompanyPricingLineId { get; set; }
        public long? MasterInterCompanyPricingId { get; set; }
        public long? SellingToId { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? BillingPercentage { get; set; }
        public string SellingCompany { get; set; }
        public string Currency { get; set; }
        public string ReferenceInfo { get; set; }
    }
}
