﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AssetMasterModel : BaseModel
    {
        public long AssetMasterId { get; set; }
        public long? InventoryTypeId { get; set; }
        public string InventoryType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AssetNo { get; set; }
        public long? LocationId { get; set; }
        public long? CategoryId { get; set; }

        public string Location { get; set; }
        public string Category { get; set; }
        public long? SubCategoryId { get; set; }
        public long? VendorId { get; set; }
        public long? MakeId { get; set; }
        public string Vendor { get; set; }
        public string Make { get; set; }
        public string ModelNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime? AcquiredDate { get; set; }
        public DateTime? DisposedDate { get; set; }
        public decimal? PurchasePrice { get; set; }
        public DateTime? DepriciatedToDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? DepriciationBasePrice { get; set; }
        public int? DepriciableLife { get; set; }
        public int? DepricationMethod { get; set; }
        public string Notes { get; set; }

    }
}
