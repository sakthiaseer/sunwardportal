﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeInformationModel : BaseModel
    {
      public long  EmployeeInformationID { get; set; }
        public long? EmployeeID { get; set; }
        public long? CompanyID { get; set; }
        public long? DivisionID { get; set; }
        public long? DepartmentID { get; set; }
        public long? SectionID { get; set; }
        public long? SubSectionID { get; set; }
        public long? SubSectionTID { get; set; }
        public long? DesignationID { get; set; }
        public long? DesignationHeadCountID { get; set; }
        public int? DesignationHeadCount { get; set; }

        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public string DivisionName { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTwoName { get; set; }
        public string Designation { get; set; }
       

        public bool? IsOversees { get; set; }

    }
}
