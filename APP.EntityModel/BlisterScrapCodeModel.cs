﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterScrapCodeModel : BaseModel
    {
        public long ScrapCodeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
