﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RecordVariationDocumentLinkModel
    {
        public long RecordVariationDocumentLinkID { get; set; }
        public long? RecordVariationID { get; set; }
        public long? RecordVariationLineID { get; set; }
        public bool? IsHeader { get; set; }
        public string DocumentLink { get; set; }
    }
}
