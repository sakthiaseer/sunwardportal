﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeSignModel
    {
        public long EmployeeSignId { get; set; }
        public long? EmployeeId { get; set; }
        public string EmployeeSignature { get; set; }
        public byte[] EmployeeProfile { get; set; }
        public string Status { get; set; }
    }
}
