﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppmaterialReturnModel : BaseModel
    {
        public long MaterialReturnID { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string DrumNo { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Uom { get; set; }
        public decimal? BalanceWeight { get; set; }
        public bool? PostedtoNav { get; set; }
        public string Photo { get; set; }
        public byte[] FrontPhoto { get; set; }
    }
}
