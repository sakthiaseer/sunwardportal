﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavItemModel : BaseModel
    {
        public long ItemId { get; set; }
        public string No { get; set; }

        public string ItemNoDetail { get; set; }
        public string RelatedItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string ItemType { get; set; }
        public decimal? Inventory { get; set; }
        public decimal? SafetyStock { get; set; }
        public decimal? DemandQuantity { get; set; }
        public decimal? SupplyQuantity { get; set; }
        public string InternalRef { get; set; }
        public string ItemRegistration { get; set; }
        public string ExpirationCalculation { get; set; }
        public string BatchNos { get; set; }
        public string ProductionRecipeNo { get; set; }
        public bool? Qcenabled { get; set; }
        public string SafetyLeadTime { get; set; }
        public string ProductionBomno { get; set; }
        public string RoutingNo { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? UnitPrice { get; set; }
        public string VendorNo { get; set; }
        public string VendorItemNo { get; set; }
        public string ItemCategoryCode { get; set; }
        public string ItemTrackingCode { get; set; }
        public string Qclocation { get; set; }
        public string Company { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncBy { get; set; }
        public long? CategoryID { get; set; }
        public List<long> NavItemCustomerItemID { get; set; }
        public bool? Steroid { get; set; }
        public string SteroidText { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public int? PackSize { get; set; }
        public string CategoryName { get; set; }
        public string PackUom { get; set; }
        public long? GenericCodeId { get; set; }
        public string GenericCode { get; set; }
        public string MethodCode { get; set; }
        public long? MethodCodeId { get; set; }
        public string ItemGroup { get; set; }

        public long? CompanyId { get; set; }
        public bool IsDifferentAcuom { get; set; }
        public decimal PackQty { get; set; }
        public string Category { get; set; }

        public string PurchaseUOM { get; set; }
        public long? ReplenishmentMethodId { get; set; }
        public string ReplenishmentMethod { get; set; }
        public string ItemNoDetailList { get; set; }
        public List<long> CountryIds { get; set; }
        public bool? IsPortal { get; set; }
        public List<NavPackingMethodModel> NavPackingMethodModels { get; set; }
    }
}
