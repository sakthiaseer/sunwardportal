﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessMachineTimeModel : BaseModel
    {
        public long ProcessMachineTimeId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProductionAreaId { get; set; }
        public bool? IsRunSteroidCompaign { get; set; }
        public long? NavMethodCodeId { get; set; }
        public long? NavBatchSizeId { get; set; }
        public long? NavProductCodeId { get; set; }
        public bool? IsInterCompanyProduction { get; set; }
        public long? NavItemCategoryId { get; set; }
        public long? ItemId { get; set; }
        public bool? IsFPItemStrip { get; set; }
        public bool? IsFPItemBox { get; set; }



        public string ProductionArea { get; set; }
        public string NavMethodCode { get; set; }
        public string NavBatchSize { get; set; }
        public string NavProductCode { get; set; }
    }
}
