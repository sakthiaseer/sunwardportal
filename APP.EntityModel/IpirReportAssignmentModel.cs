﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IpirReportAssignmentModel : BaseModel
    {
        public long IpirReportAssignmentId { get; set; }
        public long? IpirReportId { get; set; }
        public string Description { get; set; }
        public bool? Urgent { get; set; }
        public int? IntendActionId { get; set; }
        public string OthersDescription { get; set; }
        public string IntendAction { get; set; }
        public string TopicId { get; set; }
        public string UserType { get; set; }
        public string CcuserType { get; set; }
        public string IsUrgentFlag { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> CcuserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public List<long?> CcuserGroupIDs { get; set; }
        public string ToUser { get; set; }
        public string CcUser { get; set; }
        public string CcUserSessions { get; set; }
        public string ToUserSessions { get; set; }
        public string SessionsItems { get; set; }
    }
}
