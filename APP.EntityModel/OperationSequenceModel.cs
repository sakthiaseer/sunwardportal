﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OperationSequenceModel : BaseModel
    {
        public long OperationSequenceId { get; set; }
        public string ProcessType { get; set; }
        public string StartRepeatProcessNo { get; set; }
        public string EndRepeatProcessNo { get; set; }
        public string TimeGapOfNextProcess { get; set; }
        public string StartAfterProcessNo { get; set; }
        public bool? Qcapproval { get; set; }
        public string TimeGapOperand { get; set; }
        public string TimeGap { get; set; }
        public long? StandardManufacturingProcessId { get; set; }

    }
}
