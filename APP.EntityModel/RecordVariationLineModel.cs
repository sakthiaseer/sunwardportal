﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RecordVariationLineModel:BaseModel
    {
        public long RecordVariationLineId { get; set; }
        public long? RecordVariationId { get; set; }
        public long? VariationCodeId { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ProjectApprovalDate { get; set; }
        public string VariationCodeName { get; set; }
        public string VariationForm { get; set; }
        public string CorrespondenceLink { get; set; }
        public long? RegistrationVariationId { get; set; }
        public string VariationCodeOldName { get; set; }
    }
}
