﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProfileDocumentModel
    {
        public long ProfileDocumentID { get; set; }
        public long? DocumentID { get; set; }
        public long? NumberSeriesId { get; set; }
        public bool? IsLatest { get; set; }
        public string VersionNo { get; set; }
        public long? UploadedByUserID { get; set; }
        public string UploadedByUser { get; set; }
        
        public DateTime? UploadedDate { get; set; }

        public string FileName { get; set; }
        public string DisplayName { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }
        public int? DocumentType { get; set; }
        public byte?[] FileData { get; set; }
        public long? FileSize { get; set; }

    }
}
