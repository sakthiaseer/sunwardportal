﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationMasterDetailModel
    {
        public long ApplicationMasterDetailId { get; set; }
        public long ApplicationMasterId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string ApplicationMaster { get; set; }
        public string NameDescription { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileTypeName { get; set; }
        public bool? IsApplyProfile { get; set; }
        public bool? IsApplyFileProfile { get; set; }
        public long? ApplicationMasterCodeId { get; set; }
        public List<NumberSeriesCodeModel> Abbreviation1 { get; set; }
    }
}
