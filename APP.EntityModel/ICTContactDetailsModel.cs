﻿namespace APP.EntityModel
{
    public class ICTContactDetailsModel : BaseModel
    {
        public long ContactDetailsID { get; set; }
        public string Department { get; set; }
        public string Salutation { get; set; }
        public string ContactName { get; set; }
        public int? Phone { get; set; }
        public long? VendorsListID { get; set; }
        public long? SourceListID { get; set; }
        public int? ContactTypeID { get; set; }
        public string Email { get; set; }
    }
}
