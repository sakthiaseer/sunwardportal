﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ExtensionInformationLineModel : BaseModel
    {
        public long ExtensionInformationLineId { get; set; }
        public long? TenderInformationId { get; set; }
        public string QuotationNo { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? ExtendedPeriodTo { get; set; }
        public long? CurrencyId { get; set; }
        public string Currency { get; set; }
        public decimal? SellingPrice { get; set; }
    }
}
