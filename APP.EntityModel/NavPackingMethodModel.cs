﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavPackingMethodModel:BaseModel
    {
        public long PackingMethodId { get; set; }
        public long? ItemId { get; set; }
        public long? NoOfUnitsPerShipperCarton { get; set; }
        public long? PalletSize { get; set; }
        public string PalletSizeName { get; set; }
        public long? NoOfShipperCartorPerPallet { get; set; }
    }
}
