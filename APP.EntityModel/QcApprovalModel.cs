﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class QcApprovalModel : BaseModel
    {
        public long QcapprovalId { get; set; }
        public string InspectionNo { get; set; }
        public bool? IsRetest { get; set; }
        public long? TestNameId { get; set; }
        public string TestName { get; set; }
        public string ProductionOrderNo { get; set; }
        public string RePlanRefNo { get; set; }
        public string IsRetestFlag { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsSkipValidation { get; set; }
        public List<QcApprovalLineModel> QcApprovalLines { get; set; } = new List<QcApprovalLineModel>();
    }
    public class QcApprovalLineModel : BaseModel
    {
        public long QcapprovalLineId { get; set; }
        public long? QcapprovalId { get; set; }
        public string ProductionOrderNo { get; set; }
        public string SubLotNo { get; set; }
        public string DrumNo { get; set; }
        public int? MoisturePercent { get; set; }
        public string MoistureStatus { get; set; }
    }
}
