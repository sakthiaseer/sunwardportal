﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GenericItemNameListingModel:BaseModel
    {
        public long GenericItemNameListingId { get; set; }
        public string GenericName { get; set; }
        public string AlsoKownAs { get; set; }
        public long? ItemClassificationId { get; set; }
        public string ItemClassificationName { get; set; }
    }
}
