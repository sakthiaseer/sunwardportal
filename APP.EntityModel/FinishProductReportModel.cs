﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductReportModel : BaseModel
    {
        public long? FinishProductID { get; set; }
        public long? FinishProductLineID { get; set; }
        public long? DosageFormID { get; set; }
        public string ManufacturingSite { get; set; }
        public string ProductName { get; set; }
        public string MaterialName { get; set; }
        public decimal? Dosage { get; set; }
        public string Units { get; set; }
        public string PerDosage { get; set; }
        public string Overage { get; set; }
        public string OverageUnits { get; set; }
        public string OverageDosageUnits { get; set; }
    }
}
