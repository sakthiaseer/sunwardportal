﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMethodTemplateModel : BaseModel
    {
        public long ProductionMethodTemplateID { get; set; }
        public long? ICMasterOperationID { get; set; }
        public long? ProfileID { get; set; }
        public string RefNo { get; set; }
        public long? LocationID { get; set; }
        public long? MachineID { get; set; }
        public int? ManPower { get; set; }
        public int? HoursMinutes { get; set; }
        public bool? IsEachNewDay { get; set; }
        public bool? IsEndOfTheDay { get; set; }
        public bool? IsNewBatch { get; set; }
        public bool? IsNewSubLot { get; set; }
        public int? IdlingForXDays { get; set; }
        public int? IdlingForXhours { get; set; }
        public int? CampaignByXBatches { get; set; }
        public bool? ChangeShift { get; set; }

        public List<long?> ManufacturingSiteIDs { get; set; }
        public string Location { get; set; }
      
        public string Machine { get; set; }
        public string OperationName { get; set; }


    }
}
