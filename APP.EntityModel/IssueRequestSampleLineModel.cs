﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IssueRequestSampleLineModel
    {
        public long IssueRequestSampleLineId { get; set; }
        public long? SampleRequestLineId { get; set; }
        public decimal? IssueQuantity { get; set; }
        public string BatchNo { get; set; }
        public string SpecialNotes { get; set; }
        public long? AddedByUserID { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedUserID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string AddedByUser { get; set; }
        public string ModifiedByUser { get; set; }
        public long? NavcompanyId {get;set;}
        public string NavLocation { get; set; }
    } 
}
