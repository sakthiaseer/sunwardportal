﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class StandardManufacturingProcessLineModel:BaseModel
    {
        public long StandardManufacturingProcessLineId { get; set; }
        public long? StandardManufacturingProcessId { get; set; }
        public long? MethodTemplateRoutineLineId { get; set; }
        public string No { get; set; }
        public long? ProcesssStepId { get; set; }
        public long? OperationId { get; set; }
        public long? DetermineFactorId { get; set; }
        public long? MachineGroupingId { get; set; }
        public long? MachineNameId { get; set; }
        public string MachineName { get; set; }
        public string Location { get; set; }
        public string TimeRequire { get; set; }
        public string TimeGapMin { get; set; }
        public string ProcesssStepName { get; set; }
        public string OperationName { get; set; }
        public string DetermineFactorName { get; set; }
        public string MachineGroupingName { get; set; }
    }
}
