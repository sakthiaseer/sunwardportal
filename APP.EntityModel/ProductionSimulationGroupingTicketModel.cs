﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionSimulationGroupingTicketModel:BaseModel
    {
        public long ProductionSimulationGroupingTicketId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? ItemId { get; set; }
        public decimal? ExistingTicket { get; set; }
        public decimal? FullTicket { get; set; }
        public decimal? SplitTicket { get; set; }
        public decimal? Qty { get; set; }
        public DateTime? WeekOfDate { get; set; }
        public int? WeekNo { get; set; }
        public string MethodCodeName { get; set; }
        public string ItemName { get; set; }
    }
}
