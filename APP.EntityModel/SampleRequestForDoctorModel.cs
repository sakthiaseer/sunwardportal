﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SampleRequestForDoctorModel : BaseModel
    {
        public long SampleRequestForDoctorID { get; set; }
        public long? SampleRequestForDoctorHeaderId { get; set; }
        public long? CompanyId { get; set; }
        public long? ItemId { get; set; }
        public string BatchNo { get; set; }
        public DateTime? MfgDate { get; set; }
        public DateTime? ExpDate { get; set; }
        public decimal? InStockQty { get; set; }
        public decimal? OutStockQty { get; set; }
        public decimal? BalanceStockQty { get; set; }
        public string ItemNo { get; set; }
        public decimal? TotalQuantity { get; set; }
        public bool? IsOutOfStock { get; set; }
        public string ConfirmStockStatus { get; set; }
    }
}
