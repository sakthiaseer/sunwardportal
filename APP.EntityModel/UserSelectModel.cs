﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class UserSelectModel : BaseModel
    {
        public List<long> AssignedTo { get; set; }
        public List<long> AssignedCc { get; set; }
        public long? OnBehalfID { get; set; }
        public long? OwnerID { get; set; }
    }
}
