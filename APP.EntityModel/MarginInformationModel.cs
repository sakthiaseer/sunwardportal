﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MarginInformationModel : BaseModel
    {
        public long MarginInformationID { get; set; }
        public long? DistirbutorID { get; set; }
        public long? CompanyID { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public decimal? MarginPercentage { get; set; }
        public bool? NeedToKeepVersionInfo { get; set; }

        public long? DistributeForId { get; set; }
        public string DistributeFor { get; set; }

        public string Distirbutor { get; set; }

        public bool? NeedToKeepTrackVersion { get; set; }
        public Guid? VersionSessionId { get; set; }
        public string ReferenceInfo { get; set; }

        public List<MarginInformationLineModel> MarginInformationLineModels { get; set; }


    }
}
