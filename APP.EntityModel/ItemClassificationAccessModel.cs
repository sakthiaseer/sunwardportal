﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemClassificationAccessModel
    {
        public long  ItemClassificationAccessID { get; set; }
        public long? ItemClassificationID { get; set; }
        public long? UserID { get; set; }
        public long? UserGroupID { get; set; }
        public long? RoleID { get; set; }
        public long? FormId { get; set; }
        public List<long?> UserIDs { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string DepartmentName { get; set; }
        public string Designation { get; set; }
        public string UserGroupName { get; set; }
        public List<long?> ItemClassificationAccessIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public string FormName { get; set; }
        public string ClassificationTypeName { get; set; }
        public string ProfileName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
