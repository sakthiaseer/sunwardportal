﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class StandardManufacturingProcessModel:BaseModel
    {
        public long StandardManufacturingProcessId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? ProfileId { get; set; }
        public long? MethodTempalteId { get; set; }
        public string MethodTemplateName { get; set; }
        public string MachineGroupName { get; set; }
        public string TemplateName { get; set; }
        public string ManufacturingSiteName { get; set; }
        public string ProfileName { get; set; }
        public string MethodTempalteName { get; set; }
    }
}
