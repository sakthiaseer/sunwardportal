﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TemplateCaseFormNotesModel:BaseModel
    {
        public long TemplateCaseFormNotesId { get; set; }
        public long? TemplateTestCaseFormId { get; set; }
        public string Notes { get; set; }
        public string Link { get; set; }
    }
}
