﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class WikiResponsibleModel:BaseModel
    {
        public long WikiResponsibilityId { get; set; }
        public long? EmployeeId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? ApplicationWikiLineDutyId { get; set; }
        public string Title { get; set; }
        public string DutyNoName { get; set; }
        public string DutyName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }

    }
}
