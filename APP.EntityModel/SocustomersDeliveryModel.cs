﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SocustomersDeliveryModel:BaseModel
    {
        public long SocustomersDeliveryId { get; set; }
        public long? SocustomersId { get; set; }
        public bool? RequireSpecialPermit { get; set; }
        public string RequireSpecialPermitFlag { get; set; }
        public List<long?> ModeofDeliveryIds { get; set; }
        public List<long?> DeliveryTyperOfPermitIds { get; set; }
        public string ModeofDeliveryItems { get; set; }
        public string DeliveryTyperOfPermitItems { get; set; }
    }
}
