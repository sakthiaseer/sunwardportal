using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class UnitConversionModel : BaseModel
    {
        public long UnitConversionID { get; set; }
        public decimal? UnitNumber { get; set; }
        public int? Units { get; set; }
        public decimal? IsEqualValue { get; set; }
        public int? Units1 { get; set; }

        public string UnitsName1{get;set;}
        public string UnitsName2{get;set;}
    }
}
