﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SobyCustomerSunwardEquivalentModel:BaseModel
    {
        public long SobyCustomerSunwardEquivalentId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? OrderPlacetoCompanyId { get; set; }
        public long? NavItemId { get; set; }
        public string FactorVsCustomerPacking { get; set; }
        public string FactorVsPurchasePacking { get; set; }
        public string OrderPlacetoCompany { get; set; }
        public string NavItem { get; set; }
        public string ItemDescription { get; set; }
        public string ManufacturingSite { get; set; }
        public string BUOM { get; set; }
        public string InterCompanyNo { get; set; }
        public long? SocustomersItemCrossReferenceId { get; set; }
        public bool? IsThisDefault { get; set; }
        public int? StatusCodeId { get; set; }
        public string IsThisDefaultFlag { get; set; }
        public string DefaultSupply { get; set; }
        public long? SupplyToId { get; set; }
        public string InternalRef { get; set; }
        public string ItemCategory { get; set; }
        public string SupplyToName { get; set; }

    }
}
