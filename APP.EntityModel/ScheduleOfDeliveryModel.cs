using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ScheduleOfDeliveryModel : BaseModel
    {
          
        public long ScheduleOfDeliveryId { get; set; }
        public long? DeliveryCompanyId { get; set; }
        public long? ShipmentNo { get; set; }
        public long? PlanweekId { get; set; }
        public int? DayOfTheWeekId { get; set; }
      
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileId { get; set; }
        public List<int> DayOfTheWeekIds { get; set; }
        public string Planweek { get; set; }
        public string DeliveryCompany { get; set; }
        public string ShipmentDetails { get; set; }
        



    }
}
