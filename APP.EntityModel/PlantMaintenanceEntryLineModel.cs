﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlantMaintenanceEntryLineModel
    {
        public long PlantEntryLineID { get; set; }
        public long? PlantEntryId { get; set; }
        public long? ItemGroupId { get; set; }
        public string EntryNumber { get; set; }
        public string Manufacture { get; set; }
        public string Brand { get; set; }
        public string ModelNumber { get; set; }
        public string ManufactureIdNo { get; set; }
        public string ManufactureSerialNo { get; set; }
        public string SWExistingId { get; set; }
        public byte[] FrontPhoto { get; set; }
        public byte[] BackPhoto { get; set; }
        public string FrontPhotoName { get; set; }
        public string BackPhotoName { get; set; }
        public byte[] LocationVideo { get; set; }       
        public string Condition { get; set; }
        public string Recommendation { get; set; }
        public string VideoFileName { get; set; }
        public long? CategoryID { get; set; }
        public long? PlantItemGroupID { get; set; }
        public long? NameGroupID { get; set; }
        public long? PartGroupID { get; set; }
        public long? SubPartGroupID { get; set; }
        public string SpecificItemName { get; set; }

        public string Category { get; set; }
        public string PlantItemGroup { get; set; }
        public string NameGroup { get; set; }
        public string PartGroup { get; set; }
        public string SubPartGroup { get; set; }
        public PlantMaintenanceEntryMasterModel PlantMaintenanceMaster { get; set; }
        public List<string> CategoryNames { get; set; }
        public List<string> PlantItemGroupNames { get; set; }
        public List<string> NameGroupNames { get; set; }
        public List<string> PartGroupNames { get; set; }
        public List<string> SubPartGroupNames { get; set; }
        public List<PlantMaintenanceEntryModel> plantMaintenanceEntryList { get; set; }
    }
}
