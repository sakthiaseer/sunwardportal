﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationGlossaryModel:BaseModel
    {
        public long ApplicationGlossaryID { get; set; }
        public string Name { get; set; }
        public string Definition { get; set; }
    }
}
