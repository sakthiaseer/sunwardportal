﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemInformationModel : BaseModel
    {
        public long ItemInformationId { get; set; }
        public long? ItemId { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Uom { get; set; }
        public string ShelfLife { get; set; }
        public decimal? Total { get; set; }
        public string ConvertToUom { get; set; }
        public DateTime? CompletionDate { get; set; }
    }
}
