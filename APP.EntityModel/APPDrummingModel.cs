﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class APPDrummingModel : BaseModel
    {
       public long DrummingID { get; set; }
       public string WrokOrderNo { get; set; }
       public string ProdOrderNo { get; set; }
       public string ItemNo { get; set; }
       public string Description { get; set; }
       public string SublotNo { get; set; }
       public string DrumNo { get; set; }
       public decimal? DrumWeight { get; set; }
       public string BagNo { get; set; }
       public decimal? BagWeight { get; set; }
       public decimal? TotalWeight { get; set; }


    }
}
