﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesSurveyByFieldForceLineModel:BaseModel
    {
        public long SalesSurveyByFieldForceLineId { get; set; }
        public long? SalesSurveyByFieldForceId { get; set; }
        public long? ProductGroupSurveyId { get; set; }
        public bool? SpecificCombinationDrug { get; set; }
        public string Symptoms { get; set; }
        public string IsUseSwproduct { get; set; }
        public string DoctorBuyFrom { get; set; }
        public string PossibleIndicatePrice { get; set; }
        public string ProductGroupSurveyName { get; set; }
        public string SpecificCombinationDrugFlag { get; set; }
    }
}
