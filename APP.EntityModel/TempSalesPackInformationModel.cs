﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TempSalesPackInformationModel : BaseModel
    {
        public long TempSalesPackInformationID { get; set; }

        public long? FinishproductGeneralInfoID { get; set; }

        public long? FinishProductGeneralInfoLineID { get; set; }
    }
}
