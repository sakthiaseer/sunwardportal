﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductGeneralInfoSpecialInsModel:BaseModel
    {
        public long FinishProductSpecialInsId { get; set; }
        public long? FinishProductGeneralInfoLineId { get; set; }
        public long? SpecialInstructionId { get; set; }
    }
}
