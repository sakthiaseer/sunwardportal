﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NAVCompanyModel : BaseModel
    {
        public long CompanyID { get; set; }
        public string Company { get; set; }
        public string SoapURL { get; set; }
        public string ODataURL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DomainName { get; set; }
    }
}
