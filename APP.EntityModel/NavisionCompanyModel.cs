﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavisionCompanyModel:BaseModel
    {
        public long NavisionCompanyId { get; set; }
        public long? DatabaseId { get; set; }
        public long? CompanyTypeId { get; set; }
        public long? NavCustomerId { get; set; }
        public long? NavVendorId { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string DatabaseName { get; set; }
        public string CompanyTypeName { get; set; }
        public string NavCustomerName { get; set; }
        public string NavVendorName { get; set; }
        public long? ProfileID { get; set; }
        public string NavCustomerCode { get; set; }
        public string NavVendorCode { get; set; }
        public long? CompanyId { get; set; }
    }
}
