﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionCapsuleLineModel : BaseModel
    {
        public long ProductionCapsuleLineID { get; set; }
        public long? ProductionCapsuleID { get; set; }
        public long? DatabseRequireID { get; set; }
        public long? NavisionID { get; set; }
        public string DatabseRequire { get; set; }
        public string Navision { get; set; }
        public string BUOM { get; set; }


    }
}
