﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RegistrationFormatMultipleModel:BaseModel
    {
        public long RegistrationFormatId { get; set; }
        public long? RegistrationFormatInformationId { get; set; }
        public long? FormatId { get; set; }
    }
}
