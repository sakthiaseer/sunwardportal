﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeICTRoleModel
    {
        public long EmployeeRoleID { get; set; }
        public long? EmployeeICTInformationID { get; set; }
        public long? RoleID { get; set; }

    }
}
