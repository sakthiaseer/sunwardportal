﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class InventoryTypeOpeningStockBalanceModel : BaseModel
    {
        public long OpeningBalanceId { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public string ItemCategory { get; set; }
        public string InventoryType { get; set; }
        public string Location { get; set; }
        public string Area { get; set; }
        public string SpecificArea { get; set; }
    }
}
