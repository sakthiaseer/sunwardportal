using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BompackingLanguageMultipleModel : BaseModel
    {
        public long BompackingLanguageId { get; set; }
        public long? BompackingId { get; set; }
        public long? LanguageId { get; set; }
    }
}