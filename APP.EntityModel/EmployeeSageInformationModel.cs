﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class EmployeeSageInformationModel:BaseModel
    {
        public long SageInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public string SageId { get; set; }
        public DateTime? AttendenceDate { get; set; }
        public DateTime? CheckinDateTime { get; set; }
        public string Remarks { get; set; }
    }
}
