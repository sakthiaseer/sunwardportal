﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PSBCommentDetailModel : BaseModel
    {
        public long PsbreportCommentDetailId { get; set; }
        public long? DistAcId { get; set; }
        public long? ItemId { get; set; }
        public decimal? Qty { get; set; }
        public string Comment { get; set; }
        public DateTime? Date { get; set; }
        public int? Week { get; set; }
        public bool? IsApproval { get; set; }
        public decimal? IntendOrder { get; set; }
        public decimal? ProjectHoldingAmount { get; set; }
        public int? NewAcMonth { get; set; }
    }
}
