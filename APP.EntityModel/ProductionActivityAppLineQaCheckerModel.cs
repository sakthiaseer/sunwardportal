﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityAppLineQaCheckerModel
    {
        public long ProductionActivityAppLineQaCheckerId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public string QaCheckFlag { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public string QaCheckUser { get; set; }
    }
}
