﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PSBReportVersion2Model
    {
        public long ItemId { get; set; }
        public bool IsSteroid { get; set; }
        public string ItemNo { get; set; }
        public string DistNo { get; set; }
        public long DistAcId { get; set; }
       
        public long PackSize { get; set; }
       
        public decimal? Mss { get; set; }
        public decimal? AntahQty { get; set; }
        public decimal? Px { get; set; }
        public decimal? AcMonths { get; set; }
        public decimal? SwjbStockBalance { get; set; }
        public decimal? SwsinStockBalance { get; set; }
        public decimal? WipQty { get; set; }
        public decimal? AvailableStock { get; set; }
        public decimal? NewDelivery { get; set; }
        public decimal? Amount { get; set; }
        public decimal? IntendOrder { get; set; }
        public decimal? NewAcMonth { get; set; }
        public decimal? ProjectHoldingAmount { get; set; }
    }
}
