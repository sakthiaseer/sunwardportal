﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionTicketModel : BaseModel
    {
        public long ProductionTicketId { get; set; }
        public long? BaseUnitId { get; set; }
        public long? COAId { get; set; }
        public long? CompanyId { get; set; }
        public Guid? SessionID { get; set; }
        public string BatchNo { get; set; }
        public string FileName { get; set; }
    }
}
