﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonMasterToolingClassificationModel:BaseModel
    {
        public long CommonMasterToolingClassificationId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public int? ToolInfoId { get; set; }
        public string ToolName { get; set; }
        public long? LocationId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProfileName { get; set; }
        public string ToolInfoName { get; set; }
        public string LocationName { get; set; }
    }
}
