﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProdOrderModel:BaseModel
    {
        public long ProdOrderId { get; set; }
        public string ProdOrderNo { get; set; }
        public string BatchNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string InternalRef { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime CompletionDate { get; set; }
    }
}
