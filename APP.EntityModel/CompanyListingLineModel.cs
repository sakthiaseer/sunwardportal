﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyListingLineModel : BaseModel
    {
        public long CompanyListingLineID { get; set; }
        public long? CompanyListingID { get; set; }
        public long? BusinessActivityID { get; set; }
        public long? BusinessCategoryID { get; set; }
        public string BusinessActivity { get; set; }
        public string BusinessCategory { get; set; }
        public string CompanyListingName { get; set; }
    }
}
