﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyEventsModel:BaseModel
    {
        public long CompanyEventID { get; set; }
        public int? EventTypeID { get; set; }
        public DateTime? StartDate { get; set; }
        public long? CompanyID { get; set; }
        public long? DeptOwner { get; set; }
        public string Name { get; set; }
        public int? RepeatInfoID { get; set; }
        public int? StandardFrequencyID { get; set; }
        public int? FullfilmentStatus { get; set; }
        public int? NotLaterDays { get; set; }
        public DateTime? TriggerDate { get; set; }
        public string TaskContent { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public bool? AuthorityOrCompanyRequirement { get; set; }
        public bool? IsEffectProdSchdule { get; set; }
        public bool? RequiredPreparation { get; set; }
        public int? PreparationTypeID { get; set; }
        public int? PreparationDays { get; set; }
        public DateTime? Reminderby { get; set; }
        
        public string EventType { get; set; }
    }
}
