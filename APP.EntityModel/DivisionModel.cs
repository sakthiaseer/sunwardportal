﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DivisionModel : BaseModel
    {
        public long DivisionID { get; set; }
        public long? CompanyID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public string DivisionCompanyName { get; set; }

        

    }
}
