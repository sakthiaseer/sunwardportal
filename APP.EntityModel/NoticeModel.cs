﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NoticeModel : BaseModel
    {
        public long NoticeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string Wilink { get; set; }
        public bool? IsLogin { get; set; }
        public int? ModuleId { get; set; }
        public string ModuleName { get; set; }
        public long? TypeId { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public string TypeName { get; set; }
        public string UserTypes { get; set; }
    }
}
