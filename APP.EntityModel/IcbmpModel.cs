﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IcbmpModel:BaseModel
    {
        public long IcBmpId { get; set; }
        public string BmpNo { get; set; }
        public string WiLink { get; set; }
        public List<long> ManufacturingSiteIds { get; set; }
    }
}
