﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PKGApprovalInformationModel : BaseModel
    {
        public long PkgapprovalInformationId { get; set; }
        public long? PlantId { get; set; }
        public long? ProductGroupCodeId { get; set; }
        public long? ItemId { get; set; }
        public string ProductGroupCode { get; set; }
        public string NavisionItemCode { get; set; }
       
    }
}
