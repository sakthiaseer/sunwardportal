﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TemplateTestCaseCheckListModel:BaseModel
    {
        public long TemplateTestCaseCheckListId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public string Description { get; set; }
        public string Instruction { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public string No { get; set; }
        public string SeqNo { get; set; }
        public string CaseNo { get; set; }
        public bool? IsResponsibility { get; set; }
        public long? TemplateTestCaseFormId { get; set; }
        public string IsResponsibilityFlag { get; set; }
        public string TopicId { get; set; }
        public long? TemplateTestCaseCheckListFormId { get; set; }
        public bool? IsSkipTest { get; set; }
        public string IsSkipTestFlag { get; set; }
        public string ParticipantUsers { get; set; }
        public List<TemplateTestCaseCheckListResponseModel> TemplateTestCaseCheckListResponseModels { get; set; }
        public List<TemplateTestCaseCheckListResponseDutyModel> TemplateTestCaseCheckListResponseDutyModels { get; set; }
        public bool? IsMasterTemplate { get; set; }
        public List<long> ResponsibilityUsers { get; set; }
        public string Nos { get; set; }
        public long? TemplateTestCaseLinkId { get; set; }
        public long? LocationToSaveId { get; set; }
        public string NameOfTemplate { get; set; }
    }
}
