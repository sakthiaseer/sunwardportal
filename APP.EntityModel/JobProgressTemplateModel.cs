﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateModel : BaseModel
    {
        public long JobProgressTemplateId { get; set; }
        public long? TemplateId { get; set; }
        public string TemplateName { get; set; }
        public long? JobCategoryId { get; set; }
        public string JobCategory { get; set; }
        public long? JobTypeId { get; set; }
        public string JobType { get; set; }
        public string Objective { get; set; }
        public long? Pic { get; set; }
        public string PersonIncharge { get; set; }
        public long? NotificationId { get; set; }
        public int? SoftwareId { get; set; }
        public string Software { get; set; }
        public long? ModuleId { get; set; }
        public string Module { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public string ProfileName { get; set; }
        public List<JobProgressTemplateLineModel> JobProgressTemplateLineModels { get; set; }
        public List<JobProgressTemplateNotifyModel> JobProgressTemplateNotifyModels { get; set; }
        public List<long?> EmployeeIDs { get; set; }
        public List<long?> UserGroupIDs { get; set; }
        public List<long?> ItemIds { get; set; }
        public string UserType { get; set; }
        public string CurrentStatus { get; set; }
        public long? CurrentAction { get; set; }
        public string ProfileCode { get; set; }
        public string IsUpdatePersonalCalandarFlag { get; set; }
        public bool? IsUpdatePersonalCalandar { get; set; }
        public long? CountryId { get; set; }
        public string JobOrderNo { get; set; }
        public string CountryName { get; set; }
    }
}
