﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionSearchModel
    {
        public string ProductionOrderNo { get; set; }
        public string ItemName { get; set; }
        public string BatchNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public List<long?> LocationIds { get; set; }
        public List<long?> ProductionActionIds { get; set; }
        public long? PlantId { get; set; }
    }
}
