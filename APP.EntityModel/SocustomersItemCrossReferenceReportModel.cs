﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SocustomersItemCrossReferenceReportModel
    {
        public long CompanyListingID { get; set; }
        public Guid? SessionId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyReferenceNo { get; set; }
        public string Description { get; set; }
        public string CustomerPackingUomName { get; set; }
        public string PerUomName { get; set; }
        public string PurchasePerUomName { get; set; }
        public string OrderPlacetoCompany { get; set; }
        public string NavItem { get; set; }
        public string ItemDescription { get; set; }
        public string ManufacturingSite { get; set; }
        public string BUOM { get; set; }
        public string InterCompanyNo { get; set; }
        public string FactorVsCustomerPacking { get; set; }
        public string FactorVsPurchasePacking { get; set; }
        public string IsThisDefaultFlag { get; set; }
        public long? CrossReferenceID { get; set; }
        public long? SobyCustomerSunwardEquivalentId { get; set; }
        public string DefaultSupply { get; set; }
        public string SupplyToDescription { get; set; }
        public string CustomerReferenceNo2 { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long UserId { get; set; }
    }
}
