﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PKGRegisteredPackingInformationModel : BaseModel
    {
        public long PkgregisteredPackingInformationId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PkgapprovalInformationId { get; set; }
        public string PackagingItem { get; set; }
    }
}
