﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TenderInformationModel:BaseModel
    {
        public long TenderInformationId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public string ContractNo { get; set; }
        public string CustomerPonumber { get; set; }
        
        public long? ProductId { get; set; }
        public decimal? TenderQty { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public string Uom1 { get; set; }
    }
}
