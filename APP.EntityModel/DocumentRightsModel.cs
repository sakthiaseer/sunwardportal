﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{


    public class DocumentRightsModel : BaseModel
    {

        public long DocumentAccessID { get; set; }
        public long? UserID { get; set; }
        public long? DocumentID { get; set; }
        public bool? IsReadWrite { get; set; }
        public bool? IsRead { get; set; }
        public List<long?> UserIDs { get; set; }
        public List<long?> ReadWriteUserID { get; set; }
        public List<long?> ReadOnlyUserID { get; set; }
        public List<long?> assignedRights { get; set; }
        public string DocumentName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }

}
