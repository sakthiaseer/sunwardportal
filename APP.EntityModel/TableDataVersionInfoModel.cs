﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class TableDataVersionInfoModel<TEntity> : BaseModel
    {
        public long VersionTableId { get; set; }
        public int VersionNo { get; set; }
        public string TableName { get; set; }
        public string ScreenId { get; set; }
        public string ReferenceInfo { get; set; }
        public Guid SessionId { get; set; }
        public long PrimaryKey { get; set; }
        public string JsonData { get; set; }
        public long AddedByUserId { get; set; }    

        public TEntity VersionData { get; set; }
    }
}
