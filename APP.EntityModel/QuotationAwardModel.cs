﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class QuotationAwardModel : BaseModel
    {
        public long QuotationAwardId { get; set; }
        public long? QuotationHistoryId { get; set; }
        public long? TypeOfOrderId { get; set; }
        public long? ItemId { get; set; }
        public decimal? TotalQty { get; set; }
        public bool? IsCommitment { get; set; }
        public decimal? CommittedQty { get; set; }
        public DateTime? CommittedOn { get; set; }
        public bool? IsFollowDate { get; set; }
        public string NavisionNo { get; set; }
        public string TypeOfOrder { get; set; }
        public Guid? VersionSessionId { get; set; }

        public List<QuotationAwardLineModel> QuotationAwardLineModels { get; set; }
        public decimal? Price { get; set; }
        public string ReasonForDifferentQty { get; set; }
        public string ReasonForDifferentPrice { get; set; }
        public bool? IsDifferentQty { get; set; }
        public bool? IsDifferentPrice { get; set; }


    }
    public class QuotationAwardLineModel : BaseModel
    {
        public long QuotationAwardLineId { get; set; }
        public long? QuotationAwardId { get; set; }       
        public decimal? QtyPerShipment { get; set; }       
        public DateTime? ShipmentDate { get; set; }

        public decimal? HeaderTotalQty { get; set; }
        public decimal? ExistingQty { get; set; }
       


    }
}
