﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NoticeUserModel:BaseModel
    {
        public long NoticeUserId { get; set; }
        public long? NoticeId { get; set; }
        public long? UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
