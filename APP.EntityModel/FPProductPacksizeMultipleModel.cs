﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPProductPacksizeMultipleModel
    {
        public long FPProductPackSizeMultipleID { get; set; }
        public long? FPProductID { get; set; }
        public long? FinishProductGeneralInforLineID { get; set; }
    }
}
