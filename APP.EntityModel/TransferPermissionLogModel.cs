﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TransferPermissionLogModel:BaseModel
    {
        public long TransferPermissionLogId { get; set; }
        public string TableName { get; set; }
        public long? PrimaryTableId { get; set; }
        public long? PreviousUserId { get; set; }
        public long? CurrentUserId { get; set; }
        public long? AddedByUserId { get; set; }
        public string Type { get; set; }
        public string PreviousUser { get; set; }
        public string CurrentUser { get; set; }
    }
}
