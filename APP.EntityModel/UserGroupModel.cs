﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace APP.EntityModel
{
    public class UserGroupModel : BaseModel

    {
        public long UserGroupID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsTMS { get; set; }

        public List<long?> UserIDs { get; set; }
        public long? UserGroupUserId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public string UserGroupName { get; set; }
        public string UserName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
    public class UserGroupNameModel 

    {
        public long UserGroupID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

       
    }
}
