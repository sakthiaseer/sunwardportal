﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TagAlongCustomerModel
    {
        public long? CustomerID { get; set; }
        public long? TenderAgencyID { get; set; }
        public string CustomerName { get; set; }
        public bool? IsCustomer { get; set; } = false;
        public bool? IsTenderAgency { get; set; } = false;

        public string TenderAgencyName { get; set; }
        public string TagAlongCustomerName { get; set; }
    }
}
