﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RecordVariationDetailModel:BaseModel
    {
        public long RecordVariationId { get; set; }
        public string RelatedChangeControlNo { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public long? RegistrationHolderId { get; set; }
        public long? RegisterCountryId { get; set; }
        public long? ProductId { get; set; }
        public string VariationNo { get; set; }
        public DateTime? SubmittedPaidDate { get; set; }
        public DateTime? EstimateApprovalDate { get; set; }
        public string RegistrationHolderName { get; set; }
        public string RegisterCountryName { get; set; }
        public string DocumentLink { get; set; }
        public string ProductName { get; set; }
        public string ManufacturingSite { get; set; }
        public string PRHSpecificName { get; set; }
        public string RegisterProductOwner { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RegisterationCodeName { get; set; }
        public int? SubmissionStatusId { get; set; }
        public DateTime? EstimateSubmissionDate { get; set; }
       
        public string SubmissionStatus { get; set; }
        public List<RecordVariationLineModel> RecordVariationLineItems { get; set; }
        public string RegisterCountry{ get; set; }
        public string ProductionRegistrationHolder { get; set; }
        public string ChangeControlNo { get; set; }
        public DateTime? VariationSubmissionDate { get; set; }
        public string VariationApplicationNo { get; set; }
        public string EntryStatus{ get;set; }
        public string VariationCode{ get;set; }
        public DateTime? EvaluationApprovedDate { get; set; }
        public string VariationForm{ get;set; }
        public string CorrespondenceLink{ get;set; }
        public string VariationStatus{ get;set; }
        public long? VariationCodeId { get; set; }
        public long? VariationStatusId { get; set; }
        public long? ProductOwnerId { get; set; }
        public long? RegistrationVariationId { get; set; }
        public string VariationCodeOld { get; set; }
    }
}
