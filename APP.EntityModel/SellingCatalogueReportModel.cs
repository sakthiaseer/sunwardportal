﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SellingCatalogueReportModel
    {
        public long SellingPricingTiersId { get; set; }
        public string ProductName { get; set; }
        public string UomName { get; set; }
        public string NavisionList { get; set; }
        public string PricingMethodName { get; set; }
        public string CurrencyName { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Bonus { get; set; }
        public string CustomerName { get; set; }
        public string CountryName { get; set; }
    }
}
