﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateLineLineProcessModel:BaseModel
    {
        public long JobProgressTemplateLineLineProcessId { get; set; }
        public long? JobProgressTemplateLineProcessId { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public string Message { get; set; }
        public long? MonitorTypeOfJobActionId { get; set; }
        public string MonitorTypeOfJobAction { get; set; }
        public string UserType { get; set; }
        public long? UserGroupId { get; set; }
        public long? PlantId { get; set; }
        public List<JobProgressTemplateLineLineTaskLinkModel> AppWikiTaskLinkModel { get; set; }
        public List<long?> PiaIds { get; set; }
        public string SectionNo { get; set; }
        public string SectionDescription { get; set; }
        public bool? ApprovalFlow { get; set; }
        public string ParallelAction { get; set; }
        public string PossibleParallelAction { get; set; }
        public string PossibleToEnd { get; set; }
    }
    public class JobProgressTemplateLineLineTaskLinkModel
    {
        public long JobProgressTemplateLineLineTaskLinkId { get; set; }
        public long? JobProgressTemplateLineLineId { get; set; }
        public string Subject { get; set; }
        public string TaskLink { get; set; }
        public bool? IsWiki { get; set; }
        public bool? IsLatest { get; set; }
        public string IsRequireUploadFolder { get; set; }
        public string IsWithCompleteCheck { get; set; }
        public string IsEachSubjectHeadingUpload { get; set; }
    }
}
