﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemBatchInfoModel : BaseModel
    {
        public long ItemBatchId { get; set; }
        public long? ItemId { get; set; }
        public long? CompanyId { get; set; }
        public string BatchNo { get; set; }
        public string BatchDetail { get; set; }
        public string LocationCode { get; set; }
        public string LotNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public decimal? NavQuantity { get; set; }
        public decimal? IssueQuantity { get; set; }

        public string ItemNo { get; set; }
        public string Uom { get; set; }
        public string CompanyName { get; set; }
        public decimal? BalanceQuantity { get; set; }
        public decimal? StockOutConfirm { get; set; }
        public decimal? StockOutBatchConfirm { get; set; }
    }

    public class ItemStockInfoModel:BaseModel
    {
        public long ItemStockId { get; set; }
        public long ItemId { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public decimal? IssueQuantity { get; set; }
        public decimal? BalanceQuantity { get; set; }
        public decimal? StockOutConfirm { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public string BUOM { get; set; }
    }

    
}
