﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessMachineTimeProductionLineModel:BaseModel
    {
        public long ProcessMachineTimeLineId { get; set; }
        public long? ProcessMachineTimeId { get; set; }
        public long? ProductionCentreId { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string ProductionCentre { get; set; }
        public bool? IsFPItemStrip { get; set; }
        public bool? IsFPItemBox { get; set; }
    }
}
