﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterMouldInformationModel:BaseModel
    {
        public long BlisterMouldInformationId { get; set; }
        public long? BlistedMouldPartsId { get; set; }
        public long? ProfileId { get; set; }
        public string EngraveId { get; set; }
        public string NameOfMould { get; set; }
        public long? VendorId { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public decimal? FmdiameterOfMould { get; set; }
        public decimal? FmdepthOfMould { get; set; }
        public decimal? SmdiameterOfMould { get; set; }
        public decimal? SmdepthOfMould { get; set; }
        public int? NoOfTrack { get; set; }
        public decimal? CblisterLength { get; set; }
        public decimal? CblisterWidth { get; set; }
        public int? CcuNo { get; set; }
        public string BlistedMouldPartsName { get; set; }
        public string ProfileName { get; set; }
        public string VendorName { get; set; }
        public List<long> CommonFiledsProductionMachineId { get; set; }
        public Guid? SessionID { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public BlisterMouldInformationAttachmentModel DocumentAttachmentModel { get; set; }
        public decimal? FmUpperCupDiameter { get; set; }
        public decimal? FmUpperCupDepth { get; set; }
        public decimal? FmLowerCupShape { get; set; }
        public long? FeederId { get; set; }
        public long? BlisterKnurlingPatternId { get; set; }
        public int? UpperKnurlingCoverageId { get; set; }
        public decimal? LowerCupDiameter { get; set; }
        public decimal? LowerCupDepth { get; set; }
        public decimal? Widthtrack { get; set; }
        public long? FormatId { get; set; }
        public long? CuNoFormatId { get; set; }
        public bool? Perforation { get; set; }
        public string FeederName { get; set; }
        public string BlisterKnurlingPatternName { get; set; }
        public string UpperKnurlingCoverageName { get; set; }
        public string FormatName { get; set; }
        public string CuNoFormatName { get; set; }
        public string PerforationFlag { get; set; }

        public Guid? SessionId { get; set; }

    }
}
