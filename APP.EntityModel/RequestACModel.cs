﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RequestACModel:BaseModel
    {
        public long RequestAcid { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public string ProfileName { get; set; }
        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public long? PicId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DateChange { get; set; }
        public DateTime? ReferenceMonth { get; set; }
    }
}
