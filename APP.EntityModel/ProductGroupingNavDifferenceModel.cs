﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductGroupingNavDifferenceModel:BaseModel
    {
        public long ProductGroupingNavDifferenceId { get; set; }
        public long? ProductGroupingNavId { get; set; }
        public long? TypeOfDifferenceId { get; set; }
        public string TypeOfDifference { get; set; }
        public string DifferenceInfo { get; set; }
    }
}
