﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACEntryLinesModel :BaseModel
    {
        public long ACEntryLineId { get; set;}
        public long? ACEntryId { get; set; }
        public long? ItemId { get; set; }
        public string ItemName { get; set; }
        public decimal? Quantity { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string BUOM { get; set; }
        public int? PackSize { get; set; }
        public string Packuom { get; set; }
        public string ItemCategory { get; set; }
        public string AcItemNo { get; set; }
        public string VendorNo { get; set; }
        public string DistName { get; set; }
        public string GenericCode { get; set; }

         //public List<NavItemCitemList> NavItemCitemList { get; set; }
    }
}
