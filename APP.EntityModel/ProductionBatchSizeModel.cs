﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionBatchSizeModel
    {
        public long ProductionBatchSizeId { get; set; }
        public string ProductionBatchSizeName { get; set; }
        public string ProductionBatchSizeDescription { get; set; }
    }
}
