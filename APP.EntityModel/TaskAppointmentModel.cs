﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskAppointmentModel
    {
        public long TaskAppointmentID { get; set; }
        public long TaskMasterID { get; set; }
        public long AppointmentBy { get; set; }
        public string DiscussionNotes { get; set; }
        public long? SubTaskID { get; set; }

        public DateTime DiscussionDate { get; set; }
        public DateTime ClosedDate { get; set; }

        public int StatusCodeID { get; set; }

        public string Status { get; set; }
        public long? ParentTaskID { get; set; }
        public long? MainTaskId { get; set; }
        public string Title { get; set; }
        public List<long?> AssignedTo { get; set; }
        public string AssignedToNames { get; set; }
        public DateTime? DueDate { get; set; }
        public string Description { get; set; }

        public string DiscussionBy { get; set; }
        public string TaskTitle { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }









    }
}
