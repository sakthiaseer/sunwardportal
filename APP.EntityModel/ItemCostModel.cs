﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemCostModel:BaseModel
    {
        public long ItemCostId { get; set; }
        public long? PurposeOfCostingId { get; set; }
        public string PurposeOfCosting { get; set; }
        public DateTime? ValidityFrom { get; set; }
        public DateTime? ValidityTo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public bool? IsSyncWithNavision { get; set; }
        public string CostMethod { get; set; }
        public string CostCurrency { get; set; }
        public decimal? Cost { get; set; }
        public string Uom { get; set; }
        public long? CompanyId { get; set; }
        public string ItemCategoryCode { get; set; }
        public string Description { get; set; }
        public string InternalRef { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public Guid? VersionSessionId { get; set; }
        public string ReferenceInfo { get; set; }
        public List<ItemCostLineModel> ItemCostLineModels { get; set; }
    }
}
