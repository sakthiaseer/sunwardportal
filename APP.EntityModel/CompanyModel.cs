﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CompanyModel
    {
        public long CompanyId { get; set; }
        public string Code { get; set; }
    }
}
