﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SellingCatalogueModel : BaseModel
    {
        public long SellingCatalogueID { get; set; }
        public long? CountryID { get; set; }
        public long? CompanyID { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? FromValidity { get; set; }
        public DateTime? ToValidity { get; set; }
        public bool? NeedToKeepVersionInfo { get; set; }

        public string Country { get; set; }
        public string Customer { get; set; }
        public Guid? VersionSessionId { get; set; }

        public List<SellingPriceInformationModel> sellingPriceInformationModels { get; set; }


    }
}
