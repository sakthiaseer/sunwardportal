﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionFuntionOfMaterialItemsModel:BaseModel
    {
        public long ProductionFunctionOfMaterialId { get; set; }
        public long? FunctionOfMaterialId { get; set; }
        public long? ProdutionMaterialId { get; set; }
    }
}
