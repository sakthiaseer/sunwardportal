﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LinkFileProfileTypeDocumentModel:BaseModel
    {
        public long LinkFileProfileTypeDocumentId { get; set; }
        public Guid? TransactionSessionId { get; set; }
        public long? DocumentId { get; set; }
        public bool? IsSelected { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string Description { get; set; }
        public long? FolderId { get; set; }
        public bool? IsPortfolioAttachment { get; set; }
        public long? PortfolioLineId { get; set; }
        public long? TaskID { get; set; }
        public bool? IsLatestCommentAttachment { get; set; }
        public string Link { get; set; }
        public List<long?> ReadWriteUserID { get; set; }
        public string WikiStatusType { get; set; }
        public List<long?> ReadOnlyUserID { get; set; }

    }
}
