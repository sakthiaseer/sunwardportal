﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingAluminiumModel : BaseModel
    {
        public long AluminiumFoilId { get; set; }
        public bool? IsPrinted { get; set; }
        public string Printed { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? LetteringId { get; set; }
        public long? BgPantoneColorId { get; set; }
        public long? BgColorId { get; set; }
        public decimal? FoilThickness { get; set; }
        public decimal? FoilWidth { get; set; }

        public string PantoneColor { get; set; }
        public string Color { get; set; }
        public string Lettering { get; set; }
        public string BgPantoneColor { get; set; }
        public string BgColor { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public long? ReversePrint { get; set; }
        public decimal? WtPerRoll { get; set; }
        public decimal? LengthPerRoll { get; set; }
        public int? AluminiumFoilTypeId { get; set; }
        public string AluminiumFoilTypeName { get; set; }

           public bool? IsVersion { get; set; }
    }
}
