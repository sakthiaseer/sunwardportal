﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FPModel
    {
        public long Id { get; set; }
        public long ProfileId { get; set; }
        public string Description { get; set; }
        public string Hint { get; set; }
    }
}
