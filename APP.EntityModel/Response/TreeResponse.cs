﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel.Response
{
    public class TreeResponse
    {
        public List<FoldersModel> HierarchicalFolders { get; set; }
        public List<FoldersModel> FlatFolders { get; set; }
    }
    public class FoldersNameList
    {
        public string Name { get; set; }
        public long? OccuranceNo { get; set; }
    }
}
