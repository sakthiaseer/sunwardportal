﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class RequestACResponse
    {
        public List<RequestACLineModel> IncreaseACItems { get; set; } = new List<RequestACLineModel>();
        public List<RequestACLineModel> DecreaseACItems { get; set; } = new List<RequestACLineModel>();
    }
}
