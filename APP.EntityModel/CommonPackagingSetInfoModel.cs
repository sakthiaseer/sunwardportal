﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class CommonPackagingSetInfoModel:BaseModel
    {
        public long SetInformationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public long? PackagingMaterialId { get; set; }
        public string PackagingMaterialName { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PackagingItemSetInfoName { get; set; }
        public string PackagingMaterialNo { get; set; }
        public string UomName { get; set; }
    }
}
