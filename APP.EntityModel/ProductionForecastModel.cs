﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionForecastModel : BaseModel
    {
        public long ProductionForecastID { get; set; }
        public long? MethodCodeID { get; set; }
        public long? MethodCodeLineID { get; set; }
        public long? ItemId { get; set; }
        public DateTime? ProductionMonth { get; set; }
        public string BatchSize { get; set; }
        public decimal? PackQuantity { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string ItemNo { get; set; }
        public string MethodCodeName { get; set; }

    }
}
