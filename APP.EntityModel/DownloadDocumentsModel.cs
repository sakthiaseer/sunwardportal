﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace APP.EntityModel
{
    public class DownloadDocumentsModel
    {
        public long DocumentID { get; set; }
        public string FileName { get; set; }
        public string DisplayName { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }
        public int? DocumentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }

        public Stream stream { get; set; }

      
     
    }
}
