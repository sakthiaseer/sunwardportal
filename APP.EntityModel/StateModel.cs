﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class StateModel : BaseModel
    {
        public long StateID { get; set; }
        public long? CountryID { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
        public string Code { get; set; }
    }
}
