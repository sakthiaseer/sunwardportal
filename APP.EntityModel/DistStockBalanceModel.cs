﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DistStockBalanceModel :BaseModel
    {
        public long DistStockBalanceId { get; set; }
        public long? DistItemId { get; set; }
        public string DistName { get; set; }
        public string ItemName { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PoQty { get; set; }
    }
}
