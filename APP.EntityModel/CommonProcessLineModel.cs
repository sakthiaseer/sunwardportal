﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonProcessLineModel:BaseModel
    {
        public long CommonProcessLineId { get; set; }
        public long? CommonProcessId { get; set; }
        public long? NavisionNoSeriesId { get; set; }
        public string NavisionNoSeries { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public string ManufacturingStepsName { get; set; }
        public List<long?> MachineGroupingIDs { get; set; }
        public string MachineGroupingNames { get; set; }
    }
}
