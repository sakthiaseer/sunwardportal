﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ClassificationBmrModel : BaseModel
    {
        public long ClassificationBmrId { get; set; }
        public long? CompanyId { get; set; }
        public long? NavItemId { get; set; }
        public DateTime? ManufacturingStartDate { get; set; }
        public string BatchSize { get; set; }
        public string TicketNo { get; set; }
        public string Product { get; set; }
        public string ProductUom { get; set; }
        public string BatchNo { get; set; }
        public long? TypeOfProductionId { get; set; }
        public int? ProductionStatusId { get; set; }
        public string CompanyDatabaseName { get; set; }
        public string NavItemName { get; set; }
        public string TypeOfProductionName { get; set; }
        public string ProductionStatusName { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public string ProfileName { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ProdOrderNo { get; set; }
        public DateTime? ProductionSimulationDate { get; set; }
        public string ProductName { get; set; }
    }
}
