﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MachineTimeManHourInfoModel : BaseModel
    {
        public long MachineTimeManHourInfoId { get; set; }
        public long? ProcessMachineTimeLineId { get; set; }
        public decimal? Manpower { get; set; }
        public int? PreparationTimeId { get; set; }
        public string PreparationTimeType { get; set; }
        public string PreparationTime { get; set; }
        public int? ProductionTimeId { get; set; }
        public string ProductionTimeType { get; set; }
        public string ProductionTime { get; set; }
        public int? Level1CleaningId { get; set; }
        public string Level1CleaningType { get; set; }
        public string Level1Cleaning { get; set; }
        public int? Level2CleaningId { get; set; }
        public string Level2CleaningType { get; set; }
        public string Level2Cleaning { get; set; }
        public int? IntermittentCleaningAtId { get; set; }
        public string IntermittentCleaningAtType { get; set; }
        public string IntermittentCleaningAt { get; set; }
        public int? NextStepMinTimeGapId { get; set; }
        public string NextStepMinTimeGapType { get; set; }
        public string NextStepMinTimeGap { get; set; }
        public int? NextStepMaxTimeGapId { get; set; }
        public string NextStepMaxTimeGapType { get; set; }
        public string NextStepMaxTimeGap { get; set; }
        public bool? NextStepManpowerOverlapping { get; set; }
        public string NextStepManpowerOverlappingFlag { get; set; }
        public decimal? OverlapNumber { get; set; }
        public int? NextProdItemMinTimeId { get; set; }
        public string NextProdItemMinTimeType { get; set; }
        public string NextProdItemMinTime { get; set; }
        public int? NextProdItemMaxTimeId { get; set; }
        public string NextProdItemMaxTimeType { get; set; }
        public string NextProdItemMaxTime { get; set; }
        public string QctestingTime { get; set; }
        public string QcmaxCollection { get; set; }
        public string MaxCampaign { get; set; }
        public string RecommendedCycle { get; set; }
        public string NoOfCutUse { get; set; }
        public int? TypeOfFeedId { get; set; }
        public long? DepartmentId { get; set; }
        public string Department { get; set; }
        public string Information { get; set; }
        public string TypeOfFeed { get; set; }
    }
}
