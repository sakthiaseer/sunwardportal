﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class WorkOrderCommentModel
    {
        public long WorkOrderCommentId { get; set; }
        public long? WorkorderId { get; set; }
        public long? ParentCommentId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }
        public DateTime? CommentedDate { get; set; }
        public bool? IsRead { get; set; }
        public string AddedByUser { get; set; }
        public bool? IsEdit { get; set; }
        public string EditedByName { get; set; }
        public long? EditedBy { get; set; }
        public string Subject { get; set; }
        public List<WorkOrderCommentModel> WorkOrderCommentModels { get; set; }
        public long? AssignTo { get; set; }
        public bool? IsClosed { get; set; }
        public string AssignUser { get; set; }
      

    }
}

