﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IPIRMobileModel : BaseModel
    {
        public long IpirmobileId { get; set; }
        public long? CompanyId { get; set; }
        public int? IntendedActionId { get; set; }
        public string IntendedAction { get; set; }
        public string IpirreportNo { get; set; }
        public string Location { get; set; }
        public string ProductionOrderNo { get; set; }
        public string ProductName { get; set; }
        public string BatchNo { get; set; }
        public string IssueDescription { get; set; }
        public string Photo { get; set; }
        public byte[] PhotoSource { get; set; }
        public long? ProfileId { get; set; }
    }

    public class IPIRMobileActionModel : BaseModel
    {
        public long IpirmobileActionId { get; set; }
        public long? IpirmobileId { get; set; }
        public int? IntendedActionId { get; set; }
        public string IntendedAction { get; set; }
        public bool? IsConfirmIpir { get; set; }
        public string IssueDescription { get; set; }
        public string Photo { get; set; }
        public long? AssignmentTo { get; set; }
        public string AssignmentToName { get; set; }
        public int? AssignmentStatusId { get; set; }
        public string AssignmentStatus { get; set; }
        public List<long?> IssueRelatedIds { get; set; } = new List<long?>();
        public List<long?> IssueTitleIds { get; set; } = new List<long?>();
    }

    public class IPIRMobilePost : BaseModel
    {
        public long? IpirmobileId { get; set; }
        public int? IntendedActionId { get; set; }
        public List<IPIRMobileActionModel> MobileActionItems { get; set; }
    }
}
