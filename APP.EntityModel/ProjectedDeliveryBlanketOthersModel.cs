﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProjectedDeliveryBlanketOthersModel : BaseModel
    {
        public long ProjectedDeliverySalesOrderLineID { get; set; }
        public long? PurchaseItemSalesEntryLineID { get; set; }
        public long? FrequencyID { get; set; }
        public int? PerFrequencyQty { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? IsQtyReference { get; set; }
        public string Frequency { get; set; }
        public long? UomId { get; set; }
        public string Uom { get; set; }
    }
}
