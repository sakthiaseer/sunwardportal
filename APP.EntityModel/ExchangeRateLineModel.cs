using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ExchangeRateLineModel : BaseModel
    {
        public long ExchangeRateLineId { get; set; }
        public long? ExchangeRateId { get; set; }
        public decimal? Units { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? EquivalentUnitId { get; set; }
        public long? EquivalebtCurrencyId { get; set; }
        public string CurrencyName1{get;set;}
        public string CurrencyName2{get;set;}
        public string ReferenceInfo { get; set; }




    }
}
