﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductGroupSurveyLineModel : BaseModel
    {
        public long ProductGroupSurveyLineId { get; set; }
        public long? ProductGroupSurveyId { get; set; }
        public long? ProductGroupId { get; set; }
        public string ProductGroupName { get; set; }
    }
}
