﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavItemStockBalanceModel :BaseModel
    {
        public long NavStockBalanceId { get; set; }
        public long? ItemId { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? RejectQuantity { get; set; }
        public decimal? ReworkQty { get; set; }
        public decimal? Wipqty { get; set; }
        public decimal? GlobalQty { get; set; }
        public string DistName { get; set; }
        public string ItemName { get; set; }
    }
}
