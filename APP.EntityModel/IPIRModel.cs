﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class IPIRModel : BaseModel
    {
        public long Ipirid { get; set; }
        public long? CompanyId { get; set; }
        public string Ipirno { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }
        public string ReportBy { get; set; }
        public string DetectedBy { get; set; }
        public long? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public long? MachineId { get; set; }
        public string MachineName { get; set; }
        public long? ProductId { get; set; }
        public string ProductName { get; set; }
        public long? BatchNoId { get; set; }
        public string IssueDescription { get; set; }
        public List<long?> issueRelatedIds { get; set; }
        public List<long?> issueTitleIds { get; set; }
    }

    public class IPIRLineModel : BaseModel
    {
        public long IpirlineId { get; set; }
        public long? Ipirid { get; set; }
        public long? Picid { get; set; }
        public string PersonInchargeName { get; set; }
        public string Picadvice { get; set; }
        public long? Ipiraction { get; set; }
        public string IpiractionName { get; set; }
        public string AssignTo { get; set; }
        public Guid? DiscussionSessionId { get; set; }
        public Guid? AttachSessionId { get; set; }
    }

    public class IPIRIssueRelatedMultipleModel : BaseModel
    {
        public long IpirissueRelatedMultipleId { get; set; }
        public long? Ipirid { get; set; }
        public long? IssueRelatedId { get; set; }

    }

    public class IPIRIssueTitleMultipleModel : BaseModel
    {
        public long IpirissueTitleMultipleId { get; set; }
        public long? Ipirid { get; set; }
        public long? IssueTitleId { get; set; }

    }

    public class IPIRLineDocumentModel : BaseModel
    {
        public long IpirlineDocumentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsImage { get; set; }      

        public string DocumentType { get; set; }
        public bool Uploaded { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string FileProfileType { get; set; }
        public string ProfileNo { get; set; }

    }
}
