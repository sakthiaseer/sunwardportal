﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FileProfileTypeSetAccessModel
    {
        public long FileProfileTypeSetAccessId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public List<long?> UserIDs { get; set; }
        public string DocumentName { get; set; }
        public string FileProfileTypeName { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
