﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityCheckedDetailsModel : BaseModel
    {
        public long ProductionActivityCheckedDetailsId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? ProductionActivityAppId { get; set; }
        public int? ActivityInfoId { get; set; }
        public bool? IsCheckNoIssue { get; set; }
        public long? CheckedById { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedComment { get; set; }
        public bool? IsCheckReferSupportDocument { get; set; }
        public byte[] CommentImage { get; set; }
        public string CommentImageType { get; set; }

        public string ActivityInfoName { get; set; }    
        public string CheckedByUserName { get; set; }

        public long? ActivityStatusId { get; set; }
        public long? ActivityResultId { get; set; }

        public string ActivityStatusName { get; set; }
        public string ActivityResultName { get; set; }


    }
}
