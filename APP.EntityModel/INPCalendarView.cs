﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class INPCalendarView
    {
        public List<INPCalendarHeader> Headers { get; set; } = new List<INPCalendarHeader>();
        public List<INPCalendarHeader> ChildHeaders { get; set; } = new List<INPCalendarHeader>();
        public List<INPCalendarModel> Calendars { get; set; } = new List<INPCalendarModel>();
        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        public string Month4 { get; set; }
        public string Month5 { get; set; }
        public string Month6 { get; set; }
        public string Month7 { get; set; }
        public string Month8 { get; set; }
        public string Month9 { get; set; }
        public string Month10 { get; set; }
        public string Month11 { get; set; }
        public string Month12 { get; set; }

        public decimal? Month1Total { get; set; }
        public decimal? Month2Total { get; set; }
        public decimal? Month3Total { get; set; }
        public decimal? Month4Total { get; set; }
        public decimal? Month5Total { get; set; }
        public decimal? Month6Total { get; set; }
        public decimal? Month7Total { get; set; }
        public decimal? Month8Total { get; set; }
        public decimal? Month9Total { get; set; }
        public decimal? Month10Total { get; set; }
        public decimal? Month11Total { get; set; }
        public decimal? Month12Total { get; set; }

        public DateTime? EarlistShipment1 { get; set; }
        public DateTime? EarlistShipment2 { get; set; }
        public DateTime? EarlistShipment3 { get; set; }
        public DateTime? EarlistShipment4 { get; set; }
        public DateTime? EarlistShipment5 { get; set; }
        public DateTime? EarlistShipment6 { get; set; }

        public DateTime? CompletionBefore { get; set; }
    }
}
