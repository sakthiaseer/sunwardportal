﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProcessManPowerSpecialNotesModel : BaseModel
    {
        public long ProcessManPowerSpecialNotesId { get; set; }
        public long? ProcessMachineTimeLineId { get; set; }
        public long? DepartmentId { get; set; }
        public string Information { get; set; }       
        public string Department { get; set; }
    }
}
