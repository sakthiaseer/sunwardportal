﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TransferBalanceQtyModel:BaseModel
    {
        public long TransferBalanceQtyId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public string ContractNo { get; set; }
        public long? ProductId { get; set; }
        public long? TransferToContractId { get; set; }
        public string TransferToContractNo { get; set; }
        public int? TransferTimeId { get; set; }
        public DateTime? SpecificMonth { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public string TransferTimeName { get; set; }
        public string Uom { get; set; }
    }
}
