﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACItemsModel : BaseModel
    {
        public long DistACID { get; set; }
        public string DistName { get; set; }
        public long DistId { get; set; }
        public string ItemGroup { get; set; }
        public string ItemNo { get; set; }
        public long? SWItemId { get; set; }
        public List<long?> ItemIds { get; set; }
        public string SWItemNo { get; set; }
        public string SWItemDesc { get; set; }
        public string SWItemDesc2 { get; set; }
        public string GenericCode { get; set; }
        public string Packuom { get; set; }
        public string Steriod { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public string Status { get; set; }
        public string ItemDesc { get; set; }
        public string PackSize { get; set; }
        public decimal ACQty { get; set; }
        public DateTime ACMonth { get; set; }
        public long? CustomerId { get; set; }
        public long? CompanyId { get; set; }
        public string InternalRefNo { get; set; }

        public string CategoryCode { get; set; }
    }

    public class DistStockBalModel  
    {
        public string DistName { get; set; }
        public long CustomerId { get; set; }
        public decimal QtyOnHand { get; set; }
        public decimal POQty { get; set; }
        public decimal Avg6MontQty { get; set; }
        public string ItemDesc { get; set; }
        public string ItemNo { get; set; }
        public long DistAcid { get; set; }
        public long? CompanyId { get; set; }
        public long? DistItemId { get; set; }

        public long? NavItemId { get; set; }
        public DateTime? StockBalanceMonth { get; set; }
    }
}
