﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SocustomersIssueModel:BaseModel
    {
        public long SocustomersIssueId { get; set; }
        public long? SocustomersId { get; set; }
        public bool? MustSupplyInFull { get; set; }
        public bool? MustFollowDeliveryDate { get; set; }
        public DateTime? ShelfLifeRequirementDate { get; set; }
        public string MustSupplyInFullFlag { get; set; }
        public string MustFollowDeliveryDateFlag { get; set; }
        public List<long?> SobyCustomersMannerIds { get; set; }
        public List<long?> SobyCustomersTenderAgencyIds { get; set; }
        public string SobyCustomersMannerList { get; set; }
        public string SobyCustomersTenderAgencyList { get; set; }
        public long? BuyingThroughId { get; set; }
        public string BuyingThrough { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public long? CompanyListingID { get; set; }
        public string CompanyListingName { get; set; }
        public string CustomerCode { get; set; }
        public long? CustomerCodeId { get; set; }
        public string CompanyListingTypeList { get; set; }
        public long? TenderAgencyId { get; set; }
        public long? CentralizedWithId { get; set; }
        public string TenderAgencyName { get; set; }
        public string CentralizedWithName { get; set; }
        public long? SobyCustomersId { get; set; }
    }
}
