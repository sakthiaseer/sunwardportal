﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ItemClassificationPermissionModel
    {
        public long ItemClassificationPermissionId { get; set; }
        public long? ApplicationCodeId { get; set; }
        public long? ItemClassificationId { get; set; }
        public long? UserGroupId { get; set; }
        public long? UserId { get; set; }
        public string UserType { get; set; }
        public string UserGroupName { get; set; }
        public string UserName { get; set; }
        public List<long?> SelectedPermissions { get; set; }
        public int? ApplicationFormCodeId { get; set; }
        public string ApplicationFormCodeName { get; set; }
        public long? ParentId { get; set; }
        public string ParentName { get; set; }
        public string ScreenId { get; set; }
        public string ItemClassificationName { get; set; }
        public string Description { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
