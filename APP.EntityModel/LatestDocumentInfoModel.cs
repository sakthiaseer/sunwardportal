﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class LatestDocumentInfoModel
    {
        public long DocumentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public bool canDownload { get; set; }
        public long FolderID { get; set; }
        public long? MainFolderID { get; set; }
        public string FolderName { get; set; }
        public string FolderAddedName { get; set; }
        public string FolderAddedDate { get; set; }
        public string FolderModifiedName { get; set; }
        public string FolderModifiedDate { get; set; }
        public string FolderReleaseVersionNo { get; set; }
        public string TaskTitle { get; set; }
        public long TaskID { get; set; }
        public long? MainTaskID { get; set; }
        public string TaskAddedName { get; set; }
        public DateTime? TaskAddedDate { get; set; }
        public string TaskModifiedName { get; set; }
        public DateTime? TaskModifiedDate { get; set; }
        public string TaskReleaseVersionNo { get; set; }
        public string DocumentPath { get; set; }
    }
}
