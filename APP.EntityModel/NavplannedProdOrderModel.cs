﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavplannedProdOrderModel : BaseModel
    {
        public long PlannedProdOrderID { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Rpono { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public decimal? Quantity { get; set; }
        public long? InpreportId { get; set; }
    }
}
