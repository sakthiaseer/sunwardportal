﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityPlanningAppLineQaCheckerModel
    {
        public long ProductionActivityPlanningAppLineQaCheckerId { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public string QaCheckUser { get; set; }
        public string QaCheckFlag { get; set; }
    }
}
