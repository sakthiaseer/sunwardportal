﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MarginInformationLineModel : BaseModel
    {
        public long MarginInformationLineID { get; set; }
        public long? MarginInformationID { get; set; }
        public long? ManufacturingSiteID { get; set; }
        public long? ItemId { get; set; }
        public decimal? SpecialMarginPercentage { get; set; }

        public string ManufacturingSite { get; set; }
        public string Item { get; set; }

        public string Buom { get; set; }

    }
}
