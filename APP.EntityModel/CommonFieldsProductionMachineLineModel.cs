﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CommonFieldsProductionMachineLineModel:BaseModel
    {
        public long CommonFieldsProductionMachineLineId { get; set; }
        public long? CommonFieldsProductionMachineId { get; set; }
        public DateTime? QualifiedDate { get; set; }
        public decimal? FrequencyOfRequalificationYear { get; set; }
        public decimal? MinCapacity { get; set; }
        public decimal? MaxCapacity { get; set; }
        public decimal? PreferCapacity { get; set; }
        public long? CapacityUnitsId { get; set; }
        public string CapacityUnitsName { get; set; }
        public string QualificationNotes { get; set; }
        public decimal? RecommendedManpower { get; set; }
        public Guid? SessionId { get; set; }
    }
}
