﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FinishProductExcipientLineModel:BaseModel
    {
        public long FinishProductExcipientLineId { get; set; }
        public long? FinishProductExcipientId { get; set; }
        public long? MaterialId { get; set; }
        public long? ProcessId { get; set; }
        public decimal? Strength { get; set; }
        public long? Uomid { get; set; }
        public long? FunctionId { get; set; }
        public string Source { get; set; }
        public string ProcessName { get; set; }
        public string UomName { get; set; }
        public string FunctionName { get; set; }
        public string MaterialName { get; set; }
        public long? OperationProcessId { get; set; }
        public long? GenericItemNameId { get; set; }
        public long? GeneralRawMaterialNameId { get; set; }
        public long? NavisionDataBaseId { get; set; }
        public decimal? Dosage { get; set; }
        public long? DosageUnitsId { get; set; }
        public long? PerDosageId { get; set; }
        public decimal? Overage { get; set; }
        public long? OverageUnitsId { get; set; }
        public long? DosageUnitsId1 { get; set; }
        public string OperationProcessName { get; set; }
        public string GenericItemNameName { get; set; }
        public string GeneralRawMaterialName { get; set; }
        public string NavisionDataBaseName { get; set; }
        public string DosageUnitsName { get; set; }
        public string PerDosageName { get; set; }
        public string OverageUnitsName { get; set; }
        public string DosageUnitsId1Name { get; set; }

    }
}
