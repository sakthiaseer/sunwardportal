﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class FinishProductLineModel:BaseModel
    {
        public long FinishProductLineId { get; set; }
        public long? FinishProductId { get; set; }
        public long? MaterialId { get; set; }
        public decimal? DosageForm { get; set; }
        public long? Uom { get; set; }
        public string Overage { get; set; }
        public string MaterialName { get; set; }
        public string UnitName { get; set; }

        public long? DosageFormID { get; set; }
        public long? DosageUnits { get; set; }
        public string DosageUnitsName { get; set; }

        public long? OverageUnits { get; set; }
        public string OverageUnitsName { get; set; }

        public long? PerDosage { get; set; }
        public string PerDosageName { get; set; }
    }
}
