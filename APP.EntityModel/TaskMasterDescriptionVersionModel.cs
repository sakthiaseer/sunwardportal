﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TaskMasterDescriptionVersionModel:BaseModel
    {
        public long TaskMasterDescriptionVersionId { get; set; }
        public int? VersionNo { get; set; }
        public string Description { get; set; }
        public string VersionNos { get; set; }
    }
}
