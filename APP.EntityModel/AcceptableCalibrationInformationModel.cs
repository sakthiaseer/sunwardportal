﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AcceptableCalibrationInformationModel : BaseModel
    {
        public long AcceptableCalibrationInformationID { get; set; }
        public string Parameter { get; set; }
        public string StandardReference { get; set; }
        public string Max { get; set; }
        public string Min { get; set; }
        public string UnitOfMeasure { get; set; }
        public string Remarks { get; set; }
        public long? CalibrationServiceInformationID { get; set; }
        public long? DeviceCatalogMasterID { get; set; }

    }
}
