﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlanketOrderModel:BaseModel
    {
        public long BlanketOrderId { get; set; }
        public long? CustomerId { get; set; }
        public string ContractNo { get; set; }
        public long? ContractId { get; set; }
        public long? ProductId { get; set; }
        public int? TypeOfRequestId { get; set; }
        public decimal? QtyRequest { get; set; }
        public DateTime? DateOfOrder { get; set; }
        public int? ExtensionStatusId { get; set; }
        public int? QtyExtensionStatusId { get; set; }
        public int? ExtensionInformationId { get; set; }
        public decimal? SellingPrice { get; set; }
        public string SpecialRemarks { get; set; }
        public DateTime? FromMonth { get; set; }
        public DateTime? ToMonth { get; set; }
        public int? NoOfMonth { get; set; }
        public bool? LotInformation { get; set; }
        public string HowManyLot { get; set; }
        public long? CurrencyId { get; set; }
        public string Currency { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceToQuoteForExceedQty { get; set; }
        public string SpecialNotes { get; set; }
        public string Validity { get; set; }
        public Guid? SessionId { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public string TypeOfRequestName { get; set; }
        public string ExtensionInformationName { get; set; }
        public string LotInformationFlag { get; set; }
        public string Uom { get; set; }
        public decimal? ExceedQty { get; set; }
    }
}
