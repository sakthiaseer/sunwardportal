﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FileProfileTypeDetailModel
    {
        public long? FileProfileTypeId { get; set; }
        public long? ProfileId { get; set; }
        public bool? IsCompany { get; set; }
        public bool? IsDepartment { get; set; }
        public bool? IsDivision { get; set; }
        public bool? IsSection { get; set; }
        public bool? IsSubSection { get; set; }
        public List<CodeMasterModel> Companies { get; set; } = new List<CodeMasterModel>();
        public List<CodeMasterModel> Departments { get; set; } = new List<CodeMasterModel>();
        public List<CodeMasterModel> Divisions { get; set; } = new List<CodeMasterModel>();
        public List<CodeMasterModel> Sections { get; set; } = new List<CodeMasterModel>();
        public List<CodeMasterModel> SubSections { get; set; } = new List<CodeMasterModel>();
    }
}
