﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class ApplicationFormFieldModel :BaseModel
    {
        public long ApplicationFormFieldId { get; set; }
        public long ApplicationFormId { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsVisible { get; set; }
        public bool IsValueFilter { get; set; }
        public string FieldValue { get; set; }
        public long RoleId{get;set;}       
        public long ApplicationFormFieldPermissionId {get;set;}

    }
}
