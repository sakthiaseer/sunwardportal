﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
     public class DiscussionNotesModel : BaseModel
    {
        public long DiscussionNotesID { get; set; }
        public long? TaskID { get; set; }

        public string DiscussionNotes { get; set; }

        public DateTime? DiscussionDate { get; set; }

        public long? TaskUserID { get; set; }
    }
}
