﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BMRToCartonModel : BaseModel
    {
        public long BmrtoCartonId { get; set; }
        public string Displaybox { get; set; }
        public long? LoginUserId { get; set; }
    }
}
