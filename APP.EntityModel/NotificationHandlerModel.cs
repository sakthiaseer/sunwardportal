﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NotificationHandlerModel : BaseModel
    {
        public long NotificationHandlerId { get; set; }
        public DateTime? NextNotifyDate { get; set; }
        public int? NotifyStatusId { get; set; }
        public int? AddedDays { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }
        public string ConnectionId
        {
            get;
            set;
        }
    }
}
