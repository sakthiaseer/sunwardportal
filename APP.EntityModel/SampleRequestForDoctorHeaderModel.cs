﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SampleRequestForDoctorHeaderModel : BaseModel
    {
        public long SampleRequestForDoctorHeaderId { get; set; }
        public long? TypeOfStockId { get; set; }
        public long? ProfileId { get; set; }
        public string SampleChitNo { get; set; }
        public DateTime? DateBy { get; set; }
        public long? RequestById { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? ClassComCustomerId { get; set; }
        public string Purpose { get; set; }
        public string TypeOfStockName { get; set; }
        public string ProfileName { get; set; }
        public string RequestByName { get; set; }
        public string InventoryTypeName { get; set; }
        public string ClassComCustomerName { get; set; }
        public string ScreenId { get; set; }
        public string Signature { get; set; }
        public string Remarks { get; set; }
        public bool? IsConfirm { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string IsConfirmFlag { get; set; }
    }
}
