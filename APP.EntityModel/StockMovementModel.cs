﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class StockMovementModel
    {
        public DateTime? Date { get; set; }
        public string DocNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? Manufacture { get; set; }
        public DateTime? ExpDate { get; set; }
        public decimal? Increase { get; set; }
        public decimal? Decrease { get; set; }
        public string Uom { get; set; }
        public decimal? Balance { get; set; }
    }
}
