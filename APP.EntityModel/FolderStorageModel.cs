﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FolderStorageModel : BaseModel
    {
        public long FolderStorageID { get; set; }
        public long? FolderID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? SessionID { get; set; }
        public int? FolderTypeID { get; set; }
        public string FolderLocation { get; set; }
        public string StorageLimit { get; set; }
        public long? UserID { get; set; }
        public long? UserGroupID { get; set; }
        public long? UserGroupUserID { get; set; }
        public long? RoleID { get; set; }
        public long? UserRoleID { get; set; }
        public List<long?> UserSelection { get; set; }
        public List<string> UserNames { get; set; }
    }
}
