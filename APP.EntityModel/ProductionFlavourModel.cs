﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionFlavourModel : BaseModel
    {
        public long ProductionFlavourID { get; set; }
        public long? ItemNoID { get; set; }
        public long? FlavourID { get; set; }
        public long? SupplierID { get; set; }
        public string Supplier { get; set; }
        public string SupplierCode { get; set; }
        public string FlavourName { get; set; }
        public string Flavour { get; set; }
        public long? ProfileID { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }
        public string SpecNos { get; set; }
        public long SpecNoID { get; set; }
        public string SpecificationNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string PurposeName { get; set; }
        public string ProcessUseSpecName { get; set; }
        public string RndpurposeName { get; set; }
        public string StudyLink { get; set; }
    }
}
