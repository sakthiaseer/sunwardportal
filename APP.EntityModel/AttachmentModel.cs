﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AttachmentModel : BaseModel
    {
        public long TaskAttachmentID { get; set; }
        public long? TaskMasterID { get; set; }
        public long? DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
        public bool? IsVideoFile { get; set; }
    }
}
