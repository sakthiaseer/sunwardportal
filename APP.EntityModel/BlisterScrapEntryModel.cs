﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class BlisterScrapEntryModel : BaseModel
    {
        public long ScrapEntryId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string SourceItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string BatchNo { get; set; }
        public string BlisterBom { get; set; }
        public string PlasticBag { get; set; }
        public string BlisterScrapType { get; set; }
        public decimal? Weight { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        
        public long? ModifiedByUserId { get; set; }
     

        public long? PlasticBagID { get; set; }

        public long? BlisterScrapTypeID { get; set; }

    }
}
