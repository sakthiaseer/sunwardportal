﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PlantMaintenanceEntrySearchModel
    {
        public long PlantEntryId { get; set; }
        public string CompanyName { get; set; }
        public string SiteName { get; set; }
        public string ZoneName { get; set; }
        public string LocationName { get; set; }
        public string AreaName { get; set; }
        public string SpecificAreaName { get; set; }
    }
}
