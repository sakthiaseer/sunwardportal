﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MethodTemplateRoutineModel : BaseModel
    {
        public long MethodTemplateRoutineId { get; set; }
        public int? ManufacturingTypeId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string MethodTemplateNo { get; set; }
        public string TemplateName { get; set; }
        public string MachineGrouping { get; set; }
        public List<long?> LanguageIDs { get; set; }
        public string ManufacturingProcessName { get; set; }
        public string ManufacturingTypeName { get; set; }
    }
}
