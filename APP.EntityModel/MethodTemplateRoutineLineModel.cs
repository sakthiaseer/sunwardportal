﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MethodTemplateRoutineLineModel : BaseModel
    {
        public long MethodTemplateRoutineLineId { get; set; }
        public long? MethodTemplateRoutineId { get; set; }
        public string No { get; set; }
        public long? ProcesssStepId { get; set; }
        public long? OperationId { get; set; }
        public long? DetermineFactorId { get; set; }
        public string FixTimingorMin { get; set; }
        public string DetermineFactorName { get; set; }
        public string ProcesssStepName { get; set; }
        public string OperationName { get; set; }
    }
}
