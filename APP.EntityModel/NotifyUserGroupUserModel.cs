﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NotifyUserGroupUserModel
    {
        public long? NotifyUserGroupUserId { get; set; }
        public long? NotifyDocumentUserGroupId { get; set; }
        public long? UserGroupId { get; set; }
        public long? UserId { get; set; }
        public bool? IsClosed { get; set; }
    }
}
