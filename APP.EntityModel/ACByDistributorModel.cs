﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ACByDistributorModel
    {
        public long? MethodCodeLineID { get; set; }
        public long? MethodCodeID { get; set; }
        public long? ItemID { get; set; }
        public string ItemNo { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public long? CustomerID { get; set; }
        public string CustomerName { get; set; }
        public decimal? SellingPrice { get; set; }
        public string AcMonth { get; set; }
        public int? PackSize { get; set; }
        public decimal? AcQuantity { get; set; }

        public decimal? SalesAmount { get; set; }


    }
}
