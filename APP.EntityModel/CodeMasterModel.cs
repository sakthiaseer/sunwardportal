﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CodeMasterModel :BaseModel
    {
        public int CodeID { get; set; }
        public long Id { get; set; }
        public string CodeValue { get; set; }
        public string CodeType { get; set; }

        public string CodeDescription { get; set; }

    }
}
