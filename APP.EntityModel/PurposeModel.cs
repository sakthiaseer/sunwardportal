using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PurposeModel
    {      
       
        public long PurposeId { get; set; }        
        public string PurposeName { get; set; }
        public long ApplicationMasterID {get;set;}
        public string Description { get; set; }

    }
}
