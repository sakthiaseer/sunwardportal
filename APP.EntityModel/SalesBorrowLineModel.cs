﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SalesBorrowLineModel : BaseModel
    {
        public long SalesBorrowLineId { get; set; }
        public long? SalesBorrowId { get; set; }
        public long? ItemId { get; set; }
        public string Item { get; set; }


    }
}
