﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SobyCustomerContactInfoModel:BaseModel
    {
        public long SobyCustomerContactInfoId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public int? AddressTypeId { get; set; }
        public long? SobyCustomersMasterAddressId { get; set; }
        public long? StationLocationId { get; set; }
        public string StationLocationName { get; set; }
    }
}
