﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class OrderRequirementModel : BaseModel
    {
        public long OrderRequirementId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? NavItemId { get; set; }
        public long? GenericCodeId { get; set; }
        public long? DistributorItemId { get; set; }
        public string ManufacturingSiteName { set; get; }
        public string SalesCategory { get; set; }
        public string MethodCodeName { get; set; }
        public string NavItemName { get; set; }
        public string GenericCodeName { get; set; }
        public string DistributorItemName { get; set; }
        public bool IsNavSync { get; set; }

        public long? CompanyId { get; set; }

        public long? LocationID { get; set; }
    }
}
