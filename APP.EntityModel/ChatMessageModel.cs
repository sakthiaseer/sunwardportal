﻿using System;

namespace APP.EntityModel
{
    public class ChatMessageModel 
    {
        public long ChatMessageId { get; set; }
        public long? ChatGroupId { get; set; }
        public long? SentUserId { get; set; }
        public string SenderName { get; set; }
        public long? ReceiveUserId { get; set; }
        public string ReceiverName { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public long? ChatUserId { get; set; }
        public bool IsIncoming { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int MessageType { get; set; }
    }
}
