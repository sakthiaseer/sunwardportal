﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class GenericCodesModel : BaseModel
    {
        public long GenericCodeID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public string PackingCode { get; set; }
        public long? Uom { get; set; }
        public string GenericUom { get; set; }
        public long? ManufacringCountry { get; set; }
        public List<long?> ManufacturingIds { get; set; }
        public List<long?> SellingToIds { get; set; }
        public List<long?> SupplyToIds { get; set; }
        public List<string> SupplyToNames { get; set; }
        public long? ProductNameId { get; set; }
        public string ProductName { get; set; }
        public long? PackingUnitsId { get; set; }
        public string PackingUnits { get; set; }
        public long? SupplyToId { get; set; }
        public string SupplyTo { get; set; }
        public bool? IsSimulation { get; set; }
        public string Name { get; set; }
        public string UomName { get; set; }

    }
}
