﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class DocumentAlertModel : BaseModel
    {
        public long DocumentAlertId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }       
        public string Link { get; set; }
        public long? DocumentId { get; set; }
        
    }
}
