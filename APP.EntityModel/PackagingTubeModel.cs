﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PackagingTubeModel : BaseModel
    {
        public long TubeId { get; set; }
        public bool? IsPrinted { get; set; }
        public string Printed { get; set; }
        public decimal? Diameter { get; set; }
        public decimal? Width { get; set; }
        public decimal? Length { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? LetteringId { get; set; }
        public long? PackagingMaterialId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PackingUnitId { get; set; }

        public string PantoneColor { get; set; }
        public string Color { get; set; }
        public string Lettering { get; set; }
       

        public string PackagingMaterial { get; set; }
        public string PackagingItem { get; set; }
        public string PackagingUnit { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string PackagingItemName { get; set; }
        public long? CoatingMaterialId { get; set; }
        public string CoatingMaterialName { get; set; }
        public int? TubeTypeID { get; set; }
          public bool? IsVersion { get; set; }
    }
}
