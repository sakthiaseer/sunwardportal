﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
     public class ICTLayoutPlanTypesModel : BaseModel
    {
        public long ICTLayoutTypeID { get; set; }
        public long? ICTMasterID { get; set; }
       
        public string Name { get; set; }
        public string Description { get; set; }
        public long? LayoutTypeID { get; set; }
        public long? DocumentID { get; set; }
        public string VersionNo { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string LayoutTypeName { get; set; }
    }
}
