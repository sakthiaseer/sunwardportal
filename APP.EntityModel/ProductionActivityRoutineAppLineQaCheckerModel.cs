﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionActivityRoutineAppLineQaCheckerModel
    {
        public long ProductionActivityRoutineAppLineQaCheckerId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public string QaCheckUser { get; set; }
        public string QaCheckFlag { get; set; }
    }
}
