﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class AppSamplingLineModel : BaseModel
    {
        public int AppSamplingLineId { get; set; }
        public int? AppSamplingId { get; set; }
        public long? SamplingPurposeId { get; set; }
        public string DrumNo { get; set; }
        public string SamplingPurpose { get; set; }
        public string QcsampleNo { get; set; }
        public int? Qty { get; set; }
        public string Uom { get; set; }
    }
}
