﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class JobProgressTemplateLineModel : BaseModel
    {
        public long JobProgressTemplateLineId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string ActionNo { get; set; }
        public string SectionNo { get; set; }
        public string SectionDescription { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public long? Pia { get; set; }
        public string PersonInAction { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? From { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Message { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public int? SpecificDateOfEachMonth { get; set; }
        public string JobDescription { get; set; }
        public string ProfileNo { get; set; }
        public string TemplateName { get; set; }
        public long? AssignedTo { get; set; }
        public string AssignedToName { get; set; }
        public bool? IsCurrenProcess { get; set; }
        public int? ProcessStatusID { get; set; }
        public string ProcessStatus { get; set; }
        public long JobProgressTemplateLineProcessId { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? ActualCompletionDate { get; set; }
        public List<JobProgressTemplateLineLineProcessModel> JobProgressTemplateLineLineProcessModels  { get;set;}
    }
    public class JobProgressTemplateLineActionModel
    {
        public string ActionNo { get; set; }
        public string SectionNo { get; set; }
        public string SubSectionNo { get; set; }
    }
}
