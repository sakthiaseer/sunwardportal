﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class CalibrationTypeModel : BaseModel
    {
        public long CalibrationTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
