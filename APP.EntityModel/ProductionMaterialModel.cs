﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMaterialModel : BaseModel
    {
        public long ProductionMaterialId { get; set; }
        public string RawMaterialName { get; set; }
        public List<long> TypeofIngredientId{ get; set; }
        public List<long> FunctionOfMaterialId { get; set; }
        public long? GenericItemNameListingId { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }
        public string SpecNos { get; set; }
        public long SpecNoID { get; set; }
        public string SpecificationNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string PurposeName { get; set; }
        public string ProcessUseSpecName { get; set; }
        public string RndpurposeName { get; set; }
        public string StudyLink { get; set; }
    }
}
