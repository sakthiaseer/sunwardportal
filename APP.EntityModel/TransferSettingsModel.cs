﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TransferSettingsModel : BaseModel
    {
         public long TransferConfigId { get; set; }
        public long? FromLocationId { get; set; }
        public string FromLocation { get; set; }
        public long? ToLocationId { get; set; }
        public string ToLocation { get; set; }
        public bool? IsTransfer { get; set; }
        public bool? IsReclass { get; set; }
    }
}
