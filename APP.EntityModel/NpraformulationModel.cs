﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NpraformulationModel : BaseModel
    {
        public long NpraformulationId { get; set; }
        public int? RegistrationReferenceId { get; set; }
        public long? ProductNameId { get; set; }
        public string RegistrationReference { get; set; }
        public string ProductName { get; set; }
    }
}
