﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
   public class BulkTransferTaskModel
    {
        public long TransferFromUserId { get; set; }
        public long TransferToUserId { get; set; }
        public long TransferByUserId { get; set; }
        public DateTime TransferDate { get; set; }
        public List<long> TransferIds { get; set; }
    }
}
