﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SobyCustomersMasterAddressModel:BaseModel
    {
        public long SobyCustomersMasterAddressId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string AddressName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string ContactName { get; set; }
        public string PhoneNo { get; set; }
        public string EmailAddress { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string FaxNo { get; set; }
        public string Uenno { get; set; }
        public string CompanyRegisterationNo { get; set; }
        public string Vatgstno { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public int AddressTypeId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string DeliveryInformation { get; set; }
    }
}
