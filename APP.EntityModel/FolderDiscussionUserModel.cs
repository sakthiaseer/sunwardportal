﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class FolderDiscussionUserModel:BaseModel
    {
        public long FolderDiscussionNotesUserId { get; set; }
        public long FolderDiscussionNotesId { get; set; }
        public long FolderId { get; set; }
        public long UserId { get; set; }
        public bool IsRead { get; set; }

        public string FolderName { get; set; }
        public string Description { get; set; }
        public List<TransferPermissionLogModel> TransferPermissionLogModels { get; set; }
    }
}
