﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApplicationMasterParentModel:BaseModel
    {
        public long ApplicationMasterParentId { get; set; }
        public long ApplicationMasterParentCodeId { get; set; }
        public string ApplicationMasterName { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public bool? IsDisplay { get; set; }
        public string ParentName { get; set; }
    }
}
