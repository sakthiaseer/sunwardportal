﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HistoricalDetailsModel:BaseModel
    {
        public long HistoricalDetailsId { get; set; }
        public long? NavCustomerId { get; set; }
        public DateTime? Date { get; set; }
        public string Invoice { get; set; }
        public string CustNo { get; set; }
        public string Company { get; set; }
        public string BranchCode { get; set; }
        public string Branch { get; set; }
        public string Item { get; set; }
        public string Description { get; set; }
        public string Po { get; set; }
        public string TranType { get; set; }
        public string Batch { get; set; }
        public DateTime? Expiry { get; set; }
        public string Ref { get; set; }
        public decimal? Price { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Bouns { get; set; }
        public decimal? Nett { get; set; }
        public decimal? Gross { get; set; }
        public string NavCustomer { get; set; }
        public string Name { get; set; }
    }
}
