﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class MedMasterModel:BaseModel
    {
        public long MedMasterId { get; set; }
        public long? CompanyId { get; set; }
        public long? MetCatalogId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? UnitsId { get; set; }
        public string ModelNo { get; set; }
        public bool? IsCalibrationRequire { get; set; }
        public int? TotalNumber { get; set; }
        public string MetCatalogName { get; set; }
        public string Units { get; set; }
        public string IsCalibrationRequireFlag { get; set; }
    }
}
