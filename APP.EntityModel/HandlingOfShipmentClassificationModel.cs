﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class HandlingOfShipmentClassificationModel:BaseModel
    {
        public long HandlingOfShipmentId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ShipmentMethodId { get; set; }
        public long? SpecificTypeOfOrderId { get; set; }
        public string ProcessingTimeDay { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string ShipmentMethod { get; set; }
        public string SpecificTypeOfOrder { get; set; }
        public string ProfileName { get; set; }
        public List<long?> SpecificCustomerIds { get; set; }
    }
}
