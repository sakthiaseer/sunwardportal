﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ProductionMachineModel : BaseModel
    {
        public long ProductionMachineID { get; set; }
        public long? ItemNoID { get; set; }

        public string ItemNo { get; set; }
        public long? PurposeOfProcessID { get; set; }
        public string PurposeOfProcess { get; set; }
        public string NameOfTheMachine { get; set; }
        public long? ManufacturerID { get; set; }
        public string Manufacturer { get; set; }
        public string BasicModelNo { get; set; }
        public string SerialNo { get; set; }

        public List<long?> OperationProcedureIDs { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }

        public string MasterProfileReferenceNo { get; set; }
    }
}
