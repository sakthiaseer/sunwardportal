﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ApptransferRelcassEntrySearchModel
    {
        //Changes Done by Aravinth 18JUL2019 Start
        public long TransferReclassID { get; set; }
        public string TransferFromLocationName { get; set; }
        public string TransferFromAreaName { get; set; }
        public string TransferFromSpecificAreaName { get; set; }
        public string TransferToLocationName { get; set; }
        public string TransferToAreaName { get; set; }
        public string TransferToSpecificAreaName { get; set; }
        public string TransferTypeName { get; set; }

        //Changes Done by Aravinth 18JUL2019 End
    }
}
