﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SOWithoutBlanketOrderModel : BaseModel
    {
        public long SowithoutBlanketOrderId { get; set; }
        public long? SalesOrderId { get; set; }
        public long? ShipToCodeId { get; set; }
        public string ShipToCode { get; set; }
        public long? SoproductId { get; set; }
        public string SoProduct { get; set; }
        public bool? IsLotInformation { get; set; }
        public decimal? OrderQty { get; set; }
        public long? OrderCurrencyId { get; set; }
        public string OrderCurrency { get; set; }
        public decimal? SellingPrice { get; set; }
        public long? PriceUnitId { get; set; }
        public string PriceUnit { get; set; }
        public bool? IsFoc { get; set; }
        public string Foc { get; set; }
        public DateTime? RequestShipmentDate { get; set; }

    }
}
