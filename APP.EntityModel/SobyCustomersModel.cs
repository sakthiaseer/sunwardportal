﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class SobyCustomersModel : BaseModel
    {
        public long SobyCustomersId { get; set; }
        public long? ProfileID { get; set; }
        public bool? MustSupplyInFull { get; set; }
        public bool? MustFollowDeliveryDate { get; set; }
        public long? PaymentModeId { get; set; }
        public long? ActivationOfOrderId { get; set; }
        public long? PlaceOrderId { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string PaymentModeName { get; set; }
        public string ActivationOfOrderName { get; set; }
        public int? BillingAddressStatusId { get; set; }

        public string BillingAddressStatusName { get; set; }

        public List<SobyCustomersAddressModel> SobyCustomersAddressItems { get; set; }
        public List<SobyCustomerSunwardEquivalentModel> SobyCustomerSunwardEquivalentItems { get; set; }
        public List<SocustomersDeliveryModel> SocustomersDeliveryItems { get; set; }
        public List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceItems { get; set; }
        public List<SocustomersIssueModel> SocustomersIssueItems { get; set; }
        public List<SocustomersSalesInfomationModel> SocustomersSalesInfomationItems { get; set; }
        public SobyCustomersSalesAddressModel DefaultAddress { get; set; }
        public SobyCustomersSalesAddressModel ShippingAddress { get; set; }
        public SobyCustomersSalesAddressModel CustomerFinance { get; set; }
        public SobyCustomersSalesAddressModel FinanceDepartment { get; set; }
    }
}
