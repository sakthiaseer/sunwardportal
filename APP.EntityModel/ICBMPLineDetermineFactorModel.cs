﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class ICBMPLineDetermineFactorModel : BaseModel
    {
        public long ICBMPLineDetermineFactorID { get; set; }
        public long? ICBMPLineID { get; set; }
        public long? ICBMPDetermineFactorID { get; set; }
    }
}
