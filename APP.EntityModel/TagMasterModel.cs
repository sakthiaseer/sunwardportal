﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class TagMasterModel : BaseModel
    {
        public long TagMasterID { get; set; }
        public string Name { get; set; }        
        public Guid? SessionID { get; set; }
        
    }
}
