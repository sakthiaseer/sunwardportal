﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class PortfolioLineModel : BaseModel
    {
        public long PortfolioLineId { get; set; }
        public long? PortfolioId { get; set; }
        public long? WikiReferenceId { get; set; }
        public string VersionNo { get; set; }      
        public string Remarks { get; set; }
        public string WikiReference { get; set; }
        public string NameOfReferenceType { get; set; }
        public long? ChileMasterId { get; set; }
        public string ChileMaster { get; set; }
    }
}
