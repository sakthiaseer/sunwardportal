﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class UserSettingsModel
    {
        public long UserSettingsID { get; set; }
        public long? UserID { get; set; }
        public int? PageSize { get; set; }
        public string SearchQuery { get; set; }
        public string UserTheme { get; set; }
        public string TitleCaption { get; set; }
        public bool? IsEnableFolderPermission { get; set; }
    }
}
