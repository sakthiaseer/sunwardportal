﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.EntityModel
{
    public class NavProductMasterModel: BaseModel
    {
        public long ProductMasterId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? ColorId { get; set; }
        public long? ShapeId { get; set; }
        public long? MarkingId { get; set; }
        public decimal? LengthMax { get; set; }
        public decimal? LengthMin { get; set; }
        public decimal? WidthMax { get; set; }
        public decimal? WidthMin { get; set; }
        public decimal? ThicknessMax { get; set; }
        public decimal? ThicknessMin { get; set; }
        public decimal? WeightMax { get; set; }
        public decimal? WeightMin { get; set; }
        public int? TypeOfCoating { get; set; }
        public decimal? CoreSizeLengthMax { get; set; }
        public decimal? CoreSizeLengthMin { get; set; }
        public decimal? CoreSizeWidthMax { get; set; }
        public decimal? CoreSizeWidthMin { get; set; }
        public decimal? CoreThicknessMin { get; set; }
        public decimal? CoreThicknessMax { get; set; }
        public decimal? CoreWeightMax { get; set; }
        public decimal? CoreWeightMin { get; set; }
        public decimal? CoatedSizeLengthMax { get; set; }
        public decimal? CoatedSizeLengthMin { get; set; }
        public decimal? CoatedSizeWidthMax { get; set; }
        public decimal? CoatedSizeWidthMin { get; set; }
        public decimal? CoatedThicknessMin { get; set; }
        public decimal? CoatedThicknessMax { get; set; }
        public decimal? CoatedWeightMax { get; set; }
        public decimal? CoatedWeightMin { get; set; }
        public int? ProductCreationType { get; set; }

        public string MethodCodeName { get; set; }
        public string ProductName { get; set; }
        public string SalesCategoryName { get; set; }
        public string ColorName { get; set; }
        public string ShapeName { get; set; }
        public string MarkingName { get; set; }

        public string FlavourName { get; set; }


        public long? FinishProductId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsMaster { get; set; }
        public long? FlavourId { get; set; }
        public decimal? LengthVariationId { get; set; }
        public decimal? LengthVariationValue { get; set; }
        public decimal? WidthVariationId { get; set; }
        public decimal? WidthVariationValue { get; set; }
        public decimal? ThicknessVariationId { get; set; }
        public decimal? ThicknessVariationValue { get; set; }
        public decimal? WeightVariationId { get; set; }
        public decimal? WeightVariationValue { get; set; }
        public byte[] Picture { get; set; }
        public decimal? CoreSizeLengthVariationId { get; set; }
        public decimal? CoreSizeLengthVariationValue { get; set; }
        public decimal? CoreSizeWidthVariationId { get; set; }
        public decimal? CoreSizeWidthVariationValue { get; set; }
        public decimal? CoreSizeThicknessVariationId { get; set; }
        public decimal? CoreSizeThicknessVariationValue { get; set; }
        public decimal? CoreSizeWeightVariationId { get; set; }
        public decimal? CoreSizeWeightVariationValue { get; set; }
        public decimal? CoatedSizeLengthVariationId { get; set; }
        public decimal? CoatedSizeLengthVariationValue { get; set; }
        public decimal? CoatedSizeWidthVariationId { get; set; }
        public decimal? CoatedSizeWidthVariationValue { get; set; }
        public decimal? CoatedSizeThicknessVariationId { get; set; }
        public decimal? CoatedSizeThicknessVariationValue { get; set; }
        public decimal? CoatedSizeWeightVariationId { get; set; }
        public decimal? CoatedSizeWeightVariationValue { get; set; }
        public long? ClarityId { get; set; }
        public string ClarityName { get; set; }
        public decimal? Phmin { get; set; }
        public decimal? Phmax { get; set; }
        public decimal? Sgmin { get; set; }
        public decimal? Sgmax { get; set; }
        public decimal? ViscosityMin { get; set; }
        public decimal? ViscosityMax { get; set; }
        public decimal? PhvariationId { get; set; }
        public decimal? PhvariationValue { get; set; }
        public decimal? SgvariationId { get; set; }
        public decimal? SgvariationValue { get; set; }
        public decimal? ViscosityVariationId { get; set; }
        public decimal? ViscosityVariationValue { get; set; }
        public decimal? PhStandard { get; set; }
        public decimal? SgStandard { get; set; }
        public decimal? ViscosityStandard { get; set; }
        public long? TextureId { get; set; }
        public string TextureName { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public List<long> ShapeIds { get; set; }
        public List<long> FlavourIds { get; set; }
        public List<long> MarkingIds { get; set; }
        public decimal? CreamPhmin { get; set; }
        public decimal? CreamPhmax { get; set; }
        public decimal? CreamSgmin { get; set; }
        public decimal? CreamSgmax { get; set; }
        public decimal? CreamViscosityMin { get; set; }
        public decimal? CreamViscosityMax { get; set; }
        public decimal? CreamPhvariationId { get; set; }
        public decimal? CreamPhvariationValue { get; set; }
        public decimal? CreamSgvariationId { get; set; }
        public decimal? CreamSgvariationValue { get; set; }
        public decimal? CreamViscosityVariationId { get; set; }
        public decimal? CreamViscosityVariationValue { get; set; }
        public decimal? CreamPhStandard { get; set; }
        public decimal? CreamSgStandard { get; set; }
        public decimal? CreamViscosityStandard { get; set; }
        public decimal? LengthStandard { get; set; }
        public decimal?  WidthStandard { get; set; }
        public decimal?  ThicknessStandard { get; set; }
        public decimal?  WeightStandard { get; set; }
        public decimal? CoreSizeLengthStandard { get; set; }
        public decimal? CoreSizeWidthStandard { get; set; }
        public decimal? CoreThicknessStandard { get; set; }
        public decimal? CoatedSizeLengthStandard { get; set; }
        public decimal? CoatedSizeWidthStandard { get; set; }
        public decimal? CoreWeightStandard { get; set; }
        public decimal? CoatedThicknessStandard { get; set; }
        public decimal? CoatedWeightStandard { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public int? RegistrationReferenceId { get; set; }
        public string RegistrationReferenceName { get; set; }
        public string ManufacturingSite { get; set; }
        public string CustomerProductName { get; set; }
        public string RegisterCountry { get; set; }
        public string PRHSpecificName { get; set; }
        public string RegisterHolderName { get; set; }
    }
}
