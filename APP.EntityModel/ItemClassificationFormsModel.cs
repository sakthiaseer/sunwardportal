﻿using System;
using System.Collections.Generic;

namespace APP.EntityModel
{
    public class ItemClassificationFormsModel : BaseModel
    {
        public long ItemClassificationFormsID { get; set; }
        public long? ItemClassificationID { get; set; }
        public long? FormID { get; set; }
        public string FormName { get; set; }
        public string ModuleName { get; set; }
        public string Path { get; set; }
        public long? ProfileID { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string ClassificationLinkType { get; set; }
        public string ProfileName { get; set; }
        public bool MainForm { get; set; }
        public List<ApplicationFormSubModel> ApplicationFormSubModelList { get; set; }
        public DocumentPermissionModel DocumentPermissionModels { get; set; }


    }
}
