import myApi from '@/util/api'

export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
}
export function getDepartmentByDivision(id) {

    return myApi.getByID(id, 'Department/GetDepartmentByDivision')
}
export function getSections() {

    return myApi.getAll('Section/GetSections')
}
export function getSubSections() {

    return myApi.getAll('SubSection/GetSubSections')
}
export function getDivisionsByCompany(id) {

    return myApi.getByID(id, 'Division/GetDivisionsByCompany')
}
export function getProfileAutonumber(id) {

    return myApi.getByID(id, 'Appwiki/GetProfileAutonumber')
}