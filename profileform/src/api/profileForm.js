import myApi from '@/util/api'
export function uploadFiles(data) {

  return myApi.uploadFile(data, "FileProfileTypeDynamicForm/UploadDocuments");
}
export function getFileProfileSetupFormsById(id) {

  return myApi.getByID(id, 'FileProfileSetupForm/GetFileProfileSetupFormsByIds')
}
export function insertFileProfileTypeDynamicForm(data) {

  return myApi.create(data, "FileProfileTypeDynamicForm/InsertFileProfileTypeDynamicForm");
}
export function getValidFileProfileIds(id) {

  return myApi.getByID(id, 'FileProfileSetupForm/GetValidFileProfileIds')
}
export function getFileProfileTypeDynamicForm(id,fileprofiletypeid) {

  return myApi.getAll('FileProfileTypeDynamicForm/GetFileProfileTypeDynamicForm/?id='+id+'&&fileprofiletypeid='+fileprofiletypeid)
}