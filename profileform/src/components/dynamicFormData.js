const formData=[{
    fileProfileSetupFormId: 1,
    fileProfileTypeId: 1,
    controlTypeName: "text",
    controlTypeValue: "First Name",//Display Text
    propertyName: "firstname", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    placeholder: "First Name",
},
{
    fileProfileSetupFormId: 2,
    fileProfileTypeId: 1,
    controlTypeName: "text",
    controlTypeValue: "Last Name",//Display Text
    propertyName: "lastname", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    placeholder: "Last Name",
},
{
    fileProfileSetupFormId: 3,
    fileProfileTypeId: 1,
    controlTypeName: "autocomplete",
    controlTypeValue: "Autocomplete",//Display Text
    propertyName: "autocompletename", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    isMultiple: true,
    placeholder: "",
    dropDownItems: [
        { text: "autocomplete 1", value: 1 },
        { text: "autocomplete 2", value: 2 },
        { text: "autocomplete 3", value: 3 },
    ]
},
{
    fileProfileSetupFormId: 4,
    fileProfileTypeId: 1,
    controlTypeName: "radio",
    controlTypeValue: "Radio",//Display Text
    propertyName: "radioname", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    isMultiple: true,
    placeholder: "",
    dropDownItems: [
        { text: "Radio 1", value: "radio1" },
        { text: "Radio 2", value: "radio2" },
    ]
},
{
    fileProfileSetupFormId: 5,
    fileProfileTypeId: 1,
    controlTypeName: "checkbox",
    controlTypeValue: "Check Box",//Display Text
    propertyName: "checkboxname", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    isMultiple: true,
    placeholder: "",
    dropDownItems: [
        { text: "checkbox 1", value: "checkbox1" },
        { text: "checkbox 2", value: "checkbox2" },
        { text: "checkbox 3", value: "checkbox3" },
    ]
},
{
    fileProfileSetupFormId: 6,
    fileProfileTypeId: 1,
    controlTypeName: "textarea",
    controlTypeValue: "Text Areas",//Display Text
    propertyName: "textareaname", //Form Iputs Name
    defaultValue: "",
    isRequired: true,
    placeholder: "Text Area Placeholder",
},
{
    fileProfileSetupFormId: 7,
    fileProfileTypeId: 1,
    controlTypeName: "textEditor",
    controlTypeValue: "Text Editor",//Display Text
    propertyName: "textEditorname", //Form Iputs Name
    defaultValue: "",
    isRequired: false,
    placeholder: "",
},
{
    fileProfileSetupFormId: 8,
    fileProfileTypeId: 1,
    controlTypeName: "fileinput",
    controlTypeValue: "File Input",//Display Text
    propertyName: "fileinputname", //Form Iputs Name
    defaultValue: "",
    isMultiple: true,
    isRequired: false,
    placeholder: "",
},
{
    fileProfileSetupFormId: 9,
    fileProfileTypeId: 1,
    controlTypeName: "datepicker",
    controlTypeValue: "Date Picker",//Display Text
    propertyName: "datepickername", //Form Iputs Name
    defaultValue: "",
    isMultiple: true,
    isRequired: false,
    placeholder: "",
},
]
export default formData;