const dynamicFormGenerate = (item) => {
    var formItems={};
    Object.keys(item).forEach(key => {
        var controlTypeName=item[key].controlType.toLowerCase();
        formItems[item[key].propertyName]={
            name:item[key].controlTypeValue,
            rules:item[key].isRequired==true?"required":(controlTypeName=='fileinput'?"required":""),
            type:controlTypeName,
            line: controlTypeName=='texteditor'?12:6,
            props: {
              placeholder:item[key].placeholder,
            },
        };
        if(controlTypeName=='checkbox' || controlTypeName=='radio')
        {
            formItems[item[key].propertyName].isMultiple=true;
            if(item[key].dropDownItems!=undefined)
            {
            formItems[item[key].propertyName].items=item[key].dropDownItems;
            }
        }
        if(item[key].isMultiple==true ||controlTypeName=='dropdown' || controlTypeName=='select')
        {
            formItems[item[key].propertyName].props.multiple=item[key].isMultiple==true?true:false;
            if(item[key].dropDownItems!=undefined)
            {
            formItems[item[key].propertyName].items=item[key].dropDownItems;
            }
            if(controlTypeName=='dropdown' || controlTypeName=='select')
            {
                formItems[item[key].propertyName].props.chips=true;
                formItems[item[key].propertyName].props.clearable=true;
            }
        }
      });
    return formItems;
};
 export default {
    dynamicFormGenerate
  };