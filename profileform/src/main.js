import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
//import VueMce from './util/vue-mce'

/* if (window) {
  window.VueMce = VueMce

  if (window.Vue) {
    window.Vue.use(VueMce)
  }
}

Vue.use(VueMce); */
Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
