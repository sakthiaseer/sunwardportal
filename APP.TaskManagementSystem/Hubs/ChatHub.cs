﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper.Chat;
using APP.TaskManagementSystem.Hubs.Model;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Hubs
{
    public class ChatHub : Hub
    {
        private readonly CRT_TMSContext _context;
        public ChatHub(CRT_TMSContext context)
        {
            _context = context;

        }
        public Task SendMessageToAll(Message message)
        {
            message.ConnectionId = Context.ConnectionId;
            message.MessagDateTime = DateTime.Now;
            return Clients.All.SendAsync("ReceiveMessage", message);
        }
        public async Task AssociateJob(string jobId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, jobId);
        }
        public Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToUser(ChatMessageModel message)
        {
            var chatUsers = GetOnlineUsers();
            var receiveChatUser = chatUsers.FirstOrDefault(c => c.UserId == message.ReceiveUserId);
            if (receiveChatUser == null)
            {
                InsertChat(message, null);
            }
            else
            {
                InsertChat(message, receiveChatUser.ConnectionId);
                message.IsIncoming = true;
                return Clients.Client(receiveChatUser.ConnectionId).SendAsync("ReceiveMessage", message);
            }
            return Task.FromResult(0);
        }
        public void InsertChat(ChatMessageModel message, string connectionId)
        {
            if (connectionId == null)
            {
                var userExist = _context.ChatUser.FirstOrDefault(u => u.UserId == message.ReceiveUserId.Value);
                if (userExist == null)
                {
                    var newChatUser = new ChatUser
                    {
                        UserId = message.ReceiveUserId.Value,
                        SessionId = Guid.NewGuid(),
                        ConnectionId = null,
                        IsOnline = false,
                        StatusCodeId = 1,
                    };
                    _context.ChatUser.Add(newChatUser);
                }

            }
            var chatMessage = new ChatMessage
            {
                Message = message.Message,
                IsDelivered = !string.IsNullOrEmpty(connectionId),
                CreatedDateTime = DateTime.Now,
                MessageType = message.MessageType,
                ReceiveUserId = message.ReceiveUserId,
                SentUserId = message.SentUserId,
                FileName = message.FileName
            };
            _context.ChatMessage.Add(chatMessage);


            _context.SaveChanges();
        }
        public Task JoinGroup(string group)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public Task SendMessageToGroup(string group, string message)
        {
            return Clients.Group(group).SendAsync("ReceiveMessage", message);
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("UserConnected", Context.ConnectionId);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            // change user status in db..

            var userExist = _context.ChatUser.FirstOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (userExist != null)
            {
                userExist.IsOnline = false;
                _context.SaveChanges();
            }

            await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
            await base.OnDisconnectedAsync(ex);
        }

        public void InsertOnlineUserAsync(User user)
        {
            var chatUser = _context.ChatUser.FirstOrDefault(u => u.UserId == user.UserId);

            //insert chat user...

            if (chatUser == null)
            {
                var newChatUser = new ChatUser
                {
                    UserId = user.UserId,
                    SessionId = Guid.NewGuid(),
                    ConnectionId = user.ConnectionId,
                    IsOnline = true,
                    StatusCodeId = 1,
                };
                _context.ChatUser.Add(newChatUser);
            }
            else
            {
                chatUser.ConnectionId = user.ConnectionId;
                chatUser.IsOnline = true;
                chatUser.StatusCodeId = 1;
                _context.ChatUser.Update(chatUser);
            }
            _context.SaveChanges();
        }

        private List<ChatUser> GetOnlineUsers()
        {
            return _context.ChatUser.Where(u => u.IsOnline.GetValueOrDefault()).ToList();
        }
    }
}
