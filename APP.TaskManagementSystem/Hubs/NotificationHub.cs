﻿using APP.DataAccess.Models;
using APP.EntityModel;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Hubs
{
    public class NotificationHub : Hub
    {
        private readonly CRT_TMSContext _context;
        public NotificationHub(CRT_TMSContext context)
        {
            _context = context;

        }
        public Task SendMessageToAll(NotificationHandlerModel notification)
        {
            notification.ConnectionId = Context.ConnectionId;
            return Clients.All.SendAsync("ReceiveNotification", notification);
        }
        public async Task AssociateJob(string jobId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, jobId);
        }
        public Task JoinGroup(string group)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public Task SendMessageToGroup(string group, NotificationHandlerModel notification)
        {
            return Clients.Group(group).SendAsync("ReceiveNotifications", notification);
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("UserConnected", Context.ConnectionId);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            // change user status in db..

            var userExist = _context.ChatUser.FirstOrDefault(u => u.ConnectionId == Context.ConnectionId);
            if (userExist != null)
            {
                userExist.IsOnline = false;
                _context.SaveChanges();
            }

            await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
            await base.OnDisconnectedAsync(ex);
        }

    }
}

