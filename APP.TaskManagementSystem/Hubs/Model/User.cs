﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Hubs.Model
{
    public class User
    {
        public long UserId { get; set; }

        public string ConnectionId { get; set; }

        public string Name { get; set; }
    }
}
