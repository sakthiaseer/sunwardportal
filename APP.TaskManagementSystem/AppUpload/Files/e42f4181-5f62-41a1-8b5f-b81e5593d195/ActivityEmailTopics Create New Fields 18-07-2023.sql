/*
   Tuesday, July 18, 202311:17:43 AM
   User: crt
   Server: portal.sunwardpharma.com
   Database: SWUAT
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ActivityEmailTopics
	DROP CONSTRAINT FK_ProductionActivityEmailLink_CodeMaster
GO
ALTER TABLE dbo.CodeMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ActivityEmailTopics
	DROP CONSTRAINT FK_ProductionActivityEmailLink_ApplicationUser
GO
ALTER TABLE dbo.ActivityEmailTopics
	DROP CONSTRAINT FK_ProductionActivityEmailLink_ApplicationUser1
GO
ALTER TABLE dbo.ApplicationUser SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ActivityEmailTopics
	(
	ActivityEmailTopicID bigint NOT NULL IDENTITY (1, 1),
	ActivityMasterId bigint NULL,
	ManufacturingProcessId bigint NULL,
	CategoryActionId bigint NULL,
	ActionId bigint NULL,
	Comment nvarchar(MAX) NULL,
	DocumentSessionId uniqueidentifier NULL,
	SubjectName varchar(300) NULL,
	StatusCodeID int NULL,
	AddedByUserID bigint NULL,
	AddedDate datetime NULL,
	ModifiedByUserID bigint NULL,
	ModifiedDate datetime NULL,
	SessionId uniqueidentifier NULL,
	EmailTopicSessionId uniqueidentifier NULL,
	ActivityType varchar(150) NULL,
	FromId bigint NULL,
	ToIds varchar(MAX) NULL,
	CcIds varchar(MAX) NULL,
	Tags varchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ActivityEmailTopics SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_ActivityEmailTopics ON
GO
IF EXISTS(SELECT * FROM dbo.ActivityEmailTopics)
	 EXEC('INSERT INTO dbo.Tmp_ActivityEmailTopics (ActivityEmailTopicID, ActivityMasterId, ManufacturingProcessId, CategoryActionId, SubjectName, StatusCodeID, AddedByUserID, AddedDate, ModifiedByUserID, ModifiedDate, SessionId, EmailTopicSessionId, ActivityType, FromId, ToIds, CcIds, Tags)
		SELECT ActivityEmailTopicID, ActivityMasterId, ManufacturingProcessId, CategoryActionId, SubjectName, StatusCodeID, AddedByUserID, AddedDate, ModifiedByUserID, ModifiedDate, SessionId, EmailTopicSessionId, ActivityType, FromId, ToIds, CcIds, Tags FROM dbo.ActivityEmailTopics WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ActivityEmailTopics OFF
GO
DROP TABLE dbo.ActivityEmailTopics
GO
EXECUTE sp_rename N'dbo.Tmp_ActivityEmailTopics', N'ActivityEmailTopics', 'OBJECT' 
GO
ALTER TABLE dbo.ActivityEmailTopics ADD CONSTRAINT
	PK_ProductionActivityEmailLink PRIMARY KEY CLUSTERED 
	(
	ActivityEmailTopicID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ActivityEmailTopics ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationUser FOREIGN KEY
	(
	AddedByUserID
	) REFERENCES dbo.ApplicationUser
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActivityEmailTopics ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationUser1 FOREIGN KEY
	(
	ModifiedByUserID
	) REFERENCES dbo.ApplicationUser
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActivityEmailTopics ADD CONSTRAINT
	FK_ProductionActivityEmailLink_CodeMaster FOREIGN KEY
	(
	StatusCodeID
	) REFERENCES dbo.CodeMaster
	(
	CodeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'CONTROL') as Contr_Per 