/*
   Wednesday, June 28, 202311:47:08 AM
   User: crt
   Server: portal.sunwardpharma.com
   Database: SWUAT
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ApplicationMasterChild SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductActivityCase SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductActivityCase', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductActivityCase', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductActivityCase', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.ProductActivityCaseCategoryMultiple
	(
	ProductActivityCaseCategoryMultipleID bigint NOT NULL IDENTITY (1, 1),
	CategoryActionID bigint NULL,
	ProductActivityCaseID bigint NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.ProductActivityCaseCategoryMultiple ADD CONSTRAINT
	PK_ProductActivityCaseCategoryMultiple PRIMARY KEY CLUSTERED 
	(
	ProductActivityCaseCategoryMultipleID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ProductActivityCaseCategoryMultiple ADD CONSTRAINT
	FK_ProductActivityCaseCategoryMultiple_ProductActivityCase FOREIGN KEY
	(
	ProductActivityCaseID
	) REFERENCES dbo.ProductActivityCase
	(
	ProductActivityCaseID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductActivityCaseCategoryMultiple ADD CONSTRAINT
	FK_ProductActivityCaseCategoryMultiple_ApplicationMasterChild FOREIGN KEY
	(
	CategoryActionID
	) REFERENCES dbo.ApplicationMasterChild
	(
	ApplicationMasterChildID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductActivityCaseCategoryMultiple SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductActivityCaseCategoryMultiple', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductActivityCaseCategoryMultiple', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductActivityCaseCategoryMultiple', 'Object', 'CONTROL') as Contr_Per 