/*
   Sunday, July 9, 202311:58:56 AM
   User: crt
   Server: portal.sunwardpharma.com
   Database: SWUAT
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ApplicationMasterDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ApplicationMasterDetail', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterDetail', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterDetail', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductionActivityAppLine ADD
	ActivityMasterID bigint NULL,
	ActivityStatusID bigint NULL
GO
ALTER TABLE dbo.ProductionActivityAppLine ADD CONSTRAINT
	FK_ProductionActivityAppLine_ApplicationMasterDetail4 FOREIGN KEY
	(
	ActivityMasterID
	) REFERENCES dbo.ApplicationMasterDetail
	(
	ApplicationMasterDetailID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityAppLine ADD CONSTRAINT
	FK_ProductionActivityAppLine_ApplicationMasterDetail5 FOREIGN KEY
	(
	ActivityStatusID
	) REFERENCES dbo.ApplicationMasterDetail
	(
	ApplicationMasterDetailID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityAppLine SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductionActivityAppLine', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductionActivityAppLine', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductionActivityAppLine', 'Object', 'CONTROL') as Contr_Per 