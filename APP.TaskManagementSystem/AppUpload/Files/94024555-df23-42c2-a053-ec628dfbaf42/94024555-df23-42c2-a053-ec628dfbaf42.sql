/*
   Thursday, July 6, 20231:39:16 PM
   User: crt
   Server: portal.sunwardpharma.com
   Database: SWUAT
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CodeMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.CodeMaster', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ApplicationUser SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ApplicationUser', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ApplicationMasterChild SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ApplicationMasterChild', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductionActivityMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductionActivityMaster', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductionActivityMaster', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductionActivityMaster', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.ProductionActivityEmailLink
	(
	ProductionActivityEmailLinkID bigint NOT NULL IDENTITY (1, 1),
	ProductionActivityMasterID bigint NULL,
	ManufacturingProcessID bigint NULL,
	CategoryActionID bigint NULL,
	ActionID bigint NULL,
	Subject nvarchar(MAX) NULL,
	StatusCodeID int NULL,
	AddedByUserID bigint NULL,
	AddedDate datetime NULL,
	ModifiedByUserID bigint NULL,
	ModifiedDate datetime NULL,
	SessionID uniqueidentifier NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	PK_ProductionActivityEmailLink PRIMARY KEY CLUSTERED 
	(
	ProductionActivityEmailLinkID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ProductionActivityMaster FOREIGN KEY
	(
	ProductionActivityMasterID
	) REFERENCES dbo.ProductionActivityMaster
	(
	ProductionActivityMasterID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationMasterChild FOREIGN KEY
	(
	ManufacturingProcessID
	) REFERENCES dbo.ApplicationMasterChild
	(
	ApplicationMasterChildID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationMasterChild1 FOREIGN KEY
	(
	CategoryActionID
	) REFERENCES dbo.ApplicationMasterChild
	(
	ApplicationMasterChildID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationMasterChild2 FOREIGN KEY
	(
	ActionID
	) REFERENCES dbo.ApplicationMasterChild
	(
	ApplicationMasterChildID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationUser FOREIGN KEY
	(
	AddedByUserID
	) REFERENCES dbo.ApplicationUser
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_ApplicationUser1 FOREIGN KEY
	(
	ModifiedByUserID
	) REFERENCES dbo.ApplicationUser
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink ADD CONSTRAINT
	FK_ProductionActivityEmailLink_CodeMaster FOREIGN KEY
	(
	StatusCodeID
	) REFERENCES dbo.CodeMaster
	(
	CodeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductionActivityEmailLink SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ProductionActivityEmailLink', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ProductionActivityEmailLink', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ProductionActivityEmailLink', 'Object', 'CONTROL') as Contr_Per 