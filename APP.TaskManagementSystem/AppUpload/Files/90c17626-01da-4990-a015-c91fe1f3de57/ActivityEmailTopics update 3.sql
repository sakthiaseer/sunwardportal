/*
   Saturday, July 8, 20234:14:30 PM
   User: crt
   Server: portal.sunwardpharma.com
   Database: SWUAT
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.ActivityEmailTopics.Subject', N'Tmp_SubjectName_6', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.ActivityEmailTopics.Tmp_SubjectName_6', N'SubjectName', 'COLUMN' 
GO
ALTER TABLE dbo.ActivityEmailTopics SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActivityEmailTopics', 'Object', 'CONTROL') as Contr_Per 