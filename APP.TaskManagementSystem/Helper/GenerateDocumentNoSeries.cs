﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace APP.TaskManagementSystem.Helper
{
    public class GenerateDocumentNoSeries
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public GenerateDocumentNoSeries(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public string GenerateDocumentProfileAutoNumber(DocumentNoSeriesModel noSeriesModel)
        {
            var sameSettings = "No";
            var documentNo = "";
            ProfileAutoNumber profileAutoNumbers = new ProfileAutoNumber();                    
                documentNo = GenerateDocumentNo(new DocumentNoSeriesModel
                {
                    ProfileID = noSeriesModel.ProfileID,
                    AddedByUserID = noSeriesModel.AddedByUserID,
                    StatusCodeID = noSeriesModel.StatusCodeID,
                    DepartmentName = noSeriesModel.DepartmentName,
                    CompanyCode = noSeriesModel.CompanyCode,
                    SectionName = noSeriesModel.SectionName,
                    SubSectionName = noSeriesModel.SubSectionName,
                    DepartmentId = noSeriesModel.DepartmentId,
                    PlantID = noSeriesModel.PlantID,
                    SectionId = noSeriesModel.SectionId,
                    SubSectionId = noSeriesModel.SubSectionId,
                    ScreenID = noSeriesModel.ScreenID,
                    ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,
                    CompanyId= noSeriesModel.PlantID,
                   
                });
            
            return documentNo;
        }
        public string GenerateDocumentNo(DocumentNoSeriesModel noSeriesModel)
        {
            bool isCompanyDepartmentExist = false;
            string documentNo = string.Empty;
            string deptProfileCode = "";
            string sectionProfileCode = "";
            string subSectionProfileCode = "";
            if(noSeriesModel.CompanyId > 0)
            {
                noSeriesModel.CompanyCode = _context.Plant.Where(p => p.PlantId == noSeriesModel.CompanyId).FirstOrDefault()?.PlantCode;   
            }
            if (noSeriesModel.DepartmentId > 0)
            {
                deptProfileCode = _context.Department.Where(s => s.DepartmentId == noSeriesModel.DepartmentId).FirstOrDefault()?.ProfileCode;
            }
            if (noSeriesModel.SectionId > 0)
            {
                noSeriesModel.SectionName = _context.Section.Where(s => s.SectionId == noSeriesModel.SectionId).FirstOrDefault()?.ProfileCode;
                if(noSeriesModel.SectionName==null)
                {
                    noSeriesModel.SectionName = _context.Section.Where(s => s.SectionId == noSeriesModel.SectionId).FirstOrDefault()?.Code;
                }
            }
            if (noSeriesModel.SubSectionId > 0)
            {
                noSeriesModel.SubSectionName = _context.SubSection.Where(s => s.SubSectionId == noSeriesModel.SubSectionId).FirstOrDefault()?.ProfileCode;
                if(noSeriesModel.SubSectionName==null)
                {
                    noSeriesModel.SubSectionName = _context.SubSection.Where(s => s.SubSectionId == noSeriesModel.SubSectionId).FirstOrDefault()?.Code;
                }

            }
             if (noSeriesModel.ProfileID == null || noSeriesModel.ProfileID <= 0)
            {
                return null;
            }
            else
            {
                var profileSettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID);

                if (profileSettings.CompanyId > 0 && (noSeriesModel.CompanyId ==null || noSeriesModel.PlantID == null))
                {
                    noSeriesModel.CompanyCode = _context.Plant.Where(s => s.PlantId == profileSettings.CompanyId).FirstOrDefault().PlantCode;
                }
                if (profileSettings.DeparmentId > 0 && noSeriesModel.DepartmentId == null)
                {
                    var department = _context.Department.Where(s => s.DepartmentId == profileSettings.DeparmentId)?.FirstOrDefault();
                    if (department != null)
                    {
                        if (department.ProfileCode != null && department.ProfileCode != "")
                        {
                            noSeriesModel.DepartmentName = department.ProfileCode;
                        }
                        else
                        {
                            noSeriesModel.DepartmentName = department.Code;
                        }
                    }
                }
               
                List<string> numberSeriesCodes = new List<string> { "Company", "Department" };
                //List<string> numberSeriesSectionCodes = new List<string> { "Company", "Department", "Section" };
                //List<string> numberSeriesSubSectionCodes = new List<string> { "Company", "Department", "Section","SubSection" };
                List<NumberSeriesCodeModel> numberSeriesCodeModels = new List<NumberSeriesCodeModel>();

                List<Seperator> seperators = new List<Seperator>();
                seperators.Add(new Seperator { SeperatorSymbol = "/", SeperatorValue = 1 });
                seperators.Add(new Seperator { SeperatorSymbol = "-", SeperatorValue = 2 });

                var seperator = seperators.FirstOrDefault(s => s.SeperatorValue == profileSettings.SeperatorToUse.GetValueOrDefault(0));
                var seperatorSymbol = seperator != null ? seperator.SeperatorSymbol : "/";

                if (!String.IsNullOrEmpty(profileSettings.Abbreviation1))
                {
                    numberSeriesCodeModels = JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(profileSettings.Abbreviation1).ToList();
                    isCompanyDepartmentExist = numberSeriesCodeModels.Any(c => numberSeriesCodes.Contains(c.Name));
                    //isCompanyDepartmentSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSectionCodes.Contains(c.Name));
                    //isCompanyDepartmentSubSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSubSectionCodes.Contains(c.Name));
                    numberSeriesCodeModels.OrderBy(n => n.Index).ToList().ForEach(n =>
                    {
                        if (n.Name == "Company" && !string.IsNullOrEmpty(noSeriesModel.CompanyCode))
                        {
                            documentNo += noSeriesModel.CompanyCode + seperatorSymbol;
                        }
                        if (n.Name == "Department" && !string.IsNullOrEmpty(noSeriesModel.DepartmentName))
                        {
                            noSeriesModel.DepartmentName = string.IsNullOrEmpty(deptProfileCode) ? noSeriesModel.DepartmentName : deptProfileCode;
                            string[] departmentDetails = noSeriesModel.DepartmentName.Split(" ");
                            if (departmentDetails.Length > 1 && !string.IsNullOrEmpty(departmentDetails[1]))
                            {
                                documentNo += departmentDetails[1] + seperatorSymbol;
                            }
                            else if (!string.IsNullOrEmpty(noSeriesModel.DepartmentName))
                            {
                                documentNo += noSeriesModel.DepartmentName + seperatorSymbol;
                            }
                        }
                        if (n.Name == "Section" && !string.IsNullOrEmpty(noSeriesModel.SectionName))
                        {
                            noSeriesModel.SectionName = string.IsNullOrEmpty(sectionProfileCode) ? noSeriesModel.SectionName : sectionProfileCode;
                            documentNo += noSeriesModel.SectionName + seperatorSymbol;
                        }
                        if (n.Name == "SubSection" && !string.IsNullOrEmpty(noSeriesModel.SubSectionName))
                        {
                            noSeriesModel.SubSectionName = string.IsNullOrEmpty(subSectionProfileCode) ? noSeriesModel.SubSectionName : subSectionProfileCode;
                            documentNo += noSeriesModel.SubSectionName + seperatorSymbol;
                        }
                    });
                }

                if (profileSettings.AbbreviationRequired.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.Abbreviation))
                {
                    documentNo += profileSettings.Abbreviation + seperatorSymbol;
                }

                if (profileSettings.IsGroupAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.GroupAbbreviation))
                {
                    documentNo += profileSettings.GroupAbbreviation + seperatorSymbol;
                }

                if (profileSettings.IsCategoryAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.CategoryAbbreviation))
                {
                    documentNo += profileSettings.CategoryAbbreviation + seperatorSymbol;
                }

                if (!String.IsNullOrEmpty(profileSettings.SpecialWording))
                {
                    documentNo += profileSettings.SpecialWording + seperatorSymbol;
                }
                if (profileSettings.StartWithYear.GetValueOrDefault(false))
                {
                    documentNo += DateTime.Now.Year.ToString().Substring(2, 2) + seperatorSymbol;
                }
                if (profileSettings.NoOfDigit.HasValue && profileSettings.NoOfDigit > 0)
                {
                    if (isCompanyDepartmentExist)
                    {
                        if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {

                            ProfileAutoNumber profileAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);

                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {

                            ProfileAutoNumber profileAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);

                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                        {

                            ProfileAutoNumber profileAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);

                        }
                        else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {

                            ProfileAutoNumber profileAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);

                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == noSeriesModel.SubSectionId);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }

                        else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                    }

                    else
                    {
                        //var profileSectionAutoNumberNew = _context.ProfileAutoNumber.LastOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                        ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.OrderByDescending(s=>s.ProfileAutoNumberId).FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                        if (String.IsNullOrEmpty(profileSettings.LastNoUsed))
                        {
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.StartingNo) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                            //documentNo += profileSettings.LastNoUsed;
                        }
                        else
                        {
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.LastNoUsed) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                            //documentNo += profileSettings.LastNoUsed;
                        }
                    }

                    profileSettings.LastCreatedDate = DateTime.Now;
                    _context.SaveChanges();
                }
                //var existingProfileNoseries = _context.DocumentNoSeries.Where(s => s.ProfileId == profileSettings.ProfileId).ToList();
                //if (existingProfileNoseries.Count == 0)
                //{
                    var SessionId = Guid.NewGuid();
                    if(noSeriesModel.RequestorId==null)
                    {
                        noSeriesModel.RequestorId = noSeriesModel.AddedByUserID;
                    }
                    var documentNoSeries = new DocumentNoSeries
                    {
                        ProfileId = profileSettings.ProfileId,
                        DocumentNo = documentNo,
                        AddedDate = DateTime.Now,
                        AddedByUserId = noSeriesModel.AddedByUserID,
                        StatusCodeId = noSeriesModel.StatusCodeID.Value,
                        SessionId = SessionId,
                        RequestorId = noSeriesModel.RequestorId,
                        Title = noSeriesModel.Title,
                        ModifiedDate = DateTime.Now,
                        ModifiedByUserId = noSeriesModel.AddedByUserID,

                    };

                    _context.DocumentNoSeries.Add(documentNoSeries);
                    _context.SaveChanges();
                //}
                //else if (existingProfileNoseries.Count > 0)
                //{
                //    var numberSeriesId = _context.DocumentNoSeries.OrderByDescending(s => s.NumberSeriesId).FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID).NumberSeriesId;
                //    var updateDocumentNoSeries = _context.DocumentNoSeries.Where(s => s.NumberSeriesId == numberSeriesId).FirstOrDefault();
                //    if (updateDocumentNoSeries != null)
                //    {
                //        updateDocumentNoSeries.DocumentNo = documentNo;
                //        updateDocumentNoSeries.ModifiedDate = DateTime.Now;
                //        updateDocumentNoSeries.ModifiedByUserId = noSeriesModel.AddedByUserID;
                //        updateDocumentNoSeries.Title = noSeriesModel.Title;
                //    }
                //    _context.SaveChanges();
                //}
                return documentNo;
            }
        }


        private string GenerateProfileAuto(DocumentNoSeriesModel noSeriesModel, ProfileAutoNumber profileAutonumber, DocumentProfileNoSeries profilesettings, string documentNo)
        {
            //string documentNo = string.Empty;
            // profilesettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID);
            //ProfileAutoNumber profileSubSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profilesettings.ProfileId && p.CompanyId == noSeriesModel.CompanyId && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
            string LastNoUsed = "";
            if (profileAutonumber == null)
            {
               


                    LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);
                    if(noSeriesModel.PlantID == 0)
                {
                    noSeriesModel.PlantID = null;
                }
                    ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                    {
                        ProfileId = profilesettings.ProfileId,
                        CompanyId = noSeriesModel.PlantID,
                        DepartmentId = noSeriesModel.DepartmentId,
                        SectionId = noSeriesModel.SectionId,
                        SubSectionId = noSeriesModel.SubSectionId,
                        LastNoUsed = LastNoUsed,
                        ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0,
                        ScreenId = noSeriesModel.ScreenID,
                        ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,

                    };
                    documentNo += LastNoUsed;
                    _context.ProfileAutoNumber.Add(newProfileAutoNumber);
                
            }
            else if (profilesettings.StartWithYear.GetValueOrDefault(false) && profileAutonumber.ProfileYear.GetValueOrDefault(0) > 0 && profileAutonumber.ProfileYear.Value != DateTime.Now.Year)
            {
                var yearchecking = _context.ProfileAutoNumber.Where(p => p.ProfileId == profilesettings.ProfileId && p.ProfileYear > 0 && p.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                if (yearchecking == null)
                {
                    LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                    ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                    {
                        ProfileId = profilesettings.ProfileId,
                        CompanyId = noSeriesModel.PlantID,
                        DepartmentId = noSeriesModel.DepartmentId,
                        SectionId = noSeriesModel.SectionId,
                        SubSectionId = noSeriesModel.SubSectionId,
                        LastNoUsed = LastNoUsed,
                        ScreenId = noSeriesModel.ScreenID,
                        ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,
                        ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                    };
                    documentNo += LastNoUsed;
                    _context.ProfileAutoNumber.Add(newProfileAutoNumber);
                    _context.SaveChanges();
                }
               else if (yearchecking != null)
                {
                    LastNoUsed = (Convert.ToInt32(yearchecking.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                  
                    documentNo += LastNoUsed;
                    if (noSeriesModel != null)
                    {
                        if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == noSeriesModel.PlantID && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                    }
                   else  if (noSeriesModel == null)
                    {

                        var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                        if (existingprofileAutonumber != null)
                        {
                            existingprofileAutonumber.LastNoUsed = LastNoUsed;
                        }
                        _context.SaveChanges();
                    }
                }
                else
                {
                    LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                    ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                    {
                        ProfileId = profilesettings.ProfileId,
                        CompanyId = noSeriesModel.PlantID,
                        DepartmentId = noSeriesModel.DepartmentId,
                        SectionId = noSeriesModel.SectionId,
                        SubSectionId = noSeriesModel.SubSectionId,
                        LastNoUsed = LastNoUsed,
                        ScreenId = noSeriesModel.ScreenID,
                        ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,
                        ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                    };
                    documentNo += LastNoUsed;
                    _context.ProfileAutoNumber.Add(newProfileAutoNumber);
                    _context.SaveChanges();

                }
            }
            else
            {
                LastNoUsed = (Convert.ToInt32(profileAutonumber.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);
                profileAutonumber.LastNoUsed = LastNoUsed;
                documentNo += LastNoUsed;
            }
            return documentNo;
        }
    }
}
