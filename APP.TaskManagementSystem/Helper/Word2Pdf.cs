

using Microsoft.Office.Interop.Word;
using System; 
using System.Runtime.InteropServices;

namespace APP.TaskManagementSystem.Helper
{
	public class Word2Pdf
	{
		private Application MSWordDoc;

		private object UnknownType = Type.Missing;

		public object InputLocation
		{
			get;
			set;
		}

		public object OutputLocation
		{
			get;
			set;
		}

		public Word2Pdf()
		{
		}

		public string Word2PdfCOnversion()
		{
			string str;
			try
			{
				try
				{
					if (this.MSWordDoc == null)
					{
						this.MSWordDoc = (Application)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("000209FF-0000-0000-C000-000000000046")));
					}
					this.MSWordDoc.Visible = false;
					if (this.OutputLocation == null || !(this.InputLocation != null & this.OutputLocation != null) || this.InputLocation == null)
					{
						str = "Error";
					}
					else
					{
						Documents documents = this.MSWordDoc.Documents;
						object inputLocation = this.InputLocation;
						documents.Open(ref inputLocation, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType);
						this.MSWordDoc.Application.Visible = false;
						this.MSWordDoc.WindowState = WdWindowState.wdWindowStateMinimize;
						object obj = WdSaveFormat.wdFormatPDF;
						Document activeDocument = this.MSWordDoc.ActiveDocument;
						object outputLocation = this.OutputLocation;
						activeDocument.SaveAs(ref outputLocation, ref obj, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType, ref this.UnknownType);
						str = "Success";
					}
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					str = "Error";
					str = string.Concat(str, "<---Error Message--->", exception.Message);
				}
			}
			finally
			{
				if (this.MSWordDoc != null)
				{
					this.MSWordDoc.Documents.Close(ref this.UnknownType, ref this.UnknownType, ref this.UnknownType);
				}
				this.MSWordDoc.Quit(ref this.UnknownType, ref this.UnknownType, ref this.UnknownType);
			}
			return str;
		}
	}
}