﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem
{
    public class SWApiResponse
    {
        public int Code { get; set; }
        public bool IsError { get; set; } = false;
        public string Message { get; set; }
        public object Result { get; set; }
        public DateTime SentDate { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class Pagination
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; } 
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        
    }
}
