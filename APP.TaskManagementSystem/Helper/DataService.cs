﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Helper
{
    public class DataService
    {
        private readonly IConfiguration _config;
        public DataService(IConfiguration config, string company)
        {
            _config = config;
            Context = new NAV.NAV(new Uri($"{_config[company + ":OdataUrl"]}/Company('{_config[company + ":Company"]}')"))
            {
                Credentials = new NetworkCredential(_config[company + ":UserName"], _config[company + ":Password"])
            };

          //  System.Threading.Thread.Sleep(3000);
        }
        public DataService(IConfiguration config, string company,string v4)
        {
            _config = config;
            Context = new NAV.NAV(new Uri($"{_config[company + ":OdataUrl"]}V4/Company('{_config[company + ":Company"]}')"))
            {
                Credentials = new NetworkCredential(_config[company + ":UserName"], _config[company + ":Password"])
            };

            //  System.Threading.Thread.Sleep(3000);
        }
        public NAV.NAV Context { get; private set; }
    }
}
