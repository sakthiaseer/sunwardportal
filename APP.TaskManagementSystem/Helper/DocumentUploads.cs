﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using APP.EntityModel;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace APP.TaskManagementSystem.Helper
{
    public class DocumentUploads
    {
        private readonly IConfiguration _config;
        public DocumentUploads(IConfiguration config)
        {
            _config = config;
        }
        public string UploadDocumentAll(IFormFile f)
        {
            string json = string.Empty;
            
            try
            {

                var baseurl = _config["DocumentsUrl:BaseUrl"];
                var url = baseurl + "Login";
                using (WebClient wc = new WebClient())
                {
                    UserLoginModel ApplicationUserModel = new UserLoginModel();
                    ApplicationUserModel.Username = "admin";
                    ApplicationUserModel.Password = "1234";
                    string Json = JsonConvert.SerializeObject(ApplicationUserModel);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    var response = wc.UploadString(url, "POST", Json);
                    if (response != null)
                    {
                        json = UploadFileByServer(f, response);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }

            return json;
        }
        private string UploadFileByServer(IFormFile f,string token)
        {
            var baseurl = _config["DocumentsUrl:BaseUrl"];
            var url = baseurl + "FileRelation/UploadFile";
            using (WebClient wc = new WebClient())
            {
                var fs = f.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);
                var webClient = new WebClient();
                string boundary = "------------------------" + DateTime.Now.Ticks.ToString("x");
                webClient.Headers.Add("Authorization", "bearer "+token);
                webClient.Headers.Add("Content-Type", "multipart/form-data; boundary=" + boundary);
                var fileData = webClient.Encoding.GetString(document);
                var package = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"file\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n{3}\r\n--{0}--\r\n", boundary, f.FileName, f.ContentType, fileData);
                var nfile = webClient.Encoding.GetBytes(package);
                byte[] rawResponse = webClient.UploadData(url, "POST", nfile);
                return System.Text.Encoding.ASCII.GetString(rawResponse);
            }
        }
    }
}
