﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace APP.TaskManagementSystem.Helper.Extension
{

    public static class ExpressionBuilder
    {
        private static readonly MethodInfo containsMethod = typeof(string).GetMethod("Contains");
        private static readonly MethodInfo startsWithMethod =
        typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
        private static readonly MethodInfo endsWithMethod =
        typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });


        public static Expression<Func<T,
        bool>> GetExpression<T>(IList<Filter> filters)
        {
            if (filters.Count == 0)
                return null;

            ParameterExpression param = Expression.Parameter(typeof(T), "t");
            Expression exp = null;

            if (filters.Count == 1)
                exp = GetExpression<T>(param, filters[0]);
            else if (filters.Count == 2)
                exp = GetExpression<T>(param, filters[0], filters[1]);
            else
            {
                while (filters.Count > 0)
                {
                    var f1 = filters[0];
                    var f2 = filters[1];

                    if (exp == null)
                        exp = GetExpression<T>(param, filters[0], filters[1]);
                    else
                        exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0], filters[1]));

                    filters.Remove(f1);
                    filters.Remove(f2);

                    if (filters.Count == 1)
                    {
                        exp = Expression.AndAlso(exp, GetExpression<T>(param, filters[0]));
                        filters.RemoveAt(0);
                    }
                }
            }

            return Expression.Lambda<Func<T, bool>>(exp, param);
        }

        private static Expression GetExpression<T>(ParameterExpression param, Filter filter)
        {
            MemberExpression member = Expression.Property(param, filter.PropertyName);
            ConstantExpression constant = Expression.Constant(filter.Value);

            switch (filter.Operation)
            {
                case Op.EqualsNull:
                    return EqualNull(member, constant);
                case Op.EqualInt32:
                    return Expression.Equal(member, Expression.Convert(constant, typeof(Int32)));
                case Op.EqualNullInt32:
                    return EqualNullInt32(member, constant);
                case Op.Equals:
                    return Expression.Equal(member, constant);

                case Op.GreaterThan:
                    return Expression.GreaterThan(member, constant);

                case Op.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(member, constant);

                case Op.LessThan:
                    return Expression.LessThan(member, constant);

                case Op.LessThanOrEqual:
                    return Expression.LessThanOrEqual(member, constant);

                case Op.Contains:
                    var toLower = Expression.Call(member,
                                  typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                    return Expression.Call(toLower,
                                                containsMethod,
                                                constant);
                //return Expression.Call(member, containsMethod, constant);

                case Op.StartsWith:
                    return Expression.Call(member, startsWithMethod, constant);

                case Op.EndsWith:
                    return Expression.Call(member, endsWithMethod, constant);
            }

            return null;
        }
        private static Expression EqualNull(Expression memberExpression,
                                       ConstantExpression constantToCompare)
        {
            // Other cases removed, for simplicity. This answer only demonstrates
            // how to handle c => c.Weight != 5000f.
            var hasValueExpression = Expression.Property(memberExpression, "HasValue");
            var valueExpression = Expression.Property(memberExpression, "Value");
            var notEqual = Expression.Equal(valueExpression, constantToCompare);
            return Expression.AndAlso(hasValueExpression, notEqual);
        }
        private static Expression EqualInt32(Expression memberExpression,
                                      ConstantExpression constantToCompare)
        {
            var notEqual = Expression.Equal(memberExpression, Expression.Convert(constantToCompare, typeof(Int32)));
            return Expression.AndAlso(memberExpression, notEqual);
        }
        private static Expression EqualNullInt32(Expression memberExpression,
                                     ConstantExpression constantToCompare)
        {
            // Other cases removed, for simplicity. This answer only demonstrates
            // how to handle c => c.Weight != 5000f.
            var hasValueExpression = Expression.Property(memberExpression, "HasValue");
            var valueExpression = Expression.Property(memberExpression, "Value");
            // var notEqual = Expression.Equal(valueExpression, constantToCompare);
            var notEqual = Expression.Equal(valueExpression, Expression.Convert(constantToCompare, typeof(Int32)));
            return Expression.AndAlso(hasValueExpression, notEqual);
        }
        private static BinaryExpression GetExpression<T>
        (ParameterExpression param, Filter filter1, Filter filter2)
        {
            Expression bin1 = GetExpression<T>(param, filter1);
            Expression bin2 = GetExpression<T>(param, filter2);

            return Expression.AndAlso(bin1, bin2);
        }
    }
    public class Filter
    {
        public string PropertyName { get; set; }
        public Op Operation { get; set; }
        public object Value { get; set; }
    }
    public enum Op
    {
        Equals,
        EqualsNull,
        GreaterThan,
        LessThan,
        GreaterThanOrEqual,
        LessThanOrEqual,
        Contains,
        StartsWith,
        EndsWith,
        EqualInt32,
        EqualNullInt32
    }

}
