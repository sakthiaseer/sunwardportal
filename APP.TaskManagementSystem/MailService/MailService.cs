﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem
{
    public class MailService
    {

        IConfiguration _configuration;
        public MailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool EmailService(MailServiceModel value)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();

                string host = _configuration["smtp:server"];
                int port = int.Parse(_configuration["smtp:port"]);
                string emailId = _configuration["smtp:emailId"];
                string fromname = _configuration["smtp:fromName"];
                string password = _configuration["smtp:password"];
                string toEmail = _configuration["smtp:toEmail"];
                string toName = _configuration["smtp:toName"];
                string ccEmail = _configuration["smtp:ccEmail"];
                string bccEmail = _configuration["smtp:bccEmail"];

                smtpClient.EnableSsl = true;
                smtpClient.Host = host;
                smtpClient.Port = port;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtpClient.Credentials = new System.Net.NetworkCredential(emailId, password);

                MailMessage msg = new MailMessage();
                msg.To.Add(new MailAddress(toEmail, toName));
                msg.CC.Add(ccEmail);
                msg.Bcc.Add(bccEmail);
                msg.From = new MailAddress(emailId, fromname);
                msg.Subject = value.Subject;
                msg.Body = value.Body;
                msg.IsBodyHtml = true;
                smtpClient.Send(msg);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SendEmailService(MailServiceModel value)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();

                string host = _configuration["emailConfig:server"];
                int port = int.Parse(_configuration["emailConfig:port"]);
                string emailId = _configuration["emailConfig:emailId"];
                string fromname = _configuration["emailConfig:fromName"];
                string password = _configuration["emailConfig:password"];

                smtpClient.EnableSsl = false;
                smtpClient.Host = host;
                smtpClient.Port = port;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtpClient.Credentials = new System.Net.NetworkCredential(emailId, password);

                MailMessage msg = new MailMessage();
                if (value.ToMail != null && value.ToMail.Count > 0)
                {
                    value.ToMail.ForEach(s =>
                    {
                        msg.To.Add(new MailAddress(s.Email, s.Name));
                    });

                }
                if (value.CcMail != null && value.CcMail.Count > 0)
                {
                    value.CcMail.ForEach(s =>
                    {
                        msg.CC.Add(new MailAddress(s.Email, s.Name));
                    });

                }
                if (value.Attachments != null && value.Attachments.Attachments != null && value.Attachments.Attachments.Count > 0)
                {
                    value.Attachments.Attachments.ForEach(a =>
                    {
                        msg.Attachments.Add(new System.Net.Mail.Attachment(a));
                    });
                }
                if(!string.IsNullOrEmpty(value.FromName))
                {
                    fromname = value.FromName;
                }
                msg.From = new MailAddress(emailId, fromname);
                msg.Subject = value.Subject;
                msg.Body = value.Body;
                msg.IsBodyHtml = true;
                smtpClient.Send(msg);
                /*if (value.Attachments != null && value.Attachments.Attachments != null && value.Attachments.Attachments.Count > 0)
                {
                    value.Attachments.Attachments.ForEach(a =>
                    {
                        System.IO.File.Delete(a);
                    });
                    System.IO.Directory.Delete(value.Attachments.PathName);

                }*/
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    public class MailServiceModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FromName { get; set; }
        public string ToName { get; set; }
        public List<MailHeaderModel> ToMail { get; set; }
        public List<MailHeaderModel> CcMail { get; set; }
        public AttachmentsPathModel Attachments { get; set; }
    }
    public class MailHeaderModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
    public class AttachmentsPathModel
    {
        public List<string> Attachments { get; set; }
        public string PathName { get; set; }
    }
}
