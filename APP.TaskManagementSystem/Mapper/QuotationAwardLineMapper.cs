﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class QuotationAwardLineMapper : Profile
    {
        public QuotationAwardLineMapper()
        {
            CreateMap<QuotationAwardLine, QuotationAwardLineModel>();

        }
    }
}
