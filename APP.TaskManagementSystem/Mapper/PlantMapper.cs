﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PlantMapper :Profile
    {

        public PlantMapper()
        {
            CreateMap<Plant, PlantModel>();

        }
    }
}
