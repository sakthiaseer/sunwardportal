﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class FinsisProductLineMapper:Profile
    {
        public FinsisProductLineMapper()
        {
            CreateMap<FinishProductLine, FinishProductLine>();

        }
    }
}
