﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskMasterMapper : Profile
    {

        public TaskMasterMapper()
        {
            CreateMap<TaskMaster, TaskMasterModel>();

        }
    }
}
