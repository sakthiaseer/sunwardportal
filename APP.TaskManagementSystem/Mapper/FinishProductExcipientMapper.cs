﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class FinishProductExcipientMapper:Profile
    {
        public FinishProductExcipientMapper()
        {
            CreateMap<FinishProductExcipient, FinishProductExcipientModel>();

        }
    }
}
