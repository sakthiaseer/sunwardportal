﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionCapsuleMapper : Profile
    {
        public ProductionCapsuleMapper()
        {
            CreateMap<ProductionCapsule, ProductionCapsuleModel>();

        }
    }
}
