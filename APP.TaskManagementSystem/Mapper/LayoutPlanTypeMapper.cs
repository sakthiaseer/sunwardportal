﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class LayoutPlanTypeMapper : Profile
    {
        public LayoutPlanTypeMapper()
        {
            CreateMap<LayoutPlanType, LayoutPlanTypeModel>();

        }
    }
}
