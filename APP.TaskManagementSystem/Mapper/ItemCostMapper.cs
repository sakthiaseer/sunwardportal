﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class ItemCostMapper : Profile
    {
        public ItemCostMapper()
        {
            CreateMap<ItemCost, ItemCostModel >();
        }
    }
    public class ItemCostLineMapper : Profile
    {
        public ItemCostLineMapper()
        {
            CreateMap<ItemCostLine, ItemCostModel>();
        }
    }
}