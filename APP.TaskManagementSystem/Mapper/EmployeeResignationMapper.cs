﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class EmployeeResignationMapper : Profile
    {
        public EmployeeResignationMapper()
        {
            CreateMap<EmployeeResignation, EmployeeResignationModel>();

        }
    }
}
