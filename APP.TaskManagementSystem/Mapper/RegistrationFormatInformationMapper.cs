﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class RegistrationFormatInformationMapper:Profile
    {
        public RegistrationFormatInformationMapper()
        {
            CreateMap<RegistrationFormatInformation,RegistrationFormatInformationModel>();
        }

    }
}
