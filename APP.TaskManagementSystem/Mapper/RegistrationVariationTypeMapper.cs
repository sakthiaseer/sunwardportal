﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class RegistrationVariationTypeMapper:Profile
    {
        public RegistrationVariationTypeMapper()
        {
            CreateMap<RegistrationVariationType, RegistrationVariationTypeModel>();
        }
    }
}
