﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class UserSettingsMapper : Profile
    {
        public UserSettingsMapper()
        {
            CreateMap<UserSettings, UserSettingsModel>();

        }
    }
}
