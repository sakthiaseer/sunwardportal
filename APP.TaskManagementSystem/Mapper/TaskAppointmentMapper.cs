﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskAppointmentMapper :Profile
    {
        public TaskAppointmentMapper()
        {
            CreateMap<TaskAppointment, TaskAppointmentModel>();

        }
    }
}
