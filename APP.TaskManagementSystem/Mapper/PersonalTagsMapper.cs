﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PersonalTagsMapper : Profile
    {
        public PersonalTagsMapper()
        {
            CreateMap<PersonalTags, PersonalTagsModel>();

        }
    }
}
