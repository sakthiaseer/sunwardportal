﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ItemBatchInfoMapper : Profile
    {
        public ItemBatchInfoMapper()
        {
            CreateMap<ItemBatchInfo, ItemBatchInfoModel>();

        }
    }
}
