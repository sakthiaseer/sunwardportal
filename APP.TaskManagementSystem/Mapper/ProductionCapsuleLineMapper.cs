﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionCapsuleLineMapper : Profile
    {
        public ProductionCapsuleLineMapper()
        {
            CreateMap<ProductionCapsuleLine, ProductionCapsuleLineModel>();

        }
    }
}
