﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class NoticeMapper : Profile
    {
        public NoticeMapper()
        {
            CreateMap<Notice, NoticeModel>();

        }
    }
}
