﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ManufacturingProcessLineMapper:Profile
    {
        public ManufacturingProcessLineMapper()
        {
            CreateMap<ManufacturingProcessLine, ManufacturingProcessLineModel>();
        }
    }
}
