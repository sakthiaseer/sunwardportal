﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMethodTemplateMapper : Profile
    {
        public ProductionMethodTemplateMapper()
        {
            CreateMap<ProductionMethodTemplate, ProductionMethodTemplateModel>();

        }
    }
}
