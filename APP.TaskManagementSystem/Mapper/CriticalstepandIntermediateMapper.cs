﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CriticalstepandIntermediateMapper: Profile
    {
        public CriticalstepandIntermediateMapper()
        {
            CreateMap<CriticalstepandIntermediate, CriticalstepandIntermediateModel>();

        }
    }
}
