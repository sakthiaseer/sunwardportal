﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskProjectMapper : Profile
    {
        public TaskProjectMapper()
        {
            CreateMap<TaskProject, TaskProjectModel>();

        }
    }
}
