﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class FinishProductGeneralInfoMapper :Profile
    {
        public FinishProductGeneralInfoMapper()
        {
            CreateMap<FinishProductGeneralInfo, FinishProductGeneralInfoModel>();

        }
    }
}
