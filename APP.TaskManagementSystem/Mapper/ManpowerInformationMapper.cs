﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class ManpowerInformationMapper : Profile
    {
        public ManpowerInformationMapper()
        {
            CreateMap<ManpowerInformation, ManpowerInformationModel>();

        }
    }
}
