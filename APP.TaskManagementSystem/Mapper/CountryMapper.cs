﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CountryMapper :Profile
    {

        public CountryMapper()
        {
            CreateMap<Country, CountryModel>();

        }
    }
}
