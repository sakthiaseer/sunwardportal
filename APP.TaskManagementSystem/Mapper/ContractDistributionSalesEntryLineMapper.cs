﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ContractDistributionSalesEntryLineMapper : Profile
    {
        public ContractDistributionSalesEntryLineMapper()
        {
            CreateMap<ContractDistributionSalesEntryLine, ContractDistributionSalesEntryLineModel>();

        }
    }
}
