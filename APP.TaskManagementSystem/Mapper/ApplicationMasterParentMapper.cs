﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationMasterParentMapper : Profile
    {
        public ApplicationMasterParentMapper()
        {
            CreateMap<ApplicationMasterParent, ApplicationMasterParentModel>();

        }
    }
}
