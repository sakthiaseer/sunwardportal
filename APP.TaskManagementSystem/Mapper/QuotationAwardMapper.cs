﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class QuotationAwardMapper : Profile
    {
        public QuotationAwardMapper()
        {
            CreateMap<QuotationAward, QuotationAwardModel>();

        }
    }
}
