﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PKGApprovalInformationMapper : Profile
    {
        public PKGApprovalInformationMapper()
        {
            CreateMap<PkgapprovalInformation, PKGApprovalInformationModel>();

        }
    }
}
