﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class NavProductMasterMapper:Profile
    {
        public NavProductMasterMapper()
        {
            CreateMap<NavProductMaster, NavProductMasterModel>();
        }
    }
}
