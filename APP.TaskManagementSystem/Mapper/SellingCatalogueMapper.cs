﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SellingCatalogueMapper : Profile
    {
        public SellingCatalogueMapper()
        {
            CreateMap<SellingCatalogue, SellingCatalogueModel>().ReverseMap();
            
        }
    }

    public class SellingPricingTiersMapper : Profile
    {
        public SellingPricingTiersMapper()
        {
            CreateMap<SellingPricingTiers, SellingPricingTiersModel>().ReverseMap();
             

        }
    }
}
