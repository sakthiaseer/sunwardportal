﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class NAVItemLinksMapper : Profile
    {
        public NAVItemLinksMapper()
        {
            CreateMap<NavitemLinks, NAVItemLinksModel>();

        }
    }
}
