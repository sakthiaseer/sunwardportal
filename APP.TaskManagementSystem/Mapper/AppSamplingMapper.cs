﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class AppSamplingMapper : Profile
    {
        public AppSamplingMapper()
        {
            CreateMap<AppSampling, AppSamplingModel>();

        }
    }

    public class AppSamplingLineMapper : Profile
    {
        public AppSamplingLineMapper()
        {
            CreateMap<AppSamplingLine, AppSamplingLineModel>();

        }
    }
}
