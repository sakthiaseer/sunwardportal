﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class OrderRequirementMapper:Profile
    {
        public  OrderRequirementMapper()
        {
            CreateMap<OrderRequirement, OrderRequirementModel>();
        }
    }
}
