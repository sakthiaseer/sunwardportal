﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMachineMapper : Profile
    {
        public ProductionMachineMapper()
        {
            CreateMap<ProductionMachine, ProductionMachineModel>();

        }
    }
}
