using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ExchangeRateMapper : Profile
    {
        public ExchangeRateMapper()
        {
            CreateMap<ExchangeRate, ExchangeRateModel>();

        }
    }
}
