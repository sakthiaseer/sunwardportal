﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class VendorListMapper : Profile
    {
        public VendorListMapper()
        {
            CreateMap<VendorsList, VendorListModel>();

        }
    }
}
