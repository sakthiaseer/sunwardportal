﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SunwardGroupCompanyMapper : Profile
    {
        public SunwardGroupCompanyMapper()
        {
            CreateMap<SunwardGroupCompany, SunwardGroupCompanyModel>();
        }
    }
}
