﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionBatchInformationMapper : Profile
    {
        public ProductionBatchInformationMapper()
        {
            CreateMap<ProductionBatchInformation, ProductionBatchInformationModel>();

        }
    }
}
