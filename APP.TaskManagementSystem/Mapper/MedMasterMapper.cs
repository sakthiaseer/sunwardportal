﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class MedMasterMapper : Profile
    {
        public MedMasterMapper()
        {
            CreateMap<MedMaster, MedMasterModel>();

        }
    }
}
