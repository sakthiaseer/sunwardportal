﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem
{
    public class SalesOrderMapper : Profile
    {
        public SalesOrderMapper()
        {
            CreateMap<SalesOrder, SalesOrderModel>();

        }
    }
    public class SalesOrderProductMapper : Profile
    {
        public SalesOrderProductMapper()
        {
            CreateMap<SalesOrderProduct, SalesOrderProductModel>();

        }
    }

    public class SowithOutBlanketOrderMapper : Profile
    {
        public SowithOutBlanketOrderMapper()
        {
            CreateMap<SowithOutBlanketOrder, SOWithoutBlanketOrderModel>();

        }
    }

    public class SOLotInformationMapper : Profile
    {
        public SOLotInformationMapper()
        {
            CreateMap<SolotInformation, SOLotInformationModel>();

        }
    }
}
