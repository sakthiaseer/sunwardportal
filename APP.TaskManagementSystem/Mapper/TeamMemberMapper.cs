﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TeamMemberMapper : Profile
    {
        public TeamMemberMapper()
        {
            CreateMap<TeamMember, TeamMemberModel>();

        }
    }
}
