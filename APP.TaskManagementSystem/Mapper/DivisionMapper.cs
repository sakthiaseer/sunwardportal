﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class DivisionMapper :Profile
    {
        public DivisionMapper()
        {
            CreateMap<Division, DivisionModel>();

        }
    }
}
