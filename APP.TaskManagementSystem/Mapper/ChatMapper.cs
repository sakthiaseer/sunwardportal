﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class ChatMapper
    {
    }

    public class ChatMessageMapper : Profile
    {
        public ChatMessageMapper()
        {
            CreateMap<ChatMessage, ChatMessageModel>();
        }
    }

    public class ChatUserMapper : Profile
    {
        public ChatUserMapper()
        {
            CreateMap<ChatUser, ChatUserModel>();
        }
    }
}
