﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem
{
    public class ProductGroupingMapper : Profile
    {
        public ProductGroupingMapper()
        {
            CreateMap<ProductGrouping, ProductGroupingModel>();

        }
    }

    public class ProductGroupingManufactureMapper : Profile
    {
        public ProductGroupingManufactureMapper()
        {
            CreateMap<ProductGroupingManufacture, ProductGroupingManufactureModel>();

        }
    }

    public class ProductGroupingNavMapper : Profile
    {
        public ProductGroupingNavMapper()
        {
            CreateMap<ProductGroupingNav, ProductGroupingNavModel>();

        }
    }

    public class ProductGroupingNavDifferenceMapper : Profile
    {
        public ProductGroupingNavDifferenceMapper()
        {
            CreateMap<ProductGroupingNavDifference, ProductGroupingNavDifferenceModel>();

        }
    }
}
