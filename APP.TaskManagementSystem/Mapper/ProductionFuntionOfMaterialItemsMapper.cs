﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionFuntionOfMaterialItemsMapper:Profile
    {
        public ProductionFuntionOfMaterialItemsMapper()
        {
            CreateMap<ProductionFuntionOfMaterialItems, ProductionFuntionOfMaterialItemsModel>();
        }
    }
}
