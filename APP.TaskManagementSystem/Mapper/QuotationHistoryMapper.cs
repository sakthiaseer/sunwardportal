﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class QuotationHistoryMapper : Profile
    {
        public QuotationHistoryMapper()
        {
            CreateMap<QuotationHistory, QuotationHistoryModel>();

        }
    }

    public class QuotationHistoryLineMapper : Profile
    {
        public QuotationHistoryLineMapper()
        {
            CreateMap<QuotationHistoryLine, QuotationHistoryLineModel>();

        }
    }
}
