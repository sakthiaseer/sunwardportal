using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class UnitConversionMapper :Profile
    {

        public UnitConversionMapper()
        {
            CreateMap<UnitConversion, UnitConversionModel>();

        }
    }
}
