﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TaskCommentMapper : Profile
    {
        public TaskCommentMapper()
        {
            CreateMap<TaskComment, TaskCommentModel>();

        }
    }
}
