﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskAttachmentMapper : Profile
    {
        public TaskAttachmentMapper()
        {
            CreateMap<TaskAttachment, TaskAttachmentModel>();

        }
    }
}
