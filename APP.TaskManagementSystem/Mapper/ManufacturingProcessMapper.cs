﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ManufacturingProcessMapper:Profile
    {
        public ManufacturingProcessMapper()
        {
            CreateMap<ManufacturingProcess, ManufacturingProcessModel>();
        }
    }
}
