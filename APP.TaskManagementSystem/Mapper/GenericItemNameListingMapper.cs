﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class GenericItemNameListingMapper:Profile
    {
        public GenericItemNameListingMapper()
        {
            CreateMap<GenericItemNameListing, GenericItemNameListingModel>();
        }
    }
}
