﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionColorMapper : Profile
    {
        public ProductionColorMapper()
        {
            CreateMap<ProductionColor, ProductionColorModel>();

        }
    }
}
