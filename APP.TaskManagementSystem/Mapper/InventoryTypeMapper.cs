﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class InventoryTypeMapper : Profile
    {
        public InventoryTypeMapper()
        {
            CreateMap<InventoryType, InventoryTypeModel>();

        }
    }
}
