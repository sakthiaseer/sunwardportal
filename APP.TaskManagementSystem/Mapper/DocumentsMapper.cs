﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class DocumentsMapper :Profile
    {
        public DocumentsMapper()
        {
            CreateMap<Documents, DocumentsModel>();

        }
    }
}
