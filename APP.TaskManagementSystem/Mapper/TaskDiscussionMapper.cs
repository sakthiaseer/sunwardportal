﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class TaskDiscussionMapper : Profile
    {
        public TaskDiscussionMapper()
        {
            CreateMap<TaskComment, TaskCommentModel>();

        }
    }
}
