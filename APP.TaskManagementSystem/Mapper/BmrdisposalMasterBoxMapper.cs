﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class BmrdisposalMasterBoxMapper : Profile
    {
        public BmrdisposalMasterBoxMapper()
        {
            CreateMap<BmrdisposalMasterBox, BmrdisposalMasterBoxModel>();

        }
    }
}
