﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SalesBorrowHeaderMapper : Profile
    {
        public SalesBorrowHeaderMapper()
        {
            CreateMap<SalesBorrowHeader, SalesBorrowHeaderModel>();

        }
    }
}
