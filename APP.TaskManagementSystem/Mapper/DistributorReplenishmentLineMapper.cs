﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class DistributorReplenishmentLineMapper : Profile
    {
        public DistributorReplenishmentLineMapper()
        {
            CreateMap<DistributorReplenishmentLine, DistributorReplenishmentLineModel>();

        }
    }
}
