﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class ProcessTransferMapper : Profile
    {
        public ProcessTransferMapper()
        {
            CreateMap<ProcessTransfer, ProcessTransferModel>();
        }
    }
}
