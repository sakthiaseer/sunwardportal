﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMaterialLineMapper:Profile
    {
        public ProductionMaterialLineMapper()
        {
            CreateMap<ProductionMaterialLine, ProductionMaterialLineModel>();
        }
    }
}
