﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class FPProductLineMapper : Profile
    {
        public FPProductLineMapper()
        {
            CreateMap<FpproductLine, FPProductLineModel>();

        }
    }
}
