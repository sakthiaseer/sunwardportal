﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMaterialLineClassificationMapper:Profile
    {
        public ProductionMaterialLineClassificationMapper()
        {
            CreateMap<ProductionMaterialLineClassification, ProductionMaterialLineClassificationModel>();
        }
    }
}
