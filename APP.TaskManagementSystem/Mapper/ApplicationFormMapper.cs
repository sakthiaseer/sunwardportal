﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationFormMapper:Profile
    {
        public ApplicationFormMapper()
        {
            CreateMap<ApplicationForm, ApplicationFormModel>();
        }
    }
}
