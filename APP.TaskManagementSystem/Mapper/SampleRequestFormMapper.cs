﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem
{
    public class SampleRequestFormMapper : Profile
    {
        public SampleRequestFormMapper()
        {
            CreateMap<SampleRequestForm, SampleRequestFormModel>();
        }
    }
}