﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SellingPriceInformationMapper : Profile
    {
        public SellingPriceInformationMapper()
        {
            CreateMap<SellingPriceInformation, SellingPriceInformationModel>().ReverseMap();
         
        }
    }
}
