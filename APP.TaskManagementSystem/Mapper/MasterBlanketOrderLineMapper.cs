﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class MasterBlanketOrderLineMapper : Profile
    {
        public MasterBlanketOrderLineMapper()
        {
            CreateMap<MasterBlanketOrderLine, MasterBlanketOrderLineModel>();

        }
    }
}
