﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CommonPackagingCapsealMapper:Profile
    {
        public CommonPackagingCapsealMapper()
        {
            CreateMap<CommonPackagingCapseal, CommonPackagingCapsealModel>();
        }
    }
}
