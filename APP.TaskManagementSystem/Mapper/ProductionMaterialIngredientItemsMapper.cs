﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMaterialIngredientItemsMapper:Profile
    {
        public ProductionMaterialIngredientItemsMapper()
        {
            CreateMap<ProductionMaterialIngredientItems, ProductionMaterialIngredientItemsModel>();
        }
    }
}
