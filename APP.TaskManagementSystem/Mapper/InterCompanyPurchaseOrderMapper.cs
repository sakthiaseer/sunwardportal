using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class InterCompanyPurchaseOrderMapper : Profile
    {
        public InterCompanyPurchaseOrderMapper()
        {
            CreateMap<InterCompanyPurchaseOrder, InterCompanyPurchaseOrderModel>();
        }
    }

    public class InterCompanyPurchaseOrderLineMapper : Profile
    {
        public InterCompanyPurchaseOrderLineMapper()
        {
            CreateMap<InterCompanyPurchaseOrderLine, InterCompanyPurchaseOrderLineModel>();
        }
    }
}