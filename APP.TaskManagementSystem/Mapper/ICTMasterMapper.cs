﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class ICTMasterMapper : Profile
    {
        public ICTMasterMapper()
        {
            CreateMap<Ictmaster, ICTMasterModel>();

        }
    }
}
