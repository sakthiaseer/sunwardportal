﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TenderPeriodPricingLineMapper : Profile
    {
        public TenderPeriodPricingLineMapper()
        {
            CreateMap<TenderPeriodPricingLine, TenderPeriodPricingLineModel>();
        }
    }
}
