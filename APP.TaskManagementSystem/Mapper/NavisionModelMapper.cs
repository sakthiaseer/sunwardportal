﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class NavisionModelMapper
    {

    }

    public class NavCustomerMapper : Profile
    {
        public NavCustomerMapper()
        {
            CreateMap<Navcustomer, NavCustomerModel>();
        }
    }

    public class NavItemMapper : Profile
    {
        public NavItemMapper()
        {
            CreateMap<Navitems, NavItemModel>();
        }
    }

    public class NavInpMapper : Profile
    {
        public NavInpMapper()
        {
            CreateMap<Navinpreport, INPModel>();
        }
    }

    public class NavInpitemMapper : Profile
    {
        public NavInpitemMapper()
        {
            CreateMap<Navinpreport, INPItemModel>();
        }
    }

    public class NavParInfoMapper : Profile
    {
        public NavParInfoMapper()
        {
            CreateMap<Navinpreport, ParInfoModel>();
        }
    }

    public class NavPlannerCommentMapper : Profile
    {
        public NavPlannerCommentMapper()
        {
            CreateMap<Navinpreport, PlannerCommentModel>();
        }
    }
}
