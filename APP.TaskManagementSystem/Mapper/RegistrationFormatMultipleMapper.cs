﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class RegistrationFormatMultipleMapper:Profile
    {
        public RegistrationFormatMultipleMapper()
        {
            CreateMap<RegistrationFormatMultiple,RegistrationFormatMultipleModel>();
        }
    }
}
