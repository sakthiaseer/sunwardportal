﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class DeviceGroupListMapper : Profile
    {
        public DeviceGroupListMapper()
        {
            CreateMap<DeviceGroupList, DeviceGroupListModel>();

        }
    }
}
