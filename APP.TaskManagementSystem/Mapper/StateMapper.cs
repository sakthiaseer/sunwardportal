﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class StateMapper :Profile
    {

        public StateMapper()
        {
            CreateMap<State, StateModel>();

        }
    }
}
