﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class CompanyHolidaysMapper :Profile
    {
        public CompanyHolidaysMapper()
        {
            CreateMap<CompanyHolidays, CompanyHolidaysModel>();

        }
    }
}
