﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class IPIRMapper : Profile
    {
        public IPIRMapper()
        {
            CreateMap<Ipir, IPIRModel>();
        }
    }

    public class IPIRLineMapper : Profile
    {
        public IPIRLineMapper()
        {
            CreateMap<Ipirline, IPIRLineModel>();
        }
    }
}