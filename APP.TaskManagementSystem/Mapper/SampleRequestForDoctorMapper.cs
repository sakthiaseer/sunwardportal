﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class SampleRequestForDoctorMapper : Profile
    {
        public SampleRequestForDoctorMapper()
        {
            CreateMap<SampleRequestForDoctor, SampleRequestForDoctorModel>();

        }
    }
}
