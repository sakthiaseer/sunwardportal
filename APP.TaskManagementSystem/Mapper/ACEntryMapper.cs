﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ACEntryMapper : Profile
    {
        public ACEntryMapper()
        {
            CreateMap<Acentry, ACEntryModel>();

        }
    }
}
