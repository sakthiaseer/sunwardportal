﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class MasterBlanketOrderMapper : Profile
    {
        public MasterBlanketOrderMapper()
        {
            CreateMap<MasterBlanketOrder, MasterBlanketOrderModel>();

        }
    }
}
