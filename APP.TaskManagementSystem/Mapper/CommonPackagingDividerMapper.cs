﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class CommonPackagingDividerMapper:Profile
    {
        public CommonPackagingDividerMapper()
        {
            CreateMap<CommonPackagingDivider, CommonPackagingDividerModel>();
        }
    }
}
