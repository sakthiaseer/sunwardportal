﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class BlisterScrapCodeMapper :Profile
    {
        public BlisterScrapCodeMapper()
        {
            CreateMap<BlisterScrapCode, BlisterScrapCodeModel>();

        }
    }
}
