﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionActionMapper :Profile
    {
        public ProductionActionMapper()
        {
            CreateMap<ProductionAction, ProductionActionModel>();

        }
    }
}
