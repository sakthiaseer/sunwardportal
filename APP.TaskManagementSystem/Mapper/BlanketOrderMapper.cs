﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class BlanketOrderMapper:Profile
    {
        public BlanketOrderMapper()
        {
            CreateMap<BlanketOrder, BlanketOrderModel>();

        }
    }
}
