using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionBasicCycleMapper : Profile
    {
        public ProductionBasicCycleMapper()
        {

            CreateMap<ProductionCycle, ProductionCycleModel>();

        }
    }
}