﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ItemSalesPriceMapper : Profile
    {
        public ItemSalesPriceMapper()
        {
            CreateMap<ItemSalesPrice, ItemSalesPriceModel>();

        }
    }
}
