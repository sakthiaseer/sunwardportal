﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class FPCommonFieldMapper : Profile
    {
        public FPCommonFieldMapper()
        {
            CreateMap<FpcommonField, FPCommonFieldModel>();

        }
    }
}
