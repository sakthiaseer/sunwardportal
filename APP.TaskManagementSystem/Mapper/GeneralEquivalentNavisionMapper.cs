﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class GeneralEquivalentNavisionMapper : Profile
    {
        public GeneralEquivalentNavisionMapper()
        {
            CreateMap<GeneralEquivalaentNavision, GeneralEquivalentNavisionModel>();
        }
    }
}
