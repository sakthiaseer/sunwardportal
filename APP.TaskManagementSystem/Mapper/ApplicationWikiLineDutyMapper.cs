﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationWikiLineDutyMapper:Profile
    {
        public ApplicationWikiLineDutyMapper()
        {
            CreateMap<ApplicationWikiLineDuty, ApplicationWikiLineDutyModel>();
        }
    }
}
