﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationWikiLineMapper:Profile
    {
        public ApplicationWikiLineMapper()
        {
            CreateMap<ApplicationWikiLine, ApplicationWikiLineModel>();
        }
    }
}
