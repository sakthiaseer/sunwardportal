﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class InventoryTypeOpeningStockBalanceMapper : Profile
    {
        public InventoryTypeOpeningStockBalanceMapper()
        {
            CreateMap<InventoryTypeOpeningStockBalance, InventoryTypeOpeningStockBalanceModel>();

        }
    }
}
