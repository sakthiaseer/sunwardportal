﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TaskNotesMapper : Profile
    {
        public TaskNotesMapper()
        {
            CreateMap<TaskNotes, TaskNotesModel>();

        }
    }
}
