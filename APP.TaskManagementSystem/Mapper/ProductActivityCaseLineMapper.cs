﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductActivityCaseLineMapper : Profile
    {
        public ProductActivityCaseLineMapper()
        {
            CreateMap<ProductActivityCaseLine, ProductActivityCaseLineModel>();

        }
    }
}
