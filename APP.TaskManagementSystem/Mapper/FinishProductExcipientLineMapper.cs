﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class FinishProductExcipientLineMapper:Profile
    {
        public FinishProductExcipientLineMapper()
        {
            CreateMap<FinishProductExcipientLine, FinishProductExcipientLineModel>();
        }
    }
}
