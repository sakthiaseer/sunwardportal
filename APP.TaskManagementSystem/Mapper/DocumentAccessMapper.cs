﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class DocumentAccessMapper : Profile
    {

        public DocumentAccessMapper()
        {
            CreateMap<DocumentAccess, DocumentAccessModel>();

        }
    }
}
