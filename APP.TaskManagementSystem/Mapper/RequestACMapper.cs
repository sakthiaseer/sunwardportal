﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class RequestACMapper : Profile
    {
        public RequestACMapper()
        {
            CreateMap<RequestAc, RequestACModel>();

        }
    }

    public class RequestACLineMapper : Profile
    {
        public RequestACLineMapper()
        {
            CreateMap<RequestAcline, RequestACLineModel>();

        }
    }
}
