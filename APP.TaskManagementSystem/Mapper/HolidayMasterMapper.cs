﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class HolidayMasterMapper :Profile
    {
        public HolidayMasterMapper()
        {
            CreateMap<HolidayMaster, HolidayMasterModel>();

        }
    }
}
