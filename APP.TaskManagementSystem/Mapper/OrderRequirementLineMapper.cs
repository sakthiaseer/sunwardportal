﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class OrderRequirementLineMapper : Profile
    {
        public OrderRequirementLineMapper()
        {
            CreateMap<OrderRequirementLine, OrderRequirementLineModel>();
        }
    }
}
