﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TagMasterMapper :Profile
    {
        public TagMasterMapper()
        {
            CreateMap<TagMaster, TagMasterModel>();

        }
    }
}
