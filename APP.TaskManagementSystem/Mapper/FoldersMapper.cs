﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class FoldersMapper:Profile
    {
        public FoldersMapper()
        {
            CreateMap<Folders, FoldersModel>();

        }
    }
}
