﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class AppTranStockMapper : Profile
    {
        public AppTranStockMapper()
        {
            CreateMap<AppTranStock, AppTranStockModel>();

        }
    }

    public class AppTranStockLineMapper : Profile
    {
        public AppTranStockLineMapper()
        {
            CreateMap<AppTranStockLine, AppTranStockLineModel>();

        }
    }
}
