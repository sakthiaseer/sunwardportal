﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskAssignedMapper : Profile
    {
        public TaskAssignedMapper()
        {
            CreateMap<TaskAssigned, TaskAssignedModel>();

        }
    }
}
