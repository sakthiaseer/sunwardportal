﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TaskMembersMapper :Profile
    {
        public TaskMembersMapper()
        {
            CreateMap<TaskMembers, TaskMembersModel>();

        }
    }
}
