﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TransferBalanceQtyMapper:Profile
    {
        public TransferBalanceQtyMapper()
        {
            CreateMap<TransferBalanceQty, TransferBalanceQtyModel>();
        }
    }
}
