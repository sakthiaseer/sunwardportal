﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TaskLikesMapper : Profile
    {

        public TaskLikesMapper()
        {
            CreateMap<TaskLikes, TaskLikesModel>();

        }
    }
}
