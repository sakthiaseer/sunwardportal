﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TenderInformationMapper : Profile
    {
        public TenderInformationMapper()
        {
            CreateMap<TenderInformation, TenderInformationModel>();
        }
    }
}
