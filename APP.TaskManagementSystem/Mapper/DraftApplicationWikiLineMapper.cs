﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class DraftApplicationWikiLineMapper : Profile
    {
        public DraftApplicationWikiLineMapper()
        {
            CreateMap<DraftApplicationWikiLine, DraftApplicationWikiLineModel>();
        }
    }
}
