﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TenderPeriodPricingMapper:Profile
    {
        public TenderPeriodPricingMapper()
        {
            CreateMap<TenderPeriodPricing, TenderPeriodPricingModel>();
        }
    }
}
