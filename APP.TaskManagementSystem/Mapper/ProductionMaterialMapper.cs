﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionMaterialMapper:Profile
    {
        public ProductionMaterialMapper()
        {
            CreateMap<ProductionMaterial, ProductionMaterialModel>();
        }
    }
}
