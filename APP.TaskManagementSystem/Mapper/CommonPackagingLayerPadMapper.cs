﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CommonPackagingLayerPadMapper:Profile
    {
        public CommonPackagingLayerPadMapper()
        {
            CreateMap<CommonPackagingLayerPad, CommonPackagingLayerPadModel>();
        }
    }
}
