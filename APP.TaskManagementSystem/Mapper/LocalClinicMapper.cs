﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class LocalClinicMapper:Profile
    {
        public LocalClinicMapper()
        {
            CreateMap<LocalClinic, LocalClinicModel>();

        }
    }
}
