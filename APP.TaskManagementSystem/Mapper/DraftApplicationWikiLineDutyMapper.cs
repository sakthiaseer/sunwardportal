﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class DraftApplicationWikiLineDutyMapper:Profile
    {
        public DraftApplicationWikiLineDutyMapper()
        {
            CreateMap<DraftApplicationWikiLineDuty, DraftApplicationWikiLineDutyModel>();
        }
    }
}
