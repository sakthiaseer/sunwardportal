﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CriticalstepandIntermediateLineMapper : Profile
    {
        public CriticalstepandIntermediateLineMapper()
        {
            CreateMap<CriticalstepandIntermediateLine, CriticalstepandIntermediateLineModel>();

        }
    }
}
