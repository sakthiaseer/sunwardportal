﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PackageUnitBoxMapper : Profile
    {
        public PackageUnitBoxMapper()
        {
            CreateMap<PackagingUnitBox, PackagingUnitBoxModel>();

        }
    }
}
