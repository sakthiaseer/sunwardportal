﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class DistributorReplenishmentMapper: Profile
    {
        public DistributorReplenishmentMapper()
        {
            CreateMap<DistributorReplenishment, DistributorReplenishmentModel>();

        }
    }
}
