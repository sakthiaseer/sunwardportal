﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class OrderRequirementLineSplitMapper:Profile
    {
        public OrderRequirementLineSplitMapper()
        {
            CreateMap<OrderRequirementLineSplit, OrderRequirementLineSplitModel>();
        }
    }
}
