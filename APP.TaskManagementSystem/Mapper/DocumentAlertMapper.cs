﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class DocumentAlertMapper : Profile
    {
        public DocumentAlertMapper()
        {
            CreateMap<DocumentAlert, DocumentAlertModel>();

        }
    }
}
