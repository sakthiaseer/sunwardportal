﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class FPProductMapper : Profile
    {
        public FPProductMapper()
        {
            CreateMap<Fpproduct, FPProductModel>();

        }
    }
}
