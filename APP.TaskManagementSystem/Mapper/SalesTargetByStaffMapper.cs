﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SalesTargetByStaffMapper : Profile
    {
        public SalesTargetByStaffMapper()
        {
            CreateMap<SalesTargetByStaff, SalesTargetByStaffModel>();

        }
    }
}
