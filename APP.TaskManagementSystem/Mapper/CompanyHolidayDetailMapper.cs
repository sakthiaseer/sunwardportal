﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class CompanyHolidayDetailMapper :Profile
    {
        public CompanyHolidayDetailMapper()
        {
            CreateMap<CompanyHolidayDetail, CompanyHolidayDetailModel>();

        }
    }
}
