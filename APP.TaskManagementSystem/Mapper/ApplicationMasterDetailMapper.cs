﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationMasterDetailMapper : Profile
    {
        public ApplicationMasterDetailMapper()
        {
            CreateMap<ApplicationMasterDetail, ApplicationMasterDetailModel>();

        }
    }
}
