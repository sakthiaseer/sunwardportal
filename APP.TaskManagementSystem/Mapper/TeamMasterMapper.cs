﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TeamMasterMapper : Profile
    {
        public TeamMasterMapper()
        {
            CreateMap<TeamMaster, TeamMasterModel>();

        }
    }
}
