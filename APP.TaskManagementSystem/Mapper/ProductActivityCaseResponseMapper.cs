﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductActivityCaseResponseMapper : Profile
    {
        public ProductActivityCaseResponseMapper()
        {
            CreateMap<ProductActivityCaseRespons, ProductActivityCaseResponsModel>();

        }
    }
}
