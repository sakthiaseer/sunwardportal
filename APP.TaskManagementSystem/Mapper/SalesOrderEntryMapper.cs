﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SalesOrderEntryMapper : Profile
    {
        public SalesOrderEntryMapper()
        {
            CreateMap<SalesOrderEntry, SalesOrderEntryModel>();

        }
    }
}
