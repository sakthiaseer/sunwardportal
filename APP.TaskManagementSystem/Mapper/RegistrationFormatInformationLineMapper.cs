﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class RegistrationFormatInformationLineMapper:Profile
    {
        public RegistrationFormatInformationLineMapper()
        {
            CreateMap<RegistrationFormatInformationLine,RegistrationFormatInformationLineModel>();
        }
    }
}
