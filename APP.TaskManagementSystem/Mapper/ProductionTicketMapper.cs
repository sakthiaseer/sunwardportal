﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionTicketMapper : Profile
    {
        public ProductionTicketMapper()
        {
            CreateMap<ProductionTicket, ProductionTicketModel>();

        }
    }
}
