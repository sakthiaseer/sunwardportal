﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class EmployeeAdditionalInformationMapper: Profile
    {
        public EmployeeAdditionalInformationMapper()
        {
            CreateMap<EmployeeAdditionalInformation,EmployeeAdditionalInformationModel>();

        }
    }

}
