﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class FolderDiscussionMapper : Profile
    {
        public FolderDiscussionMapper()
        {
            CreateMap<FolderDiscussion, FolderDiscussionModel>();

        }
    }
}
