﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class TaskTagMapper :Profile
    {

        public TaskTagMapper()
        {
            CreateMap<TaskTag, TaskTagModel>();

        }
    }
}
