﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class AppIPIREntryMapper: Profile
    {
        public AppIPIREntryMapper()
        {
            CreateMap<AppIpirentry, AppIPIREntryModel>();

        }
    }
}
