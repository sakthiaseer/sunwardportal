﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class TaskMeetingNoteMapper : Profile
    {
        public TaskMeetingNoteMapper()
        {
            CreateMap<TaskMeetingNote, TaskMeetingNoteModel>();

        }
    }
}
