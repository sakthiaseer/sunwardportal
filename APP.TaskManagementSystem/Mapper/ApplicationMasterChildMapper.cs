﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ApplicationMasterChildMapper : Profile
    {
        public ApplicationMasterChildMapper()
        {
            CreateMap<ApplicationMasterChild, ApplicationMasterChildModel>();

        }
    }
}
