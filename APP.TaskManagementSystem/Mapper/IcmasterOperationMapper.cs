﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class IcmasterOperationMapper:Profile
    {
        public IcmasterOperationMapper()
        {
            CreateMap<IcmasterOperation, IcmasterOperationModel>();

        }
    }
}
