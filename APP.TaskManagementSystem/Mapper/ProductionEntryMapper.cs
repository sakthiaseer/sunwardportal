﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionEntryMapper : Profile
    {
        public ProductionEntryMapper()
        {
            CreateMap<ProductionEntry, ProductionEntryModel>();

        }
    }
}
