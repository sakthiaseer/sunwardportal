using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SalesOrderNovateMapper : Profile
    {
        public SalesOrderNovateMapper()
        {
            CreateMap<SalesAdhocNovate, SalesAdhocNovateModel>();

        }
    }
}
