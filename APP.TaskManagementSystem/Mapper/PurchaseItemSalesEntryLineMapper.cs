﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PurchaseItemSalesEntryLineMapper : Profile
    {
        public PurchaseItemSalesEntryLineMapper()
        {
            CreateMap<PurchaseItemSalesEntryLine, PurchaseItemSalesEntryLineModel>();

        }
    }
}
