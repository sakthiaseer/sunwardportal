﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class UserGroupMapper : Profile
    {
        public UserGroupMapper()
        {
            CreateMap<UserGroup, UserGroupModel>();

        }
    }
}
