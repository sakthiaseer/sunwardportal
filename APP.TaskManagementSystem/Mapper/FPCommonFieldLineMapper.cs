﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;


namespace APP.TaskManagementSystem.Mapper
{
    public class FPCommonFieldLineMapper :Profile
    {
        public FPCommonFieldLineMapper()
        {
            CreateMap<FpcommonFieldLine, FPCommonFieldLineModel>();

        }
    }
}
