﻿using System;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductionBatchInformationLineMapper : Profile
    {
        public ProductionBatchInformationLineMapper()
        {
            CreateMap<ProductionBatchInformationLine, ProductionBatchInformationLineModel>();

        }
    }
}
