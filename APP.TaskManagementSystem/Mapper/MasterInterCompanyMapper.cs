﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Mapper
{
    public class MasterInterCompanyMapper : Profile
    {
        public MasterInterCompanyMapper()
        {
            CreateMap<MasterInterCompanyPricing, MasterInterCompanyPricingModel>();
        }
    }

    public class MasterInterCompanyLineMapper : Profile
    {
        public MasterInterCompanyLineMapper()
        {
            CreateMap<MasterInterCompanyPricingLine, MasterInterCompanyPricingLineModel>();
        }
    }
}
