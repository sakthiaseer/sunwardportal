﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class SubSectionTwoMapper: Profile
    {
        public SubSectionTwoMapper()
        {
            CreateMap<SubSectionTwo, SubSectionTwoModel>();

        }
    }
}
