﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class ProductGroupSurveyMapper : Profile
    {
        public ProductGroupSurveyMapper()
        {
            CreateMap<ProductGroupSurvey, ProductGroupSurveyModel>();
        }
    }
}
