﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class PlasticBagMapper :Profile
    {
        public PlasticBagMapper()
        {
            CreateMap<PlasticBag, PlasticBagModel>();

        }
    }
}
