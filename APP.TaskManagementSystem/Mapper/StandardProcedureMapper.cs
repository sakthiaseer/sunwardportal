﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class StandardProcedureMapper : Profile
    {
        public StandardProcedureMapper()
        {
            CreateMap<StandardProcedure, StandardProcedureModel>();

        }
    }
}
