﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

namespace APP.TaskManagementSystem.Mapper
{
    public class AdditionalProcessMapper : Profile
    {
        public AdditionalProcessMapper()
        {
            CreateMap<AdditionalProcess, AdditionalProcessModel>();

        }
    }
}
