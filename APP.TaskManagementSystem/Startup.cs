﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using APP.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Logging;
using APP.TaskManagementSystem.Helper.Extension;
using APP.TaskManagementSystem.Helper;
using System;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using Microsoft.AspNetCore.Mvc;
using APP.TaskManagementSystem.Hubs;
using APP.BussinessObject;
using Hangfire;
using Hangfire.MemoryStorage;
using APP.TaskManagementSystem.Controllers;
using Microsoft.AspNetCore.Http.Features;
using System.Net.Sockets;
using System.Net;

namespace APP.TaskManagementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        static string LocalIPAddress()
        {
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint? endPoint = socket.LocalEndPoint as IPEndPoint;
                if (endPoint != null)
                {
                    return endPoint.Address.ToString();
                }
                else
                {
                    return "127.0.0.1";
                }
            }
        }

        public IConfiguration Configuration { get; }
        string localIP = LocalIPAddress();
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers().AddNewtonsoftJson(
                options =>
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            services.AddDbContext<CRT_TMSContext>(options =>
            {
                options.EnableSensitiveDataLogging(true);
                options.UseSqlServer(Configuration.GetConnectionString("TaskDatabase"), sqlServerOptions => sqlServerOptions.CommandTimeout((int)TimeSpan.FromMinutes(3).TotalSeconds));
            });

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder.AllowAnyOrigin()
            //        .AllowAnyMethod()
            //        .AllowAnyHeader().AllowCredentials());
            //});
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                      builder =>
                      {
                          builder.SetIsOriginAllowed(_ => true)
                          .AllowAnyMethod()
                          .AllowAnyHeader()
                          .AllowCredentials();

                      });
            });
            services.Configure<ApiBehaviorOptions>(opt =>
            {
                opt.SuppressModelStateInvalidFilter = true;
            });
            //adding validation model filter
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidateModelAttribute));
            });
            //Compression Gzip
#if (!DEBUG)
            services.AddCompression();
#endif

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Fastest);
            //    services.AddResponseCompression(options =>
            //    {
            //        options.MimeTypes = new[]
            //        {
            //    // Default
            //    "text/plain",
            //    "text/css",
            //    "application/javascript",
            //    "text/html",
            //    "application/xml",
            //    "text/xml",
            //    "application/json",
            //    "text/json",
            //    // Custom
            //    "image/svg+xml"
            //};
            //    });
            services.AddResponseCompression();


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();



            // configure jwt authentication 
            var key = Encoding.ASCII.GetBytes(Configuration["Jwt:SigningKey"]);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;

                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration["Jwt:Site"],
                    ValidIssuer = Configuration["Jwt:Site"],
                    ValidateLifetime = true,
                };
            });


            //to host in IIS
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddSignalR(hubConnections =>
            {
                hubConnections.EnableDetailedErrors = true;
                hubConnections.KeepAliveInterval = TimeSpan.FromSeconds(3);
            });
            // services.AddCompression();
            services.AddScoped<GenerateDocumentNoSeries>();
            services.AddScoped<TableVersionRepository>();
            services.AddScoped<ApprovalService>();
            services.AddScoped<NotificationService>();
            services.AddScoped<NotificationService>();
            services.AddScoped<PagedListBO>();
            services.AddTransient<MailService>();
            //hangfire 
            services.AddHangfire(config =>
                config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseDefaultTypeSerializer()
                .UseMemoryStorage());
            services.AddHangfireServer();
            services.AddScoped<NAVFuncController>();

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger,
            IBackgroundJobClient backgroundJobClient,
            IRecurringJobManager recurringJobManager, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Enable compression
#if (!DEBUG)
            app.UseCompression();
#endif

            app.UseResponseCompression();


            app.UseCors("AllowAllHeaders");
           // app.UseHttpsRedirection();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMiddleware<MaintainCorsHeadersMiddleware>();
            app.ConfigureExceptionHandler(logger);
            //app.UseRequestResponseLogging();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseCompression();

            //hangfire 
            app.UseHangfireDashboard("/dashboard");
            //backgroundJobClient.Enqueue(() => Console.WriteLine("Hello Hangfire job!"));

            //recurringJobManager.AddOrUpdate(
            //    "Run every minute",
            //    () => Console.WriteLine("Hello Hangfire recurring job!"),
            //    Cron.Minutely
            //    );
            //recurringJobManager.AddOrUpdate(
            //    "Get BMR Sync",
            //    () => serviceProvider.GetService<NAVFuncController>().GetBMRTickets("NAV_SG&NAV_JB"),
            //    Cron.Daily
            //    );

            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chatHub");
                endpoints.MapHub<NotificationHub>("/notificationHub");
                endpoints.MapControllers();
            });



        }
    }
   
}
