﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FinishProductExcipientController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public FinishProductExcipientController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetFinishProductExcipient")]
        public List<FinishProductExcipientModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var fprecipe = _context.FpmanufacturingRecipe.AsNoTracking().ToList();
            var fpbomInformationMultiple = _context.FpbomInformationMultiple.AsNoTracking().ToList();
            var finishproductexcipient = _context.FinishProductExcipient.Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(r => r.RegisterationCode)
                .Include(f => f.FinishProduct).AsNoTracking().ToList();
            List<FinishProductExcipientModel> finishProductExcipientModel = new List<FinishProductExcipientModel>();
            finishproductexcipient.ForEach(s =>
            {
                FinishProductExcipientModel finishProductExcipientModels = new FinishProductExcipientModel
                {
                    FinishProductExcipientId = s.FinishProductExcipientId,
                    FinishProductId = fprecipe != null ? fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.FpmanufacturingRecipeId).FirstOrDefault() : fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.FinishProductGeneralInfoId).FirstOrDefault(),
                    ProductName = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)).Value : "",
                    IsMasterDocument = s.IsMasterDocument,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    IsInformaionvsmaster = s.IsInformaionvsmaster.GetValueOrDefault(false),
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    BomInformationIds = fpbomInformationMultiple != null ? fpbomInformationMultiple.Where(l => l.FinishProductExcipientId == s.FinishProductExcipientId).Select(l => l.BomInformationId.Value).ToList() : new List<long>(),
                };
                finishProductExcipientModel.Add(finishProductExcipientModels);
            });
            return finishProductExcipientModel.OrderByDescending(a => a.FinishProductExcipientId).ToList();
        }
        [HttpPost]
        [Route("GetFinishProductExcipientByRefNo")]
        public List<FinishProductExcipientModel> GetFinishProductExcipientByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var finsiproductGeneralList = _context.FinishProduct.AsNoTracking().ToList();
            var fprecipe = _context.FpmanufacturingRecipe.AsNoTracking().ToList();
            var fpbomInformationMultiple = _context.FpbomInformationMultiple.AsNoTracking().ToList();
            var finishproductexcipient = _context.FinishProductExcipient.Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(r => r.RegisterationCode)
                .Include(f => f.FinishProduct).AsNoTracking().ToList();
            List<FinishProductExcipientModel> finishProductExcipientModel = new List<FinishProductExcipientModel>();
            finishproductexcipient.ForEach(s =>
            {
                FinishProductExcipientModel finishProductExcipientModels = new FinishProductExcipientModel
                {
                    FinishProductExcipientId = s.FinishProductExcipientId,
                    FinishProductId = fprecipe != null ? fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.FpmanufacturingRecipeId).FirstOrDefault() : fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.FinishProductGeneralInfoId).FirstOrDefault(),
                    ManufacturingSiteId = fprecipe != null ? fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.ManufacturingSiteId).FirstOrDefault() : fprecipe.Where(f => f.FinishProductGeneralInfoId == s.FinishProductId).Select(f => f.ManufacturingSiteId).FirstOrDefault(),
                    ManufacturingSite = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ManufacturingSiteId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ManufacturingSiteId)).Value : "",
                    RegisterCountry = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.RegisterCountry) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.RegisterCountry).Value : "",
                    ProductOwner = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.RegisterProductOwnerId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.RegisterProductOwnerId).Value : "",
                    PRHSpecificName = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.PrhspecificProductId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.PrhspecificProductId).Value : "",
                    RegisterHolderName = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductRegistrationHolderId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductRegistrationHolderId).Value : "",
                    ProductName = s.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)).Value : "",
                    IsMasterDocument = s.IsMasterDocument,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    IsInformaionvsmaster = s.IsInformaionvsmaster.GetValueOrDefault(false),
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    BomInformationIds = fpbomInformationMultiple != null ? fpbomInformationMultiple.Where(l => l.FinishProductExcipientId == s.FinishProductExcipientId).Select(l => l.BomInformationId.Value).ToList() : new List<long>(),
                };
                finishProductExcipientModel.Add(finishProductExcipientModels);
            });
            if (finishProductExcipientModel != null)
            {
                finishProductExcipientModel.ForEach(h =>
                {
                    h.BOMProductName = h.ManufacturingSite + " | " + h.RegisterCountry + " | " + h.RegisterHolderName + " | " + h.ProductOwner + " | " + h.PRHSpecificName + " | " + h.ProfileReferenceNo;

                });
            }
            if (refSearchModel.IsHeader)
            {
                return finishProductExcipientModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FinishProductExcipientId).ToList();
            }
            return finishProductExcipientModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FinishProductExcipientId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FinishProductExcipientModel> GetData(SearchModel searchModel)
        {
            var finishproductexcipient = new FinishProductExcipient();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).FirstOrDefault();
                        break;
                    case "Last":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).LastOrDefault();
                        break;
                    case "Next":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).LastOrDefault();
                        break;
                    case "Previous":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).FirstOrDefault();
                        break;
                    case "Last":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).LastOrDefault();
                        break;
                    case "Next":
                        finishproductexcipient = _context.FinishProductExcipient.OrderBy(o => o.FinishProductExcipientId).FirstOrDefault(s => s.FinishProductExcipientId > searchModel.Id);
                        break;
                    case "Previous":
                        finishproductexcipient = _context.FinishProductExcipient.OrderByDescending(o => o.FinishProductExcipientId).FirstOrDefault(s => s.FinishProductExcipientId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<FinishProductExcipientModel>(finishproductexcipient);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var finishProductItem = result.FinishProductId > 0 ? (_context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.FinishProductId).FinishProductId) : null;
                if (finishProductItem != null)
                {
                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }
            }
            return result;
        }
        [HttpPost]
        [Route("InsertFinishProductExcipient")]
        public FinishProductExcipientModel Post(FinishProductExcipientModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "FinishProductExcipient" });

            long? finishproductGeneralinfoID = _context.FpmanufacturingRecipe.Where(f => f.FpmanufacturingRecipeId == value.FinishProductId).Select(f => f.FinishProductGeneralInfoId).FirstOrDefault();
            // long? finishProductID = _context.FinishProductGeneralInfo.Where(f => f.FinishProductGeneralInfoId == finishproductGeneralinfoID).Select(f => f.FinishProductId).FirstOrDefault();
            var finishproductexcipient = new FinishProductExcipient
            {

                FinishProductId = finishproductGeneralinfoID,
                IsMasterDocument = value.IsMasterDocument,
                IsInformaionvsmaster = value.IsInformaionvsmaster,
                RegisterationCodeId = value.RegisterationCodeId,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.FinishProductExcipient.Add(finishproductexcipient);
            _context.SaveChanges();
            if (value.BomInformationIds != null && value.BomInformationIds.Any())
            {
                value.BomInformationIds.ForEach(o =>
                {
                    var BomInformationIdsItems = new FpbomInformationMultiple
                    {
                        FinishProductExcipientId = finishproductexcipient.FinishProductExcipientId,
                        BomInformationId = o,
                    };
                    _context.FpbomInformationMultiple.Add(BomInformationIdsItems);
                });
                _context.SaveChanges();
            }
            value.FinishProductExcipientId = finishproductexcipient.FinishProductExcipientId;
            value.MasterProfileReferenceNo = finishproductexcipient.MasterProfileReferenceNo;
            value.ProfileReferenceNo = finishproductexcipient.ProfileReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateFinishProductExcipient")]
        public FinishProductExcipientModel Put(FinishProductExcipientModel value)
        {
            long? finishproductGeneralinfoID = _context.FpmanufacturingRecipe.Where(f => f.FpmanufacturingRecipeId == value.FinishProductId).Select(f => f.FinishProductGeneralInfoId).FirstOrDefault();
            var finishproductexcipient = _context.FinishProductExcipient.SingleOrDefault(p => p.FinishProductExcipientId == value.FinishProductExcipientId);
            finishproductexcipient.FinishProductId = finishproductGeneralinfoID;
            finishproductexcipient.IsMasterDocument = value.IsMasterDocument;
            finishproductexcipient.RegisterationCodeId = value.RegisterationCodeId;
            finishproductexcipient.IsInformaionvsmaster = value.IsInformaionvsmaster;
            finishproductexcipient.ModifiedByUserId = value.ModifiedByUserID;
            finishproductexcipient.ModifiedDate = DateTime.Now;
            finishproductexcipient.StatusCodeId = value.StatusCodeID.Value;
            var BomInformationIdsList = _context.FpbomInformationMultiple.Where(a => a.FinishProductExcipientId == value.FinishProductExcipientId).ToList();
            if (BomInformationIdsList.Count > 0)
            {
                _context.FpbomInformationMultiple.RemoveRange(BomInformationIdsList);
                _context.SaveChanges();
            }
            _context.SaveChanges();
            if (value.BomInformationIds != null && value.BomInformationIds.Any())
            {
                value.BomInformationIds.ForEach(o =>
                {
                    var BomInformationIdsItems = new FpbomInformationMultiple
                    {
                        FinishProductExcipientId = value.FinishProductExcipientId,
                        BomInformationId = o,
                    };
                    _context.FpbomInformationMultiple.Add(BomInformationIdsItems);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpGet]
        [Route("IsMasterExist")]
        public bool IsMasterExist(int? id)
        {
            var list = _context.FinishProductExcipient.Where(s => s.FinishProductId == id && s.IsMasterDocument == true).FirstOrDefault();
            if (list != null)
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
            else
            {
                return false;
            }
        }
        [HttpGet]
        [Route("GetFinishProductExcipientByFinishProduct")]
        public List<FinishProductExcipientModel> GetFinishProductExcipientByFinishProduct(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var finishproductexcipient = _context.FinishProductExcipient.Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(r => r.RegisterationCode)
                .Include(f => f.FinishProduct).AsNoTracking().ToList();
            List<FinishProductExcipientModel> finishProductExcipientModel = new List<FinishProductExcipientModel>();
            finishproductexcipient.ForEach(s =>
            {
                FinishProductExcipientModel finishProductExcipientModels = new FinishProductExcipientModel
                {
                    FinishProductExcipientId = s.FinishProductExcipientId,
                    FinishProductId = s.FinishProductId,
                    ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)).Value : "",
                    IsMasterDocument = s.IsMasterDocument,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    //IsInformaionvsmaster = s.IsInformaionvsmaster,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                };
                finishProductExcipientModel.Add(finishProductExcipientModels);
            });
            return finishProductExcipientModel.Where(a => a.FinishProductId == id && a.IsMasterDocument == true).OrderByDescending(a => a.FinishProductExcipientId).ToList();
        }
        [HttpDelete]
        [Route("DeleteFinishProductExcipient")]
        public void Delete(int id)
        {
            var finishproductexcipient = _context.FinishProductExcipient.SingleOrDefault(p => p.FinishProductExcipientId == id);
            if (finishproductexcipient != null)
            {
                var finishproductexcipientLine = _context.FinishProductExcipientLine.Where(s => s.FinishProductExcipientId == finishproductexcipient.FinishProductExcipientId).ToList();
                if (finishproductexcipientLine != null)
                {
                    _context.FinishProductExcipientLine.RemoveRange(finishproductexcipientLine);
                    _context.SaveChanges();
                }
                _context.FinishProductExcipient.Remove(finishproductexcipient);
                _context.SaveChanges();
            }
        }
    }
}