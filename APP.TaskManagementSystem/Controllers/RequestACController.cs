﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RequestACController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IHubContext<ChatHub> _hub;
        private readonly IHostingEnvironment _hostingEnvironment;

        public RequestACController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IHubContext<ChatHub> hub, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hub = hub;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetRequestACItems")]
        public List<RequestACModel> Get()
        {
            List<RequestACModel> requestACModels = new List<RequestACModel>();
            var Acentry = _context.RequestAc.Include("AddedByUser").Include(c => c.Customer).Include("Company").Include(s => s.StatusCode).Include(s => s.Profile).Include("ModifiedByUser").OrderByDescending(o => o.RequestAcid).AsNoTracking().ToList();
            Acentry.ForEach(s =>
            {
                RequestACModel requestACModel = new RequestACModel
                {
                    RequestAcid = s.RequestAcid,
                    ProfileId = s.ProfileId,
                    ProfileNo = s.ProfileNo,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    CustomerId = s.CustomerId,
                    CompanyId = s.CompanyId,
                    CustomerName = s.Customer != null ? s.Customer.CompanyName : "",
                    DateChange = s.DateChange,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    CompanyName = s.Company.PlantCode,
                    Date = s.Date,
                    ReferenceMonth = s.ReferenceMonth,
                };
                requestACModels.Add(requestACModel);

            });
          
            return requestACModels;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetRequestACItemsByStatus")]
        public List<RequestACModel> GetRequestACItemsByStatus()
        {
            var Acentry = _context.RequestAc.Include("AddedByUser").Include(c => c.Customer).Include("Company").Include(s => s.StatusCode).Include(s => s.Profile).Include("ModifiedByUser").Select(s => new RequestACModel
            {
                RequestAcid = s.RequestAcid,
                ProfileId = s.ProfileId,
                ProfileNo = s.ProfileNo,
                ProfileName = s.Profile != null ? s.Profile.Name : "",
                CustomerId = s.CustomerId,
                CompanyId = s.CompanyId,
                CustomerName = s.Customer != null ? s.Customer.CompanyName : "",
                DateChange = s.DateChange,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                CompanyName = s.Company.PlantCode,
                Date = s.Date,
                ReferenceMonth = s.ReferenceMonth,
            }).OrderByDescending(o => o.RequestAcid).AsNoTracking().Where(a => a.StatusCodeID == 2151).ToList();
            //var result = _mapper.Map<List<ACEntryModel>>(Acentry);
            return Acentry;
        }

        [HttpGet]
        [Route("GetRequestACLinesByID")]
        public List<RequestACLineModel> GetRequestACLinesByID(int id)
        {
            var requestACLineModels = _context.RequestAcline
                                        .Include(n => n.NavItem)
                                        .Include(s => s.StatusCode)
                                        .Include(a => a.AddedByUser)
                                        .Include(m => m.ModifiedByUser)
                                        .Select(s => new RequestACLineModel
                                        {
                                            RequestAcid = s.RequestAcid,
                                            RequestAclineId = s.RequestAclineId,
                                            ItemName = s.NavItem.No,
                                            NavItemId = s.NavItemId,
                                            DistributorItemId = s.DistributorItemId,
                                            ShelfLife = s.ShelfLife,
                                            Avg12Months = s.Avg12Months,
                                            Avg6Months = s.Avg6Months,
                                            Avg3Months = s.Avg3Months,
                                            CurrentAc = s.CurrentAc,
                                            ProposedNewAc = s.ProposedNewAc,
                                            DifferenceInAc = s.DifferenceInAc,
                                            PercentDifference = s.PercentDifference,
                                            Comments = s.Comments,
                                            StatusCodeID = s.StatusCodeId,
                                            AddedByUserID = s.AddedByUserId,
                                            ModifiedByUserID = s.ModifiedByUserId,
                                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                                            AddedDate = s.AddedDate,
                                            ModifiedDate = s.ModifiedDate,
                                           
                                        }).OrderByDescending(o => o.RequestAclineId).Where(t => t.RequestAcid == id).AsNoTracking().ToList();
            return requestACLineModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RequestACModel> GetData(SearchModel searchModel)
        {
            var requestAc = new RequestAc();
            List<RequestAc> requestAcs = new List<RequestAc>();
            RequestACModel requestACModel = new RequestACModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                    case "Last":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Next":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Previous":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                    case "Last":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Next":
                        requestAc = _context.RequestAc.OrderBy(o => o.RequestAcid).FirstOrDefault(s => s.RequestAcid > searchModel.Id);
                        break;
                    case "Previous":
                        requestAc = _context.RequestAc.OrderByDescending(o => o.RequestAcid).FirstOrDefault(s => s.RequestAcid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<RequestACModel>(requestAc);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertRequestAC")]
        public async Task<RequestACModel> Post(RequestACModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title="Request AC" });
            var requestAc = new RequestAc
            {
                ProfileId = value.ProfileId,
                ProfileNo = profileNo,
                CustomerId = value.CustomerId,
                CompanyId = value.CompanyId,
                DateChange = value.DateChange,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Date = value.Date,
                ReferenceMonth = value.ReferenceMonth,
            };
            _context.RequestAc.Add(requestAc);
            _context.SaveChanges();
            value.RequestAcid = requestAc.RequestAcid;
            value.ProfileNo = requestAc.ProfileNo;

            await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Completed");
            await GetPopulateAC(value.RequestAcid, value.CompanyId.Value, value.CustomerId.Value);
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateRequestAC")]
        public async Task<RequestACModel> Put(RequestACModel value)
        {
            var requestAc = _context.RequestAc.SingleOrDefault(p => p.RequestAcid == value.RequestAcid);

            requestAc.ModifiedDate = DateTime.Now;
            requestAc.CustomerId = value.CustomerId;
            requestAc.CompanyId = value.CompanyId;
            requestAc.CustomerId = value.CustomerId;
            requestAc.CompanyId = value.CompanyId;
            requestAc.DateChange = value.DateChange;
            requestAc.ModifiedByUserId = value.ModifiedByUserID;
            requestAc.StatusCodeId = value.StatusCodeID.Value;
            requestAc.Date = value.Date;
            requestAc.ReferenceMonth = value.ReferenceMonth;
            _context.SaveChanges();
            await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Completed");

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteRequestAC")]
        public void Delete(int id)
        {
            var requestAc = _context.RequestAc.SingleOrDefault(p => p.RequestAcid == id);
            if (requestAc != null)
            {
                var requestAclines = _context.RequestAcline.Where(ac => ac.RequestAcid == requestAc.RequestAcid).AsNoTracking().ToList();
                if (requestAclines != null)
                {
                    _context.RequestAcline.RemoveRange(requestAclines);
                    _context.SaveChanges();
                }
                _context.RequestAc.Remove(requestAc);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetPopulateAC")]
        public async Task<List<RequestACLineModel>> GetPopulateAC(long acId, long companyId, long custId)
        {
            var disItems = new List<RequestACLineModel>();
            var acItemBalList = await _context.DistStockBalance.Include("DistItem.NavItemCitemList").Where(f => f.StockBalMonth > DateTime.Today.AddMonths(-12)).AsNoTracking().ToListAsync();
            var acItemBalListResult = new List<DistStockBalModel>();
            acItemBalList.ForEach(f =>
            {
                var disbal = new DistStockBalModel
                {
                    DistAcid = f.DistItem.DistAcid,
                    QtyOnHand = f.Quantity.GetValueOrDefault(0),
                    ItemNo = f.DistItem.ItemNo,
                    ItemDesc = f.DistItem.ItemDesc,
                    DistName = f.DistItem.DistName,
                    DistItemId = f.DistItemId,
                    CustomerId = f.DistItem.CustomerId.GetValueOrDefault(-1),
                    CompanyId = f.DistItem.CompanyId,
                    StockBalanceMonth = f.StockBalMonth,
                    NavItemId = f.DistItem.NavItemCitemList.Count > 0 ? f.DistItem.NavItemCitemList.FirstOrDefault().NavItemId : null
                };
                acItemBalListResult.Add(disbal);
            });
            var itemMasterforReport = await _context.Navitems.Where(s => s.StatusCodeId == 1 && s.CompanyId == companyId).AsNoTracking().ToListAsync();

            var acitemRecords = acItemBalListResult.Where(f => f.CompanyId == companyId && f.CustomerId == custId).ToList();

            acitemRecords.ForEach(f =>
            {
                var navItem = itemMasterforReport.FirstOrDefault(n => n.ItemId == f.NavItemId);
                disItems.Add(new RequestACLineModel
                {
                    RequestAcid = acId,
                    DistributorItemId = f.DistItemId,
                    NavItemId = f.NavItemId,
                    ShelfLife = navItem != null ? navItem.ShelfLife : "",
                    ItemName = f.ItemNo,
                    Avg12Months = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId).Sum(s => s.QtyOnHand) / 12,
                    Avg6Months = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId && d.StockBalanceMonth > DateTime.Today.AddMonths(-6)).Sum(s => s.QtyOnHand) / 6,
                    Months12 = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId).Sum(s => s.QtyOnHand),
                    Months6 = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId && d.StockBalanceMonth > DateTime.Today.AddMonths(-6)).Sum(s => s.QtyOnHand),
                    Avg3Months = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId && d.StockBalanceMonth > DateTime.Today.AddMonths(-3)).Sum(s => s.QtyOnHand) / 3,
                    CurrentAc = acItemBalListResult.Where(d => d.DistItemId == f.DistItemId && d.StockBalanceMonth.Value.Month == DateTime.Today.Month).Sum(s => s.QtyOnHand),
                });

            });

            disItems.ForEach(f =>
            {
                f.ProposedNewAc = f.CurrentAc;
                f.DifferenceInAc = f.CurrentAc - f.ProposedNewAc;
                f.PercentDifference = f.CurrentAc > 0 ? f.DifferenceInAc / f.CurrentAc : 0;

                var requestAC = new RequestAcline
                {
                    AddedByUserId = 1,
                    AddedDate = DateTime.Now,
                    Avg12Months = f.Avg12Months,
                    Avg3Months = f.Avg3Months,
                    Avg6Months = f.Avg6Months,
                    Comments = f.Comments,
                    CurrentAc = f.CurrentAc,
                    DifferenceInAc = f.DifferenceInAc,
                    DistributorItemId = f.DistributorItemId,
                    NavItemId = f.NavItemId,
                    PercentDifference = f.PercentDifference,
                    ProposedNewAc = f.ProposedNewAc,
                    RequestAcid = f.RequestAcid,
                    ShelfLife = f.ShelfLife,
                    StatusCodeId = 1,
                };
                _context.RequestAcline.Add(requestAC);
            });


            await _context.SaveChangesAsync();

            return disItems;
        }


    }
}