﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ExtensionInformationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ExtensionInformationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetExtensionInformationLines")]
        public List<ExtensionInformationLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var extensionInformationLines = _context.ExtensionInformationLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Where(s => s.TenderInformationId == id)
                .AsNoTracking()
                .ToList();
            List<ExtensionInformationLineModel> extensionInformationLineModels = new List<ExtensionInformationLineModel>();
            extensionInformationLines.ForEach(s =>
            {
                ExtensionInformationLineModel extensionInformationLineModel = new ExtensionInformationLineModel
                {
                    TenderInformationId = s.TenderInformationId,
                    ExtensionInformationLineId = s.ExtensionInformationLineId,
                    QuotationNo = s.QuotationNo,
                    Quantity = s.Quantity,
                    Currency = masterDetailList != null && s.CurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.CurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                    ExtendedPeriodTo = s.ExtendedPeriodTo,
                    CurrencyId = s.CurrencyId,
                    SellingPrice = s.SellingPrice,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId = s.SessionId,

                };
                extensionInformationLineModels.Add(extensionInformationLineModel);
            });
            return extensionInformationLineModels.OrderByDescending(a => a.ExtensionInformationLineId).ToList();
        }


        [HttpPost]
        [Route("InsertExtensionInformationLine")]
        public ExtensionInformationLineModel Post(ExtensionInformationLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var extensionInformationLine = new ExtensionInformationLine
            {
                TenderInformationId = value.TenderInformationId,
                QuotationNo = value.QuotationNo,
                Quantity = value.Quantity,
                ExtendedPeriodTo = value.ExtendedPeriodTo,
                CurrencyId = value.CurrencyId,
                SellingPrice = value.SellingPrice,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
                
            };
            _context.ExtensionInformationLine.Add(extensionInformationLine);
            _context.SaveChanges();
            value.ExtensionInformationLineId = extensionInformationLine.ExtensionInformationLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateExtensionInformationLine")]
        public ExtensionInformationLineModel Put(ExtensionInformationLineModel value)
        {
            var extensionInformationLine = _context.ExtensionInformationLine.SingleOrDefault(p => p.ExtensionInformationLineId == value.ExtensionInformationLineId);
            extensionInformationLine.TenderInformationId = value.TenderInformationId;
            extensionInformationLine.Quantity = value.Quantity;
            extensionInformationLine.QuotationNo = value.QuotationNo;
            extensionInformationLine.ExtendedPeriodTo = value.ExtendedPeriodTo;
            extensionInformationLine.CurrencyId = value.CurrencyId;
            extensionInformationLine.SellingPrice = value.SellingPrice;
            extensionInformationLine.StatusCodeId = value.StatusCodeID.Value;
            extensionInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            extensionInformationLine.ModifiedDate = DateTime.Now;
            extensionInformationLine.SessionId = value.SessionId;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteExtensionInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var extensionInformationLine = _context.ExtensionInformationLine.Where(p => p.ExtensionInformationLineId == id).FirstOrDefault();
                if (extensionInformationLine != null)
                {
                    _context.ExtensionInformationLine.Remove(extensionInformationLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
