﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        // public NAV.NAV Context { get; private set; }
        private readonly IConfiguration _config;
        public ProductionEntryController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host, IConfiguration config, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _generateDocumentNoSeries = generate;
            _config = config;
            //Context = new NAV.NAV(new Uri($"{_config["NAV:OdataUrl"]}/Company('{_config["NAV:Company"]}')"))
            //{
            //    Credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"])
            //};
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionEntrys")]
        public List<ProductionEntryModel> Get()
        {
            //var ProductionEntry = _context.ProductionEntry.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new ProductionEntryModel
            var OperationProcedureLine = _context.OperationProcedureLine.Include(o => o.OperationProcedure).ToList();
            List<ProductionEntryModel> ProductionEntryModels = new List<ProductionEntryModel>();
            var productionEntry = _context.ProductionEntry.Include(a => a.AddedByUser)
                .Include(b => b.StatusCode).Include(c => c.ModifiedByUser)
                .Include(d => d.ProductionAction).Include(e => e.StartOfDayProdTaskMultiple)
                .AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.ToList();
            productionEntry.ForEach(s =>
            {
                ProductionEntryModel ProductionEntryModel = new ProductionEntryModel();
                ProductionEntryModel.ProductionEntryID = s.ProductionEntryId;
                ProductionEntryModel.LocationID = s.LocationId;
                ProductionEntryModel.LocationName = s.LocationName;
                ProductionEntryModel.ItemName = s.ItemName;
                ProductionEntryModel.RoomStatus = s.RoomStatus;
                ProductionEntryModel.ProductionActionID = s.ProductionActionId;
                ProductionEntryModel.ProductionActionName = s.ProductionAction != null ? s.ProductionAction.Name : null;
                ProductionEntryModel.ProductionLineNo = s.ProdLineNo;
                ProductionEntryModel.ProductionOrderNo = s.ProductionOrderNo;
                ProductionEntryModel.BatchNumber = s.BatchNo;
                ProductionEntryModel.NumberOfWorker = s.NumberOfWorker;
                ProductionEntryModel.ModifiedByUserID = s.AddedByUserId;
                ProductionEntryModel.AddedByUserID = s.ModifiedByUserId;
                ProductionEntryModel.StatusCodeID = s.StatusCodeId;
                ProductionEntryModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                ProductionEntryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                ProductionEntryModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                ProductionEntryModel.AddedDate = s.AddedDate;
                ProductionEntryModel.ModifiedDate = s.ModifiedDate;
                ProductionEntryModel.StartDate = s.StartDate;
                ProductionEntryModel.EndDate = s.ActualStartDate;
                ProductionEntryModel.Deascription = s.Deascription;
                ProductionEntryModel.ActualStartDate = s.ActualStartDate;
                ProductionEntryModel.SessionId = s.SessionId;
                var ProdTaskIds = s.StartOfDayProdTaskMultiple != null ? s.StartOfDayProdTaskMultiple.Where(w => w.ProductionEntryId == s.ProductionEntryId).Select(s => s.ProdTaskPerformId.Value).ToList() : new List<long>();
                if (ProdTaskIds != null && ProdTaskIds.Count > 0)
                {
                    var procedureIds = OperationProcedureLine.Where(o => ProdTaskIds.Contains(o.OperationProcedureLineId)).Select(p => p.OprocedureId).ToList();
                    ProductionEntryModel.ProdTask = applicationMasterDetails != null ? string.Join(" | ", applicationMasterDetails.Where(m => procedureIds.Contains(m.ApplicationMasterDetailId)).Select(s => s.Value)) : "";
                }
                ProductionEntryModels.Add(ProductionEntryModel);
            });
            return ProductionEntryModels.OrderByDescending(o => o.ProductionEntryID).ToList();
        }

        //Changes Done by Aravinth Start

        [HttpPost]
        [Route("GetProductionEntryFilter")]
        public List<ProductionEntryModel> GetFilter(ProductionSearchModel value)
        {
            var productionDatefilter = new List<ProductionEntryModel>();
            var productionDateIds = new List<long?>();
            List<ProductionEntryModel> productionEntryModels = new List<ProductionEntryModel>();
            var productionEntry = _context.ProductionEntry.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.ProductionEntryId).AsNoTracking().ToList();
            if (productionEntry != null && productionEntry.Count > 0)
            {
                productionEntry.ForEach(s =>
                {
                    ProductionEntryModel productionEntryModel = new ProductionEntryModel
                    {
                        ProductionEntryID = s.ProductionEntryId,
                        LocationID = s.LocationId,
                        LocationName = s.LocationName,
                        ItemName = s.ItemName,
                        RoomStatus = s.RoomStatus,
                        ProductionActionID = s.ProductionActionId,
                        ProductionActionName = s.ProductionAction != null ? s.ProductionAction.Name : "",
                        ProductionLineNo = s.ProdLineNo,
                        Deascription = s.Deascription,
                        ProductionOrderNo = s.ProductionOrderNo,
                        BatchNumber = s.BatchNo,
                        NumberOfWorker = s.NumberOfWorker,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StartDate = s.StartDate.HasValue ? s.StartDate.Value : DateTime.Now,
                        EndDate = s.EndDate.HasValue ? s.EndDate.Value : DateTime.Now,
                        SessionId = s.SessionId,
                    };
                    productionEntryModels.Add(productionEntryModel);
                });
            }

            List<ProductionEntryModel> filteredItems = new List<ProductionEntryModel>();
            if (value.ItemName.Any())
            {
                var itemNameFilters = productionEntryModels.Where(p => p.ItemName != null ? (p.ItemName.ToLower() == (value.ItemName.ToLower())) : false).ToList();
                itemNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == i.ProductionEntryID))
                    {
                        filteredItems.Add(i);
                    }
                });
            }
            if (value.ProductionOrderNo.Any())
            {
                var itemProductionentry = productionEntryModels.Where(p => p.ProductionOrderNo != null ? (p.ProductionOrderNo.ToLower() == (value.ProductionOrderNo.ToLower())) : false).ToList();
                itemProductionentry.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == i.ProductionEntryID))
                    {
                        filteredItems.Add(i);
                    }
                });
            }
            if (value.BatchNo.Any())
            {
                var itemBatch = productionEntryModels.Where(p => p.BatchNumber != null ? (p.BatchNumber.ToLower() == (value.BatchNo.ToLower())) : false).ToList();
                itemBatch.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == i.ProductionEntryID))
                    {
                        filteredItems.Add(i);
                    }
                });
            }
            if (value.Description.Any())
            {
                var itemDescription = productionEntryModels.Where(p => p.Deascription != null ? (p.Deascription.ToLower() == (value.Description.ToLower())) : false).ToList();
                itemDescription.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == i.ProductionEntryID))
                    {
                        filteredItems.Add(i);
                    }
                });
            }
            if (value.Location.Any())
            {
                var itemLocation = productionEntryModels.Where(p => p.LocationName != null ? (p.LocationName.ToLower() == (value.Location.ToLower())) : false).ToList();
                itemLocation.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == i.ProductionEntryID))
                    {
                        filteredItems.Add(i);
                    }
                });
            }
            if ((value.StartDate != DateTime.Now.Date) && (value.EndDate != DateTime.Now.Date))
            {
                var dateFilter = productionEntryModels.Where(p => (p.StartDate.Value.Date >= value.StartDate.Value.Date) && (p.EndDate.Value.Date <= value.EndDate.Value.Date)).ToList();
                dateFilter.ForEach(d =>
                {
                    if (!filteredItems.Any(f => f.ProductionEntryID == d.ProductionEntryID))
                    {
                        filteredItems.Add(d);
                    }
                });
            }

            return filteredItems;
        }

        //Changes Done by Aravinth End

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionEntryModel> GetData(SearchModel searchModel)
        {
            var productionEntry = new ProductionEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).LastOrDefault();
                        break;
                    case "Next":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).LastOrDefault();
                        break;
                    case "Previous":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).LastOrDefault();
                        break;
                    case "Next":
                        productionEntry = _context.ProductionEntry.OrderBy(o => o.ProductionEntryId).FirstOrDefault(s => s.ProductionEntryId > searchModel.Id);
                        break;
                    case "Previous":
                        productionEntry = _context.ProductionEntry.OrderByDescending(o => o.ProductionEntryId).FirstOrDefault(s => s.ProductionEntryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionEntryModel>(productionEntry);
            return result;
        }
        //[HttpGet]
        //[Route("GetProdOrders")]
        //public async Task<IEnumerable<NAV.CPS_ProductionOrder>> GetProd()
        //{
        //    var context = new DataService(_config, "NAV_SG");
        //    var nquery = context.Context.CPS_ProductionOrder.Where(r => r.Status == "Released");
        //    DataServiceQuery<NAV.CPS_ProductionOrder> query = (DataServiceQuery<NAV.CPS_ProductionOrder>)nquery;

        //    TaskFactory<IEnumerable<NAV.CPS_ProductionOrder>> taskFactory = new TaskFactory<IEnumerable<NAV.CPS_ProductionOrder>>();
        //    IEnumerable<NAV.CPS_ProductionOrder> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionEntry")]
        public async Task<ProductionEntryModel> Post(ProductionEntryModel value)
        {
            try
            {
                var sessionId = value.SessionId ?? Guid.NewGuid();
                value.RoomStatus = value.RoomStatus ?? Guid.NewGuid().ToString() + ".png";
                value.RoomStatus1 = value.RoomStatus1 ?? Guid.NewGuid().ToString() + ".png";
                var scanExist = _context.ProductionEntry.Any(a => a.ProdLineNo == value.ProductionLineNo && a.ProductionOrderNo == value.ProductionOrderNo);
                if (!scanExist)
                {
                    var productionEntry = new ProductionEntry
                    {
                        LocationName = value.LocationName,
                        ItemName = value.ItemName,
                        RoomStatus = value.RoomStatus,
                        RoomStatus1 = value.RoomStatus1,
                        ProductionActionId = value.ProductionActionID,
                        NumberOfWorker = value.NumberOfWorker,
                        ProductionOrderNo = value.ProductionOrderNo,
                        AddedByUserId = value.PostedUserID,
                        AddedDate = DateTime.Now,
                        ProdLineNo = value.ProductionLineNo,
                        ActualStartDate = DateTime.Now,
                        BatchNo = value.BatchNumber,
                        Deascription = value.ItemName,
                        Company = value.CompanyName,
                        StatusCodeId = 1,
                        SessionId = sessionId,
                    };

                    if (value.FrontPhoto != null && value.FrontPhoto.Length > 0)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.RoomStatus;
                        System.IO.File.WriteAllBytes(serverPath, value.FrontPhoto);
                    }

                    if (value.BackPhoto != null && value.BackPhoto.Length > 0)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.RoomStatus1;
                        System.IO.File.WriteAllBytes(serverPath, value.BackPhoto);
                    }
                    if (value.ProdTaskIds != null)
                    {
                        value.ProdTaskIds.ForEach(m =>
                        {
                            productionEntry.StartOfDayProdTaskMultiple.Add(new StartOfDayProdTaskMultiple
                            {
                                ProdTaskPerformId = m
                            });
                        });
                    }
                    var context = new DataService(_config, value.CompanyName);
                    var Company = value.CompanyName;
                    var post = new WebIntegration.WebIntegration_PortClient();

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/WebIntegration"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                  System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    var result = await post.FnUpdateStartDateAsync(value.RePlanRefNo, DateTime.Now);
                    var startDate = result.return_value;
                    if (DateTime.TryParse(startDate, out DateTime strDate))
                    {
                        productionEntry.StartDate = strDate;
                    }
                    var nquery = context.Context.CPS_ProdOrderLine.Where(i => i.Replan_Ref_No == value.RePlanRefNo);
                    DataServiceQuery<NAV.CPS_ProdOrderLine> query = (DataServiceQuery<NAV.CPS_ProdOrderLine>)nquery;
                    TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>> taskFactory1 = new TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>>();
                    var prodOrderLines = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                    var prodList = prodOrderLines.ToList();
                    var first = prodList.FirstOrDefault(f => f.Prod_Order_No == value.ProductionOrderNo && f.Line_No == value.ProductionLineNo);
                    if (first != null)
                    {
                        productionEntry.StartDate = first.Starting_Date.HasValue ? first.Starting_Date : null;
                        //if (value.ProcessNo == 1)
                        //{
                        //    if (first.Actual_Start_Date == null || first.Actual_Start_Date == DateTime.MinValue)
                        //    {
                        //        foreach (var p in prodList)
                        //        {
                        //            p.Actual_Start_Date = DateTime.Now;
                        //            context.Context.UpdateObject(p);
                        //        }
                        //        try
                        //        {
                        //            TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                        //            var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            if (ex.InnerException != null)
                        //            {
                        //                if (!ex.InnerException.Message.Contains("Another user has already changed the record."))
                        //                {
                        //                    value.IsError = true;
                        //                    value.Errormessage = ex.Message;
                        //                    return value;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                    }

                    _context.ProductionEntry.Add(productionEntry);
                    _context.SaveChanges();
                    value.ProductionEntryID = productionEntry.ProductionEntryId;
                    await InsertProductionEntryBMR(value);
                    return value;
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "Production already started.";
                    return value;
                }

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }
        [HttpPost]
        [Route("InsertProductionEntryBMR")]
        public async Task<ProductionEntryModel> InsertProductionEntryBMR(ProductionEntryModel value)
        {
            var itemclassificationmaster = _context.ItemClassificationMaster.Where(w => w.ClassificationTypeId == 1503 && w.IsForm == true && w.ParentId != null).FirstOrDefault();
            if (itemclassificationmaster != null)
            {
                var StandardId = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMaster.ApplicationMasterCodeId == 275 && w.Value.ToLower() == "standard").Select(a => a.ApplicationMasterDetailId).FirstOrDefault();
                ClassificationBmrController classificationBmr = new ClassificationBmrController(_context, _mapper, _generateDocumentNoSeries);
                var companyId = value.CompanyName == "NAV_JB" ? 1 : 2;
                ClassificationBmrModel ClassificationBmr = new ClassificationBmrModel
                {
                    ProdOrderNo = value.ProductionOrderNo,
                    ProfileID = itemclassificationmaster.ProfileId,
                    ItemClassificationMasterId = itemclassificationmaster.ItemClassificationId,
                    TypeOfProductionId = StandardId,
                    AddedByUserID = value.PostedUserID,
                    StatusCodeID = 1,
                    ProductionStatusId = 1821,
                    ProductionSimulationDate = DateTime.Now,
                    CompanyId = companyId,
                    BatchSize = _context.ProductionSimulation.Where(w => w.ProdOrderNo == value.ProductionOrderNo && w.CompanyId == companyId && w.IsBmrticket == true).Select(a => a.BatchSize).FirstOrDefault(),
                };
                classificationBmr.Post(ClassificationBmr);
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionEntry")]
        public ProductionEntryModel Put(ProductionEntryModel value)
        {
            var productionEntry = _context.ProductionEntry.Include(s => s.StartOfDayProdTaskMultiple).SingleOrDefault(p => p.ProductionEntryId == value.ProductionEntryID);
            var sessionId = value.SessionId ?? Guid.NewGuid();
            productionEntry.ItemName = value.ItemName;
            productionEntry.LocationName = value.LocationName;
            productionEntry.RoomStatus = value.RoomStatus;
            productionEntry.ProductionOrderNo = value.ProductionOrderNo;
            productionEntry.ProdLineNo = value.ProductionLineNo;
            productionEntry.ProductionActionId = value.ProductionActionID;
            productionEntry.NumberOfWorker = value.NumberOfWorker;
            productionEntry.ModifiedByUserId = value.ModifiedByUserID;
            productionEntry.ModifiedDate = DateTime.Now;
            productionEntry.StatusCodeId = value.StatusCodeID.Value;
            productionEntry.SessionId = sessionId;
            _context.StartOfDayProdTaskMultiple.RemoveRange(productionEntry.StartOfDayProdTaskMultiple);
            if (value.ProdTaskIds != null)
            {
                value.ProdTaskIds.ForEach(m =>
                {
                    productionEntry.StartOfDayProdTaskMultiple.Add(new StartOfDayProdTaskMultiple
                    {
                        ProdTaskPerformId = m
                    });
                });
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionEntry")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var productionEntry = _context.ProductionEntry.SingleOrDefault(p => p.ProductionEntryId == id);
                if (productionEntry != null)
                {
                    _context.ProductionEntry.Remove(productionEntry);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetByCompany")]
        public List<AppDashboardModel> GetByCompany(SearchModel searchModel)
        {
            var productionEntries = _context.ProductionEntry
             .Where(t => t.ActualStartDate.Value.Date == searchModel.Date.Value.Date).AsNoTracking().ToList();

            var dashItems = new List<AppDashboardModel>();

            var item = new AppDashboardModel
            {
                DashboardType = "Malaysia",
                Name = "Company",
                TotalCount = productionEntries.Where(c => c.Company == "NAV_JB").Count().ToString(),
            };
            dashItems.Add(item);

            item = new AppDashboardModel
            {
                DashboardType = "Singapore",
                Name = "Company",
                TotalCount = productionEntries.Where(c => c.Company == "NAV_SG").Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpPost]
        [Route("GetByAction")]
        public List<AppDashboardModel> GetByAction(SearchModel searchModel)
        {
            var productionEntries = _context.ProductionEntry
             .Where(t => t.ActualStartDate.Value.Date == searchModel.Date.Value.Date).AsNoTracking().ToList();

            var dashItems = new List<AppDashboardModel>();

            var item = new AppDashboardModel
            {
                DashboardType = "Pre-line clearance Ready",
                Name = "Action",
                TotalCount = productionEntries.Where(c => c.ProductionActionId == 3).Count().ToString(),
            };
            dashItems.Add(item);

            item = new AppDashboardModel
            {
                DashboardType = "Setting up of Machine",
                Name = "Action",
                TotalCount = productionEntries.Where(c => c.ProductionActionId == 4).Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpPost]
        [Route("GetByLocation")]
        public List<ProductionEntryModel> GetByLocation(SearchModel searchModel)
        {
            List<ProductionEntryModel> productionEntryModels = new List<ProductionEntryModel>();
            var productionEntries = _context.ProductionEntry
             .Where(t => t.ActualStartDate.Value.Date == searchModel.Date.Value.Date).ToList();
            var models = productionEntries.GroupBy(l => new { l.LocationName, l.ProductionOrderNo, l.BatchNo }).ToList();
            models.ForEach(s =>
            {
                var productionEntryModel = new ProductionEntryModel
                {
                    LocationName = s.Key.LocationName,
                    ProductionOrderNo = s.Key.ProductionOrderNo,
                    BatchNumber = s.Key.BatchNo,
                    ProductionLineNo = s.ToList().FirstOrDefault().ProdLineNo,
                    ItemName = s.ToList().FirstOrDefault().ItemName,
                    Deascription = s.ToList().FirstOrDefault().Deascription,
                    NumberOfWorker = s.ToList().Sum(n => n.NumberOfWorker)
                };
                productionEntryModels.Add(productionEntryModel);
            });

            return productionEntryModels;
        }

        [HttpPost]
        [Route("GetProductionEntryByCompany")]
        public List<ProductionEntryModel> GetProductionEntryByCompany(SearchModel searchModel)
        {
            List<ProductionEntryModel> productionEntryModels = new List<ProductionEntryModel>();
            var productionEntries = _context.ProductionEntry.Include(p => p.ProductionAction)
             .Where(t => t.ActualStartDate.Value.Date == searchModel.Date.Value.Date && t.Company == searchModel.SearchString).ToList();
            productionEntries.ForEach(s =>
            {
                var productionEntryModel = new ProductionEntryModel
                {
                    LocationName = s.LocationName,
                    ProductionOrderNo = s.ProductionOrderNo,
                    BatchNumber = s.BatchNo,
                    ProductionLineNo = s.ProdLineNo,
                    ItemName = s.ItemName,
                    Deascription = s.Deascription,
                    NumberOfWorker = s.NumberOfWorker,
                    ProductionActionName = s.ProductionAction.Name,
                    CompanyName = s.Company,
                    SessionId = s.SessionId
                };
                productionEntryModels.Add(productionEntryModel);
            });

            return productionEntryModels;
        }
        [HttpPost]
        [Route("GetProductionEntryByAction")]
        public List<ProductionEntryModel> GetProductionEntryByAction(SearchModel searchModel)
        {
            List<ProductionEntryModel> productionEntryModels = new List<ProductionEntryModel>();
            var actionItem = _context.ProductionAction.FirstOrDefault(a => a.Name.Trim() == searchModel.SearchString.Trim());
            if (actionItem != null)
            {
                var productionEntries = _context.ProductionEntry.Include(p => p.ProductionAction)
             .Where(t => t.ActualStartDate.Value.Date == searchModel.Date.Value.Date && t.ProductionActionId == actionItem.ProductionActionId).ToList();
                productionEntries.ForEach(s =>
                {
                    var productionEntryModel = new ProductionEntryModel
                    {
                        LocationName = s.LocationName,
                        ProductionOrderNo = s.ProductionOrderNo,
                        BatchNumber = s.BatchNo,
                        ProductionLineNo = s.ProdLineNo,
                        ItemName = s.ItemName,
                        Deascription = s.Deascription,
                        NumberOfWorker = s.NumberOfWorker,
                        ProductionActionName = s.ProductionAction.Name,
                        CompanyName = s.Company,
                        SessionId = s.SessionId,
                    };
                    productionEntryModels.Add(productionEntryModel);
                });
            }
            return productionEntryModels;
        }

        [HttpGet]
        [Route("GetTicketNos")]
        public List<TicketModel> GetTickets()
        {
            List<TicketModel> ticketModels = new List<TicketModel>();
            var productionEntry = _context.ProductionEntry.Select(s => new { TicketNo = s.ProductionOrderNo, BatchNo = s.BatchNo }).AsNoTracking().ToList();
            productionEntry.ForEach(s =>
            {
                ticketModels.Add(new TicketModel { TicketNo = s.TicketNo, BatchNo = s.BatchNo });
            });
            return ticketModels.OrderByDescending(o => o.TicketNo).Take(50).ToList();
        }

        [HttpGet]
        [Route("GetBatches")]
        public List<TicketModel> GetBatches(string Id)
        {
            List<TicketModel> ticketModels = new List<TicketModel>();
            var productionEntry = _context.ProductionEntry.Select(s => new { TicketNo = s.ProductionOrderNo, BatchNo = s.BatchNo }).Where(d => d.TicketNo == Id).AsNoTracking().ToList();
            productionEntry.ForEach(s =>
            {
                ticketModels.Add(new TicketModel { TicketNo = s.TicketNo, BatchNo = s.BatchNo });
            });
            return ticketModels.OrderByDescending(o => o.TicketNo).Take(50).ToList();
        }
    }
}