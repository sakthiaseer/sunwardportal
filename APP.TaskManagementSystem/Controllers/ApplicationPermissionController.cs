﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationPermissionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationPermissionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetFormPermission")]
        public List<ApplicationFormFieldModel> GetFormPermission(string Id, int userId)
        {
            List<ApplicationFormFieldModel> applicationFormFieldModels = new List<ApplicationFormFieldModel>();
            var form = _context.ApplicationForm.FirstOrDefault(f => f.ScreenId == Id);
            if (form != null)
            {
                var roleIds = _context.ApplicationUserRole.Where(f => f.UserId == userId).Select(s => s.RoleId).ToList();
                var formRoleIds = _context.ApplicationFormFields.Where(f => f.ApplicationFormId == form.FormId && f.StatusCodeId == 1).Select(s => s.ApplicationFormFieldId).ToList();

                var formPermission = _context.ApplicationFormPermission.Include("ApplicationFormField").Where(f => formRoleIds.Contains(f.ApplicationFormFieldId.Value) && roleIds.Contains(f.RoleId.Value)).AsNoTracking().ToList();
                formPermission.ForEach(s =>
                {
                    ApplicationFormFieldModel applicationFormFieldModel = new ApplicationFormFieldModel
                    {
                        ColumnName = s.ApplicationFormField.ColumnName,
                        DisplayName = s.ApplicationFormField.DisplayName,
                        IsVisible = s.IsVisible,
                        IsReadOnly = s.IsReadOnly,
                        IsValueFilter = s.IsValueFilter,
                        FieldValue = s.FieldValue,

                    };
                    applicationFormFieldModels.Add(applicationFormFieldModel);
                });

                
                return applicationFormFieldModels;
            }
            return null;
        }
    }
}