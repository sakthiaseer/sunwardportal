﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MethodTemplateRoutineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MethodTemplateRoutineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetMethodTemplateRoutine")]
        public List<MethodTemplateRoutineModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<MethodTemplateRoutineModel> methodTemplateRoutineModels = new List<MethodTemplateRoutineModel>();
            var methodTemplateRoutine = _context.MethodTemplateRoutine.Include("AddedByUser")
                                            .Include("ModifiedByUser").Include("ManufacturingType")
                                            .Include(l => l.MethodTemplateRoutineLanguage).Include(s => s.StatusCode)
                                            .OrderByDescending(o => o.MethodTemplateRoutineId).AsNoTracking().ToList();
            if(methodTemplateRoutine!=null && methodTemplateRoutine.Count>0)
            {
                methodTemplateRoutine.ForEach(s =>
                {
                    MethodTemplateRoutineModel methodTemplateRoutineModel = new MethodTemplateRoutineModel
                    {
                        MethodTemplateRoutineId = s.MethodTemplateRoutineId,
                        MachineGrouping = s.MachineGrouping,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingProcessName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(a => a.Value).SingleOrDefault() : "",
                        ManufacturingTypeId = s.ManufacturingTypeId,
                        ManufacturingTypeName = s.ManufacturingType!=null? s.ManufacturingType.CodeValue : "",
                        TemplateName = s.TemplateName,
                        MethodTemplateNo = s.MethodTemplateNo,
                        LanguageIDs = s.MethodTemplateRoutineLanguage != null ? s.MethodTemplateRoutineLanguage.Select(t => t.LanguageId).ToList() : new List<long?>(),
                       
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate

                    };
                    methodTemplateRoutineModels.Add(methodTemplateRoutineModel);
                });
            }
            
            return methodTemplateRoutineModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetMethodTemplateRoutine/{id:int}")]
        public ActionResult<MethodTemplateRoutineModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var methodTemplateRoutine = _context.MethodTemplateRoutine.SingleOrDefault(p => p.MethodTemplateRoutineId == id.Value);
            var result = _mapper.Map<MethodTemplateRoutineModel>(methodTemplateRoutine);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MethodTemplateRoutineModel> GetData(SearchModel searchModel)
        {
            var methodTemplateRoutine = new MethodTemplateRoutine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).FirstOrDefault();
                        break;
                    case "Last":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).LastOrDefault();
                        break;
                    case "Next":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).LastOrDefault();
                        break;
                    case "Previous":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).FirstOrDefault();
                        break;
                    case "Last":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).LastOrDefault();
                        break;
                    case "Next":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderBy(o => o.MethodTemplateRoutineId).FirstOrDefault(s => s.MethodTemplateRoutineId > searchModel.Id);
                        break;
                    case "Previous":
                        methodTemplateRoutine = _context.MethodTemplateRoutine.OrderByDescending(o => o.MethodTemplateRoutineId).FirstOrDefault(s => s.MethodTemplateRoutineId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<MethodTemplateRoutineModel>(methodTemplateRoutine);
            if (result != null)
            {
                result.LanguageIDs = _context.MethodTemplateRoutineLanguage.Where(s => s.MethodTemplateRoutineId == result.MethodTemplateRoutineId).Select(l => l.LanguageId).ToList();
            }
                return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertMethodTemplateRoutine")]
        public MethodTemplateRoutineModel Post(MethodTemplateRoutineModel value)
        {
            var methodTemplateRoutine = new MethodTemplateRoutine
            {
                MachineGrouping = value.MachineGrouping,
                ManufacturingProcessId = value.ManufacturingProcessId,
                ManufacturingTypeId = value.ManufacturingTypeId,
                MethodTemplateNo = value.MethodTemplateNo,
                TemplateName=value.TemplateName,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            value.LanguageIDs.ForEach(l =>
            {
                MethodTemplateRoutineLanguage methodTemplateRoutineLanguage = new MethodTemplateRoutineLanguage
                {
                    LanguageId = l,
                    MethodTemplateRoutineId = value.MethodTemplateRoutineId
                };
                methodTemplateRoutine.MethodTemplateRoutineLanguage.Add(methodTemplateRoutineLanguage);
            });
            _context.MethodTemplateRoutine.Add(methodTemplateRoutine);
            _context.SaveChanges();
            value.MethodTemplateRoutineId = methodTemplateRoutine.MethodTemplateRoutineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMethodTemplateRoutine")]
        public MethodTemplateRoutineModel Put(MethodTemplateRoutineModel value)
        {
            var methodTemplateRoutineModel = _context.MethodTemplateRoutine.Include(l => l.MethodTemplateRoutineLanguage).SingleOrDefault(p => p.MethodTemplateRoutineId == value.MethodTemplateRoutineId);
            methodTemplateRoutineModel.MachineGrouping = value.MachineGrouping;
            methodTemplateRoutineModel.ManufacturingProcessId = value.ManufacturingProcessId;
            methodTemplateRoutineModel.ManufacturingTypeId = value.ManufacturingTypeId;
            methodTemplateRoutineModel.TemplateName = value.TemplateName;
            methodTemplateRoutineModel.MethodTemplateNo = value.MethodTemplateNo;
            methodTemplateRoutineModel.ModifiedByUserId = value.ModifiedByUserID;
            methodTemplateRoutineModel.ModifiedDate = DateTime.Now;
            methodTemplateRoutineModel.StatusCodeId = value.StatusCodeID.Value;
            if (methodTemplateRoutineModel.MethodTemplateRoutineLanguage != null)
            {
                _context.MethodTemplateRoutineLanguage.RemoveRange(methodTemplateRoutineModel.MethodTemplateRoutineLanguage);
                _context.SaveChanges();
            }
            value.LanguageIDs.ForEach(l =>
            {
                MethodTemplateRoutineLanguage methodTemplateRoutineLanguage = new MethodTemplateRoutineLanguage
                {
                    LanguageId = l,
                    MethodTemplateRoutineId = value.MethodTemplateRoutineId
                };
                _context.MethodTemplateRoutineLanguage.Add(methodTemplateRoutineLanguage);
            });
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMethodTemplateRoutine")]
        public void Delete(int id)
        {
            var methodTemplateRoutine = _context.MethodTemplateRoutine.SingleOrDefault(p => p.MethodTemplateRoutineId == id);
            if (methodTemplateRoutine != null)
            {
                if (methodTemplateRoutine.MethodTemplateRoutineLanguage != null)
                {
                    _context.MethodTemplateRoutineLanguage.RemoveRange(methodTemplateRoutine.MethodTemplateRoutineLanguage);
                    _context.SaveChanges();
                }
                if (methodTemplateRoutine.MethodTemplateRoutineLine != null)
                {
                    _context.MethodTemplateRoutineLine.RemoveRange(methodTemplateRoutine.MethodTemplateRoutineLine);
                    _context.SaveChanges();
                }
                _context.MethodTemplateRoutine.Remove(methodTemplateRoutine);
                _context.SaveChanges();
            }
        }
    }
}