﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemClassificationHeaderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ItemClassificationHeaderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetItemClassificationHeadersAll")]
        public List<ItemClassificationHeaderModel> GetItemClassificationHeadersAll()
        {
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var itemClassificationHeader = _context.ItemClassificationHeader
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("Profile").OrderByDescending(o => o.ItemClassificationHeaderId).AsNoTracking().ToList();
            if (itemClassificationHeader != null && itemClassificationHeader.Count > 0)
            {
                List<long?> masterIds = itemClassificationHeader.Where(w => w.MaterialId != null).Select(a => a.MaterialId).Distinct().ToList();
                masterIds.AddRange(itemClassificationHeader.Where(w => w.Uomid != null).Select(a => a.Uomid).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                itemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel
                    {
                        ItemClassificationHeaderID = s.ItemClassificationHeaderId,
                        ProfileID = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MaterialNo = s.MaterialNo,
                        MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        MaterialID = s.MaterialId,
                        AlsoKnownAs = s.AlsoKnownAs,
                        UOMID = s.Uomid,
                        UOM = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        FPMaterial = s.LinkProfileReferenceNo + '|' + s.MaterialName,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                    };
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });

            }

            return itemClassificationHeaderModels;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetItemClassificationHeaders")]
        public List<ItemClassificationHeaderModel> Get(int id)
        {
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var itemClassificationHeader = _context.ItemClassificationHeader
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Profile").Where(i => i.ItemClassificationMasterId == id).OrderByDescending(o => o.ItemClassificationHeaderId).AsNoTracking().ToList();
            if (itemClassificationHeader != null && itemClassificationHeader.Count > 0)
            {
                List<long?> masterIds = itemClassificationHeader.Where(w => w.MaterialId != null).Select(a => a.MaterialId).Distinct().ToList();
                masterIds.AddRange(itemClassificationHeader.Where(w => w.Uomid != null).Select(a => a.Uomid).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                itemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel
                    {
                        ItemClassificationHeaderID = s.ItemClassificationHeaderId,
                        ProfileID = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MaterialNo = s.MaterialNo,
                        MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        MaterialID = s.MaterialId,
                        AlsoKnownAs = s.AlsoKnownAs,
                        UOMID = s.Uomid,
                        UOM = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        FPMaterial = s.MaterialNo + '|' + s.MaterialName,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                    };
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });
            }

            if (itemClassificationHeaderModels != null && itemClassificationHeaderModels.Count > 0)
            {
                itemClassificationHeaderModels.ForEach(i =>
                {
                    i.FPMaterial = i.MaterialNo + '|' + i.MaterialName;
                });
            }
            //var result = _mapper.Map<List<ItemClassificationHeaderModel>>(ItemClassificationHeader);
            return itemClassificationHeaderModels;
        }

        [HttpGet]
        [Route("GetItemClassificationHeadersDropdown")]
        public List<ItemClassificationHeaderModel> GetItemClassificationHeadersDropdown()
        {
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var itemClassificationHeader = _context.ItemClassificationHeader
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("Profile")
               .Include(i => i.ItemClassificationMaster)
               .Where(h => h.ItemClassificationMaster.ClassificationTypeId == 1040).OrderByDescending(o => o.ItemClassificationHeaderId).AsNoTracking().ToList();
            if (itemClassificationHeader != null && itemClassificationHeader.Count > 0)
            {
                List<long?> masterIds = itemClassificationHeader.Where(w => w.MaterialId != null).Select(a => a.MaterialId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                itemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel
                    {
                        ItemClassificationHeaderID = s.ItemClassificationHeaderId,
                        MaterialNo = s.LinkProfileReferenceNo,
                        MaterialID = s.MaterialId,
                        MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        FPMaterial = s.LinkProfileReferenceNo + '|' + (applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : ""),
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                    };
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });
            }

            return itemClassificationHeaderModels;
        }

        [HttpGet]
        [Route("GetItemClassificationHeadersDropdownMATItems")]
        public List<ItemClassificationHeaderModel> GetItemClassificationHeadersDropdownMATItems()
        {
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var itemClassificationHeader = _context.ItemClassificationHeader
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("Profile")
               .Include(i => i.ItemClassificationMaster)
               .Where(h => h.ItemClassificationMaster.ClassificationTypeId == 1040).Where(t => t.MaterialNo.StartsWith("MAT")).OrderByDescending(o => o.ItemClassificationHeaderId).AsNoTracking().ToList();
            if (itemClassificationHeader != null && itemClassificationHeader.Count > 0)
            {
                List<long?> masterIds = itemClassificationHeader.Where(w => w.MaterialId != null).Select(a => a.MaterialId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                itemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel
                    {
                        ItemClassificationHeaderID = s.ItemClassificationHeaderId,
                        MaterialNo = s.LinkProfileReferenceNo,
                        MaterialID = s.MaterialId,
                        MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        FPMaterial = s.LinkProfileReferenceNo + '|' + s.MaterialName,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                    };
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });
            }

            return itemClassificationHeaderModels;
        }

        [HttpGet]
        [Route("GetItemClassificationHeadersByRefNo")]
        public List<ItemClassificationHeaderModel> GetItemClassificationHeadersByRefNo(string refNo)
        {
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var itemClassificationHeader = _context.ItemClassificationHeader
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("Profile").Where(h => h.LinkProfileReferenceNo == refNo).OrderByDescending(o => o.ItemClassificationHeaderId).AsNoTracking().ToList();
            if (itemClassificationHeader != null && itemClassificationHeader.Count > 0)
            {
                List<long?> masterIds = itemClassificationHeader.Where(w => w.MaterialId != null).Select(a => a.MaterialId).Distinct().ToList();
                masterIds.AddRange(itemClassificationHeader.Where(w => w.Uomid != null).Select(a => a.Uomid).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                itemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel
                    {
                        ItemClassificationHeaderID = s.ItemClassificationHeaderId,
                        ProfileID = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MaterialNo = s.MaterialNo,
                        MaterialID = s.MaterialId,
                        MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        AlsoKnownAs = s.AlsoKnownAs,
                        UOMID = s.Uomid,
                        UOM = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        FPMaterial = s.MaterialNo + '|' + s.MaterialName,
                    };
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });
            }

            return itemClassificationHeaderModels;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertItemClassificationHeader")]
        public ItemClassificationHeaderModel Post(ItemClassificationHeaderModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MaterialName });
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var itemClassificationHeader = new ItemClassificationHeader
            {
                ProfileId = value.ProfileID,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                MaterialNo = value.MaterialNo,
                MaterialName = value.MaterialName,
                MaterialId = value.MaterialID,
                AlsoKnownAs = value.AlsoKnownAs,
                Uomid = value.UOMID,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LinkProfileReferenceNo = profileNo,
            };
            _context.ItemClassificationHeader.Add(itemClassificationHeader);
            _context.SaveChanges();
            value.LinkProfileReferenceNo = itemClassificationHeader.LinkProfileReferenceNo;
            value.ItemClassificationHeaderID = itemClassificationHeader.ItemClassificationHeaderId;
            value.UOM = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.UOMID).Select(a => a.Value).SingleOrDefault() : "";
            value.MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.MaterialID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemClassificationHeader")]
        public ItemClassificationHeaderModel Put(ItemClassificationHeaderModel value)
        {
            var itemClassificationHeader = _context.ItemClassificationHeader.SingleOrDefault(p => p.ItemClassificationHeaderId == value.ItemClassificationHeaderID);
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            itemClassificationHeader.ProfileId = value.ProfileID;
            itemClassificationHeader.ItemClassificationMasterId = value.ItemClassificationMasterId;
            itemClassificationHeader.MaterialNo = value.MaterialNo;
            itemClassificationHeader.MaterialName = value.MaterialName;
            itemClassificationHeader.MaterialId = value.MaterialID;
            itemClassificationHeader.AlsoKnownAs = value.AlsoKnownAs;
            itemClassificationHeader.Uomid = value.UOMID;
            itemClassificationHeader.ModifiedByUserId = value.ModifiedByUserID;
            itemClassificationHeader.ModifiedDate = DateTime.Now;
            itemClassificationHeader.StatusCodeId = value.StatusCodeID.Value;
            if (itemClassificationHeader.LinkProfileReferenceNo == null)
            {
                var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MaterialName });
                itemClassificationHeader.LinkProfileReferenceNo = profileNo;
                value.LinkProfileReferenceNo = profileNo;
            }
            _context.SaveChanges();
            value.UOM = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.UOMID).Select(a => a.Value).SingleOrDefault() : "";
            value.MaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.MaterialID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemClassificationHeader")]
        public void Delete(int id)
        {
            var itemClassificationHeader = _context.ItemClassificationHeader.SingleOrDefault(p => p.ItemClassificationHeaderId == id);
            if (itemClassificationHeader != null)
            {
                _context.ItemClassificationHeader.Remove(itemClassificationHeader);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("GetClassificationList")]
        public ClassificationListingModel GetClassificationList(ClassificationParam classificationParam)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();

            switch (classificationParam.ClassificationTypeId)
            {
                case 1040:
                    commonPackagingListingModel = GetClassificationMaterialFormList(classificationParam.ClassificationId);
                    break;
                case 1041:
                    commonPackagingListingModel = GetClassificationPackagingList(classificationParam.ClassificationId);
                    break;
                case 1042:
                    commonPackagingListingModel = GetClassificationFPItemList(classificationParam.ClassificationId);
                    break;
                case 1043:
                    commonPackagingListingModel = GetClassificationCompanyList(classificationParam.ClassificationId);
                    break;
                case 1044:
                    commonPackagingListingModel = GetClassificationCommonProcessList(classificationParam.ClassificationId);
                    break;
                case 1045:
                    commonPackagingListingModel = GetClassificationMachineList(classificationParam.ClassificationId);
                    break;

                default:
                    break;
            }


            return commonPackagingListingModel;
        }

        private ClassificationListingModel GetClassificationPackagingList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {
                new Header{Text="Type",Value="typeName",Align="left" },
                new Header{Text="Profile",Value="profileName",Align="left" },
                new Header{Text="Profile",Value="profileReferenceNo",Align="left" },
                new Header{Text="Also Known As",Value="alsoKownAs",Align="left" },
                new Header{Text="Uom Name",Value="uomName",Align="left" },
                new Header{Text="Packaging Item Name",Value="packagingItemName",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },
            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();
            jsonFields.Add("Type", "typeName");
            jsonFields.Add("Profile Name", "profileName");
            jsonFields.Add("Profile", "profileReferenceNo");
            jsonFields.Add("Also Known As", "alsoKownAs");
            jsonFields.Add("Uom Name", "uomName");
            jsonFields.Add("Packaging Item Name", "packagingItemName");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<CommonPackagingItemHeaderModel> commonPackagingItemHeaderModels = new List<CommonPackagingItemHeaderModel>();
            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).Select(i => i.ItemClassificationId).ToList();
            var commonPackingItems = _context.CommonPackagingItemHeader.Include(p => p.Profile).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).ToList();
            if (commonPackingItems != null && commonPackingItems.Count > 0)
            {
                List<long?> masterIds = commonPackingItems.Where(w => w.UomId != null).Select(a => a.UomId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackingItems.ForEach(c =>
                {
                    CommonPackagingItemHeaderModel commonPackagingItemHeaderModel = new CommonPackagingItemHeaderModel
                    {
                        CommonPackagingItemId = c.CommonPackagingItemId,
                        ItemClassificationMasterId = c.ItemClassificationMasterId,
                        TypeName = c.ItemClassificationMaster != null ? c.ItemClassificationMaster.Name : null,
                        ProfileName = c.Profile != null ? c.Profile.Name : null,
                        ProfileReferenceNo = c.ProfileReferenceNo,
                        AlsoKownAs = c.AlsoKownAs,
                        UomName = c.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == c.UomId).FirstOrDefault()?.Value : "",
                        PackagingItemName = c.PackagingItemName,
                        StatusCode = c.StatusCode?.CodeValue,
                        ModifiedByUser = c.ModifiedByUser != null ? c.ModifiedByUser.UserName == null ? c.AddedByUser.UserName : c.ModifiedByUser.UserName : "",
                        ModifiedDate = c.ModifiedDate == null ? c.AddedDate : c.ModifiedDate,
                    };

                    commonPackagingItemHeaderModels.Add(commonPackagingItemHeaderModel);
                });

            }
            commonPackagingItemHeaderModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }

        private ClassificationListingModel GetClassificationFPItemList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {

                new Header{Text="Profile Name",Value="profileName",Align="left" },
                new Header{Text="Sunward Manufacturing Group No",Value="linkProfileReferenceNo",Align="left" },
                new Header{Text="SW Manufacture Group Name",Value="profileMaster",Align="left" },
                new Header{Text="Dosage Form Name",Value="dosageForm",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },




            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();
            jsonFields.Add("Profile Name", "profileName");
            jsonFields.Add("Sunward Manufacturing Group No", "linkProfileReferenceNo");
            jsonFields.Add("SW Manufacture Group Name", "profileMaster");
            jsonFields.Add("Dosage Form Name", "dosageForm");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<FPCommonFieldModel> fPCommonFieldModels = new List<FPCommonFieldModel>();
            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).Select(i => i.ItemClassificationId).ToList();
            var fpcommonField = _context.FpcommonField.Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).OrderByDescending(o => o.FpcommonFieldId).ToList();
            if (fpcommonField != null && fpcommonField.Count > 0)
            {
                fpcommonField.ForEach(c =>
                {
                    FPCommonFieldModel fPCommonFieldModel = new FPCommonFieldModel
                    {
                        FPCommonFieldID = c.FpcommonFieldId,
                        FPNumberID = c.FpnumberId,
                        Profile = c.FpnumberNavigation != null ? c.FpnumberNavigation.Name : "",
                        ProfileName = c.FpnumberNavigation != null ? c.FpnumberNavigation.Name : "",
                        FinishProductID = c.FinishProductId,
                        FPNumber = c.Fpnumber,
                        ProfileMaster = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == c.FinishProductId).Select(a => a.Value).SingleOrDefault() : "",
                        DosageForm = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == c.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                        DosageFormID = c.DosageFormId,
                        StatusCodeID = c.StatusCodeId,
                        StatusCode = c.StatusCode.CodeValue,
                        AddedByUser = c.AddedByUser != null ? c.AddedByUser.UserName : null,
                        AddedByUserID = c.AddedByUserId,
                        //ModifiedByUser = c.ModifiedByUser.UserName,
                        //ModifiedDate = c.ModifiedDate,
                        AddedDate = c.AddedDate,
                        ProfileReferenceNo = c.Fpnumber,
                        LinkProfileReferenceNo = c.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = c.MasterProfileReferenceNo,
                        ItemClassificationMasterId = c.ItemClassificationMasterId,
                        ModifiedByUser = c.ModifiedByUser != null ? c.ModifiedByUser.UserName == null ? c.AddedByUser.UserName : c.ModifiedByUser.UserName : "",
                        ModifiedDate = c.ModifiedDate == null ? c.AddedDate : c.ModifiedDate,
                    };
                    fPCommonFieldModels.Add(fPCommonFieldModel);
                });

            }
            if (fPCommonFieldModels != null && fPCommonFieldModels.Count > 0)
            {
                fPCommonFieldModels.ForEach(f =>
                {
                    f.DropDownName = f.Profile + " | " + f.ProfileMaster;

                });
            }

            fPCommonFieldModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }

        private ClassificationListingModel GetClassificationMaterialFormList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {
                new Header{Text="Reference No",Value="linkProfileReferenceNo",Align="left" },
                new Header{Text="Material Name",Value="materialName",Align="left" },
                new Header{Text="UOM",Value="uom",Align="left" },
                new Header{Text="Also Known As",Value="alsoKownAs",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },
            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();
            jsonFields.Add("Reference No", "linkProfileReferenceNo");
            jsonFields.Add("Material Name", "materialName");
            jsonFields.Add("UOM", "uom");
            jsonFields.Add("Also Known As", "alsoKownAs");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ItemClassificationHeaderModel> itemClassificationHeaderModels = new List<ItemClassificationHeaderModel>();

            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).AsNoTracking().Select(i => i.ItemClassificationId).ToList();
            var ItemClassificationHeader = _context.ItemClassificationHeader.Include(p => p.Profile).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).AsNoTracking().ToList();
            if (ItemClassificationHeader != null && ItemClassificationHeader.Count > 0)
            {
                ItemClassificationHeader.ForEach(s =>
                {
                    ItemClassificationHeaderModel itemClassificationHeaderModel = new ItemClassificationHeaderModel();

                    itemClassificationHeaderModel.ItemClassificationHeaderID = s.ItemClassificationHeaderId;
                    itemClassificationHeaderModel.ProfileID = s.ProfileId;
                    itemClassificationHeaderModel.ProfileName = s.Profile != null ? s.Profile.Name : null;
                    itemClassificationHeaderModel.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                    itemClassificationHeaderModel.MaterialNo = s.MaterialNo;
                    itemClassificationHeaderModel.MaterialName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).SingleOrDefault() : "";
                    itemClassificationHeaderModel.MaterialID = s.MaterialId;
                    itemClassificationHeaderModel.AlsoKnownAs = s.AlsoKnownAs;
                    itemClassificationHeaderModel.UOMID = s.Uomid;
                    itemClassificationHeaderModel.UOM = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "";
                    itemClassificationHeaderModel.StatusCodeID = s.StatusCodeId;
                    itemClassificationHeaderModel.StatusCode = s.StatusCode == null ? s.StatusCode.CodeValue : null;
                    itemClassificationHeaderModel.AddedByUserID = s.AddedByUserId;
                    itemClassificationHeaderModel.ModifiedByUserID = s.ModifiedByUserId;
                    //AddedByUser = s.AddedByUser.UserName,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    itemClassificationHeaderModel.AddedDate = s.AddedDate;
                    //ModifiedDate = s.ModifiedDate,
                    itemClassificationHeaderModel.FPMaterial = s.LinkProfileReferenceNo + '|' + s.MaterialName;
                    itemClassificationHeaderModel.ItemClassificationMasterId = s.ItemClassificationMasterId;
                    itemClassificationHeaderModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName == null ? s.AddedByUser.UserName : s.ModifiedByUser.UserName : "";
                    itemClassificationHeaderModel.ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate;
                    itemClassificationHeaderModels.Add(itemClassificationHeaderModel);
                });
            }

            itemClassificationHeaderModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }

        private ClassificationListingModel GetClassificationCompanyList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {

                new Header{Text="Profile Name",Value="profileName",Align="left" },
                new Header{Text="Profile No",Value="profileReferenceNo",Align="left" },
                new Header{Text="Company Name",Value="companyListingName",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },

            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();

            jsonFields.Add("Profile Name", "profileName");
            jsonFields.Add("Profile", "profileReferenceNo");

            jsonFields.Add("Company Name", "companyListingName");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var companylistingtype = _context.CompanyListingCompanyType.AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModels = new List<CompanyListingModel>();
            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).AsNoTracking().Select(i => i.ItemClassificationId).ToList();
            var companyListing = _context.CompanyListing.Include(p => p.Profile).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).AsNoTracking().ToList();
            if (companyListing != null && companyListing.Count > 0)
            {
                companyListing.ForEach(s =>
                {
                    CompanyListingModel companyListingModel = new CompanyListingModel
                    {
                        CompanyListingID = s.CompanyListingId,
                        CompanyListingName = s.CompanyName,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        //ModifiedByUser = s.ModifiedByUser.UserName,
                        StatusCode = s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        //ModifiedDate = s.ModifiedDate,
                        ProfileID = s.ProfileId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        No = s.No,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName == null ? s.AddedByUser.UserName : s.ModifiedByUser.UserName : "",
                        ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate,
                        CompanyTypeIds = companylistingtype.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CompanyTypeId).ToList(),

                    };
                    companyListingModels.Add(companyListingModel);
                });
            }
            companyListingModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }

        private ClassificationListingModel GetClassificationMachineList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {
                new Header{Text="Manufacturing Site",Value="manufacturingSiteName",Align="left" },
                new Header{Text="Manufacturing Process",Value="manufacturingProcessName",Align="left" },
                new Header{Text="Manufacturing Steps",Value="manufacturingStepsName",Align="left" },
                new Header{Text="Machine Grouping",Value="machineGroupingName",Align="left" },
                new Header{Text="Machine No",Value="machineNo",Align="left" },
                new Header{Text="Name Of The Machine",Value="nameOfTheMachine",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },


            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();
            jsonFields.Add("Manufacturing Site", "manufacturingSiteName");
            jsonFields.Add("Manufacturing Process", "manufacturingProcessName");
            jsonFields.Add("Manufacturing Steps", "manufacturingStepsName");
            jsonFields.Add("Machine Grouping", "machineGroupingName");
            jsonFields.Add("Machine No", "machineNo");
            jsonFields.Add("Name Of The Machine", "nameOfTheMachine");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModels = new List<CommonFieldsProductionMachineModel>();
            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).AsNoTracking().Select(i => i.ItemClassificationId).ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine.Include(p => p.Profile).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.LocationNavigation).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).AsNoTracking().ToList();
            if (commonFieldsProductionMachine != null && commonFieldsProductionMachine.Count > 0)
            {
                commonFieldsProductionMachine.ForEach(s =>
                {
                    CommonFieldsProductionMachineModel commonFieldsProductionMachineModel = new CommonFieldsProductionMachineModel
                    {
                        CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingStepsId = s.ManufacturingStepsId,
                        MachineGroupingId = s.MachineGroupingId,
                        ManufacturingSiteName = s.ManufacturingSiteId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingProcessName = s.ManufacturingProcessId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingStepsName = s.ManufacturingStepsId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                        MachineGroupingName = s.MachineGroupingId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "",
                        MachineNo = s.MachineNo,
                        NameOfTheMachine = s.NameOfTheMachine,
                        Location = s.Location,
                        LocationName = s.LocationNavigation != null ? s.LocationNavigation.Name : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        //ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        //ModifiedByUser = s.ModifiedByUser.UserName,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName == null ? s.AddedByUser.UserName : s.ModifiedByUser.UserName : null,
                        ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate,

                    };
                    commonFieldsProductionMachineModels.Add(commonFieldsProductionMachineModel);
                });
            }

            commonFieldsProductionMachineModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }
        [HttpPost]
        [Route("GetItemClassificationMachineDropdown")]
        public List<CommonFieldsProductionMachineModel> GetItemClassificationMachineDropdown(ClassificationMachineModel machineModel)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModels = new List<CommonFieldsProductionMachineModel>();
           // var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == 1045).AsNoTracking().Select(i => i.ItemClassificationId).ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine.Where(h =>  h.ManufacturingSiteId == machineModel.ManufacturingSiteId && h.ManufacturingProcessId == machineModel.ManufacturingProcessId  && h.ManufacturingStepsId == machineModel.ManufacturingStepsId).AsNoTracking().ToList();
            if (commonFieldsProductionMachine != null && commonFieldsProductionMachine.Count > 0)
            {
                commonFieldsProductionMachine.ForEach(s =>
                {
                    CommonFieldsProductionMachineModel commonFieldsProductionMachineModel = new CommonFieldsProductionMachineModel
                    {
                        CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingStepsId = s.ManufacturingStepsId,
                        MachineGroupingId = s.MachineGroupingId,
                        ManufacturingSiteName = s.ManufacturingSiteId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingProcessName = s.ManufacturingProcessId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingStepsName = s.ManufacturingStepsId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                        MachineGroupingName = s.MachineGroupingId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "",
                        MachineNo = s.MachineNo,
                        NameOfTheMachine = s.NameOfTheMachine,
                        Location = s.Location,                      
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        //ModifiedDate = s.ModifiedDate,                    
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,                      
                        ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate,

                    };
                    commonFieldsProductionMachineModels.Add(commonFieldsProductionMachineModel);
                });
            }

          

            return commonFieldsProductionMachineModels;
        } 

private ClassificationListingModel GetClassificationCommonProcessList(long? classificationId)
        {
            ClassificationListingModel commonPackagingListingModel = new ClassificationListingModel();
            List<Header> headers = new List<Header>
            {
                new Header{Text="Type",Value="typeName",Align="left" },
                new Header{Text="Profile Name",Value="profileName",Align="left" },
                new Header{Text="Profile Reference No",Value="profileReferenceNo",Align="left" },
                new Header{Text="Dosage Name",Value="dosageFormName",Align="left" },
                new Header{Text="Manufacturing Process",Value="manufacturingProcessName",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                new Header{Text="",Value="",Align="left" },

            };

            Dictionary<string, string> jsonFields = new Dictionary<string, string>();
            jsonFields.Add("Type", "typeName");
            jsonFields.Add("Profile Name", "profileName");
            jsonFields.Add("Profile Reference No", "profileReferenceNo");
            jsonFields.Add("Dosage Name", "dosageFormName");
            jsonFields.Add("Manufacturing Process", "manufacturingProcessName");
            jsonFields.Add("Status", "statusCode");
            jsonFields.Add("Modified By", "modifiedByUser");
            jsonFields.Add("Modified Date", "modifiedDate");


            commonPackagingListingModel.Headers = headers;
            commonPackagingListingModel.JSONFields = jsonFields;
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<CommonProcessModel> commonProcessModels = new List<CommonProcessModel>();
            var itemClassificationIds = _context.ItemClassificationMaster.Where(p => p.ParentId == classificationId).AsNoTracking().Select(i => i.ItemClassificationId).ToList();
            var commonProcess = _context.CommonProcess.Include(p => p.Profile).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.ItemClassificationMaster).Where(h => itemClassificationIds.Contains(h.ItemClassificationMasterId.Value)).AsNoTracking().ToList();
            if(commonProcess!=null && commonProcess.Count>0)
            {
                commonProcess.ForEach(s =>
                {
                    CommonProcessModel commonProcessModel = new CommonProcessModel
                    {
                        CommonProcessId = s.CommonProcessId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile!=null? s.Profile.Name : "",
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        //ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ModifiedByUser = s.ModifiedByUser!=null? s.ModifiedByUser.UserName == null ? s.AddedByUser.UserName : s.ModifiedByUser.UserName : "",
                        ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate,
                    };
                    commonProcessModels.Add(commonProcessModel);
                });

            }
           
            commonProcessModels.ForEach(c =>
            {
                commonPackagingListingModel.DynamicListItems.Add(c as dynamic);
            });

            return commonPackagingListingModel;

        }
    }
}