﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApptransferRelcassLinesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        // public NAV.NAV Context { get; private set; }
        private readonly IConfiguration _config;
        public ApptransferRelcassLinesController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;

            _config = config;
            //Context = new NAV.NAV(new Uri($"{_config["NAV:OdataUrl"]}/Company('{_config["NAV:Company"]}')"))
            //{
            //    Credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"])
            //};
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApptransferReclassEntrysByID")]
        public List<ApptransferRelcassLinesModel> Get(long Id)
        {
            var apptransferrelcasslines = _context.ApptransferRelcassLines.Select(s => new ApptransferRelcassLinesModel
            {
                ApptransferRelcassLineID = s.ApptransferRelcassLineId,
                TransferRelcassID = s.TransferRelcassId,
                DrumNo = s.DrumNo,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                Quantity = s.Quantity.GetValueOrDefault(0),
                PostedtoNav = s.PostedtoNav.GetValueOrDefault(false)

            }).OrderByDescending(o => o.ApptransferRelcassLineID).Where(w => w.TransferRelcassID == Id && (w.PostedtoNav == false || w.PostedtoNav == null)).AsNoTracking().ToList();
            return apptransferrelcasslines;
        }
        [HttpGet]
        [Route("GetApptransferReclassEntrys")]
        public List<ApptransferRelcassLinesModel> Get()
        {
            var apptransferrelcasslines = _context.ApptransferRelcassLines.Select(s => new ApptransferRelcassLinesModel
            {

                ApptransferRelcassLineID = s.ApptransferRelcassLineId,
                TransferRelcassID = s.TransferRelcassId,
                DrumNo = s.DrumNo,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                Quantity = s.Quantity.GetValueOrDefault(0)

            }).OrderByDescending(o => o.ApptransferRelcassLineID).AsNoTracking().ToList();
            return apptransferrelcasslines;
        }

        //Changes Done by Aravinth Start 18JUL2019

        [HttpPost]
        [Route("GetApptransferRelcassEntryLineFilter")]
        public List<ApptransferRelcassLinesModel> GetFilter(ApptransferRelcassLinesSearchModel value)
        {
            var appTransferReclassEntryLine = _context.ApptransferRelcassLines.Select(s => new ApptransferRelcassLinesModel
            {
                ApptransferRelcassLineID = s.ApptransferRelcassLineId,
                DrumNo = s.DrumNo,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                Quantity = s.Quantity.GetValueOrDefault(0)

            }).OrderByDescending(o => o.ApptransferRelcassLineID).AsNoTracking().ToList();

            List<ApptransferRelcassLinesModel> filteredItems = new List<ApptransferRelcassLinesModel>();
            if (value.DrumNo.Any())
            {
                var itemDrumNoFilters = appTransferReclassEntryLine.Where(p => p.DrumNo != null ? (p.DrumNo.ToLower() == (value.DrumNo.ToLower())) : false).ToList();
                itemDrumNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ProdOrderNo.Any())
            {
                var itemProdOrderNoFilters = appTransferReclassEntryLine.Where(p => p.ProdOrderNo != null ? (p.ProdOrderNo.ToLower() == (value.ProdOrderNo.ToLower())) : false).ToList();
                itemProdOrderNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ProdLineNo.ToString().Any())
            {
                var itemProdLineNoFilters = appTransferReclassEntryLine.Where(p => p.ProdLineNo.ToString() != null ? (p.ProdLineNo.ToString().ToLower() == (value.ProdLineNo.ToString().ToLower())) : false).ToList();
                itemProdLineNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ItemNo.Any())
            {
                var itemItemNoFilters = appTransferReclassEntryLine.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (value.ItemNo.ToLower())) : false).ToList();
                itemItemNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.Description.Any())
            {
                var itemDescriptionFilters = appTransferReclassEntryLine.Where(p => p.Description != null ? (p.Description.ToLower() == (value.Description.ToLower())) : false).ToList();
                itemDescriptionFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.LotNo.Any())
            {
                var itemLotNoFilters = appTransferReclassEntryLine.Where(p => p.LotNo != null ? (p.LotNo.ToLower() == (value.LotNo.ToLower())) : false).ToList();
                itemLotNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.QcrefNo.Any())
            {
                var itemQcrefNoFilters = appTransferReclassEntryLine.Where(p => p.QcrefNo != null ? (p.QcrefNo.ToLower() == (value.QcrefNo.ToLower())) : false).ToList();
                itemQcrefNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.BatchNo.Any())
            {
                var itemBatchNoFilters = appTransferReclassEntryLine.Where(p => p.BatchNo != null ? (p.BatchNo.ToLower() == (value.BatchNo.ToLower())) : false).ToList();
                itemBatchNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.Quantity.ToString().Any())
            {
                var itemQuantityFilters = appTransferReclassEntryLine.Where(p => p.Quantity != null ? (p.Quantity.ToString().ToLower() == (value.Quantity.ToString().ToLower())) : false).ToList();
                itemQuantityFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.ApptransferRelcassLineID == i.ApptransferRelcassLineID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }

            return filteredItems;
        }

        [HttpGet]
        [Route("GetApptransferRelcassEntryLineItemByID")]
        public ApptransferRelcassLinesModel GetApptransferRelcassEntryLineItemByID(long Id)
        {
            var appTransferReclassEntry = _context.ApptransferRelcassLines.Select(s => new ApptransferRelcassLinesModel
            {

                ApptransferRelcassLineID = s.ApptransferRelcassLineId,
                TransferRelcassID = s.TransferRelcassId,
                DrumNo = s.DrumNo,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                Quantity = s.Quantity.GetValueOrDefault(0),
                ReceiveQuantity = s.ReceiveQuantity.GetValueOrDefault(0)

            }).OrderByDescending(o => o.ApptransferRelcassLineID).FirstOrDefault(t => t.ApptransferRelcassLineID == Id);

            return appTransferReclassEntry;
        }

        //Changes Done by Aravinth End 18JUL2019

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApptransferRelcassLinesModel> GetData(SearchModel searchModel)
        {
            var apptransferrelcasslines = new ApptransferRelcassLines();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).LastOrDefault();
                        break;
                    case "Previous":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderBy(o => o.ApptransferRelcassLineId).FirstOrDefault(s => s.ApptransferRelcassLineId > searchModel.Id);
                        break;
                    case "Previous":
                        apptransferrelcasslines = _context.ApptransferRelcassLines.OrderByDescending(o => o.ApptransferRelcassLineId).FirstOrDefault(s => s.ApptransferRelcassLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApptransferRelcassLinesModel>(apptransferrelcasslines);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApptransferReclassLine")]
        public ApptransferRelcassLinesModel Post(ApptransferRelcassLinesModel value)
        {
            if (value.ApptransferRelcassLineID > 0)
            {
                return Put(value);
            }
            var apptransferrelcasslines = new ApptransferRelcassLines
            {
                TransferRelcassId = value.TransferRelcassID,
                DrumNo = value.DrumNo,
                ProdOrderNo = value.ProdOrderNo,
                ProdLineNo = 10000,
                ItemNo = value.ItemNo,
                Description = value.Description,
                LotNo = value.LotNo,
                QcrefNo = value.QcrefNo,
                BatchNo = value.BatchNo,
                Quantity = value.Quantity,
                ReceiveQuantity = value.ReceiveQuantity
            };
            _context.ApptransferRelcassLines.Add(apptransferrelcasslines);

            _context.SaveChanges();

            value.ApptransferRelcassLineID = apptransferrelcasslines.ApptransferRelcassLineId;
            return value;
        }


        [HttpPost]
        [Route("PostTransferReclass")]
        public async Task<ApptransferRelcassEntryModel> PostTransferReclass(ApptransferRelcassEntryModel value)
        {
            try
            {

                var context = new DataService(_config, value.CompanyName);
                var guid = Guid.NewGuid();
                var ICtLocation = _context.Ictmaster.FirstOrDefault(l => l.Name == value.ToLocation);
                if (ICtLocation != null)
                {
                    var transferTo = ICtLocation.Name;
                    var transfer = _context.ApptransferRelcassEntry.Include("TransferFromLocation").FirstOrDefault(f => f.TransferReclassId == value.TransferReclassID);
                    transfer.TransferToLocationId = ICtLocation.IctmasterId;

                    var tansferSetting = _context.TransferSettings.FirstOrDefault(t => t.FromLocationId == transfer.TransferFromLocationId && t.ToLocationId == transfer.TransferToLocationId);
                    if (tansferSetting == null)
                    {
                        value.IsError = true;
                        value.Errormessage = "Transfer settings not exist in portal.";
                        return value;
                    }

                    var appuser = _context.ApplicationUser.FirstOrDefault(u => u.UserId == value.AddedByUserID);
                    if (appuser == null)
                    {
                        value.IsError = true;
                        value.Errormessage = "APP user session expired. Please login again.";
                        return value;
                    }
                    value.AddedByUser = appuser.LoginId;

                    var Entry_Type = tansferSetting.IsReclass.Value == true ? "Reclass" : "Ship";

                    var transferOrderLines = _context.ApptransferRelcassLines.Where(t => t.TransferRelcassId == value.TransferReclassID && (t.PostedtoNav == false || t.PostedtoNav == null)).ToList();
                    foreach (var cons in transferOrderLines)
                    {
                        int onlyThisAmount = 8;
                        string ticks = DateTime.Now.Ticks.ToString();
                        ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                        int entryNumber = int.Parse(ticks);
                        // SW Reference
                        //var consumption = new CRTIMS_TransferEntry()
                        //{
                        //    Entry_No = entryNumber,
                        //    Created_by = value.AddedByUser,
                        //    Description = cons.Description,
                        //    Entry_Type = "Create",
                        //    Item_No = cons.ItemNo,
                        //    Created_on = DateTime.Now,
                        //    Lot_No = cons.LotNo,
                        //    QC_Ref_No = cons.QcrefNo,
                        //    Order_Line_No = 0,
                        //    Posting_Date = DateTime.Now,
                        //    Posted_by = value.AddedByUser,
                        //    Session_ID = guid,
                        //    Transfer_from_Code = transfer.TransferFromLocation.Name,
                        //    Transfer_to_Code = ICtLocation.Name,
                        //    Quantity = cons.Quantity,
                        //    Session_User_ID = value.AddedByUser,
                        //};
                        //context.Context.AddToCRTIMS_TransferEntry(consumption);

                        var entity = context.Context.Entities;
                        TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                        var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                        var post = new TransferEntryService.CRTIMS_PostTransferEntry_PortClient();

                        post.Endpoint.Address =
                   new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/CRTIMS_PostTransferEntry"),
                   new DnsEndpointIdentity(""));

                        post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
                        post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
                        post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
                        post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
                        post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
                        post.ClientCredentials.Windows.AllowedImpersonationLevel =
                      System.Security.Principal.TokenImpersonationLevel.Impersonation;

                        var transferNo = await post.FnRunFromAppsAsync(entryNumber);
                        transfer.TransferOrderNo = transferNo.return_value;
                        await ShipTransfer(value, cons, transferNo.return_value, guid, transfer, transferTo, Entry_Type);

                        value.PostedtoNAV = true;
                        cons.PostedtoNav = true;
                        _context.SaveChanges();
                    }
                    return value;
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "To location is not valid. It is not exist in ICT Master.";
                    return value;
                }

            }
            catch (Exception ex)
            {
                value.PostedtoNAV = false;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;
                return value;
            }
        }

        [HttpPost]
        [Route("PostTransferReceive")]
        public async Task<ApptransferRelcassEntryModel> PostTransferReceive(ApptransferRelcassEntryModel value)
        {
            try
            {

                var context = new DataService(_config, value.CompanyName);
                var guid = Guid.NewGuid();

                //var ICtLocation = _context.Ictmaster.FirstOrDefault(l => l.Name == value.ReceiveLocation);

                //if (ICtLocation != null && ICtLocation.IctmasterId > 0)
                //{
                //    var transferTo = ICtLocation.Name;

                //    var transfer = _context.ApptransferRelcassEntry.Include("TransferFromLocation").FirstOrDefault(f => f.TransferReclassId == value.TransferReclassID);
                //    transfer.TransferToLocationId = ICtLocation.IctmasterId;

                //    var tansferSetting = _context.TransferSettings.FirstOrDefault(t => t.FromLocationId == transfer.TransferFromLocationId && t.ToLocationId == transfer.TransferToLocationId);
                //    if (tansferSetting == null)
                //    {
                //        value.IsError = true;
                //        value.Errormessage = "Transfer settings not exist in portal.";
                //        return value;
                //    }

                //    var appuser = _context.ApplicationUser.FirstOrDefault(u => u.UserId == value.AddedByUserID);
                //    if (appuser == null)
                //    {
                //        value.IsError = true;
                //        value.Errormessage = "APP user session expired. Please login again.";
                //        return value;
                //    }
                //    value.AddedByUser = appuser.LoginId;

                //    var Entry_Type = tansferSetting.IsReclass.Value == true ? "Reclass" : "Ship";

                //    var transferOrderLines = _context.ApptransferRelcassLines.Where(t => t.TransferRelcassId == value.TransferReclassID && (t.PostedtoNav == false || t.PostedtoNav == null)).ToList();
                //    foreach (var cons in transferOrderLines)
                //    {
                //        int onlyThisAmount = 8;
                //        string ticks = DateTime.Now.Ticks.ToString();
                //        ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                //        int entryNumber = int.Parse(ticks);
                //        var consumption = new CRTIMS_TransferEntry()
                //        {
                //            Entry_No = entryNumber,
                //            Created_by = value.AddedByUser,
                //            Description = cons.Description,
                //            Entry_Type = "Create",
                //            Item_No = cons.ItemNo,
                //            Created_on = DateTime.Now,
                //            Lot_No = cons.LotNo,
                //            QC_Ref_No = cons.QcrefNo,
                //            Order_Line_No = 0,
                //            Posting_Date = DateTime.Now,
                //            Posted_by = value.AddedByUser,
                //            Session_ID = guid,
                //            Transfer_from_Code = transfer.TransferFromLocation.Name,
                //            Transfer_to_Code = ICtLocation.Name,
                //            Quantity = cons.Quantity,
                //            User_ID = value.AddedByUser,
                //        };
                //        context.Context.AddToCRTIMS_TransferEntry(consumption);

                //        var entity = context.Context.Entities;
                //        TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                //        var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                //        var post = new TransferEntryService.CRTIMS_PostTransferEntry_PortClient();

                //        post.Endpoint.Address =
                //   new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/CRTIMS_PostTransferEntry"),
                //   new DnsEndpointIdentity(""));

                //        post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
                //        post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
                //        post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
                //        post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
                //        post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
                //        post.ClientCredentials.Windows.AllowedImpersonationLevel =
                //      System.Security.Principal.TokenImpersonationLevel.Impersonation;

                //        var transferNo = await post.FnRunFromAppsAsync(entryNumber);

                //        await ShipTransfer(value, cons, transferNo.return_value, guid, transfer, transferTo, Entry_Type);

                //        value.PostedtoNAV = true;
                //        cons.PostedtoNav = true;
                //        cons.ReceiveQuantity = cons.Quantity;
                //        _context.SaveChanges();
                //    }
                return value;
                //}
                //else
                //{
                //    value.IsError = true;
                //    value.Errormessage = "To location is not valid. It is not exist in ICT Master.";
                //    return value;
                //}

            }
            catch (Exception ex)
            {
                value.PostedtoNAV = false;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;
                return value;
            }
        }

        private async Task<bool> ShipTransfer(ApptransferRelcassEntryModel value, ApptransferRelcassLines cons, string transferNo, Guid guid, ApptransferRelcassEntry transfer, string transferTo, string Entry_Type)
        {


            var context = new DataService(_config, value.CompanyName);
            int onlyThisAmount = 8;
            string ticks = DateTime.Now.Ticks.ToString();
            ticks = ticks.Substring(ticks.Length - onlyThisAmount);
            int entryNumber = int.Parse(ticks);
            //var consumption = new CRTIMS_TransferEntry()
            //{
            //    Entry_No = entryNumber,
            //    Transfer_Order_No = transferNo,
            //    Created_by = value.AddedByUser,
            //    Description = cons.Description,
            //    Entry_Type = Entry_Type,
            //    Item_No = cons.ItemNo,
            //    Created_on = DateTime.Now,
            //    Lot_No = cons.LotNo,
            //    QC_Ref_No = cons.QcrefNo,
            //    Order_Line_No = cons.ProdLineNo,
            //    Posting_Date = DateTime.Now,
            //    Posted_by = value.AddedByUser,
            //    Session_ID = guid,
            //    Transfer_from_Code = transfer.TransferFromLocation.Name,
            //    Transfer_to_Code = transferTo,
            //    Quantity = cons.Quantity,
            //    Session_User_ID = value.AddedByUser,
            //    Session_Type = "Mobile",

            //};
            //context.Context.AddToCRTIMS_TransferEntry(consumption);

            var entity = context.Context.Entities;
            TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
            var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

            var post = new TransferEntryService.CRTIMS_PostTransferEntry_PortClient();

            post.Endpoint.Address =
       new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/CRTIMS_PostTransferEntry"),
       new DnsEndpointIdentity(""));

            post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
            post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
            post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
            post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
            post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
            post.ClientCredentials.Windows.AllowedImpersonationLevel =
          System.Security.Principal.TokenImpersonationLevel.Impersonation;

            await post.FnRunFromAppsAsync(entryNumber);

            return true;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApptransferReclassLine")]
        public ApptransferRelcassLinesModel Put(ApptransferRelcassLinesModel value)
        {
            var apptransferrelcasslines = _context.ApptransferRelcassLines.SingleOrDefault(p => p.ApptransferRelcassLineId == value.ApptransferRelcassLineID);

            apptransferrelcasslines.TransferRelcassId = value.TransferRelcassID;
            apptransferrelcasslines.DrumNo = value.DrumNo;
            apptransferrelcasslines.ProdOrderNo = value.ProdOrderNo;
            apptransferrelcasslines.ProdLineNo = 10000;
            apptransferrelcasslines.ItemNo = value.ItemNo;
            apptransferrelcasslines.Description = value.Description;
            apptransferrelcasslines.LotNo = value.LotNo;
            apptransferrelcasslines.QcrefNo = value.QcrefNo;
            apptransferrelcasslines.BatchNo = value.BatchNo;
            apptransferrelcasslines.Quantity = value.Quantity;
            apptransferrelcasslines.ReceiveQuantity = value.ReceiveQuantity;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApptransferReclassEntry")]
        public void Delete(int id)
        {
            var apptransferrelcasslines = _context.ApptransferRelcassLines.SingleOrDefault(p => p.ApptransferRelcassLineId == id);
            var errorMessage = "";
            try
            {
                if (apptransferrelcasslines != null)
                {
                    _context.ApptransferRelcassLines.Remove(apptransferrelcasslines);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}