﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MedCatalogClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public MedCatalogClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetMedCatalogClassification")]
        public List<MedCatalogClassificationModel> Get()
        {
            List<MedCatalogClassificationModel> medCatalogClassificationModels = new List<MedCatalogClassificationModel>();
            var medCatalogClassification = _context.MedCatalogClassification
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(p => p.Profile)
                                           .Include(a => a.Category)
                                           .Include(a => a.Group)
                                           .Include(m => m.ModifiedByUser)
                                           .OrderByDescending(o => o.MedCatalogId).AsNoTracking()
                                           .ToList();

            if (medCatalogClassification != null && medCatalogClassification.Count > 0)
            {
                medCatalogClassification.ForEach(s =>
                {
                    MedCatalogClassificationModel medCatalogClassificationModel = new MedCatalogClassificationModel
                    {
                        MedCatalogId = s.MedCatalogId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        GroupId = s.GroupId,
                        CategoryId = s.CategoryId,
                        CategoryNo = s.CategoryNo + "|" + s.Category?.Description + "|" + s.Name,
                        CategoryNos = s.CategoryNo,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Description = s.Description,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        CategoryName = s.Category?.Value,
                        GroupName = s.Group?.Value,
                        SessionId = s.SessionId,
                        Name = s.Name,
                        LastNo = s.LastNo,
                    };
                    medCatalogClassificationModels.Add(medCatalogClassificationModel);
                });

            }
            return medCatalogClassificationModels;
        }

        [HttpGet]
        [Route("GetMedCatalogClassificationByRefNo")]
        public List<MedCatalogClassificationModel> GetMedCatalogClassificationByRefNo(int? id)
        {

            List<MedCatalogClassificationModel> medCatalogClassificationModels = new List<MedCatalogClassificationModel>();

            var medCatalogClassification = _context.MedCatalogClassification
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(p => p.Profile)
                                           .Include(a => a.Category)
                                           .Include(a => a.Group)
                                           .Include(m => m.ModifiedByUser)
                                           .Where(w => w.ItemClassificationMasterId == id)
                                           .OrderByDescending(o => o.MedCatalogId).AsNoTracking()
                                           .ToList();
            if (medCatalogClassification != null && medCatalogClassification.Count > 0)
            {
                medCatalogClassification.ForEach(s =>
                {
                    MedCatalogClassificationModel medCatalogClassificationModel = new MedCatalogClassificationModel
                    {
                        MedCatalogId = s.MedCatalogId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        GroupId = s.GroupId,
                        CategoryId = s.CategoryId,
                        CategoryNo = s.CategoryNo,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Description = s.Description,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        CategoryName = s.Category?.Value,
                        GroupName = s.Group?.Value,
                        SessionId = s.SessionId,
                        Name = s.Name
                    };
                    medCatalogClassificationModels.Add(medCatalogClassificationModel);
                });
            }
            return medCatalogClassificationModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertMedCatalogClassification")]
        public MedCatalogClassificationModel Post(MedCatalogClassificationModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 });
            var sessionId = Guid.NewGuid();
            int? lastnos = null;
            string lastNo = _context.MedCatalogClassification.Where(w => w.CategoryId == value.CategoryId && w.ItemClassificationMasterId == value.ItemClassificationMasterId).OrderByDescending(o => o.MedCatalogId).FirstOrDefault()?.LastNo;
            lastNo = lastNo == null ? "0" : lastNo;
            var CategoryNos = "";
            if (value.CategoryId != null)
            {
                lastnos = int.Parse(lastNo) + 1;
                string setlastnos = lastnos.ToString();
                setlastnos = lastnos <= 9 ? ("00" + setlastnos) : setlastnos;
                setlastnos = lastnos >= 10 ? ("0" + setlastnos) : setlastnos;
                var applicationDetail = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMasterDetailId == value.CategoryId).FirstOrDefault();
                value.CategoryNo = applicationDetail?.Value + '-' + setlastnos;
                CategoryNos = value.CategoryNo + "|" + value.Name;
            }
            var medCatalogClassification = new MedCatalogClassification
            {
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileId = value.ProfileId,
                CategoryId = value.CategoryId,
                GroupId = value.GroupId,
                CategoryNo = value.CategoryNo,
                ProfileReferenceNo = profileNo,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                Name = value.Name,
                Description = value.Description,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
                LastNo = lastnos.ToString(),
            };
            _context.MedCatalogClassification.Add(medCatalogClassification);
            _context.SaveChanges();
            value.MedCatalogId = medCatalogClassification.MedCatalogId;
            value.ProfileReferenceNo = medCatalogClassification.ProfileReferenceNo;
            value.SessionId = sessionId;
            value.LastNo = medCatalogClassification.LastNo;
            value.CategoryNo = CategoryNos;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMedCatalogClassification")]
        public MedCatalogClassificationModel Put(MedCatalogClassificationModel value)
        {
            var medCatalogClassification = _context.MedCatalogClassification.SingleOrDefault(p => p.MedCatalogId == value.MedCatalogId);
            int? lastnos = null;
            //value.CategoryNo = medCatalogClassification.CategoryNo;
            var CategoryNos = value.CategoryNo;
            if (medCatalogClassification.CategoryId != null && value.CategoryId != null)
            {
                if (value.CategoryId != medCatalogClassification.CategoryId)
                {
                    string lastNo = _context.MedCatalogClassification.Where(w => w.CategoryId == value.CategoryId && w.ItemClassificationMasterId == value.ItemClassificationMasterId).OrderByDescending(o => o.MedCatalogId).FirstOrDefault()?.LastNo;
                    lastNo = lastNo == null ? "0" : lastNo;
                    lastnos = int.Parse(lastNo) + 1;
                    string setlastnos = lastnos.ToString();
                    setlastnos = lastnos <= 9 ? ("00" + setlastnos) : setlastnos;
                    setlastnos = lastnos >= 10 ? ("0" + setlastnos) : setlastnos;
                    var applicationDetail = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMasterDetailId == value.CategoryId).FirstOrDefault();
                    value.CategoryNo = applicationDetail?.Value + '-' + lastnos;
                    value.LastNo = lastnos.ToString();
                    CategoryNos = value.CategoryNo + "|" + value.Name;
                }

            }
            medCatalogClassification.ProfileId = value.ProfileId;
            medCatalogClassification.CategoryId = value.CategoryId;
            medCatalogClassification.GroupId = value.GroupId;
            medCatalogClassification.CategoryNo = value.CategoryNo;
            medCatalogClassification.Description = value.Description;
            medCatalogClassification.Name = value.Name;
            medCatalogClassification.StatusCodeId = value.StatusCodeID.Value;
            medCatalogClassification.ModifiedByUserId = value.ModifiedByUserID.Value;
            medCatalogClassification.ModifiedDate = DateTime.Now;
            medCatalogClassification.LastNo = value.LastNo;
            _context.SaveChanges();
            value.CategoryNo = CategoryNos;
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMedCatalogClassification")]
        public void Delete(int id)
        {
            var medCatalogClassification = _context.MedCatalogClassification.SingleOrDefault(p => p.MedCatalogId == id);
            if (medCatalogClassification != null)
            {
                _context.MedCatalogClassification.Remove(medCatalogClassification);
                _context.SaveChanges();
            }
        }
    }
}
