﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CriticalstepandIntermediateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CriticalstepandIntermediateController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCriticalstepandIntermediates")]
        public List<CriticalstepandIntermediateModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var finsiproductList = _context.FinishProduct.ToList();
            var criticalstepandIntermediate = _context.CriticalstepandIntermediate
                                                .Include(a => a.AddedByUser)
                                                .Include(m => m.ModifiedByUser)
                                                .Include(s => s.StatusCode)
                                                .Include(r => r.RegisterationCode)
                                                .AsNoTracking().ToList();
            List<CriticalstepandIntermediateModel> criticalstepandIntermediateModel = new List<CriticalstepandIntermediateModel>();
            criticalstepandIntermediate.ForEach(s =>
            {
                CriticalstepandIntermediateModel criticalstepandIntermediateModels = new CriticalstepandIntermediateModel
                {
                    CriticalstepandIntermediateID = s.CriticalstepandIntermediateId,
                    FinishProductiID = s.FinishProductiId,
                    FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode?.CodeValue,
                    IsMasterDocument = s.IsMasterDocument,
                    IsInformationvsmaster = s.IsInformationvsmaster,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProductName = applicationmasterdetail != null && s.FinishProductGeneralInfo != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                };
                criticalstepandIntermediateModel.Add(criticalstepandIntermediateModels);
            });
            return criticalstepandIntermediateModel.OrderByDescending(a => a.CriticalstepandIntermediateID).ToList();
        }
        [HttpGet]
        [Route("GetCriticalstepandIntermediatesByFinishProduct")]
        public List<CriticalstepandIntermediateModel> GetCriticalstepandIntermediatesByFinishProduct(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var criticalstepandIntermediate = _context.CriticalstepandIntermediate.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(r => r.RegisterationCode)
                 .Include(f => f.FinishProductGeneralInfoId).AsNoTracking().ToList();
            List<CriticalstepandIntermediateModel> criticalstepandIntermediateModel = new List<CriticalstepandIntermediateModel>();
            criticalstepandIntermediate.ForEach(s =>
            {
                CriticalstepandIntermediateModel criticalstepandIntermediateModels = new CriticalstepandIntermediateModel
                {
                    CriticalstepandIntermediateID = s.CriticalstepandIntermediateId,
                    FinishProductiID = s.FinishProductiId,
                    FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode.CodeValue,
                    IsMasterDocument = s.IsMasterDocument,
                    //IsInformationvsmaster = s.IsInformationvsmaster,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                };
                criticalstepandIntermediateModel.Add(criticalstepandIntermediateModels);
            });
            return criticalstepandIntermediateModel.Where(a => a.FinishProductGeneralInfoId == id && a.IsMasterDocument == true).OrderByDescending(a => a.CriticalstepandIntermediateID).ToList();
        }
        [HttpGet]
        [Route("IsMasterExist")]
        public bool IsMasterExist(int? id)
        {
            var list = _context.CriticalstepandIntermediate.Where(s => s.FinishProductGeneralInfoId == id && s.IsMasterDocument == true).FirstOrDefault();
            if (list != null)
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
            else
            {
                return false;
            }
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CriticalstepandIntermediateModel> GetData(SearchModel searchModel)
        {
            var criticalstepandIntermediate = new CriticalstepandIntermediate();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).FirstOrDefault();
                        break;
                    case "Last":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).LastOrDefault();
                        break;
                    case "Next":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).LastOrDefault();
                        break;
                    case "Previous":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).FirstOrDefault();
                        break;
                    case "Last":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).LastOrDefault();
                        break;
                    case "Next":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderBy(o => o.CriticalstepandIntermediateId).FirstOrDefault(s => s.CriticalstepandIntermediateId > searchModel.Id);
                        break;
                    case "Previous":
                        criticalstepandIntermediate = _context.CriticalstepandIntermediate.OrderByDescending(o => o.CriticalstepandIntermediateId).FirstOrDefault(s => s.CriticalstepandIntermediateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CriticalstepandIntermediateModel>(criticalstepandIntermediate);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
                var finishProductItem = result.FinishProductGeneralInfoId > 0 ? (_context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.FinishProductGeneralInfoId).FinishProductId) : null;
                if (finishProductItem != null)
                {
                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCriticalstepandIntermediate")]
        public CriticalstepandIntermediateModel Post(CriticalstepandIntermediateModel value)
        {
            var criticalstepandIntermediate = new CriticalstepandIntermediate
            {

                FinishProductiId = value.FinishProductiID,
                FinishProductGeneralInfoId = value.FinishProductGeneralInfoId,
                IsInformationvsmaster = value.IsInformationvsmaster,
                RegisterationCodeId = value.RegisterationCodeId.Value,
                IsMasterDocument = value.IsMasterDocument,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.CriticalstepandIntermediate.Add(criticalstepandIntermediate);
            _context.SaveChanges();
            value.CriticalstepandIntermediateID = criticalstepandIntermediate.CriticalstepandIntermediateId;
            if (value.IsMasterDocument == false && value.IsInformationvsmaster == true && value.FinishProductGeneralInfoId > 0)
            {
                var CriticalstepandIntermediateId = _context.CriticalstepandIntermediate.FirstOrDefault(s => s.FinishProductGeneralInfoId == value.FinishProductGeneralInfoId && s.IsInformationvsmaster == false && s.IsMasterDocument == true).CriticalstepandIntermediateId;
                if (CriticalstepandIntermediateId > 0)
                {
                    var CriticalstepandIntermediateLineItems = _context.CriticalstepandIntermediateLine.Where(s => s.CriticalstepandIntermediateId == CriticalstepandIntermediateId).ToList();
                    if (CriticalstepandIntermediateLineItems.Count() > 0)
                    {
                        CriticalstepandIntermediateLineItems.ForEach(c =>
                        {
                            var CriticalstepandIntermediateLine = new CriticalstepandIntermediateLine
                            {
                                CriticalstepandIntermediateId = value.CriticalstepandIntermediateID,
                                TestId = c.TestId,
                                TestStageId = c.TestId,
                                SpecificationLimitsId = c.SpecificationLimitsId,
                                UnitsId = c.UnitsId,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID.Value,
                            };
                            var SamplingFrequencyIDs = _context.CriticalstepandIntermediateLine.Add(CriticalstepandIntermediateLine);
                            var SamplingFrequencyList = _context.CriticalSamplingFrequency.Where(s => s.CriticalstepandIntermediateLineId == c.CriticalstepandIntermediateLineId).ToList();
                            if (SamplingFrequencyList.Count() > 0)
                            {
                                SamplingFrequencyList.ForEach(sam =>
                                {
                                    var SamplingFrequencyListItems = new CriticalSamplingFrequency
                                    {
                                        SampleFrequencyValue = sam.SampleFrequencyValue,
                                        SamplingFrequencyId = sam.SamplingFrequencyId,
                                        CriticalstepandIntermediateLineId = CriticalstepandIntermediateLine.CriticalstepandIntermediateLineId,
                                    };
                                    CriticalstepandIntermediateLine.CriticalSamplingFrequency.Add(SamplingFrequencyListItems);
                                });
                            }
                            _context.SaveChanges();
                        });
                    }

                }
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCriticalstepandIntermediate")]
        public CriticalstepandIntermediateModel Put(CriticalstepandIntermediateModel value)
        {
            var criticalstepandIntermediate = _context.CriticalstepandIntermediate.SingleOrDefault(p => p.CriticalstepandIntermediateId == value.CriticalstepandIntermediateID);


            criticalstepandIntermediate.FinishProductiId = value.FinishProductiID;
            criticalstepandIntermediate.FinishProductGeneralInfoId = value.FinishProductGeneralInfoId;
            criticalstepandIntermediate.RegisterationCodeId = value.RegisterationCodeId.Value;
            criticalstepandIntermediate.IsMasterDocument = value.IsMasterDocument;
            criticalstepandIntermediate.IsInformationvsmaster = value.IsInformationvsmaster;
            criticalstepandIntermediate.ModifiedByUserId = value.ModifiedByUserID;
            criticalstepandIntermediate.ModifiedDate = DateTime.Now;
            criticalstepandIntermediate.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCriticalstepandIntermediate")]
        public void Delete(int id)
        {
            var criticalstepandIntermediate = _context.CriticalstepandIntermediate.SingleOrDefault(p => p.CriticalstepandIntermediateId == id);
            if (criticalstepandIntermediate != null)
            {
                var criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.Where(l => l.CriticalstepandIntermediateId == id).ToList();
                if (criticalstepandIntermediateLine.Count > 0)
                {
                    criticalstepandIntermediateLine.ForEach(c =>
                    {
                        var CriticalSamplingFrequency = _context.CriticalSamplingFrequency.Where(a => a.CriticalstepandIntermediateLineId == c.CriticalstepandIntermediateLineId).ToList();
                        if (CriticalSamplingFrequency.Count > 0)
                        {
                            _context.CriticalSamplingFrequency.RemoveRange(CriticalSamplingFrequency);
                            _context.SaveChanges();
                        }
                    });
                    _context.CriticalstepandIntermediateLine.RemoveRange(criticalstepandIntermediateLine);
                    _context.SaveChanges();
                }
                _context.CriticalstepandIntermediate.Remove(criticalstepandIntermediate);
                _context.SaveChanges();
            }
        }
    }
}