using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationFormFieldsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationFormFieldsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

            //      long ApplicationFormFieldPermissionId 
            //  ApplicationFormFieldId 
            // RoleId 
            //  IsReadOnly 
            //  IsVisible 
            // IsValueFilter 
            //  FieldValue 
            //  ModifiedDate
            //  ModifiedByUserId 
            //   ColumnName
            // DisplayName 
            //   RoleId
            //  ApplicationFormID 

            // ApplicationFormFields
            // ApplicationFormPermission

        }

        // GET: api/Project

        [HttpGet]
        [Route("GetApplicationForm")]
        public List<ApplicationFormModel> GetGetApplicationForm()
        {
            var ApplicationFormSub = _context.ApplicationFormSub.AsNoTracking().ToList();
            List<ApplicationFormModel> applicationFormModels = new List<ApplicationFormModel>();
            var ApplicationForm = _context.ApplicationForm
                                        .Include("AddedByUser")
                                        .Include(a => a.ClassificationType)
                                        .Include("ModifiedByUser")
                                        .Include("StatusCode")
                                        .Include(p => p.Profile)
                                        .OrderByDescending(o => o.FormId).Where(s => s.FormType == "Permission").AsNoTracking().ToList();
            ApplicationForm.ForEach(s =>
            {
                ApplicationFormModel applicationFormModel = new ApplicationFormModel
                {
                    FormID = s.FormId,
                    ModuleName = s.ModuleName,
                    PathURL = s.PathUrl,
                    Name = s.Name,
                    FormType = s.FormType,
                    TableName = s.TableName,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                    ApplicationFormSubIDs = ApplicationFormSub.Where(l => l.FormId == s.FormId).Select(a => a.ApplicationFormId.Value).ToList(),
                    StatusCodeID = s.StatusCodeId,
                    ClassificationTypeId = s.ClassificationTypeId,
                    ClassificationType = s.ClassificationType != null ? s.ClassificationType.CodeValue : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,                    
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                applicationFormModels.Add(applicationFormModel);
            });
            return applicationFormModels;
        }



        [HttpGet]
        [Route("GetApplicationFormFields")]
        public List<ApplicationFormFieldModel> Get()
        {
            int roleId = 1;
            List<ApplicationFormFieldModel> applicationFormFieldModels = new List<ApplicationFormFieldModel>();
            var applicationForm = _context.ApplicationFormFields
                                    .Include("ApplicationFormPermission")
                                    .Include("ModifiedByUser")
                                    .OrderByDescending(o => o.ApplicationFormFieldId)
                                    .AsNoTracking()
                                    .ToList();
            applicationForm.ForEach(s =>
            {
                ApplicationFormFieldModel applicationFormFieldModel = new ApplicationFormFieldModel
                {
                    ColumnName = s.ColumnName,
                    DisplayName = s.DisplayName,
                    ApplicationFormFieldId = s.ApplicationFormFieldId,
                    ApplicationFormFieldPermissionId = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).ApplicationFormFieldPermissionId : 0,
                    IsReadOnly = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsReadOnly : false,
                    IsVisible = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsVisible : false,
                    IsValueFilter = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsValueFilter : false,
                    FieldValue = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).FieldValue : "",
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                applicationFormFieldModels.Add(applicationFormFieldModel);
            });
            return applicationFormFieldModels;
        }

        [HttpGet]
        [Route("GetApplicationFormFieldSearch")]
        public List<ApplicationFormFieldModel> GetApplicationFormFieldsSearch(int id, int userId)
        {
            List<ApplicationFormFieldModel> applicationFormFieldModels = new List<ApplicationFormFieldModel>();
            int roleId = id, formID = userId;
            var applicationFormFields = _context.ApplicationFormFields
                                        .Include("ApplicationFormPermission")
                                        .Include("ModifiedByUser")
                                        .Where(a => a.ApplicationFormId == formID)
                                        .OrderByDescending(o => o.ApplicationFormFieldId)
                                        .AsNoTracking().ToList();
            applicationFormFields.ForEach(s =>
            {
                ApplicationFormFieldModel applicationFormFieldModel = new ApplicationFormFieldModel
                {
                    ColumnName = s.ColumnName,
                    DisplayName = s.DisplayName,
                    ApplicationFormId = s.ApplicationFormId,
                    StatusCodeID = s.StatusCodeId,
                    ApplicationFormFieldId = s.ApplicationFormFieldId,
                    ApplicationFormFieldPermissionId = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).ApplicationFormFieldPermissionId : 0,
                    IsReadOnly = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsReadOnly : false,
                    IsVisible = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsVisible : false,
                    IsValueFilter = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).IsValueFilter : false,
                    FieldValue = s.ApplicationFormPermission != null && s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId) != null ? s.ApplicationFormPermission.FirstOrDefault(p => p.ApplicationFormFieldId == s.ApplicationFormFieldId && p.RoleId == roleId).FieldValue : "",
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                applicationFormFieldModels.Add(applicationFormFieldModel);
            });


            return applicationFormFieldModels;
        }





        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationFormFieldModel> GetData(SearchModel searchModel)
        {
            var applicationFormField = new ApplicationFormFields();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).LastOrDefault();
                        break;
                    case "Next":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).LastOrDefault();
                        break;
                    case "Next":
                        applicationFormField = _context.ApplicationFormFields.OrderBy(o => o.ApplicationFormFieldId).FirstOrDefault(s => s.ApplicationFormFieldId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationFormField = _context.ApplicationFormFields.OrderByDescending(o => o.ApplicationFormFieldId).FirstOrDefault(s => s.ApplicationFormFieldId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationFormFieldModel>(applicationFormField);

            if (result != null)
            {
                var fieldPermission = _context.ApplicationFormPermission.FirstOrDefault(f => f.ApplicationFormFieldId == result.ApplicationFormFieldId);
                if (fieldPermission != null)
                {
                    result.IsVisible = fieldPermission.IsVisible;
                    result.IsReadOnly = fieldPermission.IsReadOnly;
                    result.IsValueFilter = fieldPermission.IsValueFilter;
                    result.FieldValue = fieldPermission.FieldValue;
                }

            }




            return result;
        }



        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationFormField")]
        public ApplicationFormFieldModel Post(ApplicationFormFieldModel value)
        {

            var applicationFormField = new ApplicationFormFields
            {
                ApplicationFormId = value.ApplicationFormId,
                ColumnName = value.ColumnName,
                DisplayName = value.DisplayName,
                StatusCodeId = value.StatusCodeID,
                ModifiedByUserId = value.ModifiedByUserID,
                //  ModifiedByUserId=value.ModifiedByUserId,
                ModifiedDate = DateTime.Now,


            };
            _context.ApplicationFormFields.Add(applicationFormField);
            _context.SaveChanges();
            value.ApplicationFormFieldId = applicationFormField.ApplicationFormFieldId;
            if (value.ApplicationFormFieldId != null)
            {
                var applicationFormPermission = new ApplicationFormPermission
                {
                    ApplicationFormFieldPermissionId = value.ApplicationFormFieldPermissionId,
                    ApplicationFormFieldId = value.ApplicationFormFieldId,
                    RoleId = value.RoleId,
                    IsReadOnly = value.IsReadOnly,
                    IsVisible = value.IsVisible,
                    IsValueFilter = value.IsValueFilter,
                    FieldValue = value.FieldValue,
                    ModifiedByUserId = value.ModifiedByUserID,
                    ModifiedDate = DateTime.Now,


                };
                _context.ApplicationFormPermission.Add(applicationFormPermission);
                _context.SaveChanges();
                value.ApplicationFormFieldPermissionId = applicationFormPermission.ApplicationFormFieldPermissionId;
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationFormField")]
        public ApplicationFormFieldModel Put(ApplicationFormFieldModel value)
        {
            var applicationFormField = _context.ApplicationFormFields.SingleOrDefault(p => p.ApplicationFormFieldId == value.ApplicationFormFieldId);
            applicationFormField.ApplicationFormId = value.ApplicationFormId;
            applicationFormField.ColumnName = value.ColumnName;
            applicationFormField.DisplayName = value.DisplayName;
            applicationFormField.StatusCodeId = value.StatusCodeID;
            applicationFormField.ModifiedByUserId = value.ModifiedByUserID;
            applicationFormField.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (value.ApplicationFormFieldPermissionId == 0)
            {

                var applicationFormPermission = new ApplicationFormPermission
                {
                    // ApplicationFormFieldPermissionId=value.ApplicationFormFieldPermissionId,
                    ApplicationFormFieldId = value.ApplicationFormFieldId,
                    RoleId = value.RoleId,
                    IsReadOnly = value.IsReadOnly,
                    IsVisible = value.IsVisible,
                    IsValueFilter = value.IsValueFilter,
                    FieldValue = value.FieldValue,
                    ModifiedByUserId = value.ModifiedByUserID,
                    ModifiedDate = DateTime.Now,


                };
                _context.ApplicationFormPermission.Add(applicationFormPermission);
                _context.SaveChanges();
                value.ApplicationFormFieldPermissionId = applicationFormPermission.ApplicationFormFieldPermissionId;
            }
            else
            {
                var applicationFormPermission = _context.ApplicationFormPermission.SingleOrDefault(p => p.ApplicationFormFieldPermissionId == value.ApplicationFormFieldPermissionId);
                applicationFormPermission.RoleId = value.RoleId;
                applicationFormPermission.IsReadOnly = value.IsReadOnly;
                applicationFormPermission.IsVisible = value.IsVisible;
                applicationFormPermission.IsValueFilter = value.IsValueFilter;
                applicationFormPermission.FieldValue = value.FieldValue;
                applicationFormPermission.ModifiedByUserId = value.ModifiedByUserID;
                applicationFormPermission.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationFormField")]
        public void Delete(int id)
        {
            var applicationFormField = _context.ApplicationFormPermission.SingleOrDefault(p => p.ApplicationFormFieldPermissionId == id);
            if (applicationFormField != null)
            {

                _context.ApplicationFormPermission.Remove(applicationFormField);
                _context.SaveChanges();
            }
        }


    }
}