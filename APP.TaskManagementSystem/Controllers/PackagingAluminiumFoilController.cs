﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingAluminiumFoilController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingAluminiumFoilController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingAluminiumFoils")]
        public List<PackagingAluminiumModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<PackagingAluminiumModel> packagingAluminiumModels = new List<PackagingAluminiumModel>();
            var packaginLeaflet = _context.PackagingAluminiumFoil.Include("AddedByUser").Include("ModifiedByUser").Include("AluminiumFoilType").OrderByDescending(r => r.AluminiumFoilId).AsNoTracking().ToList();
            if (packaginLeaflet != null && packaginLeaflet.Count > 0)
            {
                packaginLeaflet.ForEach(s =>
                {
                    PackagingAluminiumModel packagingAluminiumModel = new PackagingAluminiumModel()
                    {
                        AluminiumFoilId = s.AluminiumFoilId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        ColorId = s.ColorId,
                        FoilThickness = s.FoilThickness,
                        FoilWidth = s.FoilWidth,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Not-Printed",
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PantoneColorId = s.PantoneColorId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ReversePrint = s.ReversePrint,
                        WtPerRoll = s.WtPerRoll,
                        LengthPerRoll = s.LengthPerRoll,
                        AluminiumFoilTypeId = s.AluminiumFoilTypeId,
                        AluminiumFoilTypeName = s.AluminiumFoilType != null ? s.AluminiumFoilType.CodeValue : "",
                        IsVersion = s.IsVersion,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingAluminiumModels.Add(packagingAluminiumModel);
                });
            }

            return packagingAluminiumModels;
        }

        [HttpPost]
        [Route("GetPackagingAluminiumFoilsByRefNo")]
        public List<PackagingAluminiumModel> GetPackagingAluminiumFoilsByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<PackagingAluminiumModel> packagingAluminiumModels = new List<PackagingAluminiumModel>();
            List<PackagingAluminiumFoil> packagingAluminium = new List<PackagingAluminiumFoil>();
            if (refSearchModel.IsHeader)
            {
                packagingAluminium = _context.PackagingAluminiumFoil.Include("AddedByUser").Include("ModifiedByUser").Include("AluminiumFoilType").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.AluminiumFoilTypeId == refSearchModel.TypeID).OrderByDescending(o => o.AluminiumFoilId).AsNoTracking().ToList();
            }
            else
            {
                packagingAluminium = _context.PackagingAluminiumFoil.Include("AddedByUser").Include("ModifiedByUser").Include("AluminiumFoilType").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.AluminiumFoilTypeId == refSearchModel.TypeID).OrderByDescending(o => o.AluminiumFoilId).AsNoTracking().ToList();

            }
            if (packagingAluminium != null && packagingAluminium.Count > 0)
            {
                packagingAluminium.ForEach(s =>
                {
                    PackagingAluminiumModel packagingAluminiumModel = new PackagingAluminiumModel()
                    {
                        AluminiumFoilId = s.AluminiumFoilId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        ColorId = s.ColorId,
                        FoilThickness = s.FoilThickness,
                        FoilWidth = s.FoilWidth,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Not-Printed",
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PantoneColorId = s.PantoneColorId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ReversePrint = s.ReversePrint,
                        WtPerRoll = s.WtPerRoll,
                        LengthPerRoll = s.LengthPerRoll,
                        AluminiumFoilTypeId = s.AluminiumFoilTypeId,
                        AluminiumFoilTypeName = s.AluminiumFoilType != null ? s.AluminiumFoilType.CodeValue : "",
                        IsVersion = s.IsVersion,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingAluminiumModels.Add(packagingAluminiumModel);
                });
            }
            return packagingAluminiumModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetPackagingAluminiumFoil/{id:int}")]
        public ActionResult<PackagingAluminiumModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var packagingAluminiumFoil = _context.PackagingAluminiumFoil.SingleOrDefault(p => p.AluminiumFoilId == id.Value);
            var result = _mapper.Map<PackagingAluminiumModel>(packagingAluminiumFoil);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackagingAluminiumModel> GetData(SearchModel searchModel)
        {
            var packagingAluminiumFoil = new PackagingAluminiumFoil();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).LastOrDefault();
                        break;
                    case "Next":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).LastOrDefault();
                        break;
                    case "Next":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderBy(o => o.AluminiumFoilId).FirstOrDefault(s => s.AluminiumFoilId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingAluminiumFoil = _context.PackagingAluminiumFoil.OrderByDescending(o => o.AluminiumFoilId).FirstOrDefault(s => s.AluminiumFoilId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingAluminiumModel>(packagingAluminiumFoil);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingAluminiumFoil")]
        public PackagingAluminiumModel Post(PackagingAluminiumModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "PackagingAluminiumFoil" });
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingAluminiumFoil = new PackagingAluminiumFoil
            {
                BgColorId = value.BgColorId,
                BgPantoneColorId = value.BgPantoneColorId,
                ColorId = value.ColorId,
                FoilThickness = value.FoilThickness,
                FoilWidth = value.FoilWidth,
                IsPrinted = value.IsPrinted,
                LetteringId = value.LetteringId,
                NoOfColor = value.NoOfColor,
                PantoneColorId = value.PantoneColorId,
                IsVersion = value.IsVersion,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ReversePrint = value.ReversePrint,
                WtPerRoll = value.WtPerRoll,
                LengthPerRoll = value.LengthPerRoll,
                AluminiumFoilTypeId = value.AluminiumFoilTypeId,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                ProfileReferenceNo = value.ProfileLinkReferenceNo
            };
            _context.PackagingAluminiumFoil.Add(packagingAluminiumFoil);
            _context.SaveChanges();
            value.Printed = value.IsPrinted == true ? "Printed" : "Non-Printed";
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";
            value.AluminiumFoilId = packagingAluminiumFoil.AluminiumFoilId;
            value.ProfileLinkReferenceNo = value.LinkProfileReferenceNo;
            value.MasterProfileReferenceNo = packagingAluminiumFoil.MasterProfileReferenceNo;
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackagingAluminiumModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.AluminiumFoilTypeId == 1088)
            {


                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        //[Foil Thickness] mm[Foil Width] mm Aluminium Foil
                        var masterDetailList = _context.ApplicationMasterDetail.ToList();
                        if (value.IsPrinted == true)
                        {
                            itemName = "Printed" + " ";
                        }
                        if (value.IsPrinted == false)
                        {
                            itemName = "Non- Printed" + " ";
                        }
                        if (value.FoilWidth != null && value.FoilWidth > 0)
                        {
                            itemName = itemName + value.FoilWidth + "mm" + "(W) ";
                        }
                        if (value.FoilThickness != null && value.FoilThickness > 0)
                        {
                            itemName = itemName + "X " + value.FoilThickness + "mm" + "(T) " + "Aluminium Foil";
                        }
                        // var itemName = value.FoilThickness + "mm" + "(Thickness)" + " " + "X" + " " + value.FoilWidth + "mm" + "(Width)" + " " + "Aluminium Foil";
                        // itemName = value.FoilWidth + "mm" + "(W) "+ "X " + value.FoilThickness + "mm" + "(T) " +"Aluminium Foil";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            if (value.AluminiumFoilTypeId == 1089)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        //[Foil Thickness] mm[Foil Width] mm Aluminium Foil
                        //Printed 2 Color 1mm(Thickness) X 200mm(Width) Aluminium Foil
                        var masterDetailList = _context.ApplicationMasterDetail.ToList();
                        itemName = string.Empty;
                        if (value.IsPrinted == true)
                        {
                            itemName = "Printed" + " ";
                            // itemName = value.NoOfColor + " "+"Color" +" "+ value.FoilThickness + "mm" + "(Thickness)" + " " + "X" + " " + value.FoilWidth + "mm" + "(Width)" + " " + "Aluminium Foil";
                            if (value.NoOfColor != null)
                            {
                                itemName = itemName+value.NoOfColor + " " + "Color" + " ";
                            }
                            if (value.FoilWidth != null)
                            {
                                itemName = itemName + value.FoilWidth + "mm" + "(W) "+ " ";
                            }
                            if (value.FoilThickness != null)
                            {
                                itemName = itemName + value.FoilThickness + "mm" + " (T)" + " ";
                            }
                            itemName = itemName + "Aluminium Foil";
                            //itemName = value.NoOfColor + " "+"Color" +" "+ value.FoilWidth + "mm" + "(W) " + "X " + " "+value.FoilThickness + "mm" + " (T)" + " "   + "Aluminium Foil";
                        }
                        if (value.IsPrinted == false)
                        {
                            itemName = "Non- Printed" + " ";
                            //itemName = value.FoilThickness + "mm" + "(Thickness)" + " " + "X" + " " + value.FoilWidth + "mm" + "(Width)" + " " + "Aluminium Foil";
                            if (value.FoilWidth != null)
                            {
                                itemName = itemName+value.FoilWidth + " mm" + "(W) "  + " ";
                            }
                            if (value.FoilThickness != null)
                            {
                                itemName = itemName + value.FoilThickness + "mm" + " (T)" + " ";
                            }
                            itemName = itemName + "Aluminium Foil";
                            //itemName =  value.FoilWidth + " mm" + "(W) " + "X " + " "+value.FoilThickness + "mm" + " (T)" + " "   + "Aluminium Foil";
                        }
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }

            }


            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingAluminiumFoil")]
        public PackagingAluminiumModel Put(PackagingAluminiumModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingAluminiumFoil = _context.PackagingAluminiumFoil.SingleOrDefault(p => p.AluminiumFoilId == value.AluminiumFoilId);
            packagingAluminiumFoil.BgColorId = value.BgColorId;
            packagingAluminiumFoil.BgPantoneColorId = value.BgPantoneColorId;
            packagingAluminiumFoil.ColorId = value.ColorId;
            packagingAluminiumFoil.FoilThickness = value.FoilThickness;
            packagingAluminiumFoil.FoilWidth = value.FoilWidth;
            packagingAluminiumFoil.IsPrinted = value.IsPrinted;
            packagingAluminiumFoil.LetteringId = value.LetteringId;
            packagingAluminiumFoil.NoOfColor = value.NoOfColor;
            packagingAluminiumFoil.PantoneColorId = value.PantoneColorId;
            packagingAluminiumFoil.IsVersion = value.IsVersion;
            packagingAluminiumFoil.ModifiedByUserId = value.ModifiedByUserID;
            packagingAluminiumFoil.ModifiedDate = DateTime.Now;
            packagingAluminiumFoil.StatusCodeId = value.StatusCodeID.Value;
            packagingAluminiumFoil.ReversePrint = value.ReversePrint;
            packagingAluminiumFoil.WtPerRoll = value.WtPerRoll;
            packagingAluminiumFoil.LengthPerRoll = value.LengthPerRoll;
            packagingAluminiumFoil.AluminiumFoilTypeId = value.AluminiumFoilTypeId;
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            value.Printed = value.IsPrinted == true ? "Printed" : "Non-Printed";
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingAluminiumFoil")]
        public void Delete(int id)
        {
            var packagingAluminiumFoil = _context.PackagingAluminiumFoil.SingleOrDefault(p => p.AluminiumFoilId == id);
            if (packagingAluminiumFoil != null)
            {
                var packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.Where(p => p.PackagingAluminiumFoilLineId == id).AsNoTracking().ToList();
                if (packagingAluminiumFoilLine != null)
                {
                    _context.PackagingAluminiumFoilLine.RemoveRange(packagingAluminiumFoilLine);
                    _context.SaveChanges();
                }
                _context.PackagingAluminiumFoil.Remove(packagingAluminiumFoil);
                _context.SaveChanges();
            }
        }
    }
}