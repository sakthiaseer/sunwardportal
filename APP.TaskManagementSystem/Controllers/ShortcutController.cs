﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ShortcutController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ShortcutController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetShortcuts")]
        public List<ShortcutModel> Get()
        {
            var shortcut = _context.ShortCut.Include("AddedByUser").Include("ModifiedByUser").Select(s => new ShortcutModel
            {
                ShortCutID = s.ShortCutId,
                Name = s.Name,
                StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ShortCutID).AsNoTracking().ToList();
            return shortcut;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get ShortCut")]
        [HttpGet("GetShortcuts/{id:int}")]
        public ActionResult<ShortcutModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var shortcut = _context.ShortCut.SingleOrDefault(p => p.ShortCutId == id.Value);
            var result = _mapper.Map<ShortcutModel>(shortcut);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ShortcutModel> GetData(SearchModel searchModel)
        {
            var shortcut = new ShortCut();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).FirstOrDefault();
                        break;
                    case "Last":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).LastOrDefault();
                        break;
                    case "Next":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).LastOrDefault();
                        break;
                    case "Previous":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).FirstOrDefault();
                        break;
                    case "Last":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).LastOrDefault();
                        break;
                    case "Next":
                        shortcut = _context.ShortCut.OrderBy(o => o.ShortCutId).FirstOrDefault(s => s.ShortCutId > searchModel.Id);
                        break;
                    case "Previous":
                        shortcut = _context.ShortCut.OrderByDescending(o => o.ShortCutId).FirstOrDefault(s => s.ShortCutId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ShortcutModel>(shortcut);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertShortcut")]
        public ShortcutModel Post(ShortcutModel value)
        {
            var shortcut = new ShortCut
            {
                Name = value.Name,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };
            _context.ShortCut.Add(shortcut);
            _context.SaveChanges();
            value.ShortCutID = shortcut.ShortCutId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateShortcut")]
        public ShortcutModel Put(ShortcutModel value)
        {
            var shortcut = _context.ShortCut.SingleOrDefault(p => p.ShortCutId == value.ShortCutID);
            //ShortCut.ShortCutId = value.ShortCutID;
            shortcut.ModifiedByUserId = value.ModifiedByUserID;
            shortcut.ModifiedDate = DateTime.Now;
            shortcut.Name = value.Name;
            shortcut.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteShortcut")]
        public void Delete(int id)
        {
            var shortcut = _context.ShortCut.SingleOrDefault(p => p.ShortCutId == id);
            if (shortcut != null)
            {
                _context.ShortCut.Remove(shortcut);
                _context.SaveChanges();
            }
        }
    }
}