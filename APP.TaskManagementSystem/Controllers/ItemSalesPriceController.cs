﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemSalesPriceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ItemSalesPriceController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetItemSalesPrice")]
        public List<ItemSalesPriceModel> Get()
        {
            List<ItemSalesPriceModel> itemSalesPriceModels = new List<ItemSalesPriceModel>();
            var itemSalesPrice = _context.ItemSalesPrice.Include("AddedByUser").Include("ModifiedByUser").Include("Item").Include("Company").Include("Customer").OrderByDescending(o => o.ItemSalesPriceId).AsNoTracking().ToList();
            if(itemSalesPrice!=null && itemSalesPrice.Count>0)
            {
                itemSalesPrice.ForEach(s =>
                {
                    ItemSalesPriceModel itemSalesPriceModel = new ItemSalesPriceModel
                    {
                        ItemSalesPriceID = s.ItemSalesPriceId,
                        ItemId = s.Item.ItemId,
                        CompanyID = s.Company.PlantId,
                        ItemName = s.Item!=null? s.Item.No : "",
                        CompanyName = s.Company!=null? s.Company.Description : "",
                        CustomerId = s.CustomerId,
                        CustomerName = s.Customer!=null? s.Customer.Name : "",
                        SellingPrice = s.SellingPrice,
                        StartDate = s.StartDate,
                        EndDate = s.EndDate,
                        Description1 = s.Item != null ? s.Item.Description : "",
                        Description2 = s.Item != null ? s.Item.Description2 : "",
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    };
                    itemSalesPriceModels.Add(itemSalesPriceModel);
                });
            }
           
            return itemSalesPriceModels;
        }
        [HttpGet("GetItemSalesPriceByID")]
        public ActionResult<ItemSalesPriceModel> GetItemSalesPrice(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var itemSalesPrice = _context.ItemSalesPrice.SingleOrDefault(p => p.ItemSalesPriceId == id.Value);
            var result = _mapper.Map<ItemSalesPriceModel>(itemSalesPrice);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }      

     

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ItemSalesPriceModel> GetData(SearchModel searchModel)
        {
            var itemSalesPrice = new ItemSalesPrice();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).FirstOrDefault();
                        break;
                    case "Last":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).LastOrDefault();
                        break;
                    case "Next":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).LastOrDefault();
                        break;
                    case "Previous":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).FirstOrDefault();
                        break;
                    case "Last":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).LastOrDefault();
                        break;
                    case "Next":
                        itemSalesPrice = _context.ItemSalesPrice.OrderBy(o => o.ItemSalesPriceId).FirstOrDefault(s => s.ItemSalesPriceId > searchModel.Id);
                        break;
                    case "Previous":
                        itemSalesPrice = _context.ItemSalesPrice.OrderByDescending(o => o.ItemSalesPriceId).FirstOrDefault(s => s.ItemSalesPriceId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ItemSalesPriceModel>(itemSalesPrice);
            if(result!=null)
            {
               
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertItemSalesPrice")]
        public ItemSalesPriceModel Post(ItemSalesPriceModel value)
        {
            var itemSalesPrice = new ItemSalesPrice
            {
              CompanyId = value.CompanyID,
              ItemId = value.ItemId,
              CustomerId = value.CustomerId,
              SellingPrice = value.SellingPrice,
              StartDate = value.StartDate,
              EndDate = value.EndDate,
              AddedByUserId = value.AddedByUserID.Value,
              AddedDate = DateTime.Now,
              StatusCodeId = value.StatusCodeID.Value,
                


            };
            _context.ItemSalesPrice.Add(itemSalesPrice);
            _context.SaveChanges();
            value.ItemSalesPriceID = itemSalesPrice.ItemSalesPriceId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemSalesPrice")]
        public ItemSalesPriceModel Put(ItemSalesPriceModel value)
        {
            var itemSalesPrice = _context.ItemSalesPrice.SingleOrDefault(p => p.ItemSalesPriceId == value.ItemSalesPriceID);
            itemSalesPrice.ItemId = value.ItemId;
            itemSalesPrice.CustomerId = value.CustomerId;
            itemSalesPrice.CompanyId = value.CompanyID;
            itemSalesPrice.SellingPrice = value.SellingPrice;
            itemSalesPrice.StartDate = value.StartDate;
            itemSalesPrice.EndDate = value.EndDate;
            itemSalesPrice.ModifiedByUserId = value.ModifiedByUserID;
            itemSalesPrice.ModifiedDate =DateTime.Now;
            itemSalesPrice.StatusCodeId = value.StatusCodeID.Value;
           
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemSalesPrice")]
        public void Delete(int id)
        {
            var itemSalesPrice = _context.ItemSalesPrice.SingleOrDefault(p => p.ItemSalesPriceId == id);
            if (itemSalesPrice != null)
            {
                _context.ItemSalesPrice.Remove(itemSalesPrice);
                _context.SaveChanges();
            }
        }
    }
}