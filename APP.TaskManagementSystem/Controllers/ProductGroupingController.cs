﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductGroupingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductGroupingController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductGroupings")]
        public List<ProductGroupingModel> GetProductGroupings()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProductGroupingModel> productGroupingModels = new List<ProductGroupingModel>();
            var productGroupings = _context.ProductGrouping.Include("AddedByUser").Include(s => s.SupplyTo).Include("ModifiedByUser").Include(s => s.StatusCode).Include(s => s.Company).OrderByDescending(o => o.ProductGroupingId).AsNoTracking().ToList();
            if (productGroupings != null && productGroupings.Count > 0)
            {
                productGroupings.ForEach(s =>
                {
                    ProductGroupingModel productGroupingModel = new ProductGroupingModel();

                    productGroupingModel.ProductGroupingId = s.ProductGroupingId;
                    productGroupingModel.ProductGroupCode = s.ProductGroupCode;
                    productGroupingModel.ProductNameId = s.ProductNameId;
                    productGroupingModel.CompanyId = s.CompanyId;
                    productGroupingModel.CompanyName = s.Company?.PlantCode;
                    productGroupingModel.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.ProductNameId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.ProductNameId).Value : string.Empty;
                    productGroupingModel.QuotationUnitsId = s.QuotationUnitsId;
                    productGroupingModel.UOM = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.QuotationUnitsId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.QuotationUnitsId).Value : string.Empty;
                    productGroupingModel.PackingInforId = s.PackingInforId;
                    productGroupingModel.PackingUnits = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.PackingInforId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.PackingInforId).Value : string.Empty;
                    productGroupingModel.SupplyTo = s.SupplyTo?.CompanyName;
                    productGroupingModel.SupplyToId = s.SupplyToId;
                    productGroupingModel.AddedByUserID = s.AddedByUserId;
                    productGroupingModel.ModifiedByUserID = s.ModifiedByUserId;
                    productGroupingModel.StatusCodeID = s.StatusCodeId;
                    productGroupingModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    productGroupingModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    productGroupingModel.StatusCode = s.StatusCode?.CodeValue;
                    productGroupingModel.AddedDate = s.AddedDate;
                    productGroupingModel.ModifiedDate = s.ModifiedDate;
                    productGroupingModels.Add(productGroupingModel);


                });
            }

            return productGroupingModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "GetProject")]
        [HttpGet("GetProductGrouping/{id:int}")]
        public ActionResult<ProductGroupingModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var productGrouping = _context.ProductGrouping.SingleOrDefault(p => p.ProductGroupingId == id.Value);
            var result = _mapper.Map<ProductGroupingModel>(productGrouping);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductGroupingModel> GetData(SearchModel searchModel)
        {
            var productGrouping = new ProductGrouping();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).FirstOrDefault();
                        break;
                    case "Last":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).LastOrDefault();
                        break;
                    case "Next":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).LastOrDefault();
                        break;
                    case "Previous":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).FirstOrDefault();
                        break;
                    case "Last":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).LastOrDefault();
                        break;
                    case "Next":
                        productGrouping = _context.ProductGrouping.OrderBy(o => o.ProductGroupingId).FirstOrDefault(s => s.ProductGroupingId > searchModel.Id);
                        break;
                    case "Previous":
                        productGrouping = _context.ProductGrouping.OrderByDescending(o => o.ProductGroupingId).FirstOrDefault(s => s.ProductGroupingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductGroupingModel>(productGrouping);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductGrouping")]
        public ProductGroupingModel Post(ProductGroupingModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            string profileNo = string.Empty;
           
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "ProductGrouping" });
            }
            var productGrouping = new ProductGrouping
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                CompanyId = value.CompanyId,
                ProductGroupCode = profileNo,
                ProductNameId = value.ProductNameId,
                PackingInforId = value.PackingInforId,
                QuotationUnitsId = value.QuotationUnitsId,
                SupplyToId = value.SupplyToId,

            };
            _context.ProductGrouping.Add(productGrouping);
            _context.SaveChanges();
            value.AddedDate = productGrouping.AddedDate;
            if (value.CompanyId > 0)
            {
                value.CompanyName = _context.Plant.Where(s => s.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();
            }
            if (value.SupplyToId > 0)
            {
                value.SupplyTo = _context.CompanyListing.Where(s => s.CompanyListingId == value.SupplyToId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.ProductNameId > 0)
            {
                value.ProductName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.ProductNameId).Value : string.Empty;
            }
            if (value.QuotationUnitsId > 0)
            {
                value.UOM = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.QuotationUnitsId).Value : string.Empty;
            }
            if (value.PackingInforId > 0)
            {
                value.PackingUnits = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.PackingInforId).Value : string.Empty;
            }
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == productGrouping.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            value.ProductGroupingId = productGrouping.ProductGroupingId;
            value.ProductGroupCode = productGrouping.ProductGroupCode;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateProductGrouping")]
        public ProductGroupingModel Put(ProductGroupingModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productGrouping = _context.ProductGrouping.SingleOrDefault(p => p.ProductGroupingId == value.ProductGroupingId);
            productGrouping.ModifiedByUserId = value.ModifiedByUserID;
            productGrouping.ModifiedDate = DateTime.Now;
            productGrouping.StatusCodeId = value.StatusCodeID.Value;
            productGrouping.CompanyId = value.CompanyId;
            productGrouping.ProductNameId = value.ProductNameId;
            productGrouping.PackingInforId = value.PackingInforId;
            productGrouping.QuotationUnitsId = value.QuotationUnitsId;
            productGrouping.SupplyToId = value.SupplyToId;

            _context.SaveChanges();
            value.ModifiedDate = productGrouping.ModifiedDate;
            if (value.CompanyId > 0)
            {
                value.CompanyName = _context.Plant.Where(s => s.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();
            }
            if (value.SupplyToId > 0)
            {
                value.SupplyTo = _context.CompanyListing.Where(s => s.CompanyListingId == value.SupplyToId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.ProductNameId > 0)
            {
                value.ProductName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.ProductNameId).Value : string.Empty;
            }
            if (value.QuotationUnitsId > 0)
            {
                value.UOM = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.QuotationUnitsId).Value : string.Empty;
            }
            if (value.PackingInforId > 0)
            {
                value.PackingUnits = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.PackingInforId).Value : string.Empty;
            }
            value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == productGrouping.ModifiedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteProductGrouping")]
        public void Delete(int id)
        {
            try
            {
                var productGrouping = _context.ProductGrouping.SingleOrDefault(p => p.ProductGroupingId == id);
                if (productGrouping != null)
                {
                    _context.ProductGrouping.Remove(productGrouping);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Record Cannot be delete! Reference To Others", ex);
            }
        }
    }
}
