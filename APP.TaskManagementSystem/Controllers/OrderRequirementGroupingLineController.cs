﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OrderRequirementGroupingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public OrderRequirementGroupingLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetOrderRequirementGroupingLine")]
        public List<OrderRequirementGroupingLineModel> GetOrderRequirementGroupingLine(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<OrderRequirementGroupingLineModel> orderRequirementLineModels = new List<OrderRequirementGroupingLineModel>();
            var orderRequirementLine = _context.OrderRequirementGroupingLine
              .Include(s => s.AddedByUser)
              .Include(s => s.ModifiedByUser)
              .Include(s => s.StatusCode)
              .Include(s => s.Product).Where(w => w.ProductionSimulationGroupingId == id).OrderByDescending(o => o.OrderRequirementGroupingLineId).AsNoTracking().ToList();
            if (orderRequirementLine != null && orderRequirementLine.Count > 0)
            {
                orderRequirementLine.ForEach(s =>
                {
                    OrderRequirementGroupingLineModel orderRequirementLineModel = new OrderRequirementGroupingLineModel
                    {
                        OrderRequirementGroupingLineId = s.OrderRequirementGroupingLineId,
                        ProductionSimulationGroupingId = s.ProductionSimulationGroupingId,
                        ProductId = s.ProductId,
                        TicketBatchSizeId = s.TicketBatchSizeId,
                        TicketBatchSizeName = s.TicketBatchSizeId,
                        NavLocationId = s.NavLocationId,
                        NavUomid = s.NavUomid,
                        NavLocationName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.NavLocationId).Select(a => a.Value).SingleOrDefault() : "",
                        NavUomName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.NavUomid).Select(a => a.Value).SingleOrDefault() : "",
                        ProductQty = s.ProductQty,
                        NoOfTicket = s.NoOfTicket,
                        Remarks = s.Remarks,
                        ExpectedStartDate = s.ExpectedStartDate,
                        RequireToSplit = s.RequireToSplit,
                        RequireToSplitFlag = s.RequireToSplit == true ? "1" : "0",
                        ProductName = s.Product?.Description + " " + s.Product?.Description2,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        IsNavSync = s.IsNavSync.GetValueOrDefault(false),
                    };
                    orderRequirementLineModels.Add(orderRequirementLineModel);
                });
            }

            return orderRequirementLineModels;
        }
        [HttpGet]
        [Route("GetOrderRequirementGroupingLineSplit")]
        public List<OrderRequirementGrouPinglineSplitModel> GetOrderRequirementGroupingLineSplit(int? id)
        {
            List<OrderRequirementGrouPinglineSplitModel> orderRequirementLineSplitModels = new List<OrderRequirementGrouPinglineSplitModel>();
            var orderRequirementLineSplit = _context.OrderRequirementGrouPinglineSplit
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("SplitProduct").Where(a => a.OrderRequirementGroupingLineId == id).OrderByDescending(o => o.OrderRequirementGrouPinglineSplitId).AsNoTracking().ToList();
            if (orderRequirementLineSplit != null && orderRequirementLineSplit.Count > 0)
            {
                orderRequirementLineSplit.ForEach(s =>
                {
                    OrderRequirementGrouPinglineSplitModel orderRequirementLineSplitModel = new OrderRequirementGrouPinglineSplitModel
                    {
                        OrderRequirementGrouPinglineSplitId = s.OrderRequirementGrouPinglineSplitId,
                        OrderRequirementGroupingLineId = s.OrderRequirementGroupingLineId,
                        ProductQty = s.ProductQty,
                        SplitProductQty = s.SplitProductQty,
                        Remarks = s.Remarks,
                        SplitProductName = s.SplitProduct != null ? s.SplitProduct.Description : string.Empty,
                        SplitProductId = s.SplitProductId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        IsNavSync = s.IsNavSync.GetValueOrDefault(false)
                    };
                    orderRequirementLineSplitModels.Add(orderRequirementLineSplitModel);
                });

            }
            return orderRequirementLineSplitModels;
        }
        [HttpPost]
        [Route("InsertOrderRequirementGroupingLine")]
        public OrderRequirementGroupingLineModel InsertOrderRequirementGroupingLine(OrderRequirementGroupingLineModel value)
        {
            var OrderRequirementGroupingLine = new OrderRequirementGroupingLine
            {
                ProductionSimulationGroupingId = value.ProductionSimulationGroupingId,
                ProductId = value.ProductId,
                TicketBatchSizeId = value.TicketBatchSizeId,
                ProductQty = value.ProductQty,
                NoOfTicket = value.NoOfTicket,
                NavUomid = value.NavUomid,
                NavLocationId = value.NavLocationId,
                ExpectedStartDate = value.ExpectedStartDate,
                RequireToSplit = value.RequireToSplitFlag == "1" ? true : false,
                Remarks = value.Remarks,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                IsNavSync = false,
            };
            _context.OrderRequirementGroupingLine.Add(OrderRequirementGroupingLine);
            _context.SaveChanges();
            value.OrderRequirementGroupingLineId = OrderRequirementGroupingLine.OrderRequirementGroupingLineId;
            value.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            if (value.ProductId != null && value.ProductId > 0)
            {
                var product = _context.Navitems.Where(s => s.ItemId == value.ProductId).FirstOrDefault();
                if (product != null)
                {
                    value.ProductName = product.Description + " " + product.Description2;
                }
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateOrderRequirementGroupingLine")]
        public OrderRequirementGroupingLineModel UpdateOrderRequirementGroupingLine(OrderRequirementGroupingLineModel value)
        {
            var OrderRequirementGroupingLine = _context.OrderRequirementGroupingLine.SingleOrDefault(p => p.OrderRequirementGroupingLineId == value.OrderRequirementGroupingLineId);
            OrderRequirementGroupingLine.ProductionSimulationGroupingId = value.ProductionSimulationGroupingId;
            OrderRequirementGroupingLine.ProductId = value.ProductId;
            OrderRequirementGroupingLine.ProductQty = value.ProductQty;
            OrderRequirementGroupingLine.TicketBatchSizeId = value.TicketBatchSizeId;
            OrderRequirementGroupingLine.NoOfTicket = value.NoOfTicket;
            OrderRequirementGroupingLine.NavLocationId = value.NavLocationId;
            OrderRequirementGroupingLine.NavUomid = value.NavUomid;
            OrderRequirementGroupingLine.ExpectedStartDate = value.ExpectedStartDate;
            OrderRequirementGroupingLine.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            OrderRequirementGroupingLine.Remarks = value.Remarks;
            OrderRequirementGroupingLine.ModifiedByUserId = value.ModifiedByUserID;
            OrderRequirementGroupingLine.StatusCodeId = value.StatusCodeID.Value;
            OrderRequirementGroupingLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            if (value.ProductId != null && value.ProductId > 0)
            {
                var product = _context.Navitems.Where(s => s.ItemId == value.ProductId).FirstOrDefault();
                if (product != null)
                {
                    value.ProductName = product.Description + " " + product.Description2;
                }
            }
            return value;

        }
        [HttpPost]
        [Route("InsertOrderRequirementGroupingLineSplit")]
        public OrderRequirementGrouPinglineSplitModel InsertOrderRequirementGroupingLineSplit(OrderRequirementGrouPinglineSplitModel value)
        {
            var OrderRequirementGroupingLineSplit = new OrderRequirementGrouPinglineSplit
            {
                OrderRequirementGroupingLineId = value.OrderRequirementGroupingLineId,
                ProductQty = value.ProductQty,
                SplitProductQty = value.SplitProductQty,
                Remarks = value.Remarks,
                SplitProductId = value.SplitProductId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsNavSync = false,
            };
            _context.OrderRequirementGrouPinglineSplit.Add(OrderRequirementGroupingLineSplit);
            _context.SaveChanges();
            value.OrderRequirementGrouPinglineSplitId = OrderRequirementGroupingLineSplit.OrderRequirementGrouPinglineSplitId;
            return value;
        }
        [HttpPut]
        [Route("UpdateOrderRequirementGroupingLineSplit")]
        public OrderRequirementGrouPinglineSplitModel UpdateOrderRequirementGroupingLineSplit(OrderRequirementGrouPinglineSplitModel value)
        {
            var OrderRequirementGroupingLineSplit = _context.OrderRequirementGrouPinglineSplit.SingleOrDefault(p => p.OrderRequirementGrouPinglineSplitId == value.OrderRequirementGrouPinglineSplitId);
            OrderRequirementGroupingLineSplit.OrderRequirementGroupingLineId = value.OrderRequirementGroupingLineId;
            OrderRequirementGroupingLineSplit.ProductQty = value.ProductQty;
            OrderRequirementGroupingLineSplit.SplitProductQty = value.SplitProductQty;
            OrderRequirementGroupingLineSplit.Remarks = value.Remarks;
            OrderRequirementGroupingLineSplit.SplitProductId = value.SplitProductId;
            OrderRequirementGroupingLineSplit.ModifiedByUserId = value.ModifiedByUserID;
            OrderRequirementGroupingLineSplit.StatusCodeId = value.StatusCodeID.Value;
            OrderRequirementGroupingLineSplit.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteOrderRequirementGroupingLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var OrderRequirementGroupingLine = _context.OrderRequirementGroupingLine.SingleOrDefault(p => p.OrderRequirementGroupingLineId == id);
                if (OrderRequirementGroupingLine != null)
                {
                    var OrderRequirementGroupingLineSplit = _context.OrderRequirementGrouPinglineSplit.Where(s => s.OrderRequirementGroupingLineId == id).AsNoTracking().ToList();
                    if (OrderRequirementGroupingLineSplit != null)
                    {
                        _context.OrderRequirementGrouPinglineSplit.RemoveRange(OrderRequirementGroupingLineSplit);
                        _context.SaveChanges();
                    }
                    _context.OrderRequirementGroupingLine.Remove(OrderRequirementGroupingLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteOrderRequirementGroupingLineSplit")]
        public ActionResult<string> DeleteOrderRequirementGroupingLineSplit(int id)
        {
            try
            {
                var OrderRequirementGroupingLineSplit = _context.OrderRequirementGrouPinglineSplit.SingleOrDefault(p => p.OrderRequirementGrouPinglineSplitId == id);
                if (OrderRequirementGroupingLineSplit != null)
                {
                    _context.OrderRequirementGrouPinglineSplit.Remove(OrderRequirementGroupingLineSplit);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
