﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NAVLocationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NAVLocationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNAVLocation")]
        public List<NAVLocationModel> Get(int id)
        {
            var navlocation = _context.Navlocation
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Plant)
                .Include(i => i.Item).Where(t => t.PlantId == id)
               .AsNoTracking()
                .ToList();
            List<NAVLocationModel> NAVLocationModels = new List<NAVLocationModel>();
            navlocation.ForEach(s =>
            {
                NAVLocationModel NAVLocationModel = new NAVLocationModel
                {
                    NAVLocationId = s.NavlocationId,
                    Code = s.Code,
                    Name = s.Name,
                    PlantID = s.PlantId,
                    Description = s.Description,
                    ItemID = s.ItemId,
                    ItemNo = s.Item != null ? s.Item.No : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                NAVLocationModels.Add(NAVLocationModel);
            });
            return NAVLocationModels.OrderByDescending(a => a.NAVLocationId).ToList();
        }


        [HttpGet]
        [Route("GetNAVLocationByItem")]
        public List<NAVLocationModel> GetNAVLocationByItem(int id)
        {
            var navlocation = _context.Navlocation
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Plant)
                .Include(i => i.Item).Where(t => t.ItemId == id)
               .AsNoTracking()
                .ToList();
            List<NAVLocationModel> NAVLocationModels = new List<NAVLocationModel>();
            navlocation.ForEach(s =>
            {
                NAVLocationModel NAVLocationModel = new NAVLocationModel
                {
                    NAVLocationId = s.NavlocationId,
                    Code = s.Code,
                    Name = s.Name,
                    PlantID = s.PlantId,
                    Description = s.Description,
                    ItemID = s.ItemId,
                    ItemNo = s.Item != null ? s.Item.No : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                NAVLocationModels.Add(NAVLocationModel);
            });
            return NAVLocationModels.OrderByDescending(a => a.NAVLocationId).ToList();
        }
    }
}
