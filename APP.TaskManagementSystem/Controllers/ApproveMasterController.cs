﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApproveMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApproveMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApproveMasters")]
        public List<ApproveMasterModel> Get()
        {
            var approveMasters = _context.ApproveMaster
                                            .Include(s => s.User)
                                            .Include(s => s.ApproveLevel)
                                            .Include(s => s.ApproveType)
                                            .Include(s => s.StatusCode)
                                            .Select(s => new ApproveMasterModel
                                            {
                                                ApproveMasterId = s.ApproveMasterId,
                                                UserId = s.UserId,
                                                UserName = s.User.FirstName,
                                                ApproveLevelId = s.ApproveLevelId,
                                                ApproveLevel = s.ApproveLevel.CodeValue,
                                                SessionId = s.SessionId,
                                                ApproveTypeId = s.ApproveTypeId,
                                                ApproveType = s.ApproveType.CodeValue,
                                                ScreenID = s.ScreenId,
                                                ModifiedByUserID = s.AddedByUserId,
                                                AddedByUserID = s.ModifiedByUserId,
                                                StatusCodeID = s.StatusCodeId,
                                                AddedByUser = s.AddedByUser.UserName,
                                                ModifiedByUser = s.ModifiedByUser.UserName,
                                                AddedDate = s.AddedDate,
                                                ModifiedDate = s.ModifiedDate,
                                                StatusCode = s.StatusCode.CodeValue,
                                            }).OrderByDescending(o => o.ApproveMasterId).ToList();
            return approveMasters;
        }

        [HttpGet]
        [Route("GetApproveMastersByScreen")]
        public List<ApproveMasterModel> GetApproveMastersBySession(string screenId)
        {
            List<ApproveMasterModel> approveMasterModels = new List<ApproveMasterModel>();
            var approveMasters = _context.ApproveMaster
                                            .Include(s => s.User)
                                            .Include(s => s.ApproveLevel)
                                            .Include(s => s.ApproveType)
                                            .Include(s => s.StatusCode)
                                            .Include(s => s.AddedByUser)
                                            .Include(s => s.ModifiedByUser).AsNoTracking().ToList();
            approveMasters.ForEach(s =>
            {
                ApproveMasterModel approveMasterModel = new ApproveMasterModel
                {
                    ApproveMasterId = s.ApproveMasterId,
                    UserId = s.UserId,
                    UserName = s.User?.FirstName,
                    ApproveLevelId = s.ApproveLevelId,
                    ApproveLevel = s.ApproveLevel != null ? s.ApproveLevel.CodeValue : "",
                    SessionId = s.SessionId,
                    ApproveTypeId = s.ApproveTypeId,
                    ApproveType = s.ApproveType?.CodeValue,
                    ScreenID = s.ScreenId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                };

                approveMasterModels.Add(approveMasterModel);
            });
            
            return approveMasterModels.Where(s => s.ScreenID.ToString() == screenId).ToList();
        }




        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApproveMasterModel> GetData(SearchModel searchModel)
        {
            var approveMaster = new ApproveMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).LastOrDefault();
                        break;
                    case "Next":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).LastOrDefault();
                        break;
                    case "Next":
                        approveMaster = _context.ApproveMaster.OrderBy(o => o.ApproveMasterId).FirstOrDefault(s => s.ApproveMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        approveMaster = _context.ApproveMaster.OrderByDescending(o => o.ApproveMasterId).FirstOrDefault(s => s.ApproveMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApproveMasterModel>(approveMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApproveMaster")]
        public ApproveMasterModel Post(ApproveMasterModel value)
        {
            var approveMaster = new ApproveMaster
            {
                UserId = value.UserId,
                ApproveLevelId = value.ApproveLevelId,
                ApproveTypeId = value.ApproveTypeId,
                SessionId = value.SessionId,
                ScreenId = value.ScreenID,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.ApproveMaster.Add(approveMaster);
            _context.SaveChanges();
            value.ApproveMasterId = approveMaster.ApproveMasterId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApproveMaster")]
        public ApproveMasterModel Put(ApproveMasterModel value)
        {
            var approveMaster = _context.ApproveMaster.SingleOrDefault(p => p.ApproveMasterId == value.ApproveMasterId);


            approveMaster.UserId = value.UserId;
            approveMaster.ApproveLevelId = value.ApproveLevelId;
            approveMaster.ApproveTypeId = value.ApproveTypeId;
            approveMaster.SessionId = value.SessionId;
            approveMaster.ScreenId = value.ScreenID;
            approveMaster.ModifiedByUserId = value.ModifiedByUserID;
            approveMaster.ModifiedDate = DateTime.Now;
            approveMaster.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApproveMaster")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var approveMaster = _context.ApproveMaster.SingleOrDefault(p => p.ApproveMasterId == id);
                if (approveMaster != null)
                {
                    _context.ApproveMaster.Remove(approveMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Approve Master Cannot be delete! Data Reference to others");
            }
        }
    }
}
