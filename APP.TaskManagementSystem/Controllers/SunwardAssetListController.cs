﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SunwardAssetListController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SunwardAssetListController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSunwardAssetLists")]
        public List<SunwardAssetListModel> Get()
        {
            
                      
                var sunwardAssetList = _context.SunwardAssetList.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Include("DeviceCatalogMaster").Select(s => new SunwardAssetListModel
                {
                    SunwardAssetListID = s.SunwardAssetListId,
                    No = s.No,
                    CompanyID = s.CompanyId,
                    CompanyName = s.Company.Plant.Where(t => t.CompanyId == s.CompanyId).Select(t => t.Description).ToString(),
                    Description = s.Description,
                    UnitOfMeasure = s.UnitOfMeasure,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    DeviceModelName = s.DeviceModelName,
                    SerialNo = s.SerialNo,
                    OldID = s.OldId,
                    NewID = s.NewId,
                    PhotoOfDevice = s.PhotoOfDevice,
                    DeviceStatusID = s.DeviceStatusId,
                    DevicePhysicalCondition = s.DevicePhysicalCondition,
                    CalibrationStatus = s.CalibrationStatus,
                    SiteID = s.SiteId,
                    ZoneID = s.ZoneId,
                    LocationID = s.LocationId,
                    AreaID = s.AreaId,
                    SpecificAreaID = s.SpecificAreaId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode.CodeValue,
                    

                }).OrderByDescending(o => o.SunwardAssetListID).AsNoTracking().ToList();
            
           
            return sunwardAssetList;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetSunwardAssetList/{id:int}")]
        public ActionResult<SunwardAssetListModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var sunwardAssetList = _context.SunwardAssetList.SingleOrDefault(p => p.SunwardAssetListId == id.Value);
            var result = _mapper.Map<SunwardAssetListModel>(sunwardAssetList);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SunwardAssetListModel> GetData(SearchModel searchModel)
        {
            var sunwardAssetList = new SunwardAssetList();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).FirstOrDefault();
                        break;
                    case "Last":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).LastOrDefault();
                        break;
                    case "Next":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).LastOrDefault();
                        break;
                    case "Previous":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).FirstOrDefault();
                        break;
                    case "Last":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).LastOrDefault();
                        break;
                    case "Next":
                        sunwardAssetList = _context.SunwardAssetList.OrderBy(o => o.SunwardAssetListId).FirstOrDefault(s => s.SunwardAssetListId > searchModel.Id);
                        break;
                    case "Previous":
                        sunwardAssetList = _context.SunwardAssetList.OrderByDescending(o => o.SunwardAssetListId).FirstOrDefault(s => s.SunwardAssetListId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SunwardAssetListModel>(sunwardAssetList);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSunwardAssetList")]
        public SunwardAssetListModel Post(SunwardAssetListModel value)
        {
            var sunwardAssetList = new SunwardAssetList
            {
                //SunwardAssetListID = value.SunwardAssetListId,
                No = value.No,
                CompanyId = value.CompanyID,                
                Description = value.Description,
                UnitOfMeasure = value.UnitOfMeasure,
                DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                DeviceModelName = value.DeviceModelName,
                SerialNo = value.SerialNo,
                OldId = value.OldID,
                NewId = value.NewID,
                PhotoOfDevice = value.PhotoOfDevice,
                DeviceStatusId = value.DeviceStatusID,
                DevicePhysicalCondition = value.DevicePhysicalCondition,
                CalibrationStatus = value.CalibrationStatus,
                SiteId = value.SiteID,
                ZoneId = value.ZoneID,
                LocationId = value.LocationID,
                AreaId = value.AreaID,
                SpecificAreaId = value.SpecificAreaID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };
            _context.SunwardAssetList.Add(sunwardAssetList);
            var purchaseInfo = new PurchaseInfo
            {
                Purpose = value.PurchaseInformation.Purpose,
                DepartmentInCharge = value.PurchaseInformation.DepartmentInCharge,
                DateOfPurchase = value.PurchaseInformation.DateOfPurchase,
                Vendor = value.PurchaseInformation.Vendor,
                DeliveryDate = value.PurchaseInformation.DeliveryDate,
                InvoiceNo = value.PurchaseInformation.InvoiceNo,
                Price = value.PurchaseInformation.Price,

                //ContactActivities = new List<ContactActivities>(),

            };


            _context.PurchaseInfo.Add(purchaseInfo);
           
            value.PurchaseInformation.PurchaseInformationID = purchaseInfo.PurchaseInformationId;

            _context.SaveChanges();
            value.SunwardAssetListID = sunwardAssetList.SunwardAssetListId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSunwardAssetList")]
        public SunwardAssetListModel Put(SunwardAssetListModel value)
        {
            var sunwardAssetList = _context.SunwardAssetList.SingleOrDefault(p => p.SunwardAssetListId == value.SunwardAssetListID);

            sunwardAssetList.No = value.No;
            sunwardAssetList.CompanyId = value.CompanyID;
            sunwardAssetList.Description = value.Description;
            sunwardAssetList.UnitOfMeasure = value.UnitOfMeasure;
            sunwardAssetList.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
            sunwardAssetList.DeviceModelName = value.DeviceModelName;
            sunwardAssetList.SerialNo = value.SerialNo;
            sunwardAssetList.OldId = value.OldID;
            sunwardAssetList.NewId = value.NewID;
            sunwardAssetList.PhotoOfDevice = value.PhotoOfDevice;
            sunwardAssetList.DeviceStatusId = value.DeviceStatusID;
            sunwardAssetList.DevicePhysicalCondition = value.DevicePhysicalCondition;
            sunwardAssetList.CalibrationStatus = value.CalibrationStatus;
            sunwardAssetList.SiteId = value.SiteID;
            sunwardAssetList.ZoneId = value.ZoneID;
            sunwardAssetList.LocationId = value.LocationID;
            sunwardAssetList.AreaId = value.AreaID;
            sunwardAssetList.SpecificAreaId = value.SpecificAreaID;
            sunwardAssetList.ModifiedByUserId = value.ModifiedByUserID;
            sunwardAssetList.ModifiedDate = DateTime.Now;
            sunwardAssetList.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSunwardAssetList")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var sunwardAssetList = _context.SunwardAssetList.SingleOrDefault(p => p.SunwardAssetListId == id);
                if (sunwardAssetList != null)
                {
                    _context.SunwardAssetList.Remove(sunwardAssetList);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}