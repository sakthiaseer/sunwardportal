﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CloseDocumentPermissionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public CloseDocumentPermissionController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
        }

      
        [HttpGet]
        [Route("GetFileProfileTypeCloseDocumentAccess")]
        public List<CloseDocumentPermissionModel> GetFileProfileTypeCloseDocumentAccess(int id)
        {
            List<CloseDocumentPermissionModel> closeDocumentPermissionModels = new List<CloseDocumentPermissionModel>();
            var closeDocumentPermissions = _context.CloseDocumentPermission.Where(d => d.FileProfileTypeId == id).ToList();
            //List<DocumentUserRole> documentUserRoles = new List<DocumentUserRole>();

            closeDocumentPermissions = _context.CloseDocumentPermission
                   .Include("User")
                  
                   .Include("UserGroup")

                   .Where(f => f.FileProfileTypeId == id).ToList();
          
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            var documents = _context.Documents.Select(s => new
            {
                DocumentId = s.DocumentId,
                FileName = s.FileName,

            }).ToList();
            List<long> fileProfileUserIds = closeDocumentPermissions.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            List<long> userIds = closeDocumentPermissions.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee
                .Include(d => d.Department)
                .Include(d => d.Designation)
                .Include(d => d.Designation.SubSectionT)
                .Include(d => d.Designation.SubSectionT.SubSecton)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section.Department)
                .Where(e => e.UserId != null && (userIds.Distinct().ToList().Contains(e.UserId.Value) || fileProfileUserIds.Distinct().ToList().Contains(e.UserId.Value))).AsNoTracking().ToList();

            
            if (userIds != null && userIds.Count > 0)
            {
                userIds.ForEach(u =>
                {
                    if (!closeDocumentPermissionModels.Any(r => r.UserId == u))
                    {
                        var employee = employees.FirstOrDefault(e => e.UserId == u);
                        var userUserRole = closeDocumentPermissions.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                        if (userUserRole == null)
                        {
                            userUserRole = closeDocumentPermissions.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                        }
                        var userGroupRole = closeDocumentPermissions.FirstOrDefault(ug => ug.FileProfileTypeId == userUserRole.FileProfileTypeId && ug.UserGroupId != null);
                        List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u && ug.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
                        //DocumentUserRoleModel documentUserRolesModel = new DocumentUserRoleModel();
                        CloseDocumentPermissionModel closeDocumentPermissionModel = new CloseDocumentPermissionModel();
                        closeDocumentPermissionModel.CloseDocumentPermissionId = userUserRole.CloseDocumentPermissionId;
                        closeDocumentPermissionModel.FileProfileTypeId = userUserRole.FileProfileTypeId;
                        closeDocumentPermissionModel.UserId = userUserRole.UserId;              
                        closeDocumentPermissionModel.UserName = userUserRole.User.UserName;
                        closeDocumentPermissionModel.NickName = employee.NickName;                     
                        closeDocumentPermissionModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                        closeDocumentPermissionModel.DepartmentName = employee?.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                        //documentUserRolesModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                        closeDocumentPermissionModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                        closeDocumentPermissionModel.UserGroupIDs = new List<long?> { userUserRole.UserId };

                        closeDocumentPermissionModels.Add(closeDocumentPermissionModel);
                    }
                });
            }
            else
            {
                userIds = closeDocumentPermissions.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    userIds.ForEach(u =>
                    {
                        if (!closeDocumentPermissionModels.Any(r => r.UserId == u))
                        {
                            var employee = employees.FirstOrDefault(e => e.UserId == u);
                            var userUserRole = closeDocumentPermissions.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                            var userGroupRole = closeDocumentPermissions.FirstOrDefault(ug => ug.FileProfileTypeId == userUserRole.FileProfileTypeId && ug.UserGroupId != null);
                            List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u && ug.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
                            CloseDocumentPermissionModel closeDocumentPermissionModel = new CloseDocumentPermissionModel();
                            closeDocumentPermissionModel.CloseDocumentPermissionId = userUserRole.CloseDocumentPermissionId;
                            closeDocumentPermissionModel.FileProfileTypeId = userUserRole.FileProfileTypeId;
                            closeDocumentPermissionModel.UserId = userUserRole.UserId;
                            closeDocumentPermissionModel.UserName = userUserRole.User.UserName;
                            closeDocumentPermissionModel.NickName = employee.NickName;
                            closeDocumentPermissionModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                            closeDocumentPermissionModel.DepartmentName = employee?.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                            //documentUserRolesModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                            closeDocumentPermissionModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                            closeDocumentPermissionModel.UserGroupIDs = new List<long?> { userUserRole.UserId };

                            closeDocumentPermissionModels.Add(closeDocumentPermissionModel);
                        }
                    });
                }
            }
            closeDocumentPermissionModels = closeDocumentPermissionModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
            return closeDocumentPermissionModels.ToList();
        }
       
        // POST: api/User
        [HttpPost]
        [Route("InsertCloseDocumentPermission")]
        public CloseDocumentPermissionModel Post(CloseDocumentPermissionModel value)
        {
            var closeDocumentPermission = new CloseDocumentPermission();
            var existingRole = _context.CloseDocumentPermission.Where(d => d.FileProfileTypeId == value.FileProfileTypeId).ToList();
            var userExistingRole = existingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = existingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();
          
            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            if (value.FileProfileTypeId > 0)
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    if ((userList != null) && (userList.Count > 0))
                    {
                        userList.ForEach(u =>
                        {
                            var userExisting = _context.CloseDocumentPermission.Where(d => d.UserId == u && d.FileProfileTypeId == value.FileProfileTypeId).FirstOrDefault();
                            if (userExisting == null)
                            {
                                closeDocumentPermission = new CloseDocumentPermission
                                {
                                    UserId = u,

                                    UserGroupId = value.UserGroupId,
                                    FileProfileTypeId = value.FileProfileTypeId,

                                };
                                _context.CloseDocumentPermission.Add(closeDocumentPermission);
                                _context.SaveChanges();
                            }

                            if (value.FileProfileTypeIds.Count > 0)
                            {
                                value.FileProfileTypeIds.ForEach(f =>
                                {
                                    var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f && d.DocumentId == null).FirstOrDefault();
                                    if (userExisting == null)
                                    {
                                        closeDocumentPermission = new CloseDocumentPermission
                                        {
                                            UserId = u,
                                            UserGroupId = value.UserGroupId,
                                            FileProfileTypeId = f,

                                        };
                                        _context.CloseDocumentPermission.Add(closeDocumentPermission);
                                        _context.SaveChanges();
                                    }
                                });
                            }

                        });
                    }
                }
                else if ((userGroupList != null) && (userGroupList.Count > 0))
                {
                    userGroupList.ForEach(ug =>
                    {
                        List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                        currentGroupUsers.ForEach(c =>
                        {

                            var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == c.UserId).ToList();
                            if (userExist.Count == 0)
                            {
                                closeDocumentPermission = new CloseDocumentPermission
                                {
                                    UserId = c.UserId,

                                    UserGroupId = ug,
                                    FileProfileTypeId = value.FileProfileTypeId,

                                };
                                _context.CloseDocumentPermission.Add(closeDocumentPermission);
                                _context.SaveChanges();
                            }
                            if (value.FileProfileTypeIds.Count > 0)
                            {
                                value.FileProfileTypeIds.ForEach(f =>
                                {
                                    var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == f && du.UserId == c.UserId).ToList();
                                    if (userExist.Count == 0)
                                    {
                                        closeDocumentPermission = new CloseDocumentPermission
                                        {
                                            UserId = c.UserId,

                                            UserGroupId = ug,
                                            FileProfileTypeId = f,

                                        };
                                        _context.CloseDocumentPermission.Add(closeDocumentPermission);
                                        _context.SaveChanges();
                                    }

                                });
                            }
                        });



                    });
                }

            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCloseDocumentPermission")]
        public CloseDocumentPermissionModel Put(CloseDocumentPermissionModel value)
        {


            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCloseDocumentPermission")]
        public void Delete(int id)
        {
            var CloseDocumentPermission = _context.CloseDocumentPermission.SingleOrDefault(p => p.CloseDocumentPermissionId == id);
            if (CloseDocumentPermission != null)
            {

                _context.CloseDocumentPermission.Remove(CloseDocumentPermission);
                _context.SaveChanges();


            }
        }
       

    }
}
