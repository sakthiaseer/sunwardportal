﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class VendorListController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public VendorListController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetVendorList")]
        public List<VendorListModel> Get()
        {

            List<VendorListModel> vendorListModels = new List<VendorListModel>();

            var vendorList = _context.VendorsList.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.VendorsListId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel

            if (vendorList != null && vendorList.Count > 0)
            {
                vendorList.ForEach(s =>
                {
                    VendorListModel vendorListModel = new VendorListModel
                    {
                        VendorsListID = s.VendorsListId,
                        VendorNo = s.VendorNo,
                        Description = s.Description,
                        OfficeAddressID = s.OfficeAddressId.Value,
                        SiteAddressID = s.SiteAddressId.Value,
                        Name = s.Name,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedByUserID = s.AddedByUserId,
                        AddedDate = DateTime.Now,
                        StatusCodeID = s.StatusCodeId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        ModifiedDate = s.ModifiedDate,
                        SourceListIDs = s.VendorSourceList != null ? s.VendorSourceList.Where(sa => sa.VendorsListId == s.VendorsListId).Select(t => t.SourceListId).ToList() : new List<long?>(),

                    };
                    vendorListModels.Add(vendorListModel);


                });
            }


            // var vendorList = _context.VendorsList.Include("AddedByUser").Include("ModifiedByUser").Select(s => new VendorListModel
            // {
            //     VendorsListID = s.VendorsListId,
            //     VendorNo = s.VendorNo,
            //     Description = s.Description,
            //     OfficeAddressID = s.OfficeAddressId.Value,
            //     SiteAddressID = s.SiteAddressId.Value,
            //     Name = s.Name,
            //     AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
            //     ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
            //     AddedByUserID = s.AddedByUserId,
            //     AddedDate = DateTime.Now,
            //     StatusCodeID = s.StatusCodeId,
            //     ModifiedByUserID = s.ModifiedByUserId,
            //     ModifiedDate = s.ModifiedDate,
            //     SourceListIDs = s.VendorSourceList != null ? s.VendorSourceList.Where(sa => sa.VendorsListId == s.VendorsListId).Select(t => t.SourceListId).ToList() : new List<long?>(),


            // }).OrderByDescending(o => o.VendorsListID).ToList();
            // //var result = _mapper.Map<List<VendorListModel>>(VendorList);
            return vendorListModels;
        }
        [HttpGet("GetVendorByID")]
        public ActionResult<VendorListModel> GetSource(int? id)
        {
            List<ICTCertificateModel> certificateList = new List<ICTCertificateModel>();
            List<ICTContactDetailsModel> contactList = new List<ICTContactDetailsModel>();
            var vendorPayment = new VendorPaymentModel();
            var vendorInvoice = new VendorInvoiceModel();
            if (id == null)
            {
                return NotFound();
            }
            var vendorsList = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id.Value);
            if (vendorsList != null)
            {
                certificateList = _context.Ictcertificate.Select(tn => new ICTCertificateModel
                {
                    ICTCertificateID = tn.IctcertificateId,
                    Name = tn.Name,
                    CertificateType = tn.CertificateType.Value,
                    SourceListID = tn.SourceListId,
                    VendorsListID = tn.VendorsListId,
                    //CertificateType = s.certificatety
                    Link = tn.Link,
                    IsExpired = tn.IsExpired.Value,
                    ExpiryDate = tn.ExpiryDate.Value,
                    AddedByUser = tn.AddedByUser.UserName,
                    ModifiedByUser = tn.ModifiedByUser.UserName,
                    AddedByUserID = tn.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = tn.StatusCodeId,
                    ModifiedByUserID = tn.ModifiedByUserId,
                    ModifiedDate = tn.ModifiedDate,
                }).Where(a => a.VendorsListID == id).AsNoTracking().ToList();

                contactList = _context.IctcontactDetails.Select(s => new ICTContactDetailsModel
                {

                    ContactDetailsID = s.ContactDetailsId,
                    Department = s.Department,
                    Salutation = s.Salutation,
                    ContactName = s.ContactName,
                    Phone = s.Phone.Value,
                    VendorsListID = s.VendorsListId.Value,
                    SourceListID = s.SourceListId.Value,
                    ContactTypeID = s.ContactTypeId.Value,
                    Email = s.Email,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,


                }).Where(a => a.VendorsListID == id).AsNoTracking().ToList();

                vendorInvoice = _context.VendorInvoice.Select(s => new VendorInvoiceModel
                {
                    InvoiceID = s.InvoiceId,
                    GstRegistrationNo = s.GstRegistrationNo,
                    VendorListID = s.VendorListId.Value,

                }).Where(a => a.VendorListID == id).SingleOrDefault();
                vendorPayment = _context.VendorPayment.Select(s => new VendorPaymentModel
                {
                    PaymentID = s.PaymentId,
                    Description = s.Description,
                    PaymentCode = s.PaymentCode,

                    VendorListID = s.VendorListId.Value,

                }).Where(a => a.VendorListID == id).SingleOrDefault();


            }

            var result = _mapper.Map<VendorListModel>(vendorsList);
            if (certificateList.Count > 0)
            {
                result.CertificateList = certificateList;
            }
            if (contactList.Count > 0)
            {
                result.ContactList = contactList;
            }
            if (vendorInvoice != null)
            {
                result.VendorListInvoice = vendorInvoice;
            }
            if (vendorPayment != null)
            {
                result.VenorListPayment = vendorPayment;
            }

            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet]
        [Route("GetActiveVendorList")]
        public List<VendorListModel> GetActiveVendorList()
        {
            var vendorList = _context.VendorsList.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).Select(s => new VendorListModel
            {
                VendorsListID = s.VendorsListId,
                VendorNo = s.VendorNo,
                Description = s.Description,
                OfficeAddressID = s.OfficeAddressId.Value,
                SiteAddressID = s.SiteAddressId.Value,
                Name = s.Name,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.AddedByUser.UserName : "",
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,

            }).OrderByDescending(o => o.VendorsListID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<VendorListModel>>(VendorList);
            return vendorList;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<VendorListModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var vendorList = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id.Value);
            var result = _mapper.Map<VendorListModel>(vendorList);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<VendorListModel> GetData(SearchModel searchModel)
        {
            var vendorList = new VendorsList();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).LastOrDefault();
                        break;
                    case "Next":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).LastOrDefault();
                        break;
                    case "Previous":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).LastOrDefault();
                        break;
                    case "Next":
                        vendorList = _context.VendorsList.OrderBy(o => o.VendorsListId).FirstOrDefault(s => s.VendorsListId > searchModel.Id);
                        break;
                    case "Previous":
                        vendorList = _context.VendorsList.OrderByDescending(o => o.VendorsListId).FirstOrDefault(s => s.VendorsListId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<VendorListModel>(vendorList);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertVendorList")]
        public VendorListModel Post(VendorListModel value)
        {
            var address = new Address
            {
                AddressType = value.OfficeAddress.AddressType,
                PostCode = value.OfficeAddress.PostCode,
                Address1 = value.OfficeAddress.Address1,
                Address2 = value.OfficeAddress.Address2,
                City = value.OfficeAddress.City,
                CountryCode = value.OfficeAddress.CountryCode,
                Country = value.OfficeAddress.Country,
                OfficePhone = value.OfficeAddress.OfficePhone,
                Email = value.OfficeAddress.Email,
                Website = value.OfficeAddress.Website,

            };
            _context.Address.Add(address);
            _context.SaveChanges();
            value.OfficeAddress.AddressID = address.AddressId;
            var siteaddress = new Address
            {
                AddressType = value.SiteAddress.AddressType,
                PostCode = value.SiteAddress.PostCode,
                Address1 = value.SiteAddress.Address1,
                Address2 = value.SiteAddress.Address2,
                City = value.SiteAddress.City,
                CountryCode = value.SiteAddress.CountryCode,
                Country = value.SiteAddress.Country,
                OfficePhone = value.SiteAddress.OfficePhone,
                Email = value.SiteAddress.Email,
                Website = value.SiteAddress.Website,

            };
            _context.Address.Add(siteaddress);
            _context.SaveChanges();
            value.SiteAddress.AddressID = siteaddress.AddressId;
            var vendorList = new VendorsList
            {

                VendorNo = value.VendorNo,
                OfficeAddressId = value.OfficeAddress.AddressID,
                SiteAddressId = value.SiteAddress.AddressID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Name = value.Name,
                Description = value.Description,

            };
            _context.VendorsList.Add(vendorList);
            _context.SaveChanges();
            value.VendorsListID = vendorList.VendorsListId;

            var contactList = value.ContactList;
            if (contactList.Count != 0)
            {
                contactList.ForEach(contact =>
                {
                    if (contact.ContactDetailsID < 0)
                    {
                        var contactDetails = new IctcontactDetails
                        {

                            Department = contact.Department,
                            Salutation = contact.Salutation,
                            ContactName = contact.ContactName,
                            Phone = contact.Phone,
                            ContactTypeId = contact.ContactTypeID,
                            // VendorsListId = contact.VendorsListID,
                            VendorsListId = value.VendorsListID,
                            Email = contact.Email,
                            //ContactTypeId = value.ContactTypeID
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,



                        };
                        _context.IctcontactDetails.Add(contactDetails);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == contact.ContactDetailsID);
                        contactDetails.Department = contact.Department;
                        contactDetails.Salutation = contact.Salutation;
                        // ICTContactDetails.ContactDetailsId = value.ContactDetailsID;
                        contactDetails.ContactName = contact.ContactName;
                        contactDetails.Phone = contact.Phone;
                        contactDetails.VendorsListId = contact.VendorsListID;
                        //contactDetails.SourceListId = contact.SourceListID;
                        contactDetails.Email = contact.Email;
                        //ICTContactDetails.ContactTypeId = value.ContactTypeID;
                        contactDetails.ModifiedByUserId = contact.ModifiedByUserID;
                        contactDetails.ModifiedDate = DateTime.Now;

                        contactDetails.StatusCodeId = contact.StatusCodeID.Value;
                        _context.SaveChanges();

                    }

                }


                );
            }


            var certificatelist = value.CertificateList;
            if (certificatelist.Count != 0)
            {
                certificatelist.ForEach(certificate =>
                {
                    var iCTCertificate = new Ictcertificate
                    {


                        CertificateType = certificate.CertificateType,
                        Name = certificate.Name,
                        Link = certificate.Link,
                        IsExpired = certificate.IsExpired,
                        IssueDate = certificate.IssueDate,
                        ExpiryDate = certificate.ExpiryDate,
                        AddedByUserId = certificate.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = certificate.StatusCodeID.Value,
                        VendorsListId = value.VendorsListID,
                        SourceListId = certificate.SourceListID,

                    };
                    _context.Ictcertificate.Add(iCTCertificate);
                    _context.SaveChanges();
                    //value.ICTCertificateID = iCTCertificate.IctcertificateId;
                }
                 );
            }
            var vendorInvoice = value.VendorListInvoice;
            if (vendorInvoice != null)
            {
                var Invoice = new VendorInvoice
                {

                    GstRegistrationNo = value.VendorListInvoice.GstRegistrationNo,
                    VendorListId = value.VendorsListID,


                };
                _context.VendorInvoice.Add(Invoice);
                _context.SaveChanges();
                value.VendorListInvoice.InvoiceID = Invoice.InvoiceId;
            }
            var vendorpayment = value.VenorListPayment;
            if (vendorpayment != null)
            {
                var vendorPayment = new VendorPayment
                {

                    PaymentCode = value.VenorListPayment.PaymentCode,
                    Description = value.VenorListPayment.Description,
                    VendorListId = value.VendorsListID,


                };
                _context.VendorPayment.Add(vendorPayment);
                _context.SaveChanges();
                value.VenorListPayment.PaymentID = vendorPayment.PaymentId;
            }
            var sourceListIDs = value.SourceListIDs;
            if (sourceListIDs.Count != 0)
            {
                sourceListIDs.ForEach(sourceID =>
                {
                    var vendorSourceList = new VendorSourceList
                    {

                        VendorsListId = value.VendorsListID,
                        SourceListId = sourceID,


                    };
                    _context.VendorSourceList.Add(vendorSourceList);
                    _context.SaveChanges();

                }
                 );
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateVendorList")]
        public VendorListModel Put(VendorListModel value)
        {
            var vendorslist = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == value.VendorsListID);
            vendorslist.VendorNo = value.VendorNo;
            vendorslist.OfficeAddressId = value.OfficeAddressID;
            vendorslist.SiteAddressId = value.SiteAddressID;
            vendorslist.ModifiedByUserId = value.ModifiedByUserID;
            vendorslist.ModifiedDate = DateTime.Now;
            vendorslist.Name = value.Name;
            vendorslist.Description = value.Description;
            vendorslist.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            var contactList = value.ContactList;
            if (contactList.Count != 0)
            {
                contactList.ForEach(contact =>
                {
                    if (contact.ContactDetailsID < 0)
                    {
                        var contactDetails = new IctcontactDetails
                        {

                            Department = contact.Department,
                            Salutation = contact.Salutation,
                            ContactName = contact.ContactName,
                            Phone = contact.Phone,
                            VendorsListId = contact.VendorsListID,
                            ContactTypeId = contact.ContactTypeID,
                            // SourceListId = value.SourceListID,
                            Email = contact.Email,
                            //ContactTypeId = value.ContactTypeID
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,



                        };
                        _context.IctcontactDetails.Add(contactDetails);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == contact.ContactDetailsID);
                        contactDetails.Department = contact.Department;
                        contactDetails.Salutation = contact.Salutation;
                        contactDetails.ContactTypeId = contact.ContactTypeID;
                        // ICTContactDetails.ContactDetailsId = value.ContactDetailsID;
                        contactDetails.ContactName = contact.ContactName;
                        contactDetails.Phone = contact.Phone;
                        contactDetails.VendorsListId = contact.VendorsListID;
                        contactDetails.SourceListId = contact.SourceListID;
                        contactDetails.Email = contact.Email;
                        //ICTContactDetails.ContactTypeId = value.ContactTypeID;
                        contactDetails.ModifiedByUserId = contact.ModifiedByUserID;
                        contactDetails.ModifiedDate = DateTime.Now;

                        contactDetails.StatusCodeId = contact.StatusCodeID.Value;
                        _context.SaveChanges();

                    }

                }


                );
            }


            var certificatelist = value.CertificateList;
            if (certificatelist.Count != 0)
            {
                certificatelist.ForEach(certificate =>
                {
                    if (certificate.ICTCertificateID < 0)
                    {
                        var iCTCertificate = new Ictcertificate
                        {


                            CertificateType = certificate.CertificateType,
                            Name = certificate.Name,
                            Link = certificate.Link,
                            IsExpired = certificate.IsExpired,
                            IssueDate = certificate.IssueDate,
                            ExpiryDate = certificate.ExpiryDate,
                            AddedByUserId = certificate.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = certificate.StatusCodeID.Value,
                            VendorsListId = value.VendorsListID,

                        };
                        _context.Ictcertificate.Add(iCTCertificate);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var iCTCertificate = _context.Ictcertificate.SingleOrDefault(p => p.IctcertificateId == certificate.ICTCertificateID);
                        iCTCertificate.Name = certificate.Name;
                        iCTCertificate.CertificateType = certificate.CertificateType;
                        iCTCertificate.Link = certificate.Link;
                        iCTCertificate.IsExpired = certificate.IsExpired;
                        iCTCertificate.ExpiryDate = certificate.ExpiryDate;
                        iCTCertificate.ModifiedByUserId = certificate.ModifiedByUserID;
                        iCTCertificate.ModifiedDate = DateTime.Now;
                        iCTCertificate.StatusCodeId = certificate.StatusCodeID.Value;
                        iCTCertificate.SourceListId = certificate.SourceListID.Value;
                        iCTCertificate.VendorsListId = certificate.VendorsListID;
                        _context.SaveChanges();
                    }
                    //value.ICTCertificateID = iCTCertificate.IctcertificateId;
                }
                 );
            }
            var sourceListIDs = value.SourceListIDs;
            var vendorSourceList = _context.VendorSourceList.Where(l => l.VendorsListId == value.VendorsListID).AsNoTracking().ToList();
            if (vendorSourceList.Count > 0)
            {
                _context.VendorSourceList.RemoveRange(vendorSourceList);
                _context.SaveChanges();
            }
            if (sourceListIDs.Count != 0)
            {
                sourceListIDs.ForEach(sourceID =>
                {
                    var vendorsource = new VendorSourceList
                    {

                        VendorsListId = value.VendorsListID,
                        SourceListId = sourceID,


                    };
                    _context.VendorSourceList.Add(vendorsource);
                    _context.SaveChanges();

                }
                 );
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteVendorList")]
        public void Delete(int id)
        {
            var vendorList = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id);
            if (vendorList != null)
            {
                var officeaddress = _context.Address.SingleOrDefault(p => p.AddressId == vendorList.OfficeAddressId);
                if (officeaddress != null)
                {
                    _context.Address.Remove(officeaddress);
                    _context.SaveChanges();
                }
                var siteaddress = _context.Address.SingleOrDefault(p => p.AddressId == vendorList.SiteAddressId);
                if (siteaddress != null)
                {
                    _context.Address.Remove(siteaddress);
                    _context.SaveChanges();
                }
                var certificate = _context.Ictcertificate.SingleOrDefault(p => p.VendorsListId == vendorList.VendorsListId);
                if (certificate != null)
                {
                    _context.Ictcertificate.Remove(certificate);
                    _context.SaveChanges();
                }
                var contact = _context.IctcontactDetails.SingleOrDefault(p => p.VendorsListId == vendorList.VendorsListId);
                if (contact != null)
                {
                    _context.IctcontactDetails.Remove(contact);
                    _context.SaveChanges();
                }
                var invoice = _context.VendorInvoice.SingleOrDefault(p => p.VendorListId == vendorList.VendorsListId);
                if (invoice != null)
                {
                    _context.VendorInvoice.Remove(invoice);
                    _context.SaveChanges();
                }
                var payment = _context.VendorPayment.SingleOrDefault(p => p.VendorListId == vendorList.VendorsListId);
                if (payment != null)
                {
                    _context.VendorPayment.Remove(payment);
                    _context.SaveChanges();
                }
                _context.VendorsList.Remove(vendorList);
                _context.SaveChanges();
            }
        }
    }
}