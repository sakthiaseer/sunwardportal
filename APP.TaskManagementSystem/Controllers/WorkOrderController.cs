﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class WorkOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _configuration;
        private readonly IHubContext<ChatHub> _hub;
        public WorkOrderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IConfiguration configuration, IHubContext<ChatHub> hub)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _configuration = configuration;
            _hub = hub;
        }

        #region WorkOrder

        [HttpGet]
        [Route("GetWorkOrderTypes")]
        public List<WorkOrderType> GetWorkOrderTypes()
        {
            List<WorkOrderType> workOrderTypes = new List<WorkOrderType>();
            int crtId = int.Parse(_configuration["WorkOrderType:CRT"]);
            int sunwardId = int.Parse(_configuration["WorkOrderType:Sunward"]);

            workOrderTypes.Add(new WorkOrderType { Name = "CRT", Value = crtId });
            workOrderTypes.Add(new WorkOrderType { Name = "Sunward", Value = sunwardId });
            return workOrderTypes;
        }

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetWorkOrders")]
        public List<WorkOrderModel> Get(int id)
        {
            var applicationUserList = _context.ApplicationUser.Where(a => a.StatusCodeId == 1).ToList();
            var workOrders = _context.WorkOrder
                                .Include(a => a.Assignment)
                                .Include(r => r.Requirement)
                                .Include(m => m.Permission)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .AsNoTracking().OrderByDescending(o => o.WorkOrderId).ToList();
            List<WorkOrderModel> workOrderModels = new List<WorkOrderModel>();
            workOrders.ForEach(s =>
            {
                WorkOrderModel workOrderModel = new WorkOrderModel
                {
                    WorkOrderId = s.WorkOrderId,
                    AssignmentId = s.AssignmentId,
                    AssignmentName = s.Assignment?.CodeValue,
                    RequirementId = s.RequirementId,
                    RequirementName = s.Requirement?.Value,
                    ModuleId = s.ModuleId,
                    ModuleName = s.Permission?.PermissionName,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    TypeId = s.TypeId,
                    AssignTo = s.AssignTo,
                    PermissionID = s.PermissionId,
                    UserGroupId = s.UserGroupId,
                    UserGroupUserIds = s.UserGroup?.UserGroupUser?.Select(a => a.UserId).ToList(),
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    ProfileId = s.ProfileId,
                    ProfileNo = s.ProfileNo,
                    NavisionModuleId = s.NavisionModuleId,

                };
                workOrderModels.Add(workOrderModel);
            });

            return workOrderModels.Where(w => w.AddedByUserID == id || w.AssignTo == id || w.UserGroupUserIds.Contains(id)).ToList();
        }
        [HttpGet]
        [Route("GetUnReadWorkOrderLineDetails")]
        public WorkOrderLineModel GetUnReadWorkOrderLineDetails(long? id)
        {
            WorkOrderLineModel WorkOrderLineModel = new WorkOrderLineModel();
            var WorkOrderLine = _context.WorkOrderLine.Select(s => new
            {
                s.WorkOrderLineId,
                s.Description,
            }).FirstOrDefault(w => w.WorkOrderLineId == id);
            if(WorkOrderLine!=null)
            {
                WorkOrderLineModel.WorkOrderLineId = WorkOrderLine.WorkOrderLineId;
                WorkOrderLineModel.Description = WorkOrderLine.Description;
            }
            return WorkOrderLineModel;
        }
        [HttpGet]
        [Route("GetUnReadWorkOrder")]
        public List<WorkOrderReportModel> GetUnReadWorkOrder(long? id)
        {
            List<WorkOrderType> workOrderTypes = new List<WorkOrderType>();
            workOrderTypes = GetWorkOrderTypes();
            var loginUserGroup = _context.UserGroupUser.Where(s => s.UserId == id).Select(s => s.UserGroupId).ToList();
            var workOrderIds = _context.WorkOrder
                                .AsNoTracking().Where(w => w.AddedByUserId == id || w.AssignTo == id || loginUserGroup.Contains(w.UserGroupId)).OrderByDescending(o => o.WorkOrderId).AsNoTracking().Select(s => s.WorkOrderId).ToList();
            List<WorkOrderReportModel> workOrderReportModels = new List<WorkOrderReportModel>();

            var workOrderLines = _context.WorkOrderLine.Select(s => new
            {
                s.WorkOrderId,
                s.CrtstatusId,
                s.SunwardStatusId,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.StatusCodeId,
                s.PriorityId,
                s.RequirementId,
                s.WorkOrderLineId,
                s.Subject,
                s.StatusUpdateDate,
                s.TaskLink,
                s.ResultLink,
                s.SessionId,
                s.AddedDate,
                s.ModifiedDate,
                s.ExpectedReleaseDate
            }).AsNoTracking().Where(o => workOrderIds.Contains(o.WorkOrderId.Value)).AsQueryable();
            if (workOrderLines != null)
            {
                workOrderLines = workOrderLines.Where(s => s.CrtstatusId != 2373);
                workOrderLines = workOrderLines.Where(s => s.SunwardStatusId != 2350);
                workOrderLines = workOrderLines.Where(s => s.SunwardStatusId != 2353);
                workOrderLines = workOrderLines.Where(s => s.SunwardStatusId != 2357);
            }
            var workOrderLine = workOrderLines.ToList();
            var userIds = workOrderLine.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            userIds.AddRange(workOrderLine.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var codeIds = workOrderLine.Select(s => s.StatusCodeId).ToList();
            codeIds.AddRange(workOrderLine.Select(s => s.PriorityId.GetValueOrDefault(0)).ToList());
            codeIds.AddRange(workOrderLine.Select(s => s.SunwardStatusId.GetValueOrDefault(0)).ToList());
            codeIds.AddRange(workOrderLine.Select(s => s.CrtstatusId.GetValueOrDefault(0)).ToList());
            var masterIds = workOrderLine.Select(s => s.RequirementId.GetValueOrDefault(0)).ToList();
            var WorkOrderIds = workOrderLine.Select(s => s.WorkOrderId.GetValueOrDefault(0)).ToList();
            var WorkOrder = _context.WorkOrder.Select(s => new
            {
                s.WorkOrderId,
                s.TypeId,
                s.ModuleId,
                s.AssignmentId,
                s.AssignTo,
                s.PermissionId,
            }).Where(w => WorkOrderIds.Contains(w.WorkOrderId)).ToList();
            codeIds.AddRange(WorkOrder.Select(s => s.AssignmentId.GetValueOrDefault(0)).ToList());
            userIds.AddRange(WorkOrder.Select(s => s.AssignTo.GetValueOrDefault(0)).ToList());
            var permissionIds = WorkOrder.Select(s => s.PermissionId.GetValueOrDefault(0)).ToList();
            var permissionData = _context.ApplicationPermission.Select(s => new
            {
                s.PermissionId,
                s.PermissionName,
            }).Where(w => permissionIds.Contains(w.PermissionId)).ToList();
            var WorkOrderComment = _context.WorkOrderComment.Select(s => new
            {
                s.WorkorderId,
                s.IsRead,
            }).Where(w => WorkOrderIds.Contains(w.WorkorderId.Value)).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
            var masters = _context.ApplicationMasterDetail.Select(s => new
            {
                s.ApplicationMasterDetailId,
                s.Value,
            }).Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();


            workOrderLines.ToList().ForEach(s =>
            {
                var assignTo = WorkOrder.FirstOrDefault(f => f.WorkOrderId == s.WorkOrderId)?.AssignTo;
                var typeId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == s.WorkOrderId)?.TypeId;
                var assignmentId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == s.WorkOrderId)?.AssignmentId;
                var moduleId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == s.WorkOrderId)?.ModuleId;
                var permissionId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == s.WorkOrderId)?.PermissionId;
                WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel
                {
                    WorkOrderId = s.WorkOrderId.Value,
                    TypeId = typeId,
                    WorkOrderType = typeId != null ? workOrderTypes?.FirstOrDefault(w => w.Value == typeId)?.Name : "",
                    WorkOrderLineId = s.WorkOrderLineId,
                    Subject = s.Subject,
                    PriorityId = s.PriorityId,
                    PriorityName = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.PriorityId)?.CodeValue : "",
                    SunwardStatusId = s.SunwardStatusId,
                    SunwardStatusName = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.SunwardStatusId)?.CodeValue : "",
                    StatusUpdateDate = s.StatusUpdateDate,
                    ModuleId = moduleId,
                    ModuleName = permissionData != null && permissionId != null ? permissionData.FirstOrDefault(a => a.PermissionId == permissionId)?.PermissionName : "",
                    AssignmentName = codeMasters != null && assignmentId != null ? codeMasters.FirstOrDefault(a => a.CodeId == assignmentId)?.CodeValue : "",
                    AssignmentId = assignmentId,
                    TaskLink = s.TaskLink,
                    ResultLink = s.ResultLink,
                    CrtstatusId = s.CrtstatusId,
                    CrtStatusName = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.CrtstatusId)?.CodeValue : "",
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    RequirementId = s.RequirementId,
                    RequirementName = masters != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RequirementId)?.Value : "",
                    AssignTo = assignTo,
                    AssignToName = appUsers != null && assignTo != null ? appUsers.FirstOrDefault(a => a.UserId == assignTo)?.UserName : "",
                    PermissionID = permissionId,
                    ExpectedReleaseDate = s.ExpectedReleaseDate,
                    IsUnreadComment = WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead != null ? WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead : true,
                    AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.StatusCodeId)?.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,

                };
                workOrderReportModels.Add(workOrderReportModel);
            });
            return workOrderReportModels.Where(w => w.IsUnreadComment == false).ToList();
        }

        [HttpGet]
        [Route("GetWorkOrderSubject")]
        public List<WorkOrderReportModel> GetWorkOrderSubject(long? id)
        {
            List<WorkOrderReportModel> workOrderModels = new List<WorkOrderReportModel>();
            var workorderLine = _context.WorkOrderCommentUser
                .Where(w => w.UserId == id).AsNoTracking().ToList();
            var workorderLineIds = workorderLine.Select(s => s.WorkOrderLineId).Distinct().ToList();
            var WorkOrderCommentIds = workorderLine.Select(s => s.WorkOrderCommentId).Distinct().ToList();
            var workOrderComment = _context.WorkOrderComment.Select(s => new
            {
                s.IsRead,
                s.WorkOrderCommentId,
                s.WorkorderId
            }).Where(w => WorkOrderCommentIds.Contains(w.WorkOrderCommentId)).ToList();
            var workorderLines = _context.WorkOrderLine.Select(s => new
            {
                s.WorkOrderId,
                s.WorkOrderLineId,
                s.PriorityId,
                s.CrtstatusId,
                s.RequirementId,
                s.SunwardStatusId,
                s.ModifiedByUserId,
                s.StatusCodeId,
                s.AddedByUserId,
                s.Subject,
                s.StatusUpdateDate,
                s.TaskLink,
                s.ResultLink,
                s.SessionId,
                s.ExpectedReleaseDate,
                s.AddedDate,
                s.ModifiedDate
            }).Where(w=>workorderLineIds.Contains(w.WorkOrderLineId)).ToList();
            var workordersIds = workorderLines.Select(s => s.WorkOrderId).ToList();
            var userIds = workorderLines.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            userIds.AddRange(workorderLines.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var codeIds = workorderLines.Select(s => s.StatusCodeId).ToList();
            codeIds.AddRange(workorderLines.Select(s => s.PriorityId.GetValueOrDefault(0)).ToList());
            codeIds.AddRange(workorderLines.Select(s => s.SunwardStatusId.GetValueOrDefault(0)).ToList());
            codeIds.AddRange(workorderLines.Select(s => s.CrtstatusId.GetValueOrDefault(0)).ToList());
            var masterIds = workorderLines.Select(s => s.RequirementId.GetValueOrDefault(0)).ToList();
           
            var WorkOrder = _context.WorkOrder.Select(s => new
            {
                s.WorkOrderId,
                s.TypeId,
                s.ModuleId,
                s.AssignmentId,
                s.NavisionModuleId,
                s.PermissionId,
                s.AssignTo,
            }).Where(w => workordersIds.Contains(w.WorkOrderId)).ToList();
            codeIds.AddRange(WorkOrder.Select(s => s.AssignmentId.GetValueOrDefault(0)).ToList());
            masterIds.AddRange(WorkOrder.Select(s => s.NavisionModuleId.GetValueOrDefault(0)).ToList());
            var permissionIds = WorkOrder.Select(s => s.PermissionId.GetValueOrDefault(0)).ToList();
            var permissionData = _context.ApplicationPermission.Select(s => new
            {
                s.PermissionId,
                s.PermissionName,
            }).Where(w => permissionIds.Contains(w.PermissionId)).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
            var masters = _context.ApplicationMasterDetail.Select(s => new
            {
                s.ApplicationMasterDetailId,
                s.Value,
            }).Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
            if (workorderLines != null)
            {
                List<WorkOrderType> workOrderTypes = new List<WorkOrderType>();
                workOrderTypes = GetWorkOrderTypes();
                workorderLines.ForEach(s =>
                {
                    var sunwardStatusId = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.SunwardStatusId;
                    var priorityId = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.PriorityId;
                    var crtstatusId = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.CrtstatusId;
                    var requirementId = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.RequirementId;
                    var workorderId= workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.WorkOrderId;
                    WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel();

                    workOrderReportModel.WorkOrderId = (long)workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.WorkOrderId;
                    workOrderReportModel.TypeId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.TypeId;
                    workOrderReportModel.WorkOrderType = workOrderTypes?.FirstOrDefault(w => w.Value == workOrderReportModel.TypeId)?.Name;
                    workOrderReportModel.WorkOrderLineId = s.WorkOrderLineId;
                    workOrderReportModel.Subject = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.Subject;
                    //workOrderReportModel.Description = s.WorkOrderLine?.Description;
                    workOrderReportModel.PriorityId = priorityId;
                    workOrderReportModel.PriorityName = codeMasters != null && priorityId != null ? codeMasters.FirstOrDefault(a => a.CodeId == priorityId)?.CodeValue : "";
                    workOrderReportModel.SunwardStatusId = sunwardStatusId;
                    workOrderReportModel.SunwardStatusName = codeMasters != null && sunwardStatusId != null ? codeMasters.FirstOrDefault(a => a.CodeId == sunwardStatusId)?.CodeValue : "";
                    workOrderReportModel.StatusUpdateDate = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.StatusUpdateDate;
                    workOrderReportModel.ModuleId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.ModuleId;
                    var assignmentId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.AssignmentId;
                    var permissionId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.PermissionId;
                    var navisionModuleId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.NavisionModuleId;
                    if (assignmentId == 2390)
                    {
                        workOrderReportModel.ModuleName = masters != null && navisionModuleId !=null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == navisionModuleId)?.Value : "";
                    }
                    else if (assignmentId == 2391)
                    {
                        workOrderReportModel.ModuleName = permissionData != null && permissionId != null ? permissionData.FirstOrDefault(a => a.PermissionId == permissionId)?.PermissionName : "";
                    }

                    workOrderReportModel.AssignmentName = codeMasters != null && assignmentId != null ? codeMasters.FirstOrDefault(a => a.CodeId == assignmentId)?.CodeValue : "";
                    workOrderReportModel.AssignmentId = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.AssignmentId;
                    workOrderReportModel.TaskLink = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.TaskLink;
                    workOrderReportModel.ResultLink = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.ResultLink;
                    workOrderReportModel.CrtstatusId = crtstatusId;
                    workOrderReportModel.CrtStatusName = codeMasters != null && crtstatusId != null ? codeMasters.FirstOrDefault(a => a.CodeId == crtstatusId)?.CodeValue : "";
                    workOrderReportModel.SessionId = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.SessionId;
                    workOrderReportModel.ModifiedByUserID = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.AddedByUserId;
                    workOrderReportModel.AddedByUserID = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.ModifiedByUserId;
                    workOrderReportModel.AddedDate = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.AddedDate;
                    workOrderReportModel.ModifiedDate = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.ModifiedDate;
                    workOrderReportModel.RequirementId = requirementId;
                    workOrderReportModel.RequirementName = masters != null && requirementId !=null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == requirementId)?.Value : "";
                    workOrderReportModel.AssignTo = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.AssignTo;
                    //workOrderReportModel.AssignToName = s.WorkOrderLine?.WorkOrder?.AssignToNavigation?.UserName;
                    workOrderReportModel.PermissionID = WorkOrder.FirstOrDefault(f => f.WorkOrderId == workorderId)?.PermissionId;
                    workOrderReportModel.ExpectedReleaseDate = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.ExpectedReleaseDate;
                   // workOrderReportModel.IsUnreadComment = workOrderComment.FirstOrDefault(f => f.WorkOrderCommentId == s.wo)?.IsRead;
                    //workOrderReportModel.AddedByUser = s.WorkOrderLine?.AddedByUser != null ? s.WorkOrderLine?.AddedByUser?.UserName : "";
                    var modifiedUserId= workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.ModifiedByUserId;
                    workOrderReportModel.ModifiedByUser =  appUsers != null && modifiedUserId != null ? appUsers.FirstOrDefault(a => a.UserId == modifiedUserId)?.UserName : "";
                    //workOrderReportModel.StatusCode = s.WorkOrderLine?.StatusCode != null ? s.WorkOrderLine?.StatusCode.CodeValue : "";
                    workOrderReportModel.StatusCodeID = workorderLines.FirstOrDefault(f => f.WorkOrderLineId == s.WorkOrderLineId)?.StatusCodeId;

                    workOrderModels.Add(workOrderReportModel);
                });
            }
            return workOrderModels;
        }

        [HttpPost]
        [Route("GetWorkOrderBySearch")]
        public List<WorkOrderModel> GetWorkOrderBySearch(WorkOrderSearchModel workOrderSearchModel)
        {
            var applicationUserList = _context.ApplicationUser.Where(a => a.StatusCodeId == 1).ToList();
            var workorderIds = new List<long>();
            var selectIds = new List<long>();
            var workOrders = _context.WorkOrder
                                .Include(a => a.Assignment)
                                .Include(r => r.Requirement)
                                .Include(m => m.Permission)
                                .Include(n => n.NavisionModule)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .AsNoTracking().OrderByDescending(o => o.WorkOrderId)
                               .ToList();
            if (workOrderSearchModel.AssignmentId != null)
            {
                workOrders = workOrders.Where(t => t.AssignmentId == workOrderSearchModel.AssignmentId).ToList();
                //if (selectIds.Count > 0)
                //{
                //    workorderIds.AddRange(selectIds);
                //}
            }
            if (workOrderSearchModel.PermissionID != null)
            {
                workOrders = workOrders.Where(t => t.PermissionId == workOrderSearchModel.PermissionID).ToList();

            }
            if (workOrderSearchModel.NavisionModuleId != null)
            {
                workOrders = workOrders.Where(t => t.NavisionModuleId == workOrderSearchModel.NavisionModuleId).ToList();

            }
            if (workOrderSearchModel.ProfileNo != null && workOrderSearchModel.ProfileNo != "")
            {
                workOrders = workOrders.Where(t => t.ProfileNo == workOrderSearchModel.ProfileNo).ToList();

            }
            if (workOrderSearchModel.StatusCodeID != null)
            {
                workOrders = workOrders.Where(t => t.StatusCodeId == workOrderSearchModel.StatusCodeID).ToList();

            }

            List<WorkOrderModel> workOrderModels = new List<WorkOrderModel>();
            workOrders.ForEach(s =>
            {
                WorkOrderModel workOrderModel = new WorkOrderModel();

                workOrderModel.WorkOrderId = s.WorkOrderId;
                workOrderModel.AssignmentId = s.AssignmentId;
                workOrderModel.AssignmentName = s.Assignment?.CodeValue;
                workOrderModel.RequirementId = s.RequirementId;
                workOrderModel.RequirementName = s.Requirement?.Value;
                workOrderModel.ModuleId = s.ModuleId;
                if (s.PermissionId != null)
                {
                    workOrderModel.ModuleName = s.Permission?.PermissionName;
                }
                if (s.NavisionModuleId != null)
                {
                    workOrderModel.ModuleName = s.NavisionModule?.Value;
                }
                workOrderModel.AddedByUserID = s.AddedByUserId;
                workOrderModel.ModifiedByUserID = s.ModifiedByUserId;
                workOrderModel.AddedDate = s.AddedDate;
                workOrderModel.ModifiedDate = s.ModifiedDate;
                workOrderModel.TypeId = s.TypeId;
                workOrderModel.AssignTo = s.AssignTo;
                workOrderModel.PermissionID = s.PermissionId;
                workOrderModel.UserGroupId = s.UserGroupId;
                workOrderModel.UserGroupUserIds = s.UserGroup?.UserGroupUser?.Select(a => a.UserId).ToList();
                workOrderModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                workOrderModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                workOrderModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                workOrderModel.StatusCodeID = s.StatusCodeId;
                workOrderModel.ProfileId = s.ProfileId;
                workOrderModel.ProfileNo = s.ProfileNo;
                workOrderModel.NavisionModuleId = s.NavisionModuleId;


                workOrderModels.Add(workOrderModel);
            });

            return workOrderModels.Where(w => w.AddedByUserID == workOrderSearchModel.Id || w.AssignTo == workOrderSearchModel.Id).ToList();
        }

        [HttpPost]
        [Route("GetWorkOrderReportBySearch")]
        public List<WorkOrderReportModel> GetWorkOrderReportBySearch(WorkOrderReportSearchModel workOrderReportSearchModel)
        {
            var applicationUserList = _context.ApplicationUser.Where(a => a.StatusCodeId == 1).ToList();
            var loginUserGroup = _context.UserGroupUser.Where(s => s.UserId == workOrderReportSearchModel.Id).Select(s => s.UserGroupId).ToList();
            List<WorkOrderReportModel> workOrderReportModels = new List<WorkOrderReportModel>();
            var selectIds = new List<long>();
            var workOrders = _context.WorkOrder
                                .Include(a => a.Assignment)
                                .Include(r => r.Requirement)
                                .Include(m => m.Permission)
                                .Include(n => n.NavisionModule)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(s => s.WorkOrderLine)
                                .AsNoTracking().OrderByDescending(o => o.WorkOrderId)
                               .Where(w => w.AddedByUserId == workOrderReportSearchModel.Id || w.AssignTo == workOrderReportSearchModel.Id || loginUserGroup.Contains(w.UserGroupId)).OrderByDescending(o => o.WorkOrderId).AsNoTracking().ToList();
            if (workOrderReportSearchModel.AssignmentId != null)
            {
                workOrders = workOrders.Where(t => t.AssignmentId == workOrderReportSearchModel.AssignmentId).ToList();
                //if (selectIds.Count > 0)
                //{
                //    workorderIds.AddRange(selectIds);
                //}
            }
            if (workOrderReportSearchModel.PermissionID != null)
            {
                workOrders = workOrders.Where(t => t.PermissionId == workOrderReportSearchModel.PermissionID).ToList();

            }
            if (workOrderReportSearchModel.NavisionModuleId != null)
            {
                workOrders = workOrders.Where(t => t.NavisionModuleId == workOrderReportSearchModel.NavisionModuleId).ToList();

            }

            if (workOrderReportSearchModel.StatusCodeID != null)
            {
                workOrders = workOrders.Where(t => t.StatusCodeId == workOrderReportSearchModel.StatusCodeID).ToList();

            }
            List<long> workorderIds = workOrders.Select(s => s.WorkOrderId).Distinct().ToList();
            List<long> workorderIdswithoutLines = workOrders.Where(w => w.WorkOrderLine.Count == 0).Select(s => s.WorkOrderId).ToList();
            var workOrderLines = _context.WorkOrderLine
                               .Include(a => a.WorkOrder.Assignment)
                              .Include(m => m.WorkOrder.Permission)
                              .Include(p => p.Requirement)
                              .Include(p => p.Priority)
                              .Include(p => p.SunwardStatus)
                              .Include(p => p.Crtstatus)
                              .Include(p => p.AddedByUser)
                              .Include(p => p.WorkOrderComment)
                              .Include(r => r.WorkOrder.AssignToNavigation)
                              .Include(i => i.WorkOrder.NavisionModule)
                              .AsNoTracking().Where(o => workorderIds.Contains(o.WorkOrderId.Value)).ToList();
            if (workOrderReportSearchModel.PriorityId != null)
            {
                workOrderLines = workOrderLines.Where(w => w.PriorityId == workOrderReportSearchModel.PriorityId).ToList();
            }
            if (workOrderReportSearchModel.SunwardStatusId != null)
            {
                workOrderLines = workOrderLines.Where(w => w.SunwardStatusId == workOrderReportSearchModel.SunwardStatusId).ToList();
            }
            if (workOrderReportSearchModel.CrtstatusId != null)
            {
                workOrderLines = workOrderLines.Where(w => w.CrtstatusId == workOrderReportSearchModel.CrtstatusId).ToList();
            }

            workOrderLines.ForEach(s =>
            {
                WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel();

                workOrderReportModel.WorkOrderId = s.WorkOrderId.Value;
                workOrderReportModel.TypeId = s.WorkOrder?.TypeId;
                workOrderReportModel.WorkOrderLineId = s.WorkOrderLineId;
                workOrderReportModel.Subject = s.Subject;
                workOrderReportModel.Description = s.Description;
                workOrderReportModel.PriorityId = s.PriorityId;
                workOrderReportModel.PriorityName = s.Priority?.CodeValue;
                workOrderReportModel.SunwardStatusId = s.SunwardStatusId;
                workOrderReportModel.SunwardStatusName = s.SunwardStatus?.CodeValue;
                workOrderReportModel.StatusUpdateDate = s.StatusUpdateDate;
                workOrderReportModel.ModuleId = s.WorkOrder?.ModuleId;
                if (s.WorkOrder?.AssignmentId == 2390)
                {
                    workOrderReportModel.ModuleName = s.WorkOrder?.NavisionModule?.Value;
                }
                else if (s.WorkOrder?.AssignmentId == 2391)
                {
                    workOrderReportModel.ModuleName = s.WorkOrder?.Permission?.PermissionName;
                }

                workOrderReportModel.AssignmentName = s.WorkOrder?.Assignment?.CodeValue;
                workOrderReportModel.AssignmentId = s.WorkOrder?.AssignmentId;
                workOrderReportModel.TaskLink = s.TaskLink;
                workOrderReportModel.ResultLink = s.ResultLink;
                workOrderReportModel.CrtstatusId = s.CrtstatusId;
                workOrderReportModel.CrtStatusName = s.Crtstatus?.CodeValue;
                workOrderReportModel.SessionId = s.SessionId;
                workOrderReportModel.ModifiedByUserID = s.AddedByUserId;
                workOrderReportModel.AddedByUserID = s.ModifiedByUserId;
                workOrderReportModel.AddedDate = s.AddedDate;
                workOrderReportModel.ModifiedDate = s.ModifiedDate;
                workOrderReportModel.RequirementId = s.RequirementId;
                workOrderReportModel.RequirementName = s.Requirement?.Value;
                workOrderReportModel.AssignTo = s.WorkOrder?.AssignTo;
                workOrderReportModel.AssignToName = s.WorkOrder?.AssignToNavigation?.UserName;
                workOrderReportModel.PermissionID = s.WorkOrder?.PermissionId;
                workOrderReportModel.ExpectedReleaseDate = s.ExpectedReleaseDate;
                workOrderReportModel.IsUnreadComment = s.WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead != null ? s.WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead : true;
                workOrderReportModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                workOrderReportModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                workOrderReportModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                workOrderReportModel.StatusCodeID = s.StatusCodeId;

                workOrderReportModels.Add(workOrderReportModel);
            });

            //if(workorderIdswithoutLines.Count>0)
            //{
            //    workOrders.ForEach(s =>
            //    {
            //        WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel();

            //        workOrderReportModel.WorkOrderId = s.WorkOrderId;
            //        workOrderReportModel.TypeId = s.TypeId;

            //        workOrderReportModel.ModuleId = s.ModuleId;
            //        if (s.AssignmentId == 2390)
            //        {
            //            workOrderReportModel.ModuleName = s.NavisionModule?.Value;
            //        }
            //        else if (s.AssignmentId == 2391)
            //        {
            //            workOrderReportModel.ModuleName = s.Permission?.PermissionName;
            //        }

            //        workOrderReportModel.AssignmentName = s.Assignment?.CodeValue;
            //        workOrderReportModel.AssignmentId = s.AssignmentId;                   
            //        workOrderReportModel.SessionId = s.SessionId;
            //        workOrderReportModel.ModifiedByUserID = s.AddedByUserId;
            //        workOrderReportModel.AddedByUserID = s.ModifiedByUserId;
            //        workOrderReportModel.AddedDate = s.AddedDate;
            //        workOrderReportModel.ModifiedDate = s.ModifiedDate;
            //        workOrderReportModel.RequirementId = s.RequirementId;
            //        workOrderReportModel.RequirementName = s.Requirement?.Value;
            //        workOrderReportModel.AssignTo = s.AssignTo;
            //        workOrderReportModel.AssignToName = s.AssignToNavigation?.UserName;
            //        workOrderReportModel.PermissionID = s.PermissionId;                  
            //        workOrderReportModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
            //        workOrderReportModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
            //        workOrderReportModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
            //        workOrderReportModel.StatusCodeID = s.StatusCodeId;

            //        workOrderReportModels.Add(workOrderReportModel);
            //    });
            //}
            return workOrderReportModels.ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<WorkOrderModel> GetData(SearchModel searchModel)
        {
            var workOrder = new WorkOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).LastOrDefault();
                        break;
                    case "Next":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).LastOrDefault();
                        break;
                    case "Next":
                        workOrder = _context.WorkOrder.OrderBy(o => o.WorkOrderId).FirstOrDefault(s => s.WorkOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        workOrder = _context.WorkOrder.OrderByDescending(o => o.WorkOrderId).FirstOrDefault(s => s.WorkOrderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<WorkOrderModel>(workOrder);
            return result;
        }
        // POST: api/WorkOrder
        [HttpPost]
        [Route("InsertWorkOrder")]
        public WorkOrderModel Post(WorkOrderModel value)
        {
            value.ProfileId = _context.ApplicationForm.FirstOrDefault(a => a.ScreenId == value.ScreenID).ProfileId;
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Work Order" });
            var sessionId = Guid.NewGuid();
            int crtId = int.Parse(_configuration["WorkOrderType:CRT"]);
            int sunwardId = int.Parse(_configuration["WorkOrderType:Sunward"]);
            int crtuserId = int.Parse(_configuration["AssignTo:CRT"]);
            int sunwarduserId = int.Parse(_configuration["AssignTo:Sunward"]);
            if (value.TypeId == crtId)
            {
                value.AssignTo = crtuserId;
            }
            if (value.TypeId == sunwardId)
            {
                value.AssignTo = sunwarduserId;
            }
            if (value.AssignmentId == 2390)
            {
                value.ModuleId = null;
            }
            else if (value.AssignmentId == 2391)
            {
                value.NavisionModuleId = null;
            }
            var workOrder = new WorkOrder
            {
                AssignmentId = value.AssignmentId,
                RequirementId = value.RequirementId,
                ModuleId = value.ModuleId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,
                TypeId = value.TypeId,
                AssignTo = value.AssignTo,
                PermissionId = value.PermissionID,
                UserGroupId = value.UserGroupId,
                ProfileNo = profileNo,
                NavisionModuleId = value.NavisionModuleId,
            };
            _context.WorkOrder.Add(workOrder);
            _context.SaveChanges();
            value.SessionId = sessionId;
            NotificationMessage(value, value.WorkOrderId);
            value.WorkOrderId = workOrder.WorkOrderId;
            if(profileNo!=null && profileNo!="")
            {
                value.ProfileNo = profileNo;
            }
            return value;
        }
        private async void NotificationMessage(WorkOrderModel value, long WorkOrderId)
        {
            var notificationHandelerRemove = _context.NotificationHandler.Where(w => w.SessionId == value.SessionId && w.ScreenId == value.ScreenID).ToList();
            if (notificationHandelerRemove != null)
            {
                _context.NotificationHandler.RemoveRange(notificationHandelerRemove);
                _context.SaveChanges();
            }
            var notificationHandler = new NotificationHandler
            {
                SessionId = value.SessionId,
                NextNotifyDate = DateTime.Now,
                NotifyStatusId = 1,
                AddedDays = 0,
                Title = "Work Order",
                Message = WorkOrderId > 0 ? "Work Order Updated" : "New Work Order Assigned",
                ScreenId = value.ScreenID,
                NotifyTo = value.AssignTo
            };
            _context.NotificationHandler.Add(notificationHandler);
            if (WorkOrderId > 0)
            {
                await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("WorkOrder", "New");
            }
            else
            {
                await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("WorkOrder", "Update");
            }
            _context.SaveChanges();
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateWorkOrder")]
        public WorkOrderModel Put(WorkOrderModel value)
        {
            var workOrder = _context.WorkOrder.SingleOrDefault(p => p.WorkOrderId == value.WorkOrderId);
            int crtId = int.Parse(_configuration["WorkOrderType:CRT"]);
            int sunwardId = int.Parse(_configuration["WorkOrderType:Sunward"]);
            int crtuserId = int.Parse(_configuration["AssignTo:CRT"]);
            int sunwarduserId = int.Parse(_configuration["AssignTo:Sunward"]);
            if (value.TypeId == crtId)
            {
                value.AssignTo = crtuserId;
            }
            if (value.TypeId == sunwardId)
            {
                value.AssignTo = sunwarduserId;
            }
            if (value.AssignmentId == 2390)
            {
                value.ModuleId = null;
            }
            else if (value.AssignmentId == 2391)
            {
                value.NavisionModuleId = null;
            }
            workOrder.AssignmentId = value.AssignmentId;
            workOrder.RequirementId = value.RequirementId;
            workOrder.ModuleId = value.ModuleId;
            workOrder.ModifiedByUserId = value.ModifiedByUserID;
            workOrder.ModifiedDate = DateTime.Now;
            workOrder.StatusCodeId = value.StatusCodeID.Value;
            workOrder.TypeId = value.TypeId;
            workOrder.AssignTo = value.AssignTo;
            workOrder.PermissionId = value.PermissionID;
            workOrder.UserGroupId = value.UserGroupId;
            workOrder.NavisionModuleId = value.NavisionModuleId;
            _context.SaveChanges();
            NotificationMessage(value, value.WorkOrderId);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteWorkOrder")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var workOrder = _context.WorkOrder.SingleOrDefault(p => p.WorkOrderId == id);
                if (workOrder != null)
                {
                    _context.WorkOrder.Remove(workOrder);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Workorder Cannot be delete! work order Reference to others");
            }
        }

        #endregion

        #region WorkOrderLine

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetWorkOrderLines")]
        public List<WorkOrderLineModel> GetWorkOrderLines(int id)
        {
            var workOrderLines = _context.WorkOrderLine
                                .Include(p => p.Priority)
                                .Include(p => p.SunwardStatus)
                                .Include(p => p.Crtstatus)
                                .Include(p => p.StatusCode)
                                .Include(p => p.AddedByUser)
                                .Include(p => p.ModifiedByUser)
                                .Include(r => r.Requirement)
                                .AsNoTracking().Where(o => o.WorkOrderId == id).ToList();
            List<WorkOrderLineModel> workOrderLineModels = new List<WorkOrderLineModel>();
            workOrderLines.ForEach(s =>
            {
                WorkOrderLineModel workOrderLineModel = new WorkOrderLineModel
                {
                    WorkOrderId = s.WorkOrderId,
                    WorkOrderLineId = s.WorkOrderLineId,
                    Subject = s.Subject,
                    Description = s.Description,
                    PriorityId = s.PriorityId,
                    PriorityName = s.Priority?.CodeValue,
                    SunwardStatusId = s.SunwardStatusId,
                    SunwardStatusName = s.SunwardStatus?.CodeValue,
                    StatusUpdateDate = s.StatusUpdateDate,
                    ResultLink = s.ResultLink,
                    TaskLink = s.TaskLink,
                    CrtstatusId = s.CrtstatusId,
                    CrtStatusName = s.Crtstatus?.CodeValue,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    RequirementId = s.RequirementId,
                    RequirementName = s.Requirement?.Value,
                    ExpectedReleaseDate = s.ExpectedReleaseDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    LastIndex = s.LastIndex,
                };
                workOrderLineModels.Add(workOrderLineModel);
            });

            return workOrderLineModels;
        }



        // POST: api/WorkOrder
        [HttpPost]
        [Route("InsertWorkOrderLine")]
        public WorkOrderLineModel InsertWorkOrderLine(WorkOrderLineModel value)
        {
            var sessionId = Guid.NewGuid();
            value.LastIndex = "00";

            value.WorkOrderProfileNo = "";
            value.WorkOrderProfileNo = _context.WorkOrder.FirstOrDefault(s => s.WorkOrderId == value.WorkOrderId)?.ProfileNo;
            if (value.WorkOrderProfileNo == null)
            {
                value.WorkOrderProfileNo = "";
            }
            var existingworkorderlines = _context.WorkOrderLine.Where(s => s.WorkOrderId == value.WorkOrderId).ToList();
            if (existingworkorderlines.Count == 0)
            {
                value.LastIndex = "01";
            }
            else if (existingworkorderlines.Count > 0)
            {
                value.LastIndex = existingworkorderlines.OrderByDescending(s => s.WorkOrderLineId).LastOrDefault()?.LastIndex;
                value.LastIndex = (Convert.ToInt32(value.LastIndex) + existingworkorderlines.Count).ToString("D" + 2); ;
            }
            var workOrderLine = new WorkOrderLine
            {
                WorkOrderId = value.WorkOrderId,
                Subject = value.WorkOrderProfileNo + "_" + value.LastIndex + "_" + value.Subject,
                Description = value.Description,
                PriorityId = value.PriorityId,
                SunwardStatusId = value.SunwardStatusId,
                StatusUpdateDate = value.StatusUpdateDate,
                ResultLink = value.ResultLink,
                TaskLink = value.TaskLink,
                CrtstatusId = value.CrtstatusId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,
                RequirementId = value.RequirementId,
                ExpectedReleaseDate = value.ExpectedReleaseDate,
                LastIndex = value.LastIndex,
            };
            _context.WorkOrderLine.Add(workOrderLine);
            _context.SaveChanges();
            value.SessionId = sessionId;
            value.WorkOrderLineId = workOrderLine.WorkOrderLineId;
            UpdateWorkOrderLineStatus(value);
            return value;
        }
        private void UpdateWorkOrderLineStatus(WorkOrderLineModel value)
        {
            var WorkOrderLine = _context.WorkOrderLine.Where(w => w.WorkOrderId == value.WorkOrderId).ToList();
            if (WorkOrderLine.Count > 0)
            {
                var WorkOrderLineStatus = WorkOrderLine.Where(w => w.CrtstatusId == 2373).ToList();
                if (WorkOrderLineStatus.Count > 0 && WorkOrderLine.Count == WorkOrderLineStatus.Count)
                {
                    var WorkOrder = _context.WorkOrder.SingleOrDefault(s => s.WorkOrderId == value.WorkOrderId);
                    WorkOrder.StatusCodeId = 2;
                    _context.SaveChanges();
                }
            }
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateWorkOrderLine")]
        public WorkOrderLineModel UpdateWorkOrderLine(WorkOrderLineModel value)
        {
            var workOrderLine = _context.WorkOrderLine.SingleOrDefault(p => p.WorkOrderLineId == value.WorkOrderLineId);
            value.WorkOrderProfileNo = _context.WorkOrder.FirstOrDefault(s => s.WorkOrderId == workOrderLine.WorkOrderId)?.ProfileNo;
            if (value.WorkOrderProfileNo == null)
            {
                value.WorkOrderProfileNo = "";
            }
            var existingworkorderlines = _context.WorkOrderLine.Where(s => s.WorkOrderId == workOrderLine.WorkOrderId).ToList();
            //if (existingworkorderlines.Count > 0)
            //{
            //    value.LastIndex = (Convert.ToInt32(value.LastIndex) + existingworkorderlines.Count).ToString();
            //}

            workOrderLine.WorkOrderId = value.WorkOrderId;
            workOrderLine.Subject = value.Subject;
            workOrderLine.Description = value.Description;
            workOrderLine.PriorityId = value.PriorityId;
            workOrderLine.SunwardStatusId = value.SunwardStatusId;
            workOrderLine.StatusUpdateDate = value.StatusUpdateDate;
            workOrderLine.ResultLink = value.ResultLink;
            workOrderLine.CrtstatusId = value.CrtstatusId;
            workOrderLine.ModifiedByUserId = value.ModifiedByUserID;
            workOrderLine.ModifiedDate = DateTime.Now;
            workOrderLine.StatusCodeId = value.StatusCodeID.Value;
            workOrderLine.RequirementId = value.RequirementId;
            workOrderLine.TaskLink = value.TaskLink;
            workOrderLine.ExpectedReleaseDate = value.ExpectedReleaseDate;

            _context.SaveChanges();
            UpdateWorkOrderLineStatus(value);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteWorkOrderLine")]
        public ActionResult<string> DeleteWorkOrderLine(int id)
        {
            try
            {
                var workOrderLine = _context.WorkOrderLine.SingleOrDefault(p => p.WorkOrderLineId == id);
                if (workOrderLine != null)
                {
                    _context.WorkOrderLine.Remove(workOrderLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Workorderline Cannot be delete! work order line Reference to others");
            }
        }
        [HttpGet]
        [Route("GetApplicationPermissionByUserId")]
        public List<ApplicationPermissionModel> GetApplicationPermissionByUserId(int id)
        {
            List<ApplicationPermissionModel> applicationPermissionModels = new List<ApplicationPermissionModel>();

            var roleIds = _context.ApplicationUserRole.Where(s => s.UserId == id)?.Select(s => s.RoleId).ToList();
            if (roleIds != null)
            {
                var applicationPermissionIds = _context.ApplicationRolePermission.Where(s => roleIds.Contains(s.RoleId)).Select(s => s.PermissionId).ToList();
                if (applicationPermissionIds != null && applicationPermissionIds.Count > 0)
                {
                    var applicationPermissions = _context.ApplicationPermission.Where(a => applicationPermissionIds.Contains(a.PermissionId)).AsNoTracking().ToList();
                    if (applicationPermissions != null && applicationPermissions.Count > 0)
                    {
                        applicationPermissions.ForEach(s =>
                        {
                            ApplicationPermissionModel applicationPermissionModel = new ApplicationPermissionModel();
                            applicationPermissionModel.Name = s.Name;
                            applicationPermissionModel.PermissionID = s.PermissionId;
                            applicationPermissionModel.ParentID = s.ParentId;
                            applicationPermissionModel.PermissionName = s.PermissionName;
                            applicationPermissionModel.PermissionCode = s.PermissionCode;
                            applicationPermissionModel.IsHeader = s.IsHeader;
                            applicationPermissionModels.Add(applicationPermissionModel);

                        });
                    }
                }
            }

            return applicationPermissionModels.Where(r => r.PermissionName != null).ToList();
        }
        [HttpGet]
        [Route("GetApplicationPermissionPaths")]
        public List<ApplicationPermissionModel> GetApplicationPermissionPaths(int id)
        {
            List<ApplicationPermissionModel> permissionList = GetApplicationPermissionByUserId(id);
            List<ApplicationPermissionModel> permissions = new List<ApplicationPermissionModel>();
            List<ApplicationPermissionModel> applicationPermissionModels = new List<ApplicationPermissionModel>();

            permissions = FindMenuPath(permissionList);
            var permissionModellists = permissionList?.Distinct().ToList();
            if (permissionModellists != null && permissionModellists.Count > 0)
            {
                permissionModellists.ForEach(p =>
                {
                    ApplicationPermissionModel applicationPermissionModel = new ApplicationPermissionModel();
                    applicationPermissionModel.Name = p.Name;
                    applicationPermissionModel.PermissionName = p.PermissionName;
                    applicationPermissionModel.PermissionID = p.PermissionID;
                    applicationPermissionModel.ParentID = p.ParentID;
                    applicationPermissionModel.PermissionCode = p.PermissionCode;
                    applicationPermissionModel.IsHeader = p.IsHeader;
                    applicationPermissionModel.PathName = permissions?.Where(t => t.PermissionID == p.PermissionID).Select(t => t.PathName).SingleOrDefault();
                    applicationPermissionModels.Add(applicationPermissionModel);
                });

            }
            applicationPermissionModels = applicationPermissionModels.Where(s => s.ParentID != null && s.PermissionCode == null && s.IsHeader == false).ToList();
            return applicationPermissionModels;
        }
        [HttpGet]
        [Route("GetWorkOrderReport")]
        public List<WorkOrderReportModel> GetWorkOrderReport(int id)
        {
            List<WorkOrderType> workOrderTypes = new List<WorkOrderType>();
            workOrderTypes = GetWorkOrderTypes();
            var loginUserGroup = _context.UserGroupUser.Where(s => s.UserId == id).Select(s => s.UserGroupId).ToList();
            var workOrderIds = _context.WorkOrder
                                .AsNoTracking().Where(w => w.AddedByUserId == id || w.AssignTo == id || loginUserGroup.Contains(w.UserGroupId)).OrderByDescending(o => o.WorkOrderId).AsNoTracking().Select(s => s.WorkOrderId).ToList();

            List<WorkOrderReportModel> workOrderReportModels = new List<WorkOrderReportModel>();
            var workOrderLines = _context.WorkOrderLine
                                 .Include(a => a.WorkOrder.Assignment)
                                .Include(m => m.WorkOrder.Permission)
                                .Include(p => p.Requirement)
                                .Include(p => p.Priority)
                                .Include(p => p.SunwardStatus)
                                .Include(p => p.Crtstatus)
                                .Include(p => p.AddedByUser)
                                .Include(p => p.WorkOrderComment)
                                .Include(r => r.WorkOrder.AssignToNavigation)
                                .Include(i => i.WorkOrder.NavisionModule)
                                .AsNoTracking().Where(o => workOrderIds.Contains(o.WorkOrderId.Value)).ToList();

            workOrderLines.ForEach(s =>
            {
                WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel();

                workOrderReportModel.WorkOrderId = s.WorkOrderId.Value;
                workOrderReportModel.TypeId = s.WorkOrder?.TypeId;
                workOrderReportModel.WorkOrderType = workOrderTypes?.FirstOrDefault(w => w.Value == s.WorkOrder?.TypeId)?.Name;
                workOrderReportModel.WorkOrderLineId = s.WorkOrderLineId;
                workOrderReportModel.Subject = s.Subject;
                workOrderReportModel.Description = s.Description;
                workOrderReportModel.PriorityId = s.PriorityId;
                workOrderReportModel.PriorityName = s.Priority?.CodeValue;
                workOrderReportModel.SunwardStatusId = s.SunwardStatusId;
                workOrderReportModel.SunwardStatusName = s.SunwardStatus?.CodeValue;
                workOrderReportModel.StatusUpdateDate = s.StatusUpdateDate;
                workOrderReportModel.ModuleId = s.WorkOrder?.ModuleId;
                if (s.WorkOrder?.AssignmentId == 2390)
                {
                    workOrderReportModel.ModuleName = s.WorkOrder?.NavisionModule?.Value;
                }
                else if (s.WorkOrder?.AssignmentId == 2391)
                {
                    workOrderReportModel.ModuleName = s.WorkOrder?.Permission?.PermissionName;
                }

                workOrderReportModel.AssignmentName = s.WorkOrder?.Assignment?.CodeValue;
                workOrderReportModel.AssignmentId = s.WorkOrder?.AssignmentId;
                workOrderReportModel.TaskLink = s.TaskLink;
                workOrderReportModel.ResultLink = s.ResultLink;
                workOrderReportModel.CrtstatusId = s.CrtstatusId;
                workOrderReportModel.CrtStatusName = s.Crtstatus?.CodeValue;
                workOrderReportModel.SessionId = s.SessionId;
                workOrderReportModel.ModifiedByUserID = s.AddedByUserId;
                workOrderReportModel.AddedByUserID = s.ModifiedByUserId;
                workOrderReportModel.AddedDate = s.AddedDate;
                workOrderReportModel.ModifiedDate = s.ModifiedDate;
                workOrderReportModel.RequirementId = s.RequirementId;
                workOrderReportModel.RequirementName = s.Requirement?.Value;
                workOrderReportModel.AssignTo = s.WorkOrder?.AssignTo;
                workOrderReportModel.AssignToName = s.WorkOrder?.AssignToNavigation?.UserName;
                workOrderReportModel.PermissionID = s.WorkOrder?.PermissionId;
                workOrderReportModel.ExpectedReleaseDate = s.ExpectedReleaseDate;
                workOrderReportModel.IsUnreadComment = s.WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead != null ? s.WorkOrderComment?.FirstOrDefault(w => w.WorkorderId == s.WorkOrderLineId)?.IsRead : true;
                workOrderReportModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                workOrderReportModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                workOrderReportModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                workOrderReportModel.StatusCodeID = s.StatusCodeId;

                workOrderReportModels.Add(workOrderReportModel);
            });
            return workOrderReportModels;
        }
        #endregion

        [HttpGet]
        [Route("GetWorkOrderReportById")]
        public WorkOrderReportModel GetWorkOrderReportById(int id)
        {
            List<WorkOrderReportModel> workOrderReportModels = new List<WorkOrderReportModel>();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetWorkOrderReport";
            var result = _context.Set<view_GetWorkOrderReport>().FromSqlRaw(sqlQuery).AsQueryable();
            var workOrderLines = result
                 .Select(s => new
                 {
                     s.WorkOrderId,
                     s.WorkOrderLineId,
                     s.Subject,
                     s.PriorityId,
                     s.PriorityName,
                     s.AssignTo,
                     s.Description,
                     s.AssignmentId,

                     s.AssignmentName,
                     s.RequirementId,
                     s.RequirementName,
                     ModuleName = s.AssignmentId == 2390 ? s.NavisionModuleName : s.AssignmentId == 2391 ? s.PermissionName : "",

                     s.ModifiedByUserID,
                     s.AddedByUserID,
                     s.StatusCodeID,
                     s.ModifiedByUser,
                     s.AddedDate,
                     s.ModifiedDate,
                     s.StatusCode,
                     s.PermissionID,
                     s.UserGroupID,
                     s.ProfileNo,
                     s.SunwardStatusId,
                     s.CrtstatusId,
                     s.Crtstatus,
                     s.ResultLink,
                     s.TaskLink,
                     s.SunwardStatus,
                     s.StatusUpdateDate,
                     s.ExpectedReleaseDate,
                     s.AddedByUser,

                 })
                  .Where(a => a.WorkOrderLineId == id).FirstOrDefault();
            WorkOrderReportModel workOrderReportModel = new WorkOrderReportModel();

            if (workOrderLines != null)

            {
               

                workOrderReportModel.WorkOrderId = workOrderLines.WorkOrderId;
                workOrderReportModel.WorkOrderLineId = workOrderLines.WorkOrderLineId;
                workOrderReportModel.Subject = workOrderLines.Subject;
                workOrderReportModel.Description = workOrderLines.Description;
                workOrderReportModel.PriorityId = workOrderLines.PriorityId;
                workOrderReportModel.PriorityName = workOrderLines.PriorityName;
                workOrderReportModel.SunwardStatusId = workOrderLines.SunwardStatusId;
                workOrderReportModel.SunwardStatusName = workOrderLines.SunwardStatus;
                workOrderReportModel.StatusUpdateDate = workOrderLines.StatusUpdateDate;

                workOrderReportModel.ModuleName = workOrderLines.ModuleName;
                workOrderReportModel.AssignmentName = workOrderLines.AssignmentName;
                workOrderReportModel.AssignmentId = workOrderLines.AssignmentId;
                workOrderReportModel.TaskLink = workOrderLines.TaskLink;
                workOrderReportModel.ResultLink = workOrderLines.ResultLink;
                workOrderReportModel.CrtstatusId = workOrderLines.CrtstatusId;
                workOrderReportModel.CrtStatusName = workOrderLines.Crtstatus;
                workOrderReportModel.ModifiedByUserID = workOrderLines.AddedByUserID;
                workOrderReportModel.AddedByUserID = workOrderLines.ModifiedByUserID;
                workOrderReportModel.AddedDate = workOrderLines.AddedDate;
                workOrderReportModel.ModifiedDate = workOrderLines.ModifiedDate;
                workOrderReportModel.RequirementId = workOrderLines.RequirementId;
                workOrderReportModel.RequirementName = workOrderLines.RequirementName;
                workOrderReportModel.AssignTo = workOrderLines.AssignTo;
                workOrderReportModel.PermissionID = workOrderLines.PermissionID;
                workOrderReportModel.ExpectedReleaseDate = workOrderLines.ExpectedReleaseDate;
                workOrderReportModel.AddedByUser = workOrderLines.AddedByUser;
                workOrderReportModel.ModifiedByUser = workOrderLines.ModifiedByUser;
                workOrderReportModel.StatusCode = workOrderLines.StatusCode;
                workOrderReportModel.StatusCodeID = workOrderLines.StatusCodeID;
                workOrderReportModel.ProfileNo = workOrderLines.ProfileNo;

               // workOrderReportModels.Add(workOrderReportModel);
            }
           
            return workOrderReportModel;
        }


        private List<ApplicationPermissionModel> FindMenuPath(List<ApplicationPermissionModel> permissionlist)
        {
            List<ApplicationPermissionModel> path = new List<ApplicationPermissionModel>();
            var emptypermissionIds = new List<long>();
            foreach (var fn in permissionlist)
            {
                ApplicationPermissionModel f = new ApplicationPermissionModel();

                if (fn.ParentID > 0)
                {
                    f.PathName = "";
                    f.PermissionID = fn.PermissionID;
                    f.PermissionName = fn.PermissionName;
                    f.ParentID = fn.ParentID;
                    f.PathNames.Add(fn.PermissionName);
                    GenerateLocation(f, fn, permissionlist, emptypermissionIds);
                    f.PathNames.Reverse();
                    foreach (var item in f.PathNames)
                    {
                        if (f.PathNames[f.PathNames.Count - 1] != item)
                        {
                            f.PathName += item + " / ";
                        }
                        else
                        {
                            f.PathName += item;
                        }
                    }

                    path.Add(f);
                    //FindMenuPath(permissionlist);
                }
                else if (fn.ParentID == null)
                {
                    //f.FolderPath = fn.Name;
                    f.PathName = fn.PermissionName;
                    f.PermissionID = fn.PermissionID;
                    f.PermissionName = fn.PermissionName;
                    f.ParentID = fn.ParentID;
                    path.Add(f);
                }





            }
            return path.Distinct().ToList();
        }

        private void GenerateLocation(ApplicationPermissionModel ApplicationPermissionModel, ApplicationPermissionModel parentPermission, List<ApplicationPermissionModel> ApplicationPermissionModels, List<long> emptyIds)
        {
            //if (!emptyIds.Contains(parentPermission.PermissionID))
            //{
            //    emptyIds.Add(parentPermission.PermissionID);
            parentPermission = ApplicationPermissionModels.FirstOrDefault(s => s.PermissionID == parentPermission.ParentID);

            if (parentPermission != null)
            {
                ApplicationPermissionModel.PathNames.Add(parentPermission.PermissionName);
                if (parentPermission.ParentID != null)
                {
                    GenerateLocation(ApplicationPermissionModel, parentPermission, ApplicationPermissionModels, emptyIds);
                }
            }
            //}

        }
    }

    public class WorkOrderType
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

}
