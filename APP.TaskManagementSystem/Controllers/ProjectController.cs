﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProjectController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProjectController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProjects")]
        public List<ProjectModel> Get()
        {
            List<ProjectModel> projectModels = new List<ProjectModel>();
            var projects = _context.Project.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ProjectId).AsNoTracking().ToList();
            if(projects!=null && projects.Count > 0)
            {
                projects.ForEach(s =>
                {
                        ProjectModel projectModel = new ProjectModel();
                    
                        projectModel.ProjectID = s.ProjectId;
                        projectModel.Name = s.Name;
                        projectModel.Description = s.Description;
                        projectModel.AddedByUserID = s.AddedByUserId;
                        projectModel.ModifiedByUserID = s.ModifiedByUserId;
                        projectModel.StatusCodeID = s.StatusCodeId;
                        projectModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                        projectModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                        projectModel.AddedDate = s.AddedDate;
                        projectModel.TeamID = s.TeamId;
                        projectModel.ModifiedDate = s.ModifiedDate;
                        projectModels.Add(projectModel);
                    
                   
                });
            }
           
            return projectModels;
        }
        [HttpGet]
        [Route("GetActiveProjects")]
        public List<ProjectModel> GetActiveProjects()
        {
            List<ProjectModel> projectModels = new List<ProjectModel>();
            var projects = _context.Project.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).OrderByDescending(a => a.ProjectId).AsNoTracking().ToList();
             if (projects != null && projects.Count > 0)
            {
                projects.ForEach(s =>
                {
                    ProjectModel projectModel = new ProjectModel();

                    projectModel.ProjectID = s.ProjectId;
                    projectModel.Name = s.Name;
                    projectModel.Description = s.Description;
                    projectModel.ModifiedByUserID = s.AddedByUserId;
                    projectModel.AddedByUserID = s.ModifiedByUserId;
                    projectModel.StatusCodeID = s.StatusCodeId;
                    projectModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    projectModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    projectModel.AddedDate = s.AddedDate;
                    projectModel.TeamID = s.TeamId;
                    projectModel.ModifiedDate = s.ModifiedDate;
                    
                    projectModels.Add(projectModel);
                });
            }
            return projectModels;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "GetProject")]
        [HttpGet("GetProjects/{id:int}")]
        public ActionResult<ProjectModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var project = _context.Project.SingleOrDefault(p => p.ProjectId == id.Value);
            var result = _mapper.Map<ProjectModel>(project);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet("GetTeamMember")]
        public ActionResult<List<long?>> GetTeamMember(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var teamId = _context.Project.SingleOrDefault(p => p.ProjectId == id);
            var project = _context.TeamMember.Where(w => w.TeamId == teamId.TeamId).AsNoTracking().Select(p => p.MemberId).ToList();

            if (project == null)
            {
                return NotFound();
            }
            return project;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProjectModel> GetData(SearchModel searchModel)
        {
            var project = new Project();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).FirstOrDefault();
                        break;
                    case "Last":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).LastOrDefault();
                        break;
                    case "Next":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).LastOrDefault();
                        break;
                    case "Previous":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).FirstOrDefault();
                        break;
                    case "Last":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).LastOrDefault();
                        break;
                    case "Next":
                        project = _context.Project.OrderBy(o => o.ProjectId).FirstOrDefault(s => s.ProjectId > searchModel.Id);
                        break;
                    case "Previous":
                        project = _context.Project.OrderByDescending(o => o.ProjectId).FirstOrDefault(s => s.ProjectId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProjectModel>(project);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProjects")]
        public ProjectModel Post(ProjectModel value)
        {
            var project = new Project
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                Name = value.Name,
                Description = value.Description,
                TeamId = value.TeamID,

            };
            _context.Project.Add(project);
            _context.SaveChanges();
            value.ProjectID = project.ProjectId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateProjects")]
        public ProjectModel Put(ProjectModel value)
        {
            var project = _context.Project.SingleOrDefault(p => p.ProjectId == value.ProjectID);
            project.ModifiedByUserId = value.ModifiedByUserID;
            project.ModifiedDate = DateTime.Now;
            project.Name = value.Name;
            project.Description = value.Description;
            project.StatusCodeId = value.StatusCodeID;
            project.TeamId = value.TeamID;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteProjects")]
        public void Delete(int id)
        {
            var project = _context.Project.SingleOrDefault(p => p.ProjectId == id);
            if (project != null)
            {
                _context.Project.Remove(project);
                _context.SaveChanges();
            }
        }

    }
}