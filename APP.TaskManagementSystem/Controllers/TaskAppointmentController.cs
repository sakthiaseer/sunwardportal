﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskAppointmentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskAppointmentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskAppointments")]
        public List<TaskAppointmentModel> Get()
        {
            var taskAppointment = _context.TaskAppointment.Include("AddedByUser").Include("ModifiedByUser").Select(s => new TaskAppointmentModel
            {
                TaskAppointmentID = s.TaskAppointmentId,
                TaskMasterID = s.TaskMasterId.Value,
                DiscussionDate = s.DiscussionDate.Value,
                DiscussionNotes = s.DiscussionNotes,
                AppointmentBy = s.AppointmentBy.Value,
                StatusCodeID = s.StatusCodeId.Value,
                ClosedDate = s.ClosedDate.Value,
                SubTaskID = s.SubTaskId,
                //MemberIds = s.TeamMember.Select(c => c.MemberId.Value).ToList(),

            }).OrderByDescending(o => o.TaskAppointmentID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<TaskAppointmentModel>>(teamMaster);
            return taskAppointment;
        }
        [HttpGet]
        [Route("GetTaskAppointmentsByID")]
        public ActionResult<List<TaskAppointmentModel>> GetTask(int id, int userId)
        {
            List<TaskAppointmentModel> taskAppointmentModels = new List<TaskAppointmentModel>();
            var taskAppoinments = _context.TaskAppointment.Where(t => t.TaskMasterId == id && t.AppointmentBy == userId).AsNoTracking().ToList();
            var applicationUserList = _context.Employee.AsNoTracking().ToList();
            taskAppoinments.ForEach(t =>
            {
                taskAppointmentModels.Add(_mapper.Map<TaskAppointmentModel>(t));
                if (t != null)
                {
                    var assignedTo = _context.TaskAssigned.Where(a => a.TaskId == t.SubTaskId).AsNoTracking().Select(s => s.UserId).ToList();
                    taskAppointmentModels.FirstOrDefault(m => m.TaskAppointmentID == t.TaskAppointmentId).AssignedTo = assignedTo;
                    taskAppointmentModels.FirstOrDefault(m => m.TaskAppointmentID == t.TaskAppointmentId).AssignedToNames = assignedTo != null ? string.Join(",", applicationUserList.Where(a => assignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                }
            });

            return taskAppointmentModels;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get TaskAppointment")]
        [HttpGet("GetTaskAppointments/{id:int}")]
        public ActionResult<TaskAppointmentModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskAppointment = _context.TaskAppointment.SingleOrDefault(p => p.TaskAppointmentId == id.Value);
            var result = _mapper.Map<TaskAppointmentModel>(taskAppointment);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskAppointmentModel> GetData(SearchModel searchModel)
        {
            var taskAppointment = new TaskAppointment();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).LastOrDefault();
                        break;
                    case "Next":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).LastOrDefault();
                        break;
                    case "Previous":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).LastOrDefault();
                        break;
                    case "Next":
                        taskAppointment = _context.TaskAppointment.OrderBy(o => o.TaskAppointmentId).FirstOrDefault(s => s.TaskAppointmentId > searchModel.Id);
                        break;
                    case "Previous":
                        taskAppointment = _context.TaskAppointment.OrderByDescending(o => o.TaskAppointmentId).FirstOrDefault(s => s.TaskAppointmentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskAppointmentModel>(taskAppointment);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskAppointment")]
        public TaskAppointmentModel Post(TaskAppointmentModel value)
        {
            try
            {
                var taskappointment = _context.TaskAppointment.Where(p => p.TaskAppointmentId == value.TaskAppointmentID && p.AppointmentBy == value.AppointmentBy).FirstOrDefault();
                if (taskappointment == null)
                {
                    if (value.TaskMasterID != 0)
                    {
                        var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskMasterID);
                        if (taskMaster != null)
                        {
                            taskMaster.DiscussionDate = value.DiscussionDate;
                            //_context.SaveChanges();              



                        }
                        var subtaskcreate = new TaskMaster();
                        subtaskcreate.ParentTaskId = value.TaskMasterID;
                        subtaskcreate.MainTaskId = taskMaster.MainTaskId;
                        subtaskcreate.Title = value.Title;
                        subtaskcreate.Description = value.DiscussionNotes;
                        subtaskcreate.DueDate = value.DueDate;
                        subtaskcreate.AssignedTo = value.AssignedTo[0];
                        subtaskcreate.StatusCodeId = 512;
                        subtaskcreate.AddedByUserId = value.AppointmentBy;
                        subtaskcreate.OwnerId = value.AppointmentBy;
                        subtaskcreate.AddedDate = DateTime.Now;
                        if (value.AssignedTo != null)
                        {
                            value.AssignedTo.ForEach(c =>
                            {
                                var assigned = new TaskAssigned
                                {
                                    UserId = c,
                                    StatusCodeId = 512,
                                    DueDate = value.DueDate,
                                    AssignedDate = DateTime.Now,
                                    TaskOwnerId = value.AppointmentBy,
                                    IsRead = false,
                                    StartDate = DateTime.Now,

                                };
                                subtaskcreate.TaskAssigned.Add(assigned);
                            });
                        }

                        if (value.TaskMasterID > 0 && taskMaster.MainTaskId != null)
                        {
                            var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.TaskMasterID);
                            var taskNumber = "1";
                            if (tasklevel == null)
                            {
                                tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.TaskMasterID);
                                taskNumber = tasklevel.TaskNumber + ".1";
                            }
                            else
                            {
                                var numberarray = tasklevel.TaskNumber.Split('.');
                                var number = numberarray[numberarray.Length - 1].ToString();
                                var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                                var taskNo = tasklevel.TaskNumber.Remove(leng);
                                number = (double.Parse(number) + 1).ToString();
                                taskNumber = taskNo + "." + number;
                            }
                            subtaskcreate.TaskLevel = tasklevel.TaskLevel + 1;
                            subtaskcreate.TaskNumber = taskNumber;
                        }

                        _context.TaskMaster.Add(subtaskcreate);
                        _context.SaveChanges();
                        value.SubTaskID = subtaskcreate.TaskId;

                    }
                    var taskAppointment = new TaskAppointment
                    {
                        // TaskAppointmentId=value.TaskAppointmentID,
                        TaskMasterId = value.TaskMasterID,
                        DiscussionDate = value.DiscussionDate,
                        DiscussionNotes = value.DiscussionNotes,
                        AppointmentBy = value.AppointmentBy,
                        ClosedDate = value.ClosedDate,
                        StatusCodeId = 512,
                        SubTaskId = value.SubTaskID,
                    };
                    _context.TaskAppointment.Add(taskAppointment);
                    _context.SaveChanges();
                    value.TaskAppointmentID = taskAppointment.TaskAppointmentId;
                }
                else
                {
                    if (value.TaskMasterID != 0)
                    {
                        var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskMasterID);
                        if (taskMaster != null)
                        {
                            taskMaster.DiscussionDate = value.DiscussionDate;
                            //_context.SaveChanges();   
                        }
                    }
                    var subtaskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == taskappointment.SubTaskId);
                    if (subtaskMaster != null)
                    {
                        subtaskMaster.Description = value.DiscussionNotes;
                        subtaskMaster.DueDate = value.DiscussionDate;
                        var taskAssinged = _context.TaskAssigned.Where(l => l.TaskId == taskappointment.SubTaskId).ToList();
                        if (taskAssinged.Count > 0)
                        {
                            _context.TaskAssigned.RemoveRange(taskAssinged);
                            _context.SaveChanges();
                        }

                        subtaskMaster.TaskAssigned = new List<TaskAssigned>();
                        if (value.AssignedTo != null)
                        {
                            value.AssignedTo.ForEach(c =>
                            {
                                var assigned = new TaskAssigned
                                {
                                    UserId = c,
                                    StatusCodeId = 512,
                                    DueDate = value.DueDate,
                                    AssignedDate = DateTime.Now,
                                    TaskOwnerId = value.AppointmentBy,
                                    IsRead = false,
                                    StartDate = DateTime.Now,

                                };
                                subtaskMaster.TaskAssigned.Add(assigned);
                            });
                        }
                    }
                    taskappointment.DiscussionNotes = value.DiscussionNotes;
                    taskappointment.DiscussionDate = value.DiscussionDate;
                    taskappointment.AppointmentBy = value.AppointmentBy;
                    taskappointment.ClosedDate = value.ClosedDate;
                    taskappointment.StatusCodeId = 512;
                    //taskappointment.SubTaskId = value.SubTaskID;
                    _context.SaveChanges();

                }

                return value;
            }
            catch (Exception ex)
            {
                throw new AppException("Discussion already created for this task!", ex);
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskAppointment")]
        public TaskAppointmentModel Put(TaskAppointmentModel value)
        {
            var taskAppointment = _context.TaskAppointment.SingleOrDefault(p => p.TaskAppointmentId == value.TaskAppointmentID);
            //taskAppointment.TaskMasterId = value.TaskMasterID;
            taskAppointment.DiscussionDate = value.DiscussionDate;
            taskAppointment.DiscussionNotes = value.DiscussionNotes;
            taskAppointment.AppointmentBy = value.AppointmentBy;
            taskAppointment.ClosedDate = value.ClosedDate;
            taskAppointment.StatusCodeId = value.StatusCodeID;
            //taskAppointment.SubTaskId = value.SubTaskID;

            var taskMaster = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskAppointment.TaskMasterId);
            if (taskMaster != null)
            {
                taskMaster.DiscussionDate = value.DiscussionDate;
            }
            var subtaskMaster = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskAppointment.TaskMasterId);
            if (subtaskMaster != null)
            {
                subtaskMaster.DiscussionDate = value.DiscussionDate;
                subtaskMaster.DueDate = value.DiscussionDate;
                subtaskMaster.Description = value.DiscussionNotes;
            }

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskAppointment")]
        public void Delete(int id)
        {
            var taskAppointment = _context.TaskAppointment.SingleOrDefault(p => p.TaskAppointmentId == id);
            if (taskAppointment != null)
            {
                _context.TaskAppointment.Remove(taskAppointment);

                var taskAssigned = _context.TaskAssigned.Where(t => t.TaskId == taskAppointment.SubTaskId).ToList();
                if (taskAssigned.Count > 0)
                    _context.TaskAssigned.RemoveRange(taskAssigned);

                var task = _context.TaskMaster.Where(t => t.TaskId == taskAppointment.SubTaskId).FirstOrDefault();
                if (task != null)
                    _context.TaskMaster.Remove(task);

                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetTaskAppointmentsByUser")]
        public List<TaskAppointmentModel> GetTaskAppointmentsByUser(int id)
        {
            var taskAssignedIds = _context.TaskAssigned.Where(t => t.UserId == id && t.StatusCodeId == 512).AsNoTracking().Select(t => t.TaskId);
            var taskAppointments = _context.TaskAppointment.Include(a => a.AppointmentByNavigation).Include(a => a.TaskMaster).Where(t => taskAssignedIds.Contains(t.SubTaskId)).AsNoTracking().ToList();
            List<TaskAppointmentModel> taskAppointmentModels = new List<TaskAppointmentModel>();

            taskAppointments.ForEach(t =>
            {
                TaskAppointmentModel taskAppointmentModel = new TaskAppointmentModel();
                taskAppointmentModel.TaskMasterID = t.TaskMasterId.Value;
                taskAppointmentModel.SubTaskID = t.SubTaskId;
                taskAppointmentModel.Title = t.DiscussionNotes;
                taskAppointmentModel.DiscussionDate = t.DiscussionDate.Value;
                taskAppointmentModel.DiscussionBy = t.AppointmentByNavigation.UserName;
                taskAppointmentModel.TaskTitle = t.TaskMaster.Title;
                taskAppointmentModels.Add(taskAppointmentModel);
            });

            return taskAppointmentModels;
        }

        [HttpPut]
        [Route("UpdateInviteTaskDiscussion")]
        public TaskMasterModel UpdateInviteTaskDiscussion(InviteUserModel value)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            if (value.TaskAppointmentId != null)
            {
                value.TaskId = _context.TaskAppointment.Where(s => s.TaskAppointmentId == value.TaskAppointmentId).FirstOrDefault()?.SubTaskId;
            }
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assignto"))
            {
                if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                {
                    value.InviteUsers.ForEach(i =>
                    {
                        var tasAssigned = new TaskAssigned
                        {
                            TaskId = value.TaskId,
                            DueDate = value.DueDate,
                            UserId = i,
                            IsRead = false,
                            AssignedDate = DateTime.Now,
                            StatusCodeId = 512,

                        };
                        _context.TaskAssigned.Add(tasAssigned);
                    });
                }
            }
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assigncc"))
            {
                if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                {
                    value.InviteUsers.ForEach(i =>
                    {
                        var taskMembers = new TaskMembers
                        {
                            TaskId = value.TaskId,
                            DueDate = value.DueDate,
                            AssignedCc = i,
                            IsRead = false,
                            StatusCodeId = 512,

                        };
                        _context.TaskMembers.Add(taskMembers);


                    });
                }
            }
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "onbehalf"))
            {
                var taskmaster = _context.TaskMaster.Where(s => s.TaskId == value.TaskId).FirstOrDefault();
                if (taskmaster != null)
                {
                    if (value.OnBehalfId != null)
                    {
                        taskmaster.OnBehalf = value.OnBehalfId;
                    }
                }
            }
           
            _context.SaveChanges();
            return taskMasterModel;
        }
    }
}