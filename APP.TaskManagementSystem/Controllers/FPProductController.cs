﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FPProductController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FPProductController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetFpproducts")]
        public List<FPProductModel> Get()
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var Fpproduct = _context.Fpproduct.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.FpproductGeneralInfo.FinishProduct).AsNoTracking().ToList();
            List<FPProductModel> FPProductModel = new List<FPProductModel>();
            Fpproduct.ForEach(s =>
            {
                FPProductModel FPProductModels = new FPProductModel
                {
                    FPProductID = s.FpproductId,
                    FPProductGeneralInfoID = s.FpproductGeneralInfoId,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    IsActiveSalesPack = s.IsActiveSalesPack,
                    ProductName = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FpproductGeneralInfo.FinishProduct.ProductId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FpproductGeneralInfo.FinishProduct.ProductId).Value : string.Empty,
                };
                FPProductModel.Add(FPProductModels);
            });
            return FPProductModel.OrderByDescending(o => o.FPProductID).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPProductModel> GetData(SearchModel searchModel)
        {
            var Fpproduct = new Fpproduct();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).FirstOrDefault();
                        break;
                    case "Last":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).LastOrDefault();
                        break;
                    case "Next":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).LastOrDefault();
                        break;
                    case "Previous":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).FirstOrDefault();
                        break;
                    case "Last":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).LastOrDefault();
                        break;
                    case "Next":
                        Fpproduct = _context.Fpproduct.OrderBy(o => o.FpproductId).FirstOrDefault(s => s.FpproductId > searchModel.Id);
                        break;
                    case "Previous":
                        Fpproduct = _context.Fpproduct.OrderByDescending(o => o.FpproductId).FirstOrDefault(s => s.FpproductId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPProductModel>(Fpproduct);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFpproduct")]
        public FPProductModel Post(FPProductModel value)
        {
            var Fpproduct = new Fpproduct
            {
                FpproductGeneralInfoId = value.FPProductGeneralInfoID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsActiveSalesPack = value.IsActiveSalesPack,
                StatusCodeId = value.StatusCodeID,

            };
            _context.Fpproduct.Add(Fpproduct);
            _context.SaveChanges();
            value.FPProductID = Fpproduct.FpproductId;
            if (value.FinishProductGeneralInforLineIDs != null && value.FinishProductGeneralInforLineIDs.Count > 0)
            {
                value.FinishProductGeneralInforLineIDs.ForEach(f =>
                {
                    var fpproductMultiple = new FpproductPacksizeMultiple
                    {
                        FinishProductGeneralInforLineId = f,
                        FpproductId = value.FPProductID,

                    };
                    _context.FpproductPacksizeMultiple.Add(fpproductMultiple);

                });
            }
            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFpproduct")]
        public FPProductModel Put(FPProductModel value)
        {
            var fproduct = _context.Fpproduct.SingleOrDefault(p => p.FpproductId == value.FPProductID);
            fproduct.FpproductGeneralInfoId = value.FPProductGeneralInfoID;
            fproduct.ModifiedByUserId = value.ModifiedByUserID;
            fproduct.ModifiedDate = DateTime.Now;
            fproduct.StatusCodeId = value.StatusCodeID;
            fproduct.IsActiveSalesPack = value.IsActiveSalesPack;
            if (value.FinishProductGeneralInforLineIDs != null && value.FinishProductGeneralInforLineIDs.Count > 0)
            {
                value.FinishProductGeneralInforLineIDs.ForEach(f =>
                {
                    var existing = _context.FpproductPacksizeMultiple.Where(m => m.FpproductId == value.FPProductID && m.FinishProductGeneralInforLineId == f).FirstOrDefault();
                    if (existing == null)
                    {
                        var fpproductMultiple = new FpproductPacksizeMultiple
                        {
                            FinishProductGeneralInforLineId = f,
                            FpproductId = value.FPProductID,

                        };
                        _context.FpproductPacksizeMultiple.Add(fpproductMultiple);
                    }

                });
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFpproduct")]
        public void Delete(int id)
        {
            var Fpproduct = _context.Fpproduct.SingleOrDefault(p => p.FpproductId == id);
            if (Fpproduct != null)
            {
                var fpProductLine = _context.FpproductLine.Where(f => f.FpproductId == id).ToList();
                if (fpProductLine != null && fpProductLine.Count > 0)
                {
                    _context.FpproductLine.RemoveRange(fpProductLine);
                    _context.SaveChanges();
                }
                _context.Fpproduct.Remove(Fpproduct);
                _context.SaveChanges();
            }
        }
    }
}