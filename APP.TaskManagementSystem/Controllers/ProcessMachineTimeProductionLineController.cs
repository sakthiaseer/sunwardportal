﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProcessMachineTimeProductionLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProcessMachineTimeProductionLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #region ProcessMachineTimeProductionLine
        [HttpPost]
        [Route("GetProcessMachineTimeProductionLine")]
        public List<ProcessMachineTimeProductionLineModel> GetProcessMachineTimeProductionLine(ProcessMachineTimeLineSearch processMachineTimeLineSearch)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(a => a.StatusCode).Include(a => a.ProductionCentre).Include(i => i.Item).Where(w => w.ProcessMachineTimeId == processMachineTimeLineSearch.ProcessMachineTimeId && w.ItemId == processMachineTimeLineSearch.ItemId).AsNoTracking().ToList();
            List<ProcessMachineTimeProductionLineModel> ProcessMachineTimeLineModels = new List<ProcessMachineTimeProductionLineModel>();
            if (processMachineTimeProductionLine != null)
            {
                List<Navitems> navitems = new List<Navitems>();
                navitems = _context.Navitems.AsNoTracking().ToList();
                processMachineTimeProductionLine.ForEach(s =>
                {
                    ProcessMachineTimeProductionLineModel ProcessMachineTimeModel = new ProcessMachineTimeProductionLineModel
                    {
                        ProcessMachineTimeLineId = s.ProcessMachineTimeLineId,
                        ProcessMachineTimeId = s.ProcessMachineTimeId,
                        ProductionCentreId = s.ProductionCentreId,
                        StatusCodeID = s.StatusCodeId,
                        ItemId = s.ItemId,
                        ItemNo = s.Item?.No,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProductionCentre = s.ProductionCentre?.Value,
                        IsFPItemStrip = navitems.Where(a => a.ItemId == s.ItemId && a.No.ToLower().Trim().Contains("fp") && a.BaseUnitofMeasure.ToLower().Trim().Contains("strip")).ToList().Count > 0 ? true : false,
                        IsFPItemBox = navitems.Where(a => a.ItemId == s.ItemId && a.No.ToLower().Trim().Contains("fp") && (a.BaseUnitofMeasure.ToLower().Trim().Contains("bottle") || (a.BaseUnitofMeasure.ToLower().Trim().Contains("box")))).ToList().Count > 0 ? true : false,

                    };
                    ProcessMachineTimeLineModels.Add(ProcessMachineTimeModel);
                });
            }
            return ProcessMachineTimeLineModels.OrderByDescending(a => a.ProcessMachineTimeLineId).ToList();
        }
        [HttpPost]
        [Route("InsertProcessMachineTimeProductionLine")]
        public ProcessMachineTimeProductionLineModel Post(ProcessMachineTimeProductionLineModel value)
        {
            var processMachineTimeProductionLine = new ProcessMachineTimeProductionLine
            {
                ProcessMachineTimeId = value.ProcessMachineTimeId,
                ProductionCentreId = value.ProductionCentreId,
                ItemId = value.ItemId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.ProcessMachineTimeProductionLine.Add(processMachineTimeProductionLine);
            _context.SaveChanges();
            List<Navitems> navitems = new List<Navitems>();
            if (value.ItemId > 0)
            {
                navitems = _context.Navitems.Where(t => t.ItemId == value.ItemId).ToList();
                value.IsFPItemStrip = navitems.Where(a => a.ItemId == value.ItemId && a.No.Contains("FP") && a.BaseUnitofMeasure.ToLower().Trim().Contains("strip")).ToList().Count > 0 ? true : false;
                value.IsFPItemBox = navitems.Where(a => a.ItemId == value.ItemId && a.No.Contains("FP") && (a.BaseUnitofMeasure.ToLower().Trim().Contains("bottle") || (a.BaseUnitofMeasure.ToLower().Trim().Contains("box")))).ToList().Count > 0 ? true : false;
            }
            value.ProcessMachineTimeLineId = processMachineTimeProductionLine.ProcessMachineTimeLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProcessMachineTimeProductionLine")]
        public ProcessMachineTimeProductionLineModel Put(ProcessMachineTimeProductionLineModel value)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLine.SingleOrDefault(p => p.ProcessMachineTimeLineId == value.ProcessMachineTimeLineId);
            processMachineTimeProductionLine.ProcessMachineTimeId = value.ProcessMachineTimeId;
            processMachineTimeProductionLine.ProductionCentreId = value.ProductionCentreId;
            processMachineTimeProductionLine.ItemId = value.ItemId;
            processMachineTimeProductionLine.ModifiedByUserId = value.ModifiedByUserID;
            processMachineTimeProductionLine.ModifiedDate = DateTime.Now;
            processMachineTimeProductionLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteProcessMachineTimeProductionLine")]
        public void Delete(int id)
        {
            try
            {
                var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLine.SingleOrDefault(p => p.ProcessMachineTimeLineId == id);
                if (processMachineTimeProductionLine != null)
                {
                    var machineTimeManHourInfo = _context.MachineTimeManHourInfo.Where(p => p.ProcessMachineTimeLineId == id).ToList();
                    if (machineTimeManHourInfo != null)
                    {
                        _context.MachineTimeManHourInfo.RemoveRange(machineTimeManHourInfo);
                        _context.SaveChanges();
                    }
                    var processMachineTimeProductionLineLine = _context.ProcessMachineTimeProductionLineLine.Where(p => p.ProcessMachineTimeProductionLineId == id).ToList();
                    if (processMachineTimeProductionLineLine != null)
                    {
                        processMachineTimeProductionLineLine.ForEach(s =>
                        {
                            var processMachineTimeProductionMachineProcess = _context.ProcessMachineTimeProductionMachineProcess.Where(p => s.ProcessMachineTimeProductionLineLineId == id).ToList();
                            if (processMachineTimeProductionMachineProcess != null)
                            {
                                _context.ProcessMachineTimeProductionMachineProcess.RemoveRange(processMachineTimeProductionMachineProcess);
                                _context.SaveChanges();
                            }
                        });
                        _context.ProcessMachineTimeProductionLineLine.RemoveRange(processMachineTimeProductionLineLine);
                        _context.SaveChanges();
                    }
                    _context.ProcessMachineTimeProductionLine.Remove(processMachineTimeProductionLine);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("ProcessMachineTimeProductionLine Cannot be Delete! Reference to Others", ex);
            }
        }
        #endregion
        #region ProcessMachineTimeProductionLineLine
        [HttpGet]
        [Route("GetProcessMachineTimeProductionLineLine")]
        public List<ProcessMachineTimeProductionLineLineModel> GetProcessMachineTimeProductionLineLine(long? id)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLineLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(a => a.StatusCode).Include(a => a.ProductionCentreSteps).Where(w => w.ProcessMachineTimeProductionLineId == id).AsNoTracking().ToList();
            List<ProcessMachineTimeProductionLineLineModel> ProcessMachineTimeLineModels = new List<ProcessMachineTimeProductionLineLineModel>();
            if (processMachineTimeProductionLine != null)
            {
                processMachineTimeProductionLine.ForEach(s =>
                {
                    ProcessMachineTimeProductionLineLineModel ProcessMachineTimeModel = new ProcessMachineTimeProductionLineLineModel
                    {
                        ProcessMachineTimeProductionLineLineId = s.ProcessMachineTimeProductionLineLineId,
                        ProcessMachineTimeProductionLineId = s.ProcessMachineTimeProductionLineId,
                        OperationStepNo = s.OperationStepNo,
                        Description = s.Description,
                        Preference = s.Preference,
                        ProductionCentreStepsId = s.ProductionCentreStepsId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProductionCentreSteps = s.ProductionCentreSteps?.Value,
                    };
                    ProcessMachineTimeLineModels.Add(ProcessMachineTimeModel);
                });
            }
            return ProcessMachineTimeLineModels.OrderByDescending(a => a.ProcessMachineTimeProductionLineLineId).ToList();
        }
        [HttpPost]
        [Route("InsertProcessMachineTimeProductionLineLine")]
        public ProcessMachineTimeProductionLineLineModel InsertProcessMachineTimeProductionLineLine(ProcessMachineTimeProductionLineLineModel value)
        {
            int? count = _context.ProcessMachineTimeProductionLineLine.Where(p => p.ProcessMachineTimeProductionLineId == value.ProcessMachineTimeProductionLineId && p.ProductionCentreStepsId == value.ProductionCentreStepsId).ToList().Count();
            string opreationNo = Convert.ToString(count + 1);
            var processMachineTimeProductionLine = new ProcessMachineTimeProductionLineLine
            {
                ProcessMachineTimeProductionLineId = value.ProcessMachineTimeProductionLineId,
                OperationStepNo = opreationNo,
                Description = value.Description,
                Preference = value.Preference,
                ProductionCentreStepsId = value.ProductionCentreStepsId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.ProcessMachineTimeProductionLineLine.Add(processMachineTimeProductionLine);
            _context.SaveChanges();
            value.ProcessMachineTimeProductionLineLineId = processMachineTimeProductionLine.ProcessMachineTimeProductionLineLineId;
            value.OperationStepNo = processMachineTimeProductionLine.OperationStepNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateProcessMachineTimeProductionLineLine")]
        public ProcessMachineTimeProductionLineLineModel UpdateProcessMachineTimeProductionLineLine(ProcessMachineTimeProductionLineLineModel value)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLineLine.SingleOrDefault(p => p.ProcessMachineTimeProductionLineLineId == value.ProcessMachineTimeProductionLineLineId);
            processMachineTimeProductionLine.ProcessMachineTimeProductionLineId = value.ProcessMachineTimeProductionLineId;
            processMachineTimeProductionLine.OperationStepNo = value.OperationStepNo;
            processMachineTimeProductionLine.Description = value.Description;
            processMachineTimeProductionLine.ProductionCentreStepsId = value.ProductionCentreStepsId;
            processMachineTimeProductionLine.Preference = value.Preference;
            processMachineTimeProductionLine.ModifiedByUserId = value.ModifiedByUserID;
            processMachineTimeProductionLine.ModifiedDate = DateTime.Now;
            processMachineTimeProductionLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProcessMachineTimeProductionLineLine")]
        public void DeleteProcessMachineTimeProductionLineLine(int id)
        {
            try
            {
                var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionLineLine.SingleOrDefault(p => p.ProcessMachineTimeProductionLineLineId == id);
                if (processMachineTimeProductionLine != null)
                {
                    var processMachineTimeProductionMachineProcess = _context.ProcessMachineTimeProductionMachineProcess.Where(p => p.ProcessMachineTimeProductionLineLineId == id).ToList();
                    if (processMachineTimeProductionMachineProcess != null)
                    {
                        _context.ProcessMachineTimeProductionMachineProcess.RemoveRange(processMachineTimeProductionMachineProcess);
                        _context.SaveChanges();
                    }
                    _context.ProcessMachineTimeProductionLineLine.Remove(processMachineTimeProductionLine);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("ProcessMachineTimeProductionLineLine Cannot be Delete! Reference to Others", ex);
            }
        }
        #endregion
        #region ProcessMachineTimeProductionMachineProcess
        [HttpGet]
        [Route("GetProcessMachineTimeProductionMachineProcess")]
        public List<ProcessMachineTimeProductionMachineProcessModel> GetProcessMachineTimeProductionMachineProcess(long? id)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionMachineProcess.Include(a => a.AddedByUser).Include(s => s.MachineName).Include(m => m.ModifiedByUser).Include(a => a.StatusCode).Include(a => a.ManufacturingProcess).Include(a => a.ManufacturingSteps).Where(w => w.ProcessMachineTimeProductionLineLineId == id).AsNoTracking().ToList();
            List<ProcessMachineTimeProductionMachineProcessModel> ProcessMachineTimeLineModels = new List<ProcessMachineTimeProductionMachineProcessModel>();
            if (processMachineTimeProductionLine != null)
            {
                processMachineTimeProductionLine.ForEach(s =>
                {
                    ProcessMachineTimeProductionMachineProcessModel ProcessMachineTimeModel = new ProcessMachineTimeProductionMachineProcessModel
                    {
                        ProcessMachineTimeProductionMachineProcessId = s.ProcessMachineTimeProductionMachineProcessId,
                        ProcessMachineTimeProductionLineLineId = s.ProcessMachineTimeProductionLineLineId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingStepsId = s.ManufacturingStepsId,
                        UseAnyMachine = s.UseAnyMachine,
                        UseAnyMachineFlag = s.UseAnyMachine == true ? "Yes" : "No",
                        MachineNameId = s.MachineNameId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ManufacturingSteps = s.ManufacturingSteps?.Value,
                        ManufacturingProcess = s.ManufacturingProcess?.Value,
                        MachineName = s.MachineName?.NameOfTheMachine,
                        ClassificationCompanyId = s.ClassificationCompanyId,
                    };
                    ProcessMachineTimeLineModels.Add(ProcessMachineTimeModel);
                });
            }
            return ProcessMachineTimeLineModels.OrderByDescending(a => a.ProcessMachineTimeProductionLineLineId).ToList();
        }
        [HttpPost]
        [Route("InsertProcessMachineTimeProductionMachineProcess")]
        public ProcessMachineTimeProductionMachineProcessModel InsertProcessMachineTimeProductionMachineProcess(ProcessMachineTimeProductionMachineProcessModel value)
        {
            var processMachineTimeProductionLine = new ProcessMachineTimeProductionMachineProcess
            {
                ProcessMachineTimeProductionLineLineId = value.ProcessMachineTimeProductionLineLineId,
                ManufacturingProcessId = value.ManufacturingProcessId,
                ManufacturingStepsId = value.ManufacturingStepsId,
                UseAnyMachine = value.UseAnyMachine,
                MachineNameId = value.MachineNameId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.ProcessMachineTimeProductionMachineProcess.Add(processMachineTimeProductionLine);
            _context.SaveChanges();
            value.ProcessMachineTimeProductionMachineProcessId = processMachineTimeProductionLine.ProcessMachineTimeProductionMachineProcessId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProcessMachineTimeProductionMachineProcess")]
        public ProcessMachineTimeProductionMachineProcessModel UpdateProcessMachineTimeProductionMachineProcess(ProcessMachineTimeProductionMachineProcessModel value)
        {
            var processMachineTimeProductionLine = _context.ProcessMachineTimeProductionMachineProcess.SingleOrDefault(p => p.ProcessMachineTimeProductionMachineProcessId == value.ProcessMachineTimeProductionMachineProcessId);
            processMachineTimeProductionLine.ProcessMachineTimeProductionLineLineId = value.ProcessMachineTimeProductionLineLineId;
            processMachineTimeProductionLine.ManufacturingProcessId = value.ManufacturingProcessId;
            processMachineTimeProductionLine.ManufacturingStepsId = value.ManufacturingStepsId;
            processMachineTimeProductionLine.UseAnyMachine = value.UseAnyMachine;
            processMachineTimeProductionLine.MachineNameId = value.MachineNameId;
            processMachineTimeProductionLine.ModifiedByUserId = value.ModifiedByUserID;
            processMachineTimeProductionLine.ModifiedDate = DateTime.Now;
            processMachineTimeProductionLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProcessMachineTimeProductionMachineProcess")]
        public void DeleteProcessMachineTimeProductionMachineProcess(int id)
        {
            try
            {
                var processMachineTimeProductionMachineProcess = _context.ProcessMachineTimeProductionMachineProcess.SingleOrDefault(p => p.ProcessMachineTimeProductionMachineProcessId == id);
                if (processMachineTimeProductionMachineProcess != null)
                {
                    _context.ProcessMachineTimeProductionMachineProcess.Remove(processMachineTimeProductionMachineProcess);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("ProcessMachineTimeProductionMachineProcess Cannot be Delete! Reference to Others", ex);
            }
        }
        #endregion
        #region MachineTimeManHourInfo
        [HttpGet]
        [Route("GetMachineTimeManHourInfo")]
        public List<MachineTimeManHourInfoModel> GetMachineTimeManHourInfo(long? id)
        {
            var processMachineTimeProductionLine = _context.MachineTimeManHourInfo.Where(w => w.ProcessMachineTimeLineId == id).AsNoTracking().ToList();
            List<MachineTimeManHourInfoModel> ProcessMachineTimeLineModels = new List<MachineTimeManHourInfoModel>();
            if (processMachineTimeProductionLine != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
                var codeMaster = _context.CodeMaster.Select(s => new { s.CodeId, s.CodeValue }).AsNoTracking().ToList();
                processMachineTimeProductionLine.ForEach(s =>
                {
                    MachineTimeManHourInfoModel ProcessMachineTimeModel = new MachineTimeManHourInfoModel
                    {
                        MachineTimeManHourInfoId = s.MachineTimeManHourInfoId,
                        ProcessMachineTimeLineId = s.ProcessMachineTimeLineId,
                        Manpower = s.Manpower,
                        PreparationTimeId = s.PreparationTimeId,
                        PreparationTimeType = codeMaster != null && s.PreparationTimeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.PreparationTimeId)?.CodeValue : "",
                        PreparationTime = s.PreparationTime,
                        ProductionTimeId = s.ProductionTimeId,
                        ProductionTimeType = codeMaster != null && s.ProductionTimeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.ProductionTimeId)?.CodeValue : "",
                        ProductionTime = s.ProductionTime,
                        Level1CleaningId = s.Level1CleaningId,
                        Level1CleaningType = codeMaster != null && s.Level1CleaningId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.Level1CleaningId)?.CodeValue : "",
                        Level1Cleaning = s.Level1Cleaning,
                        Level2CleaningId = s.Level2CleaningId,
                        Level2CleaningType = codeMaster != null && s.Level2CleaningId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.Level2CleaningId)?.CodeValue : "",
                        Level2Cleaning = s.Level2Cleaning,
                        IntermittentCleaningAtId = s.IntermittentCleaningAtId,
                        IntermittentCleaningAtType = codeMaster != null && s.IntermittentCleaningAtId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.IntermittentCleaningAtId)?.CodeValue : "",
                        IntermittentCleaningAt = s.IntermittentCleaningAt,
                        NextStepMinTimeGapId = s.NextStepMinTimeGapId,
                        NextStepMinTimeGapType = codeMaster != null && s.NextStepMinTimeGapId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.NextStepMinTimeGapId)?.CodeValue : "",
                        NextStepMinTimeGap = s.NextStepMinTimeGap,
                        NextStepMaxTimeGapId = s.NextStepMaxTimeGapId,
                        NextStepMaxTimeGapType = codeMaster != null && s.NextStepMaxTimeGapId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.NextStepMaxTimeGapId)?.CodeValue : "",
                        NextStepMaxTimeGap = s.NextStepMaxTimeGap,
                        NextStepManpowerOverlapping = s.NextStepManpowerOverlapping,
                        NextStepManpowerOverlappingFlag = s.NextStepManpowerOverlapping == true ? "Yes" : "No",
                        OverlapNumber = s.OverlapNumber,
                        NextProdItemMinTimeId = s.NextProdItemMinTimeId,
                        NextProdItemMinTimeType = codeMaster != null && s.NextProdItemMinTimeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.NextProdItemMinTimeId)?.CodeValue : "",
                        NextProdItemMinTime = s.NextProdItemMinTime,
                        NextProdItemMaxTimeId = s.NextProdItemMaxTimeId,
                        NextProdItemMaxTimeType = codeMaster != null && s.NextProdItemMaxTimeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.NextProdItemMinTimeId)?.CodeValue : "",
                        NextProdItemMaxTime = s.NextProdItemMaxTime,
                        QctestingTime = s.QctestingTime,
                        QcmaxCollection = s.QcmaxCollection,
                        MaxCampaign = s.MaxCampaign,
                        RecommendedCycle = s.RecommendedCycle,
                        NoOfCutUse = s.NoOfCutUse,
                        TypeOfFeedId = s.TypeOfFeedId,
                        TypeOfFeed = codeMaster != null && s.TypeOfFeedId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.TypeOfFeedId)?.CodeValue : "",

                        StatusCodeID = s.StatusCodeId,
                        StatusCode = codeMaster != null && s.StatusCodeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.StatusCodeId)?.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = appUsers != null && s.AddedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "",
                        ModifiedByUser = appUsers != null && s.ModifiedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    ProcessMachineTimeLineModels.Add(ProcessMachineTimeModel);
                });
            }
            return ProcessMachineTimeLineModels.OrderByDescending(a => a.MachineTimeManHourInfoId).ToList();
        }
        [HttpPost]
        [Route("InsertMachineTimeManHourInfo")]
        public MachineTimeManHourInfoModel InsertMachineTimeManHourInfo(MachineTimeManHourInfoModel value)
        {
            var processMachineTimeProductionLine = new MachineTimeManHourInfo
            {
                ProcessMachineTimeLineId = value.ProcessMachineTimeLineId,
                Manpower = value.Manpower,
                PreparationTimeId = value.PreparationTimeId,
                PreparationTime = value.PreparationTime,
                ProductionTimeId = value.ProductionTimeId,
                ProductionTime = value.ProductionTime,
                Level1CleaningId = value.Level1CleaningId,
                Level1Cleaning = value.Level1Cleaning,
                Level2CleaningId = value.Level2CleaningId,
                Level2Cleaning = value.Level2Cleaning,
                IntermittentCleaningAtId = value.IntermittentCleaningAtId,
                IntermittentCleaningAt = value.IntermittentCleaningAt,
                NextStepMinTimeGapId = value.NextStepMinTimeGapId,
                NextStepMinTimeGap = value.NextStepMinTimeGap,
                NextStepMaxTimeGapId = value.NextStepMaxTimeGapId,
                NextStepMaxTimeGap = value.NextStepMaxTimeGap,
                NextStepManpowerOverlapping = value.NextStepManpowerOverlapping,
                OverlapNumber = value.OverlapNumber,
                NextProdItemMinTimeId = value.NextProdItemMinTimeId,
                NextProdItemMinTime = value.NextProdItemMinTime,
                NextProdItemMaxTimeId = value.NextProdItemMaxTimeId,
                NextProdItemMaxTime = value.NextProdItemMaxTime,
                QctestingTime = value.QctestingTime,
                QcmaxCollection = value.QcmaxCollection,
                MaxCampaign = value.MaxCampaign,
                RecommendedCycle = value.RecommendedCycle,
                NoOfCutUse = value.NoOfCutUse,
                TypeOfFeedId = value.TypeOfFeedId,

                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.MachineTimeManHourInfo.Add(processMachineTimeProductionLine);
            _context.SaveChanges();
            value.MachineTimeManHourInfoId = processMachineTimeProductionLine.MachineTimeManHourInfoId;
            return value;
        }
        [HttpPut]
        [Route("UpdateMachineTimeManHourInfo")]
        public MachineTimeManHourInfoModel UpdateMachineTimeManHourInfo(MachineTimeManHourInfoModel value)
        {
            var machineTimeManHourInfo = _context.MachineTimeManHourInfo.SingleOrDefault(f => f.MachineTimeManHourInfoId == value.MachineTimeManHourInfoId);
            machineTimeManHourInfo.ProcessMachineTimeLineId = value.ProcessMachineTimeLineId;
            machineTimeManHourInfo.Manpower = value.Manpower;
            machineTimeManHourInfo.PreparationTimeId = value.PreparationTimeId;
            machineTimeManHourInfo.PreparationTime = value.PreparationTime;
            machineTimeManHourInfo.ProductionTimeId = value.ProductionTimeId;
            machineTimeManHourInfo.ProductionTime = value.ProductionTime;
            machineTimeManHourInfo.Level1CleaningId = value.Level1CleaningId;
            machineTimeManHourInfo.Level1Cleaning = value.Level1Cleaning;
            machineTimeManHourInfo.Level2CleaningId = value.Level2CleaningId;
            machineTimeManHourInfo.Level2Cleaning = value.Level2Cleaning;
            machineTimeManHourInfo.IntermittentCleaningAtId = value.IntermittentCleaningAtId;
            machineTimeManHourInfo.IntermittentCleaningAt = value.IntermittentCleaningAt;
            machineTimeManHourInfo.NextStepMinTimeGapId = value.NextStepMinTimeGapId;
            machineTimeManHourInfo.NextStepMinTimeGap = value.NextStepMinTimeGap;
            machineTimeManHourInfo.NextStepMaxTimeGapId = value.NextStepMaxTimeGapId;
            machineTimeManHourInfo.NextStepMaxTimeGap = value.NextStepMaxTimeGap;
            machineTimeManHourInfo.NextStepManpowerOverlapping = value.NextStepManpowerOverlapping;
            machineTimeManHourInfo.OverlapNumber = value.OverlapNumber;
            machineTimeManHourInfo.NextProdItemMinTimeId = value.NextProdItemMinTimeId;
            machineTimeManHourInfo.NextProdItemMinTime = value.NextProdItemMinTime;
            machineTimeManHourInfo.NextProdItemMaxTimeId = value.NextProdItemMaxTimeId;
            machineTimeManHourInfo.NextProdItemMaxTime = value.NextProdItemMaxTime;
            machineTimeManHourInfo.QctestingTime = value.QctestingTime;
            machineTimeManHourInfo.QcmaxCollection = value.QcmaxCollection;
            machineTimeManHourInfo.MaxCampaign = value.MaxCampaign;
            machineTimeManHourInfo.RecommendedCycle = value.RecommendedCycle;
            machineTimeManHourInfo.NoOfCutUse = value.NoOfCutUse;
            machineTimeManHourInfo.TypeOfFeedId = value.TypeOfFeedId;

            machineTimeManHourInfo.ModifiedByUserId = value.ModifiedByUserID.Value;
            machineTimeManHourInfo.ModifiedDate = DateTime.Now;
            machineTimeManHourInfo.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteMachineTimeManHourInfo")]
        public void DeleteMachineTimeManHourInfo(int id)
        {
            try
            {
                var machineTimeManHourInfo = _context.MachineTimeManHourInfo.SingleOrDefault(p => p.MachineTimeManHourInfoId == id);
                if (machineTimeManHourInfo != null)
                {
                    _context.MachineTimeManHourInfo.Remove(machineTimeManHourInfo);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("MachineTimeManHourInfo Cannot be Delete! Reference to Others", ex);
            }
        }
        #endregion

        #region SetupNotes
        [HttpGet]
        [Route("GetProcessManPowerSpecialNotes")]
        public List<ProcessManPowerSpecialNotesModel> GetProcessManPowerSpecialNotes(long? id)
        {
            List<ProcessManPowerSpecialNotesModel> processManPowerSpecialNotesModelList = new List<ProcessManPowerSpecialNotesModel>();
            var specialNotes = _context.ProcessManPowerSpecialNotes.Include(d=>d.Department).Where(s => s.ProcessMachineTimeLineId == id).AsNoTracking().ToList();
            if(specialNotes!=null && specialNotes.Count>0)
            {
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
                var codeMaster = _context.CodeMaster.Select(s => new { s.CodeId, s.CodeValue }).AsNoTracking().ToList();
                specialNotes.ForEach(s =>
                {
                    ProcessManPowerSpecialNotesModel processManPowerSpecialNotesModel = new ProcessManPowerSpecialNotesModel
                    {
                        ProcessManPowerSpecialNotesId = s.ProcessManPowerSpecialNotesId,
                        ProcessMachineTimeLineId = s.ProcessMachineTimeLineId,
                        DepartmentId = s.DepartmentId,
                        Information = s.Information,
                        Department = s.Department?.Name,                       
                        AddedDate = s.AddedDate,
                        ModifiedByUserID = s.ModifiedByUserId,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = codeMaster != null && s.StatusCodeId != null ? codeMaster.FirstOrDefault(a => a.CodeId == s.StatusCodeId)?.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = appUsers != null && s.AddedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "",
                        ModifiedByUser = appUsers != null && s.ModifiedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "",
                        
                    };
                    processManPowerSpecialNotesModelList.Add(processManPowerSpecialNotesModel);
                });
            }



            return processManPowerSpecialNotesModelList.OrderByDescending(a => a.ProcessManPowerSpecialNotesId).ToList();
        }

        [HttpGet]
        [Route("GetSpecialNotesItem")]
        public ProcessManPowerSpecialNotesModel GetSpecialNotesItem(long? id)
        {
            ProcessManPowerSpecialNotesModel processManPowerSpecialNotesModel = new ProcessManPowerSpecialNotesModel();
            List<ProcessManPowerSpecialNotesModel> processManPowerSpecialNotesModelList = new List<ProcessManPowerSpecialNotesModel>();
            var specialNotes = _context.ProcessManPowerSpecialNotes.Where(s => s.ProcessManPowerSpecialNotesId == id).AsNoTracking().FirstOrDefault();
            if (specialNotes != null )
            {
               
               
                   processManPowerSpecialNotesModel = new ProcessManPowerSpecialNotesModel
                    {
                        ProcessManPowerSpecialNotesId = specialNotes.ProcessManPowerSpecialNotesId,
                        ProcessMachineTimeLineId = specialNotes.ProcessMachineTimeLineId,
                        DepartmentId = specialNotes.DepartmentId,
                        Information = specialNotes.Information,
                       
                        AddedDate = specialNotes.AddedDate,
                        ModifiedByUserID = specialNotes.ModifiedByUserId,
                        ModifiedDate = specialNotes.ModifiedDate,
                      
                        AddedByUserID = specialNotes.AddedByUserId,
                        StatusCodeID = specialNotes.StatusCodeId,
                      

                    };
                   
               
            }



            return processManPowerSpecialNotesModel;
        }
        [HttpPost]
        [Route("InsertProcessManPowerSpecialNotes")]
        public ProcessManPowerSpecialNotesModel InsertProcessManPowerSpecialNotes(ProcessManPowerSpecialNotesModel value)
        {
            var processManPowerSpecialNotes = new ProcessManPowerSpecialNotes
            {
                ProcessMachineTimeLineId = value.ProcessMachineTimeLineId,
                DepartmentId = value.DepartmentId,
                Information = value.Information,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.ProcessManPowerSpecialNotes.Add(processManPowerSpecialNotes);
            _context.SaveChanges();
            List<Navitems> navitems = new List<Navitems>();

            value.ProcessMachineTimeLineId = processManPowerSpecialNotes.ProcessMachineTimeLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProcessManPowerSpecialNotes")]
        public ProcessManPowerSpecialNotesModel UpdateProcessManPowerSpecialNotes(ProcessManPowerSpecialNotesModel value)
        {
            var processManPowerSpecialNotes = _context.ProcessManPowerSpecialNotes.SingleOrDefault(p => p.ProcessManPowerSpecialNotesId == value.ProcessManPowerSpecialNotesId);
            processManPowerSpecialNotes.ProcessMachineTimeLineId = value.ProcessMachineTimeLineId;
            processManPowerSpecialNotes.DepartmentId = value.DepartmentId;
            processManPowerSpecialNotes.Information = value.Information;
            processManPowerSpecialNotes.ModifiedByUserId = value.ModifiedByUserID;
            processManPowerSpecialNotes.ModifiedDate = DateTime.Now;
            processManPowerSpecialNotes.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteProcessManPowerSpecialNotes")]
        public void DeleteProcessManPowerSpecialNotes(int id)
        {
            try
            {
                var processManPowerSpecialNotes = _context.ProcessManPowerSpecialNotes.SingleOrDefault(p => p.ProcessManPowerSpecialNotesId == id);
                if (processManPowerSpecialNotes != null)
                {

                    _context.ProcessManPowerSpecialNotes.Remove(processManPowerSpecialNotes);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("ProcessManPowerSpecialNotes Cannot be Delete! Reference to Others", ex);
            }
        }

        #endregion
    }
}
