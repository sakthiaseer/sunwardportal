﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using APP.TaskManagementSystem.Helper;
using APP.EntityModel.Param;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FpCommonFieldController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateProfileClass;

        public FpCommonFieldController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateProfileClass = generate;

        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFPCommonFieldsAll")]
        public List<FPCommonFieldModel> GetFPCommonFieldsAll()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpcommonField = _context.FpcommonField.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(f => f.FpnumberNavigation).AsNoTracking().ToList();
            List<FPCommonFieldModel> fPCommonFieldModel = new List<FPCommonFieldModel>();
            fpcommonField.ForEach(s =>
            {
                FPCommonFieldModel fPCommonFieldModels = new FPCommonFieldModel
                {
                    FPCommonFieldID = s.FpcommonFieldId,
                    FPNumberID = s.FpnumberId,
                    Profile = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    ProfileName = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    FinishProductID = s.FinishProductId,
                    FPNumber = s.Fpnumber,
                    ProfileMaster = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProductId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageForm = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageFormID = s.DosageFormId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    ProfileReferenceNo = s.Fpnumber,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                };
                fPCommonFieldModel.Add(fPCommonFieldModels);
            });
            if (fPCommonFieldModel != null && fPCommonFieldModel.Count > 0)
            {
                fPCommonFieldModel.ForEach(f =>
                {
                    f.DropDownName = f.Profile + " | " + f.ProfileMaster;

                });
            }
            return fPCommonFieldModel.OrderByDescending(a => a.FPCommonFieldID).ToList();
        }
        [HttpGet]
        [Route("GetFPCommonFields")]
        public List<FPCommonFieldModel> Get(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpcommonField = _context.FpcommonField.Include(a => a.AddedByUser).Include(s=>s.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.FpnumberNavigation).AsNoTracking().ToList();
            List<FPCommonFieldModel> fPCommonFieldModel = new List<FPCommonFieldModel>();
            fpcommonField.ForEach(s =>
            {
                FPCommonFieldModel fPCommonFieldModels = new FPCommonFieldModel
                {
                    FPCommonFieldID = s.FpcommonFieldId,
                    FPNumberID = s.FpnumberId,
                    Profile = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    ProfileName = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    FinishProductID = s.FinishProductId,
                    FPNumber = s.Fpnumber,
                    ProfileMaster = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProductId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageForm = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageFormID = s.DosageFormId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    ProfileReferenceNo = s.Fpnumber,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                };
                fPCommonFieldModel.Add(fPCommonFieldModels);
            });
            if (fPCommonFieldModel != null && fPCommonFieldModel.Count > 0)
            {
                fPCommonFieldModel.ForEach(f =>
                {
                    f.DropDownName = f.Profile + " | " + f.ProfileMaster;

                });
            }
            return fPCommonFieldModel.Where(l => l.ItemClassificationMasterId == id).OrderByDescending(a => a.FPCommonFieldID).ToList();
        }
        [HttpPost]
        [Route("GetFPCommonFieldsByRefNo")]
        public List<FPCommonFieldModel> GetFPCommonFieldsByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpcommonField = _context.FpcommonField.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(f => f.FpnumberNavigation).AsNoTracking().ToList();
            List<FPCommonFieldModel> fPCommonFieldModel = new List<FPCommonFieldModel>();
            fpcommonField.ForEach(s =>
            {
                FPCommonFieldModel fPCommonFieldModels = new FPCommonFieldModel
                {
                    FPCommonFieldID = s.FpcommonFieldId,
                    FPNumberID = s.FpnumberId,
                    Profile = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    ProfileName = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    FinishProductID = s.FinishProductId,
                    FPNumber = s.Fpnumber,
                    ProfileMaster = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProductId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageForm = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageFormID = s.DosageFormId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    ProfileReferenceNo = s.Fpnumber,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo
                };
                fPCommonFieldModel.Add(fPCommonFieldModels);
            });
            if (fPCommonFieldModel != null && fPCommonFieldModel.Count > 0)
            {
                fPCommonFieldModel.ForEach(f =>
                {
                    f.DropDownName = f.Profile + " | " + f.ProfileMaster;

                });
            }
            if (refSearchModel.IsHeader)
            {
                return fPCommonFieldModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPCommonFieldID).ToList();
            }
            return fPCommonFieldModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPCommonFieldID).ToList();
        }
        [HttpGet("GetFpCommonFieldByID")]
        public ActionResult<FPCommonFieldModel> GetFpCommonField(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var FpcommonField = GetFpCommonFieldByID(id);
            var result = _mapper.Map<FPCommonFieldModel>(FpcommonField);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        private FPCommonFieldModel GetFpCommonFieldByID(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpcommonField = _context.FpcommonField.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(f => f.FpnumberNavigation).AsNoTracking().ToList();
            List<FPCommonFieldModel> fPCommonFieldModel = new List<FPCommonFieldModel>();
            fpcommonField.ForEach(s =>
            {
                FPCommonFieldModel fPCommonFieldModels = new FPCommonFieldModel
                {
                    FPCommonFieldID = s.FpcommonFieldId,
                    FPNumberID = s.FpnumberId,
                    Profile = s.FpnumberNavigation != null ? s.FpnumberNavigation.Name : "",
                    FinishProductID = s.FinishProductId,
                    FPNumber = s.Fpnumber,
                    ProfileMaster = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProductId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageForm = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageFormID = s.DosageFormId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    ProfileReferenceNo = s.Fpnumber,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo
                };
                fPCommonFieldModel.Add(fPCommonFieldModels);
            });
            return fPCommonFieldModel.OrderByDescending(a => a.FPCommonFieldID).SingleOrDefault(p => p.FPCommonFieldID == id.Value);
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPCommonFieldModel> GetData(SearchModel searchModel)
        {
            var FpcommonField = new FpcommonField();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).FirstOrDefault();
                        break;
                    case "Last":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).LastOrDefault();
                        break;
                    case "Next":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).LastOrDefault();
                        break;
                    case "Previous":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).FirstOrDefault();
                        break;
                    case "Last":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).LastOrDefault();
                        break;
                    case "Next":
                        FpcommonField = _context.FpcommonField.OrderBy(o => o.FpcommonFieldId).FirstOrDefault(s => s.FpcommonFieldId > searchModel.Id);
                        break;
                    case "Previous":
                        FpcommonField = _context.FpcommonField.OrderByDescending(o => o.FpcommonFieldId).FirstOrDefault(s => s.FpcommonFieldId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPCommonFieldModel>(FpcommonField);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFPCommonField")]
        public FPCommonFieldModel Post(FPCommonFieldModel value)
        {
            var profileNo = _generateProfileClass.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.FPNumberID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.FPNumber });

            var FpcommonField = new FpcommonField
            {
                Fpnumber = value.FPNumber,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                FpnumberId = value.FPNumberID.Value,
                DosageFormId = value.DosageFormID,
                FinishProductId = value.FinishProductID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LinkProfileReferenceNo = profileNo,

            };
            _context.FpcommonField.Add(FpcommonField);
            _context.SaveChanges();
            value.LinkProfileReferenceNo = FpcommonField.LinkProfileReferenceNo;
            value.FPCommonFieldID = FpcommonField.FpcommonFieldId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFPCommonField")]
        public FPCommonFieldModel Put(FPCommonFieldModel value)
        {
            var FpcommonField = _context.FpcommonField.SingleOrDefault(p => p.FpcommonFieldId == value.FPCommonFieldID);
            FpcommonField.FpnumberId = value.FPNumberID.Value;
            FpcommonField.Fpnumber = value.FPNumber;
            FpcommonField.FinishProductId = value.FinishProductID;
            FpcommonField.DosageFormId = value.DosageFormID;
            FpcommonField.ModifiedByUserId = value.ModifiedByUserID;
            FpcommonField.StatusCodeId = value.StatusCodeID.Value;
            FpcommonField.ModifiedDate = DateTime.Now;
            if (FpcommonField.LinkProfileReferenceNo == null)
            {
                var profileNo = _generateProfileClass.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.FPNumberID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.FPNumber });
                FpcommonField.LinkProfileReferenceNo = profileNo;
                value.LinkProfileReferenceNo = profileNo;
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFPCommonField")]
        public void Delete(int id)
        {
            var fpLine = _context.FpcommonFieldLine.Where(f => f.FpcommonFiledId == id).ToList();
            if (fpLine.Count > 0)
            {
                _context.FpcommonFieldLine.RemoveRange(fpLine);
            }
            var FpcommonField = _context.FpcommonField.SingleOrDefault(p => p.FpcommonFieldId == id);
            if (FpcommonField != null)
            {
                _context.FpcommonField.Remove(FpcommonField);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("GetFPReports")]
        public List<ReportModel> GetFPReports(ClassificationReportParam reportParam)
        {
            List<ReportModel> fpReportModels = new List<ReportModel>();
            ItemClassificationController itemClassificationController = new ItemClassificationController(_context, _mapper, _generateProfileClass);
            List<ItemClassificationFormsModel> itemClassificationFormsModels = itemClassificationController.GetItemForm((int?)reportParam.ItemClassificationMasterID,(int)reportParam.UserId);

            ReportModel fpReportModel = new ReportModel();

            List<Header> headers = new List<Header>
            {
                new Header{Text="Profile Name",Value="profileName",Align="left" },
                new Header{Text="Profile Reference No",Value="linkProfileReferenceNo",Align="left" },
                new Header{Text="SW Manufacture Group Name",Value="profileMaster",Align="left" },
                new Header{Text="Dosage Form Name",Value="dosageForm",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },

            };

            fpReportModel.HeaderName = "FP Header";
            fpReportModel.Headers = headers;
            var fpCommonFiledModel = GetFpCommonFieldByID((int?)reportParam.ItemId);
            fpReportModel.DynamicModels.Add(fpCommonFiledModel);
            fpReportModels.Add(fpReportModel);
            itemClassificationFormsModels.ForEach(i =>
            {
                fpReportModel = GenerateFpReportModel(i.ModuleName, reportParam);
                fpReportModels.Add(fpReportModel);
                if (i.ApplicationFormSubModelList != null)
                {
                    fpReportModel = new ReportModel();
                    i.ApplicationFormSubModelList.ForEach(s =>
                    {
                        fpReportModel = GenerateFpReportModel(s.ModuleName, reportParam);
                        fpReportModels.Add(fpReportModel);
                    });
                }
            });

            return fpReportModels;
        }

        private ReportModel GenerateFpReportModel(string moduleName, ClassificationReportParam reportParam)
        {
            ReportModel reportModel = new ReportModel();
            FPManufacturingRecipeController fPManufacturingRecipeController = new FPManufacturingRecipeController(_context, _mapper, _generateProfileClass);
            FinishProductExcipientController finishProductExcipientController = new FinishProductExcipientController(_context, _mapper, _generateProfileClass);
            if (moduleName == "FPManufacturingRecipe")
            {
                reportModel.HeaderName = "Manufacturing Recipe";
                List<Header> recipeHeaders = new List<Header>
                    {
                        new Header{Text="Manufacturing Site",Value="manufacturingSite",Align="left" },
                        new Header{Text="Register Country",Value="registerCountry",Align="left" },
                        new Header{Text="Register Holder Name",Value="registerHolderName",Align="left" },
                        new Header{Text="Product Name",Value="customerProductName",Align="left" },
                        new Header{Text="Is Master",Value="master",Align="left" },
                        new Header{Text="Added Date",Value="addedDate",Align="left" },
                    };
                reportModel.Headers = recipeHeaders;
                var recipeItems = fPManufacturingRecipeController.GetFPManufacturingRecipesByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in recipeItems)
                {
                    item.Master = item.IsMaster.GetValueOrDefault(false) ? "Master" : "Non-Master";
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "FinishProductExcipient")
            {
                reportModel.HeaderName = "Finish Product Excipient";
                List<Header> excipientHeaders = new List<Header>
                    {
                        new Header{Text="FP Product Name",Value="productName",Align="left" },
                        new Header{Text="Same As Master",Value="masterDocument",Align="left" },
                        new Header{Text="Same As Existing BMP",Value="isInformationvsmaster",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = excipientHeaders;
                var excipientitems = finishProductExcipientController.GetFinishProductExcipientByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in excipientitems)
                {
                    item.MasterDocument = item.IsMasterDocument.GetValueOrDefault(false) ? "Master" : "Non-Master";
                    reportModel.DynamicModels.Add(item as dynamic);
                }

            }
            return reportModel;
        }
    }
}