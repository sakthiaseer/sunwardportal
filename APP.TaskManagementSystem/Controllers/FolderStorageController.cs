﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FolderStorageController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FolderStorageController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFolderStorageModel")]
        public List<FolderStorageModel> Get()
        {
            var applicationUsers = _context.ApplicationUser.AsNoTracking().ToList();
            List<FolderStorageModel> folderStorageModels = new List<FolderStorageModel>();
            var folderStorages = _context.FolderStorage.Include("FolderStorageUser").OrderByDescending(o => o.FolderStorageId).AsNoTracking().ToList();
            folderStorages.ForEach(s =>
            {
                FolderStorageModel folderStorageModel = new FolderStorageModel
                {
                    FolderStorageID = s.FolderStorageId,
                    UserID = s.UserId,
                    FolderID = s.FolderId,
                    Name = s.Name,
                    Description = s.Description,
                    SessionID = s.SessionId,
                    FolderTypeID = s.FolderTypeId,
                    FolderLocation = s.FolderLocation,
                    StorageLimit = s.StorageLimit,
                    UserGroupID = s.UserGroupId,
                    UserGroupUserID = s.UserGroupUserId,
                    UserSelection = s.FolderStorageUser.Select(c => c.UserSelectionId).ToList(),
                    RoleID = s.RoleId,
                    UserRoleID = s.UserRoleId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = "Active",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    UserNames = applicationUsers.Where(a => a.UserId == s.UserId).Select(tn => tn.UserName).ToList(),
                };
                folderStorageModels.Add(folderStorageModel);
            });


            return folderStorageModels;
        }

        // GET: api/Project/2
        [HttpGet("GetFolderStorage/{id:int}")]
        public ActionResult<FolderStorageModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var folderStorage = _context.FolderStorage.SingleOrDefault(p => p.FolderStorageId == id.Value);
            var result = _mapper.Map<FolderStorageModel>(folderStorage);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FolderStorageModel> GetData(SearchModel searchModel)
        {
            var folderStorage = new FolderStorage();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).FirstOrDefault();
                        break;
                    case "Last":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).LastOrDefault();
                        break;
                    case "Next":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).LastOrDefault();
                        break;
                    case "Previous":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).FirstOrDefault();
                        break;
                    case "Last":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).LastOrDefault();
                        break;
                    case "Next":
                        folderStorage = _context.FolderStorage.OrderBy(o => o.FolderStorageId).FirstOrDefault(s => s.FolderStorageId > searchModel.Id);
                        break;
                    case "Previous":
                        folderStorage = _context.FolderStorage.OrderByDescending(o => o.FolderStorageId).FirstOrDefault(s => s.FolderStorageId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FolderStorageModel>(folderStorage);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFolderStorage")]
        public FolderStorageModel Post(FolderStorageModel value)
        {
            if (value.FolderTypeID == null)
            {
                var foldertype = _context.Folders.OrderByDescending(o => o.FolderTypeId).FirstOrDefault(id => id.FolderId == value.FolderID);

                var userGroupUserDetails = _context.UserGroupUser.OrderByDescending(o => o.UserId).FirstOrDefault(id => id.UserGroupId == value.UserGroupID);


                value.FolderTypeID = foldertype.FolderTypeId;
                value.SessionID = foldertype.SessionId;
                value.UserGroupID = userGroupUserDetails.UserGroupId;
                value.UserGroupUserID = userGroupUserDetails.UserGroupUserId;
                value.UserID = userGroupUserDetails.UserId;

                var userRoleDetails = _context.ApplicationUserRole.OrderByDescending(o => o.UserId).FirstOrDefault(id => id.UserId == value.UserID);

                value.UserRoleID = userRoleDetails.UserRoleId;

                value.RoleID = userRoleDetails.RoleId;

            }
            var folderStorage = new FolderStorage
            {
                FolderId = value.FolderID,
                Name = value.Name,
                Description = value.Description,
                SessionId = value.SessionID,
                FolderTypeId = value.FolderTypeID,
                FolderLocation = value.FolderLocation,
                StorageLimit = value.StorageLimit,
                UserGroupId = value.UserGroupID,
                UserGroupUserId = value.UserGroupUserID,
                UserRoleId = value.UserRoleID,
                RoleId = value.RoleID,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                UserId = value.UserSelection.Count > 0 ? value.UserSelection[0] : null,
            };

            if (value.UserSelection != null)
            {
                value.UserSelection.ForEach(c =>
                {
                    var folderstorageuser = new FolderStorageUser
                    {
                        UserSelectionId = c,
                        FolderStorageId = value.FolderStorageID,
                    };
                    folderStorage.FolderStorageUser.Add(folderstorageuser);
                });
            }
            _context.FolderStorage.Add(folderStorage);
            _context.SaveChanges();
            value.FolderStorageID = folderStorage.FolderStorageId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        [Route("UpdateFolderStorage")]
        public FolderStorageModel Put(FolderStorageModel value)
        {
            var folderStorage = _context.FolderStorage.SingleOrDefault(p => p.FolderStorageId == value.FolderStorageID);

            if (value.FolderTypeID != null)
            {
                var foldertype = _context.Folders.OrderByDescending(o => o.FolderTypeId).FirstOrDefault(id => id.FolderId == value.FolderID);

                folderStorage.FolderTypeId = foldertype.FolderTypeId;
                folderStorage.SessionId = foldertype.SessionId;

                var userGroupUserDetails = _context.UserGroupUser.OrderByDescending(o => o.UserId).FirstOrDefault(id => id.UserGroupId == value.UserGroupID);

                folderStorage.UserGroupId = userGroupUserDetails.UserGroupId;
                folderStorage.UserGroupUserId = userGroupUserDetails.UserGroupUserId;
                folderStorage.UserId = userGroupUserDetails.UserId;

                var userRoleDetails = _context.ApplicationUserRole.OrderByDescending(o => o.UserId).FirstOrDefault(id => id.UserId == value.UserID);

                folderStorage.UserRoleId = userRoleDetails.UserRoleId;

                folderStorage.RoleId = userRoleDetails.RoleId;
            }

            folderStorage.UserId = value.UserSelection[0];
            folderStorage.FolderId = value.FolderID;
            folderStorage.Name = value.Name;
            folderStorage.Description = value.Description;
            //folderStorage.SessionId = value.SessionID;
            //folderStorage.FolderTypeId = value.FolderTypeID;
            folderStorage.FolderLocation = value.FolderLocation;
            folderStorage.StorageLimit = value.StorageLimit;
            //folderStorage.UserGroupId = value.UserGroupID;
            //folderStorage.UserGroupUserId = value.UserGroupUserID;
            //folderStorage.RoleId = value.RoleID;
            folderStorage.AddedByUserId = value.AddedByUserID;
            folderStorage.AddedDate = DateTime.Now;
            folderStorage.ModifiedByUserId = value.ModifiedByUserID;
            folderStorage.StatusCodeId = value.StatusCodeID.Value;
            folderStorage.ModifiedDate = DateTime.Now;

            var folderstorageuser = _context.FolderStorageUser.Where(l => l.FolderStorageId == value.FolderStorageID).ToList();
            if (folderstorageuser.Count > 0)
            {
                _context.FolderStorageUser.RemoveRange(folderstorageuser);
                _context.SaveChanges();
            }

            if (value.UserSelection != null)
            {
                value.UserSelection.ForEach(c =>
                {
                    var folderstorageuserselection = new FolderStorageUser
                    {
                        UserSelectionId = c,
                        FolderStorageId = value.FolderStorageID,
                    };
                    folderStorage.FolderStorageUser.Add(folderstorageuserselection);
                });
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFolderStorage")]
        public ActionResult Delete(int id)
        {
            var folderStorage = _context.FolderStorage.SingleOrDefault(p => p.FolderStorageId == id);

            var folderstorageuser = _context.FolderStorageUser.Where(l => l.FolderStorageId == folderStorage.FolderStorageId).ToList();
            if (folderstorageuser.Count > 0)
            {
                _context.FolderStorageUser.RemoveRange(folderstorageuser);
                _context.SaveChanges();
            }

            if (folderStorage != null)
            {
                _context.FolderStorage.Remove(folderStorage);
                _context.SaveChanges();

                return Ok("Record Deleted.");
            }
            throw new AppException("Delete Functionality Could not work.!", null);
        }
    }
}