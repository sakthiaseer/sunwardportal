﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavItemBatchNoController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavItemBatchNoController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNavItemBatchNo")]
        public List<NavItemBatchNoModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ipirs = _context.NavItemBatchNo
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i=>i.Item).Where(t=>t.ItemId == id)
               .AsNoTracking()
                .ToList();
            List<NavItemBatchNoModel> navItemBatchNoModels = new List<NavItemBatchNoModel>();
            ipirs.ForEach(s =>
            {
                NavItemBatchNoModel navItemBatchNoModel = new NavItemBatchNoModel
                {
                    NavItemBatchNoID = s.NavItemBatchNoId,
                    ItemId = s.ItemId,
                    ItemNo = s.ItemNo,
                    BatchNo = s.BatchNo,
                    BatchSize = s.BatchSize,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                   
                };
                navItemBatchNoModels.Add(navItemBatchNoModel);
            });
            return navItemBatchNoModels.OrderByDescending(a => a.NavItemBatchNoID).ToList();
        }
    }
}