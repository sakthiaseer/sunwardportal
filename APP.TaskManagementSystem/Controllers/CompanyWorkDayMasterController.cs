﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyWorkDayMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CompanyWorkDayMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        //WorkDateId
        //WorkDateID
        [HttpGet]
        [Route("GetCompanyWorkDayMasters")]
        public List<CompanyWorkDayMasterModel> Get()
        {

            var companyWorkDayMaster = _context.CompanyWorkDayMaster.Include(a => a.AddedByUser)
                                        .Include(m => m.ModifiedByUser).Include(w => w.WorkDayShift).AsNoTracking().ToList();
            List<CompanyWorkDayMasterModel> companyWorkDayMasterModel = new List<CompanyWorkDayMasterModel>();
            companyWorkDayMaster.ForEach(s =>
            {
                CompanyWorkDayMasterModel companyWorkDayMasterModels = new CompanyWorkDayMasterModel
                {
                    WorkDateID = s.WorkDateId,
                    CodeNO = s.CodeNo,
                    CountryID = s.CountryId,
                    StateID = s.StateId,
                    Description = s.Description,
                    Reference = s.Reference,
                    CountryName = s.Country.Name,
                    StateName = s.State.Name,
                    IsMon = s.IsMon,
                    IsTue = s.IsTue,
                    IsWed = s.IsWed,
                    IsThur = s.IsThur,
                    IsFri = s.IsFri,
                    IsSat = s.IsSat,
                    IsSun = s.IsSun,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ShiftID = s.WorkDayShift.FirstOrDefault().ShiftId,
                };
                companyWorkDayMasterModel.Add(companyWorkDayMasterModels);
            });
            return companyWorkDayMasterModel.OrderByDescending(a => a.WorkDateID).ToList();

        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get CompanyWorkDayMaster")]
        [HttpGet("GetCompanyWorkDayMasters/{id:int}")]
        public ActionResult<CompanyWorkDayMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyWorkDayMaster = _context.CompanyWorkDayMaster.SingleOrDefault(p => p.WorkDateId == id.Value);
            var result = _mapper.Map<CompanyWorkDayMasterModel>(companyWorkDayMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyWorkDayMasterModel> GetData(SearchModel searchModel)
        {
            var companyWorkDayMaster = new CompanyWorkDayMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).FirstOrDefault();
                        break;
                    case "Last":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).LastOrDefault();
                        break;
                    case "Next":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).LastOrDefault();
                        break;
                    case "Previous":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).FirstOrDefault();
                        break;
                    case "Last":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).LastOrDefault();
                        break;
                    case "Next":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderBy(o => o.WorkDateId).FirstOrDefault(s => s.WorkDateId > searchModel.Id);
                        break;
                    case "Previous":
                        companyWorkDayMaster = _context.CompanyWorkDayMaster.OrderByDescending(o => o.WorkDateId).FirstOrDefault(s => s.WorkDateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyWorkDayMasterModel>(companyWorkDayMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyWorkDayMaster")]
        public CompanyWorkDayMasterModel Post(CompanyWorkDayMasterModel value)
        {
            var companyWorkDayMaster = new CompanyWorkDayMaster
            {
                //WorkDateId = value.WorkDateID,
                //CountryOptions = value.CountryOptions,
                CodeNo = value.CodeNO,
                CountryId = value.CountryID,
                StateId = value.StateID,
                Description = value.Description,
                Reference = value.Reference,
                IsMon = value.IsMon,
                IsTue = value.IsTue,
                IsWed = value.IsWed,
                IsThur = value.IsThur,
                IsFri = value.IsFri,
                IsSat = value.IsSat,
                IsSun = value.IsSun,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID

            };
            _context.CompanyWorkDayMaster.Add(companyWorkDayMaster);
            _context.SaveChanges();
            value.WorkDateID = companyWorkDayMaster.WorkDateId;

            var workDayShift = new WorkDayShift
            {
                ShiftId = value.ShiftID,
                WorkDatyId = value.WorkDateID,
            };
            _context.WorkDayShift.Add(workDayShift);
            _context.SaveChanges();
            value.WorkdayShiftID = workDayShift.WorkdayShiftId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyWorkDayMaster")]
        public CompanyWorkDayMasterModel Put(CompanyWorkDayMasterModel value)
        {
            var companyWorkDayMaster = _context.CompanyWorkDayMaster.SingleOrDefault(p => p.WorkDateId == value.WorkDateID);
            //companyWorkDayMaster.WorkDateId = value.WorkDateID;
            companyWorkDayMaster.CodeNo = value.CodeNO;
            companyWorkDayMaster.CountryId = value.CountryID;
            companyWorkDayMaster.StateId = value.StateID;
            companyWorkDayMaster.Description = value.Description;
            companyWorkDayMaster.Reference = value.Reference;
            companyWorkDayMaster.IsMon = value.IsMon;
            companyWorkDayMaster.IsTue = value.IsTue;
            companyWorkDayMaster.IsWed = value.IsWed;
            companyWorkDayMaster.IsThur = value.IsThur;
            companyWorkDayMaster.IsFri = value.IsFri;
            companyWorkDayMaster.IsSat = value.IsSat;
            companyWorkDayMaster.IsSun = value.IsSun;
            companyWorkDayMaster.ModifiedByUserId = value.ModifiedByUserID;
            companyWorkDayMaster.ModifiedDate = DateTime.Now;
            companyWorkDayMaster.StatusCodeId = value.StatusCodeID;
            var workDayShift = _context.WorkDayShift.SingleOrDefault(p => p.WorkDatyId == value.WorkDateID);
            {
                if (workDayShift != null)
                {
                    var workDay = _context.WorkDayShift.SingleOrDefault(p => p.WorkdayShiftId == workDayShift.WorkdayShiftId);
                    {
                        workDay.ShiftId = value.ShiftID;
                        workDay.WorkDatyId = value.WorkDateID;
                        companyWorkDayMaster.WorkDayShift.Add(workDay);
                    }
                }
            };
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyWorkDayMaster")]
        public void Delete(int id)
        {
            var companyWorkDayMaster = _context.CompanyWorkDayMaster.SingleOrDefault(p => p.WorkDateId == id);
            if (companyWorkDayMaster != null)
            {
                if (companyWorkDayMaster.WorkDateId > 0)
                {
                    var workDayShift = _context.WorkDayShift.Where(d => d.WorkDatyId == companyWorkDayMaster.WorkDateId).ToList();
                    if (workDayShift.Count > 0)
                    {
                        _context.WorkDayShift.RemoveRange(workDayShift);
                        _context.SaveChanges();
                    }
                }
                _context.CompanyWorkDayMaster.Remove(companyWorkDayMaster);
                _context.SaveChanges();
            }
        }
    }
}