﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskNotesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskNotesController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskNotes")]
        public ActionResult<TaskNotesModel> Get(int id, int userId)
        {

            var taskNotes = _context.TaskNotes.SingleOrDefault(t => t.TaskId == id && t.TaskUserId == userId);
            var result = _mapper.Map<TaskNotesModel>(taskNotes);
            return result;


        }

        // GET: api/Project/2
        [HttpGet("GetIsReminder")]
        public bool Get(int? id, int userId)
        {
            bool result = false;
            var taskNotes = _context.TaskNotes.FirstOrDefault(p => p.TaskId == id.Value && p.TaskUserId == userId);
            if (taskNotes != null)
            {
                result = taskNotes.IsNoReminderDate.HasValue ? taskNotes.IsNoReminderDate.Value : false;
            }
            else
                return false;
            return result;
        }
        [HttpGet("GetTaskNotes/{id:int}")]
        public ActionResult<TaskNotesModel> GetIsNotes(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskNotes = _context.TaskNotes.SingleOrDefault(p => p.TaskId == id.Value);
            var result = _mapper.Map<TaskNotesModel>(taskNotes);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetTaskPersonalNotes")]
        public List<TaskUnReadNotesModel> GetTaskPersonalNotes(TaskUnReadNotesModel value)
        {

            List<TaskUnReadNotesModel> taskUnReadNotesModels = new List<TaskUnReadNotesModel>();
            var taskNotes = _context.TaskUnReadNotes.Where(p => p.TaskCommentId==value.TaskCommentId && p.UserId == value.UserId).ToList();
            if (taskNotes != null)
            {
                taskNotes.ForEach(t =>
                {
                    TaskUnReadNotesModel taskUnReadNotesModel = new TaskUnReadNotesModel();
                    taskUnReadNotesModel.TaskCommentId = t.TaskCommentId;
                    taskUnReadNotesModel.UserId = t.UserId;
                    taskUnReadNotesModel.TaskId = t.TaskId;
                    taskUnReadNotesModel.Notes = t.Notes;
                    taskUnReadNotesModel.AddedDate = t.AddedDate;
                    taskUnReadNotesModels.Add(taskUnReadNotesModel);

                });
               
            }

            return taskUnReadNotesModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskNotesModel> GetData(SearchModel searchModel)
        {
            var taskNotes = new TaskNotes();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).FirstOrDefault();
                        break;
                    case "Last":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).LastOrDefault();
                        break;
                    case "Next":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).LastOrDefault();
                        break;
                    case "Previous":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).FirstOrDefault();
                        break;
                    case "Last":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).LastOrDefault();
                        break;
                    case "Next":
                        taskNotes = _context.TaskNotes.OrderBy(o => o.TaskNotesId).FirstOrDefault(s => s.TaskNotesId > searchModel.Id);
                        break;
                    case "Previous":
                        taskNotes = _context.TaskNotes.OrderByDescending(o => o.TaskNotesId).FirstOrDefault(s => s.TaskNotesId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskNotesModel>(taskNotes);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskNotes")]
        public TaskNotesModel Post(TaskNotesModel value)
        {
            var tasknotes = _context.TaskNotes.Where(p => p.TaskId == value.TaskID && p.TaskUserId == value.TaskUserID).FirstOrDefault();
            if (tasknotes == null)
            {
                var taskNotes = new TaskNotes
                {

                    TaskId = value.TaskID,
                    Notes = value.Notes,
                    RemainderDate = value.RemainderDate,
                    TaskUserId = value.TaskUserID,
                    ModifiedDate = value.ModifiedDate,
                    IsNoReminderDate = value.IsNoReminderDate,

                };
                _context.TaskNotes.Add(taskNotes);
                _context.SaveChanges();
                value.TaskNotesID = taskNotes.TaskNotesId;
            }
            else
            {
                tasknotes.Notes = value.Notes;
                tasknotes.RemainderDate = value.RemainderDate;
                tasknotes.TaskId = value.TaskID;
                tasknotes.TaskUserId = value.TaskUserID;
                tasknotes.IsNoReminderDate = value.IsNoReminderDate;
                _context.SaveChanges();

            }
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskNotes")]
        public TaskNotesModel Put(TaskNotesModel value)
        {
            var taskNotes = _context.TaskNotes.SingleOrDefault(p => p.TaskNotesId == value.TaskNotesID);
            //  folders.TaskMemberId = value.TaskMemberID;
            taskNotes.TaskId = value.TaskID;
            taskNotes.TaskUserId = value.TaskUserID;
            taskNotes.Notes = value.Notes;
            taskNotes.RemainderDate = value.RemainderDate;
            taskNotes.ModifiedDate = value.ModifiedDate;
            taskNotes.IsNoReminderDate = value.IsNoReminderDate;

            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("InsertTaskUnReadNotes")]
        public TaskUnReadNotesModel InsertTaskUnReadNotes(TaskUnReadNotesModel value)
        {
            //var exist = _context.TaskUnReadNotes.Where(p => p.TaskCommentId == value.TaskCommentId&& p.UserId == value.UserId).FirstOrDefault();
            //if (exist == null)
            //{
                var taskUnReadNotes = new TaskUnReadNotes
                {

                    TaskId = value.TaskId,
                    Notes = value.Notes,
                    UserId = value.UserId,
                    TaskCommentId = value.TaskCommentId,
                    MyDueDate = value.MyDueDate,
                    AddedDate = DateTime.Now,
                };
                _context.TaskUnReadNotes.Add(taskUnReadNotes);
                _context.SaveChanges();
                value.TaskUnReadNotesId = taskUnReadNotes.TaskUnReadNotesId;
            //}
            //else
            //{
            //    exist.Notes = value.Notes;
             
            //    exist.TaskId = value.TaskId;
            //    exist.UserId = value.UserId;
            //    exist.TaskCommentId = value.TaskCommentId;
            //    exist.AddedDate = DateTime.Now;
            //    _context.SaveChanges();

            //}
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskUnReadNotes")]
        public TaskUnReadNotesModel UpdateTaskUnReadNotes(TaskUnReadNotesModel value)
        {
            var taskNotes = _context.TaskUnReadNotes.SingleOrDefault(p => p.TaskUnReadNotesId == value.TaskUnReadNotesId);
            //  folders.TaskMemberId = value.TaskMemberID;
            taskNotes.Notes = value.Notes;
            taskNotes.MyDueDate = value.MyDueDate;
            taskNotes.TaskId = value.TaskId;
            taskNotes.UserId = value.UserId;
            taskNotes.TaskCommentId = value.TaskCommentId;
            taskNotes.AddedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateTaskUnReadMyDueDate")]
        public TaskUnReadNotesModel UpdateTaskUnReadMyDueDate(TaskUnReadNotesModel value)
        {
            var taskNotes = _context.TaskUnReadNotes.SingleOrDefault(p => p.TaskCommentId == value.TaskCommentId && p.UserId==value.UserId );
            if (taskNotes != null)
            {
                taskNotes.MyDueDate = value.MyDueDate;
                taskNotes.TaskCommentId = value.TaskCommentId;
            }
            else
            {
                var tasUnReadNotes = new TaskUnReadNotes
                {
                    MyDueDate=value.MyDueDate,
                    TaskId=value.TaskId,    
                    TaskCommentId=value.TaskCommentId,
                    UserId=value.UserId,
                    AddedDate=DateTime.Now,
                };
                _context.TaskUnReadNotes.Add(tasUnReadNotes);
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("DeleteTaskNotes")]
        public void DeleteTaskNotes(TaskNotesModel value)
        {
            var notes = _context.TaskNotes.Where(w => w.TaskId == value.TaskID && w.TaskUserId == value.TaskUserID).ToList();
            if (notes.Count() > 0)
            {
                _context.TaskNotes.RemoveRange(notes);
                _context.SaveChanges();
            }

        }
        [HttpPost]
        [Route("DeleteTaskUnReadNotes")]
        public void DeleteTaskUnReadNotes(TaskUnReadNotesModel value)
        {
            var notes = _context.TaskUnReadNotes.Where(w => w.TaskUnReadNotesId == value.TaskUnReadNotesId).ToList();
            if (notes.Count() > 0)
            {
                _context.TaskUnReadNotes.RemoveRange(notes);
                _context.SaveChanges();
            }

        }
    }
}