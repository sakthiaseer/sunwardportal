﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentRoleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentRoleController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentRoles")]
        public List<DocumentRoleModel> Get()
        {
            var documentRole = _context.DocumentRole.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DocumentRoleModel> documentRoleModel = new List<DocumentRoleModel>();
            documentRole.ForEach(s =>
            {
                DocumentRoleModel documentRoleModels = new DocumentRoleModel
                {
                    DocumentRoleID = s.DocumentRoleId,
                    DocumentRoleName = s.DocumentRoleName,
                    DocumentRoleDescription = s.DocumentRoleDescription,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                documentRoleModel.Add(documentRoleModels);
            });
            return documentRoleModel.OrderByDescending(a => a.DocumentRoleID).ToList();

        }
        [HttpGet]
        [Route("GetDocumentRoleIDByPermission")]
        public List<DocumentRoleModel> GetDocumentPermissionByRollIDList()
        {
            List<DocumentRoleModel> documentRoleModel = new List<DocumentRoleModel>();
            List<long?> permissionRoleIDs = _context.DocumentPermission.AsNoTracking().Select(p => p.DocumentRoleId).ToList();
            permissionRoleIDs = permissionRoleIDs == null ? new List<long?>() : permissionRoleIDs;
            //var roleList = _context.DocumentRole.Where(d => !permissionRoleIDs.Contains(d.DocumentRoleId)).ToList();
            // if (permissionRoleIDs != null && permissionRoleIDs.Count > 0)
            //{
            var documentRole = _context.DocumentRole.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            documentRole.ForEach(s =>
            {
                DocumentRoleModel documentRoleModels = new DocumentRoleModel
                {
                    DocumentRoleID = s.DocumentRoleId,
                    DocumentRoleName = s.DocumentRoleName,
                    DocumentRoleDescription = s.DocumentRoleDescription,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                documentRoleModel.Add(documentRoleModels);
            });
            documentRoleModel.OrderByDescending(a => a.DocumentRoleID).Where(d => !permissionRoleIDs.Contains(d.DocumentRoleID) && d.StatusCodeID == 1).ToList();
            // }
            return documentRoleModel;
            //return documentPermission;

        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get DocumentRole")]
        [HttpGet("GetDocumentRoles/{id:int}")]
        public ActionResult<DocumentRoleModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentRole = _context.DocumentRole.SingleOrDefault(p => p.DocumentRoleId == id.Value);
            var result = _mapper.Map<DocumentRoleModel>(documentRole);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentRoleModel> GetData(SearchModel searchModel)
        {
            var documentRole = new DocumentRole();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).LastOrDefault();
                        break;
                    case "Next":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).LastOrDefault();
                        break;
                    case "Previous":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).LastOrDefault();
                        break;
                    case "Next":
                        documentRole = _context.DocumentRole.OrderBy(o => o.DocumentRoleId).FirstOrDefault(s => s.DocumentRoleId > searchModel.Id);
                        break;
                    case "Previous":
                        documentRole = _context.DocumentRole.OrderByDescending(o => o.DocumentRoleId).FirstOrDefault(s => s.DocumentRoleId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentRoleModel>(documentRole);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentRole")]
        public DocumentRoleModel Post(DocumentRoleModel value)
        {
            var documentRole = new DocumentRole
            {
                //DocumentRoleId = value.DocumentRoleID,
                DocumentRoleName = value.DocumentRoleName,
                DocumentRoleDescription = value.DocumentRoleDescription,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };
            _context.DocumentRole.Add(documentRole);
            _context.SaveChanges();
            value.DocumentRoleID = documentRole.DocumentRoleId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentRole")]
        public DocumentRoleModel Put(DocumentRoleModel value)
        {
            var documentRole = _context.DocumentRole.SingleOrDefault(p => p.DocumentRoleId == value.DocumentRoleID);
            //DocumentRole.DocumentRoleId = value.DocumentRoleID;
            documentRole.DocumentRoleId = value.DocumentRoleID;
            documentRole.DocumentRoleName = value.DocumentRoleName;
            documentRole.DocumentRoleDescription = value.DocumentRoleDescription;
            documentRole.ModifiedByUserId = value.ModifiedByUserID;
            documentRole.ModifiedDate = DateTime.Now;
            documentRole.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentRole")]
        public void Delete(int id)
        {
            try
            {
                var documentRole = _context.DocumentRole.SingleOrDefault(p => p.DocumentRoleId == id);
                if (documentRole != null)
                {

                    _context.DocumentRole.Remove(documentRole);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Role cannot be deleted, it is used against permission", ex);
            }
        }
    }
}