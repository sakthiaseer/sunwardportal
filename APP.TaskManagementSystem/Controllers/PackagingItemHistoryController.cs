using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingItemHistoryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public PackagingItemHistoryController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetPackagingItemHistorys")]
        public List<PackagingItemHistoryModel> Get()
        {

            var packagingItemHistorylist = _context.PackagingItemHistory
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Category).Include(l => l.Item).OrderByDescending(a => a.PackagingItemHistoryId).AsNoTracking()
                .ToList();
            List<PackagingItemHistoryModel> packagingItemHistoryModels = new List<PackagingItemHistoryModel>();


            if (packagingItemHistorylist != null)
            {
                packagingItemHistorylist.ForEach(s =>
                {
                    PackagingItemHistoryModel packagingItemHistoryModel = new PackagingItemHistoryModel
                    {
                        PackagingItemHistoryId = s.PackagingItemHistoryId,
                        CategoryId = s.CategoryId,
                        ItemId = s.ItemId,
                        CategoryName = s.Category != null ? s.Category.Code : "",
                        ItemName = s.Item != null ? s.Item.Description : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    };
                    packagingItemHistoryModels.Add(packagingItemHistoryModel);
                });

            }
            return packagingItemHistoryModels;
        }

        [HttpPost()]
        [Route("GetPackagingItemHistory")]
        public ActionResult<PackagingItemHistoryModel> GetPackagingItemHistoryData(SearchModel searchModel)
        {
            var packagingItemHistory = new PackagingItemHistory();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).LastOrDefault();
                        break;
                    case "Next":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).LastOrDefault();
                        break;
                    case "Next":
                        packagingItemHistory = _context.PackagingItemHistory.OrderBy(o => o.PackagingItemHistoryId).FirstOrDefault(s => s.PackagingItemHistoryId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingItemHistory = _context.PackagingItemHistory.OrderByDescending(o => o.PackagingItemHistoryId).FirstOrDefault(s => s.PackagingItemHistoryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingItemHistoryModel>(packagingItemHistory);
            return result;
        }

        [HttpPost]
        [Route("InsertPackagingItemHistory")]
        public PackagingItemHistoryModel Post(PackagingItemHistoryModel value)
        {
            var packagingItemHistory = new PackagingItemHistory
            {
                CategoryId = value.CategoryId,
                ItemId = value.ItemId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.PackagingItemHistory.Add(packagingItemHistory);
            _context.SaveChanges();
            value.PackagingItemHistoryId = packagingItemHistory.PackagingItemHistoryId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePackagingItemHistory")]
        public PackagingItemHistoryModel Put(PackagingItemHistoryModel value)
        {
            var packagingItemHistory = _context.PackagingItemHistory.SingleOrDefault(p => p.PackagingItemHistoryId == value.PackagingItemHistoryId);
            packagingItemHistory.CategoryId = value.CategoryId;
            packagingItemHistory.ItemId = value.ItemId;
            packagingItemHistory.StatusCodeId = value.StatusCodeID.Value;
            packagingItemHistory.ModifiedByUserId = value.ModifiedByUserID;
            packagingItemHistory.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeletePackagingItemHistory")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var packagingItemHistory = _context.PackagingItemHistory.Where(p => p.PackagingItemHistoryId == id).FirstOrDefault();
                if (packagingItemHistory != null)
                {
                    var packagingItemHistoryLine = _context.PackagingItemHistoryLine.Where(p => p.PackagingItemHistoryId == id).AsNoTracking().ToList();
                    if (packagingItemHistoryLine != null)
                    {
                        packagingItemHistoryLine.ForEach(h =>
                        {
                            var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument.Where(p => p.SessionId == h.FileSessionId).AsNoTracking().ToList();
                            if (packagingHistoryItemLineDocument != null)
                            {
                                _context.PackagingHistoryItemLineDocument.RemoveRange(packagingHistoryItemLineDocument);
                                _context.SaveChanges();
                            }

                            packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument.Where(p => p.SessionId == h.ImageSessionId).AsNoTracking().ToList();
                            if (packagingHistoryItemLineDocument != null)
                            {
                                _context.PackagingHistoryItemLineDocument.RemoveRange(packagingHistoryItemLineDocument);
                                _context.SaveChanges();
                            }
                        });
                        _context.PackagingItemHistoryLine.RemoveRange(packagingItemHistoryLine);
                        _context.SaveChanges();
                    }
                    _context.PackagingItemHistory.Remove(packagingItemHistory);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("GetPackagingItemHistoryLine")]
        public List<PackagingItemHistoryLineModel> GetPackagingItemHistoryLine(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingItemHistoryLine = _context.PackagingItemHistoryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.Item).Where(w => w.PackagingItemHistoryId == id).OrderByDescending(a => a.PackagingItemHistoryLineId).AsNoTracking().ToList();

            List<PackagingItemHistoryLineModel> packagingItemHistoryLineModels = new List<PackagingItemHistoryLineModel>();
            if (packagingItemHistoryLine != null)
            {

                packagingItemHistoryLine.ForEach(s =>
                {
                    PackagingItemHistoryLineModel packagingItemHistoryLineModel = new PackagingItemHistoryLineModel
                    {


                        PackagingItemHistoryLineId = s.PackagingItemHistoryLineId,
                        PackagingItemHistoryId = s.PackagingItemHistoryId,
                        ItemId = s.ItemId,
                        Description1 = s.Item != null ? s.Item.Description : "",
                        Description2 = s.Item != null ? s.Item.Description2 : "",
                        InternalReferenceNo = s.Item != null ? s.Item.InternalRef : "",
                        PkgNo = s.Item != null ? s.Item.No : "",
                        FileSessionId = s.FileSessionId,
                        ImageSessionId = s.ImageSessionId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    packagingItemHistoryLineModels.Add(packagingItemHistoryLineModel);
                });
            }
            return packagingItemHistoryLineModels;
        }




        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingItemHistoryLine")]
        public PackagingItemHistoryLineModel InsertPackagingItemHistoryLine(PackagingItemHistoryLineModel value)
        {
            var SessionId1 = Guid.NewGuid();
            var SessionId2 = Guid.NewGuid();
            var packagingItemHistoryLine = new PackagingItemHistoryLine
            {
                PackagingItemHistoryId = value.PackagingItemHistoryId,
                ItemId = value.ItemId,
                FileSessionId = SessionId1,
                ImageSessionId = SessionId2,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.PackagingItemHistoryLine.Add(packagingItemHistoryLine);
            _context.SaveChanges();
            value.FileSessionId = SessionId1;
            value.ImageSessionId = SessionId2;
            value.PackagingItemHistoryLineId = packagingItemHistoryLine.PackagingItemHistoryLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingItemHistoryLine")]
        public PackagingItemHistoryLineModel UpdatePackagingItemHistoryLine(PackagingItemHistoryLineModel value)
        {
            var packagingItemHistoryLine = _context.PackagingItemHistoryLine.SingleOrDefault(p => p.PackagingItemHistoryLineId == value.PackagingItemHistoryLineId);


            packagingItemHistoryLine.ItemId = value.ItemId;
            packagingItemHistoryLine.FileSessionId = value.FileSessionId;
            packagingItemHistoryLine.ImageSessionId = value.ImageSessionId;
            packagingItemHistoryLine.ModifiedByUserId = value.ModifiedByUserID;
            packagingItemHistoryLine.ModifiedDate = DateTime.Now;
            packagingItemHistoryLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingItemHistoryLine")]
        public void DeletePackagingItemHistoryLine(int id)
        {
            var packagingItemHistoryLine = _context.PackagingItemHistoryLine.SingleOrDefault(p => p.PackagingItemHistoryLineId == id);
            if (packagingItemHistoryLine != null)
            {
                var query = string.Format("delete from PackagingHistoryItemLineDocument Where SessionId =" + "'" + "{0}" + "'", packagingItemHistoryLine.FileSessionId);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to update document");
                }
                var querys = string.Format("delete from PackagingHistoryItemLineDocument Where SessionId =" + "'" + "{0}" + "'", packagingItemHistoryLine.ImageSessionId);
                var rowaffecteds = _context.Database.ExecuteSqlRaw(query);
                if (rowaffecteds <= 0)
                {
                    throw new Exception("Failed to update document");
                }
                _context.PackagingItemHistoryLine.Remove(packagingItemHistoryLine);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("UploadPackagingItemHistoryLineDocuments")]
        public IActionResult UploadPackagingHistoryItemLineDocuments(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new PackagingHistoryItemLineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.PackagingHistoryItemLineDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetPackagingItemHistoryLineDocument")]
        public List<PackagingItemHistoryLineDocumentModel> GetPackagingHistoryItemLineDocument(int? id)
        {
            var packagingItemHistoryLine = _context.PackagingItemHistoryLine.Where(w => w.PackagingItemHistoryLineId == id).Select(s => s.FileSessionId).FirstOrDefault();
            var query = _context.PackagingHistoryItemLineDocument.Select(s => new PackagingItemHistoryLineDocumentModel
            {
                SessionId = s.SessionId,
                PackagingHistoryItemLineDocumentId = s.PackagingHistoryItemLineDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == packagingItemHistoryLine).OrderByDescending(o => o.PackagingHistoryItemLineDocumentId).AsNoTracking().ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadPackagingItemHistoryLineDocument")]
        public IActionResult DownLoadPackagingHistoryItemLineDocument(long id)
        {
            var document = _context.PackagingHistoryItemLineDocument.SingleOrDefault(t => t.PackagingHistoryItemLineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeletePackagingItemHistoryLineDocument")]
        public void DeletePackagingHistoryItemLineDocument(int id)
        {

            var query = string.Format("delete from PackagingHistoryItemLineDocument Where PackagingHistoryItemLineDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }

        [HttpGet]
        [Route("GetPackagingHistoryLineDocumentById")]
        public PackagingItemHistoryLineDocumentModel GetPackagingLineDocumentById(long id)
        {

            PackagingItemHistoryLineDocumentModel LineDocumentModel = new PackagingItemHistoryLineDocumentModel();
            var document = _context.PackagingHistoryItemLineDocument.FirstOrDefault(t => t.PackagingHistoryItemLineDocumentId == id);
            if (document != null)
            {


                LineDocumentModel.PackagingHistoryItemLineDocumentId = document.PackagingHistoryItemLineDocumentId;
                LineDocumentModel.SessionId = document.SessionId;
                LineDocumentModel.ContentType = document.ContentType;
                LineDocumentModel.FileName = document.FileName;
                LineDocumentModel.FileData = document.FileData;


            }
            return LineDocumentModel;
        }




        [HttpPost]
        [Route("UploadPackagingItemHistoryAttachments")]
        public IActionResult UploadPackagingItemHistoryAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenID = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new PackagingHistoryItemLineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenID
                };

                _context.PackagingHistoryItemLineDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetPackagingItemHistoryAttachmentBySessionID")]
        public List<DocumentsModel> GetPackagingItemHistoryAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.PackagingHistoryItemLineDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.PackagingHistoryItemLineDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadPackagingItemHistoryAttachment")]
        public IActionResult DownLoadPackagingItemHistoryAttachment(long id)
        {
            var document = _context.PackagingHistoryItemLineDocument.SingleOrDefault(t => t.PackagingHistoryItemLineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeletePackagingItemHistoryAttachment")]
        public void DeletePackagingItemHistoryAttachment(int id)
        {

            var query = string.Format("delete from PackagingHistoryItemLineDocument Where PackagingHistoryItemLineDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }






        //


    }
}