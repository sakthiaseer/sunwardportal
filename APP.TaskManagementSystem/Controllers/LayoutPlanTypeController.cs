﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class LayoutPlanTypeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public LayoutPlanTypeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetLayoutPlanTypes")]
        public List<LayoutPlanTypeModel> Get()
        {
            List<LayoutPlanTypeModel> layoutPlanTypeModels = new List<LayoutPlanTypeModel>();
            var LayoutPlanType = _context.LayoutPlanType.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.LayoutPlanTypeId).AsNoTracking().ToList();
            LayoutPlanType.ForEach(s =>
            {
                LayoutPlanTypeModel layoutPlanTypeModel = new LayoutPlanTypeModel
                {
                    LayoutPlanTypeID = s.LayoutPlanTypeId,
                    Name = s.Name,
                    Description = s.Description,
                    LayoutTypeID = s.LayoutPlanTypeId,
                    VersionNo = s.VersionNo,
                    EffectiveDate = s.EffectiveDate.Value,
                    SessionID = s.SessionId,
                    //StatusCode=s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                layoutPlanTypeModels.Add(layoutPlanTypeModel);
            });
           
            return layoutPlanTypeModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get LayoutPlanType")]
        [HttpGet("GetLayoutPlanTypes/{id:int}")]
        public ActionResult<LayoutPlanTypeModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var LayoutPlanType = _context.LayoutPlanType.SingleOrDefault(p => p.LayoutPlanTypeId == id.Value);
            var result = _mapper.Map<LayoutPlanTypeModel>(LayoutPlanType);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<LayoutPlanTypeModel> GetData(SearchModel searchModel)
        {
            var LayoutPlanType = new LayoutPlanType();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).LastOrDefault();
                        break;
                    case "Next":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).LastOrDefault();
                        break;
                    case "Previous":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).LastOrDefault();
                        break;
                    case "Next":
                        LayoutPlanType = _context.LayoutPlanType.OrderBy(o => o.LayoutPlanTypeId).FirstOrDefault(s => s.LayoutPlanTypeId > searchModel.Id);
                        break;
                    case "Previous":
                        LayoutPlanType = _context.LayoutPlanType.OrderByDescending(o => o.LayoutPlanTypeId).FirstOrDefault(s => s.LayoutPlanTypeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<LayoutPlanTypeModel>(LayoutPlanType);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertLayoutPlanType")]
        public LayoutPlanTypeModel Post(LayoutPlanTypeModel value)
        {
            var LayoutPlanType = new LayoutPlanType();
           
                     LayoutPlanType = new LayoutPlanType
                    {
                        //LayoutPlanTypeId = value.LayoutPlanTypeID,
                        Name = value.Name,
                        Description = value.Description,                
                        VersionNo = value.VersionNo,
                        EffectiveDate = value.EffectiveDate,                        
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        //StatusCode=value.StatusCode.Select()
                        //StatusCode = value.Status

                    };

               
          
            _context.LayoutPlanType.Add(LayoutPlanType);
            _context.SaveChanges();
            value.LayoutPlanTypeID = LayoutPlanType.LayoutPlanTypeId;
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateLayoutPlanType")]
        public LayoutPlanTypeModel Put(LayoutPlanTypeModel value)
        {
           
                        var layoutPlanType = _context.LayoutPlanType.SingleOrDefault(p => p.LayoutPlanTypeId == value.LayoutPlanTypeID);
                        if (layoutPlanType != null)
                        {
                        //LayoutPlanType.LayoutPlanTypeId = value.LayoutPlanTypeID;
                        layoutPlanType.ModifiedByUserId = value.ModifiedByUserID;
                            layoutPlanType.ModifiedDate = DateTime.Now;
                            layoutPlanType.Name = value.Name;
                            layoutPlanType.LayoutPlanTypeId = value.LayoutTypeID.Value;
                            layoutPlanType.EffectiveDate = value.EffectiveDate;
                            layoutPlanType.Description = value.Description;
                            layoutPlanType.SessionId = value.SessionID;
                            layoutPlanType.VersionNo = value.VersionNo;
                            layoutPlanType.StatusCodeId = value.StatusCodeID.Value;

                        }
                   
           
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteICTLayoutPlanType")]
        public void Delete(int id)
        {
            var LayoutPlanType = _context.IctlayoutPlanTypes.SingleOrDefault(p => p.IctlayoutTypeId == id);
            if (LayoutPlanType != null)
            {
                _context.IctlayoutPlanTypes.Remove(LayoutPlanType);
                _context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("DeleteLayoutPlanType")]
        public void DeleteLayout(int id)
        {
            var LayoutPlanType = _context.LayoutPlanType.SingleOrDefault(p => p.LayoutPlanTypeId == id);
            if (LayoutPlanType != null)
            {
                _context.LayoutPlanType.Remove(LayoutPlanType);
                _context.SaveChanges();
            }
        }
    }
}