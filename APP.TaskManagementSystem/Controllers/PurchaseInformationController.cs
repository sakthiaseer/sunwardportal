﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PurchaseInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PurchaseInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPurchaseInformation")]
        public List<PurchaseInformationModel> Get()
        {
            List<PurchaseInformationModel> purchaseInformationModels = new List<PurchaseInformationModel>();
            var purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).AsNoTracking().ToList();
            if(purchaseInfo!=null && purchaseInfo.Count>0)
            {
                purchaseInfo.ForEach(s =>
                {
                    PurchaseInformationModel purchaseInformationModel = new PurchaseInformationModel
                    {
                        PurchaseInformationID = s.PurchaseInformationId,
                        Purpose = s.Purpose,
                        DepartmentInCharge = s.DepartmentInCharge,
                        DateOfPurchase = s.DateOfPurchase,
                        Vendor = s.Vendor,
                        DeliveryDate = s.DeliveryDate,
                        InvoiceNo = s.InvoiceNo,
                        Price = s.Price,
                        
                    };
                    purchaseInformationModels.Add(purchaseInformationModel);
                });               

            }
           
            return purchaseInformationModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetPurchaseInformation/{id:int}")]
        public ActionResult<PurchaseInformationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var purchaseInfo = _context.PurchaseInfo.SingleOrDefault(p => p.PurchaseInformationId == id.Value);
            var result = _mapper.Map<PurchaseInformationModel>(purchaseInfo);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PurchaseInformationModel> GetData(SearchModel searchModel)
        {
            var purchaseInfo = new PurchaseInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseInfo = _context.PurchaseInfo.OrderBy(o => o.PurchaseInformationId).FirstOrDefault(s => s.PurchaseInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        purchaseInfo = _context.PurchaseInfo.OrderByDescending(o => o.PurchaseInformationId).FirstOrDefault(s => s.PurchaseInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PurchaseInformationModel>(purchaseInfo);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPurchaseInformation")]
        public PurchaseInformationModel Post(PurchaseInformationModel value)
        {
            var purchaseInfo = new PurchaseInfo
            {
                Purpose = value.Purpose,
                DepartmentInCharge = value.DepartmentInCharge,
                DateOfPurchase = value.DateOfPurchase,
                Vendor = value.Vendor,
                DeliveryDate = value.DeliveryDate,
                InvoiceNo = value.InvoiceNo,
                Price = value.Price,

                //ContactActivities = new List<ContactActivities>(),

            };


            _context.PurchaseInfo.Add(purchaseInfo);
            _context.SaveChanges();
            value.PurchaseInformationID = purchaseInfo.PurchaseInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePurchaseInformation")]
        public PurchaseInformationModel Put(PurchaseInformationModel value)
        {
            var purchaseInfo = _context.PurchaseInfo.SingleOrDefault(p => p.PurchaseInformationId == value.PurchaseInformationID);

            purchaseInfo.Purpose = value.Purpose;
            purchaseInfo.DepartmentInCharge = value.DepartmentInCharge;
            purchaseInfo.DateOfPurchase = value.DateOfPurchase;
            purchaseInfo.Vendor = value.Vendor;
            purchaseInfo.DeliveryDate = value.DeliveryDate;
            purchaseInfo.InvoiceNo = value.InvoiceNo;
            purchaseInfo.Price = value.Price;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePurchaseInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var purchaseInfo = _context.PurchaseInfo.SingleOrDefault(p => p.PurchaseInformationId == id);
                if (purchaseInfo != null)
                {
                    _context.PurchaseInfo.Remove(purchaseInfo);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}