﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskAssignedController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskAssignedController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskAssigned")]
        public ActionResult<TaskAssignedModel> Get(int id, int userId)
        {

            var taskAssigned = _context.TaskAssigned.SingleOrDefault(t => t.TaskId == id && t.UserId == userId);
            var result = _mapper.Map<TaskAssignedModel>(taskAssigned);
            return result;


        }

        // GET: api/Project/2
        [HttpGet("GetTaskAssigned/{id:int}")]
        //public ActionResult<TaskNotesModel> Get(int? id)
        //{
        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //var DiscussionNotes = _context.DiscussionNotes.SingleOrDefault(p => p.TaskNotesId == id.Value);
        //    //var result = _mapper.Map<TaskNotesModel>(DiscussionNotes);
        //    //if (result == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    return result;
        //}


        // POST: api/User
        [HttpPost]
        [Route("InsertTaskAssigned")]
        public TaskAssigned Post(TaskAssigned value)
        {

            
            var taskAssigned = _context.TaskAssigned.Where(p => p.TaskId == value.TaskId && p.UserId == value.UserId).FirstOrDefault();
            if (taskAssigned == null)
            {
                taskAssigned = new TaskAssigned
                {

                    TaskId = value.TaskId,
                    TaskOwnerId = value.TaskOwnerId,
                    AssignedDate = DateTime.Now,
                    StartDate = value.StartDate,
                    DueDate = value.DueDate,
                    CompletedDate = value.CompletedDate,
                    UserId = value.UserId,
                    StatusCodeId = value.StatusCodeId,
                    

                };
                //    _context.DiscussionNotes.Add(DiscussionNotes);
                //    _context.SaveChanges();
                //    value.TaskNotesID = DiscussionNotes.TaskNotesId;
            }
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskAssigned")]
        public TaskAssigned Put(TaskAssigned value)
        {
           
            var taskAssigned = _context.TaskAssigned.SingleOrDefault(p => p.TaskAssignedId == value.TaskAssignedId);
            //  folders.TaskMemberId = value.TaskMemberID;
            taskAssigned.TaskId = value.TaskId;
            taskAssigned.UserId = value.UserId;
            taskAssigned.TaskOwnerId = value.TaskOwnerId;
            taskAssigned.StartDate = value.StartDate;
            taskAssigned.CompletedDate = value.CompletedDate;
            taskAssigned.StatusCodeId = value.StatusCodeId;
            taskAssigned.DueDate = value.DueDate;
            taskAssigned.AssignedDate = value.DueDate;


            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UpdateTaskAssignCC")]
        public TaskAssignedModel UpdateTaskAssign(TaskAssignedModel value)
        {

            var taskmaster = _context.TaskMaster.Where(t => t.TaskId == value.TaskID).FirstOrDefault();
            //if(taskmaster !=null )
            //{
            //    taskmaster.AssignedTo = value.UserID;
            //}
            var taskmembers = _context.TaskMembers.Where(l => l.TaskId == value.TaskID && l.AssignedCc == value.UserID).ToList();
            if (taskmembers.Count > 0)
            {
                _context.TaskMembers.RemoveRange(taskmembers);
                _context.SaveChanges();
            }
            var taskAssigned = _context.TaskAssigned.Where(p => p.TaskId == value.TaskID && p.UserId == value.UserID).FirstOrDefault();
            if (taskAssigned == null)
            {
                try
                {
                    taskAssigned = new TaskAssigned
                    {

                        TaskId = value.TaskID,
                        TaskOwnerId = value.TaskOwnerID,
                        AssignedDate = value.AssignedDate,
                        StartDate = value.StartDate,
                        DueDate = value.DueDate,
                        CompletedDate = value.CompletedDate,
                        UserId = value.UserID,
                        StatusCodeId = 512,


                    };
                    taskmaster.TaskAssigned.Add(taskAssigned);
                    _context.SaveChanges();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                
                //    _context.DiscussionNotes.Add(DiscussionNotes);
                //    _context.SaveChanges();
                //    value.TaskNotesID = DiscussionNotes.TaskNotesId;
            }
            
            return value;

        }
    }
}