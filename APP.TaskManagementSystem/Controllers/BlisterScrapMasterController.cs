﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterScrapMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public BlisterScrapMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetBlisterScrapMasters")]
        public List<BlisterScrapMasterModel> Get()
        {
            var blisterScrapMaster = _context.BlisterScrapMaster.Include("AddedByUser").Include("ModifiedByUser").Select(s => new BlisterScrapMasterModel
            {
                BlisterScrapId = s.BlisterScrapId,
                BlisterComponent = s.BlisterComponent,
                Description = s.Description,
                Description1 = s.Description1,
                Sublot = s.Sublot,
                BatchSize = s.BatchSize,
                PackSize = s.PackSize,
                Bomtype = s.Bomtype,
                Uom = s.Uom,
                ScrapUom = s.ScrapUom,
                StatusCodeId = s.StatusCodeId,
                ModifiedByUserID = s.AddedByUserId,
                AddedByUserID = s.ModifiedByUserId,
                StatusCodeID = s.StatusCodeId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.BlisterScrapId).AsNoTracking().ToList();
            //var result = _mapper.Map<List<BlisterScrapMasterModel>>(blisterScrapMaster);
            return blisterScrapMaster;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get BlisterScrapMaster")]
        [HttpGet("GetBlisterScrapMasters/{id:int}")]
        public ActionResult<BlisterScrapMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var blisterScrapMaster = _context.BlisterScrapMaster.SingleOrDefault(p => p.BlisterScrapId == id.Value);
            var result = _mapper.Map<BlisterScrapMasterModel>(blisterScrapMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterScrapMasterModel> GetData(SearchModel searchModel)
        {
            var blisterScrapMaster = new BlisterScrapMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderBy(o => o.BlisterScrapId).FirstOrDefault(s => s.BlisterScrapId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterScrapMaster = _context.BlisterScrapMaster.OrderByDescending(o => o.BlisterScrapId).FirstOrDefault(s => s.BlisterScrapId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlisterScrapMasterModel>(blisterScrapMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBlisterScrapMaster")]
        public BlisterScrapMasterModel Post(BlisterScrapMasterModel value)
        {
            var blisterScrapMaster = new BlisterScrapMaster
            {
                BlisterComponent = value.BlisterComponent,
                Description = value.Description,
                Description1 = value.Description1,
                Sublot = value.Sublot,
                BatchSize = value.BatchSize,
                PackSize = value.PackSize,
                Bomtype = value.Bomtype,
                Uom = value.Uom,
                ScrapUom = value.ScrapUom,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                BlisterScrapLine = new List<BlisterScrapLine>(),
                BlisterAllowable = new List<BlisterAllowable>(),
            };
            value.BlisterScrapLine.ToList().ForEach(l =>
            {
                var blisterline = new BlisterScrapLine
                {
                    AddonQty = l.AddonQty,
                    CalculationType = l.CalculationType,
                    Description = l.Description,
                    Inheritance = l.Inheritance,
                    ProdOrderNo = l.ProdOrderNo,
                    Qcinheritance = l.QcInheritance,
                    QtyPer = l.QtyPer,
                    Type = l.TypeText,
                    ScrapPercentage = l.ScrapPercentage,
                    Uom = l.Uom
                };
                blisterScrapMaster.BlisterScrapLine.Add(blisterline);
            });
            value.BlisterAllowable.ToList().ForEach(l =>
            {
                var blisterline = new BlisterAllowable
                {
                    AttachedlayoutPlan = l.AttachedlayoutPlan,
                    BlisterScrapId = value.BlisterScrapId,
                    BlisterScrapTypeId = l.BlisterScrapTypeId,
                    BlisterType = l.BlisterType,
                    BlisterValue = l.BlisterValue,
                    MachineCode = l.MachineCode,
                    MachineId = l.MachineId,
                    NoOfCuts = l.NoOfCuts,

                };
                blisterScrapMaster.BlisterAllowable.Add(blisterline);
            });
            _context.BlisterScrapMaster.Add(blisterScrapMaster);
            _context.SaveChanges();
            value.BlisterScrapId = blisterScrapMaster.BlisterScrapId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBlisterScrapMaster")]
        public BlisterScrapMasterModel Put(BlisterScrapMasterModel value)
        {
            var blisterScrapMaster = _context.BlisterScrapMaster.SingleOrDefault(p => p.BlisterScrapId == value.BlisterScrapId);
            blisterScrapMaster.ModifiedByUserId = value.ModifiedByUserID;
            blisterScrapMaster.ModifiedDate = DateTime.Now;
            blisterScrapMaster.BlisterComponent = value.BlisterComponent;
            blisterScrapMaster.Description = value.Description;
            blisterScrapMaster.Description1 = value.Description1;
            blisterScrapMaster.Sublot = value.Sublot;
            blisterScrapMaster.BatchSize = value.BatchSize;
            blisterScrapMaster.PackSize = value.PackSize;
            blisterScrapMaster.Bomtype = value.Bomtype;
            blisterScrapMaster.Uom = value.Uom;
            blisterScrapMaster.ScrapUom = value.ScrapUom;
            blisterScrapMaster.StatusCodeId = value.StatusCodeID.Value;

            var blisterLines = _context.BlisterScrapLine.Where(l => l.BlisterScrapId == value.BlisterScrapId).ToList();
            if (blisterLines.Count > 0)
            {
                _context.BlisterScrapLine.RemoveRange(blisterLines);
                _context.SaveChanges();
            }
            var blisterAllowable = _context.BlisterAllowable.Where(l => l.BlisterScrapId == value.BlisterScrapId).ToList();
            if (blisterAllowable.Count > 0)
            {
                _context.BlisterAllowable.RemoveRange(blisterAllowable);
                _context.SaveChanges();
            }

            value.BlisterScrapLine.ToList().ForEach(l =>
            {
                var blisterline = new BlisterScrapLine
                {
                    AddonQty = l.AddonQty,
                    BlisterScrapId = value.BlisterScrapId,
                    CalculationType = l.CalculationType,
                    Description = l.Description,
                    Inheritance = l.Inheritance,
                    ProdOrderNo = l.ProdOrderNo,
                    Qcinheritance = l.QcInheritance,
                    QtyPer = l.QtyPer,
                    Type = l.TypeText,
                    ScrapPercentage = l.ScrapPercentage,
                    Uom = l.Uom
                };
                blisterScrapMaster.BlisterScrapLine.Add(blisterline);
            });

            value.BlisterAllowable.ToList().ForEach(l =>
            {
                var blisterline = new BlisterAllowable
                {
                     AttachedlayoutPlan =l.AttachedlayoutPlan,
                     BlisterScrapId =value.BlisterScrapId,
                     BlisterScrapTypeId =l.BlisterScrapTypeId,
                     BlisterType =l.BlisterType,
                     BlisterValue =l.BlisterValue,
                     MachineCode =l.MachineCode,
                     MachineId =l.MachineId,
                     NoOfCuts =l.NoOfCuts,
                     
                };
                blisterScrapMaster.BlisterAllowable.Add(blisterline);
            });

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBlisterScrapMaster")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var blisterScrapMaster = _context.BlisterScrapMaster.SingleOrDefault(p => p.BlisterScrapId == id);
                if (blisterScrapMaster != null)
                {
                    _context.BlisterScrapMaster.Remove(blisterScrapMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}