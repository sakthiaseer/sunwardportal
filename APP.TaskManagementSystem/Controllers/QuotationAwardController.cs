﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class QuotationAwardController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly TableVersionRepository _repository;
        public QuotationAwardController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
            _repository = repository;
        }
        [HttpGet]
        [Route("GetQuotationAward")]
        public async Task<IEnumerable<TableDataVersionInfoModel<QuotationAwardModel>>> GetExchangeVersion(string sessionID)
        {
            return await _repository.GetList<QuotationAwardModel>(sessionID);
        }
        [HttpGet]
        [Route("GetQuotationAwards")]
        public List<QuotationAwardModel> Get(long? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            var quotationHistories = _context.QuotationAward
                .Include(s => s.Item)
                .Include(s => s.TypeOfOrder)
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .OrderByDescending(a => a.QuotationAwardId).AsNoTracking()
                .Where(s => s.QuotationHistoryId == id)
                .ToList();
            List<QuotationAwardModel> QuotationAwardModels = new List<QuotationAwardModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    QuotationAwardModel QuotationAwardModel = new QuotationAwardModel
                    {
                        QuotationAwardId = s.QuotationAwardId,
                        TypeOfOrderId = s.TypeOfOrderId,
                        ItemId = s.ItemId,
                        NavisionNo = s.Item?.Code,
                        QuotationHistoryId = s.QuotationHistoryId,
                        TypeOfOrder = s.TypeOfOrder?.Value,
                        TotalQty = s.TotalQty,
                        IsCommitment = s.IsCommitment,
                        CommittedOn = s.CommittedOn,
                        CommittedQty = s.CommittedQty,
                        IsFollowDate = s.IsFollowDate,
                        SessionId = s.SessionId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser?.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        StatusCode = s.StatusCode?.CodeValue,
                        Price = s.Price,
                        IsDifferentPrice = s.IsDifferentPrice,
                        IsDifferentQty = s.IsDifferentQty,
                        ReasonForDifferentPrice = s.ReasonForDifferentPrice,
                        ReasonForDifferentQty = s.ReasonForDifferentQty,
                    };
                    QuotationAwardModels.Add(QuotationAwardModel);
                });
            }
            return QuotationAwardModels;
        }

        [HttpPost()]
        [Route("GetQuotationAwardData")]
        public ActionResult<QuotationAwardModel> GetQuotationAwardData(SearchModel searchModel)
        {
            var quotationAward = new QuotationAward();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).LastOrDefault();
                        break;
                    case "Next":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).LastOrDefault();
                        break;
                    case "Previous":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).LastOrDefault();
                        break;
                    case "Next":
                        quotationAward = _context.QuotationAward.OrderBy(o => o.QuotationAwardId).FirstOrDefault(s => s.QuotationAwardId > searchModel.Id);
                        break;
                    case "Previous":
                        quotationAward = _context.QuotationAward.OrderByDescending(o => o.QuotationAwardId).FirstOrDefault(s => s.QuotationAwardId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<QuotationAwardModel>(quotationAward);
            return result;
        }

        [HttpPost]
        [Route("InsertQuotationAward")]
        public QuotationAwardModel Post(QuotationAwardModel value)
        {
            var SessionId = Guid.NewGuid();
            var QuotationAward = new QuotationAward
            {
                TypeOfOrderId = value.TypeOfOrderId,
                ItemId = value.ItemId,
                TotalQty = value.TotalQty,
                QuotationHistoryId = value.QuotationHistoryId,
                IsCommitment = value.IsCommitment,
                CommittedOn = value.CommittedOn,
                CommittedQty = value.CommittedQty,
                IsFollowDate = value.IsFollowDate,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                Price = value.Price,
                ReasonForDifferentPrice = value.ReasonForDifferentPrice,
                ReasonForDifferentQty = value.ReasonForDifferentQty,
                IsDifferentPrice = value.IsDifferentPrice,
                IsDifferentQty = value.IsDifferentQty,
            };
            _context.QuotationAward.Add(QuotationAward);
            _context.SaveChanges();
            value.SessionId = SessionId;
            if (value.AddedByUserID != null)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID).FirstOrDefault()?.UserName;
            }
            if (value.ItemId > 0)
            {
                value.NavisionNo = _context.GenericCodes.Where(s => s.GenericCodeId == value.ItemId)?.FirstOrDefault()?.Code;
            }
            if (value.TypeOfOrderId > 0)
            {
                value.TypeOfOrder = _context.CodeMaster.Where(s => s.CodeId == value.TypeOfOrderId).FirstOrDefault()?.CodeValue;
            }
            if (value.ItemId > 0)
            {
                var quotationLines = _context.QuotationHistoryLine.Where(s => s.QutationHistoryId == value.QuotationHistoryId && s.ProductId == value.ItemId).ToList();
                if (quotationLines != null && quotationLines.Count > 0)
                {
                    quotationLines.ForEach(s => {
                        s.IsAwarded = true;
                        _context.SaveChanges();
                    });
                }
            }
            value.QuotationAwardId = QuotationAward.QuotationAwardId;
            return value;
        }
        [HttpPut]
        [Route("UpdateQuotationAward")]
        public async Task<QuotationAwardModel> Put(QuotationAwardModel value)
        {

            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var QuotationAward = _context.QuotationAward.SingleOrDefault(p => p.QuotationAwardId == value.QuotationAwardId);
            if (QuotationAward != null)
            {
                if (value.SaveVersionData)
                {
                    value.QuotationAwardLineModels = GetQuotationAwardLine((int)value.QuotationAwardId);
                    var versionInfo = new TableDataVersionInfoModel<QuotationAwardLineModel>
                    {
                        JsonData = JsonSerializer.Serialize(value),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        StatusCodeID = value.StatusCodeID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "QuotationAward",
                        PrimaryKey = value.QuotationAwardId,
                    };
                    await _repository.Insert(versionInfo);
                }
                QuotationAward.TypeOfOrderId = value.TypeOfOrderId;
                QuotationAward.ItemId = value.ItemId;
                QuotationAward.CommittedQty = value.CommittedQty;
                QuotationAward.IsCommitment = value.IsCommitment;
                QuotationAward.CommittedOn = value.CommittedOn;
                QuotationAward.IsFollowDate = value.IsFollowDate;
                QuotationAward.TotalQty = value.TotalQty;
                QuotationAward.SessionId = value.SessionId;
                QuotationAward.StatusCodeId = value.StatusCodeID.Value;
                QuotationAward.ModifiedByUserId = value.ModifiedByUserID;
                QuotationAward.ModifiedDate = DateTime.Now;
                QuotationAward.QuotationHistoryId = value.QuotationHistoryId;
                QuotationAward.Price = value.Price;
                QuotationAward.ReasonForDifferentQty = value.ReasonForDifferentQty;
                QuotationAward.ReasonForDifferentPrice = value.ReasonForDifferentPrice;
                QuotationAward.IsDifferentQty = value.IsDifferentQty;
                QuotationAward.IsDifferentPrice = value.IsDifferentPrice;
                if (value.ItemId > 0)
                {
                    var quotationLines = _context.QuotationHistoryLine.Where(s => s.QutationHistoryId == value.QuotationHistoryId && s.ProductId == value.ItemId).ToList();
                    if (quotationLines != null && quotationLines.Count > 0)
                    {
                        quotationLines.ForEach(s => {
                            s.IsAwarded = true;
                            _context.SaveChanges();
                        });
                    }
                }
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteQuotationAward")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var QuotationAward = _context.QuotationAward.Where(p => p.QuotationAwardId == id).FirstOrDefault();
                if (QuotationAward != null)
                {

                    _context.QuotationAward.Remove(QuotationAward);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        [Route("GetQuotationAwardLine")]
        public List<QuotationAwardLineModel> GetQuotationAwardLine(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var QuotationAwardLines = _context.QuotationAwardLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(w => w.QuotatonAwardId == id).OrderByDescending(a => a.QuotationAwardLineId).AsNoTracking().ToList();
            var productGroupings = _context.GenericCodes.AsNoTracking().ToList();
            List<QuotationAwardLineModel> QuotationAwardLineModels = new List<QuotationAwardLineModel>();
            if (QuotationAwardLines != null)
            {
                QuotationAwardLines.ForEach(s =>
                {
                    QuotationAwardLineModel QuotationAwardLineModel = new QuotationAwardLineModel
                    {
                        QuotationAwardLineId = s.QuotationAwardLineId,
                        QuotationAwardId = s.QuotatonAwardId,
                        QtyPerShipment = s.QtyPerShipment,
                        ShipmentDate = s.ShipmentDate,
                        SessionId = s.SessionId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    QuotationAwardLineModels.Add(QuotationAwardLineModel);
                });
            }
            if (QuotationAwardLineModels != null && QuotationAwardLineModels.Count > 0)
            {
                QuotationAwardLineModels.ForEach(q =>
                {
                    q.ExistingQty = QuotationAwardLineModels.Sum(s => s.QtyPerShipment);
               

                });
               
               
            }
            return QuotationAwardLineModels;
        }

        [Route("GetQuotationHistoryLineByProduct")]
        public QuotationHistoryLineModel GetQuotationHistoryLineByProduct(int? id)
        {
            QuotationHistoryLineModel quotationHistoryLineModel = new QuotationHistoryLineModel();
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var quotationHistoryLine = _context.QuotationHistoryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.Product).Where(w => w.ProductId == id).OrderByDescending(a => a.QuotationHistoryLineId).AsNoTracking().FirstOrDefault();
            var productGroupings = _context.GenericCodes.AsNoTracking().ToList();
            List<QuotationHistoryLineModel> quotationHistoryLineModels = new List<QuotationHistoryLineModel>();
            if (quotationHistoryLine != null)
            {

                quotationHistoryLineModel = new QuotationHistoryLineModel
                {
                    QuotationHistoryLineId = quotationHistoryLine.QuotationHistoryLineId,
                    QutationHistoryId = quotationHistoryLine.QutationHistoryId,
                    ProductId = quotationHistoryLine.ProductId,

                    ProductName = quotationHistoryLine.Product?.Code,
                    UOM = applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == quotationHistoryLine.Product?.Uom)?.Value,
                    Productdescription = quotationHistoryLine.Product?.Description,
                    Source = quotationHistoryLine.Source,
                    Quantity = quotationHistoryLine.Quantity,
                    Uomid = quotationHistoryLine.Uomid,
                    PackingId = quotationHistoryLine.PackingId,
                    Packing = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == quotationHistoryLine.PackingId)?.Value : "",
                    OfferCurrencyId = quotationHistoryLine.OfferCurrencyId,
                    OfferCurrency = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == quotationHistoryLine.OfferCurrencyId)?.Value : "",
                    OfferPrice = quotationHistoryLine.OfferPrice,
                    OfferUomid = quotationHistoryLine.OfferUomid,
                    OfferUOM = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == quotationHistoryLine.OfferUomid)?.Value : "",
                    Focqty = quotationHistoryLine.Focqty,
                    ShippingTermsId = quotationHistoryLine.ShippingTermsId,
                    ShippingTerms = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == quotationHistoryLine.ShippingTermsId)?.Value : "",
                    IsTenderExceed = quotationHistoryLine.IsTenderExceed,
                    IsAwarded = quotationHistoryLine.IsAwarded,
                    SessionId = quotationHistoryLine.SessionId,
                    StatusCode = quotationHistoryLine.StatusCode != null ? quotationHistoryLine.StatusCode.CodeValue : "",
                    StatusCodeID = quotationHistoryLine.StatusCodeId,
                    AddedByUserID = quotationHistoryLine.AddedByUserId,
                    ModifiedByUserID = quotationHistoryLine.ModifiedByUserId,
                    AddedByUser = quotationHistoryLine.AddedByUser != null ? quotationHistoryLine.AddedByUser.UserName : "",
                    ModifiedByUser = quotationHistoryLine.ModifiedByUser != null ? quotationHistoryLine.ModifiedByUser.UserName : "",
                    AddedDate = quotationHistoryLine.AddedDate,
                    ModifiedDate = quotationHistoryLine.ModifiedDate,
                    Remarks = quotationHistoryLine.Remarks,
                };


            }
            return quotationHistoryLineModel;
        }

        [HttpPost()]
        [Route("GetQuotationAwardLineData")]
        public ActionResult<QuotationAwardLineModel> GetQuotationAwardLineData(SearchModel searchModel)
        {
            var QuotationAwardLine = new QuotationAwardLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).FirstOrDefault();
                        break;
                    case "Last":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).LastOrDefault();
                        break;
                    case "Next":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).LastOrDefault();
                        break;
                    case "Previous":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).FirstOrDefault();
                        break;
                    case "Last":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).LastOrDefault();
                        break;
                    case "Next":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderBy(o => o.QuotationAwardLineId).FirstOrDefault(s => s.QuotationAwardLineId > searchModel.Id);
                        break;
                    case "Previous":
                        QuotationAwardLine = _context.QuotationAwardLine.OrderByDescending(o => o.QuotationAwardLineId).FirstOrDefault(s => s.QuotationAwardLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<QuotationAwardLineModel>(QuotationAwardLine);
            return result;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertQuotationAwardLine")]
        public QuotationAwardLineModel InsertQuotationAwardLine(QuotationAwardLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var QuotationAwardLine = new QuotationAwardLine
            {

                SessionId = SessionId,
                QuotatonAwardId = value.QuotationAwardId,
                QtyPerShipment = value.QtyPerShipment,
                ShipmentDate = value.ShipmentDate,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.QuotationAwardLine.Add(QuotationAwardLine);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.QuotationAwardLineId = QuotationAwardLine.QuotationAwardLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateQuotationAwardLine")]
        public QuotationAwardLineModel UpdateQuotationAwardLine(QuotationAwardLineModel value)
        {
            var QuotationAwardLine = _context.QuotationAwardLine.SingleOrDefault(p => p.QuotationAwardLineId == value.QuotationAwardLineId);
            QuotationAwardLine.QuotatonAwardId = value.QuotationAwardId;
            QuotationAwardLine.QtyPerShipment = value.QtyPerShipment;
            QuotationAwardLine.ShipmentDate = value.ShipmentDate;
            QuotationAwardLine.SessionId = value.SessionId;
            QuotationAwardLine.ModifiedByUserId = value.ModifiedByUserID;
            QuotationAwardLine.ModifiedDate = DateTime.Now;
            QuotationAwardLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteQuotationAwardLine")]
        public void DeleteQuotationAwardLine(int id)
        {
            var QuotationAwardLine = _context.QuotationAwardLine.SingleOrDefault(p => p.QuotationAwardLineId == id);
            if (QuotationAwardLine != null)
            {
                _context.QuotationAwardLine.Remove(QuotationAwardLine);
                _context.SaveChanges();
            }
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<QuotationAwardModel> CreateVersion(QuotationAwardModel value)
        {
            try
            {
                value.SessionId = value.SessionId.Value;
                var versionData = _context.QuotationAward.Include(s => s.QuotationAwardLine).SingleOrDefault(p => p.QuotationAwardId == value.QuotationAwardId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<QuotationAwardModel>(versionData);
                    verObject.QuotationAwardLineModels = new List<QuotationAwardLineModel>();
                    versionData.QuotationAwardLine.ToList().ForEach(f =>
                    {
                        var priceInfo = _mapper.Map<QuotationAwardLineModel>(f);
                        verObject.QuotationAwardLineModels.Add(priceInfo);
                    });


                    var versionInfo = new TableDataVersionInfoModel<QuotationAwardModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "QuotationAward",
                        PrimaryKey = value.QuotationAwardId,
                        ReferenceInfo = value.ReferenceInfo,
                        StatusCodeID = value.StatusCodeID.Value,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }
        [HttpPut]
        [Route("CheckExitDataVersion")]
        public bool CheckExitDataVersion(QuotationAwardModel value)
        {

            return false;
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<QuotationAwardModel> ApplyVersion(QuotationAwardModel value)
        {
            try
            {
                var versionData = _context.QuotationAward.Include(q => q.QuotationAwardLine).SingleOrDefault(p => p.QuotationAwardId == value.QuotationAwardId);

                if (versionData != null)
                {
                    versionData.ItemId = value.ItemId;
                    versionData.TypeOfOrderId = value.TypeOfOrderId;
                    versionData.TotalQty = value.TotalQty;
                    versionData.CommittedOn = value.CommittedOn;
                    versionData.IsCommitment = value.IsCommitment;
                    versionData.IsFollowDate = value.IsFollowDate;
                    versionData.CommittedQty = value.CommittedQty;
                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.SessionId = value.SessionId;

                    _context.QuotationAwardLine.RemoveRange(versionData.QuotationAwardLine);
                    _context.SaveChanges();
                    if (value.QuotationAwardLineModels != null)
                    {
                        value.QuotationAwardLineModels.ForEach(f =>
                        {
                            var quatationawardLine = new QuotationAwardLine
                            {
                                QuotatonAwardId = versionData.QuotationAwardId,
                                QuotationAwardLineId = f.QuotationAwardLineId,
                                QtyPerShipment = f.QtyPerShipment,
                                ShipmentDate = f.ShipmentDate,
                                StatusCodeId = f.StatusCodeID.Value,
                                AddedByUserId = value.ModifiedByUserID,
                                AddedDate = DateTime.Now,
                                ModifiedByUserId = value.ModifiedByUserID,
                                ModifiedDate = DateTime.Now,

                            };

                            _context.QuotationAwardLine.Add(quatationawardLine);
                        });
                    }
                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(QuotationAwardModel value)
        {

            var versionData = _context.QuotationAward.Include(s => s.QuotationAwardLine).SingleOrDefault(p => p.QuotationAwardId == value.QuotationAwardId);
            if (versionData != null)
            {
                var verObject = _mapper.Map<QuotationAwardModel>(versionData);
                verObject.QuotationAwardLineModels = new List<QuotationAwardLineModel>();
                versionData.QuotationAwardLine.ToList().ForEach(f =>
                {
                    var priceInfo = _mapper.Map<QuotationAwardLineModel>(f);
                    verObject.QuotationAwardLineModels.Add(priceInfo);
                });
                var versionInfo = new TableDataVersionInfoModel<QuotationAwardModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "QuotationAward",
                    PrimaryKey = value.QuotationAwardId,
                    ReferenceInfo = "Temp Version - undo",
                    StatusCodeID = value.StatusCodeID.Value,
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }

        [HttpGet]
        [Route("UndoVersion")]
        public async Task<QuotationAwardModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<QuotationAwardModel>(tableData.JsonData);

                    var versionData = _context.QuotationAward.Include(m => m.QuotationAwardLine).SingleOrDefault(p => p.QuotationAwardId == verObject.QuotationAwardId);


                    if (verObject != null && versionData != null)
                    {
                        versionData.ItemId = verObject.ItemId;
                        versionData.TypeOfOrderId = verObject.TypeOfOrderId;
                        versionData.QuotationAwardId = verObject.QuotationAwardId;
                        versionData.TotalQty = verObject.TotalQty;
                        versionData.CommittedOn = verObject.CommittedOn;
                        versionData.CommittedQty = verObject.CommittedQty;
                        versionData.IsCommitment = verObject.IsCommitment;
                        versionData.IsFollowDate = verObject.IsFollowDate;
                        versionData.StatusCodeId = verObject.StatusCodeID.Value;
                        versionData.ModifiedByUserId = verObject.ModifiedByUserID;
                        versionData.ModifiedDate = DateTime.Now;
                        versionData.SessionId = verObject.SessionId;

                        _context.QuotationAwardLine.RemoveRange(versionData.QuotationAwardLine);
                        _context.SaveChanges();
                        if (verObject.QuotationAwardLineModels != null)
                        {
                            verObject.QuotationAwardLineModels.ForEach(f =>
                            {
                                var quotationawardline = new QuotationAwardLine
                                {
                                    QuotatonAwardId = versionData.QuotationAwardId,
                                    QtyPerShipment = f.QtyPerShipment,
                                    QuotationAwardLineId = f.QuotationAwardLineId,
                                    ShipmentDate = f.ShipmentDate,
                                    StatusCodeId = f.StatusCodeID.Value,
                                    AddedByUserId = f.ModifiedByUserID,
                                    AddedDate = DateTime.Now,
                                    ModifiedByUserId = f.ModifiedByUserID,
                                    ModifiedDate = DateTime.Now,

                                };

                                _context.QuotationAwardLine.Add(quotationawardline);

                            });
                        }
                        _context.SaveChanges();
                    }
                    return verObject;
                }
                return new QuotationAwardModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                var TempVersion = _context.TempVersion.Where(w => w.VersionId == tableData.VersionTableId).ToList();
                if (TempVersion != null)
                {
                    _context.TempVersion.RemoveRange(TempVersion);
                    _context.SaveChanges();
                }
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }

    }
}
