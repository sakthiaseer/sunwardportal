using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UnitConversionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public UnitConversionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetUnitConversion")]
        public List<UnitConversionModel> Get()
        {

            List<UnitConversionModel> unitConversionModels = new List<UnitConversionModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            var unitConversionslist = _context.UnitConversion.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.UnitConversionId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel

            if (unitConversionslist != null && unitConversionslist.Count > 0)
            {
                unitConversionslist.ForEach(s =>
                {
                    UnitConversionModel unitConversionModel = new UnitConversionModel
                    {

                        UnitConversionID = s.UnitConversionId,
                        UnitNumber = s.UnitNumber,
                        Units = s.Units,
                        UnitsName1 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",
                        IsEqualValue = s.IsEqualValue,
                        Units1 = s.Units1,
                        UnitsName2 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units1).Select(a => a.Value).SingleOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser!=null?s.AddedByUser.UserName:"",
                        ModifiedByUser = s.ModifiedByUser!=null?s.ModifiedByUser.UserName:"",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate


                    };
                    unitConversionModels.Add(unitConversionModel);


                });
            }

            //var result = _mapper.Map<List<StateModel>>(State);
            return unitConversionModels;
        }
        [HttpGet]
        [Route("GetApplicationMasterDetailByType")]
        public List<ApplicationMasterDetails> GetApplicationMasterDetailByType()
        {
            var codeMasterDetails = new List<ApplicationMasterDetails>();
            var applicationmasterIds = new List<long?>() { 176, 202 };
            var applicationMasters = _context.ApplicationMaster.Where(a => applicationmasterIds.Contains(a.ApplicationMasterCodeId)).AsNoTracking().Select(m => m.ApplicationMasterId).ToList();
            if (applicationMasters != null)
            {
                codeMasterDetails = _context.ApplicationMasterDetail.Select(s => new ApplicationMasterDetails
                {
                    Units = s.ApplicationMasterDetailId,
                    Units1 = s.ApplicationMasterDetailId,
                    ApplicationMasterID = s.ApplicationMasterId,
                    UnitsName1 = s.Value,
                }).Where(d => applicationMasters.Contains(d.ApplicationMasterID)).OrderBy(o => o.UnitsName1).AsNoTracking().ToList();
            }
            return codeMasterDetails;

        }






        [HttpPost()]
        [Route("GetData")]
        public ActionResult<UnitConversionModel> GetData(SearchModel searchModel)
        {
            //var UnitConversion = new UnitConversion();
            var unitConversion = new UnitConversion();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).FirstOrDefault();
                        break;
                    case "Last":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).LastOrDefault();
                        break;
                    case "Next":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).LastOrDefault();
                        break;
                    case "Previous":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).FirstOrDefault();
                        break;
                    case "Last":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).LastOrDefault();
                        break;
                    case "Next":
                        unitConversion = _context.UnitConversion.OrderBy(o => o.UnitConversionId).FirstOrDefault(s => s.UnitConversionId > searchModel.Id);
                        break;
                    case "Previous":
                        unitConversion = _context.UnitConversion.OrderByDescending(o => o.UnitConversionId).FirstOrDefault(s => s.UnitConversionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<UnitConversionModel>(unitConversion);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertUnitConversion")]
        public UnitConversionModel Post(UnitConversionModel value)
        {
            var UnitConversion = new UnitConversion
            {
                UnitNumber = value.UnitNumber,
                Units = value.Units,
                IsEqualValue = value.IsEqualValue,
                Units1 = value.Units1,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.UnitConversion.Add(UnitConversion);
            _context.SaveChanges();
            value.UnitConversionID = UnitConversion.UnitConversionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateUnitConversion")]
        public UnitConversionModel Put(UnitConversionModel value)
        {
            var unitConversion = _context.UnitConversion.SingleOrDefault(p => p.UnitConversionId == value.UnitConversionID);

            unitConversion.UnitNumber = value.UnitNumber;
            unitConversion.Units = value.Units;
            unitConversion.IsEqualValue = value.IsEqualValue;
            unitConversion.Units1 = value.Units1;
            unitConversion.ModifiedByUserId = value.ModifiedByUserID;
            unitConversion.ModifiedDate = DateTime.Now;
            unitConversion.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteUnitConversion")]
        public void Delete(int id)
        {
            var UnitConversion = _context.UnitConversion.SingleOrDefault(p => p.UnitConversionId == id);
            if (UnitConversion != null)
            {

                _context.UnitConversion.Remove(UnitConversion);
                _context.SaveChanges();
            }
        }
    }
}