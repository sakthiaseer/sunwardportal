﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DesignationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DesignationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDesignations")]
        public List<DesignationModel> Get()
        {
            var designation = _context.Designation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(s=>s.SubSectionT)
                .Include(s=>s.SubSectionT.SubSecton)
                .Include(s => s.SubSectionT.SubSecton.Section)
                .Include(s => s.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Include(l => l.Level).AsNoTracking().ToList();
            List<DesignationModel> designationModel = new List<DesignationModel>();
            designation.ForEach(s =>
            {
                DesignationModel designationModels = new DesignationModel();
                {
                    designationModels.DesignationID = s.DesignationId;
                    designationModels.LevelID = s.LevelId;
                    designationModels.SubSectionTID = s.SubSectionTid;
                    designationModels.CompanyID = s.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.PlantId;
                    designationModels.LevelName = s.Level?.Name;
                    designationModels.Name = s.Name;
                    designationModels.SectionID = s.SubSectionT?.SubSecton?.Section?.SectionId;
                    designationModels.DivisionID = s.SubSectionT?.SubSecton?.Section?.Department?.Division?.DivisionId;
                    designationModels.DepartmentID = s.SubSectionT?.SubSecton?.Section?.Department?.DepartmentId;
                    designationModels.SubSectionID = s.SubSectionT?.SubSecton?.SubSectionId;
                    designationModels.Code = s.Code;
                    designationModels.Description = s.Description;
                    designationModels.HeadCount = s.HeadCount;
                    designationModels.SectionName = s.SubSectionT?.SubSecton?.Section?.Name;
                    designationModels.CompanyName = s.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                    designationModels.DivisionName = s.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                    designationModels.DepartmentName = s.SubSectionT?.SubSecton?.Section?.Department?.Name;
                    designationModels.SubSectionName = s.SubSectionT?.SubSecton?.Name;
                    designationModels.SubSectionTwoName = s.SubSectionT?.Name;
                    designationModels.StatusCodeID = s.StatusCodeId;
                    designationModels.AddedByUserID = s.AddedByUserId;
                    designationModels.ModifiedByUserID = s.ModifiedByUserId;
                    designationModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    designationModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    designationModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    designationModels.AddedDate = s.AddedDate;
                    designationModels.ModifiedDate = s.ModifiedDate;
                };
                designationModel.Add(designationModels);
            });
            //var result = _mapper.Map<List<DesignationModel>>(Designation);
            if (designationModel != null && designationModel.Count > 0)
            {
                designationModel.ForEach(d =>
                {
                    d.DropDownNames = d.CompanyName + " | " + d.DivisionName + " | " + d.DepartmentName + " | " + d.SectionName + " | " + d.SubSectionName + " | " + d.SubSectionTwoName + " | " + d.Name;
                });
            }
            return designationModel.OrderByDescending(a => a.DesignationID).ToList();
        }

        [HttpGet]
        [Route("GetDesignationBySubSectionTwo")]
        public List<DesignationModel> GetDesignationBySubSectionTwo(int id)
        {
            var designation = _context.Designation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.Company).Include(l => l.Level).AsNoTracking().ToList();
            List<DesignationModel> designationModel = new List<DesignationModel>();
            designation.ForEach(s =>
            {
                DesignationModel designationModels = new DesignationModel
                {
                    DesignationID = s.DesignationId,
                    LevelID = s.LevelId,
                    CompanyID = s.CompanyId,
                    CompanyNamee = s.Company?.Description,
                    SubSectionTID = s.SubSectionTid,
                    LevelName = s.Level?.Name,
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    HeadCount = s.HeadCount,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                designationModel.Add(designationModels);
            });
            return designationModel.Where(d => d.SubSectionTID == id).OrderByDescending(a => a.DesignationID).ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Designation")]
        [HttpGet("GetDesignations/{id:int}")]
        public ActionResult<DesignationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var designation = _context.Designation.SingleOrDefault(p => p.DesignationId == id.Value);
            var result = _mapper.Map<DesignationModel>(designation);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        //[HttpGet("{id}", Name = "Get Designation")]
        [HttpGet("GetDesignationHeadCount")]
        public ActionResult<DesignationModel> GetDesignationHeadCount(int? id)
        {
            var designation = _context.Designation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.Company).Include(l => l.Level).Where(d => d.DesignationId == id).OrderByDescending(a => a.DesignationId).FirstOrDefault();
            DesignationModel designationModel = new DesignationModel();
            if (designation != null)
            {
                designationModel.DesignationID = designation.DesignationId;
                designationModel.SubSectionTID = designation.SubSectionTid;
                designationModel.LevelID = designation.LevelId;
                designationModel.CompanyID = designation.CompanyId;
                designationModel.CompanyName = designation.Company?.Description;
                designationModel.LevelName = designation.Level?.Name;
                designationModel.Name = designation.Name;
                designationModel.Code = designation.Code;
                designationModel.Description = designation.Description;
                designationModel.HeadCount = designation.HeadCount;
                designationModel.StatusCodeID = designation.StatusCodeId;
                designationModel.AddedByUserID = designation.AddedByUserId;
                designationModel.ModifiedByUserID = designation.ModifiedByUserId;
                designationModel.AddedByUser = designation.AddedByUser != null ? designation.AddedByUser.UserName : "";
                designationModel.ModifiedByUser = designation.ModifiedByUser != null ? designation.ModifiedByUser.UserName : "";
                designationModel.StatusCode = designation.StatusCode != null ? designation.StatusCode.CodeValue : "";
                designationModel.AddedDate = designation.AddedDate;
                designationModel.ModifiedDate = designation.ModifiedDate;
            }

            //List<DesignationModel> designationModel = new List<DesignationModel>();
            //designation.ForEach(s =>
            //{
            //    DesignationModel designationModels = new DesignationModel
            //    {
            //        DesignationID = s.DesignationId,
            //        SubSectionTID = s.SubSectionTid,
            //        LevelID = s.LevelId,
            //        CompanyID = s.CompanyId,
            //        CompanyName = s.Company?.Description,
            //        LevelName = s.Level?.Name,
            //        Name = s.Name,
            //        Code = s.Code,
            //        Description = s.Description,
            //        HeadCount = s.HeadCount,
            //        StatusCodeID = s.StatusCodeId,
            //        AddedByUserID = s.AddedByUserId,
            //        ModifiedByUserID = s.ModifiedByUserId,
            //        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
            //        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
            //        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
            //        AddedDate = s.AddedDate,
            //        ModifiedDate = s.ModifiedDate,
            //    };
            //    designationModel.Add(designationModels);
            //});
            //designationModel.Where(d => d.DesignationID == id).OrderByDescending(a => a.DesignationID).FirstOrDefault();

            return designationModel;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DesignationModel> GetData(SearchModel searchModel)
        {
            var designation = new Designation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).FirstOrDefault();
                        break;
                    case "Last":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).LastOrDefault();
                        break;
                    case "Next":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).LastOrDefault();
                        break;
                    case "Previous":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).FirstOrDefault();
                        break;
                    case "Last":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).LastOrDefault();
                        break;
                    case "Next":
                        designation = _context.Designation.OrderBy(o => o.DesignationId).FirstOrDefault(s => s.DesignationId > searchModel.Id);
                        break;
                    case "Previous":
                        designation = _context.Designation.OrderByDescending(o => o.DesignationId).FirstOrDefault(s => s.DesignationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DesignationModel>(designation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDesignation")]
        public DesignationModel Post(DesignationModel value)
        {
            var existingDesignationList = _context.Designation.Where(s => s.SubSectionTid == value.SubSectionTID).ToList();
            Designation existing = new Designation();
            if (existingDesignationList != null && existingDesignationList.Count > 0)
            {
               
                existing = existingDesignationList.Where(s => s.Name.ToLower() == value.Name.ToLower()).FirstOrDefault();
               
            }
                if (existing == null || existing?.DesignationId == 0)
                {
                    var designation = new Designation
                    {
                        CompanyId = value.CompanyID,
                        SubSectionTid = value.SubSectionTID,
                        LevelId = value.LevelID,
                        HeadCount = value.HeadCount,
                        Name = value.Name,
                        Code = value.Code,
                        Description = value.Description,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value

                    };
                    _context.Designation.Add(designation);
                    _context.SaveChanges();
                    value.DesignationID = designation.DesignationId;
                }
                else
                {
                    throw new Exception("Designation " + value.Name + " Already Exist for Current Selection ! Please Create New Different Designation");
                }

            
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDesignation")]
        public DesignationModel Put(DesignationModel value)
        {
            var designation = _context.Designation.SingleOrDefault(p => p.DesignationId == value.DesignationID);
            var existingDesignationList = _context.Designation.Where(s => s.SubSectionTid == value.SubSectionTID && s.DesignationId !=value.DesignationID).ToList();
            var existing = existingDesignationList?.Where(s => s.Name.ToLower() == value.Name.ToLower()).FirstOrDefault();
            if (existing == null)
            {
                designation.CompanyId = value.CompanyID;
                designation.LevelId = value.LevelID;
                designation.HeadCount = value.HeadCount;
                designation.Name = value.Name;
                designation.Code = value.Code;
                designation.Description = value.Description;
                designation.ModifiedByUserId = value.ModifiedByUserID;
                designation.ModifiedDate = DateTime.Now;
                designation.StatusCodeId = value.StatusCodeID.Value;
                designation.SubSectionTid = value.SubSectionTID;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Designation " + value.Name + " Already Exist for Current Update Designation ! Please Update Different Designation");
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDesignation")]
        public void Delete(int id)
        {
            try
            { 
            var designation = _context.Designation.SingleOrDefault(p => p.DesignationId == id);
            if (designation != null)
            {
                _context.Designation.Remove(designation);
                _context.SaveChanges();
            }
            }
            catch (Exception ex)
            {
                throw new AppException("Designation Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}