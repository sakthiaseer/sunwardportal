﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HandlingOfShipmentClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public HandlingOfShipmentClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetHandlingOfShipmentClassificationByRefNo")]
        public List<HandlingOfShipmentClassificationModel> GetFpdrugClassificationByRefNo(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var handlingOfShipmentClassification = _context.HandlingOfShipmentClassification.Include(h => h.HandlingOfShipmentSpecificCustomer).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(p => p.Profile).AsNoTracking().ToList();
            List<HandlingOfShipmentClassificationModel> handlingOfShipmentClassificationModel = new List<HandlingOfShipmentClassificationModel>();
            handlingOfShipmentClassification.ForEach(s =>
            {
                HandlingOfShipmentClassificationModel handlingOfShipmentClassificationModels = new HandlingOfShipmentClassificationModel
                {
                    HandlingOfShipmentId = s.HandlingOfShipmentId,
                    ShipmentMethodId = s.ShipmentMethodId,
                    SpecificTypeOfOrderId = s.SpecificTypeOfOrderId,
                    ShipmentMethod = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.ShipmentMethodId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.ShipmentMethodId).Value : "",
                    SpecificTypeOfOrder = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SpecificTypeOfOrderId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SpecificTypeOfOrderId).Value : "",
                    SpecificCustomerIds = s.HandlingOfShipmentSpecificCustomer.Where(w => w.HandlingOfShipmentId == s.HandlingOfShipmentId).Select(ss => ss.SpecificCustomerId).ToList(),
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    ProcessingTimeDay = s.ProcessingTimeDay,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileId = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                };
                handlingOfShipmentClassificationModel.Add(handlingOfShipmentClassificationModels);
            });
            return handlingOfShipmentClassificationModel.Where(l => l.ItemClassificationMasterId == id).OrderByDescending(a => a.HandlingOfShipmentId).ToList();
        }
        [HttpPost]
        [Route("InsertHandlingOfShipmentClassification")]
        public HandlingOfShipmentClassificationModel Post(HandlingOfShipmentClassificationModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var handlingOfShipmentClassification = new HandlingOfShipmentClassification
            {
                ShipmentMethodId = value.ShipmentMethodId,
                SpecificTypeOfOrderId = value.SpecificTypeOfOrderId,
                ProcessingTimeDay = value.ProcessingTimeDay,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
            };
            _context.HandlingOfShipmentClassification.Add(handlingOfShipmentClassification);
            _context.SaveChanges();
            value.HandlingOfShipmentId = handlingOfShipmentClassification.HandlingOfShipmentId;
            value.ProfileReferenceNo = handlingOfShipmentClassification.ProfileReferenceNo;
            if (value.SpecificCustomerIds != null && value.SpecificCustomerIds.Count > 0)
            {
                value.SpecificCustomerIds.ForEach(f =>
                {
                    var handlingOfShipmentSpecificCustomer = new HandlingOfShipmentSpecificCustomer
                    {
                        HandlingOfShipmentId = value.HandlingOfShipmentId,
                        SpecificCustomerId = f,

                    };
                    _context.HandlingOfShipmentSpecificCustomer.Add(handlingOfShipmentSpecificCustomer);

                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateHandlingOfShipmentClassification")]
        public HandlingOfShipmentClassificationModel Put(HandlingOfShipmentClassificationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var handlingOfShipmentClassification = _context.HandlingOfShipmentClassification.SingleOrDefault(p => p.HandlingOfShipmentId == value.HandlingOfShipmentId);
            handlingOfShipmentClassification.ProfileReferenceNo = value.ProfileReferenceNo;
            handlingOfShipmentClassification.ShipmentMethodId = value.ShipmentMethodId;
            handlingOfShipmentClassification.ModifiedByUserId = value.ModifiedByUserID;
            handlingOfShipmentClassification.ModifiedDate = DateTime.Now;
            handlingOfShipmentClassification.ProfileId = value.ProfileId;
            handlingOfShipmentClassification.ItemClassificationMasterId = value.ItemClassificationMasterId;
            handlingOfShipmentClassification.SpecificTypeOfOrderId = value.SpecificTypeOfOrderId;
            handlingOfShipmentClassification.ProcessingTimeDay = value.ProcessingTimeDay;
            var specificCustomerItems = _context.HandlingOfShipmentSpecificCustomer.Where(t => t.HandlingOfShipmentId == handlingOfShipmentClassification.HandlingOfShipmentId).ToList();
            if (specificCustomerItems != null)
            {
                _context.HandlingOfShipmentSpecificCustomer.RemoveRange(specificCustomerItems);
                _context.SaveChanges();
            }
            if (value.SpecificCustomerIds != null && value.SpecificCustomerIds.Count > 0)
            {
                value.SpecificCustomerIds.ForEach(f =>
                {
                    var handlingOfShipmentSpecificCustomer = new HandlingOfShipmentSpecificCustomer
                    {
                        HandlingOfShipmentId = value.HandlingOfShipmentId,
                        SpecificCustomerId = f,

                    };
                    _context.HandlingOfShipmentSpecificCustomer.Add(handlingOfShipmentSpecificCustomer);

                });
                _context.SaveChanges();
            }
            value.SpecificTypeOfOrder = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.SpecificTypeOfOrderId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.SpecificTypeOfOrderId).Value : "";
            return value;
        }
        [HttpDelete]
        [Route("DeleteHandlingOfShipmentClassification")]
        public void Delete(int id)
        {
            var handlingOfShipmentClassification = _context.HandlingOfShipmentClassification.SingleOrDefault(p => p.HandlingOfShipmentId == id);
            if (handlingOfShipmentClassification != null)
            {
                var specificCustomerItems = _context.HandlingOfShipmentSpecificCustomer.Where(t => t.HandlingOfShipmentId == handlingOfShipmentClassification.HandlingOfShipmentId).ToList();
                if (specificCustomerItems != null)
                {
                    _context.HandlingOfShipmentSpecificCustomer.RemoveRange(specificCustomerItems);
                    _context.SaveChanges();
                }
                _context.HandlingOfShipmentClassification.Remove(handlingOfShipmentClassification);
                _context.SaveChanges();
            }
        }
    }
}