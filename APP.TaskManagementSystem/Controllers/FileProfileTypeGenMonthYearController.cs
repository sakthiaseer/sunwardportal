﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FileProfileTypeGenMonthYearController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        public FileProfileTypeGenMonthYearController(CRT_TMSContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("GenerateFileProfileTypeCreateByYearAndMonth")]
        public void GenerateCreateByYearAndCreateByMonth()
        {
            GenerateCreateByYear();
            GenerateCreateByMonth();
        }
        private void GenerateCreateByYear()
        {
            var fileProfileType = _context.FileProfileType.ToList();
            var fileProfileTypeFilter = fileProfileType.Where(w => w.IsCreateByYear == true).ToList();
            if (fileProfileTypeFilter != null && fileProfileTypeFilter.Count > 0)
            {
                fileProfileTypeFilter.ForEach(s =>
                {
                    var byChild = fileProfileType.Where(w => w.ParentId == s.FileProfileTypeId).ToList();
                    if (byChild != null && byChild.Count > 0)
                    {
                        if (s.IsCreateByYear == true)
                        {
                            DateTime currentDate = DateTime.Now;
                            //DateTime currentDate = new DateTime(2022, 12, 31);
                            DateTime n = new DateTime(currentDate.Year + 1, 1, 1);
                            var newDate = n.AddDays(-1);
                            if (currentDate.Date == newDate.Date)
                            {
                                byChild.ForEach(a =>
                                {
                                    var fileProfileTypes = new FileProfileType
                                    {
                                        ProfileId = a.ProfileId,
                                        Description = a.Description,
                                        Name = n.Year.ToString(),
                                        ParentId = a.FileProfileTypeId,
                                        StatusCodeId = a.StatusCodeId,
                                        AddedByUserId = a.AddedByUserId,
                                        IsExpiryDate = a.IsExpiryDate,
                                        IsAllowMobileUpload = a.IsAllowMobileUpload,
                                        IsDocumentAccess = a.IsDocumentAccess,
                                        ShelfLifeDuration = a.ShelfLifeDuration,
                                        ShelfLifeDurationId = a.ShelfLifeDurationId,
                                        Hints = a.Hints,
                                        IsEnableCreateTask = a.IsEnableCreateTask,
                                        ProfileTypeInfo = a.ProfileTypeInfo,
                                    // IsCreateByMonth = a.IsCreateByMonth,
                                    // IsCreateByYear = a.IsCreateByYear,
                                    AddedDate = DateTime.Now,
                                    };
                                    _context.FileProfileType.Add(fileProfileTypes);
                                    _context.SaveChanges();
                                });
                            }
                        }

                    }
                });
            }
        }
        private void GenerateCreateByMonth()
        {
            var fileProfileType = _context.FileProfileType.ToList();
            var fileProfileTypeFilter = fileProfileType.Where(w => w.IsCreateByMonth == true).ToList();
            if (fileProfileTypeFilter != null && fileProfileTypeFilter.Count > 0)
            {
                fileProfileTypeFilter.ForEach(s =>
                {
                    var byChild = fileProfileType.Where(w => w.ParentId == s.FileProfileTypeId).ToList();
                    if (byChild != null && byChild.Count > 0)
                    {
                        if (s.IsCreateByMonth == true)
                        {
                            DateTime currentDate = DateTime.Now;
                            //DateTime currentDate = new DateTime(2023, 02, 28);
                            DateTime ss = new DateTime(currentDate.Year, currentDate.Month, 1);
                            var newDate = ss.AddMonths(1).AddDays(-1);
                            if (currentDate == newDate)
                            {
                                var DisplayDate = ss.AddMonths(1);
                                var Dates = ss.AddMonths(1);
                                byChild.ForEach(a =>
                                {
                                    var fileProfileTypes = new FileProfileType
                                    {
                                        ProfileId = a.ProfileId,
                                        Description = a.Description,
                                        Name = Dates.ToString("MMM-yyy"),
                                        ParentId = a.FileProfileTypeId,
                                        StatusCodeId = a.StatusCodeId,
                                        AddedByUserId = a.AddedByUserId,
                                        IsExpiryDate = a.IsExpiryDate,
                                        IsAllowMobileUpload = a.IsAllowMobileUpload,
                                        IsDocumentAccess = a.IsDocumentAccess,
                                        ShelfLifeDuration = a.ShelfLifeDuration,
                                        ShelfLifeDurationId = a.ShelfLifeDurationId,
                                        Hints = a.Hints,
                                        IsEnableCreateTask = a.IsEnableCreateTask,
                                        ProfileTypeInfo = a.ProfileTypeInfo,
                                        //IsCreateByMonth = a.IsCreateByMonth,
                                        //IsCreateByYear = a.IsCreateByYear,
                                        AddedDate = DateTime.Now,
                                    };
                                    _context.FileProfileType.Add(fileProfileTypes);
                                    _context.SaveChanges();
                                });
                            }
                        }
                    }
                });
            }
        }

    }
}
