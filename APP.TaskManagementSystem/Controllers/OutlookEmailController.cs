﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using MsgReader.Outlook;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OutlookEmailController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public OutlookEmailController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;

            _hostingEnvironment = host;
        }
        [HttpPost]
        [Route("OutlookImportPublicFolder")]
        public List<OutlookEmailModel> OutlookImportPublicFolder(OutlookEmailModel value)
        {
            List<OutlookEmailModel> outlookEmailModels = new List<OutlookEmailModel>();
            if (value.ProfileId != null && value.FolderId != null)
            {
                var sessionId = Guid.NewGuid();
                var serverFullPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\OutLookMsg\";
                var serverPath = serverFullPath + sessionId + ".html";
                System.IO.File.WriteAllText(serverPath, value.BodyHtml);
                FileStream stream = System.IO.File.OpenRead(serverPath);
                var br = new BinaryReader(stream);
                Byte[] documents = br.ReadBytes((Int32)stream.Length);
                var compressedData = DocumentZipUnZip.Zip(documents);
                string profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, Title = value.Subject, AddedByUserID = value.UserId, StatusCodeID = 1 });
                var document = new Documents
                {
                    FileName = value.Subject + ".html",
                    ContentType = "text/html",
                    FileData = compressedData,
                    FileSize = new System.IO.FileInfo(serverPath).Length,
                    UploadDate = DateTime.Now,
                    AddedDate = DateTime.Now,
                    AddedByUserId = value.UserId,
                    SessionId = sessionId,
                    IsTemp = true,
                    IsCompressed = true,
                    IsLatest = true,
                    ProfileNo = profileNo,
                    FolderId = value.FolderId,
                };
                stream.Close();
                _context.Documents.Add(document);
                _context.SaveChanges();
                var mainDocumentId = document.DocumentId;
                System.IO.File.Delete(serverPath);
                var documentFolder = new DocumentFolder
                {
                    FolderId = value.FolderId,
                    DocumentId = mainDocumentId,
                    IsLatest = true,
                    IsLocked = false,
                    VersionNo = "1",
                    UploadedByUserId = value.UserId,
                    UploadedDate = DateTime.Now,
                    IsMeetingNotes = false,
                    IsMajorChange = false,
                    IsDiscussionNotes = false,
                    IsNoChange = false,
                    IsReleaseVersion = false,
                };
                _context.DocumentFolder.Add(documentFolder);
                _context.SaveChanges();
                if (value.Attachments.Count > 0)
                {
                    string profileNos = "";
                    value.Attachments.ForEach(a =>
                    {
                        if (value.ProfileId != null)
                        {
                            if (value.UserId != null)
                            {
                                profileNos = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, Title = value.Subject, AddedByUserID = value.UserId, StatusCodeID = 1 });

                            }
                        }
                        var filePath = serverFullPath + sessionId +"."+ a.Filename.Split('.').Last();
                        System.IO.File.WriteAllBytes(filePath, a.FileData);
                        FileStream stream = System.IO.File.OpenRead(filePath);
                        var br = new BinaryReader(stream);
                        Byte[] documentStream = DocumentZipUnZip.Zip(br.ReadBytes((Int32)stream.Length));
                        stream.Close();
                        string contentType = "";
                        new FileExtensionContentTypeProvider().TryGetContentType(filePath, out contentType);
                        var subDocuments = new Documents
                        {
                            FileName = a.Filename,
                            ContentType = contentType,
                            FileData = documentStream,
                            FileSize = a.Filesize,
                            UploadDate = DateTime.Now,
                            AddedDate = DateTime.Now,
                            SessionId = sessionId,
                            FolderId = value.FolderId,
                            ProfileNo = profileNos,
                            IsLatest = true,
                            AddedByUserId = value.UserId,
                            IsCompressed = true,
                            FileIndex = 0,
                            IsMainTask = false,
                        };
                        _context.Documents.Add(subDocuments);
                        _context.SaveChanges();
                        var subDocumentId = subDocuments.DocumentId;
                        System.IO.File.Delete(filePath);
                        var DocumentLink = new DocumentLink
                        {
                            DocumentId = mainDocumentId,
                            AddedByUserId = value.UserId,
                            AddedDate = DateTime.Now,
                            LinkDocumentId = subDocuments.DocumentId,
                            FolderId = value.FolderId,
                            DocumentPath = "Outlook Email Link Document",
                        };
                        _context.DocumentLink.Add(DocumentLink);
                        _context.SaveChanges();
                        var documentFolder = new DocumentFolder
                        {
                            FolderId = value.FolderId,
                            DocumentId = subDocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            VersionNo = "1",
                            UploadedByUserId = value.UserId,
                            UploadedDate = DateTime.Now,
                            IsMeetingNotes = false,
                            IsMajorChange = false,
                            IsDiscussionNotes = false,
                            IsNoChange = false,
                            IsReleaseVersion = false,
                        };
                        _context.DocumentFolder.Add(documentFolder);
                        _context.SaveChanges();
                    });
                }
            }
            return outlookEmailModels;
        }
        [HttpPost]
        [Route("ImportuploadDocuments")]
        public OutlookEmailModel ImportuploadDocuments(IFormCollection files)
        {
            OutlookEmailModel outlookEmailModel = new OutlookEmailModel();
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "OutLookMsg";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            files.Files.ToList().ForEach(f =>
            {
                var filePath = serverPath + @"\" + f.FileName;
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    f.CopyTo(targetStream);
                    targetStream.Flush();
                }
                using (var msg = new Storage.Message(filePath))
                {
                    outlookEmailModel.Sender = msg.Sender.Raw;
                    outlookEmailModel.SentOn = msg.SentOn;
                    outlookEmailModel.RecipientsTo = msg.GetEmailRecipients(RecipientType.To, false, false);
                    outlookEmailModel.RecipientsCc = msg.GetEmailRecipients(RecipientType.Cc, false, false);
                    outlookEmailModel.Subject = msg.Subject;
                    outlookEmailModel.BodyHtml = msg.BodyHtml;
                    var attachments = msg.Attachments;
                    List<OutlookEmailAttachmentModel> attachmentModels = new List<OutlookEmailAttachmentModel>();
                    if (attachments != null && attachments.Count > 0)
                    {
                        attachments.ForEach(a =>
                        {
                            var attachment = a as Storage.Message.Attachment;
                            if (attachment != null)
                            {
                                if (attachment.ContentId == null)
                                {
                                    OutlookEmailAttachmentModel attachmentModel = new OutlookEmailAttachmentModel();
                                    attachmentModel.ContentId = attachment.ContentId;
                                    attachmentModel.Filename = attachment.FileName;
                                    attachmentModel.FileData = attachment.Data;
                                    attachmentModel.Filesize = attachment.Data.Length;
                                    attachmentModel.Size = GetSizeInMemory(attachment.Data.Length);
                                    attachmentModels.Add(attachmentModel);
                                }
                                else
                                {
                                    var contentId = "cid:" + attachment.ContentId;
                                    if (outlookEmailModel.BodyHtml.Contains(contentId))
                                    {
                                        var contentType = "image/" + attachment.FileName.Split('.').Last();
                                        var base64String = Convert.ToBase64String(attachment.Data, 0, attachment.Data.Length);
                                        var filebaseString = "data:" + contentType + ";base64," + base64String;
                                        outlookEmailModel.BodyHtml = outlookEmailModel.BodyHtml.Replace(contentId, filebaseString);
                                    }
                                }
                            }
                        });
                    }
                    outlookEmailModel.Attachments = attachmentModels;
                }
                System.IO.File.Delete(filePath);
            });
            return outlookEmailModel;
        }
        public string GetSizeInMemory(long bytesize)
        {
            string[] sizes = { "B", "KB", "MB", "GB", "TB" };
            double len = Convert.ToDouble(bytesize);
            int order = 0;
            while (len >= 1024D && order < sizes.Length - 1)
            {
                order++;
                len /= 1024;
            }

            return string.Format(CultureInfo.CurrentCulture, "{0:0.##} {1}", len, sizes[order]);
        }
    }
}
