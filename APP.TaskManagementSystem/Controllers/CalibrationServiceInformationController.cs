﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CalibrationServiceInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CalibrationServiceInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCalibrationServiceInformation")]
        public List<CalibrationServiceInformationModel> Get()
        {
            var calibrationServiceInformation = _context.CalibrationServiceInfo.Include(a => a.AddedByUser)
                                                    .Include(m => m.ModifiedByUser)
                                                    .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CalibrationServiceInformationModel> calibrationServiceInformationModel = new List<CalibrationServiceInformationModel>();
            calibrationServiceInformation.ForEach(s =>
            {
                CalibrationServiceInformationModel calibrationServiceInformationModels = new CalibrationServiceInformationModel
                {
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,
                    CalibrationNo = s.CalibrationNo,
                    Instrument = s.Instrument,
                    Lot = s.Lot,
                    Machine = s.Machine,
                    SerialNo = s.SerialNo,
                    DeviceID = s.DeviceId,
                    ManufacturerID = s.ManufacturerId,
                    Model = s.Model,
                    Range = s.Range,
                    RangeOfCalibration = s.RangeOfCalibration,
                    LastCalibrationDate = s.LastCalibrationDate,
                    NextCalibrationDate = s.NextCalibrationDate,
                    Status = s.Status,
                    Remark = s.Remark,
                    Certificate = s.Certificate,
                    StickerCode = s.StickerCode,
                    ExpireMonths = s.ExpireMonths,

                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                calibrationServiceInformationModel.Add(calibrationServiceInformationModels);
            });
            return calibrationServiceInformationModel.OrderByDescending(a => a.CalibrationServiceInformationID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetCalibrationServiceInformation/{id:int}")]
        public ActionResult<CalibrationServiceInformationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var calibrationServiceInformation = _context.CalibrationServiceInfo.SingleOrDefault(p => p.CalibrationServiceInformationId == id.Value);
            var result = _mapper.Map<CalibrationServiceInformationModel>(calibrationServiceInformation);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CalibrationServiceInformationModel> GetData(SearchModel searchModel)
        {
            var calibrationServiceInformation = new CalibrationServiceInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderBy(o => o.CalibrationServiceInformationId).FirstOrDefault(s => s.CalibrationServiceInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        calibrationServiceInformation = _context.CalibrationServiceInfo.OrderByDescending(o => o.CalibrationServiceInformationId).FirstOrDefault(s => s.CalibrationServiceInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CalibrationServiceInformationModel>(calibrationServiceInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCalibrationServiceInformation")]
        public CalibrationServiceInformationModel Post(CalibrationServiceInformationModel value)
        {
            var calibrationServiceInformation = new CalibrationServiceInfo
            {
                CalibrationNo = value.CalibrationNo,
                Instrument = value.Instrument,
                Lot = value.Lot,
                Machine = value.Machine,
                SerialNo = value.SerialNo,
                DeviceId = value.DeviceID,
                ManufacturerId = value.ManufacturerID,
                Model = value.Model,
                Range = value.Range,
                RangeOfCalibration = value.RangeOfCalibration,
                LastCalibrationDate = value.LastCalibrationDate,
                NextCalibrationDate = value.NextCalibrationDate,
                Status = value.Status,
                Remark = value.Remark,
                Certificate = value.Certificate,
                StickerCode = value.StickerCode,
                ExpireMonths = value.ExpireMonths,

                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };


            _context.CalibrationServiceInfo.Add(calibrationServiceInformation);
            _context.SaveChanges();
            value.CalibrationServiceInformationID = calibrationServiceInformation.CalibrationServiceInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCalibrationServiceInformation")]
        public CalibrationServiceInformationModel Put(CalibrationServiceInformationModel value)
        {
            var calibrationServiceInformation = _context.CalibrationServiceInfo.SingleOrDefault(p => p.CalibrationServiceInformationId == value.CalibrationServiceInformationID);

            calibrationServiceInformation.CalibrationNo = value.CalibrationNo;
            calibrationServiceInformation.Instrument = value.Instrument;
            calibrationServiceInformation.Lot = value.Lot;
            calibrationServiceInformation.Machine = value.Machine;
            calibrationServiceInformation.SerialNo = value.SerialNo;
            calibrationServiceInformation.DeviceId = value.DeviceID;
            calibrationServiceInformation.ManufacturerId = value.ManufacturerID;
            calibrationServiceInformation.Model = value.Model;
            calibrationServiceInformation.Range = value.Range;
            calibrationServiceInformation.RangeOfCalibration = value.RangeOfCalibration;
            calibrationServiceInformation.LastCalibrationDate = value.LastCalibrationDate;
            calibrationServiceInformation.NextCalibrationDate = value.NextCalibrationDate;
            calibrationServiceInformation.Status = value.Status;
            calibrationServiceInformation.Remark = value.Remark;
            calibrationServiceInformation.Certificate = value.Certificate;
            calibrationServiceInformation.StickerCode = value.StickerCode;
            calibrationServiceInformation.ExpireMonths = value.ExpireMonths;
            calibrationServiceInformation.CalibrationServiceInformationId = value.CalibrationServiceInformationID;

            calibrationServiceInformation.ModifiedByUserId = value.ModifiedByUserID;
            calibrationServiceInformation.ModifiedDate = DateTime.Now;
            calibrationServiceInformation.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCalibrationServiceInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var calibrationServiceInformation = _context.CalibrationServiceInfo.SingleOrDefault(p => p.CalibrationServiceInformationId == id);
                if (calibrationServiceInformation != null)
                {
                    _context.CalibrationServiceInfo.Remove(calibrationServiceInformation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}