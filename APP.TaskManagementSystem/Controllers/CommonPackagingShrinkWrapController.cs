﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingShrinkWrapController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingShrinkWrapController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingShrinkWrap")]
        public List<CommonPackagingShrinkWrapModel> Get(string refNo)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingShrinkWrapModel> commonPackagingShrinkWrapModel = new List<CommonPackagingShrinkWrapModel>();
            commonPackagingShrinkWrap.ForEach(s =>
            {
                CommonPackagingShrinkWrapModel commonPackagingShrinkWrapModels = new CommonPackagingShrinkWrapModel
                {
                    ShrinkWrapSpecificationId = s.ShrinkWrapSpecificationId,
                    PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                    PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                    Set = s.Set,
                    MeasurementLength = s.MeasurementLength,
                    MeasurementWidth = s.MeasurementWidth,
                    Thickness = s.Thickness,
                    StatusCodeID = s.StatusCodeId,
                    VersionControl = s.VersionControl,
                    UseforItemIds = s.BottleCapUseForItems.Where(b => b.ShrinkWrapSpecificationId == s.ShrinkWrapSpecificationId).Select(b => b.UseForItemId).ToList(),
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                };
                commonPackagingShrinkWrapModel.Add(commonPackagingShrinkWrapModels);
            });
            return commonPackagingShrinkWrapModel.Where(w => w.LinkProfileReferenceNo == refNo).OrderByDescending(a => a.ShrinkWrapSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetCommonPackagingShrinkWrapByRefNo")]
        public List<CommonPackagingShrinkWrapModel> GetCommonPackagingShrinkWrapByRefNo(RefSearchModel refSearchModel)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingShrinkWrapModel> commonPackagingShrinkWrapModel = new List<CommonPackagingShrinkWrapModel>();
            commonPackagingShrinkWrap.ForEach(s =>
            {
                CommonPackagingShrinkWrapModel commonPackagingShrinkWrapModels = new CommonPackagingShrinkWrapModel
                {
                    ShrinkWrapSpecificationId = s.ShrinkWrapSpecificationId,
                    PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                    PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                    Set = s.Set,
                    MeasurementLength = s.MeasurementLength,
                    MeasurementWidth = s.MeasurementWidth,
                    Thickness = s.Thickness,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    VersionControl = s.VersionControl,
                    UseforItemIds = s.BottleCapUseForItems.Where(b => b.ShrinkWrapSpecificationId == s.ShrinkWrapSpecificationId).Select(b => b.UseForItemId).ToList(),
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                };
                commonPackagingShrinkWrapModel.Add(commonPackagingShrinkWrapModels);
            });
            if (refSearchModel.IsHeader)
            {
                return commonPackagingShrinkWrapModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ShrinkWrapSpecificationId).ToList();
            }
            return commonPackagingShrinkWrapModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ShrinkWrapSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingShrinkWrapModel> GetData(SearchModel searchModel)
        {
            var commonPackagingShrinkWrap = new CommonPackagingShrinkWrap();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderBy(o => o.ShrinkWrapSpecificationId).FirstOrDefault(s => s.ShrinkWrapSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.OrderByDescending(o => o.ShrinkWrapSpecificationId).FirstOrDefault(s => s.ShrinkWrapSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingShrinkWrapModel>(commonPackagingShrinkWrap);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingShrinkWrap")]
        public CommonPackagingShrinkWrapModel Post(CommonPackagingShrinkWrapModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingShrinkWrap" });

            var commonPackagingShrinkWrap = new CommonPackagingShrinkWrap
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                MeasurementWidth = value.MeasurementWidth,
                MeasurementLength = value.MeasurementLength,
                Thickness = value.Thickness,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingShrinkWrap.Add(commonPackagingShrinkWrap);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingShrinkWrap.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingShrinkWrap.ProfileLinkReferenceNo;
            value.ShrinkWrapSpecificationId = commonPackagingShrinkWrap.ShrinkWrapSpecificationId;
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.ShrinkWrapSpecificationId == value.ShrinkWrapSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                        var bottlecapUseForItems = new BottleCapUseForItems()
                        {
                            ShrinkWrapSpecificationId = value.ShrinkWrapSpecificationId,
                            UseForItemId = u,
                        };
                        _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                        _context.SaveChanges();
                    //}
                });
            }
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingShrinkWrapModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    if(value.MeasurementLength!=null)
                    {
                        itemName = value.MeasurementLength + "mm" + " ";

                    }
                    if(value.MeasurementWidth!=null)
                    {
                        itemName = itemName + "X" + " " + value.MeasurementWidth + "mm" + " ";

                    }
                    if(value.Thickness!=null)
                    {
                        itemName = itemName + "X" + " " + value.Thickness + "mm" + " ";
                    }
                    itemName = itemName + "ShrinkWrap";
                    //itemName = value.MeasurementLength + "mm" + " " + "X" + " " + value.MeasurementWidth + "mm" + " " + "X" + " " + value.Thickness + "mm ShrinkWrap";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingShrinkWrap")]
        public CommonPackagingShrinkWrapModel Put(CommonPackagingShrinkWrapModel value)
        {
            var commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.SingleOrDefault(p => p.ShrinkWrapSpecificationId == value.ShrinkWrapSpecificationId);
            commonPackagingShrinkWrap.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingShrinkWrap.Set = value.Set;
            commonPackagingShrinkWrap.MeasurementWidth = value.MeasurementWidth;
            commonPackagingShrinkWrap.MeasurementLength = value.MeasurementLength;
            commonPackagingShrinkWrap.Thickness = value.Thickness;
            commonPackagingShrinkWrap.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingShrinkWrap.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingShrinkWrap.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingShrinkWrap.VersionControl = value.VersionControl;
            commonPackagingShrinkWrap.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingShrinkWrap.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.ShrinkWrapSpecificationId == value.ShrinkWrapSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.ShrinkWrapSpecificationId == value.ShrinkWrapSpecificationId && b.UseForItemId == u).FirstOrDefault();
                   // if (exist == null)
                   // {
                        var bottlecapUseForItems = new BottleCapUseForItems()
                        {
                            ShrinkWrapSpecificationId = value.ShrinkWrapSpecificationId,
                            UseForItemId = u,
                        };
                        _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                        _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingShrinkWrap")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingShrinkWrap = _context.CommonPackagingShrinkWrap.Where(p => p.ShrinkWrapSpecificationId == id).FirstOrDefault();
                if (commonPackagingShrinkWrap != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.ShrinkWrapSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingShrinkWrap.Remove(commonPackagingShrinkWrap);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}