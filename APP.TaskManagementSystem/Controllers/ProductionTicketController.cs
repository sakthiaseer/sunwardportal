﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionTicketController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionTicketController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionTickets")]
        public List<ProductionTicketModel> Get()
        {
            List<ProductionTicketModel> productionTicketModels = new List<ProductionTicketModel>();
            var productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).AsNoTracking().ToList();
            if (productionTicket != null && productionTicket.Count > 0)
            {
                productionTicket.ForEach(s =>
                {
                    ProductionTicketModel productionTicketModel = new ProductionTicketModel
                    {
                        ProductionTicketId = s.ProductionTicketId,
                        BaseUnitId = s.BaseUnitId,
                        BatchNo = s.BaseUnit != null ? s.BaseUnit.Code : "",
                        CompanyId = s.ComapnyId,
                        CompanyName = s.Comapny != null ? s.Comapny.Description : "",
                        COAId = s.Coaid,
                        FileName = s.Coa != null ? s.Coa.FileName : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,

                    };
                    productionTicketModels.Add(productionTicketModel);
                });
            }

            return productionTicketModels;
        }
        [HttpGet]
        [Route("GetNavBaseUnitByID")]
        public List<NAVBaseUnitModel> GetNavBaseUnit(long? id)
        {
            List<NAVBaseUnitModel> nAVBaseUnitModels = new List<NAVBaseUnitModel>();
            var productionTicket = _context.NavbaseUnit.Where(o => o.CompanyId == id).AsNoTracking().ToList();
            if (productionTicket != null && productionTicket.Count > 0)
            {
                productionTicket.ForEach(s =>
                {
                    NAVBaseUnitModel nAVBaseUnitModel = new NAVBaseUnitModel
                    {
                        NavBaseUnitId = s.NavBaseUnitId,
                        CompanyId = s.CompanyId,
                        Code = s.Code,
                        Description = s.Description,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                    };
                    nAVBaseUnitModels.Add(nAVBaseUnitModel);
                });
            }

            return nAVBaseUnitModels;
        }


        [HttpPost()]
        [Route("GetProductionTicket")]
        public ActionResult<ProductionTicketModel> GetData(SearchModel searchModel)
        {
            var productionTicket = new ProductionTicket();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).FirstOrDefault();
                        break;
                    case "Last":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).LastOrDefault();
                        break;
                    case "Next":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).LastOrDefault();
                        break;
                    case "Previous":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).FirstOrDefault();
                        break;
                    case "Last":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).LastOrDefault();
                        break;
                    case "Next":
                        productionTicket = _context.ProductionTicket.OrderBy(o => o.ProductionTicketId).FirstOrDefault(s => s.ProductionTicketId > searchModel.Id);
                        break;
                    case "Previous":
                        productionTicket = _context.ProductionTicket.OrderByDescending(o => o.ProductionTicketId).FirstOrDefault(s => s.ProductionTicketId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionTicketModel>(productionTicket);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionTicket")]
        public ProductionTicketModel Post(ProductionTicketModel value)
        {
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).AsNoTracking().ToList();
                if (docu != null && docu.Count > 0)
                {
                    docu.ForEach(file =>
                    {
                        var productionTicket = new ProductionTicket
                        {
                            BaseUnitId = value.BaseUnitId,
                            Coaid = file.DocumentId,
                            ComapnyId = value.CompanyId,
                            StatusCodeId = value.StatusCodeID.Value,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,

                        };
                        _context.ProductionTicket.Add(productionTicket);
                        _context.SaveChanges();
                        value.ProductionTicketId = productionTicket.ProductionTicketId;

                    });
                }

            }
            else
            {
                var productionTicket = new ProductionTicket
                {
                    BaseUnitId = value.BaseUnitId,
                    ComapnyId = value.CompanyId,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,

                };
                _context.ProductionTicket.Add(productionTicket);
                _context.SaveChanges();
                value.ProductionTicketId = productionTicket.ProductionTicketId;
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionTicket")]
        public ProductionTicketModel Put(ProductionTicketModel value)
        {
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).AsNoTracking().ToList();
                if (docu != null && docu.Count > 0)
                {
                    docu.ForEach(file =>
                    {
                        var productionTicket = _context.ProductionTicket.SingleOrDefault(p => p.ProductionTicketId == value.ProductionTicketId);
                        if (productionTicket != null)
                        {
                            productionTicket.BaseUnitId = value.BaseUnitId;
                            productionTicket.Coaid = file.DocumentId;
                            productionTicket.ComapnyId = value.CompanyId;
                            productionTicket.ModifiedByUserId = value.ModifiedByUserID;
                            productionTicket.ModifiedDate = DateTime.Now;
                        }
                    });
                    _context.SaveChanges();

                }
            }
            else
            {
                var productionTicket = _context.ProductionTicket.SingleOrDefault(p => p.ProductionTicketId == value.ProductionTicketId);
                if (productionTicket != null)
                {
                    productionTicket.BaseUnitId = value.BaseUnitId;
                    productionTicket.ComapnyId = value.CompanyId;
                    productionTicket.ModifiedByUserId = value.ModifiedByUserID;
                    productionTicket.ModifiedDate = DateTime.Now;
                }
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionTicket")]
        public void Delete(int id)
        {
            var productionTicket = _context.ProductionTicket.SingleOrDefault(p => p.ProductionTicketId == id);
            if (productionTicket != null)
            {
                _context.ProductionTicket.Remove(productionTicket);
                _context.SaveChanges();
            }
        }
    }
}