﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProjectedDeliverySalesOrderLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProjectedDeliverySalesOrderLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProjectedDeliverys")]
        public List<ProjectedDeliverySalesOrderLineModel> Get(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var projectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s=>s.StatusCode).Where(s=>s.ContractDistributionSalesEntryLineId==id)
                            .AsNoTracking().ToList();
            List<ProjectedDeliverySalesOrderLineModel> ProjectedDeliverySalesOrderLineModel = new List<ProjectedDeliverySalesOrderLineModel>();
            projectedDeliverySalesOrderLine.ForEach(s =>
            {
                ProjectedDeliverySalesOrderLineModel ProjectedDeliverySalesOrderLineModels = new ProjectedDeliverySalesOrderLineModel
                {
                    ProjectedDeliverySalesOrderLineID = s.ProjectedDeliverySalesOrderLineId,   
                    FrequencyID = s.FrequencyId,
                    PerFrequencyQty = s.PerFrequencyQty,
                    IsQtyReference = s.IsQtyReference,
                    StartDate = s.StartDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser =  s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    EndDate = s.EndDate,
                    Frequency = masterDetailList != null && s.FrequencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.FrequencyId).Select(a => a.Value).SingleOrDefault() : "",
                };
                ProjectedDeliverySalesOrderLineModel.Add(ProjectedDeliverySalesOrderLineModels);
            });
            return ProjectedDeliverySalesOrderLineModel.OrderByDescending(a => a.ProjectedDeliverySalesOrderLineID).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProjectedDeliverySalesOrderLineModel> GetData(SearchModel searchModel)
        {
            var ProjectedDeliverySalesOrderLine = new ProjectedDeliverySalesOrderLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Previous":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderBy(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault(s => s.ProjectedDeliverySalesOrderLineId > searchModel.Id);
                        break;
                    case "Previous":
                        ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault(s => s.ProjectedDeliverySalesOrderLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProjectedDeliverySalesOrderLineModel>(ProjectedDeliverySalesOrderLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProjectedDelivery")]
        public ProjectedDeliverySalesOrderLineModel Post(ProjectedDeliverySalesOrderLineModel value)
        {
           
            var ProjectedDeliverySalesOrderLine = new ProjectedDeliverySalesOrderLine
            {

                ContractDistributionSalesEntryLineId = value.ContractDistributionSalesEntryLineID,
                FrequencyId = value.FrequencyID,
                PerFrequencyQty = value.PerFrequencyQty,
                StartDate = value.StartDate,
                IsQtyReference = value.IsQtyReference,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                EndDate = value.EndDate,
               

            };
            _context.ProjectedDeliverySalesOrderLine.Add(ProjectedDeliverySalesOrderLine);
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.Frequency = masterDetailList != null && value.FrequencyID > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.FrequencyID).Select(a => a.Value).SingleOrDefault() : "";
           // value.ProjectedDeliverySalesOrderLineID = ProjectedDeliverySalesOrderLine.ProjectedDeliverySalesOrderLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProjectedDelivery")]
        public ProjectedDeliverySalesOrderLineModel Put(ProjectedDeliverySalesOrderLineModel value)
        {
            var projectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.SingleOrDefault(p => p.ProjectedDeliverySalesOrderLineId == value.ProjectedDeliverySalesOrderLineID);


            projectedDeliverySalesOrderLine.ContractDistributionSalesEntryLineId = value.ContractDistributionSalesEntryLineID;
            projectedDeliverySalesOrderLine.FrequencyId = value.FrequencyID;
            projectedDeliverySalesOrderLine.PerFrequencyQty = value.PerFrequencyQty;
            projectedDeliverySalesOrderLine.StartDate = value.StartDate;
            projectedDeliverySalesOrderLine.IsQtyReference = value.IsQtyReference;
            projectedDeliverySalesOrderLine.StatusCodeId = value.StatusCodeID.Value;
            projectedDeliverySalesOrderLine.ModifiedByUserId = value.ModifiedByUserID;
            projectedDeliverySalesOrderLine.ModifiedDate = DateTime.Now;
            projectedDeliverySalesOrderLine.EndDate = value.EndDate;
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.Frequency = masterDetailList != null && value.FrequencyID > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.FrequencyID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProjectedDelivery")]
        public void Delete(int id)
        {
            var ProjectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.SingleOrDefault(p => p.ProjectedDeliverySalesOrderLineId == id);
            if (ProjectedDeliverySalesOrderLine != null)
            {
                _context.ProjectedDeliverySalesOrderLine.Remove(ProjectedDeliverySalesOrderLine);
                _context.SaveChanges();
            }
        }
    }
}