﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesPromotionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SalesPromotionController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetSalesPromotion")]
        public List<SalesPromotionModel> Get()
        {
            var applicationMasterIds = _context.ApplicationMaster.Where(f => f.ApplicationMasterCodeId == 287).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var SalesPromotions = _context.SalesPromotion
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .AsNoTracking()
                .ToList();
            List<SalesPromotionModel> SalesPromotionModels = new List<SalesPromotionModel>();
            SalesPromotions.ForEach(s =>
            {
                SalesPromotionModel SalesPromotionModel = new SalesPromotionModel
                {
                    SalesPromotionId = s.SalesPromotionId,
                    CompanyId = s.CompanyId,
                    CompanyName = s.Company?.PlantCode,
                    ProfileId = s.ProfileId,
                    PromotionNo = s.PromotionNo,
                    PurposeOfPromotionId = s.PurposeOfPromotionId,
                    PurposeOfPromotion = applicationMasterDetails != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeOfPromotionId)?.Value : "",
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    SessionId = s.SessionId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                  
                };
                SalesPromotionModels.Add(SalesPromotionModel);
            });
            return SalesPromotionModels.OrderByDescending(a => a.SalesPromotionId).ToList();
        }

        [HttpPost()]
        [Route("GetSalesPromotionData")]
        public ActionResult<SalesPromotionModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var SalesPromotion = new SalesPromotion();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).LastOrDefault();
                        break;
                    case "Next":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).LastOrDefault();
                        break;
                    case "Previous":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).LastOrDefault();
                        break;
                    case "Next":
                        SalesPromotion = _context.SalesPromotion.OrderBy(o => o.SalesPromotionId).FirstOrDefault(s => s.SalesPromotionId > searchModel.Id);
                        break;
                    case "Previous":
                        SalesPromotion = _context.SalesPromotion.OrderByDescending(o => o.SalesPromotionId).FirstOrDefault(s => s.SalesPromotionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SalesPromotionModel>(SalesPromotion);
            return result;
        }

        [HttpPost]
        [Route("InsertSalesPromotion")]
        public SalesPromotionModel Post(SalesPromotionModel value)
        {
            var SessionId = Guid.NewGuid();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => f.ApplicationMasterCodeId == 287).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            string profileNo = "";
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Sales Promotion" });
            }
            var SalesPromotion = new SalesPromotion
            {
                CompanyId = value.CompanyId,
                ProfileId = value.ProfileId,
                PromotionNo = profileNo,
                PurposeOfPromotionId = value.PurposeOfPromotionId,
                StartDate = value.StartDate,
                EndDate = value.EndDate,              
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            _context.SalesPromotion.Add(SalesPromotion);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.SalesPromotionId = SalesPromotion.SalesPromotionId;
            value.PromotionNo = profileNo;
            if(value.PurposeOfPromotionId>0)
            {
                value.PurposeOfPromotion = applicationMasterDetails.FirstOrDefault(s => s.ApplicationMasterDetailId == value.PurposeOfPromotionId)?.Value;
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesPromotion")]
        public SalesPromotionModel Put(SalesPromotionModel value)
        {
            var applicationMasterIds = _context.ApplicationMaster.Where(f => f.ApplicationMasterCodeId == 287).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;

            }
            if (value.ProfileId > 0 && value.PromotionNo == null)
            {
                var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Sales Promotion" });               
                value.PromotionNo = profileNo;
            }
            var SalesPromotion = _context.SalesPromotion.SingleOrDefault(p => p.SalesPromotionId == value.SalesPromotionId);
            SalesPromotion.CompanyId = value.CompanyId;
            SalesPromotion.ProfileId = value.ProfileId;
            SalesPromotion.PromotionNo = value.PromotionNo;
            SalesPromotion.PurposeOfPromotionId = value.PurposeOfPromotionId;
            SalesPromotion.StartDate = value.StartDate;
            SalesPromotion.EndDate = value.EndDate;
            SalesPromotion.StatusCodeId = value.StatusCodeID.Value;
            SalesPromotion.ModifiedByUserId = value.ModifiedByUserID;
            SalesPromotion.ModifiedDate = DateTime.Now;
            SalesPromotion.SessionId = value.SessionId;
            if (value.PurposeOfPromotionId > 0)
            {
                value.PurposeOfPromotion = applicationMasterDetails.FirstOrDefault(s => s.ApplicationMasterDetailId == value.PurposeOfPromotionId)?.Value;
            }
            _context.SaveChanges();
           
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesPromotion")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var SalesPromotion = _context.SalesPromotion.Where(p => p.SalesPromotionId == id).FirstOrDefault();
                if (SalesPromotion != null)
                {

                    _context.SalesPromotion.Remove(SalesPromotion);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteSalesMainPromotionList")]
        public ActionResult<string> DeleteSalesMainPromotionList(int id)
        {
            try
            {

                var SalesMainPromotionLists = _context.SalesMainPromotionList.Where(p => p.SalesMainPromotionListId == id).AsNoTracking().FirstOrDefault();
                if (SalesMainPromotionLists != null)
                {                   
                    _context.SalesMainPromotionList.Remove(SalesMainPromotionLists);
                    _context.SaveChanges();
                }


                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetSalesMainPromotionListsByID")]
        public List<SalesMainPromotionListModel> GetSalesMainPromotionList(int? id)
        {
            var SalesMainPromotionLists = _context.SalesMainPromotionList.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(g=>g.GenericCode).AsNoTracking().ToList();
            var masterIds = _context.ApplicationMaster.Where(s => s.ApplicationMasterCodeId == 234).Select(s => s.ApplicationMasterId).FirstOrDefault();
            var masterDetailList = _context.ApplicationMasterDetail.Where(a=> a.ApplicationMasterId ==masterIds ).AsNoTracking().ToList();
            List<SalesMainPromotionListModel> SalesMainPromotionListModels = new List<SalesMainPromotionListModel>();
            SalesMainPromotionLists.ForEach(s =>
            {
                SalesMainPromotionListModel SalesMainPromotionListModel = new SalesMainPromotionListModel
                {
                    SalesMainPromotionListId = s.SalesMainPromotionListId,
                    SalesPromotionId = s.SalesPromotionId,
                    GenericCodeId = s.GenericCodeId,  
                    IsFocList = s.IsFocList,
                    GenericCode = s.GenericCode?.Code,
                    ItemUom = masterDetailList != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.GenericCode?.Uom)?.Value : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                SalesMainPromotionListModels.Add(SalesMainPromotionListModel);
            });
            return SalesMainPromotionListModels.Where(w => w.SalesPromotionId == id).OrderByDescending(a => a.SalesMainPromotionListId).ToList();
        }



        // POST: api/User
        [HttpPost]
        [Route("InsertSalesMainPromotionList")]
        public SalesMainPromotionListModel InsertSalesMainPromotionList(SalesMainPromotionListModel value)
        {
            var SessionId = Guid.NewGuid();
            var masterIds = _context.ApplicationMaster.Where(s => s.ApplicationMasterCodeId == 234).Select(s => s.ApplicationMasterId).FirstOrDefault();
            var masterDetailList = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == masterIds).AsNoTracking().ToList();
            var SalesMainPromotionList = new SalesMainPromotionList
            {
                SalesPromotionId = value.SalesPromotionId,
                GenericCodeId = value.GenericCodeId,
                IsFocList = value.IsFocList,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.SalesMainPromotionList.Add(SalesMainPromotionList);
            _context.SaveChanges();
            value.SalesMainPromotionListId = SalesMainPromotionList.SalesMainPromotionListId;
            if (value.GenericCodeId != null)
            {
                var genriccode = _context.GenericCodes.FirstOrDefault(s => s.GenericCodeId == value.GenericCodeId);
                if (genriccode!=null)
                {
                    value.ItemUom = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == genriccode.Uom)?.Value;
                    value.GenericCode = genriccode.Code;
                 }
            }
          
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSalesMainPromotionList")]
        public SalesMainPromotionListModel UpdateSalesMainPromotionList(SalesMainPromotionListModel value)
        {
            var masterIds = _context.ApplicationMaster.Where(s => s.ApplicationMasterCodeId == 234).Select(s => s.ApplicationMasterId).FirstOrDefault();
            var masterDetailList = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == masterIds).AsNoTracking().ToList();
            var SalesMainPromotionList = _context.SalesMainPromotionList.SingleOrDefault(p => p.SalesMainPromotionListId == value.SalesMainPromotionListId);
            SalesMainPromotionList.SalesPromotionId = value.SalesPromotionId;
            SalesMainPromotionList.IsFocList = value.IsFocList;
            SalesMainPromotionList.GenericCodeId = value.GenericCodeId;
            SalesMainPromotionList.ModifiedByUserId = value.ModifiedByUserID;
            SalesMainPromotionList.ModifiedDate = DateTime.Now;
            SalesMainPromotionList.StatusCodeId = value.StatusCodeID.Value;
            value.SalesMainPromotionListId = SalesMainPromotionList.SalesMainPromotionListId;

            _context.SaveChanges();
            if (value.GenericCodeId != null)
            {
                var genriccode = _context.GenericCodes.FirstOrDefault(s => s.GenericCodeId == value.GenericCodeId);
                if (genriccode != null)
                {
                    value.ItemUom = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == genriccode.Uom)?.Value;
                    value.GenericCode = genriccode.Code;
                }
            }
            return value;
        }


      
    }
}
