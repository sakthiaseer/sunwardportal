﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonProcessController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonProcess")]
        public List<CommonProcessModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonProcess = _context.CommonProcess.
               Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(s => s.StatusCode)
               .Include(p => p.Profile).Include(c => c.CommonProcessDosageMultiple)
               .AsNoTracking().ToList();
            List<CommonProcessModel> commonProcessModel = new List<CommonProcessModel>();
            commonProcess.ForEach(s =>
            {
                CommonProcessModel commonProcessModels = new CommonProcessModel
                {
                    CommonProcessId = s.CommonProcessId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DosageFormIds = s.CommonProcessDosageMultiple != null ? s.CommonProcessDosageMultiple.Select(d => d.DosageFormId).ToList() : new List<long?>(),
                    ManufacturingProcessId = s.ManufacturingProcessId,
                    ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                };
                commonProcessModel.Add(commonProcessModels);
            });
            commonProcessModel.ForEach(c =>
            {
                if (c.DosageFormIds.Count > 0)
                {
                    c.DosageFormName = masterDetailList.Count > 0 && masterDetailList.Where(m => c.DosageFormIds.Contains(m.ApplicationMasterDetailId)) != null ? string.Join(",", masterDetailList.Where(m => c.DosageFormIds.Contains(m.ApplicationMasterDetailId)).Select(e => e.Value)) : "";
                }
            });
            return commonProcessModel.Where(w => w.ItemClassificationMasterId == id).OrderByDescending(a => a.CommonProcessId).ToList();
        }
        [HttpGet]
        [Route("GetCommonProcessLineItems")]
        public List<CommonProcessLineModel> GetCommonProcessLineItems()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonProcessLine = _context.CommonProcessLine.
                Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode).
                Include(c => c.CommonProcessMachineGroupingMultiple)
                .AsNoTracking().ToList();
            List<CommonProcessLineModel> commonProcessLineModel = new List<CommonProcessLineModel>();
            if (commonProcessLine != null)
            {
                commonProcessLine.ForEach(s =>
                {
                    CommonProcessLineModel commonProcessLineModels = new CommonProcessLineModel();
                    commonProcessLineModels.CommonProcessLineId = s.CommonProcessLineId;
                    commonProcessLineModels.CommonProcessId = s.CommonProcessId;
                    commonProcessLineModels.ManufacturingStepsId = s.ManufacturingStepsId;
                    commonProcessLineModels.ManufacturingStepsName = s.ManufacturingStepsId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "";
                    commonProcessLineModels.NavisionNoSeriesId = s.NavisionNoSeriesId;
                    commonProcessLineModels.NavisionNoSeries = s.NavisionNoSeriesId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.NavisionNoSeriesId).FirstOrDefault().Value : "";
                    commonProcessLineModels.AddedByUserID = s.AddedByUserId;
                    commonProcessLineModels.ModifiedByUserID = s.ModifiedByUserId;
                    commonProcessLineModels.AddedDate = s.AddedDate;
                    commonProcessLineModels.ModifiedDate = s.ModifiedDate;
                    commonProcessLineModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                    commonProcessLineModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    commonProcessLineModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                    commonProcessLineModels.StatusCodeID = s.StatusCodeId;
                    commonProcessLineModels.MachineGroupingIDs = s.CommonProcessMachineGroupingMultiple != null ? s.CommonProcessMachineGroupingMultiple.Select(m => m.MachineGroupingId).ToList() : new List<long?>();
                    commonProcessLineModel.Add(commonProcessLineModels);
                });
            }
            commonProcessLineModel.ForEach(c =>
            {
                if (c.MachineGroupingIDs.Count > 0)
                {
                    c.MachineGroupingNames = masterDetailList.Count > 0 && masterDetailList.Where(m => c.MachineGroupingIDs.Contains(m.ApplicationMasterDetailId)) != null ? string.Join(",", masterDetailList.Where(m => c.MachineGroupingIDs.Contains(m.ApplicationMasterDetailId)).Select(e => e.Value)) : "";
                }
            });
            return commonProcessLineModel.OrderByDescending(a => a.CommonProcessLineId).ToList();

        }
        [HttpGet]
        [Route("GetCommonProcessLine")]
        public List<CommonProcessLineModel> CommonProcessLine(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonProcessLine = _context.CommonProcessLine.
                Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser).Include(c => c.CommonProcessMachineGroupingMultiple)
                .Include(s => s.StatusCode)
                .AsNoTracking().ToList();
            List<CommonProcessLineModel> commonProcessLineModel = new List<CommonProcessLineModel>();
            if (commonProcessLine != null)
            {
                commonProcessLine.ForEach(s =>
                {
                    CommonProcessLineModel commonProcessLineModels = new CommonProcessLineModel();
                    commonProcessLineModels.CommonProcessLineId = s.CommonProcessLineId;
                    commonProcessLineModels.CommonProcessId = s.CommonProcessId;
                    commonProcessLineModels.ManufacturingStepsId = s.ManufacturingStepsId;
                    commonProcessLineModels.ManufacturingStepsName = s.ManufacturingStepsId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "";
                    commonProcessLineModels.NavisionNoSeriesId = s.NavisionNoSeriesId;
                    commonProcessLineModels.NavisionNoSeries = s.NavisionNoSeriesId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.NavisionNoSeriesId).FirstOrDefault().Value : "";
                    commonProcessLineModels.AddedByUserID = s.AddedByUserId;
                    commonProcessLineModels.ModifiedByUserID = s.ModifiedByUserId;
                    commonProcessLineModels.AddedDate = s.AddedDate;
                    commonProcessLineModels.ModifiedDate = s.ModifiedDate;
                    commonProcessLineModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                    commonProcessLineModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    commonProcessLineModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                    commonProcessLineModels.StatusCodeID = s.StatusCodeId;
                    commonProcessLineModels.MachineGroupingIDs = s.CommonProcessMachineGroupingMultiple != null ? s.CommonProcessMachineGroupingMultiple.Select(m => m.MachineGroupingId).ToList() : new List<long?>();
                    commonProcessLineModel.Add(commonProcessLineModels);
                });
            }
            commonProcessLineModel.ForEach(c =>
            {
                if (c.MachineGroupingIDs.Count > 0)
                {
                    c.MachineGroupingNames = masterDetailList.Count > 0 && masterDetailList.Where(m => c.MachineGroupingIDs.Contains(m.ApplicationMasterDetailId)) != null ? string.Join(",", masterDetailList.Where(m => c.MachineGroupingIDs.Contains(m.ApplicationMasterDetailId)).Select(e => e.Value)) : "";
                }
            });
            return commonProcessLineModel.Where(w => w.CommonProcessId == id).OrderByDescending(a => a.CommonProcessLineId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonProcessModel> GetData(SearchModel searchModel)
        {
            var commonProcess = new CommonProcess();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).LastOrDefault();
                        break;
                    case "Next":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).LastOrDefault();
                        break;
                    case "Previous":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).LastOrDefault();
                        break;
                    case "Next":
                        commonProcess = _context.CommonProcess.OrderBy(o => o.CommonProcessId).FirstOrDefault(s => s.CommonProcessId > searchModel.Id);
                        break;
                    case "Previous":
                        commonProcess = _context.CommonProcess.OrderByDescending(o => o.CommonProcessId).FirstOrDefault(s => s.CommonProcessId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonProcessModel>(commonProcess);
            result.DosageFormIds = _context.CommonProcessDosageMultiple.Where(p => p.CommonProcessId == result.CommonProcessId) != null ? _context.CommonProcessDosageMultiple.Where(p => p.CommonProcessId == result.CommonProcessId).Select(d => d.DosageFormId).ToList() : new List<long?>();
            if (result.DosageFormIds.Count > 0)
            {
                result.DosageFormName = masterDetailList.Count > 0 && masterDetailList.Where(m => result.DosageFormIds.Contains(m.ApplicationMasterDetailId)) != null ? string.Join(",", masterDetailList.Where(m => result.DosageFormIds.Contains(m.ApplicationMasterDetailId)).Select(e => e.Value)) : "";
            };
            return result;
        }
        [HttpPost]
        [Route("InsertCommonProcess")]
        public CommonProcessModel Post(CommonProcessModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title=itemclassificationName });
            var commonProcess = new CommonProcess
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
            };

            if (value.DosageFormIds != null && value.DosageFormIds.Count > 0)
            {
                value.DosageFormIds.ForEach(d =>
                {
                    CommonProcessDosageMultiple commonProcessDosageMultiple = new CommonProcessDosageMultiple
                    {
                        DosageFormId = d,
                        CommonProcessId = commonProcess.CommonProcessId
                    };
                    commonProcess.CommonProcessDosageMultiple.Add(commonProcessDosageMultiple);
                });

            }

            _context.CommonProcess.Add(commonProcess);
            _context.SaveChanges();
            value.CommonProcessId = commonProcess.CommonProcessId;
            value.ProfileReferenceNo = commonProcess.ProfileReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonProcess")]
        public CommonProcessModel Put(CommonProcessModel value)
        {
            var commonProcess = _context.CommonProcess.SingleOrDefault(p => p.CommonProcessId == value.CommonProcessId);
            commonProcess.ManufacturingProcessId = value.ManufacturingProcessId;
            commonProcess.StatusCodeId = value.StatusCodeID.Value;
            commonProcess.ModifiedByUserId = value.ModifiedByUserID;
            commonProcess.ModifiedDate = DateTime.Now;
            commonProcess.ProfileReferenceNo = value.ProfileReferenceNo;
            commonProcess.ProfileId = value.ProfileId;
            var commonProcessDosageMultiples = _context.CommonProcessDosageMultiple.Where(l => l.CommonProcessId == value.CommonProcessId).ToList();
            if (commonProcessDosageMultiples.Count > 0)
            {
                _context.CommonProcessDosageMultiple.RemoveRange(commonProcessDosageMultiples);
            }
            if (value.DosageFormIds != null && value.DosageFormIds.Count > 0)
            {
                value.DosageFormIds.ForEach(d =>
                {
                    CommonProcessDosageMultiple commonProcessDosageMultiple = new CommonProcessDosageMultiple
                    {
                        DosageFormId = d,
                        CommonProcessId = commonProcess.CommonProcessId
                    };
                    commonProcess.CommonProcessDosageMultiple.Add(commonProcessDosageMultiple);
                });

            }
            _context.SaveChanges();
            return value;
        }
        [Route("InsertCommonProcessLine")]
        public CommonProcessLineModel CommonProcessLine(CommonProcessLineModel value)
        {
            var commonProcessLine = new CommonProcessLine
            {
                CommonProcessId = value.CommonProcessId,
                ManufacturingStepsId = value.ManufacturingStepsId,
                NavisionNoSeriesId = value.NavisionNoSeriesId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            if (value.MachineGroupingIDs != null && value.MachineGroupingIDs.Count > 0)
            {
                value.MachineGroupingIDs.ForEach(d =>
                {
                    CommonProcessMachineGroupingMultiple commonProcessMachineGroupingMultiple = new CommonProcessMachineGroupingMultiple
                    {
                        MachineGroupingId = d,
                    };
                    commonProcessLine.CommonProcessMachineGroupingMultiple.Add(commonProcessMachineGroupingMultiple);
                });

            }
            _context.CommonProcessLine.Add(commonProcessLine);
            _context.SaveChanges();
            value.CommonProcessLineId = commonProcessLine.CommonProcessLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonProcessLine")]
        public CommonProcessLineModel Put(CommonProcessLineModel value)
        {
            var commonProcessLine = _context.CommonProcessLine.SingleOrDefault(p => p.CommonProcessLineId == value.CommonProcessLineId);
            commonProcessLine.CommonProcessId = value.CommonProcessId;
            commonProcessLine.ManufacturingStepsId = value.ManufacturingStepsId;
            commonProcessLine.StatusCodeId = value.StatusCodeID.Value;
            commonProcessLine.ModifiedByUserId = value.ModifiedByUserID;
            commonProcessLine.ModifiedDate = DateTime.Now;
            commonProcessLine.NavisionNoSeriesId = value.NavisionNoSeriesId;
            var commonProcessMachineGroupingMultiples = _context.CommonProcessMachineGroupingMultiple.Where(l => l.CommonProcessLineId == value.CommonProcessLineId).ToList();
            if (commonProcessMachineGroupingMultiples.Count > 0)
            {
                _context.CommonProcessMachineGroupingMultiple.RemoveRange(commonProcessMachineGroupingMultiples);
            }
            if (value.MachineGroupingIDs != null && value.MachineGroupingIDs.Count > 0)
            {
                value.MachineGroupingIDs.ForEach(d =>
                {
                    CommonProcessMachineGroupingMultiple commonProcessMachineGroupingMultiple = new CommonProcessMachineGroupingMultiple
                    {
                        MachineGroupingId = d,
                        CommonProcessLineId = commonProcessLine.CommonProcessLineId
                    };
                    commonProcessLine.CommonProcessMachineGroupingMultiple.Add(commonProcessMachineGroupingMultiple);
                });

            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonProcess")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonProcess = _context.CommonProcess.Where(p => p.CommonProcessId == id).FirstOrDefault();
                if (commonProcess != null)
                {
                    var commonprocessLine = _context.CommonProcessLine.Where(l => l.CommonProcessId == id).ToList();
                    if (commonprocessLine.Count > 0)
                    {
                        commonprocessLine.ForEach(line =>
                        {
                            CommonProcessLine((int?)line.CommonProcessLineId);
                        });
                    }
                    var commonProcessDosageMultiples = _context.CommonProcessDosageMultiple.Where(l => l.CommonProcessId == commonProcess.CommonProcessId).ToList();
                    if (commonProcessDosageMultiples.Count > 0)
                    {
                        _context.CommonProcessDosageMultiple.RemoveRange(commonProcessDosageMultiples);
                    }
                    _context.CommonProcess.Remove(commonProcess);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteCommonProcessLine")]
        public ActionResult<string> CommonProcessLine(int id)
        {
            try
            {
                var commonprocessLine = _context.CommonProcessLine.Where(p => p.CommonProcessLineId == id).FirstOrDefault();
                if (commonprocessLine != null)
                {
                    _context.CommonProcessLine.Remove(commonprocessLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}