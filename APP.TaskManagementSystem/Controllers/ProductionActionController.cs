﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionActionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionActions")]
        public List<ProductionActionModel> Get()
        {
            List<ProductionActionModel> productionActionModels = new List<ProductionActionModel>();
            var productionAction = _context.ProductionAction.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.ProductionActionId).AsNoTracking().ToList();
            if(productionAction!=null && productionAction.Count > 0)
            {
                productionAction.ForEach(s =>
                {
                    ProductionActionModel productionActionModel = new ProductionActionModel
                    {
                        ProductionActionID = s.ProductionActionId,
                        Name = s.Name,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,                       

                    };
                    productionActionModels.Add(productionActionModel);
                });
            }

            return productionActionModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionActionModel> GetData(SearchModel searchModel)
        {
            var productionAction = new ProductionAction();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).FirstOrDefault();
                        break;
                    case "Last":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).LastOrDefault();
                        break;
                    case "Next":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).LastOrDefault();
                        break;
                    case "Previous":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).FirstOrDefault();
                        break;
                    case "Last":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).LastOrDefault();
                        break;
                    case "Next":
                        productionAction = _context.ProductionAction.OrderBy(o => o.ProductionActionId).FirstOrDefault(s => s.ProductionActionId > searchModel.Id);
                        break;
                    case "Previous":
                        productionAction = _context.ProductionAction.OrderByDescending(o => o.ProductionActionId).FirstOrDefault(s => s.ProductionActionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionActionModel>(productionAction);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionAction")]
        public ProductionActionModel Post(ProductionActionModel value)
        {
            var productionAction = new ProductionAction
            {
                //ProductionActionID = value.ProductionActionId,
                Name = value.Name,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };

            _context.ProductionAction.Add(productionAction);
            _context.SaveChanges();
            value.ProductionActionID = productionAction.ProductionActionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionAction")]
        public ProductionActionModel Put(ProductionActionModel value)
        {
            var productionAction = _context.ProductionAction.SingleOrDefault(p => p.ProductionActionId == value.ProductionActionID);

            productionAction.Name = value.Name;
           
            productionAction.ModifiedByUserId = value.ModifiedByUserID;
            productionAction.ModifiedDate = DateTime.Now;
            productionAction.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionAction")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var productionAction = _context.ProductionAction.SingleOrDefault(p => p.ProductionActionId == id);
                if (productionAction != null)
                {
                    _context.ProductionAction.Remove(productionAction);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}