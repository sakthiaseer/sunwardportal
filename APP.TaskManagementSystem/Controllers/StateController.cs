﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class StateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public StateController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetStates")]
        public List<StateModel> Get()
        {
            var State = _context.State.Include("AddedByUser").Include("ModifiedByUser").Include("Name").Select(s => new StateModel
            {
                StateID = s.StateId,
                CountryID = s.CountryId,
                CountryName = s.Country.Name,
                //CountryOptions = s.Country.Name,
                Name = s.Name,
                Code = s.Code,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.StateID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<StateModel>>(State);
            return State;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get State")]
        [HttpGet("GetStates/{id:int}")]
        public ActionResult<StateModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var State = _context.State.SingleOrDefault(p => p.StateId == id.Value);
            var result = _mapper.Map<StateModel>(State);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        // GET: api/Project
        [HttpGet("GetCountries/{id:int}")]
        public ActionResult<StateModel> GetCountry(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var state = _context.State.SingleOrDefault(p => p.CountryId == id.Value);
            var result = _mapper.Map<StateModel>(state);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<StateModel> GetData(SearchModel searchModel)
        {
            var State = new State();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        State = _context.State.OrderByDescending(o => o.StateId).FirstOrDefault();
                        break;
                    case "Last":
                        State = _context.State.OrderByDescending(o => o.StateId).LastOrDefault();
                        break;
                    case "Next":
                        State = _context.State.OrderByDescending(o => o.StateId).LastOrDefault();
                        break;
                    case "Previous":
                        State = _context.State.OrderByDescending(o => o.StateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        State = _context.State.OrderByDescending(o => o.StateId).FirstOrDefault();
                        break;
                    case "Last":
                        State = _context.State.OrderByDescending(o => o.StateId).LastOrDefault();
                        break;
                    case "Next":
                        State = _context.State.OrderBy(o => o.StateId).FirstOrDefault(s => s.StateId > searchModel.Id);
                        break;
                    case "Previous":
                        State = _context.State.OrderByDescending(o => o.StateId).FirstOrDefault(s => s.StateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<StateModel>(State);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertState")]
        public StateModel Post(StateModel value)
        {
            var State = new State
            {
                //StateId = value.StateID,
                //CountryOptions = value.CountryOptions,
                CountryId = value.CountryID,
                Name = value.Name,
                Code = value.Code,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.State.Add(State);
            _context.SaveChanges();
            value.StateID = State.StateId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateState")]
        public StateModel Put(StateModel value)
        {
            var State = _context.State.SingleOrDefault(p => p.StateId == value.StateID);
            //State.StateId = value.StateID;
            State.CountryId = value.CountryID;
            State.ModifiedByUserId = value.ModifiedByUserID;
            State.ModifiedDate = DateTime.Now;
            State.Name = value.Name;
            State.Code = value.Code;
            //State.AddedByUserId = value.AddedByUserID;
            // State.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            State.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteState")]
        public void Delete(int id)
        {
            var State = _context.State.SingleOrDefault(p => p.StateId == id);
            if (State != null)
            {
                _context.State.Remove(State);
                _context.SaveChanges();
            }
        }
    }
}