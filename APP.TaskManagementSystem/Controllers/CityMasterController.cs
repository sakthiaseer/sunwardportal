﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]

    public class CityMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CityMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCityMasters")]
        public List<CityMasterModel> Get()
        {
            var cityMaster = _context.CityMaster.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CityMasterModel> cityMasterModels = new List<CityMasterModel>();
            cityMaster.ForEach(s =>
            {
                CityMasterModel cityMasterModel = new CityMasterModel
                {
                    CityID = s.CityId,
                    Name = s.Name,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                cityMasterModels.Add(cityMasterModel);
            });
            return cityMasterModels.OrderByDescending(a => a.CityID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get CityMaster")]
        [HttpGet("GetCityMasters/{id:int}")]
        public ActionResult<CityMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var cityMaster = _context.CityMaster.SingleOrDefault(p => p.CityId == id.Value);
            var result = _mapper.Map<CityMasterModel>(cityMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CityMasterModel> GetData(SearchModel searchModel)
        {
            var cityMaster = new CityMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).FirstOrDefault();
                        break;
                    case "Last":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).LastOrDefault();
                        break;
                    case "Next":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).LastOrDefault();
                        break;
                    case "Previous":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).FirstOrDefault();
                        break;
                    case "Last":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).LastOrDefault();
                        break;
                    case "Next":
                        cityMaster = _context.CityMaster.OrderBy(o => o.CityId).FirstOrDefault(s => s.CityId > searchModel.Id);
                        break;
                    case "Previous":
                        cityMaster = _context.CityMaster.OrderByDescending(o => o.CityId).FirstOrDefault(s => s.CityId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CityMasterModel>(cityMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCityMaster")]
        public CityMasterModel Post(CityMasterModel value)
        {
            var cityMaster = new CityMaster
            {
                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.CityMaster.Add(cityMaster);
            _context.SaveChanges();
            value.CityID = cityMaster.CityId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCityMaster")]
        public CityMasterModel Put(CityMasterModel value)
        {
            var cityMaster = _context.CityMaster.SingleOrDefault(p => p.CityId == value.CityID);

            cityMaster.Name = value.Name;
            cityMaster.Description = value.Description;
            cityMaster.ModifiedByUserId = value.ModifiedByUserID;
            cityMaster.ModifiedDate = DateTime.Now;
            cityMaster.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCityMaster")]
        public void Delete(int id)
        {
            var cityMaster = _context.CityMaster.SingleOrDefault(p => p.CityId == id);
            if (cityMaster != null)
            {
                _context.CityMaster.Remove(cityMaster);
                _context.SaveChanges();
            }
        }
    }
}