﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductGroupSurveyController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductGroupSurveyController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetProductGroupSurvey")]
        public List<ProductGroupSurveyModel> Get()
        {
            List<ProductGroupSurveyModel> productGroupSurveyModels = new List<ProductGroupSurveyModel>();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productGroupSurvey = _context.ProductGroupSurvey.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.StatusCode).Include(d => d.Company).AsNoTracking().ToList();
            productGroupSurvey.ForEach(s =>
            {
                ProductGroupSurveyModel productGroupSurveyModel = new ProductGroupSurveyModel();
                productGroupSurveyModel.ProductGroupSurveyId = s.ProductGroupSurveyId;
                productGroupSurveyModel.PharmacologicalCategoryId = s.PharmacologicalCategoryId;
                productGroupSurveyModel.CompanyId = s.CompanyId;
                productGroupSurveyModel.SurveyDate = s.SurveyDate;
                productGroupSurveyModel.CompanyName = s.Company?.Description;
                productGroupSurveyModel.PharmacologicalCategory = s.PharmacologicalCategoryId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PharmacologicalCategoryId).Select(m => m.Value).FirstOrDefault() : "");
                productGroupSurveyModel.AddedDate = s.AddedDate;
                productGroupSurveyModel.ModifiedDate = s.ModifiedDate;
                productGroupSurveyModel.AddedByUser = s.AddedByUser?.UserName;
                productGroupSurveyModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                productGroupSurveyModel.StatusCode = s.StatusCode?.CodeValue;
                productGroupSurveyModel.StatusCodeID = s.StatusCodeId;
                productGroupSurveyModel.ProductGroupSurveyName = productGroupSurveyModel.CompanyName + "|"+ s.SurveyDate.Value.ToString("MMM - yyy") +"|"+ productGroupSurveyModel.PharmacologicalCategory;
                productGroupSurveyModels.Add(productGroupSurveyModel);

            });
            return productGroupSurveyModels.OrderByDescending(o => o.ProductGroupSurveyId).ToList();
        }
        [HttpGet]
        [Route("GetProductGroupSurveyLine")]
        public List<ProductGroupSurveyLineModel> GetProductGroupSurveyLine(long? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<ProductGroupSurveyLineModel> productGroupSurveyLineModels = new List<ProductGroupSurveyLineModel>();
            var productGroupSurveyLine = _context.ProductGroupSurveyLine.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.StatusCode).Where(w => w.ProductGroupSurveyId == id).AsNoTracking().ToList();
            productGroupSurveyLine.ForEach(s =>
            {
                ProductGroupSurveyLineModel productGroupSurveyLineModel = new ProductGroupSurveyLineModel();
                productGroupSurveyLineModel.ProductGroupSurveyLineId = s.ProductGroupSurveyLineId;
                productGroupSurveyLineModel.ProductGroupSurveyId = s.ProductGroupSurveyId;
                productGroupSurveyLineModel.ProductGroupId = s.ProductGroupId;
                productGroupSurveyLineModel.ProductGroupName = s.ProductGroupId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ProductGroupId).Select(m => m.Value).FirstOrDefault() : "";
                productGroupSurveyLineModel.AddedDate = s.AddedDate;
                productGroupSurveyLineModel.ModifiedDate = s.ModifiedDate;
                productGroupSurveyLineModel.AddedByUser = s.AddedByUser?.UserName;
                productGroupSurveyLineModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                productGroupSurveyLineModel.StatusCode = s.StatusCode?.CodeValue;
                productGroupSurveyLineModel.StatusCodeID = s.StatusCodeId;
                productGroupSurveyLineModels.Add(productGroupSurveyLineModel);
            });
            return productGroupSurveyLineModels.OrderByDescending(o => o.ProductGroupSurveyLineId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductGroupSurveyModel> GetData(SearchModel searchModel)
        {
            var productGroupSurvey = new ProductGroupSurvey();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).FirstOrDefault();
                        break;
                    case "Last":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).LastOrDefault();
                        break;
                    case "Next":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).LastOrDefault();
                        break;
                    case "Previous":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).FirstOrDefault();
                        break;
                    case "Last":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).LastOrDefault();
                        break;
                    case "Next":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderBy(o => o.ProductGroupSurveyId).FirstOrDefault(s => s.ProductGroupSurveyId > searchModel.Id);
                        break;
                    case "Previous":
                        productGroupSurvey = _context.ProductGroupSurvey.OrderByDescending(o => o.ProductGroupSurveyId).FirstOrDefault(s => s.ProductGroupSurveyId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductGroupSurveyModel>(productGroupSurvey);
            return result;
        }
        [HttpPost]
        [Route("InsertProductGroupSurvey")]
        public ProductGroupSurveyModel Post(ProductGroupSurveyModel value)
        {
            var productGroupSurvey = new ProductGroupSurvey
            {
                CompanyId = value.CompanyId,
                SurveyDate = value.SurveyDate,
                PharmacologicalCategoryId = value.PharmacologicalCategoryId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.ProductGroupSurvey.Add(productGroupSurvey);
            _context.SaveChanges();
            value.ProductGroupSurveyId = productGroupSurvey.ProductGroupSurveyId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductGroupSurvey")]
        public ProductGroupSurveyModel Put(ProductGroupSurveyModel value)
        {
            var productGroupSurvey = _context.ProductGroupSurvey.SingleOrDefault(p => p.ProductGroupSurveyId == value.ProductGroupSurveyId);
            productGroupSurvey.CompanyId = value.CompanyId;
            productGroupSurvey.SurveyDate = value.SurveyDate;
            productGroupSurvey.PharmacologicalCategoryId = value.PharmacologicalCategoryId;
            productGroupSurvey.ModifiedByUserId = value.ModifiedByUserID;
            productGroupSurvey.StatusCodeId = value.StatusCodeID.Value;
            productGroupSurvey.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertProductGroupSurveyLine")]
        public ProductGroupSurveyLineModel InsertProductGroupSurveyLine(ProductGroupSurveyLineModel value)
        {
            var productGroupSurveyLine = new ProductGroupSurveyLine
            {
                ProductGroupSurveyId = value.ProductGroupSurveyId,
                ProductGroupId = value.ProductGroupId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.ProductGroupSurveyLine.Add(productGroupSurveyLine);
            _context.SaveChanges();
            value.ProductGroupSurveyLineId = productGroupSurveyLine.ProductGroupSurveyLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductGroupSurveyLine")]
        public ProductGroupSurveyLineModel UpdateProductGroupSurveyLine(ProductGroupSurveyLineModel value)
        {
            var productGroupSurveyLine = _context.ProductGroupSurveyLine.SingleOrDefault(p => p.ProductGroupSurveyLineId == value.ProductGroupSurveyLineId);
            productGroupSurveyLine.ProductGroupSurveyId = value.ProductGroupSurveyId;
            productGroupSurveyLine.ProductGroupId = value.ProductGroupId;
            productGroupSurveyLine.ModifiedByUserId = value.ModifiedByUserID;
            productGroupSurveyLine.StatusCodeId = value.StatusCodeID.Value;
            productGroupSurveyLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductGroupSurvey")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var productGroupSurvey = _context.ProductGroupSurvey.Where(p => p.ProductGroupSurveyId == id).FirstOrDefault();
                if (productGroupSurvey != null)
                {
                    var productGroupSurveyLine = _context.ProductGroupSurveyLine.Where(p => p.ProductGroupSurveyId == id).AsNoTracking().ToList();
                    if (productGroupSurveyLine != null)
                    {
                        _context.ProductGroupSurveyLine.RemoveRange(productGroupSurveyLine);
                        _context.SaveChanges();
                    }
                    _context.ProductGroupSurvey.Remove(productGroupSurvey);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteProductGroupSurveyLine")]
        public ActionResult<string> DeleteProductGroupSurveyLine(int id)
        {
            try
            {
                var productGroupSurveyLine = _context.ProductGroupSurveyLine.SingleOrDefault(p => p.ProductGroupSurveyLineId == id);
                if (productGroupSurveyLine != null)
                {
                    _context.ProductGroupSurveyLine.Remove(productGroupSurveyLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
