using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Http;

using APP.TaskManagementSystem.Helper;
using APP.Common;
using System.Text.Json;
using APP.BussinessObject;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionBasicCycleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly TableVersionRepository _repository;
        public ProductionBasicCycleController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
            _repository = repository;
        }

        // GET: api/GetMonthsAndYears
        [HttpGet]
        [Route("GetMonthsAndYears")]
        public List<ProdMonthAndYearModel> Get()
        {
            ProdMonthAndYearModel productionbase;
            List<ProdMonthAndYearModel> ProductionBasic = new List<ProdMonthAndYearModel>();
            DateTime months = DateTime.Now;
            int Years = months.Year;
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Jan - " + Years;
            productionbase.MonthId = "Jan";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Feb - " + Years;
            productionbase.MonthId = "Feb";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Mar - " + Years;
            productionbase.MonthId = "Mar";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Apr - " + Years;
            productionbase.MonthId = "Apr";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "May - " + Years;
            productionbase.MonthId = "May";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Jun - " + Years;
            productionbase.MonthId = "Jun";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Jul - " + Years;
            productionbase.MonthId = "Jul";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Aug - " + Years;
            productionbase.MonthId = "Aug";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Sep - " + Years;
            productionbase.MonthId = "Sep";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Oct - " + Years;
            productionbase.MonthId = "Oct";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Nov - " + Years;
            productionbase.MonthId = "Nov";
            ProductionBasic.Add(productionbase);
            productionbase = new ProdMonthAndYearModel();
            productionbase.MonthName = "Dec - " + Years;
            productionbase.MonthId = "Dec";
            ProductionBasic.Add(productionbase);

            return ProductionBasic;


        }
        [HttpGet]
        [Route("GetProductionCycleVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ProductionCycleModel>>> GetProductionCycleVersion(string sessionID)
        {
            return await _repository.GetList<ProductionCycleModel>(sessionID);
        }
        [HttpGet]
        [Route("GetProdBasicCycle")]
        public List<ProductionCycleModel> GetProdBasicCycle()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProductionCycleModel> productionCycleModels = new List<ProductionCycleModel>();
            var prodCycle = _context.ProductionCycle.Include(p => p.Company).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).OrderByDescending(o => o.ProdCycleId).AsNoTracking().ToList();
            if (prodCycle != null && prodCycle.Count > 0)
            {
                prodCycle.ForEach(s =>
                {
                    ProductionCycleModel productionCycleModel = new ProductionCycleModel
                    {
                        ProdCycleId = s.ProdCycleId,
                        CompanyId = s.CompanyId,
                        CompanyName = s.Company?.PlantCode,
                        MasterFileId = s.MasterFileId,
                        MasterFileName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MasterFileId).Select(a => a.Value).SingleOrDefault() : "",
                        FileName = s.FileName,
                        DraftVersion = s.DraftVersion,
                        ReleaseVersion = s.ReleaseVersion,
                        ValidPeriodFromDate = s.ValidPeriodFromDate,
                        ValidPeriodToDate = s.ValidPeriodToDate,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        VersionSessionId = s.VersionSessionId,

                    };
                    productionCycleModels.Add(productionCycleModel);
                });
            }


            return productionCycleModels;
        }

        [HttpGet]
        [Route("GetProdRecipeCycleByID")]
        public List<ProdRecipeCycleModel> GetProdRecipeCycleByID(long id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProdRecipeCycleModel> prodRecipeCycleModels = new List<ProdRecipeCycleModel>();
            var prodRecipeCycleLine = _context.ProdRecipeCycle.Include(m => m.MethodCode).Include(b => b.BatchSize).OrderByDescending(o => o.ProdRecipeCycleId).Where(t => t.ProdCycleId == id).AsNoTracking().ToList();
            if (prodRecipeCycleLine != null && prodRecipeCycleLine.Count > 0)
            {
                prodRecipeCycleLine.ForEach(s =>
                {
                    ProdRecipeCycleModel prodRecipeCycleModel = new ProdRecipeCycleModel
                    {
                        ProdRecipeCycleId = s.ProdRecipeCycleId,
                        ProdCycleId = s.ProdCycleId,
                        ItemTypeId = "" + s.ItemTypeId,
                        DosageFormId = s.ItemTypeId,
                        ItemType = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemTypeId).Select(a => a.Value).SingleOrDefault() : "",
                        SalesCategoryId = s.SalesCategoryId,
                        MethodCodeId = s.MethodCodeId,
                        MethodName = s.MethodCode?.MethodName,
                        BatchSizeId = s.BatchSizeId,
                        BatchSizeName = s.BatchSize?.BatchSize,
                        NoOfTickets = s.NoOfTickets,
                        PlanningMonth = s.PlanningMonth,
                        ResonForChangeId = s.ResonForChangeId,
                        SessionId = s.SessionId,
                        RreasonForChange = s.RreasonForChange,
                        Months = (s.PlanningMonth.Split(',', StringSplitOptions.None)).ToList(),
                        CycleMethodId = s.CycleMethodId,
                        ByMonth = s.ByMonth,
                        SpecificMonthOfTheYear = s.SpecificMonthOfTheYear,
                        MustFollow = s.MustFollow,
                        Notes = s.Notes,

                    };
                    prodRecipeCycleModels.Add(prodRecipeCycleModel);
                });

            }
            return prodRecipeCycleModels;
        }
        [HttpGet]
        [Route("GetNAVMethodCodeBatchByID")]
        public List<NavMethodCodeBatchModel> GetNAVMethodCodeBatchByID(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavMethodCodeBatchModel> navMethodCodeBatchModels = new List<NavMethodCodeBatchModel>();

            var navmethodCodeBatch = _context.NavmethodCodeBatch.OrderByDescending(o => o.MethodCodeBatchId).Where(t => t.NavMethodCodeId == id).AsNoTracking().ToList();
            if (navmethodCodeBatch != null && navmethodCodeBatch.Count > 0)
            {
                navmethodCodeBatch.ForEach(s =>
                {
                    NavMethodCodeBatchModel navMethodCodeBatchModel = new NavMethodCodeBatchModel
                    {
                        MethodCodeBatchId = s.MethodCodeBatchId,
                        NavMethodCodeId = s.NavMethodCodeId,
                        BatchSize = s.BatchSize,
                        DefaultBatchSize = s.DefaultBatchSize,
                        BatchUnitSize = s.BatchUnitSize,
                    };
                    navMethodCodeBatchModels.Add(navMethodCodeBatchModel);
                });

            }
            return navMethodCodeBatchModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionCycleModel> GetData(SearchModel searchModel)
        {
            var navSalesCategory = _context.NavSaleCategory.AsNoTracking().ToList();
            var methodCode = _context.NavMethodCode.AsNoTracking().ToList();
            var productionCycle = new ProductionCycle();
            var productionCycleModel = new ProductionCycleModel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).LastOrDefault();
                        break;
                    case "Next":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).LastOrDefault();
                        break;
                    case "Previous":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).LastOrDefault();
                        break;
                    case "Next":
                        productionCycle = _context.ProductionCycle.OrderBy(o => o.ProdCycleId).FirstOrDefault(s => s.ProdCycleId > searchModel.Id);
                        break;
                    case "Previous":
                        productionCycle = _context.ProductionCycle.OrderByDescending(o => o.ProdCycleId).FirstOrDefault(s => s.ProdCycleId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionCycleModel>(productionCycle);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertProductionCycle")]
        public ProductionCycleModel Post(ProductionCycleModel value)
        {
            var SessionId = Guid.NewGuid();
            var productionCycle = new ProductionCycle
            {
                CompanyId = value.CompanyId,
                MasterFileId = value.MasterFileId,
                FileName = value.FileName,
                ValidPeriodFromDate = value.ValidPeriodFromDate,
                ValidPeriodToDate = value.ValidPeriodToDate,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                VersionSessionId = SessionId,

            };
            if (productionCycle.StatusCodeId == 1410)
            {
                productionCycle.DraftVersion = 0;
                productionCycle.ReleaseVersion = 1;
            }
            if (productionCycle.StatusCodeId == 1411)
            {
                productionCycle.ReleaseVersion = 1;
                productionCycle.DraftVersion = 0;

            }

            _context.ProductionCycle.Add(productionCycle);
            _context.SaveChanges();
            value.ProdCycleId = productionCycle.ProdCycleId;
            return value;


        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionCycle")]
        public async Task<ProductionCycleModel> Put(ProductionCycleModel value)
        {
            value.VersionSessionId ??= Guid.NewGuid();
            var productionCycle = _context.ProductionCycle.SingleOrDefault(p => p.ProdCycleId == value.ProdCycleId);
            productionCycle.CompanyId = value.CompanyId;
            productionCycle.MasterFileId = value.MasterFileId;

            productionCycle.FileName = value.FileName;
            if (value.StatusCodeID == 1410)
            {
                productionCycle.DraftVersion = value.DraftVersion + 1;
            }
            if (value.StatusCodeID == 1411)
            {
                productionCycle.ReleaseVersion = value.ReleaseVersion + 1;
            }

            productionCycle.ValidPeriodFromDate = value.ValidPeriodFromDate;
            productionCycle.ValidPeriodToDate = value.ValidPeriodToDate;
            productionCycle.ModifiedByUserId = value.ModifiedByUserID;
            productionCycle.ModifiedDate = DateTime.Now;
            productionCycle.StatusCodeId = value.StatusCodeID.Value;
            productionCycle.VersionSessionId = value.VersionSessionId;
            _context.SaveChanges();

            return value;
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<ProductionCycleModel> CreateVersion(ProductionCycleModel value)
        {
            value.VersionSessionId = value.VersionSessionId.Value;
            var ProductionCycle = _context.ProductionCycle.SingleOrDefault(p => p.ProdCycleId == value.ProdCycleId);
            if (ProductionCycle != null)
            {
                var verObject = _mapper.Map<ProductionCycleModel>(ProductionCycle);
                verObject.ProdReceipeLine = GetProdRecipeCycleByID(value.ProdCycleId);
                var versionInfo = new TableDataVersionInfoModel<ProductionCycleModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ProductionCycle",
                    PrimaryKey = value.ProdCycleId,
                    ReferenceInfo = value.ReferenceInfo
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("ApplyVersion")]
        public ProductionCycleModel ApplyVersion(ProductionCycleModel value)
        {
            var productionCycle = _context.ProductionCycle.Include(m => m.ProdRecipeCycle).SingleOrDefault(p => p.ProdCycleId == value.ProdCycleId);
            productionCycle.CompanyId = value.CompanyId;
            productionCycle.MasterFileId = value.MasterFileId;
            productionCycle.FileName = value.FileName;
            productionCycle.DraftVersion = value.DraftVersion;
            productionCycle.ReleaseVersion = value.ReleaseVersion;
            productionCycle.ValidPeriodFromDate = value.ValidPeriodFromDate;
            productionCycle.ValidPeriodToDate = value.ValidPeriodToDate;
            productionCycle.ModifiedByUserId = value.ModifiedByUserID;
            productionCycle.ModifiedDate = DateTime.Now;
            productionCycle.StatusCodeId = value.StatusCodeID.Value;
            productionCycle.VersionSessionId = value.VersionSessionId;
            _context.SaveChanges();

            if (value.ProdReceipeLine != null)
            {
                _context.ProdRecipeCycle.RemoveRange(productionCycle.ProdRecipeCycle);
                _context.SaveChanges();
            }
            if (value.ProdReceipeLine != null)
            {
                foreach (var lineModel in value.ProdReceipeLine)
                {
                    var ProdRecipeCycle = new ProdRecipeCycle
                    {
                        ProdCycleId = lineModel.ProdCycleId,
                        MethodCodeId = lineModel.MethodCodeId,
                        BatchSizeId = lineModel.BatchSizeId,
                        NoOfTickets = lineModel.NoOfTickets,
                        ResonForChangeId = lineModel.ResonForChangeId,
                        PlanningMonth = String.Join(",", lineModel.Months.ToArray()),
                        SessionId = lineModel.SessionId,
                        RreasonForChange = lineModel.RreasonForChange,
                        CycleMethodId = lineModel.CycleMethodId,
                        ByMonth = lineModel.ByMonth,
                        SpecificMonthOfTheYear = lineModel.SpecificMonthOfTheYear,
                        Notes = lineModel.Notes,
                        MustFollow = lineModel.MustFollow,
                    };
                    _context.ProdRecipeCycle.Add(ProdRecipeCycle);
                }
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(ProductionCycleModel value)
        {
            value.VersionSessionId ??= value.VersionSessionId.Value;
            var ProductionCycle = _context.ProductionCycle.SingleOrDefault(p => p.ProdCycleId == value.ProdCycleId);

            if (ProductionCycle != null)
            {
                var verObject = _mapper.Map<ProductionCycleModel>(ProductionCycle);
                verObject.ProdReceipeLine = GetProdRecipeCycleByID(value.ProdCycleId);
                var versionInfo = new TableDataVersionInfoModel<ProductionCycleModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ProductionCycle",
                    PrimaryKey = value.ProdCycleId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<ProductionCycleModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<ProductionCycleModel>(tableData.JsonData);

                    var ProductionCycle = _context.ProductionCycle.Include(m => m.ProdRecipeCycle).SingleOrDefault(p => p.ProdCycleId == verObject.ProdCycleId);


                    if (verObject != null && ProductionCycle != null)
                    {
                        ProductionCycle.CompanyId = verObject.CompanyId;
                        ProductionCycle.MasterFileId = verObject.MasterFileId;
                        ProductionCycle.FileName = verObject.FileName;
                        ProductionCycle.DraftVersion = verObject.DraftVersion;
                        ProductionCycle.ReleaseVersion = verObject.ReleaseVersion;
                        ProductionCycle.ValidPeriodFromDate = verObject.ValidPeriodFromDate;
                        ProductionCycle.ValidPeriodToDate = verObject.ValidPeriodToDate;
                        ProductionCycle.ModifiedByUserId = verObject.ModifiedByUserID;
                        ProductionCycle.ModifiedDate = DateTime.Now;
                        ProductionCycle.StatusCodeId = verObject.StatusCodeID.Value;
                        ProductionCycle.VersionSessionId = verObject.VersionSessionId;
                        _context.SaveChanges();
                        if (verObject.ProdReceipeLine != null)
                        {
                            _context.ProdRecipeCycle.RemoveRange(ProductionCycle.ProdRecipeCycle);
                            _context.SaveChanges();
                        }
                        if (verObject.ProdReceipeLine != null)
                        {
                            foreach (var lineModel in verObject.ProdReceipeLine)
                            {
                                var ProdRecipeCycle = new ProdRecipeCycle
                                {
                                    ProdCycleId = lineModel.ProdCycleId,
                                    MethodCodeId = lineModel.MethodCodeId,
                                    BatchSizeId = lineModel.BatchSizeId,
                                    NoOfTickets = lineModel.NoOfTickets,
                                    ResonForChangeId = lineModel.ResonForChangeId,
                                    PlanningMonth = String.Join(",", lineModel.Months.ToArray()),
                                    SessionId = lineModel.SessionId,
                                    RreasonForChange = lineModel.RreasonForChange,
                                    CycleMethodId = lineModel.CycleMethodId,
                                    ByMonth = lineModel.ByMonth,
                                    SpecificMonthOfTheYear = lineModel.SpecificMonthOfTheYear,
                                    Notes = lineModel.Notes,
                                    MustFollow = lineModel.MustFollow,
                                };
                                _context.ProdRecipeCycle.Add(ProdRecipeCycle);
                            }
                            _context.SaveChanges();
                        }
                    }
                    return verObject;
                }
                return new ProductionCycleModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionCycle")]
        public void Delete(int id)
        {
            // ProductionCycleModel
            var productionCycle = _context.ProductionCycle.SingleOrDefault(p => p.ProdCycleId == id);
            if (productionCycle != null)
            {
                var prodRecipeCycle = _context.ProdRecipeCycle.Where(prc => prc.ProdCycleId == productionCycle.ProdCycleId).AsNoTracking().ToList();
                if (prodRecipeCycle != null)
                {
                    var query = string.Format("delete from ProductRecipeDocument Where SessionId='{0}'", String.Join(',', prodRecipeCycle.Select(s => s.SessionId)));
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to delete document");
                    }
                    _context.ProdRecipeCycle.RemoveRange(prodRecipeCycle);
                    _context.SaveChanges();
                }

                _context.ProductionCycle.Remove(productionCycle);
                _context.SaveChanges();
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProdRecipeCycle")]
        public void DeleteProdRecipeCycle(int id)
        {

            var prodRecipeCycle = _context.ProdRecipeCycle.SingleOrDefault(p => p.ProdRecipeCycleId == id);
            if (prodRecipeCycle != null)
            {
                var query = string.Format("delete from ProductRecipeDocument Where SessionId =" + "'" + "{0}" + "'", prodRecipeCycle.SessionId);

                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
                _context.ProdRecipeCycle.Remove(prodRecipeCycle);
                _context.SaveChanges();

            }
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertProdRecipeCycle")]
        public ProdRecipeCycleModel PostProdRecipeCycle(ProdRecipeCycleModel value)
        {
            var SessionId = Guid.NewGuid();
            var prodRecipeCycle = new ProdRecipeCycle
            {
                ProdCycleId = value.ProdCycleId,
                MethodCodeId = value.MethodCodeId,
                BatchSizeId = value.BatchSizeId,
                NoOfTickets = value.NoOfTickets,
                ResonForChangeId = value.ResonForChangeId,
                PlanningMonth = String.Join(",", value.Months.ToArray()),
                SessionId = SessionId,
                RreasonForChange = value.RreasonForChange,
                CycleMethodId = value.CycleMethodId,
                ByMonth = value.ByMonth,
                SpecificMonthOfTheYear = value.SpecificMonthOfTheYear,
                Notes = value.Notes,
                MustFollow = value.MustFollow,
            };
            _context.ProdRecipeCycle.Add(prodRecipeCycle);
            _context.SaveChanges();
            value.ProdRecipeCycleId = prodRecipeCycle.ProdRecipeCycleId;
            value.SessionId = SessionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProdRecipeCycle")]
        public ProdRecipeCycleModel PutProdRecipeCycle(ProdRecipeCycleModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionId == null)
            {
                value.SessionId = SessionId;
            }
            var prodRecipeCycle = _context.ProdRecipeCycle.SingleOrDefault(p => p.ProdRecipeCycleId == value.ProdRecipeCycleId);
            prodRecipeCycle.ProdCycleId = value.ProdCycleId;
            prodRecipeCycle.MethodCodeId = value.MethodCodeId;
            prodRecipeCycle.BatchSizeId = value.BatchSizeId;
            prodRecipeCycle.NoOfTickets = value.NoOfTickets;
            prodRecipeCycle.ResonForChangeId = value.ResonForChangeId;
            prodRecipeCycle.PlanningMonth = String.Join(",", value.Months.ToArray());
            prodRecipeCycle.SessionId = value.SessionId;
            prodRecipeCycle.CycleMethodId = value.CycleMethodId;
            prodRecipeCycle.ByMonth = value.ByMonth;
            prodRecipeCycle.SpecificMonthOfTheYear = value.SpecificMonthOfTheYear;
            prodRecipeCycle.Notes = value.Notes;
            prodRecipeCycle.MustFollow = value.MustFollow;
            _context.SaveChanges();
            return value;
        }




        //

        [HttpPost]
        [Route("UploadProductionBasicCycleLineDocuments")]
        public IActionResult UploadProductionBasicCycleLineDocuments(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new ProductRecipeDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.ProductRecipeDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetProductionBasicCycleLineDocuments")]
        public List<ProductRecipeDocumentModel> GetProductionBasicCycleLineDocuments(int? id)
        {
            var prodcutRecipe = _context.ProdRecipeCycle.Where(w => w.ProdRecipeCycleId == id).Select(s => s.SessionId).FirstOrDefault();
            var query = _context.ProductRecipeDocument.Select(s => new ProductRecipeDocumentModel
            {
                SessionId = s.SessionId,
                ProductRecipeDocumentId = s.ProductRecipeDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == prodcutRecipe).OrderByDescending(o => o.ProductRecipeDocumentId).AsNoTracking().ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadProductionBasicCycleLineDocuments")]
        public IActionResult DownLoadProductionBasicCycleLineDocuments(long id)
        {
            var document = _context.ProductRecipeDocument.SingleOrDefault(t => t.ProductRecipeDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteProductionBasicCycleLineDocuments")]
        public void DeleteProductionBasicCycleLineDocuments(int id)
        {
            var query = string.Format("delete from ProductRecipeDocument Where ProductRecipeDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }

        [HttpGet]
        [Route("GetProductionBasicCycleLineDocumentsById")]
        public ProductRecipeDocumentModel GetProductionBasicCycleLineDocumentsById(long id)
        {

            ProductRecipeDocumentModel LineDocumentModel = new ProductRecipeDocumentModel();
            var document = _context.ProductRecipeDocument.FirstOrDefault(t => t.ProductRecipeDocumentId == id);
            if (document != null)
            {


                LineDocumentModel.ProductRecipeDocumentId = document.ProductRecipeDocumentId;
                LineDocumentModel.SessionId = document.SessionId;
                LineDocumentModel.ContentType = document.ContentType;
                LineDocumentModel.FileName = document.FileName;
                LineDocumentModel.FileData = document.FileData;


            }
            return LineDocumentModel;
        }




        [HttpGet]
        [Route("GetNavMethodCode")]
        public List<NavMethodCodeModel> GetNavMethodCode(int? id)
        {
            List<NavMethodCodeModel> navMethodCodeModels = new List<NavMethodCodeModel>();
            var navMethodCode = _context.NavMethodCode.Where(w => w.CompanyId == id).OrderByDescending(o => o.MethodCodeId).AsNoTracking().ToList();
            if (navMethodCode != null && navMethodCode.Count > 0)
            {
                navMethodCode.ForEach(s =>
                {
                    NavMethodCodeModel navMethodCodeModel = new NavMethodCodeModel
                    {
                        MethodCodeID = s.MethodCodeId,
                        MethodName = s.MethodName,
                        CompanyId = s.CompanyId,
                        MethodDescription = s.MethodDescription,
                        NavinpCategoryID = s.NavinpcategoryId,
                        ItemIds = s.NavMethodCodeLines != null ? s.NavMethodCodeLines.Where(al => al.MethodCodeId == s.MethodCodeId).Select(al => al.ItemId).ToList() : new List<long?>(),
                        ItemNos = String.Join("|", s.NavMethodCodeLines.Where(al => al.MethodCodeId == s.MethodCodeId).Select(al => al.Item.No)),
                    };
                    navMethodCodeModels.Add(navMethodCodeModel);
                });
            }

            // var methodCodeIds = navMethodCodeModels.Select(m => m.MethodCodeID).ToList();
            // var navMethodCodeBatch = _context.NavmethodCodeBatch.Where(m => methodCodeIds.Contains(m.NavMethodCodeId)).AsNoTracking().ToList();
            // navMethodCodeModels.ForEach(nav =>
            // {
            //     nav.BatchSizeIds = navMethodCodeBatch.Where(b => b.NavMethodCodeId == nav.MethodCodeID).Select(bs => bs.BatchSize).ToList();
            //     nav.BatchSizeId = navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID) != null ? navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID).DefaultBatchSize : default(long?);
            //     nav.BatchSizeNo = navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID) != null ? navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID).BatchUnitSize : default(decimal?);
            // });

            return navMethodCodeModels;
        }


        [HttpPost]
        [Route("UploadProductRecipeAttachments")]
        public IActionResult UploadProductRecipeAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new ProductRecipeDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId
                };

                _context.ProductRecipeDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetProductRecipeAttachmentBySessionID")]
        public List<DocumentsModel> GetProductRecipeAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.ProductRecipeDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.ProductRecipeDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadProductRecipeAttachment")]
        public IActionResult DownLoadProductRecipeAttachment(long id)
        {
            var document = _context.ProductRecipeDocument.SingleOrDefault(t => t.ProductRecipeDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteProductRecipeAttachment")]
        public void DeleteProductRecipeAttachment(int id)
        {

            var query = string.Format("delete from ProductRecipeDocument Where ProductRecipeDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }





        //







    }
}