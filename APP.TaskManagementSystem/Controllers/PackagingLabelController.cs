﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingLabelController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingLabelController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingLabels")]
        public List<PackagingLabelModel> Get()
        {
            List<PackagingLabelModel> packagingLabelModels = new List<PackagingLabelModel>();
            var packagingLabels = _context.PackagingLabel.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).OrderByDescending(o => o.PackagingLabelId).AsNoTracking().ToList();
            if (packagingLabels != null && packagingLabels.Count > 0)
            {
                List<long?> masterIds = packagingLabels.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList();
                masterIds.AddRange(packagingLabels.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.BgPantoneColorId != null).Select(a => a.BgPantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.BgColorId != null).Select(a => a.BgColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.PackagingUnitId != null).Select(a => a.PackagingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingLabels.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingLabels.ForEach(s =>
                {
                    PackagingLabelModel packagingLabelModel = new PackagingLabelModel
                    {
                        PackagingLabelId = s.PackagingLabelId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        ColorId = s.ColorId,
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PackagingUnitId = s.PackagingUnitId,
                        PantoneColorId = s.PantoneColorId,
                        SizeLength = s.SizeLength,
                        SizeWidth = s.SizeWidth,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        LabelTypeID = s.LabelTypeId,
                        IsVersion = s.IsVersion,
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileRefereceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingLabelModels.Add(packagingLabelModel);
                });
            }

            return packagingLabelModels;
        }

        // GET: api/Project
        [HttpPost]
        [Route("GetPackagingLabelsByRefNo")]
        public List<PackagingLabelModel> GetPackagingLabelsByRefNo(RefSearchModel refSearchModel)
        {
            List<PackagingLabelModel> packagingLabelModels = new List<PackagingLabelModel>();
            List<PackagingLabel> packagingLabel = new List<PackagingLabel>();
            if (refSearchModel.IsHeader)
            {
                packagingLabel = _context.PackagingLabel.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.LabelTypeId == refSearchModel.TypeID).OrderByDescending(o => o.PackagingLabelId).AsNoTracking().ToList();
            }
            else
            {
                packagingLabel = _context.PackagingLabel.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Where(r => r.MasterProfileRefereceNo == refSearchModel.ProfileReferenceNo && r.LabelTypeId == refSearchModel.TypeID).OrderByDescending(o => o.PackagingLabelId).AsNoTracking().ToList();

            }
            if (packagingLabel != null && packagingLabel.Count > 0)
            {
                List<long?> masterIds = packagingLabel.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList();
                masterIds.AddRange(packagingLabel.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.BgPantoneColorId != null).Select(a => a.BgPantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.BgColorId != null).Select(a => a.BgColorId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.PackagingUnitId != null).Select(a => a.PackagingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingLabel.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingLabel.ForEach(s =>
                {
                    PackagingLabelModel packagingLabelModel = new PackagingLabelModel
                    {
                        PackagingLabelId = s.PackagingLabelId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        ColorId = s.ColorId,
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PackagingUnitId = s.PackagingUnitId,
                        PantoneColorId = s.PantoneColorId,
                        SizeLength = s.SizeLength,
                        SizeWidth = s.SizeWidth,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        LabelTypeID = s.LabelTypeId,
                        IsVersion = s.IsVersion,
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileRefereceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingLabelModels.Add(packagingLabelModel);
                });
            }
            return packagingLabelModels;


        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackagingLabelModel> GetData(SearchModel searchModel)
        {
            var packagingLabel = new PackagingLabel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).LastOrDefault();
                        break;
                    case "Next":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).LastOrDefault();
                        break;
                    case "Next":
                        packagingLabel = _context.PackagingLabel.OrderBy(o => o.PackagingLabelId).FirstOrDefault(s => s.PackagingLabelId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingLabel = _context.PackagingLabel.OrderByDescending(o => o.PackagingLabelId).FirstOrDefault(s => s.PackagingLabelId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingLabelModel>(packagingLabel);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingLabel")]
        public PackagingLabelModel Post(PackagingLabelModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "PackagingLabel" });
            var packagingLabel = new PackagingLabel
            {

                BgColorId = value.BgColorId,
                BgPantoneColorId = value.BgPantoneColorId,
                ColorId = value.ColorId,
                LetteringId = value.LetteringId,
                NoOfColor = value.NoOfColor,
                PackagingItemId = value.PackagingItemId,
                PackagingMaterialId = value.PackagingMaterialId,
                PackagingUnitId = value.PackagingUnitId,
                PantoneColorId = value.PantoneColorId,
                SizeLength = value.SizeLength,
                SizeWidth = value.SizeWidth,
                LabelTypeId = value.LabelTypeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                IsVersion = value.IsVersion,
                ProfileReferenceNo = profileNo,
                MasterProfileRefereceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo


            };
            _context.PackagingLabel.Add(packagingLabel);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = packagingLabel.MasterProfileRefereceNo;
            value.PackagingLabelId = packagingLabel.PackagingLabelId;
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackagingLabelModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LabelTypeID == 1080)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {

                        var PackagingItemName = value.PackagingItemId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingItemId).Select(m => m.Value).FirstOrDefault() : "";
                        var PackagingMeterialName = value.PackagingMaterialId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialId).Select(m => m.Value).FirstOrDefault() : "";
                        if (value.NoOfColor != null)
                        {
                            itemName = value.NoOfColor + " " + "Color" + " ";
                        }
                        if (value.SizeLength != null)
                        {
                            itemName = itemName + value.SizeLength + "mm" + "(L)" + " ";
                        }
                        if (value.SizeWidth != null)
                        {
                            itemName = itemName + "X" + " " + value.SizeWidth + "mm" + "(W)" + " ";
                        }
                        if (!string.IsNullOrEmpty(PackagingItemName))
                        {
                            itemName = itemName + PackagingItemName + " ";
                        }
                        itemName = itemName + "Label";
                        //itemName = value.NoOfColor + " " + "Color" + " " + value.SizeLength + "mm" + "(L)" + " " + "X" + " " + value.SizeWidth + "mm" + "(W)" +" " + PackagingItemName + " " + "Label";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            if (value.LabelTypeID == 1081)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {

                        var PackagingItemName = value.PackagingItemId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingItemId).Select(m => m.Value).FirstOrDefault() : "";
                        var PackagingMeterialName = value.PackagingMaterialId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialId).Select(m => m.Value).FirstOrDefault() : "";
                        if (value.NoOfColor != null)
                        {
                            itemName = value.NoOfColor + " " + "Color" + " ";
                        }
                        if (value.SizeLength != null)
                        {
                            itemName = itemName + value.SizeLength + "mm" + "(L)" + " " + "X" + " ";
                        }
                        if (value.SizeWidth != null)
                        {
                            itemName = itemName + value.SizeWidth + "mm" + "(W)" + " ";
                        }
                        if (PackagingMeterialName != null)
                        {
                            itemName = itemName + PackagingMeterialName + " ";
                        }
                        if (PackagingItemName != null)
                        {
                            itemName = itemName + PackagingItemName + " ";
                        }
                        itemName = itemName + "Label";
                        //itemName = value.NoOfColor + " " + "Color" + " " + value.SizeLength + "mm" + "(L)" + " " + "X" + " " + value.SizeWidth + "mm" + "(W)" + " " + PackagingMeterialName + " " + PackagingItemName + " " + "Label";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingLabel")]
        public PackagingLabelModel Put(PackagingLabelModel value)
        {
            var packagingLabel = _context.PackagingLabel.SingleOrDefault(p => p.PackagingLabelId == value.PackagingLabelId);
            packagingLabel.PackagingLabelId = value.PackagingLabelId;
            packagingLabel.BgColorId = value.BgColorId;
            packagingLabel.BgPantoneColorId = value.BgPantoneColorId;
            packagingLabel.ColorId = value.ColorId;
            packagingLabel.LetteringId = value.LetteringId;
            packagingLabel.NoOfColor = value.NoOfColor;
            packagingLabel.PackagingItemId = value.PackagingItemId;
            packagingLabel.PackagingMaterialId = value.PackagingMaterialId;
            packagingLabel.PackagingUnitId = value.PackagingUnitId;
            packagingLabel.PantoneColorId = value.PantoneColorId;
            packagingLabel.SizeLength = value.SizeLength;
            packagingLabel.SizeWidth = value.SizeWidth;
            packagingLabel.IsVersion = value.IsVersion;
            packagingLabel.ModifiedByUserId = value.ModifiedByUserID;
            packagingLabel.ModifiedDate = DateTime.Now;
            packagingLabel.StatusCodeId = value.StatusCodeID.Value;
            packagingLabel.LabelTypeId = value.LabelTypeID;
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingLabel")]
        public void Delete(int id)
        {
            var packagingLabel = _context.PackagingLabel.SingleOrDefault(p => p.PackagingLabelId == id);
            if (packagingLabel != null)
            {
                _context.PackagingLabel.Remove(packagingLabel);
                _context.SaveChanges();
            }
        }
    }
}