﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TenderInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TenderInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [Route("GetTenderInformation")]
        public List<TenderInformationModel> Get()
        {
            var tenderInformation = _context.TenderInformation.Include(p => p.Product).Include(c => c.Contract).Include(c => c.Customer).Include(ss => ss.Product.SobyCustomers).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
           
            List<TenderInformationModel> tenderInformationModel = new List<TenderInformationModel>();
            if(tenderInformation!=null && tenderInformation.Count>0)
            {
                var masterDetailIds = tenderInformation.Where(s => s.Product?.PerUomId != null).Select(s => s.Product?.PerUomId).ToList();
                if(masterDetailIds.Count>0)
                {
                     masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
            }
            tenderInformation.ForEach(s =>
            {
                TenderInformationModel tenderInformationModels = new TenderInformationModel
                {
                    TenderInformationId = s.TenderInformationId,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.CompanyName,
                    ContractId = s.ContractId,
                    ContractNo = s.Contract?.OrderNo,
                    ProductId = s.ProductId,
                    CustomerPonumber = s.CustomerPonumber,
                    TenderQty = s.TenderQty,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProductName = s.Product != null && s.Product != null ? s.Product.CustomerReferenceNo + "|" + s.Product.Description : null,
                    Uom1 = masterDetailList != null && s.Product?.PerUomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    SessionId = s.SessionId,
                };
                tenderInformationModel.Add(tenderInformationModels);
            });
            return tenderInformationModel.OrderByDescending(a => a.TenderInformationId).ToList();
        }
        [HttpGet]
        [Route("GetTenderInformationById")]
        public TenderInformationModel GetTenderInformationById(int id)
        {
            TenderInformationModel tenderInformationModel = new TenderInformationModel();
            var tenderInformation = _context.TenderInformation.Where(s => s.ContractId == id).FirstOrDefault();
            if(tenderInformation!=null)
            {
                tenderInformationModel.TenderQty = tenderInformation.TenderQty;
            }
            return tenderInformationModel;
        }
        [HttpPost()]
        [Route("GetTenderQty")]
        public ActionResult<TenderInformationModel> GetTenderQty(GetTenderQtyModel getTenderQtyModel)
        {
            TenderInformationModel tenderInformationModel = new TenderInformationModel();
            var tenderInfo = _context.TenderInformation.Where(s => s.ContractId == getTenderQtyModel.ContractId && s.CustomerId == getTenderQtyModel.CustomerId && s.ProductId == getTenderQtyModel.ProductId).FirstOrDefault();
            if(tenderInfo!=null)
            {
                tenderInformationModel.TenderQty = tenderInfo.TenderQty;
            }
            return tenderInformationModel;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TenderInformationModel> GetData(SearchModel searchModel)
        {
            var tenderInformation = new TenderInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).LastOrDefault();
                        break;
                    case "Next":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).LastOrDefault();
                        break;
                    case "Next":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderBy(o => o.TenderInformationId).FirstOrDefault(s => s.TenderInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        tenderInformation = _context.TenderInformation.Include(c => c.Contract).OrderByDescending(o => o.TenderInformationId).FirstOrDefault(s => s.TenderInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TenderInformationModel>(tenderInformation);
            if (result != null)
            {
                result.ContractNo = tenderInformation.Contract?.OrderNo;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertTenderInformation")]
        public TenderInformationModel Post(TenderInformationModel value)
        {
            var SessionId = Guid.NewGuid();
            var tenderInformation = new TenderInformation
            {
                CustomerId = value.CustomerId,
                ContractId = value.ContractId,
                ProductId = value.ProductId,
                CustomerPonumber = value.CustomerPonumber,
                TenderQty = value.TenderQty,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = SessionId,
            };
            _context.TenderInformation.Add(tenderInformation);
            _context.SaveChanges();
            value.TenderInformationId = tenderInformation.TenderInformationId;
            value.SessionId = tenderInformation.SessionId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTenderInformation")]
        public TenderInformationModel Put(TenderInformationModel value)
        {
            var tenderInformation = _context.TenderInformation.SingleOrDefault(p => p.TenderInformationId == value.TenderInformationId);
            tenderInformation.CustomerId = value.CustomerId;
            tenderInformation.ContractId = value.ContractId;
            tenderInformation.ProductId = value.ProductId;
            tenderInformation.TenderQty = value.TenderQty;
            tenderInformation.CustomerPonumber = value.CustomerPonumber;
            tenderInformation.ModifiedByUserId = value.ModifiedByUserID;
            tenderInformation.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTenderInformation")]
        public void Delete(int id)
        {
            var tenderInformation = _context.TenderInformation.SingleOrDefault(p => p.TenderInformationId == id);
            if (tenderInformation != null)
            {
                var packetInformationLine = _context.PacketInformationLine.Where(p => p.TenderInformationId == id).ToList();
                if (packetInformationLine != null)
                {
                    _context.PacketInformationLine.RemoveRange(packetInformationLine);
                    _context.SaveChanges();
                }
                var novateInformationLine = _context.NovateInformationLine.Where(p => p.TenderInformationId == id).ToList();
                if (novateInformationLine != null)
                {
                    if(novateInformationLine!=null)
                    {
                        novateInformationLine.ForEach(h =>
                        {
                            var extensionInformationLine = _context.ExtensionInformationLine.Where(p => p.TenderInformationId == h.NovateInformationLineId).ToList();
                            if (extensionInformationLine != null)
                            {
                                _context.ExtensionInformationLine.RemoveRange(extensionInformationLine);
                                _context.SaveChanges();
                            }
                        });
                    }
                    _context.NovateInformationLine.RemoveRange(novateInformationLine);
                    _context.SaveChanges();
                }
                _context.TenderInformation.Remove(tenderInformation);
                _context.SaveChanges();
            }
        }
    }
}