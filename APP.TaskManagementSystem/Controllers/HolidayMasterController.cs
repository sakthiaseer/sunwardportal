﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HolidayMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public HolidayMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetHolidays")]
        public List<HolidayMasterModel> Get()
        {
            var HolidayMaster = _context.HolidayMaster.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<HolidayMasterModel> HolidayMasterModel = new List<HolidayMasterModel>();
            HolidayMaster.ForEach(s =>
            {
                HolidayMasterModel HolidayMasterModels = new HolidayMasterModel
                {
                    HolidayID = s.HolidayId,
                    StateID = s.StateId,
                    Name = s.Name,
                    StateName = s.State.Name,
                    Description = s.Description,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                HolidayMasterModel.Add(HolidayMasterModels);
            });
            return HolidayMasterModel.OrderByDescending(o => o.HolidayID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get HolidayMaster")]
        [HttpGet("GetHolidays/{id:int}")]
        public ActionResult<HolidayMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var HolidayMaster = _context.HolidayMaster.SingleOrDefault(p => p.HolidayId == id.Value);
            var result = _mapper.Map<HolidayMasterModel>(HolidayMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<HolidayMasterModel> GetData(SearchModel searchModel)
        {
            var HolidayMaster = new HolidayMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).FirstOrDefault();
                        break;
                    case "Last":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).LastOrDefault();
                        break;
                    case "Next":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).LastOrDefault();
                        break;
                    case "Previous":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).FirstOrDefault();
                        break;
                    case "Last":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).LastOrDefault();
                        break;
                    case "Next":
                        HolidayMaster = _context.HolidayMaster.OrderBy(o => o.HolidayId).FirstOrDefault(s => s.HolidayId > searchModel.Id);
                        break;
                    case "Previous":
                        HolidayMaster = _context.HolidayMaster.OrderByDescending(o => o.HolidayId).FirstOrDefault(s => s.HolidayId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<HolidayMasterModel>(HolidayMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertHoliday")]
        public HolidayMasterModel Post(HolidayMasterModel value)
        {
            var HolidayMaster = new HolidayMaster
            {
                // HolidayMasterId=value.HolidayMasterID,
                StateId = value.StateID,
                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID
            };
            _context.HolidayMaster.Add(HolidayMaster);
            _context.SaveChanges();
            value.HolidayID = HolidayMaster.HolidayId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateHoliday")]
        public HolidayMasterModel Put(HolidayMasterModel value)
        {
            var HolidayMaster = _context.HolidayMaster.SingleOrDefault(p => p.HolidayId == value.HolidayID);
            HolidayMaster.StateId = value.StateID;
            HolidayMaster.ModifiedByUserId = value.ModifiedByUserID;
            HolidayMaster.ModifiedDate = DateTime.Now;
            HolidayMaster.Name = value.Name;
            HolidayMaster.Description = value.Description;
            //HolidayMaster.AddedByUserId = value.AddedByUserID;
            // HolidayMaster.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            HolidayMaster.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteHoliday")]
        public void Delete(int id)
        {
            var HolidayMaster = _context.HolidayMaster.SingleOrDefault(p => p.HolidayId == id);
            if (HolidayMaster != null)
            {
                _context.HolidayMaster.Remove(HolidayMaster);
                _context.SaveChanges();
            }
        }
    }
}