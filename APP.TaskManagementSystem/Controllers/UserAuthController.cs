﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace APP.TaskManagementSystem.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IHubContext<ChatHub> _hubContext;
        public UserAuthController(CRT_TMSContext context, IMapper mapper, IConfiguration configuration, IHubContext<ChatHub> hubContext)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _hubContext = hubContext;
        }

        [Route("Login")] // /login
        [HttpPost]
        public ActionResult Login([FromBody] ApplicationUserModel model)
        {
            var password = EncryptDecryptPassword.Encrypt(model.LoginPassword);
            var user = _context.ApplicationUser.Include("UserSettings").FirstOrDefault(u => u.LoginId == model.LoginID && u.LoginPassword == password && u.StatusCodeId != 2);
            if (user != null)
            {
                var claim = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, user.LoginId)
                };
                var signinKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

                int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);
                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier,user.UserName),
                    new Claim(ClaimTypes.Role,"Admin")
                };
                var token = new JwtSecurityToken(
                  issuer: _configuration["Jwt:Site"],
                  audience: _configuration["Jwt:Site"],
                  claims,
                  expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                  signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256)
                );

                return Ok(
                  new
                  {
                      token = new JwtSecurityTokenHandler().WriteToken(token),
                      expiration = token.ValidTo,
                      userName = user.UserName,
                      userId = user.UserId,
                      loginID = user.LoginId,
                      userCode = user.UserCode,
                      sessionId = user.SessionId,
                      loginPassword = EncryptDecryptPassword.Decrypt(user.LoginPassword),
                      pageSize = user.UserSettings != null && user.UserSettings.Count > 0 ? user.UserSettings.First().PageSize : 20,
                      userTheme = user.UserSettings != null && user.UserSettings.Count > 0 ? user.UserSettings.First().UserTheme : "indigo",
                  });
            }
            return Unauthorized();
        }
    }
}