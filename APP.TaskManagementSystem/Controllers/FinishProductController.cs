﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FinishProductController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FinishProductController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetFinishProductsList")]
        public List<FinishProductModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var pharamacological = _context.PharmacologicalCategory.AsNoTracking().ToList();
            var EquivalentBrand = _context.FinishProductEquivalentBrandCategory.AsNoTracking().ToList();
            var chemicalSubGroup = _context.ProductChemicalSubGroup.AsNoTracking().ToList();
            var GenericList = _context.FinisihProductGenericCategory.AsNoTracking().ToList();
            var finishProductItems = _context.FinishProduct.Include(m => m.RegisterationCode).Include(s => s.StatusCode).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<FinishProductModel> finishProductModel = new List<FinishProductModel>();
            finishProductItems.ForEach(s =>
            {
                FinishProductModel finishProductModels = new FinishProductModel
                {
                    FinishProductId = s.FinishProductId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingSiteName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                    DosageFormId = s.DosageFormId,
                    DosageFormName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageFormId).Select(a => a.Value).SingleOrDefault() : "",
                    ProductId = s.ProductId,
                    ProductName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ProductId).Select(a => a.Value).SingleOrDefault() : "",
                    RegistrationFileName = s.RegistrationFileName,
                    //ChemicalSubgroupId=s.ChemicalSubgroup,
                    ChemicalSubgroupName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ChemicalSubgroup).Select(a => a.Value).SingleOrDefault() : "",
                    Atccode = s.Atccode,
                    Gtinno = s.Gtinno,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ChemicalSubgroupId = chemicalSubGroup.Where(p => p.FinishProductId == s.FinishProductId).Select(p => p.ChemicalSubGroupId).ToList(),
                    EquvalentBrandNameId = EquivalentBrand.Where(p => p.FinishProductId == s.FinishProductId).Select(p => p.EquivalentBrandCategoryId).ToList(),
                    GenericNameId = GenericList.Where(p => p.FinishProductId == s.FinishProductId).Select(p => p.GenericCategoryId).ToList(),
                    PharmacologicalCategoryId = pharamacological.Where(p => p.FinishProductId == s.FinishProductId).Select(p => p.PharmacologicalCategoryId).ToList(),
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    DrugClassificationID = s.DrugClassificationId,
                    RegistrationProductCategory = s.RegistrationProductCategory,
                };
                finishProductModel.Add(finishProductModels);
            });
            return finishProductModel.OrderByDescending(a => a.FinishProductId).ToList();
        }
        [HttpGet]
        [Route("GetFinishProductsListForGeneralInfo")]
        public List<FinishProductModel> GetFinishProductsListForGeneralInfo()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var pharamacological = _context.PharmacologicalCategory.AsNoTracking().ToList();
            var chemicalSubGroup = _context.ProductChemicalSubGroup.AsNoTracking().ToList();
            var finishProductItems = _context.FinishProduct.Include(m => m.RegisterationCode).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<FinishProductModel> finishProductModel = new List<FinishProductModel>();
            finishProductItems.ForEach(s =>
            {
                FinishProductModel finishProductModels = new FinishProductModel
                {
                    FinishProductId = s.FinishProductId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingSiteName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                    ProductId = s.ProductId,
                    ProductName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ProductId).Select(a => a.Value).SingleOrDefault() + " | " + applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ProductId).Select(a => a.Value).SingleOrDefault(),
                    ChemicalSubgroupName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ChemicalSubgroup).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                };
                finishProductModel.Add(finishProductModels);
            });
            return finishProductModel.OrderByDescending(a => a.FinishProductId).Where(f => f.StatusCodeID == 1182).ToList();
        }

        [HttpGet]
        [Route("GetFinishProducts")]
        public List<FinishProductModel> GetFinishProducts()
        {
            List<FinishProductModel> finishProductModels = new List<FinishProductModel>();
            var finishProductItems = _context.FinishProduct.Where(f => f.StatusCodeId == 1).AsNoTracking().ToList();
            var finishProductGeneralInfoItems = _context.FinishProductGeneralInfo.Where(f => f.StatusCodeId == 1).AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            finishProductItems.ForEach(f =>
            {
                FinishProductModel finishProductModel = new FinishProductModel();
                finishProductModel.FinishProductId = f.FinishProductId;
                finishProductModel.ProductId = f.ProductId;
                finishProductModel.ProductName = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == f.ProductId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == f.ProductId).Value : string.Empty;
                finishProductModel.ManufacturingSiteId = f.ManufacturingSiteId;
                finishProductModel.ManufacturingSiteName = applicationMasterDetails != null && applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == f.ManufacturingSiteId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == f.ManufacturingSiteId).Value : string.Empty;
                //finishProductModel.ChemicalSubgroup = f.ChemicalSubgroup;
                finishProductModel.ChemicalSubgroupName = applicationMasterDetails != null ? applicationMasterDetails.Where(a => a.ApplicationMasterDetailId == f.ChemicalSubgroup).Select(a => a.Value).SingleOrDefault() : "";
                finishProductModel.RegisterationCodeId = f.RegisterationCodeId;
                finishProductModel.RegistrationProductCategory = f.RegistrationProductCategory;
                finishProductModel.DrugClassificationID = f.DrugClassificationId;
                var productInfo = finishProductGeneralInfoItems.FirstOrDefault(p => p.FinishProductId == f.FinishProductId);
                if (productInfo != null)
                {
                    var productOwnerItem = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == productInfo.RegisterProductOwnerId);
                    if (productOwnerItem != null)
                    {
                        finishProductModel.ProductOwner = productOwnerItem.Value;
                    }
                    var registerCountry = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == productInfo.RegisterCountry);
                    if (registerCountry != null)
                    {
                        finishProductModel.Country = registerCountry.Value;
                    }
                }
                finishProductModels.Add(finishProductModel);
            });
            return finishProductModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FinishProductModel> GetData(SearchModel searchModel)
        {
            var finishproduct = new FinishProduct();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).FirstOrDefault();
                        break;
                    case "Last":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).LastOrDefault();
                        break;
                    case "Next":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).LastOrDefault();
                        break;
                    case "Previous":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).FirstOrDefault();
                        break;
                    case "Last":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).LastOrDefault();
                        break;
                    case "Next":
                        finishproduct = _context.FinishProduct.OrderBy(o => o.FinishProductId).FirstOrDefault(s => s.FinishProductId > searchModel.Id);
                        break;
                    case "Previous":
                        finishproduct = _context.FinishProduct.OrderByDescending(o => o.FinishProductId).FirstOrDefault(s => s.FinishProductId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<FinishProductModel>(finishproduct);
            if (result != null)
            {
                result.PharmacologicalCategoryId = _context.PharmacologicalCategory.Where(p => p.FinishProductId == result.FinishProductId).AsNoTracking().Select(p => p.PharmacologicalCategoryId).ToList();
                result.EquvalentBrandNameId = _context.FinishProductEquivalentBrandCategory.Where(p => p.FinishProductId == result.FinishProductId).AsNoTracking().Select(p => p.EquivalentBrandCategoryId).ToList();
                result.GenericNameId = _context.FinisihProductGenericCategory.Where(p => p.FinishProductId == result.FinishProductId).AsNoTracking().Select(p => p.GenericCategoryId).ToList();
            }
            return result;
        }
        [HttpPost]
        [Route("InsertFinishProdut")]
        public FinishProductModel Post(FinishProductModel value)
        {

            var finishproduct = new FinishProduct
            {
                ManufacturingSiteId = value.ManufacturingSiteId,
                DosageFormId = value.DosageFormId,
                ProductId = value.ProductId,
                RegistrationFileName = value.RegistrationFileName,
                EquvalentBrandName = value.EquvalentBrandName,

                Atccode = value.Atccode,
                Gtinno = value.Gtinno,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                //RegisterationCodeId = value.RegisterationCodeId?.Value,
                DrugClassificationId = value.DrugClassificationID,
                RegistrationProductCategory = value.RegistrationProductCategory,
            };
            _context.FinishProduct.Add(finishproduct);
            _context.SaveChanges();
            value.FinishProductId = finishproduct.FinishProductId;
            if (value.PharmacologicalCategoryId != null)
            {
                value.PharmacologicalCategoryId.ForEach(c =>
                {
                    var pharmacological = new PharmacologicalCategory
                    {
                        PharmacologicalCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.PharmacologicalCategory.Add(pharmacological);
                });
            }
            if (value.GenericNameId != null)
            {
                value.GenericNameId.ForEach(c =>
                {
                    var genericCategory = new FinisihProductGenericCategory
                    {
                        GenericCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.FinisihProductGenericCategory.Add(genericCategory);
                });
            }
            if (value.EquvalentBrandNameId != null)
            {
                value.EquvalentBrandNameId.ForEach(c =>
                {
                    var EquivalentBrand = new FinishProductEquivalentBrandCategory
                    {
                        EquivalentBrandCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.FinishProductEquivalentBrandCategory.Add(EquivalentBrand);
                });
            }
            if (value.ChemicalSubgroupId != null)
            {
                value.ChemicalSubgroupId.ForEach(c =>
                {
                    var productChemicalSubGroup = new ProductChemicalSubGroup
                    {
                        ChemicalSubGroupId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.ProductChemicalSubGroup.Add(productChemicalSubGroup);
                });
            }
            _context.SaveChanges();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.ManufacturingSiteName = applicationMasterDetails != null ? applicationMasterDetails.Where(a => a.ApplicationMasterDetailId == value.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "";
            value.ProductName = applicationMasterDetails != null ? applicationMasterDetails.Where(a => a.ApplicationMasterDetailId == value.ProductId).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        [HttpPut]
        [Route("UpdateFinishProdut")]
        public FinishProductModel Put(FinishProductModel value)
        {
            var finishproduct = _context.FinishProduct.SingleOrDefault(p => p.FinishProductId == value.FinishProductId);

            finishproduct.ManufacturingSiteId = value.ManufacturingSiteId;
            finishproduct.DosageFormId = value.DosageFormId;
            finishproduct.ProductId = value.ProductId;
            finishproduct.RegistrationFileName = value.RegistrationFileName;
            finishproduct.EquvalentBrandName = value.EquvalentBrandName;
            //finishproduct.ChemicalSubgroup = value.ChemicalSubgroup;
            finishproduct.Atccode = value.Atccode;
            finishproduct.Gtinno = value.Gtinno;
            finishproduct.ModifiedByUserId = value.ModifiedByUserID;
            finishproduct.ModifiedDate = DateTime.Now;
            finishproduct.StatusCodeId = value.StatusCodeID.Value;
            //finishproduct.RegisterationCodeId = value.RegisterationCodeId;
            finishproduct.DrugClassificationId = value.DrugClassificationID;
            finishproduct.RegistrationProductCategory = value.RegistrationProductCategory;
            var pharmacologicalcategory = _context.PharmacologicalCategory.Where(l => l.FinishProductId == value.FinishProductId).ToList();
            if (pharmacologicalcategory.Count > 0)
            {
                _context.PharmacologicalCategory.RemoveRange(pharmacologicalcategory);
                _context.SaveChanges();
            }
            if (value.PharmacologicalCategoryId != null)
            {
                value.PharmacologicalCategoryId.ForEach(c =>
                {
                    var pharmacological = new PharmacologicalCategory
                    {
                        PharmacologicalCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.PharmacologicalCategory.Add(pharmacological);
                });
            }
            var FinisihProductGenericCategory = _context.FinisihProductGenericCategory.Where(l => l.FinishProductId == value.FinishProductId).ToList();
            if (FinisihProductGenericCategory.Count > 0)
            {
                _context.FinisihProductGenericCategory.RemoveRange(FinisihProductGenericCategory);
                _context.SaveChanges();
            }
            if (value.GenericNameId != null)
            {
                value.GenericNameId.ForEach(c =>
                {
                    var genericCategory = new FinisihProductGenericCategory
                    {
                        GenericCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.FinisihProductGenericCategory.Add(genericCategory);
                });
            }
            var FinishProductEquivalentBrandCategory = _context.FinishProductEquivalentBrandCategory.Where(l => l.FinishProductId == value.FinishProductId).ToList();
            if (FinishProductEquivalentBrandCategory.Count > 0)
            {
                _context.FinishProductEquivalentBrandCategory.RemoveRange(FinishProductEquivalentBrandCategory);
                _context.SaveChanges();
            }
            if (value.EquvalentBrandNameId != null)
            {
                value.EquvalentBrandNameId.ForEach(c =>
                {
                    var EquivalentBrand = new FinishProductEquivalentBrandCategory
                    {
                        EquivalentBrandCategoryId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.FinishProductEquivalentBrandCategory.Add(EquivalentBrand);
                });
            }
            var FinisihProductChemicalSubgroup = _context.ProductChemicalSubGroup.Where(l => l.FinishProductId == value.FinishProductId).ToList();
            if (FinisihProductChemicalSubgroup.Count > 0)
            {
                _context.ProductChemicalSubGroup.RemoveRange(FinisihProductChemicalSubgroup);
                _context.SaveChanges();
            }
            if (value.ChemicalSubgroupId != null)
            {
                value.ChemicalSubgroupId.ForEach(c =>
                {
                    var productChemicalSubGroup = new ProductChemicalSubGroup
                    {
                        ChemicalSubGroupId = c,
                        FinishProductId = value.FinishProductId,
                    };
                    finishproduct.ProductChemicalSubGroup.Add(productChemicalSubGroup);
                });
            }
            _context.SaveChanges();

            return value;
        }

        [HttpDelete]
        [Route("DeleteFinishProduct")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var finishproduct = _context.FinishProduct.SingleOrDefault(p => p.FinishProductId == id);
                if (finishproduct != null)
                {
                    var FinishProductLine = _context.FinishProductLine.Where(s => s.FinishProductId == finishproduct.FinishProductId).ToList();
                    if (FinishProductLine != null)
                    {
                        _context.FinishProductLine.RemoveRange(FinishProductLine);
                        _context.SaveChanges();
                    }
                    var pharmacologicalCategories = _context.PharmacologicalCategory.Where(m => m.FinishProductId == finishproduct.FinishProductId).ToList();
                    if (pharmacologicalCategories.Any())
                    {
                        _context.PharmacologicalCategory.RemoveRange(pharmacologicalCategories);
                        _context.SaveChanges();
                    }
                    var FinisihProductGenericCategory = _context.FinisihProductGenericCategory.Where(l => l.FinishProductId == finishproduct.FinishProductId).ToList();
                    if (FinisihProductGenericCategory.Count > 0)
                    {
                        _context.FinisihProductGenericCategory.RemoveRange(FinisihProductGenericCategory);
                        _context.SaveChanges();
                    }
                    var FinishProductEquivalentBrandCategory = _context.FinishProductEquivalentBrandCategory.Where(l => l.FinishProductId == finishproduct.FinishProductId).ToList();
                    if (FinishProductEquivalentBrandCategory.Count > 0)
                    {
                        _context.FinishProductEquivalentBrandCategory.RemoveRange(FinishProductEquivalentBrandCategory);
                        _context.SaveChanges();
                    }
                    _context.FinishProduct.Remove(finishproduct);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new AppException("Product Already Mapped Someother Items", null);
            }
        }

        [HttpPost]
        [Route("GetFinishProductReport")]
        public List<FinishProductReportModel> GetFinishProductReport(FinishProductReportParam finishProductReportParam)
        {
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishProductLine = _context.FinishProductLine.Include(m => m.FinishProduct).AsNoTracking().ToList();
            List<FinishProductReportModel> finishProductReportModel = new List<FinishProductReportModel>();
            finishProductLine.ForEach(s =>
            {
                FinishProductReportModel finishProductReportModels = new FinishProductReportModel
                {
                    FinishProductID = s.FinishProductId,
                    FinishProductLineID = s.FinishProductLineId,
                    DosageFormID = s.FinishProduct.DosageFormId,
                    StatusCodeID = s.FinishProduct.StatusCodeId,
                    ManufacturingSite = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId).Select(a => a.Value).FirstOrDefault(),
                    ProductName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId).Select(a => a.Value).FirstOrDefault(),
                    Dosage = s.DosageForm,
                    Units = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.Uom).Select(a => a.Value).FirstOrDefault(),
                    PerDosage = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.PerDosage).Select(a => a.Value).FirstOrDefault(),
                    MaterialName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).FirstOrDefault(),
                    Overage = s.Overage,
                    OverageDosageUnits = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.DosageUnits).Select(a => a.Value).FirstOrDefault(),
                    OverageUnits = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.OverageUnits).Select(a => a.Value).FirstOrDefault(),
                };
                finishProductReportModel.Add(finishProductReportModels);
            });
            if (finishProductReportParam.DosageFormIds.Count > 0)
            {
                finishProductReportModel = finishProductReportModel.Where(d => finishProductReportParam.DosageFormIds.Contains(d.DosageFormID)).ToList();
            }
            if (finishProductReportParam.StatusCodeIds.Count > 0)
            {
                finishProductReportModel = finishProductReportModel.Where(d => finishProductReportParam.StatusCodeIds.Contains(d.StatusCodeID)).ToList();
            }
            return finishProductReportModel.OrderByDescending(f => f.FinishProductID).ToList();

        }
    }
}