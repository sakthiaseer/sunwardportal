﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingItemHeaderController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingItemHeaderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingItemHeader")]
        public List<CommonPackagingItemHeaderModel> Get(int id)
        {
            var commonPackagingItemHeaderList = _context.CommonPackagingItemHeader
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode).Where(w => w.ItemClassificationMasterId == id).AsNoTracking().ToList();
            List<CommonPackagingItemHeaderModel> commonPackagingItemHeaderModel = new List<CommonPackagingItemHeaderModel>();
            if (commonPackagingItemHeaderList.Count > 0)
            {
                List<long?> masterIds = commonPackagingItemHeaderList.Where(w => w.UomId != null).Select(a => a.UomId).Distinct().ToList();
                masterIds.AddRange(commonPackagingItemHeaderList.Where(w => w.PackagingItemCategoryId != null).Select(a => a.PackagingItemCategoryId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingItemHeaderList.ForEach(s =>
                {
                    CommonPackagingItemHeaderModel commonPackagingItemHeaderModels = new CommonPackagingItemHeaderModel
                    {
                        CommonPackagingItemId = s.CommonPackagingItemId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        AlsoKownAs = s.AlsoKownAs,
                        PackagingItemName = s.PackagingItemName,
                        PackagingItemCategoryId = s.PackagingItemCategoryId,
                        PackagingItemCategoryName = s.PackagingItemCategoryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "",
                        UomId = s.UomId,
                        UomName = s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        PackagingMaterialNo = (s.Profile != null ? s.Profile.Name : "") + " | " + (s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : ""),
                    };
                    commonPackagingItemHeaderModel.Add(commonPackagingItemHeaderModels);
                });
            }
            return commonPackagingItemHeaderModel.OrderByDescending(a => a.CommonPackagingItemId).ToList();
        }
        [HttpGet]
        [Route("GetCommonPackagingItemHeaderAll")]
        public List<CommonPackagingItemHeaderModel> GetCommonPackagingItemHeaderAll()
        {
            var commonPackagingItemHeaderList = _context.CommonPackagingItemHeader
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingItemHeaderModel> commonPackagingItemHeaderModel = new List<CommonPackagingItemHeaderModel>();
            if (commonPackagingItemHeaderList.Count > 0)
            {
                List<long?> masterIds = commonPackagingItemHeaderList.Where(w => w.UomId != null).Select(a => a.UomId).Distinct().ToList();
                masterIds.AddRange(commonPackagingItemHeaderList.Where(w => w.PackagingItemCategoryId != null).Select(a => a.PackagingItemCategoryId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();

                commonPackagingItemHeaderList.ForEach(s =>
                {
                    CommonPackagingItemHeaderModel commonPackagingItemHeaderModels = new CommonPackagingItemHeaderModel
                    {
                        CommonPackagingItemId = s.CommonPackagingItemId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        AlsoKownAs = s.AlsoKownAs,
                        PackagingItemName = s.PackagingItemName,
                        PackagingItemCategoryId = s.PackagingItemCategoryId,
                        PackagingItemCategoryName = s.PackagingItemCategoryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "",
                        UomId = s.UomId,
                        UomName = s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        PackagingMaterialNo = (s.Profile != null ? s.Profile.Name : "") + "|" + (s.PackagingItemName) + " | " + (s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : ""),
                    };
                    commonPackagingItemHeaderModel.Add(commonPackagingItemHeaderModels);
                });
            }
            return commonPackagingItemHeaderModel.OrderByDescending(a => a.CommonPackagingItemId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingItemHeaderModel> GetData(SearchModel searchModel)
        {
            var commonPackagingItemHeader = new CommonPackagingItemHeader();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderBy(o => o.CommonPackagingItemId).FirstOrDefault(s => s.CommonPackagingItemId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingItemHeader = _context.CommonPackagingItemHeader.OrderByDescending(o => o.CommonPackagingItemId).FirstOrDefault(s => s.CommonPackagingItemId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingItemHeaderModel>(commonPackagingItemHeader);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingItemHeader")]
        public CommonPackagingItemHeaderModel Post(CommonPackagingItemHeaderModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonPackagingItemHeader = new CommonPackagingItemHeader
            {
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                PackagingItemCategoryId = value.PackagingItemCategoryId,
                ProfileId = value.ProfileId,
                AlsoKownAs = value.AlsoKownAs,
                UomId = value.UomId,
                ProfileReferenceNo = profileNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingItemHeader.Add(commonPackagingItemHeader);
            _context.SaveChanges();
            value.ProfileReferenceNo = commonPackagingItemHeader.ProfileReferenceNo;
            value.CommonPackagingItemId = commonPackagingItemHeader.CommonPackagingItemId;
            value.PackagingItemCategoryName = value.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "";
            value.UomName = value.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.UomId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingItemHeader")]
        public CommonPackagingItemHeaderModel Put(CommonPackagingItemHeaderModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonPackagingItemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.CommonPackagingItemId == value.CommonPackagingItemId);
            commonPackagingItemHeader.PackagingItemCategoryId = value.PackagingItemCategoryId;
            commonPackagingItemHeader.ProfileId = value.ProfileId;
            commonPackagingItemHeader.ItemClassificationMasterId = value.ItemClassificationMasterId;
            commonPackagingItemHeader.AlsoKownAs = value.AlsoKownAs;
            commonPackagingItemHeader.UomId = value.UomId;
            commonPackagingItemHeader.ProfileReferenceNo = value.ProfileReferenceNo;
            commonPackagingItemHeader.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingItemHeader.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingItemHeader.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.PackagingItemCategoryName = value.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "";
            value.UomName = value.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.UomId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingItemHeader")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingItemHeader = _context.CommonPackagingItemHeader.Where(p => p.CommonPackagingItemId == id).FirstOrDefault();
                if (commonPackagingItemHeader != null)
                {
                    _context.CommonPackagingItemHeader.Remove(commonPackagingItemHeader);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetCommonPackagingItemHeaderById")]
        public List<CommonPackagingItemHeaderModel> GetCommonPackagingItemHeaderById(int id)
        {
            var commonPackagingItemHeaderList = _context.CommonPackagingItemHeader
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode).Where(w => w.CommonPackagingItemId == id).AsNoTracking().ToList();
            List<CommonPackagingItemHeaderModel> commonPackagingItemHeaderModel = new List<CommonPackagingItemHeaderModel>();
            if (commonPackagingItemHeaderList.Count > 0)
            {
                List<long?> masterIds = commonPackagingItemHeaderList.Where(w => w.UomId != null).Select(a => a.UomId).Distinct().ToList();
                masterIds.AddRange(commonPackagingItemHeaderList.Where(w => w.PackagingItemCategoryId != null).Select(a => a.PackagingItemCategoryId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingItemHeaderList.ForEach(s =>
                {
                    CommonPackagingItemHeaderModel commonPackagingItemHeaderModels = new CommonPackagingItemHeaderModel
                    {
                        CommonPackagingItemId = s.CommonPackagingItemId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        AlsoKownAs = s.AlsoKownAs,
                        PackagingItemName = s.PackagingItemName,
                        PackagingItemCategoryId = s.PackagingItemCategoryId,
                        PackagingItemCategoryName = s.PackagingItemCategoryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "",
                        UomId = s.UomId,
                        UomName = s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                    };
                    commonPackagingItemHeaderModel.Add(commonPackagingItemHeaderModels);
                });
            }
            return commonPackagingItemHeaderModel.OrderByDescending(a => a.CommonPackagingItemId).ToList();
        }
        [HttpPost]
        [Route("GetCommonPackageReports")]
        public List<ReportModel> GetCommonPackageReports(ClassificationReportParam reportParam)
        {
            List<ReportModel> reportModels = new List<ReportModel>();
            ItemClassificationController itemClassificationController = new ItemClassificationController(_context, _mapper, _generateDocumentNoSeries);
            List<ItemClassificationFormsModel> itemClassificationFormsModels = itemClassificationController.GetItemForm((int?)reportParam.ItemClassificationMasterID, (int)reportParam.UserId);

            ReportModel reportModel = new ReportModel();

            List<Header> headers = new List<Header>
            {
                new Header{Text="Profile Reference No",Value="profileReferenceNo",Align="left" },
                new Header{Text="Packaging Item Name",Value="packagingItemName",Align="left" },
                new Header{Text="UOM",Value="uomName",Align="left" },
                new Header{Text="Also Kown As",Value="alsoKownAs",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },

            };
            reportModel.Headers = headers;
            reportModel.HeaderName = "Package Header";
            var commonPackageModel = GetCommonPackagingItemHeaderById((int)reportParam.ItemId);
            commonPackageModel.ForEach(c =>
            {
                reportModel.DynamicModels.Add(c as dynamic);
            });

            reportModels.Add(reportModel);
            itemClassificationFormsModels.ForEach(i =>
            {
                reportModel = GenerateCPReportModel(i.ModuleName, reportParam);
                reportModels.Add(reportModel);
                if (i.ApplicationFormSubModelList != null)
                {
                    reportModel = new ReportModel();
                    i.ApplicationFormSubModelList.ForEach(s =>
                    {
                        reportModel = GenerateCPReportModel(s.ModuleName, reportParam);
                        reportModels.Add(reportModel);
                    });
                }
            });

            return reportModels;
        }

        private ReportModel GenerateCPReportModel(string moduleName, ClassificationReportParam reportParam)
        {
            ReportModel reportModel = new ReportModel();
            CommonPackagingBottleController commonPackagingBottleController = new CommonPackagingBottleController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingBottleCapController commonPackagingBottleCapController = new CommonPackagingBottleCapController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingBottleInsertController commonPackagingBottleInsertController = new CommonPackagingBottleInsertController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingCapsealController commonPackagingCapsealController = new CommonPackagingCapsealController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingCartonController commonPackagingCartonController = new CommonPackagingCartonController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingDividerController commonPackagingDividerController = new CommonPackagingDividerController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingLayerPadController commonPackagingLayerPadController = new CommonPackagingLayerPadController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingNestingController commonPackagingNestingController = new CommonPackagingNestingController(_context, _mapper, _generateDocumentNoSeries);
            CommonPackagingShrinkWrapController commonPackagingShrinkWrapController = new CommonPackagingShrinkWrapController(_context, _mapper, _generateDocumentNoSeries);
            PackagingLabelController packagingLabelController = new PackagingLabelController(_context, _mapper, _generateDocumentNoSeries);
            PackagingLeafletController packagingLeafletController = new PackagingLeafletController(_context, _mapper, _generateDocumentNoSeries);
            PackagingTubeController packagingTubeController = new PackagingTubeController(_context, _mapper, _generateDocumentNoSeries);
            PackagingUnitBoxController packagingUnitBoxController = new PackagingUnitBoxController(_context, _mapper, _generateDocumentNoSeries);
            PackagingPvcFoilController packagingPvcFoilController = new PackagingPvcFoilController(_context, _mapper, _generateDocumentNoSeries);
            PackagingAluminiumFoilController packagingAluminiumFoilController = new PackagingAluminiumFoilController(_context, _mapper, _generateDocumentNoSeries);
            GeneralEquivalentNavisionController generalEquivalentNavisionController = new GeneralEquivalentNavisionController(_context, _mapper, _generateDocumentNoSeries);
            if (moduleName == "CommonPackagingBottle")
            {
                reportModel.HeaderName = "Bottle";
                List<Header> recipeHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Volume",Value="volume",Align="left" },
                        new Header{Text="Units",Value="packSizeunitsName",Align="left" },
                        new Header{Text="Code",Value="code",Align="left" },
                        new Header{Text="Color",Value="colorName",Align="left" },
                        new Header{Text="Shape",Value="shapeName",Align="left" },
                        new Header{Text="Type",Value="typeName",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = recipeHeaders;
                var commonPackagingBottles = commonPackagingBottleController.GetCommonPackagingBottleByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });
                foreach (var item in commonPackagingBottles)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingBottleCap")
            {
                reportModel.HeaderName = "Bottle Cap";
                List<Header> bottleCapHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Cap Measurement/mm",Value="capMeasurement",Align="left" },
                        new Header{Text="Code",Value="code",Align="left" },
                        new Header{Text="Cap Material",Value="packagingTypeName",Align="left" },
                        new Header{Text="Color",Value="packagingMaterialColorName",Align="left" },
                        new Header{Text="Cap Type",Value="capTypeName",Align="left" },
                    };
                reportModel.Headers = bottleCapHeaders;
                var bottleCapModels = commonPackagingBottleCapController.GetCommonPackagingBottleCapByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in bottleCapModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }

            }

            if (moduleName == "CommonPackagingBottleInsert")
            {
                reportModel.HeaderName = "Bottle Insert";
                List<Header> bottleInsertHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Code",Value="code",Align="left" },
                        new Header{Text="Measurement/mm",Value="measurement",Align="left" },
                        new Header{Text="Color",Value="packagingMaterialColorName",Align="left" },
                        new Header{Text="Material",Value="packagingTypeName",Align="left" },
                        new Header{Text="Shape",Value="packagingMaterialShapeName",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = bottleInsertHeaders;
                var commonPackagingBottleInserts = commonPackagingBottleInsertController.GetCommonPackagingBottleInsertByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingBottleInserts)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingCapseal")
            {
                reportModel.HeaderName = "Capseal";
                List<Header> capsealHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Measurement(L)/mm",Value="measurementLength",Align="left" },
                        new Header{Text="Measurement(W)/mm",Value="measurementWidth",Align="left" },
                        new Header{Text="Measurement(T)/mm",Value="measurementThickness",Align="left" },
                        new Header{Text="Thickness ±/mm",Value="thickness",Align="left" },
                        new Header{Text="Printing",Value="printing",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = capsealHeaders;
                var commonPackagingCapsealModels = commonPackagingCapsealController.GetCommonPackagingCapsealByRefByNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingCapsealModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingCarton")
            {
                reportModel.HeaderName = "Carton";
                List<Header> cartonHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Outer(L)/mm",Value="outerMeasurementLength",Align="left" },
                        new Header{Text="Outer(W)/mm",Value="outerMeasurementWidth",Align="left" },
                        new Header{Text="Outer(H)/mm",Value="outerMeasurementHeight",Align="left" },
                        new Header{Text="Inner(L)/mm",Value="innerMeasurementLength",Align="left" },
                        new Header{Text="Inner(W)/mm",Value="innerMeasurementWidth",Align="left" },
                        new Header{Text="Inner(H)/mm",Value="innerMeasurementHeight",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = cartonHeaders;
                var commonPackagingCartonModels = commonPackagingCartonController.GetCommonPackagingCartonByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingCartonModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingDivider")
            {
                reportModel.HeaderName = "Divider";
                List<Header> dividerHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Measurement(L)/mm",Value="measurementLength",Align="left" },
                        new Header{Text="Measurement(W)/mm",Value="measurementWidth",Align="left" },
                        new Header{Text="Divider To",Value="dividerTo",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = dividerHeaders;
                var commonPackagingDividerModels = commonPackagingDividerController.GetCommonPackagingDividerByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingDividerModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingLayerPad")
            {
                reportModel.HeaderName = "LayerPad";
                List<Header> layerPadHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Measurement(L)/mm",Value="measurementLength",Align="left" },
                        new Header{Text="Measurement(W)/mm",Value="measurementWidth",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = layerPadHeaders;
                var commonPackagingLayerPadModels = commonPackagingLayerPadController.GetCommonPackagingLayerPadByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingLayerPadModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingNesting")
            {
                reportModel.HeaderName = "Nesting";
                List<Header> nestingHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Row Measurement(L)",Value="rowMeasurementLength",Align="left" },
                        new Header{Text="Row Measurement(W)",Value="rowMeasurementWidth",Align="left" },
                        new Header{Text="Column Measurement(L)",Value="columnMeasurementLength",Align="left" },
                        new Header{Text="Column Measurement(W)",Value="columnMeasurementWidth",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = nestingHeaders;
                var commonPackagingNestingModels = commonPackagingNestingController.GetCommonPackagingNestingByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingNestingModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "CommonPackagingShrinkWrap")
            {
                reportModel.HeaderName = "ShrinkWrap";
                List<Header> shrinkWrapHeaders = new List<Header>
                    {
                        new Header{Text="Version Control",Value="versionControl",Align="left" },
                        new Header{Text="Measurement (L)/mm",Value="measurementLength",Align="left" },
                        new Header{Text="Measurement (w)/mm",Value="measurementWidth",Align="left" },
                        new Header{Text="Thickness/mm",Value="thickness",Align="left" },
                        new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                        new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                    };
                reportModel.Headers = shrinkWrapHeaders;
                var commonPackagingShrinkWrapModels = commonPackagingShrinkWrapController.GetCommonPackagingShrinkWrapByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in commonPackagingShrinkWrapModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingLabel")
            {
                reportModel.HeaderName = "Label";
                List<Header> labelHeaders = new List<Header>
                    {
                        new Header{Text="No of color",Value="noOfColor",Align="left" },
                        new Header{Text="Pantone Color",Value="pantoneColor",Align="left" },
                        new Header{Text="Color Value",Value="color",Align="left" },
                        new Header{Text="Lettering",Value="lettering",Align="left" },
                        new Header{Text="Background pantone Color",Value="bgPantoneColor",Align="left" },
                        new Header{Text="Background colorvalue",Value="bgColor",Align="left" }
                    };
                reportModel.Headers = labelHeaders;
                var packagingLabelModels = packagingLabelController.GetPackagingLabelsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1080 });

                foreach (var item in packagingLabelModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingLeaflet")
            {
                reportModel.HeaderName = "Leaflet";
                List<Header> leafletHeaders = new List<Header>
                    {

            new Header{Text= "Version Control", Value= "isVersion", Align="left" },
                        new Header{Text= "Size Length/mm", Value= "lengthSize", Align="left" },
                        new Header{Text= "Size Width/mm", Value= "widthSize", Align="left" },
                        new Header{Text= "Packing Material", Value= "packingMaterial", Align="left" },
                        new Header{Text= "Packing Item", Value= "packingItem", Align="left" },
                        new Header{Text= "Packing Unit", Value= "packingUnit", Align="left" },
                        new Header { Text = "Staus", Value = "statusCode", Align = "left" },
                    };
                reportModel.Headers = leafletHeaders;
                var packagingLeafletModels = packagingLeafletController.GetPackagingLeafletsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1082 });

                foreach (var item in packagingLeafletModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingTube")
            {
                reportModel.HeaderName = "Tube";
                List<Header> tubeHeaders = new List<Header>
                    {

                        new Header{Text= "Version Control", Value= "isVersion", Align="left" },
                        new Header{Text= "Size Length/mm", Value= "length", Align="left" },
                        new Header{Text= "Size Width/mm", Value= "width", Align="left" },
                        new Header{Text= "No of color", Value= "noOfColor", Align="left" },
                        new Header{Text= "Pantone Color", Value= "pantoneColor", Align="left" },
                        new Header{Text= "Color Value", Value= "color", Align="left" },
                        new Header { Text = "Lettering", Value = "lettering", Align = "left" },
                        new Header { Text = "Staus", Value = "statusCode", Align = "left" },
                    };
                reportModel.Headers = tubeHeaders;
                var packagingTubeModels = packagingTubeController.GetPackagingTubesByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1084 });

                foreach (var item in packagingTubeModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingUnitBox")
            {
                reportModel.HeaderName = "UnitBox";
                List<Header> unitBoxHeaders = new List<Header>
                    {


                        new Header{Text= "Version Control", Value= "isVersion", Align="left" },
                        new Header{Text= "Size Length/mm", Value= "sizeLength", Align="left" },
                        new Header{Text= "Size Width/mm", Value= "sizeWidth", Align="left" },
                        new Header{Text= "Size Height/mm", Value= "sizeHeight", Align="left" },
                        new Header{Text= "No of color", Value= "noOfColor", Align="left" },
                        new Header{Text= "Pantone Color", Value= "pantoneColor", Align="left" },
                        new Header { Text = "Color Value", Value = "color", Align = "left" },
                        new Header { Text = "Lettering", Value = "lettering", Align = "left" },
                        new Header { Text = "Background pantone Color", Value = "bgPantoneColor", Align = "left" },
                        new Header { Text = "Background colorvalue", Value = "bgColor", Align = "left" },

                    };
                reportModel.Headers = unitBoxHeaders;
                var packagingUnitBoxModels = packagingUnitBoxController.GetPackagingUnitBoxByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1086 });

                foreach (var item in packagingUnitBoxModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingPvcFoil")
            {
                reportModel.HeaderName = "PvcFoil";
                List<Header> pvcFoilHeaders = new List<Header>
                    {

                        new Header{Text= "Version Control", Value= "isVersion", Align="left" },
                        new Header{Text= "PVC Type", Value= "blisterType", Align="left" },
                        new Header{Text= "Foil Thickness /mm", Value= "foilThickness", Align="left" },
                        new Header{Text= "Foil Width /mm", Value= "foilWidth", Align="left" },
                        new Header{Text= "Staus", Value= "stausCode", Align="left" },
                        new Header{Text= "AddedBy", Value= "addedByUser", Align="left" },
                        new Header { Text = "ModifiedBy", Value = "modifiedByUser", Align = "left" },
                        new Header { Text = "AddedDate", Value = "addedDate", Align = "left" },


                    };
                reportModel.Headers = pvcFoilHeaders;
                var packagingPvcFoilHeadersModels = packagingPvcFoilController.GetPackagingPvcFoilsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1090 });

                foreach (var item in packagingPvcFoilHeadersModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingaluminiumFoil")
            {
                reportModel.HeaderName = "AluminiumFoil";
                List<Header> aluminiumFoilHeaders = new List<Header>
                    {
                        new Header{Text= "Version Control", Value= "isVersion", Align="left" },
                        new Header{Text= "No of Color", Value= "noOfColor", Align="left" },
                        new Header { Text = "Pantone Color", Value = "pantoneColor", Align = "left" },
                        new Header { Text = "Color Value", Value = "color", Align = "left" },
                        new Header { Text = "Lettering", Value = "lettering", Align = "left" },
                        new Header { Text = "Background Pantone Color", Value = "bgPantoneColor", Align = "left" },
                        new Header { Text = "Background Color Value", Value = "bgColor", Align = "left" },
                        new Header{Text= "Foil Thickness", Value= "foilThickness", Align="left" },
                        new Header{Text= "Foil Width", Value= "foilWidth", Align="left" },



                    };
                reportModel.Headers = aluminiumFoilHeaders;
                var packagingAluminiumFoilHeadersModels = packagingAluminiumFoilController.GetPackagingAluminiumFoilsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1088 });

                foreach (var item in packagingAluminiumFoilHeadersModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "GeneralNamingEquivalent")
            {
                reportModel.HeaderName = "GeneralNamingEquivalent";
                List<Header> generalNamingEquivalentHeaders = new List<Header>
                    {

                        new Header{Text= "Database", Value= "database", Align="left" },
                        new Header{Text= "Navision Name", Value= "navision", Align="left" },
                        new Header { Text = "BUOM", Value = "navisionBuom", Align = "left" },
                        new Header { Text = "Navision Status", Value = "statusCode", Align = "left" },
                        new Header { Text = "Added By", Value = "addedByUser", Align = "left" },
                        new Header { Text = "Added Date", Value = "addedDate", Align = "left" },
                        new Header { Text = "Background Color Value", Value = "bgColor", Align = "left" },
                        new Header{Text= "Foil Thickness", Value= "foilThickness", Align="left" },
                        new Header{Text= "Foil Width", Value= "foilWidth", Align="left" },

                    };
                reportModel.Headers = generalNamingEquivalentHeaders;
                var generalNamingEquivalentHeadersModels = generalEquivalentNavisionController.GetGeneralEquivalentNavisionsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });

                foreach (var item in generalNamingEquivalentHeadersModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingLabelForCost")
            {
                reportModel.HeaderName = "LabelForCost";
                List<Header> labelForCostHeaders = new List<Header>
                    {
                        new Header{Text="No Of Color",Value="noOfColor",Align="left" },
                        new Header{Text="Size(L)",Value="sizeLength",Align="left" },
                        new Header{Text="Size(W)",Value="sizeWidth",Align="left" },
                        new Header{Text="Packaging Material",Value="packagingMaterial",Align="left" },
                        new Header{Text="Packing",Value="packagingItem",Align="left" },

                    };
                reportModel.Headers = labelForCostHeaders;
                var packagingLabelForCostModels = packagingLabelController.GetPackagingLabelsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1081 });

                foreach (var item in packagingLabelForCostModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingLeafletForCost")
            {
                reportModel.HeaderName = "LeafletForCost";
                List<Header> leafletForCostHeaders = new List<Header>
                    {

                        new Header{Text= "Printed On", Value= "printedOnType", Align="left" },
                        new Header{Text= "Size (L)", Value= "lengthSize", Align="left" },
                        new Header{Text= "Size (W)", Value= "widthSize", Align="left" },
                        new Header{Text= "Packing Material", Value= "packingMaterial", Align="left" },
                        new Header{Text = "Staus", Value = "statusCode", Align = "left" },
                    };
                reportModel.Headers = leafletForCostHeaders;
                var packagingleafletForCostModels = packagingLeafletController.GetPackagingLeafletsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1083 });

                foreach (var item in packagingleafletForCostModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingTubeForCost")
            {
                reportModel.HeaderName = "TubeForCost";
                List<Header> tubeForCostHeaders = new List<Header>
                    {
                        new Header{Text= "Printed", Value= "printed", Align="left" },
                        new Header { Text = "No of color", Value = "noOfColor", Align = "left" },
                        new Header{Text= "Diameter", Value= "diameter", Align="left" },
                        new Header{Text= "Width", Value= "width", Align="left" },

                        new Header{Text= "Length", Value= "length", Align="left" },
                        new Header{Text= "Coating Material", Value= "coatingMaterialName", Align="left" },
                        new Header { Text = "Staus", Value = "statusCode", Align = "left" },
                    };
                reportModel.Headers = tubeForCostHeaders;
                var packagingtubeForCostModels = packagingTubeController.GetPackagingTubesByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1085 });

                foreach (var item in packagingtubeForCostModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingUnitBoxForCost")
            {
                reportModel.HeaderName = "UnitBoxForCost";
                List<Header> unitBoxForCostHeaders = new List<Header>
                    {


                         new Header { Text = "No of color", Value = "noOfColor", Align = "left" },
                        new Header{Text= "Size(L)", Value= "sizeLength", Align="left" },
                        new Header{Text= "Size(W)", Value= "sizeWidth", Align="left" },
                        new Header{Text= "Size(H)", Value= "sizeHeight", Align="left" },

                        new Header{Text= "Grammage", Value= "grammage", Align="left" },
                        new Header { Text = "Finishing", Value = "finishingName", Align = "left" },
                        new Header { Text = "Packaging Material", Value = "packagingMaterial", Align = "left" },


                    };
                reportModel.Headers = unitBoxForCostHeaders;
                var packagingunitBoxForCostModels = packagingUnitBoxController.GetPackagingUnitBoxByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1087 });

                foreach (var item in packagingunitBoxForCostModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "PackagingPvcFoilForCost")
            {
                reportModel.HeaderName = "PvcFoilForCost";
                List<Header> pvcFoilForCostHeaders = new List<Header>
                    {
                        new Header{Text= "Blister Type", Value= "blisterType", Align="left" },
                        new Header{Text= "Foil Thickness", Value= "foilThickness", Align="left" },
                        new Header{Text= "Foil Width", Value= "foilWidth", Align="left" },


                    };
                reportModel.Headers = pvcFoilForCostHeaders;
                var pvcFoilForCostHeadersModels = packagingPvcFoilController.GetPackagingPvcFoilsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1091 });

                foreach (var item in pvcFoilForCostHeadersModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }

            if (moduleName == "PackagingaluminiumFoilForCost")
            {
                reportModel.HeaderName = "AluminiumFoilForCost";
                List<Header> aluminiumFoilForCostHeaders = new List<Header>
                    {
                        new Header{Text= "Printed", Value= "printed", Align="left" },
                        new Header{Text= "No of Color", Value= "noOfColor", Align="left" },
                        new Header{Text= "Foil Thickness", Value= "foilThickness", Align="left" },
                        new Header{Text= "Foil Width", Value= "foilWidth", Align="left" },
                    };
                reportModel.Headers = aluminiumFoilForCostHeaders;
                var packagingAluminiumFoilForCostHeadersModels = packagingAluminiumFoilController.GetPackagingAluminiumFoilsByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo, TypeID = 1089 });

                foreach (var item in packagingAluminiumFoilForCostHeadersModels)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            return reportModel;
        }
    }
}