﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using APP.EntityModel.SearchParam;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskCommentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly MailService _mailService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public TaskCommentController(CRT_TMSContext context, IMapper mapper, MailService mailService, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _mailService = mailService;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskComment")]
        public List<TaskCommentModel> Get(long Id, long userId)
        {
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            var applicationUsersList = _context.Employee.Include(a => a.User).Where(s => s.User.StatusCodeId == 1).AsNoTracking().ToList();
            var documents = _context.Documents.Select(s => new
            {
                s.DocumentId,
                s.ContentType,
                s.FileName,
                s.SessionId,
            }).ToList();
            var taskCommentUser = _context.TaskCommentUser.Select(s => new
            {
                s.TaskCommentId,
                s.TaskCommentUserId,
                s.TaskMasterId,
                s.UserId,
                s.IsRead,
                s.IsPin,
                s.IsClosed,
                s.IsAssignedTo,
                s.PinnedBy,
                s.DueDate,
            }).Where(t => t.TaskMasterId == Id).AsNoTracking().ToList();
            var taskComments = _context.TaskComment
                                    .Include("CommentedByNavigation")
                                    .Include("TaskCommentUser")
                                    .Include("TaskCommentUser.User")
                                    .Include("TaskMaster")
                                    .Include("CommentAttachment").Where(t => t.TaskMasterId == Id && t.ParentCommentId == null && t.IsDocument == false).OrderBy(o => o.TaskCommentId).AsNoTracking().ToList();
            var query = taskCommentUser?.Where(t => t.UserId == userId).ToList();
            var alltaskcommentUser = taskCommentUser?.Where(t => t.UserId != userId).ToList();
            var taskCommentIds = new List<long>();
            var unAssignedtaskCommentIds = new List<long>();
            var parentCommentIds = new List<long?>();
            List<long> filtertaskcommentIds = new List<long>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = taskCommentUser?.Where(t => t.UserId == userId).Select(s => s.TaskCommentId).Distinct().ToList();
            //if (commentIds.Distinct().Any())
            //{
            //    commentIds.ForEach(c =>
            //    {
            //        var commentTotalCount = taskCommentUser?.Where(w => w.TaskCommentId == c).ToList().Count;
            //        var unAssignedcount = taskCommentUser?.Where(w => w.TaskCommentId == c && w.IsAssignedTo != true).ToList().Count;
            //        if (commentTotalCount == unAssignedcount)
            //        {
            //            long? commetid = new long?();
            //            commetid = taskCommentUser?.FirstOrDefault(w => w.TaskCommentId == c && w.IsAssignedTo != true)?.TaskCommentId;
            //            if (commetid != null)
            //            {
            //                taskCommentIds.Add(commetid.Value);
            //            }
            //        }
            //    });
            //    //var taskCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo != true).Select(s => s.TaskCommentId).ToList();
            //    assignedCommentIds = taskCommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId != userId).Select(s => s.TaskCommentId).Distinct().ToList();
            //    assignedUserCommentIds = taskCommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId == userId).Select(s => s.TaskCommentId).Distinct().ToList();
            //}


            taskComments = taskComments?.Where(t => commentIds.Contains(t.TaskCommentId)).ToList();
            taskComments.ForEach(s =>
            {
                TaskCommentModel taskCommentModel = new TaskCommentModel();

                taskCommentModel.TaskCommentID = s.TaskCommentId;
                taskCommentModel.TaskSubject = s.TaskSubject;
                taskCommentModel.TaskMasterID = s.TaskMasterId;
                taskCommentModel.Comment = s.Comment;
                taskCommentModel.CommentedBy = s.CommentedBy;
                taskCommentModel.CommentedByName = s.CommentedByNavigation.UserName;
                taskCommentModel.AddedByUser = s.CommentedByNavigation.UserName;
                taskCommentModel.AddedByUserID = s.CommentedByNavigation.UserId;
                taskCommentModel.CommentedDate = s.CommentedDate;
                taskCommentModel.ParentCommentId = s.ParentCommentId;
                taskCommentModel.MainTaskId = s.MainTaskId;
                taskCommentModel.TaskId = s.TaskId;
                taskCommentModel.OverDueAllowed = s.TaskMaster.OverDueAllowed == "true" ? false : true;
                if (s.TaskMaster.IsNoDueDate == false)
                {
                    taskCommentModel.DueDate = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.UserId==userId).Select(t => t.DueDate).FirstOrDefault() == null ? s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true && tc.UserId == userId).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.UserId == userId).Select(t => t.DueDate).FirstOrDefault();
                }
                taskCommentModel.MessageAssignedTos = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.UserId).Distinct().ToList();
                //AssignedToUsers = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.User?.Employe?.FirstName +"|"+ t.User?.Employe?.LastName +"|"+ t.User?.Employe?.NickName).Distinct().ToList(),
                taskCommentModel.CommentAttachmentModel = new CommentAttachmentModel
                {
                    DocumentId = s.CommentAttachment.FirstOrDefault() != null ? s.CommentAttachment.FirstOrDefault().DocumentId : 0,
                    FileName = s.CommentAttachment.FirstOrDefault() != null ? s.CommentAttachment.FirstOrDefault().FileName : string.Empty,
                    FileNames = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(c => c.FileName).ToList(),
                    DocumentIdList = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(c => c.DocumentId).ToList(),
                    FileDocumentList = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(t => new DocumentsModel
                    {
                        DocumentID = t.DocumentId,
                        FileName = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.FileName,
                        ContentType = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.ContentType,

                    }).ToList(),
                    //ContentType = s.CommentAttachment.FirstOrDefault().Document != null ? s.CommentAttachment.FirstOrDefault().Document.ContentType : string.Empty,
                    ContentTypes = documents?.Where(f => f.DocumentId == s.CommentAttachment.FirstOrDefault()?.DocumentId).Select(c => c.ContentType).ToList(),
                };

                taskCommentModel.TaskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskMaster.TaskId,
                    AssignedTo = new List<long?> { s.TaskMaster.AssignedTo },
                    AddedByUserID = s.TaskMaster.AddedByUserId,
                };
                taskCommentModel.IsEdited = s.IsEdited;
                taskCommentModel.IsQuote = s.IsQuote;


                taskCommentModel.IsRead = s.CommentedBy == userId ? false : s.TaskCommentUser.Where(u => u.UserId == userId).Any(a => a.IsRead == false);
                //CssClass = s.TaskCommentUser.Any(u => u.StatusCodeId == 516) == false ? "transparent" : "blue-grey lighten-3",
                //SubjectStatus = s.TaskCommentUser.Any(u => u.StatusCodeId == 516) == false ? "Open" : "Closed",
                taskCommentModel.CssClass = s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 516 ? "blue-grey lighten-3" : s.TaskCommentUser?.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 513 ? "yellow" : "transparent";
                taskCommentModel.SubjectStatus = s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 516 ? "Closed" : s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 513 ? "Completed" : "Open";
                taskCommentModel.EditedBy = s.EditedBy;
                taskCommentModel.EditedDate = s.EditedDate;
                taskCommentModel.StatusCodeID = s.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == s.TaskCommentId && u.UserId == s.CommentedBy)?.StatusCodeId;
                taskCommentModel.SubCommentStatusCodeID = s.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == s.TaskCommentId && t.UserId == s.CommentedBy && s.ParentCommentId == null)?.StatusCodeId;
                taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == userId).Select(t => t.IsPin).FirstOrDefault();
                taskCommentModel.PinnedBy = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == userId).Select(t => t.PinnedBy).FirstOrDefault();
                taskCommentModel.PinnedByUser = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsPin == true && tc.PinnedByNavigation != null).Select(t => t.PinnedByNavigation.UserName).FirstOrDefault();
                taskCommentModel.TaskCommentModels = new List<TaskCommentModel>();

                taskCommentModels.Add(taskCommentModel);

            });


            taskCommentModels.ForEach(sc =>
            {
                List<TaskCommentModel> subMessagesModels = new List<TaskCommentModel>();
                sc.CssClass = sc.StatusCodeID == 516 ? "blue-grey lighten-3" : sc.StatusCodeID == 513 ? "yellow" : "transparent";
                var subMessages = _context.TaskComment
                                    .Include("CommentedByNavigation")
                                    .Include("TaskCommentUser")
                                    .Include("CommentAttachment")
                                    .Include("QuoteComment")
                                    .Include("QuoteComment.CommentedByNavigation")
                                    .Where(t => t.ParentCommentId == sc.TaskCommentID).AsNoTracking().ToList();
                subMessages.ForEach(reply =>
                {
                    TaskCommentModel subMessageModel = new TaskCommentModel();

                    subMessageModel.TaskCommentID = reply.TaskCommentId;
                    subMessageModel.TaskSubject = reply.TaskSubject;
                    subMessageModel.TaskMasterID = reply.TaskMasterId;
                    subMessageModel.Comment = reply.Comment;
                    subMessageModel.CommentedBy = reply.CommentedBy;
                    subMessageModel.AddedByUser = reply.CommentedByNavigation?.UserName;
                    subMessageModel.CommentedDate = reply.CommentedDate;
                    subMessageModel.ParentCommentId = reply.ParentCommentId;
                    subMessageModel.MainTaskId = reply.MainTaskId;
                    subMessageModel.TaskId = reply.TaskId;
                    subMessageModel.QuoteCommentId = reply.QuoteCommentId;
                    subMessageModel.QuoteComment = reply.QuoteComment?.Comment;
                    subMessageModel.QuoteCommentDate = reply.QuoteComment?.CommentedDate;
                    subMessageModel.QuoteCommentUser = reply.QuoteComment?.CommentedByNavigation?.UserName;
                    subMessageModel.CommentAttachmentModel = new CommentAttachmentModel
                    {
                        DocumentId = reply.CommentAttachment.FirstOrDefault() != null ? reply.CommentAttachment.FirstOrDefault().DocumentId : 0,
                        FileName = reply.CommentAttachment.FirstOrDefault() != null ? reply.CommentAttachment.FirstOrDefault().FileName : string.Empty,
                        FileNames = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => c.FileName).ToList(),
                        DocumentIdList = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => c.DocumentId).ToList(),
                        FileDocumentList = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(t => new DocumentsModel
                        {
                            DocumentID = t.DocumentId,
                            FileName = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.FileName,
                            ContentType = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.ContentType,
                        }).ToList(),
                        ContentTypes = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => documents?.FirstOrDefault(f => f.DocumentId == c.DocumentId)?.ContentType).ToList(),
                    };
                    subMessageModel.IsEdited = reply.IsEdited;
                    subMessageModel.IsQuote = reply.IsQuote;

                    subMessageModel.IsRead = reply.CommentedBy == userId ? false : reply.TaskCommentUser.Where(u => u.UserId == userId).Any(a => a.IsRead == false);
                    subMessageModel.CssClass = reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy)?.StatusCodeId == 516 ? "blue-grey lighten-3" : reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy)?.StatusCodeId == 513 ? "yellow" : "transparent";
                    subMessageModel.SubjectStatus = reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy)?.StatusCodeId == 516 ? "Closed" : reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy)?.StatusCodeId == 513 ? "Completed" : "Open";
                    //CssClass= reply.StatusCodeID == 516 ? "blue-grey lighten-3" : sc.StatusCodeID == 513 ? "yellow" : "transparent",
                    subMessageModel.EditedBy = reply.EditedBy;
                    subMessageModel.EditedDate = reply.EditedDate;
                    subMessageModel.StatusCodeID = reply.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == reply.TaskCommentId && u.UserId == reply.CommentedBy)?.StatusCodeId;
                    subMessageModel.SubCommentStatusCodeID = reply.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == reply.CommentedBy && reply.ParentCommentId == null)?.StatusCodeId;
                    subMessageModel.IsPin = reply.TaskCommentUser.Where(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == userId).Select(t => t.IsPin).FirstOrDefault();
                    subMessageModel.PinnedBy = reply.TaskCommentUser.Where(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == userId).Select(t => t.PinnedBy).FirstOrDefault();
                    subMessageModel.PinnedByUser = reply.TaskCommentUser.Where(tc => tc.TaskMasterId == reply.TaskMasterId && tc.TaskCommentId == reply.TaskCommentId && tc.UserId == userId && tc.IsPin == true).Select(t => t.PinnedByNavigation?.UserName).FirstOrDefault();

                    subMessagesModels.Add(subMessageModel);
                });


                sc.TaskCommentModels = subMessagesModels;
                //sc.IsRead = sc.TaskCommentModels.Any(m => m.IsRead == false);

            });

            taskCommentModels.ForEach(t =>
            {
                if (t.MessageAssignedTos != null && t.MessageAssignedTos.Count > 0)
                {
                    t.AssignedToUsers = applicationUsersList?.Where(a => t.MessageAssignedTos.Contains(a.User.UserId)).Select(s => s.FirstName + " | " + s.LastName + " | " + s.NickName).ToList();
                }
                if (t.TaskCommentModels.Any())
                {
                    var lastTaskComment = t.TaskCommentModels.LastOrDefault();
                    t.LastCommentedDate = lastTaskComment.CommentedDate;
                    t.LastCommentedBy = lastTaskComment.AddedByUser;
                    t.IsRead = t.TaskCommentModels.Any(c => c.IsRead.GetValueOrDefault(false));
                }
                else
                {
                    t.LastCommentedDate = t.CommentedDate;
                    t.LastCommentedBy = t.AddedByUser;
                }
            });

            return taskCommentModels;
        }
        [HttpPost]
        [Route("UpdateTaskCommentDueExtendDate")]
        public TaskCommentModel UpdateTaskCommentDueExtendDate(TaskCommentModel TaskCommentModel)
        {
            var TaskCommentUser = _context.TaskCommentUser.Where(w => w.TaskCommentId == TaskCommentModel.TaskCommentID && w.UserId==TaskCommentModel.AddedByUserID).ToList();
            if (TaskCommentUser != null)
            {
                TaskCommentUser.ForEach(s =>
                {
                    s.DueDate = TaskCommentModel.DueDate;
                });
                _context.SaveChanges();
            }
            var latestExtenddue = _context.TaskCommentUser.Where(w => w.TaskMasterId == TaskCommentModel.TaskMasterID && w.UserId == TaskCommentModel.AddedByUserID).OrderByDescending(t => t.DueDate).FirstOrDefault()?.DueDate;
            var Task = _context.TaskMaster.Where(w => w.TaskId == TaskCommentModel.TaskMasterID).FirstOrDefault();
            if (Task != null)
            {
                Task.NewDueDate = latestExtenddue != null ? latestExtenddue : null;
                _context.SaveChanges();
            }
            var TaskAssigned = _context.TaskAssigned.Where(w => w.TaskId == TaskCommentModel.TaskMasterID && w.UserId== TaskCommentModel.AddedByUserID).ToList();
            if (TaskAssigned != null)
            {
                TaskAssigned.ForEach(t =>
                {
                    t.NewDueDate = latestExtenddue != null ? latestExtenddue : null;
                    _context.SaveChanges();
                });

            }
            var TaskMembers = _context.TaskMembers.Where(w => w.TaskId == TaskCommentModel.TaskMasterID && w.AssignedCc == TaskCommentModel.AddedByUserID).ToList();
            if (TaskMembers != null)
            {
                TaskMembers.ForEach(t =>
                {
                    t.NewDueDate = latestExtenddue != null ? latestExtenddue : null;
                    _context.SaveChanges();
                });

            }
            return TaskCommentModel;
        }
        [HttpGet]
        [Route("GetTaskCommentUnreadClick")]
        public List<TaskCommentModel> GetTaskCommentUnreadClick(long Id, long userId)
        {
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            var applicationUsersList = _context.Employee.Include(a => a.User).Where(s => s.User.StatusCodeId == 1).AsNoTracking().ToList();
            var taskCommentUser = _context.TaskCommentUser.Select(s => new
            {
                s.TaskCommentId,
                s.TaskCommentUserId,
                s.TaskMasterId,
                s.UserId,
                s.IsRead,
                s.IsPin,
                s.IsClosed,
                s.IsAssignedTo,
                s.PinnedBy,
                s.DueDate,
            }).Where(t => t.TaskMasterId == Id).AsNoTracking().ToList();
            var alltaskcommentUser = _context.TaskCommentUser.Where(t => t.UserId != userId && t.TaskMasterId == Id).AsNoTracking().ToList();
            var taskCommentIds = new List<long>();
            var parentCommentIds = new List<long?>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = taskCommentUser?.Select(s => s.TaskCommentId).ToList();
            //if (commentIds.Any())
            //{
            //    commentIds.ForEach(c =>
            //    {
            //        var commentTotalCount = taskCommentUser?.Where(w => w.TaskCommentId == c).ToList().Count;
            //        var unAssignedcount = taskCommentUser?.Where(w => w.TaskCommentId == c && w.IsAssignedTo != true).ToList().Count;
            //        if (commentTotalCount == unAssignedcount)
            //        {
            //            long? commetid = new long?();
            //            commetid = taskCommentUser?.FirstOrDefault(w => w.TaskCommentId == c && w.IsAssignedTo != true)?.TaskCommentId;
            //            if (commetid != null)
            //            {
            //                taskCommentIds.Add(commetid.Value);
            //            }
            //        }
            //    });
            //    //taskCommentIds = alltaskcommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo != true).Select(s => s.TaskCommentId).Distinct().ToList();
            //    //var taskCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo != true).Select(s => s.TaskCommentId).ToList();
            //    assignedCommentIds = taskCommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId != userId).Select(s => s.TaskCommentId).Distinct().ToList();
            //    assignedUserCommentIds = taskCommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId == userId).Select(s => s.TaskCommentId).Distinct().ToList();
            //}
            //var assignedCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo == true).Select(s => s.TaskCommentId).ToList();
            var dashItems = new List<TaskCommentModel>();
            var subCommentItems = new List<TaskCommentModel>();
            var mainComments = new List<TaskCommentModel>();
            //if (assignedUserCommentIds.Any())
            //{
            //    taskCommentIds.AddRange(assignedUserCommentIds);
            //}
            //if (assignedCommentIds.Any())
            //{
            //    assignedCommentIds.ForEach(a =>
            //    {
            //        if (taskCommentIds.Contains(a))
            //        {
            //            taskCommentIds.Remove(a);
            //        }

            //    });
            //}

            var documents = _context.Documents.Select(s => new
            {
                s.DocumentId,
                s.ContentType,
                s.FileName,
                s.SessionId,
            }).ToList();
            var taskComments = _context.TaskComment
                                    .Include("CommentedByNavigation")
                                    .Include("TaskCommentUser")
                                    .Include("TaskCommentUser.User")
                                    .Include("TaskMaster")
                                    .Include("CommentAttachment").Where(t => t.TaskMasterId == Id && (t.ParentCommentId == null && t.IsDocument == false || t.CommentedBy == userId)).OrderBy(o => o.TaskCommentId).AsNoTracking().ToList();
            //taskComments = taskComments?.Where(t => taskCommentIds.Contains(t.TaskCommentId) || t.CommentedBy == userId).ToList();
            taskComments.ForEach(s =>
            {
                TaskCommentModel taskCommentModel = new TaskCommentModel();

                taskCommentModel.TaskCommentID = s.TaskCommentId;
                taskCommentModel.TaskSubject = s.TaskSubject;
                taskCommentModel.TaskMasterID = s.TaskMasterId;
                taskCommentModel.Comment = s.Comment;
                taskCommentModel.CommentedBy = s.CommentedBy;
                taskCommentModel.CommentedByName = s.CommentedByNavigation.UserName;
                taskCommentModel.AddedByUser = s.CommentedByNavigation.UserName;
                taskCommentModel.AddedByUserID = s.CommentedByNavigation.UserId;
                taskCommentModel.CommentedDate = s.CommentedDate;
                taskCommentModel.ParentCommentId = s.ParentCommentId;
                taskCommentModel.MainTaskId = s.MainTaskId;
                taskCommentModel.TaskId = s.TaskId;
                if (s.TaskMaster.IsNoDueDate == false)
                {
                    taskCommentModel.DueDate = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId).Select(t => t.DueDate).FirstOrDefault() == null ? s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId).Select(t => t.DueDate).FirstOrDefault();
                }
                taskCommentModel.MessageAssignedTos = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.UserId).Distinct().ToList();
                //AssignedToUsers = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.User?.Employe?.FirstName +"|"+ t.User?.Employe?.LastName +"|"+ t.User?.Employe?.NickName).Distinct().ToList(),
                taskCommentModel.CommentAttachmentModel = new CommentAttachmentModel
                {
                    DocumentId = s.CommentAttachment.FirstOrDefault() != null ? s.CommentAttachment.FirstOrDefault().DocumentId : 0,
                    FileName = s.CommentAttachment.FirstOrDefault() != null ? s.CommentAttachment.FirstOrDefault().FileName : string.Empty,
                    FileNames = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(c => c.FileName).ToList(),
                    DocumentIdList = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(c => c.DocumentId).ToList(),
                    FileDocumentList = s.CommentAttachment.Where(c => c.TaskCommentId == s.TaskCommentId).Select(t => new DocumentsModel
                    {
                        DocumentID = t.DocumentId,
                        FileName = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.FileName,
                        ContentType = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.ContentType,

                    }).ToList(),
                    //ContentType = s.CommentAttachment.FirstOrDefault().Document != null ? s.CommentAttachment.FirstOrDefault().Document.ContentType : string.Empty,
                    ContentTypes = documents?.Where(f => f.DocumentId == s.CommentAttachment.FirstOrDefault()?.DocumentId).Select(c => c.ContentType).ToList(),
                };

                taskCommentModel.TaskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskMaster.TaskId,
                    AssignedTo = new List<long?> { s.TaskMaster.AssignedTo },
                    AddedByUserID = s.TaskMaster.AddedByUserId,
                };
                taskCommentModel.IsEdited = s.IsEdited;
                taskCommentModel.IsQuote = s.IsQuote;


                taskCommentModel.IsRead = s.CommentedBy == userId ? false : s.TaskCommentUser.Where(u => u.UserId == userId).Any(a => a.IsRead == false);
                //CssClass = s.TaskCommentUser.Any(u => u.StatusCodeId == 516) == false ? "transparent" : "blue-grey lighten-3",
                //SubjectStatus = s.TaskCommentUser.Any(u => u.StatusCodeId == 516) == false ? "Open" : "Closed",
                taskCommentModel.CssClass = s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 516 ? "blue-grey lighten-3" : s.TaskCommentUser?.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 513 ? "yellow" : "transparent";
                taskCommentModel.SubjectStatus = s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 516 ? "Closed" : s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy)?.StatusCodeId == 513 ? "Completed" : "Open";
                taskCommentModel.EditedBy = s.EditedBy;
                taskCommentModel.EditedDate = s.EditedDate;
                taskCommentModel.StatusCodeID = s.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == s.TaskCommentId && u.UserId == s.CommentedBy)?.StatusCodeId;
                taskCommentModel.SubCommentStatusCodeID = s.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == s.TaskCommentId && t.UserId == s.CommentedBy && s.ParentCommentId == null)?.StatusCodeId;
                taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == userId).Select(t => t.IsPin).FirstOrDefault();
                taskCommentModel.PinnedBy = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == userId).Select(t => t.PinnedBy).FirstOrDefault();
                taskCommentModel.PinnedByUser = s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsPin == true && tc.PinnedByNavigation != null).Select(t => t.PinnedByNavigation.UserName).FirstOrDefault();
                taskCommentModel.TaskCommentModels = new List<TaskCommentModel>();

                taskCommentModels.Add(taskCommentModel);

            });


            taskCommentModels.ForEach(sc =>
            {
                List<TaskCommentModel> subMessagesModels = new List<TaskCommentModel>();
                sc.CssClass = sc.StatusCodeID == 516 ? "blue-grey lighten-3" : sc.StatusCodeID == 513 ? "yellow" : "transparent";
                var subMessages = _context.TaskComment
                                    .Include("CommentedByNavigation")
                                    .Include("TaskCommentUser")
                                    .Include("CommentAttachment")
                                    .Include("QuoteComment")
                                    .Include("QuoteComment.CommentedByNavigation")
                                    .Where(t => t.ParentCommentId == sc.TaskCommentID).AsNoTracking().ToList();
                subMessages.ForEach(reply =>
                {
                    TaskCommentModel subMessageModel = new TaskCommentModel();

                    subMessageModel.TaskCommentID = reply.TaskCommentId;
                    subMessageModel.TaskSubject = reply.TaskSubject;
                    subMessageModel.TaskMasterID = reply.TaskMasterId;
                    subMessageModel.Comment = reply.Comment;
                    subMessageModel.CommentedBy = reply.CommentedBy;
                    subMessageModel.AddedByUser = reply.CommentedByNavigation?.UserName;
                    subMessageModel.CommentedDate = reply.CommentedDate;
                    subMessageModel.ParentCommentId = reply.ParentCommentId;
                    subMessageModel.MainTaskId = reply.MainTaskId;
                    subMessageModel.TaskId = reply.TaskId;
                    subMessageModel.QuoteCommentId = reply.QuoteCommentId;
                    subMessageModel.QuoteComment = reply.QuoteComment?.Comment;
                    subMessageModel.QuoteCommentDate = reply.QuoteComment?.CommentedDate;
                    subMessageModel.QuoteCommentUser = reply.QuoteComment?.CommentedByNavigation?.UserName;
                    subMessageModel.CommentAttachmentModel = new CommentAttachmentModel
                    {
                        DocumentId = reply.CommentAttachment.FirstOrDefault() != null ? reply.CommentAttachment.FirstOrDefault().DocumentId : 0,
                        FileName = reply.CommentAttachment.FirstOrDefault() != null ? reply.CommentAttachment.FirstOrDefault().FileName : string.Empty,
                        FileNames = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => c.FileName).ToList(),
                        DocumentIdList = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => c.DocumentId).ToList(),
                        FileDocumentList = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(t => new DocumentsModel
                        {
                            DocumentID = t.DocumentId,
                            FileName = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.FileName,
                            ContentType = documents?.FirstOrDefault(f => f.DocumentId == t.DocumentId)?.ContentType,
                        }).ToList(),
                        //ContentType = reply.CommentAttachment.FirstOrDefault().Document != null ? reply.CommentAttachment.FirstOrDefault().Document.ContentType : string.Empty,
                        ContentTypes = reply.CommentAttachment.Where(c => c.TaskCommentId == reply.TaskCommentId).Select(c => documents?.FirstOrDefault(f => f.DocumentId == c.DocumentId)?.ContentType).ToList(),
                    };
                    subMessageModel.IsEdited = reply.IsEdited;
                    subMessageModel.IsQuote = reply.IsQuote;

                    subMessageModel.IsRead = reply.CommentedBy == userId ? false : reply.TaskCommentUser.Where(u => u.UserId == userId).Any(a => a.IsRead == false);
                    subMessageModel.CssClass = reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy).StatusCodeId == 516 ? "blue-grey lighten-3" : reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy).StatusCodeId == 513 ? "yellow" : "transparent";
                    subMessageModel.SubjectStatus = reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy).StatusCodeId == 516 ? "Closed" : reply.TaskCommentUser.FirstOrDefault(u => u.UserId == reply.CommentedBy).StatusCodeId == 513 ? "Completed" : "Open";
                    //CssClass= reply.StatusCodeID == 516 ? "blue-grey lighten-3" : sc.StatusCodeID == 513 ? "yellow" : "transparent",
                    subMessageModel.EditedBy = reply.EditedBy;
                    subMessageModel.EditedDate = reply.EditedDate;
                    subMessageModel.StatusCodeID = reply.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == reply.TaskCommentId && u.UserId == reply.CommentedBy).StatusCodeId;
                    subMessageModel.SubCommentStatusCodeID = reply.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == reply.CommentedBy && reply.ParentCommentId == null)?.StatusCodeId;
                    subMessageModel.IsPin = reply.TaskCommentUser.Where(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == userId).Select(t => t.IsPin).FirstOrDefault();
                    subMessageModel.PinnedBy = reply.TaskCommentUser.Where(t => t.TaskCommentId == reply.TaskCommentId && t.UserId == userId).Select(t => t.PinnedBy).FirstOrDefault();
                    subMessageModel.PinnedByUser = reply.TaskCommentUser.Where(tc => tc.TaskMasterId == reply.TaskMasterId && tc.TaskCommentId == reply.TaskCommentId && tc.UserId == userId && tc.IsPin == true).Select(t => t.PinnedByNavigation?.UserName).FirstOrDefault();

                    subMessagesModels.Add(subMessageModel);
                });


                sc.TaskCommentModels = subMessagesModels;
                //sc.IsRead = sc.TaskCommentModels.Any(m => m.IsRead == false);

            });

            taskCommentModels.ForEach(t =>
            {
                if (t.MessageAssignedTos != null && t.MessageAssignedTos.Count > 0)
                {
                    t.AssignedToUsers = applicationUsersList?.Where(a => t.MessageAssignedTos.Contains(a.User.UserId)).Select(s => s.FirstName + " | " + s.LastName + " | " + s.NickName).ToList();
                }
                if (t.TaskCommentModels.Any())
                {
                    var lastTaskComment = t.TaskCommentModels.LastOrDefault();
                    t.LastCommentedDate = lastTaskComment.CommentedDate;
                    t.LastCommentedBy = lastTaskComment.AddedByUser;
                    t.IsRead = t.TaskCommentModels.Any(c => c.IsRead.GetValueOrDefault(false));
                }
                else
                {
                    t.LastCommentedDate = t.CommentedDate;
                    t.LastCommentedBy = t.AddedByUser;
                }
            });

            return taskCommentModels;
        }
        [HttpGet]
        [Route("GetTaskOpenCommentByUserID")]
        public List<TaskCommentModel> GetTaskOpenCommentByUserID(long id)
        {
            List<TaskMaster> taskMasters = new List<TaskMaster>();
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            List<long?> taskIds = new List<long?>();
            var taskAssigned = _context.TaskAssigned.AsNoTracking().ToList();
            var taskMembers = _context.TaskMembers.AsNoTracking().ToList();
            var taskComments = _context.TaskComment
                                .Include(c => c.CommentedByNavigation)
                                .Include(c => c.TaskCommentUser)
                                .Where(t => t.IsDocument == false)
                                .OrderByDescending(o => o.TaskCommentId).AsNoTracking().ToList();
            taskIds = taskComments?.Select(s => s.TaskMasterId).Distinct().ToList();
            if (taskIds != null && taskIds.Count > 0)
            {
                taskMasters = _context.TaskMaster.Where(s => taskIds.Contains(s.TaskId)).AsNoTracking().ToList();
            }
            if (taskComments != null && taskComments.Count > 0)
            {
                taskComments.ForEach(s =>
                {
                    TaskCommentModel taskCommentModel = new TaskCommentModel();
                    {
                        taskCommentModel.TaskCommentID = s.TaskCommentId;
                        taskCommentModel.TaskSubject = s.TaskSubject;
                        taskCommentModel.TaskMasterID = s.TaskMasterId;
                        taskCommentModel.Comment = s.Comment;
                        taskCommentModel.CommentedBy = s.CommentedBy;
                        taskCommentModel.CommentedByName = s.CommentedByNavigation != null ? s.CommentedByNavigation.UserName : string.Empty;
                        taskCommentModel.AddedByUser = s.CommentedByNavigation != null ? s.CommentedByNavigation.UserName : string.Empty;
                        taskCommentModel.AddedByUserID = s.CommentedByNavigation != null ? s.CommentedByNavigation.UserId : new long?();
                        taskCommentModel.CommentedDate = s.CommentedDate;
                        taskCommentModel.ParentCommentId = s.ParentCommentId;
                        taskCommentModel.MainTaskId = s.MainTaskId;
                        taskCommentModel.TaskId = s.TaskId;
                        taskCommentModel.DueDate = s.TaskCommentUser != null ? s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault() : new DateTime?();
                        taskCommentModel.MessageAssignedTos = s.TaskCommentUser != null ? s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.UserId).Distinct().ToList() : new List<long>();
                        taskCommentModel.AssignedToUsers = s.TaskCommentUser != null ? s.TaskCommentUser.Where(tc => tc.TaskMasterId == s.TaskMasterId && tc.TaskCommentId == s.TaskCommentId && tc.IsAssignedTo == true).Select(t => t.User?.UserName).Distinct().ToList() : new List<string>();

                        taskCommentModel.IsEdited = s.IsEdited;
                        taskCommentModel.IsQuote = s.IsQuote;
                        taskCommentModel.IsRead = s.CommentedBy == id ? false : s.TaskCommentUser.Where(u => u.UserId == id).Any(a => a.IsRead == false);
                        taskCommentModel.CssClass = s.TaskCommentUser != null ? s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy) != null ? s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy).StatusCodeId == 516 ? "blue-grey lighten-3" : s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy).StatusCodeId == 513 ? "yellow" : "transparent" : string.Empty : string.Empty;
                        taskCommentModel.SubjectStatus = s.TaskCommentUser != null ? s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy) != null ? s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy).StatusCodeId == 516 ? "Closed" : s.TaskCommentUser.FirstOrDefault(u => u.UserId == s.CommentedBy).StatusCodeId == 513 ? "Completed" : "Open" : string.Empty : string.Empty;
                        taskCommentModel.EditedBy = s.EditedBy;
                        taskCommentModel.EditedDate = s.EditedDate;
                        taskCommentModel.StatusCodeID = s.TaskCommentUser != null ? s.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == s.TaskCommentId && u.UserId == s.CommentedBy) != null ? s.TaskCommentUser.FirstOrDefault(u => u.TaskCommentId == s.TaskCommentId && u.UserId == s.CommentedBy).StatusCodeId : null : null;
                        taskCommentModel.SubCommentStatusCodeID = s.TaskCommentUser != null ? s.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == s.TaskCommentId && t.UserId == s.CommentedBy && s.ParentCommentId == null) != null ? s.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == s.TaskCommentId && t.UserId == s.CommentedBy && s.ParentCommentId == null) != null ? s.TaskCommentUser.FirstOrDefault(t => t.TaskCommentId == s.TaskCommentId && t.UserId == s.CommentedBy && s.ParentCommentId == null).StatusCodeId : null : null : null;
                        taskCommentModel.IsPin = s.TaskCommentUser != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == id).Select(t => t.IsPin).FirstOrDefault() : false;

                        taskCommentModel.TaskCommentModels = new List<TaskCommentModel>();
                        taskCommentModels.Add(taskCommentModel);
                    };

                });

            }
            if (taskCommentModels != null && taskCommentModels.Count > 0)
            {
                taskCommentModels.ForEach(t =>
                {
                    bool isAssigned = taskAssigned.Any(f => f.TaskId == t.TaskMasterID && f.UserId == id);
                    bool isOnBehalf = false;
                    bool isAssignedCC = false;
                    if (!isAssigned)
                    {
                        isOnBehalf = taskAssigned.Any(f => f.TaskId == t.TaskMasterID && f.OnBehalfId == id);
                    }
                    if (!isOnBehalf)
                    {
                        isAssignedCC = taskMembers.Any(f => f.TaskId == t.TaskMasterID && f.AssignedCc == id);
                    }
                    if (t.TaskCommentModels.Any())
                    {

                        var lastTaskComment = t.TaskCommentModels.LastOrDefault();
                        t.LastCommentedDate = lastTaskComment.CommentedDate;
                        t.LastCommentedBy = lastTaskComment.AddedByUser;
                        t.IsRead = t.TaskCommentModels.Any(c => c.IsRead.GetValueOrDefault(false));
                        t.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                        t.TaskName = taskMasters?.Where(task => task.TaskId == t.TaskMasterID).Select(ta => ta.Title).FirstOrDefault();
                        t.TaskDueDate = taskMasters?.Where(task => task.TaskId == t.TaskMasterID).Select(ta => ta.DueDate).FirstOrDefault();
                        t.SubjectDueDate = t.DueDate;
                    }
                    else
                    {
                        t.LastCommentedDate = t.CommentedDate;
                        t.LastCommentedBy = t.AddedByUser;
                        t.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                        t.TaskName = taskMasters?.Where(task => task.TaskId == t.TaskMasterID).Select(ta => ta.Title).FirstOrDefault();
                        t.TaskDueDate = taskMasters?.Where(task => task.TaskId == t.TaskMasterID).Select(ta => ta.DueDate).FirstOrDefault();
                        t.SubjectDueDate = t.DueDate;
                    }
                });
            }
            return taskCommentModels;
        }
        [HttpPost()]
        [Route("GetTaskMessageBySearch")]
        public List<TaskCommentModel> GetTaskMessageBySearch(MessageSearchModel messageSearch)
        {
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            List<TaskCommentModel> query = new List<TaskCommentModel>();
            var taskComment = GetTaskOpenCommentByUserID(messageSearch.UserID.Value);
            if (messageSearch.CommentBy != null)
            {
                query = taskComment.Where(t => t.CommentedBy == messageSearch.CommentBy).ToList();
                taskCommentModels = query;
            }
            if (messageSearch.StatusCodeID != null)
            {
                if (query != null && query.Count > 0)
                {
                    query = query.Where(t => t.StatusCodeID == messageSearch.StatusCodeID).ToList();
                    taskCommentModels = query;
                }
                else
                {
                    query = taskComment.Where(t => t.StatusCodeID == messageSearch.StatusCodeID).ToList();
                    taskCommentModels = query;
                }

            }
            if (messageSearch.FromDate != DateTime.Today || messageSearch.ToDate != DateTime.Today)
            {
                if (query != null && query.Count > 0)
                {
                    query = query.Where(t => t.LastCommentedDate >= messageSearch.FromDate && t.LastCommentedDate <= messageSearch.ToDate).ToList();
                    taskCommentModels = query;
                }
                else
                {
                    query = taskComment.Where(t => t.LastCommentedDate >= messageSearch.FromDate && t.LastCommentedDate <= messageSearch.ToDate).ToList();
                    taskCommentModels = query;
                }
            }

            return taskCommentModels;

        }

        [HttpPost()]
        [Route("GetTaskSubCommentFilter")]
        public List<TaskCommentModel> GetSubcomment(TaskCommentSearchModel taskSearch)
        {
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            var taskComment = Get(taskSearch.TaskID.Value, taskSearch.UserID.Value);

            taskComment.ForEach(t =>
            {
                if (t.StatusCodeID == taskSearch.StatusCodeID || (!String.IsNullOrEmpty(taskSearch.SearchText) && t.TaskSubject.Trim().ToLower().Contains(taskSearch.SearchText.Trim().ToLower())))
                {
                    taskCommentModels.Add(t);
                }
                else if (t.TaskCommentModels.Any(s => s.StatusCodeID == taskSearch.StatusCodeID))
                {
                    taskCommentModels.Add(t);
                }
            });

            return taskCommentModels;

        }
        [HttpGet]
        [Route("GetDocComment")]
        public List<TaskCommentModel> GetDocComment(long Id, long userId)
        {
            var taskComment = _context.TaskComment.Include("CommentedByNavigation").Include("TaskCommentUser").Where(t => t.DocumentId == Id && t.ParentCommentId == null && t.IsDocument == true).Select(s => new TaskCommentModel
            {
                TaskCommentID = s.TaskCommentId,
                TaskSubject = s.TaskSubject,
                TaskMasterID = s.TaskMasterId,
                Comment = s.Comment,
                CommentedBy = s.CommentedBy,
                AddedByUser = s.CommentedByNavigation.UserName,
                CommentedDate = s.CommentedDate,
                ParentCommentId = s.ParentCommentId,
                IsEdited = s.IsEdited,
                EditedBy = s.EditedBy,
                EditedDate = s.EditedDate,
                IsRead = s.TaskCommentUser.Any(u => u.IsRead == false && u.UserId == userId),
                TaskCommentModels = new List<TaskCommentModel>()
            }).OrderBy(o => o.TaskCommentID).AsNoTracking().ToList();

            taskComment.ToList().ForEach(sc =>
            {

                var subMessages = _context.TaskComment.Include("CommentedByNavigation").Include("TaskCommentUser").Where(t => t.ParentCommentId == sc.TaskCommentID).Select(s => new TaskCommentModel
                {
                    TaskCommentID = s.TaskCommentId,
                    TaskSubject = s.TaskSubject,
                    TaskMasterID = s.TaskMasterId,
                    Comment = s.Comment,
                    CommentedBy = s.CommentedBy,
                    AddedByUser = s.CommentedByNavigation.UserName,
                    CommentedDate = s.CommentedDate,
                    IsRead = s.TaskCommentUser.Any(u => u.IsRead == false && u.UserId == userId),
                    ParentCommentId = s.ParentCommentId,
                    IsEdited = s.IsEdited,
                    EditedBy = s.EditedBy,
                    EditedDate = s.EditedDate,
                }).OrderBy(o => o.TaskCommentID).AsNoTracking().ToList();

                sc.TaskCommentModels = subMessages;

            });

            return taskComment;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskComment/{id:int}")]
        public ActionResult<TaskCommentModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskComment = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == id.Value);
            var result = _mapper.Map<TaskCommentModel>(taskComment);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskCommentModel> GetData(SearchModel searchModel)
        {
            var taskComment = new TaskComment();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).LastOrDefault();
                        break;
                    case "Next":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).LastOrDefault();
                        break;
                    case "Previous":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).LastOrDefault();
                        break;
                    case "Next":
                        taskComment = _context.TaskComment.OrderBy(o => o.TaskCommentId).FirstOrDefault(s => s.TaskCommentId > searchModel.Id);
                        break;
                    case "Previous":
                        taskComment = _context.TaskComment.OrderByDescending(o => o.TaskCommentId).FirstOrDefault(s => s.TaskCommentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskCommentModel>(taskComment);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskComment")]
        public TaskCommentModel Post(TaskCommentModel value)
        {
            var taskcommentUser = _context.TaskCommentUser.Where(t => t.TaskCommentId == value.TaskCommentID && t.TaskMasterId == value.TaskMasterID).ToList();

            var taskComment = new TaskComment
            {
                //  TaskCommentId = value.TaskCommentID,
                TaskMasterId = value.TaskMasterID,
                TaskSubject = value.TaskSubject,
                Comment = value.Comment,
                CommentedBy = value.CommentedBy,
                ParentCommentId = value.ParentCommentId,
                DocumentId = value.DocumentId,
                IsDocument = value.DocumentId != null ? true : false,
                CommentedDate = DateTime.Now,
                IsRead = false,
                IsEdited = false,
                SessionId = value.SessionID,
                QuoteCommentId = value.QuoteCommentId,
                IsQuote = value.IsQuote,
            };
            _context.TaskComment.Add(taskComment);
            value.TaskCommentID = taskComment.TaskCommentId;
            value.LastCommentedBy = value.AddedByUser;
            value.LastCommentedDate = value.CommentedDate;
            var users = new List<long?>();
            var taskassignee = _context.TaskAssigned.Where(p => p.TaskId == value.TaskMasterID).ToList();
            if (taskassignee != null && taskassignee.Count > 0)
            {
                users.Add(taskassignee.First().TaskOwnerId);
                if (taskassignee.First().OnBehalfId != null)
                {
                    users.Add(taskassignee.First().OnBehalfId);
                }
                taskassignee.ForEach(task =>
                {
                    users.Add(task.UserId);
                    if (value.CommentedBy != task.UserId)
                    {
                        task.StatusCodeId = 512;
                        task.IsRead = false;
                    }
                });

                var maintask = _context.TaskMaster.FirstOrDefault(f => f.TaskId == value.TaskMasterID);
                maintask.StatusCodeId = 512;
                if (value.CommentedBy != maintask.AddedByUserId)
                {
                    maintask.IsRead = false;
                }

                if (value.ParentCommentId != null)
                {
                    if (value.IsQuote == false)
                    {
                        var parComment = _context.TaskComment.Include("TaskCommentUser").FirstOrDefault(u => u.TaskCommentId == value.ParentCommentId);
                        if (parComment != null)
                        {
                            parComment.TaskCommentUser.ToList().ForEach(u =>
                            {
                                u.StatusCodeId = value.StatusCodeID;
                            });
                            _context.SaveChanges();
                        }
                    }
                }
            }


            if (value.DocumentId > 0)
            {
                var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == value.DocumentId && t.TaskMasterId == value.TaskMasterID);
                if (taskattach != null)
                {
                    taskattach.IsDiscussionNotes = true;
                }
            }

            _context.SaveChanges();
            var documentIds = _context.TaskAttachment.Where(t => t.IsLatestCommentAttachment == true).Select(t => t.DocumentId).ToList();
            if (documentIds.Count > 0)
            {
                var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FileName }).Where(d => documentIds.Contains(d.DocumentId)).ToList();
                value.CommentAttachmentModel = new CommentAttachmentModel();
                value.TaskCommentModels = new List<TaskCommentModel>();
                value.TaskMasterModel = new TaskMasterModel();

                if (documents.Any())
                {
                    var commentAttachement = new CommentAttachment();
                    documents.ForEach(d =>
                    {
                        commentAttachement = new CommentAttachment
                        {
                            TaskCommentId = taskComment.TaskCommentId,
                            DocumentId = d.DocumentId,
                            UserId = taskComment.CommentedBy.Value,
                            FileName = d.FileName
                        };
                        _context.CommentAttachment.Add(commentAttachement);
                        var latesttaskAttachment = _context.TaskAttachment.Where(t => t.IsLatestCommentAttachment == true && documentIds.Contains(t.DocumentId)).ToList();
                        if (latesttaskAttachment.Count > 0)
                        {
                            latesttaskAttachment.ForEach(f =>
                            {
                                f.IsLatestCommentAttachment = false;
                                f.TaskCommentId = taskComment.TaskCommentId;
                                f.IsComment = true;
                                _context.SaveChanges();

                            });
                        }
                        //var taskAttachment = new TaskAttachment
                        //{
                        //    DocumentId = d.DocumentId,
                        //    IsDiscussionNotes = false,
                        //    IsLatest = true,
                        //    IsLocked = false,
                        //    IsComment = true,
                        //    TaskCommentId = taskComment.TaskCommentId,
                        //    IsMajorChange = false,
                        //    IsMeetingNotes = false,
                        //    TaskMasterId = value.TaskMasterID,
                        //    UploadedByUserId = taskComment.CommentedBy,
                        //    UploadedDate = DateTime.Now,
                        //    VersionNo = "1.0",

                        //};
                        //_context.TaskAttachment.Add(taskAttachment);

                    });
                    value.CommentAttachmentModel = new CommentAttachmentModel
                    {
                        TaskCommentId = commentAttachement.TaskCommentId,
                        DocumentId = commentAttachement.DocumentId,
                        UserId = commentAttachement.UserId,
                        FileName = commentAttachement.FileName,
                        FileNames = documents.Where(d => d.SessionId == value.SessionID).Select(d => d.FileName).ToList(),
                        DocumentIdList = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.DocumentId).ToList(),
                        FileDocumentList = _context.CommentAttachment.Select(t => new DocumentsModel
                        {
                            ParentTaskID = t.TaskCommentId,
                            DocumentID = t.DocumentId,
                            FileName = t.Document.FileName,
                            ContentType = t.Document.ContentType,
                        }).Where(c => c.ParentTaskID == value.TaskCommentID).ToList(),
                        ContentTypes = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.Document.ContentType).ToList(),

                    };
                }
                _context.SaveChanges();
            }

            value.TaskCommentID = taskComment.TaskCommentId;
            value.TaskCommentModels = new List<TaskCommentModel>();
            var taskCC = _context.TaskMembers.Where(p => p.TaskId == value.TaskMasterID).Select(s => s.AssignedCc).ToList();
            if (taskCC.Count > 0)
                users.AddRange(taskCC);

            var status = value.DocumentId != null ? 552 : 551;

            //AddNotification(users, value.Comment, status);
            users = users.Distinct().ToList();
            users.ForEach(u =>
            {
                var taskcommentExituser = _context.TaskCommentUser.Where(t => t.TaskMasterId == value.TaskMasterID && t.TaskCommentId == taskComment.TaskCommentId && t.UserId == u).FirstOrDefault();
                if (taskcommentExituser == null)
                {
                    var userComment = new TaskCommentUser
                    {
                        IsRead = false,
                        IsClosed = false,
                        TaskCommentId = taskComment.TaskCommentId,
                        TaskMasterId = value.TaskMasterID.Value,
                        UserId = u.Value,
                        StatusCodeId = value.StatusCodeID,
                    };
                    _context.TaskCommentUser.Add(userComment);
                }
                _context.SaveChanges();
            });
            _context.SaveChanges();
            if ((value.MessageAssignedTos != null) && value.MessageAssignedTos.Any())
            {
                var taskcommentupdate = _context.TaskCommentUser.Where(t => t.TaskCommentId == taskComment.TaskCommentId && value.MessageAssignedTos.Contains(t.UserId)).ToList();
                if (taskcommentupdate.Count > 0)
                {
                    taskcommentupdate.ForEach(ta =>
                    {
                        var taskcommentuser = taskcommentupdate.Where(t => t.TaskCommentUserId == ta.TaskCommentUserId).FirstOrDefault();
                        taskcommentuser.IsAssignedTo = true;
                        taskcommentuser.DueDate = value.DueDate;
                        taskcommentuser.StatusCodeId = value.StatusCodeID;
                    });
                }


            }
            else
            {
                var taskcommentusertest = _context.TaskCommentUser.Where(t => t.TaskCommentId == value.TaskCommentID && t.TaskMasterId == value.TaskMasterID).ToList();
                taskcommentusertest.ForEach(tc =>
                {
                    tc.DueDate = value.DueDate;
                    tc.StatusCodeId = value.StatusCodeID;

                });


            }
            if (value.ParentCommentId != null)
            {
                var parenttaskcommentuser = _context.TaskCommentUser.Where(d => d.TaskCommentId == value.ParentCommentId && d.UserId == value.AddedByUserID).FirstOrDefault();
                if (parenttaskcommentuser != null)
                {
                    parenttaskcommentuser.IsClosed = false;
                }
            }
            _context.SaveChanges();
            value.MessageAssignedTos = value.MessageAssignedTos;
            value.AssignedToUsers = _context.TaskCommentUser.Where(tc => tc.TaskMasterId == value.TaskMasterID && tc.TaskCommentId == taskComment.TaskCommentId && tc.IsAssignedTo == true).Select(tc => tc.User.UserName).Distinct().ToList();
            value.SubjectStatus = taskcommentUser.Any(u => u.StatusCodeId == 516) == false ? "Open" : "Closed";
            if (value.InviteUserData != null)
            {
                value.InviteUserData.TaskCommentId = value.TaskCommentID;
                InsertTaskInviteUser(value.InviteUserData);
                if (value.InviteUserData.AssignTask.ToLower().Trim() == "inviteuser")
                {
                    SendEmailInviteMessage(value);
                }
                else
                {
                    SendEmailMessage(value);
                }
            }
            else
            {
                SendEmailMessage(value);
            }
            return value;
        }
        [HttpPost]
        [Route("InsertTaskInviteUser")]
        public TaskMasterModel InsertTaskInviteUser(InviteUserModel value)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "inviteuser"))
            {
                if (value.InviteUsers.Count > 0)
                {
                    var inviteuser = _context.TaskInviteUser.Where(w => w.TaskId == value.TaskId).ToList();
                    if (inviteuser.Count > 0)
                    {
                        _context.TaskInviteUser.RemoveRange(inviteuser);
                        _context.SaveChanges();
                    }
                    value.InviteUsers.ForEach(i =>
                    {

                        var taskInviteUser = _context.TaskInviteUser.Where(p => p.TaskId == value.TaskId && p.InviteUserId == i).FirstOrDefault();
                        if (taskInviteUser == null)
                        {
                            var taskInvite = new TaskInviteUser
                            {

                                TaskId = value.TaskId,
                                InviteUserId = i,
                                DueDate = value.DueDate,
                                InvitedDate = DateTime.Now,
                                StatusCodeId = 1,
                                //  IsRead = value.IsRead,

                            };
                            _context.TaskInviteUser.Add(taskInvite);
                            _context.SaveChanges();

                        }
                    });
                }
            }
            else
            {
                if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assignto"))
                {
                    if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                    {
                        value.InviteUsers.ForEach(i =>
                        {
                            var tasAssigned = new TaskAssigned
                            {
                                TaskId = value.TaskId,
                                DueDate = value.DueDate,
                                UserId = i,
                                IsRead = false,
                                AssignedDate = DateTime.Now,
                                StatusCodeId = 512,

                            };
                            _context.TaskAssigned.Add(tasAssigned);
                        });
                    }
                }
                if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assigncc"))
                {
                    if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                    {
                        value.InviteUsers.ForEach(i =>
                        {
                            var taskMembers = new TaskMembers
                            {
                                TaskId = value.TaskId,
                                DueDate = value.DueDate,
                                AssignedCc = i,
                                IsRead = false,
                                StatusCodeId = 512,

                            };
                            _context.TaskMembers.Add(taskMembers);


                        });
                    }
                }
                if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "onbehalf"))
                {
                    var taskmaster = _context.TaskMaster.Where(s => s.TaskId == value.TaskId).FirstOrDefault();
                    if (taskmaster != null)
                    {
                        if (value.OnBehalfId != null)
                        {
                            taskmaster.OnBehalf = value.OnBehalfId;
                        }
                    }
                }
                if (value.TaskCommentId != null)
                {
                    if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                    {
                        value.InviteUsers.ForEach(i =>
                        {
                            var taskcommentUser = new TaskCommentUser
                            {
                                TaskMasterId = value.TaskId.Value,
                                DueDate = value.DueDate,
                                UserId = i.Value,
                                IsRead = false,
                                IsAssignedTo = true,
                                TaskCommentId = value.TaskCommentId.Value,
                                //StatusCodeId = 512,
                            };
                            _context.TaskCommentUser.Add(taskcommentUser);


                        });
                    }

                }
            }
            _context.SaveChanges();
            return taskMasterModel;
        }
        private void AddNotification(List<long?> users, string notifi, int status)
        {
            users.ForEach(u =>
            {
                var notification = new Notification
                {
                    IsRead = false,
                    ModuleId = 550,
                    NotifiedUserId = u,
                    UpdatedDate = DateTime.Now,
                    Notification1 = notifi,
                };
                _context.Notification.Add(notification);
            });
            _context.SaveChanges();
        }
        private void SendEmailInviteMessage(TaskCommentModel value)
        {
            var TaskComment = _context.TaskComment.Where(w => w.TaskCommentId == value.ParentCommentId).FirstOrDefault();
            var tasMaster = _context.TaskMaster.Where(w => w.TaskId == value.TaskMasterID).FirstOrDefault();
            List<long> users = new List<long>();
            var taskAssignedUsers = _context.TaskInviteUser.Where(w => w.TaskId == value.TaskMasterID).Select(s => s.InviteUserId.GetValueOrDefault(0)).ToList();
            users.AddRange(taskAssignedUsers);
            users = users.Distinct().ToList();
            var sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", users.Distinct().ToList()) + ")";
            var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            var employee = result.Select(s => new
            {
                s.Status,
                s.EmployeeID,
                s.FirstName,
                s.LastName,
                s.NickName,
                s.Gender,
                s.DepartmentName,
                s.PlantID,
                UserId = s.UserID,
                s.Email
            }).Where(w => w.Email != null).ToList();
            MailServiceModel mailServiceModel = new MailServiceModel();
            var title = "Task - [" + value.TaskSubject + "/" + tasMaster?.Title + "] ";
            if (value.ParentCommentId == null)
            {

            }
            else
            {
                title = "Task - [" + TaskComment?.TaskSubject + "/" + tasMaster?.Title + "] ";
            }
            var message = "";
            message += "Please be informed that you have received a unread subject commented by " + value.AddedByUser + " in portal: <br><br>";
            message += "<table>";
            message += "<tr><td>Task name:</td><td>" + tasMaster?.Title + "</td></tr>";
            var url = value.Baseurl + "unReadSubject?id=" + tasMaster?.MainTaskId + "&taskid=" + tasMaster?.TaskId + "&taskCommentID=" + value.TaskCommentID + "&type=Dashboard&goBackframe=back";
            if (value.ParentCommentId == null)
            {
                message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + value.TaskSubject + "</a></td></tr>";
            }
            else
            {
                if (TaskComment != null)
                {
                    message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + TaskComment.TaskSubject + "</a></td></tr>";
                }
            }


            message += "<tr><td>Subject sent date & time: </td><td>" + DateTime.Now.ToString("dd/MMM/yyyy hh:mm tt") + "</td></tr>";
            if (tasMaster.IsNoDueDate == true)
            {
                message += "<tr><td>Subject due date:</td><td></td></tr>";
            }
            else
            {
                message += "<tr><td>Subject due date:</td><td>" + (value.DueDate != null ? value.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
            }
            message += "</table>";
            message += "<table>";
            message += "<tr><td>Message:</td><td>" + value.Comment;
            message += "</table>";
            var counts = "No";
            if (value.ParentCommentId != null)
            {
                var taskComments = _context.TaskComment.Include(a => a.CommentedByNavigation).Where(w => w.ParentCommentId == value.ParentCommentId && w.TaskCommentId != value.TaskCommentID).OrderByDescending(o => o.TaskCommentId).ToList();
                if (taskComments != null && taskComments.Count > 0)
                {
                    counts = "Yes";
                    message += "<hr>";
                    message += "<div style='font - weight: bold;'>History</div>";
                    message += "<hr>";
                    taskComments.ForEach(c =>
                    {
                        message += "<table>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Created By:</td>";
                        message += "<td style='font - weight: bold;'>" + c.CommentedByNavigation?.UserName + "</td>";
                        message += "</tr>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Created Date:</td>";
                        message += "<td style='font - weight: bold;'>" + (c.CommentedDate != null ? c.CommentedDate.Value.ToString("dd/MMM/yyyy") : "") + "</td>";
                        message += "</tr>";
                        message += "</table>";
                        message += "<table>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Message:</td>";
                        message += "<td>" + c.Comment + "</td>";
                        message += "</tr>";
                        message += "</table>";
                        message += "<hr>";
                    });
                }
            }
            var mainCont = _context.TaskComment.Include(a => a.CommentedByNavigation).Include(t => t.TaskMaster).Where(w => w.TaskCommentId == value.ParentCommentId && w.TaskCommentId != value.TaskCommentID).OrderByDescending(o => o.TaskCommentId).FirstOrDefault();
            if (mainCont != null)
            {
                if (counts == "No")
                {
                     message += "<hr>";
                     message += "<div style='font - weight: bold;'>History</div>";
                }
                message += "<hr>";
                message += "<table>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Created By:</td>";
                message += "<td style='font - weight: bold;'>" + mainCont?.CommentedByNavigation?.UserName + "</td>";
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Created Date:</td>";
                message += "<td style='font - weight: bold;'>" + (mainCont.CommentedDate != null ? mainCont.CommentedDate.Value.ToString("dd/MMM/yyyy") : "") + "</td>";
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Due Date:</td>";
                if (mainCont.TaskMaster.IsNoDueDate == true)
                {
                    message += "<td></td></tr>";
                }
                else
                {
                    message += "<td>" + (mainCont.TaskMaster.DueDate != null ? mainCont.TaskMaster.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
                }
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Subject:</td>";
                message += "<td style='font - weight: bold;'>" + mainCont.TaskSubject + "</td>";
                message += "</tr>";
                message += "</table>";
                message += "<table>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Message:</td>";
                message += "<td>" + mainCont.Comment + "</td>";
                message += "</tr>";
                message += "</table>";
                message += "<hr>";
            }
            mailServiceModel.FromName = value.AddedByUser;
            mailServiceModel.Subject = title;
            mailServiceModel.Body = message;
            List<MailHeaderModel> mailHeaderModels = new List<MailHeaderModel>();
            mailHeaderModels.AddRange(employee.Where(w => taskAssignedUsers.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
            { Email = s.Email, Name = s.FirstName }).ToList());
            mailServiceModel.ToMail = mailHeaderModels;
            mailServiceModel.Attachments = addAttachment(value);
            _mailService.SendEmailService(mailServiceModel);
        }
        private AttachmentsPathModel addAttachment(TaskCommentModel value)
        {
            AttachmentsPathModel attachmentsPathModel = new AttachmentsPathModel();
            List<string> pathList = new List<string>();
            var doc = _context.CommentAttachment.Where(s => s.TaskCommentId == value.TaskCommentID).ToList();
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "Attachment";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            if (doc != null)
            {
                var sessionId = Guid.NewGuid();
                string FromLocation = folderName + @"\" + newFolderName;
                serverPath = System.IO.Path.Combine(FromLocation, sessionId.ToString());
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                string newPath = folderName + @"\" + newFolderName + @"\" + sessionId.ToString();
                doc.ForEach(a =>
                {
                    var docs = _context.Documents.Select(s => new { s.FileData, s.DocumentId, s.FileName, s.IsCompressed, s.ContentType }).FirstOrDefault(w => w.DocumentId == a.DocumentId);
                    string FromLocations = newPath + @"\" + docs.FileName;
                    var compressedData = docs.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(docs.FileData) : docs.FileData;
                    System.IO.File.WriteAllBytes((string)FromLocations, compressedData);
                    pathList.Add(FromLocations);
                });
                attachmentsPathModel.PathName = newPath;
                attachmentsPathModel.Attachments = pathList;
            }
            return attachmentsPathModel;
        }
        private void SendEmailMessage(TaskCommentModel value)
        {
            if (value.MessageAssignedTos != null && value.MessageAssignedTos.Count > 0)
            {
                value.MessageAssignedTos.ForEach(a =>
                {
                    var tUser = _context.TaskAssigned.Where(w => w.TaskId == value.TaskMasterID && w.UserId == a).Count();
                    if(tUser>0)
                    {

                    }
                    else
                    {
                        var tasAssigned = new TaskAssigned
                        {
                            TaskId = value.TaskMasterID,
                            DueDate = value.DueDate,
                            UserId = a,
                            IsRead = false,
                            AssignedDate = DateTime.Now,
                            StatusCodeId = 512,

                        };
                        _context.TaskAssigned.Add(tasAssigned);
                    }
                });
                _context.SaveChanges();
            }
                var TaskComment = _context.TaskComment.Where(w => w.TaskCommentId == value.ParentCommentId).FirstOrDefault();
            var tasMaster = _context.TaskMaster.Where(w => w.TaskId == value.TaskMasterID).FirstOrDefault();
            List<long> users = new List<long>();
            var taskAssignedUsers = _context.TaskAssigned.Where(w => w.TaskId == value.TaskMasterID).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
            users.AddRange(taskAssignedUsers);
            var taskMembers = _context.TaskMembers.Where(w => w.TaskId == value.TaskMasterID).Select(s => s.AssignedCc.GetValueOrDefault(0)).ToList();
            users.AddRange(taskMembers);
            if (tasMaster.AddedByUserId != null)
            {
                users.Add(tasMaster.AddedByUserId.Value);
            }
            if (tasMaster.OnBehalf != null)
            {
                users.Add(tasMaster.OnBehalf.Value);
            }
            users = users.Distinct().ToList();
            var sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", users.Distinct().ToList()) + ")";
            var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            var employee = result.Select(s => new
            {
                s.Status,
                s.EmployeeID,
                s.FirstName,
                s.LastName,
                s.NickName,
                s.Gender,
                s.DepartmentName,
                s.PlantID,
                UserId = s.UserID,
                s.Email
            }).Where(w => w.Email != null).ToList();
            MailServiceModel mailServiceModel = new MailServiceModel();
            var title = "Task - [" + value.TaskSubject + "/" + tasMaster?.Title + "] ";
            if (value.ParentCommentId == null)
            {

            }
            else
            {
                title = "Task - [" + TaskComment?.TaskSubject + "/" + tasMaster?.Title + "] ";
            }
            var message = "";
            message += "Please be informed that you have received a unread subject commented by " + value.AddedByUser + " in portal: <br><br>";
            message += "<table>";
            message += "<tr><td>Task name:</td><td>" + tasMaster?.Title + "</td></tr>";
            var url = value.Baseurl + "unReadSubject?id=" + tasMaster?.MainTaskId + "&taskid=" + tasMaster?.TaskId + "&taskCommentID=" + value.TaskCommentID + "&type=Dashboard&goBackframe=back";
            if (value.ParentCommentId == null)
            {
                message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + value.TaskSubject + "</a></td></tr>";
            }
            else
            {
                if (TaskComment != null)
                {
                    message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + TaskComment.TaskSubject + "</a></td></tr>";
                }
            }


            message += "<tr><td>Subject sent date & time: </td><td>" + DateTime.Now.ToString("dd/MMM/yyyy hh:mm tt") + "</td></tr>";
            if (tasMaster.IsNoDueDate == true)
            {
                message += "<tr><td>Subject due date:</td><td></td></tr>";
            }
            else
            {
                message += "<tr><td>Subject due date:</td><td>" + (value.DueDate != null ? value.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
            }
            message += "</table>";
            message += "<table>";
            message += "<tr><td>Message:</td><td>" + value.Comment;
            message += "</table>";
            var counts = "No";
            if (value.ParentCommentId != null)
            {
                var taskComments = _context.TaskComment.Include(a => a.CommentedByNavigation).Where(w => w.ParentCommentId == value.ParentCommentId && w.TaskCommentId != value.TaskCommentID).OrderByDescending(o => o.TaskCommentId).ToList();
                if (taskComments != null && taskComments.Count > 0)
                {
                    counts = "Yes";
                    message += "<hr>";
                    message += "<div style='font - weight: bold;'>History</div>";
                    message += "<hr>";
                    taskComments.ForEach(c =>
                    {
                        message += "<table>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Created By:</td>";
                        message += "<td style='font - weight: bold;'>" + c.CommentedByNavigation?.UserName + "</td>";
                        message += "</tr>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Created Date:</td>";
                        message += "<td style='font - weight: bold;'>" + (c.CommentedDate != null ? c.CommentedDate.Value.ToString("dd/MMM/yyyy") : "") + "</td>";
                        message += "</tr>";
                        message += "</table>";
                        message += "<table>";
                        message += "<tr>";
                        message += "<td style='font - weight: bold;'>Message:</td>";
                        message += "<td>" + c.Comment + "</td>";
                        message += "</tr>";
                        message += "</table>";
                        message += "<hr>";
                    });
                }
            }
            var mainCont = _context.TaskComment.Include(a => a.CommentedByNavigation).Include(t => t.TaskMaster).Where(w => w.TaskCommentId == value.ParentCommentId && w.TaskCommentId != value.TaskCommentID).OrderByDescending(o => o.TaskCommentId).FirstOrDefault();
            if (mainCont != null)
            {
                if (counts == "No")
                {
                    message += "<hr>";
                    message += "<div style='font - weight: bold;'>History</div>";
                }
                message += "<hr>";
                message += "<table>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Created By:</td>";
                message += "<td style='font - weight: bold;'>" + mainCont?.CommentedByNavigation?.UserName + "</td>";
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Created Date:</td>";
                message += "<td style='font - weight: bold;'>" + (mainCont.CommentedDate != null ? mainCont.CommentedDate.Value.ToString("dd/MMM/yyyy") : "") + "</td>";
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Due Date:</td>";
                if (mainCont.TaskMaster.IsNoDueDate == true)
                {
                    message += "<td></td></tr>";
                }
                else
                {
                    message += "<td>" + (mainCont.TaskMaster.DueDate != null ? mainCont.TaskMaster.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
                }
                message += "</tr>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Subject:</td>";
                message += "<td style='font - weight: bold;'>" + mainCont.TaskSubject + "</td>";
                message += "</tr>";
                message += "</table>";
                message += "<table>";
                message += "<tr>";
                message += "<td style='font - weight: bold;'>Message:</td>";
                message += "<td>" + mainCont.Comment + "</td>";
                message += "</tr>";
                message += "</table>";
                message += "<hr>";
            }
            mailServiceModel.FromName = value.AddedByUser;
            mailServiceModel.Subject = title;
            mailServiceModel.Body = message;
            if (value.MessageAssignedTos != null && value.MessageAssignedTos.Count > 0)
            {
                List<MailHeaderModel> mailHeaderModels = new List<MailHeaderModel>();
                mailHeaderModels.AddRange(employee.Where(w => value.MessageAssignedTos.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                { Email = s.Email, Name = s.FirstName }).ToList());

                mailServiceModel.ToMail = mailHeaderModels;
                List<long> usersIds = new List<long>();
                List<MailHeaderModel> ccmailHeaderModels = new List<MailHeaderModel>();
                var taskMemberCC = taskMembers.Except(value.MessageAssignedTos).ToList();
                if (taskMemberCC != null && taskMemberCC.Count > 0)
                {
                    usersIds.AddRange(taskMemberCC);
                }
                var taskAssignedUserCC = taskAssignedUsers.Except(value.MessageAssignedTos).ToList();
                if (taskAssignedUserCC != null && taskAssignedUserCC.Count > 0)
                {
                    usersIds.AddRange(taskAssignedUserCC);
                }

                if (tasMaster.AddedByUserId != null)
                {
                    usersIds.Add(tasMaster.AddedByUserId.Value);
                }
                if (tasMaster.OnBehalf != null)
                {
                    usersIds.Add(tasMaster.OnBehalf.Value);
                }

                var usersId = usersIds.Except(value.MessageAssignedTos).ToList();
                if (usersId != null && usersId.Count > 0)
                {
                    ccmailHeaderModels.AddRange(employee.Where(w => usersId.Distinct().Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                    { Email = s.Email, Name = s.FirstName }).ToList());
                    mailServiceModel.CcMail = ccmailHeaderModels;
                }
            }
            else
            {
                List<MailHeaderModel> mailHeaderModels = new List<MailHeaderModel>();
                mailHeaderModels.AddRange(employee.Where(w => taskAssignedUsers.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                { Email = s.Email, Name = s.FirstName }).ToList());
                if (tasMaster.AddedByUserId != null)
                {
                    mailHeaderModels.AddRange(employee.Where(w => tasMaster.AddedByUserId == w.UserId).Select(s => new MailHeaderModel
                    { Email = s.Email, Name = s.FirstName }).ToList());
                }
                mailServiceModel.ToMail = mailHeaderModels;
                List<MailHeaderModel> ccmailHeaderModels = new List<MailHeaderModel>();
                ccmailHeaderModels.AddRange(employee.Where(w => taskMembers.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                { Email = s.Email, Name = s.FirstName }).ToList());
                if (tasMaster.OnBehalf != null && tasMaster.OnBehalf != tasMaster.AddedByUserId)
                {
                    ccmailHeaderModels.AddRange(employee.Where(w => tasMaster.OnBehalf == w.UserId).Select(s => new MailHeaderModel
                    { Email = s.Email, Name = s.FirstName }).ToList());
                }
                mailServiceModel.CcMail = ccmailHeaderModels;
            }
            mailServiceModel.Attachments = addAttachment(value);
            _mailService.SendEmailService(mailServiceModel);
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskComment")]
        public TaskCommentModel Put(TaskCommentModel value)
        {
            var project = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == value.TaskCommentID);
            project.Comment = value.Comment;
            project.EditedBy = value.CommentedBy;
            project.TaskSubject = value.TaskSubject;
            project.IsEdited = true;
            project.EditedDate = DateTime.Now;
            project.IsRead = false;
            // project.CommentedBy = value.CommentedBy;
            project.CommentedDate = DateTime.Now;
            _context.SaveChanges();
            CommentAttachmentModel commentAttachmentModel = new CommentAttachmentModel();
            var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FileName }).Where(d => d.SessionId == value.SessionID).ToList();
            value.CommentAttachmentModel = new CommentAttachmentModel();
            value.TaskCommentModels = new List<TaskCommentModel>();
            value.TaskMasterModel = new TaskMasterModel();
            var documentIds = _context.TaskAttachment.Where(t => t.IsLatestCommentAttachment == true).Select(t => t.DocumentId).ToList();
            if (documentIds.Count > 0)
            {
                var documentss = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FileName }).Where(d => documentIds.Contains(d.DocumentId)).ToList();
                value.CommentAttachmentModel = new CommentAttachmentModel();
                value.TaskCommentModels = new List<TaskCommentModel>();
                value.TaskMasterModel = new TaskMasterModel();

                if (documentss.Any())
                {
                    var commentAttachement = new CommentAttachment();
                    documentss.ForEach(d =>
                    {
                        commentAttachement = new CommentAttachment
                        {
                            TaskCommentId = value.TaskCommentID,
                            DocumentId = d.DocumentId,
                            UserId = value.CommentedBy.Value,
                            FileName = d.FileName
                        };
                        _context.CommentAttachment.Add(commentAttachement);
                        var latesttaskAttachment = _context.TaskAttachment.Where(t => t.IsLatestCommentAttachment == true && documentIds.Contains(t.DocumentId)).ToList();
                        if (latesttaskAttachment.Count > 0)
                        {
                            latesttaskAttachment.ForEach(f =>
                            {
                                f.IsLatestCommentAttachment = false;
                                f.TaskCommentId = value.TaskCommentID;
                                f.IsComment = true;
                                _context.SaveChanges();

                            });
                        }

                    });
                }
            }
            if (documents.Any())
            {
                var commentAttachement = new CommentAttachment();
                documents.ForEach(d =>
                {
                    commentAttachement = new CommentAttachment
                    {
                        TaskCommentId = value.TaskCommentID,
                        DocumentId = d.DocumentId,
                        UserId = value.CommentedBy.Value,
                        FileName = d.FileName
                    };
                    _context.CommentAttachment.Add(commentAttachement);

                    var taskAttachment = new TaskAttachment
                    {
                        DocumentId = d.DocumentId,
                        IsDiscussionNotes = false,
                        IsLatest = true,
                        IsLocked = false,
                        IsMajorChange = false,
                        IsMeetingNotes = false,
                        IsComment = true,
                        TaskCommentId = value.TaskCommentID,
                        TaskMasterId = value.TaskMasterID,
                        UploadedByUserId = value.CommentedBy,
                        UploadedDate = DateTime.Now,
                        VersionNo = "1.0",

                    };
                    _context.TaskAttachment.Add(taskAttachment);
                    _context.SaveChanges();
                });
                value.CommentAttachmentModel = new CommentAttachmentModel
                {
                    TaskCommentId = commentAttachement.TaskCommentId,
                    DocumentId = commentAttachement.DocumentId,
                    UserId = commentAttachement.UserId,
                    FileName = commentAttachement.FileName,
                    FileNames = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.FileName).ToList(),
                    DocumentIdList = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.DocumentId).ToList(),
                    FileDocumentList = _context.CommentAttachment.Select(t => new DocumentsModel
                    {
                        ParentTaskID = t.TaskCommentId,
                        DocumentID = t.DocumentId,
                        FileName = t.Document.FileName,
                        ContentType = t.Document.ContentType,
                    }).Where(c => c.ParentTaskID == value.TaskCommentID).ToList(),
                    ContentTypes = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.Document.ContentType).ToList(),

                };

            }
            else
            {
                var commentAttachement = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).FirstOrDefault();
                if (commentAttachement != null)
                {
                    value.CommentAttachmentModel = new CommentAttachmentModel
                    {

                        TaskCommentId = value.TaskCommentID,
                        DocumentId = commentAttachement.DocumentId,
                        UserId = commentAttachement.UserId,
                        FileName = commentAttachement.FileName,
                        DocumentIdList = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.DocumentId).ToList(),
                        FileNames = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.FileName).ToList(),
                        FileDocumentList = _context.CommentAttachment.Select(t => new DocumentsModel
                        {
                            ParentTaskID = t.TaskCommentId,
                            DocumentID = t.DocumentId,
                            FileName = t.Document.FileName,
                            ContentType = t.Document.ContentType,
                        }).Where(c => c.ParentTaskID == value.TaskCommentID).ToList(),
                        ContentTypes = _context.CommentAttachment.Where(c => c.TaskCommentId == value.TaskCommentID).Select(c => c.Document.ContentType).ToList(),

                    };
                }

            }
            if (value.MessageAssignedTos != null && value.MessageAssignedTos.Count != 0)
            {
                var taskcommentupdate = _context.TaskCommentUser.Where(t => t.TaskCommentId == value.TaskCommentID && value.MessageAssignedTos.Contains(t.UserId)).ToList();
                if (taskcommentupdate.Count > 0)
                {
                    taskcommentupdate.ForEach(ta =>
                    {
                        var taskcommentuser = taskcommentupdate.Where(t => t.TaskCommentUserId == ta.TaskCommentUserId).FirstOrDefault();
                        taskcommentuser.IsAssignedTo = true;
                        taskcommentuser.DueDate = value.DueDate;
                        taskcommentuser.StatusCodeId = value.StatusCodeID;

                    });
                }
                _context.SaveChanges();

                value.MessageAssignedTos.ForEach(u =>
                {
                    var taskcommentExituser = _context.TaskCommentUser.Where(t => t.TaskMasterId == value.TaskMasterID && t.TaskCommentId == value.TaskCommentID && t.UserId == u).FirstOrDefault();
                    if (taskcommentExituser == null)
                    {
                        var userComment = new TaskCommentUser
                        {
                            IsRead = false,
                            IsClosed = false,
                            TaskCommentId = value.TaskCommentID,
                            TaskMasterId = value.TaskMasterID.Value,
                            UserId = u,
                            StatusCodeId = value.StatusCodeID,
                        };
                        _context.TaskCommentUser.Add(userComment);
                    }
                    _context.SaveChanges();
                });
            }
            value.AssignedToUsers = _context.TaskCommentUser.Where(tc => tc.TaskMasterId == value.TaskMasterID && tc.TaskCommentId == value.TaskCommentID && tc.IsAssignedTo == true).Select(tc => tc.User.UserName).Distinct().ToList();
            _context.SaveChanges();
            value.LastCommentedDate = value.CommentedDate;
            value.LastCommentedBy = value.AddedByUser;
            value.SubjectStatus = _context.TaskCommentUser.Any(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID && c.TaskCommentId == value.TaskCommentID && c.StatusCodeId == 516) == false ? "Open" : "Closed";
            if (value.InviteUserData != null)
            {
                value.InviteUserData.TaskCommentId = value.TaskCommentID;
                InsertTaskInviteUser(value.InviteUserData);
                if (value.InviteUserData.AssignTask.ToLower().Trim() == "inviteuser")
                {
                    SendEmailInviteMessage(value);
                }
                else
                {
                    SendEmailMessage(value);
                }
            }
            else
            {
                SendEmailMessage(value);
            }
            return value;
        }

        [HttpPut]
        [Route("UpdateTaskCommentUser")]
        public TaskCommentModel PutComment(TaskCommentModel value)
        {
            var taskcommentuser = _context.TaskCommentUser.Where(t => t.TaskCommentId == value.TaskCommentID && t.UserId == value.AddedByUserID).FirstOrDefault();
            taskcommentuser.IsPin = value.IsPin;
            taskcommentuser.PinnedBy = value.AddedByUserID;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateTaskLink")]
        public TaskCommentModel UpdateTaskLink(TaskCommentModel value)
        {
            var project = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == value.TaskCommentID);
            project.TaskId = value.TaskMasterID;
            project.MainTaskId = value.MainTaskId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateRead")]
        public TaskCommentModel UpdateRead(TaskCommentModel value)
        {
            //var project = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == value.TaskCommentID);           
            //project.IsEdited = true;

            var userComment = _context.TaskCommentUser.Where(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID).ToList();
            userComment.ForEach(u =>
            {
                u.IsRead = true;

            });
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateClosed")]
        public TaskCommentModel UpdateClosed(TaskCommentModel value)
        {
            //var project = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == value.TaskCommentID);           
            //project.IsEdited = true;
            var comment = _context.TaskComment.Where(t => t.TaskCommentId == value.TaskCommentID)?.FirstOrDefault();
            if (comment != null)
            {
                var unreadCommentids = _context.TaskComment.Where(s => s.ParentCommentId == value.TaskCommentID).Select(s => s.TaskCommentId).ToList();
                var userComments = _context.TaskCommentUser.Where(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID && unreadCommentids.Contains(c.TaskCommentId)).ToList();
                if (userComments != null && userComments.Count > 0)
                {
                    userComments.ForEach(f =>
                    {
                        f.IsClosed = true;
                        _context.SaveChanges();
                    });
                    var maincomment = _context.TaskCommentUser.Where(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID && c.TaskCommentId == value.TaskCommentID).FirstOrDefault();
                    if (maincomment != null)
                    {
                        maincomment.IsClosed = true;
                        _context.SaveChanges();
                    }
                }
                else
                {
                    var userComment = _context.TaskCommentUser.Where(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID && c.TaskCommentId == value.TaskCommentID).FirstOrDefault();

                    if (userComment != null)
                    {
                        userComment.IsClosed = true;
                    }
                }
            }
            else
            {
                var userComment = _context.TaskCommentUser.Where(c => c.UserId == value.AddedByUserID && c.TaskMasterId == value.TaskMasterID && c.TaskCommentId == value.TaskCommentID).FirstOrDefault();
                if (userComment != null)
                {
                    userComment.IsClosed = true;
                }
                // _context.SaveChanges();
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateDocRead")]
        public TaskCommentModel UpdateDocRead(TaskCommentModel value)
        {
            var project = _context.TaskComment.Where(p => p.TaskMasterId == value.TaskMasterID && p.DocumentId == value.DocumentId).Select(s => s.TaskCommentId).ToList();
            //project.IsEdited = true;

            var userComment = _context.TaskCommentUser.Where(c => project.Contains(c.TaskCommentId) && c.UserId == value.AddedByUserID).ToList();
            userComment.ForEach(u =>
            {
                u.IsRead = true;
            });
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskComment")]
        public void Delete(int id)
        {
            var message = "";
            try
            {
                var taskComment = _context.TaskComment.SingleOrDefault(p => p.TaskCommentId == id);
                if (taskComment != null && taskComment.TaskId == null)
                {
                    var commentUser = _context.TaskCommentUser.Where(c => c.TaskCommentId == id).ToList();
                    var commentAttachment = _context.CommentAttachment.Where(c => c.TaskCommentId == id).ToList();
                    _context.TaskCommentUser.RemoveRange(commentUser);
                    _context.CommentAttachment.RemoveRange(commentAttachment);
                    _context.TaskComment.Remove(taskComment);
                    _context.SaveChanges();

                    var comments = _context.TaskComment.Where(c => c.DocumentId == taskComment.DocumentId).Count();
                    if (comments == 0)
                    {
                        var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == taskComment.DocumentId && t.TaskMasterId == taskComment.TaskMasterId);
                        if (taskattach != null)
                        {
                            taskattach.IsDiscussionNotes = false;
                        }
                        _context.SaveChanges();
                    }

                }
                else
                {
                    message = "This conversation has been converted as task.Delete operation can not be completed.!";
                    throw new AppException(message, null);
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(message))
                {
                    message = "There are replies for this conversation.Delete operation can not be completed.!";
                }
                throw new AppException(message, ex);

            }
        }
        [HttpPut]
        [Route("ReOpenClosedComments")]
        public void ReOpenClosedComments(CommentParam commentParam)
        {
            var userComments = new List<TaskCommentUser>();
            if (commentParam.UserId > 0 && commentParam.TaskCommentId > 0)
            {
                userComments = _context.TaskCommentUser.Where(c => c.TaskMasterId == commentParam.TaskId && c.UserId == commentParam.UserId && c.TaskCommentId == commentParam.TaskCommentId).ToList();
                var taskComment = _context.TaskComment.FirstOrDefault(c => c.TaskCommentId == commentParam.TaskCommentId);
                if (taskComment.ParentCommentId != null)
                {
                    //var taskComments = _context.TaskCommentUser.Where(c => c.TaskCommentId == taskComment.ParentCommentId).ToList();
                    //taskComments.ForEach(t =>
                    //{
                    //    if (!userComments.Any(u => u.TaskCommentUserId == t.TaskCommentUserId))
                    //    {
                    //        userComments.Add(t);
                    //    }
                    //});
                }
                else
                {
                    var taskComments = _context.TaskComment.Where(c => c.ParentCommentId == taskComment.TaskCommentId).ToList();
                    var taskCommentIDs = taskComments.Select(s => s.TaskCommentId).ToList();
                    taskCommentIDs.Add(taskComment.TaskCommentId);
                    var taskCommentUsers = _context.TaskCommentUser.Where(c => taskCommentIDs.Contains(c.TaskCommentId)).ToList();
                    taskCommentUsers.ForEach(t =>
                    {
                        if (!userComments.Any(u => u.TaskCommentUserId == t.TaskCommentUserId))
                        {
                            userComments.Add(t);
                        }
                    });
                }
                userComments.ForEach(u =>
                {
                    u.StatusCodeId = 512;
                });

                _context.SaveChanges();

            }
        }

        [HttpPut]
        [Route("CloseComments")]
        public void CloseComments(CommentParam commentParam)
        {
            var userComments = new List<TaskCommentUser>();
            if (commentParam.UserId > 0 && commentParam.TaskCommentId > 0)
            {
                userComments = _context.TaskCommentUser.Where(c => c.TaskMasterId == commentParam.TaskId && c.UserId == commentParam.UserId && c.TaskCommentId == commentParam.TaskCommentId).ToList();
                var taskComment = _context.TaskComment.FirstOrDefault(c => c.TaskCommentId == commentParam.TaskCommentId);
                if (taskComment.ParentCommentId != null)
                {
                    //var taskComments = _context.TaskCommentUser.Where(c => c.TaskCommentId == taskComment.ParentCommentId).ToList();
                    //taskComments.ForEach(t =>
                    //{
                    //    if (!userComments.Any(u => u.TaskCommentUserId == t.TaskCommentUserId))
                    //    {
                    //        userComments.Add(t);
                    //    }
                    //});
                }
                else
                {
                    var taskComments = _context.TaskComment.Where(c => c.ParentCommentId == taskComment.TaskCommentId).ToList();
                    var taskCommentIDs = taskComments.Select(s => s.TaskCommentId).ToList();
                    taskCommentIDs.Add(taskComment.TaskCommentId);
                    var taskCommentUsers = _context.TaskCommentUser.Where(c => taskCommentIDs.Contains(c.TaskCommentId)).ToList();
                    taskCommentUsers.ForEach(t =>
                    {
                        if (!userComments.Any(u => u.TaskCommentUserId == t.TaskCommentUserId))
                        {
                            userComments.Add(t);
                        }
                    });
                }
            }
            else
            {
                userComments = _context.TaskCommentUser.Where(c => c.TaskMasterId == commentParam.TaskId).ToList();
            }
            userComments.ForEach(u =>
            {
                u.StatusCodeId = 516;
            });

            _context.SaveChanges();

            var allclose = _context.TaskCommentUser.Where(w => w.TaskMasterId == commentParam.TaskId).All(s => s.StatusCodeId == 516);
            if (allclose)
            {
                var taskmas = _context.TaskMaster.FirstOrDefault(t => t.TaskId == commentParam.TaskId);
                if (taskmas != null && taskmas.StatusCodeId != 516)
                {
                    taskmas.StatusCodeId = 516;

                    var taskAssigned = _context.TaskAssigned.Where(a => a.TaskId == taskmas.TaskId).ToList();
                    taskAssigned.ForEach(a =>
                    {
                        a.StatusCodeId = 516;
                    });

                    var taskFiles = _context.TaskAttachment.Count(c => c.TaskMasterId == commentParam.TaskId);
                    if (taskFiles > 0)
                    {
                        var title = "Automated task for DMS files arrangement  " + taskmas.Title;
                        var taskobjexist = _context.TaskMaster.FirstOrDefault(w => w.Title == title);
                        if (taskobjexist == null)
                        {
                            var task = new TaskMaster()
                            {
                                Title = title,
                                Description = "Automated task created to arrange DMS files",
                                ParentTaskId = commentParam.TaskId,
                                MainTaskId = taskmas.MainTaskId,
                                AssignedTo = taskmas.AssignedTo,
                                DueDate = DateTime.Now.AddDays(7),
                                OwnerId = taskmas.AddedByUserId,
                                AddedByUserId = taskmas.AddedByUserId,
                                AddedDate = DateTime.Now,
                                StatusCodeId = 512,
                            };
                            if (commentParam.TaskId > 0 && taskmas.MainTaskId != null)
                            {
                                var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == commentParam.TaskId);
                                var taskNumber = "1";
                                if (tasklevel == null)
                                {
                                    tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == commentParam.TaskId);
                                    taskNumber = tasklevel.TaskNumber + ".1";
                                }
                                else
                                {
                                    var numberarray = tasklevel.TaskNumber.Split('.');
                                    var number = numberarray[numberarray.Length - 1].ToString();
                                    var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                                    var taskNo = tasklevel.TaskNumber.Remove(leng);
                                    number = (double.Parse(number) + 1).ToString();
                                    taskNumber = taskNo + "." + number;
                                }
                                task.TaskLevel = tasklevel.TaskLevel + 1;
                                task.TaskNumber = taskNumber;
                            }

                            var assigned = new TaskAssigned
                            {
                                UserId = taskmas.AddedByUserId,
                                StatusCodeId = 512,
                                DueDate = DateTime.Now.AddDays(7),
                                AssignedDate = DateTime.Now,
                                TaskOwnerId = taskmas.AddedByUserId,
                                IsRead = false,
                                StartDate = DateTime.Now,
                            };
                            task.TaskAssigned.Add(assigned);
                            _context.TaskMaster.Add(task);
                        }
                    }
                    _context.SaveChanges();
                }

            }
        }

        [HttpPost]
        [Route("GetAssignedAndCCUsers")]
        public RepliedUserParam GetAssignedAndCCUsers(CommentParam param)
        {
            RepliedUserParam repliedUsers = new RepliedUserParam();
            repliedUsers.Assigned = new List<RepliedUserModel>();
            repliedUsers.AssignedCC = new List<RepliedUserModel>();
            repliedUsers.Owners = new List<RepliedUserModel>();
            List<long> userIds = new List<long>();
            if (param.AssignedUserIds != null && param.AssignedUserIds.Count > 0)
            {
                userIds.AddRange(param.AssignedUserIds);
            }
            if (param.AssignedCCUserIds != null && param.AssignedCCUserIds.Count > 0)
            {
                userIds.AddRange(param.AssignedCCUserIds);
            }
            if (param.OwnerUserIds != null && param.OwnerUserIds.Count > 0)
            {
                userIds.AddRange(param.OwnerUserIds);
            }
            //var commentUsers = _context.TaskCommentUser.Where(t => t.TaskMasterId == param.TaskId).ToList();
            if (userIds != null && userIds.Count > 0)
            {
                var allUsers = _context.ApplicationUser.Where(u => userIds.Contains(u.UserId)).ToList();
                //List<RepliedUserModel> assignedUsers = new List<RepliedUserModel>();
                var readStatus = false;
                if (param.AssignedUserIds != null && param.AssignedUserIds.Count > 0)
                {
                    foreach (var id in param.AssignedUserIds.Distinct())
                    {
                        readStatus = false;
                        if (param.UserId > 0)
                        {
                            if (param.UserId != id)
                            {
                                var commentedby = _context.TaskComment.Include("TaskCommentUser").Where(c => c.CommentedBy == id && c.TaskMasterId == param.TaskId).ToList();

                                if (commentedby != null && commentedby.Count > 0)
                                {
                                    commentedby.ForEach(mess =>
                                    {
                                        if (!readStatus)
                                            readStatus = mess.TaskCommentUser.Where(u => u.UserId == param.UserId).Any(a => a.IsRead == false);
                                    });

                                }
                            }
                            //var assignedUsers = commentUsers.Where(c => c.UserId == id).ToList();
                        }
                        repliedUsers.Assigned.Add(new RepliedUserModel
                        {
                            TaskId = param.TaskId,
                            UserId = id,
                            IsRead = readStatus, // assignedUsers.Any(u => u.IsRead == false),
                            UserName = allUsers.Where(u => u.UserId == id).FirstOrDefault()?.UserName,
                        });

                    }
                }
                if (param.AssignedCCUserIds != null && param.AssignedCCUserIds.Count > 0)
                {
                    foreach (var id in param.AssignedCCUserIds.Distinct())
                    {
                        readStatus = false;
                        if (param.UserId > 0)
                        {
                            if (param.UserId != id)
                            {
                                var commentedby = _context.TaskComment.Include("TaskCommentUser").Where(c => c.CommentedBy == id && c.TaskMasterId == param.TaskId).ToList();
                                if (commentedby != null && commentedby.Count > 0)
                                {
                                    commentedby.ForEach(mess =>
                                    {
                                        if (!readStatus)
                                            readStatus = mess.TaskCommentUser.Where(u => u.UserId == param.UserId).Any(a => a.IsRead == false);
                                    });

                                }
                            }
                        }
                        repliedUsers.AssignedCC.Add(new RepliedUserModel
                        {
                            TaskId = param.TaskId,
                            UserId = id,
                            IsRead = readStatus, //assignedCCUsers.Any(u => u.IsRead == false),
                            UserName = allUsers.Where(u => u.UserId == id).FirstOrDefault()?.UserName,
                        });


                    }
                }
                if (param.OwnerUserIds != null && param.OwnerUserIds.Count > 0)
                {
                    foreach (var id in param.OwnerUserIds.Distinct())
                    {
                        readStatus = false;
                        if (param.UserId > 0)
                        {
                            if (param.UserId != id)
                            {
                                var commentedby = _context.TaskComment.Include("TaskCommentUser").Where(c => c.CommentedBy == id && c.TaskMasterId == param.TaskId).ToList();
                                if (commentedby != null && commentedby.Count > 0)
                                {
                                    commentedby.ForEach(mess =>
                                    {
                                        if (!readStatus && param.UserId > 0)
                                            readStatus = mess.TaskCommentUser.Where(u => u.UserId == param.UserId).Any(a => a.IsRead == false);
                                    });

                                }
                            }
                        }

                        repliedUsers.Owners.Add(new RepliedUserModel
                        {
                            TaskId = param.TaskId,
                            UserId = id,
                            IsRead = readStatus, //assignedCCUsers.Any(u => u.IsRead == false),
                            UserName = allUsers.Where(u => u.UserId == id).FirstOrDefault()?.UserName,
                        });


                    }
                }
            }
            return repliedUsers;
        }

        [HttpDelete]
        [Route("DeleteCommentFile")]
        public void DeleteCommentFile(int id)
        {
            var message = "";
            try
            {
                var taskComment = _context.TaskComment.Include(i => i.CommentAttachment).SingleOrDefault(p => p.TaskCommentId == id);
                if (taskComment != null && taskComment.CommentAttachment.Count > 0)
                {
                    _context.RemoveRange(taskComment.CommentAttachment);
                    var docIds = new List<long>();
                    taskComment.CommentAttachment.ToList().ForEach(d =>
                    {
                        docIds.Add(d.DocumentId);
                    });

                    var taskAttachments = _context.TaskAttachment.Where(t => docIds.Contains(t.DocumentId.Value)).ToList();
                    _context.RemoveRange(taskAttachments);

                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(message))
                {
                    message = "There are some transaction records.Delete operation can not be completed.!";
                }
                throw new AppException(message, ex);

            }
        }


        [HttpPut]
        [Route("UpdateCommentAttachement")]
        public void UpdateCommentAttachement(CommentAttachmentModel commentAttachment)
        {
            var message = "";
            try
            {
                var taskComment = _context.CommentAttachment.Where(p => p.TaskCommentId == commentAttachment.TaskCommentId && p.DocumentId == commentAttachment.DocumentId).FirstOrDefault();
                if (taskComment != null)
                {
                    var taskId = _context.TaskComment.FirstOrDefault(f => f.TaskCommentId == commentAttachment.TaskCommentId)?.TaskMasterId;
                    _context.CommentAttachment.Remove(taskComment);
                    _context.SaveChanges();
                    var taskAttchment = _context.TaskAttachment.FirstOrDefault(f => f.DocumentId == commentAttachment.DocumentId && f.TaskMasterId == taskId);
                    if (taskAttchment != null)
                    {
                        _context.TaskAttachment.Remove(taskAttchment);
                        _context.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(message))
                {
                    message = "There are some transaction records.Delete operation can not be completed.!";
                }
                throw new AppException(message, ex);

            }
        }
        [HttpPut]
        [Route("DeleteTaskAttachment")]
        public TaskAttachmentModel DeleteTaskAttachment(TaskAttachmentModel value)
        {
            var taskAttachment = _context.TaskAttachment.Where(p => p.TaskMasterId == value.TaskMasterID && p.IsComment == true && p.IsLatestCommentAttachment == true).ToList();
            if (taskAttachment != null)
            {
                _context.TaskAttachment.RemoveRange(taskAttachment);
                _context.SaveChanges();
            }

            return value;
        }
        [HttpPost]
        [Route("GetTaskCommentDashboardExcel")]
        public List<TaskCommentModel> GetTaskCommentDashboardExcel(SearchModel searchModel)
        {
            var id = searchModel.UserID;
            var taskIds = new List<long>();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetUnReadTaskComment";

            var result = _context.Set<ViewGetUnReadTaskComment>().FromSqlRaw(sqlQuery).AsQueryable();
            var taskCommentUser = _context.TaskCommentUser.Select(s => new
            {
                s.TaskCommentId,
                s.TaskCommentUserId,
                s.TaskMasterId,
                s.UserId,
                s.IsRead,
                s.IsPin,
                s.IsClosed,
                s.IsAssignedTo,
                s.PinnedBy,
                s.DueDate,

            }).Where(d => d.IsClosed == false || d.IsClosed == null).AsNoTracking().ToList();
            var query = taskCommentUser?.Where(t => t.UserId == id).ToList();
            var alltaskcommentUser = taskCommentUser?.Where(t => t.UserId != id).ToList();
            var taskCommentIds = new List<long>();
            //var TaskCommentUnreadIds = _context.TaskComment.Where(w => taskMasterIds.Contains(w.TaskMasterId.GetValueOrDefault(0))).Select(s => s.TaskCommentId).ToList();
            // taskCommentIds.AddRange(TaskCommentUnreadIds);
            var parentCommentIds = new List<long?>();
            List<long> filtertaskcommentIds = new List<long>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = taskCommentUser?.Select(s => s.TaskCommentId).Distinct().ToList();
            var dashItems = new List<TaskCommentModel>();

            taskCommentIds.AddRange(query.Where(t => (t.UserId == id && t.IsClosed != true) || (t.IsPin == true && t.PinnedBy == id)).Select(t => t.TaskCommentId).ToList());

            var taskComment = result.
                   Select(s => new
                   {
                       s.TaskCommentId,
                       s.TaskMasterId,
                       s.CommentedBy,
                       s.CommentedByName,
                       s.ParentCommentId,
                       s.Comment,
                       s.IsNoDueDate,
                       s.MainTaskId,
                       s.TaskSubject,
                       s.CommentedDate,
                       s.TaskDueDate,
                       s.TaskName,
                       s.SubjectDueDate
                   }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && (t.CommentedBy != id));
            //var taskComments = taskComment.ToList();
            var taskCommentId = taskComment.Select(s => s.TaskCommentId).ToList();
            var CommentedByIds = taskComment.Select(s => s.CommentedBy).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => CommentedByIds.Contains(w.UserId)).AsNoTracking().ToList();

            var ParentCommentIds = taskComment.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
            var ParentComment = _context.TaskComment.Select(s => new
            {
                s.TaskCommentId,
                s.TaskSubject,
            }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
            List<long> taskId = new List<long>();
            taskId = _context.TaskAssigned.Where(t => (t.TaskOwnerId == searchModel.UserID && t.IsRead == false)).Select(s => s.TaskId.Value).Distinct().ToList();
            taskIds.AddRange(taskId);
            taskId = new List<long>();
            taskId = _context.TaskMembers.Where(t => t.AssignedCc == searchModel.UserID && t.IsRead == false).Select(s => s.TaskId.Value).ToList();
            taskIds.AddRange(taskId);
            taskIds = new List<long>();
            taskId = _context.TaskMaster.Where(t => (t.OnBehalf == searchModel.UserID || t.OwnerId == searchModel.UserID || (taskIds.Contains(t.TaskId))) && (t.IsEmail == true)).Select(s => s.TaskId).ToList();
            taskIds.AddRange(taskId);

            var taskMasterId = taskComment.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
            var taskmaster = _context.TaskMaster.Select(s => new
            {
                s.TaskId,
                s.Title,
                s.DueDate,
                s.IsNoDueDate,
            }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();
            //List<long> taskIds = taskComment.Select(t => t.TaskMasterId.Value).Distinct().ToList();
            bool isAssigned = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.UserId == id);
            bool isOnBehalf = false;
            bool isAssignedCC = false;
            if (!isAssigned)
            {
                isOnBehalf = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.OnBehalfId == id);
            }
            if (!isOnBehalf)
            {
                isAssignedCC = _context.TaskMembers.Any(t => taskIds.Contains(t.TaskId.Value) && t.AssignedCc == id);
            }
            // List<TaskComment> taskCommentItems = taskComments.ToList();
            var selectTaskCommentIds = taskComment.Where(s => s.ParentCommentId != null).Select(s => s.TaskCommentId).Distinct().ToList();
            var selcommentIds = taskComment.GroupBy(d => new { d.ParentCommentId, d.TaskCommentId }).Select(s => new TaskCommentModel
            {
                TaskCommentID = s.Key.TaskCommentId,
                ParentCommentId = s.Key.ParentCommentId,

            });
            if (taskCommentIds.Any())
            {


                //var subCommentItems = new List<TaskComment>();
                //var mainComments = new List<TaskComment>();

                var mainComments = taskComment?.Where(d => d.ParentCommentId == null);
                filtertaskcommentIds = mainComments.Select(s => s.TaskCommentId).Distinct().ToList();
                var subCommentItems = taskComment?.Where(d => d.ParentCommentId != null);
                subCommentItems.ToList().ForEach(d =>
                {
                    if (d.ParentCommentId != null)
                    {
                        var subcomment = subCommentItems?.Where(s => s.ParentCommentId == d.ParentCommentId).Select(s => s.TaskCommentId).ToList();
                        if (subcomment != null && subcomment.Count > 0)
                        {
                            if (subcomment.Count > 1)
                            {
                                var selsubcomment = subcomment.LastOrDefault();
                                if (!mainComments.ToList().Exists(m => m.ParentCommentId == d.ParentCommentId))
                                {
                                    var existingmain = mainComments.Where(m => m.TaskCommentId == d.ParentCommentId).FirstOrDefault();
                                    if (existingmain == null)
                                    {
                                        //mainComments.ToList().Add(selsubcomment);
                                        filtertaskcommentIds.Add(selsubcomment);
                                    }
                                }

                            }
                            else
                            {
                                if (!mainComments.ToList().Exists(m => m.ParentCommentId == d.ParentCommentId))
                                {
                                    var existingmain = mainComments.Where(m => m.TaskCommentId == d.ParentCommentId).FirstOrDefault();
                                    if (existingmain == null)
                                    {
                                        //mainComments.ToList().AddRange(subcomment);
                                        filtertaskcommentIds.AddRange(subcomment);
                                    }
                                }
                            }
                        }
                    }

                });
            }
            var selecttaskComment = taskComment.
                   Select(s => new
                   {
                       s.TaskCommentId,
                       s.TaskMasterId,
                       s.CommentedBy,
                       s.CommentedByName,
                       s.ParentCommentId,
                       s.Comment,
                       s.IsNoDueDate,
                       s.MainTaskId,
                       s.TaskSubject,
                       s.CommentedDate,
                       AddedDate = s.CommentedDate,
                       s.TaskDueDate,
                       s.TaskName,
                       s.SubjectDueDate
                   }).Where(t => filtertaskcommentIds.Contains(t.TaskCommentId)).ToList();

            if (selecttaskComment != null)
            {
                selecttaskComment.ToList().ForEach(s =>
                {
                    var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                    TaskCommentModel taskCommentModel = new TaskCommentModel();

                    taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                    taskCommentModel.TaskMasterID = s.TaskMasterId;
                    taskCommentModel.TaskCommentID = s.TaskCommentId;
                    // taskCommentModel.Comment = s.Comment;
                    taskCommentModel.MainTaskId = s.MainTaskId;
                    taskCommentModel.TaskName = s.TaskName;
                    taskCommentModel.TaskDueDate = s.TaskDueDate;
                    taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                    // taskCommentModel.SubjectDueDate = s.ParentCommentId != null ?taskCommentUser !=null? taskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault(): null : taskCommentUser!=null? taskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault(): null;
                    //taskCommentModel.SubjectDueDate = taskCommentUser!=null? taskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() :null;
                    taskCommentModel.CommentedByName = s.CommentedByName;
                    taskCommentModel.CommentedBy = s.CommentedBy;
                    taskCommentModel.ParentCommentId = s.ParentCommentId;
                    taskCommentModel.LastCommentedDate = s.CommentedDate;
                    //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                    taskCommentModel.IsPin = taskCommentUser?.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();
                    taskCommentModel.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                    dashItems.Add(taskCommentModel);

                });
            }

            dashItems.ForEach(d =>
            {

                if (d.ParentCommentId != null)
                {
                    if (d.SubjectDueDate == null)
                    {
                        d.SubjectDueDate = taskCommentUser != null ? taskCommentUser.Where(t => t.TaskCommentId == d.ParentCommentId && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : null;
                    }


                }
                else
                {
                    d.SubjectDueDate = taskCommentUser != null ? taskCommentUser.Where(t => t.TaskCommentId == d.TaskCommentID && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : null;

                    if (d.SubjectDueDate == null)
                    {
                        d.SubjectDueDate = d.IsNoDueDate == null || (d.IsNoDueDate != null && d.IsNoDueDate == false) ? d.TaskDueDate : null;
                    }
                    else if (d.IsNoDueDate == true)
                    {
                        d.SubjectDueDate = null;
                        d.TaskDueDate = null;
                    }
                }
            });
            return dashItems;
        }

    }
}