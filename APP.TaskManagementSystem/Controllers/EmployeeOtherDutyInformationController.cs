﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class EmployeeOtherDutyInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public EmployeeOtherDutyInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        [HttpGet]
        [Route("GetEmployeeOtherDutyInformationByID")]
        public List<EmployeeOtherDutyInformationModel> GetEmployeeOtherDutyInformationByID(int id)
        {
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.Include(a => a.AddedByUser).
                Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(d => d.Designation).Include(ss => ss.SubSectionT).AsNoTracking().ToList();

            List<EmployeeOtherDutyInformationModel> employeeOtherDutyInformationModel = new List<EmployeeOtherDutyInformationModel>();
            employeeOtherDutyInformation.ForEach(s =>
            {
                EmployeeOtherDutyInformationModel employeeOtherDutyInformationModels = new EmployeeOtherDutyInformationModel
                {
                    EmployeeOtherDutyInformationID = s.EmployeeOtherDutyInformationId,
                    EmployeeID = s.EmployeeId,
                    DesignationTypeID = s.DesignationTypeId,
                    DesignationID = s.DesignationId,
                    SubSectionTID = s.SubSectionTid,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    HeadCount = s.HeadCount,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Designation = s.Designation != null ? s.Designation.Name : "",
                    SubSectionT = s.SubSectionT != null ? s.SubSectionT.Name : "",
                    DesignationType = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.DesignationTypeId).Select(a => a.Value).FirstOrDefault(),
                };
                employeeOtherDutyInformationModel.Add(employeeOtherDutyInformationModels);
            });
            return employeeOtherDutyInformationModel.OrderByDescending(a => a.EmployeeOtherDutyInformationID).Where(w => w.EmployeeID == id).ToList();

        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<EmployeeOtherDutyInformationModel> GetData(SearchModel searchModel)
        {
            var EmployeeOtherDutyInformation = new EmployeeOtherDutyInformation();
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).LastOrDefault();
                        break;
                    case "Next":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).LastOrDefault();
                        break;
                    case "Next":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderBy(o => o.EmployeeOtherDutyInformationId).FirstOrDefault(s => s.EmployeeOtherDutyInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.OrderByDescending(o => o.EmployeeOtherDutyInformationId).FirstOrDefault(s => s.EmployeeOtherDutyInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<EmployeeOtherDutyInformationModel>(EmployeeOtherDutyInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertEmployeeOtherDutyInformation")]
        public EmployeeOtherDutyInformationModel Post(EmployeeOtherDutyInformationModel value)
        {
            var applicationdetail = _context.ApplicationMasterDetail.ToList();
            var EmployeeOtherDutyInformation = new EmployeeOtherDutyInformation
            {
                EmployeeId = value.EmployeeID,
                DesignationId = value.DesignationID,
                DesignationTypeId = value.DesignationTypeID,
                SubSectionTid = value.SubSectionTID,
                StartDate = value.StartDate,
                EndDate = value.EndDate,
                HeadCount = value.HeadCount,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.EmployeeOtherDutyInformation.Add(EmployeeOtherDutyInformation);
            _context.SaveChanges();
            value.Designation = _context.Designation.Where(d => d.DesignationId == value.DesignationID).Select(d => d.Name).FirstOrDefault();
            value.SubSectionT = _context.SubSectionTwo.Where(d => d.SubSectionTid == value.SubSectionTID).Select(d => d.Name).FirstOrDefault();

            value.DesignationType = applicationdetail.Where(a => a.ApplicationMasterDetailId == value.DesignationTypeID).Select(a => a.Value).FirstOrDefault();
            value.EmployeeOtherDutyInformationID = EmployeeOtherDutyInformation.EmployeeOtherDutyInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateEmployeeOtherDutyInformation")]
        public EmployeeOtherDutyInformationModel Put(EmployeeOtherDutyInformationModel value)
        {
            var applicationdetail = _context.ApplicationMasterDetail.ToList();
            var EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.SingleOrDefault(p => p.EmployeeOtherDutyInformationId == value.EmployeeOtherDutyInformationID);

            EmployeeOtherDutyInformation.EmployeeId = value.EmployeeID;
            EmployeeOtherDutyInformation.DesignationTypeId = value.DesignationTypeID;
            EmployeeOtherDutyInformation.DesignationId = value.DesignationID;
            EmployeeOtherDutyInformation.StartDate = value.StartDate;
            EmployeeOtherDutyInformation.EndDate = value.EndDate;
            EmployeeOtherDutyInformation.SubSectionTid = value.SubSectionTID;
            EmployeeOtherDutyInformation.HeadCount = value.HeadCount;
            EmployeeOtherDutyInformation.ModifiedByUserId = value.ModifiedByUserID;
            EmployeeOtherDutyInformation.ModifiedDate = DateTime.Now;
            EmployeeOtherDutyInformation.StatusCodeId = value.StatusCodeID.Value;
            value.SubSectionT = _context.SubSectionTwo.Where(d => d.SubSectionTid == value.SubSectionTID).Select(d => d.Name).FirstOrDefault();

            value.Designation = _context.Designation.Where(d => d.DesignationId == value.DesignationID).Select(d => d.Name).FirstOrDefault();
            value.DesignationType = applicationdetail.Where(a => a.ApplicationMasterDetailId == value.DesignationTypeID).Select(a => a.Value).FirstOrDefault();

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteEmployeeOtherDutyInformation")]
        public void Delete(int id)
        {
            var EmployeeOtherDutyInformation = _context.EmployeeOtherDutyInformation.SingleOrDefault(p => p.EmployeeOtherDutyInformationId == id);
            if (EmployeeOtherDutyInformation != null)
            {
                _context.EmployeeOtherDutyInformation.Remove(EmployeeOtherDutyInformation);
                _context.SaveChanges();
            }
        }
    }
}