﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FpdrugClassificationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public FpdrugClassificationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetFpdrugClassificationLineByID")]
        public List<FpdrugClassificationLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpdrugClassificationLine = _context.FpdrugClassificationLine.Include(p=>p.Purpose).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<FpdrugClassificationLineModel> fpdrugClassificationLineModel = new List<FpdrugClassificationLineModel>();
            fpdrugClassificationLine.ForEach(s =>
            {
                FpdrugClassificationLineModel fpdrugClassificationLineModels = new FpdrugClassificationLineModel
                {
                    FpdrugClassificationLineId=s.FpdrugClassificationLineId,
                    FpdrugClassificationId = s.FpdrugClassificationId,
                    PurposeId = s.PurposeId,
                    PurposeName=s.Purpose!=null?s.Purpose.CodeValue:null,
                    RequirementId = s.RequirementId,
                    TimeRequire = s.TimeRequire,
                    CalculationPointId=s.CalculationPointId,
                    WiLink=s.WiLink,
                    RequirementName = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.RequirementId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.RequirementId).Value : "",
                    CalculationPoint = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CalculationPointId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CalculationPointId).Value : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                fpdrugClassificationLineModel.Add(fpdrugClassificationLineModels);
            });
            return fpdrugClassificationLineModel.Where(c => c.FpdrugClassificationId == id).OrderByDescending(a => a.FpdrugClassificationLineId).ToList();
        }
        [HttpPost]
        [Route("InsertFpdrugClassificationLine")]
        public FpdrugClassificationLineModel Post(FpdrugClassificationLineModel value)
        {
            var fpdrugClassificationLine = new FpdrugClassificationLine
            {
                FpdrugClassificationId = value.FpdrugClassificationId,
                PurposeId = value.PurposeId,
                RequirementId = value.RequirementId,
                TimeRequire=value.TimeRequire,
                CalculationPointId=value.CalculationPointId,
                WiLink=value.WiLink,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.FpdrugClassificationLine.Add(fpdrugClassificationLine);
            _context.SaveChanges();
           value.FpdrugClassificationLineId = fpdrugClassificationLine.FpdrugClassificationLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateFpdrugClassificationLine")]
        public FpdrugClassificationLineModel Put(FpdrugClassificationLineModel value)
        {
            var fpdrugClassificationLine = _context.FpdrugClassificationLine.SingleOrDefault(p => p.FpdrugClassificationLineId == value.FpdrugClassificationLineId);
            fpdrugClassificationLine.FpdrugClassificationId = value.FpdrugClassificationId;
            fpdrugClassificationLine.PurposeId = value.PurposeId;
            fpdrugClassificationLine.RequirementId = value.RequirementId;
            fpdrugClassificationLine.TimeRequire = value.TimeRequire;
            fpdrugClassificationLine.CalculationPointId = value.CalculationPointId;
            fpdrugClassificationLine.WiLink = value.WiLink;
            fpdrugClassificationLine.ModifiedByUserId = value.ModifiedByUserID;
            fpdrugClassificationLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
          return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFpdrugClassificationLine")]
        public void Delete(int id)
        {
            var fpdrugClassificationLine = _context.FpdrugClassificationLine.SingleOrDefault(p => p.FpdrugClassificationLineId == id);
            if (fpdrugClassificationLine != null)
            {
                _context.FpdrugClassificationLine.Remove(fpdrugClassificationLine);
                _context.SaveChanges();
            }
        }
    }
}