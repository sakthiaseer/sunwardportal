﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PurchaseOrderLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PurchaseOrderLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetPurchaseOrderLine")]
        public List<PurchaseOrderLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var purchaseOrderLine = _context.PurchaseOrderLine.Include(a => a.DeliveryTo).Include(m => m.NavItem).Where(c => c.PurchaseOrderId == id).OrderByDescending(a => a.PurchaseOrderLineId).AsNoTracking().ToList();
            List<PurchaseOrderLineModel> purchaseOrderLineModel = new List<PurchaseOrderLineModel>();
            purchaseOrderLine.ForEach(s =>
            {
                PurchaseOrderLineModel purchaseOrderLineModels = new PurchaseOrderLineModel
                {
                    PurchaseOrderLineId = s.PurchaseOrderLineId,
                    PurchaseOrderId = s.PurchaseOrderId,
                    NavItemId = s.NavItemId,
                    PurchaseUOM = s.NavItem?.PurchaseUom,
                    NavDescription = s.NavItem?.Description,
                    Qty = s.Qty,
                    CurrencyId = s.CurrencyId,
                    CurrencyName = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CurrencyId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CurrencyId).Value : "",
                    DeliveryType = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.DeliveryTypeId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.DeliveryTypeId).Value : "",
                    UnitPrice = s.UnitPrice,
                    DeliveryTypeId = s.DeliveryTypeId,
                    ExpectedDeliveryDate = s.ExpectedDeliveryDate,
                    DeliveryToId = s.DeliveryToId,
                    Remarks = s.Remarks,
                    StatusCode = s.DeliveryTo != null ? s.DeliveryTo.Description : "",
                    NavisionItem = s.NavItem != null ? (s.NavItem.No + "|" + s.NavItem.Description + "|" + s.NavItem.Description2) : "",
                };
                purchaseOrderLineModel.Add(purchaseOrderLineModels);
            });
            return purchaseOrderLineModel;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PurchaseOrderLineModel> GetData(SearchModel searchModel)
        {
            var purchaseOrderLine = new PurchaseOrderLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Previous":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderBy(o => o.PurchaseOrderLineId).FirstOrDefault(s => s.PurchaseOrderLineId > searchModel.Id);
                        break;
                    case "Previous":
                        purchaseOrderLine = _context.PurchaseOrderLine.OrderByDescending(o => o.PurchaseOrderLineId).FirstOrDefault(s => s.PurchaseOrderLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PurchaseOrderLineModel>(purchaseOrderLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPurchaseOrderLine")]
        public PurchaseOrderLineModel Post(PurchaseOrderLineModel value)
        {
            var purchaseOrderLine = new PurchaseOrderLine
            {
                PurchaseOrderId = value.PurchaseOrderId,
                NavItemId = value.NavItemId,
                Qty = value.Qty,
                CurrencyId = value.CurrencyId,
                UnitPrice = value.UnitPrice,
                DeliveryTypeId = value.DeliveryTypeId,
                ExpectedDeliveryDate = value.ExpectedDeliveryDate,
                DeliveryToId = value.DeliveryToId,
                Remarks = value.Remarks,
            };
            _context.PurchaseOrderLine.Add(purchaseOrderLine);
            _context.SaveChanges();
            value.PurchaseOrderLineId = purchaseOrderLine.PurchaseOrderLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePurchaseOrderLine")]
        public PurchaseOrderLineModel Put(PurchaseOrderLineModel value)
        {
            var purchaseOrderLine = _context.PurchaseOrderLine.SingleOrDefault(p => p.PurchaseOrderLineId == value.PurchaseOrderLineId);
            purchaseOrderLine.PurchaseOrderId = value.PurchaseOrderId;
            purchaseOrderLine.NavItemId = value.NavItemId;
            purchaseOrderLine.Qty = value.Qty;
            purchaseOrderLine.CurrencyId = value.CurrencyId;
            purchaseOrderLine.UnitPrice = value.UnitPrice;
            purchaseOrderLine.DeliveryTypeId = value.DeliveryTypeId;
            purchaseOrderLine.ExpectedDeliveryDate = value.ExpectedDeliveryDate;
            purchaseOrderLine.DeliveryToId = value.DeliveryToId;
            purchaseOrderLine.Remarks = value.Remarks;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePurchaseOrderLine")]
        public void Delete(int id)
        {
            var purchaseOrderLine = _context.PurchaseOrderLine.SingleOrDefault(p => p.PurchaseOrderLineId == id);
            if (purchaseOrderLine != null)
            {
                _context.PurchaseOrderLine.Remove(purchaseOrderLine);
                _context.SaveChanges();
            }
        }
    }
}