﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ItemClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetItemClassifications")]
        public List<ItemClassificationModel> Get()
        {
            List<ItemClassificationModel> itemClassificationModels = new List<ItemClassificationModel>();
            var ItemClassification = _context.ItemClassificationMaster.Include("ItemClassificationForms").Include(c => c.ClassificationType).Include("Profile").Include("Form").Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ItemClassificationId).AsNoTracking().ToList();
            if (ItemClassification != null && ItemClassification.Count > 0)
            {
                ItemClassification.ForEach(s =>
                {
                    ItemClassificationModel itemClassificationModel = new ItemClassificationModel
                    {
                        ItemClassificationID = s.ItemClassificationId,
                        MainClassificationID = s.MainClassificationId,
                        ParentID = s.ParentId,
                        Name = s.Name,
                        Description = s.Description,
                        FormTitle = s.FormTitle,
                        IsForm = s.IsForm,
                        FormId = s.FormId,
                        IsApprovalForm = s.IsApprovalForm,
                        SessionId = s.SessionId,
                        FormName = s.Form != null ? s.Form.Name : string.Empty,
                        ProfileID = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ClassificationTypeId = s.ClassificationTypeId,
                        ScreenID = s.ClassificationType.CodeValue,
                        ClassificationLinkType = s.ClassificationLinkType,
                        itemClassificationFormIds = s.ItemClassificationForms.Where(i => i.ItemClassificationId == s.ItemClassificationId).Select(i => i.FormId).ToList(),
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser.UserName,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    itemClassificationModels.Add(itemClassificationModel);
                });
            }

            return itemClassificationModels;
        }
        [HttpGet]
        [Route("GetApplicationFormMasterPermissionTree")]
        public List<ApplicationFormMasterPermissionModel> GetApplicationFormMasterPermissionTree(long? id)
        {
            List<ApplicationFormMasterPermissionModel> ApplicationFormMasterPermissionModels = new List<ApplicationFormMasterPermissionModel>();
            var applicationFormMasterPermission = _context.ApplicationFormMasterPermission.Where(w => w.ScreenId != null).ToList();
            var itemClassificationMaster = _context.ItemClassificationMaster.Where(w => w.MainClassificationId == id).Select(s => s.ClassificationTypeId).Distinct().ToList();
            List<string> screenIds = new List<string>();
            itemClassificationMaster.ForEach(s =>
            {
                if (s == 1043)
                {
                    //Company
                    screenIds.Add("CLASSICOM001");
                }
                if (s == 1041)
                {
                    //Package Form
                    screenIds.Add("CLASSICOM002");
                }
                if (s == 1040)
                {
                    //Material Form
                    screenIds.Add("CLASSIMAT001");
                }
                if (s == 1042)
                {
                    //FP Item
                    screenIds.Add("CLASSIFP001");
                }
                if (s == 1503)
                {
                    //Classification BMR
                    screenIds.Add("BMR2020");
                }
                if (s == 1045)
                {
                    //Machine
                    screenIds.Add("CLASSICOM004");
                }
                if (s == 1049)
                {
                    //Shipment
                    screenIds.Add("CLASSISHIP001");
                }
                if (s == 1501)
                {
                    //Shipment
                    screenIds.Add("CLASSIFPDRUG001");
                }
                if (s == 1048)
                {
                    //Transport
                    screenIds.Add("CLASSITRANS001");
                }
                if (s == 1044)
                {
                    //Common Process
                    screenIds.Add("CLASSICOM003");
                }
                if (s == 1046)
                {
                    //Tooling
                    screenIds.Add("CLASSICOM005");
                }
                if (s == 1504)
                {
                    //Tooling
                    screenIds.Add("MET001");
                }
                if (s == 1505)
                {
                    //Tooling
                    screenIds.Add("PROCLASSICOM001");
                }
            });
            ApplicationFormMasterPermissionModels = ItemClassificationMasterForScreenId(screenIds);
            return ApplicationFormMasterPermissionModels;
        }
        private List<ApplicationFormMasterPermissionModel> ItemClassificationMasterForScreenId(List<string> screenIds)
        {
            List<ApplicationFormMasterPermissionModel> ApplicationFormMasterPermissionModels = new List<ApplicationFormMasterPermissionModel>();

            var applicationFormMasterPermissionTree = _context.ApplicationFormMasterPermission.Include(a => a.ApplicationFormCode)
                .AsNoTracking().ToList();
            applicationFormMasterPermissionTree.ForEach(s =>
            {
                ApplicationFormMasterPermissionModel ApplicationFormMasterPermissionModel = new ApplicationFormMasterPermissionModel();
                ApplicationFormMasterPermissionModel.ApplicationFormMasterPermissionId = s.ApplicationFormMasterPermissionId;
                ApplicationFormMasterPermissionModel.ScreenId = s.ScreenId;
                ApplicationFormMasterPermissionModel.ApplicationFormCodeId = s.ApplicationFormCodeId;
                ApplicationFormMasterPermissionModel.ParentId = s.ParentId;
                ApplicationFormMasterPermissionModel.ApplicationCodeId = s.ApplicationCodeId;
                ApplicationFormMasterPermissionModel.ApplicationFormCodeName = s.ApplicationFormCode != null ? s.ApplicationFormCode.CodeValue : null;
                ApplicationFormMasterPermissionModel.Id = s.ApplicationFormMasterPermissionId;
                ApplicationFormMasterPermissionModel.Name = ApplicationFormMasterPermissionModel.ApplicationFormCodeName;
                ApplicationFormMasterPermissionModels.Add(ApplicationFormMasterPermissionModel);
            });

            var lookup = ApplicationFormMasterPermissionModels.ToLookup(x => x.ParentId);

            Func<long?, List<ApplicationFormMasterPermissionModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new ApplicationFormMasterPermissionModel()
                    {
                        ApplicationFormMasterPermissionId = x.ApplicationFormMasterPermissionId,
                        ParentId = x.ParentId,
                        ScreenId = x.ScreenId,
                        ApplicationFormCodeId = x.ApplicationFormCodeId,
                        Children = build(x.ApplicationCodeId),
                        ApplicationFormCodeName = x.ApplicationFormCodeName,
                        Id = x.ApplicationCodeId,
                        Name = x.ApplicationFormCodeName,
                        ApplicationCodeId = x.ApplicationCodeId,
                    })
                    .ToList();
            var nestedlist = build(null).ToList();
            return nestedlist.Where(w => screenIds.Contains(w.ScreenId)).ToList();
        }
        [HttpGet]
        [Route("GetItemClassificationPermission")]
        public ItemClassificationPermissionModel GetItemClassificationPermission(long? ItemClassificationId, string UserType, long? userId)
        {
            ItemClassificationPermissionModel ItemClassificationPermissionModels = new ItemClassificationPermissionModel();
            var itemClassificationPermission = _context.ItemClassificationPermission.Where(w => w.ItemClassificationId == ItemClassificationId).ToList();
            ItemClassificationPermissionModels.UserType = UserType;
            ItemClassificationPermissionModels.ItemClassificationId = ItemClassificationId;
            ItemClassificationPermissionModels.SelectedPermissions = new List<long?>();
            if (UserType == "user-group")
            {
                ItemClassificationPermissionModels.SelectedPermissions = itemClassificationPermission.Where(w => w.ItemClassificationId == ItemClassificationId && w.UserGroupId != null && w.UserType == UserType && w.UserGroupId == userId).Select(u => u.ApplicationCodeId).Distinct().ToList();
            }
            else
            {
                ItemClassificationPermissionModels.SelectedPermissions = itemClassificationPermission.Where(w => w.ItemClassificationId == ItemClassificationId && w.UserId != null && w.UserType == UserType && w.UserId == userId).Select(u => u.ApplicationCodeId).Distinct().ToList();
            }
            return ItemClassificationPermissionModels;

        }
        [HttpGet]
        [Route("GetItemClassificationPermissionByClass")]
        public List<ItemClassificationPermissionModel> GetItemClassificationPermission(long? ItemClassificationId)
        {
            List<ItemClassificationPermissionModel> ItemClassificationPermissionModels = new List<ItemClassificationPermissionModel>();
            var userIDs = _context.ItemClassificationPermission.Where(w => w.ItemClassificationId == ItemClassificationId).GroupBy(g => g.UserId).Select(s => s.Key).ToList();
            var appUsers = _context.ApplicationUser.Where(w => userIDs.Contains(w.UserId)).ToList();
            if (appUsers != null && appUsers.Count > 0)
            {
                appUsers.ForEach(a =>
                {
                    ItemClassificationPermissionModel ItemClassificationPermissionModel = new ItemClassificationPermissionModel();
                    ItemClassificationPermissionModel.ItemClassificationPermissionId = 0;
                    ItemClassificationPermissionModel.UserId = a.UserId;
                    ItemClassificationPermissionModel.UserName = a.UserName;
                    ItemClassificationPermissionModels.Add(ItemClassificationPermissionModel);
                });
            }
            return ItemClassificationPermissionModels;

        }
        [HttpGet]
        [Route("GetItemClassificationId")]
        public ItemClassificationModel GetItemClassificationId(int? id, int userId)
        {


            var itemclassification = _context.ItemClassificationMaster.Include("ItemClassificationForms").Include(c => c.ClassificationType).Include("Profile").Include("Form").Include("AddedByUser").Include("ModifiedByUser").Select(s => new ItemClassificationModel
            {
                ItemClassificationID = s.ItemClassificationId,
                MainClassificationID = s.MainClassificationId,
                ParentID = s.ParentId,
                ProfileID = s.ProfileId,
                ProfileReferenceNo = s.ProfileReferenceNo,
                FormId = s.FormId,
                Name = s.Name,
                IsForm = s.IsForm,
                Description = s.Description,
                FormTitle = s.FormTitle,
                IsApprovalForm = s.IsApprovalForm,
                SessionId = s.SessionId,
                ScreenID = s.ClassificationType != null ? s.ClassificationType.CodeValue : "",
                ClassificationTypeId = s.ClassificationTypeId,
                ClassificationLinkType = s.ClassificationLinkType,
                itemClassificationFormIds = s.ItemClassificationForms.Where(i => i.ItemClassificationId == s.ItemClassificationId).Select(i => i.FormId).OrderBy(a => a).ToList(),
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                StatusCodeID = s.StatusCodeId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                AddedByUserID = s.AddedByUserId,

            }).OrderByDescending(o => o.ItemClassificationID).Where(i => i.ItemClassificationID == id).FirstOrDefault();

            return itemclassification;
        }
        [HttpGet]
        [Route("GetItemForm")]
        public List<ItemClassificationFormsModel> GetItemForm(int? id, int? userId)
        {
            var DocumentPermission = _context.DocumentPermission.ToList();
            var ApplicationSubFormList = _context.ApplicationFormSub.Include(t => t.ApplicationForm).Include(t => t.ApplicationForm.ItemClassificationAccess).AsNoTracking().ToList();
            List<ItemClassificationFormsModel> itemClassificationFormsModels = new List<ItemClassificationFormsModel>();
            var itemClassificationForms = _context.ItemClassificationForms.Include(f => f.Form).Include(i => i.Form.ItemClassificationAccess).Include(p => p.ItemClassification).Where(a => a.ItemClassificationId == id).OrderBy(o => o.FormId).AsNoTracking().ToList();
            if (itemClassificationForms != null && itemClassificationForms.Count > 0)
            {
                DocumentPermissionController documentPermissionController = new DocumentPermissionController(_context, _mapper);

                itemClassificationForms.ForEach(s =>
                {
                    ItemClassificationFormsModel itemClassificationFormsModel = new ItemClassificationFormsModel();
                    itemClassificationFormsModel.ItemClassificationFormsID = s.ItemClassificationFormsId;
                    itemClassificationFormsModel.ItemClassificationID = s.ItemClassificationId;
                    itemClassificationFormsModel.FormID = s.FormId;
                    itemClassificationFormsModel.ScreenID = s.Form != null ? s.Form.ScreenId : "";
                    itemClassificationFormsModel.ModuleName = s.Form != null ? s.Form.ModuleName : "";
                    itemClassificationFormsModel.Path = s.Form != null ? s.Form.PathUrl : "";
                    itemClassificationFormsModel.FormName = s.Form != null ? s.Form.Name : "";
                    itemClassificationFormsModel.ProfileID = s.Form != null ? s.Form.ProfileId : null;
                    itemClassificationFormsModel.ProfileName = s.Form.Profile != null ? s.Form.Profile.Name : "";
                    itemClassificationFormsModel.LinkProfileReferenceNo = s.ItemClassification != null ? s.ItemClassification.ProfileReferenceNo : "";
                    itemClassificationFormsModel.ClassificationLinkType = s.ItemClassification != null ? s.ItemClassification.ClassificationLinkType : "";
                    itemClassificationFormsModel.MainForm = true;
                    var userid = s.Form != null && s.Form.ItemClassificationAccess != null ? s.Form.ItemClassificationAccess.Where(w => w.FormId == s.FormId && w.UserId == userId).Select(a => a.RoleId).FirstOrDefault() : null;
                    if (userid != null)
                    {
                        itemClassificationFormsModel.DocumentPermissionModels = documentPermissionController.GetDocumentPermissionByRoll((int)userid);
                    }
                    if (ApplicationSubFormList != null)
                    {
                        List<ApplicationFormSubModel> ApplicationFormSubModels = new List<ApplicationFormSubModel>();
                        ApplicationSubFormList.Where(c => c.FormId == s.FormId).ToList().ForEach(h =>
                        {
                            ApplicationFormSubModel ApplicationFormSubModel = new ApplicationFormSubModel();
                            ApplicationFormSubModel.ApplicationFormId = h.ApplicationFormId;
                            ApplicationFormSubModel.ApplicationFormSubId = h.ApplicationFormSubId;
                            ApplicationFormSubModel.FormId = h.FormId;
                            ApplicationFormSubModel.FormName = h.ApplicationForm != null ? h.ApplicationForm.Name : "";
                            ApplicationFormSubModel.ModuleName = h.ApplicationForm != null ? h.ApplicationForm.ModuleName : "";
                            ApplicationFormSubModel.Path = h.ApplicationForm != null ? h.ApplicationForm.PathUrl : "";
                            ApplicationFormSubModel.ProfileID = h.ApplicationForm != null ? h.ApplicationForm.ProfileId : null;
                            ApplicationFormSubModel.ScreenID = h.ApplicationForm != null ? h.ApplicationForm.ScreenId : "";
                            var userid = h.Form != null && h.Form.ItemClassificationAccess != null ? h.Form.ItemClassificationAccess.Where(w => w.FormId == h.FormId && w.UserId == userId).Select(a => a.RoleId).FirstOrDefault() : null;
                            if (userid != null)
                            {
                                ApplicationFormSubModel.DocumentPermissionModels = documentPermissionController.GetDocumentPermissionByRoll((int)userid);
                            }
                            ApplicationFormSubModels.Add(ApplicationFormSubModel);
                        });
                        itemClassificationFormsModel.ApplicationFormSubModelList = ApplicationFormSubModels;
                    }
                    itemClassificationFormsModels.Add(itemClassificationFormsModel);
                });

            }
            return itemClassificationFormsModels;
        }
        [HttpGet]
        [Route("GetItemClassificationSubItems")]
        public List<ItemClassificationModel> GetItemClassificationSubItems(int? id, int userId)
        {

            List<ItemClassificationModel> itemClassificationModels = new List<ItemClassificationModel>();
            var itemclassification = _context.ItemClassificationMaster.Include("ItemClassificationForms").OrderByDescending(o => o.ItemClassificationId).Where(i => i.ParentId == id).AsNoTracking().ToList();
            if (itemclassification != null && itemclassification.Count > 0)
            {
                itemclassification.ForEach(s =>
                {
                    ItemClassificationModel itemClassificationModel = new ItemClassificationModel
                    {
                        ItemClassificationID = s.ItemClassificationId,
                        MainClassificationID = s.MainClassificationId,
                        ParentID = s.ParentId,
                        Name = s.Name,
                        itemClassificationFormIds = s.ItemClassificationForms.Where(i => i.ItemClassificationId == s.ItemClassificationId).Select(i => i.FormId).ToList(),
                        IsForm = s.IsForm,
                        Description = s.Description,
                        FormTitle = s.FormTitle,
                        ProfileID = s.ProfileId,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ClassificationTypeId = s.ClassificationTypeId,
                        ClassificationLinkType = s.ClassificationLinkType,
                        IsApprovalForm = s.IsApprovalForm,
                        SessionId = s.SessionId,
                    };
                    itemClassificationModels.Add(itemClassificationModel);
                });
            }

            return itemClassificationModels;
        }
        [HttpGet]
        [Route("GetItemClassificationTree")]
        public List<ItemClassificationModel> GetItemClassificationTree(int? id, int userId)
        {
            var itemClassificationTree = _context.ItemClassificationMaster.FirstOrDefault(t => t.ItemClassificationId == id);
            if (itemClassificationTree == null)
            {
                return null;
            }

            var classificationTree = _context.ItemClassificationMaster.Include("Profile").Include("Form").Include("AddedByUser").Include("ModifiedByUser")
                .AsNoTracking().Select(s => new
                {
                    s.ItemClassificationId,
                    s.MainClassificationId,
                    s.ClassificationTypeId,
                    s.ClassificationLinkType,
                    s.ParentId,
                    s.Name,
                    s.Description,
                    s.FormId,
                    s.ProfileId,
                    s.StatusCodeId,
                    s.AddedByUserId,
                    s.AddedDate,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    s.IsForm,
                    s.ProfileReferenceNo,
                    ProfileName = s.Profile.Name,
                    s.IsApprovalForm,
                    s.SessionId,

                })
             .Where(i => i.MainClassificationId == itemClassificationTree.MainClassificationId).ToList();

            var lookup = classificationTree.ToLookup(x => x.ParentId);

            Func<long?, List<ItemClassificationModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new ItemClassificationModel()
                    {
                        ItemClassificationID = x.ItemClassificationId,
                        ParentID = x.ParentId,
                        MainClassificationID = x.MainClassificationId,
                        ClassificationTypeId = x.ClassificationTypeId,
                        ClassificationLinkType = x.ClassificationLinkType,
                        Name = x.Name,
                        Description = x.Description,
                        ProfileID = x.ProfileId,
                        ProfileName = x.ProfileName,
                        ProfileReferenceNo = x.ProfileReferenceNo,
                        FormId = x.FormId,
                        AddedByUserID = x.AddedByUserId,
                        ModifiedByUserID = x.ModifiedByUserId,
                        StatusCodeID = x.StatusCodeId,
                        IsForm = x.IsForm,
                        IsApprovalForm = x.IsApprovalForm,
                        SessionId = x.SessionId,
                        Children = build(x.ItemClassificationId),

                    })
                    .ToList();
            var nestedlist = build(null).ToList();
            return nestedlist;
        }
        [HttpGet]
        [Route("GetItemClassificationPermissionList")]
        public List<ItemClassificationPermissionModel> GetItemClassificationPermissionList(int? id, int userId)
        {
            List<ItemClassificationPermissionModel> ItemClassificationPermissionModels = new List<ItemClassificationPermissionModel>();
            var usercount = _context.ItemClassificationPermission.Where(w => w.UserId == userId && w.UserType == "user").Count();
            var usergroupcount = _context.ItemClassificationPermission.Where(w => w.UserId == userId && w.UserType == "user-group").Count();
            if (usercount > 0 || usergroupcount > 0)
            {
                var ItemClassificationPermission = _context.ItemClassificationPermission.Include(a => a.ApplicationCode).Include(b => b.ApplicationCode.ApplicationFormCode).Include(c => c.ApplicationCode.Parent).Include(d => d.ApplicationCode.Parent.ApplicationFormCode).Where(w => w.ItemClassificationId == id && w.UserId == userId).ToList();
                if (ItemClassificationPermission == null || ItemClassificationPermission.Count == 0)
                {
                    ItemClassificationPermission = _context.ItemClassificationPermission.Include(a => a.ApplicationCode).Include(b => b.ApplicationCode.ApplicationFormCode).Include(c => c.ApplicationCode.Parent).Include(d => d.ApplicationCode.Parent.ApplicationFormCode).Where(w => w.ItemClassificationId == id && w.UserId == userId && w.UserType == "user-group").ToList();
                }
                ItemClassificationPermission.ForEach(s =>
                {
                    ItemClassificationPermissionModel ItemClassificationPermissionModel = new ItemClassificationPermissionModel();
                    ItemClassificationPermissionModel.ItemClassificationPermissionId = s.ItemClassificationPermissionId;
                    ItemClassificationPermissionModel.ApplicationCodeId = s.ApplicationCodeId;
                    ItemClassificationPermissionModel.ItemClassificationId = s.ItemClassificationId;
                    ItemClassificationPermissionModel.UserId = s.UserId;
                    ItemClassificationPermissionModel.ApplicationFormCodeId = s.ApplicationCode?.ApplicationFormCode?.CodeId;
                    ItemClassificationPermissionModel.ApplicationFormCodeName = s.ApplicationCode?.ApplicationFormCode?.CodeValue;
                    ItemClassificationPermissionModel.ParentId = s.ApplicationCode?.ParentId;
                    ItemClassificationPermissionModel.ParentName = s.ApplicationCode?.Parent?.ApplicationFormCode?.CodeValue;
                    ItemClassificationPermissionModel.ScreenId = s.ApplicationCode?.ScreenId;
                    ItemClassificationPermissionModels.Add(ItemClassificationPermissionModel);
                });
            }
            return ItemClassificationPermissionModels;
        }
        [HttpGet]
        [Route("GetItemClassification")]
        public List<ItemClassificationModel> GetItemClassification(int id)
        {
            List<ItemClassificationModel> itemClassificationModels = new List<ItemClassificationModel>();
            var itemClassification = _context.ItemClassificationMaster.Include("ItemClassificationForms").Include("Profile").Include("Form").Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ItemClassificationId).Where(t => t.ItemClassificationId == id).AsNoTracking().ToList();
            if (itemClassification != null && itemClassification.Count > 0)
            {
                itemClassification.ForEach(s =>
                {
                    ItemClassificationModel itemClassificationModel = new ItemClassificationModel
                    {
                        ItemClassificationID = s.ItemClassificationId,
                        MainClassificationID = s.MainClassificationId,
                        ParentID = s.ParentId,
                        Name = s.Name,
                        Description = s.Description,
                        FormTitle = s.FormTitle,
                        IsForm = s.IsForm,
                        FormName = s.Form != null ? s.Form.Name : string.Empty,
                        ProfileID = s.ProfileId,
                        ClassificationTypeId = s.ClassificationTypeId,
                        ClassificationLinkType = s.ClassificationLinkType,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        itemClassificationFormIds = s.ItemClassificationForms.Where(i => i.ItemClassificationId == s.ItemClassificationId).Select(i => i.FormId).ToList(),
                        ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsApprovalForm = s.IsApprovalForm,
                        SessionId = s.SessionId,
                    };
                    itemClassificationModels.Add(itemClassificationModel);
                });
            }

            return itemClassificationModels;
        }
        [HttpGet]
        [Route("GetApplicationForms")]
        public List<ApplicationFormModel> GetApplicationForms()
        {
            List<ApplicationFormModel> applicationFormModels = new List<ApplicationFormModel>();
            var applicationform = _context.ApplicationForm.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.FormId).AsNoTracking().ToList();
            if (applicationform != null && applicationform.Count > 0)
            {
                applicationform.ForEach(s =>
                {
                    ApplicationFormModel applicationFormModel = new ApplicationFormModel
                    {
                        Name = s.Name,
                        FormID = s.FormId,
                        ModuleName = s.ModuleName,
                        PathURL = s.PathUrl,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ClassificationTypeId = s.ClassificationTypeId,
                    };
                    applicationFormModels.Add(applicationFormModel);
                });

            }

            return applicationFormModels;
        }
        [HttpGet]
        [Route("GetApplicationFormsType")]
        public List<ApplicationFormModel> GetApplicationFormsType(int id)
        {
            var classificationType = _context.CodeMaster.FirstOrDefault(c => c.CodeId == id);
            List<ApplicationFormModel> applicationFormModels = new List<ApplicationFormModel>();
            var applicationform = _context.ApplicationForm.Include("AddedByUser").Include(c => c.ClassificationType).Include("ModifiedByUser").Where(s => s.FormType == "Dynamic" && s.ClassificationTypeId == classificationType.CodeId).OrderByDescending(o => o.FormId).AsNoTracking().ToList();
            if (applicationform != null && applicationform.Count > 0)
            {
                applicationform.ForEach(s =>
                {
                    ApplicationFormModel applicationFormModel = new ApplicationFormModel
                    {
                        Name = s.Name,
                        FormID = s.FormId,
                        ModuleName = s.ModuleName,
                        PathURL = s.PathUrl,
                        FormType = s.FormType,
                        ClassificationTypeId = s.ClassificationTypeId,
                        ClassificationType = s.ClassificationType != null ? s.ClassificationType.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    applicationFormModels.Add(applicationFormModel);
                });
            }


            return applicationFormModels;
        }
        [HttpGet]
        [Route("GetItemClassificationAccess")]
        public List<ItemClassificationAccessModel> GetItemClassificationAccess(int id)
        {

            var itemClassificationAccess = _context.ItemClassificationAccess.Include("User").Include("Role").Include("UserGroup").Where(f => f.ItemClassificationId == id).AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            List<long> userIds = itemClassificationAccess.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee.Include("Department").Include("Designation").Where(e => e.UserId != null && userIds.Distinct().ToList().Contains(e.UserId.Value)).AsNoTracking().ToList();
            List<ItemClassificationAccessModel> itemClassificationAccessModels = new List<ItemClassificationAccessModel>();
            userIds.ForEach(u =>
            {
                if (!itemClassificationAccessModels.Any(r => r.UserID == u))
                {
                    var employee = employees.FirstOrDefault(e => e.UserId == u);
                    var userUserRole = itemClassificationAccess.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                    var userGroupRole = itemClassificationAccess.FirstOrDefault(ug => ug.ItemClassificationId == userUserRole.ItemClassificationId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                    List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u).Select(s => s.UserGroupId.Value).ToList();
                    ItemClassificationAccessModel itemClassificationAccessModel = new ItemClassificationAccessModel();
                    itemClassificationAccessModel.ItemClassificationAccessID = userUserRole.ItemClassificationAccessId;
                    itemClassificationAccessModel.UserID = userUserRole.UserId;
                    itemClassificationAccessModel.RoleID = userUserRole.RoleId;
                    itemClassificationAccessModel.ItemClassificationID = userUserRole.ItemClassificationId;
                    itemClassificationAccessModel.RoleName = userUserRole.Role.DocumentRoleName;
                    itemClassificationAccessModel.UserName = userUserRole.User.UserName;
                    itemClassificationAccessModel.NickName = employee.NickName;
                    itemClassificationAccessModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                    itemClassificationAccessModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                    itemClassificationAccessModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;

                    itemClassificationAccessModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                    itemClassificationAccessModels.Add(itemClassificationAccessModel);
                }
            });

            itemClassificationAccessModels = itemClassificationAccessModels.OrderByDescending(o => o.ItemClassificationID).Where(s => s.ItemClassificationID == id).ToList();
            return itemClassificationAccessModels;

        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ItemClassificationModel> GetData(SearchModel searchModel)
        {
            var itemClassification = new ItemClassificationMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).FirstOrDefault();
                        break;
                    case "Last":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).LastOrDefault();
                        break;
                    case "Next":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).LastOrDefault();
                        break;
                    case "Previous":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).FirstOrDefault();
                        break;
                    case "Last":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).LastOrDefault();
                        break;
                    case "Next":
                        itemClassification = _context.ItemClassificationMaster.OrderBy(o => o.ItemClassificationId).FirstOrDefault(s => s.ItemClassificationId > searchModel.Id);
                        break;
                    case "Previous":
                        itemClassification = _context.ItemClassificationMaster.OrderByDescending(o => o.ItemClassificationId).FirstOrDefault(s => s.ItemClassificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ItemClassificationModel>(itemClassification);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertItemClassification")]
        public ItemClassificationModel Post(ItemClassificationModel value)
        {

            // var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID });
            var itemClassificationMaster = new ItemClassificationMaster
            {
                ParentId = value.ParentID,
                MainClassificationId = value.MainClassificationID,
                Name = value.Name,
                Description = value.Description,
                FormTitle = value.FormTitle,
                IsForm = value.IsForm,
                ProfileId = value.ProfileID,
                FormId = value.FormId,
                ClassificationTypeId = value.ClassificationTypeId,
                ClassificationLinkType = value.ClassificationLinkType,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsApprovalForm = value.IsApprovalForm,
                SessionId = Guid.NewGuid(),
                //   ProfileReferenceNo = profileNo,
            };
            _context.ItemClassificationMaster.Add(itemClassificationMaster);
            _context.SaveChanges();
            value.ItemClassificationID = itemClassificationMaster.ItemClassificationId;
            value.ScreenID = _context.CodeMaster.FirstOrDefault(c => c.CodeId == value.ClassificationTypeId).CodeValue;
            if (value.itemClassificationFormIds.Count > 0)
            {
                value.itemClassificationFormIds.ForEach(i =>
                {
                    var itemClassificationforms = new ItemClassificationForms
                    {
                        ItemClassificationId = value.ItemClassificationID,
                        FormId = i,
                    };
                    _context.ItemClassificationForms.Add(itemClassificationforms);

                });
            }
            if (value.ParentID == null && value.MainClassificationID == null)
            {
                var mainid = _context.ItemClassificationMaster.FirstOrDefault(id => id.ItemClassificationId == itemClassificationMaster.ItemClassificationId);
                mainid.MainClassificationId = itemClassificationMaster.ItemClassificationId;

                _context.SaveChanges();

                value.MainClassificationID = mainid.MainClassificationId;
            }
            _context.SaveChanges();
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemClassification")]
        public ItemClassificationModel Put(ItemClassificationModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var itemClassificationMaster = _context.ItemClassificationMaster.SingleOrDefault(p => p.ItemClassificationId == value.ItemClassificationID);
            //Plant.PlantId = value.PlantID;
            itemClassificationMaster.ModifiedByUserId = value.ModifiedByUserID;
            itemClassificationMaster.MainClassificationId = value.MainClassificationID;
            itemClassificationMaster.ModifiedDate = DateTime.Now;
            itemClassificationMaster.ParentId = value.ParentID;
            itemClassificationMaster.Name = value.Name;
            itemClassificationMaster.Description = value.Description;
            itemClassificationMaster.FormTitle = value.FormTitle;
            itemClassificationMaster.IsForm = value.IsForm;
            itemClassificationMaster.IsApprovalForm = value.IsApprovalForm;
            itemClassificationMaster.ProfileId = value.ProfileID;
            itemClassificationMaster.FormId = value.FormId;
            itemClassificationMaster.ClassificationTypeId = value.ClassificationTypeId;
            itemClassificationMaster.ClassificationLinkType = value.ClassificationLinkType;
            itemClassificationMaster.StatusCodeId = value.StatusCodeID;
            itemClassificationMaster.SessionId = value.SessionId;
            var ItemClassificationForms = _context.ItemClassificationForms.Where(m => m.ItemClassificationId == value.ItemClassificationID).ToList();
            if (ItemClassificationForms.Count() > 0)
            {
                _context.ItemClassificationForms.RemoveRange(ItemClassificationForms);
                _context.SaveChanges();
            }
            if (value.itemClassificationFormIds.Count > 0)
            {
                value.itemClassificationFormIds.ForEach(i =>
                {
                    var existing = _context.ItemClassificationForms.Where(item => item.FormId == i && item.ItemClassificationId == value.ItemClassificationID).FirstOrDefault();
                    if (existing == null)
                    {
                        var itemClassificationforms = new ItemClassificationForms
                        {
                            ItemClassificationId = value.ItemClassificationID,
                            FormId = i,
                        };
                        _context.ItemClassificationForms.Add(itemClassificationforms);
                        _context.SaveChanges();
                    }
                });
            }
            _context.SaveChanges();



            return value;
        }
        public IEnumerable<ItemClassificationModel> GetAllChildren(ItemClassificationModel item)
        {
            return item.Children.Concat(item.Children.SelectMany(GetAllChildren));
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemClassification")]
        public void Delete(int id)
        {
            ItemClassificationModel itemClassificationModel = new ItemClassificationModel();
            List<ItemClassificationModel> itemClassificationModels = new List<ItemClassificationModel>();
            itemClassificationModel.Children = GetItemClassificationTree(id, 1);
            itemClassificationModels.Add(itemClassificationModel);
            var allDescendantNames = GetAllChildren(itemClassificationModel).Select(child => child.Children).ToList();
            List<ItemClassificationModel> itemClassificationModelsLists = new List<ItemClassificationModel>();
            allDescendantNames.ForEach(h =>
            {
                ItemClassificationModel itemClassificationModelList = new ItemClassificationModel();
                if (h.Count > 0)
                {
                    h.ForEach(a =>
                    {
                        ItemClassificationModel itemClassificationModelList = new ItemClassificationModel();
                        itemClassificationModelList.ItemClassificationID = a.ItemClassificationID;
                        itemClassificationModelList.MainClassificationID = a.MainClassificationID;
                        itemClassificationModelsLists.Add(itemClassificationModelList);
                    });
                }
            });

            if (itemClassificationModelsLists.Count > 0)
            {
                var errorMessages = "";
                try
                {
                    itemClassificationModelsLists.OrderByDescending(o => o.ItemClassificationID).ToList().ForEach(b =>
                  {
                      var ItemClassificationForms = _context.ItemClassificationForms.Where(w => w.ItemClassificationId == b.ItemClassificationID).ToList();
                      if (ItemClassificationForms != null)
                      {
                          _context.ItemClassificationForms.RemoveRange(ItemClassificationForms);
                          _context.SaveChanges();
                      }
                      var subClassification = _context.ItemClassificationMaster.Where(p => p.ItemClassificationId == b.ItemClassificationID).FirstOrDefault();
                      _context.ItemClassificationMaster.Remove(subClassification);
                      _context.SaveChanges();
                  });
                }

                catch (Exception ex)
                {
                    if (string.IsNullOrEmpty(errorMessages))
                    {
                        errorMessages = "There is a transaction record for this company record cannot be deleted!";
                    }
                    throw new AppException(errorMessages, ex);
                }


            }
            var itemClassificationMaster = _context.ItemClassificationMaster.FirstOrDefault(p => p.ItemClassificationId == id);
            var errorMessage = "";
            try
            {
                var subClassification = _context.ItemClassificationMaster.Where(p => p.ParentId == id).ToList();
                if (subClassification.Count == 0)
                {
                    if (itemClassificationMaster != null)
                    {
                        var itemClassificationAccess = _context.ItemClassificationAccess.Where(i => i.ItemClassificationId == id).ToList();
                        if (itemClassificationAccess.Count > 0 && itemClassificationAccess != null)
                        {
                            _context.ItemClassificationAccess.RemoveRange(itemClassificationAccess);
                            _context.SaveChanges();
                        }
                        _context.ItemClassificationMaster.Remove(itemClassificationMaster);
                        _context.SaveChanges();
                    }

                }
                else
                {
                    throw new AppException("There is a transaction record for this company record cannot be deleted!", null);
                }

            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction record for this company record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        [HttpPost]
        [Route("InsertItemClassificationAccess")]
        public ItemClassificationModel PostRole(ItemClassificationModel value)
        {
            var itemClassificationIDs = value.ItemClassificationIDs;
            CreateItemClassificationAccess(value);
            _context.SaveChanges();
            if (itemClassificationIDs != null && itemClassificationIDs.Any())
            {
                itemClassificationIDs.ForEach(f =>
                {
                    value.ItemClassificationID = f.Value;
                    CreateItemClassificationAccess(value);
                    _context.SaveChanges();

                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("GetItemClassificationByUserID")]
        public List<ItemClassificationModel> GetItemClassificationByUserID(int id)
        {
            var ItemClassification = new List<ItemClassificationModel>();
            var documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            var itemAccessRoleList = _context.ItemClassificationAccess.AsNoTracking().ToList();
            var userGroupList = _context.UserGroupUser.Where(f => f.UserId == id).AsNoTracking().ToList();
            var filterItem = _context.ItemClassificationMaster.Include("AddedByUser").Include(s => s.ClassificationType).Include(p => p.Profile).Where(t => t.StatusCodeId == 1).AsNoTracking().ToList();
            List<ItemClassificationModel> permissionItem = new List<ItemClassificationModel>();
            if (filterItem.Count > 0)
            {
                ItemClassification = filterItem.Select(s => new ItemClassificationModel
                {
                    ItemClassificationID = s.ItemClassificationId,
                    MainClassificationID = s.MainClassificationId,
                    ParentID = s.ParentId,
                    IsForm = s.IsForm,
                    FormId = s.FormId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    Name = s.Name,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    ScreenID = s.ClassificationType.CodeValue,
                    SessionId = s.SessionId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ClassificationTypeId = s.ClassificationTypeId,
                    ClassificationLinkType = s.ClassificationLinkType

                }).OrderByDescending(o => o.ItemClassificationID).Where(t => t.ParentID == null && t.StatusCodeID == 1).ToList();

                if (itemAccessRoleList.Count > 0)
                {

                    ItemClassification.ForEach(item =>
                    {
                        var docuRoles = itemAccessRoleList.Where(f => f.ItemClassificationId == item.ItemClassificationID && item.AddedByUserID != id).ToList();
                        if (docuRoles.Count == 0)
                        {
                            if (!permissionItem.Any(f => f.ItemClassificationID == item.ItemClassificationID) && item.ItemClassificationID == item.ItemClassificationID && item.AddedByUserID == id)
                            {
                                item.IsDelete = true;
                                item.IsRead = true;
                                item.IsEdit = true;
                                permissionItem.Add(item);
                            }
                        }
                        else
                        {
                            docuRoles.ForEach(access =>
                            {
                                if (access.ItemClassificationId == item.ItemClassificationID && item.AddedByUserID != id)
                                {
                                    if (access.UserId == id)
                                    {
                                        var userpermission = documentpermission.Where(d => d.DocumentRoleId == access.RoleId).FirstOrDefault();
                                        if (userpermission != null)
                                        {
                                            item.RoleID = userpermission.DocumentRoleId;
                                            item.UserID = access.UserId;
                                            item.UserGroupID = access.UserGroupId;
                                            item.IsDelete = userpermission.IsDelete;
                                            item.IsRead = userpermission.IsRead;
                                            item.IsEdit = userpermission.IsEdit;
                                            permissionItem.Add(item);
                                        }
                                        else
                                        {
                                            item.IsDelete = item.IsDelete;
                                            item.IsRead = item.IsRead;
                                            item.IsEdit = item.IsEdit;
                                            permissionItem.Add(item);
                                        }
                                    }

                                    else if (!permissionItem.Any(f => f.ItemClassificationID == access.ItemClassificationId) && item.ItemClassificationID == access.ItemClassificationId)
                                    {

                                        item.IsDelete = false;
                                        item.IsRead = false;
                                        item.IsEdit = false;
                                        permissionItem.Add(item);
                                    }

                                }
                                else if (!permissionItem.Any(f => f.ItemClassificationID == access.ItemClassificationId) && item.ItemClassificationID == access.ItemClassificationId && item.AddedByUserID == id)
                                {

                                    item.IsDelete = true;
                                    item.IsRead = true;
                                    item.IsEdit = true;
                                    permissionItem.Add(item);

                                }

                            });
                        }
                    });
                }
                else
                {

                    ItemClassification.ForEach(item =>
                    {
                        if (!permissionItem.Any(f => f.ItemClassificationID == item.ItemClassificationID) && item.ItemClassificationID == item.ItemClassificationID && item.AddedByUserID == id)
                        {
                            item.IsDelete = true;
                            item.IsRead = true;
                            item.IsEdit = true;
                            permissionItem.Add(item);
                        }
                    });

                }
            }
            return ItemClassification;
        }

        [HttpGet]
        [Route("GetItemClassificationForBMR")]
        public List<ItemClassificationModel> GetItemClassificationForBMR(int id)
        {
            var ItemClassification = new List<ItemClassificationModel>();
            var documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            var itemAccessRoleList = _context.ItemClassificationAccess.AsNoTracking().ToList();
            var userGroupList = _context.UserGroupUser.Where(f => f.UserId == id).AsNoTracking().ToList();
            var filterItem = _context.ItemClassificationMaster.Include("AddedByUser").Include(s => s.ClassificationType).Include(p => p.Profile).Where(t => t.StatusCodeId == 1).AsNoTracking().ToList();
            List<ItemClassificationModel> permissionItem = new List<ItemClassificationModel>();
            if (filterItem.Count > 0)
            {
                ItemClassification = filterItem.Select(s => new ItemClassificationModel
                {
                    ItemClassificationID = s.ItemClassificationId,
                    MainClassificationID = s.MainClassificationId,
                    ParentID = s.ParentId,
                    IsForm = s.IsForm,
                    FormId = s.FormId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    Name = s.Name,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    ScreenID = s.ClassificationType.CodeValue,
                    SessionId = s.SessionId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ClassificationTypeId = s.ClassificationTypeId,
                    ClassificationLinkType = s.ClassificationLinkType

                }).OrderByDescending(o => o.ItemClassificationID).Where(t => t.ParentID == null && t.ClassificationTypeId == 1503 && t.StatusCodeID == 1).ToList();

                if (itemAccessRoleList.Count > 0)
                {

                    ItemClassification.ForEach(item =>
                    {
                        var docuRoles = itemAccessRoleList.Where(f => f.ItemClassificationId == item.ItemClassificationID && item.AddedByUserID != id).ToList();
                        if (docuRoles.Count == 0)
                        {
                            if (!permissionItem.Any(f => f.ItemClassificationID == item.ItemClassificationID) && item.ItemClassificationID == item.ItemClassificationID && item.AddedByUserID == id)
                            {
                                item.IsDelete = true;
                                item.IsRead = true;
                                item.IsEdit = true;
                                permissionItem.Add(item);
                            }
                        }
                        else
                        {
                            docuRoles.ForEach(access =>
                            {
                                if (access.ItemClassificationId == item.ItemClassificationID && item.AddedByUserID != id)
                                {
                                    if (access.UserId == id)
                                    {
                                        var userpermission = documentpermission.Where(d => d.DocumentRoleId == access.RoleId).FirstOrDefault();
                                        if (userpermission != null)
                                        {
                                            item.RoleID = userpermission.DocumentRoleId;
                                            item.UserID = access.UserId;
                                            item.UserGroupID = access.UserGroupId;
                                            item.IsDelete = userpermission.IsDelete;
                                            item.IsRead = userpermission.IsRead;
                                            item.IsEdit = userpermission.IsEdit;
                                            permissionItem.Add(item);
                                        }
                                        else
                                        {
                                            item.IsDelete = item.IsDelete;
                                            item.IsRead = item.IsRead;
                                            item.IsEdit = item.IsEdit;
                                            permissionItem.Add(item);
                                        }
                                    }

                                    else if (!permissionItem.Any(f => f.ItemClassificationID == access.ItemClassificationId) && item.ItemClassificationID == access.ItemClassificationId)
                                    {

                                        item.IsDelete = false;
                                        item.IsRead = false;
                                        item.IsEdit = false;
                                        permissionItem.Add(item);
                                    }

                                }
                                else if (!permissionItem.Any(f => f.ItemClassificationID == access.ItemClassificationId) && item.ItemClassificationID == access.ItemClassificationId && item.AddedByUserID == id)
                                {

                                    item.IsDelete = true;
                                    item.IsRead = true;
                                    item.IsEdit = true;
                                    permissionItem.Add(item);

                                }

                            });
                        }
                    });
                }
                else
                {

                    ItemClassification.ForEach(item =>
                    {
                        if (!permissionItem.Any(f => f.ItemClassificationID == item.ItemClassificationID) && item.ItemClassificationID == item.ItemClassificationID && item.AddedByUserID == id)
                        {
                            item.IsDelete = true;
                            item.IsRead = true;
                            item.IsEdit = true;
                            permissionItem.Add(item);
                        }
                    });

                }
            }
            return ItemClassification;
        }
        private void CreateItemClassificationAccess(ItemClassificationModel value)
        {
            var itemClassificationAccess = new ItemClassificationAccess();
            var existingRole = _context.ItemClassificationAccess.Where(d => d.ItemClassificationId == value.ItemClassificationID).ToList();
            var userExistingRole = existingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = existingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();
            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            if (value.ItemClassificationID > 0)
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    if (userExistingRole == null)
                    {
                        if ((userList != null) && (userList.Count > 0))
                        {
                            userList.ForEach(u =>
                            {
                                var userExisting = _context.ItemClassificationAccess.Where(d => d.UserId == u && d.ItemClassificationId == value.ItemClassificationID).FirstOrDefault();
                                if (userExisting == null)
                                {
                                    itemClassificationAccess = new ItemClassificationAccess
                                    {
                                        UserId = u,
                                        RoleId = value.RoleID,
                                        UserGroupId = value.UserGroupID,
                                        ItemClassificationId = value.ItemClassificationID,
                                    };
                                    _context.ItemClassificationAccess.Add(itemClassificationAccess);
                                    _context.SaveChanges();
                                }
                            });

                        }
                    }
                    else
                    {
                        throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                    }
                }
                else if ((userGroupList != null) && (userGroupList.Count > 0))
                {
                    if (userGroupExistingRole == null)
                    {
                        userGroupList.ForEach(ug =>
                        {
                            List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                            currentGroupUsers.ForEach(c =>
                            {
                                var userExist = _context.ItemClassificationAccess.Where(du => du.ItemClassificationId == value.ItemClassificationID && du.UserId == c.UserId).ToList();
                                if (userExist.Count == 0)
                                {
                                    itemClassificationAccess = new ItemClassificationAccess
                                    {
                                        UserId = c.UserId,
                                        RoleId = value.RoleID,
                                        UserGroupId = ug,
                                        ItemClassificationId = value.ItemClassificationID,
                                    };
                                    _context.ItemClassificationAccess.Add(itemClassificationAccess);
                                    _context.SaveChanges();
                                }
                            });
                        });

                    }
                    else
                    {
                        throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                    }
                }

            }

        }
        [HttpPut]
        [Route("UpdateItemClassificationAccess")]
        public ItemClassificationAccessModel UpdateItemClassificationAccess(ItemClassificationAccessModel value)
        {
            var itemAccess = _context.ItemClassificationAccess.Where(i => i.ItemClassificationAccessId == value.ItemClassificationAccessID).FirstOrDefault();
            if (itemAccess != null)
            {
                itemAccess.ItemClassificationId = value.ItemClassificationID;
                itemAccess.RoleId = value.RoleID;
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("DeleteItemClassificationAccess")]
        public ItemClassificationAccessModel DeleteItemClassificationAccess(ItemClassificationAccessModel value)
        {

            var selectItemClassificationAccessIDs = value.ItemClassificationAccessIDs;
            if (selectItemClassificationAccessIDs.Count > 0)
            {
                selectItemClassificationAccessIDs.ForEach(sel =>
                {
                    var itemAccess = _context.ItemClassificationAccess.Where(p => p.ItemClassificationAccessId == sel).FirstOrDefault();
                    if (itemAccess != null)
                    {

                        var documentExistingUser = _context.ItemClassificationAccess.Where(p => p.UserId == itemAccess.UserId && p.ItemClassificationId == itemAccess.ItemClassificationId).ToList();
                        if (documentExistingUser.Count > 0 && documentExistingUser != null)
                        {
                            _context.ItemClassificationAccess.RemoveRange(documentExistingUser);
                        }
                        _context.ItemClassificationAccess.Remove(itemAccess);
                        _context.SaveChanges();
                    }
                });

                _context.SaveChanges();
            }


            return value;
        }
        [HttpPost]
        [Route("InsertItemClassificationPermission")]
        public ItemClassificationPermissionModel Post(ItemClassificationPermissionModel value)
        {
            var userGroupList = _context.UserGroupUser.ToList();
            if (value.UserType == "user-group")
            {
                var ItemClassificationPermission = _context.ItemClassificationPermission.Where(w => w.ItemClassificationId == value.ItemClassificationId && w.UserType == value.UserType && w.UserGroupId == value.UserGroupId).ToList();
                if (ItemClassificationPermission != null)
                {
                    _context.ItemClassificationPermission.RemoveRange(ItemClassificationPermission);
                    _context.SaveChanges();
                }
                if (value.UserGroupId != null)
                {
                    var userGroupLists = userGroupList.Where(w => w.UserGroupId == value.UserGroupId).ToList();
                    if (userGroupLists != null && userGroupLists.Count > 0)
                    {
                        userGroupLists.ForEach(s =>
                        {
                            if (value.SelectedPermissions.Count > 0)
                            {
                                value.SelectedPermissions.ForEach(p =>
                                {
                                    var itemClassificationPermissions = new ItemClassificationPermission
                                    {
                                        UserGroupId = s.UserGroupId,
                                        UserId = s.UserId,
                                        UserType = value.UserType,
                                        ItemClassificationId = value.ItemClassificationId,
                                        ApplicationCodeId = p,
                                    };
                                    _context.ItemClassificationPermission.Add(itemClassificationPermissions);
                                });
                            }
                            _context.SaveChanges();
                        });

                    }
                }
            }
            if (value.UserType == "user")
            {
                var ItemClassificationPermission = _context.ItemClassificationPermission.Where(w => w.ItemClassificationId == value.ItemClassificationId && w.UserType == value.UserType && w.UserId == value.UserId).ToList();
                if (ItemClassificationPermission != null)
                {
                    _context.ItemClassificationPermission.RemoveRange(ItemClassificationPermission);
                    _context.SaveChanges();
                }
                if (value.UserId != null)
                {
                    if (value.SelectedPermissions.Count > 0)
                    {
                        value.SelectedPermissions.ForEach(p =>
                        {
                            var itemClassificationPermissions = new ItemClassificationPermission
                            {
                                UserGroupId = userGroupList.Where(w => w.UserId == value.UserId).Select(s => s?.UserGroupId).FirstOrDefault(),
                                UserId = value.UserId,
                                UserType = value.UserType,
                                ItemClassificationId = value.ItemClassificationId,
                                ApplicationCodeId = p,
                            };
                            _context.ItemClassificationPermission.Add(itemClassificationPermissions);
                        });
                        _context.SaveChanges();
                    }
                }
            }
            return value;
        }

        [HttpGet]
        [Route("GetItemClassificationTreePaths")]
        public List<ItemClassificationModel> GetItemClassificationTreePaths()
        {
            List<ItemClassificationModel> ItemClassificationList = Get();
            List<ItemClassificationModel> itemClassification = new List<ItemClassificationModel>();
            List<ItemClassificationModel> itemClassificationModels = new List<ItemClassificationModel>();

            itemClassification = FindMenuPath(ItemClassificationList);
            var itemClassificationModellist = ItemClassificationList?.Distinct().ToList();
            if (itemClassificationModellist != null && itemClassificationModellist.Count > 0)
            {
                itemClassificationModellist.ForEach(p =>
                {
                    ItemClassificationModel applicationPermissionModel = new ItemClassificationModel();
                    applicationPermissionModel.Name = p.Name;
                    applicationPermissionModel.ItemClassificationID = p.ItemClassificationID;
                    applicationPermissionModel.ParentID = p.ParentID;

                    applicationPermissionModel.PathName = itemClassification?.Where(t => t.ItemClassificationID == p.ItemClassificationID).Select(t => t.PathName).SingleOrDefault();
                    itemClassificationModels.Add(applicationPermissionModel);
                });

            }
            return itemClassificationModels.ToList();
        }
        private List<ItemClassificationModel> FindMenuPath(List<ItemClassificationModel> permissionlist)
        {
            List<ItemClassificationModel> path = new List<ItemClassificationModel>();
            var emptypermissionIds = new List<long>();
            foreach (var fn in permissionlist)
            {
                ItemClassificationModel f = new ItemClassificationModel();

                if (fn.ParentID > 0)
                {
                    f.PathName = "";
                    f.ItemClassificationID = fn.ItemClassificationID;
                    f.Name = fn.Name;
                    f.ParentID = fn.ParentID;
                    f.PathNames.Add(fn.Name);
                    GenerateLocation(f, fn, permissionlist, emptypermissionIds);
                    f.PathNames.Reverse();
                    foreach (var item in f.PathNames)
                    {
                        if (f.PathNames[f.PathNames.Count - 1] != item)
                        {
                            f.PathName += item + " / ";
                        }
                        else
                        {
                            f.PathName += item;
                        }
                    }

                    path.Add(f);
                    //FindMenuPath(permissionlist);
                }
                else if (fn.ParentID == null)
                {
                    //f.FolderPath = fn.Name;
                    f.PathName = fn.Name;
                    f.ItemClassificationID = fn.ItemClassificationID;
                    f.Name = fn.Name;
                    f.ParentID = fn.ParentID;
                    path.Add(f);
                }





            }
            return path.Distinct().ToList();
        }

        private void GenerateLocation(ItemClassificationModel ItemClassificationModel, ItemClassificationModel parentClassification, List<ItemClassificationModel> ItemClassificationModels, List<long> emptyIds)
        {
            //if (!emptyIds.Contains(parentPermission.PermissionID))
            //{
            //    emptyIds.Add(parentPermission.PermissionID);
            parentClassification = ItemClassificationModels.FirstOrDefault(s => s.ItemClassificationID == parentClassification.ParentID);

            if (parentClassification != null)
            {
                ItemClassificationModel.PathNames.Add(parentClassification.Name);
                if (parentClassification.ParentID != null)
                {
                    GenerateLocation(ItemClassificationModel, parentClassification, ItemClassificationModels, emptyIds);
                }
            }
            //}

        }
    }
}
