﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DepartmentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DepartmentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDepartments")]
        public List<DepartmentModel> Get()
        {
            var department = _context.Department.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.Company).Include(h => h.Hod).Include(d => d.Division).Include(dc => dc.Division.Company).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(s =>
            {
                DepartmentModel departmentModels = new DepartmentModel
                {
                    DepartmentId = s.DepartmentId,
                    Hodid = s.Hodid,
                    CompanyId = s.CompanyId,
                    CompanyName = s.Division != null && s.Division.Company != null ? s.Division.Company.Description : "",
                    HodName = s.Hod?.FirstName,
                    Name = s.Name,
                    Code = s.Code,
                    DivisionID = s.DivisionId,
                    Description = s.Description,
                    HeadCount = s.HeadCount,
                    DivisionName = s.Division != null ? s.Division.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileCode = s.ProfileCode,
                    CompanyDivisionName = (s.Division != null && s.Division.Company != null ? s.Division.Company.Description : "") + " | " + (s.Division != null ? s.Division.Name : "") + " | " + s.Name,

                };
                departmentModel.Add(departmentModels);
            });
            return departmentModel.OrderByDescending(a => a.DepartmentId).ToList();
        }
        [HttpGet]
        [Route("GetDepartmentsByStatus")]
        public List<DepartmentModel> GetDepartmentsByStatus()
        {
            var department = _context.Department.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.Company).Include(h => h.Hod).Include(d => d.Division).Include(dc => dc.Division.Company).Where(s => s.StatusCodeId != 2).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(s =>
            {
                DepartmentModel departmentModels = new DepartmentModel
                {
                    DepartmentId = s.DepartmentId,
                    Hodid = s.Hodid,
                    CompanyId = s.CompanyId,
                    CompanyName = s.Division != null && s.Division.Company != null ? s.Division.Company.Description : "",
                    HodName = s.Hod?.FirstName,
                    Name = s.Name,
                    Code = s.Code,
                    DivisionID = s.DivisionId,
                    Description = s.Description,
                    HeadCount = s.HeadCount,
                    DivisionName = s.Division != null ? s.Division.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileCode = s.ProfileCode,
                    CompanyDivisionName = (s.Division != null && s.Division.Company != null ? s.Division.Company.Description : "") + " | " + (s.Division != null ? s.Division.Name : "") + " | " + s.Name,
                    DepartmentCompanyName = (s.Division != null && s.Division.Company != null ? s.Division.Company.PlantCode : "") + " | " + (s.Division != null ? s.Division.Name : "") + " | " + s.Name,
                };
                departmentModel.Add(departmentModels);
            });
            return departmentModel.OrderByDescending(a => a.DepartmentId).ToList();
        }

        [HttpGet]
        [Route("GetActiveDepartments")]
        public List<CodeMasterModel> GetActiveDepartments()
        {
            List<long> departmentIds = new List<long> { 8, 9, 10, 12 };
            var department = _context.Department.Where(s => s.StatusCodeId != 2 && s.Description != string.Empty && departmentIds.Contains(s.DepartmentId)).AsNoTracking().ToList();
            List<CodeMasterModel> departmentModel = new List<CodeMasterModel>();
            department.ForEach(s =>
            {
                CodeMasterModel departmentModels = new CodeMasterModel
                {
                    Id = s.DepartmentId,
                    CodeDescription = s.Description,
                    CodeValue = s.Code,
                };
                departmentModel.Add(departmentModels);
            });
            return departmentModel.OrderByDescending(a => a.Id).ToList();
        }
        [HttpGet]
        [Route("GetDepartmentsByDivisionStatus")]
        public List<DepartmentModel> GetDepartmentsByDivisionStatus(long? id)
        {
            var department = _context.Division.Include(d => d.Department).Where(w => w.StatusCodeId != 2 && w.CompanyId == id).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(d =>
            {
                if (d.Department != null)
                {
                    d.Department.Where(a => a.StatusCodeId != 2).ToList().ForEach(s =>
                         {
                             DepartmentModel departmentModels = new DepartmentModel
                             {
                                 DepartmentId = s.DepartmentId,
                                 Name = s.Name,
                                 Code = s.Code,
                                 StatusCodeID = s.StatusCodeId,
                             };
                             departmentModel.Add(departmentModels);
                         });
                }
            });
            return departmentModel.OrderBy(a => a.Name).ToList();
        }
        [HttpPost()]
        [Route("GetDepartmentsByPlantString")]
        public List<DepartmentModel> GetDepartmentsByPlantString(SearchModel searchModel)
        {
            long? id = new long?();
            if (searchModel.SearchString.Contains("Sdn Phd"))
            {
                id = 1;
            }
            else if (searchModel.SearchString.Contains("Pte Ltd"))
            {
                id = 2;
            }
            var department = _context.Department.Where(w => w.StatusCodeId != 2 && w.CompanyId == id).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(s =>
            {
                DepartmentModel departmentModels = new DepartmentModel
                {
                    DepartmentId = s.DepartmentId,
                    Name = s.Name,
                    Code = s.Code,
                    StatusCodeID = s.StatusCodeId,
                };
                departmentModel.Add(departmentModels);

            });
            return departmentModel.OrderBy(a => a.Name).ToList();
        }
        [HttpGet]
        [Route("GetDepartmentsByPlant")]
        public List<DepartmentModel> GetDepartmentsByPlant(long? id)
        {
            var department = _context.Department.Where(w => w.StatusCodeId != 2 && w.CompanyId == id).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(s =>
            {
                DepartmentModel departmentModels = new DepartmentModel
                {
                    DepartmentId = s.DepartmentId,
                    Name = s.Name,
                    Code = s.Code,
                    StatusCodeID = s.StatusCodeId,
                };
                departmentModel.Add(departmentModels);

            });
            return departmentModel.OrderBy(a => a.Name).ToList();
        }
        [HttpGet]
        [Route("GetDepartmentByDivision")]
        public List<DepartmentModel> GetDepartmentByDivision(int id)
        {
            var department = _context.Department.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.Company).Where(o => o.DivisionId == id).AsNoTracking().ToList();
            List<DepartmentModel> departmentModel = new List<DepartmentModel>();
            department.ForEach(s =>
            {
                DepartmentModel departmentModels = new DepartmentModel
                {
                    DepartmentId = s.DepartmentId,
                    Hodid = s.Hodid,
                    CompanyId = s.CompanyId,
                    CompanyNamee = s.Company != null ? s.Company.Description : "",
                    HodName = s.Hod?.FirstName,
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    HeadCount = s.HeadCount,
                    DivisionID = s.DivisionId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ProfileCode = s.ProfileCode,
                    ModifiedDate = s.ModifiedDate
                };
                departmentModel.Add(departmentModels);
            });
            return departmentModel.OrderByDescending(a => a.DepartmentId).ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Department")]
        [HttpGet("GetDepartments/{id:int}")]
        public ActionResult<DepartmentModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var department = _context.Department.SingleOrDefault(p => p.DepartmentId == id.Value);
            var result = _mapper.Map<DepartmentModel>(department);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DepartmentModel> GetData(SearchModel searchModel)
        {
            var department = new Department();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).FirstOrDefault();
                        break;
                    case "Last":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).LastOrDefault();
                        break;
                    case "Next":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).LastOrDefault();
                        break;
                    case "Previous":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).FirstOrDefault();
                        break;
                    case "Last":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).LastOrDefault();
                        break;
                    case "Next":
                        department = _context.Department.OrderBy(o => o.DepartmentId).FirstOrDefault(s => s.DepartmentId > searchModel.Id);
                        break;
                    case "Previous":
                        department = _context.Department.OrderByDescending(o => o.DepartmentId).FirstOrDefault(s => s.DepartmentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DepartmentModel>(department);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDepartment")]
        public DepartmentModel Post(DepartmentModel value)
        {
            var department = new Department
            {
                CompanyId = value.CompanyId,
                Hodid = value.Hodid,
                Description = value.Description,
                HeadCount = value.HeadCount,
                Name = value.Name,
                Code = value.Code,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                DivisionId = value.DivisionID,
                ProfileCode = value.ProfileCode,
            };
            _context.Department.Add(department);
            _context.SaveChanges();
            value.DepartmentId = department.DepartmentId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDepartment")]
        public DepartmentModel Put(DepartmentModel value)
        {
            var department = _context.Department.SingleOrDefault(p => p.DepartmentId == value.DepartmentId);

            department.CompanyId = value.CompanyId;
            department.Hodid = value.Hodid;
            department.Description = value.Description;
            department.HeadCount = value.HeadCount;
            department.Name = value.Name;
            department.Code = value.Code;
            department.ModifiedByUserId = value.ModifiedByUserID;
            department.ModifiedDate = DateTime.Now;
            department.StatusCodeId = value.StatusCodeID.Value;
            department.DivisionId = value.DivisionID;
            department.ProfileCode = value.ProfileCode;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDepartment")]
        public void Delete(int id)
        {
            try
            {
                var department = _context.Department.SingleOrDefault(p => p.DepartmentId == id);
                if (department != null)
                {
                    _context.Department.Remove(department);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Department Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}