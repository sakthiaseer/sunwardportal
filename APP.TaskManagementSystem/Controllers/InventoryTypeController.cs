﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class InventoryTypeController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public InventoryTypeController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetInventoryType")]
        public List<InventoryTypeModel> Get()
        {

            List<InventoryTypeModel> inventoryTypeModels = new List<InventoryTypeModel>();
            InventoryTypeModel inventoryTypeModel = new InventoryTypeModel();
            var InventoryType = _context.InventoryType.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Include(s => s.Company).Include(o => o.Owner).Include(i => i.ItemSource).OrderByDescending(o => o.InventoryTypeId).AsNoTracking().ToList();
            if (InventoryType != null && InventoryType.Count > 0)
            {
                InventoryType.ForEach(s =>
                {
                    inventoryTypeModel = new InventoryTypeModel();
                    inventoryTypeModel.InventoryTypeId = s.InventoryTypeId;
                    inventoryTypeModel.OwnerId = s.OwnerId;
                    inventoryTypeModel.CompanyId = s.CompanyId;
                    inventoryTypeModel.Owner = s.Owner?.Name;
                    inventoryTypeModel.Name = s.Name;
                    inventoryTypeModel.Description = s.Description;
                    inventoryTypeModel.ItemSourceId = s.ItemSourceId;
                    inventoryTypeModel.SourceTreeId = s.SourceTreeId;
                    inventoryTypeModel.ItemSource = s.ItemSource?.CodeValue;
                    inventoryTypeModel.AddedByUser = s.AddedByUser?.UserName;
                    inventoryTypeModel.CompanyName = s.Company?.Description;
                    inventoryTypeModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    inventoryTypeModel.StatusCode = s.StatusCode?.CodeValue;
                    inventoryTypeModel.AddedByUserID = s.AddedByUserId;
                    inventoryTypeModel.ModifiedByUserID = s.ModifiedByUserId;
                    inventoryTypeModel.StatusCodeID = s.StatusCodeId;
                    inventoryTypeModel.AddedDate = s.AddedDate;
                    inventoryTypeModel.ModifiedDate = s.ModifiedDate;
                    inventoryTypeModels.Add(inventoryTypeModel);
                });

            }
            return inventoryTypeModels;
        }

        [HttpGet]
        [Route("GetInventoryTypeDropDown")]
        public List<InventoryTypeModel> GetInventoryTypeDropDown()
        {

            List<InventoryTypeModel> inventoryTypeModels = new List<InventoryTypeModel>();
            InventoryTypeModel inventoryTypeModel = new InventoryTypeModel();
            var InventoryType = _context.InventoryType.Include(s => s.Company).Include(o => o.Owner).OrderByDescending(o => o.InventoryTypeId).AsNoTracking().ToList();
            if (InventoryType != null && InventoryType.Count > 0)
            {
                InventoryType.ForEach(s =>
                {
                    string companyName = string.Empty;
                    inventoryTypeModel = new InventoryTypeModel();
                    inventoryTypeModel.InventoryTypeId = s.InventoryTypeId;
                    inventoryTypeModel.OwnerId = s.OwnerId;
                    inventoryTypeModel.CompanyId = s.CompanyId;
                    inventoryTypeModel.Owner = s.Owner?.Name;
                    inventoryTypeModel.Name = s.Name;
                    inventoryTypeModel.Description = s.Description;
                    inventoryTypeModel.ItemSourceId = s.ItemSourceId;
                    inventoryTypeModel.SourceTreeId = s.SourceTreeId;
                    inventoryTypeModel.CompanyName = s.Company?.Description;
                    if (!string.IsNullOrEmpty(inventoryTypeModel.CompanyName))
                    {
                        companyName = Regex.Replace(s.Company.Description, @"[^0-9a-zA-Z]+", "").ToLower();
                    }
                    inventoryTypeModel.CountryId = s.Company != null ? companyName.Contains("sdnbhd") ? 1 : companyName.Contains("pteltd") ? 2 : new long() : new long();
                    inventoryTypeModel.DropDownName = s.Company?.Description + " | " + s.Owner?.Name + " | " + s.Name;
                    inventoryTypeModels.Add(inventoryTypeModel);
                });

            }
            return inventoryTypeModels;
        }

        [HttpGet]
        [Route("GetInventoryTypeDisposalItem")]
        public List<InventoryTypeModel> GetInventoryTypeDisposalItem()
        {

            List<InventoryTypeModel> inventoryTypeModels = new List<InventoryTypeModel>();
            InventoryTypeModel inventoryTypeModel = new InventoryTypeModel();
            var InventoryType = _context.InventoryType.Include(s => s.Company).Include(o => o.Owner).OrderByDescending(o => o.InventoryTypeId).AsNoTracking().ToList();
            if (InventoryType != null && InventoryType.Count > 0)
            {
                InventoryType.ForEach(s =>
                {
                    string companyName = string.Empty;
                    inventoryTypeModel = new InventoryTypeModel();
                    inventoryTypeModel.InventoryTypeId = s.InventoryTypeId;
                    inventoryTypeModel.OwnerId = s.OwnerId;
                    inventoryTypeModel.CompanyId = s.CompanyId;
                    inventoryTypeModel.Owner = s.Owner?.Name;
                    inventoryTypeModel.Name = s.Name;
                    inventoryTypeModel.Description = s.Description;
                    inventoryTypeModel.ItemSourceId = s.ItemSourceId;
                    inventoryTypeModel.SourceTreeId = s.SourceTreeId;
                    inventoryTypeModel.CompanyName = s.Company?.Description;
                    if (!string.IsNullOrEmpty(inventoryTypeModel.CompanyName))
                    {
                        companyName = Regex.Replace(s.Company.Description, @"[^0-9a-zA-Z]+", "").ToLower();
                    }
                    inventoryTypeModel.CountryId = s.Company != null ? companyName.Contains("sdnbhd") ? 1 : companyName.Contains("pteltd") ? 2 : new long() : new long();
                    inventoryTypeModel.DropDownName = s.Company?.Description + " | " + s.Name;
                    inventoryTypeModels.Add(inventoryTypeModel);
                });

            }
            return inventoryTypeModels;
        }
        [HttpGet]
        [Route("GetInventoryTypeForMovedStock")]
        public List<InventoryTypeModel> GetInventoryTypeForMovedStock()
        {

            List<InventoryTypeModel> inventoryTypeModels = new List<InventoryTypeModel>();
            InventoryTypeModel inventoryTypeModel = new InventoryTypeModel();
            var InventoryType = _context.InventoryType.Include(s => s.Company).Include(o => o.Owner).OrderByDescending(o => o.InventoryTypeId).AsNoTracking().ToList();
            if (InventoryType != null && InventoryType.Count > 0)
            {
                InventoryType.ForEach(s =>
                {
                    inventoryTypeModel = new InventoryTypeModel();
                    inventoryTypeModel.InventoryTypeId = s.InventoryTypeId;
                    inventoryTypeModel.OwnerId = s.OwnerId;
                    inventoryTypeModel.CompanyId = s.CompanyId;
                    inventoryTypeModel.Owner = s.Owner?.Name;
                    inventoryTypeModel.Name = s.Name;
                    inventoryTypeModel.Description = s.Description;
                    inventoryTypeModel.ItemSourceId = s.ItemSourceId;
                    inventoryTypeModel.SourceTreeId = s.SourceTreeId;
                    inventoryTypeModel.CompanyName = s.Company?.Description;
                    inventoryTypeModel.DropDownName = s.Company?.Description + " | " + s.Name;
                    inventoryTypeModels.Add(inventoryTypeModel);
                });

            }
            return inventoryTypeModels;
        }
        [HttpPost()]
        [Route("GetInventoryTypeData")]
        public ActionResult<InventoryTypeModel> GetData(SearchModel searchModel)
        {
            var InventoryType = new InventoryType();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).LastOrDefault();
                        break;
                    case "Next":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).LastOrDefault();
                        break;
                    case "Previous":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).LastOrDefault();
                        break;
                    case "Next":
                        InventoryType = _context.InventoryType.OrderBy(o => o.InventoryTypeId).FirstOrDefault(s => s.InventoryTypeId > searchModel.Id);
                        break;
                    case "Previous":
                        InventoryType = _context.InventoryType.OrderByDescending(o => o.InventoryTypeId).FirstOrDefault(s => s.InventoryTypeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<InventoryTypeModel>(InventoryType);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertInventoryType")]
        public InventoryTypeModel Post(InventoryTypeModel value)
        {

            var inventoryType = new InventoryType
            {
                CompanyId = value.CompanyId,
                OwnerId = value.OwnerId,
                Name = value.Name,
                Description = value.Description,
                ItemSourceId = value.ItemSourceId,
                SourceTreeId = value.SourceTreeId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.InventoryType.Add(inventoryType);
            value.AddedDate = inventoryType.AddedDate;
            if (value.CompanyId > 0)
            {
                value.CompanyName = _context.Plant.Where(a => a.PlantId == value.CompanyId)?.FirstOrDefault()?.Description;
            }
            if (value.OwnerId > 0)
            {
                value.Owner = _context.Department.Where(d => d.DepartmentId == value.OwnerId)?.FirstOrDefault()?.Name;
            }
            if (value.ItemSourceId > 0)
            {
                value.ItemSource = _context.CodeMaster.Where(c => c.CodeId == value.ItemSourceId)?.FirstOrDefault()?.CodeValue;
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
            }
            _context.SaveChanges();
            value.InventoryTypeId = inventoryType.InventoryTypeId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateInventoryType")]
        public InventoryTypeModel Put(InventoryTypeModel value)
        {

            var inventoryType = _context.InventoryType.SingleOrDefault(p => p.InventoryTypeId == value.InventoryTypeId);
            if (inventoryType != null)
            {


                inventoryType.ModifiedByUserId = value.ModifiedByUserID;
                inventoryType.ModifiedDate = DateTime.Now;
                inventoryType.StatusCodeId = value.StatusCodeID.Value;
                inventoryType.CompanyId = value.CompanyId;
                inventoryType.OwnerId = value.OwnerId;
                inventoryType.Name = value.Name;
                inventoryType.Description = value.Description;
                inventoryType.ItemSourceId = value.ItemSourceId;
                inventoryType.SourceTreeId = value.SourceTreeId;
                inventoryType.StatusCodeId = value.StatusCodeID.Value;

            }
            _context.SaveChanges();
            value.ModifiedDate = inventoryType.ModifiedDate;
            if (value.CompanyId > 0)
            {
                value.CompanyName = _context.Plant.Where(a => a.PlantId == value.CompanyId)?.FirstOrDefault()?.Description;
            }
            if (value.OwnerId > 0)
            {
                value.Owner = _context.Department.Where(d => d.DepartmentId == value.OwnerId)?.FirstOrDefault()?.Name;
            }
            if (value.ItemSourceId > 0)
            {
                value.ItemSource = _context.CodeMaster.Where(c => c.CodeId == value.ItemSourceId)?.FirstOrDefault()?.CodeValue;
            }
            if (value.ModifiedByUserID > 0)
            {
                value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == value.ModifiedByUserID)?.FirstOrDefault().UserName;
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInventoryType")]
        public void Delete(int id)
        {
            var InventoryType = _context.InventoryType.SingleOrDefault(p => p.InventoryTypeId == id);
            if (InventoryType != null)
            {
                _context.InventoryType.Remove(InventoryType);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("InsertPortalNavItem")]
        public PortalNavItemsModel InsertPortalNavItem(PortalNavItemsModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "NavisionCompany" });
            var openingStockId = _context.InventoryTypeOpeningStockBalance.Where(s => s.InventoryTypeId == value.InventoryTypeId).FirstOrDefault()?.OpeningBalanceId;
            var navItem = new Navitems
            {
                No = profileNo,
                BaseUnitofMeasure = value.UOM,
                Description = value.Description,
                Description2 = value.Description2,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ItemCategoryCode = value.ItemCategoryCode,
                CompanyId = value.CompanyId,
                IsPortal = true,
            };
            _context.Navitems.Add(navItem);
            _context.SaveChanges();
            value.No = profileNo;
            value.ItemId = navItem.ItemId;
            var sessionId = Guid.NewGuid();

            var inventoryTypeOpeningStockBalanceLine = new InventoryTypeOpeningStockBalanceLine
            {
                OpeningStockId = openingStockId,
                ItemId = value.ItemId,
                QtyOnHand = value.QuantityOnHand,
                BatchNo = value.BatchNo,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,
            };
            _context.InventoryTypeOpeningStockBalanceLine.Add(inventoryTypeOpeningStockBalanceLine);
            _context.SaveChanges();
            if (value.ItemId != null && value.BatchNo != null)
            {
                var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
                if (itemBatchinfo != null)
                {
                    itemBatchinfo.QuantityOnHand = itemBatchinfo.QuantityOnHand + value.QuantityOnHand;
                    _context.SaveChanges();
                }
                else if (itemBatchinfo == null)
                {
                    var itembatchinfo = new ItemBatchInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QuantityOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        BatchNo = value.BatchNo,
                        ManufacturingDate = value.ManufacturingDate,
                        ExpiryDate = value.ExpiryDate,
                        CompanyId = value.CompanyId,
                    };
                    _context.ItemBatchInfo.Add(itembatchinfo);
                    _context.SaveChanges();
                }
                var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();

                if (itemStockinfo == null)
                {
                    var itemstockinfo = new ItemStockInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QuantityOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                    };
                    _context.ItemStockInfo.Add(itemstockinfo);

                }
                else if (itemStockinfo != null)
                {
                    itemStockinfo.QuantityOnHand = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId).Sum(t => t.QuantityOnHand);
                }

                _context.SaveChanges();
            }
            return value;
        }
        [HttpGet]
        [Route("GetInventoryTypeByItem")]
        public List<NavItemModel> GetInventoryTypeByItem(long? id)
        {
            List<NavItemModel> NavItemModels = new List<NavItemModel>();
            List<long?> itemIds = _context.InventoryTypeOpeningStockBalanceLine.Include(a => a.OpeningStock).AsNoTracking().Where(w => w.OpeningStock.InventoryTypeId == id && w.ItemId != null).Select(s => s.ItemId).Distinct().ToList();
            itemIds.AddRange(_context.IssueRequestSampleLine.Include(a => a.SampleRequestLine).Include(s => s.SampleRequestLine.SampleRequestForm).AsNoTracking().Where(w => w.SampleRequestLine.SampleRequestForm.InventoryTypeId == id && w.SampleRequestLine.ItemId != null).Select(s => s.SampleRequestLine.ItemId).Distinct().ToList());
            var itemId = itemIds.Distinct().ToList();
            var items = _context.Navitems.Where(w => itemId.Contains(w.ItemId)).ToList();
            items.ForEach(s =>
            {
                NavItemModel NavItemModel = new NavItemModel();
                NavItemModel.ItemId = s.ItemId;
                NavItemModel.No = s.No;
                NavItemModel.Description = s.Description;
                NavItemModel.ItemNoDetailList = s.No + "|" + s.Description;
                NavItemModels.Add(NavItemModel);
            });
            return NavItemModels;
        }

        [HttpGet]
        [Route("GetItemStockInfoByInventoryType")]
        public List<ItemStockInfoModel> GetItemStockInfoByInventoryType(long? id)
        {
            List<ItemStockInfoModel> itemStockInfoModels = new List<ItemStockInfoModel>();
            List<long?> itemIds = _context.InventoryTypeOpeningStockBalanceLine.Include(a => a.OpeningStock).AsNoTracking().Where(w => w.OpeningStock.InventoryTypeId == id && w.ItemId != null).Select(s => s.ItemId).Distinct().ToList();
            itemIds.AddRange(_context.IssueRequestSampleLine.Include(a => a.SampleRequestLine).Include(s => s.SampleRequestLine.SampleRequestForm).AsNoTracking().Where(w => w.SampleRequestLine.SampleRequestForm.InventoryTypeId == id && w.SampleRequestLine.ItemId != null).Select(s => s.SampleRequestLine.ItemId).Distinct().ToList());
            var itemId = itemIds.Distinct().ToList();
            var items = _context.ItemStockInfo.Include(e => e.Item).Where(w => itemId.Contains(w.ItemId)).ToList();
            items.ForEach(s =>
            {
                ItemStockInfoModel itemStockInfoModel = new ItemStockInfoModel();
                itemStockInfoModel.ItemId = s.ItemId;
                itemStockInfoModel.ItemNo = s.Item?.No;
                itemStockInfoModel.Description = s.Item?.Description;
                itemStockInfoModel.BatchNo = s.Item?.BatchNos;
                itemStockInfoModel.BUOM=s.Item?.BaseUnitofMeasure;
                itemStockInfoModel.QuantityOnHand = s.QuantityOnHand;
                itemStockInfoModels.Add(itemStockInfoModel);
            });
            return itemStockInfoModels;
        }

        [HttpPost]
        [Route("GetStockMovement")]
        public List<StockMovementModel> GetStockMovement(SearchModel searchModel)
        {
            List<StockMovementModel> stockMovementModels = new List<StockMovementModel>();
            var opening = _context.InventoryTypeOpeningStockBalanceLine.Include(a => a.Item).Include(a => a.OpeningStock).AsNoTracking().Where(w => w.OpeningStock.InventoryTypeId == searchModel.Id && w.ItemId == searchModel.MasterTypeID).ToList();
            var stockIn = _context.IssueRequestSampleLine.Include(a => a.SampleRequestLine).Include(a => a.SampleRequestLine.Item).Include(a => a.SampleRequestLine.SampleRequestForm).AsNoTracking().Where(w => w.SampleRequestLine.SampleRequestForm.InventoryTypeId == searchModel.Id && w.SampleRequestLine.ItemId == searchModel.MasterTypeID).Distinct().ToList();
            var stockOut = _context.SampleRequestForDoctor.Include(a => a.Item).Include(a => a.SampleRequestForDoctorHeader).AsNoTracking().Where(w => w.SampleRequestForDoctorHeader.InventoryTypeId == searchModel.Id && w.ItemId == searchModel.MasterTypeID).Distinct().ToList();
            opening.ForEach(s =>
            {
                StockMovementModel stockMovementModel = new StockMovementModel();
                stockMovementModel.BatchNo = s.BatchNo;
                stockMovementModel.Increase = s.QtyOnHand;
                stockMovementModel.Decrease = 0;
                stockMovementModel.Date = s.AddedDate;
                stockMovementModel.Uom = s.Item?.BaseUnitofMeasure;
                stockMovementModels.Add(stockMovementModel);
            });
            stockIn.ForEach(s =>
            {
                StockMovementModel stockMovementModel = new StockMovementModel();
                stockMovementModel.DocNo = s.SampleRequestLine?.SampleRequestForm?.ProfileNo;
                stockMovementModel.BatchNo = s.BatchNo;
                stockMovementModel.Increase = s.IssueQuantity;
                stockMovementModel.Decrease = 0;
                stockMovementModel.Manufacture = s.SampleRequestLine?.MfgDate;
                stockMovementModel.ExpDate = s.SampleRequestLine?.ExpiryDate;
                stockMovementModel.Date = s.AddedDate;
                stockMovementModel.Uom = s.SampleRequestLine?.Item?.BaseUnitofMeasure;
                stockMovementModels.Add(stockMovementModel);
            });
            stockOut.ForEach(s =>
            {
                StockMovementModel stockMovementModel = new StockMovementModel();
                stockMovementModel.DocNo = s.SampleRequestForDoctorHeader?.SampleChitNo;
                stockMovementModel.BatchNo = s.BatchNo;
                stockMovementModel.Decrease = s.InStockQty;
                stockMovementModel.Increase = 0;
                stockMovementModel.Manufacture = s.MfgDate;
                stockMovementModel.ExpDate = s.ExpDate;
                stockMovementModel.Date = s.AddedDate;
                stockMovementModel.Uom = s.Item?.BaseUnitofMeasure;
                stockMovementModels.Add(stockMovementModel);
            });
            var stockMovementData = stockMovementModels.Where(w => w.Date != null).OrderBy(o => o.Date).ToList();
            List<StockMovementModel> stockMovements = new List<StockMovementModel>();
            int a = 0;
            decimal? balance = 0;
            stockMovementData.ForEach(s =>
            {
                if (a == 0)
                {
                    balance = s.Increase > 0 ? s.Increase : s.Decrease;
                }
                else
                {
                    if (s.Increase > 0)
                    {
                        balance += s.Increase;
                    }
                    if (s.Decrease > 0)
                    {
                        balance -= s.Decrease;
                    }
                }
                StockMovementModel stockMovement = new StockMovementModel();
                stockMovement.Date = s.Date;
                stockMovement.DocNo = s.DocNo;
                stockMovement.BatchNo = s.BatchNo;
                stockMovement.Manufacture = s.Manufacture;
                stockMovement.ExpDate = s.ExpDate;
                stockMovement.Increase = s.Increase;
                stockMovement.Decrease = s.Decrease;
                stockMovement.Uom = s.Uom;
                stockMovement.Balance = balance;
                stockMovements.Add(stockMovement);
                a++;
            });
            return stockMovements;
        }
    }
}
