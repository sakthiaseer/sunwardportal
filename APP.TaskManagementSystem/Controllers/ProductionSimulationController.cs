﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionSimulationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IHubContext<ChatHub> _hub;

        private readonly IBackgroundJobClient _backgroundJobClient;
        public ProductionSimulationController(CRT_TMSContext context, IMapper mapper,
            IConfiguration config, IHubContext<ChatHub> hub,
            IBackgroundJobClient backgroundJobClient)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hub = hub;

            _backgroundJobClient = backgroundJobClient;
        }
        [HttpGet]
        [Route("GetProductionSimulation")]
        public List<ProductionSimulationModel> Get()
        {
            DateTime now = DateTime.Now;
            DateTime firstDay = new DateTime(now.Year, now.Month, 1);
            DateTime lastDay = firstDay.AddMonths(7).AddDays(-1);
            List<ProductionSimulationModel> productionSimulationModels = new List<ProductionSimulationModel>();
            var productionSimulation = _context.ProductionSimulation
               .Include("Item").OrderByDescending(o => o.ProductionSimulationId).Where(f => (f.StartingDate >= firstDay && f.StartingDate <= lastDay) && (f.IsBmrticket == false || f.IsBmrticket == null)).AsNoTracking().ToList();
            if (productionSimulation != null && productionSimulation.Count > 0)
            {
                productionSimulation.ForEach(s =>
                {
                    ProductionSimulationModel productionSimulationModel = new ProductionSimulationModel
                    {
                        ProductionSimulationId = s.ProductionSimulationId,
                        ProdOrderNo = s.ProdOrderNo,
                        ItemId = s.ItemId,
                        ItemNo = s.ItemNo,
                        Description = s.Description,
                        PackSize = s.PackSize,
                        Quantity = s.Quantity,

                        PlannedQuantity = s.PlannedQty.GetValueOrDefault(0),
                        Uom = s.Uom,
                        PerQuantity = s.PerQuantity,
                        PerQtyUom = s.PerQtyUom,
                        BatchNo = s.BatchNo,
                        StartingDate = s.StartingDate,
                        ItemName = s.Item != null ? s.Item.Description : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsOutput = s.IsOutput.GetValueOrDefault(false),
                        Dispense = s.Dispense

                    };
                    productionSimulationModels.Add(productionSimulationModel);

                });

            }

            return productionSimulationModels;
        }
        [HttpGet]
        [Route("RefreshProductionSimulation")]
        public async Task<List<ProductionSimulationModel>> RefreshProductionSimulation(int Id)
        {
            DateTime now = DateTime.Now;
            DateTime firstDay = new DateTime(now.Year, now.Month, 1);
            DateTime lastDay = firstDay.AddMonths(7).AddDays(-1);

            NAVFuncController navFunc = new NAVFuncController(_context, _mapper, _config, _hub, _backgroundJobClient);
            var company = "NAV_JB";
            if (Id == 2)
            {
                company = "NAV_SG";
            }
            var deleteexisting = _context.ProductionSimulation.Where(f => f.CompanyId == Id && (f.IsBmrticket == false || f.IsBmrticket == null)).AsNoTracking().ToList();
            if (deleteexisting.Count > 0)
            {
                _context.ProductionSimulation.RemoveRange(deleteexisting);
                _context.SaveChanges();
            }

            await navFunc.GetProdPlanningLine(company);
            await navFunc.GetProdOrderOutputLine(company);
            await navFunc.GetProdGroupLine(company);
            await navFunc.GetProdNotStart(company);

            List<ProductionSimulationModel> productionSimulationModels = new List<ProductionSimulationModel>();
            var productionSimulation = _context.ProductionSimulation

               .Include("Item").OrderByDescending(o => o.ProductionSimulationId).Where(f => f.StartingDate >= firstDay && f.StartingDate <= lastDay && (f.IsBmrticket == false || f.IsBmrticket == null)).AsNoTracking().ToList();
            if (productionSimulation != null && productionSimulation.Count > 0)
            {
                productionSimulation.ForEach(s =>
                {
                    ProductionSimulationModel productionSimulationModel = new ProductionSimulationModel
                    {

                        ProductionSimulationId = s.ProductionSimulationId,
                        ProdOrderNo = s.ProdOrderNo,
                        ItemId = s.ItemId,
                        ItemNo = s.ItemNo,
                        Description = s.Description,
                        PackSize = s.PackSize,
                        Quantity = s.Quantity,
                        PlannedQuantity = s.PlannedQty.GetValueOrDefault(0),
                        Uom = s.Uom,
                        PerQuantity = s.PerQuantity,
                        PerQtyUom = s.PerQtyUom,
                        BatchNo = s.BatchNo,
                        StartingDate = s.StartingDate,
                        ItemName = s.Item.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsOutput = s.IsOutput.GetValueOrDefault(false),
                        Dispense = s.Dispense,
                    };
                    productionSimulationModels.Add(productionSimulationModel);
                });
            }

            return productionSimulationModels;
        }

        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult Upload(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            //_context.SaveChanges();
            return Content(SessionId.ToString());
        }

        [HttpPost]
        [Route("InsertProductionSimulation")]
        public ProductionSimulationModel InsertProductionSimulation(ProductionSimulationModel value)
        {
            var ProductionSimulation = new ProductionSimulation
            {
                ProdOrderNo = value.ProdOrderNo,
                ItemId = value.ItemId,
                ItemNo = value.ItemNo,
                Description = value.Description,
                PackSize = value.PackSize,
                Quantity = value.Quantity,
                Uom = value.Uom,
                PerQtyUom = value.PerQtyUom,
                PerQuantity = value.PerQuantity,
                BatchNo = value.BatchNo,
                StartingDate = value.StartingDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                Dispense = value.Dispense,
            };
            _context.ProductionSimulation.Add(ProductionSimulation);
            _context.SaveChanges();
            value.ProductionSimulationId = ProductionSimulation.ProductionSimulationId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionSimulation")]
        public ProductionSimulationModel Put(ProductionSimulationModel value)
        {
            var productionSimulation = _context.ProductionSimulation.SingleOrDefault(p => p.ProductionSimulationId == value.ProductionSimulationId);
            productionSimulation.ProdOrderNo = value.ProdOrderNo;
            productionSimulation.ItemId = value.ItemId;
            productionSimulation.ItemNo = value.ItemNo;
            productionSimulation.Description = value.Description;
            productionSimulation.PackSize = value.PackSize;
            productionSimulation.Quantity = value.Quantity;
            productionSimulation.Uom = value.Uom;
            productionSimulation.PerQtyUom = value.PerQtyUom;
            productionSimulation.PerQuantity = value.PerQuantity;
            productionSimulation.BatchNo = value.BatchNo;
            productionSimulation.StartingDate = value.StartingDate;
            productionSimulation.StatusCodeId = value.StatusCodeID.Value;
            productionSimulation.ModifiedByUserId = value.ModifiedByUserID;
            productionSimulation.ModifiedDate = DateTime.Now;
            productionSimulation.Dispense = value.Dispense;
            _context.SaveChanges();
            return value;
        }


        [HttpDelete]
        [Route("DeleteProductionSimulation")]
        public ActionResult<string> DeleteProductionSimulation(int id)
        {
            try
            {
                var deleteProductionSimulation = _context.ProductionSimulation.Where(p => p.ProductionSimulationId == id).FirstOrDefault();
                if (deleteProductionSimulation != null)
                {
                    _context.ProductionSimulation.Remove(deleteProductionSimulation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}