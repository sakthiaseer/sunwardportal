﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavProductMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavProductMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNavProductMaster")]
        public List<NavProductMasterModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavProductMasterModel> navProductMasterModels = new List<NavProductMasterModel>();
            var finsiproductList = _context.FinishProduct.ToList();
            var productmaster = _context.NavProductMaster
                               .Include("ApplicationUser")
                               .Include("AddedByUser")
                               .Include("ModifiedByUser")
                               .Include("StatusCode")
                               .Include("NavMethodCode")
                               .Include(s => s.FinishProductGeneralInfo)
                               .Include(s => s.NavProductMultiple)
                               .Include(s => s.NavProductShapeMultiple)
                                .Include(s => s.NavMarkingMultiple)
                               .Include(s => s.RegistrationReference).OrderByDescending(o => o.ProductMasterId).AsNoTracking().ToList();
            if (productmaster != null && productmaster.Count > 0)
            {
                productmaster.ForEach(s =>
                {
                    NavProductMasterModel navProductMasterModel = new NavProductMasterModel
                    {
                        ProductMasterId = s.ProductMasterId,
                        MethodCodeId = s.MethodCodeId,
                        MethodCodeName = s.MethodCode != null ? s.MethodCode.MethodName : string.Empty,
                        SalesCategoryId = s.SalesCategoryId,
                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                        SalesCategoryName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SalesCategoryId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SalesCategoryId).Select(a => a.Value).FirstOrDefault() : "",
                        ColorId = s.ColorId,
                        ColorName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ColorId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeId = s.ShapeId,
                        ShapeName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ShapeId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ShapeId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeIds = s.NavProductMultiple != null ? s.NavProductMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingId = s.MarkingId,
                        MarkingName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.MarkingId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourId = s.FlavourId,
                        FlavourName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FlavourId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourIds = s.NavProductShapeMultiple != null ? s.NavProductShapeMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingIds = s.NavMarkingMultiple != null ? s.NavMarkingMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        LengthMax = s.LengthMax,
                        LengthMin = s.LengthMin,
                        WidthMax = s.WidthMax,
                        WidthMin = s.WidthMin,
                        ThicknessMax = s.ThicknessMax,
                        ThicknessMin = s.ThicknessMin,
                        WeightMax = s.WeightMax,
                        WeightMin = s.WeightMin,
                        TypeOfCoating = s.TypeOfCoating,
                        CoreSizeLengthMax = s.CoreSizeLengthMax,
                        CoreSizeLengthMin = s.CoreSizeLengthMin,
                        CoreSizeWidthMax = s.CoreSizeWidthMax,
                        CoreSizeWidthMin = s.CoreSizeWidthMin,
                        CoreThicknessMin = s.CoreThicknessMin,
                        CoreThicknessMax = s.CoreThicknessMax,
                        CoreWeightMax = s.CoreWeightMax,
                        CoreWeightMin = s.CoreWeightMin,
                        CoatedSizeLengthMax = s.CoatedSizeLengthMax,
                        CoatedSizeLengthMin = s.CoatedSizeLengthMin,
                        CoatedSizeWidthMax = s.CoatedSizeWidthMax,
                        CoatedSizeWidthMin = s.CoatedSizeWidthMin,
                        CoatedThicknessMin = s.CoatedThicknessMin,
                        CoatedThicknessMax = s.CoatedThicknessMax,
                        CoatedWeightMax = s.CoatedWeightMax,
                        CoatedWeightMin = s.CoatedWeightMin,
                        ProductCreationType = s.ProductCreationType,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCodeID = s.StatusCodeId,
                        LengthVariationId = s.LengthVariationId,
                        LengthVariationValue = s.LengthVariationValue,
                        WidthVariationId = s.WidthVariationId,
                        WidthVariationValue = s.WidthVariationValue,
                        ThicknessVariationId = s.ThicknessVariationId,
                        ThicknessVariationValue = s.ThicknessVariationValue,
                        WeightVariationId = s.WeightVariationId,
                        WeightVariationValue = s.WeightVariationValue,
                        CoreSizeLengthVariationId = s.CoreSizeLengthVariationId,
                        CoreSizeLengthVariationValue = s.CoreSizeLengthVariationValue,
                        CoreSizeWidthVariationId = s.CoreSizeWidthVariationId,
                        CoreSizeWidthVariationValue = s.CoreSizeWidthVariationValue,
                        CoreSizeThicknessVariationValue = s.CoreSizeThicknessVariationValue,
                        CoreSizeThicknessVariationId = s.CoreSizeThicknessVariationId,
                        CoreSizeWeightVariationId = s.CoreSizeWeightVariationId,
                        CoreSizeWeightVariationValue = s.CoreSizeWeightVariationValue,
                        CoatedSizeLengthVariationId = s.CoatedSizeLengthVariationId,
                        CoatedSizeLengthVariationValue = s.CoatedSizeLengthVariationValue,
                        CoatedSizeWidthVariationId = s.CoatedSizeWidthVariationId,
                        CoatedSizeWidthVariationValue = s.CoatedSizeWidthVariationValue,
                        CoatedSizeThicknessVariationId = s.CoatedSizeThicknessVariationId,
                        CoatedSizeThicknessVariationValue = s.CoatedSizeThicknessVariationValue,
                        CoatedSizeWeightVariationId = s.CoatedSizeWeightVariationId,
                        CoatedSizeWeightVariationValue = s.CoatedSizeWeightVariationValue,
                        ClarityId = s.ClarityId,
                        ClarityName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ClarityId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ClarityId).Select(a => a.Value).FirstOrDefault() : "",
                        Phmin = s.Phmin,
                        Phmax = s.Phmax,
                        PhvariationId = s.PhvariationId,
                        PhvariationValue = s.PhvariationValue,
                        Sgmin = s.Sgmin,
                        Sgmax = s.Sgmax,
                        SgvariationId = s.SgvariationId,
                        SgvariationValue = s.SgvariationValue,
                        ViscosityMin = s.ViscosityMin,
                        ViscosityMax = s.ViscosityMax,
                        ViscosityVariationId = s.ViscosityVariationId,
                        ViscosityVariationValue = s.ViscosityVariationValue,
                        TextureId = s.TextureId,
                        TextureName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TextureId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TextureId).Select(a => a.Value).FirstOrDefault() : "",
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        IsMasterDocument = s.IsMasterDocument,
                        IsInformationvsMaster = s.IsInformationvsMaster,
                        PhStandard = s.Phstandard,
                        SgStandard = s.Sgstandard,
                        ViscosityStandard = s.ViscosityStandard,
                        CreamPhmax = s.CreamPhmax,
                        CreamPhmin = s.CreamPhmin,
                        CreamSgmax = s.CreamSgmax,
                        CreamSgmin = s.CreamSgmin,
                        CreamViscosityMax = s.CreamViscosityMax,
                        CreamViscosityMin = s.CreamViscosityMin,
                        CreamPhStandard = s.CreamPhstandard,
                        CreamSgStandard = s.CreamSgstandard,
                        CreamViscosityStandard = s.CreamViscosityStandard,
                        CreamPhvariationId = s.CreamPhvariationId,
                        CreamPhvariationValue = s.CreamPhvariationValue,
                        CreamSgvariationId = s.CreamSgvariationId,
                        CreamSgvariationValue = s.CreamSgvariationValue,
                        CreamViscosityVariationId = s.CreamViscosityVariationId,
                        CreamViscosityVariationValue = s.CreamViscosityVariationValue,
                        LengthStandard = s.LengthStandard,
                        CoreSizeLengthStandard = s.CoreSizeLengthStandard,
                        CoatedSizeLengthStandard = s.CoatedSizeLengthStandard,
                        WidthStandard = s.WidthStandard,
                        CoreSizeWidthStandard = s.CoreSizeWidthStandard,
                        CoreWeightStandard = s.CoreWeightStandard,
                        CoreThicknessStandard = s.CoreThicknessStandard,
                        WeightStandard = s.WeightStandard,
                        CoatedSizeWidthStandard = s.CoatedSizeWidthStandard,
                        CoatedThicknessStandard = s.CoatedThicknessStandard,
                        CoatedWeightStandard = s.CoatedWeightStandard,
                        ThicknessStandard = s.ThicknessStandard,
                        RegistrationReferenceId = s.RegistrationReferenceId,
                        RegistrationReferenceName = s.RegistrationReference.CodeValue,

                    };
                    navProductMasterModels.Add(navProductMasterModel);
                });
            }

            return navProductMasterModels;
        }
        // GET: api/Project/2
        [HttpGet]
        [Route("GetNavProductMasterByType")]
        public List<NavProductMasterModel> Get(int? id)
        {
            List<NavProductMasterModel> navProductMasterModels = new List<NavProductMasterModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.ToList();
            var productmaster = _context.NavProductMaster
                               .Include("AddedByUser")
                               .Include("ModifiedByUser")
                               .Include("StatusCode")
                               .Include(s => s.NavProductMultiple)
                               .Include(s => s.NavProductShapeMultiple)
                               .Include(s => s.NavMarkingMultiple)
                               .Include(s=>s.FinishProductGeneralInfo)
                               .Include(s=>s.FinishProductGeneralInfo.FinishProduct)
                               .Include(s => s.RegistrationReference).Where(s => s.ProductCreationType == id.Value).AsNoTracking().ToList();
            if (productmaster != null && productmaster.Count > 0)
            {
                productmaster.ForEach(s =>
                {
                    NavProductMasterModel navProductMasterModel = new NavProductMasterModel
                    {
                        ProductMasterId = s.ProductMasterId,
                        MethodCodeId = s.MethodCodeId,
                        MethodCodeName = s.MethodCode != null ? s.MethodCode.MethodName : string.Empty,
                        FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                        ProductName = applicationmasterdetail != null && s.FinishProductGeneralInfo != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                        SalesCategoryId = s.SalesCategoryId,
                        SalesCategoryName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SalesCategoryId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SalesCategoryId).Select(a => a.Value).FirstOrDefault() : "",
                        ColorId = s.ColorId,
                        ColorName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ColorId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeId = s.ShapeId,
                        ShapeName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ShapeId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ShapeId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeIds = s.NavProductMultiple != null ? s.NavProductMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingId = s.MarkingId,
                        MarkingName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.MarkingId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourId = s.FlavourId,
                        FlavourName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FlavourId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourIds = s.NavProductShapeMultiple != null ? s.NavProductShapeMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingIds = s.NavMarkingMultiple != null ? s.NavMarkingMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        LengthMax = s.LengthMax,
                        LengthMin = s.LengthMin,
                        WidthMax = s.WidthMax,
                        WidthMin = s.WidthMin,
                        ThicknessMax = s.ThicknessMax,
                        ThicknessMin = s.ThicknessMin,
                        WeightMax = s.WeightMax,
                        WeightMin = s.WeightMin,
                        TypeOfCoating = s.TypeOfCoating,
                        CoreSizeLengthMax = s.CoreSizeLengthMax,
                        CoreSizeLengthMin = s.CoreSizeLengthMin,
                        CoreSizeWidthMax = s.CoreSizeWidthMax,
                        CoreSizeWidthMin = s.CoreSizeWidthMin,
                        CoreThicknessMin = s.CoreThicknessMin,
                        CoreThicknessMax = s.CoreThicknessMax,
                        CoreWeightMax = s.CoreWeightMax,
                        CoreWeightMin = s.CoreWeightMin,
                        CoatedSizeLengthMax = s.CoatedSizeLengthMax,
                        CoatedSizeLengthMin = s.CoatedSizeLengthMin,
                        CoatedSizeWidthMax = s.CoatedSizeWidthMax,
                        CoatedSizeWidthMin = s.CoatedSizeWidthMin,
                        CoatedThicknessMin = s.CoatedThicknessMin,
                        CoatedThicknessMax = s.CoatedThicknessMax,
                        CoatedWeightMax = s.CoatedWeightMax,
                        CoatedWeightMin = s.CoatedWeightMin,
                        ProductCreationType = s.ProductCreationType,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCodeID = s.StatusCodeId,
                        LengthVariationId = s.LengthVariationId,
                        LengthVariationValue = s.LengthVariationValue,
                        WidthVariationId = s.WidthVariationId,
                        WidthVariationValue = s.WidthVariationValue,
                        ThicknessVariationId = s.ThicknessVariationId,
                        ThicknessVariationValue = s.ThicknessVariationValue,
                        WeightVariationId = s.WeightVariationId,
                        WeightVariationValue = s.WeightVariationValue,
                        CoreSizeLengthVariationId = s.CoreSizeLengthVariationId,
                        CoreSizeLengthVariationValue = s.CoreSizeLengthVariationValue,
                        CoreSizeWidthVariationId = s.CoreSizeWidthVariationId,
                        CoreSizeWidthVariationValue = s.CoreSizeWidthVariationValue,
                        CoreSizeThicknessVariationValue = s.CoreSizeThicknessVariationValue,
                        CoreSizeThicknessVariationId = s.CoreSizeThicknessVariationId,
                        CoreSizeWeightVariationId = s.CoreSizeWeightVariationId,
                        CoreSizeWeightVariationValue = s.CoreSizeWeightVariationValue,
                        CoatedSizeLengthVariationId = s.CoatedSizeLengthVariationId,
                        CoatedSizeLengthVariationValue = s.CoatedSizeLengthVariationValue,
                        CoatedSizeWidthVariationId = s.CoatedSizeWidthVariationId,
                        CoatedSizeWidthVariationValue = s.CoatedSizeWidthVariationValue,
                        CoatedSizeThicknessVariationId = s.CoatedSizeThicknessVariationId,
                        CoatedSizeThicknessVariationValue = s.CoatedSizeThicknessVariationValue,
                        CoatedSizeWeightVariationId = s.CoatedSizeWeightVariationId,
                        CoatedSizeWeightVariationValue = s.CoatedSizeWeightVariationValue,
                        ClarityId = s.ClarityId,
                        ClarityName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ClarityId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ClarityId).Select(a => a.Value).FirstOrDefault() : "",
                        Phmin = s.Phmin,
                        Phmax = s.Phmax,
                        PhvariationId = s.PhvariationId,
                        PhvariationValue = s.PhvariationValue,
                        Sgmin = s.Sgmin,
                        Sgmax = s.Sgmax,
                        SgvariationId = s.SgvariationId,
                        SgvariationValue = s.SgvariationValue,
                        ViscosityMin = s.ViscosityMin,
                        ViscosityMax = s.ViscosityMax,
                        ViscosityVariationId = s.ViscosityVariationId,
                        ViscosityVariationValue = s.ViscosityVariationValue,
                        TextureId = s.TextureId,
                        TextureName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TextureId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TextureId).Select(a => a.Value).FirstOrDefault() : "",
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        IsMasterDocument = s.IsMasterDocument,
                        IsInformationvsMaster = s.IsInformationvsMaster,
                        PhStandard = s.Phstandard,
                        SgStandard = s.Sgstandard,
                        ViscosityStandard = s.ViscosityStandard,
                        CreamPhmax = s.CreamPhmax,
                        CreamPhmin = s.CreamPhmin,
                        CreamSgmax = s.CreamSgmax,
                        CreamSgmin = s.CreamSgmin,
                        CreamViscosityMax = s.CreamViscosityMax,
                        CreamViscosityMin = s.CreamViscosityMin,
                        CreamPhStandard = s.CreamPhstandard,
                        CreamSgStandard = s.CreamSgstandard,
                        CreamViscosityStandard = s.CreamViscosityStandard,
                        CreamPhvariationId = s.CreamPhvariationId,
                        CreamPhvariationValue = s.CreamPhvariationValue,
                        CreamSgvariationId = s.CreamSgvariationId,
                        CreamSgvariationValue = s.CreamSgvariationValue,
                        CreamViscosityVariationId = s.CreamViscosityVariationId,
                        CreamViscosityVariationValue = s.CreamViscosityVariationValue,
                        LengthStandard = s.LengthStandard,
                        CoreSizeLengthStandard = s.CoreSizeLengthStandard,
                        CoatedSizeLengthStandard = s.CoatedSizeLengthStandard,
                        WidthStandard = s.WidthStandard,
                        CoreSizeWidthStandard = s.CoreSizeWidthStandard,
                        CoreWeightStandard = s.CoreWeightStandard,
                        CoreThicknessStandard = s.CoreThicknessStandard,
                        WeightStandard = s.WeightStandard,
                        CoatedSizeWidthStandard = s.CoatedSizeWidthStandard,
                        CoatedThicknessStandard = s.CoatedThicknessStandard,
                        CoatedWeightStandard = s.CoatedWeightStandard,
                        ThicknessStandard = s.ThicknessStandard,
                        RegistrationReferenceId = s.RegistrationReferenceId,
                        RegistrationReferenceName = s.RegistrationReference.CodeValue,
                        ManufacturingSite = applicationmasterdetail != null && s.FinishProductGeneralInfo != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId)?.Value : string.Empty,
                        CustomerProductName = applicationmasterdetail != null &&  s.FinishProductGeneralInfo != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId)?.Value : string.Empty,
                        RegisterCountry = applicationmasterdetail != null && s.FinishProductGeneralInfo != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry)?.Value : string.Empty,
                        RegisterHolderName = applicationmasterdetail != null && s.FinishProductGeneralInfo != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId)?.Value : string.Empty,

                    };
                    navProductMasterModels.Add(navProductMasterModel);
                });
            }

            return navProductMasterModels;
        }

        [HttpGet]
        [Route("GetNavProductMasterByFinishProduct")]
        public List<NavProductMasterModel> GetNavProductMasterByFinishProduct(int? id)
        {
            List<NavProductMasterModel> navProductMasterModels = new List<NavProductMasterModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var productmaster = _context.NavProductMaster
                       .Include("AddedByUser")
                       .Include("ModifiedByUser")
                       .Include("StatusCode")
                       .Include("NavMethodCode")
                       .Include("CodeMaster")
                       .Include(s => s.FinishProductGeneralInfo)
                       .Include(s => s.NavProductMultiple)
                       .Include(s => s.NavProductShapeMultiple)
                       .Include(s => s.NavMarkingMultiple)
                       .Include(s => s.RegistrationReference).Where(a => a.FinishProductGeneralInfoId == id && a.IsMasterDocument == true).AsNoTracking().ToList();
            if (productmaster != null && productmaster.Count > 0)
            {
                productmaster.ForEach(s =>
                {
                    NavProductMasterModel navProductMasterModel = new NavProductMasterModel
                    {
                        ProductMasterId = s.ProductMasterId,
                        MethodCodeId = s.MethodCodeId,
                        FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                        SalesCategoryId = s.SalesCategoryId,
                        SalesCategoryName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SalesCategoryId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SalesCategoryId).Select(a => a.Value).FirstOrDefault() : "",
                        ColorId = s.ColorId,
                        ColorName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ColorId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeId = s.ShapeId,
                        ShapeName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ShapeId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ShapeId).Select(a => a.Value).FirstOrDefault() : "",
                        ShapeIds = s.NavProductMultiple != null ? s.NavProductMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingId = s.MarkingId,
                        MarkingName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.MarkingId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourId = s.FlavourId,
                        FlavourName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FlavourId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MarkingId).Select(a => a.Value).FirstOrDefault() : "",
                        FlavourIds = s.NavProductShapeMultiple != null ? s.NavProductShapeMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        MarkingIds = s.NavMarkingMultiple != null ? s.NavMarkingMultiple.Select(m => m.ApplicationMasterDetailId.Value).ToList() : null,
                        LengthMax = s.LengthMax,
                        LengthMin = s.LengthMin,
                        WidthMax = s.WidthMax,
                        WidthMin = s.WidthMin,
                        ThicknessMax = s.ThicknessMax,
                        ThicknessMin = s.ThicknessMin,
                        WeightMax = s.WeightMax,
                        WeightMin = s.WeightMin,
                        TypeOfCoating = s.TypeOfCoating,
                        CoreSizeLengthMax = s.CoreSizeLengthMax,
                        CoreSizeLengthMin = s.CoreSizeLengthMin,
                        CoreSizeWidthMax = s.CoreSizeWidthMax,
                        CoreSizeWidthMin = s.CoreSizeWidthMin,
                        CoreThicknessMin = s.CoreThicknessMin,
                        CoreThicknessMax = s.CoreThicknessMax,
                        CoreWeightMax = s.CoreWeightMax,
                        CoreWeightMin = s.CoreWeightMin,
                        CoatedSizeLengthMax = s.CoatedSizeLengthMax,
                        CoatedSizeLengthMin = s.CoatedSizeLengthMin,
                        CoatedSizeWidthMax = s.CoatedSizeWidthMax,
                        CoatedSizeWidthMin = s.CoatedSizeWidthMin,
                        CoatedThicknessMin = s.CoatedThicknessMin,
                        CoatedThicknessMax = s.CoatedThicknessMax,
                        CoatedWeightMax = s.CoatedWeightMax,
                        CoatedWeightMin = s.CoatedWeightMin,
                        ProductCreationType = s.ProductCreationType,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : string.Empty,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty,
                        LengthVariationId = s.LengthVariationId,
                        LengthVariationValue = s.LengthVariationValue,
                        WidthVariationId = s.WidthVariationId,
                        WidthVariationValue = s.WidthVariationValue,
                        ThicknessVariationId = s.ThicknessVariationId,
                        ThicknessVariationValue = s.ThicknessVariationValue,
                        WeightVariationId = s.WeightVariationId,
                        WeightVariationValue = s.WeightVariationValue,
                        CoreSizeLengthVariationId = s.CoreSizeLengthVariationId,
                        CoreSizeLengthVariationValue = s.CoreSizeLengthVariationValue,
                        CoreSizeWidthVariationId = s.CoreSizeWidthVariationId,
                        CoreSizeWidthVariationValue = s.CoreSizeWidthVariationValue,
                        CoreSizeThicknessVariationValue = s.CoreSizeThicknessVariationValue,
                        CoreSizeThicknessVariationId = s.CoreSizeThicknessVariationId,
                        CoreSizeWeightVariationId = s.CoreSizeWeightVariationId,
                        CoreSizeWeightVariationValue = s.CoreSizeWeightVariationValue,
                        CoatedSizeLengthVariationId = s.CoatedSizeLengthVariationId,
                        CoatedSizeLengthVariationValue = s.CoatedSizeLengthVariationValue,
                        CoatedSizeWidthVariationId = s.CoatedSizeWidthVariationId,
                        CoatedSizeWidthVariationValue = s.CoatedSizeWidthVariationValue,
                        CoatedSizeThicknessVariationId = s.CoatedSizeThicknessVariationId,
                        CoatedSizeThicknessVariationValue = s.CoatedSizeThicknessVariationValue,
                        CoatedSizeWeightVariationId = s.CoatedSizeWeightVariationId,
                        CoatedSizeWeightVariationValue = s.CoatedSizeWeightVariationValue,
                        ClarityId = s.ClarityId,
                        ClarityName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ClarityId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ClarityId).Select(a => a.Value).FirstOrDefault() : "",
                        Phmin = s.Phmin,
                        Phmax = s.Phmax,
                        PhvariationId = s.PhvariationId,
                        PhvariationValue = s.PhvariationValue,
                        Sgmin = s.Sgmin,
                        Sgmax = s.Sgmax,
                        SgvariationId = s.SgvariationId,
                        SgvariationValue = s.SgvariationValue,
                        ViscosityMin = s.ViscosityMin,
                        ViscosityMax = s.ViscosityMax,
                        ViscosityVariationId = s.ViscosityVariationId,
                        ViscosityVariationValue = s.ViscosityVariationValue,
                        TextureId = s.TextureId,
                        TextureName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TextureId) != null) ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TextureId).Select(a => a.Value).FirstOrDefault() : "",
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        IsMasterDocument = s.IsMasterDocument,
                        IsInformationvsMaster = true,
                        PhStandard = s.Phstandard,
                        SgStandard = s.Sgstandard,
                        ViscosityStandard = s.ViscosityStandard,
                        CreamPhmax = s.CreamPhmax,
                        CreamPhmin = s.CreamPhmin,
                        CreamSgmax = s.CreamSgmax,
                        CreamSgmin = s.CreamSgmin,
                        CreamViscosityMax = s.CreamViscosityMax,
                        CreamViscosityMin = s.CreamViscosityMin,
                        CreamPhStandard = s.CreamPhstandard,
                        CreamSgStandard = s.CreamSgstandard,
                        CreamViscosityStandard = s.CreamViscosityStandard,
                        CreamPhvariationId = s.CreamPhvariationId,
                        CreamPhvariationValue = s.CreamPhvariationValue,
                        CreamSgvariationId = s.CreamSgvariationId,
                        CreamSgvariationValue = s.CreamSgvariationValue,
                        CreamViscosityVariationId = s.CreamViscosityVariationId,
                        CreamViscosityVariationValue = s.CreamViscosityVariationValue,
                        LengthStandard = s.LengthStandard,
                        CoreSizeLengthStandard = s.CoreSizeLengthStandard,
                        CoatedSizeLengthStandard = s.CoatedSizeLengthStandard,
                        WidthStandard = s.WidthStandard,
                        CoreSizeWidthStandard = s.CoreSizeWidthStandard,
                        CoreWeightStandard = s.CoreWeightStandard,
                        CoreThicknessStandard = s.CoreThicknessStandard,
                        WeightStandard = s.WeightStandard,
                        CoatedSizeWidthStandard = s.CoatedSizeWidthStandard,
                        CoatedThicknessStandard = s.CoatedThicknessStandard,
                        CoatedWeightStandard = s.CoatedWeightStandard,
                        ThicknessStandard = s.ThicknessStandard,
                        RegistrationReferenceId = s.RegistrationReferenceId,
                        RegistrationReferenceName = s.RegistrationReference.CodeValue,

                    };
                    navProductMasterModels.Add(navProductMasterModel);
                });
            }

            return navProductMasterModels;
        }

        [HttpGet]
        [Route("IsMasterExist")]
        public bool IsMasterExist(int? id)
        {
            var list = _context.NavProductMaster.Where(s => s.FinishProductGeneralInfoId == id && s.IsMasterDocument == true).FirstOrDefault();
            if (list != null)
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
            else
            {
                return false;
            }
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NavProductMasterModel> GetData(SearchModel searchModel)
        {
            var productmaster = new NavProductMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).LastOrDefault();
                        break;
                    case "Next":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).LastOrDefault();
                        break;
                    case "Next":
                        productmaster = _context.NavProductMaster.OrderBy(o => o.ProductMasterId).FirstOrDefault(s => s.ProductMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        productmaster = _context.NavProductMaster.OrderByDescending(o => o.ProductMasterId).FirstOrDefault(s => s.ProductMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NavProductMasterModel>(productmaster);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var finishProductItem = result.FinishProductGeneralInfoId > 0 ? (_context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.FinishProductGeneralInfoId).FinishProductId) : null;
                if (finishProductItem != null)
                {
                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }

                result.ShapeIds = _context.NavProductMultiple.Where(p => p.ProductMasterId == result.ProductMasterId).AsNoTracking().Select(p => p.ApplicationMasterDetailId.Value).ToList();
                result.FlavourIds = _context.NavProductShapeMultiple.Where(p => p.ProductMasterId == result.ProductMasterId).AsNoTracking().Select(p => p.ApplicationMasterDetailId.Value).ToList();
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertNavProductMaster")]
        public NavProductMasterModel Post(NavProductMasterModel value)
        {
            bool isMasterExist = false;
            if (value.IsMasterDocument.GetValueOrDefault())
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var generalinfo = _context.FinishProductGeneralInfo.FirstOrDefault(p => p.FinishProductGeneralInfoId == value.FinishProductGeneralInfoId);
                var finishProduct = _context.FinishProduct.FirstOrDefault(p => p.FinishProductId == generalinfo.FinishProductId);
                string productName = string.Empty;
                if (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == finishProduct.ProductId) != null)
                {
                    productName = applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == finishProduct.ProductId).Value;
                }

                var finishProducts = _context.FinishProduct?.Select(fp =>
                                    new
                                    {
                                        fp.FinishProductId,    
                                        fp.ProductId,
                                        ProductName =fp.ProductId!=null? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == fp.ProductId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == fp.ProductId).Value : "" : "",
                                    });

                var finishProductIds = finishProducts.Where(f => f.ProductName == productName)?.Select(i => i.FinishProductId)?.ToList();
                isMasterExist = _context.NavProductMaster.Any(f => finishProductIds.Contains(f.FinishProductId.Value) && f.IsMasterDocument == value.IsMasterDocument);
            }
            if (!isMasterExist)
            {
                var productmaster = new NavProductMaster
                {
                    MethodCodeId = value.MethodCodeId,
                    //FinishProductId = value.FinishProductId,
                    FinishProductGeneralInfoId = value.FinishProductGeneralInfoId,
                    RegistrationReferenceId = value.RegistrationReferenceId,
                    SalesCategoryId = value.SalesCategoryId,
                    ColorId = value.ColorId,
                    ShapeId = value.ShapeId,
                    MarkingId = value.MarkingId,
                    LengthMax = value.LengthMax,
                    LengthMin = value.LengthMin,
                    WidthMax = value.WidthMax,
                    WidthMin = value.WidthMin,
                    ThicknessMax = value.ThicknessMax,
                    ThicknessMin = value.ThicknessMin,
                    WeightMax = value.WeightMax,
                    WeightMin = value.WeightMin,
                    TypeOfCoating = value.TypeOfCoating,
                    CoreSizeLengthMax = value.CoreSizeLengthMax,
                    CoreSizeLengthMin = value.CoreSizeLengthMin,
                    CoreSizeWidthMax = value.CoreSizeWidthMax,
                    CoreSizeWidthMin = value.CoreSizeWidthMin,
                    CoreThicknessMin = value.CoreThicknessMin,
                    CoreThicknessMax = value.CoreThicknessMax,
                    CoreWeightMax = value.CoreWeightMax,
                    CoreWeightMin = value.CoreWeightMin,
                    CoatedSizeLengthMax = value.CoatedSizeLengthMax,
                    CoatedSizeLengthMin = value.CoatedSizeLengthMin,
                    CoatedSizeWidthMax = value.CoatedSizeWidthMax,
                    CoatedSizeWidthMin = value.CoatedSizeWidthMin,
                    CoatedThicknessMin = value.CoatedThicknessMin,
                    CoatedThicknessMax = value.CoatedThicknessMax,
                    CoatedWeightMin = value.CoatedWeightMin,
                    CoatedWeightMax = value.CoatedWeightMax,
                    ProductCreationType = value.ProductCreationType,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    LengthVariationId = value.LengthVariationId,
                    LengthVariationValue = value.LengthVariationValue,
                    WidthVariationId = value.WidthVariationId,
                    WidthVariationValue = value.WidthVariationValue,
                    ThicknessVariationId = value.ThicknessVariationId,
                    ThicknessVariationValue = value.ThicknessVariationValue,
                    WeightVariationId = value.WeightVariationId,
                    WeightVariationValue = value.WeightVariationValue,
                    CoreSizeLengthVariationId = value.CoreSizeLengthVariationId,
                    CoreSizeLengthVariationValue = value.CoreSizeLengthVariationValue,
                    CoreSizeWidthVariationId = value.CoreSizeWidthVariationId,
                    CoreSizeWidthVariationValue = value.CoreSizeWidthVariationValue,
                    CoreSizeThicknessVariationValue = value.CoreSizeThicknessVariationValue,
                    CoreSizeThicknessVariationId = value.CoreSizeThicknessVariationId,
                    CoreSizeWeightVariationId = value.CoreSizeWeightVariationId,
                    CoreSizeWeightVariationValue = value.CoreSizeWeightVariationValue,
                    CoatedSizeLengthVariationId = value.CoatedSizeLengthVariationId,
                    CoatedSizeLengthVariationValue = value.CoatedSizeLengthVariationValue,
                    CoatedSizeWidthVariationId = value.CoatedSizeWidthVariationId,
                    CoatedSizeWidthVariationValue = value.CoatedSizeWidthVariationValue,
                    CoatedSizeThicknessVariationId = value.CoatedSizeThicknessVariationId,
                    CoatedSizeThicknessVariationValue = value.CoatedSizeThicknessVariationValue,
                    CoatedSizeWeightVariationId = value.CoatedSizeWeightVariationId,
                    CoatedSizeWeightVariationValue = value.CoatedSizeWeightVariationValue,
                    ClarityId = value.ClarityId,
                    Phmin = value.Phmin,
                    Phmax = value.Phmax,
                    PhvariationId = value.PhvariationId,
                    PhvariationValue = value.PhvariationValue,
                    Sgmin = value.Sgmin,
                    Sgmax = value.Sgmax,
                    SgvariationId = value.SgvariationId,
                    SgvariationValue = value.SgvariationValue,
                    ViscosityMin = value.ViscosityMin,
                    ViscosityMax = value.ViscosityMax,
                    ViscosityVariationId = value.ViscosityVariationId,
                    ViscosityVariationValue = value.ViscosityVariationValue,
                    TextureId = value.TextureId,
                    ManufacturingSiteId = value.ManufacturingSiteId,
                    IsMasterDocument = value.IsMasterDocument,
                    IsInformationvsMaster = value.IsInformationvsMaster,
                    FlavourId = value.FlavourId,
                    Phstandard = value.PhStandard,
                    Sgstandard = value.SgStandard,
                    ViscosityStandard = value.ViscosityStandard,
                    CreamPhmax = value.CreamPhmax,
                    CreamPhmin = value.CreamPhmin,
                    CreamSgmax = value.CreamSgmax,
                    CreamSgmin = value.CreamSgmin,
                    CreamViscosityMax = value.CreamViscosityMax,
                    CreamViscosityMin = value.CreamViscosityMin,
                    CreamPhstandard = value.CreamPhStandard,
                    CreamSgstandard = value.CreamSgStandard,
                    CreamViscosityStandard = value.CreamViscosityStandard,
                    CreamPhvariationId = value.CreamPhvariationId,
                    CreamPhvariationValue = value.CreamPhvariationValue,
                    CreamSgvariationId = value.CreamSgvariationId,
                    CreamSgvariationValue = value.CreamSgvariationValue,
                    CreamViscosityVariationId = value.CreamViscosityVariationId,
                    CreamViscosityVariationValue = value.CreamViscosityVariationValue,
                    LengthStandard = value.LengthStandard,
                    CoreSizeLengthStandard = value.CoreSizeLengthStandard,
                    CoatedSizeLengthStandard = value.CoatedSizeLengthStandard,
                    WidthStandard = value.WidthStandard,
                    CoreSizeWidthStandard = value.CoreSizeWidthStandard,
                    CoreWeightStandard = value.CoreWeightStandard,
                    CoreThicknessStandard = value.CoreThicknessStandard,
                    WeightStandard = value.WeightStandard,
                    CoatedSizeWidthStandard = value.CoatedSizeWidthStandard,
                    CoatedThicknessStandard = value.CoatedThicknessStandard,
                    CoatedWeightStandard = value.CoatedWeightStandard,
                    ThicknessStandard = value.ThicknessStandard,
                };
                if (value.ShapeIds != null)
                {
                    value.ShapeIds.ForEach(c =>
                    {
                        var navProductMultiple = new NavProductMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavProductMultiple.Add(navProductMultiple);
                    });
                }
                if (value.MarkingIds != null)
                {
                    value.MarkingIds.ForEach(c =>
                    {
                        var navMarkingId = new NavMarkingMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavMarkingMultiple.Add(navMarkingId);
                    });
                }
                if (value.FlavourIds != null)
                {
                    value.FlavourIds.ForEach(c =>
                    {
                        var navProductFlavourMultiple = new NavProductShapeMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavProductShapeMultiple.Add(navProductFlavourMultiple);
                    });
                }
                _context.NavProductMaster.Add(productmaster);
                _context.SaveChanges();
                value.ProductMasterId = productmaster.ProductMasterId;
                return value;
            }
            else
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavProductMaster")]
        public NavProductMasterModel Put(NavProductMasterModel value)
        {
            bool isMasterExist = false;
            if (value.IsMasterDocument.GetValueOrDefault())
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var generalinfo = _context.FinishProductGeneralInfo.FirstOrDefault(p => p.FinishProductGeneralInfoId == value.FinishProductGeneralInfoId);
                var finishProduct = _context.FinishProduct.FirstOrDefault(p => p.FinishProductId == generalinfo.FinishProductId);
                string productName = string.Empty;
                if (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == finishProduct.ProductId) != null)
                {
                    productName = applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == finishProduct.ProductId).Value;
                }

                var finishProducts = _context.FinishProduct.Select(fp =>
                                    new
                                    {
                                        fp.FinishProductId,
                                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == fp.ProductId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == fp.ProductId).Value : "",
                                    });

                var finishProductIds = finishProducts.Where(f => f.ProductName == productName).AsNoTracking().Select(i => i.FinishProductId).ToList();
                isMasterExist = _context.NavProductMaster.Any(f => finishProductIds.Contains(f.FinishProductId.Value) && f.IsMasterDocument == value.IsMasterDocument);
            }
            if (!isMasterExist)
            {
                var productmaster = _context.NavProductMaster.SingleOrDefault(p => p.ProductMasterId == value.ProductMasterId);
                productmaster.FinishProductGeneralInfoId = value.FinishProductGeneralInfoId;
                productmaster.RegistrationReferenceId = value.RegistrationReferenceId;
                productmaster.MethodCodeId = value.MethodCodeId;
                productmaster.SalesCategoryId = value.SalesCategoryId;
                productmaster.ColorId = value.ColorId;
                productmaster.ShapeId = value.ShapeId;
                productmaster.MarkingId = value.MarkingId;
                productmaster.LengthMax = value.LengthMax;
                productmaster.LengthMin = value.LengthMin;
                productmaster.WidthMax = value.WidthMax;
                productmaster.WidthMin = value.WidthMin;
                productmaster.ThicknessMax = value.ThicknessMax;
                productmaster.ThicknessMin = value.ThicknessMin;
                productmaster.WeightMax = value.WeightMax;
                productmaster.WeightMin = value.WeightMin;
                productmaster.TypeOfCoating = value.TypeOfCoating;
                productmaster.CoreSizeLengthMax = value.CoreSizeLengthMax;
                productmaster.CoreSizeLengthMin = value.CoreSizeLengthMin;
                productmaster.CoreSizeWidthMax = value.CoreSizeWidthMax;
                productmaster.CoreSizeWidthMin = value.CoreSizeWidthMin;
                productmaster.CoreThicknessMin = value.CoreThicknessMin;
                productmaster.CoreThicknessMax = value.CoreThicknessMax;
                productmaster.CoreWeightMax = value.CoreWeightMax;
                productmaster.CoreWeightMin = value.CoreWeightMin;
                productmaster.CoatedSizeLengthMax = value.CoatedSizeLengthMax;
                productmaster.CoatedSizeLengthMin = value.CoatedSizeLengthMin;
                productmaster.CoatedSizeWidthMax = value.CoatedSizeWidthMax;
                productmaster.CoatedSizeWidthMin = value.CoatedSizeWidthMin;
                productmaster.CoatedThicknessMin = value.CoatedThicknessMin;
                productmaster.CoatedThicknessMax = value.CoatedThicknessMax;
                productmaster.CoatedWeightMin = value.CoatedWeightMin;
                productmaster.CoatedWeightMax = value.CoatedWeightMax;
                productmaster.ProductCreationType = value.ProductCreationType;
                productmaster.ModifiedByUserId = value.ModifiedByUserID;
                productmaster.ModifiedDate = DateTime.Now;
                productmaster.StatusCodeId = value.StatusCodeID.Value;
                productmaster.LengthVariationId = value.LengthVariationId;
                productmaster.LengthVariationValue = value.LengthVariationValue;
                productmaster.WidthVariationId = value.WidthVariationId;
                productmaster.WidthVariationValue = value.WidthVariationValue;
                productmaster.ThicknessVariationId = value.ThicknessVariationId;
                productmaster.ThicknessVariationValue = value.ThicknessVariationValue;
                productmaster.WeightVariationId = value.WeightVariationId;
                productmaster.WeightVariationValue = value.WeightVariationValue;
                productmaster.CoreSizeLengthVariationId = value.CoreSizeLengthVariationId;
                productmaster.CoreSizeLengthVariationValue = value.CoreSizeLengthVariationValue;
                productmaster.CoreSizeWidthVariationId = value.CoreSizeWidthVariationId;
                productmaster.CoreSizeWidthVariationValue = value.CoreSizeWidthVariationValue;
                productmaster.CoreSizeThicknessVariationValue = value.CoreSizeThicknessVariationValue;
                productmaster.CoreSizeThicknessVariationId = value.CoreSizeThicknessVariationId;
                productmaster.CoreSizeWeightVariationId = value.CoreSizeWeightVariationId;
                productmaster.CoreSizeWeightVariationValue = value.CoreSizeWeightVariationValue;
                productmaster.CoatedSizeLengthVariationId = value.CoatedSizeLengthVariationId;
                productmaster.CoatedSizeLengthVariationValue = value.CoatedSizeLengthVariationValue;
                productmaster.CoatedSizeWidthVariationId = value.CoatedSizeWidthVariationId;
                productmaster.CoatedSizeWidthVariationValue = value.CoatedSizeWidthVariationValue;
                productmaster.CoatedSizeThicknessVariationId = value.CoatedSizeThicknessVariationId;
                productmaster.CoatedSizeThicknessVariationValue = value.CoatedSizeThicknessVariationValue;
                productmaster.CoatedSizeWeightVariationId = value.CoatedSizeWeightVariationId;
                productmaster.CoatedSizeWeightVariationValue = value.CoatedSizeWeightVariationValue;
                productmaster.ClarityId = value.ClarityId;
                productmaster.Phmin = value.Phmin;
                productmaster.Phmax = value.Phmax;
                productmaster.PhvariationId = value.PhvariationId;
                productmaster.PhvariationValue = value.PhvariationValue;
                productmaster.Sgmin = value.Sgmin;
                productmaster.Sgmax = value.Sgmax;
                productmaster.SgvariationId = value.SgvariationId;
                productmaster.SgvariationValue = value.SgvariationValue;
                productmaster.ViscosityMin = value.ViscosityMin;
                productmaster.ViscosityMax = value.ViscosityMax;
                productmaster.ViscosityVariationId = value.ViscosityVariationId;
                productmaster.ViscosityVariationValue = value.ViscosityVariationValue;
                productmaster.TextureId = value.TextureId;
                productmaster.ManufacturingSiteId = value.ManufacturingSiteId;
                productmaster.IsMasterDocument = value.IsMasterDocument;
                productmaster.IsInformationvsMaster = value.IsInformationvsMaster;
                productmaster.FlavourId = value.FlavourId;
                productmaster.Phstandard = value.PhStandard;
                productmaster.Sgstandard = value.SgStandard;
                productmaster.ViscosityStandard = value.ViscosityStandard;
                productmaster.CreamPhmax = value.CreamPhmax;
                productmaster.CreamPhmin = value.CreamPhmin;
                productmaster.CreamSgmax = value.CreamSgmax;
                productmaster.CreamSgmin = value.CreamSgmin;
                productmaster.CreamViscosityMax = value.CreamViscosityMax;
                productmaster.CreamViscosityMin = value.CreamViscosityMin;
                productmaster.CreamPhstandard = value.CreamPhStandard;
                productmaster.CreamSgstandard = value.CreamSgStandard;
                productmaster.CreamViscosityStandard = value.CreamViscosityStandard;
                productmaster.CreamPhvariationId = value.CreamPhvariationId;
                productmaster.CreamPhvariationValue = value.CreamPhvariationValue;
                productmaster.CreamSgvariationId = value.CreamSgvariationId;
                productmaster.CreamSgvariationValue = value.CreamSgvariationValue;
                productmaster.CreamViscosityVariationId = value.CreamViscosityVariationId;
                productmaster.CreamViscosityVariationValue = value.CreamViscosityVariationValue;
                productmaster.LengthStandard = value.LengthStandard;
                productmaster.CoreSizeLengthStandard = value.CoreSizeLengthStandard;
                productmaster.CoatedSizeLengthStandard = value.CoatedSizeLengthStandard;
                productmaster.WidthStandard = value.WidthStandard;
                productmaster.CoreSizeWidthStandard = value.CoreSizeWidthStandard;
                productmaster.CoreWeightStandard = value.CoreWeightStandard;
                productmaster.CoreThicknessStandard = value.CoreThicknessStandard;
                productmaster.WeightStandard = value.WeightStandard;
                productmaster.CoatedSizeWidthStandard = value.CoatedSizeWidthStandard;
                productmaster.CoatedThicknessStandard = value.CoatedThicknessStandard;
                productmaster.CoatedWeightStandard = value.CoatedWeightStandard;
                productmaster.ThicknessStandard = value.ThicknessStandard;

                var shapeItems = _context.NavProductMultiple.Where(l => l.ProductMasterId == value.ProductMasterId).AsNoTracking().ToList();
                if (shapeItems.Count > 0)
                {
                    _context.NavProductMultiple.RemoveRange(shapeItems);
                }
                if (value.ShapeIds != null)
                {
                    value.ShapeIds.ForEach(c =>
                    {
                        var navProductMultiple = new NavProductMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavProductMultiple.Add(navProductMultiple);
                    });
                }
                var markingItems = _context.NavProductMultiple.Where(l => l.ProductMasterId == value.ProductMasterId).AsNoTracking().ToList();
                if (markingItems.Count > 0)
                {
                    _context.NavProductMultiple.RemoveRange(markingItems);
                }
                if (value.MarkingIds != null)
                {
                    value.MarkingIds.ForEach(c =>
                    {
                        var navMarkingId = new NavMarkingMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavMarkingMultiple.Add(navMarkingId);
                    });
                }
                var flavourItems = _context.NavProductShapeMultiple.Where(l => l.ProductMasterId == value.ProductMasterId).AsNoTracking().ToList();
                if (flavourItems.Count > 0)
                {
                    _context.NavProductShapeMultiple.RemoveRange(flavourItems);
                }
                if (value.FlavourIds != null)
                {
                    value.FlavourIds.ForEach(c =>
                    {
                        var navProductMultiple = new NavProductShapeMultiple
                        {
                            ApplicationMasterDetailId = c,
                            ProductMasterId = value.ProductMasterId,
                        };
                        productmaster.NavProductShapeMultiple.Add(navProductMultiple);
                    });
                }

                _context.SaveChanges();

                return value;
            }
            else
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNavProductMaster")]
        public void Delete(int id)
        {
            var productmaster = _context.NavProductMaster.SingleOrDefault(p => p.ProductMasterId == id);
            if (productmaster != null)
            {
                var navProductMultiples = _context.NavProductMultiple.Where(m => m.ProductMasterId == id).AsNoTracking().ToList();
                if (navProductMultiples.Any())
                {
                    _context.NavProductMultiple.RemoveRange(navProductMultiples);
                    _context.SaveChanges();
                }
                var flavourItems = _context.NavProductShapeMultiple.Where(m => m.ProductMasterId == id).AsNoTracking().ToList();
                if (flavourItems.Any())
                {
                    _context.NavProductShapeMultiple.RemoveRange(flavourItems);
                    _context.SaveChanges();
                }
                _context.NavProductMaster.Remove(productmaster);
                _context.SaveChanges();
            }
        }
    }
}