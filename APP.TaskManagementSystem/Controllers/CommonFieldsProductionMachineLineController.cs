﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonFieldsProductionMachineLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonFieldsProductionMachineLineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [Route("GetCommonFieldsProductionMachineLine")]
        public List<CommonFieldsProductionMachineLineModel> Get(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLine
                                                .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                                .AsNoTracking().ToList();

            List<CommonFieldsProductionMachineLineModel> commonFieldsProductionMachineLineModel = new List<CommonFieldsProductionMachineLineModel>();
            commonFieldsProductionMachineLine.ForEach(s =>
            {
                CommonFieldsProductionMachineLineModel commonFieldsProductionMachineLineModels = new CommonFieldsProductionMachineLineModel
                {
                    CommonFieldsProductionMachineLineId = s.CommonFieldsProductionMachineLineId,
                    CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                    QualifiedDate = s.QualifiedDate,
                    QualificationNotes = s.QualificationNotes,
                    FrequencyOfRequalificationYear = s.FrequencyOfRequalificationYear,
                    MinCapacity = s.MinCapacity,
                    MaxCapacity = s.MaxCapacity,
                    PreferCapacity = s.PreferCapacity,
                    CapacityUnitsId = s.CapacityUnitsId,
                    RecommendedManpower = s.RecommendedManpower,
                    SessionId = s.SessionId,
                    CapacityUnitsName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CapacityUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                commonFieldsProductionMachineLineModel.Add(commonFieldsProductionMachineLineModels);
            });
            return commonFieldsProductionMachineLineModel.Where(w => w.CommonFieldsProductionMachineId == id).OrderByDescending(a => a.CommonFieldsProductionMachineLineId).ToList();
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCommonFieldsProductionMachineLine")]
        public CommonFieldsProductionMachineLineModel Post(CommonFieldsProductionMachineLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var commonFieldsProductionMachineLine = new CommonFieldsProductionMachineLine
            {
                CommonFieldsProductionMachineId = value.CommonFieldsProductionMachineId,
                QualifiedDate = value.QualifiedDate,
                QualificationNotes = value.QualificationNotes,
                FrequencyOfRequalificationYear = value.FrequencyOfRequalificationYear,
                MinCapacity = value.MinCapacity,
                MaxCapacity = value.MaxCapacity,
                PreferCapacity = value.PreferCapacity,
                CapacityUnitsId = value.CapacityUnitsId,
                RecommendedManpower = value.RecommendedManpower,
                SessionId = SessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.CommonFieldsProductionMachineLine.Add(commonFieldsProductionMachineLine);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.CommonFieldsProductionMachineLineId = commonFieldsProductionMachineLine.CommonFieldsProductionMachineLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCommonFieldsProductionMachineLine")]
        public CommonFieldsProductionMachineLineModel Put(CommonFieldsProductionMachineLineModel value)
        {
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLine.SingleOrDefault(p => p.CommonFieldsProductionMachineLineId == value.CommonFieldsProductionMachineLineId);
            commonFieldsProductionMachineLine.CommonFieldsProductionMachineId = value.CommonFieldsProductionMachineId;
            commonFieldsProductionMachineLine.QualifiedDate = value.QualifiedDate;
            commonFieldsProductionMachineLine.FrequencyOfRequalificationYear = value.FrequencyOfRequalificationYear;
            commonFieldsProductionMachineLine.QualificationNotes = value.QualificationNotes;
            commonFieldsProductionMachineLine.MaxCapacity = value.MaxCapacity;
            commonFieldsProductionMachineLine.MinCapacity = value.MinCapacity;
            commonFieldsProductionMachineLine.PreferCapacity = value.PreferCapacity;
            commonFieldsProductionMachineLine.CapacityUnitsId = value.CapacityUnitsId;
            commonFieldsProductionMachineLine.RecommendedManpower = value.RecommendedManpower;
            commonFieldsProductionMachineLine.SessionId = value.SessionId;
            commonFieldsProductionMachineLine.ModifiedByUserId = value.ModifiedByUserID;
            commonFieldsProductionMachineLine.ModifiedDate = DateTime.Now;
            commonFieldsProductionMachineLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCommonFieldsProductionMachineLine")]
        public void Delete(int id)
        {
            var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLine.SingleOrDefault(p => p.CommonFieldsProductionMachineLineId == id);
            if (commonFieldsProductionMachineLine != null)
            {
                var machineDocumentbandlist = _context.CommonFieldsProductionMachineDocument.Where(p => p.SessionId == commonFieldsProductionMachineLine.SessionId).Select(s => s.MachineDocumentId).ToList();
                if (machineDocumentbandlist != null)
                {
                    machineDocumentbandlist.ForEach(h =>
                    {
                        var machineDocumentbanditem = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == h);
                        _context.CommonFieldsProductionMachineDocument.Remove(machineDocumentbanditem);
                        _context.SaveChanges();
                    });

                }
                _context.CommonFieldsProductionMachineLine.Remove(commonFieldsProductionMachineLine);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("UploadMachineDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new CommonFieldsProductionMachineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.CommonFieldsProductionMachineDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetMachineDocument")]
        public List<CommonFieldsProductionMachineDocumentModel> GetMachineDocument(Guid? SessionId)
        {
            var query = _context.CommonFieldsProductionMachineDocument.Select(s => new CommonFieldsProductionMachineDocumentModel
            {
                SessionId = s.SessionId,
                MachineDocumentId = s.MachineDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
            }).Where(l => l.SessionId == SessionId).OrderByDescending(o => o.MachineDocumentId).ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadMachineDocument")]
        public IActionResult DownLoadMachineDocument(long id)
        {
            var document = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(t => t.MachineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteMachineDocument")]
        public void MachineDocuments(int id)
        {

            var query = string.Format("delete from CommonFieldsProductionMachineDocument Where MachineDocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
            //var machineDocument = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == id);
            //if (machineDocument != null)
            //{
            //    _context.CommonFieldsProductionMachineDocument.Remove(machineDocument);
            //    _context.SaveChanges();
            //}
        }

        //
        [HttpPost]
        [Route("UploadMachineDocument")]
        public IActionResult UploadMachineDocument(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new CommonFieldsProductionMachineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId
                };

                _context.CommonFieldsProductionMachineDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetMachineDocumentBySessionID")]
        public List<DocumentsModel> GetMachineDocumentBySessionIDs(Guid? sessionId, int userId)
        {//
            var query = _context.CommonFieldsProductionMachineDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.MachineDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadMachineDocuments")]
        public IActionResult DownLoadMachineDocuments(long id)
        {
            var document = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(t => t.MachineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteMachineDocuments")]
        public void DeleteMachineDocuments(int id)
        {

            var query = string.Format("delete from CommonFieldsProductionMachineDocument Where MachineDocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
            //var query = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == id);
            //if (query != null)
            //{
            //    _context.CommonFieldsProductionMachineDocument.Remove(query);
            //    _context.SaveChanges();
            //}
        }

        //
    }
}