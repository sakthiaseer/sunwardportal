﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductGroupingNavDifferenceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductGroupingNavDifferenceController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductGroupingNavDifferenceItems")]
        public List<ProductGroupingNavDifferenceModel> GetProductGroupingNavItems(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProductGroupingNavDifferenceModel> productGroupingNavDifferenceModels = new List<ProductGroupingNavDifferenceModel>();
            var productGroupingNavDifferences = _context.ProductGroupingNavDifference.Include("AddedByUser").Include("ModifiedByUser").Where(p => p.ProductGroupingNavId == id).OrderByDescending(o => o.ProductGroupingNavDifferenceId).AsNoTracking().ToList();
            if (productGroupingNavDifferences != null && productGroupingNavDifferences.Count > 0)
            {
                productGroupingNavDifferences.ForEach(s =>
                {
                    ProductGroupingNavDifferenceModel productGroupingNavDifferenceModel = new ProductGroupingNavDifferenceModel();
                    productGroupingNavDifferenceModel.ProductGroupingNavDifferenceId = s.ProductGroupingNavDifferenceId;
                    productGroupingNavDifferenceModel.ProductGroupingNavId = s.ProductGroupingNavId;
                    productGroupingNavDifferenceModel.DifferenceInfo = s.DifferenceInfo;
                    productGroupingNavDifferenceModel.TypeOfDifferenceId = s.TypeOfDifferenceId;
                    productGroupingNavDifferenceModel.TypeOfDifference = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.TypeOfDifferenceId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.TypeOfDifferenceId).Value : string.Empty;
                    productGroupingNavDifferenceModel.AddedByUserID = s.AddedByUserId;
                    productGroupingNavDifferenceModel.ModifiedByUserID = s.ModifiedByUserId;
                    productGroupingNavDifferenceModel.StatusCodeID = s.StatusCodeId;
                    productGroupingNavDifferenceModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    productGroupingNavDifferenceModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    productGroupingNavDifferenceModel.AddedDate = s.AddedDate;
                    productGroupingNavDifferenceModel.ModifiedDate = s.ModifiedDate;
                    productGroupingNavDifferenceModels.Add(productGroupingNavDifferenceModel);


                });
            }

            return productGroupingNavDifferenceModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertProductGroupingNavDifference")]
        public ProductGroupingNavDifferenceModel Post(ProductGroupingNavDifferenceModel value)
        {
            var productGroupingNavDifference = new ProductGroupingNavDifference
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProductGroupingNavId = value.ProductGroupingNavId,
                TypeOfDifferenceId = value.TypeOfDifferenceId,
                DifferenceInfo = value.DifferenceInfo
            };
            _context.ProductGroupingNavDifference.Add(productGroupingNavDifference);
            _context.SaveChanges();
            value.ProductGroupingNavDifferenceId = productGroupingNavDifference.ProductGroupingNavDifferenceId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateProductGroupingNavDifference")]
        public ProductGroupingNavDifferenceModel Put(ProductGroupingNavDifferenceModel value)
        {
            var productGroupingNavDifference = _context.ProductGroupingNavDifference.SingleOrDefault(p => p.ProductGroupingNavDifferenceId == value.ProductGroupingNavDifferenceId);
            productGroupingNavDifference.ModifiedByUserId = value.ModifiedByUserID;
            productGroupingNavDifference.ModifiedDate = DateTime.Now;
            productGroupingNavDifference.StatusCodeId = value.StatusCodeID.Value;
            productGroupingNavDifference.ProductGroupingNavId = value.ProductGroupingNavId;
            productGroupingNavDifference.TypeOfDifferenceId = value.TypeOfDifferenceId;
            productGroupingNavDifference.DifferenceInfo = value.DifferenceInfo;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteProductGroupingNavDifference")]
        public void Delete(int id)
        {
            var productGroupingNavDifference = _context.ProductGroupingNavDifference.SingleOrDefault(p => p.ProductGroupingNavDifferenceId == id);
            if (productGroupingNavDifference != null)
            {
                _context.ProductGroupingNavDifference.Remove(productGroupingNavDifference);
                _context.SaveChanges();
            }
        }
    }
}
