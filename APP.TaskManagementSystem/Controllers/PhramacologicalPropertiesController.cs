﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PhramacologicalPropertiesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PhramacologicalPropertiesController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetPhramacologicalProperties")]
        public List<PhramacologicalPropertiesModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            List<PhramacologicalPropertiesModel> phramacologicalPropertiesModels = new List<PhramacologicalPropertiesModel>();
            var phramacologicalproperties = _context.PhramacologicalProperties
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("RegisterationCode")
                .Include(s => s.FinishProduct).OrderByDescending(o => o.PharmacologicalpropertiesId).AsNoTracking().ToList();
            if (phramacologicalproperties != null && phramacologicalproperties.Count > 0)
            {
                phramacologicalproperties.ForEach(s =>
                {
                    PhramacologicalPropertiesModel phramacologicalPropertiesModel = new PhramacologicalPropertiesModel
                    {
                        PharmacologicalpropertiesId = s.PharmacologicalpropertiesId,
                        FinishProductId = s.FinishProductId,
                        RegisterationCodeId = s.RegisterationCodeId,
                        RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)).Value : "",
                        IsMasterDocuement = s.IsMasterDocuement,
                        IsInformationvsMaster = s.IsInformationvsMaster,
                        Pharmacodynamics = s.Pharmacodynamics,
                        Pharmacokinetics = s.Pharmacokinetics,
                        Indication = s.Indication,
                        RecommendedDose = s.RecommendedDose,
                        RouteOfAdministration = s.RouteOfAdministration,
                        Contraindications = s.Contraindications,
                        Warningandprecautions = s.Warningandprecautions,
                        Intractionwithothermedicaments = s.Intractionwithothermedicaments,
                        Pregnancyandlactations = s.Pregnancyandlactations,
                        Sideeffects = s.Sideeffects,
                        Preclinicalsafetydata = s.Preclinicalsafetydata,
                        Symptomsandoverdose = s.Symptomsandoverdose,
                        Driveandusemachine = s.Driveandusemachine,
                        StatusCodeID = s.StatusCodeId,
                        Instructionforuse = s.Instructionforuse,
                        Storagecondition = s.Storagecondition,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    };
                    phramacologicalPropertiesModels.Add(phramacologicalPropertiesModel);
                });
            }

            return phramacologicalPropertiesModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PhramacologicalPropertiesModel> GetData(SearchModel searchModel)
        {
            var phramacologicalproperties = new PhramacologicalProperties();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).FirstOrDefault();
                        break;
                    case "Last":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).LastOrDefault();
                        break;
                    case "Next":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).LastOrDefault();
                        break;
                    case "Previous":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).FirstOrDefault();
                        break;
                    case "Last":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).LastOrDefault();
                        break;
                    case "Next":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderBy(o => o.PharmacologicalpropertiesId).FirstOrDefault(s => s.PharmacologicalpropertiesId > searchModel.Id);
                        break;
                    case "Previous":
                        phramacologicalproperties = _context.PhramacologicalProperties.OrderByDescending(o => o.PharmacologicalpropertiesId).FirstOrDefault(s => s.PharmacologicalpropertiesId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<PhramacologicalPropertiesModel>(phramacologicalproperties);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var finishProductItem = _context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.FinishProductId).FinishProductId;
                if (finishProductItem != null)
                {
                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }
            }
            return result;
        }
        [HttpPost]
        [Route("InsertPhramacologicalProperties")]
        public PhramacologicalPropertiesModel Post(PhramacologicalPropertiesModel value)
        {
            var phramacologicalproperties = new PhramacologicalProperties
            {

                FinishProductId = value.FinishProductId,
                IsMasterDocuement = value.IsMasterDocuement,
                IsInformationvsMaster = value.IsInformationvsMaster,
                Pharmacodynamics = value.Pharmacodynamics,
                Pharmacokinetics = value.Pharmacokinetics,
                Indication = value.Indication,
                RecommendedDose = value.RecommendedDose,
                RouteOfAdministration = value.RouteOfAdministration,
                Contraindications = value.Contraindications,
                Warningandprecautions = value.Warningandprecautions,
                Intractionwithothermedicaments = value.Intractionwithothermedicaments,
                Pregnancyandlactations = value.Pregnancyandlactations,
                Sideeffects = value.Sideeffects,
                Symptomsandoverdose = value.Symptomsandoverdose,
                Driveandusemachine = value.Driveandusemachine,
                Preclinicalsafetydata = value.Preclinicalsafetydata,
                Instructionforuse = value.Instructionforuse,
                Storagecondition = value.Storagecondition,
                RegisterationCodeId = value.RegisterationCodeId,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.PhramacologicalProperties.Add(phramacologicalproperties);
            _context.SaveChanges();
            value.PharmacologicalpropertiesId = phramacologicalproperties.PharmacologicalpropertiesId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePhramacologicalProperties")]
        public PhramacologicalPropertiesModel Put(PhramacologicalPropertiesModel value)
        {
            var phramacologicalproperties = _context.PhramacologicalProperties.SingleOrDefault(p => p.PharmacologicalpropertiesId == value.PharmacologicalpropertiesId);
            phramacologicalproperties.FinishProductId = value.FinishProductId;
            phramacologicalproperties.RegisterationCodeId = value.RegisterationCodeId;
            phramacologicalproperties.IsMasterDocuement = value.IsMasterDocuement;
            phramacologicalproperties.IsInformationvsMaster = value.IsInformationvsMaster;
            phramacologicalproperties.Pharmacodynamics = value.Pharmacodynamics;
            phramacologicalproperties.Pharmacokinetics = value.Pharmacokinetics;
            phramacologicalproperties.Indication = value.Indication;
            phramacologicalproperties.RecommendedDose = value.RecommendedDose;
            phramacologicalproperties.RouteOfAdministration = value.RouteOfAdministration;
            phramacologicalproperties.Contraindications = value.Contraindications;
            phramacologicalproperties.Warningandprecautions = value.Warningandprecautions;
            phramacologicalproperties.Intractionwithothermedicaments = value.Intractionwithothermedicaments;
            phramacologicalproperties.Pregnancyandlactations = value.Pregnancyandlactations;
            phramacologicalproperties.Sideeffects = value.Sideeffects;
            phramacologicalproperties.Symptomsandoverdose = value.Symptomsandoverdose;
            phramacologicalproperties.Driveandusemachine = value.Driveandusemachine;
            phramacologicalproperties.Preclinicalsafetydata = value.Preclinicalsafetydata;
            phramacologicalproperties.Instructionforuse = value.Instructionforuse;
            phramacologicalproperties.Storagecondition = value.Storagecondition;
            phramacologicalproperties.ModifiedByUserId = value.ModifiedByUserID;
            phramacologicalproperties.ModifiedDate = DateTime.Now;
            phramacologicalproperties.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("IsMasterExist")]
        public bool IsMasterExist(int? id)
        {
            var list = _context.PhramacologicalProperties.Where(s => s.FinishProductId == id && s.IsMasterDocuement == true).FirstOrDefault();
            if (list != null)
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
            else
            {
                return false;
            }
        }
        [HttpGet]
        [Route("getPhramacologicalPropertiesByFinishProduct")]
        public List<PhramacologicalPropertiesModel> getPhramacologicalPropertiesByFinishProduct(int? id)
        {
            List<PhramacologicalPropertiesModel> phramacologicalPropertiesModels = new List<PhramacologicalPropertiesModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var phramacologicalproperties = _context.PhramacologicalProperties
               .Include(a => a.AddedByUser)
               .Include(a => a.ModifiedByUser)
               .Include(a => a.StatusCode)
                .Include(a => a.RegisterationCode)
               .Include(s => s.FinishProduct).OrderByDescending(o => o.PharmacologicalpropertiesId).Where(w => w.FinishProductId == id).AsNoTracking().ToList();

            if (phramacologicalproperties != null && phramacologicalproperties.Count > 0)
            {
                phramacologicalproperties.ForEach(s =>
                {
                    PhramacologicalPropertiesModel phramacologicalPropertiesModel = new PhramacologicalPropertiesModel
                    {
                        PharmacologicalpropertiesId = s.PharmacologicalpropertiesId,
                        FinishProductId = s.FinishProductId,
                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProduct.FinishProductId).ProductId)).Value : "",
                        //ProductName= applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId).Value : "",
                        IsMasterDocuement = s.IsMasterDocuement,
                        RegisterationCodeId = s.RegisterationCodeId,
                        RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                        IsInformationvsMaster = s.IsInformationvsMaster,
                        Pharmacodynamics = s.Pharmacodynamics,
                        Pharmacokinetics = s.Pharmacokinetics,
                        Indication = s.Indication,
                        RecommendedDose = s.RecommendedDose,
                        RouteOfAdministration = s.RouteOfAdministration,
                        Contraindications = s.Contraindications,
                        Warningandprecautions = s.Warningandprecautions,
                        Intractionwithothermedicaments = s.Intractionwithothermedicaments,
                        Pregnancyandlactations = s.Pregnancyandlactations,
                        Preclinicalsafetydata = s.Preclinicalsafetydata,
                        Instructionforuse = s.Instructionforuse,
                        Storagecondition = s.Storagecondition,
                        Sideeffects = s.Sideeffects,
                        Symptomsandoverdose = s.Symptomsandoverdose,
                        Driveandusemachine = s.Driveandusemachine,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                    };
                    phramacologicalPropertiesModels.Add(phramacologicalPropertiesModel);
                });
            }

            return phramacologicalPropertiesModels;
        }
        [HttpDelete]
        [Route("DeletePhramacologicalProperties")]
        public void Delete(int id)
        {
            var phramacologicalproperties = _context.PhramacologicalProperties.SingleOrDefault(p => p.PharmacologicalpropertiesId == id);
            if (phramacologicalproperties != null)
            {
                _context.PhramacologicalProperties.Remove(phramacologicalproperties);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("GetPharmacologicalInformationReports")]
        public List<PharmacologicalReportModel> GetPharmacologicalInformationReports(PharmacologicalReportParam pharmacologicalReportParam)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var pharamacological = _context.PharmacologicalCategory.AsNoTracking().ToList();
            var EquivalentBrand = _context.FinishProductEquivalentBrandCategory.AsNoTracking().ToList();
            var chemicalSubGroup = _context.ProductChemicalSubGroup.AsNoTracking().ToList();
            var GenericList = _context.FinisihProductGenericCategory.AsNoTracking().ToList();
            List<PharmacologicalReportModel> pharmacologicalReportModels = new List<PharmacologicalReportModel>();
            var phramacologicalproperties = _context.PhramacologicalProperties
                .Include(s => s.AddedByUser)
                .Include(s => s.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.RegisterationCode)
                .Include(s => s.FinishProduct)
                .Include(s => s.FinishProduct.FinishProduct).AsNoTracking().ToList();
            phramacologicalproperties.ForEach(s =>
            {
                PharmacologicalReportModel pharmacologicalReportModel = new PharmacologicalReportModel
                {
                    PharmacologicalpropertiesId = s.PharmacologicalpropertiesId,
                    FinishProductId = s.FinishProductId,
                    ManufacturingSiteId = s.FinishProduct != null ? s.FinishProduct.FinishProduct != null ? s.FinishProduct.FinishProduct.ManufacturingSiteId : null : null,
                    ProductName = applicationmasterdetail != null && s.FinishProduct != null && s.FinishProduct.FinishProduct != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.ProductId.Value) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.ProductId.Value).Value : "",
                    ManufacturingSiteName = applicationmasterdetail != null && s.FinishProduct != null && s.FinishProduct.FinishProduct != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.ManufacturingSiteId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.ManufacturingSiteId).Value : "",
                    RegistrationFileName = s.FinishProduct != null ? s.FinishProduct.FinishProduct != null ? s.FinishProduct.FinishProduct.RegistrationFileName : "" : "",
                    Atccode = s.FinishProduct != null ? s.FinishProduct.FinishProduct != null ? s.FinishProduct.FinishProduct.Atccode : "" : "",
                    Gtinno = s.FinishProduct != null ? s.FinishProduct.FinishProduct != null ? s.FinishProduct.FinishProduct.Gtinno : "" : "",
                    DrugClassification = s.FinishProduct != null && s.FinishProduct.FinishProduct != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.DrugClassificationId).Select(a => a.Value).FirstOrDefault() : "",
                    RegistrationProductCategoryName = s.FinishProduct != null && s.FinishProduct.FinishProduct != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.FinishProduct.FinishProduct.RegistrationProductCategory).Select(a => a.Value).FirstOrDefault() : "",
                    PharmacologicalCategory = String.Join(",", pharamacological.Where(p => p.FinishProductId == s.FinishProductId).Select(p => applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == p.PharmacologicalCategoryId).Select(a => a.Value).FirstOrDefault()).ToList()),
                    EquvalentBrand = String.Join(",", EquivalentBrand.Where(p => p.FinishProductId == s.FinishProductId).Select(p => applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == p.EquivalentBrandCategoryId).Select(a => a.Value).FirstOrDefault()).ToList()),
                    GenericName = String.Join(",", GenericList.Where(p => p.FinishProductId == s.FinishProductId).Select(p => applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == p.GenericCategoryId).Select(a => a.Value).FirstOrDefault()).ToList()),
                    ChemicalSubgroup = String.Join(",", chemicalSubGroup.Where(p => p.FinishProductId == s.FinishProductId).Select(p => applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == p.ChemicalSubGroupId).Select(a => a.Value).FirstOrDefault()).ToList()),
                };
                pharmacologicalReportModels.Add(pharmacologicalReportModel);
            });

            if (pharmacologicalReportParam.ManufacturingSiteIds.Count > 0)
            {
                pharmacologicalReportModels = pharmacologicalReportModels.Where(d => pharmacologicalReportParam.ManufacturingSiteIds.Contains(d.ManufacturingSiteId)).ToList();
            }
            return pharmacologicalReportModels.OrderByDescending(f => f.PharmacologicalpropertiesId).ToList();
        }
    }
}