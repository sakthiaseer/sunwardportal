﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CountryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CountryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCountries")]
        public List<CountryModel> Get()
        {
            var country = _context.Country.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser)
                            .AsNoTracking().ToList();
            List<CountryModel> countryModel = new List<CountryModel>();
            country.ForEach(s =>
            {
                CountryModel countryModels = new CountryModel
                {
                    CountryID = s.CountryId,
                    Name = s.Name,
                    Code = s.Code,
                    //StatusCode=s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                countryModel.Add(countryModels);
            });
            return countryModel.OrderByDescending(a => a.CountryID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetCountries/{id:int}")]
        public ActionResult<CountryModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var Country = _context.Country.SingleOrDefault(p => p.CountryId == id.Value);
            var result = _mapper.Map<CountryModel>(Country);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CountryModel> GetData(SearchModel searchModel)
        {
            var Country = new Country();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).FirstOrDefault();
                        break;
                    case "Last":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).LastOrDefault();
                        break;
                    case "Next":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).LastOrDefault();
                        break;
                    case "Previous":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).FirstOrDefault();
                        break;
                    case "Last":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).LastOrDefault();
                        break;
                    case "Next":
                        Country = _context.Country.OrderBy(o => o.CountryId).FirstOrDefault(s => s.CountryId > searchModel.Id);
                        break;
                    case "Previous":
                        Country = _context.Country.OrderByDescending(o => o.CountryId).FirstOrDefault(s => s.CountryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CountryModel>(Country);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCountry")]
        public CountryModel Post(CountryModel value)
        {
            var Country = new Country
            {
                //CountryId = value.CountryID,
                Name = value.Name,
                Code = value.Code,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };
            _context.Country.Add(Country);
            _context.SaveChanges();
            value.CountryID = Country.CountryId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCountry")]
        public CountryModel Put(CountryModel value)
        {
            var Country = _context.Country.SingleOrDefault(p => p.CountryId == value.CountryID);
            //Country.CountryId = value.CountryID;
            Country.ModifiedByUserId = value.ModifiedByUserID;
            Country.ModifiedDate = DateTime.Now;
            Country.Name = value.Name;
            Country.Code = value.Code;
            //Country.AddedByUserId = value.AddedByUserID;
            // Country.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            Country.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCountry")]
        public void Delete(int id)
        {
            var Country = _context.Country.SingleOrDefault(p => p.CountryId == id);
            if (Country != null)
            {
                _context.Country.Remove(Country);
                _context.SaveChanges();
            }
        }
    }
}