﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingDividerController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingDividerController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingDivider")]
        public List<CommonPackagingDividerModel> Get()
        {
            var commonPackagingDivider = _context.CommonPackagingDivider
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingDividerModel> commonPackagingDividerModel = new List<CommonPackagingDividerModel>();
            if (commonPackagingDivider.Count > 0)
            {
                List<long?> masterIds = commonPackagingDivider.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingDivider.ForEach(s =>
                {
                    CommonPackagingDividerModel commonPackagingDividerModels = new CommonPackagingDividerModel
                    {
                        DividerSpecificationId = s.DividerSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.DividerSpecificationId == s.DividerSpecificationId).Select(b => b.UseForItemId).ToList(),
                        DividerTo = s.DividerTo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    commonPackagingDividerModel.Add(commonPackagingDividerModels);
                });
            }
            return commonPackagingDividerModel.OrderByDescending(a => a.DividerSpecificationId).ToList();
        }

        [HttpPost]
        [Route("GetCommonPackagingDividerByRefNo")]
        public List<CommonPackagingDividerModel> GetCommonPackagingDividerByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingDivider = _context.CommonPackagingDivider
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).Where(w => w.DividerSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingDivider = commonPackagingDivider.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingDivider = commonPackagingDivider.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingDividers = commonPackagingDivider.AsNoTracking().ToList();
            List<CommonPackagingDividerModel> commonPackagingDividerModel = new List<CommonPackagingDividerModel>();
            if (commonPackagingDividers.Count > 0)
            {
                List<long?> masterIds = commonPackagingDividers.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingDividers.ForEach(s =>
                {
                    CommonPackagingDividerModel commonPackagingDividerModels = new CommonPackagingDividerModel
                    {
                        DividerSpecificationId = s.DividerSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        StatusCodeID = s.StatusCodeId,
                        DividerTo = s.DividerTo,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.DividerSpecificationId == s.DividerSpecificationId).Select(b => b.UseForItemId).ToList(),
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    commonPackagingDividerModel.Add(commonPackagingDividerModels);
                });
            }
            return commonPackagingDividerModel.OrderByDescending(o => o.DividerSpecificationId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingDividerModel> GetData(SearchModel searchModel)
        {
            var commonPackagingDivider = new CommonPackagingDivider();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderBy(o => o.DividerSpecificationId).FirstOrDefault(s => s.DividerSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingDivider = _context.CommonPackagingDivider.OrderByDescending(o => o.DividerSpecificationId).FirstOrDefault(s => s.DividerSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingDividerModel>(commonPackagingDivider);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingDivider")]
        public CommonPackagingDividerModel Post(CommonPackagingDividerModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingDivider" });
            var commonPackagingDivider = new CommonPackagingDivider
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                MeasurementWidth = value.MeasurementWidth,
                MeasurementLength = value.MeasurementLength,
                DividerTo = value.DividerTo,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingDivider.Add(commonPackagingDivider);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingDivider.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingDivider.ProfileLinkReferenceNo;
            value.DividerSpecificationId = commonPackagingDivider.DividerSpecificationId;
            value.PackagingItemName = UpdateCommonPackage(value);
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.DividerSpecificationId == value.DividerSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        DividerSpecificationId = value.DividerSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingDividerModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    if (value.MeasurementLength != null && value.MeasurementLength > 0)
                    {
                        itemName += value.MeasurementLength + "mm" + "(L)" + " ";
                    }
                    if (value.MeasurementWidth != null && value.MeasurementWidth > 0)
                    {
                        itemName = itemName + "X" + " " + value.MeasurementWidth + "mm" + "(W)" + " ";
                    }
                    if (value.DividerTo != null && value.DividerTo > 0)
                    {
                        itemName = itemName + "(" + value.DividerTo + ") Divider";
                    }
                    //itemName = value.MeasurementLength + "mm" + "(L)" + " " + "X" + " " + value.MeasurementWidth + "mm" + "(W)" + " " + "(" + value.DividerTo + ") Divider";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingDivider")]
        public CommonPackagingDividerModel Put(CommonPackagingDividerModel value)
        {
            var commonPackagingDivider = _context.CommonPackagingDivider.SingleOrDefault(p => p.DividerSpecificationId == value.DividerSpecificationId);
            commonPackagingDivider.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingDivider.Set = value.Set;
            commonPackagingDivider.MeasurementWidth = value.MeasurementWidth;
            commonPackagingDivider.MeasurementLength = value.MeasurementLength;
            commonPackagingDivider.DividerTo = value.DividerTo;
            commonPackagingDivider.VersionControl = value.VersionControl;
            commonPackagingDivider.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingDivider.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingDivider.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingDivider.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingDivider.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.DividerSpecificationId == value.DividerSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.DividerSpecificationId == value.DividerSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        DividerSpecificationId = value.DividerSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingDivider")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingDivider = _context.CommonPackagingDivider.Where(p => p.DividerSpecificationId == id).FirstOrDefault();
                if (commonPackagingDivider != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.DividerSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingDivider.Remove(commonPackagingDivider);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}