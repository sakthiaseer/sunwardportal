﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IPIRController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public IPIRController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetIPIRItems")]
        public List<IPIRModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ipirs = _context.Ipir
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(p => p.Product)
                .Include(d => d.Department)
                .Include(d => d.IpirissueRelatedMultiple)
                .Include(d => d.Machine)
                .Include(l => l.Location).AsNoTracking()
                .ToList();
            List<IPIRModel> iPIRModels = new List<IPIRModel>();
            ipirs.ForEach(s =>
            {
                IPIRModel iPIRModel = new IPIRModel
                {
                    Ipirid = s.Ipirid,
                    CompanyId = s.CompanyId,
                    CompanyName = s.Company?.PlantCode,
                    Ipirno = s.Ipirno,
                    Date = s.Date,
                    Time = s.Time,
                    ReportBy = s.ReportBy,
                    DetectedBy = s.DetectedBy,
                    DepartmentId = s.DepartmentId,
                    DepartmentName = s.Department?.Name,
                    LocationId = s.LocationId,
                    LocationName = s.Location?.Name,
                    MachineId = s.MachineId,
                    ProductId = s.ProductId,
                    ProductName = s.Product?.No,
                    MachineName = s.Machine?.NameOfTheMachine,
                    BatchNoId = s.BatchNoId,
                    IssueDescription = s.IssueDescription,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    issueRelatedIds = s.IpirissueRelatedMultiple?.Where(i => i.Ipirid == s.Ipirid).Select(i => i.IssueRelatedId).ToList(),
                    issueTitleIds = s.IpirissueTitleMultiple?.Where(i => i.Ipirid == s.Ipirid).Select(i => i.IssueTitleId).ToList(),
                };
                iPIRModels.Add(iPIRModel);
            });
            return iPIRModels.OrderByDescending(a => a.Ipirid).ToList();
        }

        [HttpPost()]
        [Route("GetIPIRData")]
        public ActionResult<IPIRModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var ipir = new Ipir();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                    case "Last":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Next":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Previous":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                    case "Last":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Next":
                        ipir = _context.Ipir.OrderBy(o => o.Ipirid).FirstOrDefault(s => s.Ipirid > searchModel.Id);
                        break;
                    case "Previous":
                        ipir = _context.Ipir.OrderByDescending(o => o.Ipirid).FirstOrDefault(s => s.Ipirid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<IPIRModel>(ipir);
            return result;
        }

        [HttpPost]
        [Route("InsertIPIR")]
        public IPIRModel Post(IPIRModel value)
        {
            var ipir = new Ipir
            {
                CompanyId = value.CompanyId,
                Ipirno = value.Ipirno,
                Date = value.Date,
                Time = value.Time,
                ReportBy = value.ReportBy,
                DetectedBy = value.DetectedBy,
                DepartmentId = value.DepartmentId,
                LocationId = value.LocationId,
                MachineId = value.MachineId,
                ProductId = value.ProductId,
                BatchNoId = value.BatchNoId,

                IssueDescription = value.IssueDescription,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.Ipir.Add(ipir);
            _context.SaveChanges();
            if (value.issueRelatedIds != null && value.issueRelatedIds != null)
            {
                value.issueRelatedIds.ForEach(i =>
                {
                    var existing = _context.IpirissueRelatedMultiple.Where(issue => issue.Ipirid == value.Ipirid && issue.IssueRelatedId == i).FirstOrDefault();
                    if (existing == null)
                    {
                        IpirissueRelatedMultiple ipirissueRelatedMultiple = new IpirissueRelatedMultiple
                        {
                            Ipirid = value.Ipirid,
                            IssueRelatedId = i,


                        };
                        _context.IpirissueRelatedMultiple.Add(ipirissueRelatedMultiple);
                        _context.SaveChanges();
                    }
                });
            }
            if (value.issueTitleIds != null && value.issueTitleIds != null)
            {
                value.issueTitleIds.ForEach(i =>
                {
                    var existing = _context.IpirissueTitleMultiple.Where(issue => issue.Ipirid == value.Ipirid && issue.IssueTitleId == i).FirstOrDefault();
                    if (existing == null)
                    {
                        IpirissueTitleMultiple ipirissueTitleMultiple = new IpirissueTitleMultiple
                        {
                            Ipirid = value.Ipirid,
                            IssueTitleId = i,


                        };
                        _context.IpirissueTitleMultiple.Add(ipirissueTitleMultiple);
                        _context.SaveChanges();
                    }
                });
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateIPIR")]
        public IPIRModel Put(IPIRModel value)
        {
            var ipir = _context.Ipir.SingleOrDefault(p => p.Ipirid == value.Ipirid);
            ipir.CompanyId = value.CompanyId;
            ipir.Ipirno = value.Ipirno;
            ipir.Date = value.Date;
            ipir.Time = value.Time;
            ipir.ReportBy = value.ReportBy;
            ipir.DetectedBy = value.DetectedBy;
            ipir.DepartmentId = value.DepartmentId;
            ipir.LocationId = value.LocationId;
            ipir.MachineId = value.MachineId;
            ipir.ProductId = value.ProductId;
            ipir.BatchNoId = value.BatchNoId;
            ipir.IssueDescription = value.IssueDescription;
            ipir.StatusCodeId = value.StatusCodeID.Value;
            ipir.ModifiedByUserId = value.ModifiedByUserID;
            ipir.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (value.issueRelatedIds != null && value.issueRelatedIds != null)
            {
                value.issueRelatedIds.ForEach(i =>
                {
                    var existing = _context.IpirissueRelatedMultiple.Where(issue => issue.Ipirid == value.Ipirid && issue.IssueRelatedId == i).FirstOrDefault();
                    if (existing == null)
                    {
                        IpirissueRelatedMultiple ipirissueRelatedMultiple = new IpirissueRelatedMultiple
                        {
                            Ipirid = value.Ipirid,
                            IssueRelatedId = i,


                        };
                        _context.IpirissueRelatedMultiple.Add(ipirissueRelatedMultiple);
                        _context.SaveChanges();
                    }
                });
            }
            if (value.issueTitleIds != null && value.issueTitleIds != null)
            {
                value.issueTitleIds.ForEach(i =>
                {
                    var existing = _context.IpirissueTitleMultiple.Where(issue => issue.Ipirid == value.Ipirid && issue.IssueTitleId == i).FirstOrDefault();
                    if (existing == null)
                    {
                        IpirissueTitleMultiple ipirissueTitleMultiple = new IpirissueTitleMultiple
                        {
                            Ipirid = value.Ipirid,
                            IssueTitleId = i,


                        };
                        _context.IpirissueTitleMultiple.Add(ipirissueTitleMultiple);
                        _context.SaveChanges();
                    }
                });
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteIPIR")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var ipir = _context.Ipir.Where(p => p.Ipirid == id).FirstOrDefault();
                if (ipir != null)
                {

                    _context.Ipir.Remove(ipir);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteIPIRLine")]
        public ActionResult<string> DeleteIPIRLine(int id)
        {
            try
            {

                var ipirlines = _context.Ipirline.Where(p => p.IpirlineId == id).AsNoTracking().FirstOrDefault();
                if (ipirlines != null)
                {
                    var ipirlinesmultiple = _context.IpirissueTitleMultiple.Where(p => p.Ipirid == id).AsNoTracking().ToList();
                    if (ipirlinesmultiple != null)
                    {
                        _context.IpirissueTitleMultiple.RemoveRange(ipirlinesmultiple);
                        _context.SaveChanges();
                    }
                    var issuerelated = _context.IpirissueRelatedMultiple.Where(p => p.Ipirid == id).AsNoTracking().ToList();
                    if (issuerelated != null)
                    {
                        _context.IpirissueRelatedMultiple.RemoveRange(issuerelated);
                        _context.SaveChanges();
                    }
                    var sessionIDDocs = _context.IpirlineDocument.Where(s => s.SessionId == ipirlines.AttachSessionId).AsNoTracking().ToList();
                    if (sessionIDDocs != null && sessionIDDocs.Count > 0)
                    {
                        _context.IpirlineDocument.RemoveRange(sessionIDDocs);
                        _context.SaveChanges();
                    }
                    _context.Ipirline.Remove(ipirlines);
                    _context.SaveChanges();
                }


                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [Route("GetIPIRLinesByID")]
        public List<IPIRLineModel> GetIPIRLine(int? id)
        {
            var ipirlines = _context.Ipirline.Include(p => p.Pic).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<IPIRLineModel> iPIRLineModels = new List<IPIRLineModel>();
            ipirlines.ForEach(s =>
            {
                IPIRLineModel iPIRLineModel = new IPIRLineModel
                {
                    IpirlineId = s.IpirlineId,
                    Ipirid = s.Ipirid,
                    AssignTo = s.AssignTo,
                    Ipiraction = s.Ipiraction,
                    IpiractionName = s.Ipiraction != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.Ipiraction).Select(m => m.Value).FirstOrDefault() : "",

                    Picid = s.Picid,
                    PersonInchargeName = s.Pic?.UserName,
                    Picadvice = s.Picadvice,
                    AttachSessionId = s.AttachSessionId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                iPIRLineModels.Add(iPIRLineModel);
            });
            return iPIRLineModels.Where(w => w.Ipirid == id).OrderByDescending(a => a.IpirlineId).ToList();
        }



        // POST: api/User
        [HttpPost]
        [Route("InsertIPIRLine")]
        public IPIRLineModel InsertIPIRLine(IPIRLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var ipirline = new Ipirline
            {
                Ipirid = value.Ipirid,
                AssignTo = value.AssignTo,
                Ipiraction = value.Ipiraction,
                Picid = value.Picid,
                Picadvice = value.Picadvice,
                AttachSessionId = SessionId,
                DiscussionSessionId = value.DiscussionSessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.Ipirline.Add(ipirline);
            _context.SaveChanges();
            value.IpirlineId = ipirline.IpirlineId;
            value.AttachSessionId = SessionId;

            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateIPIRLine")]
        public IPIRLineModel UpdateIPIRLine(IPIRLineModel value)
        {
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;

            }
            var ipirline = _context.Ipirline.SingleOrDefault(p => p.IpirlineId == value.IpirlineId);
            ipirline.Ipirid = value.Ipirid;
            ipirline.AssignTo = value.AssignTo;
            ipirline.Ipiraction = value.Ipiraction;
            ipirline.Picid = value.Picid;
            ipirline.Picadvice = value.Picadvice;
            ipirline.AttachSessionId = value.AttachSessionId;
            ipirline.DiscussionSessionId = value.DiscussionSessionId;
            ipirline.ModifiedByUserId = value.ModifiedByUserID;
            ipirline.ModifiedDate = DateTime.Now;
            ipirline.StatusCodeId = value.StatusCodeID.Value;
            value.IpirlineId = ipirline.IpirlineId;

            _context.SaveChanges();
            return value;
        }


        [HttpGet]
        [Route("GetIPIRLineDocument")]
        public List<IPIRLineDocumentModel> Get(Guid? SessionId)
        {
            List<IPIRLineDocumentModel> iPIRLineDocumentModels = new List<IPIRLineDocumentModel>();
            List<IpirlineDocument> ipirlineDocuments = new List<IpirlineDocument>();



            ipirlineDocuments = _context.IpirlineDocument.Select(s=> new IpirlineDocument { IpirlineDocumentId=s.IpirlineDocumentId,FileName= s.FileName,ContentType= s.ContentType,FileSize= s.FileSize,UploadDate = s.UploadDate,SessionId = s.SessionId }).Where(f => f.SessionId == SessionId).AsNoTracking().ToList();
            if (ipirlineDocuments != null && ipirlineDocuments.Count > 0)
            {
                ipirlineDocuments.ForEach(s =>
                {
                    IPIRLineDocumentModel iPIRLineDocumentModel = new IPIRLineDocumentModel
                    {
                        IpirlineDocumentId = s.IpirlineDocumentId,
                        FileName = s.FileName,
                        ContentType = s.ContentType,
                        FileSize = s.FileSize,
                        UploadDate = s.UploadDate,
                        SessionId = s.SessionId,
                        IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
                    };
                    iPIRLineDocumentModels.Add(iPIRLineDocumentModel);
                });

            }


            return iPIRLineDocumentModels;
        }

        [HttpDelete]
        [Route("DeleteDocuments")]
        public void DeleteDocuments(int id)
        {
            try
            {
                var ipirlineDocument = _context.IpirlineDocument.FirstOrDefault(f => f.IpirlineDocumentId == id);
                if (ipirlineDocument != null)
                {
                    _context.IpirlineDocument.Remove(ipirlineDocument);
                    _context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPost]
        [Route("UploadIPIRLineDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {

            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var ipir = new IpirlineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    //Description = files.desc
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,

                };
                _context.IpirlineDocument.Add(ipir);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("DownLoadIPIRLineDocument")]
        public IActionResult DownLoadIPIRLineDocument(long id)
        {
            var document = _context.IpirlineDocument.SingleOrDefault(t => t.IpirlineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }

        //[HttpGet]
        //[Route("DownLoadIPIRLineDocumentList")]
        //public List<IActionResult> DownLoadIPIRLineDocumentList(long id)
        //{
        //    List<Stream> streamlist = new List<Stream>();
        //    List<IPIRLineDocumentModel> iPIRLineDocumentModels = new List<IPIRLineDocumentModel>();
        //    var ipirlinesessionid = _context.Ipirline.Where(s => s.IpirlineId == id).FirstOrDefault().AttachSessionId;
        //    var ipirlineDocumentIds = _context.IpirlineDocument.Where(f => f.SessionId == ipirlinesessionid).AsNoTracking().Select(f=>f.IpirlineDocumentId).ToList();
        //    //if (ipirlineDocuments != null && ipirlineDocuments.Count > 0)
        //    //{
        //    //    ipirlineDocuments.ForEach(s =>
        //    //    {
        //    //        IPIRLineDocumentModel iPIRLineDocumentModel = new IPIRLineDocumentModel
        //    //        {
        //    //            IpirlineDocumentId = s.IpirlineDocumentId,
        //    //            FileName = s.FileName,
        //    //            ContentType = s.ContentType,
        //    //            FileSize = s.FileSize,
        //    //            UploadDate = s.UploadDate,
        //    //            SessionId = s.SessionId,
        //    //            IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
        //    //        };
        //    //        iPIRLineDocumentModels.Add(iPIRLineDocumentModel);
        //    //    });

        //    //}

        //    //var documentlist = _context.IpirlineDocument.Where(t => ipirlineDocumentIds.Contains(t.IpirlineDocumentId) && t.ContentType.ToLower().Contains("image")).ToList();
        //    //if (documentlist != null)
        //    //{
        //    //    documentlist.ForEach(document =>
        //    //    {


        //    //    var stream = new MemoryStream(document.FileData);
        //    //    });
        //    //    if (streamlist == null)
        //    //        return NotFound();

        //    //    return Ok(streamlist);
        //    //}
        //    //return NotFound();
        //}
        [HttpGet]
        [Route("GetIPIRLineDocumentById")]
        public IPIRLineDocumentModel GetIPIRLineDocumentById(long id)
        {
            IPIRLineDocumentModel iPIRLineDocumentModel = new IPIRLineDocumentModel();
            var document = _context.IpirlineDocument.Select(s=> new { s.IpirlineDocumentId, s.SessionId,s.FileName,s.FileSize,s.ContentType}).FirstOrDefault(t => t.IpirlineDocumentId == id);
            if (document != null)
            {


                iPIRLineDocumentModel.IpirlineDocumentId = document.IpirlineDocumentId;
                iPIRLineDocumentModel.SessionId = document.SessionId;
                iPIRLineDocumentModel.ContentType = document.ContentType;
                iPIRLineDocumentModel.FileName = document.FileName;
                //iPIRLineDocumentModel.FileData = document.FileData;


            }
            return iPIRLineDocumentModel;
        }



        [HttpPost]
        [Route("UploadIpirlineDocument")]
        public IActionResult UploadIpirlineDocument(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenID = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710 ,Title=profile.Name});
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new IpirlineDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenID
                };

                _context.IpirlineDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetIpirlineDocumentBySessionID")]
        public List<DocumentsModel> GetIpirlineDocumentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.IpirlineDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.IpirlineDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadIpirlineDocument")]
        public IActionResult DownLoadIpirlineDocument(long id)
        {
            var document = _context.IpirlineDocument.SingleOrDefault(t => t.IpirlineDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteIpirlineDocument")]
        public void DeleteIpirlineDocument(int id)
        {
            var query = _context.IpirlineDocument.SingleOrDefault(p => p.IpirlineDocumentId == id);
            if (query != null)
            {
                _context.IpirlineDocument.Remove(query);
                _context.SaveChanges();
            }
        }

    }
}