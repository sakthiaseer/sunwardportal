﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TeamMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TeamMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTeams")]
        public List<TeamMasterModel> Get()
        {
            List<TeamMasterModel> teamMasterModels = new List<TeamMasterModel>();
            var teamMaster = _context.TeamMaster.Include("AddedByUser").Include("ModifiedByUser").Include("TeamMember").OrderByDescending(o => o.TeamMasterId).AsNoTracking().ToList();
            teamMaster.ForEach(s =>
            {
                TeamMasterModel teamMasterModel = new TeamMasterModel
                {
                    TeamMasterID = s.TeamMasterId,
                    Name = s.Name,
                    Description = s.Description,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    MemberIds = s.TeamMember != null ? s.TeamMember.Select(c => c.MemberId.Value).ToList() : new List<long>(),

                };
                teamMasterModels.Add(teamMasterModel);
            });
            return teamMasterModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get TeamMaster")]
        [HttpGet("GetTeams/{id:int}")]
        public ActionResult<TeamMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var teamMaster = _context.TeamMaster.SingleOrDefault(p => p.TeamMasterId == id.Value);
            var result = _mapper.Map<TeamMasterModel>(teamMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TeamMasterModel> GetData(SearchModel searchModel)
        {
            var teamMaster = new TeamMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).LastOrDefault();
                        break;
                    case "Next":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).LastOrDefault();
                        break;
                    case "Next":
                        teamMaster = _context.TeamMaster.OrderBy(o => o.TeamMasterId).FirstOrDefault(s => s.TeamMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        teamMaster = _context.TeamMaster.OrderByDescending(o => o.TeamMasterId).FirstOrDefault(s => s.TeamMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TeamMasterModel>(teamMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTeam")]
        public TeamMasterModel Post(TeamMasterModel value)
        {
            var teamMaster = new TeamMaster
            {
                // TeamMasterId=value.TeamMasterID,
                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                TeamMember = new List<TeamMember>(),

            };

            value.MemberIds.ForEach(c =>
            {
                var member = new TeamMember
                {
                    MemberId = c,
                };
                teamMaster.TeamMember.Add(member);
            });

            _context.TeamMaster.Add(teamMaster);
            _context.SaveChanges();
            value.TeamMasterID = teamMaster.TeamMasterId;

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTeam")]
        public TeamMasterModel Put(TeamMasterModel value)
        {
            var teamMaster = _context.TeamMaster.SingleOrDefault(p => p.TeamMasterId == value.TeamMasterID);
            teamMaster.ModifiedByUserId = value.ModifiedByUserID;
            teamMaster.ModifiedDate = DateTime.Now;
            teamMaster.Name = value.Name;
            teamMaster.Description = value.Description;
            //teamMaster.AddedByUserId = value.AddedByUserID;
            // teamMaster.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            teamMaster.StatusCodeId = value.StatusCodeID.Value;


            var teammember = _context.TeamMember.Where(l => l.TeamId == value.TeamMasterID).ToList();
            if (teammember.Count > 0)
            {
                _context.TeamMember.RemoveRange(teammember);
                _context.SaveChanges();
            }
            value.MemberIds.ForEach(c =>
            {
                var member = new TeamMember
                {
                    MemberId = c,
                    TeamId = value.TeamMasterID,
                };
                teamMaster.TeamMember.Add(member);
            });

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTeam")]
        public void Delete(int id)
        {
            var teamMaster = _context.TeamMaster.SingleOrDefault(p => p.TeamMasterId == id);
            if (teamMaster != null)
            {
                _context.TeamMaster.Remove(teamMaster);
                _context.SaveChanges();
            }
        }
    }
}