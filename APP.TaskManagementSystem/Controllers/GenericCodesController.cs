﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class GenericCodesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public GenericCodesController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodes")]
        public List<GenericCodesModel> Get()
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235 };
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            //var GenericCodes = _context.GenericCodes.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new GenericCodesModel
            var genericCodes = _context.GenericCodes.Include(a => a.AddedByUser).Include(s => s.GenericCodeSupplyToMultiple).Include("GenericCodeSupplyToMultiple.SupplyTo").Include(g => g.GenericCodeCountry).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    Description2 = s.Description2,
                    ProfileNo = s.ProfileNo,
                    PackingCode = s.PackingCode,
                    Uom = s.Uom,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null,
                    ManufacringCountry = s.ManufacringCountry,
                    ManufacturingIds = s.GenericCodeCountry != null ? s.GenericCodeCountry.Select(s => s.CountryId).ToList() : null,
                    SellingToIds = s.GenericCodeCountry != null ? s.GenericCodeCountry.Where(g => g.IsSellingToCountry).Select(s => s.CountryId).ToList() : null,
                    SupplyToIds = s.GenericCodeSupplyToMultiple != null ? s.GenericCodeSupplyToMultiple.Select(s => s.SupplyToId).ToList() : null,
                    SupplyToNames = s.GenericCodeSupplyToMultiple != null ? s.GenericCodeSupplyToMultiple.Select(s => s.SupplyTo?.CompanyName).ToList() : null,
                    PackingUnitsId = s.PackingUnitsId,
                    PackingUnits = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PackingUnitsId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PackingUnitsId).Value : null,
                    ProductNameId = s.ProductNameId,
                    ProductName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProductNameId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProductNameId).Value : null,
                    SupplyToId = s.SupplyToId,
                    SupplyTo = s.SupplyTo?.CompanyName,
                    IsSimulation = s.IsSimulation,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                };
                GenericCodesModel.Add(GenericCodesModels);
            });
            if (GenericCodesModel != null && GenericCodesModel.Count > 0)
            {
                GenericCodesModel.ForEach(g =>
                {
                    g.Name = g.Code + " | " + g.ProductName;
                });
            }
            return GenericCodesModel.OrderBy(o => o.Code).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodesByCompany")]
        public List<GenericCodesModel> GetGenericCodesByCompany(int? id)
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235 };
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var genericCodes = _context.GenericCodes.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").ToList();
            //var genericCodes = _context.GenericCodeCountry.Include(m => m.GenericCode).Where(w=>w.CountryId==id).AsNoTracking().ToList();
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    ProfileNo = s.ProfileNo,
                    PackingCode = s.PackingCode,
                    Uom = s.Uom,
                    Name = s.Code + "|" + s.Description,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null,
                };
                GenericCodesModel.Add(GenericCodesModels);
            });
            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodeSupplyToMultipleById")]
        public List<GenericCodeSupplyToMultipleModel> GetGenericCodeSupplyToMultipleById(int? id)
        {
            var genericCodeSupplyToMultiples = _context.GenericCodeSupplyToMultiple.Where(g => g.GenericCodeId == id).ToList();
            List<GenericCodeSupplyToMultipleModel> genericCodeSupplyToMultipleModels = new List<GenericCodeSupplyToMultipleModel>();
            genericCodeSupplyToMultiples.ForEach(s =>
            {
                GenericCodeSupplyToMultipleModel genericCodeSupplyToMultipleModel = new GenericCodeSupplyToMultipleModel();

                genericCodeSupplyToMultipleModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                genericCodeSupplyToMultipleModel.GenericCodeId = s.GenericCodeId;
                genericCodeSupplyToMultipleModel.Description = s.GenericCodeSupplyDescription;
                if (s.GenericCodeId != null)
                {
                    var Navitems = _context.ProductGroupingNav.Include(p => p.ProductGroupingManufacture).Include(g => g.ProductGroupingManufacture.ProductGrouping).Include(c => c.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry).Include(n => n.Item).Where(w => w.ProductGroupingManufacture.ProductGroupingId == s.GenericCodeId && w.GenericCodeSupplyToMultipleId == s.GenericCodeSupplyToMultipleId && w.Item.StatusCodeId == 1).ToList();
                    if (Navitems != null)
                    {
                        List<NavItemModel> NavItemModel = new List<NavItemModel>();
                        Navitems.ForEach(h =>
                        {
                            if (h.Item != null)
                            {
                                NavItemModel NavItemModels = new NavItemModel();
                                NavItemModels.ItemId = h.Item.ItemId;
                                NavItemModels.No = h.Item.No;
                                NavItemModels.Description = h.Item.Description;
                                NavItemModels.Description2 = h.Item.Description2;
                                NavItemModels.InternalRef = h.Item.InternalRef;
                                NavItemModels.ItemCategoryCode = h.Item.ItemCategoryCode;
                                NavItemModels.BaseUnitofMeasure = h.Item.BaseUnitofMeasure;
                                NavItemModels.CompanyId = h.Item.CompanyId;
                                NavItemModels.VendorNo = h.Item.VendorNo;
                                NavItemModels.CountryIds = h.ProductGroupingManufacture != null && h.ProductGroupingManufacture.ProductGrouping != null && h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry != null && h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry != null ? h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry.Select(c => c.CountryId.Value).ToList() : new List<long>();
                                NavItemModel.Add(NavItemModels);
                            }
                        });
                        genericCodeSupplyToMultipleModel.NavItemsList = NavItemModel;
                    }
                }
                genericCodeSupplyToMultipleModels.Add(genericCodeSupplyToMultipleModel);
            });
            return genericCodeSupplyToMultipleModels.OrderByDescending(o => o.GenericCodeSupplyToMultipleId).ToList();
        }
        [HttpGet]
        [Route("GetGenericCodeSupplyToMultipleByCountry")]
        public List<GenericCodeSupplyToMultipleModel> GetGenericCodeSupplyToMultipleByCountry(int? id, long? countryId)
        {
            var genericCodeSupplyToMultiples = _context.GenericCodeSupplyToMultiple.Where(g => g.GenericCodeId == id).ToList();
            List<GenericCodeSupplyToMultipleModel> genericCodeSupplyToMultipleModels = new List<GenericCodeSupplyToMultipleModel>();
            genericCodeSupplyToMultiples.ForEach(s =>
            {
                GenericCodeSupplyToMultipleModel genericCodeSupplyToMultipleModel = new GenericCodeSupplyToMultipleModel();

                genericCodeSupplyToMultipleModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                genericCodeSupplyToMultipleModel.GenericCodeId = s.GenericCodeId;
                genericCodeSupplyToMultipleModel.Description = s.GenericCodeSupplyDescription;
                if (s.GenericCodeId != null)
                {
                    var Navitems = _context.ProductGroupingNav.Include(p => p.ProductGroupingManufacture).Include(g => g.ProductGroupingManufacture.ProductGrouping).Include(c => c.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry).Include(n => n.Item).Where(w => w.ProductGroupingManufacture.ProductGroupingId == s.GenericCodeId && w.GenericCodeSupplyToMultipleId == s.GenericCodeSupplyToMultipleId && w.Item.StatusCodeId == 1).ToList();
                    if (Navitems != null)
                    {
                        List<NavItemModel> NavItemModel = new List<NavItemModel>();
                        Navitems.ForEach(h =>
                        {
                            if (h.Item != null)
                            {
                                NavItemModel NavItemModels = new NavItemModel();
                                NavItemModels.ItemId = h.Item.ItemId;
                                NavItemModels.No = h.Item.No;
                                NavItemModels.Description = h.Item.Description;
                                NavItemModels.Description2 = h.Item.Description2;
                                NavItemModels.InternalRef = h.Item.InternalRef;
                                NavItemModels.ItemCategoryCode = h.Item.ItemCategoryCode;
                                NavItemModels.BaseUnitofMeasure = h.Item.BaseUnitofMeasure;
                                //NavItemModels.CompanyId = h.Item.CompanyId;
                                NavItemModels.VendorNo = h.Item.VendorNo;
                                NavItemModels.CompanyId = countryId;
                                NavItemModels.CountryIds = h.ProductGroupingManufacture != null && h.ProductGroupingManufacture.ProductGrouping != null && h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry != null && h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry != null ? h.ProductGroupingManufacture.ProductGrouping.GenericCodeCountry.Select(c => c.CountryId.Value).ToList() : new List<long>();
                                NavItemModel.Add(NavItemModels);
                            }
                        });
                        genericCodeSupplyToMultipleModel.NavItemsList = NavItemModel.Where(w => w.CountryIds.Contains(countryId.Value)).ToList();
                    }
                }
                genericCodeSupplyToMultipleModels.Add(genericCodeSupplyToMultipleModel);
            });
            return genericCodeSupplyToMultipleModels.OrderByDescending(o => o.GenericCodeSupplyToMultipleId).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodesByStatus")]
        public List<GenericCodesModel> GetGenericCodesByStatus()
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235 };
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var genericCodes = _context.GenericCodes.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(g => g.StatusCodeId == 1).ToList();
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    ProfileNo = s.ProfileNo,
                    PackingCode = s.PackingCode,
                    Uom = s.Uom,
                    Name = s.Code + "|" + s.Description,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null,
                };
                GenericCodesModel.Add(GenericCodesModels);
            });

            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }


        [HttpGet]
        [Route("GetGenericCodeItems")]
        public List<GenericCodesModel> GetGenericCodeItems()
        {

            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            var applicationMasterId = _context.ApplicationMaster.Where(a => a.ApplicationMasterCodeId == 234).Select(a => a.ApplicationMasterId).FirstOrDefault();
            if (applicationMasterId > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == applicationMasterId).AsNoTracking().ToList();
            }
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1).ToList();
            genericCodes.ForEach(g =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = g.GenericCodeId,
                    Code = g.Code,
                    Description = g.Description,
                    Description2 = g.Description2,
                    ProfileNo = g.ProfileNo,
                    PackingCode = g.PackingCode,
                    Uom = g.Uom,
                    //Name = g.Code + "|" + g.Description,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom).Value : null,
                };
                GenericCodesModel.Add(GenericCodesModels);
            });
            if (GenericCodesModel != null && GenericCodesModel.Count > 0)
            {
                GenericCodesModel.ForEach(g =>
                {
                    g.Name = g.Code + " | " + g.Description;

                });
            }
            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodesByCompanyItems")]
        public List<GenericCodesModel> GetGenericCodesByCompanyItems()
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            var applicationMasterId = _context.ApplicationMaster.Where(a => a.ApplicationMasterCodeId == 234).Select(a => a.ApplicationMasterId).FirstOrDefault();
            if (applicationMasterId > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == applicationMasterId).AsNoTracking().ToList();
            }
            var soByCustomers = _context.SobyCustomers.Include("SocustomersItemCrossReference.SobyCustomerSunwardEquivalent").ToList();
            var genericCodeIds = soByCustomers.SelectMany(s => s.SocustomersItemCrossReference).ToList();

            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();

            genericCodeIds.ForEach(s =>
            {
                List<long?> productGroupId = new List<long?>();
                productGroupId.AddRange(s.SobyCustomerSunwardEquivalent.Select(s => s.NavItemId).ToList());
                var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && productGroupId.Contains(g.GenericCodeId)).ToList(); ;
                var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                genericCodes.ForEach(g =>
                {
                    var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && g.GenericCodeId == s.NavItemId).FirstOrDefault();
                    var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                    GenericCodesModel GenericCodesModels = new GenericCodesModel
                    {
                        SupplyToIds = sobyCustomerSunwardEquivalent.Select(e => e.SupplyToId).ToList(),
                        GenericCodeID = g.GenericCodeId,
                        Code = g.Code,
                        Description = g.Description,
                        ProfileNo = g.ProfileNo,
                        PackingCode = g.PackingCode,
                        Uom = g.Uom,
                        Name = g.Code + "|" + g.Description,
                        UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom).Value : null,
                    };
                    GenericCodesModel.Add(GenericCodesModels);
                });
            });
            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }



        // GET: api/Project
        [HttpGet]
        [Route("GetGenericCodesByCompanyLisitng")]
        public List<GenericCodesModel> GetGenericCodesByCompanyLisitng(string refNo)
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            var applicationMasterId = _context.ApplicationMaster.Where(a => a.ApplicationMasterCodeId == 234).Select(a => a.ApplicationMasterId).FirstOrDefault();
            if (applicationMasterId > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == applicationMasterId).AsNoTracking().ToList();
            }
            var soByCustomers = _context.SobyCustomers.Include("SocustomersItemCrossReference.SobyCustomerSunwardEquivalent").Where(s => s.LinkProfileReferenceNo == refNo).ToList();
            // var genericCodeIds = soByCustomers.SelectMany(s => s.SocustomersItemCrossReference).Select(n => n.NavItemId).ToList();
            var genericCodeIds = soByCustomers.SelectMany(s => s.SocustomersItemCrossReference).ToList();

            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();

            genericCodeIds.ForEach(s =>
            {
                List<long?> productGroupId = new List<long?>();
                productGroupId.AddRange(s.SobyCustomerSunwardEquivalent.Select(s => s.NavItemId).ToList());
                var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && productGroupId.Contains(g.GenericCodeId)).ToList(); ;
                var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                genericCodes.ForEach(g =>
                {
                    var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && g.GenericCodeId == s.NavItemId).FirstOrDefault();
                    var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                    GenericCodesModel GenericCodesModels = new GenericCodesModel
                    {
                        SupplyToIds = sobyCustomerSunwardEquivalent.Select(e => e.SupplyToId).ToList(),
                        GenericCodeID = g.GenericCodeId,
                        Code = g.Code,
                        Description = g.Description,
                        ProfileNo = g.ProfileNo,
                        PackingCode = g.PackingCode,
                        Uom = g.Uom,
                        Name = g.Code + "|" + g.Description,
                        UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom).Value : null,
                    };
                    GenericCodesModel.Add(GenericCodesModels);
                });
            });
            /*var genericCodes = _context.GenericCodes.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(g => g.StatusCodeId == 1 && genericCodeIds.Contains(g.GenericCodeId)).ToList();
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    ProfileNo = s.ProfileNo,
                    PackingCode = s.PackingCode,
                    Uom = s.Uom,
                    Name = s.Code + "|" + s.Description,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null,
                };
                GenericCodesModel.Add(GenericCodesModels);
            });*/
            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }


        [HttpGet]
        [Route("GetGenericCodesByCompanyLisitngID")]
        public List<GenericCodesModel> GetGenericCodesByCompanyLisitngID(int id)
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            var applicationMasterId = _context.ApplicationMaster.Where(a => a.ApplicationMasterCodeId == 234).Select(a => a.ApplicationMasterId).FirstOrDefault();
            if (applicationMasterId > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == applicationMasterId).AsNoTracking().ToList();
            }
            var companyListing = _context.CompanyListing.FirstOrDefault(c => c.CompanyListingId == id);
            var soByCustomers = _context.SobyCustomers.Include("SocustomersItemCrossReference.SobyCustomerSunwardEquivalent").Where(s => s.LinkProfileReferenceNo == companyListing.ProfileReferenceNo).ToList();
            var genericCodeIds = soByCustomers.SelectMany(s => s.SocustomersItemCrossReference).ToList();

            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();

            genericCodeIds.ForEach(s =>
            {
                List<long?> productGroupId = new List<long?>();
                productGroupId.AddRange(s.SobyCustomerSunwardEquivalent.Select(s => s.NavItemId).ToList());
                var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && productGroupId.Contains(g.GenericCodeId)).ToList(); ;
                var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                genericCodes.ForEach(g =>
                {
                    var genericCodes = _context.GenericCodes.Where(g => g.StatusCodeId == 1 && g.GenericCodeId == s.NavItemId).FirstOrDefault();
                    var sobyCustomerSunwardEquivalent = s.SobyCustomerSunwardEquivalent.Where(a => a.SupplyToId != null).ToList();
                    GenericCodesModel GenericCodesModels = new GenericCodesModel
                    {
                        SupplyToIds = sobyCustomerSunwardEquivalent.Select(e => e.SupplyToId).ToList(),
                        GenericCodeID = g.GenericCodeId,
                        Code = g.Code,
                        Description = g.Description,
                        ProfileNo = g.ProfileNo,
                        PackingCode = g.PackingCode,
                        Uom = g.Uom,
                        Name = g.Code + "|" + g.Description,
                        UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == g.Uom).Value : null,
                    };
                    GenericCodesModel.Add(GenericCodesModels);
                });
            });
            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<GenericCodesModel> GetData(SearchModel searchModel)
        {
            var GenericCodes = new GenericCodes();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).LastOrDefault();
                        break;
                    case "Next":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).LastOrDefault();
                        break;
                    case "Previous":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).LastOrDefault();
                        break;
                    case "Next":
                        GenericCodes = _context.GenericCodes.OrderBy(o => o.GenericCodeId).FirstOrDefault(s => s.GenericCodeId > searchModel.Id);
                        break;
                    case "Previous":
                        GenericCodes = _context.GenericCodes.OrderByDescending(o => o.GenericCodeId).FirstOrDefault(s => s.GenericCodeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<GenericCodesModel>(GenericCodes);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertGenericCodes")]
        public GenericCodesModel Post(GenericCodesModel value)
        {
            string profileNo = "";
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Code });
            }
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235, 267, 190 };
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var genericCodes = new GenericCodes
            {
                //GenericCodesID = value.GenericCodesId,
                Code = value.Code,
                Description = value.Description,
                Description2 = value.Description,
                PackingCode = value.PackingCode,
                ProfileNo = profileNo,
                Uom = value.Uom,
                ManufacringCountry = value.ManufacringCountry,
                IsSimulation = value.IsSimulation,
                ProductNameId = value.ProductNameId,
                PackingUnitsId = value.PackingUnitsId,
                SupplyToId = value.SupplyToId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };
            _context.GenericCodes.Add(genericCodes);
            _context.SaveChanges();

            if (value.ManufacturingIds != null)
            {
                value.ManufacturingIds.ForEach(u =>
                {
                    var genericCodeCountry = new GenericCodeCountry()
                    {
                        CountryId = u,
                        GenericCodeId = genericCodes.GenericCodeId
                    };
                    _context.GenericCodeCountry.Add(genericCodeCountry);
                });
                _context.SaveChanges();
            }

            string description = "";
            string PackingUnits = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PackingUnitsId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PackingUnitsId).Value : null;
            string ProductName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.ProductNameId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.ProductNameId).Value : null;
            if (value.SupplyToIds != null)
            {
                value.SupplyToIds.ForEach(u =>
                {
                    var companyListing = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).FirstOrDefault(c => c.CompanyListingId == u);
                    var genericCodeSupplyTo = new GenericCodeSupplyToMultiple()
                    {
                        SupplyToId = u,
                        GenericCodeId = genericCodes.GenericCodeId,
                        GenericCodeSupplyDescription = ProductName + " " + PackingUnits + " " + (companyListing.CompanyListingCustomerCode != null && applicationMasterDetails != null ? string.Join(",", companyListing.CompanyListingCustomerCode.Select(b => applicationMasterDetails.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : ""),
                    };
                    _context.GenericCodeSupplyToMultiple.Add(genericCodeSupplyTo);
                    description += genericCodeSupplyTo.GenericCodeSupplyDescription + Environment.NewLine;
                });
                _context.SaveChanges();
            }
            var genericCode = _context.GenericCodes.FirstOrDefault(g => g.GenericCodeId == genericCodes.GenericCodeId);
            genericCode.Description = description;
            _context.SaveChanges();
            value.GenericCodeID = genericCodes.GenericCodeId;
            value.ProfileNo = genericCodes.ProfileNo;
            value.Description = description;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateGenericCodes")]
        public GenericCodesModel Put(GenericCodesModel value)
        {
            var genericCodes = _context.GenericCodes.Include(g => g.GenericCodeCountry).Include(s => s.GenericCodeSupplyToMultiple).SingleOrDefault(p => p.GenericCodeId == value.GenericCodeID);
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235, 267, 190 };
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            string profileNo = "";
            string description = "";
            if (value.ProfileId > 0 && value.ProfileNo == null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Code });
                genericCodes.ProfileNo = profileNo;
                value.ProfileNo = profileNo;
            }
            genericCodes.Code = value.Code;
            genericCodes.Description = value.Description;
            genericCodes.Description2 = value.Description2;
            genericCodes.PackingCode = value.PackingCode;
            genericCodes.Uom = value.Uom;
            genericCodes.ManufacringCountry = value.ManufacringCountry;
            genericCodes.PackingUnitsId = value.PackingUnitsId;
            genericCodes.ProductNameId = value.ProductNameId;
            genericCodes.IsSimulation = value.IsSimulation;
            genericCodes.SupplyToId = value.SupplyToId;
            genericCodes.ModifiedByUserId = value.ModifiedByUserID;
            genericCodes.ModifiedDate = DateTime.Now;
            genericCodes.StatusCodeId = value.StatusCodeID.Value;
            string PackingUnits = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PackingUnitsId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PackingUnitsId).Value : null;
            string ProductName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.ProductNameId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == value.ProductNameId).Value : null;

            if (value.ManufacturingIds != null)
            {
                value.ManufacturingIds.ForEach(u =>
                {
                    var existing = _context.GenericCodeCountry.Where(s => s.GenericCodeId == genericCodes.GenericCodeId && s.CountryId == u).FirstOrDefault();
                    if (existing == null)
                    {
                        var genericCodeCountry = new GenericCodeCountry()
                        {
                            IsSellingToCountry = true,
                            CountryId = u,
                            GenericCodeId = genericCodes.GenericCodeId
                        };
                        _context.GenericCodeCountry.Add(genericCodeCountry);
                        _context.SaveChanges();
                    }
                });

            }

            if (value.SupplyToIds != null)
            {
                value.SupplyToIds.ForEach(u =>
                {
                    var existing = _context.GenericCodeSupplyToMultiple.Where(s => s.GenericCodeId == genericCodes.GenericCodeId && s.SupplyToId == u).FirstOrDefault();
                    if (existing == null)
                    {
                        var companyListing = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).FirstOrDefault(c => c.CompanyListingId == u);
                        var genericCodeSupplyTo = new GenericCodeSupplyToMultiple()
                        {
                            SupplyToId = u,
                            GenericCodeId = genericCodes.GenericCodeId,
                            //GenericCodeSupplyDescription = ProductName + " " + PackingUnits + " " + (companyListing.CompanyListingCustomerCode != null && applicationMasterDetails != null ? string.Join(",", companyListing.CompanyListingCustomerCode.Select(b => applicationMasterDetails.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : ""),
                        };
                        _context.GenericCodeSupplyToMultiple.Add(genericCodeSupplyTo);
                        _context.SaveChanges();

                    }

                });

            }
            var genericCodeSupplyToIds = _context.GenericCodeSupplyToMultiple.Where(s => s.GenericCodeId == genericCodes.GenericCodeId).Select(s => s.SupplyToId).ToList();
            var customerCodes = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).Where(c => genericCodeSupplyToIds.Contains(c.CompanyListingId)).ToList();
            if (customerCodes != null && customerCodes.Count > 0)
            {
                customerCodes.ForEach(cu =>
                {
                    var genericCodeSupply = _context.GenericCodeSupplyToMultiple.Where(g => g.SupplyToId == cu.CompanyListingId && g.GenericCodeId == value.GenericCodeID).FirstOrDefault();
                    if (genericCodeSupply != null)
                    {
                        genericCodeSupply.GenericCodeSupplyDescription = ProductName + " " + PackingUnits + " " + (cu.CompanyListingCustomerCode != null && applicationMasterDetails != null ? string.Join(",", cu.CompanyListingCustomerCode.Select(b => applicationMasterDetails.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "");
                        _context.SaveChanges();
                        description += genericCodeSupply.GenericCodeSupplyDescription + Environment.NewLine;
                    }
                });
            }
            genericCodes.Description = description;
            value.Description = description;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteGenericCodes")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var genericCodes = _context.GenericCodes.Include(g => g.GenericCodeSupplyToMultiple).Include(s => s.GenericCodeCountry).SingleOrDefault(p => p.GenericCodeId == id);
                if (genericCodes != null)
                {
                    _context.GenericCodes.Remove(genericCodes);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                var genericCode = _context.GenericCodes.SingleOrDefault(p => p.GenericCodeId == id);
                _context.Entry(genericCode).State = EntityState.Unchanged;
                if (genericCode != null)
                {
                    genericCode.StatusCodeId = 2;
                    _context.SaveChanges();
                }
                throw new Exception("Generic Code is already linked and it cannot be deleted so it is moving to inactive state.", null);
            }
        }
        [HttpPut]
        [Route("DeleteGenericCodeCountry")]
        public ActionResult<string> DeleteGenericCodeCountry(GenericCodeCountry value)
        {
            try
            {
                var genericCodeCountry = _context.GenericCodeCountry.Where(s => s.GenericCodeId == value.GenericCodeId && s.CountryId == value.CountryId).FirstOrDefault();
                if (genericCodeCountry != null)
                {
                    _context.GenericCodeCountry.Remove(genericCodeCountry);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Country already linked and it cannot be deleted", ex);
            }
        }

        [HttpPut]
        [Route("DeleteGenericCodeSupplyToMultiple")]
        public ActionResult<string> DeleteGenericCodeSupplyToMultiple(GenericCodeSupplyToMultipleModel value)
        {
            try
            {
                var genericCodeSupplyToMultiple = _context.GenericCodeSupplyToMultiple.Where(s => s.SupplyToId == value.SupplyToId && s.GenericCodeId == value.GenericCodeId).FirstOrDefault();
                if (genericCodeSupplyToMultiple != null)
                {
                    _context.GenericCodeSupplyToMultiple.Remove(genericCodeSupplyToMultiple);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Supply already linked and it cannot be deleted", ex);
            }
        }

    }
}