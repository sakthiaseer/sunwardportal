﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActivityMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public ProductionActivityMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetProductActivityCaseCategory")]
        public List<ProductActivityCaseLineModel> GetProductActivityCaseCategory(long? mprocessId, long? id)
        {
            List<ProductActivityCaseLineModel> templateTestCaseLinkModels = new List<ProductActivityCaseLineModel>();
            var templateTestCaseLink = _context.ProductionActivityMasterLine.Include(a => a.ProductionActivityMaster)
                .Include(a => a.TemplateProfile)
                .Include(a => a.ProdActivityActionChild)
                .Include(a => a.ProdActivityCategoryChild)
                .Where(w => w.ProdActivityCategoryChildId == id && w.ProductionActivityMaster.ManufacturingProcessId == mprocessId).ToList();
            templateTestCaseLink.ForEach(s =>
            {
                ProductActivityCaseLineModel templateTestCaseModel = new ProductActivityCaseLineModel();
                templateTestCaseModel.ProductActivityCaseLineId = s.ProductionActivityMasterLineId;
                templateTestCaseModel.ProductActivityCaseId = s.ProductionActivityMasterId;
                templateTestCaseModel.NameOfTemplate = s.NameOfTemplate;
                templateTestCaseModel.Subject = s.Subject;
                templateTestCaseModel.Link = s.Link;
                templateTestCaseModel.LocationName = s.LocationName;
                templateTestCaseModel.Naming = s.Naming;
                templateTestCaseModel.IsAutoNumbering = s.IsAutoNumbering;
                templateTestCaseModel.AutoNumbering = s.IsAutoNumbering == true ? "Yes" : "No";
                templateTestCaseModel.TemplateProfileId = s.TemplateProfileId;
                templateTestCaseModel.TemplateProfileName = s.TemplateProfile?.Name;
                templateTestCaseModel.ProdActivityActionChildId = s.ProdActivityActionChildId;
                templateTestCaseModel.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                templateTestCaseModel.ProdActivityCategory = s.ProdActivityCategoryChild?.Value;
                templateTestCaseModel.ProdActivityAction = s.ProdActivityActionChild?.Value;
                templateTestCaseModel.DocumentNo = s.DocumentNo;
                templateTestCaseModel.LocationToSaveId = s.LocationToSaveId;
                templateTestCaseLinkModels.Add(templateTestCaseModel);
            });
            return templateTestCaseLinkModels;
        }
        [HttpGet]
        [Route("GetProductionActivityMaster")]
        public List<ProductionActivityMasterModel> Get()
        {
            List<ProductionActivityMasterModel> productionActivityMasterModels = new List<ProductionActivityMasterModel>();

            var productionActivityMaster = _context.ProductionActivityMaster.Include(s => s.AddedByUser).Include(a => a.ManufacturingProcess).Include(a => a.Action).Include(s => s.ModifiedByUser).Include(s => s.CategoryAction).OrderByDescending(o => o.ProductionActivityMasterId).AsNoTracking().ToList();

            if (productionActivityMaster != null && productionActivityMaster.Count > 0)
            {
                productionActivityMaster.ForEach(s =>
                {
                    ProductionActivityMasterModel productionActivityMasterModel = new ProductionActivityMasterModel
                    {
                        ProductionActivityMasterId = s.ProductionActivityMasterId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        CategoryActionId = s.CategoryActionId,
                        ActionId = s.ActionId,
                        ManufacturingProcess = s.ManufacturingProcess?.Value,
                        CategoryAction = s.CategoryAction?.Value,
                        Action = s.Action?.Value,
                        Reference = s.Reference,
                        CheckerNotes = s.CheckerNotes,
                        Upload = s.Upload,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        SessionId = s.SessionId,

                    };
                    productionActivityMasterModels.Add(productionActivityMasterModel);
                });
            }
            return productionActivityMasterModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityMaster")]
        public ProductionActivityMasterModel InsertProductionActivityMaster(ProductionActivityMasterModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var productionActivityMaster = new ProductionActivityMaster
            {
                CategoryActionId = value.CategoryActionId,
                ActionId = value.ActionId,
                ManufacturingProcessId = value.ManufacturingProcessId,
                Reference = value.Reference,
                CheckerNotes = value.CheckerNotes,
                Upload = value.Upload,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.SessionId,

            };
            _context.ProductionActivityMaster.Add(productionActivityMaster);
            _context.SaveChanges();
            value.ProductionActivityMasterId = productionActivityMaster.ProductionActivityMasterId;
            value.SessionId = value.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityMaster")]
        public ProductionActivityMasterModel UpdateProductionActivityMaster(ProductionActivityMasterModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var productionActivityMaster = _context.ProductionActivityMaster.SingleOrDefault(p => p.ProductionActivityMasterId == value.ProductionActivityMasterId);
            productionActivityMaster.CategoryActionId = value.CategoryActionId;
            productionActivityMaster.ActionId = value.ActionId;
            productionActivityMaster.ManufacturingProcessId = value.ManufacturingProcessId;
            productionActivityMaster.Reference = value.Reference;
            productionActivityMaster.CheckerNotes = value.CheckerNotes;
            productionActivityMaster.Upload = value.Upload;
            productionActivityMaster.StatusCodeId = value.StatusCodeID.Value;
            productionActivityMaster.ModifiedByUserId = value.AddedByUserID;
            productionActivityMaster.ModifiedDate = DateTime.Now;
            productionActivityMaster.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityMaster")]
        public ActionResult<string> DeleteProductionActivityMaster(int id)
        {
            try
            {
                var productionActivityMaster = _context.ProductionActivityMaster.SingleOrDefault(p => p.ProductionActivityMasterId == id);
                if (productionActivityMaster != null)
                {
                    _context.ProductionActivityMaster.Remove(productionActivityMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductionActivityMaster Cannot be delete! Reference to others");
            }
        }
        [HttpGet]
        [Route("GetProductionActivityMasterRespons")]
        public List<ProductionActivityMasterResponsModel> GetProductionActivityMasterRespons(long id)
        {
            List<ProductionActivityMasterResponsModel> productionActivityMasterModels = new List<ProductionActivityMasterResponsModel>();

            var productionActivityMaster = _context.ProductionActivityMasterRespons.Include(s => s.AddedByUser).Include(a => a.PageLinkNavigation).Include(a => a.FunctionLinkNavigation).Include(a => a.Duty).Include(s => s.ProductionActivityMasterResponsWeekly).Include(s => s.ModifiedByUser).Include(a => a.StatusCode).Where(w => w.ProductionActivityMasterId == id).OrderByDescending(o => o.ProductionActivityMasterResponsId).AsNoTracking().ToList();

            if (productionActivityMaster != null && productionActivityMaster.Count > 0)
            {
                productionActivityMaster.ForEach(s =>
                {
                    ProductionActivityMasterResponsModel productionActivityMasterModel = new ProductionActivityMasterResponsModel
                    {
                        ProductionActivityMasterId = s.ProductionActivityMasterId,
                        ProductionActivityMasterResponsId = s.ProductionActivityMasterResponsId,
                        DutyId = s.DutyId,
                        Responsibility = s.Responsibility,
                        FunctionLink = s.FunctionLink,
                        PageLink = s.PageLink,
                        AddedByUserId = s.AddedByUserId,
                        AddedByUser = s.AddedByUser?.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode?.CodeValue,
                        AddedDate = s.AddedDate,
                        StatusCodeId = s.StatusCodeId,
                        NotificationAdvice = s.NotificationAdvice,
                        NotificationAdviceFlag = s.NotificationAdvice == true ? "Yes" : "No",
                        NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                        RepeatId = s.RepeatId,
                        CustomId = s.CustomId,
                        DueDate = s.DueDate,
                        Monthly = s.Monthly,
                        Yearly = s.Yearly,
                        EventDescription = s.EventDescription,
                        DaysOfWeek = s.DaysOfWeek,
                        // SessionId = SessionId,
                        NotificationStatusId = s.NotificationStatusId,
                        Title = s.Title,
                        Message = s.Message,
                        ScreenId = s.ScreenId,
                        NotifyEndDate = s.NotifyEndDate,
                        IsAllowDocAccess = s.IsAllowDocAccess,
                        WeeklyIds = s.ProductionActivityMasterResponsWeekly != null ? s.ProductionActivityMasterResponsWeekly.Where(c => c.ProductionActivityMasterResponsId == s.ProductionActivityMasterResponsId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        DaysOfWeekIds = s.ProductionActivityMasterResponsWeekly != null ? s.ProductionActivityMasterResponsWeekly.Where(c => c.ProductionActivityMasterResponsId == s.ProductionActivityMasterResponsId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        PageLinkName = s.PageLinkNavigation?.PermissionName,
                        FunctionLinkName = s.FunctionLinkNavigation?.PermissionName,
                        DutyName = s.Duty?.Value,
                    };
                    productionActivityMasterModels.Add(productionActivityMasterModel);
                });
            }
            return productionActivityMasterModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityMasterRespons")]
        public ProductionActivityMasterResponsModel InsertProductionActivityMasterRespons(ProductionActivityMasterResponsModel value)
        {
            //var SessionId = Guid.NewGuid();
            var productionActivityMaster = new ProductionActivityMasterRespons
            {
                ProductionActivityMasterId = value.ProductionActivityMasterId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeId.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                // SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = value.ScreenId,
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess = value.IsAllowDocAccess,
            };
            _context.ProductionActivityMasterRespons.Add(productionActivityMaster);
            _context.SaveChanges();
            value.ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId;
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductionActivityMasterResponsWeekly()
                    {
                        ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.ProductionActivityMasterResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductionActivityMasterResponsWeekly()
                    {
                        ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.ProductionActivityMasterResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            //value.SessionId = SessionId;
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityMasterRespons")]
        public ProductionActivityMasterResponsModel UpdateProductionActivityMasterRespons(ProductionActivityMasterResponsModel value)
        {
            var productionActivityMaster = _context.ProductionActivityMasterRespons.SingleOrDefault(p => p.ProductionActivityMasterResponsId == value.ProductionActivityMasterResponsId);

            productionActivityMaster.ProductionActivityMasterId = value.ProductionActivityMasterId;
            productionActivityMaster.DutyId = value.DutyId;
            productionActivityMaster.Responsibility = value.Responsibility;
            productionActivityMaster.FunctionLink = value.FunctionLink;
            productionActivityMaster.PageLink = value.PageLink;
            productionActivityMaster.ModifiedByUserId = value.AddedByUserID;
            productionActivityMaster.ModifiedDate = DateTime.Now;
            productionActivityMaster.StatusCodeId = value.StatusCodeId.Value;
            productionActivityMaster.NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false;
            productionActivityMaster.NotificationAdviceTypeId = value.NotificationAdviceTypeId;
            productionActivityMaster.RepeatId = value.RepeatId;
            productionActivityMaster.CustomId = value.CustomId;
            productionActivityMaster.DueDate = value.DueDate == null ? DateTime.Now : value.DueDate;
            productionActivityMaster.Monthly = value.Monthly;
            productionActivityMaster.Yearly = value.Yearly;
            productionActivityMaster.EventDescription = value.EventDescription;
            productionActivityMaster.DaysOfWeek = value.DaysOfWeek;
            // SessionId = SessionId,
            productionActivityMaster.NotificationStatusId = value.NotificationStatusId;
            productionActivityMaster.Title = value.Title;
            productionActivityMaster.Message = value.Message;
            productionActivityMaster.ScreenId = value.ScreenId;
            productionActivityMaster.NotifyEndDate = value.NotifyEndDate;
            productionActivityMaster.IsAllowDocAccess = value.IsAllowDocAccess;
            _context.SaveChanges();
            var applicationWikiWeeklyRemove = _context.ProductionActivityMasterResponsWeekly.Where(p => p.ProductionActivityMasterResponsId == value.ProductionActivityMasterResponsId).ToList();
            if (applicationWikiWeeklyRemove != null)
            {
                _context.ProductionActivityMasterResponsWeekly.RemoveRange(applicationWikiWeeklyRemove);
                _context.SaveChanges();
            }
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductionActivityMasterResponsWeekly()
                    {
                        ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.ProductionActivityMasterResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductionActivityMasterResponsWeekly()
                    {
                        ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.ProductionActivityMasterResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.ProductionActivityMasterResponsId = productionActivityMaster.ProductionActivityMasterResponsId;
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityMasterRespons")]
        public ActionResult<string> DeleteProductionActivityMasterRespons(int id)
        {
            try
            {
                var productionActivityMaster = _context.ProductionActivityMasterRespons.SingleOrDefault(p => p.ProductionActivityMasterResponsId == id);
                if (productionActivityMaster != null)
                {
                    var applicationWikiWeeklyRemove = _context.ProductionActivityMasterResponsWeekly.Where(p => p.ProductionActivityMasterResponsId == id).ToList();
                    if (applicationWikiWeeklyRemove != null)
                    {
                        _context.ProductionActivityMasterResponsWeekly.RemoveRange(applicationWikiWeeklyRemove);
                        _context.SaveChanges();
                    }
                    _context.ProductionActivityMasterRespons.Remove(productionActivityMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductionActivityMasterRespons Cannot be delete! Reference to others");
            }
        }
        [HttpGet]
        [Route("GetRecurrence")]
        public List<ProductionActivityMasterResponsRecurrenceModel> GetRecurrence(long id)
        {
            List<ProductionActivityMasterResponsRecurrenceModel> productionActivityMasterResponsRecurrenceModels = new List<ProductionActivityMasterResponsRecurrenceModel>();
            var recurrence = _context.ProductionActivityMasterResponsRecurrence
                 .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(b => b.StatusCode)
                .Include(c => c.Type)
                .Include(d => d.OccurenceOption)
                .AsNoTracking().Where(w => w.ProductionActivityMasterResponsId == id).OrderByDescending(o => o.ProductionActivityMasterResponsRecurrenceId).AsNoTracking().ToList();
            recurrence.ForEach(s =>
            {
                ProductionActivityMasterResponsRecurrenceModel productionActivityMasterResponsRecurrenceModel = new ProductionActivityMasterResponsRecurrenceModel
                {
                    ProductionActivityMasterResponsRecurrenceId = s.ProductionActivityMasterResponsRecurrenceId,
                    ProductionActivityMasterResponsId = s.ProductionActivityMasterResponsId,
                    TypeId = s.TypeId,
                    RepeatNos = s.RepeatNos,
                    OccurenceOptionId = s.OccurenceOptionId,
                    NoOfOccurences = s.NoOfOccurences,
                    Sunday = s.Sunday,
                    Monday = s.Monday,
                    Tuesday = s.Tuesday,
                    Wednesday = s.Wednesday,
                    Thursday = s.Thursday,
                    Friday = s.Friday,
                    Saturyday = s.Saturyday,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    TypeName = s.Type.CodeValue,
                    OccurenceOptionName = s.OccurenceOption?.CodeValue,
                };
                productionActivityMasterResponsRecurrenceModels.Add(productionActivityMasterResponsRecurrenceModel);
            });
            return productionActivityMasterResponsRecurrenceModels;
        }
        [HttpPost]
        [Route("InsertRecurrence")]
        public ProductionActivityMasterResponsRecurrenceModel InsertRecurrence(ProductionActivityMasterResponsRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.ProductionActivityMasterResponsRecurrence.Where(p => p.ProductionActivityMasterResponsId == value.ProductionActivityMasterResponsId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.ProductionActivityMasterResponsRecurrence.SingleOrDefault(p => p.ProductionActivityMasterResponsId == value.ProductionActivityMasterResponsId);
                ApplicationWikiRecurrences.ProductionActivityMasterResponsId = value.ProductionActivityMasterResponsId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new ProductionActivityMasterResponsRecurrence
                {
                    ProductionActivityMasterResponsId = value.ProductionActivityMasterResponsId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.ProductionActivityMasterResponsRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.ProductionActivityMasterResponsRecurrenceId = ApplicationWikiRecurrence.ProductionActivityMasterResponsRecurrenceId;
                return value;
            }
        }

        #region ProductionActivityMasterLine
        [HttpGet]
        [Route("GetProductionActivityMasterLine")]
        public List<ProductionActivityMasterLineModel> GetProductionActivityMasterLine(long? id)
        {
            List<ProductionActivityMasterLineModel> templateTestCaseLinkModels = new List<ProductionActivityMasterLineModel>();
            var templateTestCaseLink = _context.ProductionActivityMasterLine.Include(a => a.ProductionActivityMaster).Include(a => a.ProductionActivityMaster.ManufacturingProcess).Include(a => a.TemplateProfile).Include(a => a.ProdActivityCategoryChild).Include(a => a.ProdActivityActionChild).Where(w => w.ProductionActivityMasterId == id).ToList();
            templateTestCaseLink.ForEach(s =>
            {
                ProductionActivityMasterLineModel templateTestCaseModel = new ProductionActivityMasterLineModel();
                templateTestCaseModel.ManufacturingProcessChilds = s.ProductionActivityMaster?.ManufacturingProcess?.Value;
                templateTestCaseModel.ProductionActivityMasterLineId = s.ProductionActivityMasterLineId;
                templateTestCaseModel.ProductionActivityMasterId = s.ProductionActivityMasterId;
                templateTestCaseModel.NameOfTemplate = s.NameOfTemplate;
                templateTestCaseModel.Subject = s.Subject;
                templateTestCaseModel.Link = s.Link;
                templateTestCaseModel.LocationName = s.LocationName;
                templateTestCaseModel.Naming = s.Naming;
                templateTestCaseModel.IsAutoNumbering = s.IsAutoNumbering;
                templateTestCaseModel.AutoNumbering = s.IsAutoNumbering == true ? "Yes" : "No";
                templateTestCaseModel.TemplateProfileId = s.TemplateProfileId;
                templateTestCaseModel.TemplateProfileName = s.TemplateProfile?.Name;
                templateTestCaseModel.ProdActivityActionChildId = s.ProdActivityActionChildId;
                templateTestCaseModel.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                templateTestCaseModel.ProdActivityCategory = s.ProdActivityCategoryChild?.Value;
                templateTestCaseModel.ProdActivityAction = s.ProdActivityActionChild?.Value;
                templateTestCaseModel.DocumentNo = s.DocumentNo;
                templateTestCaseModel.LocationToSaveId = s.LocationToSaveId;
                templateTestCaseModel.TopicId = s.TopicId;
                templateTestCaseLinkModels.Add(templateTestCaseModel);
            });
            return templateTestCaseLinkModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityMasterLine")]
        public ProductionActivityMasterLineModel InsertProductionActivityMasterLine(ProductionActivityMasterLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCaseLink = new ProductionActivityMasterLine
            {
                ProductionActivityMasterId = value.ProductionActivityMasterId,
                Link = value.Link,
                Subject = value.Subject,
                NameOfTemplate = value.NameOfTemplate,
                LocationName = value.LocationName,
                ProdActivityCategoryChildId = value.ProdActivityCategoryChildId,
                ProdActivityActionChildId = value.ProdActivityActionChildId,
                IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false,
                TemplateProfileId = value.TemplateProfileId,
                Naming = value.Naming,
                SessionId = value.SessionId,
                LocationToSaveId = value.LocationToSaveId,
            };
            _context.ProductionActivityMasterLine.Add(templateTestCaseLink);
            _context.SaveChanges();
            value.ProductionActivityMasterLineId = templateTestCaseLink.ProductionActivityMasterLineId;
            value.SessionId = value.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityMasterLine")]
        public ProductionActivityMasterLineModel UpdateProductionActivityMasterLine(ProductionActivityMasterLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.ProductionActivityMasterLine.SingleOrDefault(p => p.ProductionActivityMasterLineId == value.ProductionActivityMasterLineId);
            templateTestCase.ProductionActivityMasterId = value.ProductionActivityMasterId;
            templateTestCase.Link = value.Link;
            templateTestCase.Subject = value.Subject;
            templateTestCase.NameOfTemplate = value.NameOfTemplate;
            templateTestCase.IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.LocationName = value.LocationName;
            templateTestCase.ProdActivityCategoryChildId = value.ProdActivityCategoryChildId;
            templateTestCase.ProdActivityActionChildId = value.ProdActivityActionChildId;
            templateTestCase.Naming = value.Naming;
            templateTestCase.SessionId = value.SessionId;
            templateTestCase.LocationToSaveId = value.LocationToSaveId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityMasterLineTopic")]
        public ProductionActivityMasterLineModel UpdateProductionActivityMasterLineTopic(ProductionActivityMasterLineModel value)
        {
            var templateTestCase = _context.ProductionActivityMasterLine.SingleOrDefault(p => p.ProductionActivityMasterLineId == value.ProductionActivityMasterLineId);
            templateTestCase.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityMasterLine")]
        public ActionResult<string> DeleteProductionActivityMasterLine(int id)
        {
            try
            {
                var templateTestCase = _context.ProductionActivityMasterLine.SingleOrDefault(p => p.ProductionActivityMasterLineId == id);
                if (templateTestCase != null)
                {
                    _context.ProductionActivityMasterLine.Remove(templateTestCase);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductionActivityMasterLine Cannot be delete! Reference to others");
            }
        }
        #endregion

        #region ActivityMasterinfo

        [HttpGet]
        [Route("GetActivityInfo")]
        public List<ActivityInfoModel> GetActivityInfo(long? id)
        {
            var ActivityMasterMultipleLists = new List<ActivityMasterMultiple>();
            List<long?> applicationMasterCodeIds = new List<long?> { 340 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var ActivityMasterMultiple = _context.ActivityMasterMultiple.Where(s => s.ProductionActivityAppLineId == id).ToList();
            List<ActivityInfoModel> ActivityInfoModels = new List<ActivityInfoModel>();

            if (ActivityMasterMultiple.Count > 0)
            {
                ActivityMasterMultiple.ForEach(s =>
                {
                    ActivityInfoModel ActivityInfoModel = new ActivityInfoModel();
                    ActivityInfoModel.ActivityMasterMultipleId = s.ActivityMasterMultipleId;
                    ActivityInfoModel.ActivityMasterNames = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.AcitivityMasterId).FirstOrDefault()?.Value;
                   
                    ActivityInfoModels.Add(ActivityInfoModel);

                });


            }

            return ActivityInfoModels;
        }
        [HttpPost]
        [Route("InsertActivityInfo")]
        public ActivityInfoModel InsertActivityInfo(ActivityInfoModel value)
        {
            var ActivityMasterMultiple = _context.ActivityMasterMultiple.Where(r => r.ProductionActivityAppLineId == value.ProductionActivityAppLineId).ToList();
            if (ActivityMasterMultiple != null && ActivityMasterMultiple.Count > 0)
            {
                _context.ActivityMasterMultiple.RemoveRange(ActivityMasterMultiple);
                _context.SaveChanges();
            }
            if (value.ActivityInfoIds.Count > 0)
            {
                value.ActivityInfoIds.ForEach(r =>
                {
                    var routineMultiple = new ActivityMasterMultiple();
                    routineMultiple.AcitivityMasterId = r;
                    routineMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                  
                    _context.ActivityMasterMultiple.Add(routineMultiple);

                });
                _context.SaveChanges();

            }


            return value;
        }
        [HttpPut]
        [Route("UpdateActivityInfo")]
        public ActivityInfoModel UpdateActivityInfo(ActivityInfoModel value)
        {
            var ActivityMasterMultiple = _context.ActivityMasterMultiple.Where(r => r.ProductionActivityAppLineId == value.ProductionActivityAppLineId).ToList();
            if (ActivityMasterMultiple != null && ActivityMasterMultiple.Count > 0)
            {
                _context.ActivityMasterMultiple.RemoveRange(ActivityMasterMultiple);
                _context.SaveChanges();
            }
            if (value.ActivityInfoIds.Count > 0)
            {
                value.ActivityInfoIds.ForEach(r =>
                {
                    var routineMultiple = new ActivityMasterMultiple();
                    routineMultiple.AcitivityMasterId = r;
                    routineMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                    _context.ActivityMasterMultiple.Add(routineMultiple);

                });
                _context.SaveChanges();

            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteActivityInfo")]
        public void DeleteActivityInfo(int id)
        {
            var activityMasterMultiple = _context.ActivityMasterMultiple.SingleOrDefault(p => p.ActivityMasterMultipleId == id);
            if (activityMasterMultiple != null)
            {
                _context.ActivityMasterMultiple.Remove(activityMasterMultiple);
                _context.SaveChanges();
            }
        }

        #endregion
    }
}
