﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HumanMovementController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public HumanMovementController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetHumanMovementSearchBySession")]
        public HumanMovementModel GetHumanMovementSearchBySession(Guid? SessionId, long? statusId)
        {
            HumanMovementModel humanMovementModel = new HumanMovementModel();
            //var humanMovementPrivate = _context.HumanMovementPrivate.Include(e => e.Employee).ToList();
            var humanMovement = _context.HumanMovement
               .Include(a => a.HumanMovementCompanyRelated).Include(c => c.HumanMovementPrivate).Where(w => w.SessionId == SessionId && w.DateAndTime.Value.Date == DateTime.Today.Date && w.MovementStatusId == statusId)
               .OrderByDescending(o => o.HumanMovementId).AsNoTracking().FirstOrDefault();
            if (humanMovement == null)
                return null;

            if (humanMovement != null)
            {
                humanMovementModel.HumanMovementId = humanMovement.HumanMovementId;
                humanMovementModel.Location = humanMovement.Location;
                humanMovementModel.Language = humanMovement.Language;
                humanMovementModel.IsSwstaffFlag = humanMovement.IsSwstaff == null ? "interCompany" : (humanMovement.IsSwstaff == true ? "Yes" : "No");
                humanMovementModel.EmployeeId = humanMovement.EmployeeId;
                humanMovementModel.EmployeeName = humanMovement.EmployeeName;
                humanMovementModel.ContactNo = humanMovement.ContactNo;
                humanMovementModel.VisitingPurpose = humanMovement.VisitingPurpose;
                humanMovementModel.PurposeOfVisit = humanMovement.PurposeOfVisit;
                humanMovementModel.CompanyId = humanMovement.CompanyId;
                humanMovementModel.NameOfCompany = humanMovement.NameOfCompany;
                humanMovementModel.CompanyRelatedpurposeOfVisit = humanMovement.CompanyRelatedpurposeOfVisit;
                humanMovementModel.GatheringFlag = humanMovement.Gathering == true ? "Yes" : "No";
                humanMovementModel.GatheringDetails = humanMovement.GatheringDetails;
                humanMovementModel.TravellingCountryFlag = humanMovement.TravellingCountry == true ? "Yes" : "No";
                humanMovementModel.TravellingCountryDetails = humanMovement.TravellingCountryDetails;
                humanMovementModel.ContactPatientsFlag = humanMovement.ContactPatients == true ? "Yes" : "No";
                humanMovementModel.ContactPatientsDetails = humanMovement.ContactPatientsDetails;
                humanMovementModel.Temperature = humanMovement.Temperature;
                humanMovementModel.HignFeverFlag = humanMovement.HignFever == true ? "Yes" : "No";
                humanMovementModel.CoughingFlag = humanMovement.Coughing == true ? "Yes" : "No";
                humanMovementModel.MildFeverFlag = humanMovement.MildFever == true ? "Yes" : "No";
                humanMovementModel.SoreThroatFlag = humanMovement.SoreThroat == true ? "Yes" : "No";
                humanMovementModel.MuscleJointPainFlag = humanMovement.MuscleJointPain == true ? "Yes" : "No";
                humanMovementModel.HeadacheFlag = humanMovement.Headache == true ? "Yes" : "No";
                humanMovementModel.ShortnessOfBreadFlag = humanMovement.ShortnessOfBread == true ? "Yes" : "No";
                humanMovementModel.DiarrheaFlag = humanMovement.Diarrhea == true ? "Yes" : "No";
                humanMovementModel.LossOfVoiceFlag = humanMovement.LossOfVoice == true ? "Yes" : "No";
                humanMovementModel.PhlegmFlag = humanMovement.Phlegm == true ? "Yes" : "No";
                humanMovementModel.FluFlag = humanMovement.Flu == true ? "Yes" : "No";
                humanMovementModel.NauseaVomitingFlag = humanMovement.NauseaVomiting == true ? "Yes" : "No";
                humanMovementModel.FatigueFlag = humanMovement.Fatigue == true ? "Yes" : "No";
                humanMovementModel.ConjunctivitsFlag = humanMovement.Conjunctivits == true ? "Yes" : "No";
                humanMovementModel.LossOfTasteSmellFlag = humanMovement.LossOfTasteSmell == true ? "Yes" : "No";
                humanMovementModel.LossOfApetiteFlag = humanMovement.LossOfApetite == true ? "Yes" : "No";
                humanMovementModel.RunnyStuffyNoseFlag = humanMovement.RunnyStuffyNose == true ? "Yes" : "No";
                humanMovementModel.SneezingFlag = humanMovement.Sneezing == true ? "Yes" : "No";
                humanMovementModel.ChillsFlag = humanMovement.Chills == true ? "Yes" : "No";
                humanMovementModel.NoForAllAboveFlag = humanMovement.NoForAllAbove == true ? "Yes" : "No";
                humanMovementModel.CoughingFlag = humanMovement.Coughing == true ? "Yes" : "No";
                humanMovementModel.SessionId = humanMovement.SessionId;
                humanMovementModel.PersonToSeeIds = humanMovement.HumanMovementPrivate != null ? (humanMovement.HumanMovementPrivate.Where(w => w.HumanMovementId == humanMovement.HumanMovementId).Select(w => w.EmployeeId.GetValueOrDefault(0)).ToList()) : new List<long>();
                humanMovementModel.CompanyRelatedpersonToSeeIds = humanMovement.HumanMovementCompanyRelated != null ? (humanMovement.HumanMovementCompanyRelated.Where(w => w.HumanMovementId == humanMovement.HumanMovementId).Select(w => w.EmployeeId.GetValueOrDefault(0)).ToList()) : new List<long>();
                humanMovementModel.MovementStatusId = humanMovement.MovementStatusId;
            }
            return humanMovementModel;
        }
        [HttpGet]
        [Route("GetHumanMovement")]
        public List<HumanMovementModel> GetHumanMovement()
        {
            List<HumanMovementModel> humanMovementModels = new List<HumanMovementModel>();
            var humanMovementCompanyRelated = _context.HumanMovementCompanyRelated.Include(e => e.Employee).ToList();
            var humanMovementPrivate = _context.HumanMovementPrivate.Include(e => e.Employee).ToList();
            var humanMovement = _context.HumanMovement
               .Include(a => a.Employee).Include(c => c.Company)
               .OrderByDescending(o => o.HumanMovementId).AsNoTracking().ToList();
            if (humanMovement != null && humanMovement.Count > 0)
            {
                humanMovement.ForEach(s =>
                {
                    HumanMovementModel humanMovementModel = new HumanMovementModel();
                    humanMovementModel.HumanMovementId = s.HumanMovementId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = s.Employee != null ? s.Employee.FirstName : null;
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (s.Employee != null ? s.Employee.FirstName : null);
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyName = s.Company != null ? s.Company.CompanyName : null;
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.Gathering = s.Gathering;
                    humanMovementModel.GatheringFlag = s.Gathering == true ? "Yes" : "No";
                    humanMovementModel.GatheringDetails = s.GatheringDetails;
                    humanMovementModel.TravellingCountry = s.TravellingCountry;
                    humanMovementModel.TravellingCountryFlag = s.TravellingCountry == true ? "Yes" : "No";
                    humanMovementModel.ContactPatients = s.ContactPatients;
                    humanMovementModel.TravellingCountryDetails = s.TravellingCountryDetails;
                    humanMovementModel.ContactPatientsFlag = s.ContactPatients == true ? "Yes" : "No";
                    humanMovementModel.ContactPatientsDetails = s.ContactPatientsDetails;
                    humanMovementModel.Temperature = s.Temperature;
                    humanMovementModel.HignFever = s.HignFever;
                    humanMovementModel.HignFeverFlag = s.HignFever == true ? "Yes" : "No";
                    humanMovementModel.Coughing = s.Coughing;
                    humanMovementModel.CoughingFlag = s.Coughing == true ? "Yes" : "No";
                    humanMovementModel.MildFever = s.MildFever;
                    humanMovementModel.MildFeverFlag = s.MildFever == true ? "Yes" : "No";
                    humanMovementModel.SoreThroat = s.SoreThroat;
                    humanMovementModel.SoreThroatFlag = s.SoreThroat == true ? "Yes" : "No";
                    humanMovementModel.MuscleJointPain = s.MuscleJointPain;
                    humanMovementModel.MuscleJointPainFlag = s.MuscleJointPain == true ? "Yes" : "No";
                    humanMovementModel.Headache = s.Headache;
                    humanMovementModel.HeadacheFlag = s.Headache == true ? "Yes" : "No";
                    humanMovementModel.ShortnessOfBread = s.ShortnessOfBread;
                    humanMovementModel.ShortnessOfBreadFlag = s.ShortnessOfBread == true ? "Yes" : "No";
                    humanMovementModel.Diarrhea = s.Diarrhea;
                    humanMovementModel.DiarrheaFlag = s.Diarrhea == true ? "Yes" : "No";
                    humanMovementModel.LossOfVoice = s.LossOfVoice;
                    humanMovementModel.LossOfVoiceFlag = s.LossOfVoice == true ? "Yes" : "No";
                    humanMovementModel.Phlegm = s.Phlegm;
                    humanMovementModel.PhlegmFlag = s.Phlegm == true ? "Yes" : "No";
                    humanMovementModel.Flu = s.Flu;
                    humanMovementModel.FluFlag = s.Flu == true ? "Yes" : "No";
                    humanMovementModel.NauseaVomiting = s.NauseaVomiting;
                    humanMovementModel.NauseaVomitingFlag = s.NauseaVomiting == true ? "Yes" : "No";
                    humanMovementModel.Fatigue = s.Fatigue;
                    humanMovementModel.FatigueFlag = s.Fatigue == true ? "Yes" : "No";
                    humanMovementModel.Conjunctivits = s.Conjunctivits;
                    humanMovementModel.ConjunctivitsFlag = s.Conjunctivits == true ? "Yes" : "No";
                    humanMovementModel.LossOfTasteSmell = s.LossOfTasteSmell;
                    humanMovementModel.LossOfTasteSmellFlag = s.LossOfTasteSmell == true ? "Yes" : "No";
                    humanMovementModel.LossOfApetite = s.LossOfApetite;
                    humanMovementModel.LossOfApetiteFlag = s.LossOfApetite == true ? "Yes" : "No";
                    humanMovementModel.RunnyStuffyNose = s.RunnyStuffyNose;
                    humanMovementModel.RunnyStuffyNoseFlag = s.RunnyStuffyNose == true ? "Yes" : "No";
                    humanMovementModel.Sneezing = s.Sneezing;
                    humanMovementModel.SneezingFlag = s.Sneezing == true ? "Yes" : "No";
                    humanMovementModel.Chills = s.Chills;
                    humanMovementModel.ChillsFlag = s.Chills == true ? "Yes" : "No";
                    humanMovementModel.NoForAllAbove = s.NoForAllAbove;
                    humanMovementModel.NoForAllAboveFlag = s.NoForAllAbove == true ? "Yes" : "No";
                    humanMovementModel.Coughing = s.Coughing;
                    humanMovementModel.CoughingFlag = s.Coughing == true ? "Yes" : "No";
                    humanMovementModel.SymptomsLists = ((s.HignFever == true ? "High Fever(38.0°C and above)," : "") +
                    (s.MildFever == true ? " Mild Fever(around 37.4°C - 37.9°C )," : "") +
                    (s.Coughing == true ? "Coughing," : "") +
                    (s.SoreThroat == true ? "Sore Throat," : "") +
                    (s.MuscleJointPain == true ? "Muscle/Joint pain," : "") +
                    (s.Headache == true ? "Headache," : "") +
                    (s.ShortnessOfBread == true ? "Shortness of breath," : "") +
                    (s.Diarrhea == true ? "Diarrhea," : "") +
                    (s.LossOfVoice == true ? "Loss of voice," : "") +
                    (s.Phlegm == true ? "Phlegm," : "") +
                    (s.NauseaVomiting == true ? "Nausea/Vomiting," : "") +
                    (s.Fatigue == true ? "Fatigue," : "") +
                    (s.Conjunctivits == true ? "Conjunctivitis," : "") +
                    (s.LossOfTasteSmell == true ? "Loss Of Taste/Smell," : "") +
                    (s.LossOfApetite == true ? "Loss Of Appetite," : "") +
                    (s.RunnyStuffyNose == true ? "Runny/StuffyNose," : "") +
                    (s.Sneezing == true ? "Sneezing," : "") +
                    (s.Chills == true ? "Chills/Repeated shaking with chills," : "")
                    // (s.Gathering == true ? "Gathering," : "") +
                    //(s.TravellingCountry == true ? "Traveling," : "") +
                    ).TrimEnd(',') + " " +
                    //(s.ContactPatients == true ? "In contact," : "")).TrimEnd(',')+" "+ 
                    (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + ",") : "") +
                    (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + ",") : "") +
                    (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                    humanMovementModel.LongQuestions = "b. -> Gathering:" + " " + s.GatheringDetails + "," +
                    "c-> Traveling:" + " " + s.TravellingCountryDetails + "," +
                    "d.-> In contact:" + " " + s.ContactPatientsDetails;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.HumanMovementId == s.HumanMovementId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.HumanMovementId == s.HumanMovementId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.HumanMovementId == s.HumanMovementId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.MovementStatusId = s.MovementStatusId;
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels;
        }
        [HttpPost]
        [Route("GetHumanMovementSearch")]
        public List<HumanMovementModel> GetHumanMovementSearch(HumanMovementSearchModel searchModel)
        {
            List<HumanMovementModel> humanMovementModels = new List<HumanMovementModel>();
            var humanMovement = _context.HumanMovement.Include(a => a.Employee).Include(p => p.Employee.Plant).Include(d => d.Employee.DepartmentNavigation).Include(p => p.Employee.Plant).Include(c => c.Company).Include(x => x.HumanMovementAction).Where(s => s.HumanMovementId > 0);
            if (searchModel.MovementStatusId != null)
            {
                humanMovement = humanMovement.Where(s => s.MovementStatusId == searchModel.MovementStatusId);
            }
            if (searchModel.StartDate != null && searchModel.EndDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime >= searchModel.StartDate);
            }
            if (searchModel.EndDate != null && searchModel.StartDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime <= searchModel.EndDate);
            }
            if (searchModel.StartDate != null && searchModel.EndDate != null)
            {
                humanMovement = humanMovement.Where(s => (s.DateAndTime >= searchModel.StartDate) && (s.DateAndTime <= searchModel.EndDate));
            }
            if (searchModel.LocationId.Count > 0 && searchModel.AllLocation == false)
            {
                humanMovement = humanMovement.Where(s => searchModel.LocationId.Contains(s.Location));
            }
            if (searchModel.CompanyId.Count > 0 && searchModel.AllVisitorCompany == false)
            {
                humanMovement = humanMovement.Where(s => searchModel.CompanyId.Contains(s.CompanyId));
                if (searchModel.IsSwstaff == "")
                {
                    humanMovement = humanMovement.Where(o => o.IsSwstaff != null);
                }
            }
            if (searchModel.AllVisitorCompany == true)
            {
                humanMovement = humanMovement.Where(o => o.IsSwstaff == false);
            }
            if (searchModel.Temperature == ">36")
            {
                humanMovement = humanMovement.Where(o => o.Temperature > 36);
            }
            if (searchModel.Temperature == ">37")
            {
                humanMovement = humanMovement.Where(o => o.Temperature > 37);
            }
            if (searchModel.Temperature == ">37.5")
            {
                humanMovement = humanMovement.Where(o => o.Temperature > (decimal)37.5);
            }
            if (searchModel.Temperature == ">38")
            {
                humanMovement = humanMovement.Where(o => o.Temperature > 38);
            }
            var humanMovements = humanMovement.OrderByDescending(o => o.HumanMovementId).AsNoTracking().ToList();
            List<HumanMovement> companyFilters = new List<HumanMovement>();
            if (searchModel.IsSwstaff != null)
            {
                if (searchModel.IsSwstaff == "Yes")
                {
                    humanMovements = humanMovements.Where(o => o.IsSwstaff == true).ToList();
                }
                else
                {
                    /*else if (searchModel.IsSwstaff == "No")
                    {
                        humanMovements = humanMovements.Where(o => o.IsSwstaff == false).ToList();
                    }*/
                    if (searchModel.IsSwstaff == "SWGP")
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == "SWMY" || h.Employee.Plant.PlantCode.Trim() == "SWSG")
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                    else
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == searchModel.IsSwstaff)
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                }
            }
            var questionnaireFlag = searchModel.Questionnaire == "Yes" ? true : false;
            if (searchModel.Questionnaire == "Yes")
            {
                humanMovements = humanMovements.Where(o => o.HignFever == questionnaireFlag ||
                o.MildFever == questionnaireFlag ||
                o.SoreThroat == questionnaireFlag ||
                o.MuscleJointPain == questionnaireFlag ||
                o.Headache == questionnaireFlag ||
                o.ShortnessOfBread == questionnaireFlag ||
                o.Diarrhea == questionnaireFlag ||
                o.LossOfVoice == questionnaireFlag ||
                o.Phlegm == questionnaireFlag ||
                o.NauseaVomiting == questionnaireFlag ||
                o.Fatigue == questionnaireFlag ||
                o.Conjunctivits == questionnaireFlag ||
                o.LossOfTasteSmell == questionnaireFlag ||
                o.LossOfApetite == questionnaireFlag ||
                o.RunnyStuffyNose == questionnaireFlag ||
                o.Sneezing == questionnaireFlag ||
                o.Chills == questionnaireFlag ||
                o.Gathering == questionnaireFlag ||
                o.TravellingCountry == questionnaireFlag ||
                o.ContactPatients == questionnaireFlag ||
                o.Coughing == questionnaireFlag
                ).ToList();
            }
            if (searchModel.Questionnaire == "No")
            {
                humanMovements = humanMovements.Where(o => o.HignFever == questionnaireFlag &&
                o.MildFever == questionnaireFlag &&
                o.SoreThroat == questionnaireFlag &&
                o.MuscleJointPain == questionnaireFlag &&
                o.Headache == questionnaireFlag &&
                o.ShortnessOfBread == questionnaireFlag &&
                o.Diarrhea == questionnaireFlag &&
                o.LossOfVoice == questionnaireFlag &&
                o.Phlegm == questionnaireFlag &&
                o.NauseaVomiting == questionnaireFlag &&
                o.Fatigue == questionnaireFlag &&
                o.Conjunctivits == questionnaireFlag &&
                o.LossOfTasteSmell == questionnaireFlag &&
                o.LossOfApetite == questionnaireFlag &&
                o.RunnyStuffyNose == questionnaireFlag &&
                o.Sneezing == questionnaireFlag &&
                o.Chills == questionnaireFlag &&
                o.Gathering == questionnaireFlag &&
                o.TravellingCountry == questionnaireFlag &&
                o.ContactPatients == questionnaireFlag &&
                o.Coughing == questionnaireFlag).ToList();
            }
            if (humanMovements != null && humanMovements.Count > 0)
            {
                var humanMovementCompanyRelated = _context.HumanMovementCompanyRelated.Include(e => e.Employee).ToList();
                var humanMovementPrivate = _context.HumanMovementPrivate.Include(e => e.Employee).ToList();
                humanMovements.ForEach(s =>
                {
                    HumanMovementModel humanMovementModel = new HumanMovementModel();
                    humanMovementModel.HumanMovementId = s.HumanMovementId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null;
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null);
                    humanMovementModel.SageId = s.Employee != null ? s.Employee.SageId : null;
                    humanMovementModel.Department = s.Employee != null && s.Employee.DepartmentNavigation != null ? s.Employee.DepartmentNavigation.Name : "";
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.CompanyName = s.IsSwstaff == false ? (s.Company != null ? s.Company.CompanyName : s.NameOfCompany) : (s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null);
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.Gathering = s.Gathering;
                    humanMovementModel.GatheringFlag = s.Gathering == true ? "Yes" : "No";
                    humanMovementModel.GatheringDetails = s.GatheringDetails;
                    humanMovementModel.TravellingCountry = s.TravellingCountry;
                    humanMovementModel.TravellingCountryFlag = s.TravellingCountry == true ? "Yes" : "No";
                    humanMovementModel.ContactPatients = s.ContactPatients;
                    humanMovementModel.TravellingCountryDetails = s.TravellingCountryDetails;
                    humanMovementModel.ContactPatientsFlag = s.ContactPatients == true ? "Yes" : "No";
                    humanMovementModel.ContactPatientsDetails = s.ContactPatientsDetails;
                    humanMovementModel.Temperature = s.Temperature;
                    humanMovementModel.HignFever = s.HignFever;
                    humanMovementModel.HignFeverFlag = s.HignFever == true ? "Yes" : "No";
                    humanMovementModel.MildFever = s.MildFever;
                    humanMovementModel.MildFeverFlag = s.MildFever == true ? "Yes" : "No";
                    humanMovementModel.Coughing = s.Coughing;
                    humanMovementModel.CoughingFlag = s.Coughing == true ? "Yes" : "No";
                    humanMovementModel.SoreThroat = s.SoreThroat;
                    humanMovementModel.SoreThroatFlag = s.SoreThroat == true ? "Yes" : "No";
                    humanMovementModel.MuscleJointPain = s.MuscleJointPain;
                    humanMovementModel.MuscleJointPainFlag = s.MuscleJointPain == true ? "Yes" : "No";
                    humanMovementModel.Headache = s.Headache;
                    humanMovementModel.HeadacheFlag = s.Headache == true ? "Yes" : "No";
                    humanMovementModel.ShortnessOfBread = s.ShortnessOfBread;
                    humanMovementModel.ShortnessOfBreadFlag = s.ShortnessOfBread == true ? "Yes" : "No";
                    humanMovementModel.Diarrhea = s.Diarrhea;
                    humanMovementModel.DiarrheaFlag = s.Diarrhea == true ? "Yes" : "No";
                    humanMovementModel.LossOfVoice = s.LossOfVoice;
                    humanMovementModel.LossOfVoiceFlag = s.LossOfVoice == true ? "Yes" : "No";
                    humanMovementModel.Phlegm = s.Phlegm;
                    humanMovementModel.PhlegmFlag = s.Phlegm == true ? "Yes" : "No";
                    humanMovementModel.Flu = s.Flu;
                    humanMovementModel.FluFlag = s.Flu == true ? "Yes" : "No";
                    humanMovementModel.NauseaVomiting = s.NauseaVomiting;
                    humanMovementModel.NauseaVomitingFlag = s.NauseaVomiting == true ? "Yes" : "No";
                    humanMovementModel.Fatigue = s.Fatigue;
                    humanMovementModel.FatigueFlag = s.Fatigue == true ? "Yes" : "No";
                    humanMovementModel.Conjunctivits = s.Conjunctivits;
                    humanMovementModel.ConjunctivitsFlag = s.Conjunctivits == true ? "Yes" : "No";
                    humanMovementModel.LossOfTasteSmell = s.LossOfTasteSmell;
                    humanMovementModel.LossOfTasteSmellFlag = s.LossOfTasteSmell == true ? "Yes" : "No";
                    humanMovementModel.LossOfApetite = s.LossOfApetite;
                    humanMovementModel.LossOfApetiteFlag = s.LossOfApetite == true ? "Yes" : "No";
                    humanMovementModel.RunnyStuffyNose = s.RunnyStuffyNose;
                    humanMovementModel.RunnyStuffyNoseFlag = s.RunnyStuffyNose == true ? "Yes" : "No";
                    humanMovementModel.Sneezing = s.Sneezing;
                    humanMovementModel.SneezingFlag = s.Sneezing == true ? "Yes" : "No";
                    humanMovementModel.Chills = s.Chills;
                    humanMovementModel.ChillsFlag = s.Chills == true ? "Yes" : "No";
                    humanMovementModel.NoForAllAbove = s.NoForAllAbove;
                    humanMovementModel.NoForAllAboveFlag = s.NoForAllAbove == true ? "Yes" : "No";
                    humanMovementModel.PlantCode = s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null;
                    if (questionnaireFlag == true)
                    {
                        humanMovementModel.SymptomsLists = ((s.HignFever == true ? "High Fever(38.0°C and above)," : "") +
                        (s.MildFever == true ? " Mild Fever(around 37.4°C - 37.9°C )," : "") +
                        (s.Coughing == true ? "Coughing," : "") +
                        (s.SoreThroat == true ? "Sore Throat," : "") +
                        (s.MuscleJointPain == true ? "Muscle/Joint pain," : "") +
                        (s.Headache == true ? "Headache," : "") +
                        (s.ShortnessOfBread == true ? "Shortness of breath," : "") +
                        (s.Diarrhea == true ? "Diarrhea," : "") +
                        (s.LossOfVoice == true ? "Loss of voice," : "") +
                        (s.Phlegm == true ? "Phlegm," : "") +
                        (s.NauseaVomiting == true ? "Nausea/Vomiting," : "") +
                        (s.Fatigue == true ? "Fatigue," : "") +
                        (s.Conjunctivits == true ? "Conjunctivitis," : "") +
                        (s.LossOfTasteSmell == true ? "Loss Of Taste/Smell," : "") +
                        (s.LossOfApetite == true ? "Loss Of Appetite," : "") +
                        (s.RunnyStuffyNose == true ? "Runny/StuffyNose," : "") +
                        (s.Sneezing == true ? "Sneezing," : "") +
                        (s.Chills == true ? "Chills/Repeated shaking with chills," : "")
                        // (s.Gathering == true ? "Gathering," : "") +
                        //(s.TravellingCountry == true ? "Traveling," : "") +
                        ).TrimEnd(',') + " " +
                        //(s.ContactPatients == true ? "In contact," : "")).TrimEnd(',')+" "+ 
                        (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + ",") : "") +
                        (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + ",") : "") +
                        (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                        humanMovementModel.SymptomsListsSeparate = ((s.HignFever == true ? "- High Fever(38.0°C and above)\n" : "") +
                        (s.MildFever == true ? "- Mild Fever(around 37.4°C - 37.9°C )\n" : "") +
                        (s.Coughing == true ? "- Coughing\n" : "") +
                        (s.SoreThroat == true ? "- Sore Throat\n" : "") +
                        (s.MuscleJointPain == true ? "- Muscle/Joint pain\n" : "") +
                        (s.Headache == true ? "- Headache\n" : "") +
                        (s.ShortnessOfBread == true ? "- Shortness of breath\n" : "") +
                        (s.Diarrhea == true ? "- Diarrhea\n" : "") +
                        (s.LossOfVoice == true ? "- Loss of voice\n" : "") +
                        (s.Phlegm == true ? "- Phlegm\n" : "") +
                        (s.NauseaVomiting == true ? "- Nausea/Vomiting\n" : "") +
                        (s.Fatigue == true ? "- Fatigue\n" : "") +
                        (s.Conjunctivits == true ? "- Conjunctivitis\n" : "") +
                        (s.LossOfTasteSmell == true ? "- Loss Of Taste/Smell\n" : "") +
                        (s.LossOfApetite == true ? "- Loss Of Appetite\n" : "") +
                        (s.RunnyStuffyNose == true ? "- Runny/StuffyNose\n" : "") +
                        (s.Sneezing == true ? "- Sneezing\n" : "") +
                        (s.Chills == true ? "- Chills/Repeated shaking with chills\n" : "")
                        // (s.Gathering == true ? "Gathering," : "") +
                        //(s.TravellingCountry == true ? "Traveling," : "") +
                        ).TrimEnd(',') + " " +
                        //(s.ContactPatients == true ? "In contact," : "")).TrimEnd(',')+" "+ 
                        (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + "\n") : "") +
                        (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + "\n") : "") +
                        (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                    }
                    /*else
                    {
                        humanMovementModel.SymptomsLists = ((s.HignFever == false ? "High Fever(38.0°C and above)," : "") +
                        (s.MildFever == false ? " Mild Fever(around 37.4°C - 37.9°C )," : "") +
                        (s.Coughing == false ? "Coughing," : "") +
                        (s.SoreThroat == false ? "Sore Throat," : "") +
                        (s.MuscleJointPain == false ? "Muscle/Joint pain," : "") +
                        (s.Headache == false ? "Headache," : "") +
                        (s.ShortnessOfBread == false ? "Shortness of breath," : "") +
                        (s.Diarrhea == false ? "Diarrhea," : "") +
                        (s.LossOfVoice == false ? "Loss of voice," : "") +
                        (s.Phlegm == false ? "Phlegm," : "") +
                        (s.NauseaVomiting == false ? "Nausea/Vomiting," : "") +
                        (s.Fatigue == false ? "Fatigue," : "") +
                        (s.Conjunctivits == false ? "Conjunctivitis," : "") +
                        (s.LossOfTasteSmell == false ? "Loss Of Taste/Smell," : "") +
                        (s.LossOfApetite == false ? "Loss Of Appetite," : "") +
                        (s.RunnyStuffyNose == false ? "Runny/StuffyNose," : "") +
                        (s.Sneezing == false ? "Sneezing," : "") +
                        (s.Chills == false ? "Chills/Repeated shaking with chills," : "")
                        // (s.Gathering == true ? "Gathering," : "") +
                        //(s.TravellingCountry == true ? "Traveling," : "") +
                        ).TrimEnd(',') + " " +
                        //(s.ContactPatients == true ? "In contact," : "")).TrimEnd(',')+" "+ 
                        (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + ",") : "") +
                        (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + ",") : "") +
                        (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                    }*/
                    humanMovementModel.LongQuestions = "b. -> Gathering:" + " " + s.GatheringDetails + "," +
                    "c-> Traveling:" + " " + s.TravellingCountryDetails + "," +
                    "d.-> In contact:" + " " + s.ContactPatientsDetails;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.HumanMovementId == s.HumanMovementId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        humanMovementModel.PurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.HumanMovementId == s.HumanMovementId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.HumanMovementId == s.HumanMovementId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.MovementStatusId = s.MovementStatusId;
                    var humanmovementAtion = _context.HumanMovementAction.SingleOrDefault(ww => ww.HumanMovementId == s.HumanMovementId);
                    humanMovementModel.Description = humanmovementAtion != null ? humanmovementAtion.Description : null;
                    humanMovementModel.Hrdescription = humanmovementAtion != null ? humanmovementAtion.Hrdescription : null;
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels.OrderBy(w => w.PlantCode).ToList();
        }
        [HttpPost]
        [Route("GetHumanMovementActionSearch")]
        public List<HumanMovementModel> GetHumanMovementActionSearch(HumanMovementSearchModel searchModel)
        {
            List<HumanMovementModel> humanMovementModels = new List<HumanMovementModel>();
            var humanMovement = _context.HumanMovement.Include(a => a.Employee).Include(p => p.Employee.Plant).Include(d => d.Employee.DepartmentNavigation).Include(p => p.Employee.Plant).Include(c => c.Company).Include(x => x.HumanMovementAction).Where(s => s.HumanMovementId > 0);
            if (searchModel.StartDate != null && searchModel.EndDate != null)
            {
                humanMovement = humanMovement.Where(s => (s.DateAndTime >= searchModel.StartDate) && (s.DateAndTime <= searchModel.EndDate));
            }
            if (searchModel.Temperature == ">37.5")
            {
                humanMovement = humanMovement.Where(o => o.Temperature > (decimal)37.5);
            }
            if (searchModel.Temperature == "<37.5")
            {
                humanMovement = humanMovement.Where(o => o.Temperature < (decimal)37.5);
            }
            var humanMovements = humanMovement.OrderByDescending(o => o.HumanMovementId).AsNoTracking().ToList();
            List<HumanMovement> companyFilters = new List<HumanMovement>();
            if (searchModel.IsSwstaff != null)
            {
                if (searchModel.IsSwstaff == "Yes")
                {
                    humanMovements = humanMovements.Where(o => o.IsSwstaff == true).ToList();
                }
            }
            var questionnaireFlag = searchModel.Questionnaire == "Yes" ? true : false;
            if (searchModel.Questionnaire == "Yes")
            {
                humanMovements = humanMovements.Where(o => o.HignFever == questionnaireFlag ||
                o.MildFever == questionnaireFlag ||
                o.SoreThroat == questionnaireFlag ||
                o.MuscleJointPain == questionnaireFlag ||
                o.Headache == questionnaireFlag ||
                o.ShortnessOfBread == questionnaireFlag ||
                o.Diarrhea == questionnaireFlag ||
                o.LossOfVoice == questionnaireFlag ||
                o.Phlegm == questionnaireFlag ||
                o.NauseaVomiting == questionnaireFlag ||
                o.Fatigue == questionnaireFlag ||
                o.Conjunctivits == questionnaireFlag ||
                o.LossOfTasteSmell == questionnaireFlag ||
                o.LossOfApetite == questionnaireFlag ||
                o.RunnyStuffyNose == questionnaireFlag ||
                o.Sneezing == questionnaireFlag ||
                o.Chills == questionnaireFlag ||
                o.Gathering == questionnaireFlag ||
                o.TravellingCountry == questionnaireFlag ||
                o.ContactPatients == questionnaireFlag ||
                o.Coughing == questionnaireFlag
                ).ToList();
            }
            if (searchModel.Questionnaire == "No")
            {
                humanMovements = humanMovements.Where(o => o.HignFever == questionnaireFlag ||
                o.MildFever == questionnaireFlag &&
                o.SoreThroat == questionnaireFlag &&
                o.MuscleJointPain == questionnaireFlag &&
                o.Headache == questionnaireFlag &&
                o.ShortnessOfBread == questionnaireFlag &&
                o.Diarrhea == questionnaireFlag &&
                o.LossOfVoice == questionnaireFlag &&
                o.Phlegm == questionnaireFlag &&
                o.NauseaVomiting == questionnaireFlag &&
                o.Fatigue == questionnaireFlag &&
                o.Conjunctivits == questionnaireFlag &&
                o.LossOfTasteSmell == questionnaireFlag &&
                o.LossOfApetite == questionnaireFlag &&
                o.RunnyStuffyNose == questionnaireFlag &&
                o.Sneezing == questionnaireFlag &&
                o.Chills == questionnaireFlag &&
                o.Gathering == questionnaireFlag &&
                o.TravellingCountry == questionnaireFlag &&
                o.ContactPatients == questionnaireFlag &&
                o.Coughing == questionnaireFlag).ToList();
            }
           // var employeeList = humanMovements.GroupBy(g => g.EmployeeId).Select(g => g.Key).ToList();
            var employeeList = humanMovements.ToList();
            if (employeeList != null && employeeList.Count > 0)
            {
                var humanMovementCompanyRelated = _context.HumanMovementCompanyRelated.Include(e => e.Employee).ToList();
                var humanMovementPrivate = _context.HumanMovementPrivate.Include(e => e.Employee).ToList();
                employeeList.ForEach(e =>
                {
                    var s = e;
                   // var s = humanMovements.Where(w => w.EmployeeId == e && w.DateAndTime.Value.Date==searchModel.StartDate.Value).OrderBy(o => o.DateAndTime).FirstOrDefault();
                    HumanMovementModel humanMovementModel = new HumanMovementModel();
                    humanMovementModel.HumanMovementId = s.HumanMovementId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null;
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null);
                    humanMovementModel.SageId = s.Employee != null ? s.Employee.SageId : null;
                    humanMovementModel.Department = s.Employee != null && s.Employee.DepartmentNavigation != null ? s.Employee.DepartmentNavigation.Name : "";
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.CompanyName = s.IsSwstaff == false ? (s.Company != null ? s.Company.CompanyName : s.NameOfCompany) : (s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null);
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.Gathering = s.Gathering;
                    humanMovementModel.GatheringFlag = s.Gathering == true ? "Yes" : "No";
                    humanMovementModel.GatheringDetails = s.GatheringDetails;
                    humanMovementModel.TravellingCountry = s.TravellingCountry;
                    humanMovementModel.TravellingCountryFlag = s.TravellingCountry == true ? "Yes" : "No";
                    humanMovementModel.ContactPatients = s.ContactPatients;
                    humanMovementModel.TravellingCountryDetails = s.TravellingCountryDetails;
                    humanMovementModel.ContactPatientsFlag = s.ContactPatients == true ? "Yes" : "No";
                    humanMovementModel.ContactPatientsDetails = s.ContactPatientsDetails;
                    humanMovementModel.Temperature = s.Temperature;
                    humanMovementModel.HignFever = s.HignFever;
                    humanMovementModel.HignFeverFlag = s.HignFever == true ? "Yes" : "No";
                    humanMovementModel.MildFever = s.MildFever;
                    humanMovementModel.MildFeverFlag = s.MildFever == true ? "Yes" : "No";
                    humanMovementModel.Coughing = s.Coughing;
                    humanMovementModel.CoughingFlag = s.Coughing == true ? "Yes" : "No";
                    humanMovementModel.SoreThroat = s.SoreThroat;
                    humanMovementModel.SoreThroatFlag = s.SoreThroat == true ? "Yes" : "No";
                    humanMovementModel.MuscleJointPain = s.MuscleJointPain;
                    humanMovementModel.MuscleJointPainFlag = s.MuscleJointPain == true ? "Yes" : "No";
                    humanMovementModel.Headache = s.Headache;
                    humanMovementModel.HeadacheFlag = s.Headache == true ? "Yes" : "No";
                    humanMovementModel.ShortnessOfBread = s.ShortnessOfBread;
                    humanMovementModel.ShortnessOfBreadFlag = s.ShortnessOfBread == true ? "Yes" : "No";
                    humanMovementModel.Diarrhea = s.Diarrhea;
                    humanMovementModel.DiarrheaFlag = s.Diarrhea == true ? "Yes" : "No";
                    humanMovementModel.LossOfVoice = s.LossOfVoice;
                    humanMovementModel.LossOfVoiceFlag = s.LossOfVoice == true ? "Yes" : "No";
                    humanMovementModel.Phlegm = s.Phlegm;
                    humanMovementModel.PhlegmFlag = s.Phlegm == true ? "Yes" : "No";
                    humanMovementModel.Flu = s.Flu;
                    humanMovementModel.FluFlag = s.Flu == true ? "Yes" : "No";
                    humanMovementModel.NauseaVomiting = s.NauseaVomiting;
                    humanMovementModel.NauseaVomitingFlag = s.NauseaVomiting == true ? "Yes" : "No";
                    humanMovementModel.Fatigue = s.Fatigue;
                    humanMovementModel.FatigueFlag = s.Fatigue == true ? "Yes" : "No";
                    humanMovementModel.Conjunctivits = s.Conjunctivits;
                    humanMovementModel.ConjunctivitsFlag = s.Conjunctivits == true ? "Yes" : "No";
                    humanMovementModel.LossOfTasteSmell = s.LossOfTasteSmell;
                    humanMovementModel.LossOfTasteSmellFlag = s.LossOfTasteSmell == true ? "Yes" : "No";
                    humanMovementModel.LossOfApetite = s.LossOfApetite;
                    humanMovementModel.LossOfApetiteFlag = s.LossOfApetite == true ? "Yes" : "No";
                    humanMovementModel.RunnyStuffyNose = s.RunnyStuffyNose;
                    humanMovementModel.RunnyStuffyNoseFlag = s.RunnyStuffyNose == true ? "Yes" : "No";
                    humanMovementModel.Sneezing = s.Sneezing;
                    humanMovementModel.SneezingFlag = s.Sneezing == true ? "Yes" : "No";
                    humanMovementModel.Chills = s.Chills;
                    humanMovementModel.ChillsFlag = s.Chills == true ? "Yes" : "No";
                    humanMovementModel.NoForAllAbove = s.NoForAllAbove;
                    humanMovementModel.NoForAllAboveFlag = s.NoForAllAbove == true ? "Yes" : "No";
                    humanMovementModel.PlantCode = s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null;
                    if (questionnaireFlag == true)
                    {
                        humanMovementModel.SymptomsLists = ((s.HignFever == true ? "High Fever(38.0°C and above)," : "") +
                        (s.MildFever == true ? " Mild Fever(around 37.4°C - 37.9°C )," : "") +
                        (s.Coughing == true ? "Coughing," : "") +
                        (s.SoreThroat == true ? "Sore Throat," : "") +
                        (s.MuscleJointPain == true ? "Muscle/Joint pain," : "") +
                        (s.Headache == true ? "Headache," : "") +
                        (s.ShortnessOfBread == true ? "Shortness of breath," : "") +
                        (s.Diarrhea == true ? "Diarrhea," : "") +
                        (s.LossOfVoice == true ? "Loss of voice," : "") +
                        (s.Phlegm == true ? "Phlegm," : "") +
                        (s.NauseaVomiting == true ? "Nausea/Vomiting," : "") +
                        (s.Fatigue == true ? "Fatigue," : "") +
                        (s.Conjunctivits == true ? "Conjunctivitis," : "") +
                        (s.LossOfTasteSmell == true ? "Loss Of Taste/Smell," : "") +
                        (s.LossOfApetite == true ? "Loss Of Appetite," : "") +
                        (s.RunnyStuffyNose == true ? "Runny/StuffyNose," : "") +
                        (s.Sneezing == true ? "Sneezing," : "") +
                        (s.Chills == true ? "Chills/Repeated shaking with chills," : "")
                        ).TrimEnd(',') + " " +
                        (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + ",") : "") +
                        (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + ",") : "") +
                        (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                        humanMovementModel.SymptomsListsSeparate = ((s.HignFever == true ? "- High Fever(38.0°C and above)\n" : "") +
                        (s.MildFever == true ? "- Mild Fever(around 37.4°C - 37.9°C )\n" : "") +
                        (s.Coughing == true ? "- Coughing\n" : "") +
                        (s.SoreThroat == true ? "- Sore Throat\n" : "") +
                        (s.MuscleJointPain == true ? "- Muscle/Joint pain\n" : "") +
                        (s.Headache == true ? "- Headache\n" : "") +
                        (s.ShortnessOfBread == true ? "- Shortness of breath\n" : "") +
                        (s.Diarrhea == true ? "- Diarrhea\n" : "") +
                        (s.LossOfVoice == true ? "- Loss of voice\n" : "") +
                        (s.Phlegm == true ? "- Phlegm\n" : "") +
                        (s.NauseaVomiting == true ? "- Nausea/Vomiting\n" : "") +
                        (s.Fatigue == true ? "- Fatigue\n" : "") +
                        (s.Conjunctivits == true ? "- Conjunctivitis\n" : "") +
                        (s.LossOfTasteSmell == true ? "- Loss Of Taste/Smell\n" : "") +
                        (s.LossOfApetite == true ? "- Loss Of Appetite\n" : "") +
                        (s.RunnyStuffyNose == true ? "- Runny/StuffyNose\n" : "") +
                        (s.Sneezing == true ? "- Sneezing\n" : "") +
                        (s.Chills == true ? "- Chills/Repeated shaking with chills\n" : "")
                        ).TrimEnd(',') + " " +
                        (s.Gathering == true ? ("b. -> Gathering:" + " " + s.GatheringDetails + "\n") : "") +
                        (s.TravellingCountry == true ? ("c-> Traveling:" + " " + s.TravellingCountryDetails + "\n") : "") +
                        (s.ContactPatients == true ? ("d.-> In contact:" + " " + s.ContactPatientsDetails) : "");
                    }
                    humanMovementModel.LongQuestions = "b. -> Gathering:" + " " + s.GatheringDetails + "," +
                    "c-> Traveling:" + " " + s.TravellingCountryDetails + "," +
                    "d.-> In contact:" + " " + s.ContactPatientsDetails;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.HumanMovementId == s.HumanMovementId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        humanMovementModel.PurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.HumanMovementId == s.HumanMovementId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.HumanMovementId == s.HumanMovementId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.MovementStatusId = s.MovementStatusId;
                    var humanmovementAtion = _context.HumanMovementAction.SingleOrDefault(ww => ww.HumanMovementId == s.HumanMovementId);
                    humanMovementModel.Description = humanmovementAtion != null ? humanmovementAtion.Description : null;
                    humanMovementModel.Hrdescription = humanmovementAtion != null ? humanmovementAtion.Hrdescription : null;
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels.OrderBy(w => w.PlantCode).ToList();
        }
        [HttpGet]
        [Route("GetHumanMovementLocation")]
        public List<HumanMovementModel> GetHumanMovementLocation()
        {
            List<HumanMovementModel> humanMovementModels = new List<HumanMovementModel>();
            var humanMovement = _context.HumanMovement.Where(l => l.Location != null).OrderByDescending(o => o.HumanMovementId).AsNoTracking().Select(s => s.Location).Distinct().ToList();
            if (humanMovement != null && humanMovement.Count > 0)
            {
                humanMovement.ForEach(s =>
                {
                    HumanMovementModel humanMovementModel = new HumanMovementModel();
                    humanMovementModel.Location = s;
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels;
        }
        [HttpPost]
        [Route("InsertHumanMovement")]
        public HumanMovementModel Post(HumanMovementModel value)
        {
            var SessionId = Guid.NewGuid();
            var humanMovement = new HumanMovement
            {
                DateAndTime = value.DateAndTime,
                Language = value.Language,
                Location = value.Location,
                IsSwstaff = value.IsSwstaffFlag == "interCompany" ? default(bool?) : (value.IsSwstaffFlag == "Yes" ? true : false),
                EmployeeId = value.EmployeeId,
                EmployeeName = value.EmployeeName,
                ContactNo = value.ContactNo,
                VisitingPurpose = value.VisitingPurpose,
                PurposeOfVisit = value.PurposeOfVisit,
                CompanyId = value.CompanyId == -1 ? null : value.CompanyId,
                NameOfCompany = value.NameOfCompany,
                CompanyRelatedpurposeOfVisit = value.CompanyRelatedpurposeOfVisit,
                HignFever = value.HignFeverFlag == "Yes" ? true : false,
                MildFever = value.MildFeverFlag == "Yes" ? true : false,
                SoreThroat = value.SoreThroatFlag == "Yes" ? true : false,
                MuscleJointPain = value.MuscleJointPainFlag == "Yes" ? true : false,
                Headache = value.HeadacheFlag == "Yes" ? true : false,
                ShortnessOfBread = value.ShortnessOfBreadFlag == "Yes" ? true : false,
                Diarrhea = value.DiarrheaFlag == "Yes" ? true : false,
                LossOfVoice = value.LossOfVoiceFlag == "Yes" ? true : false,
                Phlegm = value.PhlegmFlag == "Yes" ? true : false,
                Flu = value.FluFlag == "Yes" ? true : false,
                Gathering = value.GatheringFlag == "Yes" ? true : false,
                GatheringDetails = value.GatheringDetails,
                TravellingCountry = value.TravellingCountryFlag == "Yes" ? true : false,
                TravellingCountryDetails = value.TravellingCountryDetails,
                ContactPatients = value.ContactPatientsFlag == "Yes" ? true : false,
                ContactPatientsDetails = value.ContactPatientsDetails,
                Temperature = value.Temperature,
                SessionId = SessionId,
                NauseaVomiting = value.NauseaVomitingFlag == "Yes" ? true : false,
                Sneezing = value.SneezingFlag == "Yes" ? true : false,
                Fatigue = value.FatigueFlag == "Yes" ? true : false,
                Conjunctivits = value.ConjunctivitsFlag == "Yes" ? true : false,
                LossOfTasteSmell = value.LossOfTasteSmellFlag == "Yes" ? true : false,
                LossOfApetite = value.LossOfApetiteFlag == "Yes" ? true : false,
                RunnyStuffyNose = value.RunnyStuffyNoseFlag == "Yes" ? true : false,
                Chills = value.ChillsFlag == "Yes" ? true : false,
                NoForAllAbove = value.NoForAllAbove,
                Coughing = value.CoughingFlag == "Yes" ? true : false,
                MovementStatusId = value.MovementStatusId,
            };
            if (value.PersonToSeeIds != null)
            {
                value.PersonToSeeIds.ForEach(c =>
                {
                    var PersonToSeeItems = new HumanMovementPrivate
                    {
                        EmployeeId = c,
                    };
                    humanMovement.HumanMovementPrivate.Add(PersonToSeeItems);
                });
            }
            if (value.CompanyRelatedpersonToSeeIds != null)
            {
                value.CompanyRelatedpersonToSeeIds.ForEach(c =>
                {
                    if (c != 0)
                    {
                        var cPersonToSeeItems = new HumanMovementCompanyRelated
                        {
                            EmployeeId = c == -1 ? default(long?) : c,
                        };
                        humanMovement.HumanMovementCompanyRelated.Add(cPersonToSeeItems);
                    }
                });
            }
            _context.HumanMovement.Add(humanMovement);
            _context.SaveChanges();
            value.HumanMovementId = humanMovement.HumanMovementId;
            value.SessionId = SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateHumanMovement")]
        public HumanMovementModel Put(HumanMovementModel value)
        {
            var humanMovement = _context.HumanMovement.SingleOrDefault(p => p.HumanMovementId == value.HumanMovementId);
            humanMovement.MovementStatusId = value.MovementStatusId;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UploadHumanDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new HumanMovementDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    SessionId = SessionId,
                };
                _context.HumanMovementDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpPost]
        [Route("GetLocationCompany")]
        public SearchModel GetLocationCompany(SearchModel searchModel)
        {
            var locationCompany = _context.Ictmaster.Include(c => c.Company).Where(s => s.Name == searchModel.SearchString).FirstOrDefault();
            SearchModel searchItem = new SearchModel();
            if (locationCompany != null)
            {
                searchItem.MasterTypeID = locationCompany.IctmasterId;
                searchItem.SearchString = locationCompany.Company != null ? locationCompany.Company.PlantCode : null;
            };
            return searchItem;
        }
        [HttpPost]
        [Route("InsertEmployeeSageInformation")]
        public EmployeeSageInformationModel Post(EmployeeSageInformationModel value)
        {
            var employeeSageInformation = new EmployeeSageInformation
            {
                EmployeeId = value.EmployeeId,
                SageId = value.SageId,
                CheckinDateTime = value.CheckinDateTime,
                AttendenceDate = value.AttendenceDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                Remarks = value.Remarks,
            };
            _context.EmployeeSageInformation.Add(employeeSageInformation);
            _context.SaveChanges();
            value.SageInformationId = employeeSageInformation.SageInformationId;
            return value;
        }
        [HttpPost]
        [Route("AttendanceSync")]
        public string AttendanceSync(List<EmployeeSageInformationModel> values)
        {
            var employee = _context.Employee.Select(s => new
            {
                s.SageId,
                s.EmployeeId
            }).ToList();

            values.ForEach(value =>
            {
                var emp = employee.FirstOrDefault(f => f.SageId == value.SageId);
                if (emp != null)
                {
                    var employeeSageInformation = new EmployeeSageInformation
                    {
                        EmployeeId = emp.EmployeeId,
                        SageId = value.SageId,
                        CheckinDateTime = value.CheckinDateTime,
                        AttendenceDate = value.AttendenceDate,
                        StatusCodeId = 1,
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now,
                        Remarks = value.Remarks,
                    };
                    _context.EmployeeSageInformation.Add(employeeSageInformation);
                }
            });
            _context.SaveChanges();
            return "";
        }
        [HttpPut]
        [Route("UpdateEmployeeSageInformation")]
        public EmployeeSageInformationModel Put(EmployeeSageInformationModel value)
        {
            var employeeSageInformation = _context.EmployeeSageInformation.SingleOrDefault(p => p.SageInformationId == value.SageInformationId);
            //employeeSageInformation.EmployeeId = value.EmployeeId;
            //employeeSageInformation.SageId = value.SageId;
            //employeeSageInformation.CheckinDateTime = value.CheckinDateTime;
            //employeeSageInformation.AttendenceDate = value.AttendenceDate;
            employeeSageInformation.StatusCodeId = value.StatusCodeID.Value;
            employeeSageInformation.ModifiedByUserId = value.ModifiedByUserID;
            employeeSageInformation.ModifiedDate = DateTime.Now;
            employeeSageInformation.Remarks = value.Remarks;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("GetHumanMovementSage")]
        public List<HumanMovementSageModel> GetHumanMovementSage(HumanMovementSearchModel searchModel)
        {
            var humanMovementAction = _context.HumanMovementAction.Include(s => s.Pharmacist).ToList();
            List<HumanMovementSageModel> humanMovementSageModels = new List<HumanMovementSageModel>();
            var humanMovementSages = _context.EmployeeSageInformation.Include(e => e.Employee).Where(w => w.SageInformationId > 0);

            var humanMovement = _context.HumanMovement.Include(a => a.Employee).Include(p => p.Employee.Plant).Where(s => s.HumanMovementId > 0 && s.IsSwstaff == true);
            if (searchModel.StartDate != null)
            {
                humanMovement = humanMovement.Where(w => w.DateAndTime.Value.Date >= searchModel.StartDate.Value.Date && w.DateAndTime.Value.Date <= searchModel.EndDate.Value.Date);
                humanMovementSages = humanMovementSages.Where(w => w.AttendenceDate.Value.Day == searchModel.StartDate.Value.Day && w.AttendenceDate.Value.Month == searchModel.StartDate.Value.Month && w.AttendenceDate.Value.Year == searchModel.StartDate.Value.Year);
            }
            if (searchModel.PlantId != null)
            {
                humanMovement = humanMovement.Where(w => w.Employee.PlantId == searchModel.PlantId);
                humanMovementSages = humanMovementSages.Where(w => w.Employee.PlantId == searchModel.PlantId);
            }
            var humanMovements = humanMovement.OrderByDescending(o => o.HumanMovementId).ToList();
            var humanMovementSage = humanMovementSages.OrderByDescending(o => o.SageInformationId).ToList();
            var humanMovementempList = humanMovement.GroupBy(s => s.EmployeeId).Select(s => s.Key).ToList();
            var humanMovementSageEmpList = humanMovementSages.GroupBy(s => s.EmployeeId).Select(s => s.Key).ToList();
            var emplist = (humanMovementempList.Concat(humanMovementSageEmpList).ToList()).Distinct().ToList();
            if (emplist != null)
            {
                emplist.ForEach(h =>
                {
                    var sageItems = humanMovementSage.Where(w => w.EmployeeId == h).FirstOrDefault();
                    var movementList = humanMovements.Where(w => w.EmployeeId == h).OrderBy(w => w.HumanMovementId).FirstOrDefault();
                    HumanMovementSageModel humanMovementSageModel = new HumanMovementSageModel();
                    humanMovementSageModel.SageInformationId = sageItems != null ? sageItems.SageInformationId : 0;
                    humanMovementSageModel.SageId = sageItems != null ? sageItems.SageId : "";
                    humanMovementSageModel.Remarks = sageItems != null ? sageItems.Remarks : "";
                    humanMovementSageModel.Name = sageItems != null && sageItems.Employee != null ? (sageItems.Employee.FirstName + " " + sageItems.Employee.LastName) : "";
                    humanMovementSageModel.ScanStatus = sageItems != null ? "Yes" : "No";
                    humanMovementSageModel.EmployeeId = h;
                    humanMovementSageModel.HumanMovementId = movementList != null ? movementList.HumanMovementId : 0;
                    humanMovementSageModel.Temperature = movementList != null ? movementList.Temperature : null;
                    humanMovementSageModel.ActionByPharmacist = "No";
                    humanMovementSageModel.MovementStatus = "No";
                    if (movementList != null)
                    {
                        humanMovementSageModel.SageId = movementList != null && movementList.Employee != null ? movementList.Employee.SageId : "";
                        humanMovementSageModel.Name = movementList != null && movementList.Employee != null ? (movementList.Employee.FirstName + " " + movementList.Employee.LastName) : "";
                        var humanMovementActionData = movementList != null ? humanMovementAction.Where(w => w.HumanMovementId == movementList.HumanMovementId).Count() : 0;
                        humanMovementSageModel.ActionByPharmacist = humanMovementActionData > 0 ? "Yes" : "No";
                        humanMovementSageModel.MovementStatus = "Yes";
                    }
                    humanMovementSageModels.Add(humanMovementSageModel);

                });
            }
            return humanMovementSageModels;
        }

        [HttpGet]
        [Route("GetManPowerBySage")]
        public List<ManPowerModel> GetManPowerBySage()
        {
            List<ManPowerModel> manPowerModels = new List<ManPowerModel>();
            var employeeSageItems = _context.EmployeeSageInformation.Where(s => s.AttendenceDate.Value.Date == DateTime.Now.Date);
            List<long?> ids = employeeSageItems.Select(s => s.EmployeeId).ToList();
            var employees = _context.Employee.Where(e => ids.Contains(e.EmployeeId)).ToList();
            employees.ForEach(e =>
            {
                manPowerModels.Add(new ManPowerModel
                {
                    EmployeeID = e.EmployeeId,
                    Name = e.FirstName,
                });
            });
            return manPowerModels;
        }
    }
}