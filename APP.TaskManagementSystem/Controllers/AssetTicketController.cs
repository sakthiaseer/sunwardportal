﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AssetTicketController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AssetTicketController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [Route("GetAssetTicket")]
        public List<AssetTicketModel> Get()
        {
            List<AssetTicketModel> AssetTicketModels = new List<AssetTicketModel>();
            var AssetTicket = _context.AssetTicket
                 .Include(a => a.AddedByUser)
                .Include(b => b.ModifiedByUser)
                .Include(c => c.StatusCode)
                .Include(a => a.Asset)
                .Include(d => d.PriorityStatusNavigation)
                .Include(e => e.UrgencyStatusNavigation)
                .Include(f => f.ImpactStatusNavigation)
                .OrderByDescending(o => o.AssetTicketId).AsNoTracking().ToList();
            if (AssetTicket != null && AssetTicket.Count > 0)
            {
                AssetTicket.ForEach(s =>
                {
                    AssetTicketModel AssetTicketModel = new AssetTicketModel
                    {
                        AssetTicketId = s.AssetTicketId,
                        AssetId = s.AssetId,
                        AssetName = s.Asset?.Name,
                        UrgencyStatus = s.UrgencyStatus,
                        ImpactStatus = s.ImpactStatus,
                        PriorityStatus = s.PriorityStatus,
                        Description = s.Description,
                        UrgencyStatusName = s.UrgencyStatusNavigation?.CodeValue,
                        ImpactStatusName = s.ImpactStatusNavigation?.CodeValue,
                        PriorityStatusName = s.PriorityStatusNavigation?.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",

                    };
                    AssetTicketModels.Add(AssetTicketModel);
                });
            }
            return AssetTicketModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AssetTicketModel> GetData(SearchModel searchModel)
        {
            var AssetTicket = new AssetTicket();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).FirstOrDefault();
                        break;
                    case "Last":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).LastOrDefault();
                        break;
                    case "Next":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).LastOrDefault();
                        break;
                    case "Previous":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).FirstOrDefault();
                        break;
                    case "Last":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).LastOrDefault();
                        break;
                    case "Next":
                        AssetTicket = _context.AssetTicket.OrderBy(o => o.AssetTicketId).FirstOrDefault(s => s.AssetTicketId > searchModel.Id);
                        break;
                    case "Previous":
                        AssetTicket = _context.AssetTicket.OrderByDescending(o => o.AssetTicketId).FirstOrDefault(s => s.AssetTicketId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AssetTicketModel>(AssetTicket);
            return result;
        }
        [HttpPost]
        [Route("InsertAssetTicket")]
        public AssetTicketModel Post(AssetTicketModel value)
        {
            var AssetTicket = new AssetTicket
            {
                AssetId = value.AssetId,
                UrgencyStatus = value.UrgencyStatus,
                ImpactStatus = value.ImpactStatus,
                PriorityStatus = value.PriorityStatus,
                Description = value.Description,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
            };
            _context.AssetTicket.Add(AssetTicket);
            _context.SaveChanges();
            value.AssetTicketId = AssetTicket.AssetTicketId;
            return value;
        }
        [HttpPut]
        [Route("UpdateAssetTicket")]
        public AssetTicketModel Put(AssetTicketModel value)
        {
            var AssetTicket = _context.AssetTicket.SingleOrDefault(p => p.AssetTicketId == value.AssetTicketId);
            AssetTicket.AssetId = value.AssetId;
            AssetTicket.UrgencyStatus = value.UrgencyStatus;
            AssetTicket.ImpactStatus = value.ImpactStatus;
            AssetTicket.PriorityStatus = value.PriorityStatus;
            AssetTicket.Description = value.Description;
            AssetTicket.StatusCodeId = value.StatusCodeID.Value;
            AssetTicket.ModifiedByUserId = value.ModifiedByUserID;
            AssetTicket.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteAssetTicket")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var AssetTicket = _context.AssetTicket.Where(p => p.AssetTicketId == id).FirstOrDefault();
                if (AssetTicket != null)
                {
                    _context.AssetTicket.Remove(AssetTicket);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
