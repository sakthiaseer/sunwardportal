﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavisionCompanyController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public NavisionCompanyController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [HttpPost]
        [Route("GetNavisionCompanyByRefNo")]
        public List<NavisionCompanyModel> GetNavisionCompanyByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavisionCompanyModel> navisionCompanys = new List<NavisionCompanyModel>();
            List<NavisionCompany> navisionCompany = new List<NavisionCompany>();
            if (refSearchModel.IsHeader)
            {
                navisionCompany = _context.NavisionCompany.Include(c => c.NavCustomer).Include(d=>d.Database).Include(v => v.NavVendor).Include(s => s.StatusCode).Include("AddedByUser").Include("ModifiedByUser").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.NavisionCompanyId).OrderByDescending(l => l.NavisionCompanyId).AsNoTracking().ToList();
            }
            else
            {
                navisionCompany = _context.NavisionCompany.Include(c => c.NavCustomer).Include(d => d.Database).Include(v => v.NavVendor).Include(s => s.StatusCode).Include("AddedByUser").Include("ModifiedByUser").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.NavisionCompanyId).OrderByDescending(l => l.NavisionCompanyId).AsNoTracking().ToList();

            }
            if (navisionCompany != null && navisionCompany.Count > 0)
            {
                navisionCompany.ForEach(s =>
                {
                    NavisionCompanyModel navisionCompanyModel = new NavisionCompanyModel
                    {
                        NavisionCompanyId = s.NavisionCompanyId,
                        DatabaseId = s.DatabaseId,
                        CompanyTypeId = s.CompanyTypeId,
                        NavCustomerId = s.NavCustomerId,
                        NavVendorId = s.NavVendorId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        NavCustomerName = s.NavCustomer != null ? s.NavCustomer.Name : null,
                        NavVendorName = s.NavVendor != null ? s.NavVendor.VendorName : null,
                        DatabaseName =s.Database!=null?s.Database.Description:null, 
                        CompanyTypeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CompanyTypeId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,

                    };
                    navisionCompanys.Add(navisionCompanyModel);

                });
            }
            return navisionCompanys;
        }
        [HttpPost]
        [Route("InsertNavisionCompany")]
        public NavisionCompanyModel Post(NavisionCompanyModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "NavisionCompany" });
            var navisionCompany = new NavisionCompany
            {
                DatabaseId = value.DatabaseId,
                CompanyTypeId = value.CompanyTypeId,
                NavCustomerId = value.NavCustomerId,
                NavVendorId = value.NavVendorId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileLinkReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.NavisionCompany.Add(navisionCompany);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = navisionCompany.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = navisionCompany.ProfileLinkReferenceNo;
            value.NavisionCompanyId = navisionCompany.NavisionCompanyId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavisionCompany")]
        public NavisionCompanyModel Put(NavisionCompanyModel value)
        {
            var navisionCompany = _context.NavisionCompany.SingleOrDefault(p => p.NavisionCompanyId == value.NavisionCompanyId);
            navisionCompany.DatabaseId = value.DatabaseId;
            navisionCompany.CompanyTypeId = value.CompanyTypeId;
            navisionCompany.NavCustomerId = value.NavCustomerId;
            navisionCompany.NavVendorId = value.NavVendorId;
            navisionCompany.ModifiedByUserId = value.ModifiedByUserID;
            navisionCompany.ModifiedDate = DateTime.Now;
            navisionCompany.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNavisionCompany")]
        public void Delete(int id)
        {
            var navisionCompany = _context.NavisionCompany.SingleOrDefault(p => p.NavisionCompanyId == id);
            if (navisionCompany != null)
            {
                _context.NavisionCompany.Remove(navisionCompany);
                _context.SaveChanges();
            }
        }
        // PUT: api/User/5
        [HttpPost]
        [Route("NavisionCompanyReport")]
        public List<NavisionCompanyModel> NavisionCompanyReport(NavisionCompanyModel searchModel)
        {
            var companyListing = _context.CompanyListing.AsNoTracking().ToList();

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavisionCompanyModel> navisionCompanys = new List<NavisionCompanyModel>();
            var navisionCompany = _context.NavisionCompany.Include(c => c.NavCustomer).Include(d => d.Database).Include(v=>v.NavVendor).Where(w=>w.NavisionCompanyId>0);
            if(searchModel.DatabaseId>0)
            {
                navisionCompany = navisionCompany.Where(w=>w.DatabaseId==searchModel.DatabaseId);
            }
            if (searchModel.CompanyTypeId > 0)
            {
                navisionCompany = navisionCompany.Where(w => w.CompanyTypeId == searchModel.CompanyTypeId);
            }
            var navisionCompanyItems = navisionCompany.ToList();
            if (navisionCompanyItems != null && navisionCompanyItems.Count > 0)
            {
                navisionCompanyItems.ForEach(s =>
                {
                    NavisionCompanyModel navisionCompanyModel = new NavisionCompanyModel
                    {
                        NavisionCompanyId = s.NavisionCompanyId,
                        NavCustomerCode = s.NavCustomer != null ? s.NavCustomer.Code : null,
                        NavCustomerName = s.NavCustomer != null ? s.NavCustomer.Name : null,
                        NavVendorName = s.NavVendor != null ? s.NavVendor.VendorName : null,
                        NavVendorCode = s.NavVendor != null ? s.NavVendor.VendorNo : null,
                        DatabaseName = s.Database != null ? s.Database.Description : null,
                        CompanyTypeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CompanyTypeId).Select(a => a.Value).SingleOrDefault() : "",
                        CompanyId = companyListing.Count > 0 ? (companyListing.Where(w => w.ProfileReferenceNo == s.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault()) : 0,
                        CompanyName = companyListing.Count > 0 ? (companyListing.Where(w => w.ProfileReferenceNo == s.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault()) : "",
                    };
                    navisionCompanys.Add(navisionCompanyModel);

                });
                navisionCompanys.ForEach(h =>
                {
                    if(h.CompanyTypeName=="Vendor")
                    {
                        h.NavCustomerCode = h.NavVendorCode;
                        h.NavCustomerName = h.NavVendorName;
                    }
                });
            }
            if(searchModel.CompanyId>0)
            {
                navisionCompanys = navisionCompanys.Where(w=>w.CompanyId==searchModel.CompanyId).ToList();
            }
            return navisionCompanys.ToList();
        }
    }
}