﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ACEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ACEntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetACEntries")]
        public List<ACEntryModel> Get()
        {
            List<ACEntryModel> aCEntryModels = new List<ACEntryModel>();
            var Acentry = _context.Acentry.Include("AddedByUser").Include("Company").Include("ModifiedByUser").Include(c => c.Customer).Include(d => d.Document).OrderByDescending(o => o.AcentryId).AsNoTracking().ToList();
            Acentry.ForEach(s =>
            {
                ACEntryModel aCEntryModel = new ACEntryModel
                {
                    ACEntryId = s.AcentryId,
                    FromDate = s.FromDate,
                    ToDate = s.ToDate,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.Name,
                    DocumentId = s.DocumentId,
                    CompanyId = s.CompanyId,
                    Version = s.Version,
                    Remark = s.Remark,
                    FileName = s.Document?.FileName,
                    SessionId = s.SessionId,

                    //Quantity = s.AcentryLines.Where(al => al.AcentryId == s.AcentryId).Select(al => al.Quantity).SingleOrDefault(),
                    //StatusCode=s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    CompanyName = s.Company?.PlantCode,
                };
                aCEntryModels.Add(aCEntryModel);
            });


            return aCEntryModels;
        }

        [HttpGet]
        [Route("GetACEntryLinesByID")]
        public List<ACEntryLinesModel> GetACEntryLinesByID(int id)
        {
            var customerId = _context.Acentry.FirstOrDefault(f => f.AcentryId == id).CustomerId;

            var dist = "";
            if (customerId == 21)
            {
                dist = "Apex";
            }
            else if (customerId == 10)
            {
                dist = "PSB PX";
            }
            else if (customerId == 42)
            {
                dist = "MSS";
            }
            else if (customerId == 60)
            {
                dist = "SG Tender";
            }
            else
            {
                dist = "Antah";
            }
            List<ACEntryLinesModel> aCEntryLinesModels = new List<ACEntryLinesModel>();
            //Include("Navitems.NavItemCitemList.Acitems")
            var ACEntryLines = _context.AcentryLines
                                 .Include(i => i.Item)
                                .Include("Item.NavItemCitemList.NavItemCustomerItem")
                                 .Include("Item.GenericCode")
                                  .Include("Item.CompanyNavigation")
                                .OrderByDescending(o => o.AcentryLineId)
                                .Where(t => t.AcentryId == id)
                                .AsNoTracking().ToList();
            ACEntryLines.ForEach(s =>
            {
                ACEntryLinesModel aCEntryLinesModel = new ACEntryLinesModel
                {
                    ACEntryLineId = s.AcentryLineId,
                    ACEntryId = s.AcentryId,
                    ItemId = s.ItemId,
                    ItemName = s.Item?.No,
                    Quantity = s.Quantity,
                    Description = s.Item?.Description,
                    Description2 = s.Item?.Description2,
                    BUOM = s.Item?.BaseUnitofMeasure,
                    ItemCategory = s.Item?.ItemCategoryCode,
                    PackSize = s.Item?.PackSize,
                    Packuom = s.Item?.PackUom,
                    DistName = s.Item?.NavItemCitemList.Count > 0 ? s.Item?.NavItemCitemList.FirstOrDefault()?.NavItemCustomerItem?.DistName : "",
                    AcItemNo = s.Item?.NavItemCitemList.Count > 0 ? s.Item?.NavItemCitemList.FirstOrDefault()?.NavItemCustomerItem?.ItemNo : "",
                    VendorNo = s.Item?.VendorNo,
                    GenericCode = s.Item?.GenericCode != null ? s.Item?.GenericCode.Code : "",
                    CompanyName = s.Item?.CompanyNavigation?.PlantCode,
                };
                aCEntryLinesModels.Add(aCEntryLinesModel);
            });

            var itemsIds = aCEntryLinesModels.Select(s => s.ItemId).ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            var cusItemList = _context.NavItemCitemList.Include("NavItemCustomerItem").Where(f => itemsIds.Contains(f.NavItemId) && f.NavItemCustomerItem.CustomerId == customerId).ToList();
            aCEntryLinesModels.ForEach(a =>
            {
                var acitem = cusItemList.Where(f => f.NavItemId == a.ItemId).ToList();
                a.AcItemNo = string.Join(",", acitem.Select(s => s.NavItemCustomerItem?.ItemNo).ToList());
                //acitem.ForEach(f =>
                //{
                //    if (f.NavItemCustomerItem.DistName == dist)
                //    {
                //        a.AcItemNo = f.NavItemCustomerItem.ItemNo;
                //    }
                //});
            });

            return aCEntryLinesModels;
        }

        [HttpGet]
        [Route("GetACEntryLines")]
        public List<ACEntryLinesModel> GetACEntryLines()
        {
            var ACEntryLines = _context.AcentryLines.Include(i => i.Item).Select(s => new ACEntryLinesModel
            {
                ACEntryLineId = s.AcentryLineId,
                ACEntryId = s.AcentryId,
                ItemId = s.ItemId,
                ItemName = s.Item != null ? s.Item.No : "",
                Quantity = s.Quantity

            }).OrderByDescending(o => o.ACEntryLineId).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            return ACEntryLines;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ACEntryModel> GetData(SearchModel searchModel)
        {
            var Acentry = new Acentry();
            List<Acentry> acentrylist = new List<Acentry>();
            ACEntryModel acentryModel = new ACEntryModel();
            //ACEntryLinesModel entryLinesModel = new ACEntryLinesModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).FirstOrDefault();
                        break;
                    case "Last":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).LastOrDefault();
                        break;
                    case "Next":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).LastOrDefault();
                        break;
                    case "Previous":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).FirstOrDefault();
                        break;
                    case "Last":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).LastOrDefault();
                        break;
                    case "Next":
                        Acentry = _context.Acentry.OrderBy(o => o.AcentryId).FirstOrDefault(s => s.AcentryId > searchModel.Id);
                        break;
                    case "Previous":
                        Acentry = _context.Acentry.OrderByDescending(o => o.AcentryId).FirstOrDefault(s => s.AcentryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ACEntryModel>(Acentry);
            if (result != null)
            {

                acentryModel.Acentrylines = _context.AcentryLines.Select(s =>
                     new ACEntryLinesModel
                     {

                         ACEntryId = s.AcentryId,
                         ACEntryLineId = s.AcentryLineId,
                         Quantity = s.Quantity,
                         ItemId = s.ItemId,
                         ItemName = s.Item.No,

                     }).OrderByDescending(o => o.ACEntryLineId).Where(t => t.ACEntryId == result.ACEntryId).ToList();

                if (acentryModel.Acentrylines.Count > 0)
                {
                    result.Acentrylines = acentryModel.Acentrylines;
                }
                if (result.DocumentId > 0)
                {
                    result.FileName = _context.Documents.FirstOrDefault(t => t.DocumentId == result.DocumentId)?.FileName;
                }
            }
            return result;
        }
        [HttpPost]
        [Route("CopyACEntry")]
        public ACEntryModel CopyACEntry(ACEntryModel value)
        {
            var acEntry = _context.Acentry.Include("AcentryLines").FirstOrDefault(f => f.AcentryId == value.ACEntryId);

            if (acEntry != null)
            {
                var acItem = new Acentry
                {
                    FromDate = acEntry.ToDate.Value.AddDays(1),
                    ToDate = acEntry.ToDate.Value.AddMonths(1),
                    CustomerId = acEntry.CustomerId,
                    DocumentId = acEntry.DocumentId,
                    Version = acEntry.Version,
                    Remark = acEntry.Remark,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                    CompanyId = acEntry.CompanyId,
                    SessionId = acEntry.SessionId,

                };
                acItem.AcentryLines = new List<AcentryLines>();
                acEntry.AcentryLines.ToList().ForEach(f =>
                {
                    var acEntryLine = new AcentryLines
                    {
                        ItemId = f.ItemId,
                        Quantity = f.Quantity,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 1,
                        AddedByUserId = value.AddedByUserID,

                    };
                    acItem.AcentryLines.Add(acEntryLine);
                });
                _context.Acentry.Add(acItem);
                _context.SaveChanges();
            }
            return value;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertACEntry")]
        public ACEntryModel Post(ACEntryModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionID == null)
            {
                var acEntry = new Acentry
                {
                    FromDate = value.FromDate,
                    ToDate = value.ToDate,
                    CustomerId = value.CustomerId,
                    DocumentId = value.DocumentId,
                    Version = value.Version,
                    Remark = value.Remark,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    CompanyId = value.CompanyId,
                    SessionId = SessionId,
                };
                _context.Acentry.Add(acEntry);
                _context.SaveChanges();
                value.ACEntryId = acEntry.AcentryId;
                value.SessionId = acEntry.SessionId;
            }
            else
            {
                var document = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).ToList();
                if (document != null && document.Count > 0)
                {
                    document.ForEach(doc =>
                    {
                        var acEntry = new Acentry
                        {
                            FromDate = value.FromDate,
                            ToDate = value.ToDate,
                            CustomerId = value.CustomerId,
                            CompanyId = value.CompanyId,
                            DocumentId = doc.DocumentId,
                            Version = value.Version,
                            Remark = value.Remark,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,

                        };
                        _context.Acentry.Add(acEntry);
                        _context.SaveChanges();
                        value.ACEntryId = acEntry.AcentryId;
                    });

                }

            }


            var aCEntryLinesModels = value.Acentrylines;
            //if (aCEntryLinesModels.Count != 0)
            //{
            //    aCEntryLinesModels.ForEach(aCEntryLinesModel =>
            //    {
            //        var acentryLines = new AcentryLines()
            //        {
            //            AcentryId = value.ACEntryId,
            //            ItemId = aCEntryLinesModel.ItemId,
            //            Quantity = aCEntryLinesModel.Quantity
            //        };
            //        _context.AcentryLines.Add(acentryLines);
            //    });
            //}
            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateACEntry")]
        public ACEntryModel Put(ACEntryModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionId == null)
            {
                value.SessionId = SessionId;
            }
            if (value.SessionID == null)
            {

                var acEntry = _context.Acentry.SingleOrDefault(p => p.AcentryId == value.ACEntryId);

                acEntry.ModifiedDate = DateTime.Now;
                acEntry.FromDate = value.FromDate;
                acEntry.ToDate = value.ToDate;
                acEntry.CustomerId = value.CustomerId;
                acEntry.DocumentId = value.DocumentId;
                acEntry.CompanyId = value.CompanyId;
                acEntry.Version = value.Version;
                acEntry.Remark = value.Remark;
                //acEntry.Quantity = value.Quantity;
                acEntry.ModifiedByUserId = value.ModifiedByUserID;
                acEntry.StatusCodeId = value.StatusCodeID.Value;
                acEntry.SessionId = value.SessionId;
            }
            else
            {
                var document = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).ToList();
                if (document != null && document.Count > 0)
                {
                    document.ForEach(doc =>
                    {
                        var acEntry = _context.Acentry.SingleOrDefault(p => p.AcentryId == value.ACEntryId);
                        //Acentry.AcentryId = value.ACEntryID;
                        acEntry.ModifiedByUserId = value.ModifiedByUserID;
                        acEntry.ModifiedDate = DateTime.Now;
                        acEntry.FromDate = value.FromDate;
                        acEntry.ToDate = value.ToDate;
                        acEntry.CustomerId = value.CustomerId;
                        acEntry.DocumentId = doc.DocumentId;
                        acEntry.CompanyId = value.CompanyId;
                        acEntry.Version = value.Version;
                        acEntry.Remark = value.Remark;
                        //acEntry.Quantity = value.Quantity;                       
                        acEntry.StatusCodeId = value.StatusCodeID.Value;
                    });
                }
            }
            var aCEntryLines = value.Acentrylines;
            //if (aCEntryLines.Count > 0)
            //{
            //    aCEntryLines.ForEach(aEL =>
            //    {
            //        if (aEL.ACEntryLineId > 0)
            //        {
            //            var acEntryLine = _context.AcentryLines.SingleOrDefault(p => p.AcentryLineId == aEL.ACEntryLineId);
            //            {
            //                if (acEntryLine != null)
            //                {
            //                    acEntryLine.ItemId = aEL.ItemId;
            //                    acEntryLine.Quantity = aEL.Quantity;
            //                }
            //            };
            //        }
            //        else
            //        {
            //            var acentryLine = new AcentryLines()
            //            {
            //                AcentryId = value.ACEntryId,
            //                ItemId = aEL.ItemId,
            //                Quantity = aEL.Quantity

            //            };
            //            _context.AcentryLines.Add(acentryLine);

            //        }
            //    });
            //}
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteACEntry")]
        public void Delete(int id)
        {
            var acEntry = _context.Acentry.SingleOrDefault(p => p.AcentryId == id);
            if (acEntry != null)
            {
                var acentryLines = _context.AcentryLines.Where(ac => ac.AcentryId == acEntry.AcentryId).ToList();
                if (acentryLines != null)
                {
                    _context.AcentryLines.RemoveRange(acentryLines);
                    _context.SaveChanges();
                }
                _context.Acentry.Remove(acEntry);
                _context.SaveChanges();
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteACEntryLine")]
        public void DeleteACEntryLine(int id)
        {
            var acEntry = _context.AcentryLines.SingleOrDefault(p => p.AcentryLineId == id);
            if (acEntry != null)
            {
                _context.AcentryLines.Remove(acEntry);
                _context.SaveChanges();

            }
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertACEntryLine")]
        public ACEntryLinesModel PostACEntryLine(ACEntryLinesModel value)
        {

            var acEntryLine = new AcentryLines
            {
                AcentryId = value.ACEntryId,
                ItemId = value.ItemId,
                Quantity = value.Quantity,
                AddedDate = DateTime.Now,
                StatusCodeId = 1,
                AddedByUserId = value.AddedByUserID,

            };
            _context.AcentryLines.Add(acEntryLine);
            _context.SaveChanges();
            value.ACEntryLineId = acEntryLine.AcentryLineId;

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateACEntryLine")]
        public ACEntryLinesModel PutACEntryLine(ACEntryLinesModel value)
        {
            var acEntryLine = _context.AcentryLines.SingleOrDefault(p => p.AcentryLineId == value.ACEntryLineId);

            acEntryLine.AcentryId = value.ACEntryId;
            acEntryLine.ItemId = value.ItemId;
            acEntryLine.Quantity = value.Quantity;
            acEntryLine.ModifiedByUserId = value.ModifiedByUserID;
            acEntryLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }




    }
}