﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionCapsuleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionCapsuleController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionCapsules")]
        public List<ProductionCapsuleModel> Get()
        {
            List<ProductionCapsuleModel> productionCapsuleModels = new List<ProductionCapsuleModel>();
            var productionCapsule = _context.ProductionCapsule.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("ModifiedByUser").OrderByDescending(o => o.ProductionCapsuleId).AsNoTracking().ToList();
            if (productionCapsule != null && productionCapsule.Count > 0)
            {
                List<long?> masterIds = productionCapsule.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList();
                masterIds.AddRange(productionCapsule.Where(w => w.ItemNoId != null).Select(a => a.ItemNoId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.SizeId != null).Select(a => a.SizeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionCapsule.ForEach(s =>
                {
                    ProductionCapsuleModel productionCapsuleModel = new ProductionCapsuleModel
                    {
                        ProductionCapsuleID = s.ProductionCapsuleId,
                        ColorID = s.ColorId,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        ItemNoID = s.ItemNoId,
                        ItemNo = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNoId).Select(a => a.Value).SingleOrDefault() : "",
                        SizeID = s.SizeId,
                        Size = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SizeId).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec != null ? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",


                    };
                    productionCapsuleModels.Add(productionCapsuleModel);
                });
            }


            return productionCapsuleModels;
        }

        [HttpPost]
        [Route("GetProductionCapsulesByRefNo")]
        public List<ProductionCapsuleModel> GetProductionCapsulesByRefNo(RefSearchModel refSearchModel)
        {
            List<ProductionCapsuleModel> productionCapsuleModels = new List<ProductionCapsuleModel>();
            List<ProductionCapsule> productionCapsule = new List<ProductionCapsule>();

            if (refSearchModel.IsHeader)
            {
                productionCapsule = _context.ProductionCapsule.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionCapsuleId).AsNoTracking().ToList();
            }
            else
            {
                productionCapsule = _context.ProductionCapsule.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionCapsuleId).AsNoTracking().ToList();
            }

            if (productionCapsule != null && productionCapsule.Count > 0)
            {
                List<long?> masterIds = productionCapsule.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList();
                masterIds.AddRange(productionCapsule.Where(w => w.ItemNoId != null).Select(a => a.ItemNoId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.SizeId != null).Select(a => a.SizeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionCapsule.ForEach(s =>
                {
                    ProductionCapsuleModel productionCapsuleModel = new ProductionCapsuleModel
                    {
                        ProductionCapsuleID = s.ProductionCapsuleId,
                        ColorID = s.ColorId,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        ItemNoID = s.ItemNoId,
                        ItemNo = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNoId).Select(a => a.Value).SingleOrDefault() : "",
                        SizeID = s.SizeId,
                        Size = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SizeId).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        SpecNoID = s.ProductionCapsuleId,
                        SpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                        SpecNos = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()) + " " + "|" + " " + s.MaterialName,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec != null ? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",

                    };
                    productionCapsuleModels.Add(productionCapsuleModel);
                });

            }
            return productionCapsuleModels;
        }

        [HttpGet]
        [Route("GetActiveProductionCapsule")]
        public List<ProductionCapsuleModel> GetActiveProductionCapsule()
        {
            List<ProductionCapsuleModel> productionCapsuleModels = new List<ProductionCapsuleModel>();
            var productionCapsule = _context.ProductionCapsule.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").OrderByDescending(o => o.ProductionCapsuleId).AsNoTracking().ToList();
            if (productionCapsule != null && productionCapsule.Count > 0)
            {
                List<long?> masterIds = productionCapsule.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList();
                masterIds.AddRange(productionCapsule.Where(w => w.ItemNoId != null).Select(a => a.ItemNoId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.SizeId != null).Select(a => a.SizeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionCapsule.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionCapsule.ForEach(s =>
                {
                    ProductionCapsuleModel productionCapsuleModel = new ProductionCapsuleModel
                    {
                        ProductionCapsuleID = s.ProductionCapsuleId,
                        ColorID = s.ColorId,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        ItemNoID = s.ItemNoId,
                        ItemNo = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNoId).Select(a => a.Value).SingleOrDefault() : "",
                        SizeID = s.SizeId,
                        Size = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SizeId).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                    };
                    productionCapsuleModels.Add(productionCapsuleModel);
                });
            }
            return productionCapsuleModels;
        }



        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionCapsuleModel> GetData(SearchModel searchModel)
        {
            var productionCapsule = new ProductionCapsule();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).LastOrDefault();
                        break;
                    case "Next":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).LastOrDefault();
                        break;
                    case "Previous":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).LastOrDefault();
                        break;
                    case "Next":
                        productionCapsule = _context.ProductionCapsule.OrderBy(o => o.ProductionCapsuleId).FirstOrDefault(s => s.ProductionCapsuleId > searchModel.Id);
                        break;
                    case "Previous":
                        productionCapsule = _context.ProductionCapsule.OrderByDescending(o => o.ProductionCapsuleId).FirstOrDefault(s => s.ProductionCapsuleId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionCapsuleModel>(productionCapsule);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionCapsule")]
        public ProductionCapsuleModel Post(ProductionCapsuleModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MaterialName });
            var ProductionCapsuleCount = _context.ProductionCapsule.Where(p => p.LinkProfileReferenceNo == value.LinkProfileReferenceNo).Count();
            var productionCapsule = new ProductionCapsule
            {
                ColorId = value.ColorID,
                ItemNoId = value.ItemNoID,
                SizeId = value.SizeID,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                IsSameAsMaster = value.IsSameAsMaster,
                MaterialName = value.MaterialName,
                SpecNo = ProductionCapsuleCount + 1,
                PurposeId = value.PurposeId,
                StudyLink = value.StudyLink,
                ProcessUseSpecId = value.ProcessUseSpecId,
                RndpurposeId = value.RndpurposeId,
            };
            _context.ProductionCapsule.Add(productionCapsule);
            _context.SaveChanges();
            value.ProductionCapsuleID = productionCapsule.ProductionCapsuleId;
            value.SpecificationNo = "Spec" + " " + ((productionCapsule.SpecNo > 10) ? productionCapsule.SpecNo.ToString() : "0" + productionCapsule.SpecNo.ToString());
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionCapsule")]
        public ProductionCapsuleModel Put(ProductionCapsuleModel value)
        {
            var productionCapsule = _context.ProductionCapsule.SingleOrDefault(p => p.ProductionCapsuleId == value.ProductionCapsuleID);
            productionCapsule.ItemNoId = value.ItemNoID;
            productionCapsule.ColorId = value.ColorID;
            productionCapsule.SizeId = value.SizeID;
            productionCapsule.ModifiedByUserId = value.ModifiedByUserID;
            productionCapsule.ModifiedDate = DateTime.Now;
            productionCapsule.IsSameAsMaster = value.IsSameAsMaster;
            productionCapsule.MaterialName = value.MaterialName;
            productionCapsule.PurposeId = value.PurposeId;
            productionCapsule.StudyLink = value.StudyLink;
            productionCapsule.ProcessUseSpecId = value.ProcessUseSpecId;
            productionCapsule.RndpurposeId = value.RndpurposeId;
            productionCapsule.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            value.SpecificationNo = "Spec" + " " + ((productionCapsule.SpecNo > 10) ? productionCapsule.SpecNo.ToString() : "0" + productionCapsule.SpecNo.ToString());
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionCapsule")]
        public void Delete(int id)
        {
            try
            {
                var productionCapsule = _context.ProductionCapsule.SingleOrDefault(p => p.ProductionCapsuleId == id);
                if (productionCapsule != null)
                {
                    var ProductionCapsuleLine = _context.ProductionCapsuleLine.Where(m => m.ProductionCapsuleId == id).ToList();
                    if (ProductionCapsuleLine.Count() > 0)
                    {
                        _context.ProductionCapsuleLine.RemoveRange(ProductionCapsuleLine);
                        _context.SaveChanges();
                    }
                    _context.ProductionCapsule.Remove(productionCapsule);
                    _context.SaveChanges();
                    /*var ProductionCapsuleList = _context.ProductionCapsule.Where(p => p.LinkProfileReferenceNo == productionCapsule.LinkProfileReferenceNo).OrderBy(a => a.ProductionCapsuleId).AsNoTracking().ToList();
                    int inc = 1;
                    if (ProductionCapsuleList != null)
                    {
                        ProductionCapsuleList.ForEach(h =>
                        {
                            var ProductionCapsuleListItems = _context.ProductionCapsule.SingleOrDefault(p => p.ProductionCapsuleId == h.ProductionCapsuleId);
                            ProductionCapsuleListItems.SpecNo = inc;
                            _context.SaveChanges();
                            inc++;
                        });
                    }*/
                }
            }
            catch (Exception ex)
            {
                throw new AppException("You’re not allowed to delete this record", ex);
            }
        }
    }
}