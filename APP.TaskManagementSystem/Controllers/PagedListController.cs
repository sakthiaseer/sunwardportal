﻿using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PagedListController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PagedListController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpPost()]
        [Route("GetData")]
        public async Task<SWApiResponse> GetData(SearchModel searchModel)
        {
            try
            {
                Type pageList = typeof(PagedListBO);
                Type[] types = new Type[1];
                types[0] = typeof(CRT_TMSContext);
                ConstructorInfo magicConstructor = pageList.GetConstructor(types);
                object magicClassObject = magicConstructor.Invoke(new object[] { _context });

                MethodInfo magicMethod = pageList.GetMethod(searchModel.MethodName);
                var pagedvalue = (Task<(IEnumerable<dynamic>, int rowcount)>)magicMethod.Invoke(magicClassObject, new object[] { searchModel });

                var data = await pagedvalue;
                var page = new Pagination
                {
                    PageNumber = searchModel.PageCount,
                    PageSize = searchModel.PageSize,
                    TotalPages = data.rowcount,
                    TotalRecords = data.rowcount
                };

                var result = new SWApiResponse
                {
                    IsError = false,
                    Code = 200,
                    SentDate = DateTime.Now,
                    Result = data.Item1,
                    Message = "Success!",
                    Pagination = page
                };
                return result;
            }
            catch (Exception ex)
            {
                var result = new SWApiResponse
                {
                    IsError = true,
                    Code = 500,
                    SentDate = DateTime.Now,
                    Result = null,
                    Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message,
                    Pagination = new Pagination()
                };
                return result;
            }
        }
    }
}
