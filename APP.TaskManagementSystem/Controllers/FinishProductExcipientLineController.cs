﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FinishProductExcipientLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FinishProductExcipientLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetFinishProductExcipientLine")]
        public List<FinishProductExcipientLineModel> Get(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishproductexcipientline = _context.FinishProductExcipientLine.
                    Include(a => a.AddedByUser)
                    .Include(m => m.ModifiedByUser)
                    .Include(s => s.StatusCode)
                    .Include(r => r.Material)
                    .Include(g => g.GenericItemName)
                    .Include(f => f.GeneralRawMaterialName).AsNoTracking().ToList();
            List<FinishProductExcipientLineModel> finishProductExcipientLineModel = new List<FinishProductExcipientLineModel>();
            finishproductexcipientline.ForEach(s =>
            {
                FinishProductExcipientLineModel finishProductExcipientLineModels = new FinishProductExcipientLineModel
                {
                    FinishProductExcipientLineId = s.FinishProductExcipientLineId,
                    FinishProductExcipientId = s.FinishProductExcipientId,
                    MaterialId = s.MaterialId,
                    MaterialName = s.Material != null ? (s.Material.No + '|' + s.Material.Description) : "",
                    ProcessId = s.ProcessId,
                    Uomid = s.Uomid,
                    FunctionId = s.FunctionId,
                    Strength = s.Strength,
                    Source = s.Source,
                    ProcessName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProcessId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProcessId).Value : "",
                    UomName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uomid) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uomid).Value : "",
                    FunctionName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FunctionId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FunctionId).Value : "",
                    OperationProcessId = s.OperationProcessId,
                    GenericItemNameId = s.GenericItemNameId,
                    GeneralRawMaterialNameId = s.GeneralRawMaterialNameId,
                    NavisionDataBaseId = s.NavisionDataBaseId,
                    Dosage = s.Dosage,
                    DosageUnitsId = s.DosageUnitsId,
                    PerDosageId = s.PerDosageId,
                    Overage = s.Overage,
                    OverageUnitsId = s.OverageUnitsId,
                    DosageUnitsId1 = s.DosageUnitsId1,
                    OperationProcessName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OperationProcessId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OperationProcessId).Value : "",
                    GenericItemNameName = s.GenericItemName != null ? s.GenericItemName.GenericName : "",
                    GeneralRawMaterialName = s.GeneralRawMaterialName != null ? s.GeneralRawMaterialName.RawMaterialName : "",
                    NavisionDataBaseName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.NavisionDataBaseId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.NavisionDataBaseId).Value : "",
                    DosageUnitsName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DosageUnitsId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DosageUnitsId).Value : "",
                    PerDosageName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PerDosageId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PerDosageId).Value : "",
                    OverageUnitsName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OverageUnitsId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OverageUnitsId).Value : "",
                    DosageUnitsId1Name = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DosageUnitsId1) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DosageUnitsId1).Value : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                };
                finishProductExcipientLineModel.Add(finishProductExcipientLineModels);
            });
            return finishProductExcipientLineModel.Where(w => w.FinishProductExcipientId == id).OrderByDescending(a => a.FinishProductExcipientLineId).ToList();
        }
        [HttpPost]
        [Route("InsertFinishProductExcipientLine")]
        public FinishProductExcipientLineModel Post(FinishProductExcipientLineModel value)
        {
            var finishproductexcipientline = new FinishProductExcipientLine
            {
                FinishProductExcipientId = value.FinishProductExcipientId,
                ProcessId = value.ProcessId,
                MaterialId = value.MaterialId,
                Uomid = value.Uomid,
                FunctionId = value.FunctionId,
                Strength = value.Strength,
                Source = value.Source,
                OperationProcessId = value.OperationProcessId,
                GenericItemNameId = value.GenericItemNameId,
                GeneralRawMaterialNameId = value.GeneralRawMaterialNameId,
                NavisionDataBaseId = value.NavisionDataBaseId,
                Dosage = value.Dosage,
                DosageUnitsId = value.DosageUnitsId,
                PerDosageId = value.PerDosageId,
                Overage = value.Overage,
                OverageUnitsId = value.OverageUnitsId,
                DosageUnitsId1 = value.DosageUnitsId1,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.FinishProductExcipientLine.Add(finishproductexcipientline);
            _context.SaveChanges();
            value.FinishProductExcipientLineId = finishproductexcipientline.FinishProductExcipientLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateFinishProductExcipientLine")]
        public FinishProductExcipientLineModel Put(FinishProductExcipientLineModel value)
        {
            var finishproductexcipientline = _context.FinishProductExcipientLine.SingleOrDefault(p => p.FinishProductExcipientLineId == value.FinishProductExcipientLineId);
            finishproductexcipientline.FinishProductExcipientId = value.FinishProductExcipientId;
            finishproductexcipientline.ProcessId = value.ProcessId;
            finishproductexcipientline.MaterialId = value.MaterialId;
            finishproductexcipientline.Uomid = value.Uomid;
            finishproductexcipientline.FunctionId = value.FunctionId;
            finishproductexcipientline.Strength = value.Strength;
            finishproductexcipientline.Source = value.Source;
            finishproductexcipientline.ModifiedByUserId = value.ModifiedByUserID;
            finishproductexcipientline.ModifiedDate = DateTime.Now;
            finishproductexcipientline.StatusCodeId = value.StatusCodeID.Value;
            finishproductexcipientline.OperationProcessId = value.OperationProcessId;
            finishproductexcipientline.GenericItemNameId = value.GenericItemNameId;
            finishproductexcipientline.GeneralRawMaterialNameId = value.GeneralRawMaterialNameId;
            finishproductexcipientline.NavisionDataBaseId = value.NavisionDataBaseId;
            finishproductexcipientline.Dosage = value.Dosage;
            finishproductexcipientline.DosageUnitsId = value.DosageUnitsId;
            finishproductexcipientline.PerDosageId = value.PerDosageId;
            finishproductexcipientline.Overage = value.Overage;
            finishproductexcipientline.OverageUnitsId = value.OverageUnitsId;
            finishproductexcipientline.DosageUnitsId1 = value.DosageUnitsId1;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteFinishProductExcipientLine")]
        public void Delete(int id)
        {
            var finishproductexcipientline = _context.FinishProductExcipientLine.SingleOrDefault(p => p.FinishProductExcipientLineId == id);
            if (finishproductexcipientline != null)
            {
                _context.FinishProductExcipientLine.Remove(finishproductexcipientline);
                _context.SaveChanges();
            }
        }
    }
}