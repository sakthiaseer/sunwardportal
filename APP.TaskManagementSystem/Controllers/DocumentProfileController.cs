﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentProfileController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentProfileController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentProfiles")]
        public List<DocumentProfileNoSeriesModel> Get()
        {
            var documentProfiles = _context.DocumentProfileNoSeries.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DocumentProfileNoSeriesModel> documentProfileNoSeriesModel = new List<DocumentProfileNoSeriesModel>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileNoSeriesModel documentProfileNoSeriesModels = new DocumentProfileNoSeriesModel
                {
                    ProfileID = s.ProfileId,
                    Name = s.Name,
                    Description = s.Description,
                    Abbreviation = s.Abbreviation,
                    Abbreviation1 = s.Abbreviation1,
                    Abbreviation2 = s.Abbreviation2,
                    AbbreviationRequired = s.AbbreviationRequired,
                    SpecialWording = s.SpecialWording,
                    StartingNo = s.StartingNo,
                    StartWithYear = s.StartWithYear,
                    NoOfDigit = s.NoOfDigit,
                    IncrementalNo = s.IncrementalNo,
                    TranslationRequired = s.TranslationRequired,
                    LastCreatedDate = s.LastCreatedDate,
                    LastNoUsed = s.LastNoUsed,
                    Note = s.Note,
                    CategoryAbbreviation = s.CategoryAbbreviation,
                    CategoryId = s.CategoryId,
                    IsCategoryAbbreviation = s.IsCategoryAbbreviation,
                    CompanyId = s.CompanyId,
                    DepartmentId = s.DeparmentId,
                    GroupId = s.GroupId,
                    GroupAbbreviation = s.GroupAbbreviation,
                    IsGroupAbbreviation = s.IsGroupAbbreviation,
                    LinkId = s.LinkId,
                    ProfileTypeId = s.ProfileTypeId,
                    SeperatorToUse = s.SeperatorToUse,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SampleDocumentNo = s.SampleDocumentNo,
                    IsEnableTask = s.IsEnableTask,

                };
                documentProfileNoSeriesModel.Add(documentProfileNoSeriesModels);
            });
            documentProfileNoSeriesModel.OrderByDescending(a => a.ProfileID).ToList();
            documentProfileNoSeriesModel.ForEach(d =>
            {
                if (!string.IsNullOrEmpty(d.Abbreviation1))
                {
                    List<NumberSeriesCodeModel> numberSeriesCodeModels = JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(d.Abbreviation1);

                    numberSeriesCodeModels.OrderBy(s => s.Index).ToList().ForEach(n =>
                      {
                          d.EstablishNoSeriesCodeIDs.Add(n.Id);
                      });
                }
                if (d.EstablishNoSeriesCodeIDs.Count == 0)
                {
                    d.IsNoSerieswithOrganisationinfo = false;
                }
                else
                {
                    d.IsNoSerieswithOrganisationinfo = true;
                }
            });
            return documentProfileNoSeriesModel;
        }
        [HttpGet]
        [Route("GetDocumentProfilesByStatus")]
        public List<DocumentProfileNoSeriesModel> GetDocumentProfilesByStatus()
        {
            var documentProfiles = _context.DocumentProfileNoSeries.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(w=>w.StatusCodeId==664).AsNoTracking().ToList();
            List<DocumentProfileNoSeriesModel> documentProfileNoSeriesModel = new List<DocumentProfileNoSeriesModel>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileNoSeriesModel documentProfileNoSeriesModels = new DocumentProfileNoSeriesModel
                {
                    ProfileID = s.ProfileId,
                    Name = s.Name,
                    Description = s.Description,
                    Abbreviation = s.Abbreviation,
                    Abbreviation1 = s.Abbreviation1,
                    Abbreviation2 = s.Abbreviation2,
                    AbbreviationRequired = s.AbbreviationRequired,
                    SpecialWording = s.SpecialWording,
                    StartingNo = s.StartingNo,
                    StartWithYear = s.StartWithYear,
                    NoOfDigit = s.NoOfDigit,
                    IncrementalNo = s.IncrementalNo,
                    TranslationRequired = s.TranslationRequired,
                    LastCreatedDate = s.LastCreatedDate,
                    LastNoUsed = s.LastNoUsed,
                    Note = s.Note,
                    CategoryAbbreviation = s.CategoryAbbreviation,
                    CategoryId = s.CategoryId,
                    IsCategoryAbbreviation = s.IsCategoryAbbreviation,
                    CompanyId = s.CompanyId,
                    DepartmentId = s.DeparmentId,
                    GroupId = s.GroupId,
                    GroupAbbreviation = s.GroupAbbreviation,
                    IsGroupAbbreviation = s.IsGroupAbbreviation,
                    LinkId = s.LinkId,
                    ProfileTypeId = s.ProfileTypeId,
                    SeperatorToUse = s.SeperatorToUse,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SampleDocumentNo = s.SampleDocumentNo,
                    IsEnableTask = s.IsEnableTask,
                };
                documentProfileNoSeriesModel.Add(documentProfileNoSeriesModels);
            });
            documentProfileNoSeriesModel.OrderByDescending(a => a.ProfileID).ToList();
            return documentProfileNoSeriesModel;
        }

        [HttpGet]
        [Route("GetDocumentProfilesForCreateTask")]
        public List<DocumentProfileNoSeriesModel> GetDocumentProfilesForCreateTask()
        {
            var documentProfiles = _context.DocumentProfileNoSeries.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(w => w.IsEnableTask == true).AsNoTracking().ToList();
            List<DocumentProfileNoSeriesModel> documentProfileNoSeriesModel = new List<DocumentProfileNoSeriesModel>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileNoSeriesModel documentProfileNoSeriesModels = new DocumentProfileNoSeriesModel
                {
                    ProfileID = s.ProfileId,
                    Name = s.Name,
                    Description = s.Description,
                    Abbreviation = s.Abbreviation,
                    Abbreviation1 = s.Abbreviation1,
                    Abbreviation2 = s.Abbreviation2,
                    AbbreviationRequired = s.AbbreviationRequired,
                    SpecialWording = s.SpecialWording,
                    StartingNo = s.StartingNo,
                    StartWithYear = s.StartWithYear,
                    NoOfDigit = s.NoOfDigit,
                    IncrementalNo = s.IncrementalNo,
                    TranslationRequired = s.TranslationRequired,
                    LastCreatedDate = s.LastCreatedDate,
                    LastNoUsed = s.LastNoUsed,
                    Note = s.Note,
                    CategoryAbbreviation = s.CategoryAbbreviation,
                    CategoryId = s.CategoryId,
                    IsCategoryAbbreviation = s.IsCategoryAbbreviation,
                    CompanyId = s.CompanyId,
                    DepartmentId = s.DeparmentId,
                    GroupId = s.GroupId,
                    GroupAbbreviation = s.GroupAbbreviation,
                    IsGroupAbbreviation = s.IsGroupAbbreviation,
                    LinkId = s.LinkId,
                    ProfileTypeId = s.ProfileTypeId,
                    SeperatorToUse = s.SeperatorToUse,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SampleDocumentNo = s.SampleDocumentNo,
                    IsEnableTask = s.IsEnableTask,
                };
                documentProfileNoSeriesModel.Add(documentProfileNoSeriesModels);
            });
            documentProfileNoSeriesModel.OrderByDescending(a => a.ProfileID).ToList();
            return documentProfileNoSeriesModel;
        }
        [HttpGet]
        [Route("GetDocumentProfileItems")]
        public List<DocumentProfileItem> GetDocumentProfileItems()
        {
            var documentProfiles = _context.DocumentProfileNoSeries.AsNoTracking().ToList();
            List<DocumentProfileItem> documentProfileItem = new List<DocumentProfileItem>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileItem documentProfileItems = new DocumentProfileItem
                {
                    ProfileId = s.ProfileId,
                    ProfileName = s.Name,
                    Abbreviation1 = s.Abbreviation1,
                    StatusCodeID = s.StatusCodeId,
                };
                documentProfileItem.Add(documentProfileItems);
            });
            documentProfileItem.OrderByDescending(a => a.ProfileId).ToList();

            foreach (var documentProfile in documentProfileItem)
            {
                if (!string.IsNullOrEmpty(documentProfile.Abbreviation1))
                {
                    List<NumberSeriesCodeModel> numberSeriesCodeModels = JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(documentProfile.Abbreviation1).ToList();
                    numberSeriesCodeModels.ForEach(n =>
                    {
                        if (n.Name == "Company")
                        {
                            documentProfile.IsCompany = true;
                        }
                        else if (n.Name == "Department")
                        {
                            documentProfile.IsDepartment = true;
                        }
                        else if (n.Name == "Section")
                        {
                            documentProfile.IsSection = true;
                        }
                        else if (n.Name == "SubSection")
                        {
                            documentProfile.IsSubSection = true;
                        }
                    });
                }

            }
            return documentProfileItem;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentProfileNoSeriesModel> GetData(SearchModel searchModel)
        {
            var documentProfile = new DocumentProfileNoSeries();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).FirstOrDefault();
                        break;
                    case "Last":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).LastOrDefault();
                        break;
                    case "Next":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).LastOrDefault();
                        break;
                    case "Previous":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).FirstOrDefault();
                        break;
                    case "Last":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).LastOrDefault();
                        break;
                    case "Next":
                        documentProfile = _context.DocumentProfileNoSeries.OrderBy(o => o.ProfileId).FirstOrDefault(s => s.ProfileId > searchModel.Id);
                        break;
                    case "Previous":
                        documentProfile = _context.DocumentProfileNoSeries.OrderByDescending(o => o.ProfileId).FirstOrDefault(s => s.ProfileId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentProfileNoSeriesModel>(documentProfile);
            return result;
        }

       
            // POST: api/User
        [HttpPost]
        [Route("InsertDocumentProfile")]
        public DocumentProfileNoSeriesModel Post(DocumentProfileNoSeriesModel value)
        {
            var errorMessage = "";
            if (value.IsNoSerieswithOrganisationinfo == false)
            {
                value.Abbreviation1 = "";
            }
            var existingProfile = _context.DocumentProfileNoSeries.Where(d => d.Name.ToLower() == value.Name.ToLower() || d.Abbreviation.ToLower() == value.Abbreviation.ToLower()).FirstOrDefault();
            if (existingProfile == null)
            {
                var documentProfile = new DocumentProfileNoSeries
                {
                    //ProfileId = value.ProfileID,
                    Name = value.Name,
                    Description = value.Description,
                    Abbreviation = value.Abbreviation,
                    Abbreviation1 = value.Abbreviation1,
                    Abbreviation2 = value.Abbreviation2,
                    AbbreviationRequired = value.AbbreviationRequired,
                    SpecialWording = value.SpecialWording,
                    StartingNo = value.StartingNo,
                    StartWithYear = value.StartWithYear,
                    NoOfDigit = value.NoOfDigit,
                    IncrementalNo = value.IncrementalNo,
                    TranslationRequired = value.TranslationRequired,
                    LastCreatedDate = value.LastCreatedDate,
                    LastNoUsed = value.LastNoUsed,
                    Note = value.Note,
                    CategoryAbbreviation = value.CategoryAbbreviation,
                    CategoryId = value.CategoryId,
                    IsCategoryAbbreviation = value.IsCategoryAbbreviation,
                    CompanyId = value.CompanyId,
                    DeparmentId = value.DepartmentId,
                    GroupId = value.GroupId,
                    GroupAbbreviation = value.GroupAbbreviation,
                    IsGroupAbbreviation = value.IsGroupAbbreviation,
                    SeperatorToUse = value.SeperatorToUse,
                    LinkId = value.LinkId,
                    ProfileTypeId = value.ProfileTypeId,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    IsEnableTask = value.IsEnableTask,

                };
                _context.DocumentProfileNoSeries.Add(documentProfile);
                _context.SaveChanges();
                value.ProfileID = documentProfile.ProfileId;
              
            }

            else
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "This Profile Name or Profile Appreviation already exist!";
                }
                throw new AppException(errorMessage);

            }

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentProfile")]
        public DocumentProfileNoSeriesModel Put(DocumentProfileNoSeriesModel value)
        {
            var documentProfile = _context.DocumentProfileNoSeries.SingleOrDefault(p => p.ProfileId == value.ProfileID);
            List<NumberSeriesCodeModel> numberSeriesCodes = new List<NumberSeriesCodeModel>();
            numberSeriesCodes.Add(new NumberSeriesCodeModel { Id = 1, Name = "Company" });
            numberSeriesCodes.Add(new NumberSeriesCodeModel { Id = 2, Name = "Department" });
            numberSeriesCodes.Add(new NumberSeriesCodeModel { Id = 3, Name = "Section" });
            numberSeriesCodes.Add(new NumberSeriesCodeModel { Id = 4, Name = "SubSection" });
            List<NumberSeriesCodeModel> selectedNumberSeriesCodes = new List<NumberSeriesCodeModel>();
            if (value.IsNoSerieswithOrganisationinfo == true)
            {
                if (value.EstablishNoSeriesCodeIDs.Any())
                {
                    int i = 1;
                    value.EstablishNoSeriesCodeIDs.ForEach(c =>
                    {
                        var selectedNumberSeriesCode = numberSeriesCodes.FirstOrDefault(s => s.Id == c);
                        selectedNumberSeriesCode.Index = i++;
                        selectedNumberSeriesCodes.Add(selectedNumberSeriesCode);
                    });
                }


                //documentProfile.ProfileID = value.ProfileID;
                if (selectedNumberSeriesCodes.Any())
                {
                    documentProfile.Abbreviation1 = JsonConvert.SerializeObject(selectedNumberSeriesCodes).ToString();
                   
                }
            }
            if (value.IsNoSerieswithOrganisationinfo == false)
            {
                documentProfile.Abbreviation1 = "";
            }
            documentProfile.ProfileId = value.ProfileID;
            documentProfile.Name = value.Name;
            documentProfile.Description = value.Description;
            documentProfile.Abbreviation = value.Abbreviation;
            documentProfile.Abbreviation2 = value.Abbreviation2;
            documentProfile.AbbreviationRequired = value.AbbreviationRequired;
            documentProfile.SpecialWording = value.SpecialWording;
            documentProfile.StartingNo = value.StartingNo;
            documentProfile.StartWithYear = value.StartWithYear;
            documentProfile.NoOfDigit = value.NoOfDigit;
            documentProfile.IncrementalNo = value.IncrementalNo;
            documentProfile.TranslationRequired = value.TranslationRequired;
            documentProfile.LastCreatedDate = value.LastCreatedDate;
            documentProfile.LastNoUsed = value.LastNoUsed;
            documentProfile.Note = value.Note;
            documentProfile.CategoryAbbreviation = value.CategoryAbbreviation;
            documentProfile.CategoryId = value.CategoryId;
            documentProfile.IsCategoryAbbreviation = value.IsCategoryAbbreviation;
            documentProfile.CompanyId = value.CompanyId;
            documentProfile.DeparmentId = value.DepartmentId;
            documentProfile.GroupId = value.GroupId;
            documentProfile.GroupAbbreviation = value.GroupAbbreviation;
            documentProfile.IsGroupAbbreviation = value.IsGroupAbbreviation;
            documentProfile.SeperatorToUse = value.SeperatorToUse;
            documentProfile.LinkId = value.LinkId;
            documentProfile.ProfileTypeId = value.ProfileTypeId;
            documentProfile.StatusCodeId = value.StatusCodeID.Value;
            documentProfile.ModifiedByUserId = value.ModifiedByUserID;
            documentProfile.ModifiedDate = DateTime.Now;
            documentProfile.IsEnableTask = value.IsEnableTask;
            DocumentNoSeriesModel documentNoSeriesModel = new DocumentNoSeriesModel();
            documentNoSeriesModel.ProfileID = documentProfile.ProfileId;
            documentNoSeriesModel.PlantID = documentProfile.CompanyId;
            documentNoSeriesModel.DepartmentId = documentProfile.DeparmentId;
           
            documentProfile.SampleDocumentNo = GenerateSampleDocumentNo(documentNoSeriesModel);            
            value.SampleDocumentNo =  documentProfile.SampleDocumentNo;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentProfile")]
        public void Delete(int id)
        {
            try
            {
                var documentProfile = _context.DocumentProfileNoSeries.FirstOrDefault(p => p.ProfileId == id);
                if (documentProfile != null)
                {
                    _context.DocumentProfileNoSeries.Remove(documentProfile);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Profile already in use, cannot delete the profile", ex);

            }

        }
        public string GenerateSampleDocumentNo(DocumentNoSeriesModel documentNoSeriesModel)
        {
            bool isCompanyDepartmentExist = false;
            string documentNo = string.Empty;
            if (documentNoSeriesModel.ProfileID == null || documentNoSeriesModel.ProfileID <= 0)
            {
                return null;
            }
            else
            {
                var profileSettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == documentNoSeriesModel.ProfileID);
                
                List<string> numberSeriesCodes = new List<string> { "Company", "Department" };
                //List<string> numberSeriesSectionCodes = new List<string> { "Company", "Department", "Section" };
                //List<string> numberSeriesSubSectionCodes = new List<string> { "Company", "Department", "Section","SubSection" };
                List<NumberSeriesCodeModel> numberSeriesCodeModels = new List<NumberSeriesCodeModel>();

                List<Seperator> seperators = new List<Seperator>();
                seperators.Add(new Seperator { SeperatorSymbol = "/", SeperatorValue = 1 });
                seperators.Add(new Seperator { SeperatorSymbol = "-", SeperatorValue = 2 });

                var seperator = seperators.FirstOrDefault(s => s.SeperatorValue == profileSettings.SeperatorToUse.GetValueOrDefault(0));
                var seperatorSymbol = seperator != null ? seperator.SeperatorSymbol : "/";

                if (!String.IsNullOrEmpty(profileSettings.Abbreviation1))
                {
                    numberSeriesCodeModels = JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(profileSettings.Abbreviation1).ToList();
                    isCompanyDepartmentExist = numberSeriesCodeModels.Any(c => numberSeriesCodes.Contains(c.Name));
                    //isCompanyDepartmentSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSectionCodes.Contains(c.Name));
                    //isCompanyDepartmentSubSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSubSectionCodes.Contains(c.Name));
                    numberSeriesCodeModels.OrderBy(n => n.Index).ToList().ForEach(n =>
                    {
                        if (n.Name == "Company")
                        {
                            documentNo += "Company" + seperatorSymbol;
                        }
                        if (n.Name == "Department" )
                        {                           
                         documentNo += "Department" + seperatorSymbol;                            
                        }
                        if (n.Name == "Section")
                        {
                            documentNo += "Section" + seperatorSymbol;
                        }
                        if (n.Name == "SubSection" )
                        {
                            documentNo += "SubSection" + seperatorSymbol;
                        }
                        //documentNo += seperatorSymbol;
                    });
                }

                if (profileSettings.AbbreviationRequired.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.Abbreviation))
                {
                    documentNo += profileSettings.Abbreviation + seperatorSymbol;
                }

                if (profileSettings.IsGroupAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.GroupAbbreviation))
                {
                    documentNo += profileSettings.GroupAbbreviation + seperatorSymbol;
                }

                if (profileSettings.IsCategoryAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.CategoryAbbreviation))
                {
                    documentNo += profileSettings.CategoryAbbreviation + seperatorSymbol;
                }

                if (!String.IsNullOrEmpty(profileSettings.SpecialWording))
                {
                    documentNo += profileSettings.SpecialWording + seperatorSymbol;
                }
                if (profileSettings.StartWithYear.GetValueOrDefault(false))
                {
                    documentNo += DateTime.Now.Year.ToString().Substring(2, 2) + seperatorSymbol;
                }
                if (profileSettings.NoOfDigit.HasValue && profileSettings.NoOfDigit > 0)
                {
                    ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                    if (isCompanyDepartmentExist)
                    {
                        if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId == null)
                        {

                            var profileAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null).ToList();
                            if (profileAutoNumberlist != null && profileAutoNumberlist.Count > 0)
                            {
                                 profileAutoNumber = profileAutoNumberlist.LastOrDefault();
                            documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId == null)
                        {

                            var profileAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null).ToList();
                            if (profileAutoNumberlist != null && profileAutoNumberlist.Count > 0)
                            {
                              profileAutoNumber = profileAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId == null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == null).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();

                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                              profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId != null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                            documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId != null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId != null)
                        {

                            var profileAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileAutoNumberList != null && profileAutoNumberList.Count > 0)
                            {
                                profileAutoNumber = profileAutoNumberList.LastOrDefault();
                           documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }

                        }
                        else if (documentNoSeriesModel.PlantID != null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId == null)
                        {
                           
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == documentNoSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == null).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId == null)
                        {

                            var profileAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null).ToList();
                            if (profileAutoNumberlist != null && profileAutoNumberlist.Count > 0)
                            {
                                 profileAutoNumber = profileAutoNumberlist.LastOrDefault();
                               documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = new ProfileAutoNumber();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId == null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == null).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId != null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId != null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == documentNoSeriesModel.DepartmentId && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                               profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId != null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                  profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId != null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == documentNoSeriesModel.SubSectionId).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId != null && documentNoSeriesModel.SubSectionId == null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == documentNoSeriesModel.SectionId && p.SubSectionId == null).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                 profileAutoNumber = null;
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }

                        else if (documentNoSeriesModel.PlantID == null && documentNoSeriesModel.DepartmentId == null && documentNoSeriesModel.SectionId == null && documentNoSeriesModel.SubSectionId == null)
                        {
                            var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null).ToList();
                            if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                            {
                                ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            }
                            else
                            {
                                
                                documentNo = GenerateProfileAuto(documentNoSeriesModel, profileAutoNumber, profileSettings, documentNo);
                            }
                        }
                    }

                    else
                    {
                        ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                        if (String.IsNullOrEmpty(profileSettings.LastNoUsed))
                        {
                            documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.StartingNo) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                            //documentNo += profileSettings.LastNoUsed;
                        }
                        else
                        {
                            documentNo = GenerateProfileAuto(documentNoSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                            //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.LastNoUsed) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                            //documentNo += profileSettings.LastNoUsed;
                        }
                    }
                }
                    return documentNo;
            }
        }


        private string GenerateProfileAuto(DocumentNoSeriesModel noSeriesModel, ProfileAutoNumber profileAutonumber, DocumentProfileNoSeries profilesettings, string documentNo)
        {
            //string documentNo = string.Empty;
            // profilesettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID);
            //ProfileAutoNumber profileSubSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profilesettings.ProfileId && p.CompanyId == noSeriesModel.CompanyId && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
            string LastNoUsed = "";
            if (profileAutonumber == null)
            {
                
                LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);
                if(noSeriesModel.PlantID == 0)
                {
                    noSeriesModel.PlantID = null;
                }
                ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                {
                    ProfileId = profilesettings.ProfileId,
                    CompanyId = noSeriesModel.PlantID,
                    DepartmentId = noSeriesModel.DepartmentId,
                    SectionId = noSeriesModel.SectionId,
                    SubSectionId = noSeriesModel.SubSectionId,
                    LastNoUsed = LastNoUsed,
                    ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                };
                documentNo += LastNoUsed;
                _context.ProfileAutoNumber.Add(newProfileAutoNumber);
            }
            else if (profilesettings.StartWithYear.GetValueOrDefault(false) && profileAutonumber.ProfileYear.GetValueOrDefault(0) > 0 && profileAutonumber.ProfileYear.Value != DateTime.Now.Year)
            {
                var yearchecking = _context.ProfileAutoNumber.Where(p => p.ProfileId == profilesettings.ProfileId && p.ProfileYear > 0 && p.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                if (yearchecking != null)
                {
                    LastNoUsed = (Convert.ToInt32(yearchecking.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                 
                    documentNo += LastNoUsed;
                    if (noSeriesModel != null)
                    {
                        if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == noSeriesModel.PlantID && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                    }
                    else if (noSeriesModel == null)
                    {

                        var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                        if (existingprofileAutonumber != null)
                        {
                            existingprofileAutonumber.LastNoUsed = LastNoUsed;
                        }
                        _context.SaveChanges();
                    }
                }
                else
                {
                    LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                    ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                    {
                        ProfileId = profilesettings.ProfileId,
                        CompanyId = noSeriesModel.PlantID,
                        DepartmentId = noSeriesModel.DepartmentId,
                        SectionId = noSeriesModel.SectionId,
                        SubSectionId = noSeriesModel.SubSectionId,
                        LastNoUsed = LastNoUsed,
                        ScreenId = noSeriesModel.ScreenID,
                        ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,
                        ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                    };
                    documentNo += LastNoUsed;
                    _context.ProfileAutoNumber.Add(newProfileAutoNumber);
                    _context.SaveChanges();
                }
            }           
            else
            {
                LastNoUsed = (Convert.ToInt32(profileAutonumber.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);
                profileAutonumber.LastNoUsed = LastNoUsed;
                documentNo += LastNoUsed;
            }
            _context.SaveChanges();
            return documentNo;
        }
        [HttpPut]
        [Route("UpdateDocumentProfileStatus")]
        public DocumentProfileNoSeriesModel UpdateDocumentProfileStatus(DocumentProfileNoSeriesModel value)
        {
            var documentProfile = _context.DocumentProfileNoSeries.SingleOrDefault(p => p.ProfileId == value.ProfileID);
            if(documentProfile!=null)
            {
                documentProfile.StatusCodeId = 665;
                documentProfile.ModifiedByUserId = value.ModifiedByUserID;
                documentProfile.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }
          
            return value;
        }
            [HttpGet]
        [Route("GetDocumentProfileType")]
        public List<DocumentProfileNoSeriesModel> DocumentProfileType(int? id)
        {
            var documentProfiles = _context.DocumentProfileNoSeries.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).ToList();
            List<DocumentProfileNoSeriesModel> documentProfileNoSeriesModel = new List<DocumentProfileNoSeriesModel>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileNoSeriesModel documentProfileNoSeriesModels = new DocumentProfileNoSeriesModel
                {
                    ProfileID = s.ProfileId,
                    Name = s.Name,
                    Description = s.Description,
                    Abbreviation = s.Abbreviation,
                    Abbreviation1 = s.Abbreviation1,
                    Abbreviation2 = s.Abbreviation2,
                    AbbreviationRequired = s.AbbreviationRequired,
                    SpecialWording = s.SpecialWording,
                    StartingNo = s.StartingNo,
                    StartWithYear = s.StartWithYear,
                    NoOfDigit = s.NoOfDigit,
                    IncrementalNo = s.IncrementalNo,
                    TranslationRequired = s.TranslationRequired,
                    LastCreatedDate = s.LastCreatedDate,
                    LastNoUsed = s.LastNoUsed,
                    Note = s.Note,
                    CategoryAbbreviation = s.CategoryAbbreviation,
                    CategoryId = s.CategoryId,
                    IsCategoryAbbreviation = s.IsCategoryAbbreviation,
                    CompanyId = s.CompanyId,
                    DepartmentId = s.DeparmentId,
                    GroupId = s.GroupId,
                    GroupAbbreviation = s.GroupAbbreviation,
                    IsGroupAbbreviation = s.IsGroupAbbreviation,
                    LinkId = s.LinkId,
                    ProfileTypeId = s.ProfileTypeId,
                    SeperatorToUse = s.SeperatorToUse,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsEnableTask = s.IsEnableTask,
                };
                documentProfileNoSeriesModel.Add(documentProfileNoSeriesModels);
            });
            return documentProfileNoSeriesModel.Where(d => d.ProfileTypeId == id).OrderByDescending(a => a.ProfileID).ToList();

        }



    }
}