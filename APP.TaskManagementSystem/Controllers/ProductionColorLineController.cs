﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionColorLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionColorLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionColorLinesByID")]
        public List<ProductionColorLineModel> GetProductionColorLinesByID(long id)
        {
            List<ProductionColorLineModel> productionColorLineModels = new List<ProductionColorLineModel>();
            var productionColorLine = _context.ProductionColorLine.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.StatusCode).Where(l => l.ProductionColorId == id).OrderByDescending(o => o.ProductionColorLineId).AsNoTracking().ToList();
            if (productionColorLine != null && productionColorLine.Count > 0)
            {
                List<long?> masterIds = productionColorLine.Where(w => w.DataBaseRequireId != null).Select(a => a.DataBaseRequireId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                List<long?> navisionIds = productionColorLine.Where(w => w.NavisionId != null).Select(a => a.NavisionId).Distinct().ToList();
                var items = _context.Navitems.Where(w => navisionIds.Contains(w.ItemId)).AsNoTracking().ToList();
                productionColorLine.ForEach(s =>
                {
                    ProductionColorLineModel productionColorLineModel = new ProductionColorLineModel
                    {
                        ProductionColorLineID = s.ProductionColorLineId,
                        ProductionColorID = s.ProductionColorId,
                        DatabseRequireID = s.DataBaseRequireId,
                        NavisionID = s.NavisionId,
                        DatabseRequire = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DataBaseRequireId).Select(a => a.Value).SingleOrDefault() : "",
                        BUOM = s.Buom,
                        Navision = items.Where(t => t.ItemId == s.NavisionId).Select(t => t.No).FirstOrDefault(),
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionColorLineModels.Add(productionColorLineModel);
                });
            }

            return productionColorLineModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionColorLineModel> GetData(SearchModel searchModel)
        {
            var productionColorLine = new ProductionColorLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).LastOrDefault();
                        break;
                    case "Previous":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionColorLine = _context.ProductionColorLine.OrderBy(o => o.ProductionColorLineId).FirstOrDefault(s => s.ProductionColorLineId > searchModel.Id);
                        break;
                    case "Previous":
                        productionColorLine = _context.ProductionColorLine.OrderByDescending(o => o.ProductionColorLineId).FirstOrDefault(s => s.ProductionColorLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionColorLineModel>(productionColorLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionColorLine")]
        public ProductionColorLineModel Post(ProductionColorLineModel value)
        {
            var productionColorLine = new ProductionColorLine
            {
                ProductionColorId = value.ProductionColorID,
                DataBaseRequireId = value.DatabseRequireID,
                NavisionId = value.NavisionID,
                Buom = value.BUOM,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,




            };
            _context.ProductionColorLine.Add(productionColorLine);
            _context.SaveChanges();
            value.ProductionColorLineID = productionColorLine.ProductionColorLineId;
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionColorLine")]
        public ProductionColorLineModel Put(ProductionColorLineModel value)
        {
            var productionColorLine = _context.ProductionColorLine.SingleOrDefault(p => p.ProductionColorLineId == value.ProductionColorLineID);
            productionColorLine.ProductionColorId = value.ProductionColorID;
            productionColorLine.NavisionId = value.NavisionID;
            productionColorLine.DataBaseRequireId = value.DatabseRequireID;
            productionColorLine.Buom = value.BUOM;
            productionColorLine.ModifiedByUserId = value.ModifiedByUserID;
            productionColorLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionColorLine")]
        public void Delete(int id)
        {
            var productionColorLine = _context.ProductionColorLine.SingleOrDefault(p => p.ProductionColorLineId == id);
            if (productionColorLine != null)
            {
                _context.ProductionColorLine.Remove(productionColorLine);
                _context.SaveChanges();
            }
        }
    }
}