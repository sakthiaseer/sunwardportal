﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ICTCertificateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ICTCertificateController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetICTCertificate")]
        public List<ICTCertificateModel> Get()
        {
            var iCTCertificate = _context.Ictcertificate.Include("AddedByUser").Include("ModifiedByUser").AsNoTracking().Select(s => new ICTCertificateModel
            {
                ICTCertificateID = s.IctcertificateId,
                Name = s.Name,
                CertificateType = s.CertificateType.Value,
                SourceListID = s.SourceListId,
                VendorsListID = s.VendorsListId,
                //CertificateType = s.certificatety
                Link = s.Link,
                IsExpired = s.IsExpired.Value,
                ExpiryDate = s.ExpiryDate.Value,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,


            }).OrderByDescending(o => o.ICTCertificateID).ToList();
            //var result = _mapper.Map<List<ICTCertificateModel>>(ICTCertificate);
            return iCTCertificate;
        }
        [HttpGet]
        [Route("GetActiveICTCertificate")]
        public List<ICTCertificateModel> GetActiveICTCertificate()
        {
            var iCTCertificate = _context.Ictcertificate.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).AsNoTracking().Select(s => new ICTCertificateModel
            {
                ICTCertificateID = s.IctcertificateId,
                Name = s.Name,
                CertificateType = s.CertificateType.Value,
                //CertificateType = s.certificatety
                Link = s.Link,
                IsExpired = s.IsExpired.Value,
                SourceListID = s.SourceListId,
                VendorsListID = s.VendorsListId,
                ExpiryDate = s.ExpiryDate.Value,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,

            }).OrderByDescending(o => o.ICTCertificateID).ToList();
            //var result = _mapper.Map<List<ICTCertificateModel>>(ICTCertificate);
            return iCTCertificate;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<ICTCertificateModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var iCTCertificate = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id.Value);
            var result = _mapper.Map<ICTCertificateModel>(iCTCertificate);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ICTCertificateModel> GetData(SearchModel searchModel)
        {
            var iCTCertificate = new Ictcertificate();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).FirstOrDefault();
                        break;
                    case "Last":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).LastOrDefault();
                        break;
                    case "Next":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).LastOrDefault();
                        break;
                    case "Previous":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).FirstOrDefault();
                        break;
                    case "Last":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).LastOrDefault();
                        break;
                    case "Next":
                        iCTCertificate = _context.Ictcertificate.OrderBy(o => o.IctcertificateId).FirstOrDefault(s => s.IctcertificateId > searchModel.Id);
                        break;
                    case "Previous":
                        iCTCertificate = _context.Ictcertificate.OrderByDescending(o => o.IctcertificateId).FirstOrDefault(s => s.IctcertificateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ICTCertificateModel>(iCTCertificate);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertICTCertificate")]
        public ICTCertificateModel Post(ICTCertificateModel value)
        {
            var iCTCertificate = new Ictcertificate
            {


               CertificateType = value.CertificateType,
                Name = value.Name,
                Link = value.Link,
                IsExpired = value.IsExpired,
                IssueDate = value.IssueDate,
                ExpiryDate = value.ExpiryDate,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SourceListId = value.SourceListID.Value,
                VendorsListId = value.VendorsListID,

            };
            _context.Ictcertificate.Add(iCTCertificate);
            _context.SaveChanges();
            value.ICTCertificateID = iCTCertificate.IctcertificateId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateICTCertificate")]
        public ICTCertificateModel Put(ICTCertificateModel value)
        {
            var iCTCertificate = _context.Ictcertificate.SingleOrDefault(p => p.IctcertificateId == value.ICTCertificateID);
            iCTCertificate.Name = value.Name;
            iCTCertificate.CertificateType = value.CertificateType;
            iCTCertificate.Link = value.Link;
            iCTCertificate.IsExpired = value.IsExpired;
            iCTCertificate.ExpiryDate = value.ExpiryDate;
            iCTCertificate.ModifiedByUserId = value.ModifiedByUserID;
            iCTCertificate.ModifiedDate = DateTime.Now;
            iCTCertificate.StatusCodeId = value.StatusCodeID.Value;
            iCTCertificate.SourceListId = value.SourceListID.Value;
            iCTCertificate.VendorsListId = value.VendorsListID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteICTCertificate")]
        public void Delete(int id)
        {
            var iCTCertificate = _context.Ictcertificate.SingleOrDefault(p => p.IctcertificateId == id);
            if (iCTCertificate != null)
            {
                _context.Ictcertificate.Remove(iCTCertificate);
                _context.SaveChanges();
            }
        }
    }
}