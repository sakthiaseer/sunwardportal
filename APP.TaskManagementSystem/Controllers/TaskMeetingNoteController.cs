﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskMeetingNoteController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskMeetingNoteController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet("TaskMeetingNote")]
        [Route("GetTaskMeetingNote")]
        public List<TaskMeetingNoteModel> Get(TaskMeetingNoteModel taskmeeting)
        {

            var taskMeetingNote = _context.TaskMeetingNote.Select
                (t => new TaskMeetingNoteModel
                {
                    TaskAttachmentID = t.TaskAttachmentId,
                    MeetingNotes = t.MeetingNotes,
                    MeetingDate = t.MeetingDate,
                    AddedByUserID = t.AddedByUserId,
                    AddedDate = t.AddedDate,
                    AddedByUser = t.AddedByUser.UserName,
                    TaskMeetingNoteID = t.TaskMeetingNoteId

                }).OrderByDescending(a => a.TaskAttachmentID).Where(t => t.TaskAttachmentID == taskmeeting.TaskAttachmentID.Value).AsNoTracking().ToList();



            return taskMeetingNote;


        }

        // GET: api/Project/2
        [HttpGet("GetTaskAssigned/{id:int}")]
        //public ActionResult<TaskNotesModel> Get(int? id)
        //{
        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //var DiscussionNotes = _context.DiscussionNotes.SingleOrDefault(p => p.TaskNotesId == id.Value);
        //    //var result = _mapper.Map<TaskNotesModel>(DiscussionNotes);
        //    //if (result == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    return result;
        //}


        // POST: api/User
        [HttpPost]
        [Route("InsertTaskMeetingNote")]
        public TaskMeetingNoteModel Post(TaskMeetingNoteModel value)
        {
            var taskMeetingNote = new TaskMeetingNote
            {
                TaskAttachmentId = value.TaskAttachmentID,
                MeetingDate = value.MeetingDate,
                MeetingNotes = value.MeetingNotes,
                AddedByUserId = value.AddedByUserID,
                AddedDate = value.AddedDate,

            };
            _context.TaskMeetingNote.Add(taskMeetingNote);
            if (value.TaskAttachmentID > 0)
            {
                var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.TaskAttachmentId == value.TaskAttachmentID);
                if (taskattach != null)
                {
                    taskattach.IsMeetingNotes = true;
                }
            }

            _context.SaveChanges();
            value.TaskMeetingNoteID = taskMeetingNote.TaskMeetingNoteId;
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskMeetingNote")]
        public TaskMeetingNoteModel Put(TaskMeetingNoteModel value)
        {

            var taskMeetingNote = _context.TaskMeetingNote.SingleOrDefault(p => p.TaskMeetingNoteId == value.TaskMeetingNoteID);
            //  folders.TaskMemberId = value.TaskMemberID;
            //taskMeetingNote.TaskMeetingNoteId = value.TaskMeetingNoteId;
            //taskMeetingNote.TaskAttachmentId = value.TaskAttachmentId;
            taskMeetingNote.MeetingDate = value.MeetingDate;
            taskMeetingNote.MeetingNotes = value.MeetingNotes;
            taskMeetingNote.AddedByUserId = value.AddedByUserID;
            taskMeetingNote.AddedDate = value.AddedDate;



            _context.SaveChanges();
            return value;
        }

        [HttpDelete]
        [Route("DeleteMeetingNote")]
        public void Delete(int id)
        {
            var State = _context.TaskMeetingNote.SingleOrDefault(p => p.TaskMeetingNoteId == id);
            if (State != null)
            {
                _context.TaskMeetingNote.Remove(State);
                _context.SaveChanges();

                var comments = _context.TaskMeetingNote.Where(c => c.TaskAttachmentId == State.TaskAttachmentId).Count();
                if (comments == 0)
                {
                    var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.TaskAttachmentId == State.TaskAttachmentId);
                    if (taskattach != null)
                    {
                        taskattach.IsMeetingNotes = false;
                    }
                    _context.SaveChanges();
                }

            }
        }
    }
}