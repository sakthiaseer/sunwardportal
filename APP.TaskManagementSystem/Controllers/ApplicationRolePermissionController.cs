﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationRolePermissionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationRolePermissionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApplicationRolePermissions")]
        public List<ApplicationRolePermissionModel> Get()
        {
            var applicationRolePermission = _context.ApplicationRolePermission.Include("Company").Include("ModifiedByUser").Select(s => new ApplicationRolePermissionModel
            {

                RolePermissionID = s.RolePermissionId,
                RoleID = s.RoleId,
                PermissionID = s.PermissionId,

            }).OrderByDescending(o => o.RolePermissionID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ApplicationRolePermissionModel>>(ApplicationRolePermission);
            return applicationRolePermission;
        }


        [HttpGet]
        [Route("GetApplicationRolePermission")]
        public List<ApplicationRolePermissionModel> GetApplicationRolePermission(int id)
        {
            var applicationRolePermission = _context.ApplicationRolePermission.Select(s => new ApplicationRolePermissionModel
            {
                PermissionID = s.PermissionId,
                RoleID = s.RoleId,
                RolePermissionID = s.RolePermissionId,

            }).OrderByDescending(o => o.RolePermissionID).Where(t => t.RolePermissionID == id).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ApplicationRolePermissionModel>>(ApplicationRolePermission);
            return applicationRolePermission;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationRolePermissionModel> GetData(SearchModel searchModel)
        {
            var ApplicationRolePermission = new ApplicationRolePermission();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).LastOrDefault();
                        break;
                    case "Previous":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderBy(o => o.RolePermissionId).FirstOrDefault(s => s.RolePermissionId > searchModel.Id);
                        break;
                    case "Previous":
                        ApplicationRolePermission = _context.ApplicationRolePermission.OrderByDescending(o => o.RolePermissionId).FirstOrDefault(s => s.RolePermissionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationRolePermissionModel>(ApplicationRolePermission);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationRolePermission")]
        public ApplicationRolePermissionModel Post(ApplicationRolePermissionModel value)
        {


            var applicationPermission = new ApplicationPermission
            {
                PermissionName = value.PermissionModel.PermissionName,
                PermissionCode = value.PermissionModel.PermissionCode,
                ParentId = value.PermissionModel.ParentID,
                PermissionLevel = value.PermissionModel.PermissionLevel,
                ControllerName = value.PermissionModel.ControllerName,
                ActionName = value.PermissionModel.ActionName,
                Icon = value.PermissionModel.Icon,
                MenuId = value.PermissionModel.MenuId,
                PermissionUrl = value.PermissionModel.PermissionURL,
                PermissionGroup = value.PermissionModel.PermissionGroup,
                PermissionOrder = value.PermissionModel.PermissionOrder,
                IsDisplay = value.PermissionModel.IsDisplay,
                AppplicationPermissionId = value.PermissionModel.AppplicationPermissionID.Value,

            };
            var applicationRolePermission = new ApplicationRolePermission
            {
                RoleId = value.RoleID.Value,
                PermissionId = applicationPermission.PermissionId,



            };
            _context.ApplicationPermission.Add(applicationPermission);
            _context.ApplicationRolePermission.Add(applicationRolePermission);
            _context.SaveChanges();

            // _context.IctlayoutApplicationRolePermissionypes.Add(ictLayout);
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationRolePermission")]
        public ApplicationRolePermissionModel Put(ApplicationRolePermissionModel value)
        {
            var applicationRolePermission = _context.ApplicationRolePermission.SingleOrDefault(p => p.RolePermissionId == value.RolePermissionID);
            //ApplicationRolePermission.ApplicationRolePermissionId = value.ApplicationRolePermissionID;
            applicationRolePermission.RoleId = value.RoleID.Value;
            applicationRolePermission.PermissionId = value.PermissionID.Value;
            var applicationPermission = _context.ApplicationPermission.SingleOrDefault(a => a.PermissionId == value.PermissionModel.PermissionID);
            if (applicationPermission != null)
            {
                applicationPermission.PermissionLevel = value.PermissionModel.PermissionLevel;
                applicationPermission.PermissionName = value.PermissionModel.PermissionName;
                applicationPermission.PermissionCode = value.PermissionModel.PermissionCode;
                applicationPermission.ParentId = value.PermissionModel.ParentID;
                applicationPermission.ControllerName = value.PermissionModel.ControllerName;
                applicationPermission.ActionName = value.PermissionModel.ActionName;
                applicationPermission.Icon = value.PermissionModel.Icon;
                applicationPermission.PermissionUrl = value.PermissionModel.PermissionURL;
                applicationPermission.PermissionGroup = value.PermissionModel.PermissionGroup;
                applicationPermission.PermissionOrder = value.PermissionModel.PermissionOrder;
                applicationPermission.IsDisplay = value.PermissionModel.IsDisplay;
                applicationPermission.AppplicationPermissionId = value.PermissionModel.AppplicationPermissionID.Value;
            }

            _context.SaveChanges();



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationRolePermission")]
        public void Delete(int id)
        {
            var ApplicationRolePermission = _context.ApplicationRolePermission.SingleOrDefault(p => p.RolePermissionId == id);
            var errorMessage = "";
            try
            {
                if (ApplicationRolePermission != null)
                {
                    _context.ApplicationRolePermission.Remove(ApplicationRolePermission);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction record for this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }


        [HttpGet]
        [Route("GetApplicationPermissions")]
        public async Task<ApplicationPermissionData> GetApplicationPermissions(int id)
        {
            ApplicationPermissionData applicationPermissionData = new ApplicationPermissionData();
            List<ApplicationPermissionModel> applicationPermissions = new List<ApplicationPermissionModel>();
            List<ApplicationPermissionTree> applicationPermissionTreeItems = new List<ApplicationPermissionTree>();
            try
            {
                // Settings.  
                SqlParameter roleParam = new SqlParameter("@RoleID", id);

                // Processing.  
                string sqlQuery = "EXEC [dbo].[sp_Get_ApplicationRolePermission] @RoleID";

                var spApplicationPermission = await _context.Set<SpApplicationPermission>().FromSqlRaw(sqlQuery, roleParam).AsNoTracking().ToListAsync();

                spApplicationPermission.Where(f => f.IsNewPortal.GetValueOrDefault(false) == true).ToList().ForEach(s =>
                   {
                       if (!s.ParentID.HasValue)
                       {
                           ApplicationPermissionModel applicationPermissionModel = new ApplicationPermissionModel
                           {
                               PermissionID = s.PermissionID,
                               Name = s.PermissionName,
                               PermissionName = s.PermissionName,
                               ParentID = s.ParentID,
                               Icon = s.Icon,
                               PermissionGroup = s.PermissionGroup,
                               IsSelected = s.IsSelected
                           };
                           applicationPermissions.Add(applicationPermissionModel);
                       }
                       else
                       {
                           var applicationPermission = applicationPermissions.FirstOrDefault(a => a.PermissionID == s.ParentID);
                           if (applicationPermission != null)
                           {
                               applicationPermission.Children.Add(new ApplicationPermissionModel
                               {
                                   PermissionID = s.PermissionID,
                                   Name = s.PermissionName,
                                   PermissionName = s.PermissionName,
                                   ParentID = s.ParentID,
                                   Icon = s.Icon,
                                   PermissionGroup = s.PermissionGroup,
                                   IsSelected = s.IsSelected
                               });
                           }
                           else
                           {
                               applicationPermissions.ToList().ForEach(applicationPermissionModel =>
                               {
                                   AddChildLevelPermission(applicationPermissionModel, s);
                               });
                           }
                       }
                   });

                int index = 0;
                List<long> selectionIds = new List<long>();

                applicationPermissions.ToList().ForEach(ap =>
                {
                    ap.Id = ++index;
                    List<long> removeSelectionIDs = new List<long>();
                    if (ap.IsSelected == 1)
                    {
                        if (ap.Children.Any() && ap.Children.All(c => c.IsSelected == 1))
                        {
                            selectionIds.Add(ap.Id);
                        }
                    }
                    ApplicationPermissionTree applicationPermissionTree = new ApplicationPermissionTree
                    {
                        Id = ap.Id,
                        Name = ap.PermissionName
                    };
                    if (ap.Children.Any())
                    {
                        AddChildLevelTree(ap, applicationPermissionTree, ref index, selectionIds, removeSelectionIDs);
                    }
                    if (removeSelectionIDs.Any())
                    {
                        removeSelectionIDs.ForEach(r =>
                        {
                            selectionIds.Remove(r);
                        });
                        selectionIds.Remove(ap.Id);
                    }
                    applicationPermissionTreeItems.Add(applicationPermissionTree);
                });

                applicationPermissionData.ApplicationPermissionModels = applicationPermissions;
                applicationPermissionData.ApplicationPermissionTreeItems = applicationPermissionTreeItems;
                applicationPermissionData.SelectedIds = selectionIds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return applicationPermissionData;
        }

        private void AddChildLevelTree(ApplicationPermissionModel applicationPermissionModel, ApplicationPermissionTree applicationPermissionTree, ref int index, List<long> selectionIds, List<long> removeSelectionIDs)
        {
            foreach (var c in applicationPermissionModel.Children)
            {
                c.Id = ++index;
                ApplicationPermissionTree childApplicationPermissionTree = new ApplicationPermissionTree { Id = c.Id, Name = c.PermissionName };
                applicationPermissionTree.Children.Add(childApplicationPermissionTree);
                if (c.IsSelected == 1)
                {
                    if (c.Children.Any() && c.Children.All(ch => ch.IsSelected == 1))
                    {
                        selectionIds.Add(c.Id);
                    }
                    else if (c.Children.Any() && !c.Children.All(ch => ch.IsSelected == 1) && !removeSelectionIDs.Contains(applicationPermissionModel.Id))
                    {
                        removeSelectionIDs.Add(applicationPermissionModel.Id);
                    }
                    else if (c.Children.Count == 0 && c.IsSelected == 1)
                    {
                        selectionIds.Add(c.Id);
                    }
                }
                if (c.Children.Any())
                {
                    AddChildLevelTree(c, childApplicationPermissionTree, ref index, selectionIds, removeSelectionIDs);
                }
            }
        }

        private void AddChildLevelPermission(ApplicationPermissionModel applicationPermissionModel, SpApplicationPermission childPermission)
        {
            applicationPermissionModel.Children.ToList().ForEach(parent =>
            {
                if (parent.PermissionID == childPermission.ParentID)
                {
                    parent.Children.Add(new ApplicationPermissionModel
                    {
                        PermissionID = childPermission.PermissionID,
                        Name = childPermission.PermissionName,
                        PermissionName = childPermission.PermissionName,
                        ParentID = childPermission.ParentID,
                        Icon = childPermission.Icon,
                        PermissionGroup = childPermission.PermissionGroup,
                        IsSelected = childPermission.IsSelected
                    });
                }
                else
                {
                    AddChildLevelPermission(parent, childPermission);
                }
            });
        }

        [HttpPost]
        [Route("InsertApplicationRolePermissions")]
        public bool InsertApplicationRolePermissions(InsertModel insertModel)
        {
            bool isSaveSuccess = false;
            try
            {
                var applicationPermissionModels = insertModel.ApplicationPermissionModels;
                var selectionIds = insertModel.Ids;
                List<long> permissionIds = GetPermissionIds(selectionIds, applicationPermissionModels);
                // Settings.  
                SqlParameter roleParam = new SqlParameter("@RoleID", insertModel.Id);
                SqlParameter permissionParam = new SqlParameter("@PermissionIDs", SqlDbType.Structured);
                DataTable permissionDataTable = new DataTable();
                permissionDataTable.Columns.Add("ID");
                permissionIds.ForEach(p =>
                {
                    permissionDataTable.Rows.Add(p);
                });
                permissionParam.Value = permissionDataTable;
                permissionParam.TypeName = "ListOfID";


                // Processing.  
                string sqlQuery = "EXEC [dbo].[sp_Ins_ApplicationRolePermission] @RoleID, @PermissionIDs";

                var spApplicationPermission = _context.Database.ExecuteSqlRaw(sqlQuery, parameters: new[] { roleParam, permissionParam });  //_context.Set<SpApplicationRolePermission>().ex(sqlQuery, roleParam, permissionParam).ToList();
                isSaveSuccess = true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            return isSaveSuccess;
        }

        private List<long> GetPermissionIds(List<int> ids, List<ApplicationPermissionModel> applicationPermissionModels)
        {
            List<long> permissionIds = new List<long>();
            applicationPermissionModels.ForEach(s =>
            {
                if (ids.Contains((int)s.Id))
                {
                    permissionIds.Add(s.PermissionID);
                }
                if (s.Children.Any())
                {
                    AddChildPermissionIds(ids, s, permissionIds);
                }
            });
            return permissionIds;
        }

        private void AddChildPermissionIds(List<int> ids, ApplicationPermissionModel applicationPermissionModel, List<long> permissionIds)
        {
            applicationPermissionModel.Children.ForEach(c =>
            {
                if (ids.Contains((int)c.Id))
                {
                    permissionIds.Add(c.PermissionID);
                }
                if (c.Children.Any())
                {
                    AddChildPermissionIds(ids, c, permissionIds);
                }

            });
        }

    }
}