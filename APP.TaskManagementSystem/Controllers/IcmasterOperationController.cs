﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IcmasterOperationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public IcmasterOperationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetIcmasterOperation")]
        public List<IcmasterOperationModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<IcmasterOperationModel> icmasterOperationModels = new List<IcmasterOperationModel>();
            var icmasterOperation = _context.IcmasterOperation
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode").OrderByDescending(o => o.IcmasterOperationId).AsNoTracking().ToList();
            if(icmasterOperation!=null && icmasterOperation.Count>0)
            {
                icmasterOperation.ForEach(s =>
                {
                    IcmasterOperationModel icmasterOperationModel = new IcmasterOperationModel
                    {
                        IcmasterOperationId = s.IcmasterOperationId,
                        WiLink = s.WiLink,
                        ManufacturingStepsId = s.ManufacturingStepsId,
                        ManufacturingStepsName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSteps.ManufacturingStepsId).Select(a => a.Value).SingleOrDefault() : "",
                        MasterOperation = s.MasterOperation,
                        Training = s.Training,
                        TrainingFlag = (s.Training == true) ? "1" : "0",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    icmasterOperationModels.Add(icmasterOperationModel);
                });
            }
          
            return icmasterOperationModels;
        }
        [HttpPost]
        [Route("GetIcmasterOperationByRefNo")]
        public List<IcmasterOperationModel> GetCommonPackagingDividerByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<IcmasterOperationModel> icmasterOperationModels = new List<IcmasterOperationModel>();
            List<IcmasterOperation> icmasterOperation = new List<IcmasterOperation>();
            if (refSearchModel.IsHeader)
            {
                icmasterOperation = _context.IcmasterOperation.Include(ms=>ms.ManufacturingSteps).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.IcmasterOperationId).AsNoTracking().ToList();

            }
            else
            {
                icmasterOperation = _context.IcmasterOperation.Include(ms => ms.ManufacturingSteps).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.IcmasterOperationId).AsNoTracking().ToList();

            }
            if(icmasterOperation!=null && icmasterOperation.Count>0)
            {
                icmasterOperation.ForEach(s =>
                {

               
                IcmasterOperationModel icmasterOperationModel = new IcmasterOperationModel
                {
                    IcmasterOperationId = s.IcmasterOperationId,
                    WiLink = s.WiLink,
                    ManufacturingStepsId = s.ManufacturingStepsId,
                    ManufacturingStepsName = s.ManufacturingSteps!=null && applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSteps.ManufacturingStepsId).Select(a => a.Value).SingleOrDefault() : "",
                    MasterOperation = s.MasterOperation,
                    Training = s.Training,
                    TrainingFlag = (s.Training == true) ? "1" : "0",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo
                };
                    icmasterOperationModels.Add(icmasterOperationModel);
                });
            }               
            return icmasterOperationModels;
           
        }
        [HttpGet]
        [Route("GetCommonProcessLine")]
        public List<CommonProcessLineModel> CommonProcessLine()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<CommonProcessLineModel> commonProcessLineModels = new List<CommonProcessLineModel>();
            var commonProcessLine = _context.CommonProcessLine
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode").OrderByDescending(o => o.CommonProcessLineId).AsNoTracking().ToList();
            if(commonProcessLine!=null && commonProcessLine.Count>0)
            {
                commonProcessLine.ForEach(s =>
                {
                    CommonProcessLineModel commonProcessLineModel = new CommonProcessLineModel
                    {
                        CommonProcessLineId = s.CommonProcessLineId,
                        CommonProcessId = s.CommonProcessId,
                        ManufacturingStepsId = s.ManufacturingStepsId,
                        ManufacturingStepsName = s.ManufacturingStepsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                    };
                    commonProcessLineModels.Add(commonProcessLineModel);
                });

            }           
            return commonProcessLineModels;
        }
        [HttpPost]
        [Route("GetData")]
        public ActionResult<IcmasterOperationModel> GetData(SearchModel searchModel)
        {
            var icmasterOperation = new IcmasterOperation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).FirstOrDefault();
                        break;
                    case "Last":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).LastOrDefault();
                        break;
                    case "Next":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).LastOrDefault();
                        break;
                    case "Previous":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).FirstOrDefault();
                        break;
                    case "Last":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).LastOrDefault();
                        break;
                    case "Next":
                        icmasterOperation = _context.IcmasterOperation.OrderBy(o => o.IcmasterOperationId).FirstOrDefault(s => s.IcmasterOperationId > searchModel.Id);
                        break;
                    case "Previous":
                        icmasterOperation = _context.IcmasterOperation.OrderByDescending(o => o.IcmasterOperationId).FirstOrDefault(s => s.IcmasterOperationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<IcmasterOperationModel>(icmasterOperation);
            return result;
        }
        [HttpPost]
        [Route("InsertIcmasterOperation")]
        public IcmasterOperationModel Post(IcmasterOperationModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MasterOperation });
            var icmasterOperation = new IcmasterOperation
            {
                ManufacturingStepsId = value.ManufacturingStepsId,
                WiLink = value.WiLink,
                MasterOperation=value.MasterOperation,
                Training=(value.TrainingFlag=="1")?true:false,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.IcmasterOperation.Add(icmasterOperation);
            _context.SaveChanges();
            value.IcmasterOperationId = icmasterOperation.IcmasterOperationId;
            value.MasterProfileReferenceNo = icmasterOperation.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = icmasterOperation.ProfileLinkReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateIcmasterOperation")]
        public IcmasterOperationModel Put(IcmasterOperationModel value)
        {
            var icmasterOperation = _context.IcmasterOperation.SingleOrDefault(p => p.IcmasterOperationId == value.IcmasterOperationId);
            icmasterOperation.ManufacturingStepsId = value.ManufacturingStepsId;
            icmasterOperation.WiLink = value.WiLink;
            icmasterOperation.MasterOperation = value.MasterOperation;
            icmasterOperation.Training = (value.TrainingFlag == "1") ? true : false;
            icmasterOperation.StatusCodeId = value.StatusCodeID.Value;
            icmasterOperation.ModifiedByUserId = value.ModifiedByUserID;
            icmasterOperation.ModifiedDate = DateTime.Now;
            icmasterOperation.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            icmasterOperation.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteIcmasterOperation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var icmasterOperation = _context.IcmasterOperation.Where(p => p.IcmasterOperationId == id).FirstOrDefault();
                if (icmasterOperation != null)
                {
                    _context.IcmasterOperation.Remove(icmasterOperation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}