﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyCalendarLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public CompanyCalendarLineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
        }
        [HttpGet]
        [Route("GetCompanyCalendarLine")]
        public List<CompanyCalendarLineModel> Get(long id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var CompanyCalendarLine = _context.CompanyCalendarLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(c => c.CompanyCalendar)
                .Include(s => s.StatusCode)
                .Include(l => l.Machine)
                .Include(a => a.TypeOfEvent)
                .Include(z => z.CalenderStatus)
                .Include(a => a.CompanyCalendarLineUser)
                .Include(p => p.Vendor).Include(x => x.CompanyCalendarLineLocation).Where(w => w.CompanyCalendarId == id)
                .AsNoTracking().ToList();
            List<CompanyCalendarLineModel> CompanyCalendarLineModels = new List<CompanyCalendarLineModel>();
            CompanyCalendarLine.ForEach(s =>
            {
                CompanyCalendarLineModel CompanyCalendarLineModel = new CompanyCalendarLineModel();
                CompanyCalendarLineModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                CompanyCalendarLineModel.CompanyCalendarId = s.CompanyCalendarId;
                CompanyCalendarLineModel.EventDate = s.EventDate;
                CompanyCalendarLineModel.VendorId = s.VendorId;
                CompanyCalendarLineModel.TypeOfServiceId = s.TypeOfServiceId;
                CompanyCalendarLineModel.MachineId = s.MachineId;
                CompanyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                CompanyCalendarLineModel.Notes = s.Notes;
                CompanyCalendarLineModel.StatusCodeID = s.StatusCodeId;
                CompanyCalendarLineModel.AddedByUserID = s.AddedByUserId;
                CompanyCalendarLineModel.ModifiedByUserID = s.ModifiedByUserId;
                CompanyCalendarLineModel.AddedDate = s.AddedDate;
                CompanyCalendarLineModel.ModifiedDate = s.ModifiedDate;
                CompanyCalendarLineModel.LinkDocComment = s.LinkDocComment;
                CompanyCalendarLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                CompanyCalendarLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                CompanyCalendarLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                CompanyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                CompanyCalendarLineModel.MachineName = s.Machine != null ? s.Machine.NameOfTheMachine : "";
                CompanyCalendarLineModel.VendorName = s.Vendor != null ? s.Vendor.VendorName : "";
                CompanyCalendarLineModel.LocationIds = s.CompanyCalendarLineLocation != null ? s.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                CompanyCalendarLineModel.Name = CompanyCalendarLineModel.TypeOfServiceName;
                CompanyCalendarLineModel.AllEventFlag = s.AllEventFlag;
                CompanyCalendarLineModel.FromTime = s.FromTime;
                CompanyCalendarLineModel.ToTime = s.ToTime;
                CompanyCalendarLineModel.DueDate = s.DueDate;
                CompanyCalendarLineModel.TypeOfEventId = s.TypeOfEventId;
                CompanyCalendarLineModel.Subject = s.Subject;
                CompanyCalendarLineModel.SessionId = s.SessionId;
                CompanyCalendarLineModel.CalenderStatusId = s.CalenderStatusId;
                CompanyCalendarLineModel.CalenderStatus = s.CalenderStatus?.Value;
                CompanyCalendarLineModel.CalanderParentId = s.CalanderParentId;
                CompanyCalendarLineModel.TypeOfEventName = s.TypeOfEvent?.Value;
                CompanyCalendarLineModel.Tentative = s.Tentative;
                CompanyCalendarLineModel.ParticipantsIds = s.CompanyCalendarLineUser != null ? s.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId && w.TypeName == "Participants").Select(a => a.UserId).ToList() : new List<long?>();
                CompanyCalendarLineModel.InformationIds = s.CompanyCalendarLineUser != null ? s.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId && w.TypeName == "Information").Select(a => a.UserId).ToList() : new List<long?>();
                CompanyCalendarLineModel.Information = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.InformationIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                CompanyCalendarLineModel.Participants = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.ParticipantsIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                CompanyCalendarLineModels.Add(CompanyCalendarLineModel);
            });
            return CompanyCalendarLineModels.OrderByDescending(a => a.CompanyCalendarId).ToList();
        }
        [HttpGet]
        [Route("GetCompanyCalendarDocuments")]
        public List<NotifyDocumentModel> GetCompanyCalendarDocuments(long id)
        {
            List<NotifyDocumentModel> documentsModels = new List<NotifyDocumentModel>();
            var companyCalendarLines = _context.CompanyCalendarLine.Where(w => w.SessionId != null).Select(a => new { a.SessionId, a.CompanyCalendarLineId, a.CalenderStatusId }).ToList();
            var sessionIds = companyCalendarLines.Select(s => s.SessionId).ToList();
            var latestDoc = _context.Documents.Select(e => new
            {
                e.DocumentId,
                e.FileName,
                e.ContentType,
                FileProfileTypeId = e.FilterProfileTypeId,
                e.SessionId,
                e.AddedByUserId,
                e.StatusCodeId,
                e.ModifiedByUserId,
                e.IsLatest,
                e.DocumentParentId,
                e.FileIndex,
            }).Where(n => sessionIds.Contains(n.SessionId) && n.IsLatest == true).ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            if (latestDoc != null && latestDoc.Count > 0)
            {
                var userIds = latestDoc.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
                userIds.AddRange(latestDoc.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
                var codeIds = latestDoc.Select(s => s.StatusCodeId).ToList();
                var fileProfileTypeIds = latestDoc.Select(s => s.FileProfileTypeId.GetValueOrDefault(0)).ToList();
                var fileProfileType = _context.FileProfileType.AsNoTracking().ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
                latestDoc.ForEach(s =>
                {
                    NotifyDocumentModel documentModel = new NotifyDocumentModel();
                    var companyCalendarLine = companyCalendarLines.FirstOrDefault(f => f.SessionId == s.SessionId);
                    var userIdExits = GetNotesUserIds(companyCalendarLine.CompanyCalendarLineId).Contains(id);
                    if (userIdExits == true)
                    {
                        var fileName = s.FileName.Split('.');
                        documentModel.DocumentParentId = s.DocumentParentId;
                        documentModel.DocumentID = s.DocumentId;
                        documentModel.DocumentId = s.DocumentId;
                        documentModel.DocumentShareId = companyCalendarLine?.CompanyCalendarLineId;
                        documentModel.DocumentName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName;
                        documentModel.ContentType = s.ContentType;
                        documentModel.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == s.FileProfileTypeId)?.FirstOrDefault()?.Name;
                        var mainprofileId = GetMainProfileId(fileProfileType, s.FileProfileTypeId);
                        documentModel.FileProfileTypeId = s.FileProfileTypeId;
                        documentModel.ParentId = mainprofileId?.FirstOrDefault()?.FileProfileTypeId;
                        documentModel.SessionId = s.SessionId;
                        documentModel.Type = "Document";
                        documentModel.ModifiedByUser = appUsers != null && s.ModifiedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "";
                        documentModel.AddedByUser = appUsers != null && s.AddedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "";
                        documentModel.StatusCode = masterDetailList != null && companyCalendarLine?.CalenderStatusId != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == companyCalendarLine.CalenderStatusId)?.Value : "";
                        documentsModels.Add(documentModel);
                    }
                });
            }
            return documentsModels;
        }
        private List<FileProfileType> GetMainProfileId(List<FileProfileType> fileprofile, long? mainProfileId)
        {
            var fileprofileList = fileprofile;
            fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList().ForEach(s =>
            {
                if (s.ParentId != null)
                {
                    fileprofileList = GetMainProfileId(fileprofileList, s.ParentId);
                }
                else
                {
                    fileprofileList = fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList();
                }
            });
            return fileprofileList;
        }
        [HttpPost]
        [Route("GetCompanyCalendarLineAssistance")]
        public List<CompanyCalendarLineModel> GetCompanyCalendarLineAssistance(SearchModel searchModel)
        {

            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName, s.SessionId }).AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var calendarAssistance = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMaster.ApplicationMasterCodeId == 278 && w.Value.ToLower() == "calender assistance").FirstOrDefault()?.ApplicationMasterDetailId;
            List<CompanyCalendarLineModel> CompanyCalendarLineModels = new List<CompanyCalendarLineModel>();
            var startDate = searchModel.StartDate.Value.AddDays(-10).Date;
            var endDate = searchModel.EndDate.Value.AddDays(10).Date;
            List<long> masterIds = _context.CompanyCalendarLine.Where(w => w.EventDate.Value.Date >= startDate.Date && w.EventDate.Value.Date <= endDate.Date).Select(s => s.CompanyCalendarLineId).Distinct().ToList();
            masterIds.AddRange(_context.CompanyCalendarLine.Where(w => w.DueDate.Value.Date >= startDate.Date && w.DueDate.Value.Date <= endDate.Date).Select(s => s.CompanyCalendarLineId).Distinct().ToList());
            var appUserId = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => w.SessionId == searchModel.SessionID).FirstOrDefault()?.UserId;
            if (calendarAssistance != null && masterIds.Count > 0 && appUserId != null)
            {
                var CompanyCalendarLines = _context.CompanyCalendarLine
                    .Include(a => a.AddedByUser)
                    .Include(m => m.ModifiedByUser)
                    .Include(c => c.CompanyCalendar)
                    .Include(s => s.StatusCode)
                    .Include(c => c.TypeOfEvent)
                    .Include(b => b.CalenderStatus).
                    Include(x => x.CompanyCalendarLineLocation)
                    .Include(a => a.CompanyCalendarLineUser)
                     .Include(a => a.CompanyCalendar.CompanyCalendarAssistance)
                     .Include(a => a.CompanyCalendar.Company)
                    .Where(w => w.CompanyCalendar.CalenderEventId == calendarAssistance && masterIds.Distinct().ToList().Contains(w.CompanyCalendarLineId));
                if (searchModel.LocationEventId != null && searchModel.LocationEventId.Count > 0)
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => searchModel.LocationEventId.Contains(w.TypeOfServiceId));
                }
                if (!string.IsNullOrEmpty(searchModel.Subject))
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => w.Subject.Contains(searchModel.Subject));
                }
                if (searchModel.CalenderStatusId != null && searchModel.CalenderStatusId.Count > 0)
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => searchModel.CalenderStatusId.Contains(w.CalenderStatusId));
                }
                if (searchModel.TypeOfEventId != null && searchModel.TypeOfEventId.Count > 0)
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => searchModel.TypeOfEventId.Contains(w.TypeOfEventId));
                }
                if (searchModel.PlantId != null)
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => w.CompanyCalendar.CompanyId == searchModel.PlantId);
                }
                if (searchModel.OverDue == true)
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => w.EventDate.Value.Date < DateTime.Now.Date);
                }
                else
                {
                    CompanyCalendarLines = CompanyCalendarLines.Where(w => w.EventDate.Value.Date >= DateTime.Now.Date);
                }
                var CompanyCalendarLine = CompanyCalendarLines.AsNoTracking().ToList();
                if (CompanyCalendarLine != null && CompanyCalendarLine.Count > 0)
                {
                    CompanyCalendarLine.ForEach(s =>
                    {
                        var userIdExits = GetNotesUserIds(s.CompanyCalendarLineId).Contains(searchModel.Id);
                        if (userIdExits == true)
                        {
                            CompanyCalendarLineModel CompanyCalendarLineModel = new CompanyCalendarLineModel();
                            CompanyCalendarLineModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                            CompanyCalendarLineModel.Subject = s.Subject;
                            CompanyCalendarLineModel.CompanyCalendarId = s.CompanyCalendarId;
                            CompanyCalendarLineModel.CompanyId = s.CompanyCalendar?.CompanyId;
                            CompanyCalendarLineModel.CompanyName = s.CompanyCalendar?.Company?.PlantCode;
                            CompanyCalendarLineModel.EventDate = s.EventDate;
                            CompanyCalendarLineModel.VendorId = s.VendorId;
                            CompanyCalendarLineModel.TypeOfServiceId = s.TypeOfServiceId;
                            CompanyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                            CompanyCalendarLineModel.Notes = s.Notes;
                            CompanyCalendarLineModel.StatusCodeID = s.StatusCodeId;
                            CompanyCalendarLineModel.AddedByUserID = s.AddedByUserId;
                            CompanyCalendarLineModel.ModifiedByUserID = s.ModifiedByUserId;
                            CompanyCalendarLineModel.AddedDate = s.AddedDate;
                            CompanyCalendarLineModel.ModifiedDate = s.ModifiedDate;
                            CompanyCalendarLineModel.LinkDocComment = s.LinkDocComment;
                            CompanyCalendarLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                            CompanyCalendarLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                            CompanyCalendarLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                            CompanyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                            CompanyCalendarLineModel.LocationIds = s.CompanyCalendarLineLocation != null ? s.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                            CompanyCalendarLineModel.Name = CompanyCalendarLineModel.TypeOfServiceName;
                            CompanyCalendarLineModel.AllEventFlag = s.AllEventFlag;
                            CompanyCalendarLineModel.FromTime = s.FromTime;
                            CompanyCalendarLineModel.ToTime = s.ToTime;
                            CompanyCalendarLineModel.DueDate = s.DueDate;
                            CompanyCalendarLineModel.TypeOfEventId = s.TypeOfEventId;
                            CompanyCalendarLineModel.RequireAssistance = s.CompanyCalendar?.RequireAssistance;
                            CompanyCalendarLineModel.RequireAssistanceFlag = s.CompanyCalendar?.RequireAssistance == true ? "Yes" : "No";
                            CompanyCalendarLineModel.TypeOfEventName = s.TypeOfEvent?.Value;
                            CompanyCalendarLineModel.SessionId = s.SessionId;
                            CompanyCalendarLineModel.CalenderStatusId = s.CalenderStatusId;
                            CompanyCalendarLineModel.CalenderStatus = s.CalenderStatus?.Value;
                            CompanyCalendarLineModel.ParticipantsIds = s.CompanyCalendarLineUser != null ? s.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId && w.TypeName == "Participants").Select(a => a.UserId).ToList() : new List<long?>();
                            CompanyCalendarLineModel.InformationIds = s.CompanyCalendarLineUser != null ? s.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId && w.TypeName == "Information").Select(a => a.UserId).ToList() : new List<long?>();
                            CompanyCalendarLineModel.Information = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.InformationIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                            CompanyCalendarLineModel.Participants = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.ParticipantsIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                            CompanyCalendarLineModel.LinkItems = GetCompanyCalendarLineLink(s.CompanyCalendarLineId);
                            CompanyCalendarLineModel.ApplicationUsers = GetNotesUsers(s.CompanyCalendarLineId);
                            CompanyCalendarLineModel.HeadersUserIds = GetHeaderUserIds(s.CompanyCalendarId.Value);
                            CompanyCalendarLineModel.OwnerId = s.CompanyCalendar?.OwnerId;
                            CompanyCalendarLineModel.AssistanceFromId = s.CompanyCalendar?.AssistanceFromId;
                            CompanyCalendarLineModel.RequestorId = s.CompanyCalendar?.AddedByUserId;
                            CompanyCalendarLineModel.OnBehalfId = s.CompanyCalendar?.OnBehalfId;
                            CompanyCalendarLineModel.Tentative = s.Tentative;
                            CompanyCalendarLineModel.OwnerName = CompanyCalendarLineModel.OwnerId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.OwnerId).Select(m => m.UserName).FirstOrDefault() : "";
                            CompanyCalendarLineModel.AssistanceFromIds = s.CompanyCalendar?.CompanyCalendarAssistance != null ? s.CompanyCalendar.CompanyCalendarAssistance.Where(w => w.CompanyCalendarId == s.CompanyCalendarId).Select(a => a.UserId).ToList() : new List<long?>();
                            CompanyCalendarLineModel.AssistanceFrom = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.AssistanceFromIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                            CompanyCalendarLineModel.OnBehalfName = CompanyCalendarLineModel.OnBehalfId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.OnBehalfId).Select(m => m.UserName).FirstOrDefault() : "";
                            CompanyCalendarLineModel.RequestorName = CompanyCalendarLineModel.RequestorId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.RequestorId).Select(m => m.UserName).FirstOrDefault() : "";
                            CompanyCalendarLineModels.Add(CompanyCalendarLineModel);
                        }
                    });
                }
            }
            if (searchModel.ParentID > 0)
            {
                var ifExits = CompanyCalendarLineModels.Where(w => w.CompanyCalendarLineId == searchModel.ParentID).Count();
                if (ifExits > 0)
                {
                }
                else
                {
                    var companyCalendarLine = _context.CompanyCalendarLine
                       .Include(a => a.AddedByUser)
                       .Include(m => m.ModifiedByUser)
                       .Include(c => c.CompanyCalendar)
                       .Include(s => s.StatusCode)
                       .Include(c => c.TypeOfEvent)
                       .Include(b => b.CalenderStatus).
                       Include(x => x.CompanyCalendarLineLocation)
                       .Include(a => a.CompanyCalendarLineUser)
                        .Include(a => a.CompanyCalendar.CompanyCalendarAssistance)
                        .Include(a => a.CompanyCalendar.Company)
                       .FirstOrDefault(w => w.CompanyCalendarLineId == searchModel.ParentID);
                    if (companyCalendarLine != null)
                    {
                        CompanyCalendarLineModel CompanyCalendarLineModel = new CompanyCalendarLineModel();
                        CompanyCalendarLineModel.CompanyCalendarLineId = companyCalendarLine.CompanyCalendarLineId;
                        CompanyCalendarLineModel.Subject = companyCalendarLine.Subject;
                        CompanyCalendarLineModel.CompanyCalendarId = companyCalendarLine.CompanyCalendarId;
                        CompanyCalendarLineModel.CompanyId = companyCalendarLine.CompanyCalendar?.CompanyId;
                        CompanyCalendarLineModel.CompanyName = companyCalendarLine.CompanyCalendar?.Company?.PlantCode;
                        CompanyCalendarLineModel.EventDate = companyCalendarLine.EventDate;
                        CompanyCalendarLineModel.VendorId = companyCalendarLine.VendorId;
                        CompanyCalendarLineModel.TypeOfServiceId = companyCalendarLine.TypeOfServiceId;
                        CompanyCalendarLineModel.NoOfHoursRequire = companyCalendarLine.NoOfHoursRequire;
                        CompanyCalendarLineModel.Notes = companyCalendarLine.Notes;
                        CompanyCalendarLineModel.StatusCodeID = companyCalendarLine.StatusCodeId;
                        CompanyCalendarLineModel.AddedByUserID = companyCalendarLine.AddedByUserId;
                        CompanyCalendarLineModel.ModifiedByUserID = companyCalendarLine.ModifiedByUserId;
                        CompanyCalendarLineModel.AddedDate = companyCalendarLine.AddedDate;
                        CompanyCalendarLineModel.ModifiedDate = companyCalendarLine.ModifiedDate;
                        CompanyCalendarLineModel.LinkDocComment = companyCalendarLine.LinkDocComment;
                        CompanyCalendarLineModel.AddedByUser = companyCalendarLine.AddedByUser != null ? companyCalendarLine.AddedByUser.UserName : "";
                        CompanyCalendarLineModel.ModifiedByUser = companyCalendarLine.ModifiedByUser != null ? companyCalendarLine.ModifiedByUser.UserName : "";
                        CompanyCalendarLineModel.StatusCode = companyCalendarLine.StatusCode != null ? companyCalendarLine.StatusCode.CodeValue : "";
                        CompanyCalendarLineModel.TypeOfServiceName = companyCalendarLine.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == companyCalendarLine.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                        CompanyCalendarLineModel.LocationIds = companyCalendarLine.CompanyCalendarLineLocation != null ? companyCalendarLine.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == companyCalendarLine.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                        CompanyCalendarLineModel.Name = CompanyCalendarLineModel.TypeOfServiceName;
                        CompanyCalendarLineModel.AllEventFlag = companyCalendarLine.AllEventFlag;
                        CompanyCalendarLineModel.FromTime = companyCalendarLine.FromTime;
                        CompanyCalendarLineModel.ToTime = companyCalendarLine.ToTime;
                        CompanyCalendarLineModel.DueDate = companyCalendarLine.DueDate;
                        CompanyCalendarLineModel.TypeOfEventId = companyCalendarLine.TypeOfEventId;
                        CompanyCalendarLineModel.RequireAssistance = companyCalendarLine.CompanyCalendar?.RequireAssistance;
                        CompanyCalendarLineModel.RequireAssistanceFlag = companyCalendarLine.CompanyCalendar?.RequireAssistance == true ? "Yes" : "No";
                        CompanyCalendarLineModel.TypeOfEventName = companyCalendarLine.TypeOfEvent?.Value;
                        CompanyCalendarLineModel.SessionId = companyCalendarLine.SessionId;
                        CompanyCalendarLineModel.CalenderStatusId = companyCalendarLine.CalenderStatusId;
                        CompanyCalendarLineModel.CalenderStatus = companyCalendarLine.CalenderStatus?.Value;
                        CompanyCalendarLineModel.ParticipantsIds = companyCalendarLine.CompanyCalendarLineUser != null ? companyCalendarLine.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == companyCalendarLine.CompanyCalendarLineId && w.TypeName == "Participants").Select(a => a.UserId).ToList() : new List<long?>();
                        CompanyCalendarLineModel.InformationIds = companyCalendarLine.CompanyCalendarLineUser != null ? companyCalendarLine.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == companyCalendarLine.CompanyCalendarLineId && w.TypeName == "Information").Select(a => a.UserId).ToList() : new List<long?>();
                        CompanyCalendarLineModel.Information = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.InformationIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                        CompanyCalendarLineModel.Participants = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.ParticipantsIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                        CompanyCalendarLineModel.LinkItems = GetCompanyCalendarLineLink(companyCalendarLine.CompanyCalendarLineId);
                        CompanyCalendarLineModel.ApplicationUsers = GetNotesUsers(companyCalendarLine.CompanyCalendarLineId);
                        CompanyCalendarLineModel.HeadersUserIds = GetHeaderUserIds(companyCalendarLine.CompanyCalendarId.Value);
                        CompanyCalendarLineModel.OwnerId = companyCalendarLine.CompanyCalendar?.OwnerId;
                        CompanyCalendarLineModel.AssistanceFromId = companyCalendarLine.CompanyCalendar?.AssistanceFromId;
                        CompanyCalendarLineModel.RequestorId = companyCalendarLine.CompanyCalendar?.AddedByUserId;
                        CompanyCalendarLineModel.OnBehalfId = companyCalendarLine.CompanyCalendar?.OnBehalfId;
                        CompanyCalendarLineModel.Tentative = companyCalendarLine.Tentative;
                        CompanyCalendarLineModel.OwnerName = CompanyCalendarLineModel.OwnerId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.OwnerId).Select(m => m.UserName).FirstOrDefault() : "";
                        CompanyCalendarLineModel.AssistanceFromIds = companyCalendarLine.CompanyCalendar?.CompanyCalendarAssistance != null ? companyCalendarLine.CompanyCalendar.CompanyCalendarAssistance.Where(w => w.CompanyCalendarId == companyCalendarLine.CompanyCalendarId).Select(a => a.UserId).ToList() : new List<long?>();
                        CompanyCalendarLineModel.AssistanceFrom = appUsers != null ? string.Join(",", appUsers.Where(w => CompanyCalendarLineModel.AssistanceFromIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                        CompanyCalendarLineModel.OnBehalfName = CompanyCalendarLineModel.OnBehalfId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.OnBehalfId).Select(m => m.UserName).FirstOrDefault() : "";
                        CompanyCalendarLineModel.RequestorName = CompanyCalendarLineModel.RequestorId != null && appUsers != null ? appUsers.Where(m => m.UserId == CompanyCalendarLineModel.RequestorId).Select(m => m.UserName).FirstOrDefault() : "";
                        CompanyCalendarLineModels.Add(CompanyCalendarLineModel);
                    }
                }
            }
            return CompanyCalendarLineModels.OrderByDescending(a => a.CompanyCalendarId).ToList();
        }
        private List<long?> GetHeaderUserIds(long id)
        {
            List<long?> userIds = new List<long?>();
            var companycalendar = _context.CompanyCalendar.Include(s => s.CompanyCalendarAssistance).SingleOrDefault(w => w.CompanyCalendarId == id);
            if (companycalendar != null)
            {
                if (companycalendar != null)
                {
                    userIds.Add(companycalendar.OwnerId.GetValueOrDefault(0));
                    if (companycalendar.RequireAssistance == true && companycalendar.CompanyCalendarAssistance != null && companycalendar.CompanyCalendarAssistance.Count > 0)
                    {
                        userIds.AddRange(companycalendar.CompanyCalendarAssistance?.Select(s => s.UserId).ToList());
                    }
                    userIds.Add(companycalendar.AddedByUserId.GetValueOrDefault(0));
                    userIds.Add(companycalendar.OnBehalfId.GetValueOrDefault(0));
                }
            }
            return userIds.Where(w => w > 0).Distinct().ToList();
        }
        [HttpGet]
        [Route("GetCompanyCalendarLineNotes")]
        public List<CompanyCalendarLineNotesModel> GetCompanyCalendarLineNotes(long? id)
        {
            List<CompanyCalendarLineNotesModel> companyCalendarLineNotesModels = new List<CompanyCalendarLineNotesModel>();
            var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Include(a => a.AddedByUser).Where(w => w.CompanyCalendarLineId == id).ToList();
            if (companyCalendarLineNotes != null)
            {
                companyCalendarLineNotes.ForEach(s =>
                {
                    CompanyCalendarLineNotesModel companyCalendarLineNotesModel = new CompanyCalendarLineNotesModel();
                    companyCalendarLineNotesModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                    companyCalendarLineNotesModel.CompanyCalendarLineNotesId = s.CompanyCalendarLineNotesId;
                    companyCalendarLineNotesModel.NotesDetails = s.NotesDetails;
                    companyCalendarLineNotesModel.AddedByUserId = s.AddedByUserId;
                    companyCalendarLineNotesModel.AddedDate = s.AddedDate;
                    companyCalendarLineNotesModel.AddedByUser = s.AddedByUser?.UserName;
                    companyCalendarLineNotesModels.Add(companyCalendarLineNotesModel);
                });
            }
            return companyCalendarLineNotesModels;
        }
        [HttpGet]
        [Route("GetCompanyCalendarLineLink")]
        public List<CompanyCalendarLineLinkModel> GetCompanyCalendarLineLink(long? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            List<CompanyCalendarLineLinkModel> companyCalendarLineNotesModels = new List<CompanyCalendarLineLinkModel>();
            var companyCalendarLineNotes = _context.CompanyCalendarLineLink.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(a => a.CompanyCalendarLineLinkUser).Include(a => a.CompanyCalendarLineLinkPrintUser).Where(w => w.CompanyCalendarLineId == id).ToList();
            if (companyCalendarLineNotes != null)
            {
                companyCalendarLineNotes.ForEach(s =>
                {
                    CompanyCalendarLineLinkModel companyCalendarLineNotesModel = new CompanyCalendarLineLinkModel();
                    companyCalendarLineNotesModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                    companyCalendarLineNotesModel.UrlLink = s.UrlLink;
                    companyCalendarLineNotesModel.UrlText = s.UrlText;
                    companyCalendarLineNotesModel.CompanyCalendarLineLinkId = s.CompanyCalendarLineLinkId;
                    companyCalendarLineNotesModel.RestrictUserIds = s.CompanyCalendarLineLinkUser != null ? s.CompanyCalendarLineLinkUser.Select(a => a.UserId).ToList() : new List<long?>();
                    companyCalendarLineNotesModel.RestrictUsers = appUsers != null ? string.Join(",", appUsers.Where(w => companyCalendarLineNotesModel.RestrictUserIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                    companyCalendarLineNotesModel.AddedByUserID = s.AddedByUserId;
                    companyCalendarLineNotesModel.AddedDate = s.AddedDate;
                    companyCalendarLineNotesModel.ModifiedDate = s.ModifiedDate;
                    companyCalendarLineNotesModel.ModifiedDate = s.ModifiedDate;
                    companyCalendarLineNotesModel.StatusCodeID = s.StatusCodeId;
                    companyCalendarLineNotesModel.IsPrint = s.IsPrint;
                    companyCalendarLineNotesModel.AddedByUser = s.AddedByUser?.UserName;
                    companyCalendarLineNotesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    companyCalendarLineNotesModel.SessionId = s.SessionId;
                    companyCalendarLineNotesModel.PrintUserIds = s.CompanyCalendarLineLinkPrintUser != null ? s.CompanyCalendarLineLinkPrintUser.Select(a => a.UserId).ToList() : new List<long?>();
                    companyCalendarLineNotesModel.PrintUsers = appUsers != null ? string.Join(",", appUsers.Where(w => companyCalendarLineNotesModel.PrintUserIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";

                    companyCalendarLineNotesModels.Add(companyCalendarLineNotesModel);
                });
            }
            return companyCalendarLineNotesModels;
        }
        [HttpGet]
        [Route("GetCompanyCalendarLineLinkAll")]
        public List<CompanyCalendarLineLinkModel> GetCompanyCalendarLineLinkAll(Guid? sessionId, long? id, int? userId)
        {
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            List<CompanyCalendarLineLinkModel> companyCalendarLineNotesModels = new List<CompanyCalendarLineLinkModel>();
            var companyCalendarLineNotes = _context.CompanyCalendarLineLink.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(a => a.CompanyCalendarLineLinkUser).Include(a => a.CompanyCalendarLineLinkPrintUser).Where(w => w.CompanyCalendarLineId == id && w.IsPrint == true).ToList();
            if (companyCalendarLineNotes != null)
            {
                companyCalendarLineNotes.ForEach(s =>
                {
                    CompanyCalendarLineLinkModel companyCalendarLineNotesModel = new CompanyCalendarLineLinkModel();
                    companyCalendarLineNotesModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                    companyCalendarLineNotesModel.UrlLink = s.UrlLink;
                    companyCalendarLineNotesModel.UrlText = s.UrlText;
                    companyCalendarLineNotesModel.Description = s.Description;
                    companyCalendarLineNotesModel.CompanyCalendarLineLinkId = s.CompanyCalendarLineLinkId;
                    companyCalendarLineNotesModel.RestrictUserIds = s.CompanyCalendarLineLinkUser != null ? s.CompanyCalendarLineLinkUser.Select(a => a.UserId).ToList() : new List<long?>();
                    companyCalendarLineNotesModel.RestrictUsers = appUsers != null ? string.Join(",", appUsers.Where(w => companyCalendarLineNotesModel.RestrictUserIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                    companyCalendarLineNotesModel.AddedByUserID = s.AddedByUserId;
                    companyCalendarLineNotesModel.AddedDate = s.AddedDate;
                    companyCalendarLineNotesModel.ModifiedDate = s.ModifiedDate;
                    companyCalendarLineNotesModel.ModifiedDate = s.ModifiedDate;
                    companyCalendarLineNotesModel.StatusCodeID = s.StatusCodeId;
                    companyCalendarLineNotesModel.IsPrint = s.IsPrint;
                    companyCalendarLineNotesModel.AddedByUser = s.AddedByUser?.UserName;
                    companyCalendarLineNotesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    companyCalendarLineNotesModel.Type = "Link";
                    companyCalendarLineNotesModel.PrintUserIds = s.CompanyCalendarLineLinkPrintUser != null ? s.CompanyCalendarLineLinkPrintUser.Select(a => a.UserId).ToList() : new List<long?>();
                    companyCalendarLineNotesModel.PrintUsers = appUsers != null ? string.Join(",", appUsers.Where(w => companyCalendarLineNotesModel.PrintUserIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";

                    companyCalendarLineNotesModels.Add(companyCalendarLineNotesModel);
                });
            }
            DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
            var documents = itemClassificationController.GetDocumentsBySessionID(sessionId, userId, "").Where(w => w.IsPrint == true).ToList();
            if (documents != null && documents.Count > 0)
            {
                documents.ForEach(s =>
                {
                    CompanyCalendarLineLinkModel companyCalendarLineNotesModel = new CompanyCalendarLineLinkModel();
                    companyCalendarLineNotesModel.CompanyCalendarLineLinkId = s.DocumentID;
                    companyCalendarLineNotesModel.CompanyCalendarLineId = id;
                    companyCalendarLineNotesModel.Type = "Document";
                    companyCalendarLineNotesModel.UrlText = s.FileName;
                    companyCalendarLineNotesModel.AddedByUser = s.AddedByUser;
                    companyCalendarLineNotesModel.AddedByUserID = s.AddedByUserID;
                    companyCalendarLineNotesModel.Description = s.Description;
                    companyCalendarLineNotesModels.Add(companyCalendarLineNotesModel);
                });
            }
            return companyCalendarLineNotesModels;
        }
        private List<long> GetNotesUserIds(long id)
        {
            List<long> userIds = new List<long>();
            var companycalendarLine = _context.CompanyCalendarLine.Include(a => a.CompanyCalendar).Include(s => s.CompanyCalendar.CompanyCalendarAssistance).Include(c => c.CompanyCalendarLineUser).SingleOrDefault(w => w.CompanyCalendarLineId == id);
            if (companycalendarLine != null)
            {
                if (companycalendarLine.CompanyCalendarLineUser != null)
                {
                    userIds.AddRange(companycalendarLine.CompanyCalendarLineUser.Select(s => s.UserId.GetValueOrDefault(0)).ToList());
                }
                if (companycalendarLine.CompanyCalendar != null)
                {
                    userIds.Add(companycalendarLine.CompanyCalendar.OwnerId.GetValueOrDefault(0));
                    if (companycalendarLine?.CompanyCalendar?.RequireAssistance == true)
                    {
                        userIds.AddRange(companycalendarLine.CompanyCalendar?.CompanyCalendarAssistance?.Select(s => s.UserId.GetValueOrDefault(0)).ToList());
                    }
                    userIds.AddRange(companycalendarLine.CompanyCalendar?.CompanyCalendarAssistance?.Select(s => s.UserId.GetValueOrDefault(0)).ToList());
                    userIds.Add(companycalendarLine.CompanyCalendar.AddedByUserId.GetValueOrDefault(0));
                    userIds.Add(companycalendarLine.CompanyCalendar.OnBehalfId.GetValueOrDefault(0));
                }
            }
            return userIds.Where(w => w > 0).Distinct().ToList();
        }
        [HttpGet]
        [Route("GetNotesUsers")]
        public List<ApplicationUserModel> GetNotesUsers(long id)
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            var userIds = GetNotesUserIds(id);
            if (userIds != null && userIds.Count > 0)
            {
                var applicationUsers = _context.Employee.Include(u => u.User).Include("Designation").Where(w => userIds.Distinct().ToList().Contains(w.UserId.Value)).AsNoTracking().ToList();
                applicationUsers.ForEach(s =>
                {
                    ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                    applicationUserModel.UserID = s.UserId.Value;
                    applicationUserModel.EmployeeID = s.EmployeeId;
                    applicationUserModel.UserName = s.FirstName;
                    applicationUserModel.DepartmentID = s.DepartmentId;
                    applicationUserModel.StatusCodeID = s.User?.StatusCodeId;
                    applicationUserModel.AddedByUserID = s.AddedByUserId;
                    applicationUserModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationUserModel.AddedDate = s.AddedDate;
                    applicationUserModel.ModifiedDate = s.ModifiedDate;
                    applicationUserModel.NickName = s.NickName;
                    applicationUserModel.FirstName = s.FirstName;
                    applicationUserModel.Designation = s.Designation != null ? s.Designation.Name : "No Designation";
                    applicationUserModel.Name = s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                    applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                    applicationUserModels.Add(applicationUserModel);
                });
            }
            return applicationUserModels;
        }
        [HttpGet]
        [Route("GetCalandarNotesUsers")]
        public List<CompanyCalendarModel> GetCalandarNotesUsers(long id)
        {
            List<CompanyCalendarModel> CompanyCalendarModels = new List<CompanyCalendarModel>();
            var calendarStatus = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMaster.ApplicationMasterCodeId == 315 && w.Value.ToLower() == "closed").FirstOrDefault()?.ApplicationMasterDetailId;
            //var CompanyCalendarLineNotesUser = _context.CompanyCalendarLineNotesUser.Where(w=>w.UserId==id).Select(s=>s.CompanyCalendarLineNotesId).Distinct().ToList();
            var companyCalendarLineNotesUser = _context.CompanyCalendarLineNotesUser.Include(a => a.CompanyCalendarLineNotes).Include(a => a.CompanyCalendarLineNotes.CompanyCalendarLine)
                .Include(a => a.CompanyCalendarLineNotes.CompanyCalendarLine.CompanyCalendar).Include(a => a.CompanyCalendarLineNotes.AddedByUser).Include(a => a.CompanyCalendarLineNotes.ModifiedByUser)
                .Where(w => w.UserId == id && w.CompanyCalendarLineNotes.StatusCodeId != 162 && w.CompanyCalendarLineNotes.CompanyCalendarLine.CalenderStatusId != calendarStatus).ToList();

            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            if (companyCalendarLineNotesUser != null && companyCalendarLineNotesUser.Count > 0)
            {
                var companyCalendarLineIds = companyCalendarLineNotesUser.Select(s => s.CompanyCalendarLineNotes?.CompanyCalendarLineId.GetValueOrDefault(0)).ToList();
                var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Where(w => companyCalendarLineIds.Contains(w.CompanyCalendarLineId) && w.AddedByUserId == id).ToList();
                companyCalendarLineNotesUser.ForEach(s =>
                {
                    var companyCalendarLineNotesisRead = companyCalendarLineNotes.FirstOrDefault(f => f.CompanyCalendarLineId == s.CompanyCalendarLineNotes?.CompanyCalendarLineId && f.AddedByUserId == id);
                    CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                    {
                        CompanyCalendarLineNotesId = s.CompanyCalendarLineNotesId,
                        CompanyCalendarIds = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.CompanyCalendarId,
                        CompanyCalendarLineId = s.CompanyCalendarLineNotes?.CompanyCalendarLineId,
                        CalendarEventDate = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.CompanyCalendar?.CalendarEventDate,
                        EventDate = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.EventDate,
                        Subject = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.Subject,
                        TypeOfServiceName = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendarLineNotes?.CompanyCalendarLine?.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "",
                        TypeOfEventName = s.CompanyCalendarLineNotes?.CompanyCalendarLine?.TypeOfEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendarLineNotes?.CompanyCalendarLine?.TypeOfEventId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUser = s.CompanyCalendarLineNotes?.AddedByUserId != null ? s.CompanyCalendarLineNotes?.AddedByUser.UserName : "",
                        ModifiedByUser = s.CompanyCalendarLineNotes?.ModifiedByUserId != null ? s.CompanyCalendarLineNotes?.ModifiedByUser.UserName : "",
                        AddedDate = s.CompanyCalendarLineNotes?.AddedDate,
                        ModifiedDate = s.CompanyCalendarLineNotes?.ModifiedDate,
                        Notes = s.CompanyCalendarLineNotes?.NotesDetails,
                        IsRead = companyCalendarLineNotesisRead != null ? true : false,
                    };
                    CompanyCalendarModels.Add(CompanyCalendarModel);
                });
            }
            return CompanyCalendarModels;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetCalandarNotes")]
        public List<CompanyCalendarLineNotesModel> GetCalandarNotes(long id, long userId)
        {
            List<CompanyCalendarLineNotesModel> companyCalendarLineNotesModels = new List<CompanyCalendarLineNotesModel>();
            var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Include(a => a.CompanyCalendarLineNotesUser).Include(a => a.AddedByUser).Where(t => t.CompanyCalendarLineId == id).AsNoTracking().OrderByDescending(o => o.CompanyCalendarLineNotesId).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            companyCalendarLineNotes.ForEach(s =>
            {
                CompanyCalendarLineNotesModel companyCalendarLineNotesModel = new CompanyCalendarLineNotesModel();
                companyCalendarLineNotesModel.UserIds = s.CompanyCalendarLineNotesUser != null ? s.CompanyCalendarLineNotesUser.Where(w => w.CompanyCalendarLineNotesId == s.CompanyCalendarLineNotesId).Select(a => a.UserId).ToList() : new List<long?>();
                if (s.AddedByUserId == userId || companyCalendarLineNotesModel.UserIds.Contains(userId))
                {
                    companyCalendarLineNotesModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                    companyCalendarLineNotesModel.CompanyCalendarLineNotesId = s.CompanyCalendarLineNotesId;
                    companyCalendarLineNotesModel.NotesDetails = s.NotesDetails;
                    companyCalendarLineNotesModel.AddedByUserId = s.AddedByUserId;
                    companyCalendarLineNotesModel.AddedDate = s.AddedDate;
                    companyCalendarLineNotesModel.AddedByUser = s.AddedByUser?.UserName;
                    companyCalendarLineNotesModel.CalendarNotesId = s.CalendarNotesId;
                    companyCalendarLineNotesModel.IsQuote = s.IsQuote;
                    companyCalendarLineNotesModel.IsShowInfoAllUser = s.IsShowInfoAllUser;
                    companyCalendarLineNotesModel.IsUrgent = s.IsUrgent;
                    companyCalendarLineNotesModel.UserId = s.UserId;
                    companyCalendarLineNotesModel.Users = appUsers != null ? string.Join(",", appUsers.Where(w => companyCalendarLineNotesModel.UserIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
                    if (s.IsShowInfoAllUser == true)
                    {
                        companyCalendarLineNotesModel.Users = "All in the calandar list";
                    }
                    companyCalendarLineNotesModels.Add(companyCalendarLineNotesModel);
                }
            });

            return companyCalendarLineNotesModels;
        }
        [HttpGet]
        [Route("GetCompanyCalendarDetails")]
        public List<CompanyCalendarLineModel> GetCompanyCalendarDetails()
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 278, 279, 280, 281, 282, 283 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            var locations = _context.Ictmaster.Where(i => i.MasterType == 572).ToList();
            var CompanyCalendarLine = _context.CompanyCalendarLine
                .Include(cc => cc.CompanyCalendar)
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Machine)
                .Include(p => p.Vendor).
                Include(x => x.CompanyCalendarLineLocation).
                Include(s => s.CompanyCalendar.Site)
                .Include(s => s.CompanyCalendar.Company)
                .Include(c => c.CalenderStatus)
                .Where(w => w.EventDate.Value.Date >= DateTime.Now.Date)
                .AsNoTracking().ToList();
            List<CompanyCalendarLineModel> CompanyCalendarLineModels = new List<CompanyCalendarLineModel>();
            CompanyCalendarLine.ForEach(s =>
            {
                CompanyCalendarLineModel companyCalendarLineModel = new CompanyCalendarLineModel();

                companyCalendarLineModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                companyCalendarLineModel.CompanyCalendarId = s.CompanyCalendarId;
                companyCalendarLineModel.EventDate = s.EventDate;
                companyCalendarLineModel.VendorId = s.VendorId;
                companyCalendarLineModel.TypeOfServiceId = s.TypeOfServiceId;
                companyCalendarLineModel.MachineId = s.MachineId;
                companyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                companyCalendarLineModel.Notes = s.Notes;
                companyCalendarLineModel.StatusCodeID = s.StatusCodeId;
                companyCalendarLineModel.AddedByUserID = s.AddedByUserId;
                companyCalendarLineModel.ModifiedByUserID = s.ModifiedByUserId;
                companyCalendarLineModel.AddedDate = s.AddedDate;
                companyCalendarLineModel.ModifiedDate = s.ModifiedDate;
                companyCalendarLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                companyCalendarLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                companyCalendarLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                companyCalendarLineModel.MachineName = s.Machine != null ? s.Machine.NameOfTheMachine : "";
                companyCalendarLineModel.VendorName = s.Vendor != null ? s.Vendor.VendorName : "";
                companyCalendarLineModel.LocationIds = s.CompanyCalendarLineLocation != null ? s.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                companyCalendarLineModel.CalendarEventDate = s.CompanyCalendar != null ? s.CompanyCalendar.CalendarEventDate : null;
                companyCalendarLineModel.CalenderEventId = s.CompanyCalendar != null ? s.CompanyCalendar.CalenderEventId : null;
                companyCalendarLineModel.SessionId = s.CompanyCalendar != null ? s.CompanyCalendar.SessionId : null;
                companyCalendarLineModel.CalenderEventName = s.CompanyCalendar != null && s.CompanyCalendar.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendar.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "";
                companyCalendarLineModel.SiteName = s.CompanyCalendar != null && s.CompanyCalendar.Site != null ? s.CompanyCalendar.Site.Name : "";
                companyCalendarLineModel.CompanyName = s.CompanyCalendar != null && s.CompanyCalendar.Company != null ? s.CompanyCalendar.Company.Description : "";
                companyCalendarLineModel.CompanyId = s.CompanyCalendar != null ? s.CompanyCalendar.CompanyId : null;
                companyCalendarLineModel.SiteId = s.CompanyCalendar != null ? s.CompanyCalendar.SiteId : null;
                companyCalendarLineModel.LinkDocComment = s.LinkDocComment;
                companyCalendarLineModel.Subject = s.Subject;
                companyCalendarLineModel.CalenderStatusId = s.CalenderStatusId;
                companyCalendarLineModel.CalenderStatus = s.CalenderStatus?.Value;
                if (companyCalendarLineModel.CalenderEventName.ToLower().Trim() == "yearly holiday")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "green";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.IsYearly = true;
                }
                if (companyCalendarLineModel.CalenderEventName.ToLower().Trim() == "external service")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "blue";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.VendorId = s.VendorId;
                    companyCalendarLineModel.VendorName = s.Vendor?.VendorName;
                    companyCalendarLineModel.IsService = true;
                }
                if (companyCalendarLineModel.CalenderEventName.ToLower().Trim() == "in house service machine related")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "yellow";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.MachineId = s.MachineId;
                    companyCalendarLineModel.MachineName = s.Machine?.NameOfTheMachine;
                    companyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                    companyCalendarLineModel.IsMachine = true;
                }
                if (companyCalendarLineModel.CalenderEventName.ToLower().Trim() == "in house service location related")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "grey";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.LocationIds = s.CompanyCalendarLineLocation != null ? s.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                    companyCalendarLineModel.LocationName = locations.Count > 0 ? string.Join(',', locations.Where(l => companyCalendarLineModel.LocationIds.Contains(l.IctmasterId)).Select(c => c.Name)) : "";
                    companyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                    companyCalendarLineModel.IsLocation = true;
                }
                if (companyCalendarLineModel.LinkDocComment != "" && companyCalendarLineModel.LinkDocComment != null)
                {
                    companyCalendarLineModel.IsLinkDocument = true;
                }
                CompanyCalendarLineModels.Add(companyCalendarLineModel);
            });
            return CompanyCalendarLineModels.OrderBy(a => a.EventDate).ToList();
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLine")]
        public CompanyCalendarLineModel Post(CompanyCalendarLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var CompanyCalendarLine = new CompanyCalendarLine
            {
                CompanyCalendarId = value.CompanyCalendarId,
                EventDate = value.EventDate,
                VendorId = value.VendorId,
                TypeOfServiceId = value.TypeOfServiceId,
                MachineId = value.MachineId,
                NoOfHoursRequire = value.NoOfHoursRequire,
                Notes = value.Notes,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                AllEventFlag = value.AllEventFlag,
                FromTime = value.FromTime,
                ToTime = value.ToTime,
                LinkDocComment = value.LinkDocComment,
                DueDate = value.DueDate,
                TypeOfEventId = value.TypeOfEventId,
                Subject = value.Subject,
                SessionId = sessionId,
                CalenderStatusId = value.CalenderStatusId,
                Tentative = value.Tentative,
            };
            _context.CompanyCalendarLine.Add(CompanyCalendarLine);
            _context.SaveChanges();
            if (value.LocationIds != null && value.LocationIds.Count > 0)
            {
                value.LocationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineLocation
                    {
                        LocationId = h,
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineLocation.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.ParticipantsIds != null && value.ParticipantsIds.Count > 0)
            {
                value.ParticipantsIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Participants",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.InformationIds != null && value.InformationIds.Count > 0)
            {
                value.InformationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Information",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.TypeOfServiceId).ToList();
            value.CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId;
            value.SessionId = CompanyCalendarLine.SessionId;
            value.TypeOfServiceName = value.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";

            return value;
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLineLinkCalandar")]
        public CompanyCalendarLineModel InsertCompanyCalendarLineLinkCalandar(CompanyCalendarLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var CompanyCalendarLine = new CompanyCalendarLine
            {
                CompanyCalendarId = value.CompanyCalendarId,
                EventDate = value.EventDate,
                VendorId = value.VendorId,
                TypeOfServiceId = value.TypeOfServiceId,
                MachineId = value.MachineId,
                NoOfHoursRequire = value.NoOfHoursRequire,
                Notes = value.Notes,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                AllEventFlag = value.AllEventFlag,
                FromTime = value.FromTime,
                ToTime = value.ToTime,
                LinkDocComment = value.LinkDocComment,
                DueDate = value.DueDate,
                TypeOfEventId = value.TypeOfEventId,
                Subject = value.Subject,
                SessionId = sessionId,
                CalenderStatusId = value.CalenderStatusId,
                Tentative = value.Tentative,
                CalanderParentId = value.CalanderParentId,
            };
            _context.CompanyCalendarLine.Add(CompanyCalendarLine);
            _context.SaveChanges();
            if (value.LocationIds != null && value.LocationIds.Count > 0)
            {
                value.LocationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineLocation
                    {
                        LocationId = h,
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineLocation.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.ParticipantsIds != null && value.ParticipantsIds.Count > 0)
            {
                value.ParticipantsIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Participants",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.InformationIds != null && value.InformationIds.Count > 0)
            {
                value.InformationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Information",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
            #region CompanyCalendarLineMeetingNotes
            var companyCalendarLineMeetingNotes = _context.CompanyCalendarLineMeetingNotes.Where(w => w.CompanyCalendarLineId == value.CalanderParentId).ToList();
            if (companyCalendarLineMeetingNotes != null && companyCalendarLineMeetingNotes.Count > 0)
            {
                companyCalendarLineMeetingNotes.ForEach(a =>
                {
                    var companyCalendarLineMeetingNotes = new CompanyCalendarLineMeetingNotes
                    {
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        MeetingData = a.MeetingData,
                        StatusCodeId = a.StatusCodeId,
                        IsTitleFlag = a.IsTitleFlag,
                    };
                    _context.CompanyCalendarLineMeetingNotes.Add(companyCalendarLineMeetingNotes);
                });
            }
            #endregion
            #region CompanyCalendarLineLink
            var companyCalendarLineLinks = _context.CompanyCalendarLineLink.Include(x => x.CompanyCalendarLineLinkUser).Include(c => c.CompanyCalendarLineLinkPrintUser).Where(a => a.CompanyCalendarLineId == value.CalanderParentId).ToList();
            var companyCalendarLineLinkUserLinkAll = _context.CompanyCalendarLineLinkUserLink.ToList();
            if (companyCalendarLineLinks != null && companyCalendarLineLinks.Count > 0)
            {
                companyCalendarLineLinks.ForEach(a =>
                {
                    var companyCalendarLineLink = new CompanyCalendarLineLink
                    {
                        UrlLink = a.UrlLink,
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                        UrlText = a.UrlText,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = a.StatusCodeId,
                        IsPrint = a.IsPrint,
                        SessionId = Guid.NewGuid(),
                    };
                    _context.CompanyCalendarLineLink.Add(companyCalendarLineLink);
                    _context.SaveChanges();
                    if (a.CompanyCalendarLineLinkUser != null && a.CompanyCalendarLineLinkUser.Count > 0)
                    {
                        a.CompanyCalendarLineLinkUser.ToList().ForEach(h =>
                        {
                            var CompanyCalendarLineLinkUser = new CompanyCalendarLineLinkUser
                            {
                                UserId = h.UserId,
                                CompanyCalendarLineLinkId = companyCalendarLineLink.CompanyCalendarLineLinkId,
                            };
                            _context.CompanyCalendarLineLinkUser.Add(CompanyCalendarLineLinkUser);
                        });
                        _context.SaveChanges();
                    }
                    if (a.CompanyCalendarLineLinkPrintUser != null && a.CompanyCalendarLineLinkPrintUser.Count > 0)
                    {
                        a.CompanyCalendarLineLinkPrintUser.ToList().ForEach(p =>
                        {
                            var companyCalendarLineLinkPrintUser = new CompanyCalendarLineLinkPrintUser
                            {
                                CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                                CompanyCalendarLineLinkId = companyCalendarLineLink.CompanyCalendarLineLinkId,
                                UserId = p.UserId,
                                DocumentId = p.DocumentId,
                            };
                            _context.CompanyCalendarLineLinkPrintUser.Add(companyCalendarLineLinkPrintUser);
                            _context.SaveChanges();
                            var companyCalendarLineLinkUserLink = companyCalendarLineLinkUserLinkAll.Where(w => w.CompanyCalendarLineLinkUserId == p.CompanyCalendarLineLinkUserId).ToList();
                            if (companyCalendarLineLinkUserLink != null && companyCalendarLineLinkUserLink.Count > 0)
                            {
                                companyCalendarLineLinkUserLink.ForEach(x =>
                                {
                                    var insertcompanyCalendarLineLinkUserLink = new CompanyCalendarLineLinkUserLink
                                    {
                                        UserId = x.UserId,
                                        StatusCodeId = x.StatusCodeId,
                                        AddedByUserId = value.AddedByUserID,
                                        AddedDate = DateTime.Now,
                                        CompanyCalendarLineLinkUserId = companyCalendarLineLinkPrintUser.CompanyCalendarLineLinkUserId,
                                    };
                                    _context.CompanyCalendarLineLinkUserLink.Add(insertcompanyCalendarLineLinkUserLink);
                                });
                                _context.SaveChanges();
                            }
                        });

                    }
                });
            }
            #endregion
            #region CompanyCalendarLineLinkPrintUser With DocumentId
            var companyCalendarLineLinkPrintUserDoc = _context.CompanyCalendarLineLinkPrintUser.Where(w => w.CompanyCalendarLineId == value.CalanderParentId && w.CompanyCalendarLineLinkId == null).ToList();
            if (companyCalendarLineLinkPrintUserDoc != null && companyCalendarLineLinkPrintUserDoc.Count > 0)
            {
                companyCalendarLineLinkPrintUserDoc.ToList().ForEach(p =>
                {
                    var companyCalendarLineLinkPrintUserDoc = new CompanyCalendarLineLinkPrintUser
                    {
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                        CompanyCalendarLineLinkId = p.CompanyCalendarLineLinkId,
                        UserId = p.UserId,
                        DocumentId = p.DocumentId,
                    };
                    _context.CompanyCalendarLineLinkPrintUser.Add(companyCalendarLineLinkPrintUserDoc);
                    _context.SaveChanges();
                    var companyCalendarLineLinkUserLink = companyCalendarLineLinkUserLinkAll.Where(w => w.CompanyCalendarLineLinkUserId == p.CompanyCalendarLineLinkUserId).ToList();
                    if (companyCalendarLineLinkUserLink != null && companyCalendarLineLinkUserLink.Count > 0)
                    {
                        companyCalendarLineLinkUserLink.ForEach(x =>
                        {
                            var insertcompanyCalendarLineLinkUserLinkDoc = new CompanyCalendarLineLinkUserLink
                            {
                                UserId = x.UserId,
                                StatusCodeId = x.StatusCodeId,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                CompanyCalendarLineLinkUserId = companyCalendarLineLinkPrintUserDoc.CompanyCalendarLineLinkUserId,
                            };
                            _context.CompanyCalendarLineLinkUserLink.Add(insertcompanyCalendarLineLinkUserLinkDoc);
                        });
                        _context.SaveChanges();
                    }
                });

            }
            #endregion
            #region CompanyCalendarLineNotes
            var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Include(a => a.CompanyCalendarLineNotesUser).Where(w => w.CompanyCalendarLineId == value.CalanderParentId).ToList();
            if (companyCalendarLineNotes != null && companyCalendarLineNotes.Count > 0)
            {
                var companyCalendarLineNote = companyCalendarLineNotes.Where(w => w.CalendarNotesId == null).ToList();
                if (companyCalendarLineNote != null && companyCalendarLineNote.Count > 0)
                {
                    companyCalendarLineNote.ForEach(c =>
                    {
                        var CompanyCalendarLineNotes = new CompanyCalendarLineNotes
                        {
                            NotesDetails = c.NotesDetails,
                            CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            CalendarNotesId = c.CalendarNotesId,
                            IsUrgent = c.IsUrgent,
                            IsQuote = c.IsQuote,
                            IsShowInfoAllUser = c.IsShowInfoAllUser,
                            UserId = c.UserId,
                            StatusCodeId = c.StatusCodeId,
                        };
                        if (c.CompanyCalendarLineNotesUser != null && c.CompanyCalendarLineNotesUser.Count > 0)
                        {
                            c.CompanyCalendarLineNotesUser.ToList().ForEach(a =>
                            {
                                var companyCalendarLineNotesUser = new CompanyCalendarLineNotesUser
                                {
                                    UserId = a.UserId,
                                    StatusCodeId = a.StatusCodeId,
                                };
                                CompanyCalendarLineNotes.CompanyCalendarLineNotesUser.Add(companyCalendarLineNotesUser);
                            });
                        }
                        _context.CompanyCalendarLineNotes.Add(CompanyCalendarLineNotes);
                        _context.SaveChanges();
                        var calendarNotesChild = companyCalendarLineNotes.Where(w => w.CalendarNotesId == c.CompanyCalendarLineNotesId).ToList();
                        if (calendarNotesChild != null && calendarNotesChild.Count > 0)
                        {
                            calendarNotesChild.ForEach(n =>
                            {
                                var CompanyCalendarLineNotesChild = new CompanyCalendarLineNotes
                                {
                                    NotesDetails = n.NotesDetails,
                                    CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    CalendarNotesId = CompanyCalendarLineNotes.CompanyCalendarLineNotesId,
                                    IsUrgent = n.IsUrgent,
                                    IsQuote = n.IsQuote,
                                    IsShowInfoAllUser = n.IsShowInfoAllUser,
                                    UserId = n.UserId,
                                    StatusCodeId = n.StatusCodeId,
                                };
                                if (n.CompanyCalendarLineNotesUser != null && n.CompanyCalendarLineNotesUser.Count > 0)
                                {
                                    n.CompanyCalendarLineNotesUser.ToList().ForEach(a =>
                                    {
                                        var companyCalendarLineNotesUserChild = new CompanyCalendarLineNotesUser
                                        {
                                            UserId = a.UserId,
                                            StatusCodeId = a.StatusCodeId,
                                        };
                                        CompanyCalendarLineNotesChild.CompanyCalendarLineNotesUser.Add(companyCalendarLineNotesUserChild);
                                    });
                                }
                                _context.CompanyCalendarLineNotes.Add(CompanyCalendarLineNotesChild);
                                _context.SaveChanges();
                            });
                        }
                    });
                }

            }
            #endregion
            _context.SaveChanges();
            AddDocumentInCalandarLinkDoc(value, sessionId);
            var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.TypeOfServiceId).ToList();
            value.CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId;
            value.SessionId = CompanyCalendarLine.SessionId;
            value.TypeOfServiceName = value.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";

            return value;
        }
        private void AddDocumentInCalandarLinkDoc(CompanyCalendarLineModel value, Guid sessionIds)
        {
            var docments = _context.Documents.Where(w => w.SessionId == value.SessionId).ToList();
            var linkFileProfileTypeDocument = _context.LinkFileProfileTypeDocument.Where(w => w.TransactionSessionId == value.SessionId).ToList();
            if (linkFileProfileTypeDocument != null && linkFileProfileTypeDocument.Count > 0)
            {
                linkFileProfileTypeDocument.ForEach(c =>
                {
                    var insertlinkFileProfileTypeDocument = new LinkFileProfileTypeDocument
                    {
                        TransactionSessionId = sessionIds,
                        DocumentId = c.DocumentId,
                        FileProfileTypeId = c.FileProfileTypeId,
                        FolderId = c.FolderId,
                        AddedDate = c.AddedDate,
                        AddedByUserId = c.AddedByUserId,
                        Description = c.Description,
                        IsWikiDraft = c.IsWikiDraft,
                        IsWiki = c.IsWiki,
                    };
                    _context.LinkFileProfileTypeDocument.Add(insertlinkFileProfileTypeDocument);
                    _context.SaveChanges();
                });
            }
            if (docments != null && docments.Count > 0)
            {
                var docu = docments.Where(w => w.DocumentParentId == null).ToList();
                if (docu != null && docu.Count > 0)
                {
                    docu.ForEach(d =>
                    {
                        var documents = new Documents
                        {
                            FileName = d.FileName,
                            DisplayName = d.DisplayName,
                            Extension = d.Extension,
                            ContentType = d.ContentType,
                            DocumentType = d.DocumentType,
                            FileData = d.FileData,
                            FileSize = d.FileSize,
                            UploadDate = d.UploadDate,
                            SessionId = sessionIds,
                            LinkId = d.LinkId,
                            IsSpecialFile = d.IsSpecialFile,
                            IsTemp = d.IsTemp,
                            DepartmentId = d.DepartmentId,
                            WikiId = d.WikiId,
                            CategoryId = d.CategoryId,
                            StatusCodeId = d.StatusCodeId,
                            ReferenceNumber = d.ReferenceNumber,
                            Description = d.Description,
                            AddedByUserId = d.AddedByUserId,
                            AddedDate = d.AddedDate,
                            ModifiedByUserId = d.ModifiedByUserId,
                            ModifiedDate = d.ModifiedDate,
                            FilterProfileTypeId = d.FilterProfileTypeId,
                            ProfileNo = d.ProfileNo,
                            TableName = d.TableName,
                            DocumentParentId = d.DocumentParentId,
                            ScreenId = d.ScreenId,
                            IsLatest = d.IsLatest,
                            IsLocked = d.IsLocked,
                            LockedDate = d.LockedDate,
                            LockedByUserId = d.LockedByUserId,
                            IsWikiDraft = d.IsWikiDraft,
                            IsWikiDraftDelete = d.IsWikiDraftDelete,
                            IsMobileUpload = d.IsMobileUpload,
                            IsCompressed = d.IsCompressed,
                            IsHeaderImage = d.IsHeaderImage,
                            FileIndex = d.FileIndex,
                            IsVideoFile = d.IsVideoFile,
                            CloseDocumentId = d.CloseDocumentId,
                            ArchiveStatusId = d.ArchiveStatusId,
                            IsPublichFolder = d.IsPublichFolder,
                            FolderId = d.FolderId,
                            TaskId = d.TaskId,
                            IsMainTask = d.IsMainTask,
                            IsPrint = d.IsPrint,
                            IsWiki = d.IsWiki,
                            ExpiryDate = d.ExpiryDate,
                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                        var docuChild = docments.Where(w => w.DocumentParentId == d.DocumentId).ToList();
                        if (docuChild != null && docuChild.Count > 0)
                        {
                            docuChild.ForEach(q =>
                            {
                                var document = new Documents
                                {
                                    FileName = q.FileName,
                                    DisplayName = q.DisplayName,
                                    Extension = q.Extension,
                                    ContentType = q.ContentType,
                                    DocumentType = q.DocumentType,
                                    FileData = q.FileData,
                                    FileSize = q.FileSize,
                                    UploadDate = q.UploadDate,
                                    SessionId = sessionIds,
                                    LinkId = q.LinkId,
                                    IsSpecialFile = q.IsSpecialFile,
                                    IsTemp = q.IsTemp,
                                    DepartmentId = q.DepartmentId,
                                    WikiId = q.WikiId,
                                    CategoryId = q.CategoryId,
                                    StatusCodeId = q.StatusCodeId,
                                    ReferenceNumber = q.ReferenceNumber,
                                    Description = q.Description,
                                    AddedByUserId = q.AddedByUserId,
                                    AddedDate = q.AddedDate,
                                    ModifiedByUserId = q.ModifiedByUserId,
                                    ModifiedDate = q.ModifiedDate,
                                    FilterProfileTypeId = q.FilterProfileTypeId,
                                    ProfileNo = q.ProfileNo,
                                    TableName = q.TableName,
                                    DocumentParentId = documents.DocumentId,
                                    ScreenId = q.ScreenId,
                                    IsLatest = q.IsLatest,
                                    IsLocked = q.IsLocked,
                                    LockedDate = q.LockedDate,
                                    LockedByUserId = q.LockedByUserId,
                                    IsWikiDraft = q.IsWikiDraft,
                                    IsWikiDraftDelete = q.IsWikiDraftDelete,
                                    IsMobileUpload = q.IsMobileUpload,
                                    IsCompressed = q.IsCompressed,
                                    IsHeaderImage = q.IsHeaderImage,
                                    FileIndex = q.FileIndex,
                                    IsVideoFile = q.IsVideoFile,
                                    CloseDocumentId = q.CloseDocumentId,
                                    ArchiveStatusId = q.ArchiveStatusId,
                                    IsPublichFolder = q.IsPublichFolder,
                                    FolderId = q.FolderId,
                                    TaskId = q.TaskId,
                                    IsMainTask = q.IsMainTask,
                                    IsPrint = q.IsPrint,
                                    IsWiki = q.IsWiki,
                                    ExpiryDate = q.ExpiryDate,
                                };
                                _context.Documents.Add(document);
                                _context.SaveChanges();
                            });
                        }
                    });
                }
            }
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLineNotes")]
        public CompanyCalendarLineNotesModel InsertCompanyCalendarLineNotes(CompanyCalendarLineNotesModel value)
        {
            var CompanyCalendarLineNotes = new CompanyCalendarLineNotes
            {
                NotesDetails = value.NotesDetails,
                CompanyCalendarLineId = value.CompanyCalendarLineId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                CalendarNotesId = value.CalendarNotesId,
                IsUrgent = value.IsUrgent,
                IsQuote = value.IsQuote,
                IsShowInfoAllUser = value.IsShowInfoAllUser,
                UserId = value.UserId,
            };
            _context.CompanyCalendarLineNotes.Add(CompanyCalendarLineNotes);
            _context.SaveChanges();
            if (value.IsShowInfoAllUser == false)
            {
                if (value.UserIds != null && value.UserIds.Count > 0)
                {
                    value.UserIds.ForEach(h =>
                    {
                        var companyCalendarLineNotesUser = new CompanyCalendarLineNotesUser
                        {
                            UserId = h,
                            CompanyCalendarLineNotesId = CompanyCalendarLineNotes.CompanyCalendarLineNotesId,
                        };
                        _context.CompanyCalendarLineNotesUser.Add(companyCalendarLineNotesUser);
                    });
                    _context.SaveChanges();
                }
            }
            else
            {
                var userIds = GetNotesUserIds(CompanyCalendarLineNotes.CompanyCalendarLineId.Value);
                if (userIds.Count > 0)
                {
                    userIds.ForEach(h =>
                    {
                        var companyCalendarLineNotesUser = new CompanyCalendarLineNotesUser
                        {
                            UserId = h,
                            CompanyCalendarLineNotesId = CompanyCalendarLineNotes.CompanyCalendarLineNotesId,
                        };
                        _context.CompanyCalendarLineNotesUser.Add(companyCalendarLineNotesUser);
                    });
                    _context.SaveChanges();
                    value.UserIds = new List<long?>();
                }
            }
            return value;
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLineLink")]
        public CompanyCalendarLineLinkModel InsertCompanyCalendarLineLink(CompanyCalendarLineLinkModel value)
        {
            var sessionId = Guid.NewGuid();
            var companyCalendarLineLink = new CompanyCalendarLineLink
            {
                UrlLink = value.UrlLink,
                CompanyCalendarLineId = value.CompanyCalendarLineId,
                UrlText = value.UrlText,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                IsPrint = value.IsPrint,
                SessionId = sessionId,
            };
            _context.CompanyCalendarLineLink.Add(companyCalendarLineLink);
            _context.SaveChanges();
            if (value.RestrictUserIds != null && value.RestrictUserIds.Count > 0)
            {
                value.RestrictUserIds.ForEach(h =>
                {
                    var CompanyCalendarLineLinkUser = new CompanyCalendarLineLinkUser
                    {
                        UserId = h,
                        CompanyCalendarLineLinkId = companyCalendarLineLink.CompanyCalendarLineLinkId,
                    };
                    _context.CompanyCalendarLineLinkUser.Add(CompanyCalendarLineLinkUser);
                });
                _context.SaveChanges();
            }
            value.CompanyCalendarLineLinkId = companyCalendarLineLink.CompanyCalendarLineLinkId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLineLinkUserLinkByUserStatus")]
        public DocumentsModel CompanyCalendarLineLinkUserLinkByUserStatus(DocumentsModel value)
        {
            var companyCalendarLineLinkUserLink = _context.CompanyCalendarLineLinkUserLink.SingleOrDefault(w => w.CompanyCalendarLineLinkUserLinkId == value.CompanyCalendarLineLinkUserLinkId);
            companyCalendarLineLinkUserLink.StatusCodeId = 162;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLineCloseNotes")]
        public CompanyCalendarModel UpdateCompanyCalendarLineCloseNotes(CompanyCalendarModel value)
        {
            var companyCalendarLineLinkUserLink = _context.CompanyCalendarLineNotes.SingleOrDefault(w => w.CompanyCalendarLineNotesId == value.CompanyCalendarLineNotesId);
            companyCalendarLineLinkUserLink.StatusCodeId = 162;
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("CompanyCalendarLineLinkUserLinkByUser")]
        public List<DocumentsModel> CompanyCalendarLineLinkUserLinkByUser(long? id)
        {
            List<DocumentsModel> companyCalendarLineLinkModels = new List<DocumentsModel>();
            var companyCalendarLineLinkUserLink = _context.CompanyCalendarLineLinkUserLink.Include(a => a.CompanyCalendarLineLinkUser).Include(a => a.CompanyCalendarLineLinkUser.CompanyCalendarLineLink).Include(a => a.CompanyCalendarLineLinkUser.CompanyCalendarLine).Where(w => w.UserId == id && w.StatusCodeId != 162).ToList();
            var masterDetailList = _context.ApplicationMasterDetail.Select(s => new { s.Value, s.ApplicationMasterDetailId }).ToList();
            if (companyCalendarLineLinkUserLink != null && companyCalendarLineLinkUserLink.Count > 0)
            {
                companyCalendarLineLinkUserLink.ForEach(s =>
                {
                    DocumentsModel companyCalendarLineLinkModel = new DocumentsModel();
                    companyCalendarLineLinkModel.SubjectName = s.CompanyCalendarLineLinkUser?.CompanyCalendarLine?.Subject;
                    companyCalendarLineLinkModel.TypeOfEventName = masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendarLineLinkUser?.CompanyCalendarLine?.TypeOfEventId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineLinkModel.CalenderStatusName = masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendarLineLinkUser?.CompanyCalendarLine?.CalenderStatusId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineLinkModel.TypeOfServiceName = masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendarLineLinkUser?.CompanyCalendarLine?.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineLinkModel.StatusCodeID = s.StatusCodeId;
                    companyCalendarLineLinkModel.AddedByUserID = s.AddedByUserId;
                    companyCalendarLineLinkModel.ModifiedByUserID = s.ModifiedByUserId;
                    companyCalendarLineLinkModel.CompanyCalendarLineLinkUserLinkId = s.CompanyCalendarLineLinkUserLinkId;
                    if (s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink != null)
                    {
                        if (s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink?.IsPrint == true)
                        {
                            companyCalendarLineLinkModel.CompanyCalendarLineId = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineId;
                            companyCalendarLineLinkModel.FileProfileTypeParentId = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLinkId;
                            companyCalendarLineLinkModel.FileName = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink?.UrlLink;
                            companyCalendarLineLinkModel.ContentType = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink?.UrlText;
                            companyCalendarLineLinkModel.Description = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink?.Description;
                            companyCalendarLineLinkModel.Type = "Link";
                            companyCalendarLineLinkModel.DocumentName = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineLink?.UrlText;
                        }
                    }
                    if (s.CompanyCalendarLineLinkUser != null && s.CompanyCalendarLineLinkUser.DocumentId != null)
                    {
                        var docu = _context.Documents.Select(d => new { d.DocumentId, d.FileName, d.IsPrint, d.ContentType, d.FileSize, d.Description }).FirstOrDefault(f => f.DocumentId == s.CompanyCalendarLineLinkUser.DocumentId);
                        if (docu != null)
                        {
                            companyCalendarLineLinkModel.CompanyCalendarLineId = s.CompanyCalendarLineLinkUser?.CompanyCalendarLineId;
                            companyCalendarLineLinkModel.DocumentID = docu.DocumentId;
                            companyCalendarLineLinkModel.FileName = docu?.FileName;
                            companyCalendarLineLinkModel.DocumentName = docu?.FileName;
                            companyCalendarLineLinkModel.ContentType = docu?.ContentType;
                            companyCalendarLineLinkModel.FileSize = docu?.FileSize;
                            companyCalendarLineLinkModel.Type = "Document";
                            companyCalendarLineLinkModel.Description = docu?.Description;
                            companyCalendarLineLinkModel.DocumentName = docu?.FileName;
                        }
                    }
                    companyCalendarLineLinkModels.Add(companyCalendarLineLinkModel);
                });
            }
            return companyCalendarLineLinkModels;
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLineLinkPrint")]
        public CompanyCalendarLineLinkModel InsertCompanyCalendarLineLinkPrint(CompanyCalendarLineLinkModel value)
        {
            var companyCalendarLineLinkAll = GetCompanyCalendarLineLinkAll(value.SessionId, value.CompanyCalendarLineId, (int)value.AddedByUserID);
            var CompanyCalendarLineLinkUserRemove = _context.CompanyCalendarLineLinkPrintUser.Where(w => w.CompanyCalendarLineId == value.CompanyCalendarLineId).ToList();
            if (CompanyCalendarLineLinkUserRemove != null)
            {
                CompanyCalendarLineLinkUserRemove.ForEach(a =>
                {
                    var CompanyCalendarLineLinkUserRemove = _context.CompanyCalendarLineLinkUserLink.Where(w => w.CompanyCalendarLineLinkUserId == a.CompanyCalendarLineLinkUserId && w.StatusCodeId != 162).ToList();
                    if (CompanyCalendarLineLinkUserRemove != null)
                    {
                        _context.CompanyCalendarLineLinkUserLink.RemoveRange(CompanyCalendarLineLinkUserRemove);
                        _context.SaveChanges();
                    }
                });
                _context.CompanyCalendarLineLinkPrintUser.RemoveRange(CompanyCalendarLineLinkUserRemove);
                _context.SaveChanges();
            }
            companyCalendarLineLinkAll.ForEach(s =>
            {
                var CompanyCalendarLineLinkUser = new CompanyCalendarLineLinkPrintUser();

                CompanyCalendarLineLinkUser.CompanyCalendarLineId = s.CompanyCalendarLineId;
                if (s.Type == "Document")
                {
                    CompanyCalendarLineLinkUser.DocumentId = s.CompanyCalendarLineLinkId;
                }
                if (s.Type == "Link")
                {
                    CompanyCalendarLineLinkUser.CompanyCalendarLineLinkId = s.CompanyCalendarLineLinkId;
                }
                _context.CompanyCalendarLineLinkPrintUser.Add(CompanyCalendarLineLinkUser);
                _context.SaveChanges();
                if (value.PrintUserIds != null && value.PrintUserIds.Count > 0)
                {
                    value.PrintUserIds.ForEach(h =>
                    {
                        //var exits = _context.CompanyCalendarLineLinkUserLink.SingleOrDefault(w=>w.CompanyCalendarLineLinkUserId== CompanyCalendarLineLinkUser.CompanyCalendarLineLinkUserId && w.UserId==h);
                        // if (exits == null)
                        // {
                        var CompanyCalendarLineLinkUserLink = new CompanyCalendarLineLinkUserLink
                        {
                            UserId = h,
                            CompanyCalendarLineLinkUserId = CompanyCalendarLineLinkUser.CompanyCalendarLineLinkUserId,
                        };
                        _context.CompanyCalendarLineLinkUserLink.Add(CompanyCalendarLineLinkUserLink);
                        //}
                    });
                    _context.SaveChanges();
                }
            });
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLineNotes")]
        public CompanyCalendarLineNotesModel UpdateCompanyCalendarLineNotes(CompanyCalendarLineNotesModel value)
        {
            var CompanyCalendarLineNotes = _context.CompanyCalendarLineNotes.SingleOrDefault(p => p.CompanyCalendarLineNotesId == value.CompanyCalendarLineNotesId);
            CompanyCalendarLineNotes.NotesDetails = value.NotesDetails;
            CompanyCalendarLineNotes.CompanyCalendarLineId = value.CompanyCalendarLineId;
            CompanyCalendarLineNotes.ModifiedByUserId = value.ModifiedByUserID;
            CompanyCalendarLineNotes.ModifiedDate = DateTime.Now;
            CompanyCalendarLineNotes.CalendarNotesId = value.CalendarNotesId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLineLinks")]
        public CompanyCalendarLineLinkModel UpdateCompanyCalendarLineLinks(CompanyCalendarLineLinkModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var companyCalendarLineLink = _context.CompanyCalendarLineLink.SingleOrDefault(p => p.CompanyCalendarLineLinkId == value.CompanyCalendarLineLinkId);
            companyCalendarLineLink.UrlText = value.UrlText;
            companyCalendarLineLink.CompanyCalendarLineId = value.CompanyCalendarLineId;
            companyCalendarLineLink.UrlLink = value.UrlLink;
            companyCalendarLineLink.ModifiedByUserId = value.AddedByUserID;
            companyCalendarLineLink.ModifiedDate = DateTime.Now;
            companyCalendarLineLink.StatusCodeId = value.StatusCodeID;
            companyCalendarLineLink.IsPrint = value.IsPrint;
            companyCalendarLineLink.SessionId = value.SessionId;
            _context.SaveChanges();
            var CompanyCalendarLineLinkUserRemove = _context.CompanyCalendarLineLinkUser.Where(w => w.CompanyCalendarLineLinkId == value.CompanyCalendarLineLinkId).ToList();
            if (CompanyCalendarLineLinkUserRemove != null)
            {
                _context.CompanyCalendarLineLinkUser.RemoveRange(CompanyCalendarLineLinkUserRemove);
                _context.SaveChanges();
            }
            if (value.RestrictUserIds != null && value.RestrictUserIds.Count > 0)
            {
                value.RestrictUserIds.ForEach(h =>
                {
                    var CompanyCalendarLineLinkUser = new CompanyCalendarLineLinkUser
                    {
                        UserId = h,
                        CompanyCalendarLineLinkId = companyCalendarLineLink.CompanyCalendarLineLinkId,
                    };
                    _context.CompanyCalendarLineLinkUser.Add(CompanyCalendarLineLinkUser);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLine")]
        public CompanyCalendarLineModel Put(CompanyCalendarLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var CompanyCalendarLine = _context.CompanyCalendarLine.SingleOrDefault(p => p.CompanyCalendarLineId == value.CompanyCalendarLineId);
            CompanyCalendarLine.CompanyCalendarId = value.CompanyCalendarId;
            CompanyCalendarLine.EventDate = value.EventDate;
            CompanyCalendarLine.VendorId = value.VendorId;
            CompanyCalendarLine.TypeOfServiceId = value.TypeOfServiceId;
            CompanyCalendarLine.MachineId = value.MachineId;
            CompanyCalendarLine.NoOfHoursRequire = value.NoOfHoursRequire;
            CompanyCalendarLine.Notes = value.Notes;
            CompanyCalendarLine.StatusCodeId = value.StatusCodeID;
            CompanyCalendarLine.ModifiedByUserId = value.ModifiedByUserID;
            CompanyCalendarLine.ModifiedDate = DateTime.Now;
            CompanyCalendarLine.AllEventFlag = value.AllEventFlag;
            CompanyCalendarLine.FromTime = value.FromTime;
            CompanyCalendarLine.ToTime = value.ToTime;
            CompanyCalendarLine.LinkDocComment = value.LinkDocComment;
            CompanyCalendarLine.DueDate = value.DueDate;
            CompanyCalendarLine.TypeOfEventId = value.TypeOfEventId;
            CompanyCalendarLine.Subject = value.Subject;
            CompanyCalendarLine.SessionId = value.SessionId;
            CompanyCalendarLine.CalenderStatusId = value.CalenderStatusId;
            CompanyCalendarLine.Tentative = value.Tentative;
            _context.SaveChanges();
            var CompanyCalendarLineLocations = _context.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
            if (CompanyCalendarLineLocations != null)
            {
                _context.CompanyCalendarLineLocation.RemoveRange(CompanyCalendarLineLocations);
                _context.SaveChanges();
            }
            var companyCalendarLineUser = _context.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
            if (companyCalendarLineUser != null)
            {
                _context.CompanyCalendarLineUser.RemoveRange(companyCalendarLineUser);
                _context.SaveChanges();
            }
            if (value.LocationIds != null && value.LocationIds.Count > 0)
            {
                value.LocationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineLocation
                    {
                        LocationId = h,
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineLocation.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.ParticipantsIds != null && value.ParticipantsIds.Count > 0)
            {
                value.ParticipantsIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Participants",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            if (value.InformationIds != null && value.InformationIds.Count > 0)
            {
                value.InformationIds.ForEach(h =>
                {
                    var CompanyCalendarLineLocation = new CompanyCalendarLineUser
                    {
                        UserId = h,
                        TypeName = "Information",
                        CompanyCalendarLineId = CompanyCalendarLine.CompanyCalendarLineId,
                    };
                    _context.CompanyCalendarLineUser.Add(CompanyCalendarLineLocation);
                });
                _context.SaveChanges();
            }
            var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.TypeOfServiceId).ToList();
            value.TypeOfServiceName = value.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarEventLine")]
        public CompanyCalendarLineModel UpdateCompanyCalendarEventLine(CompanyCalendarLineModel value)
        {
            var CompanyCalendarLine = _context.CompanyCalendarLine.SingleOrDefault(p => p.CompanyCalendarLineId == value.CompanyCalendarLineId);
            if (value.Type == "Location")
            {
                CompanyCalendarLine.TypeOfServiceId = value.TypeOfServiceId;
            }
            if (value.Type == "Date")
            {
                CompanyCalendarLine.EventDate = value.EventDate;
                CompanyCalendarLine.AllEventFlag = "All Day";
            }
            if (value.Type == "FromTime")
            {
                CompanyCalendarLine.FromTime = value.FromTime;
                CompanyCalendarLine.AllEventFlag = "Specific Hours";
            }
            if (value.Type == "ToTime")
            {
                CompanyCalendarLine.ToTime = value.ToTime;
                CompanyCalendarLine.AllEventFlag = "Specific Hours";
            }
            if (value.Type == "Status")
            {
                CompanyCalendarLine.CalenderStatusId = value.CalenderStatusId;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCompanyCalendarLineNotes")]
        public ActionResult<string> DeleteCompanyCalendarLineNotes(int id)
        {
            try
            {
                var CompanyCalendarLine = _context.CompanyCalendarLineNotes.Where(b => b.CompanyCalendarLineNotesId == id).FirstOrDefault();
                _context.CompanyCalendarLineNotes.Remove(CompanyCalendarLine);
                _context.SaveChanges();
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteCompanyCalendarLineLink")]
        public ActionResult<string> DeleteCompanyCalendarLineLink(int id)
        {
            try
            {
                var companyCalendarLineLink = _context.CompanyCalendarLineLink.Where(b => b.CompanyCalendarLineLinkId == id).FirstOrDefault();
                var CompanyCalendarLineLinkUserRemove = _context.CompanyCalendarLineLinkUser.Where(w => w.CompanyCalendarLineLinkId == id).ToList();
                if (CompanyCalendarLineLinkUserRemove != null)
                {
                    _context.CompanyCalendarLineLinkUser.RemoveRange(CompanyCalendarLineLinkUserRemove);
                    _context.SaveChanges();
                }
                _context.CompanyCalendarLineLink.Remove(companyCalendarLineLink);
                _context.SaveChanges();
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteCompanyCalendarLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var CompanyCalendarLine = _context.CompanyCalendarLine.Where(b => b.CompanyCalendarLineId == id).FirstOrDefault();
                if (CompanyCalendarLine != null)
                {
                    var CompanyCalendarLineLocation = _context.CompanyCalendarLineLocation.Where(b => b.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
                    if (CompanyCalendarLineLocation != null)
                    {
                        _context.CompanyCalendarLineLocation.RemoveRange(CompanyCalendarLineLocation);
                        _context.SaveChanges();
                    }
                    var companyCalendarLineUser = _context.CompanyCalendarLineUser.Where(w => w.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
                    if (companyCalendarLineUser != null)
                    {
                        _context.CompanyCalendarLineUser.RemoveRange(companyCalendarLineUser);
                        _context.SaveChanges();
                    }
                    var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Where(b => b.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
                    if (companyCalendarLineNotes != null)
                    {
                        _context.CompanyCalendarLineNotes.RemoveRange(companyCalendarLineNotes);
                        _context.SaveChanges();
                    }

                    var companyCalendarLineLink = _context.CompanyCalendarLineLink.Where(b => b.CompanyCalendarLineId == CompanyCalendarLine.CompanyCalendarLineId).ToList();
                    if (companyCalendarLineLink != null)
                    {
                        companyCalendarLineLink.ForEach(s =>
                        {
                            var CompanyCalendarLineLinkUserRemove = _context.CompanyCalendarLineLinkUser.Where(w => w.CompanyCalendarLineLinkId == s.CompanyCalendarLineLinkId).ToList();
                            if (CompanyCalendarLineLinkUserRemove != null)
                            {
                                _context.CompanyCalendarLineLinkUser.RemoveRange(CompanyCalendarLineLinkUserRemove);
                                _context.SaveChanges();
                            }
                        });
                        _context.CompanyCalendarLineLink.RemoveRange(companyCalendarLineLink);
                        _context.SaveChanges();
                    }
                    _context.CompanyCalendarLine.Remove(CompanyCalendarLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("UpdatecompanyCalendarLinePrintNotes")]
        public CompanyCalendarLineLinkModel UpdatecompanyCalendarLinePrintNotes(CompanyCalendarLineLinkModel value)
        {
            if (value.Type == "Link")
            {
                var companyCalendarLineLink = _context.CompanyCalendarLineLink.FirstOrDefault(w => w.CompanyCalendarLineLinkId == value.CompanyCalendarLineLinkId);
                companyCalendarLineLink.Description = value.Description;
                _context.SaveChanges();
            }
            if (value.Type == "Document")
            {
                var query = string.Format("Update Documents Set Description = '{1}' Where  DocumentId={0}", value.CompanyCalendarLineLinkId, value.Description);
                _context.Database.ExecuteSqlRaw(query);
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPost]
        [Route("InsertCompanyCalendarLineMeetingNotes")]
        public CompanyCalendarLineMeetingNotesModel InsertCompanyCalendarLineMeetingNotes(CompanyCalendarLineMeetingNotesModel value)
        {
            var companyCalendarLineMeetingNotes = new CompanyCalendarLineMeetingNotes
            {
                CompanyCalendarLineId = value.CompanyCalendarLineId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                MeetingData = value.MeetingData,
                StatusCodeId = value.StatusCodeID,
                Subject = value.Subject,
                IsTitleFlag = value.IsTitleFlag,
            };
            _context.CompanyCalendarLineMeetingNotes.Add(companyCalendarLineMeetingNotes);
            _context.SaveChanges();
            value.CompanyCalendarLineMeetingNotesId = companyCalendarLineMeetingNotes.CompanyCalendarLineMeetingNotesId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendarLineMeetingNotes")]
        public CompanyCalendarLineMeetingNotesModel UpdateCompanyCalendarLineMeetingNotes(CompanyCalendarLineMeetingNotesModel value)
        {
            var companyCalendarLineLink = _context.CompanyCalendarLineMeetingNotes.FirstOrDefault(w => w.CompanyCalendarLineMeetingNotesId == value.CompanyCalendarLineMeetingNotesId);
            companyCalendarLineLink.MeetingData = value.MeetingData;
            companyCalendarLineLink.ModifiedByUserId = value.ModifiedByUserID;
            companyCalendarLineLink.ModifiedDate = DateTime.Now;
            companyCalendarLineLink.Subject = value.Subject;
            companyCalendarLineLink.IsTitleFlag = value.IsTitleFlag;
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("GetCompanyCalendarLineMeetingNotes")]
        public List<CompanyCalendarLineMeetingNotesModel> GetCompanyCalendarLineMeetingNotes(long? id)
        {
            List<CompanyCalendarLineMeetingNotesModel> companyCalendarLineMeetingNotesModels = new List<CompanyCalendarLineMeetingNotesModel>();
            var companyCalendarLineMeetingNotes = _context.CompanyCalendarLineMeetingNotes.Include(a => a.AddedByUser).Where(w => w.CompanyCalendarLineId == id).ToList();
            companyCalendarLineMeetingNotes.ForEach(s =>
            {
                CompanyCalendarLineMeetingNotesModel companyCalendarLineMeetingNotesModel = new CompanyCalendarLineMeetingNotesModel();
                companyCalendarLineMeetingNotesModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                companyCalendarLineMeetingNotesModel.CompanyCalendarLineMeetingNotesId = s.CompanyCalendarLineMeetingNotesId;
                companyCalendarLineMeetingNotesModel.AddedByUserID = s.AddedByUserId;
                companyCalendarLineMeetingNotesModel.StatusCodeID = s.StatusCodeId;
                companyCalendarLineMeetingNotesModel.MeetingData = s.MeetingData;
                companyCalendarLineMeetingNotesModel.AddedDate = s.AddedDate;
                companyCalendarLineMeetingNotesModel.AddedByUser = s.AddedByUser?.UserName;
                companyCalendarLineMeetingNotesModel.Subject = s.Subject;
                companyCalendarLineMeetingNotesModel.IsTitleFlag = s.IsTitleFlag;
                companyCalendarLineMeetingNotesModels.Add(companyCalendarLineMeetingNotesModel);
            });
            return companyCalendarLineMeetingNotesModels.OrderByDescending(o => o.CompanyCalendarLineMeetingNotesId).ToList();
        }
        [HttpPost]
        [Route("InsertRestrictUsers")]
        public CompanyCalandarDocumentPermissionModel InsertRestrictUsers(CompanyCalandarDocumentPermissionModel value)
        {
            if (value.RestrictUserIds != null && value.RestrictUserIds.Count > 0)
            {
                value.RestrictUserIds.ForEach(s =>
                {
                    var companyCalendarLineMeetingNotes = new CompanyCalandarDocumentPermission
                    {
                        UserId = s,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 1,
                        DocumentId = value.DocumentId
                    };
                    _context.CompanyCalandarDocumentPermission.Add(companyCalendarLineMeetingNotes);
                });

                _context.SaveChanges();
            }
            return value;
        }
        [HttpGet]
        [Route("GetRestrictUsers")]
        public CompanyCalandarDocumentPermissionModel GetRestrictUsers(long? id)
        {
            CompanyCalandarDocumentPermissionModel companyCalendarLineMeetingNotesModels = new CompanyCalandarDocumentPermissionModel();
            var userIds = _context.CompanyCalandarDocumentPermission.Where(w => w.DocumentId == id).Select(s => s.UserId).ToList();
            companyCalendarLineMeetingNotesModels.CompanyCalandarDocumentPermissionId = 0;
            companyCalendarLineMeetingNotesModels.RestrictUserIds = userIds != null && userIds.Count > 0 ? userIds : new List<long?>();
            return companyCalendarLineMeetingNotesModels;
        }
    }
}
