﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PlantController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PlantController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPlants")]
        public List<PlantModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<PlantModel> plantModels = new List<PlantModel>();
            var plant = _context.Plant.Include(p=>p.Company).Include(p=>p.AddedByUser).Include(p=>p.StatusCode).Include(p=>p.ModifiedByUser).OrderByDescending(o => o.PlantId).AsNoTracking().ToList();
            if(plant!=null && plant.Count>0)
            {
                plant.ForEach(s =>
                {

                    PlantModel plantModel = new PlantModel();
                    string companyName = string.Empty;
                    plantModel.PlantID = s.PlantId;
                    plantModel.PlantCode = s.PlantCode;
                    plantModel.CompanyID = s.CompanyId;
                    plantModel.Description = s.Description;
                    plantModel.IsLinkNav = s.IsLinkNav != null ? s.IsLinkNav.Value : false;
                    plantModel.RegisteredCountryID = s.RegisteredCountryId;
                    plantModel.RegistrationNo = s.RegistrationNo;
                    plantModel.EstablishedDate = s.EstablishedDate;
                    plantModel.GSTNo = s.Gstno;
                    plantModel.RegisteredCountry = s.RegisteredCountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisteredCountryId).Select(m => m.Value).FirstOrDefault() : "";
                    plantModel.CompanyName = s.Company != null ? s.Company.CompanyName : "";
                    plantModel.NavCompanyName = s.NavCompanyName;
                    plantModel.NavSoapAddress = s.NavSoapAddress;
                    plantModel.NavPassword = s.NavPassword;
                    plantModel.NavUserName = s.NavUserName;
                    plantModel.NavOdataAddress = s.NavOdataAddress;
                    plantModel.StatusCodeID = s.StatusCodeId;
                    plantModel.AddedByUserID = s.AddedByUserId;
                    plantModel.ModifiedByUserID = s.ModifiedByUserId;
                    plantModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    plantModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    plantModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    plantModel.AddedDate = s.AddedDate;
                    plantModel.ModifiedDate = s.ModifiedDate;
                         if (!string.IsNullOrEmpty(plantModel.Description))
                    {
                        companyName = Regex.Replace(s.Description, @"[^0-9a-zA-Z]+", "").ToLower();
                    }
                    plantModel.CountryId = companyName.Trim().Contains("sdnbhd") ? 1 : companyName.Trim().Contains("pteltd") ? 2 : new long();

                    
                    plantModels.Add(plantModel);
                });
            }
          
            return plantModels;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetMobilePlants")]
        public List<PlantMobileModel> GetPlants()
        {
            List<PlantMobileModel> plantMobileModels = new List<PlantMobileModel>();
            var plant = _context.Plant.Where(c => c.StatusCodeId == 1).OrderByDescending(o => o.PlantId).AsNoTracking().ToList();
            if(plant!=null && plant.Count>0)
            {
                plant.ForEach(s =>
                {
                    PlantMobileModel plantMobileModel = new PlantMobileModel
                    {
                        PlantID = s.PlantId,
                        PlantCode = s.PlantCode,
                        Description = s.Description,
                        NavCompanyName = s.NavCompanyName,
                        NavSoapAddress = s.NavSoapAddress,
                        NavPassword = s.NavPassword,
                        NavUserName = s.NavUserName,
                        NavOdataAddress = s.NavOdataAddress,
                    };
                    plantMobileModels.Add(plantMobileModel);
                });
            }
           
            return plantMobileModels;
        }

        [HttpGet]
        [Route("GetPlant")]
        public List<PlantModel> GetICT(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<PlantModel> plantModels = new List<PlantModel>();
            var plant = _context.Plant.Include(p => p.Company).Include(p => p.AddedByUser).Include(p => p.StatusCode).Include(p => p.ModifiedByUser).OrderByDescending(o => o.PlantId).Where(t => t.PlantId == id).AsNoTracking().ToList();
            if(plant!=null && plant.Count>0)
            {
                plant.ForEach(s =>
                {
                    PlantModel plantModel = new PlantModel
                    {
                        PlantID = s.PlantId,
                        PlantCode = s.PlantCode,
                        CompanyID = s.CompanyId,
                        Description = s.Description,
                        IsLinkNav = s.IsLinkNav.Value,
                        RegisteredCountryID = s.RegisteredCountryId,
                        RegistrationNo = s.RegistrationNo,
                        EstablishedDate = s.EstablishedDate,
                        GSTNo = s.Gstno,
                        RegisteredCountry = s.RegisteredCountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisteredCountryId).Select(m => m.Value).FirstOrDefault() : "",
                        CompanyName = s.Company!=null? s.Company.CompanyName : "",
                        NavCompanyName = s.NavCompanyName,                       
                        NavSoapAddress = s.NavSoapAddress,
                        NavPassword = s.NavPassword,
                        NavUserName = s.NavUserName,
                        NavOdataAddress = s.NavOdataAddress,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantModels.Add(plantModel);
                });

            }        
           
            return plantModels; 
        }
       
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PlantModel> GetData(SearchModel searchModel)
        {
            var Plant = new Plant();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).FirstOrDefault();
                        break;
                    case "Last":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).LastOrDefault();
                        break;
                    case "Next":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).LastOrDefault();
                        break;
                    case "Previous":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).FirstOrDefault();
                        break;
                    case "Last":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).LastOrDefault();
                        break;
                    case "Next":
                        Plant = _context.Plant.OrderBy(o => o.PlantId).FirstOrDefault(s => s.PlantId > searchModel.Id);
                        break;
                    case "Previous":
                        Plant = _context.Plant.OrderByDescending(o => o.PlantId).FirstOrDefault(s => s.PlantId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PlantModel>(Plant);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPlant")]
        public PlantModel Post(PlantModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var Plant = new Plant
            {
                //PlantId = value.PlantID,

                PlantCode = value.PlantCode,
                CompanyId = value.CompanyID,
                Description = value.Description,
                IsLinkNav = value.IsLinkNav,
                RegisteredCountryId = value.RegisteredCountryID,
                RegistrationNo = value.RegistrationNo,
                EstablishedDate = value.EstablishedDate,
               
                Gstno = value.GSTNo,
                NavCompanyName = value.NavCompanyName,
                NavSoapAddress = value.NavSoapAddress,
                NavPassword = value.NavPassword,
                NavUserName = value.NavUserName,
                NavOdataAddress = value.NavOdataAddress,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                ModifiedByUserId = value.ModifiedByUserID,
                AddedDate = value.AddedDate,
                ModifiedDate = value.ModifiedDate
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };


            _context.Plant.Add(Plant);
            _context.SaveChanges();
            value.PlantID = Plant.PlantId;
            value.RegisteredCountry = value.RegisteredCountryID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisteredCountryID).Select(m => m.Value).FirstOrDefault() : "";
            value.StatusCode = _context.CodeMaster.Where(s => s.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault();
            // _context.IctlayoutPlanTypes.Add(ictLayout);
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePlant")]
        public PlantModel Put(PlantModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var Plant = _context.Plant.SingleOrDefault(p => p.PlantId == value.PlantID);
            //Plant.PlantId = value.PlantID;
            Plant.ModifiedByUserId = value.ModifiedByUserID;
            Plant.ModifiedDate = DateTime.Now;
            Plant.PlantCode = value.PlantCode;
            Plant.CompanyId = value.CompanyID;
            Plant.Description = value.Description;
            Plant.IsLinkNav = value.IsLinkNav;
            Plant.RegisteredCountryId = value.RegisteredCountryID;
            Plant.RegistrationNo = value.RegistrationNo;
            Plant.EstablishedDate = value.EstablishedDate;
            Plant.Gstno = value.GSTNo;
            Plant.NavCompanyName = value.NavCompanyName;
            Plant.NavSoapAddress = value.NavSoapAddress;
            Plant.NavPassword = value.NavPassword;

            Plant.NavUserName = value.NavUserName;
            Plant.NavOdataAddress = value.NavOdataAddress;
            Plant.ModifiedByUserId = value.ModifiedByUserID;
            Plant.ModifiedDate = DateTime.Now;
            Plant.StatusCodeId = value.StatusCodeID;
            
            _context.SaveChanges();
            value.RegisteredCountry = value.RegisteredCountryID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisteredCountryID).Select(m => m.Value).FirstOrDefault() : "";



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePlant")]
        public void Delete(int id)
        {
            var Plant = _context.Plant.SingleOrDefault(p => p.PlantId == id);
            var errorMessage = "";
            try
            {
                if (Plant != null)
                {
                    _context.Plant.Remove(Plant);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction record for this company record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}