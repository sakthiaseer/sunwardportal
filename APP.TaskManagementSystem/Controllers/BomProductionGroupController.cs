﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BomProductionGroupController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public BomProductionGroupController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetBomProductionGroups")]
        public List<BomProductionGroup> Get()
        {

            List<BomProductionGroup> bomProductionGroups = new List<BomProductionGroup>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var bomProductionGroupslist = _context.BomproductionGroup.Include(s => s.ManufacturingRecipe.FinishProductGeneralInfo)
                                            .Include(g => g.ManufacturingRecipe.FinishProductGeneralInfo).Include("AddedByUser")
                                            .Include("ModifiedByUser").OrderByDescending(o => o.BomproductionGroupId).AsNoTracking()
                                            .ToList();//.Select(s => new BomProductionGroup

            if (bomProductionGroupslist != null && bomProductionGroupslist.Count > 0)
            {
                bomProductionGroupslist.ForEach(s =>
                {
                    BomProductionGroup bomProductionGroup = new BomProductionGroup
                    {
                        BomproductionGroupId = s.BomproductionGroupId,
                        FpexcipientId = s.FpexcipientId,
                        ManufacturingRecipeId = s.ManufacturingRecipeId,
                        ManufacturingSite = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => s.ManufacturingRecipe!=null &&  a.ApplicationMasterDetailId == s.ManufacturingRecipe.ManufacturingSiteId) != null ? applicationmasterdetail.FirstOrDefault(a => s.ManufacturingRecipe != null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.ManufacturingSiteId).Value : "",
                        RegisterCountry = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                        RegisterHolderName = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                        ProductOwner = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterProductOwnerId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterProductOwnerId).Value : string.Empty,
                        PRHSpecificProductName = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.PrhspecificProductId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.PrhspecificProductId).Value : string.Empty,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,


                    };
                    bomProductionGroups.Add(bomProductionGroup);
                });

            }




            return bomProductionGroups;
        }

        [HttpPost]
        [Route("GetBomProductionGroupsByRefNo")]
        public List<BomProductionGroup> GetProductionColorsByRefNo(RefSearchModel refSearchModel)
        {

            List<BomProductionGroup> bomProductionGroups = new List<BomProductionGroup>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var BomproductionGrouplist = _context.BomproductionGroup
                                            .Include(s => s.ManufacturingRecipe.FinishProductGeneralInfo)
                                            .Include(g => g.ManufacturingRecipe.FinishProductGeneralInfo)
                                            .Include("AddedByUser").Include("ModifiedByUser")
                                            .OrderByDescending(o => o.BomproductionGroupId).AsNoTracking().ToList();
            if (BomproductionGrouplist != null && BomproductionGrouplist.Count > 0)
            {
                BomproductionGrouplist.ForEach(s =>
                {
                    BomProductionGroup bomProductionGroup = new BomProductionGroup
                    {
                        BomproductionGroupId = s.BomproductionGroupId,
                        FpexcipientId = s.FpexcipientId,
                        ManufacturingRecipeId = s.ManufacturingRecipeId,
                        ManufacturingSite = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => s.ManufacturingRecipe!=null &&  a.ApplicationMasterDetailId == s.ManufacturingRecipe.ManufacturingSiteId) != null ? applicationmasterdetail.FirstOrDefault(a => s.ManufacturingRecipe != null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.ManufacturingSiteId).Value : "",
                        RegisterCountry = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                        RegisterHolderName = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                        ProductOwner = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterProductOwnerId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.RegisterProductOwnerId).Value : string.Empty,
                        PRHSpecificProductName = applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.PrhspecificProductId) != null ? applicationmasterdetail.SingleOrDefault(a => s.ManufacturingRecipe!=null && a.ApplicationMasterDetailId == s.ManufacturingRecipe.FinishProductGeneralInfo.PrhspecificProductId).Value : string.Empty,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,

                    };
                    bomProductionGroups.Add(bomProductionGroup);
                });

            }

   if (refSearchModel.IsHeader)
            {
                return bomProductionGroups.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BomproductionGroupId).ToList();
            }
            return bomProductionGroups.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BomproductionGroupId).ToList();
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertBomProductionGroup")]
        public BomProductionGroup Post(BomProductionGroup value)
        {
            var profileNo = "";
           
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 });
            }
            var bomproductionGroup = new BomproductionGroup
            {
                ManufacturingRecipeId = value.ManufacturingRecipeId,
                FpexcipientId = value.FpexcipientId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                ProfileReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.BomproductionGroup.Add(bomproductionGroup);
            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBomProductionGroup")]
        public BomProductionGroup Put(BomProductionGroup value)
        {
            var bomproductionGroup = _context.BomproductionGroup.SingleOrDefault(p => p.BomproductionGroupId == value.BomproductionGroupId);
            bomproductionGroup.ManufacturingRecipeId = value.ManufacturingRecipeId;
            bomproductionGroup.FpexcipientId = value.FpexcipientId;
            bomproductionGroup.ModifiedByUserId = value.ModifiedByUserID;
            bomproductionGroup.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBomProductionGroup")]
        public void Delete(int id)
        {
            var bomproductionGroup = _context.BomproductionGroup.SingleOrDefault(p => p.BomproductionGroupId == id);
            if (bomproductionGroup != null)
            {
                _context.BomproductionGroup.Remove(bomproductionGroup);
                _context.SaveChanges();
            }
        }
    }
}