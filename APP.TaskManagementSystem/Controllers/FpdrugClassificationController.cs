﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FpdrugClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public FpdrugClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetFpdrugClassificationByRefNo")]
        public List<FpdrugClassificationModel> GetFpdrugClassificationByRefNo(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var fpdrugClassification = _context.FpdrugClassification.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(p => p.Profile).AsNoTracking().ToList();
            List<FpdrugClassificationModel> fpdrugClassificationModel = new List<FpdrugClassificationModel>();
            fpdrugClassification.ForEach(s =>
            {
                FpdrugClassificationModel fpdrugClassificationModels = new FpdrugClassificationModel
                {
                    FpdrugClassificationId = s.FpdrugClassificationId,
                    DrugClassificationId = s.DrugClassificationId,
                    CountryId = s.CountryId,
                    CountryName = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CountryId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.CountryId).Value : "",
                    DrugClassification = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.DrugClassificationId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.DrugClassificationId).Value : "",
                    IsInstruction = s.IsInstruction,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileId = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    IsInstructionFlag = s.IsInstruction == true ? "Yes" : "No",
                };
                fpdrugClassificationModel.Add(fpdrugClassificationModels);
            });
            return fpdrugClassificationModel.Where(l => l.ItemClassificationMasterId == id).OrderByDescending(a => a.FpdrugClassificationId).ToList();
        }
        [HttpPost]
        [Route("InsertFpdrugClassification")]
        public FpdrugClassificationModel Post(FpdrugClassificationModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var fpdrugClassification = new FpdrugClassification
            {
                DrugClassificationId = value.DrugClassificationId,
                CountryId = value.CountryId,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
                IsInstruction = value.IsInstructionFlag == "Yes" ? true : false,
            };
            _context.FpdrugClassification.Add(fpdrugClassification);
            _context.SaveChanges();
            value.FpdrugClassificationId = fpdrugClassification.FpdrugClassificationId;
            value.ProfileReferenceNo = fpdrugClassification.ProfileReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateFpdrugClassification")]
        public FpdrugClassificationModel Put(FpdrugClassificationModel value)
        {
            var fpdrugClassification = _context.FpdrugClassification.SingleOrDefault(p => p.FpdrugClassificationId == value.FpdrugClassificationId);
            fpdrugClassification.ProfileReferenceNo = value.ProfileReferenceNo;
            fpdrugClassification.CountryId = value.CountryId;
            fpdrugClassification.ModifiedByUserId = value.ModifiedByUserID;
            fpdrugClassification.ModifiedDate = DateTime.Now;
            fpdrugClassification.IsInstruction = value.IsInstructionFlag == "Yes" ? true : false;
            fpdrugClassification.ProfileId = value.ProfileId;
            fpdrugClassification.ItemClassificationMasterId = value.ItemClassificationMasterId;
            if (value.IsInstructionFlag != "Yes")
            {
                var fpdrugClassificationLine = _context.FpdrugClassificationLine.Where(t => t.FpdrugClassificationId == fpdrugClassification.FpdrugClassificationId).ToList();
                if (fpdrugClassificationLine != null)
                {
                    _context.FpdrugClassificationLine.RemoveRange(fpdrugClassificationLine);
                    _context.SaveChanges();
                }
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteFpdrugClassification")]
        public void Delete(int id)
        {
            var fpdrugClassification = _context.FpdrugClassification.SingleOrDefault(p => p.FpdrugClassificationId == id);
            if (fpdrugClassification != null)
            {
                var fpdrugClassificationLine = _context.FpdrugClassificationLine.Where(t => t.FpdrugClassificationId == fpdrugClassification.FpdrugClassificationId).ToList();
                if (fpdrugClassificationLine != null)
                {
                    _context.FpdrugClassificationLine.RemoveRange(fpdrugClassificationLine);
                    _context.SaveChanges();
                }
                _context.FpdrugClassification.Remove(fpdrugClassification);
                _context.SaveChanges();
            }
        }
    }
}