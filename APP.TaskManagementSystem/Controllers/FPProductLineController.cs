﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FPProductLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FPProductLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetFpproductLines")]
        public List<FPProductLineModel> Get(int id)
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var FpproductLine = _context.FpproductLine.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.Navision).AsNoTracking().ToList();
            List<FPProductLineModel> FPProductLineModel = new List<FPProductLineModel>();
            FpproductLine.ForEach(s =>
            {
                FPProductLineModel FPProductLineModels = new FPProductLineModel
                {
                    FPProductLineID = s.FpproductLineId,
                    DatabaseID = s.DatabaseId,
                    NavisionID = s.NavisionId,
                    FPProductID = s.FpproductId,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                    Navision = s.Navision != null ? (s.Navision.No + '|' + s.Navision.Description) : "",
                };
                FPProductLineModel.Add(FPProductLineModels);
            });
            return FPProductLineModel.Where(f => f.FPProductID == id).OrderByDescending(o => o.FPProductLineID).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPProductLineModel> GetData(SearchModel searchModel)
        {
            var FpproductLine = new FpproductLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).LastOrDefault();
                        break;
                    case "Next":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).LastOrDefault();
                        break;
                    case "Previous":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).LastOrDefault();
                        break;
                    case "Next":
                        FpproductLine = _context.FpproductLine.OrderBy(o => o.FpproductLineId).FirstOrDefault(s => s.FpproductLineId > searchModel.Id);
                        break;
                    case "Previous":
                        FpproductLine = _context.FpproductLine.OrderByDescending(o => o.FpproductLineId).FirstOrDefault(s => s.FpproductLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPProductLineModel>(FpproductLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFpproductLine")]
        public FPProductLineModel Post(FPProductLineModel value)
        {
            var FpproductLine = new FpproductLine
            {

                DatabaseId = value.DatabaseID,
                NavisionId = value.NavisionID,
                FpproductId = value.FPProductID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,



            };
            _context.FpproductLine.Add(FpproductLine);
            _context.SaveChanges();
            value.FPProductLineID = FpproductLine.FpproductLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFpproductLine")]
        public FPProductLineModel Put(FPProductLineModel value)
        {
            var fproduct = _context.FpproductLine.SingleOrDefault(p => p.FpproductLineId == value.FPProductLineID);
            fproduct.DatabaseId = value.DatabaseID;
            fproduct.NavisionId = value.NavisionID;
            fproduct.FpproductId = value.FPProductID;
            fproduct.StatusCodeId = value.StatusCodeID;
            fproduct.ModifiedByUserId = value.ModifiedByUserID;
            fproduct.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFpproductLine")]
        public void Delete(int id)
        {
            var FpproductLine = _context.FpproductLine.SingleOrDefault(p => p.FpproductLineId == id);
            if (FpproductLine != null)
            {
                _context.FpproductLine.Remove(FpproductLine);
                _context.SaveChanges();
            }
        }
    }
}