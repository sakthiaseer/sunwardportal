﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentAlertController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentAlertController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentAlerts")]
        public List<DocumentAlertModel> Get()
        {
            var DocumentAlert = _context.DocumentAlert.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DocumentAlertModel> DocumentAlertModel = new List<DocumentAlertModel>();
            DocumentAlert.ForEach(s =>
            {
                DocumentAlertModel DocumentAlertModels = new DocumentAlertModel
                {
                    DocumentAlertId = s.DocumentAlertId,                   
                    Name = s.Name,
                    Link = s.Link,
                    Description = s.Description,                   
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentId = s.DocumentId,

                };
                DocumentAlertModel.Add(DocumentAlertModels);
            });
            return DocumentAlertModel.OrderByDescending(a => a.DocumentAlertId).ToList();
        }
      


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentAlertModel> GetData(SearchModel searchModel)
        {
            var documentAlert = new DocumentAlert();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).FirstOrDefault();
                        break;
                    case "Last":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).LastOrDefault();
                        break;
                    case "Next":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).LastOrDefault();
                        break;
                    case "Previous":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).FirstOrDefault();
                        break;
                    case "Last":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).LastOrDefault();
                        break;
                    case "Next":
                        documentAlert = _context.DocumentAlert.OrderBy(o => o.DocumentAlertId).FirstOrDefault(s => s.DocumentAlertId > searchModel.Id);
                        break;
                    case "Previous":
                        documentAlert = _context.DocumentAlert.OrderByDescending(o => o.DocumentAlertId).FirstOrDefault(s => s.DocumentAlertId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentAlertModel>(documentAlert);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentAlert")]
        public DocumentAlertModel Post(DocumentAlertModel value)
        {
            var SessionId = Guid.NewGuid();
            var documentAlert = new DocumentAlert
            {
               
                Description = value.Description,
                Link = value.Link,
                Name = value.Name,
                DocumentId  = value.DocumentId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = SessionId,
            };
            _context.DocumentAlert.Add(documentAlert);
            _context.SaveChanges();
            value.DocumentAlertId = documentAlert.DocumentAlertId;
            value.SessionId = SessionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentAlert")]
        public DocumentAlertModel Put(DocumentAlertModel value)
        {
            var documentAlert = _context.DocumentAlert.SingleOrDefault(p => p.DocumentAlertId == value.DocumentAlertId);

           
            documentAlert.Description = value.Description;
            documentAlert.Link = value.Link;
            documentAlert.Name = value.Name;
           
            documentAlert.ModifiedByUserId = value.ModifiedByUserID;
            documentAlert.ModifiedDate = DateTime.Now;
            documentAlert.StatusCodeId = value.StatusCodeID.Value;
            documentAlert.DocumentId = value.DocumentId;
           
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentAlert")]
        public void Delete(int id)
        {
            try
            {
                var documentAlert = _context.DocumentAlert.SingleOrDefault(p => p.DocumentAlertId == id);
                if (documentAlert != null)
                {
                    _context.DocumentAlert.Remove(documentAlert);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("DocumentAlert Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
