﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavisionSpecificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public NavisionSpecificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetNavisionSpecifications")]
        public List<NavisionSpecificationModel> Get()
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.ToList();
            List<NavisionSpecificationModel> navisionSpecificationModels = new List<NavisionSpecificationModel>();
            var navisionSpecification = _context.NavisionSpecification.Include("AddedByUser").Include("ModifiedByUser").Include("Navision").Include(s=>s.Navision.StatusCode).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            if(navisionSpecification!=null && navisionSpecification.Count>0)
            {
                navisionSpecification.ForEach(s =>
                {
                    NavisionSpecificationModel navisionSpecificationModel = new NavisionSpecificationModel
                    {
                        NavisionSpecificationId = s.NavisionSpecificationId,
                        DatabaseId = s.DatabaseId,
                        NavisionId = s.NavisionId,
                        CompanyName = s.Navision!=null? s.Navision.Company : string.Empty,
                        Description = s.Navision != null ? s.Navision.Description : string.Empty,
                        Description2 = s.Navision != null ? s.Navision.Description2 : string.Empty,
                        InternalReference = s.Navision != null ? s.Navision.InternalRef : string.Empty,
                        NavisionBuom = s.Navision != null ? s.Navision.BaseUnitofMeasure : string.Empty,
                        NavisionStatus = s.Navision != null && s.Navision.StatusCode != null ? s.Navision.StatusCode.CodeValue : string.Empty,
                        AddedDate = s.AddedDate,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                        Navision = s.Navision != null ? s.Navision.No + '|' + s.Navision.Description + '|' + s.Navision.Description2 : string.Empty,
                        ProfileLinkReferenceNo = s.ProfileRefNo,
                        LinkProfileReferenceNo = s.LinkProfileRefNo,
                        MasterProfileReferenceNo = s.MasterProfileRefNo

                    };
                    navisionSpecificationModels.Add(navisionSpecificationModel);
                });
            }
            return navisionSpecificationModels;
        }

        [HttpPost]
        [Route("GetNavisionSpecificationsByRefNo")]
        public List<NavisionSpecificationModel> GetNavisionSpecificationsByRefNo(RefSearchModel refSearchModel)
        {
            List<NavisionSpecification> navisionSpecification = new List<NavisionSpecification>();
            List<NavisionSpecificationModel> navisionSpecificationModels = new List<NavisionSpecificationModel>();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavSpecModel> navList = new List<NavSpecModel>();
            if (refSearchModel.NavType == "ProductionColor")
            {
                 navList = _context.ProductionColor.Select(s => new NavSpecModel
                {
                    NavisionSpecificationId = s.ProductionColorId,
                    MaterialSpecificationName = s.MaterialName,
                    MaterialSpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                }).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            else if (refSearchModel.NavType == "ProductionFlavour")
            {
                navList = _context.ProductionFlavour.Select(s => new NavSpecModel
                {
                    NavisionSpecificationId = s.ProductionFlavourId,
                    MaterialSpecificationName = s.MaterialName,
                    MaterialSpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                }).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            if (refSearchModel.NavType == "ProductionCapsule")
            {
                navList = _context.ProductionCapsule.Select(s => new NavSpecModel
                {
                    NavisionSpecificationId = s.ProductionCapsuleId,
                    MaterialSpecificationName = s.MaterialName,
                    MaterialSpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                }).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            if (refSearchModel.NavType == "ProductionMaterial")
            {
                navList = _context.ProductionMaterial.Select(s => new NavSpecModel
                {
                    NavisionSpecificationId = s.ProductionMaterialId,
                    MaterialSpecificationName = s.MaterialName,
                    MaterialSpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                }).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            
            if (refSearchModel.IsHeader)
            {
                navisionSpecification = _context.NavisionSpecification.Include("AddedByUser").Include("ModifiedByUser").Include("Navision").Include(s => s.Navision.StatusCode).Where(r => r.LinkProfileRefNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            else
            {
                navisionSpecification = _context.NavisionSpecification.Include("AddedByUser").Include("ModifiedByUser").Include("Navision").Include(s => s.Navision.StatusCode).Where(r => r.MasterProfileRefNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.NavisionSpecificationId).AsNoTracking().ToList();
            }
            if(navisionSpecification != null && navisionSpecification.Count>0)
            {
                navisionSpecification.ForEach(s =>
                {
                    NavisionSpecificationModel navisionSpecificationModel = new NavisionSpecificationModel
                    {
                        NavisionSpecificationId = s.NavisionSpecificationId,
                        DatabaseId = s.DatabaseId,
                        NavisionId = s.NavisionId,
                        CompanyName = s.Navision!=null? s.Navision.Company : string.Empty,
                        Description = s.Navision != null ? s.Navision.Description : string.Empty,
                        Description2 = s.Navision != null ? s.Navision.Description2 : string.Empty,
                        InternalReference = s.Navision != null ? s.Navision.InternalRef : string.Empty,
                        NavisionBuom = s.Navision != null ? s.Navision.BaseUnitofMeasure : string.Empty,                       
                        AddedDate = s.AddedDate,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                        Navision = s.Navision != null ? s.Navision.No + '|' + s.Navision.Description + '|' + s.Navision.Description2 : string.Empty,
                        NavisionStatus = s.Navision != null && s.Navision.StatusCode !=null? s.Navision.StatusCode.CodeValue : string.Empty,
                        ProfileLinkReferenceNo = s.ProfileRefNo,
                        LinkProfileReferenceNo = s.LinkProfileRefNo,
                        MasterProfileReferenceNo = s.MasterProfileRefNo,
                        MaterialSpecificationId = s.MaterialSpecificationId,
                        MaterialSpecificationName = navList != null && navList.SingleOrDefault(a => a.NavisionSpecificationId == s.MaterialSpecificationId) != null ? navList.SingleOrDefault(a => a.NavisionSpecificationId == s.MaterialSpecificationId).MaterialSpecificationName : string.Empty,
                        MaterialSpecificationNo = navList != null && navList.SingleOrDefault(a => a.NavisionSpecificationId == s.MaterialSpecificationId) != null ? navList.SingleOrDefault(a => a.NavisionSpecificationId == s.MaterialSpecificationId).MaterialSpecificationNo : string.Empty,
                    };
                    navisionSpecificationModels.Add(navisionSpecificationModel);
                });
            }           
            return navisionSpecificationModels;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertNavisionSpecification")]
        public NavisionSpecificationModel Post(NavisionSpecificationModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "NavisionSpecification" });
            var navisionSpecification = new NavisionSpecification
            {
                MaterialSpecificationId = value.MaterialSpecificationId,
                DatabaseId = value.DatabaseId,
                NavisionId = value.NavisionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                ProfileRefNo = profileNo,
                LinkProfileRefNo = value.LinkProfileReferenceNo,
                MasterProfileRefNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.NavisionSpecification.Add(navisionSpecification);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = navisionSpecification.MasterProfileRefNo;
            value.ProfileLinkReferenceNo = navisionSpecification.ProfileRefNo;
            value.NavisionSpecificationId = navisionSpecification.NavisionSpecificationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavisionSpecification")]
        public NavisionSpecificationModel Put(NavisionSpecificationModel value)
        {
            var navisionSpecification = _context.NavisionSpecification.SingleOrDefault(p => p.NavisionSpecificationId == value.NavisionSpecificationId);
            navisionSpecification.MaterialSpecificationId = value.MaterialSpecificationId;
            navisionSpecification.DatabaseId = value.DatabaseId;
            navisionSpecification.NavisionId = value.NavisionId;
            navisionSpecification.StatusCodeId = value.StatusCodeID;
            navisionSpecification.ModifiedByUserId = value.ModifiedByUserID;
            navisionSpecification.ModifiedDate = DateTime.Now;
            navisionSpecification.MasterProfileRefNo = value.MasterProfileReferenceNo;
            navisionSpecification.LinkProfileRefNo = value.LinkProfileReferenceNo;
            navisionSpecification.ProfileRefNo = value.ProfileLinkReferenceNo;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNavisionSpecification")]
        public void Delete(int id)
        {
            var navisionSpecification = _context.NavisionSpecification.SingleOrDefault(p => p.NavisionSpecificationId == id);
            if (navisionSpecification != null)
            {
                _context.NavisionSpecification.Remove(navisionSpecification);
                _context.SaveChanges();
            }
        }
    }
}