﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterProductionProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MasterProductionProcessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetMasterManufacturingProcess")]
        public List<MasterManufacturingProcessModel> Get()
        {
            List<MasterManufacturingProcessModel> masterManufacturingProcessModels = new List<MasterManufacturingProcessModel>();
            var masterManufacturingProcessItems = _context.OperationProcedure.Include(o => o.OperationProcedureLine)
                                                    .Include("AddedByUser").Include("ModifiedByUser")
                                                    .OrderByDescending(o => o.OperationProcedureId).AsNoTracking().ToList();
            if(masterManufacturingProcessItems!=null && masterManufacturingProcessItems.Count>0)
            {
                masterManufacturingProcessItems.ForEach(s =>
                {
                    MasterManufacturingProcessModel masterManufacturingProcessModel = new MasterManufacturingProcessModel
                    {
                        OperationProcedureId = s.OperationProcedureId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        OperationProcedureIds = s.OperationProcedureLine != null ? s.OperationProcedureLine.Select(o => o.OperationProcedureId).ToList() : new List<long?>(),
                        DosageFormId = s.DosageFormId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    masterManufacturingProcessModels.Add(masterManufacturingProcessModel);
                });
            }
          
            return masterManufacturingProcessModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetMasterManufacturingProcess/{id:int}")]
        public ActionResult<MasterManufacturingProcessModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var operationProcedure = _context.OperationProcedure.SingleOrDefault(p => p.OperationProcedureId == id.Value);
            var result = _mapper.Map<MasterManufacturingProcessModel>(operationProcedure);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MasterManufacturingProcessModel> GetData(SearchModel searchModel)
        {
            var operationProcedure = new OperationProcedure();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Previous":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        operationProcedure = _context.OperationProcedure.OrderBy(o => o.OperationProcedureId).FirstOrDefault(s => s.OperationProcedureId > searchModel.Id);
                        break;
                    case "Previous":
                        operationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault(s => s.OperationProcedureId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MasterManufacturingProcessModel>(operationProcedure);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertMasterManufacturingProcess")]
        public MasterManufacturingProcessModel Post(MasterManufacturingProcessModel value)
        {
            var operationProcedure = new OperationProcedure
            {
                DosageFormId = value.DosageFormId,
                ManufacturingProcessId = value.ManufacturingProcessId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.OperationProcedure.Add(operationProcedure);
            _context.SaveChanges();
            value.OperationProcedureId = operationProcedure.OperationProcedureId;
            if (value.OperationProcedureIds != null)
            {
                value.OperationProcedureIds.ForEach(c =>
                {
                    var operationProcedureLine = new OperationProcedureLine
                    {
                        OperationProcedureId = c,
                    };
                    operationProcedure.OperationProcedureLine.Add(operationProcedureLine);
                });
            }
            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMasterManufacturingProcess")]
        public MasterManufacturingProcessModel Put(MasterManufacturingProcessModel value)
        {
            var operationProcedure = _context.OperationProcedure.SingleOrDefault(p => p.OperationProcedureId == value.OperationProcedureId);
            operationProcedure.ModifiedByUserId = value.ModifiedByUserID;
            operationProcedure.ModifiedDate = DateTime.Now;
            operationProcedure.DosageFormId = value.DosageFormId;
            operationProcedure.ManufacturingProcessId = value.ManufacturingProcessId;
            operationProcedure.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            var operationProcedureLines = _context.OperationProcedureLine.Where(l => l.OperationProcedureId == value.OperationProcedureId).ToList();
            if (operationProcedureLines.Count > 0)
            {
                _context.OperationProcedureLine.RemoveRange(operationProcedureLines);
                _context.SaveChanges();
            }
            if (value.OperationProcedureIds != null)
            {
                value.OperationProcedureIds.ForEach(c =>
                {
                    var operationProcedureLine = new OperationProcedureLine
                    {
                        OperationProcedureId = c,
                    };
                    operationProcedure.OperationProcedureLine.Add(operationProcedureLine);
                });
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMasterManufacturingProcess")]
        public void Delete(int id)
        {
            var operationProcedure = _context.OperationProcedure.SingleOrDefault(p => p.OperationProcedureId == id);
            if (operationProcedure != null)
            {
                var operationProcedureLines = _context.OperationProcedureLine.Where(l => l.OperationProcedureId == id).ToList();
                if (operationProcedureLines.Count > 0)
                {
                    _context.OperationProcedureLine.RemoveRange(operationProcedureLines);
                    _context.SaveChanges();
                }
                _context.OperationProcedure.Remove(operationProcedure);
                _context.SaveChanges();
            }
        }
    }
}