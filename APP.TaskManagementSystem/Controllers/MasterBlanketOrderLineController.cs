﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterBlanketOrderLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MasterBlanketOrderLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetMasterBlanketOrderLines")]
        public List<MasterBlanketOrderLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var MasterBlanketOrders = _context.MasterBlanketOrderLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Where(s=>s.MasterBlanketOrderId == id)
                .AsNoTracking()
                .ToList();
            List<MasterBlanketOrderLineModel> masterBlanketOrderLineModels = new List<MasterBlanketOrderLineModel>();
            MasterBlanketOrders.ForEach(s =>
            {
                MasterBlanketOrderLineModel masterBlanketOrderLineModel = new MasterBlanketOrderLineModel
                {
                    MasterBlanketOrderId = s.MasterBlanketOrderId,
                    MasterBlanketOrderLineID = s.MasterBlanketOrderLineId,
                    NoOfWeeksPerMonth = s.NoOfWeeksPerMonth,
                    WeekOneStartDate = s.WeekOneStartDate,
                    WeekOneLargestOrder = s.WeekOneLargestOrder,
                    WeekTwoStartDate = s.WeekTwoStartDate,
                    WeekTwoLargestOrder =s.WeekTwoLargestOrder,
                    WeekThreeStartDate = s.WeekThreeStartDate,
                    WeekThreeLargestOrder = s.WeekThreeLargestOrder,
                    WeekFourStartDate = s.WeekFourStartDate,
                    WeekFourLargestOrder = s.WeekFourLargestOrder,
                    BalanceQtyForCalculate = s.BalanceQtyForCalculate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                masterBlanketOrderLineModels.Add(masterBlanketOrderLineModel);
            });
            return masterBlanketOrderLineModels.OrderByDescending(a => a.MasterBlanketOrderLineID).ToList();
        }

        [HttpPost()]
        [Route("GetMasterBlanketOrderLineData")]
        public ActionResult<MasterBlanketOrderLineModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var masterBlanketOrderLine = new MasterBlanketOrderLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).LastOrDefault();
                        break;
                    case "Previous":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderBy(o => o.MasterBlanketOrderLineId).FirstOrDefault(s => s.MasterBlanketOrderLineId > searchModel.Id);
                        break;
                    case "Previous":
                        masterBlanketOrderLine = _context.MasterBlanketOrderLine.OrderByDescending(o => o.MasterBlanketOrderLineId).FirstOrDefault(s => s.MasterBlanketOrderLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MasterBlanketOrderLineModel>(masterBlanketOrderLine);
            return result;
        }

        [HttpPost]
        [Route("InsertMasterBlanketOrderLine")]
        public MasterBlanketOrderLineModel Post(MasterBlanketOrderLineModel value)
        {
            var masterBlanketOrderLine = new MasterBlanketOrderLine
            {
                MasterBlanketOrderId = value.MasterBlanketOrderId,
                NoOfWeeksPerMonth = value.NoOfWeeksPerMonth,
                WeekOneStartDate = value.WeekOneStartDate,
                WeekOneLargestOrder = value.WeekOneLargestOrder,
                WeekTwoStartDate = value.WeekTwoStartDate,
                WeekTwoLargestOrder = value.WeekTwoLargestOrder,
                WeekThreeStartDate = value.WeekThreeStartDate,
                WeekThreeLargestOrder = value.WeekThreeLargestOrder,
                WeekFourStartDate = value.WeekFourStartDate,
                WeekFourLargestOrder = value.WeekFourLargestOrder,
                BalanceQtyForCalculate = value.BalanceQtyForCalculate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.MasterBlanketOrderLine.Add(masterBlanketOrderLine);
            _context.SaveChanges();
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == masterBlanketOrderLine.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            value.AddedDate = masterBlanketOrderLine.AddedDate;
            value.MasterBlanketOrderLineID = masterBlanketOrderLine.MasterBlanketOrderLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateMasterBlanketOrderLine")]
        public MasterBlanketOrderLineModel Put(MasterBlanketOrderLineModel value)
        {
            var masterBlanketOrderline = _context.MasterBlanketOrderLine.SingleOrDefault(p => p.MasterBlanketOrderLineId == value.MasterBlanketOrderLineID);
            masterBlanketOrderline.MasterBlanketOrderId = value.MasterBlanketOrderId;
            masterBlanketOrderline.NoOfWeeksPerMonth = value.NoOfWeeksPerMonth;
            masterBlanketOrderline.WeekOneStartDate = value.WeekOneStartDate;
            masterBlanketOrderline.WeekOneLargestOrder = value.WeekOneLargestOrder;
            masterBlanketOrderline.WeekTwoStartDate = value.WeekTwoStartDate;
            masterBlanketOrderline.WeekTwoLargestOrder = value.WeekTwoLargestOrder;
            masterBlanketOrderline.WeekThreeStartDate = value.WeekThreeStartDate;
            masterBlanketOrderline.WeekThreeLargestOrder = value.WeekThreeLargestOrder;
            masterBlanketOrderline.WeekFourStartDate = value.WeekFourStartDate;
            masterBlanketOrderline.WeekFourLargestOrder = value.WeekFourLargestOrder;
            masterBlanketOrderline.BalanceQtyForCalculate = value.BalanceQtyForCalculate;
            masterBlanketOrderline.StatusCodeId = value.StatusCodeID.Value;
            masterBlanketOrderline.ModifiedByUserId = value.ModifiedByUserID;
            masterBlanketOrderline.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteMasterBlanketOrderLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var masterBlanketOrderLine = _context.MasterBlanketOrderLine.Where(p => p.MasterBlanketOrderLineId == id).FirstOrDefault();
                if (masterBlanketOrderLine != null)
                {

                    _context.MasterBlanketOrderLine.Remove(masterBlanketOrderLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}