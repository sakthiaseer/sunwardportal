﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OrderRequirementController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public OrderRequirementController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetOrderRequirement")]
        public List<OrderRequirementModel> Get()
        {
            List<OrderRequirementModel> orderRequirementModels = new List<OrderRequirementModel>();
            var orderRequirement = _context.OrderRequirement
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("ManufacturingSite")
               .Include("SalesCategory")
               .Include("GenericCode")
               .Include("NavItem")
               .Include("MethodCode").OrderByDescending(o => o.OrderRequirementId).AsNoTracking().ToList();
            if(orderRequirement!=null && orderRequirement.Count>0)
            {
                orderRequirement.ForEach(s =>
                {
                    OrderRequirementModel orderRequirementModel = new OrderRequirementModel
                    {
                        OrderRequirementId = s.OrderRequirementId,
                        ManufacturingSiteId = s.ManufacturingSiteId,
                        ManufacturingSiteName = s.ManufacturingSite!=null? s.ManufacturingSite.Description : string.Empty,
                        SalesCategoryId = s.SalesCategoryId,
                        SalesCategory = s.SalesCategory!=null? s.SalesCategory.Description : string.Empty,
                        LocationID = s.SalesCategory != null ? s.SalesCategory.LocationId : null,
                        MethodCodeId = s.MethodCodeId,
                        MethodCodeName = s.MethodCode!=null? s.MethodCode.MethodName : string.Empty,
                        NavItemId = s.NavItemId,
                        NavItemName = s.NavItem!=null? s.NavItem.Description : string.Empty,
                        GenericCodeId = s.GenericCodeId,
                        GenericCodeName = s.GenericCode!=null? s.GenericCode.Code : string.Empty,
                        DistributorItemId = s.DistributorItemId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        IsNavSync = s.IsNavSync.GetValueOrDefault(false),

                    };
                    orderRequirementModels.Add(orderRequirementModel);
                });

            }
           
            return orderRequirementModels;
        }
        [HttpGet]
        [Route("GetOrderRequirementLine")]
        public List<OrderRequirementLineModel> GetOrderRequirementLine(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<OrderRequirementLineModel> orderRequirementLineModels = new List<OrderRequirementLineModel>();
            var orderRequirementLine = _context.OrderRequirementLine
              .Include(s=>s.AddedByUser)
              .Include(s=>s.ModifiedByUser)
              .Include(s=>s.StatusCode)
              .Include(s=>s.Product).Where(w => w.OrderRequirementId == id).OrderByDescending(o => o.OrderRequirementLineId).AsNoTracking().ToList();
            if(orderRequirementLine!=null && orderRequirementLine.Count>0)
            {
                orderRequirementLine.ForEach(s =>
                {
                    OrderRequirementLineModel orderRequirementLineModel = new OrderRequirementLineModel
                    {
                        OrderRequirementLineId = s.OrderRequirementLineId,
                        OrderRequirementId = s.OrderRequirementId,
                        ProductId = s.ProductId,
                        TicketBatchSizeId = s.TicketBatchSizeId,
                        TicketBatchSizeName = s.TicketBatchSizeId,
                        NavLocationId = s.NavLocationId,
                        NavUomid = s.NavUomid,
                        NavLocationName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.NavLocationId).Select(a => a.Value).SingleOrDefault() : "",
                        NavUomName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.NavUomid).Select(a => a.Value).SingleOrDefault() : "",
                        ProductQty = s.ProductQty,
                        NoOfTicket = s.NoOfTicket,
                        Remarks = s.Remarks,
                        ExpectedStartDate = s.ExpectedStartDate,
                        RequireToSplit = s.RequireToSplit,
                        RequireToSplitFlag = s.RequireToSplit == true ? "1" : "0",
                        ProductName =  s.Product?.Description + " " + s.Product?.Description2,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        IsNavSync = s.IsNavSync.GetValueOrDefault(false),
                    };
                    orderRequirementLineModels.Add(orderRequirementLineModel);
                });
            }            
            
            return orderRequirementLineModels;
        }
        [HttpGet]
        [Route("GetOrderRequirementLineSplit")]
        public List<OrderRequirementLineSplitModel> GetOrderRequirementLineSplit(int? id)
        {
            List<OrderRequirementLineSplitModel> orderRequirementLineSplitModels = new List<OrderRequirementLineSplitModel>();
            var orderRequirementLineSplit = _context.OrderRequirementLineSplit
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("SplitProduct").Where(a => a.OrderRequirementLineId == id).OrderByDescending(o => o.OrderRequirementLineSplitId).AsNoTracking().ToList();
            if(orderRequirementLineSplit!=null && orderRequirementLineSplit.Count>0)
            {
                orderRequirementLineSplit.ForEach(s =>
                {
                    OrderRequirementLineSplitModel orderRequirementLineSplitModel = new OrderRequirementLineSplitModel
                    {
                        OrderRequirementLineSplitId = s.OrderRequirementLineSplitId,
                        OrderRequirementLineId = s.OrderRequirementLineId,
                        ProductQty = s.ProductQty,
                        SplitProductQty = s.SplitProductQty,
                        Remarks = s.Remarks,
                        SplitProductName = s.SplitProduct!=null? s.SplitProduct.Description : string.Empty,
                        SplitProductId = s.SplitProductId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        IsNavSync = s.IsNavSync.GetValueOrDefault(false)
                    };
                    orderRequirementLineSplitModels.Add(orderRequirementLineSplitModel);
                });

            }          
            return orderRequirementLineSplitModels;
        }
        [HttpPost]
        [Route("GetOrderRequirementID")]
        public List<OrderRequirementModel> GetOrderRequirementID(OrderRequirementModel value)
        {
            var OrderRequirement = _context.OrderRequirement
                .Select(s => new OrderRequirementModel
                {
                    OrderRequirementId = s.OrderRequirementId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    SalesCategoryId = s.SalesCategoryId,
                    MethodCodeId = s.MethodCodeId,
                    NavItemId = s.NavItemId,
                    GenericCodeId = s.GenericCodeId,
                    DistributorItemId = s.DistributorItemId,
                    IsNavSync = s.IsNavSync.GetValueOrDefault(false)

                }).Where(w => w.NavItemId == value.NavItemId && w.ManufacturingSiteId == value.ManufacturingSiteId && w.GenericCodeId == value.GenericCodeId && w.MethodCodeId == value.MethodCodeId).OrderByDescending(o => o.OrderRequirementId).AsNoTracking().ToList();
            return OrderRequirement;
        }
        [HttpPost]
        [Route("InsertOrderRequirement")]
        public OrderRequirementModel Post(OrderRequirementModel value)
        {
            var OrderRequirementInsert = new OrderRequirement
            {
                ManufacturingSiteId = value.ManufacturingSiteId,
                SalesCategoryId = value.SalesCategoryId,
                MethodCodeId = value.MethodCodeId,
                NavItemId = value.NavItemId,
                GenericCodeId = value.GenericCodeId,
                DistributorItemId = value.DistributorItemId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.OrderRequirement.Add(OrderRequirementInsert);
            _context.SaveChanges();
            
            value.OrderRequirementId = OrderRequirementInsert.OrderRequirementId;

            return value;
        }
        [HttpPut]
        [Route("UpdateOrderRequirement")]
        public OrderRequirementModel Put(OrderRequirementModel value)
        {
            var OrderRequirement = _context.OrderRequirement.SingleOrDefault(p => p.OrderRequirementId == value.OrderRequirementId);
            OrderRequirement.ManufacturingSiteId = value.ManufacturingSiteId;
            OrderRequirement.SalesCategoryId = value.SalesCategoryId;
            OrderRequirement.MethodCodeId = value.MethodCodeId;
            OrderRequirement.NavItemId = value.NavItemId;
            OrderRequirement.GenericCodeId = value.GenericCodeId;
            OrderRequirement.DistributorItemId = value.DistributorItemId;
            OrderRequirement.ModifiedByUserId = value.ModifiedByUserID;
            OrderRequirement.StatusCodeId = value.StatusCodeID.Value;
            OrderRequirement.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertOrderRequirementLine")]
        public OrderRequirementLineModel InsertOrderRequirementLine(OrderRequirementLineModel value)
        {
            var OrderRequirementLine = new OrderRequirementLine
            {
                OrderRequirementId = value.OrderRequirementId,
                ProductId = value.ProductId,
                TicketBatchSizeId = value.TicketBatchSizeId,
                ProductQty = value.ProductQty,
                NoOfTicket = value.NoOfTicket,
                NavUomid = value.NavUomid,
                NavLocationId = value.NavLocationId,
                ExpectedStartDate = value.ExpectedStartDate,
                RequireToSplit = value.RequireToSplitFlag == "1" ? true : false,
                Remarks = value.Remarks,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                IsNavSync = false,
            };
            _context.OrderRequirementLine.Add(OrderRequirementLine);

            var orderRequirement = _context.OrderRequirement.FirstOrDefault(f => f.OrderRequirementId == value.OrderRequirementId);
            orderRequirement.IsNavSync = false;

            _context.SaveChanges();
            value.OrderRequirementLineId = OrderRequirementLine.OrderRequirementLineId;
            value.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            if (value.ProductId != null && value.ProductId > 0)
            {
                var product = _context.Navitems.Where(s => s.ItemId == value.ProductId).FirstOrDefault();
                if(product!=null)
                {
                    value.ProductName = product.Description + " " + product.Description2;
                }
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateOrderRequirementLine")]
        public OrderRequirementLineModel UpdateOrderRequirementLine(OrderRequirementLineModel value)
        {
            var OrderRequirementLine = _context.OrderRequirementLine.SingleOrDefault(p => p.OrderRequirementLineId == value.OrderRequirementLineId);
            OrderRequirementLine.OrderRequirementId = value.OrderRequirementId;
            OrderRequirementLine.ProductId = value.ProductId;
            OrderRequirementLine.ProductQty = value.ProductQty;
            OrderRequirementLine.TicketBatchSizeId = value.TicketBatchSizeId;
            OrderRequirementLine.NoOfTicket = value.NoOfTicket;
            OrderRequirementLine.NavLocationId = value.NavLocationId;
            OrderRequirementLine.NavUomid = value.NavUomid;
            OrderRequirementLine.ExpectedStartDate = value.ExpectedStartDate;
            OrderRequirementLine.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            OrderRequirementLine.Remarks = value.Remarks;
            OrderRequirementLine.ModifiedByUserId = value.ModifiedByUserID;
            OrderRequirementLine.StatusCodeId = value.StatusCodeID.Value;
            OrderRequirementLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.RequireToSplit = value.RequireToSplitFlag == "1" ? true : false;
            if (value.ProductId != null && value.ProductId > 0)
            {
                var product = _context.Navitems.Where(s => s.ItemId == value.ProductId).FirstOrDefault();
                if (product != null)
                {
                    value.ProductName = product.Description + " " + product.Description2;
                }
            }
            return value;

        }
        [HttpPost]
        [Route("InsertOrderRequirementLineSplit")]
        public OrderRequirementLineSplitModel InsertOrderRequirementLineSplit(OrderRequirementLineSplitModel value)
        {
            var OrderRequirementLineSplit = new OrderRequirementLineSplit
            {
                OrderRequirementLineId = value.OrderRequirementLineId,
                ProductQty = value.ProductQty,
                SplitProductQty = value.SplitProductQty,
                Remarks = value.Remarks,
                SplitProductId = value.SplitProductId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsNavSync = false,
            };
            _context.OrderRequirementLineSplit.Add(OrderRequirementLineSplit);
            _context.SaveChanges();
            value.OrderRequirementLineSplitId = OrderRequirementLineSplit.OrderRequirementLineSplitId;
            return value;
        }
        [HttpPut]
        [Route("UpdateOrderRequirementLineSplit")]
        public OrderRequirementLineSplitModel UpdateOrderRequirementLineSplit(OrderRequirementLineSplitModel value)
        {
            var OrderRequirementLineSplit = _context.OrderRequirementLineSplit.SingleOrDefault(p => p.OrderRequirementLineSplitId == value.OrderRequirementLineSplitId);
            OrderRequirementLineSplit.OrderRequirementLineId = value.OrderRequirementLineId;
            OrderRequirementLineSplit.ProductQty = value.ProductQty;
            OrderRequirementLineSplit.SplitProductQty = value.SplitProductQty;
            OrderRequirementLineSplit.Remarks = value.Remarks;
            OrderRequirementLineSplit.SplitProductId = value.SplitProductId;
            OrderRequirementLineSplit.ModifiedByUserId = value.ModifiedByUserID;
            OrderRequirementLineSplit.StatusCodeId = value.StatusCodeID.Value;
            OrderRequirementLineSplit.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteOrderRequirementLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var OrderRequirementLine = _context.OrderRequirementLine.SingleOrDefault(p => p.OrderRequirementLineId == id);
                if (OrderRequirementLine != null)
                {
                    var OrderRequirementLineSplit = _context.OrderRequirementLineSplit.Where(s => s.OrderRequirementLineId == id).AsNoTracking().ToList();
                    if (OrderRequirementLineSplit != null)
                    {
                        _context.OrderRequirementLineSplit.RemoveRange(OrderRequirementLineSplit);
                        _context.SaveChanges();
                    }
                    _context.OrderRequirementLine.Remove(OrderRequirementLine);
                    _context.SaveChanges();

                    var SyncOrderRequirementLine = _context.OrderRequirementLine.Where(f => f.IsNavSync == false && f.OrderRequirementId == OrderRequirementLine.OrderRequirementId).AsNoTracking().ToList();
                    if (SyncOrderRequirementLine.Count == 0)
                    {
                        var orderrequirement = _context.OrderRequirement.FirstOrDefault(f => f.OrderRequirementId == OrderRequirementLine.OrderRequirementId);
                        orderrequirement.IsNavSync = true;
                        _context.SaveChanges();
                    }
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteOrderRequirementLineSplit")]
        public ActionResult<string> DelDeleteOrderRequirementLineSplitete(int id)
        {
            try
            {
                var OrderRequirementLineSplit = _context.OrderRequirementLineSplit.SingleOrDefault(p => p.OrderRequirementLineSplitId == id);
                if (OrderRequirementLineSplit != null)
                {
                    _context.OrderRequirementLineSplit.Remove(OrderRequirementLineSplit);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}