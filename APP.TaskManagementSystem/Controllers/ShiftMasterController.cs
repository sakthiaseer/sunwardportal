﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ShiftMasterController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ShiftMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetShiftMasters")]
        public List<ShiftMasterModel> Get()
        {
            var shiftMaster = _context.ShiftMaster.Include("AddedByUser").Include("ModifiedByUser").Select(s => new ShiftMasterModel
            {
                ShiftID = s.ShiftId,
                Name = s.Name,
                Code = s.Code,
                StartTime = s.StartTime,
                EndTime = s.EndTime,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ShiftID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ShiftMasterModel>>(ShiftMaster);
            return shiftMaster;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get ShiftMaster")]
        [HttpGet("GetShiftMasters/{id:int}")]
        public ActionResult<ShiftMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var shiftMaster = _context.ShiftMaster.SingleOrDefault(p => p.ShiftId == id.Value);
            var result = _mapper.Map<ShiftMasterModel>(shiftMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ShiftMasterModel> GetData(SearchModel searchModel)
        {
            var shiftMaster = new ShiftMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).FirstOrDefault();
                        break;
                    case "Last":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).LastOrDefault();
                        break;
                    case "Next":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).LastOrDefault();
                        break;
                    case "Previous":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).FirstOrDefault();
                        break;
                    case "Last":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).LastOrDefault();
                        break;
                    case "Next":
                        shiftMaster = _context.ShiftMaster.OrderBy(o => o.ShiftId).FirstOrDefault(s => s.ShiftId > searchModel.Id);
                        break;
                    case "Previous":
                        shiftMaster = _context.ShiftMaster.OrderByDescending(o => o.ShiftId).FirstOrDefault(s => s.ShiftId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ShiftMasterModel>(shiftMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertShiftMaster")]
        public ShiftMasterModel Post(ShiftMasterModel value)
        {
            var shiftMaster = new ShiftMaster
            {
                //ShiftMasterId = value.ShiftMasterID,
                Name = value.Name,
                Code = value.Code,
                StartTime = DateTime.Now,
                EndTime = DateTime.Now,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };
            _context.ShiftMaster.Add(shiftMaster);
            _context.SaveChanges();
            value.ShiftID = shiftMaster.ShiftId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateShiftMaster")]
        public ShiftMasterModel Put(ShiftMasterModel value)
        {
            var shiftMaster = _context.ShiftMaster.SingleOrDefault(p => p.ShiftId == value.ShiftID);
            //ShiftMaster.ShiftMasterId = value.ShiftMasterID;
            shiftMaster.ModifiedByUserId = value.ModifiedByUserID;
            shiftMaster.ModifiedDate = DateTime.Now;
            shiftMaster.Name = value.Name;
            shiftMaster.Code = value.Code;
            shiftMaster.StartTime = value.StartTime;
            shiftMaster.EndTime = value.EndTime;
            //ShiftMaster.AddedByUserId = value.AddedByUserID;
            // ShiftMaster.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            shiftMaster.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteShiftMaster")]
        public void Delete(int id)
        {
            var shiftMaster = _context.ShiftMaster.SingleOrDefault(p => p.ShiftId == id);
            if (shiftMaster != null)
            {
                _context.ShiftMaster.Remove(shiftMaster);
                _context.SaveChanges();
            }
        }
    }
}