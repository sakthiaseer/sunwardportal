﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentUserRoleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentUserRoleController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentUserRoles")]
        public List<DocumentUserRoleModel> Get()
        {
            var documentUserRole = _context.DocumentUserRole.AsNoTracking().ToList();
            List<DocumentUserRoleModel> documentUserRoleModel = new List<DocumentUserRoleModel>();
            documentUserRole.ForEach(s =>
            {
                DocumentUserRoleModel documentUserRoleModels = new DocumentUserRoleModel
                {
                    DocumentUserRoleID = s.DocumentUserRoleId,
                    UserID = s.UserId,
                    RoleID = s.RoleId,
                    FolderID = s.FolderId,
                    DocumentID = s.DocumentId,
                    UserGroupID = s.UserGroupId,
                    IsFolderLevel = s.IsFolderLevel,
                    LevelID = s.LevelId,
                };
                documentUserRoleModel.Add(documentUserRoleModels);
            });
            return documentUserRoleModel.OrderByDescending(a => a.DocumentUserRoleID).ToList();
        }

        [HttpGet]
        [Route("GetDocumentUserRolesByFolderID")]
        public List<DocumentUserRoleModel> GetByFolderID(int id)
        {
            var documentUserRole = _context.DocumentUserRole.AsNoTracking().ToList();
            List<DocumentUserRoleModel> documentUserRoleModel = new List<DocumentUserRoleModel>();
            documentUserRole.ForEach(s =>
            {
                DocumentUserRoleModel documentUserRoleModels = new DocumentUserRoleModel
                {
                    DocumentUserRoleID = s.DocumentUserRoleId,
                    UserID = s.UserId,
                    RoleID = s.RoleId,
                    RoleName = s.Role.DocumentRoleName,
                    UserName = s.User.UserName,
                    UserGroupName = s.UserGroup.Name,
                    FolderID = s.FolderId,
                    DocumentID = s.DocumentId,
                    UserGroupID = s.UserGroupId,
                    IsFolderLevel = s.IsFolderLevel,
                    LevelID = s.LevelId,
                };
                documentUserRoleModel.Add(documentUserRoleModels);
            });
            return documentUserRoleModel.OrderByDescending(a => a.DocumentUserRoleID).Where(o => o.FolderID == id && o.DocumentID == null).ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get DocumentUserRole")]
        [HttpGet("GetDocumentUserRoles/{id:int}")]
        public ActionResult<DocumentUserRoleModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentUserRole = _context.DocumentUserRole.SingleOrDefault(p => p.DocumentUserRoleId == id.Value);
            var result = _mapper.Map<DocumentUserRoleModel>(documentUserRole);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentUserRoleModel> GetData(SearchModel searchModel)
        {
            var documentUserRole = new DocumentUserRole();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).FirstOrDefault();
                        break;
                    case "Last":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).LastOrDefault();
                        break;
                    case "Next":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).LastOrDefault();
                        break;
                    case "Previous":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).FirstOrDefault();
                        break;
                    case "Last":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).LastOrDefault();
                        break;
                    case "Next":
                        documentUserRole = _context.DocumentUserRole.OrderBy(o => o.DocumentUserRoleId).FirstOrDefault(s => s.DocumentUserRoleId > searchModel.Id);
                        break;
                    case "Previous":
                        documentUserRole = _context.DocumentUserRole.OrderByDescending(o => o.DocumentUserRoleId).FirstOrDefault(s => s.DocumentUserRoleId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentUserRoleModel>(documentUserRole);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentUserRole")]
        public DocumentUserRoleModel Post(DocumentUserRoleModel value)
        {
            var documentUserRole = new DocumentUserRole
            {
                UserId = value.UserID,
                RoleId = value.RoleID,
                UserGroupId = value.UserGroupID,
                DocumentId = value.DocumentID,
                IsFolderLevel = value.IsFolderLevel,
                LevelId = value.LevelID,
            };
            _context.DocumentUserRole.Add(documentUserRole);
            _context.SaveChanges();
            value.DocumentUserRoleID = documentUserRole.DocumentUserRoleId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentUserRole")]
        public DocumentUserRoleModel Put(DocumentUserRoleModel value)
        {
            var documentUserRole = _context.DocumentUserRole.SingleOrDefault(p => p.DocumentUserRoleId == value.DocumentUserRoleID);
            //DocumentUserRole.DocumentUserRoleId = value.DocumentUserRoleID;
            documentUserRole.UserId = value.UserID;
            documentUserRole.RoleId = value.RoleID;
            documentUserRole.UserGroupId = value.UserGroupID;
            documentUserRole.DocumentId = value.DocumentID;
            documentUserRole.IsFolderLevel = value.IsFolderLevel;
            documentUserRole.LevelId = value.LevelID;
            var selectDocumentRoleIds = value.DocumetUserRoleIDs;
            if (selectDocumentRoleIds.Count > 0)
            {

            }
            _context.SaveChanges();

            return value;
        }

        [HttpPut]
        [Route("DeleteDocumentUserRoles")]
        public DocumentUserRoleModel DeleteDocumentUserRole(DocumentUserRoleModel value)
        {

            var selectDocumentRoleIds = value.DocumetUserRoleIDs;
            if (selectDocumentRoleIds.Count > 0)
            {
                selectDocumentRoleIds.ForEach(sel =>
                {
                    var documentUserRole = _context.DocumentUserRole.Where(p => p.DocumentUserRoleId == sel).FirstOrDefault();
                    if (documentUserRole != null)
                    {


                        _context.DocumentUserRole.Remove(documentUserRole);
                        _context.SaveChanges();
                        var documentExistingUser = _context.DocumentUserRole.Where(p => p.UserId == documentUserRole.UserId && p.FolderId == documentUserRole.FolderId).ToList();
                        if (documentExistingUser != null && documentExistingUser.Count > 0)
                        {
                            _context.DocumentUserRole.RemoveRange(documentExistingUser);
                            _context.SaveChanges();
                        }

                        // _context.SaveChanges();
                    }
                });

                _context.SaveChanges();
            }


            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentUserRole")]
        public void Delete(int id)
        {
            var documentUserRole = _context.DocumentUserRole.SingleOrDefault(p => p.DocumentUserRoleId == id);
            if (documentUserRole != null)
            {
                _context.DocumentUserRole.Remove(documentUserRole);
                _context.SaveChanges();
            }
        }
    }
}