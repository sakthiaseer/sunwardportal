﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SalesOrderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        [HttpGet]
        [Route("GetSalesOrders")]
        public List<SalesOrderModel> Get()
        {
                       
            var salesOrders = _context.SalesOrder
                .Include(p => p.Profile)
                .Include(a => a.AddedByUser)
                .Include(c => c.Customer)
                .Include(s => s.PurchaseOrderIssue)
                .Include(s => s.ShipToCode)
                 .Include(s => s.ShipToCode.SobyCustomersMasterAddress)
                .Include(c => c.StatusCode)
                .Include(a => a.ModifiedByUser)
                .Include(z => z.SobyCustomersSalesAddress)
                 .Include(z => z.SobyCustomersSalesAddress.SobyCustomersMasterAddress)
                .AsNoTracking()
                .ToList();
          
            List<SalesOrderModel> salesOrderModels = new List<SalesOrderModel>();
            salesOrders.ForEach(s =>
            {
                SalesOrderModel salesOrderModel = new SalesOrderModel();
                salesOrderModel.SalesOrderId = s.SalesOrderId;
                salesOrderModel.ProfileName = s.Profile?.Name;
                salesOrderModel.ProfileId = s.ProfileId;
                salesOrderModel.DocumentNo = s.DocumentNo;
                salesOrderModel.DateOfOrder = s.DateOfOrder;
                salesOrderModel.Ponumber = s.Ponumber;
                salesOrderModel.PurchaseOrderIssueId = s.PurchaseOrderIssueId;
                salesOrderModel.PurchaseOrderIssue = s.PurchaseOrderIssue?.PlantCode;
                salesOrderModel.CustomerId = s.CustomerId;
                salesOrderModel.Customer = s.Customer?.CompanyName;
                salesOrderModel.RequestShipmentDate = s.RequestShipmentDate;
                salesOrderModel.ShipingCodeType = s.ShipingCodeType;
                if (s.ShipingCodeType == "SobyCustomersAddress")
                {
                    salesOrderModel.ShipToCodeId = s.ShipToCode.SobyCustomersMasterAddressId;
                    salesOrderModel.ShipToCode = s.ShipToCode != null ? (s.ShipToCode.LocationNo + "|" + s.ShipToCode.LocationName) : "";
                }
                if (s.ShipingCodeType == "SOByCustomersSalesAddress")
                {
                    salesOrderModel.ShipToCodeId = s.ShipToCode.SobyCustomersMasterAddressId;
                    salesOrderModel.ShipToCode = s.SobyCustomersSalesAddress != null && s.SobyCustomersSalesAddress.SobyCustomersMasterAddress != null ? s.SobyCustomersSalesAddress.SobyCustomersMasterAddress.Address : "";

                }
                salesOrderModel.VanDeliveryDate = s.VanDeliveryDate;
                salesOrderModel.StatusCodeID = s.StatusCodeId;
                salesOrderModel.AddedByUserID = s.AddedByUserId;
                salesOrderModel.ModifiedByUserID = s.ModifiedByUserId;
                salesOrderModel.AddedDate = s.AddedDate;
                salesOrderModel.ModifiedDate = s.ModifiedDate;
                salesOrderModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";

                salesOrderModels.Add(salesOrderModel);
            });
            return salesOrderModels.OrderByDescending(a => a.SalesOrderId).ToList();
        }
        [HttpGet]
        [Route("GetSalesOrderByID")]
        public SalesOrderModel GetSalesOrderByID(int id)
        {
          
            var s = _context.SalesOrder
                .Include(p => p.Profile)
                .Include(a => a.AddedByUser)
                .Include(c => c.Customer)
                .Include(s => s.PurchaseOrderIssue)
                .Include(s => s.ShipToCode)
                .Include(c => c.StatusCode)
                .Include(a => a.ModifiedByUser)
                .Include(z => z.SobyCustomersSalesAddress).Where(w => w.SalesOrderId == id)
                .AsNoTracking()
                .FirstOrDefault();
            SalesOrderModel salesOrderModel = new SalesOrderModel();
            if (s != null)
            {
                salesOrderModel.SalesOrderId = s.SalesOrderId;
                salesOrderModel.ProfileName = s.Profile?.Name;
                salesOrderModel.ProfileId = s.ProfileId;
                salesOrderModel.DocumentNo = s.DocumentNo;
                salesOrderModel.DateOfOrder = s.DateOfOrder;
                salesOrderModel.Ponumber = s.Ponumber;
                salesOrderModel.PurchaseOrderIssueId = s.PurchaseOrderIssueId;
                salesOrderModel.PurchaseOrderIssue = s.PurchaseOrderIssue?.PlantCode;
                salesOrderModel.CustomerId = s.CustomerId;
                salesOrderModel.Customer = s.Customer?.CompanyName;
                salesOrderModel.RequestShipmentDate = s.RequestShipmentDate;
                salesOrderModel.ShipingCodeType = s.ShipingCodeType;
                if (s.ShipingCodeType == "SobyCustomersAddress")
                {
                    salesOrderModel.ShipToCodeId = s.ShipToCodeId;
                    salesOrderModel.ShipToCode = s.ShipToCode != null ? (s.ShipToCode.LocationNo + "|" + s.ShipToCode.LocationName) : "";
                }
                if (s.ShipingCodeType == "SOByCustomersSalesAddress")
                {
                    salesOrderModel.ShipToCodeId = s.SobyCustomersSalesAddressId;
                    //salesOrderModel.ShipToCode = s.SobyCustomersSalesAddress != null ? s.SobyCustomersSalesAddress.Address : "";

                }
                salesOrderModel.VanDeliveryDate = s.VanDeliveryDate;
                salesOrderModel.StatusCodeID = s.StatusCodeId;
                salesOrderModel.AddedByUserID = s.AddedByUserId;
                salesOrderModel.ModifiedByUserID = s.ModifiedByUserId;
                salesOrderModel.AddedDate = s.AddedDate;
                salesOrderModel.ModifiedDate = s.ModifiedDate;
                salesOrderModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
            }
            return salesOrderModel;
        }
        [HttpPost()]
        [Route("GetSalesOrderData")]
        public ActionResult<SalesOrderModel> GetSalesOrderData(SearchModel searchModel)
        {
            var salesOrder = new SalesOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).LastOrDefault();
                        break;
                    case "Next":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).LastOrDefault();
                        break;
                    case "Next":
                        salesOrder = _context.SalesOrder.OrderBy(o => o.SalesOrderId).FirstOrDefault(s => s.SalesOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        salesOrder = _context.SalesOrder.OrderByDescending(o => o.SalesOrderId).FirstOrDefault(s => s.SalesOrderId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SalesOrderModel>(salesOrder);
            return result;
        }

        [HttpPost]
        [Route("InsertSalesOrder")]
        public SalesOrderModel Post(SalesOrderModel value)
        {
            var profileNo = "";
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Ponumber });
            }
            var salesOrder = new SalesOrder();
            salesOrder.ProfileId = value.ProfileId;
            salesOrder.DocumentNo = profileNo;
            salesOrder.DateOfOrder = value.DateOfOrder;
            salesOrder.Ponumber = value.Ponumber;
            salesOrder.PurchaseOrderIssueId = value.PurchaseOrderIssueId;
            salesOrder.CustomerId = value.CustomerId;
            salesOrder.RequestShipmentDate = value.RequestShipmentDate;
            salesOrder.ShipingCodeType = value.ShipingCodeType;
            salesOrder.ShipToCodeId = value.ShipingCodeType == "SobyCustomersAddress" ? value.ShipToCodeId : null;
            salesOrder.SobyCustomersSalesAddressId = value.ShipingCodeType == "SOByCustomersSalesAddress" ? value.ShipToCodeId : null;
            salesOrder.VanDeliveryDate = value.VanDeliveryDate;
            salesOrder.StatusCodeId = value.StatusCodeID.Value;
            salesOrder.AddedByUserId = value.AddedByUserID;
            salesOrder.AddedDate = DateTime.Now;    
            _context.SalesOrder.Add(salesOrder);
            _context.SaveChanges();
            value.SalesOrderId = salesOrder.SalesOrderId;
            value.ProfileName = _context.DocumentProfileNoSeries.Where(s => s.ProfileId == value.ProfileId).Select(s => s.Name).FirstOrDefault();
            List<long?> soCusomerId = SelectedCustomerList(value.CustomerId);
            if (soCusomerId != null && soCusomerId.Count > 0)
            {
                soCusomerId.ForEach(so =>
                {

                    var salesOrderEntries = _context.SalesOrderEntry.Include(p => p.PurchaseItemSalesEntryLine).Where(c => c.CustomerId == so && (c.OrderById == 1403 || c.TypeOfOrderId == 1403 || c.TypeOfOrderId == 1651) && c.StatusCodeId != 1421);

                    if (salesOrderEntries.Any())
                    {
                        salesOrderEntries.ToList().ForEach(s =>
                        {
                            if (s.PurchaseItemSalesEntryLine != null && s.PurchaseItemSalesEntryLine.Any())
                            {
                                s.PurchaseItemSalesEntryLine.Where(i => i.StatusCodeId == 1422).ToList().ForEach(p =>
                                {
                                    SalesOrderProduct salesOrderProduct = new SalesOrderProduct();
                                    salesOrderProduct.SalesOrderId = salesOrder.SalesOrderId;
                                    salesOrderProduct.CrossReferenceProductId = p.SobyCustomersId;
                                    salesOrderProduct.AddedDate = DateTime.Now;
                                    salesOrderProduct.StatusCodeId = 1422;
                                    salesOrderProduct.AddedByUserId = value.AddedByUserID;
                                    salesOrderProduct.PurchaseItemId = p.PurchaseItemSalesEntryLineId;
                                    salesOrderProduct.SalesOrderEntryId = s.SalesOrderEntryId;
                                    _context.SalesOrderProduct.Add(salesOrderProduct);
                                });
                            }
                        });
                    }
                    _context.SaveChanges();
                });
            }

            //if (IsValidCustomer(value.CustomerId))
            //{
            //    var salesOrderEntries = _context.SalesOrderEntry.Include(p => p.PurchaseItemSalesEntryLine).Where(c => c.CustomerId == value.CustomerId && (c.OrderById == 1403 || c.TypeOfOrderId == 1403));

            //    if (salesOrderEntries.Any())
            //    {
            //        salesOrderEntries.ToList().ForEach(s =>
            //        {
            //            if (s.PurchaseItemSalesEntryLine != null && s.PurchaseItemSalesEntryLine.Any())
            //            {
            //                s.PurchaseItemSalesEntryLine.Where(i => i.StatusCodeId == 1422).ToList().ForEach(p =>
            //                    {
            //                        SalesOrderProduct salesOrderProduct = new SalesOrderProduct();
            //                        salesOrderProduct.SalesOrderId = salesOrder.SalesOrderId;
            //                        salesOrderProduct.CrossReferenceProductId = p.SobyCustomersId;
            //                        salesOrderProduct.AddedDate = DateTime.Now;
            //                        salesOrderProduct.StatusCodeId = 1422;
            //                        salesOrderProduct.AddedByUserId = value.AddedByUserID;
            //                        salesOrderProduct.PurchaseItemId = p.PurchaseItemSalesEntryLineId;
            //                        salesOrderProduct.SalesOrderEntryId = s.SalesOrderEntryId;
            //                        _context.SalesOrderProduct.Add(salesOrderProduct);
            //                    });
            //            }
            //        });
            //    }
            //    _context.SaveChanges();
            //}
            value.DocumentNo = salesOrder.DocumentNo;
            if (value.CustomerId > 0)
            {
                value.Customer = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.ShipToCodeId > 0)
            {
                value.ShipToCode = _context.SobyCustomersAddress.Include(t=>t.SobyCustomersMasterAddress).Where(s => s.SobyCustomersAddressId == value.ShipToCodeId).Select(s => s.SobyCustomersMasterAddress.PostalCode).FirstOrDefault();
            }
            if (value.PurchaseOrderIssueId > 0)
            {
                value.PurchaseOrderIssue = _context.Plant.Where(s => s.PlantId == value.PurchaseOrderIssueId).Select(s => s.PlantCode).FirstOrDefault();
            }
            if (value.ProfileId > 0)
            {
                value.ProfileName = _context.DocumentProfileNoSeries.Where(s => s.ProfileId == value.ProfileId).Select(s => s.Name).FirstOrDefault();
            }

            value.AddedDate = salesOrder.AddedDate;
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == salesOrder.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }

        private bool IsValidCustomer(long? cusomerId)
        {
            if (cusomerId == null)
            {
                return false;
            }
            var applicationMasterDetail = _context.ApplicationMasterDetail.FirstOrDefault(d => d.Value.Contains("By Tender Agency"));
            var companyListing = _context.CompanyListing.FirstOrDefault(c => c.CompanyListingId == cusomerId);
            var soCustomers = _context.SobyCustomers.Include(s => s.SocustomersIssue).Include("SocustomersIssue.SobyCustomersManner").Where(c => c.LinkProfileReferenceNo == companyListing.ProfileReferenceNo).ToList();

            foreach (var s in soCustomers)
            {
                if (s.SocustomersIssue != null && s.SocustomersIssue.Any())
                {
                    foreach (var i in s.SocustomersIssue.ToList())
                    {
                        if (i.SobyCustomersManner != null && i.SobyCustomersManner.Any(m => m.MannersId == applicationMasterDetail.ApplicationMasterDetailId))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private List<long?> SelectedCustomerList(long? cusomerId)
        {
            List<long?> socustomerissueIds = new List<long?>();
            var companyListing = _context.CompanyListing.FirstOrDefault(c => c.CompanyListingId == cusomerId);
            var centralizedApplicationMasterId = _context.ApplicationMasterDetail.Where(d => d.Value.Contains("Centralized")).Select(s => s.ApplicationMasterDetailId).FirstOrDefault();
            if (companyListing != null)
            {
                var SocustomersIssueId = _context.SobyCustomers.Include(s => s.SocustomersIssue).Where(c => c.LinkProfileReferenceNo == companyListing.ProfileReferenceNo).Select(s => s.SobyCustomersId).FirstOrDefault();
                if (SocustomersIssueId > 0)
                {
                    var SOByCustomersManner = _context.SobyCustomersManner.Include(s => s.SocustomersIssue).Where(m => m.MannersId == centralizedApplicationMasterId && m.SocustomersIssue.SocustomersId == SocustomersIssueId).ToList();

                    if (SOByCustomersManner != null)
                    {
                        SOByCustomersManner.ForEach(h =>
                        {
                            if (h.SocustomersIssue != null)
                            {
                                socustomerissueIds.Add(h.SocustomersIssue.CentralizedWithId);
                            }
                        });
                    }
                }
            }
            return socustomerissueIds;
        }
        [HttpPut]
        [Route("UpdateSalesOrder")]
        public SalesOrderModel Put(SalesOrderModel value)
        {
            var salesOrder = _context.SalesOrder.SingleOrDefault(p => p.SalesOrderId == value.SalesOrderId);
            salesOrder.ProfileId = value.ProfileId;
            salesOrder.DateOfOrder = value.DateOfOrder;
            salesOrder.Ponumber = value.Ponumber;
            salesOrder.PurchaseOrderIssueId = value.PurchaseOrderIssueId;
            salesOrder.CustomerId = value.CustomerId;
            salesOrder.RequestShipmentDate = value.RequestShipmentDate;
            salesOrder.ShipingCodeType = value.ShipingCodeType;
            salesOrder.ShipToCodeId = value.ShipingCodeType == "SobyCustomersAddress" ? value.ShipToCodeId : null;
            salesOrder.SobyCustomersSalesAddressId = value.ShipingCodeType == "SOByCustomersSalesAddress" ? value.ShipToCodeId : null;
            salesOrder.VanDeliveryDate = value.VanDeliveryDate;
            salesOrder.StatusCodeId = value.StatusCodeID.Value;
            salesOrder.ModifiedByUserId = value.ModifiedByUserID;
            salesOrder.ModifiedDate = DateTime.Now;
            salesOrder.DateOfOrder = value.DateOfOrder;
            salesOrder.Ponumber = value.Ponumber;
            _context.SaveChanges();
            value.ProfileName = _context.DocumentProfileNoSeries.Where(s => s.ProfileId == value.ProfileId).Select(s => s.Name).FirstOrDefault();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesOrder")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var salesOrder = _context.SalesOrder.Where(p => p.SalesOrderId == id).FirstOrDefault();
                if (salesOrder != null)
                {
                    var salesOrderProducts = _context.SalesOrderProduct.Where(p => p.SalesOrderId == salesOrder.SalesOrderId).ToList();
                    if (salesOrderProducts != null)
                    {
                        _context.SalesOrderProduct.RemoveRange(salesOrderProducts);
                        _context.SaveChanges();
                    }
                    var sowithOutBlanketOrders = _context.SowithOutBlanketOrder.Where(p => p.SalesOrderId == salesOrder.SalesOrderId).ToList();
                    if (sowithOutBlanketOrders != null)
                    {
                        _context.SowithOutBlanketOrder.RemoveRange(sowithOutBlanketOrders);
                        _context.SaveChanges();
                    }
                    _context.SalesOrder.Remove(salesOrder);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
