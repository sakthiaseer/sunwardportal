﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class StartOfDayController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public StartOfDayController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetStartOfDays")]
        public List<StartOfDayModel> Get()
        {
            List<StartOfDayModel> startOfDayModels = new List<StartOfDayModel>();
          
            var employeelist = _context.Employee.Include(e=>e.User).AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var startOfDays = _context.StartOfDay.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).AsNoTracking().ToList();
            var OperationProcedureLine = _context.OperationProcedureLine.Include(o => o.OperationProcedure).AsNoTracking().ToList();
            startOfDays.ForEach(s =>
            {
                var startOfDayModel = new StartOfDayModel();

                startOfDayModel.StartOfDayId = s.StartOfDayId;
                startOfDayModel.CompanyDbid = s.CompanyDbid;
                startOfDayModel.WorkingBlockId = s.WorkingBlockId;
                startOfDayModel.WorkingBlock = s.WorkingBlockId !=null && applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WorkingBlockId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WorkingBlockId).Value : "";
                startOfDayModel.CompanyName =s.CompanyDbid!=null && applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CompanyDbid) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CompanyDbid).Value : "";
                var ProdTaskIds = s.StartOfDayProdTaskMultiple != null ? s.StartOfDayProdTaskMultiple.Where(w=>w.StartOfDayId==s.StartOfDayId).Select(s => s.ProdTaskPerformId.Value).ToList() : new List<long>();
                if(ProdTaskIds!=null && ProdTaskIds.Count>0)
                {
                    var procedureIds = OperationProcedureLine.Where(o => ProdTaskIds.Contains(o.OperationProcedureLineId)).Select(p => p.OprocedureId).ToList();
                    startOfDayModel.ProdTask = applicationMasterDetails != null ? string.Join(" | ", applicationMasterDetails.Where(m => procedureIds.Contains(m.ApplicationMasterDetailId)).Select(s=>s.Value)):"";
                }
                var ManPowerIds = s.StartOfDayManPowerMultiple != null ? s.StartOfDayManPowerMultiple.Select(s => s.StartOfDayManpowerId.Value).ToList() : new List<long>();
                startOfDayModel.ManPowerIds = s.StartOfDayManPowerMultiple != null ? s.StartOfDayManPowerMultiple.Select(s => s.StartOfDayManpowerId.Value).ToList() : new List<long>();
                if (ManPowerIds != null && ManPowerIds.Count > 0)
                {
                    startOfDayModel.ManPower = employeelist != null ? string.Join(" | ", employeelist.Where(s=> ManPowerIds.Contains(s.EmployeeId)).Select(s => s.User?.UserName)).TrimEnd() : "";
                }
                startOfDayModel.ProdTaskIds = s.StartOfDayProdTaskMultiple != null ? s.StartOfDayProdTaskMultiple.Select(s => s.ProdTaskPerformId.Value).ToList() : new List<long>();
                startOfDayModel.ModifiedByUserID = s.AddedByUserId;
                startOfDayModel.AddedByUserID = s.ModifiedByUserId;
                startOfDayModel.StatusCodeID = s.StatusCodeId;
                startOfDayModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                startOfDayModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                startOfDayModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                startOfDayModel.AddedDate = s.AddedDate;
                startOfDayModel.ModifiedDate = s.ModifiedDate;
                
                startOfDayModels.Add(startOfDayModel);
            });
            return startOfDayModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<StartOfDayModel> GetData(SearchModel searchModel)
        {
            var startOfDay = new StartOfDay();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).FirstOrDefault();
                        break;
                    case "Last":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).LastOrDefault();
                        break;
                    case "Next":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).LastOrDefault();
                        break;
                    case "Previous":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).FirstOrDefault();
                        break;
                    case "Last":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderByDescending(o => o.StartOfDayId).LastOrDefault();
                        break;
                    case "Next":
                        startOfDay = _context.StartOfDay.Include(s => s.StartOfDayManPowerMultiple).Include(m => m.StartOfDayProdTaskMultiple).OrderBy(o => o.StartOfDayId).FirstOrDefault(s => s.StartOfDayId > searchModel.Id);
                        break;
                    case "Previous":
                        startOfDay = _context.StartOfDay.OrderByDescending(o => o.StartOfDayId).FirstOrDefault(s => s.StartOfDayId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<StartOfDayModel>(startOfDay);

            result.ManPowerIds = startOfDay.StartOfDayManPowerMultiple.Select(s => s.StartOfDayManpowerId.Value).ToList();
            result.ProdTaskIds = startOfDay.StartOfDayProdTaskMultiple.Select(s => s.ProdTaskPerformId.Value).ToList();


            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertStartOfDay")]
        public async Task<StartOfDayModel> Post(StartOfDayModel value)
        {
            try
            {
                var startOfDay = new StartOfDay
                {
                    WorkingBlockId = value.WorkingBlockId,
                    CompanyDbid = value.CompanyDbid,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                if (value.ManPowerIds != null)
                {
                    value.ManPowerIds.ForEach(m =>
                    {
                        startOfDay.StartOfDayManPowerMultiple.Add(new StartOfDayManPowerMultiple
                        {
                            StartOfDayManpowerId = m
                        });
                    });
                }
                if (value.ProdTaskIds != null)
                {
                    value.ProdTaskIds.ForEach(m =>
                    {
                        startOfDay.StartOfDayProdTaskMultiple.Add(new StartOfDayProdTaskMultiple
                        {
                            ProdTaskPerformId = m
                        });
                    });
                }
                _context.StartOfDay.Add(startOfDay);
                _context.SaveChanges();
                value.StartOfDayId = startOfDay.StartOfDayId;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateStartOfDay")]
        public StartOfDayModel Put(StartOfDayModel value)
        {
            var startOfDay = _context.StartOfDay.Include(i => i.StartOfDayManPowerMultiple).Include(p => p.StartOfDayProdTaskMultiple).SingleOrDefault(p => p.StartOfDayId == value.StartOfDayId);

            startOfDay.WorkingBlockId = value.WorkingBlockId;
            startOfDay.CompanyDbid = value.CompanyDbid;
            startOfDay.ModifiedByUserId = value.ModifiedByUserID.Value;
            startOfDay.ModifiedDate = DateTime.Now;
            startOfDay.StatusCodeId = value.StatusCodeID.Value;
            _context.StartOfDayProdTaskMultiple.RemoveRange(startOfDay.StartOfDayProdTaskMultiple);
            _context.StartOfDayManPowerMultiple.RemoveRange(startOfDay.StartOfDayManPowerMultiple);
            _context.SaveChanges();
            if (value.ManPowerIds != null)
            {
                value.ManPowerIds.ForEach(m =>
                {
                    startOfDay.StartOfDayManPowerMultiple.Add(new StartOfDayManPowerMultiple
                    {
                        StartOfDayManpowerId = m
                    });
                });
            }
            if (value.ProdTaskIds != null)
            {
                value.ProdTaskIds.ForEach(m =>
                {
                    startOfDay.StartOfDayProdTaskMultiple.Add(new StartOfDayProdTaskMultiple
                    {
                        ProdTaskPerformId = m
                    });
                });
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteStartOfDay")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var startOfDay = _context.StartOfDay.Include(i => i.StartOfDayManPowerMultiple).Include(p => p.StartOfDayProdTaskMultiple).SingleOrDefault(p => p.StartOfDayId == id);
                if (startOfDay != null)
                {
                    _context.StartOfDay.Remove(startOfDay);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
