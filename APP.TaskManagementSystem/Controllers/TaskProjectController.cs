﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskProjectController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskProjectController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskProject")]
        public ActionResult<TaskProjectModel> Get(int id, int taskID)
        {

            var taskProject = _context.TaskProject.SingleOrDefault(t => t.TaskProjectId == id && t.TaskId == taskID);
            var result = _mapper.Map<TaskProjectModel>(taskProject);
            return result = null;


        }

        // GET: api/Project/2
        [HttpGet("GetTaskProject/{id:int}")]
        //public ActionResult<TaskNotesModel> Get(int? id)
        //{
        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //var DiscussionNotes = _context.DiscussionNotes.SingleOrDefault(p => p.TaskNotesId == id.Value);
        //    //var result = _mapper.Map<TaskNotesModel>(DiscussionNotes);
        //    //if (result == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    return result;
        //}


        // POST: api/User
        [HttpPost]
        [Route("InsertTaskProject")]
        public TaskProject Post(TaskProject value)
        {


            var taskProject = _context.TaskProject.Where(p => p.TaskProjectId == value.TaskProjectId).FirstOrDefault();
            if (taskProject == null)
            {
                taskProject = new TaskProject
                {

                    ProjectId = value.ProjectId,
                    TaskId = value.TaskId,
                   



                };
                //    _context.DiscussionNotes.Add(DiscussionNotes);
                //    _context.SaveChanges();
                //    value.TaskNotesID = DiscussionNotes.TaskNotesId;
            }
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskProject")]
        public TaskProject Put(TaskProject value)
        {

            var taskProject = _context.TaskProject.SingleOrDefault(p => p.TaskProjectId == value.TaskProjectId);
            //  folders.TaskMemberId = value.TaskMemberID;
            taskProject.TaskProjectId = value.TaskProjectId;
            taskProject.TaskId = value.TaskId;
            taskProject.ProjectId = value.ProjectId;
            



            _context.SaveChanges();
            return value;
        }
    }
}