﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterAllowableController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public BlisterAllowableController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetBlisterAllowable")]
        public List<BlisterAllowableModel> Get(int id)
        {
            var blisterAllowable = _context.BlisterAllowable.Select(s => new BlisterAllowableModel
            {
                BlisterAllowableId = s.BlisterAllowableId,
                BlisterScrapId=s.BlisterScrapId,
                MachineCode=s.MachineCode,
                NoOfCuts=s.NoOfCuts,
                AttachedlayoutPlan=s.AttachedlayoutPlan,
                TestRun=s.TestRun,
                Trimming=s.Trimming,
                DeBlister=s.DeBlister,
                BlisterScrapTypeId = s.BlisterScrapTypeId,
                BlisterValue =s.BlisterValue,
                BlisterType =s.BlisterType,
                MachineId =s.MachineId,
                

            }).OrderByDescending(o => o.BlisterAllowableId).Where(s=>s.BlisterScrapId == id).AsNoTracking().ToList();
             
            return blisterAllowable;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get BlisterAllowable")]
        [HttpGet("GetBlisterAllowable/{id:int}")]
        public ActionResult<BlisterAllowableModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var blisterAllowable = _context.BlisterAllowable.SingleOrDefault(p => p.BlisterAllowableId == id.Value);
            var result = _mapper.Map<BlisterAllowableModel>(blisterAllowable);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterAllowableModel> GetData(SearchModel searchModel)
        {
            var blisterAllowable = new BlisterAllowable();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).LastOrDefault();
                        break;
                    case "Next":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).LastOrDefault();
                        break;
                    case "Next":
                        blisterAllowable = _context.BlisterAllowable.OrderBy(o => o.BlisterAllowableId).FirstOrDefault(s => s.BlisterAllowableId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterAllowable = _context.BlisterAllowable.OrderByDescending(o => o.BlisterAllowableId).FirstOrDefault(s => s.BlisterAllowableId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlisterAllowableModel>(blisterAllowable);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBlisterAllowable")]
        public BlisterAllowableModel Post(BlisterAllowableModel value)
        {
            var blisterAllowable = new BlisterAllowable
            {
                // BlisterAllowableId=value.BlisterAllowableId,
                BlisterScrapId = value.BlisterScrapId,
                MachineCode = value.MachineCode,
                NoOfCuts = value.NoOfCuts,
                AttachedlayoutPlan = value.AttachedlayoutPlan,
                TestRun = value.TestRun,
                Trimming = value.Trimming,
                DeBlister = value.DeBlister
                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID.Value

            };
            _context.BlisterAllowable.Add(blisterAllowable);
            _context.SaveChanges();
            value.BlisterAllowableId = blisterAllowable.BlisterAllowableId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBlisterAllowable")]
        public BlisterAllowableModel Put(BlisterAllowableModel value)
        {
            var blisterAllowable = _context.BlisterAllowable.SingleOrDefault(p => p.BlisterAllowableId == value.BlisterAllowableId);
            //blisterAllowable.ModifiedByUserId = value.ModifiedByUserID;
            //blisterAllowable.ModifiedDate = DateTime.Now;
            blisterAllowable.BlisterScrapId = value.BlisterScrapId;
            blisterAllowable.MachineCode = value.MachineCode;
            blisterAllowable.NoOfCuts = value.NoOfCuts;
            blisterAllowable.AttachedlayoutPlan = value.AttachedlayoutPlan;
            blisterAllowable.TestRun = value.TestRun;
            blisterAllowable.Trimming = value.Trimming;
            blisterAllowable.DeBlister = value.DeBlister;
            //blisterAllowable.AddedByUserId = value.AddedByUserID;
            // blisterAllowable.AddedDate = DateTime.Now;
            //blisterAllowable.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBlisterAllowable")]
        public void Delete(int id)
        {
            var blisterAllowable = _context.BlisterAllowable.SingleOrDefault(p => p.BlisterAllowableId == id);
            if (blisterAllowable != null)
            {
                _context.BlisterAllowable.Remove(blisterAllowable);
                _context.SaveChanges();
            }
        }
    }
}