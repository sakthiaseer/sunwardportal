﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.EntityModel.SearchParam;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesItemPackingInfoController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SalesItemPackingInfoController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSalesItemPackingInfo")]
        public List<SalesItemPackingInfoModel> Get()
        {
            List<SalesItemPackingInfoModel> salesItemPackingInfoModels = new List<SalesItemPackingInfoModel>();
            var methodTemplateRoutineModels = _context.SalesItemPackingInfo.Include("AddedByUser").Include("FpnoNavigation").Include("ModifiedByUser").Include(l => l.SalesItemPackingRequirement)
                .Include(s => s.StatusCode)
                .OrderByDescending(o => o.SalesItemPackingInfoId).AsNoTracking().ToList();
            methodTemplateRoutineModels.ForEach(s =>
            {
                SalesItemPackingInfoModel salesItemPackingInfoModel = new SalesItemPackingInfoModel
                {
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    ManufacturingSiteID = s.ManufacturingSiteId,
                    FinishProductGeneralInfoLineID = s.FinishProductGeneralInfoLineId,
                    Fpname = s.Fpname,
                    Fpno = s.Fpno,
                    FpnoName = s.FpnoNavigation?.Name,
                    IsSalesPackRegistration = s.IsSalesPackRegistration,
                    IsSalesPackRegistrationFlag = (s.IsSalesPackRegistration == true) ? "0" : "1",
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    ManufacturingSite = s.ManufacturingSite,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    PackageRequirementIds = s.SalesItemPackingRequirement.Where(l => l.SalesItemPackingInfoId == s.SalesItemPackingInfoId).Select(r => r.PackingRequirementId).ToList(),
                    PackagingType = s.PackagingType,
                    ProductionRegistrationHolder = s.ProductionRegistrationHolder,
                    ProductName = s.ProductName,
                    ProductOwner = s.ProductOwner,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    RegisterCountry = s.RegisterCountry,
                    RegistrationFactor = s.RegistrationFactor,
                    RegistrationPerPack = s.RegistrationPerPack,
                    SalePerPack = s.SalePerPack,
                    SalesFactor = s.SalesFactor,
                    SalesInformation = s.SalesInformation,
                    SalesItemPackingInfoId = s.SalesItemPackingInfoId,
                    SmallestPackQty = s.SmallestPackQty,
                    SmallestQtyperPack = s.SmallestQtyperPack,
                    SmallestQtyunit = s.SmallestQtyunit,
                    StatusCode = s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    MasterPackagingStatus = s.MasterPackagingStatus,
                    MasterPackagingStatusFlag = (s.MasterPackagingStatus == true) ? "0" : "1",
                    SalesInformationFlag = (s.SalesInformation == true) ? "0" : "1"
                };
                salesItemPackingInfoModels.Add(salesItemPackingInfoModel);
            });

            return salesItemPackingInfoModels;
        }
        [HttpGet]
        [Route("GetSalesItemPackingInfoLinkProfileNo")]
        public List<SalesItemPackingInfoModel> GetSalesItemPackingInfoLinkProfileNo(string type)
        {
            List<SalesItemPackingInfoModel> salesItemPackingInfoModels = new List<SalesItemPackingInfoModel>();
            var methodTemplateRoutineModels = _context.SalesItemPackingInfo.Include("AddedByUser").Include("FpnoNavigation").Include("ModifiedByUser").Include(l => l.SalesItemPackingRequirement)
                .Include(s => s.StatusCode)
                .Where(l => l.LinkProfileReferenceNo == type).OrderByDescending(o => o.SalesItemPackingInfoId).AsNoTracking().ToList();
            methodTemplateRoutineModels.ForEach(s =>
            {
                SalesItemPackingInfoModel salesItemPackingInfoModel = new SalesItemPackingInfoModel
                {
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    ManufacturingSiteID = s.ManufacturingSiteId,
                    FinishProductGeneralInfoLineID = s.FinishProductGeneralInfoLineId,
                    Fpname = s.Fpname,
                    Fpno = s.Fpno,
                    FpnoName = s.FpnoNavigation?.Name,
                    IsSalesPackRegistration = s.IsSalesPackRegistration,
                    IsSalesPackRegistrationFlag = (s.IsSalesPackRegistration == true) ? "0" : "1",
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    ManufacturingSite = s.ManufacturingSite,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    PackageRequirementIds = s.SalesItemPackingRequirement.Where(l => l.SalesItemPackingInfoId == s.SalesItemPackingInfoId).Select(r => r.PackingRequirementId).ToList(),
                    PackagingType = s.PackagingType,
                    ProductionRegistrationHolder = s.ProductionRegistrationHolder,
                    ProductName = s.ProductName,
                    ProductOwner = s.ProductOwner,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    RegisterCountry = s.RegisterCountry,
                    RegistrationFactor = s.RegistrationFactor,
                    RegistrationPerPack = s.RegistrationPerPack,
                    SalePerPack = s.SalePerPack,
                    SalesFactor = s.SalesFactor,
                    SalesInformation = s.SalesInformation,
                    SalesItemPackingInfoId = s.SalesItemPackingInfoId,
                    SmallestPackQty = s.SmallestPackQty,
                    SmallestQtyperPack = s.SmallestQtyperPack,
                    SmallestQtyunit = s.SmallestQtyunit,
                    StatusCode = s.StatusCode.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    MasterPackagingStatus = s.MasterPackagingStatus,
                    MasterPackagingStatusFlag = (s.MasterPackagingStatus == true) ? "0" : "1",
                    SalesInformationFlag = (s.SalesInformation == true) ? "0" : "1"
                };
                salesItemPackingInfoModels.Add(salesItemPackingInfoModel);
            });
            return salesItemPackingInfoModels;
        }
        [HttpPost]
        [Route("GenerateSalesItemPackingInfos")]
        public List<SalesItemPackingInfoModel> GenerateSalesPackingInfo(SalesPackingInformationParam salesPackinginfoParam)
        {
            List<FinishProductModel> finishProductModels = new List<FinishProductModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishProductItems = _context.FinishProduct.AsNoTracking().ToList();
            var applicationmasterId = _context.ApplicationMaster.FirstOrDefault(m => m.ApplicationMasterCodeId == 126).ApplicationMasterId;
            var fPCommonFieldLine = _context.FpcommonFieldLine.Where(s => s.FpcommonFiledId == salesPackinginfoParam.ProductID).Select(s => s.MaterialName).AsNoTracking().ToList();
            var materialIDs = applicationmasterdetail.Where(m => m.ApplicationMasterId == applicationmasterId && fPCommonFieldLine.Contains(m.Value)).Select(s => s.ApplicationMasterDetailId).ToList();
            var finishProductIds = _context.FinishProductLine.Where(c => materialIDs.Contains(c.MaterialId.Value)).AsNoTracking().Select(s => s.FinishProductId).ToList();
            var finishProducts = _context.FinishProduct.Include("FinishProductLine").Where(c => finishProductIds.Contains(c.FinishProductId)).AsNoTracking().ToList();
            finishProducts.ForEach(s =>
            {
                FinishProductModel finishProductModel = new FinishProductModel
                {
                    FinishProductId = s.FinishProductId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingSiteName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Value : "",
                    ProductId = s.ProductId,
                    FinishProductLineId = s.FinishProductLine.Select(m => m.MaterialId.Value).ToList(),
                    ProductName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProductId).Value : "",
                };
                finishProductModels.Add(finishProductModel);
            });


            List<Product> products = new List<Product>();
            List<FPManufacturingRecipeModel> fpManufacturingRecipeModels = new List<FPManufacturingRecipeModel>();
            finishProductModels.ForEach(f =>
            {
                Product product = new Product();
                product.ProductID = f.ProductId;
                product.ProductName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == f.ProductId).Value : "";
                products.Add(product);
                if (f.FinishProductLineId.Count > 0)
                {
                    f.FinishProductLineId.ToList().ForEach(l =>
                    {
                        Product productLine = new Product();
                        productLine.ProductID = l;
                        productLine.ProductName = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == l).Value : "";
                        products.Add(productLine);
                    });
                }
            });

            var productNames = products.Select(p => p.ProductName.Trim().ToLower()).ToList();

            var finishProductGeneralInfos = _context.FinishProductGeneralInfo.Include(f => f.FinishProduct).AsNoTracking().ToList();
            List<SalesItemPackingInfoModel> salesItemPackingInfoModels = new List<SalesItemPackingInfoModel>();
            finishProductGeneralInfos.ForEach(g =>
            {
                SalesItemPackingInfoModel salesItemPackingInfo = new SalesItemPackingInfoModel
                {
                    ManufacturingSite = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.FinishProduct.ManufacturingSiteId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.FinishProduct.ManufacturingSiteId).Value : "",
                    RegisterCountry = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.RegisterCountry) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.RegisterCountry).Value : "",
                    ProductionRegistrationHolder = g.ProductRegistrationHolderId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == g.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "",
                    ProductOwner = applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.RegisterProductOwnerId).Value,
                    ProductName = g.RegisterCountryProductName,
                    ManufacturingSiteID = g.FinishProduct.ManufacturingSiteId,
                    FinishProductGeneralInfoID = g.FinishProductGeneralInfoId,
                    SmallestPackQty = g.FinishProdcutGeneralInfoLine.Where(t => t.FinishProductGeneralInfoId == g.FinishProductGeneralInfoId).Select(t => t.PackQty).FirstOrDefault()
                };
                salesItemPackingInfoModels.Add(salesItemPackingInfo);
            });
           
            //).Where(r => productNames.Contains(r.ProductName.Trim().ToLower())).ToList();

            List<SalesItemPackingInfo> salespackinginfos = new List<SalesItemPackingInfo>();
            var ManufacturingSiteIDs = salesItemPackingInfoModels.Select(m => m.ManufacturingSiteID).ToList();
            var FinishProductGeneralInfoIDs = salesItemPackingInfoModels.Select(m => m.FinishProductGeneralInfoID).ToList();
            var existingsalesinfo = _context.SalesItemPackingInfo.Where(c => (ManufacturingSiteIDs.Contains(c.ManufacturingSiteId) && FinishProductGeneralInfoIDs.Contains(c.FinishProductGeneralInfoId)));

            var ExistingManufacturingSiteIDs = existingsalesinfo.Select(m => m.ManufacturingSiteId).ToList();
            var ExistingFinishProductGeneralInfoIDs = existingsalesinfo.Select(m => m.FinishProductGeneralInfoId).ToList();
            var notExistManufacturingModels = salesItemPackingInfoModels.Where(r => !(ExistingManufacturingSiteIDs.Contains(r.ManufacturingSiteID) && ExistingFinishProductGeneralInfoIDs.Contains(r.FinishProductGeneralInfoID))).ToList();
            existingsalesinfo.ToList().ForEach(f =>
            {
                string profileNo = string.Empty;
                if (salesPackinginfoParam.ProfileID != null)
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = salesPackinginfoParam.ProfileID, Title="Sales Item Packing Info" });
                }
                f.ProfileReferenceNo = profileNo;
                f.LinkProfileReferenceNo = salesPackinginfoParam.LinkProfileReferenceNo;
                f.MasterProfileReferenceNo = string.IsNullOrEmpty(salesPackinginfoParam.MasterProfileReferenceNo) ? profileNo : salesPackinginfoParam.MasterProfileReferenceNo;
            });
            notExistManufacturingModels.ForEach(m =>
            {
                string profileNo = string.Empty;
                if (salesPackinginfoParam.ProfileID != null)
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = salesPackinginfoParam.ProfileID, Title = "Sales Item Packing Info" });
                }


                var salesItemPackingInfo = new SalesItemPackingInfo
                {
                    ManufacturingSiteId = m.ManufacturingSiteID,
                    FinishProductGeneralInfoId = m.FinishProductGeneralInfoID,
                    ManufacturingSite = m.ManufacturingSite,
                    MasterProfileReferenceNo = m.MasterProfileReferenceNo,
                    PackagingType = m.PackagingType,
                    ProductionRegistrationHolder = m.ProductionRegistrationHolder,
                    ProductName = m.ProductName,
                    ProductOwner = m.ProductOwner,
                    ProfileReferenceNo = m.ProfileReferenceNo,
                    RegisterCountry = m.RegisterCountry,
                    RegistrationFactor = m.RegistrationFactor,
                    RegistrationPerPack = m.RegistrationPerPack,
                    SalePerPack = m.SalePerPack,
                    SalesFactor = m.SalesFactor,
                    SalesInformation = m.SalesInformation,
                    SmallestPackQty = m.SmallestPackQty,
                    SmallestQtyperPack = m.SmallestQtyperPack,
                    SmallestQtyunit = m.SmallestQtyunit,
                    AddedByUserId = m.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = m.StatusCodeID.Value,
                };


                salesPackinginfoParam.PackingRequirementIds.ForEach(l =>
                {
                    SalesItemPackingRequirement salesItemPackingRequirement = new SalesItemPackingRequirement
                    {
                        PackingRequirementId = l,
                        SalesItemPackingInfoId = salesItemPackingInfo.SalesItemPackingInfoId
                    };
                    salesItemPackingInfo.SalesItemPackingRequirement.Add(salesItemPackingRequirement);
                });
                salespackinginfos.Add(salesItemPackingInfo);
            });
            _context.SalesItemPackingInfo.AddRange(salespackinginfos);
            _context.SaveChanges();

            var generalInfos = _context.SalesItemPackingInfo.Include("FinishProductGeneralInfo.FinishProduct").Where(r => FinishProductGeneralInfoIDs.Contains(r.FinishProductGeneralInfoId)).AsNoTracking().ToList();

            var result = new List<SalesItemPackingInfoModel>();
            generalInfos.ForEach(sa =>
            {

                result.Add(new SalesItemPackingInfoModel
                {
                    ManufacturingSite = sa.ManufacturingSite,
                    ManufacturingSiteID = sa.ManufacturingSiteId,
                    FinishProductGeneralInfoID = sa.FinishProductGeneralInfoId,
                    MasterProfileReferenceNo = sa.MasterProfileReferenceNo,
                    PackagingType = sa.PackagingType,
                    ProductionRegistrationHolder = sa.ProductionRegistrationHolder,
                    ProductName = sa.ProductName,
                    ProductOwner = sa.ProductOwner,
                    ProfileReferenceNo = sa.ProfileReferenceNo,
                    RegisterCountry = sa.RegisterCountry,
                    RegistrationFactor = sa.RegistrationFactor,
                    RegistrationPerPack = sa.RegistrationPerPack,
                    SalePerPack = sa.SalePerPack,
                    SalesFactor = sa.SalesFactor,
                    SalesInformation = sa.SalesInformation,
                    SmallestPackQty = sa.SmallestPackQty,
                    SmallestQtyperPack = sa.SmallestQtyperPack,
                    SmallestQtyunit = sa.SmallestQtyunit,
                    AddedByUserID = sa.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = sa.StatusCodeId,

                });

            });
            return result;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetSalesItemPackInfo/{id:int}")]
        public ActionResult<SalesItemPackingInfoModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var salesItemPackingInfo = _context.SalesItemPackingInfo.SingleOrDefault(p => p.SalesItemPackingInfoId == id.Value);
            var result = _mapper.Map<SalesItemPackingInfoModel>(salesItemPackingInfo);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SalesItemPackingInfoModel> GetData(SearchModel searchModel)
        {
            var salesItemPackingInfo = new SalesItemPackingInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).FirstOrDefault();
                        break;
                    case "Last":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).LastOrDefault();
                        break;
                    case "Next":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).LastOrDefault();
                        break;
                    case "Previous":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).FirstOrDefault();
                        break;
                    case "Last":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).LastOrDefault();
                        break;
                    case "Next":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderBy(o => o.SalesItemPackingInfoId).FirstOrDefault(s => s.SalesItemPackingInfoId > searchModel.Id);
                        break;
                    case "Previous":
                        salesItemPackingInfo = _context.SalesItemPackingInfo.OrderByDescending(o => o.SalesItemPackingInfoId).FirstOrDefault(s => s.SalesItemPackingInfoId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SalesItemPackingInfoModel>(salesItemPackingInfo);
            if (result != null)
            {
                result.PackageRequirementIds = _context.SalesItemPackingRequirement.Where(s => s.SalesItemPackingInfoId == result.SalesItemPackingInfoId).AsNoTracking().Select(l => l.PackingRequirementId).ToList();
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSalesItemPackInfo")]
        public SalesItemPackingInfoModel Post(SalesItemPackingInfoModel value)
        {
            var salesItemPackingInfo = new SalesItemPackingInfo
            {
                Fpname = value.Fpname,
                Fpno = value.Fpno,
                IsSalesPackRegistration = value.IsSalesPackRegistration,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ManufacturingSite = value.ManufacturingSite,
                MasterProfileReferenceNo = value.MasterProfileReferenceNo,
                PackagingType = value.PackagingType,
                ProductionRegistrationHolder = value.ProductionRegistrationHolder,
                ProductName = value.ProductName,
                ProductOwner = value.ProductOwner,
                ProfileReferenceNo = value.ProfileReferenceNo,
                RegisterCountry = value.RegisterCountry,
                RegistrationFactor = value.RegistrationFactor,
                RegistrationPerPack = value.RegistrationPerPack,
                SalePerPack = value.SalePerPack,
                SalesFactor = value.SalesFactor,
                SalesInformation = value.SalesInformation,
                SmallestPackQty = value.SmallestPackQty,
                SmallestQtyperPack = value.SmallestQtyperPack,
                SmallestQtyunit = value.SmallestQtyunit,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                MasterPackagingStatus = value.MasterPackagingStatus,
            };
            value.PackageRequirementIds.ForEach(l =>
            {
                SalesItemPackingRequirement salesItemPackingRequirement = new SalesItemPackingRequirement
                {
                    PackingRequirementId = l,
                    SalesItemPackingInfoId = value.SalesItemPackingInfoId
                };
                salesItemPackingInfo.SalesItemPackingRequirement.Add(salesItemPackingRequirement);
            });
            _context.SalesItemPackingInfo.Add(salesItemPackingInfo);
            _context.SaveChanges();
            value.SalesItemPackingInfoId = salesItemPackingInfo.SalesItemPackingInfoId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSalesItemPackingInfo")]
        public SalesItemPackingInfoModel Put(SalesItemPackingInfoModel value)
        {
            var salesItemPackingInfo = _context.SalesItemPackingInfo.Include(l => l.SalesItemPackingRequirement).SingleOrDefault(p => p.SalesItemPackingInfoId == value.SalesItemPackingInfoId);
            salesItemPackingInfo.Fpname = value.Fpname;
            salesItemPackingInfo.Fpno = value.Fpno;
            salesItemPackingInfo.IsSalesPackRegistration = (value.IsSalesPackRegistrationFlag == "0") ? true : false;
            salesItemPackingInfo.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            salesItemPackingInfo.ManufacturingSite = value.ManufacturingSite;
            salesItemPackingInfo.MasterProfileReferenceNo = value.MasterProfileReferenceNo;
            salesItemPackingInfo.PackagingType = value.PackagingType;
            salesItemPackingInfo.ProductionRegistrationHolder = value.ProductionRegistrationHolder;
            salesItemPackingInfo.ProductName = value.ProductName;
            salesItemPackingInfo.ProductOwner = value.ProductOwner;
            salesItemPackingInfo.ProfileReferenceNo = value.ProfileReferenceNo;
            salesItemPackingInfo.RegisterCountry = value.RegisterCountry;
            salesItemPackingInfo.RegistrationFactor = value.RegistrationFactor;
            salesItemPackingInfo.RegistrationPerPack = value.RegistrationPerPack;
            salesItemPackingInfo.SalePerPack = value.SalePerPack;
            salesItemPackingInfo.SalesFactor = value.SalesFactor;
            salesItemPackingInfo.SalesInformation = (value.SalesInformationFlag == "0") ? true : false;
            salesItemPackingInfo.SmallestPackQty = value.SmallestPackQty;
            salesItemPackingInfo.SmallestQtyperPack = value.SmallestQtyperPack;
            salesItemPackingInfo.SmallestQtyunit = value.SmallestQtyunit;
            salesItemPackingInfo.AddedByUserId = value.AddedByUserID;
            salesItemPackingInfo.ModifiedByUserId = value.ModifiedByUserID;
            salesItemPackingInfo.ModifiedDate = DateTime.Now;
            salesItemPackingInfo.StatusCodeId = value.StatusCodeID.Value;
            salesItemPackingInfo.MasterPackagingStatus = (value.MasterPackagingStatusFlag == "0") ? true : false;
            if (salesItemPackingInfo.SalesItemPackingRequirement != null)
            {
                _context.SalesItemPackingRequirement.RemoveRange(salesItemPackingInfo.SalesItemPackingRequirement);
                _context.SaveChanges();
            }
            value.PackageRequirementIds.ForEach(l =>
            {
                SalesItemPackingRequirement salesItemPackingRequirement = new SalesItemPackingRequirement
                {
                    PackingRequirementId = l,
                    SalesItemPackingInfoId = value.SalesItemPackingInfoId
                };
                _context.SalesItemPackingRequirement.Add(salesItemPackingRequirement);
            });
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSalesItemPackingInfo")]
        public void Delete(int id)
        {
            var salesItemPackingInfo = _context.SalesItemPackingInfo.SingleOrDefault(p => p.SalesItemPackingInfoId == id);
            if (salesItemPackingInfo != null)
            {
                var salesItemPackingRequirement = _context.SalesItemPackingRequirement.Where(s => s.SalesItemPackingInfoId == id).AsNoTracking().ToList();
                if (salesItemPackingRequirement != null)
                {
                    _context.SalesItemPackingRequirement.RemoveRange(salesItemPackingRequirement);
                    _context.SaveChanges();
                }
                var salesItemPackingInforLine = _context.SalesItemPackingInforLine.Where(l => l.SalesItemPackingInfoId == id).AsNoTracking().ToList();
                if (salesItemPackingInforLine != null)
                {
                    salesItemPackingInforLine.ForEach(h =>
                    {
                        var salesItemPackingRequirementLineItems = _context.SalesItemPackingRequirement.Where(s => s.SalesItemPackingInfoLineId == h.SalesItemPackingInfoLineId).AsNoTracking().ToList();
                        if (salesItemPackingRequirementLineItems != null)
                        {
                            _context.SalesItemPackingRequirement.RemoveRange(salesItemPackingRequirementLineItems);
                            _context.SaveChanges();
                        }
                    });

                    _context.SalesItemPackingInforLine.RemoveRange(salesItemPackingInforLine);
                    _context.SaveChanges();
                }
                _context.SalesItemPackingInfo.Remove(salesItemPackingInfo);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetSalesItemPackingInfoItems")]
        public List<SalesItemPackingInfo> GetSalesItemPackingInfoItems(string id, int userId)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fPManufacturingRecipe = _context.FpmanufacturingRecipe.Where(s => s.LinkProfileReferenceNo == id).AsNoTracking().ToList();
            var salesItemPackingInfoDetail = new List<SalesItemPackingInfo>();
            if (fPManufacturingRecipe != null)
            {
                fPManufacturingRecipe.ForEach(h =>
                {
                    var finishProdcutGeneralInfoLineItems = _context.FinishProdcutGeneralInfoLine.Include("FinishProductGeneralInfo").Include(s => s.FinishProductGeneralInfo.FinishProduct).Where(s => s.FinishProductGeneralInfoId == h.FinishProductGeneralInfoId).AsNoTracking().ToList();
                    if (finishProdcutGeneralInfoLineItems != null)
                    {
                        finishProdcutGeneralInfoLineItems.ForEach(l =>
                        {
                            var salesItemPackingInfoItems = _context.SalesItemPackingInfo.Where(w => w.FinishProductGeneralInfoLineId == l.FinishProductGeneralInfoLineId).AsNoTracking().ToList();
                            if (salesItemPackingInfoItems.Count == 0)
                            {
                                var salesItemPackingInfoDetails = new SalesItemPackingInfo
                                {
                                    LinkProfileReferenceNo = h.LinkProfileReferenceNo,
                                    FinishProductGeneralInfoLineId = l.FinishProductGeneralInfoLineId,
                                    ManufacturingSiteId = h.ManufacturingSiteId,
                                    FinishProductGeneralInfoId = h.FinishProductGeneralInfoId.Value,
                                    ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == l.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == l.FinishProductGeneralInfo.FinishProduct.ProductId).Value : "",
                                    ManufacturingSite = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == h.ManufacturingSiteId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == h.ManufacturingSiteId).Value : "",
                                    RegisterCountry = l.FinishProductGeneralInfo.RegisterCountry != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.FinishProductGeneralInfo.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "",
                                    ProductionRegistrationHolder = l.FinishProductGeneralInfo.ProductRegistrationHolderId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.FinishProductGeneralInfo.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "",
                                    ProductOwner = l.FinishProductGeneralInfo.RegisterProductOwnerId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.FinishProductGeneralInfo.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : "",
                                    PackagingType = l.PackagingTypeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                                    SmallestPackQty = l.PackQty,
                                    SmallestQtyunit = l.PackQtyunitId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.PackQtyunitId.Value).Select(m => m.Value).FirstOrDefault() : "",
                                    SmallestQtyperPack = l.PerPackId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.PerPackId).Select(m => m.Value).FirstOrDefault() : "",
                                    RegistrationFactor = l.FactorOfSmallestProductionPack.ToString(),
                                    RegistrationPerPack = l.SalesPerPackId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "",
                                    SalePerPack = l.SalesPerPackId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == l.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "",
                                    AddedByUserId = userId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = 1,
                                    SalesFactor = 1,
                                    MasterPackagingStatus = h.IsMaster,
                                    SalesInformation = false,

                                };
                                _context.SalesItemPackingInfo.Add(salesItemPackingInfoDetails);
                                _context.SaveChanges();
                            }
                        });
                    }
                });
            }
            return salesItemPackingInfoDetail;
        }
    }
}