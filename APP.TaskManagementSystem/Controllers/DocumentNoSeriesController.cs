﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentNoSeriesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;


        public DocumentNoSeriesController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("UpdateDocumentNoSeriesItems")]
        public void UpdateDocumentNoSeriesItems()
        {
            var documentNoSeries = _context.DocumentNoSeries.Where(w => w.ModifiedDate == null).AsNoTracking().ToList();
            if (documentNoSeries != null)
            {
                documentNoSeries.ForEach(s =>
                {
                    var query = string.Format("Update DocumentNoSeries Set ModifiedDate='{1}', ModifiedByUserId='{2}'  Where NumberSeriesId= {0}  ", s.NumberSeriesId, s.AddedDate, s.AddedByUserId);
                    _context.Database.ExecuteSqlRaw(query);
                });
            }

        }
        [HttpGet]
        [Route("InsertDocumentNoSeriesItems")]
        public void InsertDocumentNoSeriesItems()
        {
            var documentNos = _context.DocumentNoSeries.Select(s => s.DocumentNo).AsNoTracking().ToList();
            var documents = _context.Documents.Select(s => new { s.DocumentId, s.ProfileNo, s.UploadDate, s.AddedByUserId, s.FilterProfileTypeId, s.SessionId }).Where(w => w.ProfileNo != null && !documentNos.Contains(w.ProfileNo)).AsNoTracking().ToList();
            var profileNos = documents.Select(s => s.ProfileNo).Distinct().ToList();
            var filterProfileTypeIds = documents.Where(w => w.FilterProfileTypeId > 0).Select(s => s.FilterProfileTypeId.GetValueOrDefault(0)).Distinct().ToList();
            var fileProfileType = _context.FileProfileType.Where(w => filterProfileTypeIds.Contains(w.FileProfileTypeId)).Select(s => new { s.FileProfileTypeId, s.ProfileId }).ToList();
            if (profileNos != null && documentNos.Count > 0)
            {
                profileNos.ForEach(a =>
                {
                    var documentsData = documents.Where(w => w.ProfileNo == a).OrderByDescending(o => o.DocumentId).FirstOrDefault();
                    if (documentsData != null)
                    {
                        var profileId = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == documentsData.FilterProfileTypeId)?.ProfileId;
                        if (profileId != null)
                        {
                            var documentNoSeries = new DocumentNoSeries
                            {
                                ProfileId = profileId,
                                DocumentNo = a,
                                Date = documentsData.UploadDate,
                                RequestorId = documentsData.AddedByUserId,
                                StatusCodeId = 710,
                                AddedByUserId = documentsData.AddedByUserId,
                                AddedDate = documentsData.UploadDate.Value,
                                SessionId = documentsData.SessionId,
                                ModifiedDate = documentsData.UploadDate,
                                ModifiedByUserId = documentsData.AddedByUserId,
                            };
                            _context.DocumentNoSeries.Add(documentNoSeries);
                        }
                    }
                });
                _context.SaveChanges();
            }
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentNoSeriesItems")]
        public List<DocumentNoSeriesModel> Get()
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate != null ? s.AddedDate : s.ModifiedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                    FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                    SessionId = s.SessionId,

                };
                documentNoSeriesModel.Add(documentNoSeriesModels);
            });
            return documentNoSeriesModel.OrderByDescending(a => a.NumberSeriesId).ToList();
        }
        [HttpGet]
        [Route("GetDocumentNoSeriesItemsByProfileIdFlag")]
        public List<DocumentNoSeriesModel> GetDocumentNoSeriesItemsByProfileIdFlag(long? id)
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(a => a.FileProfileType).Include(m => m.ModifiedByUser).Where(p => p.ProfileId == id && p.IsUpload == false).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title + "||" + s.DocumentNo + "||" + s.FileProfileType?.Name,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate != null ? s.AddedDate : s.ModifiedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                    FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                    SessionId = s.SessionId,
                    FileProfileTypeId = s.FileProfileTypeId,

                };
                documentNoSeriesModel.Add(documentNoSeriesModels);
            });
            return documentNoSeriesModel.OrderByDescending(a => a.NumberSeriesId).ToList();
        }

        [HttpGet]
        [Route("GetDocumentNoSeriesItemsByProfileId")]
        public List<DocumentNoSeriesModel> GetDocumentNoSeriesItemsByProfileId(long? id)
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Where(p => p.ProfileId == id).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate != null ? s.AddedDate : s.ModifiedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                    FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                    SessionId = s.SessionId,

                };
                documentNoSeriesModel.Add(documentNoSeriesModels);
            });
            return documentNoSeriesModel.OrderByDescending(a => a.NumberSeriesId).ToList();
        }

        [HttpPost]
        [Route("GetDocumentReserveNoSeriesByProfileId")]
        public List<DocumentNoSeriesModel> GetDocumentReserveNoSeriesByProfileId(ProfileReserveDataModel profileReserveDataModel)
        {

            var fileProfileTypeIds = _context.FileProfileType.Where(d => d.ProfileId == profileReserveDataModel.ProfileId && d.FileProfileTypeId == profileReserveDataModel.FileProfileTypeId).Select(s => s.FileProfileTypeId).ToList();
            var existingNos = _context.Documents.Where(d => fileProfileTypeIds.Contains(d.FilterProfileTypeId.Value)).Select(s => s.ProfileNo).ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Where(p => p.ProfileId == profileReserveDataModel.ProfileId && !existingNos.Contains(p.DocumentNo)).AsNoTracking().ToList();
            if (documentNoSeries != null)
            {
                documentNoSeries.ForEach(s =>
                {
                    var isFile = true;
                    if (s.FileProfileTypeId != null)
                    {
                        var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(w => w.SessionId == s.SessionId).Count();
                        if (documents > 0)
                        {
                            isFile = false;
                        }
                    }
                    if (isFile == true)
                    {
                        DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                        {
                            NumberSeriesId = s.NumberSeriesId,
                            ProfileID = s.ProfileId,
                            ProfileName = s.Profile != null ? s.Profile.Name : "",
                            DocumentNo = s.DocumentNo,
                            VersionNo = s.VersionNo,
                            Title = s.Title,
                            EffectiveDate = s.EffectiveDate,
                            Implementation = s.Implementation,
                            NextReviewDate = s.NextReviewDate,
                            Date = s.Date,
                            RequestorId = s.RequestorId,
                            RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                            Link = s.Link,
                            Description = s.Description,
                            StatusCodeID = s.StatusCodeId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            AddedDate = s.AddedDate != null ? s.AddedDate : s.ModifiedDate,
                            ModifiedDate = s.ModifiedDate,
                            DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                            FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                            SessionId = s.SessionId,
                            FileProfileTypeId = s.FileProfileTypeId,

                        };
                        documentNoSeriesModel.Add(documentNoSeriesModels);
                    }
                });
            }
            return documentNoSeriesModel.OrderByDescending(a => a.NumberSeriesId).ToList();
        }
        [HttpPost]
        [Route("GetDocumentNoSeriesBySearch")]
        public List<DocumentNoSeriesModel> GetDocumentNoSeriesBySearch(SearchModel searchModel)
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                    FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                    SessionId = s.SessionId,

                };
                documentNoSeriesModel.Add(documentNoSeriesModels);
            });
            documentNoSeriesModel = documentNoSeriesModel.Where(d => (d.DocumentNo != null && d.DocumentNo.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.RequestorName != null && d.RequestorName.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.ProfileName != null && d.ProfileName.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.VersionNo != null && d.VersionNo.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.Title != null && d.Title.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.AddedByUser != null && d.AddedByUser.ToLower().Contains(searchModel.SearchString.ToLower())) || (d.ModifiedByUser != null && d.ModifiedByUser.ToLower().Contains(searchModel.SearchString.ToLower()))).ToList();
            return documentNoSeriesModel.OrderByDescending(a => a.NumberSeriesId).ToList();
        }
        [HttpPost]
        [Route("GetDocumentNoSeriesSearch")]
        public List<DocumentNoSeriesModel> GetDocumentNoSeriesSearch(DocumentNoSeriesSearchModel documentNoSeriesSearchModel)
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModels = new List<DocumentNoSeriesModel>();

            if (documentNoSeriesSearchModel.CompanyId != null)
            {
                documentNoSeries = documentNoSeries.Where(s => s.Profile?.CompanyId == documentNoSeriesSearchModel.CompanyId).ToList();
            }
            if (documentNoSeriesSearchModel.DepartmentId != null)
            {
                documentNoSeries = documentNoSeries.Where(s => s.Profile?.DeparmentId == documentNoSeriesSearchModel.DepartmentId).ToList();

            }
            if (documentNoSeriesSearchModel.ProfileID != null)
            {
                documentNoSeries = documentNoSeries.Where(s => s.ProfileId == documentNoSeriesSearchModel.ProfileID).ToList();
            }
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModel = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocumentID = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.DocumentId).FirstOrDefault(),
                    FileName = s.ProfileDocument.Where(t => t.NumberSeriesId == s.NumberSeriesId && t.IsLatest == true && t.DocumentId != null).Select(t => t.Document.FileName).FirstOrDefault(),
                    SessionId = s.SessionId,

                };
                documentNoSeriesModels.Add(documentNoSeriesModel);
            });
            return documentNoSeriesModels;
        }
        [HttpGet]
        [Route("GetProfileDocumentById")]
        public List<ProfileDocumentModel> GetProfileDocumentById(int id)
        {
            var profileDocument = _context.ProfileDocument.Include(p => p.Document).Include(r => r.UploadedByUser).AsNoTracking().ToList();
            List<ProfileDocumentModel> profileDocumentModel = new List<ProfileDocumentModel>();
            profileDocument.ForEach(s =>
            {
                ProfileDocumentModel profileDocumentModels = new ProfileDocumentModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    VersionNo = s.VersionNo,
                    UploadedByUserID = s.UploadedByUserId,
                    UploadedDate = s.UploadedDate,
                    DocumentID = s.DocumentId,
                    IsLatest = s.IsLatest,
                    FileName = s.Document != null ? s.Document.FileName : "",
                    FileSize = s.Document.FileSize,
                    ContentType = s.Document.ContentType,
                    UploadedByUser = s.UploadedByUser != null ? s.UploadedByUser.UserName : "",
                };
                profileDocumentModel.Add(profileDocumentModels);
            });
            return profileDocumentModel.Where(o => o.NumberSeriesId == id).OrderByDescending(a => a.NumberSeriesId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentNoSeriesModel> GetData(SearchModel searchModel)
        {
            var documentNoSeries = new DocumentNoSeries();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).FirstOrDefault();
                        break;
                    case "Last":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).LastOrDefault();
                        break;
                    case "Next":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).LastOrDefault();
                        break;
                    case "Previous":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).FirstOrDefault();
                        break;
                    case "Last":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).LastOrDefault();
                        break;
                    case "Next":
                        documentNoSeries = _context.DocumentNoSeries.OrderBy(o => o.NumberSeriesId).FirstOrDefault(s => s.NumberSeriesId > searchModel.Id);
                        break;
                    case "Previous":
                        documentNoSeries = _context.DocumentNoSeries.OrderByDescending(o => o.NumberSeriesId).FirstOrDefault(s => s.NumberSeriesId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentNoSeriesModel>(documentNoSeries);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentNoSeries")]
        public DocumentNoSeriesModel Post(DocumentNoSeriesModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.RequestorId == null)
            {
                value.RequestorId = value.AddedByUserID;
            }
            var documentNoSeries = new DocumentNoSeries
            {
                ProfileId = value.ProfileID,
                DocumentNo = value.DocumentNo,
                VersionNo = value.VersionNo,
                Title = value.Title,
                EffectiveDate = value.EffectiveDate,
                Implementation = value.Implementation,
                NextReviewDate = value.NextReviewDate,
                Date = value.Date,
                RequestorId = value.RequestorId,
                Link = value.Link,
                Description = value.Description,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
                ModifiedDate = DateTime.Now,
                ModifiedByUserId = value.AddedByUserID,
                IsUpload = false,
                FileProfileTypeId = value.FileProfileTypeId,

            };

            documentNoSeries.DocumentNo = GenerateDocumentNo(value);

            _context.DocumentNoSeries.Add(documentNoSeries);
            _context.SaveChanges();
            value.NumberSeriesId = documentNoSeries.NumberSeriesId;
            value.DocumentNo = documentNoSeries.DocumentNo;
            value.SessionId = SessionId;

            return value;
        }

        private string GenerateDocumentNo(DocumentNoSeriesModel noSeriesModel)
        {
            bool isCompanyDepartmentExist = false;
            string deptProfileCode = "";
            string sectionProfileCode = "";
            string subSectionProfileCode = "";
            //bool isCompanyDepartmentSectionExist = false;
            //bool isCompanyDepartmentSubSectionExist = false;
            string documentNo = string.Empty;
            if (noSeriesModel.DepartmentId > 0)
            {
                deptProfileCode = _context.Department.Where(s => s.DepartmentId == noSeriesModel.DepartmentId).FirstOrDefault().ProfileCode;
            }
            if (noSeriesModel.SectionId > 0)
            {
                sectionProfileCode = _context.Section.Where(s => s.SectionId == noSeriesModel.SectionId).FirstOrDefault().ProfileCode;
            }
            if (noSeriesModel.SubSectionId > 0)
            {
                subSectionProfileCode = _context.SubSection.Where(s => s.SubSectionId == noSeriesModel.SubSectionId).FirstOrDefault().ProfileCode;

            }
            var profileSettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID);
            List<string> numberSeriesCodes = new List<string> { "Company", "Department" };
            //List<string> numberSeriesSectionCodes = new List<string> { "Company", "Department", "Section" };
            //List<string> numberSeriesSubSectionCodes = new List<string> { "Company", "Department", "Section","SubSection" };
            List<NumberSeriesCodeModel> numberSeriesCodeModels = new List<NumberSeriesCodeModel>();

            List<Seperator> seperators = new List<Seperator>();
            seperators.Add(new Seperator { SeperatorSymbol = "/", SeperatorValue = 1 });
            seperators.Add(new Seperator { SeperatorSymbol = "-", SeperatorValue = 2 });

            var seperator = seperators.FirstOrDefault(s => s.SeperatorValue == profileSettings.SeperatorToUse.GetValueOrDefault(0));
            var seperatorSymbol = seperator != null ? seperator.SeperatorSymbol : "/";

            if (!String.IsNullOrEmpty(profileSettings.Abbreviation1))
            {
                numberSeriesCodeModels = JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(profileSettings.Abbreviation1).ToList();
                isCompanyDepartmentExist = numberSeriesCodeModels.Any(c => numberSeriesCodes.Contains(c.Name));
                //isCompanyDepartmentSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSectionCodes.Contains(c.Name));
                //isCompanyDepartmentSubSectionExist = numberSeriesCodeModels.Any(c => numberSeriesSubSectionCodes.Contains(c.Name));
                numberSeriesCodeModels.OrderBy(n => n.Index).ToList().ForEach(n =>
                {
                    if (n.Name == "Company" && !string.IsNullOrEmpty(noSeriesModel.CompanyCode))
                    {
                        documentNo += noSeriesModel.CompanyCode + seperatorSymbol;
                    }
                    if (n.Name == "Department")
                    {
                        noSeriesModel.DepartmentName = string.IsNullOrEmpty(deptProfileCode) ? noSeriesModel.DepartmentName : deptProfileCode;
                        string[] departmentDetails = noSeriesModel.DepartmentName.Split(" ");
                        if (departmentDetails.Length > 1 && !string.IsNullOrEmpty(departmentDetails[1]))
                        {
                            documentNo += departmentDetails[1] + seperatorSymbol;
                        }
                        else if (!string.IsNullOrEmpty(noSeriesModel.DepartmentName))
                        {
                            documentNo += noSeriesModel.DepartmentName + seperatorSymbol;
                        }
                    }
                    if (n.Name == "Section" && !string.IsNullOrEmpty(noSeriesModel.SectionName))
                    {
                        noSeriesModel.SectionName = string.IsNullOrEmpty(sectionProfileCode) ? noSeriesModel.SectionName : sectionProfileCode;
                        documentNo += noSeriesModel.SectionName + seperatorSymbol;
                    }
                    if (n.Name == "SubSection" && !string.IsNullOrEmpty(noSeriesModel.SubSectionName))
                    {
                        noSeriesModel.SubSectionName = string.IsNullOrEmpty(subSectionProfileCode) ? noSeriesModel.SubSectionName : subSectionProfileCode;
                        documentNo += noSeriesModel.SubSectionName + seperatorSymbol;
                    }
                });
            }

            if (profileSettings.AbbreviationRequired.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.Abbreviation))
            {
                documentNo += profileSettings.Abbreviation + seperatorSymbol;
            }

            if (profileSettings.IsGroupAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.GroupAbbreviation))
            {
                documentNo += profileSettings.GroupAbbreviation + seperatorSymbol;
            }

            if (profileSettings.IsCategoryAbbreviation.GetValueOrDefault(false) && !String.IsNullOrEmpty(profileSettings.CategoryAbbreviation))
            {
                documentNo += profileSettings.CategoryAbbreviation + seperatorSymbol;
            }

            if (!String.IsNullOrEmpty(profileSettings.SpecialWording))
            {
                documentNo += profileSettings.SpecialWording + seperatorSymbol;
            }
            if (profileSettings.StartWithYear.GetValueOrDefault(false))
            {
                documentNo += DateTime.Now.Year.ToString().Substring(2, 2) + seperatorSymbol;
            }
            if (profileSettings.NoOfDigit.HasValue && profileSettings.NoOfDigit > 0)
            {
                if (isCompanyDepartmentExist)
                {
                    if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                    {
                        var profileAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null).ToList();
                        if (profileAutoNumberList != null && profileAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileAutoNumber = profileAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                    {

                        var profileAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null).ToList();
                        if (profileAutoNumberList != null && profileAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileAutoNumber = profileAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }

                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                    {
                        var profileSectionAutoNumberlist = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null).ToList();
                        if (profileSectionAutoNumberlist != null && profileSectionAutoNumberlist.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberlist.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                    {

                        var profileAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileAutoNumberList != null && profileAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileAutoNumber = profileAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }

                    }
                    else if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == noSeriesModel.PlantID && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                    {
                        var profileAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == null && p.SubSectionId == null).ToList();
                        if (profileAutoNumberList != null && profileAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileAutoNumber = profileAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }

                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == noSeriesModel.SubSectionId).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId == null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == null).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }

                    else if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                    {
                        var profileSectionAutoNumberList = _context.ProfileAutoNumber.Where(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null).ToList();
                        if (profileSectionAutoNumberList != null && profileSectionAutoNumberList.Count > 0)
                        {
                            ProfileAutoNumber profileSectionAutoNumber = profileSectionAutoNumberList.LastOrDefault();
                            documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        }
                        else
                        {
                            ProfileAutoNumber profileAutoNumber = new ProfileAutoNumber();
                            profileAutoNumber = null;
                            documentNo = GenerateProfileAuto(noSeriesModel, profileAutoNumber, profileSettings, documentNo);
                        }
                    }
                }

                else
                {
                    ProfileAutoNumber profileSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profileSettings.ProfileId && p.CompanyId == null && p.DepartmentId == null && p.SectionId == null && p.SubSectionId == null);
                    if (String.IsNullOrEmpty(profileSettings.LastNoUsed))
                    {
                        documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.StartingNo) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                        //documentNo += profileSettings.LastNoUsed;
                    }
                    else
                    {
                        documentNo = GenerateProfileAuto(noSeriesModel, profileSectionAutoNumber, profileSettings, documentNo);
                        //profileSettings.LastNoUsed = (Convert.ToInt32(profileSettings.LastNoUsed) + profileSettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profileSettings.NoOfDigit);
                        //documentNo += profileSettings.LastNoUsed;
                    }
                }

                profileSettings.LastCreatedDate = DateTime.Now;
                _context.SaveChanges();
            }

            return documentNo;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentNoSeries")]
        public DocumentNoSeriesModel Put(DocumentNoSeriesModel value)
        {
            var SessionId = Guid.NewGuid();

            if (value.SessionId == null)
            {
                value.SessionId = SessionId;
            }
            var documentNoSeries = _context.DocumentNoSeries.SingleOrDefault(p => p.NumberSeriesId == value.NumberSeriesId);
            //documentNoSeries.NumberSeriesId = value.NumberSeriesId;

            documentNoSeries.ProfileId = value.ProfileID;
            documentNoSeries.DocumentNo = value.DocumentNo;
            documentNoSeries.VersionNo = value.VersionNo;
            documentNoSeries.Title = value.Title;
            documentNoSeries.EffectiveDate = value.EffectiveDate;
            documentNoSeries.Implementation = value.Implementation;
            documentNoSeries.NextReviewDate = value.NextReviewDate;
            documentNoSeries.StatusCodeId = value.StatusCodeID.Value;
            documentNoSeries.ModifiedByUserId = value.ModifiedByUserID;
            documentNoSeries.ReasonToVoid = value.ReasonToVoid;
            documentNoSeries.ModifiedDate = DateTime.Now;
            documentNoSeries.VersionNo = value.VersionNo;
            documentNoSeries.Link = value.Link;
            documentNoSeries.Description = value.Description;
            documentNoSeries.SessionId = value.SessionId;
            documentNoSeries.FileProfileTypeId = value.FileProfileTypeId;
            var profiledocument = _context.ProfileDocument.Where(p => p.NumberSeriesId == value.NumberSeriesId && p.DocumentId != null && p.IsLatest == true).FirstOrDefault();
            if (profiledocument != null)
            {
                profiledocument.IsLatest = false;
            }

            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new { s.DocumentId, s.SessionId }).FirstOrDefault(d => d.SessionId == value.SessionID);
                if (docu != null)
                {
                    var profiledocumentadd = new ProfileDocument
                    {
                        DocumentId = docu.DocumentId,
                        NumberSeriesId = value.NumberSeriesId,
                        IsLatest = true,
                        VersionNo = value.VersionNo,
                        UploadedByUserId = value.UploadedByUserID,
                        UploadedDate = DateTime.Now,
                    };
                    _context.ProfileDocument.Add(profiledocumentadd);
                }

            }
            _context.SaveChanges();

            return value;
        }


        [HttpPut]
        [Route("UpdateDescriptionNoSeries")]
        public DocumentNoSeriesModel UpdateDescriptionNoSeries(DocumentNoSeriesModel value)
        {

            var documentNoSeries = _context.DocumentNoSeries.SingleOrDefault(p => p.NumberSeriesId == value.NumberSeriesId);
            if (documentNoSeries != null)
            {
                documentNoSeries.Description = value.Description;

                _context.SaveChanges();
            }

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentNoSeries")]
        public void Delete(int id)
        {
            var documentNoSeries = _context.DocumentNoSeries.SingleOrDefault(p => p.NumberSeriesId == id);
            if (documentNoSeries != null)
            {
                var noSeriesItems = _context.ProfileDocument.Where(p => p.NumberSeriesId == documentNoSeries.NumberSeriesId).ToList();
                if (noSeriesItems.Any())
                {
                    _context.ProfileDocument.RemoveRange(noSeriesItems);
                }
                _context.DocumentNoSeries.Remove(documentNoSeries);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetDocumentNoSeriesItemsList")]
        public List<DocumentNoSeriesModel> GeGetDocumentNoSeriesItemsList(int? id)
        {
            var documentNoSeries = _context.DocumentNoSeries.Include(p => p.Profile).Include(r => r.Requestor).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DocumentNoSeriesModel> documentNoSeriesModel = new List<DocumentNoSeriesModel>();
            documentNoSeries.ForEach(s =>
            {
                DocumentNoSeriesModel documentNoSeriesModels = new DocumentNoSeriesModel
                {
                    NumberSeriesId = s.NumberSeriesId,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    DocumentNo = s.DocumentNo,
                    VersionNo = s.VersionNo,
                    Title = s.Title,
                    EffectiveDate = s.EffectiveDate,
                    Implementation = s.Implementation,
                    NextReviewDate = s.NextReviewDate,
                    Date = s.Date,
                    RequestorId = s.RequestorId,
                    RequestorName = s.Requestor != null ? s.Requestor.UserName : "",
                    Link = s.Link,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ReasonToVoid = s.ReasonToVoid,
                    SessionId = s.SessionId,


                };
                documentNoSeriesModel.Add(documentNoSeriesModels);
            });
            return documentNoSeriesModel.Where(o => o.ProfileID == id).OrderByDescending(a => a.NumberSeriesId).ToList();
        }
        private string GenerateProfileAuto(DocumentNoSeriesModel noSeriesModel, ProfileAutoNumber profileAutonumber, DocumentProfileNoSeries profilesettings, string documentNo)
        {
            //string documentNo = string.Empty;
            // profilesettings = _context.DocumentProfileNoSeries.FirstOrDefault(s => s.ProfileId == noSeriesModel.ProfileID);
            //ProfileAutoNumber profileSubSectionAutoNumber = _context.ProfileAutoNumber.FirstOrDefault(p => p.ProfileId == profilesettings.ProfileId && p.CompanyId == noSeriesModel.CompanyId && p.DepartmentId == noSeriesModel.DepartmentId && p.SectionId == noSeriesModel.SectionId && p.SubSectionId == noSeriesModel.SubSectionId);
            string LastNoUsed = "";
            if (profileAutonumber == null)
            {
                LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                {
                    ProfileId = profilesettings.ProfileId,
                    CompanyId = noSeriesModel.PlantID,
                    DepartmentId = noSeriesModel.DepartmentId,
                    SectionId = noSeriesModel.SectionId,
                    SubSectionId = noSeriesModel.SubSectionId,
                    LastNoUsed = LastNoUsed,
                    ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                };
                documentNo += LastNoUsed;
                _context.ProfileAutoNumber.Add(newProfileAutoNumber);
            }
            else if (profilesettings.StartWithYear.GetValueOrDefault(false) && profileAutonumber.ProfileYear.GetValueOrDefault(0) > 0 && profileAutonumber.ProfileYear.Value != DateTime.Now.Year)
            {
                var yearchecking = _context.ProfileAutoNumber.Where(p => p.ProfileId == profilesettings.ProfileId && p.ProfileYear > 0 && p.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                if (yearchecking != null)
                {
                    LastNoUsed = (Convert.ToInt32(yearchecking.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                    documentNo += LastNoUsed;
                    if (noSeriesModel != null)
                    {
                        if (noSeriesModel.PlantID != null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == noSeriesModel.PlantID && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId != null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == noSeriesModel.DepartmentId && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId != null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == noSeriesModel.SectionId && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId != null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == noSeriesModel.SubSectionId && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                        if (noSeriesModel.PlantID == null && noSeriesModel.DepartmentId == null && noSeriesModel.SectionId == null && noSeriesModel.SubSectionId == null)
                        {
                            var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                            if (existingprofileAutonumber != null)
                            {
                                existingprofileAutonumber.LastNoUsed = LastNoUsed;
                            }
                            _context.SaveChanges();
                        }
                    }
                    else if (noSeriesModel == null)
                    {

                        var existingprofileAutonumber = _context.ProfileAutoNumber.Where(t => t.ProfileId == profilesettings.ProfileId && t.DepartmentId == null && t.CompanyId == null && t.SectionId == null && t.SubSectionId == null && t.ProfileYear == DateTime.Now.Year).FirstOrDefault();
                        if (existingprofileAutonumber != null)
                        {
                            existingprofileAutonumber.LastNoUsed = LastNoUsed;
                        }
                        _context.SaveChanges();
                    }
                }
                else
                {
                    LastNoUsed = (Convert.ToInt32(profilesettings.StartingNo) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);

                    ProfileAutoNumber newProfileAutoNumber = new ProfileAutoNumber
                    {
                        ProfileId = profilesettings.ProfileId,
                        CompanyId = noSeriesModel.PlantID,
                        DepartmentId = noSeriesModel.DepartmentId,
                        SectionId = noSeriesModel.SectionId,
                        SubSectionId = noSeriesModel.SubSectionId,
                        LastNoUsed = LastNoUsed,
                        ScreenId = noSeriesModel.ScreenID,
                        ScreenAutoNumberId = noSeriesModel.ScreenAutoNumberId,
                        ProfileYear = profilesettings.StartWithYear.GetValueOrDefault(false) ? DateTime.Now.Year : 0
                    };
                    documentNo += LastNoUsed;
                    _context.ProfileAutoNumber.Add(newProfileAutoNumber);

                }
            }
            else
            {
                LastNoUsed = (Convert.ToInt32(profileAutonumber.LastNoUsed) + profilesettings.IncrementalNo.GetValueOrDefault(0)).ToString("D" + profilesettings.NoOfDigit);
                profileAutonumber.LastNoUsed = LastNoUsed;
                documentNo += LastNoUsed;
            }
            _context.SaveChanges();
            return documentNo;
        }

        [HttpGet]
        [Route("IsProfileNoGenerated")]
        public bool IsProfileNoGenerated(long id)
        {
            return _context.DocumentNoSeries.Any(p => p.ProfileId == id);
        }

    }
}