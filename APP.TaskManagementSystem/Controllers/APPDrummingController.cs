﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class APPDrummingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public APPDrummingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPDrummings")]
        public List<APPDrummingModel> Get()
        {
            var drumming = _context.Appdrumming.Include("ModifiedByUser").Select(s => new APPDrummingModel
            {

                DrummingID = s.DrummingId,
                WrokOrderNo = s.WrokOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                ProdOrderNo = s.ProdOrderNo,
                SublotNo = s.SublotNo,
                DrumNo = s.DrumNo,
                DrumWeight = s.DrumWeight,
                BagNo = s.BagNo,
                BagWeight = s.BagWeight,
                TotalWeight = s.TotalWeight,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.DrummingID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPDrummingModel>>(APPDrumming);
            return drumming;
        }



        [HttpGet]
        [Route("GetAPPDrumming")]
        public List<APPDrummingModel> GetICT(int id)
        {
            var drumming = _context.Appdrumming.Include("AddedByUser").Include("ModifiedByUser").Select(s => new APPDrummingModel
            {
               
                ProdOrderNo = s.ProdOrderNo,
                DrummingID = s.DrummingId,
                WrokOrderNo = s.WrokOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,               
                SublotNo = s.SublotNo,
                DrumNo = s.DrumNo,
                DrumWeight = s.DrumWeight,
                BagNo = s.BagNo,
                BagWeight = s.BagWeight,
                TotalWeight = s.TotalWeight,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.DrummingID).Where(t => t.DrummingID == id).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPDrummingModel>>(APPDrumming);
            return drumming;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPDrummingModel> GetData(SearchModel searchModel)
        {
            var drumming = new Appdrumming();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).FirstOrDefault();
                        break;
                    case "Last":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).LastOrDefault();
                        break;
                    case "Next":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).LastOrDefault();
                        break;
                    case "Previous":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).FirstOrDefault();
                        break;
                    case "Last":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).LastOrDefault();
                        break;
                    case "Next":
                        drumming = _context.Appdrumming.OrderBy(o => o.DrummingId).FirstOrDefault(s => s.DrummingId > searchModel.Id);
                        break;
                    case "Previous":
                        drumming = _context.Appdrumming.OrderByDescending(o => o.DrummingId).FirstOrDefault(s => s.DrummingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPDrummingModel>(drumming);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAPPDrumming")]
        public APPDrummingModel Post(APPDrummingModel value)
        {            
           
                var drumming = new Appdrumming
                {
                    WrokOrderNo = value.WrokOrderNo,           
                    ProdOrderNo = value.ProdOrderNo,
                    ItemNo = value.ItemNo,
                    Description = value.Description,
                    SublotNo = value.SublotNo,
                    DrumNo = value.DrumNo,
                    DrumWeight = value.DrumWeight,
                    BagNo = value.BagNo,
                    BagWeight = value.BagWeight,
                    TotalWeight = value.TotalWeight,
                    StatusCodeId = 1,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,


                };
                _context.Appdrumming.Add(drumming);
           
           
            _context.SaveChanges();

            value.DrummingID = drumming.DrummingId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAPPDrumming")]
        public APPDrummingModel Put(APPDrummingModel value)
        {
            var appdrumming = _context.Appdrumming.SingleOrDefault(p => p.DrummingId == value.DrummingID);
            appdrumming.WrokOrderNo = value.WrokOrderNo;
            appdrumming.ProdOrderNo = value.ProdOrderNo;
            appdrumming.ItemNo = value.ItemNo;
            appdrumming.Description = value.Description;            
            appdrumming.SublotNo = value.SublotNo;
            appdrumming.DrumNo = value.DrumNo;
            appdrumming.DrumWeight = value.DrumWeight;
            appdrumming.BagNo = value.BagNo;
            appdrumming.BagWeight = value.BagWeight;
            appdrumming.TotalWeight = value.TotalWeight;
            //APPDrumming.APPDrummingId = value.APPDrummingID;
            appdrumming.ModifiedByUserId = value.ModifiedByUserID;
            appdrumming.ModifiedDate = DateTime.Now;

            appdrumming.ModifiedByUserId = value.ModifiedByUserID;
            appdrumming.ModifiedDate = value.ModifiedDate;
            appdrumming.StatusCodeId = value.StatusCodeID;
            appdrumming.AddedByUserId = value.AddedByUserID;
            appdrumming.AddedDate = value.AddedDate;
            _context.SaveChanges();



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAPPDrumming")]
        public void Delete(int id)
        {
            var appdrumming = _context.Appdrumming.SingleOrDefault(p => p.DrummingId == id);
            var errorMessage = "";
            try
            {
                if (appdrumming != null)
                {
                    _context.Appdrumming.Remove(appdrumming);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}