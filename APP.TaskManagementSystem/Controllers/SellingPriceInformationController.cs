﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SellingPriceInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public SellingPriceInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetSellingPriceInformationsByID")]
        public List<SellingPriceInformationModel> GetSellingPriceInformationsByID(int id)
        {
            //List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            //List<long?> masterDetailIds = new List<long?>();
            var sellingPriceInformations = _context.SellingPriceInformation
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.Product)
                .Include(s => s.SellingPricingTiers)
                .Include(b=>b.BonusCurrency)
                .Include(c=>c.PricingMethod)
                .Where(s => s.SellingCatalogueId == id)
               .AsNoTracking()
                .ToList();
            List<SellingPriceInformationModel> SellingPriceInformationModels = new List<SellingPriceInformationModel>();
            //if (sellingPriceInformations != null && sellingPriceInformations.Count > 0)
            //{
            //    var bonuscurrencyIds = sellingPriceInformations.Where(s => s.BonusCurrencyId != null).Select(s => s.BonusCurrencyId).ToList();
            //    var mincurrencyIds = sellingPriceInformations.Where(s => s.MinCurrencyId != null).Select(s => s.MinCurrencyId).ToList();
            //    var quantityIds = sellingPriceInformations.Where(s => s.Quantity != null).Select(s => s.Quantity).ToList();
            //    var pricingMethodIds = sellingPriceInformations.Where(s => s.PricingMethodId != null).Select(s => s.PricingMethodId).ToList();
            //    if (bonuscurrencyIds.Count > 0)
            //    {
            //        masterDetailIds.AddRange(bonuscurrencyIds);
            //    }
            //    if (mincurrencyIds.Count > 0)
            //    {
            //        masterDetailIds.AddRange(mincurrencyIds);
            //    }
            //    if (quantityIds.Count > 0)
            //    {
            //        masterDetailIds.AddRange(quantityIds);
            //    }
            //    if (pricingMethodIds.Count > 0)
            //    {
            //        masterDetailIds.AddRange(pricingMethodIds);
            //    }
            //    if (masterDetailIds.Count > 0)
            //    {
            //        applicationmasterdetail = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
            //    }
                sellingPriceInformations.ForEach(s =>
                {
                    SellingPriceInformationModel sellingPriceInformationModel = new SellingPriceInformationModel();

                    sellingPriceInformationModel.SellingPriceInformationID = s.SellingPriceInformationId;
                    sellingPriceInformationModel.SellingCatalogueID = s.SellingCatalogueId;
                    sellingPriceInformationModel.ManufacturingID = s.ManufacturingId;
                    sellingPriceInformationModel.ProductID = s.ProductId;
                    sellingPriceInformationModel.PricingMethodID = s.PricingMethodId;
                    sellingPriceInformationModel.BonusCurrencyID = s.BonusCurrencyId;
                    sellingPriceInformationModel.BonusSellingPrice = s.BonusSellingPrice;
                    sellingPriceInformationModel.Quantity = s.Quantity;
                    sellingPriceInformationModel.Bonus = s.Bonus;
                    sellingPriceInformationModel.MinCurrencyID = s.MinCurrencyId;
                    sellingPriceInformationModel.MinPrice = s.MinPrice;
                    sellingPriceInformationModel.MinQty = s.MinQty;
                    sellingPriceInformationModel.StatusCodeID = s.StatusCodeId;
                    sellingPriceInformationModel.AddedByUserID = s.AddedByUserId;
                    sellingPriceInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                    sellingPriceInformationModel.AddedDate = s.AddedDate;
                    sellingPriceInformationModel.ModifiedDate = s.ModifiedDate;
                    sellingPriceInformationModel.ProductName = s.Product?.Code;
                    sellingPriceInformationModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                    if (s.BonusCurrencyId != null)
                    {
                        sellingPriceInformationModel.Currency = s.BonusCurrency?.Value;
                        sellingPriceInformationModel.isBonus = true;
                    }
                    if (s.MinCurrencyId != null)
                    {
                        sellingPriceInformationModel.isMinQty = true;
                       // sellingPriceInformationModel.Currency =  s.MinCurrency?.Value;
                    }
                   
                    sellingPriceInformationModel.PricingMethod = s.PricingMethod?.Value;
                    sellingPriceInformationModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    sellingPriceInformationModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    sellingPriceInformationModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";

                    if (s.SellingPricingTiers != null)
                    {
                        if (sellingPriceInformationModel.PricingMethod.ToLower().Contains("min"))
                        {
                            sellingPriceInformationModel.MinQty = s.SellingPricingTiers.Where(m => m.MinQty != null).Min(s => s.MinQty);
                            decimal? minquantity = s.SellingPricingTiers.Where(m => m.MinQty != null).Min(s => s.MinQty);
                            if (minquantity != null)
                            {
                                sellingPriceInformationModel.BonusSellingPrice = s.SellingPricingTiers.FirstOrDefault(a => a.SellingPrice != null && a.MinQty == minquantity)?.SellingPrice;
                              
                            }
                        }
                        if (sellingPriceInformationModel.PricingMethod.ToLower().Contains("bonus"))
                        {
                            sellingPriceInformationModel.MinQty = s.SellingPricingTiers.Where(m => m.Quantity != null).Min(s => s.Quantity);
                            sellingPriceInformationModel.Bonus = s.SellingPricingTiers.Where(m => m.Bonus != null).Min(s => s.Bonus);
                            decimal? minquantity = s.SellingPricingTiers.Where(m => m.MinQty != null).Min(s => s.MinQty);
                            if (minquantity != null)
                            {
                                sellingPriceInformationModel.BonusSellingPrice = s.SellingPricingTiers.FirstOrDefault(a => a.SellingPrice != null && a.MinQty == minquantity)?.SellingPrice;

                            }
                        }
                    }

                    sellingPriceInformationModel.sellingPricingTiersList = getSellingPriceTiersByID((int)s.SellingPriceInformationId);

                    SellingPriceInformationModels.Add(sellingPriceInformationModel);
                });
           // }
            return SellingPriceInformationModels.OrderByDescending(a => a.SellingPriceInformationID).ToList();
        }

        [HttpGet]
        [Route("GetSellingPriceTiersByID")]
        public List<SellingPricingTiersModel> getSellingPriceTiersByID(int id)
        {
            List<SellingPricingTiersModel> sellingPricingTiersModels = new List<SellingPricingTiersModel>();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            
            var sellingPricingTiers = _context.SellingPricingTiers.Where(t => t.SellingPriceInfoId == id).AsNoTracking().ToList();
            if (sellingPricingTiers != null && sellingPricingTiers.Count > 0)
            {
                var masterDetailIds = sellingPricingTiers.Where(s => s.SellingCurrencyId != null).Select(s => s.SellingCurrencyId).ToList();
                if(masterDetailIds.Count>0)
                {
                    applicationmasterdetail = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                sellingPricingTiers.ForEach(s =>
                {
                    SellingPricingTiersModel sellingPricingTiersModel = new SellingPricingTiersModel();
                    sellingPricingTiersModel.SellingPriceInfoID = s.SellingPriceInfoId;
                    sellingPricingTiersModel.SellingPricingTiersID = s.SellingPricingTiersId;
                    sellingPricingTiersModel.Quantity = s.Quantity;
                    sellingPricingTiersModel.Bonus = s.Bonus;
                    sellingPricingTiersModel.SellingPrice = s.SellingPrice;
                    sellingPricingTiersModel.SellingCurrency = applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == s.SellingCurrencyId) != null ? applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == s.SellingCurrencyId).Value : string.Empty;
                    sellingPricingTiersModel.MinQty = s.MinQty;
                    sellingPricingTiersModel.SellingCurrencyId = s.SellingCurrencyId;
                    sellingPricingTiersModels.Add(sellingPricingTiersModel);
                });

            }
            return sellingPricingTiersModels.OrderByDescending(t => t.SellingPricingTiersID).ToList();
        }
        [HttpPost()]
        [Route("GetSellingPriceInformationData")]
        public ActionResult<SellingPriceInformationModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var SellingPriceInformation = new SellingPriceInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).LastOrDefault();
                        break;
                    case "Next":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).LastOrDefault();
                        break;
                    case "Next":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderBy(o => o.SellingPriceInformationId).FirstOrDefault(s => s.SellingPriceInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        SellingPriceInformation = _context.SellingPriceInformation.OrderByDescending(o => o.SellingPriceInformationId).FirstOrDefault(s => s.SellingPriceInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SellingPriceInformationModel>(SellingPriceInformation);
            return result;
        }

        [HttpPost]
        [Route("InsertSellingPriceInformation")]
        public SellingPriceInformationModel Post(SellingPriceInformationModel value)
        {
            if (value.PricingMethodID != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.PricingMethodID).AsNoTracking().ToList();
                value.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PricingMethodID).Select(a => a.Value).SingleOrDefault() : "";
            }
            if (value.PricingMethod.ToLower().Trim() == "bonus")
            {
                value.MinCurrencyID = null;
                value.MinPrice = null;
                value.MinQty = null;
            }
            if (value.PricingMethod.ToLower().Trim() == "min qty")
            {
                //value.BonusCurrencyID = null;
                value.BonusSellingPrice = null;
            }
            var sellingPriceInformation = new SellingPriceInformation
            {
                SellingCatalogueId = value.SellingCatalogueID,
                ManufacturingId = value.ManufacturingID,
                ProductId = value.ProductID,
                PricingMethodId = value.PricingMethodID,
                BonusCurrencyId = value.BonusCurrencyID,
                BonusSellingPrice = value.BonusSellingPrice,
                Quantity = value.Quantity,
                Bonus = value.Bonus,
                MinCurrencyId = value.MinCurrencyID,
                MinPrice = value.MinPrice,
                MinQty = value.MinQty,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                GenericCodeSupplyToMultipleId=value.GenericCodeSupplyToMultipleId,
            };
            _context.SellingPriceInformation.Add(sellingPriceInformation);
            _context.SaveChanges();

            value.SellingPriceInformationID = sellingPriceInformation.SellingPriceInformationId;
            if (value.ProductID > 0)
            {
                value.ProductName = _context.GenericCodes.Where(s => s.GenericCodeId == value.ProductID).Select(s => s.Code).FirstOrDefault();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSellingPriceInformation")]
        public SellingPriceInformationModel Put(SellingPriceInformationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var sellingPriceInformation = _context.SellingPriceInformation.SingleOrDefault(p => p.SellingPriceInformationId == value.SellingPriceInformationID);
            value.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PricingMethodID).Select(a => a.Value).SingleOrDefault() : "";

            value.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PricingMethodID).Select(a => a.Value).SingleOrDefault() : "";
            if (value.PricingMethod.ToLower().Trim() == "bonus")
            {
                value.MinCurrencyID = null;
                value.MinPrice = null;
                value.MinQty = null;
            }
            if (value.PricingMethod.ToLower().Trim() == "min qty")
            {
                value.BonusSellingPrice = null;
            }
            sellingPriceInformation.SellingCatalogueId = value.SellingCatalogueID;
            sellingPriceInformation.ManufacturingId = value.ManufacturingID;
            sellingPriceInformation.ProductId = value.ProductID;
            sellingPriceInformation.PricingMethodId = value.PricingMethodID;
            sellingPriceInformation.BonusCurrencyId = value.BonusCurrencyID;
            sellingPriceInformation.BonusSellingPrice = value.BonusSellingPrice;
            sellingPriceInformation.MinCurrencyId = value.MinCurrencyID;
            sellingPriceInformation.MinPrice = value.MinPrice;
            sellingPriceInformation.MinQty = value.MinQty;
            sellingPriceInformation.Quantity = value.Quantity;
            sellingPriceInformation.Bonus = value.Bonus;
            sellingPriceInformation.StatusCodeId = value.StatusCodeID.Value;
            sellingPriceInformation.ModifiedByUserId = value.ModifiedByUserID;
            sellingPriceInformation.ModifiedDate = DateTime.Now;
            sellingPriceInformation.GenericCodeSupplyToMultipleId = value.GenericCodeSupplyToMultipleId;
            _context.SaveChanges();
            //if (sellingPriceInformation.PricingMethodId > 0)
            //{
            //    var prisingmethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PricingMethodID).Select(a => a.Value).SingleOrDefault() : "";
            //    if (prisingmethod.ToLower().Trim() == "min qty")
            //    {
            //        var SellingPriceTires = _context.SellingPricingTiers.Where(p => p.SellingPriceInfoId == sellingPriceInformation.SellingPriceInformationId).ToList();
            //        if (SellingPriceTires != null)
            //        {
            //            _context.SellingPricingTiers.RemoveRange(SellingPriceTires);
            //            _context.SaveChanges();

            //        }
            //    }
            //}
            if (value.ProductID > 0)
            {
                value.ProductName = _context.GenericCodes.Where(s => s.GenericCodeId == value.ProductID).Select(s => s.Code).FirstOrDefault();
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteSellingPriceInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var SellingPriceInformation = _context.SellingPriceInformation.Include(s=>s.SellingPricingTiers).Where(p => p.SellingPriceInformationId == id).FirstOrDefault();
                if (SellingPriceInformation != null)
                {
                    _context.SellingPriceInformation.Remove(SellingPriceInformation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("InsertSellingPriceTiers")]
        public SellingPricingTiersModel InsertSellingPriceTiers(SellingPricingTiersModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            ApplicationMasterDetail applicationMasterDetail = null;
            if (value.SellingCurrencyId != null)
            {
                applicationMasterDetail = applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == value.SellingCurrencyId) != null ? applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == value.SellingCurrencyId) : null;
            }
            var sellingPriceTiers = new SellingPricingTiers
            {
                Quantity = value.Quantity,
                Bonus = value.Bonus,
                SellingPriceInfoId = value.SellingPriceInfoID,
                SellingCurrencyId = value.SellingCurrencyId,
                PricingMethodId = value.PricingMethodId,
                MinQty = value.MinQty,
                SellingPrice = value.SellingPrice
            };
            _context.SellingPricingTiers.Add(sellingPriceTiers);
            _context.SaveChanges();
            value.SellingPricingTiersID = sellingPriceTiers.SellingPricingTiersId;
            value.SellingCurrency = applicationMasterDetail != null ? applicationMasterDetail.Value : string.Empty;
            return value;
        }
        [HttpPut]
        [Route("UpdateSellingPriceTiers")]
        public SellingPricingTiersModel UpdateSellingPriceTiers(SellingPricingTiersModel value)
        {
            var sellingPricingTiers = _context.SellingPricingTiers.SingleOrDefault(p => p.SellingPricingTiersId == value.SellingPricingTiersID);

            sellingPricingTiers.Quantity = value.Quantity;
            sellingPricingTiers.Bonus = value.Bonus;
            sellingPricingTiers.SellingPriceInfoId = value.SellingPriceInfoID;
            sellingPricingTiers.SellingCurrencyId = value.SellingCurrencyId;
            sellingPricingTiers.PricingMethodId = value.PricingMethodId;
            sellingPricingTiers.MinQty = value.MinQty;
            sellingPricingTiers.SellingPrice = value.SellingPrice;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteSellingPriceTiers")]
        public ActionResult<string> DeleteSellingPriceTiers(int id)
        {
            try
            {
                var SellingPriceTires = _context.SellingPricingTiers.Where(p => p.SellingPricingTiersId == id).FirstOrDefault();
                if (SellingPriceTires != null)
                {
                    _context.SellingPricingTiers.Remove(SellingPriceTires);
                    _context.SaveChanges();

                }

                return Ok("Record deleted.");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        [Route("GetSellingPriceInformationsByCompanyID")]
        public List<DistributorPricingListModel> GetSellingPriceInformationsByCompanyID(MarginInformationSearchModel searchModel)
        {
            var MarginInformationItems = _context.MarginInformation.Where(c => c.DistirbutorId == searchModel.CustomerId && c.DistributeForId == searchModel.CompanyId).FirstOrDefault();
            var SellingPricingTiers = _context.SellingPricingTiers.AsNoTracking().ToList();
            var navItem = _context.Navitems.Include(g => g.GenericCode).Where(w => w.ItemId > 0);
            if (searchModel.GenericCodeId != null)
            {
                navItem = navItem.Where(w => w.GenericCodeId == searchModel.GenericCodeId);
            }
            if (searchModel.NavItemId != null)
            {
                navItem = navItem.Where(w => w.ItemId == searchModel.NavItemId);
            }
            var navItems = navItem.ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var sellingPriceInformation = _context.SellingPriceInformation.Include(ss => ss.SellingCatalogue).Include(sc => sc.SellingCatalogue.Company).Include(p => p.Product).Where(s => s.SellingPriceInformationId > 0);
            if (searchModel.TillDate != null)
            {
                sellingPriceInformation = sellingPriceInformation.Where(s => searchModel.TillDate.Value.Month == s.SellingCatalogue.FromValidity.Value.Month && searchModel.TillDate.Value.Year == s.SellingCatalogue.FromValidity.Value.Year);
            }
            var sellingPriceInformations = sellingPriceInformation.Where(s => s.SellingCatalogue.CompanyId == searchModel.CustomerId).AsNoTracking().ToList();
            List<DistributorPricingListModel> DistributorPricingListModels = new List<DistributorPricingListModel>();
            List<SellingPriceInformationModel> SellingPriceInformationModels = new List<SellingPriceInformationModel>();
            if (sellingPriceInformations != null && sellingPriceInformations.Count > 0)
            {
                sellingPriceInformations.ForEach(s =>
                {
                    SellingPriceInformationModel sellingPriceInformationModel = new SellingPriceInformationModel();
                    sellingPriceInformationModel.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PricingMethodId).Select(a => a.Value).SingleOrDefault() : "";
                    var navList = navItems.Where(w => w.GenericCodeId == s.ProductId).ToList();
                    if (navList.Count > 0)
                    {
                        List<NavItemModel> NavItemModels = new List<NavItemModel>();
                        navList.ForEach(h =>
                        {
                            NavItemModel NavItemModel = new NavItemModel();
                            NavItemModel.GenericCodeId = h.GenericCodeId;
                            NavItemModel.ItemId = h.ItemId;
                            NavItemModel.No = h.No;
                            NavItemModel.InternalRef = h.InternalRef;
                            NavItemModel.ItemCategoryCode = h.ItemCategoryCode;
                            NavItemModel.Description = h.Description;
                            NavItemModel.Description2 = h.Description2;
                            NavItemModel.BaseUnitofMeasure = h.BaseUnitofMeasure;
                            NavItemModel.GenericCode = h.GenericCode?.Code;
                            NavItemModels.Add(NavItemModel);
                        });
                        sellingPriceInformationModel.ProductItemList = NavItemModels;
                    }
                    if (sellingPriceInformationModel.PricingMethod == "Bonus")
                    {
                        var SellingPricingTiersList = SellingPricingTiers.Where(w => w.SellingPriceInfoId == s.SellingPriceInformationId && w.PricingMethodId == s.PricingMethodId).ToList();
                        if (SellingPricingTiersList.Count > 0)
                        {
                            List<SellingPricingTiersModel> SellingPricingTiersModels = new List<SellingPricingTiersModel>();
                            SellingPricingTiersList.ForEach(h =>
                            {
                                SellingPricingTiersModel SellingPricingTiersModel = new SellingPricingTiersModel();
                                SellingPricingTiersModel.SellingPriceInfoID = h.SellingPriceInfoId;
                                SellingPricingTiersModel.SellingPricingTiersID = h.SellingPricingTiersId;
                                SellingPricingTiersModel.Quantity = h.Quantity;
                                SellingPricingTiersModel.Bonus = h.Bonus;
                                SellingPricingTiersModel.TotalQtyBouns = (h.Bonus != null ? h.Bonus : 0) + (h.Quantity != null ? h.Quantity : 0);
                                var priceTire = (h.Quantity / SellingPricingTiersModel.TotalQtyBouns) * h.Bonus;
                                SellingPricingTiersModel.TotalQtyBounsPrice = priceTire;
                                SellingPricingTiersModels.Add(SellingPricingTiersModel);
                            });
                            sellingPriceInformationModel.sellingPricingTiersList = SellingPricingTiersModels;
                            var minSellingPrice = SellingPricingTiersModels.Where(w => w.TotalQtyBounsPrice > 0).Min(w => w.TotalQtyBounsPrice);
                            sellingPriceInformationModel.MinSellingPrice = minSellingPrice * (1 - (MarginInformationItems.MarginPercentage / 100));
                        }
                        sellingPriceInformationModel.CurrencyId = s.BonusCurrencyId;
                        sellingPriceInformationModel.Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BonusCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    if (sellingPriceInformationModel.PricingMethod == "Min Qty")
                    {
                        var minQty = SellingPricingTiers.Where(w => w.SellingPriceInfoId == s.SellingPriceInformationId && w.PricingMethodId == s.PricingMethodId).OrderBy(m => m.MinQty).FirstOrDefault();
                        sellingPriceInformationModel.CurrencyId = s.BonusCurrencyId;
                        sellingPriceInformationModel.Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BonusCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                        sellingPriceInformationModel.MinSellingPrice = minQty != null ? ((minQty.SellingPrice > 0 ? minQty.SellingPrice : 0) / (minQty.MinQty > 0 ? minQty.MinQty : 1)) : 0;
                    }
                    SellingPriceInformationModels.Add(sellingPriceInformationModel);
                });
                DistributorPricingListModel DistributorPricingList = new DistributorPricingListModel();
                DistributorPricingList.MarginPercentage = MarginInformationItems != null ? MarginInformationItems.MarginPercentage : null;
                List<SellingPriceInformations> SellingPriceInformations = new List<SellingPriceInformations>();
                SellingPriceInformationModels.ForEach(h =>
                {
                    if (h.ProductItemList != null)
                    {
                        h.ProductItemList.ForEach(a =>
                        {
                            SellingPriceInformations DistributorPricingListModel = new SellingPriceInformations();
                            DistributorPricingListModel.MinSellingPrice = h.MinSellingPrice;
                            DistributorPricingListModel.No = a.No;
                            DistributorPricingListModel.Description = a.Description;
                            DistributorPricingListModel.Description = a.Description2;
                            DistributorPricingListModel.BaseUnitofMeasure = a.BaseUnitofMeasure;
                            DistributorPricingListModel.GenericCode = a.GenericCode;
                            DistributorPricingListModel.Currency = h.Currency;
                            DistributorPricingListModel.CurrencyId = h.CurrencyId;
                            DistributorPricingListModel.InternalRef = a.InternalRef;
                            DistributorPricingListModel.ItemCategoryCode = a.ItemCategoryCode;
                            SellingPriceInformations.Add(DistributorPricingListModel);
                        });
                    }
                    DistributorPricingList.SellingPriceInformations = SellingPriceInformations;
                });
                DistributorPricingListModels.Add(DistributorPricingList);
            }
            return DistributorPricingListModels.ToList();
        }
    }
}