﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModels;
using APP.TaskManagementSystem.Helper.Extension;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class WikiPediaController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public WikiPediaController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        /// <summary>
        /// Get Wikipage by User Permission from API by PageCount and UserID
        /// PageCount is to take how many record from DB 
        /// UserID is to check permmsion.
        /// </summary>
        /// <param name="pageCount"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetWikiPage")]
        public List<WikiListModel> Get(int pageCount, long userId)
        {
            var statusIds = new List<int>
            {
                13,
                18,
                9,
                6,
                7
            };
            var dataSource = new List<WikiListModel>();
            var accessPageIds = _context.WikiAccessRight.Where(a => a.UserId == userId && a.PageId != null).AsNoTracking().Select(s => s.PageId).ToList();

            //for split USER company to string and if = SWGP ,then means include SWMY and SWSG
            var usercompnay = _context.ApplicationUserPlant.Include("Plant").Where(w => w.UserId == userId).Select(t => t.Plant.PlantCode).FirstOrDefault();
            var Company = usercompnay == "SWGP" ? "SWMY,SWGP,SWSG" : usercompnay;
            var CompanyList = new List<string>();
            if (Company.Split(',').ToList().Count() > 1)
            {
                Company.Split(',').ToList().ForEach(r => { CompanyList.Add(r); });
            }


            var Page = _context.WikiPage.Include("Group").Include("Department").Where(s => statusIds.Contains(s.StatusCodeId)).OrderByDescending(o => o.AddedDate).Take(pageCount).Select(s => new
            {
                s.ShortDescription,
                s.PageId,
                s.Titile,
                s.StatusCodeId,
                s.AddedByUserId,
                s.AddedDate,
                s.OwnerId,
                s.IsPublishToAll,
                s.Department,
                s.Group

            }).AsNoTracking().ToList();

            Page.ForEach(w =>
            {
                var WikiCompanyList = new List<string>();
                var WikiCodeList = "";
                //for split WIKI company to string and if = SWGP ,then means include SWMY and SWSG
                var companyids = _context.WikiCompany.Include("Company").Where(t => t.PageId == w.PageId).AsNoTracking().Select(a => a.Company.PlantCode).ToList();
                companyids.ForEach(t =>
                {
                    WikiCodeList = t.ToString() + ",";
                });
                WikiCodeList = WikiCodeList.Substring(0, WikiCodeList.Length - 1);

                var wikiCompany = WikiCodeList.Contains("SWGP") ? "SWMY,SWGP,SWSG" : WikiCodeList;
                if (wikiCompany.Split(',').ToList().Count() > 1)
                {
                    wikiCompany.Split(',').ToList().ForEach(r => { WikiCompanyList.Add(r); });
                }

                var data = new WikiListModel
                {
                    ShortDescription = w.ShortDescription,
                    PageID = w.PageId,
                    Title = w.Titile,
                    StatusCodeId = w.StatusCodeId,
                    AddedByUserId = w.AddedByUserId,
                    AddedDate = w.AddedDate,
                    WikiCompany = WikiCodeList,
                    DepartmentName = w.Department?.Description,
                    CategoryName = w.Group?.Name,
                    RequiredPermission = w.OwnerId == userId ? true : w.IsPublishToAll == true ? (companyids.Contains(Company) || CompanyList.Contains(WikiCodeList) || WikiCompanyList.Contains(Company)) ? true : false : accessPageIds.Any(up => up.Value == w.PageId),
                };

                dataSource.Add(data);
            });
            return dataSource;
        }
        /// <summary>
        /// For display the wiki information in wikipage.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetWikiInfo")]
        public WikiDetailModel GetData(long id, long userId)
        {
            var dataSource = new WikiDetailModel();
            var Page = _context.WikiPage.Include("Owner").Include("Category").Include("Department").Include("WikiAccess").Include("Group").Where(s => s.PageId == id).Select(s => new WikiPage
            {
                ShortDescription = s.ShortDescription,
                PageId = s.PageId,
                Titile = s.Titile,
                StatusCodeId = s.StatusCodeId,
                AddedByUserId = s.AddedByUserId,
                AddedDate = s.AddedDate,
                Owner = s.Owner,
                IsPublishToAll = s.IsPublishToAll,
                WikiAccess = s.WikiAccess,
                Group = s.Group,
                Category = s.Category,
                Section = s.Section,
                EffectiveDate = s.EffectiveDate,
                NextReviewDate = s.NextReviewDate,
                ImplementationDate = s.ImplementationDate,
                PublishDate = s.PublishDate,
                RevisionNo = s.RevisionNo,
                IssueNo = s.IssueNo,
                Department = s.Department

            }).FirstOrDefault();
            if (Page != null)
            {

                var methodID = Page.WikiAccess.FirstOrDefault()?.TrainingMethod;
                var trainingType = _context.CodeMaster.Where(w => w.CodeId == methodID).FirstOrDefault();
                var companyids = _context.WikiCompany.Include("Company").Where(t => t.PageId == Page.PageId).Select(a => a.Company.PlantCode).ToList();
                var companyName = "";
                companyids.ForEach(t =>
                {
                    companyName = t.ToString() + ",";
                });
                companyName = companyName.Substring(0, companyName.Length - 1);
                var data = new WikiDetailModel
                {
                    ShortDescription = Page.ShortDescription,
                    PageID = Page.PageId,
                    Title = Page.Titile,
                    CompanyName = companyName,
                    DepartmentName = Page.Department?.Description,
                    DocumentType = Page.Group?.Name,
                    CategoryName = Page.Category?.Name,
                    Topic = Page.Section?.Name,
                    EffectiveDate = Page.EffectiveDate,
                    NextReviewDate = Page.NextReviewDate,
                    ImplementationDate = Page.ImplementationDate,
                    PublishDate = Page.PublishDate,
                    Owner = Page.Owner.UserName,
                    RevisionNo = Page.RevisionNo,
                    IssueNo = Page.IssueNo,
                    StatusCodeId = Page.StatusCodeId,
                    AddedByUserId = Page.AddedByUserId,
                    AddedDate = Page.AddedDate,
                    TrainingMethod = trainingType?.CodeValue,

                };
                dataSource = data;
            }
            return dataSource;
        }
        /// <summary>
        /// Go in to the Page Detail by PageID and UserID
        /// UserID is just to get the permission
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPageDetail")]
        public WikiDetailModel GetDetail(long id, long userId)
        {
            var dataSource = new WikiDetailModel();
            var Page = _context.WikiPage.Include("Owner").Include("Category").Include("Department").Include("WikiCompany.Company").Include("WikiAccess").Include("Group").Where(s => s.PageId == id).FirstOrDefault();
            if (Page != null)
            {

                var methodID = Page.WikiAccess.FirstOrDefault()?.TrainingMethod;
                var trainingType = _context.CodeMaster.Where(w => w.CodeId == methodID).FirstOrDefault();
                var companyList = Page.WikiCompany.Select(t => t.Company?.PlantCode).ToList();
                var companyName = "";

                companyList.ForEach(t => { companyName += t.ToString() + ","; });


                companyName = companyName.Substring(0, companyName.Length - 1);
                var data = new WikiDetailModel
                {
                    ShortDescription = Page.ShortDescription,
                    PageID = Page.PageId,
                    Title = Page.Titile,
                    CompanyName = companyName,
                    DepartmentName = Page.Department?.Description,
                    DocumentType = Page.Group?.Name,
                    CategoryName = Page.Category?.Name,
                    Topic = Page.Section?.Name,
                    EffectiveDate = Page.EffectiveDate,
                    NextReviewDate = Page.NextReviewDate,
                    ImplementationDate = Page.ImplementationDate,
                    PublishDate = Page.PublishDate,
                    Owner = Page.Owner.UserName,
                    RevisionNo = Page.RevisionNo,
                    IssueNo = Page.IssueNo,
                    StatusCodeId = Page.StatusCodeId,
                    AddedByUserId = Page.AddedByUserId,
                    AddedDate = Page.AddedDate,
                    TrainingMethod = trainingType?.CodeValue,
                    PrintPermission = _context.WikiAccessRight.Any(g => g.PageId == id && g.UserId == userId && g.IsPrintable == true),
                    PageContent = Page.PageContent,
                    TotalComments = _context.PageComment.Count(l => l.PageId == id),
                    TotalLikes = _context.PageLikes.Count(l => l.PageId == id),
                    SessionId = Page.SessionId,
                    Comments = new List<WikiCommentModel>(),

                };
                var pagecomments = _context.PageComment.Include("User").Where(c => c.PageId == id).AsNoTracking().ToList();
                pagecomments.ForEach(pc =>
                {
                    var comment = new WikiCommentModel
                    {
                        WikiPageID = pc.PageId,
                        CommentID = pc.PageCommentId,
                        AddedDate = pc.AddedDate.Value,
                        AddedByUser = pc.User?.UserName,
                        AddedByUserID = pc.UserId,
                        Comment = pc.Comment,
                        IsEdit = pc.IsEdit,
                        ModifiedDate = pc.ModifiedDate
                    };
                    data.Comments.Add(comment);
                });
                dataSource = data;
            }
            return dataSource;
        }
        /// <summary>
        /// For Searching wiki by title
        /// </summary>
        /// <param name="search"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSearchWiki")]
        public List<WikiListModel> Get(string search, long userId)
        {
            var statusIds = new List<int>();
            statusIds.Add(13);
            statusIds.Add(18);
            statusIds.Add(9);
            statusIds.Add(6);
            statusIds.Add(7);
            var dataSource = new List<WikiListModel>();
            var accessPageIds = _context.WikiAccessRight.Where(a => a.UserId == userId && a.PageId != null).AsNoTracking().Select(s => s.PageId).ToList();
            var usercompnay = _context.ApplicationUserPlant.Include("Plant").Where(w => w.UserId == userId).Select(t => t.Plant.PlantCode).FirstOrDefault();
            var Company = usercompnay == "SWGP" ? "SWMY,SWGP,SWSG" : usercompnay;
            var CompanyList = new List<string>();
            var Page = new List<WikiPage>();
            if (Company.Split(',').ToList().Count() > 1)
            {
                Company.Split(',').ToList().ForEach(r => { CompanyList.Add(r); });
            }
            if (search == null)
            {
                Page = _context.WikiPage.Include("Group").Include("Department").Include("Owner").Where(s => statusIds.Contains(s.StatusCodeId)).OrderByDescending(o => o.AddedDate).Take(20).Select(w => new WikiPage
                {

                    ShortDescription = w.ShortDescription,
                    PageId = w.PageId,
                    Titile = w.Titile,
                    StatusCodeId = w.StatusCodeId,
                    AddedByUserId = w.AddedByUserId,
                    AddedDate = w.AddedDate,
                    Owner = w.Owner,
                    IsPublishToAll = w.IsPublishToAll,
                    Department = w.Department,
                    Group = w.Group,

                }).AsNoTracking().ToList();
            }
            else
            {
                Page = _context.WikiPage.Include("Owner").Include("Group").Include("Department").Where(s => statusIds.Contains(s.StatusCodeId) && s.Titile.Contains(search)).OrderByDescending(o => o.AddedDate).Select(w => new WikiPage
                {

                    ShortDescription = w.ShortDescription,
                    PageId = w.PageId,
                    Titile = w.Titile,
                    StatusCodeId = w.StatusCodeId,
                    AddedByUserId = w.AddedByUserId,
                    AddedDate = w.AddedDate,
                    Owner = w.Owner,
                    IsPublishToAll = w.IsPublishToAll,
                    Department = w.Department,
                    Group = w.Group,

                }).AsNoTracking().ToList();
            }
            Page.ForEach(w =>
            {

                try
                {
                    var WikiCompanyList = new List<string>();
                    var WikiCodeList = "";
                    var companyids = _context.WikiCompany.Include("Company").Where(t => t.PageId == w.PageId).AsNoTracking().Select(a => a.Company.PlantCode).ToList();
                    companyids.ForEach(t =>
                    {
                        WikiCodeList = t.ToString() + ",";
                    });
                    WikiCodeList = WikiCodeList.Substring(0, WikiCodeList.Length - 1);

                    var wikiCompany = WikiCodeList.Contains("SWGP") ? "SWMY,SWGP,SWSG" : WikiCodeList;
                    if (wikiCompany.Split(',').ToList().Count() > 1)
                    {
                        wikiCompany.Split(',').ToList().ForEach(r => { WikiCompanyList.Add(r); });
                    }

                    var data = new WikiListModel
                    {
                        ShortDescription = w.ShortDescription,
                        PageID = w.PageId,
                        Title = w.Titile,
                        StatusCodeId = w.StatusCodeId,
                        AddedByUserId = w.AddedByUserId,
                        AddedDate = w.AddedDate,
                        DepartmentName = w.Department?.Description,
                        CategoryName = w.Group?.Name,
                        WikiCompany = WikiCodeList,
                        RequiredPermission = w.Owner?.UserId == userId ? true : w.IsPublishToAll == true ? (companyids.Contains(Company) || CompanyList.Contains(WikiCodeList) || WikiCompanyList.Contains(Company)) ? true : false : accessPageIds.Any(up => up.Value == w.PageId),
                    };

                    dataSource.Add(data);
                }
                catch (Exception ex)
                {
                    throw new AppException(ex.Message);
                }
            });
            return dataSource;
        }
        /// <summary>
        /// Get wikipage duty
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDuty")]
        public List<WikiAccessRightModel> GetData(long id)
        {
            var dataSource = new List<WikiAccessRightModel>();
            var member = _context.WikiAccessRight.Include("User").Include("Group").Where(w => w.PageId == id).Select(w => new
            {
                UserID = w.UserId,
                PageID = w.PageId,
                UserGroup = w.Group.Name,
                Name = w.User.UserName,
                WikiAccessRightID = w.WikiAccessRightId,
                TypeOfRights = w.TypeOfRights,
                Duty = w.Duty,
            }).AsNoTracking().ToList();
            member.ForEach(c =>
            {
                var model = new WikiAccessRightModel()
                {
                    UserID = c.UserID,
                    UserName = c.Name,
                    Group = c.UserGroup,
                    PageID = c.PageID,
                    WikiAccessRightID = c.WikiAccessRightID,
                    TypeOfRights = c.TypeOfRights,
                    Duty = c.Duty,
                };
                dataSource.Add(model);
            });

            return dataSource.Where(t => t.Duty == 65 || t.Duty == 66).ToList();

        }
        /// <summary>
        /// Post and Save Comment in wiki.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertComment")]
        public WikiCommentModel Post(WikiCommentModel value)
        {
            var comments = new PageComment
            {
                Comment = value.Comment,
                PageId = value.WikiPageID,
                AddedDate = DateTime.Now,
                UserId = value.AddedByUserID.Value,
                IsEdit = false
            };
            _context.PageComment.Add(comments);
            _context.SaveChanges();

            value.CommentID = comments.PageCommentId;

            return value;

        }
        /// <summary>
        /// Modify and save Wiki Comment
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateComment")]
        public WikiCommentModel Put(WikiCommentModel value)
        {
            var data = _context.PageComment.SingleOrDefault(p => p.PageCommentId == value.CommentID);
            data.Comment = value.Comment;
            data.IsEdit = true;
            data.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }

        /// <summary>
        /// Delete comment only
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Route("DeleteComment")]
        public void Delete(int id)
        {
            var data = _context.PageComment.SingleOrDefault(p => p.PageCommentId == id);
            if (data != null)
            {
                _context.PageComment.Remove(data);
                _context.SaveChanges();
            }
        }
        /// <summary>
        /// This is to get the 'Page link related' (See also) data for WIKI by PageID..
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSeeAlso")]
        public List<LinkPageModel> GetSeeAlso(long id, long userID)
        {
            var accessPageIds = _context.WikiAccessRight.Where(a => a.UserId == userID && a.PageId != null).AsNoTracking().Select(s => s.PageId).ToList();
            var LinkMasterList = new List<PageLinkCondition>();
            var LinkModelList = new List<LinkPageModel>();
            var related = _context.LinkPages.Include("Page").Include("LinkPageCondition").Where(t => t.ParentPageId == id).AsNoTracking().ToList();
            related.ForEach(e =>
            {
                var linkmodel = new LinkPageModel()
                {
                    Code = e.LinkPageCondition.Code,
                    LinkCondition = e.LinkPageCondition.LinkCondition,
                    PageLinkConditionID = e.LinkPageConditionId.Value,
                    PageID = e.PageId.Value,
                    Title = e.Page.Titile,
                    Status = e.Page.StatusCodeId,
                    RequiredPermission = e.Page.OwnerId == userID ? true : e.Page.IsPublishToAll == true ? true : accessPageIds.Any(up => up.Value == e.PageId),
                };
                LinkModelList.Add(linkmodel);
            });
            return LinkModelList;
        }
        /// <summary>
        ///  This is to get the 'attachment document' data for WIKI by SessionID.
        /// </summary>
        /// <param name="SessionID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAttachFile")]
        public List<DocumentModel> Get(Guid SessionID, long userID)
        {
            var userId = userID;
            var document = _context.Documents.Include("DocumentRights.User").Select(s => new { s.SessionId, s.DocumentId, s.FileName, s.IsSpecialFile, s.DocumentType, s.IsTemp, s.DocumentRights }).Where(d => d.SessionId == SessionID && d.DocumentType == 321 && d.IsTemp == false).AsNoTracking().ToList();
            var AddDoc = new List<DocumentModel>();
            document.ForEach(d =>
            {
                var doc = new DocumentModel
                {
                    DocumentID = d.DocumentId,
                    LinkFileName = d.FileName,
                    IsSpecialFile = d.IsSpecialFile,
                    UserAccessID = new List<string>(),
                    HasAccess = d.IsSpecialFile == true ? d.DocumentRights.Any(r => r.UserId == userId && r.DocumentId == d.DocumentId) : true,

                };
                if (d.DocumentRights != null && d.DocumentRights.Count > 0)
                {
                    d.DocumentRights.ToList().ForEach(da =>
                    {
                        if (da.User != null)
                        {
                            doc.UserAccess += da.User.UserName.ToString() + ",";
                            doc.UserAccessID.Add(da.User.UserName.ToString());
                        }
                    });
                }
                AddDoc.Add(doc);
            });

            return AddDoc;
        }

        [HttpGet]
        [Route("GetWikiCategory")]
        public List<Category> GetCategory()
        {
            var categories = _context.Category.Where(s => s.StatusCodeId == 1).Select(w => new Category
            {
                CategoryId = w.CategoryId,
                Name = w.Name
            }).AsNoTracking().ToList();
            return categories;
        }

        [HttpGet]
        [Route("GetWikiDepartment")]
        public List<Department> GetDepartment()
        {
            var departments = _context.Department.Where(s => s.StatusCodeId == 1).AsNoTracking().Select(w => new Department { DepartmentId = w.DepartmentId, Name = w.Name }).ToList();
            return departments;
        }

        [HttpGet]
        [Route("GetPlant")]
        public List<Plant> GetPlant()
        {
            var plants = _context.Plant.AsNoTracking().Select(w => new Plant { PlantId = w.PlantId, PlantCode = w.PlantCode, Description = w.Description }).ToList();
            return plants;
        }


        [HttpGet]
        [Route("GetLanguage")]
        public List<LanguageMaster> GetLanguage()
        {

            var languages = _context.LanguageMaster.AsNoTracking().Select(w => new LanguageMaster { LanguageId = w.LanguageId, Name = w.Name }).ToList();
            return languages;
        }

        [HttpGet]
        [Route("GetWikiTag")]
        public List<Tag> GetWikiTag()
        {

            var tags = _context.Tag.Where(x => x.StatusCodeId == 1).AsNoTracking().Select(w => new Tag { TagId = w.TagId, Tag1 = w.Tag1 }).ToList();
            return tags;
        }

        [HttpGet]
        [Route("GetTopic")]
        public List<Section> GetTopic()
        {

            var topic = _context.Section.Where(s => s.IsWiki == true && s.StatusCodeId == 1).AsNoTracking().Select(w => new Section { SectionId = w.SectionId, Name = w.Name }).ToList();
            return topic;
        }

        [HttpGet]
        [Route("GetDocumentType")]
        public List<WikiGroup> GetDocumentType()
        {

            var documentType = _context.WikiGroup.Where(s => s.StatusCodeId == 1).AsNoTracking().Select(w => new WikiGroup { WikiGroupId = w.WikiGroupId, Name = w.Name }).ToList();
            return documentType;
        }

        [HttpGet]
        [Route("GetTrainingMethod")]
        public List<CodeMaster> GetTrainingMethod()
        {

            var codemethod = _context.CodeMaster.Where(q => q.CodeType == "TrainingMethod").AsNoTracking().ToList();
            return codemethod;
        }

        [HttpGet]
        [Route("GetStatus")]
        public List<CodeMaster> GetStatus()
        {
            var statusIds = new List<int>();
            statusIds.Add(13);
            statusIds.Add(18);
            statusIds.Add(9);
            statusIds.Add(6);
            statusIds.Add(7);
            var codemethod = _context.CodeMaster.Where(q => statusIds.Contains(q.CodeId)).AsNoTracking().ToList();
            return codemethod;
        }

        [HttpGet]
        [Route("GetTrainer")]
        public List<ApplicationUserModel> GetTrainer()
        {

            var trainer = _context.WikiAccess.Where(w => w.UserId != null).AsNoTracking().Select(t => t.UserId).ToList();

            var employeeData = new List<ApplicationUserModel>();
            if (trainer.Count > 0)
            {
                employeeData = _context.Employee.Include("User").Include("Designation").Include("Section").Include("Department").Include("SubSection").Select(c => new ApplicationUserModel
                {
                    LoginID = c.User.LoginId,
                    UserID = c.UserId.Value,
                    UserName = c.User.UserName,
                    Designation = c.Designation.Name,
                    NickName = c.NickName,
                    Section = c.Section.Name

                }).Where(p => trainer.Contains(p.UserID)).AsNoTracking().ToList();
            }
            return employeeData;
        }

        [HttpGet]
        [Route("GetOwner")]
        public List<ApplicationUserModel> GetOwner()
        {

            var wikiOwner = _context.WikiPage.Where(w => w.OwnerId != null).AsNoTracking().Select(t => t.OwnerId).ToList();
            var employeeData = new List<ApplicationUserModel>();
            if (wikiOwner.Count > 0)
            {
                employeeData = _context.Employee.Include("User").Include("Designation").Include("Section").Include("Department").Include("SubSection").Select(c => new ApplicationUserModel
                {
                    LoginID = c.User.LoginId,
                    UserID = c.UserId.Value,
                    UserName = c.User.UserName,
                    Designation = c.Designation.Name,
                    NickName = c.NickName,
                    Section = c.Section.Name

                }).Where(p => wikiOwner.Contains(p.UserID)).AsNoTracking().ToList();
            }
            return employeeData;
        }

        [HttpGet]
        [Route("GetPrimaryApp")]
        public List<ApplicationUserModel> GetPrimaryApp()
        {

            var PrimaryApproverId = _context.WikiPage.Where(w => w.PrimaryApproverId != null).AsNoTracking().Select(t => t.PrimaryApproverId).ToList();

            var employeeData = new List<ApplicationUserModel>();
            if (PrimaryApproverId.Count > 0)
            {
                employeeData = _context.Employee.Include("User").Include("Designation").Include("Section").Include("Department").Include("SubSection").Select(c => new ApplicationUserModel
                {
                    LoginID = c.User.LoginId,
                    UserID = c.UserId.Value,
                    UserName = c.User.UserName,
                    Designation = c.Designation.Name,
                    NickName = c.NickName,
                    Section = c.Section.Name

                }).Where(p => PrimaryApproverId.Contains(p.UserID)).AsNoTracking().ToList();
            }
            return employeeData;
        }

        [HttpGet]
        [Route("GetNextApp")]
        public List<ApplicationUserModel> GetNextApp()
        {
            var NextApproverId = _context.WikiPage.Where(w => w.NextApproverId != null).AsNoTracking().Select(t => t.NextApproverId).ToList();

            var employeeData = new List<ApplicationUserModel>();
            if (NextApproverId.Count > 0)
            {
                employeeData = _context.Employee.Include("User").Include("Designation").Include("Section").Include("Department").Include("SubSection").Select(c => new ApplicationUserModel
                {
                    LoginID = c.User.LoginId,
                    UserID = c.UserId.Value,
                    UserName = c.User.UserName,
                    Designation = c.Designation.Name,
                    NickName = c.NickName,
                    Section = c.Section.Name

                }).Where(p => NextApproverId.Contains(p.UserID)).AsNoTracking().ToList();
            }
            return employeeData;
        }



        [HttpPost]
        [Route("GetAdvSearch")]
        public List<WikiListModel> GetAdvSearch(WikiSearchModel model)
        {
            var wikipage = new List<WikiListModel>();
            try
            {
                // generating dynamic query filter 
                var filters = new List<Filter>();
                var filter = new Filter();
                //if (!string.IsNullOrEmpty(model.DocumentNo))
                //{
                //    filter = new Filter
                //    {
                //        PropertyName = "IssueNo",
                //        Operation = Op.Contains,
                //        Value = model.DocumentNo
                //    };
                //    filters.Add(filter);
                //}
                //if (!string.IsNullOrEmpty(model.ShortDesc))
                //{
                //    filter = new Filter
                //    {
                //        PropertyName = "ShortDescription",
                //        Operation = Op.Contains,
                //        Value = model.ShortDesc
                //    };
                //    filters.Add(filter);
                //}

                if (model.CategoryID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "CategoryID",
                        Operation = Op.EqualsNull,
                        Value = model.CategoryID
                    };
                    filters.Add(filter);
                }
                if (model.DepartmentID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "DepartmentID",
                        Operation = Op.EqualsNull,
                        Value = model.DepartmentID.Value
                    };
                    filters.Add(filter);
                }
                if (model.DocumentTypeID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "GroupID",
                        Operation = Op.EqualsNull,
                        Value = model.DocumentTypeID.Value
                    };
                    filters.Add(filter);
                }
                if (model.LanguageID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "LanguageID",
                        Operation = Op.EqualsNull,
                        Value = model.LanguageID.Value
                    };
                    filters.Add(filter);
                }
                if (model.IsChinese == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "IsChinese",
                        Operation = Op.EqualsNull,
                        Value = model.IsChinese
                    };
                    filters.Add(filter);
                }
                if (model.IsMalay == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "IsMalay",
                        Operation = Op.EqualsNull,
                        Value = model.IsMalay
                    };
                    filters.Add(filter);
                }
                if (model.NoTranslate == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "NoTransalation",
                        Operation = Op.EqualsNull,
                        Value = model.NoTranslate
                    };
                    filters.Add(filter);
                }
                if (model.IsGMP == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "IsGMPRelated",
                        Operation = Op.EqualsNull,
                        Value = model.IsGMP
                    };
                    filters.Add(filter);
                }
                if (model.IsSME == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "SMECommentRequired",
                        Operation = Op.EqualsNull,
                        Value = model.IsSME
                    };
                    filters.Add(filter);
                }
                if (model.IsTest == true)
                {
                    filter = new Filter
                    {
                        PropertyName = "IsTestPage",
                        Operation = Op.EqualsNull,
                        Value = model.IsTest
                    };
                    filters.Add(filter);
                }
                if (model.OwnerID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "OwnerID",
                        Operation = Op.EqualsNull,
                        Value = model.OwnerID
                    };
                    filters.Add(filter);
                }

                if (model.PriApprovalID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "PrimaryApproverID",
                        Operation = Op.EqualsNull,
                        Value = model.PriApprovalID
                    };
                    filters.Add(filter);
                }
                if (model.NextApprovalID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "NextApproverID",
                        Operation = Op.EqualsNull,
                        Value = model.NextApprovalID
                    };
                    filters.Add(filter);
                }
                if (model.StatusID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "StatusCodeID",
                        Operation = Op.EqualInt32,
                        Value = model.StatusID
                    };
                    filters.Add(filter);
                }
                if (model.SecondStatusID > 0)
                {
                    filter = new Filter
                    {
                        PropertyName = "SecondaryStatusID",
                        Operation = Op.EqualNullInt32,
                        Value = model.SecondStatusID
                    };
                    filters.Add(filter);
                }
                if (model.EffectiveDate != null && model.EffectiveDate != DateTime.MinValue)
                {
                    filter = new Filter
                    {
                        PropertyName = "EffectiveDate",
                        Operation = Op.EqualsNull,
                        Value = model.EffectiveDate
                    };
                    filters.Add(filter);
                }
                if (model.ExpectedDate != null && model.ExpectedDate != DateTime.MinValue)
                {
                    filter = new Filter
                    {
                        PropertyName = "ExpectedDate",
                        Operation = Op.EqualsNull,
                        Value = model.ExpectedDate
                    };
                    filters.Add(filter);
                }
                if (model.ImplementDate != null && model.ImplementDate != DateTime.MinValue)
                {
                    filter = new Filter
                    {
                        PropertyName = "ImplementationDate",
                        Operation = Op.EqualsNull,
                        Value = model.ImplementDate
                    };
                    filters.Add(filter);
                }
                if (model.NextReviewDate != null && model.NextReviewDate != DateTime.MinValue)
                {
                    filter = new Filter
                    {
                        PropertyName = "NextReviewDate",
                        Operation = Op.EqualsNull,
                        Value = model.NextReviewDate
                    };
                    filters.Add(filter);
                }
                if (model.ReviewDate != null && model.ReviewDate != DateTime.MinValue)
                {
                    filter = new Filter
                    {
                        PropertyName = "StartReviewDate",
                        Operation = Op.EqualsNull,
                        Value = model.ReviewDate
                    };
                    filters.Add(filter);
                }

                var titlepage = new List<WikiPage>();
                var page = new List<WikiPage>();
                var pages = new List<WikiPage>();
                var pagesCompany = new List<WikiPage>();

                var statusIds = new List<int>();
                statusIds.Add(13);
                statusIds.Add(18);
                statusIds.Add(9);
                statusIds.Add(6);
                statusIds.Add(7);

                if (filters.Count > 0)
                {
                    var dynamicCondition = ExpressionBuilder.GetExpression<WikiPage>(filters).Compile();
                    pages = _context.WikiPage.Include("Group").Include("Department").AsNoTracking().Where(dynamicCondition).Where(w => statusIds.Contains(w.StatusCodeId)).Select(s => new WikiPage
                    {
                        ShortDescription = s.ShortDescription,
                        PageId = s.PageId,
                        Titile = s.Titile,
                        StatusCodeId = s.StatusCodeId,
                        AddedByUserId = s.AddedByUserId,
                        AddedDate = s.AddedDate,
                        OwnerId = s.OwnerId,
                        Group = s.Group,
                        Department = s.Department,
                        IsPublishToAll = s.IsPublishToAll

                    }).ToList();
                }
                else
                {
                    pages = _context.WikiPage.Include("Group").Include("Department").Where(w => statusIds.Contains(w.StatusCodeId)).Select(s => new WikiPage
                    {
                        ShortDescription = s.ShortDescription,
                        PageId = s.PageId,
                        Titile = s.Titile,
                        StatusCodeId = s.StatusCodeId,
                        AddedByUserId = s.AddedByUserId,
                        AddedDate = s.AddedDate,
                        OwnerId = s.OwnerId,
                        Group = s.Group,
                        Department = s.Department,
                        IsPublishToAll = s.IsPublishToAll

                    }).AsNoTracking().ToList();
                }

                if (!string.IsNullOrEmpty(model.DocumentNo))
                {
                    pages = pages.Where(id => id.IssueNo.Contains(model.DocumentNo)).ToList();
                }
                if (!string.IsNullOrEmpty(model.ShortDesc))
                {
                    pages = pages.Where(id => id.ShortDescription.Contains(model.ShortDesc)).ToList();

                }

                if (!string.IsNullOrEmpty(model.Title))
                {
                    var titles = model.Title.ToLower().Split(';').ToList();
                    if (titles.Count() > 0)
                    {
                        titles.ForEach(w =>
                        {
                            var pagesearch = pages.Where(id => id.Titile.ToLower().Contains(w)).ToList();
                            titlepage.AddRange(pagesearch);
                        });

                    }
                    pages = titlepage;
                }

                if (model.CompanyID > 0)
                {
                    var wikiIds = _context.WikiCompany.Where(t => t.CompanyId == model.CompanyID).Select(s => s.PageId).ToList();
                    pages = pages.Where(id => wikiIds.Contains(id.PageId)).ToList();
                }

                // checking tagID if exist filter by wiki tags 
                if (model.TagID > 0)
                {
                    var wikiIds = _context.WikiTag.Where(t => t.TagId == model.TagID).Select(s => s.WikiPageId).ToList();

                    pages = pages.Where(id => wikiIds.Contains(id.PageId)).ToList();
                }
                if (model.TrainerID > 0)
                {
                    var wikiIds = _context.WikiAccess.Where(t => t.UserId == model.TrainerID).Select(s => s.PageId).ToList();

                    pages = pages.Where(id => wikiIds.Contains(id.PageId)).ToList();
                }

                var accessPageIds = _context.WikiAccessRight.Where(a => a.UserId == model.ByUserID && a.PageId != null).Select(s => s.PageId).ToList();
                //for split USER company to string and if = SWGP ,then means include SWMY and SWSG
                var usercompnay = _context.ApplicationUserPlant.Include("Plant").Where(w => w.UserId == model.ByUserID).Select(t => t.Plant.PlantCode).FirstOrDefault();
                var Company = usercompnay == "SWGP" ? "SWMY,SWGP,SWSG" : usercompnay;
                var CompanyList = new List<string>();
                if (Company.Split(',').ToList().Count() > 1)
                {
                    Company.Split(',').ToList().ForEach(r => { CompanyList.Add(r); });
                }

                pages.ForEach(x =>
                {
                    var WikiCompanyList = new List<string>();
                    var WikiCodeList = "";
                    //for split WIKI company to string and if = SWGP ,then means include SWMY and SWSG
                    var companyids = _context.WikiCompany.Include("Company").Where(t => t.PageId == x.PageId).Select(a => a.Company.PlantCode).ToList();
                    companyids.ForEach(t =>
                    {
                        WikiCodeList = t.ToString() + ",";
                    });
                    WikiCodeList = WikiCodeList.Substring(0, WikiCodeList.Length - 1);

                    var wikiCompany = WikiCodeList.Contains("SWGP") ? "SWMY,SWGP,SWSG" : WikiCodeList;
                    if (wikiCompany.Split(',').ToList().Count() > 1)
                    {
                        wikiCompany.Split(',').ToList().ForEach(r => { WikiCompanyList.Add(r); });
                    }
                    var module = new WikiListModel
                    {
                        ShortDescription = x.ShortDescription,
                        PageID = x.PageId,
                        Title = x.Titile,
                        StatusCodeId = x.StatusCodeId,
                        AddedByUserId = x.AddedByUserId,
                        AddedDate = x.AddedDate,
                        DepartmentName = x.Department?.Description,
                        CategoryName = x.Group?.Name,
                        WikiCompany = WikiCodeList,
                        RequiredPermission = x.OwnerId == model.ByUserID ? true : x.IsPublishToAll == true ? (companyids.Contains(Company) || CompanyList.Contains(WikiCodeList) || WikiCompanyList.Contains(Company)) ? true : false : accessPageIds.Any(up => up.Value == x.PageId),
                    };
                    wikipage.Add(module);
                });
            }
            catch (Exception ex)
            {
                throw new AppException(ex.Message);
            }
            return wikipage;
        }
    }
}