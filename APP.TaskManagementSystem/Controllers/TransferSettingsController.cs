﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TransferSettingsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TransferSettingsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTransferSettings")]
        public List<TransferSettingsModel> Get()
        {
            var transferSettings = _context.TransferSettings.Include("AddedByUser").Include("ModifiedByUser").Select(s => new TransferSettingsModel
            {
                TransferConfigId = s.TransferConfigId,
                FromLocationId = s.FromLocationId,
                FromLocation = s.FromLocation.Name,
                ToLocationId = s.ToLocationId,
                ToLocation = s.ToLocation.Name,
                IsTransfer = s.IsTransfer,
                IsReclass = s.IsReclass,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.TransferConfigId).AsNoTracking().ToList();
            //var result = _mapper.Map<List<TransferSettingsModel>>(TransferSettings);
            return transferSettings;
        }
        [HttpGet]
        [Route("GetICTFromToLocations")]
        public List<ICTMasterModel> GetICTFromtoLocations()
        {
            var ICTMaster = _context.Ictmaster.Include("AddedByUser").Include("ModifiedByUser").Include("IctlayoutPlanTypes").Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyID = s.CompanyId.Value,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId.Value,
                ParentICTID = s.ParentIctid,

                //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                VersionNo = s.VersionNo,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ICTMasterID).Where(t=>t.MasterType == 572 || t.MasterType == 573 || t.MasterType == 574 && t.StatusCodeID == 1).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            return ICTMaster;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get TransferSettings")]

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TransferSettingsModel> GetData(SearchModel searchModel)
        {
            var transferSettings = new TransferSettings();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).FirstOrDefault();
                        break;
                    case "Last":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).LastOrDefault();
                        break;
                    case "Next":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).LastOrDefault();
                        break;
                    case "Previous":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).FirstOrDefault();
                        break;
                    case "Last":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).LastOrDefault();
                        break;
                    case "Next":
                        transferSettings = _context.TransferSettings.OrderBy(o => o.TransferConfigId).FirstOrDefault(s => s.TransferConfigId > searchModel.Id);
                        break;
                    case "Previous":
                        transferSettings = _context.TransferSettings.OrderByDescending(o => o.TransferConfigId).FirstOrDefault(s => s.TransferConfigId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TransferSettingsModel>(transferSettings);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTransferSettings")]
        public TransferSettingsModel Post(TransferSettingsModel value)
        {
            var transferSettings = new TransferSettings();

            transferSettings = new TransferSettings
            {
                //TransferSettingsId = value.TransferSettingsID,
                FromLocationId = value.FromLocationId,
                ToLocationId = value.ToLocationId,
                IsTransfer = value.IsTransfer,
                IsReclass = value.IsReclass,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };



            _context.TransferSettings.Add(transferSettings);
            _context.SaveChanges();
            value.TransferConfigId = transferSettings.TransferConfigId;
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTransferSettings")]
        public TransferSettingsModel Put(TransferSettingsModel value)
        {

            var transferSettings = _context.TransferSettings.SingleOrDefault(p => p.TransferConfigId == value.TransferConfigId);
            if (transferSettings != null)
            {
                //TransferSettings.TransferSettingsId = value.TransferSettingsID;
                transferSettings.ModifiedByUserId = value.ModifiedByUserID;
                transferSettings.ModifiedDate = DateTime.Now;
                transferSettings.FromLocationId = value.FromLocationId;
                transferSettings.ToLocationId = value.ToLocationId;
                transferSettings.IsTransfer = value.IsTransfer;
                transferSettings.IsReclass = value.IsReclass;
                transferSettings.StatusCodeId = value.StatusCodeID.Value;

            }


            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
       
        [HttpDelete]
        [Route("DeleteTransferSettings")]
        public void DeleteLayout(int id)
        {
            var transferSettings = _context.TransferSettings.SingleOrDefault(p => p.TransferConfigId == id);
            if (transferSettings != null)
            {
                _context.TransferSettings.Remove(transferSettings);
                _context.SaveChanges();
            }
        }
    }
}