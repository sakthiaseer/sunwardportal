﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavItemPackingMethodController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavItemPackingMethodController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNavPackingMethodLines")]
        public List<NavPackingMethodModel> Get(int id)
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var navPackingMethods = _context.NavPackingMethod
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Where(s => s.ItemId == id)
                .AsNoTracking()
                .ToList();
            List<NavPackingMethodModel> navPackingMethodModels = new List<NavPackingMethodModel>();
            navPackingMethods.ForEach(s =>
            {
                NavPackingMethodModel navPackingMethodModel = new NavPackingMethodModel
                {
                    PackingMethodId = s.PackingMethodId,
                    ItemId = s.ItemId,
                    NoOfShipperCartorPerPallet = s.NoOfShipperCartorPerPallet,
                    NoOfUnitsPerShipperCarton = s.NoOfUnitsPerShipperCarton,
                    PalletSize = s.PalletSize,
                    PalletSizeName = applicationMasterDetails != null && applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PalletSize) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PalletSize).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                navPackingMethodModels.Add(navPackingMethodModel);
            });
            return navPackingMethodModels.OrderByDescending(a => a.PackingMethodId).ToList();
        }
        [HttpPost]
        [Route("InsertNavPackingMethodLine")]
        public NavPackingMethodModel Post(NavPackingMethodModel value)
        {
            var navPackingMethod = new NavPackingMethod
            {
                ItemId = value.ItemId,
                NoOfShipperCartorPerPallet = value.NoOfShipperCartorPerPallet,
                NoOfUnitsPerShipperCarton = value.NoOfUnitsPerShipperCarton,
                PalletSize = value.PalletSize,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.NavPackingMethod.Add(navPackingMethod);
            _context.SaveChanges();
            value.PackingMethodId = navPackingMethod.PackingMethodId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNavPackingMethodLine")]
        public NavPackingMethodModel Put(NavPackingMethodModel value)
        {
            var packingMethod = _context.NavPackingMethod.SingleOrDefault(p => p.PackingMethodId == value.PackingMethodId);
            packingMethod.ItemId = value.ItemId;
            packingMethod.NoOfShipperCartorPerPallet = value.NoOfShipperCartorPerPallet;
            packingMethod.NoOfUnitsPerShipperCarton = value.NoOfUnitsPerShipperCarton;
            packingMethod.PalletSize = value.PalletSize;
            packingMethod.StatusCodeId = value.StatusCodeID.Value;
            packingMethod.ModifiedByUserId = value.ModifiedByUserID;
            packingMethod.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteNavPackingMethod")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var navPackingMethod = _context.NavPackingMethod.Where(p => p.PackingMethodId == id).FirstOrDefault();
                if (navPackingMethod != null)
                {

                    _context.NavPackingMethod.Remove(navPackingMethod);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
