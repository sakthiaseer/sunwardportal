﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;

using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Services.Client;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Admin")]
    public class NAVFuncController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IHubContext<ChatHub> _hub;

        private readonly IBackgroundJobClient _backgroundJobClient;
        // public NAV.NAV Context { get; private set; }

        public NAVFuncController(CRT_TMSContext context, IMapper mapper,
            IConfiguration config, IHubContext<ChatHub> hub,
            IBackgroundJobClient backgroundJobClient)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hub = hub;

            _backgroundJobClient = backgroundJobClient;
            //Context = new NAV.NAV(new Uri($"{_config["NAV:OdataUrl"]}/Company('{_config["NAV:Company"]}')"))
            //{
            //    Credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"])
            //};
        }
        private int GetWeekNumberOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDay = (int)firstDayOfMonth.DayOfWeek;
            if (firstDay == 0)
            {
                firstDay = 7;
            }
            double d = (firstDay + date.Day - 1) / 7.0;
            return d > 5 ? (int)Math.Floor(d) : (int)Math.Ceiling(d);
        }
        [HttpPost]
        [Route("GetNAVStockBalance")]
        public async Task GetNAVStockBalance(StockBalanceSearch searchModel)
        {
            try
            {
                //_queue.QueueAsyncTask(() => UpdateSotckBalance(searchModel));
                _backgroundJobClient.Enqueue(() => UpdateSotckBalance(searchModel));
                _backgroundJobClient.Enqueue(() => UpdateKIVQty(searchModel));
                // _backgroundJobClient.Enqueue(() => GetSalesProcessQty(searchModel));
                //await UpdateSotckBalance(searchModel);
            }
            catch (Exception ex)
            {
                throw new AppException("Stock balance update failed with some unexpected errors!", ex);
            }
        }
        [HttpPost]
        [Route("GetPARReport")]
        public async Task GetPARReport(StockBalanceSearch searchModel)
        {
            try
            {
                await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Deleting existing record, PAR report");

                int pageSize = 500;
                int page = 0;
                while (true)
                {
                    var inpReports = _context.Navinpreport.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        _context.Navinpreport.RemoveRange(inpReports);
                        _context.SaveChanges();
                    }
                    if (inpReports.Count < 500)
                        break;
                    page++;
                }
                //_queue.QueueAsyncTask(() => UpdateSotckBalance(searchModel));
                _backgroundJobClient.Enqueue(() => GetINPDemand(searchModel));
                _backgroundJobClient.Enqueue(() => GetINPSupply(searchModel));
                //await UpdateSotckBalance(searchModel);
            }
            catch (Exception ex)
            {
                throw new AppException("PAR Report Sync failed with some unexpected errors!", ex);
            }
        }
        public async Task UpdateSotckBalance(StockBalanceSearch searchModel)
        {
            await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating stock balance, reterive company item card.");

            var company = await _context.Plant.FirstOrDefaultAsync(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company != null)
            {
                var Company = company.NavCompanyName;

                var context = new DataService(_config, Company);

                var post = new SWWebIntergartonPort.WebIntegration_PortClient();

                post.Endpoint.Address =
           new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/WebIntegration"),
           new DnsEndpointIdentity(""));

                post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                post.ClientCredentials.Windows.AllowedImpersonationLevel =
              System.Security.Principal.TokenImpersonationLevel.Impersonation;

                var inventoryItems = await _context.Navitems.Where(f => f.CompanyId == searchModel.CompanyId && f.No.StartsWith("FP-")).ToListAsync();
                //var firstDayCurrentMonth = new DateTime(searchModel.StkMonth.Year, searchModel.StkMonth.Month, 1);
                //var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);

                var month = searchModel.StkMonth.Month;// lastDayLastMonth.Month;
                var year = searchModel.StkMonth.Year;// lastDayLastMonth.Year;

                var weekofMonth = GetWeekNumberOfMonth(searchModel.StkMonth);
                int count = 0;
                int totalItems = inventoryItems.Count();
                inventoryItems.ForEach(f =>
                {
                    var stockdate = searchModel.StkMonth;

                    var itemCount = count + " of " + totalItems;
                    //notify client progress update
                    var itemName = string.Format("{0} {1}-{2}  {3}", itemCount, f.No, f.Description, "from NAV Stock balance");
                    _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", itemName);

                    var stockBalance = post.FnCalculateInventoryAsync(f.No, stockdate).Result.return_value;
                    var fmqty = post.FnCalculateFMInventoryAsync(f.No, stockdate).Result.return_value;
                    var reqty = post.FnCalculateRWInventoryAsync(f.No, stockdate).Result.return_value;
                    var wipqty = post.FnCalculateWIPInventoryAsync(f.No, stockdate).Result.return_value;
                    var notStartInvQty = post.FnCalcNotStartInventoryAsync(f.No, stockdate).Result.return_value;

                    _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating Dist. KIV Qty");
                    var kivQty = 0;

                    var stockNav = _context.NavitemStockBalance.FirstOrDefault(d => d.ItemId == f.ItemId && d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && d.StockBalWeek == weekofMonth);
                    if (stockNav == null)
                    {
                        f.NavitemStockBalance.Add(new NavitemStockBalance
                        {
                            ItemId = f.ItemId,
                            AddedByUserId = 1,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            Quantity = stockBalance,
                            GlobalQty = fmqty,
                            ReworkQty = reqty,
                            Wipqty = wipqty,
                            Kivqty = kivQty,
                            RejectQuantity = 0,
                            StockBalWeek = weekofMonth,
                            StockBalMonth = stockdate,
                            Supply1ProcessQty = 0,
                            SupplyWipqty = 0,
                            NotStartInvQty= notStartInvQty,
                        });
                    }
                    else
                    {
                        stockNav.Quantity = stockBalance;
                        stockNav.StockBalWeek = weekofMonth;
                        stockNav.Wipqty = wipqty;
                        stockNav.ReworkQty = reqty;
                        stockNav.GlobalQty = fmqty;
                        stockNav.Kivqty = kivQty;
                        stockNav.NotStartInvQty = notStartInvQty;
                    }
                    _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Update stock balance into portal database");

                    _context.SaveChanges();
                    count++;
                });
                await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Completed");
            }
        }

        [HttpPost]
        [Route("UpdateKIVQty")]
        public async Task UpdateKIVQty(StockBalanceSearch searchModel)
        {
            try
            {
                await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating stock balance, reterive company item card.");

                var company = await _context.Plant.FirstOrDefaultAsync(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
                if (company != null)
                {
                    var Company = company.NavCompanyName;

                    await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Getting KIV Qty by item from NAV");
                    var custMasts = _context.Navcustomer.Where(f => f.CompanyId == searchModel.CompanyId).ToList();
                    var inventoryItems = _context.Navitems.Where(f => f.CompanyId == searchModel.CompanyId && f.No.StartsWith("FP-")).ToList();
                    inventoryItems.ForEach(f =>
                    {
                        var context = new DataService(_config, Company);
                        var nquery = context.Context.SalesOrderLineKIV.Where(cs => cs.No == f.No);
                        DataServiceQuery<SalesOrderLineKIV> query = (DataServiceQuery<SalesOrderLineKIV>)nquery;
                        TaskFactory<IEnumerable<SalesOrderLineKIV>> taskFactory = new TaskFactory<IEnumerable<SalesOrderLineKIV>>();
                        IEnumerable<SalesOrderLineKIV> salesResults = taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar)).GetAwaiter().GetResult();
                        var salesList = salesResults.ToList();

                        var month = searchModel.StkMonth.Month;// lastDayLastMonth.Month;
                        var year = searchModel.StkMonth.Year;// lastDayLastMonth.Year;
                        var stockdate = searchModel.StkMonth;
                        var weekofMonth = GetWeekNumberOfMonth(searchModel.StkMonth);


                        _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating KIV Qty by item & Customer");
                        salesList.ForEach(c =>
                        {
                            var itemMas = f;
                            var custMas = custMasts.FirstOrDefault(i => i.Code == c.Sell_to_Customer_No);
                            var stockNav = _context.DistStockBalanceKiv.FirstOrDefault(d => d.ItemNo == f.No && d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && d.StockBalWeek == weekofMonth && d.CustomerId == custMas.CustomerId);
                            if (stockNav == null)
                            {
                                var distKIV = new DistStockBalanceKiv
                                {
                                    ItemId = itemMas?.ItemId,
                                    ItemNo = c.No,
                                    CompanyId = searchModel.CompanyId,
                                    CustomerId = custMas?.CustomerId,
                                    Quantity = c.Outstanding_Quantity,
                                    CustomerNo = c.Sell_to_Customer_Name,
                                    StockBalWeek = weekofMonth,
                                    StockBalMonth = stockdate,
                                };
                                _context.DistStockBalanceKiv.Add(distKIV);
                            }
                            else
                            {
                                stockNav.Quantity += c.Outstanding_Quantity;
                            }
                            _context.SaveChanges();

                        });
                    });
                    await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Update KIV Qty by customer to portal");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [Route("GetINPDemand")]
        public async Task<List<INPModel>> GetINPDemand(StockBalanceSearch searchModel)
        {
            var iNPItems = new List<INPModel>();
            try
            {
                var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
                if (company != null)
                {
                    var context = new DataService(_config, company.NavCompanyName);

                    int pageSize = 1000;
                    int page = 0;
                    while (true)
                    {
                        //sales line  || f.Document_Type == "Blanket Order")
                        var nquery = context.Context.OutstandingSalesOrderLine.Where(f => f.Outstanding_Quantity > 0 && f.Document_Type == "Order").Skip(page * pageSize).Take(pageSize);
                        DataServiceQuery<OutstandingSalesOrderLine> query = (DataServiceQuery<OutstandingSalesOrderLine>)nquery;
                        TaskFactory<IEnumerable<OutstandingSalesOrderLine>> taskFactory = new TaskFactory<IEnumerable<OutstandingSalesOrderLine>>();
                        IEnumerable<OutstandingSalesOrderLine> salesResults = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                        var salesList = salesResults.ToList();
                        salesList.ToList().ForEach(s =>
                        {
                            var inpReport = new Navinpreport
                            {
                                ItemNo = s.No,
                                Description = s.Description,
                                DocumentNo = s.Document_No,
                                Quantity = s.Outstanding_Quantity,
                                QuantityBase = s.Outstanding_Quantity,
                                Soqty = s.Outstanding_Quantity,
                                FilterType = "Demand",
                                TableName = "Sales Order",
                                ReservedQty = 0,
                                ShipmentDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                ShipingType = s.Document_Type,
                                //LocationCode = s.lo,
                                AvailableQty = s.Outstanding_Quantity,
                                Balance = 0,
                                BatchNo = null,
                                CompletionDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                ReplanRefNo = null,
                                Company = company.NavCompanyName,
                                Demand = s.Outstanding_Quantity,
                                ExpiryDate = null,
                                ExpReciptDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                ItemCategory = null,
                                LotNo = null,
                                MethodCode = null,
                                OrderDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                OutstandingQty = null,
                                Pobalance = s.Outstanding_Quantity,
                                Pono = null,
                                ProdOrder = null,
                                QcrefNo = null,
                                Qcstatus = null,
                                Rpobalance = null,
                                Rpono = null,
                                SaftyLeadTime = null,
                                SaftyStock = null,
                                SourceName = null,
                                Supply = null,
                                Uom = s.Unit_of_Measure_Code,
                                CustomerName = s.Sell_to_Customer_No,
                                CompanyId = company.PlantId,

                            };

                            _context.Navinpreport.Add(inpReport);
                        });
                        _context.SaveChanges();

                        if (salesList.Count < 1000)
                            break;
                        page++;
                    }

                    page = 0;
                    while (true)
                    {
                        // blanket order 
                        var nblaquery = context.Context.OutstandingSalesOrderLine.Where(f => f.Document_Type == "Blanket Order").Skip(page * pageSize).Take(pageSize);
                        DataServiceQuery<OutstandingSalesOrderLine> qblauery = (DataServiceQuery<OutstandingSalesOrderLine>)nblaquery;
                        TaskFactory<IEnumerable<OutstandingSalesOrderLine>> blataskFactory = new TaskFactory<IEnumerable<OutstandingSalesOrderLine>>();
                        IEnumerable<OutstandingSalesOrderLine> blasalesResults = await blataskFactory.FromAsync(qblauery.BeginExecute(null, null), iar => qblauery.EndExecute(iar));

                        var balmketSalesList = blasalesResults.ToList();

                        balmketSalesList.ForEach(s =>
                        {
                            var inpReport = new Navinpreport
                            {
                                ItemNo = s.No,
                                Description = s.Description,
                                DocumentNo = s.Document_No,
                                Quantity = s.Outstanding_Quantity,
                                QuantityBase = s.Outstanding_Quantity,
                                Soqty = s.Outstanding_Quantity,
                                FilterType = "Demand",
                                TableName = "Blanket Order",
                                ReservedQty = 0,
                                ShipmentDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                ShipingType = s.Document_Type,
                                //LocationCode = s.Location_Code,
                                AvailableQty = s.Outstanding_Quantity,
                                Balance = 0,
                                BatchNo = null,
                                CompletionDate = null,
                                ReplanRefNo = null,
                                Company = company.NavCompanyName,
                                Demand = s.Outstanding_Quantity,
                                ExpiryDate = null,
                                ExpReciptDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                                ItemCategory = null,
                                LotNo = null,
                                MethodCode = null,
                                OrderDate = null,
                                OutstandingQty = null,
                                Pobalance = null,
                                Pono = null,
                                ProdOrder = null,
                                QcrefNo = null,
                                Qcstatus = null,
                                Rpobalance = null,
                                Rpono = null,
                                SaftyLeadTime = null,
                                SaftyStock = null,
                                SourceName = null,
                                Supply = null,
                                Uom = s.Unit_of_Measure_Code,
                                CustomerName = s.Sell_to_Customer_No,
                                CompanyId = company.PlantId,
                            };

                            _context.Navinpreport.Add(inpReport);
                        });
                        _context.SaveChanges();

                        if (balmketSalesList.Count < 1000)
                            break;
                        page++;
                    }

                    page = 0;
                    while (true)
                    {
                        //ProdOrderComp line 
                        var pcquery = context.Context.ProdOrderCompLineList.Where(c => c.Status == "Planned" || c.Status == "Released").Skip(page * pageSize).Take(pageSize);
                        DataServiceQuery<ProdOrderCompLineList> prodCoquery = (DataServiceQuery<ProdOrderCompLineList>)pcquery;
                        TaskFactory<IEnumerable<ProdOrderCompLineList>> prodcotaskFactory = new TaskFactory<IEnumerable<ProdOrderCompLineList>>();
                        IEnumerable<ProdOrderCompLineList> prodCOresult = await prodcotaskFactory.FromAsync(prodCoquery.BeginExecute(null, null), iar => prodCoquery.EndExecute(iar));
                        var prodComList = prodCOresult.ToList();

                        prodComList.ForEach(s =>
                        {
                            var inpReport = new Navinpreport
                            {
                                ItemNo = s.Item_No,
                                Description = s.Description,
                                DocumentNo = null,
                                Quantity = s.Quantity,
                                QuantityBase = s.Quantity_Base,
                                ProdCompQty = s.Quantity,
                                FilterType = "Demand",
                                TableName = "Prod. Order Component",
                                ReservedQty = null,
                                ShipmentDate = null,
                                ShipingType = null,
                                LocationCode = s.Location_Code,
                                AvailableQty = s.Quantity_Base,
                                Balance = 0,
                                BatchNo = null,
                                CompletionDate = s.Due_Date == DateTime.MinValue ? null : s.Due_Date,
                                ReplanRefNo = null,
                                Company = company.NavCompanyName,
                                Demand = s.Quantity,
                                ExpiryDate = null,
                                ExpReciptDate = s.Due_Date == DateTime.MinValue ? null : s.Due_Date,
                                ItemCategory = null,
                                LotNo = null,
                                MethodCode = null,
                                OrderDate = null,
                                OutstandingQty = null,
                                Pobalance = null,
                                Pono = null,
                                ProdOrder = s.Prod_Order_No,
                                QcrefNo = null,
                                Qcstatus = null,
                                Rpobalance = null,
                                Rpono = s.Prod_Order_No,
                                SaftyLeadTime = null,
                                SaftyStock = null,
                                SourceName = null,
                                Supply = null,
                                Uom = s.Unit_of_Measure_Code,
                                //CustomerName = s.,
                                CompanyId = company.PlantId,
                            };
                            _context.Navinpreport.Add(inpReport);
                        });
                        _context.SaveChanges();

                        if (prodComList.Count < 1000)
                            break;
                        page++;
                    }

                }
                return iNPItems;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        [Route("GetINPSupply")]
        public async Task<List<INPModel>> GetINPSupply(StockBalanceSearch searchModel)
        {
            var iNPItems = new List<INPModel>();
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company != null)
            {
                var context = new DataService(_config, company.NavCompanyName);

                int pageSize = 1000;
                int page = 0;
                while (true)
                {
                    //sales return 
                    var salesquery = context.Context.OutstandingSalesOrderLine.Where(f => f.Document_Type == "Return Order").Skip(page * pageSize).Take(pageSize);
                    DataServiceQuery<OutstandingSalesOrderLine> salequery = (DataServiceQuery<OutstandingSalesOrderLine>)salesquery;
                    TaskFactory<IEnumerable<OutstandingSalesOrderLine>> saleFactory = new TaskFactory<IEnumerable<OutstandingSalesOrderLine>>();
                    IEnumerable<OutstandingSalesOrderLine> salesResults = await saleFactory.FromAsync(salequery.BeginExecute(null, null), iar => salequery.EndExecute(iar));
                    var returnOrder = salesResults.ToList();
                    returnOrder.ForEach(s =>
                    {
                        var inpReport = new Navinpreport
                        {
                            ItemNo = s.No,
                            Description = s.Description,
                            DocumentNo = s.Document_No,
                            Quantity = s.Outstanding_Quantity,
                            QuantityBase = s.Outstanding_Quantity,
                            Soqty = s.Outstanding_Quantity,
                            FilterType = "Supply",
                            TableName = "Return Order",
                            ReservedQty = 0,
                            ShipmentDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                            ShipingType = s.Document_Type,
                            //LocationCode = s.Location_Code,
                            AvailableQty = s.Outstanding_Quantity,
                            Balance = 0,
                            BatchNo = null,
                            CompletionDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                            ReplanRefNo = null,
                            Company = company.NavCompanyName,
                            Demand = null,
                            ExpiryDate = null,
                            ExpReciptDate = s.Shipment_Date == DateTime.MinValue ? null : s.Shipment_Date,
                            ItemCategory = null,
                            LotNo = null,
                            MethodCode = null,
                            OrderDate = null,
                            OutstandingQty = s.Outstanding_Quantity,
                            Pobalance = s.Outstanding_Quantity,
                            Pono = s.Document_No,
                            ProdOrder = null,
                            QcrefNo = null,
                            Qcstatus = null,
                            Rpobalance = null,
                            Rpono = null,
                            SaftyLeadTime = null,
                            SaftyStock = null,
                            SourceName = null,
                            Supply = s.Outstanding_Quantity,
                            Uom = s.Unit_of_Measure_Code,
                            CustomerName = s.Sell_to_Customer_No,
                            CompanyId = company.PlantId,
                        };

                        _context.Navinpreport.Add(inpReport);
                    });
                    _context.SaveChanges();
                    if (returnOrder.Count < 1000)
                        break;
                    page++;
                }

                page = 0;
                while (true)
                {
                    //Purchase line 
                    var nquery = context.Context.PurchaseLines.Where(w => w.Document_Type == "Order" && w.Type == "Item" && w.Closed == false).Skip(page * pageSize).Take(pageSize);
                    DataServiceQuery<PurchaseLines> query = (DataServiceQuery<PurchaseLines>)nquery;
                    TaskFactory<IEnumerable<PurchaseLines>> taskFactory = new TaskFactory<IEnumerable<PurchaseLines>>();
                    IEnumerable<PurchaseLines> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                    var purList = result.ToList();
                    purList.ForEach(s =>
                    {
                        var inpReport = new Navinpreport
                        {
                            ItemNo = s.No,
                            Description = s.Description,
                            DocumentNo = s.Document_No,
                            Quantity = s.Quantity,
                            QuantityBase = s.Quantity_Base,
                            Poqty = s.Quantity,
                            FilterType = "Supply",
                            TableName = "Purchase Order",
                            ReservedQty = null,
                            ShipmentDate = s.Expected_Receipt_Date == DateTime.MinValue ? null : s.Expected_Receipt_Date,
                            ShipingType = null,
                            LocationCode = s.Location_Code,
                            AvailableQty = s.Quantity_Base,
                            Balance = 0,
                            BatchNo = null,
                            CompletionDate = s.Expected_Receipt_Date == DateTime.MinValue ? null : s.Expected_Receipt_Date,
                            ReplanRefNo = null,
                            Company = company.NavCompanyName,
                            Demand = null,
                            ExpiryDate = null,
                            ExpReciptDate = s.Expected_Receipt_Date == DateTime.MinValue ? null : s.Expected_Receipt_Date,
                            ItemCategory = null,
                            LotNo = null,
                            MethodCode = null,
                            OrderDate = null,
                            OutstandingQty = s.Outstanding_Quantity,
                            Pobalance = s.Quantity,
                            Pono = s.No,
                            ProdOrder = s.Document_No,
                            QcrefNo = null,
                            Qcstatus = null,
                            Rpobalance = null,
                            Rpono = null,
                            SaftyLeadTime = null,
                            SaftyStock = null,
                            SourceName = null,
                            Supply = s.Quantity,
                            Uom = s.Unit_of_Measure_Code,
                            //CustomerName = s.cus,
                            CompanyId = company.PlantId,
                        };
                        _context.Navinpreport.Add(inpReport);
                    });
                    _context.SaveChanges();
                    if (purList.Count < 1000)
                        break;
                    page++;
                }

                page = 0;
                while (true)
                {
                    //ProdOrderLineList line 
                    var pcquery = context.Context.ProdOrderLineList.Where(f => f.Status == "Released").Skip(page * pageSize).Take(pageSize);
                    DataServiceQuery<ProdOrderLineList> prodCoquery = (DataServiceQuery<ProdOrderLineList>)pcquery;
                    TaskFactory<IEnumerable<ProdOrderLineList>> prodcotaskFactory = new TaskFactory<IEnumerable<ProdOrderLineList>>();
                    IEnumerable<ProdOrderLineList> prodCOresult = await prodcotaskFactory.FromAsync(prodCoquery.BeginExecute(null, null), iar => prodCoquery.EndExecute(iar));
                    var prodList = prodCOresult.ToList();
                    prodList.ForEach(s =>
                    {
                        var inpReport = new Navinpreport
                        {
                            ItemNo = s.Item_No,
                            Description = s.Description,
                            DocumentNo = s.Prod_Order_No,
                            Quantity = s.Quantity,
                            QuantityBase = s.Quantity_Base,
                            ProdQty = s.Quantity,
                            FilterType = "Supply",
                            TableName = "Production Order",
                            ReservedQty = null,
                            ShipmentDate = s.Ending_Date == DateTime.MinValue ? null : s.Ending_Date,
                            ShipingType = null,
                            LocationCode = s.Location_Code,
                            AvailableQty = s.Quantity_Base,
                            Balance = 0,
                            BatchNo = null,
                            CompletionDate = s.Completion_Date == DateTime.MinValue ? null : s.Completion_Date,
                            ReplanRefNo = s.Prod_Order_No,
                            Company = company.NavCompanyName,
                            Demand = null,
                            ExpiryDate = null,
                            ExpReciptDate = s.ETD == DateTime.MinValue ? null : s.ETD,
                            ItemCategory = null,
                            LotNo = null,
                            MethodCode = null,
                            OrderDate = null,
                            OutstandingQty = null,
                            Pobalance = null,
                            Pono = null,
                            ProdOrder = null,
                            QcrefNo = null,
                            Qcstatus = null,
                            Rpobalance = s.Remaining_Qty_Base,
                            Rpono = s.Prod_Order_No,
                            SaftyLeadTime = null,
                            SaftyStock = null,
                            SourceName = s.Description,
                            Supply = s.Quantity,
                            Uom = null,
                            //CustomerName = s.cus,
                            CompanyId = company.PlantId,
                        };
                        _context.Navinpreport.Add(inpReport);
                    });
                    _context.SaveChanges();
                    if (prodList.Count < 1000)
                        break;
                    page++;
                }
            }
            return iNPItems;

        }

        [HttpPost]
        [Route("GetUOMByItemNo")]

        public async Task<ApplicationMasterDetailModel> GetUOMByItemNo(SearchModel searchModel)
        {
            var detailModel = new ApplicationMasterDetailModel();
            if (!String.IsNullOrEmpty(searchModel.CompanyName))
            {
                var context = new DataService(_config, searchModel.CompanyName);
                int pageSize = 1000;
                int page = 0;
                var processItem = searchModel.SearchString.Split('~');
                var orderNo = processItem[0];
                var lineNo = Convert.ToInt32(processItem[1]);

                var pcquery = context.Context.ProdOrderLineList.Where(f => f.Prod_Order_No == orderNo && f.Line_No == lineNo).Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<ProdOrderLineList> prodCoquery = (DataServiceQuery<ProdOrderLineList>)pcquery;
                TaskFactory<IEnumerable<ProdOrderLineList>> prodcotaskFactory = new TaskFactory<IEnumerable<ProdOrderLineList>>();
                IEnumerable<ProdOrderLineList> prodCOresult = await prodcotaskFactory.FromAsync(prodCoquery.BeginExecute(null, null), iar => prodCoquery.EndExecute(iar));
                var prodList = prodCOresult.ToList();
                var itemNo = prodList.FirstOrDefault().Item_No;

                var nquery = context.Context.ItemList.Where(f => f.No == itemNo).Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<ItemList> query = (DataServiceQuery<ItemList>)nquery;

                TaskFactory<IEnumerable<ItemList>> taskFactory = new TaskFactory<IEnumerable<ItemList>>();
                IEnumerable<ItemList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemResult = result.FirstOrDefault();

                //if (itemResult != null)
                {
                    detailModel = new ApplicationMasterDetailModel
                    {
                        Value = itemResult.Base_Unit_of_Measure,
                        Description = itemResult.Base_Unit_of_Measure
                    };

                }
            }


            return detailModel;
        }

        [HttpPost]
        [Route("GetBMRLines")]
        public async Task<List<BmrmovementLineModel>> GetBMRLines(SearchModel searchModel)
        {
            var bmrmovementLineModels = new List<BmrmovementLineModel>();
            if (!String.IsNullOrEmpty(searchModel.CompanyName))
            {
                var context = new DataService(_config, searchModel.CompanyName);

                int pageSize = 1000;
                int page = 0;
                {
                    //ProdOrderLineList line 
                    var pcquery = context.Context.ProdOrderLineList.Where(f => f.Prod_Order_No == searchModel.SearchString).Skip(page * pageSize).Take(pageSize);
                    DataServiceQuery<ProdOrderLineList> prodCoquery = (DataServiceQuery<ProdOrderLineList>)pcquery;
                    TaskFactory<IEnumerable<ProdOrderLineList>> prodcotaskFactory = new TaskFactory<IEnumerable<ProdOrderLineList>>();
                    IEnumerable<ProdOrderLineList> prodCOresult = await prodcotaskFactory.FromAsync(prodCoquery.BeginExecute(null, null), iar => prodCoquery.EndExecute(iar));
                    var prodList = prodCOresult.ToList();

                    prodList.ForEach(p =>
                    {
                        BmrmovementLineModel bmrmovementLineModel = new BmrmovementLineModel
                        {
                            Bmrname = p.Item_No,
                            BatchNo = p.Batch_No,
                            ItemNo = p.Item_No,
                            Description1 = p.Description,
                            Description2 = p.Description_2,
                            Qty = p.Quantity,
                            LocationCode = p.Location_Code,
                            ProductionOrderNo = p.Prod_Order_No,
                            LineNo = p.Line_No,
                        };
                        bmrmovementLineModels.Add(bmrmovementLineModel);

                    });

                }
            }
            return bmrmovementLineModels;
        }

        [HttpGet]
        [Route("GetINPReport")]
        public async Task<List<INPModel>> GetINPReport(string company)
        {
            var iNPItems = new List<INPModel>();
            var intercompanyIds = new List<long?> { 1, 2 };
            intercompanyIds.ForEach(companyId =>
            {
                var inpReportList = _context.Navinpreport
                    .Select(s => new
                    {
                        s.FilterType,
                        s.ItemNo,
                        s.Description,
                        s.Quantity,
                        s.CompletionDate,
                        s.Uom,
                        s.ShipmentDate,
                        s.ItemCategory,
                        s.QuantityBase,
                        s.CustomerId,
                        s.CompanyId,
                        s.DocumentNo
                    }).Where(c => c.CompanyId == companyId).AsNoTracking()
                    .ToList();
                var inpReport = inpReportList.GroupBy(g => new { g.ItemNo, g.FilterType }).ToList();

                var demandList = inpReport.Where(e => e.Key.FilterType == "Demand").ToList();
                var supplyList = inpReport.Where(e => e.Key.FilterType == "Supply").ToList();

                supplyList.ToList().ForEach(k =>
                {
                    if (!string.IsNullOrEmpty(k.Key.ItemNo))
                    {
                        var qty = k.Sum(s => s.Quantity);
                        var qtybase = k.Sum(s => s.QuantityBase);
                        var report = new INPModel
                        {
                            ItemNo = k.First().ItemNo,
                            Description = k.First().Description,
                            CompletionDate = k.First().CompletionDate,
                            ShipmentDate = k.First().ShipmentDate,
                            Uom = k.First().Uom,
                            ItemCategory = k.First().ItemCategory,
                            QuantityRequired = qty,
                            FilterType = k.First().FilterType,
                            Demand = 0,
                            Supply = qty,
                            QuantityBase = qtybase,
                            CompanyId = k.First().CompanyId,
                            CustomerId = k.First().CustomerId,
                            DocumentNo = k.First().DocumentNo
                            //ItemId = k.First().ItemNo,
                        };
                        iNPItems.Add(report);
                    }
                });
                var inventoryList = _context.Navitems.AsNoTracking().ToList();
                demandList.ForEach(k =>
                {
                    if (!string.IsNullOrEmpty(k.Key.ItemNo))
                    {
                        var qty = k.Sum(s => s.Quantity);
                        var qtybase = k.Sum(s => s.QuantityBase);
                        var item = iNPItems.FirstOrDefault(s => s.FilterType == "Supply" && s.ItemNo == k.Key.ItemNo);
                        var inventory = inventoryList.FirstOrDefault(ni => ni.No == k.Key.ItemNo && ni.CompanyId == companyId);
                        if (item != null)
                        {
                            item.AvailableQty = inventory?.Inventory;
                            item.SaftyStock = 0;
                            item.Demand = qty;
                            item.Quantity = item.Supply - qty;
                            item.QuantityRequired = item.Supply - qty;
                            item.ItemCategory = inventory?.ItemCategoryCode;
                            item.Uom = inventory?.BaseUnitofMeasure;
                            item.QuantityBase = qtybase;
                            item.CustomerId = inventory?.CompanyId;
                            item.ItemId = inventory?.ItemId;
                            item.CompanyId = companyId;
                        }
                        else
                        {
                            item = new INPModel
                            {
                                ItemNo = k.First().ItemNo,
                                Description = k.First().Description,
                                CompletionDate = k.First().CompletionDate,
                                ShipmentDate = k.First().ShipmentDate,
                                Uom = inventory?.BaseUnitofMeasure,
                                ItemCategory = inventory?.ItemCategoryCode,
                                QuantityRequired = qty,
                                FilterType = k.First().FilterType,
                                Demand = qty,
                                Supply = 0,
                                Quantity = qty,
                                AvailableQty = inventory?.Inventory,
                                SaftyStock = 0,
                                QuantityBase = qtybase,
                                CompanyId = k.First().CompanyId,
                                CustomerId = k.First().CustomerId,
                                DocumentNo = k.First().DocumentNo,
                                ItemId = inventory?.ItemId,
                            };
                            iNPItems.Add(item);
                        }

                    }
                });

                iNPItems.ForEach(r =>
                {
                    var reports = new NavinpitemReport
                    {
                        Company = company,
                        CompletionDate = r.CompletionDate,
                        DemandQuantity = r.Demand,
                        Description = r.Description,
                        ItemNo = r.ItemNo,
                        Quantity = r.Quantity,
                        QuantityBase = r.QuantityBase,
                        Uom = r.Uom,
                        ItemCategory = r.ItemCategory,
                        ShipmentDate = r.ShipmentDate,
                        SupplyQuantity = r.Supply,
                        Inventory = r.AvailableQty,
                        SaftyStock = r.SaftyStock,
                        CustomerId = r.CustomerId,
                        CompanyId = r.CompanyId,
                        ItemId = r.ItemId,
                        DocumentNo = r.DocumentNo
                    };
                    _context.NavinpitemReport.Add(reports);
                });
                _context.SaveChanges();
            });
            return iNPItems;
        }


        [HttpPost]
        [Route("GetOutstandingSalesOrderLine")]
        public async Task<List<OutstandingSalesOrderLine>> GetOutstandingSalesOrderLine(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);


            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.OutstandingSalesOrderLine.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<OutstandingSalesOrderLine> query = (DataServiceQuery<OutstandingSalesOrderLine>)nquery;

                TaskFactory<IEnumerable<OutstandingSalesOrderLine>> taskFactory = new TaskFactory<IEnumerable<OutstandingSalesOrderLine>>();
                IEnumerable<OutstandingSalesOrderLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();

                itemList.ForEach(f =>
                {
                    var prodOrder = new NavSalesOrderLine
                    {
                        DocumentNo = f.Document_No,
                        DocumentType = f.Document_Type,
                        Description = f.Description,
                        Description1 = f.Description_2,
                        ItemNo = f.No,
                        OrderLineNo = f.Line_No,
                        LastSyncDate = DateTime.Now,
                        LastSyncUserId = searchModel.UserId,
                        UnitofMeasureCode = f.Unit_of_Measure_Code,
                        OutstandingQuantity = f.Outstanding_Quantity,
                        PromisedDeliveryDate = f.Promised_Delivery_Date == DateTime.MinValue ? null : f.Promised_Delivery_Date,
                        SelltoCustomerNo = f.Sell_to_Customer_No,
                        ShipmentDate = f.Shipment_Date == DateTime.MinValue ? null : f.Shipment_Date,
                    };
                    _context.NavSalesOrderLine.Add(prodOrder);
                });
                _context.SaveChanges();

                if (itemList.Count < 1000)
                    break;
                page++;
            }
            return new List<OutstandingSalesOrderLine>();
        }
        [HttpPost]
        [Route("GetOutstandingProdOrderLine")]
        public async Task<List<OutstandingProdOrderLine>> GetOutstandingProdOrderLine(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);


            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.OutstandingProdOrderLine.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<OutstandingProdOrderLine> query = (DataServiceQuery<OutstandingProdOrderLine>)nquery;

                TaskFactory<IEnumerable<OutstandingProdOrderLine>> taskFactory = new TaskFactory<IEnumerable<OutstandingProdOrderLine>>();
                IEnumerable<OutstandingProdOrderLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();

                itemList.ForEach(f =>
                {
                    var prodOrder = new NavprodOrderLine
                    {
                        CompletionDate = f.Completion_Date == DateTime.MinValue ? null : f.Completion_Date,
                        Description = f.Description,
                        Description1 = f.Description_2,
                        ItemNo = f.Item_No,
                        OrderLineNo = f.Line_No,
                        LastSyncDate = DateTime.Now,
                        LastSyncUserId = searchModel.UserId,
                        ProdOrderNo = f.Prod_Order_No,
                        RemainingQuantity = f.Remaining_Quantity,
                        Status = f.Status,
                        UnitofMeasureCode = f.Unit_of_Measure_Code,
                    };
                    _context.NavprodOrderLine.Add(prodOrder);
                });
                _context.SaveChanges();

                if (itemList.Count < 1000)
                    break;
                page++;
            }

            return new List<OutstandingProdOrderLine>();
        }

        [HttpPost]
        [Route("GetGroupPlanning")]
        public async Task<List<APP.DataAccess.Models.GroupPlanning>> GetGroupPlanning(StockBalanceSearch searchModel)
        {

            await GetSalesOrderLineKIV(searchModel);
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));

            if (company == null)
                return null;
            var companyName = company.NavCompanyName;
            var context = new DataService(_config, companyName);

            await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM GroupPlanning WHERE CompanyId = {company.PlantId}");

            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var groupPlanning = new List<DataAccess.Models.GroupPlanning>();
                var nquery = context.Context.GroupPlanning.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.GroupPlanning> query = (DataServiceQuery<NAV.GroupPlanning>)nquery;

                TaskFactory<IEnumerable<NAV.GroupPlanning>> taskFactory = new TaskFactory<IEnumerable<NAV.GroupPlanning>>();
                IEnumerable<NAV.GroupPlanning> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();

                itemList.ForEach(f =>
                {
                    var groupPlan = new APP.DataAccess.Models.GroupPlanning
                    {
                        BatchSize = f.Batch_Size,
                        CompanyId = company.PlantId,
                        ItemDescription = f.Description,
                        ItemDescription1 = f.Description_2,
                        ItemNo = f.Item_No,
                        NoOfTicket = f.No_of_Ticket,
                        //OrderCreated = f.Order_Created, 

                        //Arun need to check
                        //ProductDescription = f.Product_Description,
                        ProductGroupCode = f.Product_Group_Code,
                        Quantity = f.Quantity,
                        //Arun need to check
                        //RecipeNo = f.Recipe_No,
                        StartDate = f.Start_Date == DateTime.MinValue ? DateTime.Now : f.Start_Date,
                        Uom = f.Unit_Of_Measure_Code
                    };
                    groupPlanning.Add(groupPlan);
                    //_context.GroupPlanning.Add(groupPlan);
                });
                //_context.SaveChanges();             

                //_context.BulkInsert(groupPlanning);

                SqlBulkUpload objBulkInsert = new SqlBulkUpload(_context, _config);
                await objBulkInsert.BulkInsertAsync(groupPlanning, "GroupPlanning");


                if (itemList.Count < 1000)
                    break;
                page++;
            }

            return new List<APP.DataAccess.Models.GroupPlanning>();
        }


        [HttpPost]
        [Route("GetItemBatch")]
        public async Task<List<ItemBatchInfoModel>> GetItemBatch(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));

            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);

            var NavItems = _context.Navitems.Where(f => f.CompanyId == company.PlantId).AsNoTracking().ToList();

            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.ItemBatchInfo.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.ItemBatchInfo> query = (DataServiceQuery<NAV.ItemBatchInfo>)nquery;

                TaskFactory<IEnumerable<NAV.ItemBatchInfo>> taskFactory = new TaskFactory<IEnumerable<NAV.ItemBatchInfo>>();
                IEnumerable<NAV.ItemBatchInfo> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();
                var batchItems = _context.ItemBatchInfo.AsNoTracking().ToList();
                var itemLocation = _context.Navlocation.AsNoTracking().ToList();
                var newLocations = new List<Navlocation>();
                var newbatchItems = new List<DataAccess.Models.ItemBatchInfo>();
                var updatebatchItems = new List<DataAccess.Models.ItemBatchInfo>();
                itemList.ForEach(f =>
                {
                    var itemNav = NavItems.FirstOrDefault(i => i.No == f.Item_No);

                    if (itemNav != null)
                    {
                        var itemId = itemNav.ItemId;

                        if (itemId > 0)
                        {
                            if (!itemLocation.Any(a => a.Code == f.Location_Code && a.ItemId == itemId))
                            {
                                var navLocation = new Navlocation
                                {
                                    AddedByUserId = searchModel.UserId,
                                    PlantId = searchModel.CompanyId,
                                    Code = f.Location_Code,
                                    Name = f.Location_Code,
                                    ItemId = itemId,
                                };
                                //_context.Navlocation.Add(navLocation);
                                itemLocation.Add(navLocation);
                                newLocations.Add(navLocation);
                            }
                            //_context.BulkMerge(itemLocation, option => option.ColumnPrimaryKeyExpression = c => new { c.ItemId, c.Code });



                            if (!batchItems.Any(a => a.ItemId == itemId && a.BatchNo == f.Batch_No && a.LocationCode == f.Location_Code && a.LotNo == f.Lot_No))
                            {
                                var groupPlan = new APP.DataAccess.Models.ItemBatchInfo
                                {
                                    //CompanyId = company.PlantId,
                                    CompanyId = 1,
                                    ExpiryDate = f.Expiration_Date == DateTime.MinValue ? null : f.Expiration_Date,
                                    ManufacturingDate = f.Manufacturing_Date == DateTime.MinValue ? null : f.Manufacturing_Date,
                                    NavQuantity = f.Remaining_Quantity,
                                    BatchNo = f.Batch_No,
                                    ItemId = itemId,
                                    AddedByUserId = searchModel.UserId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = 1,
                                    BalanceQuantity = 0,
                                    IssueQuantity = 0,
                                    QuantityOnHand = 0,
                                    LocationCode = f.Location_Code,
                                    LotNo = f.Lot_No,
                                };
                                batchItems.Add(groupPlan);
                                newbatchItems.Add(groupPlan);
                                //_context.ItemBatchInfo.Add(groupPlan);
                                //_context.SaveChanges();
                            }
                            else
                            {
                                var batch = _context.ItemBatchInfo.FirstOrDefault(u => u.ItemId == itemId && u.BatchNo == f.Batch_No && u.LotNo == f.Lot_No && u.LocationCode == f.Location_Code);
                                if (batch != null)
                                {
                                    batch.LocationCode = f.Location_Code;
                                    batch.NavQuantity += f.Remaining_Quantity;
                                    batch.LotNo = f.Lot_No;
                                    batch.ExpiryDate = f.Expiration_Date == DateTime.MinValue ? null : f.Expiration_Date;
                                    batch.ManufacturingDate = f.Manufacturing_Date == DateTime.MinValue ? null : f.Manufacturing_Date;
                                    updatebatchItems.Add(batch);
                                }
                                else
                                {
                                    var newupdate = newbatchItems.FirstOrDefault(u => u.ItemId == itemId && u.BatchNo == f.Batch_No && u.LotNo == f.Lot_No && u.LocationCode == f.Location_Code);
                                    if (newupdate != null)
                                    {
                                        newupdate.LocationCode = f.Location_Code;
                                        newupdate.NavQuantity += f.Remaining_Quantity;
                                        newupdate.LotNo = f.Lot_No;
                                        newupdate.ExpiryDate = f.Expiration_Date == DateTime.MinValue ? null : f.Expiration_Date;
                                        newupdate.ManufacturingDate = f.Manufacturing_Date == DateTime.MinValue ? null : f.Manufacturing_Date;
                                    }
                                }

                                //_context.SaveChanges();
                            }
                        }
                    }
                });

                var grouped = updatebatchItems.GroupBy(g => g.ItemBatchId).ToList();
                var updateRecord = new List<DataAccess.Models.ItemBatchInfo>();
                grouped.ForEach(f =>
                {
                    updateRecord.Add(f.LastOrDefault());
                });


                List<NavLocaltionModel> navLocaltionmodellist = new List<NavLocaltionModel>();

                List<ItemBatchInfoModel> navbatchitemlist = new List<ItemBatchInfoModel>();

                newLocations.ForEach(s =>
                {
                    NavLocaltionModel _navLocationmodel = new NavLocaltionModel()
                    {
                        Name = s.Name,
                        Code = s.Code,
                        Description = s.Description,
                        ItemId = s.ItemId,
                        PlantId = s.PlantId,
                        StatusCodeId = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId

                    };
                    navLocaltionmodellist.Add(_navLocationmodel);
                });


                newbatchItems.ForEach(m =>
                {
                    ItemBatchInfoModel _itembatchinfomodel = new ItemBatchInfoModel()
                    {
                        ItemBatchId = m.ItemBatchId,
                        ItemId = m.ItemId,
                        CompanyId = m.CompanyId,
                        LocationCode = m.LocationCode,
                        BatchNo = m.BatchNo,
                        LotNo = m.LotNo,
                        ExpiryDate = m.ExpiryDate,
                        ManufacturingDate = m.ManufacturingDate,
                        QuantityOnHand = m.QuantityOnHand,
                        NavQuantity = m.NavQuantity,
                        IssueQuantity = m.IssueQuantity,
                        BalanceQuantity = m.BalanceQuantity,
                        AddedDate = m.AddedDate == DateTime.MinValue ? DateTime.Now : m.AddedDate,
                        StatusCodeID = 1


                    };
                    navbatchitemlist.Add(_itembatchinfomodel);
                });



                var objbatchitmes = (from m in newbatchItems
                                     select new
                                     {
                                         ItemBatchId = m.ItemBatchId,
                                         ItemId = m.ItemId,
                                         CompanyId = m.CompanyId,
                                         LocationCode = m.LocationCode,
                                         BatchNo = m.BatchNo,
                                         LotNo = m.LotNo,
                                         ExpiryDate = m.ExpiryDate,
                                         ManufacturingDate = m.ManufacturingDate,
                                         QuantityOnHand = m.QuantityOnHand,
                                         NavQuantity = m.NavQuantity,
                                         IssueQuantity = m.IssueQuantity,
                                         BalanceQuantity = m.BalanceQuantity,
                                         StatusCodeId = m.StatusCodeId,
                                         AddedByUserId = m.AddedByUserId,
                                         AddedDate = m.AddedDate,
                                         ModifiedByUserId = m.ModifiedByUserId,
                                         ModifiedDate = m.ModifiedDate,
                                         StockOutBatchConfirm = m.StockOutBatchConfirm

                                     }).ToList();


                //_context.BulkInsert(newLocations);
                //_context.BulkInsert(newbatchItems);
                // _context.BulkUpdate(updateRecord, option => option.ColumnPrimaryKeyExpression = c => new { c.ItemBatchId });

                SqlBulkUpload objBulkInsertLocation = new SqlBulkUpload(_context, _config);
                await objBulkInsertLocation.BulkInsertAsync(navLocaltionmodellist, "Navlocation");

                SqlBulkUpload objBulkInsertBatch = new SqlBulkUpload(_context, _config);
                await objBulkInsertBatch.BulkInsertAsync(navbatchitemlist, "ItemBatchInfo", new List<string>()
                {
                    "ItemId",
                    "CompanyId",
                    "LocationCode",
                    "BatchNo",
                    "LotNo",
                    "ExpiryDate",
                    "ManufacturingDate",
                    "QuantityOnHand",
                    "NavQuantity",
                    "IssueQuantity",
                    "BalanceQuantity",
                    "AddedDate"
                });

                if (updateRecord.Count > 0)
                {
                    SqlBulkUpload objBulkInsert = new SqlBulkUpload(_context, _config);
                    objBulkInsert.BulkUpdateData(updateRecord, "ItemBatchId", new List<string>() { "AddedByUser", "Company", "Item", "ModifiedByUser"
                ,"StatusCode","AddedByUserId","AddedDate",}, "ItemBatchInfo");

                }

                if (itemList.Count < 1000)
                    break;
                page++;
            }

            return await GetItemBatchItems(searchModel);
        }

        [HttpPost]
        [Route("GetItemBatchItems")]
        public async Task<List<ItemBatchInfoModel>> GetItemBatchItems(StockBalanceSearch searchModel)
        {
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = await _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(s => s.Company).Where(c => c.CompanyId == searchModel.CompanyId).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToListAsync();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }

        [HttpPost]
        [Route("GetSalesOrderLineKIV")]
        public async Task<bool> GetSalesOrderLineKIV(StockBalanceSearch searchModel)
        {
            var reutData = await GetSalesProcessQty(searchModel);
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return false;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);

            //var deleteExisting = _context.NavSalesOrderKiv.Where(f => f.CompanyId == company.PlantId).ToList();
            await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM NavSalesOrderKiv WHERE CompanyId = {company.PlantId}");
            //if (deleteExisting.Count > 0)
            //{
            //    //vijay/// _context.BulkDelete(deleteExisting);               



            //    //_context.NavSalesOrderKiv.RemoveRange(deleteExisting);
            //    //_context.SaveChanges();

            //}
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.SalesOrderLineKIV.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.SalesOrderLineKIV> query = (DataServiceQuery<NAV.SalesOrderLineKIV>)nquery;

                TaskFactory<IEnumerable<NAV.SalesOrderLineKIV>> taskFactory = new TaskFactory<IEnumerable<NAV.SalesOrderLineKIV>>();
                IEnumerable<NAV.SalesOrderLineKIV> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();
                var KivItems = new List<NavSalesOrderKiv>();
                var NavItems = _context.Navitems.Where(f => f.CompanyId == company.PlantId).AsNoTracking().ToList();

                itemList.ForEach(f =>
                {
                    KivItems.Add(new NavSalesOrderKiv
                    {
                        CategoryCode = f.Item_Category_Code,
                        CompanyId = company.PlantId,
                        Description = f.Description,
                        Description1 = f.Description_2,
                        DocumentNo = f.Document_No,
                        DocumentType = f.Document_Type,
                        ExternalDocNo = f.External_Document_No,
                        InternalRef = f.Internal_Ref,
                        ItemNo = f.No,
                        ItemTrackingQty = f.Item_Tacking_Quantity,
                        LocationCode = f.Location_Code,
                        ItemId = NavItems.FirstOrDefault(i => i.No == f.No).ItemId,
                        OutstandingQty = f.Outstanding_Quantity,
                        Quantity = f.Quantity,
                        SellToCustomerName = f.Sell_to_Customer_Name,
                        SellToCustomerNo = f.Sell_to_Customer_No,
                        ShipmentDate = f.Shipment_Date,
                        TenderNo = f.Tender_No,
                        UnitPrice = f.Unit_Price,
                        Uom = f.Unit_of_Measure_Code,
                    });
                });
                // vijay // _context.BulkInsert(KivItems);              

                SqlBulkUpload objBulkInsertBatch = new SqlBulkUpload(_context, _config);
                await objBulkInsertBatch.BulkInsertAsync(KivItems, "NavSalesOrderKiv");

                //_context.NavSalesOrderKiv.AddRange(KivItems);
                //_context.SaveChanges();

                if (itemList.Count < 1000)
                    break;
                page++;
            }

            return true;
        }

        [HttpPost]
        [Route("GetSalesOrderKIVItems")]
        public async Task<List<NavSalesOrderKivModel>> GetSalesOrderKIVItems(StockBalanceSearch searchModel)
        {
            List<NavSalesOrderKivModel> navSalesOrderKivModels = new List<NavSalesOrderKivModel>();
            var navSalesOrderKivs = await _context.NavSalesOrderKiv.Where(s => s.CompanyId == searchModel.CompanyId).AsNoTracking().ToListAsync();
            navSalesOrderKivs.ForEach(n =>
            {
                navSalesOrderKivModels.Add(new NavSalesOrderKivModel
                {
                    SalesOrderKivid = n.SalesOrderKivid,
                    CompanyId = n.CompanyId,
                    ItemId = n.ItemId,
                    ItemNo = n.ItemNo,
                    Description = n.Description,
                    Description1 = n.Description1,
                    DocumentNo = n.DocumentNo,
                    DocumentType = n.DocumentType,
                    InternalRef = n.InternalRef,
                    ExternalDocNo = n.ExternalDocNo,
                    CategoryCode = n.CategoryCode,
                    ItemTrackingQty = n.ItemTrackingQty,
                    LocationCode = n.LocationCode,
                    OutstandingQty = n.OutstandingQty,
                    Quantity = n.Quantity,
                    SellToCustomerName = n.SellToCustomerName,
                    SellToCustomerNo = n.SellToCustomerNo,
                    ShipmentDate = n.ShipmentDate,
                    TenderNo = n.TenderNo,
                    Uom = n.Uom,
                    UnitPrice = n.UnitPrice
                });
            });
            return navSalesOrderKivModels;
        }

        [HttpPost]
        [Route("GetSalesProcessQty")]
        public async Task<bool> GetSalesProcessQty(StockBalanceSearch searchModel)
        {
            try
            {
                await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating stock balance, reterive company item card.");

                var company = await _context.Plant.FirstOrDefaultAsync(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
                if (company != null)
                {
                    var Company = company.NavCompanyName;

                    var context = new DataService(_config, Company);

                    var post = new WebIntegration.WebIntegration_PortClient();

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/WebIntegration"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                  System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    var inventoryItems = await _context.Navitems.Where(f => f.CompanyId == searchModel.CompanyId && f.No.StartsWith("FP-")).ToListAsync();


                    var month = searchModel.StkMonth.Month;// lastDayLastMonth.Month;
                    var year = searchModel.StkMonth.Year;// lastDayLastMonth.Year;

                    var weekofMonth = GetWeekNumberOfMonth(searchModel.StkMonth);
                    int count = 0;
                    int totalItems = inventoryItems.Count();
                    inventoryItems.ForEach(f =>
                    {
                        var stockdate = searchModel.StkMonth;

                        var itemCount = count + " of " + totalItems;
                        //notify client progress update
                        var itemName = string.Format("{0} {1}-{2}  {3}", itemCount, f.No, f.Description, "from NAV Stock balance");
                        _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", itemName);

                        var SupplyWipqty = post.FnCalculateSupplyWIPAsync(f.No, stockdate).Result.return_value;
                        var Supply1ProcessQty = post.FnCalculateSupply1stProcessAsync(f.No, stockdate).Result.return_value;

                        _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Updating Dist. KIV Qty");


                        var stockNav = _context.NavitemStockBalance.FirstOrDefault(d => d.ItemId == f.ItemId && d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && d.StockBalWeek == weekofMonth);
                        if (stockNav == null)
                        {
                            f.NavitemStockBalance.Add(new NavitemStockBalance
                            {
                                ItemId = f.ItemId,
                                AddedByUserId = 1,
                                AddedDate = DateTime.Now,
                                StatusCodeId = 1,
                                Supply1ProcessQty = Supply1ProcessQty,
                                SupplyWipqty = SupplyWipqty,
                                StockBalWeek = weekofMonth,
                                StockBalMonth = stockdate,
                                GlobalQty = 0,
                                Kivqty = 0,
                                Quantity = 0,
                                RejectQuantity = 0,
                                ReworkQty = 0,
                                Wipqty = 0,
                            });
                        }
                        else
                        {
                            stockNav.SupplyWipqty = SupplyWipqty;
                            stockNav.Supply1ProcessQty = Supply1ProcessQty;
                        }
                        _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Update stock balance into portal database");

                        _context.SaveChanges();
                        count++;
                    });
                    await _hub.Clients.Group(searchModel.UserId.ToString()).SendAsync("progress", "Completed");
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        [HttpGet]
        [Route("GetSalesPrice")]
        public async Task<List<SalesPrices>> GetSalesPrice(string company)
        {
            var context = new DataService(_config, company);
            var sales = new List<SalesPrices>();

            int pageSize = 1000;
            int page = 0;
            var itemList = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CompanyId == 1).AsNoTracking().ToList();
            var customers = _context.Navcustomer.AsNoTracking().ToList();

            var saldelete = _context.ItemSalesPrice.AsNoTracking().ToList();
            if (saldelete.Count > 0)
            {
                _context.ItemSalesPrice.RemoveRange(saldelete);
                _context.SaveChanges();
            }


            while (true)
            {
                var nquery = context.Context.SalesPrices.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<SalesPrices> query = (DataServiceQuery<SalesPrices>)nquery;

                TaskFactory<IEnumerable<SalesPrices>> taskFactory = new TaskFactory<IEnumerable<SalesPrices>>();
                IEnumerable<SalesPrices> results = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var salesPrices = results.ToList();

                salesPrices.ForEach(f =>
                {
                    DateTime? statDate = DateTime.Now;
                    if (f.Starting_Date == DateTime.MinValue)
                    {
                        statDate = null;
                    }
                    else
                    {
                        statDate = f.Starting_Date;
                    }
                    var item = itemList.FirstOrDefault(i => i.No == f.Item_No);
                    var cust = customers.FirstOrDefault(i => i.Code == f.Sales_Code);
                    var saleModel = new ItemSalesPrice
                    {
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now,
                        CompanyId = 1,
                        StatusCodeId = 1,
                        SellingPrice = f.Cost,
                        StartDate = statDate,
                        EndDate = f.Ending_Date == DateTime.MinValue ? null : f.Ending_Date,
                        ItemId = item?.ItemId,
                        CustomerId = cust?.CustomerId
                    };
                    _context.ItemSalesPrice.Add(saleModel);
                });

                _context.SaveChanges();

                if (salesPrices.Count < 1000)
                    break;
                page++;
            }

            return sales;
        }

        [HttpGet]
        [Route("GetIExcelReport")]
        public async Task<List<view_INPProdPlanning>> GetIExcelReport(string company)
        {
            string sqlQuery = string.Empty;

            sqlQuery = "Select  * from view_INPProdPlanning ";
            var applicationUser = _context.Set<view_INPProdPlanning>().FromSqlRaw(sqlQuery).AsNoTracking().ToList();

            return applicationUser;
        }

        private async void SyncAndGetItems(IEnumerable<ItemList> itemLists, string company, long? companyId)
        {

            var items = itemLists.ToList();
            List<string> itemsNos = items.Select(s => s.No.Trim().ToLower()).ToList();
            List<Navitems> existingNavitems = _context.Navitems.Where(n => itemsNos.Contains(n.No.Trim().ToLower()) && n.CompanyId == companyId).AsNoTracking().ToList();
            List<string> existingNos = existingNavitems.Select(n => n.No.Trim().ToLower()).ToList();
            List<ItemList> existingItems = items.Where(i => existingNos.Contains(i.No.Trim().ToLower())).ToList();
            List<ItemList> newItemLists = items.Where(i => !existingNos.Contains(i.No.Trim().ToLower())).ToList();
            List<Navitems> navItems = GenerateNewItems(newItemLists, company, companyId);
            List<Navitems> updatedNavItems = UpdateNavItems(existingNavitems, existingItems, company);
            //_context.BulkUpdate(updatedNavItems);
            //_context.BulkInsert(navItems);


            List<NavItemModel> updatenavitemModelList = new List<NavItemModel>();


            var updatedNavItemslist = (from s in updatedNavItems
                                       select new
                                       {
                                           ItemId = s.ItemId,
                                           CompanyId = s.CompanyId,
                                           No = s.No,
                                           RelatedItemNo = s.RelatedItemNo,
                                           Description = s.Description,
                                           Description2 = s.Description2,
                                           ItemType = s.ItemType,
                                           Inventory = s.Inventory,
                                           InternalRef = s.InternalRef,
                                           ItemRegistration = s.ItemRegistration,
                                           ExpirationCalculation = s.ExpirationCalculation,
                                           BatchNos = s.BatchNos,
                                           ProductionRecipeNo = s.ProductionRecipeNo,
                                           Qcenabled = s.Qcenabled,
                                           SafetyLeadTime = s.SafetyLeadTime,
                                           ProductionBomno = s.ProductionBomno,
                                           RoutingNo = s.RoutingNo,
                                           BaseUnitofMeasure = s.BaseUnitofMeasure,
                                           UnitCost = s.UnitCost,
                                           UnitPrice = s.UnitPrice,
                                           VendorNo = s.VendorNo,
                                           VendorItemNo = s.VendorItemNo,
                                           ItemCategoryCode = s.ItemCategoryCode,
                                           ItemTrackingCode = s.ItemTrackingCode,
                                           Qclocation = s.Qclocation,
                                           Company = s.Company,
                                           LastSyncDate = s.LastSyncDate,
                                           LastSyncBy = s.LastSyncBy,
                                           CategoryId = s.CategoryId,
                                           Steroid = s.Steroid,
                                           ShelfLife = s.ShelfLife,
                                           Quota = s.Quota,
                                           PackSize = s.PackSize,
                                           StatusCodeId = s.StatusCodeId,
                                           AddedByUserId = s.AddedByUserId,
                                           AddedDate = s.AddedDate,
                                           ModifiedByUserId = s.ModifiedByUserId,
                                           ModifiedDate = s.ModifiedDate,
                                           PackUom = s.PackUom,
                                           GenericCodeId = s.GenericCodeId,
                                           IsDifferentAcuom = s.IsDifferentAcuom,
                                           PackQty = s.PackQty,
                                           PurchaseUom = s.PurchaseUom,
                                           ReplenishmentMethodId = s.ReplenishmentMethodId,
                                           ImageUrl = s.ImageUrl,
                                           IsPortal = s.IsPortal,
                                           ItemSerialNo = s.ItemSerialNo,
                                           PackSizeId = s.PackSizeId,
                                           SupplyToId = s.SupplyToId,
                                           UomId = s.UomId,
                                       }).ToList();


            var objnavItems = (from s in navItems
                               select new
                               {
                                   ItemId = s.ItemId,
                                   CompanyId = s.CompanyId,
                                   No = s.No,
                                   RelatedItemNo = s.RelatedItemNo,
                                   Description = s.Description,
                                   Description2 = s.Description2,
                                   ItemType = s.ItemType,
                                   Inventory = s.Inventory,
                                   InternalRef = s.InternalRef,
                                   ItemRegistration = s.ItemRegistration,
                                   ExpirationCalculation = s.ExpirationCalculation,
                                   BatchNos = s.BatchNos,
                                   ProductionRecipeNo = s.ProductionRecipeNo,
                                   Qcenabled = s.Qcenabled,
                                   SafetyLeadTime = s.SafetyLeadTime,
                                   ProductionBomno = s.ProductionBomno,
                                   RoutingNo = s.RoutingNo,
                                   BaseUnitofMeasure = s.BaseUnitofMeasure,
                                   UnitCost = s.UnitCost,
                                   UnitPrice = s.UnitPrice,
                                   VendorNo = s.VendorNo,
                                   VendorItemNo = s.VendorItemNo,
                                   ItemCategoryCode = s.ItemCategoryCode,
                                   ItemTrackingCode = s.ItemTrackingCode,
                                   Qclocation = s.Qclocation,
                                   Company = s.Company,
                                   LastSyncDate = s.LastSyncDate,
                                   LastSyncBy = s.LastSyncBy,
                                   CategoryId = s.CategoryId,
                                   Steroid = s.Steroid,
                                   ShelfLife = s.ShelfLife,
                                   Quota = s.Quota,
                                   PackSize = s.PackSize,
                                   StatusCodeId = s.StatusCodeId,
                                   AddedByUserId = s.AddedByUserId,
                                   AddedDate = s.AddedDate,
                                   ModifiedByUserId = s.ModifiedByUserId,
                                   ModifiedDate = s.ModifiedDate,
                                   PackUom = s.PackUom,
                                   GenericCodeId = s.GenericCodeId,
                                   IsDifferentAcuom = s.IsDifferentAcuom,
                                   PackQty = s.PackQty,
                                   PurchaseUom = s.PurchaseUom,
                                   ReplenishmentMethodId = s.ReplenishmentMethodId,
                                   ImageUrl = s.ImageUrl,
                                   IsPortal = s.IsPortal,
                                   ItemSerialNo = s.ItemSerialNo,
                                   PackSizeId = s.PackSizeId,
                                   SupplyToId = s.SupplyToId,
                                   UomId = s.UomId,
                               }).ToList();




            if (updatedNavItems.Count > 0)
            {
                SqlBulkUpload objUpdate = new SqlBulkUpload(_context, _config);
                objUpdate.BulkUpdateData(updatedNavItemslist, "ItemId", new List<string>() { "AddedByUser" }, "Navitems");
            }

            SqlBulkUpload objInsert = new SqlBulkUpload(_context, _config);
            await objInsert.BulkInsertAsync(objnavItems, "Navitems");
        }

        private List<Navitems> GenerateNewItems(List<ItemList> newItemLists, string company, long? companyId)
        {
            List<Navitems> navitems = new List<Navitems>();
            newItemLists.ForEach(item =>
            {
                if (item.No == "FP-PP-CRM-088")
                {

                }
                Navitems newNavItem = new Navitems
                {
                    No = item.No,
                    RelatedItemNo = item.Related_Item_No,
                    Description = item.Description,
                    Description2 = item.Description_2,
                    ItemType = item.Type,
                    StatusCodeId = item.Blocked.GetValueOrDefault(false) ? 2 : 1,
                    Inventory = item.Inventory,
                    InternalRef = item.Internal_Ref,
                    ItemRegistration = item.Item_Registration,
                    ExpirationCalculation = item.Expiration_Calculation,
                    BatchNos = item.Batch_Nos,
                    ProductionRecipeNo = item.Production_Recipe_No,
                    Qcenabled = item.QC_Enabled,
                    //SafetyLeadTime = item.Safety_Lead_Time,
                    ProductionBomno = item.Production_BOM_No,
                    RoutingNo = item.Routing_No,
                    BaseUnitofMeasure = item.Base_Unit_of_Measure,
                    UnitCost = item.Unit_Cost,
                    UnitPrice = item.Unit_Price,
                    VendorNo = item.Replenishment_System,
                    //VendorItemNo = item.Vendor_Item_No,
                    ItemCategoryCode = item.Item_Category_Code,
                    ItemTrackingCode = item.Item_Tracking_Code,
                    Qclocation = item.QC_Location,
                    //LastSyncDate = item.Last_Date_Modified,
                    Company = company,
                    CompanyId = companyId,
                    PurchaseUom = item.Purch_Unit_of_Measure,
                    ShelfLife = item.Expiration_Calculation,
                };
                navitems.Add(newNavItem);
            });
            return navitems;
        }

        private List<Navitems> UpdateNavItems(List<Navitems> existingNavitems, List<ItemList> existingItems, string company)
        {
            existingNavitems.ForEach(e =>
            {
                var item = existingItems.FirstOrDefault(i => i.No.Trim().ToLower() == e.No.Trim().ToLower());
                e.No = item.No;
                e.RelatedItemNo = item.Related_Item_No;
                e.ShelfLife = item.Expiration_Calculation;
                //e.Description = item.Description;
                //e.Description2 = item.Description_2;
                //e.ItemType = item.Type;
                //// e.Inventory = item.Inventory;
                //e.InternalRef = item.Internal_Ref;
                //e.ItemRegistration = item.Item_Registration;
                //e.ExpirationCalculation = item.Expiration_Calculation;
                //e.BatchNos = item.Batch_Nos;
                //e.ProductionRecipeNo = item.Production_Recipe_No;
                //e.Qcenabled = item.QC_Enabled;
                //e.SafetyLeadTime = item.Safety_Lead_Time;
                //e.ProductionBomno = item.Production_BOM_No;
                //e.RoutingNo = item.Routing_No;
                //e.BaseUnitofMeasure = item.Base_Unit_of_Measure;
                //e.UnitCost = item.Unit_Cost;
                //e.UnitPrice = item.Unit_Price;
                //e.VendorNo = item.Replenishment_System;
                //e.VendorItemNo = item.Vendor_Item_No;
                //e.ItemCategoryCode = item.Item_Category_Code;
                //e.ItemTrackingCode = item.Item_Tracking_Code;
                //e.Qclocation = item.QC_Location;
                // e.LastSyncDate = item.Last_Date_Modified;
                e.StatusCodeId = item.Blocked.GetValueOrDefault(false) ? 2 : 1;
                //e.Company = company;
                //e.CompanyId = 2;
                e.PurchaseUom = item.Purch_Unit_of_Measure;
            });
            return existingNavitems;
        }

        [HttpGet]
        [Route("GetProdOrders")]
        public async Task<IEnumerable<CPS_ProductionOrder>> Get(string company)
        {
            var context = new DataService(_config, company);
            var nquery = context.Context.CPS_ProductionOrder.Where(r => r.Status == "Released");
            DataServiceQuery<CPS_ProductionOrder> query = (DataServiceQuery<CPS_ProductionOrder>)nquery;

            TaskFactory<IEnumerable<CPS_ProductionOrder>> taskFactory = new TaskFactory<IEnumerable<CPS_ProductionOrder>>();
            IEnumerable<CPS_ProductionOrder> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


            return result;
        }
        [HttpPost]
        [Route("GetVendors")]
        public async Task<IEnumerable<Navvendor>> GetVendors(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);
            var nquery = context.Context.Vendor;
            DataServiceQuery<Vendor> query = (DataServiceQuery<Vendor>)nquery;

            TaskFactory<IEnumerable<Vendor>> taskFactory = new TaskFactory<IEnumerable<Vendor>>();
            IEnumerable<Vendor> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
            var vendorList = result.ToList();
            vendorList.ForEach(v =>
            {
                var vendorExist = _context.Navvendor.Any(f => f.VendorNo == v.No && f.CompanyId == searchModel.CompanyId);
                if (!vendorExist)
                {
                    var vendor = new Navvendor
                    {
                        VendorNo = v.No,
                        VendorName = v.Name,
                        StatusCodeId = 1,
                        AddedByUserId = searchModel.UserId,
                        AddedDate = DateTime.Now,
                        VendorItemNo = v.Location_Code,
                        CompanyId = searchModel.CompanyId
                    };
                    _context.Navvendor.Add(vendor);
                }
            });
            _context.SaveChanges();
            return _context.Navvendor.ToList();
        }
        [HttpGet]
        [Route("GetProdCodes")]
        public async Task<IEnumerable<ProductCodeList>> GetProdCodes(string company)
        {
            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }

            var context = new DataService(_config, company);
            var nquery = context.Context.ProductCodeList;
            DataServiceQuery<ProductCodeList> query = (DataServiceQuery<ProductCodeList>)nquery;

            TaskFactory<IEnumerable<ProductCodeList>> taskFactory = new TaskFactory<IEnumerable<ProductCodeList>>();
            IEnumerable<ProductCodeList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

            var prodCodes = result.ToList();

            prodCodes.ForEach(f =>
            {
                var codes = new NavproductCode
                {
                    ProductCode = f.Code,
                    ProductCodeName = f.Description,
                    StatusCodeId = 1,
                    AddedDate = DateTime.Now,
                    AddedByUserId = 1,
                    CompanyId = companyId,
                };
                _context.NavproductCode.Add(codes);
            });
            _context.SaveChanges();
            return result;
        }
        [HttpGet]
        [Route("GetLocation")]
        public async Task<IEnumerable<Location>> GetLocation(string company)
        {
            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }

            var context = new DataService(_config, company);
            var nquery = context.Context.Location;
            DataServiceQuery<Location> query = (DataServiceQuery<Location>)nquery;

            TaskFactory<IEnumerable<Location>> taskFactory = new TaskFactory<IEnumerable<Location>>();
            IEnumerable<Location> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

            var prodCodes = result.ToList();

            prodCodes.ForEach(f =>
            {
                var appMaster = new ApplicationMasterDetail
                {
                    AddedByUserId = 1,
                    ApplicationMasterId = 20077,
                    Description = f.Name,
                    Value = f.Code,
                    StatusCodeId = 1,
                };
                _context.ApplicationMasterDetail.Add(appMaster);
            });
            _context.SaveChanges();
            return result;
        }
        [HttpGet]
        [Route("GetProdPlanningLine")]
        public async Task<IEnumerable<ProdPlanningLine>> GetProdPlanningLine(string company)
        {
            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }
            var fromMonth = DateTime.Today;
            var year = fromMonth.Year - 1;
            var tomonth = DateTime.Today.AddMonths(6);
            var context = new DataService(_config, company);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.ProdPlanningLine.Where(f => f.g_intYr >= year).Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<ProdPlanningLine> query = (DataServiceQuery<ProdPlanningLine>)nquery;

                TaskFactory<IEnumerable<ProdPlanningLine>> taskFactory = new TaskFactory<IEnumerable<ProdPlanningLine>>();
                IEnumerable<ProdPlanningLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();
                var itemMaster = _context.Navitems.AsNoTracking().ToList();
                // var prodOrder = _context.ProductionSimulation.ToList();
                prodCodes.ForEach(f =>
                {
                    var item = itemMaster.FirstOrDefault(i => i.No == f.Item_No && i.CompanyId == companyId);
                    if (item != null)
                    {
                        var appMaster = new ProductionSimulation
                        {
                            AddedByUserId = 1,
                            StatusCodeId = 1,
                            CompanyId = companyId,
                            AddedDate = DateTime.Now,
                            BatchNo = f.Batch_No,
                            Description = f.Description,
                            ItemNo = f.Item_No,
                            PackSize = f.g_cdePackSize,
                            PerQtyUom = f.g_cdeSmallQtyUOM,
                            PerQuantity = f.g_decSmallQty.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            ProdOrderNo = f.Prod_Order_No,
                            Quantity = f.Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            PlannedQty = f.Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            StartingDate = f.Starting_Date.GetValueOrDefault(DateTime.Today),
                            Uom = f.Unit_of_Measure_Code,
                            IsOutput = false,
                            ItemId = item.ItemId,
                            ProcessDate = f.g_dte1stProcessDate,
                            // SW Reference BatchSize = f.Batch_Size,
                            RePlanRefNo = f.Prod_Order_No,
                            IsBmrticket = false,
                        };

                        var history = new ProductionSimulationHistory
                        {
                            AddedByUserId = 1,
                            StatusCodeId = 1,
                            CompanyId = companyId,
                            AddedDate = DateTime.Now,
                            BatchNo = f.Batch_No,
                            Description = f.Description,
                            ItemNo = f.Item_No,
                            PackSize = f.g_cdePackSize,
                            PerQtyUom = f.g_cdeSmallQtyUOM,
                            PerQuantity = f.g_decSmallQty.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            ProdOrderNo = f.Prod_Order_No,
                            Quantity = f.Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            StartingDate = f.Starting_Date.GetValueOrDefault(DateTime.Today),
                            Uom = f.Unit_of_Measure_Code,
                            IsOutput = false,
                            ItemId = item.ItemId,


                        };

                        _context.ProductionSimulation.Add(appMaster);

                        //history.ProductionSimulationId = appMaster.ProductionSimulationId;
                        _context.ProductionSimulationHistory.Add(history);
                        //_context.SaveChanges();
                    }
                });
                await _context.SaveChangesAsync();
                if (prodCodes.Count < 1000)
                    break;
                page++;
            }
            return null;
        }
        [HttpGet]
        [Route("GetProdGroupLine")]
        public async Task<IEnumerable<NAV.GroupPlanning>> GetProdGroupLine(string company)
        {

            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }
            await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM GroupPlaningTicket WHERE CompanyId = {companyId}");

            var fromMonth = DateTime.Today;
            var year = fromMonth.Year - 1;
            var tomonth = DateTime.Today.AddMonths(6);
            var context = new DataService(_config, company);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var GroupPlaningTicket = new List<DataAccess.Models.GroupPlaningTicket>();
                var nquery = context.Context.GroupPlanning.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.GroupPlanning> query = (DataServiceQuery<NAV.GroupPlanning>)nquery;

                TaskFactory<IEnumerable<NAV.GroupPlanning>> taskFactory = new TaskFactory<IEnumerable<NAV.GroupPlanning>>();
                IEnumerable<NAV.GroupPlanning> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();

                prodCodes.ForEach(f =>
                {
                    if (f.Item_No == "FP-NP-TAB-100" || f.Item_No == "FP-NP-TAB-105")
                    {

                    }

                    var appMaster = new GroupPlaningTicket
                    {
                        BatchName = f.Batch_Name,
                        BatchSize = f.Batch_Size,
                        CompanyId = companyId,
                        Description = f.Description,
                        Description1 = f.Description_2,
                        ItemNo = f.Item_No,
                        NoOfTicket = f.No_of_Ticket,
                        ProductGroupCode = f.Product_Group_Code,
                        Quantity = f.Quantity,
                        StartDate = f.Start_Date == DateTime.MinValue ? DateTime.Now : f.Start_Date,
                        TotalQuantity = f.Total_Quantity,
                        Uom = f.Unit_Of_Measure_Code,

                    };
                    GroupPlaningTicket.Add(appMaster);
                });
                //await _context.SaveChangesAsync();              

                SqlBulkUpload objBInsert = new SqlBulkUpload(_context, _config);
                await objBInsert.BulkInsertAsync(GroupPlaningTicket, "GroupPlaningTicket");


                if (prodCodes.Count < 1000)
                    break;
                page++;
            }

            page = 0;
            while (true)
            {
                var GroupPlaningTicket = new List<DataAccess.Models.GroupPlaningTicket>();
                var nquery = context.Context.GroupPlanningSplit.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.GroupPlanningSplit> query = (DataServiceQuery<NAV.GroupPlanningSplit>)nquery;

                TaskFactory<IEnumerable<NAV.GroupPlanningSplit>> taskFactory = new TaskFactory<IEnumerable<NAV.GroupPlanningSplit>>();
                IEnumerable<NAV.GroupPlanningSplit> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();

                prodCodes.ForEach(fg =>
                {
                    if (fg.Item_No == "FP-NP-TAB-100" || fg.Item_No == "FP-NP-TAB-105")
                    {

                    }
                    //var groupTicket = _context.GroupPlaningTicket.FirstOrDefault(f => f.BatchName == fg.Batch_Name && f.ItemNo == fg.Item_No && f.ProductGroupCode == fg.Product_Group_Code && f.StartDate == fg.Start_Date && f.CompanyId == companyId);
                    //if (groupTicket != null)
                    //{
                    //    groupTicket.NoOfTicket = fg.No_of_Prod_Order;
                    //    groupTicket.Quantity = fg.Quantity;
                    //}
                    //else
                    //{
                    var appMaster = new GroupPlaningTicket
                    {
                        BatchName = fg.Batch_Name,
                        BatchSize = fg.Batch_Name,
                        CompanyId = companyId,
                        Description = fg.Description,
                        Description1 = fg.Description_2,
                        ItemNo = fg.Item_No,
                        NoOfTicket = fg.No_of_Prod_Order,
                        ProductGroupCode = fg.Product_Group_Code,
                        Quantity = fg.Quantity,
                        StartDate = fg.Start_Date == DateTime.MinValue ? DateTime.Now : fg.Start_Date,
                        TotalQuantity = fg.Quantity * fg.No_of_Prod_Order,
                        Uom = fg.Unit_Of_Measure_Code
                    };
                    GroupPlaningTicket.Add(appMaster);
                    //}

                });
                // await _context.SaveChangesAsync();          

                SqlBulkUpload objInsert = new SqlBulkUpload(_context, _config);
                await objInsert.BulkInsertAsync(GroupPlaningTicket, "GroupPlaningTicket");


                if (prodCodes.Count < 1000)
                    break;
                page++;
            }
            return null;
        }

        [HttpGet]
        [Route("GetProdNotStart")]
        public async Task<IEnumerable<NAV.ProdOrderLineList>> GetProdNotStart(string company)
        {

            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }
            //var exisdatas = _context.ProdOrderNotStart.Where(f => f.CompanyId == companyId).AsNoTracking().ToList();
            await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM ProdOrderNotStart WHERE CompanyId = {companyId}");


            var fromMonth = DateTime.Today;
            var year = fromMonth.Year - 1;
            var tomonth = DateTime.Today.AddMonths(6);
            var context = new DataService(_config, company);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.ProdOrderLineList.Where(f => f.Actual_Start_Date == null && f.Starting_Date >= DateTime.Today).Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.ProdOrderLineList> query = (DataServiceQuery<NAV.ProdOrderLineList>)nquery;

                TaskFactory<IEnumerable<NAV.ProdOrderLineList>> taskFactory = new TaskFactory<IEnumerable<NAV.ProdOrderLineList>>();
                IEnumerable<NAV.ProdOrderLineList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();
                var prodNotStartList = new List<ProdOrderNotStart>();
                prodCodes.ForEach(f =>
                {
                    var prodNotStart = new ProdOrderNotStart
                    {
                        CompanyId = companyId,
                        ItemNo = f.Item_No,
                        PackSize = f.Batch_No,
                        ProdOrderNo = f.Prod_Order_No,
                        Quantity = f.Quantity,
                        StartDate = f.Starting_Date == DateTime.MinValue ? null : f.Starting_Date,
                    };
                    prodNotStartList.Add(prodNotStart);
                });

                //_context.BulkInsert(prodNotStartList);

                SqlBulkUpload obj = new SqlBulkUpload(_context, _config);
                await obj.BulkInsertAsync(prodNotStartList, "ProdOrderNotStart");

                if (prodCodes.Count < 1000)
                    break;
                page++;
            }

            return null;
        }

        [HttpGet]
        [Route("GetProdOrderOutputLine")]
        public async Task<IEnumerable<ProdOrderOutputLine>> GetProdOrderOutputLine(string company)
        {
            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }
            var todayDate = DateTime.Today;
            var year = todayDate.Year - 1;
            var context = new DataService(_config, company);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.ProdOrderOutputLine.Where(f => f.g_intYr >= year).Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<ProdOrderOutputLine> query = (DataServiceQuery<ProdOrderOutputLine>)nquery;

                TaskFactory<IEnumerable<ProdOrderOutputLine>> taskFactory = new TaskFactory<IEnumerable<ProdOrderOutputLine>>();
                IEnumerable<ProdOrderOutputLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();

                // var prodOrder = _context.ProductionSimulation.ToList();
                var itemMaster = _context.Navitems.AsNoTracking().ToList();
                prodCodes.ForEach(f =>
                {
                    var item = itemMaster.FirstOrDefault(i => i.No == f.Item_No && i.CompanyId == companyId);
                    if (item != null)
                    {
                        var appMaster = new ProductionSimulation
                        {
                            AddedByUserId = 1,
                            StatusCodeId = 1,
                            CompanyId = companyId,
                            AddedDate = DateTime.Now,
                            BatchNo = f.Batch_No,
                            Description = f.Description,
                            ItemNo = f.Item_No,
                            PackSize = f.g_cdePackSize,
                            PerQtyUom = f.g_cdeSmallQtyUOM,
                            PerQuantity = f.g_decSmallQty.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            ProdOrderNo = f.Prod_Order_No,
                            Quantity = f.Finished_Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            StartingDate = f.g_dteOutputDate.GetValueOrDefault(DateTime.Today),
                            Uom = f.Unit_of_Measure_Code,
                            IsOutput = true,
                            OutputQty = f.Finished_Quantity * item.PackQty.GetValueOrDefault(1),
                            PlannedQty = f.Quantity * item.PackQty.GetValueOrDefault(1),
                            ItemId = item.ItemId,
                            //// SW Reference BatchSize = f.Batch_Size,
                            RePlanRefNo = f.Prod_Order_No,
                            IsBmrticket = false,
                        };
                        //_context.ProductionSimulation.Add(appMaster);
                        var history = new ProductionSimulationHistory
                        {
                            AddedByUserId = 1,
                            StatusCodeId = 1,
                            CompanyId = companyId,
                            AddedDate = DateTime.Now,
                            BatchNo = f.Batch_No,
                            Description = f.Description,
                            ItemNo = f.Item_No,
                            PackSize = f.g_cdePackSize,
                            PerQtyUom = f.g_cdeSmallQtyUOM,
                            PerQuantity = f.g_decSmallQty.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            ProdOrderNo = f.Prod_Order_No,
                            Quantity = f.Finished_Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                            StartingDate = f.g_dteOutputDate.GetValueOrDefault(DateTime.Today),
                            Uom = f.Unit_of_Measure_Code,
                            IsOutput = true,
                            OutputQty = f.Finished_Quantity * item.PackQty.GetValueOrDefault(1),
                            PlannedQty = f.Quantity * item.PackQty.GetValueOrDefault(1),
                            ItemId = item.ItemId,
                        };

                        _context.ProductionSimulation.Add(appMaster);

                        //history.ProductionSimulationId = appMaster.ProductionSimulationId;
                        _context.ProductionSimulationHistory.Add(history);
                        //_context.SaveChanges();
                    }
                });
                await _context.SaveChangesAsync();
                if (prodCodes.Count < 1000)
                    break;
                page++;
            }
            return null;
        }

        [HttpGet]
        [Route("GetBMRTickets")]
        public async Task<IEnumerable<SWDAllProdOutputLine>> GetBMRTickets(string company)
        {
            company.Split("&").ToList().ForEach(async f =>
            {
                var companyId = 1;
                if (f == "NAV_SG")
                {
                    companyId = 2;
                }
                var todayDate = DateTime.Today;
                var year = todayDate.Year - 1;
                var context = new DataService(_config, f);
                int pageSize = 1000;
                int page = 0;
                var itemMaster = _context.Navitems.AsNoTracking().ToList();
                while (true)
                {
                    var nquery = context.Context.SWDAllProdOutputLine.Skip(page * pageSize).Take(pageSize);
                    DataServiceQuery<SWDAllProdOutputLine> query = (DataServiceQuery<SWDAllProdOutputLine>)nquery;

                    TaskFactory<IEnumerable<SWDAllProdOutputLine>> taskFactory = new TaskFactory<IEnumerable<SWDAllProdOutputLine>>();
                    IEnumerable<SWDAllProdOutputLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                    var prodCodes = result.ToList();
                    prodCodes.ForEach(f =>
                    {
                        var recordExist = _context.ProductionSimulation.Any(a => a.ProdOrderNo == f.Prod_Order_No && a.ItemNo == f.Item_No && a.IsBmrticket == true);
                        if (!recordExist)
                        {
                            var item = itemMaster.FirstOrDefault(i => i.No == f.Item_No && i.CompanyId == companyId);
                            if (item != null)
                            {
                                var appMaster = new ProductionSimulation
                                {
                                    AddedByUserId = 1,
                                    StatusCodeId = 1,
                                    CompanyId = companyId,
                                    AddedDate = DateTime.Now,
                                    BatchNo = f.g_cdeBatchNo,
                                    Description = f.Product_Name,
                                    ItemNo = f.Item_No,
                                    PackSize = f.g_cdePackSize,
                                    PerQtyUom = f.g_cdeSmallQtyUOM,
                                    // PerQuantity = f.g_decSmallQty.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                                    ProdOrderNo = f.Prod_Order_No,
                                    //Quantity = f.Finished_Quantity.GetValueOrDefault(0) * item.PackQty.GetValueOrDefault(1),
                                    StartingDate = f.g_dteOutputDate.GetValueOrDefault(DateTime.Today),
                                    Uom = f.Unit_of_Measure_Code,
                                    IsOutput = true,
                                    // OutputQty = f.Finished_Quantity * item.PackQty.GetValueOrDefault(1),
                                    // PlannedQty = f.Quantity * item.PackQty.GetValueOrDefault(1),
                                    ItemId = item.ItemId,
                                    BatchSize = f.g_cdeBatchSize,
                                    RePlanRefNo = f.Replan_Ref_No,
                                    IsBmrticket = true,
                                };
                                _context.ProductionSimulation.Add(appMaster);
                            }
                        }
                    });
                    await _context.SaveChangesAsync();
                    if (prodCodes.Count < 1000)
                        break;
                    page++;
                }
            });
            return null;
        }

        [HttpGet]
        [Route("GetBaseUnit")]
        public async Task<IEnumerable<CRTSHF_UnitOfMeasures>> GetBaseUnit(string company)
        {
            var context = new DataService(_config, company);
            var nquery = context.Context.CRTSHF_UnitOfMeasures;
            DataServiceQuery<CRTSHF_UnitOfMeasures> query = (DataServiceQuery<CRTSHF_UnitOfMeasures>)nquery;
            var companyId = 1;
            if (company == "NAV_SG")
            {
                companyId = 2;
            }
            TaskFactory<IEnumerable<CRTSHF_UnitOfMeasures>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_UnitOfMeasures>>();
            IEnumerable<CRTSHF_UnitOfMeasures> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
            var bomresults = result.ToList();

            bomresults.ForEach(f =>
            {
                var baseUnit = _context.NavbaseUnit.Any(b => b.Code == f.Code);
                if (!baseUnit)
                {
                    var bom = new NavbaseUnit
                    {
                        Code = f.Code,
                        Description = f.Description,
                        StatusCodeId = 1,
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now,
                        CompanyId = companyId,
                    };
                    _context.NavbaseUnit.Add(bom);
                    _context.SaveChanges();
                }
            });

            return bomresults;
        }
        [HttpPost]
        [Route("GetItems")]
        public async Task<List<NavItemModel>> GetItems(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);


            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.ItemList.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<ItemList> query = (DataServiceQuery<ItemList>)nquery;

                TaskFactory<IEnumerable<ItemList>> taskFactory = new TaskFactory<IEnumerable<ItemList>>();
                IEnumerable<ItemList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var itemList = result.ToList();

                SyncAndGetItems(itemList, companyName, searchModel.CompanyId);

                if (itemList.Count < 1000)
                    break;
                page++;
            }
            NavItemController navItemController = new NavItemController(_context, _mapper);
            return navItemController.Get();
        }
        [HttpPost]
        [Route("GetCustomers")]
        public async Task<List<NavCustomerModel>> GetCustomers(StockBalanceSearch searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;
            var companyName = company.NavCompanyName;

            var context = new DataService(_config, companyName);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = context.Context.Customer.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<Customer> query = (DataServiceQuery<Customer>)nquery;

                TaskFactory<IEnumerable<Customer>> taskFactory = new TaskFactory<IEnumerable<Customer>>();
                IEnumerable<Customer> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var custList = result.ToList();

                SyncAndGetCustomers(custList, companyName, searchModel.CompanyId);

                if (custList.Count < 1000)
                    break;
                page++;

            }
            NavCustomerController navCustomerController = new NavCustomerController(_context, _mapper);
            return navCustomerController.Get();
        }


        private async void SyncAndGetCustomers(IEnumerable<Customer> customers, string company, long? companyId)
        {
            try
            {
                var cusList = customers.ToList();
                List<string> customerNos = cusList.Select(s => s.No.Trim().ToLower()).ToList();
                List<Navcustomer> existingNavCustomers = _context.Navcustomer.Where(n => customerNos.Contains(n.Code.Trim().ToLower()) && n.CompanyId == companyId).AsNoTracking().ToList();
                List<string> existingNos = existingNavCustomers.Select(n => n.Code.Trim().ToLower()).ToList();
                List<Customer> existingCusteoms = cusList.Where(i => existingNos.Contains(i.No.Trim().ToLower())).ToList();
                List<Customer> newCustomerLists = cusList.Where(i => !existingNos.Contains(i.No.Trim().ToLower())).ToList();
                List<Navcustomer> navCustomers = GenerateNewCustomers(newCustomerLists, company, companyId);
                List<Navcustomer> updatedNavCustomers = UpdateNavCustomers(existingNavCustomers, existingCusteoms, companyId);
                //_context.BulkUpdate(updatedNavCustomers);
                //_context.BulkInsert(navCustomers);



                List<NavCustomerModel> updatedNavCustomersList = new List<NavCustomerModel>();
                updatedNavCustomers.ForEach(s =>
                {
                    NavCustomerModel _updatenavCustomerModel = new NavCustomerModel()
                    {
                        CustomerId = s.CustomerId,
                        Code = s.Code,
                        Name = s.Name,
                        ResponsibilityCenter = s.ResponsibilityCenter,
                        LocationCode = s.LocationCode,
                        Address = s.Address,
                        Address2 = s.Address2,
                        City = s.City,
                        PostCode = s.PostCode,
                        County = s.County,
                        CountryRegionCode = s.CountryRegionCode,
                        PhoneNo = s.PhoneNo,
                        Contact = s.Contact,
                        SalespersonCode = s.SalespersonCode,
                        CustomerPostingGroup = s.CustomerPostingGroup,
                        GenBusPostingGroup = s.GenBusPostingGroup,
                        VatbusPostingGroup = s.VatbusPostingGroup,
                        PaymentTermsCode = s.PaymentTermsCode,
                        CurrencyCode = s.CurrencyCode,
                        LanguageCode = s.LanguageCode,
                        ShippingAdvice = s.ShippingAdvice,
                        ShippingAgentCode = s.ShippingAgentCode,
                        BalanceLcy = s.BalanceLcy,
                        BalanceDueLcy = s.BalanceDueLcy,
                        SalesLcy = s.SalesLcy,
                        LastSyncDate = s.LastSyncDate == DateTime.MinValue ? null : s.LastSyncDate,
                        LastSyncBy = s.LastSyncBy,
                        StatusCodeId = s.StatusCodeId,
                        Company = s.Company,
                        CompanyId = s.CompanyId

                    };
                    updatedNavCustomersList.Add(_updatenavCustomerModel);
                });

                if (updatedNavCustomers.Count > 0)
                {
                    SqlBulkUpload objBulkUpdate = new SqlBulkUpload(_context, _config);
                    objBulkUpdate.BulkUpdateData(updatedNavCustomersList, "CustomerId", new List<string>() { "CustomerName" }, "Navcustomer");

                }

                List<NavCustomerModel> navcutomerModelList = new List<NavCustomerModel>();
                navCustomers.ForEach(l =>
                {
                    NavCustomerModel _navCustomerModels = new NavCustomerModel()
                    {
                        CustomerId = l.CustomerId,
                        Code = l.Code,
                        Name = l.Name,
                        ResponsibilityCenter = l.ResponsibilityCenter,
                        LocationCode = l.LocationCode,
                        Address = l.Address,
                        Address2 = l.Address2,
                        City = l.City,
                        PostCode = l.PostCode,
                        County = l.County,
                        CountryRegionCode = l.CountryRegionCode,
                        PhoneNo = l.PhoneNo,
                        Contact = l.Contact,
                        SalespersonCode = l.SalespersonCode,
                        CustomerPostingGroup = l.CustomerPostingGroup,
                        GenBusPostingGroup = l.GenBusPostingGroup,
                        VatbusPostingGroup = l.VatbusPostingGroup,
                        PaymentTermsCode = l.PaymentTermsCode,
                        CurrencyCode = l.CurrencyCode,
                        LanguageCode = l.LanguageCode,
                        ShippingAdvice = l.ShippingAdvice,
                        ShippingAgentCode = l.ShippingAgentCode,
                        BalanceLcy = l.BalanceLcy,
                        BalanceDueLcy = l.BalanceDueLcy,
                        SalesLcy = l.SalesLcy,
                        LastSyncDate = l.LastSyncDate == DateTime.MinValue ? DateTime.Now : l.LastSyncDate,
                        //LastSyncBy = l.LastSyncBy,
                        StatusCodeId = l.StatusCodeId,
                        Company = l.Company,
                        //CustomerName= l.Name,
                        CompanyId = l.CompanyId

                    };
                    navcutomerModelList.Add(_navCustomerModels);
                });


                SqlBulkUpload objBulkInsert = new SqlBulkUpload(_context, _config);
                await objBulkInsert.BulkInsertAsync(navcutomerModelList, "Navcustomer");
                //await objBulkInsert.BulkInsertAsync(navCustomers, "Navcustomer", new List<string>()
                //{
                //  "Name","Code","Description","PlantID","StatusCodeID","AddedByUserID","AddedDate","ItemID"
                //});
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        private List<Navcustomer> GenerateNewCustomers(List<Customer> customers, string company, long? companyId)
        {
            List<Navcustomer> navCustomers = new List<Navcustomer>();
            customers.ForEach(item =>
            {
                Navcustomer newNavCustomer = new Navcustomer
                {
                    Code = item.No,
                    Name = item.Name,
                    ResponsibilityCenter = item.Responsibility_Center,
                    LocationCode = item.Location_Code,
                    Address = item.Address,
                    Address2 = item.Address_2,
                    City = item.City,
                    PostCode = item.Post_Code,
                    County = item.County,
                    CountryRegionCode = item.Country_Region_Code,
                    PhoneNo = item.Phone_No,
                    Contact = item.Contact,
                    SalespersonCode = item.Salesperson_Code,
                    CustomerPostingGroup = item.Customer_Posting_Group,
                    GenBusPostingGroup = item.Gen_Bus_Posting_Group,
                    VatbusPostingGroup = item.VAT_Bus_Posting_Group,
                    PaymentTermsCode = item.Payment_Terms_Code,
                    CurrencyCode = item.Currency_Code,
                    //LanguageCode = item.Language_Code,
                    ShippingAdvice = item.Shipping_Advice,
                    ShippingAgentCode = item.Shipping_Agent_Code,
                    // BalanceLcy = item.Balance_LCY,
                    //BalanceDueLcy = item.Balance_Due_LCY,
                    //SalesLcy = item.Sales_LCY,
                    LastSyncDate = item.Last_Date_Modified,
                    CompanyId = companyId,

                };

                navCustomers.Add(newNavCustomer);

            });
            return navCustomers;
        }

        private List<Navcustomer> UpdateNavCustomers(List<Navcustomer> existingNavCustomers, List<Customer> existingCustomers, long? companyId)
        {
            existingNavCustomers.ForEach(e =>
            {
                var item = existingCustomers.FirstOrDefault(i => i.No.Trim().ToLower() == e.Code.Trim().ToLower());
                e.Code = item.No;
                e.Name = item.Name;
                e.ResponsibilityCenter = item.Responsibility_Center;
                e.LocationCode = item.Location_Code;
                e.Address = item.Address;
                e.Address2 = item.Address_2;
                e.City = item.City;
                e.PostCode = item.Post_Code;
                e.County = item.County;
                e.CountryRegionCode = item.Country_Region_Code;
                e.PhoneNo = item.Phone_No;
                e.Contact = item.Contact;
                e.SalespersonCode = item.Salesperson_Code;
                e.CustomerPostingGroup = item.Customer_Posting_Group;
                e.GenBusPostingGroup = item.Gen_Bus_Posting_Group;
                e.VatbusPostingGroup = item.VAT_Bus_Posting_Group;
                e.PaymentTermsCode = item.Payment_Terms_Code;
                e.CurrencyCode = item.Currency_Code;
                //e.LanguageCode = item.Language_Code;
                e.ShippingAdvice = item.Shipping_Advice;
                e.ShippingAgentCode = item.Shipping_Agent_Code;
                // e.BalanceLcy = item.Balance_LCY;
                //e.BalanceDueLcy = item.Balance_Due_LCY;
                //e.SalesLcy = item.Sales_LCY;
                e.LastSyncDate = item.Last_Date_Modified;
                e.CompanyId = companyId;
            });
            return existingNavCustomers;
        }

        [HttpGet]
        [Route("GetProdOrderLines")]
        public async Task<IEnumerable<CPS_ProdOrderLine>> GetProdOrderLines(string id, string company)
        {
            var context = new DataService(_config, company);
            var nquery = context.Context.CPS_ProdOrderLine.Where(r => r.Prod_Order_No == id);
            DataServiceQuery<CPS_ProdOrderLine> query = (DataServiceQuery<CPS_ProdOrderLine>)nquery;

            TaskFactory<IEnumerable<CPS_ProdOrderLine>> taskFactory = new TaskFactory<IEnumerable<CPS_ProdOrderLine>>();
            IEnumerable<CPS_ProdOrderLine> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


            return result;
        }
        [HttpGet]
        [Route("GetRecipes")]
        public async Task<IEnumerable<NAVRecipesModel>> GetRecipes(string itemNo, string company)
        {
            var context = new DataService(_config, company);
            var nquery = context.Context.ProductionRecipe.Where(r => r.Item_No == itemNo);
            DataServiceQuery<ProductionRecipe> query = (DataServiceQuery<ProductionRecipe>)nquery;

            TaskFactory<IEnumerable<ProductionRecipe>> taskFactory = new TaskFactory<IEnumerable<ProductionRecipe>>();
            IEnumerable<ProductionRecipe> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
            var recipes = new List<NAVRecipesModel>();
            var recipeList = result.ToList();
            recipeList.ForEach(r =>
            {
                var recipe = new NAVRecipesModel
                {
                    ItemNo = r.Item_No,
                    Description = r.Description,
                    BatchSize = r.Batch_Size,
                    RecipeNo = r.Recipe_No,
                    Remark = r.Remark,
                };
                recipes.Add(recipe);
            });

            return recipes;
        }
        [HttpGet]
        [Route("GetSycRecipes")]
        public async Task<IEnumerable<NAVRecipesModel>> GetSycRecipes(string company)
        {
            var context = new DataService(_config, company);

            int pageSize = 1000;
            int page = 0;
            while (true)
            {

                var nquery = context.Context.ProductionRecipe.Skip(page * pageSize).Take(pageSize); ;
                DataServiceQuery<ProductionRecipe> query = (DataServiceQuery<ProductionRecipe>)nquery;

                TaskFactory<IEnumerable<ProductionRecipe>> taskFactory = new TaskFactory<IEnumerable<ProductionRecipe>>();
                IEnumerable<ProductionRecipe> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var recipeList = result.ToList();
                recipeList.ForEach(r =>
                {
                    var recipe = new DataAccess.Models.Navrecipes()
                    {
                        BatchSize = r.Batch_Size,
                        Description = r.Description,
                        ItemDescription = r.Item_Description,
                        ItemDescription2 = r.Item_Description_2,
                        ItemNo = r.Item_No,
                        MachineCenterCode = r.Machine_Center_Code,
                        MachineCenterName = r.Machine_Center_Name,
                        ProductionBomno = r.Production_BOM_No,
                        RecipeNo = r.Recipe_No,
                        Remark = r.Remark,
                        RoutingNo = r.Routing_No,
                        Status = r.Status,
                        CompanyId = 2,
                        StatusCodeId = 1,
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now,
                    };
                    _context.Navrecipes.Add(recipe);
                });
                _context.SaveChanges();
                if (recipeList.Count < 1000)
                    break;
                page++;

            }
            return null;
        }
        [HttpGet]
        [Route("GetSyncProdBomList")]
        public async Task<IEnumerable<CRTSHF_ProdBOMLines>> GetSyncProdBomList(string company)
        {
            var context = new DataService(_config, company);
            int pageSize = 1000;
            int page = 0;
            var resultList = new List<CRTSHF_ProdBOMLines>();
            while (true)
            {
                var nquery = context.Context.CRTSHF_ProdBOMLines.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<CRTSHF_ProdBOMLines> query = (DataServiceQuery<CRTSHF_ProdBOMLines>)nquery;

                TaskFactory<IEnumerable<CRTSHF_ProdBOMLines>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ProdBOMLines>>();
                IEnumerable<CRTSHF_ProdBOMLines> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                var bomList = result.ToList();
                resultList.AddRange(bomList);
                var localBomList = new List<NavprodBom>();
                bomList.ForEach(b =>
                {
                    localBomList.Add(new NavprodBom
                    {
                        BatchSize = b.Batch_Size,
                        Description = b.Description,
                        BomlineNo = b.Line_No,
                        ItemNo = b.No,
                        ProdBomno = b.Production_BOM_No,
                        Quantity = b.Quantity.GetValueOrDefault(0),
                        QuantityPer = b.Quantity_per.GetValueOrDefault(0),
                        QuantityPerUnit = b.Qty_per_Unit.GetValueOrDefault(0),
                        Type = b.Type,
                        Uom = b.Unit_of_Measure_Code,
                    });
                });
                _context.NavprodBom.AddRange(localBomList);
                _context.SaveChanges();

                if (bomList.Count < 1000)
                    break;
                page++;

            }
            return resultList;
        }
        [HttpGet]
        [Route("GetProdBomList")]
        public async Task<IEnumerable<NAVBOMLineModel>> GetProdBomList(string company, string itemNo)
        {

            var bom = _context.Navitems.FirstOrDefault(f => f.No == itemNo).ProductionBomno;
            var bomList = _context.NavprodBom.Where(b => b.ProdBomno == bom).ToList();
            var bomLines = new List<NAVBOMLineModel>();
            bomList.ForEach(b =>
            {
                bomLines.Add(new NAVBOMLineModel
                {
                    BatchSize = b.BatchSize,
                    Description = b.Description,
                    LineNo = b.BomlineNo.GetValueOrDefault(0),
                    No = b.ItemNo,
                    ProdBomNo = b.ProdBomno,
                    Quantity = b.Quantity.GetValueOrDefault(0),
                    QuantityPer = b.QuantityPer.GetValueOrDefault(0),
                    QuantityPerUnit = b.QuantityPerUnit.GetValueOrDefault(0),
                    Type = b.Type,
                    UOM = b.Uom,

                    Name = b.ItemNo + "  |  " + b.Description + "  |  " + b.Uom + "  |  " + b.QuantityPer,
                });
            });
            return bomLines;
        }

        [HttpGet]
        [Route("GetSyncProdBOMTree")]
        public IEnumerable<NavProdBomTreeModel> GetSyncProdBOMTree(string company, string itemNo)
        {
            List<NavProdBomTree> navProdBomTrees = new List<NavProdBomTree>();
            if (itemNo != null)
            {
                var treeItems = _context.NavProdBomTree.Where(t => t.MainParentItemNo == itemNo).ToList();
                _context.NavProdBomTree.RemoveRange(treeItems);
                _context.SaveChanges();

                var item = _context.Navitems.FirstOrDefault(f => f.No == itemNo);
                if (item != null)
                {
                    NavProdBomTree navProdBomTree = new NavProdBomTree();
                    string parentRecipeNo = item.ProductionRecipeNo;
                    string parentItemNo = item.No;

                    navProdBomTree.ParentRecipeNo = parentRecipeNo;
                    navProdBomTree.ItemNo = parentItemNo;
                    navProdBomTree.RecipeNo = parentRecipeNo;
                    navProdBomTree.Description = item.Description;
                    navProdBomTree.Uom = item.BaseUnitofMeasure;
                    navProdBomTree.BatchNo = item.BatchNos;
                    navProdBomTree.LeadTime = item.SafetyLeadTime;
                    navProdBomTree.Quantity = item.Inventory;
                    navProdBomTree.MainParentItemNo = itemNo;

                    _context.NavProdBomTree.Add(navProdBomTree);
                    navProdBomTrees.Add(navProdBomTree);

                    var recipes = _context.NavprodBom.Where(b => b.ProdBomno == item.ProductionRecipeNo).AsNoTracking().ToList();

                    foreach (var recipeItem in recipes)
                    {
                        var navItem = _context.Navitems.FirstOrDefault(f => f.No == recipeItem.ItemNo);

                        if (navItem != null)
                        {
                            NavProdBomTree childNavProdBomTree = new NavProdBomTree();
                            childNavProdBomTree.ParentItemNo = navProdBomTree.ItemNo;
                            childNavProdBomTree.ParentRecipeNo = navProdBomTree.RecipeNo;
                            childNavProdBomTree.ItemNo = navItem.No;
                            childNavProdBomTree.RecipeNo = navItem.ProductionRecipeNo;
                            childNavProdBomTree.Description = navItem.Description;
                            childNavProdBomTree.Uom = navItem.BaseUnitofMeasure;
                            childNavProdBomTree.BatchNo = navItem.BatchNos;
                            childNavProdBomTree.LeadTime = navItem.SafetyLeadTime;
                            childNavProdBomTree.Quantity = navItem.Inventory;
                            childNavProdBomTree.MainParentItemNo = itemNo;
                            _context.NavProdBomTree.Add(childNavProdBomTree);
                            navProdBomTrees.Add(childNavProdBomTree);
                            AddChildProdBomTrees(childNavProdBomTree, navProdBomTrees, itemNo);

                        }

                        var recipeNavigationItem = _context.Navitems.FirstOrDefault(f => f.No == navItem.ProductionRecipeNo);

                        if (recipeNavigationItem != null)
                        {
                            NavProdBomTree childNavProdBomTree = new NavProdBomTree();
                            childNavProdBomTree.ParentItemNo = navProdBomTree.ItemNo;
                            childNavProdBomTree.ParentRecipeNo = navProdBomTree.RecipeNo;
                            childNavProdBomTree.ItemNo = recipeNavigationItem.No;
                            childNavProdBomTree.RecipeNo = recipeNavigationItem.ProductionRecipeNo;
                            childNavProdBomTree.Description = recipeNavigationItem.Description;
                            childNavProdBomTree.Uom = recipeNavigationItem.BaseUnitofMeasure;
                            childNavProdBomTree.BatchNo = recipeNavigationItem.BatchNos;
                            childNavProdBomTree.LeadTime = recipeNavigationItem.SafetyLeadTime;
                            childNavProdBomTree.Quantity = recipeNavigationItem.Inventory;
                            childNavProdBomTree.MainParentItemNo = itemNo;
                            _context.NavProdBomTree.Add(childNavProdBomTree);
                            navProdBomTrees.Add(childNavProdBomTree);
                            AddChildProdBomTrees(childNavProdBomTree, navProdBomTrees, itemNo);
                        }

                    }
                }
                _context.SaveChanges();
                navProdBomTrees = _context.NavProdBomTree.Where(s => s.MainParentItemNo == itemNo).AsNoTracking().ToList();
            }
            else
            {
                List<string> itemNos = new List<string>() { "FP-PP-TAB-003", "FP-PP-SYR-243", "FP-PP-TAB-144", "FP-NP-TAB-070", "FP-PP-TAB-152" };

                var treeItems = _context.NavProdBomTree.Where(t => itemNos.Contains(t.MainParentItemNo)).AsNoTracking().ToList();
                _context.NavProdBomTree.RemoveRange(treeItems);
                _context.SaveChanges();

                var items = _context.Navitems.Where(f => itemNos.Contains(f.No)).AsNoTracking().ToList();
                if (items.Any())
                {
                    items.ForEach(item =>
                    {
                        NavProdBomTree navProdBomTree = new NavProdBomTree();
                        string parentRecipeNo = item.ProductionRecipeNo;
                        string parentItemNo = item.No;

                        navProdBomTree.ParentRecipeNo = parentRecipeNo;
                        navProdBomTree.ItemNo = parentItemNo;
                        navProdBomTree.RecipeNo = parentRecipeNo;
                        navProdBomTree.Description = item.Description;
                        navProdBomTree.Uom = item.BaseUnitofMeasure;
                        navProdBomTree.BatchNo = item.BatchNos;
                        navProdBomTree.LeadTime = item.SafetyLeadTime;
                        navProdBomTree.Quantity = item.Inventory;
                        navProdBomTree.MainParentItemNo = item.No;

                        _context.NavProdBomTree.Add(navProdBomTree);
                        navProdBomTrees.Add(navProdBomTree);

                        var recipes = _context.NavprodBom.Where(b => b.ProdBomno == item.ProductionRecipeNo).AsNoTracking().ToList();

                        foreach (var recipeItem in recipes)
                        {
                            var navItem = _context.Navitems.FirstOrDefault(f => f.No == recipeItem.ItemNo);

                            if (navItem != null)
                            {
                                NavProdBomTree childNavProdBomTree = new NavProdBomTree();
                                childNavProdBomTree.ParentItemNo = navProdBomTree.ItemNo;
                                childNavProdBomTree.ParentRecipeNo = navProdBomTree.RecipeNo;
                                childNavProdBomTree.ItemNo = navItem.No;
                                childNavProdBomTree.RecipeNo = navItem.ProductionRecipeNo;
                                childNavProdBomTree.Description = navItem.Description;
                                childNavProdBomTree.Uom = navItem.BaseUnitofMeasure;
                                childNavProdBomTree.BatchNo = navItem.BatchNos;
                                childNavProdBomTree.LeadTime = navItem.SafetyLeadTime;
                                childNavProdBomTree.Quantity = navItem.Inventory;
                                childNavProdBomTree.MainParentItemNo = item.No;
                                _context.NavProdBomTree.Add(childNavProdBomTree);
                                navProdBomTrees.Add(childNavProdBomTree);
                                AddChildProdBomTrees(childNavProdBomTree, navProdBomTrees, item.No);

                            }

                            var recipeNavigationItem = _context.Navitems.FirstOrDefault(f => f.No == navItem.ProductionRecipeNo);

                            if (recipeNavigationItem != null)
                            {
                                NavProdBomTree childNavProdBomTree = new NavProdBomTree();
                                childNavProdBomTree.ParentItemNo = navProdBomTree.ItemNo;
                                childNavProdBomTree.ParentRecipeNo = navProdBomTree.RecipeNo;
                                childNavProdBomTree.ItemNo = recipeNavigationItem.No;
                                childNavProdBomTree.RecipeNo = recipeNavigationItem.ProductionRecipeNo;
                                childNavProdBomTree.Description = recipeNavigationItem.Description;
                                childNavProdBomTree.Uom = recipeNavigationItem.BaseUnitofMeasure;
                                childNavProdBomTree.BatchNo = recipeNavigationItem.BatchNos;
                                childNavProdBomTree.LeadTime = recipeNavigationItem.SafetyLeadTime;
                                childNavProdBomTree.Quantity = recipeNavigationItem.Inventory;
                                childNavProdBomTree.MainParentItemNo = item.No;
                                _context.NavProdBomTree.Add(childNavProdBomTree);
                                navProdBomTrees.Add(childNavProdBomTree);
                                AddChildProdBomTrees(childNavProdBomTree, navProdBomTrees, item.No);
                            }

                        }
                    });
                }

                _context.SaveChanges();
                navProdBomTrees = _context.NavProdBomTree.Where(s => itemNos.Contains(s.MainParentItemNo)).AsNoTracking().ToList();
            }
            var hierarchyData = GenerateHierarchicalData(navProdBomTrees);
            return hierarchyData;
        }

        private void AddChildProdBomTrees(NavProdBomTree childNavProdBomTree, List<NavProdBomTree> navProdBomTrees, string mainParentItemNo)
        {
            var recipes = _context.NavprodBom.Where(b => b.ProdBomno == childNavProdBomTree.ItemNo).AsNoTracking().ToList();

            foreach (var recipeItem in recipes)
            {
                var navItem = _context.Navitems.FirstOrDefault(f => f.No == recipeItem.ItemNo);

                if (navItem != null)
                {
                    NavProdBomTree cNavProdBomTree = new NavProdBomTree();
                    cNavProdBomTree.ParentItemNo = childNavProdBomTree.ItemNo;
                    cNavProdBomTree.ParentRecipeNo = childNavProdBomTree.RecipeNo;
                    cNavProdBomTree.ItemNo = navItem.No;
                    cNavProdBomTree.RecipeNo = navItem.ProductionRecipeNo;
                    cNavProdBomTree.Description = navItem.Description;
                    cNavProdBomTree.Uom = navItem.BaseUnitofMeasure;
                    cNavProdBomTree.BatchNo = navItem.BatchNos;
                    cNavProdBomTree.LeadTime = navItem.SafetyLeadTime;
                    cNavProdBomTree.Quantity = navItem.Inventory;
                    cNavProdBomTree.MainParentItemNo = mainParentItemNo;
                    _context.NavProdBomTree.Add(cNavProdBomTree);
                    navProdBomTrees.Add(childNavProdBomTree);
                    AddChildProdBomTrees(cNavProdBomTree, navProdBomTrees, mainParentItemNo);
                }

                var recipeNavigationItem = _context.Navitems.FirstOrDefault(f => f.No == navItem.ProductionRecipeNo);

                if (recipeNavigationItem != null)
                {
                    NavProdBomTree cNavProdBomTree = new NavProdBomTree();
                    cNavProdBomTree.ParentItemNo = childNavProdBomTree.ItemNo;
                    cNavProdBomTree.ParentRecipeNo = childNavProdBomTree.RecipeNo;
                    cNavProdBomTree.ItemNo = recipeNavigationItem.No;
                    cNavProdBomTree.RecipeNo = recipeNavigationItem.ProductionRecipeNo;
                    cNavProdBomTree.Description = recipeNavigationItem.Description;
                    cNavProdBomTree.Uom = recipeNavigationItem.BaseUnitofMeasure;
                    cNavProdBomTree.BatchNo = recipeNavigationItem.BatchNos;
                    cNavProdBomTree.LeadTime = recipeNavigationItem.SafetyLeadTime;
                    cNavProdBomTree.Quantity = recipeNavigationItem.Inventory;
                    cNavProdBomTree.MainParentItemNo = mainParentItemNo;
                    _context.NavProdBomTree.Add(cNavProdBomTree);
                    navProdBomTrees.Add(childNavProdBomTree);
                    AddChildProdBomTrees(cNavProdBomTree, navProdBomTrees, mainParentItemNo);
                }
            }
        }

        private List<NavProdBomTreeModel> GenerateHierarchicalData(List<NavProdBomTree> bomTrees)
        {
            List<NavProdBomTreeModel> navProdBomTreeModels = new List<NavProdBomTreeModel>();
            var lookup = bomTrees.ToLookup(x => x.ParentItemNo);
            int i = 1;
            Func<string, List<NavProdBomTreeModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new NavProdBomTreeModel()
                    {
                        ItemNo = x.ItemNo,
                        Id = i++,
                        Name = x.ItemNo + "  |  " + x.Description + "  |  " + x.Uom + "  |  " + x.Quantity,
                        ParentItemNo = x.ParentItemNo,
                        RecipeNo = x.RecipeNo,
                        ParentRecipeNo = x.ParentRecipeNo,
                        Description = x.Description,
                        Quantity = x.Quantity,
                        Uom = x.Uom,
                        LeadTime = x.LeadTime,
                        BatchNo = x.BatchNo,
                        MainParentItemNo = x.MainParentItemNo,
                        Children = build(x.ItemNo),
                    })
                    .ToList();
            var nestedlist = build(null).ToList();

            return nestedlist;
        }

        private void IterateChildTreeNodes(List<NavProdBomTreeModel> navProdBomTreeModels, NavProdBomTree navProdBomTree)
        {
            navProdBomTreeModels.ForEach(child =>
            {
                if (child.Children.Any(c => c.ItemNo == navProdBomTree.ItemNo))
                {
                    var navProdBomTreeModel = child.Children.FirstOrDefault(c => c.ParentItemNo == navProdBomTree.ItemNo);
                    NavProdBomTreeModel navBomTreeModel = new NavProdBomTreeModel();
                    navBomTreeModel.ItemNo = navProdBomTree.ItemNo;
                    navBomTreeModel.ParentItemNo = navProdBomTree.ParentItemNo;
                    navBomTreeModel.RecipeNo = navProdBomTree.RecipeNo;
                    navBomTreeModel.ParentRecipeNo = navProdBomTree.ParentRecipeNo;
                    navBomTreeModel.Description = navProdBomTree.Description;
                    navBomTreeModel.Uom = navProdBomTree.Uom;
                    navBomTreeModel.LeadTime = navProdBomTree.LeadTime;
                    navBomTreeModel.BatchNo = navProdBomTree.BatchNo;
                    navBomTreeModel.Name = navProdBomTree.ItemNo + "  |  " + navProdBomTree.Description + "  |  " + navProdBomTree.Uom;
                    long index = navBomTreeModel.Children.Count > 0 ? ((long)navBomTreeModel.Children.Max(s => s.Id)) : navBomTreeModel.Id;
                    navBomTreeModel.Id = index++;
                    navProdBomTreeModel.Children.Add(navBomTreeModel);
                }
                else
                {
                    IterateChildTreeNodes(child.Children, navProdBomTree);
                }
            });
        }

        [HttpGet]
        [Route("GetItemnByNo")]
        public async Task<NavItemModel> GetItemnByNo(string company, string itemNo)
        {
            var reportItem = new NavItemModel();
            var context = new DataService(_config, company);
            var nquery = context.Context.ItemList.Where(i => i.No == itemNo);
            DataServiceQuery<ItemList> query = (DataServiceQuery<ItemList>)nquery;

            TaskFactory<IEnumerable<ItemList>> taskFactory = new TaskFactory<IEnumerable<ItemList>>();
            IEnumerable<ItemList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
            var item = result.FirstOrDefault();
            var repItem = _context.NavinpitemReport.FirstOrDefault(f => f.ItemNo == itemNo);
            if (item != null && repItem != null)
            {
                reportItem = new NavItemModel
                {
                    No = item.No,
                    Description = item.Description,
                    BatchNos = item.Batch_Nos,
                    BaseUnitofMeasure = item.Base_Unit_of_Measure,
                    Inventory = item.Inventory,
                    SafetyStock = 0,
                    // SafetyLeadTime = item.Safety_Lead_Time,
                    DemandQuantity = repItem?.DemandQuantity,
                    SupplyQuantity = repItem?.SupplyQuantity,
                    ItemId = repItem.InpreportId,
                };
            }
            return reportItem;
        }

        //[HttpGet]
        //[Route("GetBOMHeader")]
        //public async Task<IEnumerable<CRTSHF_ProductionBOMList>> GetBOMHeader()
        //{
        //    var nquery = Context.CRTSHF_ProductionBOMList.Where(r => r.Status == "Under Development");
        //    DataServiceQuery<CRTSHF_ProductionBOMList> query = (DataServiceQuery<CRTSHF_ProductionBOMList>)nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ProductionBOMList>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ProductionBOMList>>();
        //    IEnumerable<CRTSHF_ProductionBOMList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        //[HttpGet]
        //[Route("GetBOMLine")]
        //public async Task<IEnumerable<CRTSHF_ProdBOMLines>> GetBOMLine()
        //{
        //    var nquery = Context.CRTSHF_ProdBOMLines.Where(r => r.Status == "Under Development");
        //    DataServiceQuery<CRTSHF_ProdBOMLines> query = (DataServiceQuery<CRTSHF_ProdBOMLines>)nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ProdBOMLines>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ProdBOMLines>>();
        //    IEnumerable<CRTSHF_ProdBOMLines> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        //[HttpGet]
        //[Route("GetBOMVersion")]
        //public async Task<IEnumerable<CRTSHF_ProductionBOMVersion>> GetBOMVersion()
        //{
        //    var nquery = Context.CRTSHF_ProductionBOMVersion;
        //    DataServiceQuery<CRTSHF_ProductionBOMVersion> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ProductionBOMVersion>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ProductionBOMVersion>>();
        //    IEnumerable<CRTSHF_ProductionBOMVersion> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        [HttpGet]
        [Route("GetMachine")]
        public async Task<IEnumerable<MachineCenterList>> GetMachine(string company)
        {
            var context = new DataService(_config, company);
            var nquery = context.Context.MachineCenterList;
            DataServiceQuery<MachineCenterList> query = nquery;

            TaskFactory<IEnumerable<MachineCenterList>> taskFactory = new TaskFactory<IEnumerable<MachineCenterList>>();
            IEnumerable<MachineCenterList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


            return result;
        }
        //[HttpGet]
        //[Route("GetScrapCode")]
        //public async Task<IEnumerable<CRTSHF_ScrapCodes>> GetScrapCode()
        //{
        //    var nquery = Context.CRTSHF_ScrapCodes;
        //    DataServiceQuery<CRTSHF_ScrapCodes> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ScrapCodes>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ScrapCodes>>();
        //    IEnumerable<CRTSHF_ScrapCodes> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        //[HttpGet]
        //[Route("GetBOMType")]
        //public async Task<IEnumerable<CRTSHF_BOMTypes>> GetBOMType()
        //{
        //    var nquery = Context.CRTSHF_BOMTypes;
        //    DataServiceQuery<CRTSHF_BOMTypes> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_BOMTypes>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_BOMTypes>>();
        //    IEnumerable<CRTSHF_BOMTypes> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}
        //[HttpGet]
        //[Route("GetUOM")]
        //public async Task<IEnumerable<CRTSHF_UnitOfMeasures>> GetUOM()
        //{
        //    var nquery = Context.CRTSHF_UnitOfMeasures;
        //    DataServiceQuery<CRTSHF_UnitOfMeasures> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_UnitOfMeasures>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_UnitOfMeasures>>();
        //    IEnumerable<CRTSHF_UnitOfMeasures> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}

        //[HttpGet]
        //[Route("GetItemUOM")]
        //public async Task<IEnumerable<CRTSHF_ItemUnitOfMeasure>> GetItemUOM(string id)
        //{
        //    var nquery = Context.CRTSHF_ItemUnitOfMeasure;
        //    DataServiceQuery<CRTSHF_ItemUnitOfMeasure> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ItemUnitOfMeasure>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ItemUnitOfMeasure>>();
        //    IEnumerable<CRTSHF_ItemUnitOfMeasure> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar).Where(i => i.Item_No == id));


        //    return result;
        //}

        //[HttpGet]
        //[Route("GetPackSize")]
        //public async Task<IEnumerable<CRTSHF_PackSize>> GetPackSize()
        //{
        //    var nquery = Context.CRTSHF_PackSize;
        //    DataServiceQuery<CRTSHF_PackSize> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_PackSize>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_PackSize>>();
        //    IEnumerable<CRTSHF_PackSize> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));


        //    return result;
        //}

        //[HttpPost]
        //[Route("PostScrapJournal")]
        //public async Task<ActionResult<CRTSHF_ScrapItemJnlLine>> PostScrapJournal(BlisterScrapEntryModel value)
        //{
        //    try
        //    {
        //        var guid = Guid.NewGuid();
        //        int onlyThisAmount = 6;
        //        string ticks = DateTime.Now.Ticks.ToString();
        //        ticks = ticks.Substring(ticks.Length - onlyThisAmount);

        //        var model = new CRTSHF_ScrapItemJnlLine
        //        {
        //            Journal_Template_Name = "SW-" + ticks,
        //            Journal_Batch_Name = ticks,
        //            //Prod_Order_No = value.ProdOrderNo,
        //            App_GUID = guid,
        //            Batch_No = value.BatchNo,
        //            Item_Description = value.ItemDescription,
        //            // De_Blister_Weight = value.Weight,
        //            Line_No = int.Parse(ticks),
        //            Posting_Date = DateTime.Now,
        //            // Prod_Order_Line_No = value.ProdLineNo,
        //            Source_Item_No = value.SourceItemNo,
        //            //Scrap_Code = value.BlisterScrapType,
        //            Test_Run_Weight = value.Weight,
        //            Trimming_Weight = value.Weight,
        //            Others_Weight = value.Weight,
        //            Prod_Order_Status = "Finished",
        //            //Prod_Order_Status = "1"
        //        };

        //        Context.AddToCRTSHF_ScrapItemJnlLine(model);

        //        //var nquery = Context.CRTSHF_ScrapItemJnlLine;
        //        // Context.AddObject("CRTSHF_ScrapItemJnlLine", model);

        //        // DataServiceQuery<CRTSHF_ScrapItemJnlLine> query = nquery;


        //        //TaskFactory<CRTSHF_ScrapItemJnlLine> taskFactory1 = new TaskFactory<CRTSHF_ScrapItemJnlLine>();
        //        //CRTSHF_ScrapItemJnlLine result = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar).FirstOrDefault());
        //        //var first = result;

        //        //first.Prod_Order_No = model.Prod_Order_No;
        //        //first.Trimming_Weight = 1234;
        //        //Context.UpdateObject(first);
        //        //ICredentials credentials = null;
        //        //credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"]);
        //        //ODataClientSettings settings = new ODataClientSettings(new Uri($"{_config["NAV:OdataUrl"]}/Company(%27{_config["NAV:Company"]}%27)"), credentials);
        //        ////settings.IncludeAnnotationsInResults = true;
        //        ////settings.PayloadFormat = ODataPayloadFormat.Atom;
        //        //settings.IgnoreUnmappedProperties = true;
        //        //settings.BeforeRequest = BeforeRequest;

        //        //var client = new ODataClient(settings);

        //        //var journal = await  client.For<Company>("Company").Key(_config["NAV:Company"]).NavigateTo("CRTSHF_ScrapItemJnlLine").Set(model).InsertEntryAsync();

        //        var entity = Context.Entities;



        //        TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
        //        var response = await taskFactory.FromAsync(Context.BeginSaveChanges(null, null), iar => Context.EndSaveChanges(iar));

        //        return Ok(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message);
        //    }
        //}
        //[HttpPost]
        //[Route("PostScrapEntry")]
        //public async Task<FnPost_Result> PostScrapEntry(CRTSHF_ScrapItemJnlLine value)
        //{

        //    var post = new NAVSoapService.CRTSHF_PostScrapJournal_PortClient();
        //    var fnobject = Guid.NewGuid().ToString();
        //    return await post.FnPostAsync(fnobject);
        //}


        //[HttpPost]
        //[Route("PostProdBOM")]
        //public async Task<ActionResult<CRTSHF_ProductionBOMList>> PostProdBOM(BlisterScrapMasterModel value)
        //{
        //    try
        //    {
        //        var guid = Guid.NewGuid();
        //        int onlyThisAmount = 6;
        //        string ticks = DateTime.Now.Ticks.ToString();
        //        ticks = ticks.Substring(ticks.Length - onlyThisAmount);
        //        var Entry_No = int.Parse(ticks);

        //        var model = new CRTSHF_ProductionBOMList
        //        {
        //            No = value.BlisterComponent,
        //            Description = value.Description,
        //            Description_2 = value.Description1,
        //            BOM_Type = value.Bomtype,
        //            Batch_Size = value.BatchSize,
        //            //Pack_Size = value.PackSize,
        //            Unit_of_Measure_Code = value.Uom,
        //            Scrap_Unit_of_Measure = value.ScrapUom,
        //            Status = "New",
        //            Sub_Lot = int.Parse(value.Sublot),

        //        };

        //        var nquery = Context.CRTSHF_ProductionBOMList;
        //        DataServiceQuery<CRTSHF_ProductionBOMList> query = nquery;
        //        TaskFactory<IEnumerable<CRTSHF_ProductionBOMList>> taskFactory1 = new TaskFactory<IEnumerable<CRTSHF_ProductionBOMList>>();
        //        IEnumerable<CRTSHF_ProductionBOMList> result = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar).Where(b => b.No == value.BlisterComponent));

        //        if (result.Count() == 0)
        //        {
        //            Context.AddToCRTSHF_ProductionBOMList(model);

        //            value.BlisterScrapLine.ToList().ForEach(l =>
        //            {
        //                ticks = ticks.Substring(ticks.Length - onlyThisAmount);
        //                Entry_No = int.Parse(ticks);
        //                var bomline = new CRTSHF_ProdBOMLines
        //                {
        //                    Type = l.Type,
        //                    No = l.ProdOrderNo,
        //                    Description = l.Description,
        //                    Description_2 = l.Description,
        //                    Calculation_Type = "Standard",
        //                    Add_on_Quantity = l.QtyPer,
        //                    Quantity_per = l.QtyPer,
        //                    Unit_of_Measure_Code = l.Uom,
        //                    Scrap_Percent = l.ScrapPercentage,
        //                    Scrap_Percentage_Percent = l.ScrapPercentage,
        //                    Inheritance = l.Inheritance,
        //                    QC_Inheritance = l.QcInheritance,
        //                    Line_No = Entry_No,
        //                    Production_BOM_No = value.BlisterComponent,

        //                };
        //                Context.AddToCRTSHF_ProdBOMLines(bomline);
        //            });
        //        }

        //        TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
        //        var response = await taskFactory.FromAsync(Context.BeginSaveChanges(null, null), iar => Context.EndSaveChanges(iar));

        //        return model;
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message);
        //    }

        //}

        //[HttpGet]
        //[Route("GetScrapCodeSync")]
        //public async Task<IEnumerable<BlisterScrapCodeModel>> GetScrapCodeSync()
        //{
        //    var nquery = Context.CRTSHF_ScrapCodes;
        //    DataServiceQuery<CRTSHF_ScrapCodes> query = nquery;

        //    TaskFactory<IEnumerable<CRTSHF_ScrapCodes>> taskFactory = new TaskFactory<IEnumerable<CRTSHF_ScrapCodes>>();
        //    IEnumerable<CRTSHF_ScrapCodes> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

        //    result.ToList().ForEach(s =>
        //    {
        //        var exist = _context.BlisterScrapCode.FirstOrDefault(c => c.Name == s.Code);
        //        if (exist == null)
        //        {
        //            var scrapcode = new BlisterScrapCode
        //            {
        //                Name = s.Code,
        //                Description = s.Description,
        //                AddedByUserId = 2,
        //                AddedDate = DateTime.Now,
        //            };
        //            _context.BlisterScrapCode.Add(scrapcode);
        //        }
        //        else
        //        {
        //            exist.Description = s.Description;
        //            exist.ModifiedByUserId = 2;
        //            exist.ModifiedDate = DateTime.Now;
        //            _context.SaveChanges();
        //        }
        //    });
        //    _context.SaveChanges();

        //    var blisterScrapCode = _context.BlisterScrapCode.Include("AddedByUser").Include("ModifiedByUser").Select(s => new BlisterScrapCodeModel
        //    {
        //        ScrapCodeID = s.ScrapCodeId,
        //        Name = s.Name,
        //        Description = s.Description,
        //        StatusCodeID = s.StatusCodeId,
        //        AddedByUserID = s.AddedByUserId,
        //        ModifiedByUserID = s.ModifiedByUserId,
        //        AddedByUser = s.AddedByUser.UserName,
        //        ModifiedByUser = s.ModifiedByUser.UserName,
        //        AddedDate = s.AddedDate,
        //        ModifiedDate = s.ModifiedDate

        //    }).OrderByDescending(o => o.ScrapCodeID).ToList();
        //    return blisterScrapCode;


        //}

        [HttpGet]
        [Route("GetNAVProdOrderLineList")]
        public List<NavprodOrderLineModel> GetNAVProdOrderLineList(string company)

        {
            List<NavprodOrderLineModel> navprodOrderLineModels = new List<NavprodOrderLineModel>();
         var productionLinelist = _context.NavprodOrderLine.Include(t=>t.Company).ToList();
            if(productionLinelist!=null && productionLinelist.Count>0)
            {
                productionLinelist.ForEach(p => {
                    NavprodOrderLineModel navprodOrderLineModel = new NavprodOrderLineModel();
                    navprodOrderLineModel.ProdOrderNo = p.ProdOrderNo;
                    navprodOrderLineModel.ItemNo = p.ItemNo;
                    navprodOrderLineModel.StartDate = p.StartDate;
                    navprodOrderLineModel.Status = p.Status;
                    navprodOrderLineModel.BatchNo = p.BatchNo;
                    navprodOrderLineModel.RePlanRefNo = p.RePlanRefNo;
                    navprodOrderLineModel.CompletionDate = p.CompletionDate;
                    navprodOrderLineModel.Description = p.Description;
                    navprodOrderLineModel.Description1 =   p.Description1;
                    navprodOrderLineModel.CompanyName = p.Company?.PlantCode;
                    navprodOrderLineModels.Add(navprodOrderLineModel);

                });
            }
            return navprodOrderLineModels;
        }

    
         [HttpGet]
        [Route("GetNAVProdOrderLine")]
        public async Task<IEnumerable<NAV.ProdOrderLineList>> GetNAVProdOrderLine(long? id)
        {

            var companyId = 1;

            //var exisdatas = _context.ProdOrderNotStart.Where(f => f.CompanyId == companyId).AsNoTracking().ToList();
            //await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM NAVProdOrderLine WHERE CompanyId = {companyId}");
            var plant = "";
            if(id == 1)
            {
               plant= "Nav_JB";
            }
            if (id == 2)
            {
                plant="Nav_SG";
                companyId = 2;
            }

            var fromMonth = DateTime.Today;
            var year = fromMonth.Year - 1;
            var tomonth = DateTime.Today.AddMonths(6);
            var context = new DataService(_config, plant);
            int pageSize = 1000;
            int page = 0;
            var productionLinelist = _context.NavprodOrderLine.Where(w => w.CompanyId == companyId).Select(t=> new {t.ProdOrderNo,t.RePlanRefNo,t.CompanyId}).ToList();
            

            while (true)
            {
                var nquery = context.Context.ProdOrderLineList.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<NAV.ProdOrderLineList> query = (DataServiceQuery<NAV.ProdOrderLineList>)nquery;

                TaskFactory<IEnumerable<NAV.ProdOrderLineList>> taskFactory = new TaskFactory<IEnumerable<NAV.ProdOrderLineList>>();
                IEnumerable<NAV.ProdOrderLineList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                var prodCodes = result.ToList();
                var prodNotStartList = new List<NavprodOrderLine>();
                prodCodes.ForEach(f =>
                {
                    if (f.Line_No > 0)
                    {
                        string refNo = String.Empty;
                        if (!string.IsNullOrEmpty(f.Prod_Order_No))
                        {
                            var refPlanNo = f.Prod_Order_No.Split("-");

                            if (refPlanNo.Length == 1)
                            {
                                refNo = refPlanNo[0];
                            }
                            else
                            {
                                refNo = refPlanNo[0] + "-" + refPlanNo[1];
                            }
                        }
                        var exist = productionLinelist.Where(p => p.RePlanRefNo == refNo && p.CompanyId == companyId && p.ProdOrderNo == f.Prod_Order_No).FirstOrDefault();
                        if (exist == null)
                        {
                            var prodNotStart = new NavprodOrderLine
                            {
                                RePlanRefNo = refNo,
                                CompanyId = companyId,
                                ProdOrderNo = f.Prod_Order_No,
                                OrderLineNo = f.Line_No,
                                ItemNo = f.Item_No,
                                Description = f.Description,
                                Description1 = f.Description_2,
                                CompletionDate = f.Completion_Date == DateTime.MinValue ? null : f.Completion_Date,
                                RemainingQuantity = f.Remaining_Quantity,
                                BatchNo = f.Batch_No,
                                Status = f.Status,
                                OutputQty = f.Finished_Quantity,
                                StartDate = f.Starting_Date == DateTime.MinValue ? null : f.Starting_Date,
                                LastSyncDate = DateTime.Now,
                            };

                            prodNotStartList.Add(prodNotStart);
                        }
                    }
                });


                SqlBulkUpload obj = new SqlBulkUpload(_context, _config);
                await obj.BulkInsertAsync(prodNotStartList, "NAVProdOrderLine");

                if (prodCodes.Count < 1000)
                    break;
                page++;
            }

            return null;
        }

    }
}