﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.SearchParam;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FPManufacturingRecipeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public FPManufacturingRecipeController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetFPManufacturingRecipes")]
        public List<FPManufacturingRecipeModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpmanufacturingRecipe = _context.FpmanufacturingRecipe.Include(f => f.FinishProductGeneralInfo).Include(c => c.FinishProductGeneralInfo.FinishProduct).Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedUser).AsNoTracking().ToList();
            List<FPManufacturingRecipeModel> fPManufacturingRecipeModel = new List<FPManufacturingRecipeModel>();
            fpmanufacturingRecipe.ForEach(s =>
            {
                FPManufacturingRecipeModel fPManufacturingRecipeModels = new FPManufacturingRecipeModel
                {
                    FPManufacturingRecipeID = s.FpmanufacturingRecipeId,
                    ManufacturingSiteID = s.ManufacturingSiteId,
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    IsMaster = s.IsMaster,
                    ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedUser != null ? s.ModifiedUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    CustomerProductName = s.FinishProductGeneralInfo != null && s.FinishProductGeneralInfo.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId).Value : string.Empty,
                    RegisterCountry = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                    RegisterHolderName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                };
                fPManufacturingRecipeModel.Add(fPManufacturingRecipeModels);
            });
            if (fPManufacturingRecipeModel != null && fPManufacturingRecipeModel.Count > 0)
            {
                fPManufacturingRecipeModel.ForEach(h =>
                {
                    h.ProductName = h.ManufacturingSite + " | " + h.RegisterCountry + " | " + h.RegisterHolderName + " | " + h.CustomerProductName;
                });
            }
            //var result = _mapper.Map<List<FPManufacturingRecipeModel>>(FPManufacturingRecipe);
            return fPManufacturingRecipeModel.OrderByDescending(o => o.FPManufacturingRecipeID).ToList();
        }
        [HttpPost]
        [Route("GetFPManufacturingRecipesByRefNo")]
        public List<FPManufacturingRecipeModel> GetFPManufacturingRecipesByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpmanufacturingRecipe = _context.FpmanufacturingRecipe.Include(f => f.FinishProductGeneralInfo).Include(c => c.FinishProductGeneralInfo.FinishProduct).Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedUser).AsNoTracking().ToList();
            List<FPManufacturingRecipeModel> fPManufacturingRecipeModel = new List<FPManufacturingRecipeModel>();
            fpmanufacturingRecipe.ForEach(s =>
            {
                FPManufacturingRecipeModel fPManufacturingRecipeModels = new FPManufacturingRecipeModel
                {
                    FPManufacturingRecipeID = s.FpmanufacturingRecipeId,
                    ManufacturingSiteID = s.ManufacturingSiteId,
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    IsMaster = s.IsMaster,
                    Master = s.IsMaster == true ? "Master" : "Not-Master",
                    ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedUser != null ? s.ModifiedUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    CustomerProductName = s.FinishProductGeneralInfo != null && s.FinishProductGeneralInfo.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId).Value : string.Empty,
                    RegisterCountry = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                    RegisterHolderName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                    ProductOwner = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId).Value : string.Empty,
                    PRHSpecificName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId).Value : string.Empty,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                };
                fPManufacturingRecipeModel.Add(fPManufacturingRecipeModels);
            });
            if (fPManufacturingRecipeModel != null && fPManufacturingRecipeModel.Count > 0)
            {
                fPManufacturingRecipeModel.ForEach(h =>
                {
                    h.ProductName = h.ManufacturingSite + " | " + h.RegisterCountry + " | " + h.RegisterHolderName + " | " + h.CustomerProductName + " | " + h.PRHSpecificName + " | " + h.Master;
                });
            }
            if (refSearchModel.IsHeader)
            {
                return fPManufacturingRecipeModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPManufacturingRecipeID).ToList();
            }
            return fPManufacturingRecipeModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPManufacturingRecipeID).ToList();
        }

        [HttpPost]
        [Route("GetManufacturingRecipesForBOMByRefNo")]
        public List<FPManufacturingRecipeModel> GetManufacturingRecipesForBOMByRefNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpmanufacturingRecipe = _context.FpmanufacturingRecipe.Include(f => f.FinishProductGeneralInfo).Include(c => c.FinishProductGeneralInfo.FinishProduct).Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedUser).AsNoTracking().ToList();
            List<FPManufacturingRecipeModel> fPManufacturingRecipeModel = new List<FPManufacturingRecipeModel>();
            fpmanufacturingRecipe.ForEach(s =>
            {
                FPManufacturingRecipeModel fPManufacturingRecipeModels = new FPManufacturingRecipeModel
                {
                    FPManufacturingRecipeID = s.FpmanufacturingRecipeId,
                    ManufacturingSiteID = s.ManufacturingSiteId,
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                    CustomerProductName = s.FinishProductGeneralInfo != null && s.FinishProductGeneralInfo.FinishProduct != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId).Value : string.Empty,
                    RegisterCountry = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                    RegisterHolderName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                    ProductOwner = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId).Value : string.Empty,
                    PRHSpecificName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId).Value : string.Empty,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                };
                fPManufacturingRecipeModel.Add(fPManufacturingRecipeModels);
            });
            if (fPManufacturingRecipeModel != null && fPManufacturingRecipeModel.Count > 0)
            {
                fPManufacturingRecipeModel.ForEach(h =>
                {
                    h.ProductName = h.ManufacturingSite + " | " + h.RegisterCountry + " | " + h.RegisterHolderName + " | " + h.ProductOwner + " | " + h.PRHSpecificName;
                });
            }
            if (refSearchModel.IsHeader)
            {
                return fPManufacturingRecipeModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPManufacturingRecipeID).ToList();
            }
            return fPManufacturingRecipeModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.FPManufacturingRecipeID).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPManufacturingRecipeModel> GetData(SearchModel searchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var fpmanufacturingRecipe = new FpmanufacturingRecipe();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).FirstOrDefault();
                        break;
                    case "Last":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).LastOrDefault();
                        break;
                    case "Next":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).LastOrDefault();
                        break;
                    case "Previous":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).FirstOrDefault();
                        break;
                    case "Last":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).LastOrDefault();
                        break;
                    case "Next":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderBy(o => o.FpmanufacturingRecipeId).FirstOrDefault(s => s.FpmanufacturingRecipeId > searchModel.Id);
                        break;
                    case "Previous":
                        fpmanufacturingRecipe = _context.FpmanufacturingRecipe.OrderByDescending(o => o.FpmanufacturingRecipeId).FirstOrDefault(s => s.FpmanufacturingRecipeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPManufacturingRecipeModel>(fpmanufacturingRecipe);
            if (result != null)
            {
                if (result.ManufacturingSiteID > 0)
                {
                    result.ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == result.ManufacturingSiteID).Select(a => a.Value).SingleOrDefault() : "";
                }

            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFPManufacturingRecipe")]
        public FPManufacturingRecipeModel Post(FPManufacturingRecipeModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var fpmanufacturingRecipe = new FpmanufacturingRecipe
            {

                ManufacturingSiteId = value.ManufacturingSiteID,
                FinishProductGeneralInfoId = value.FinishProductGeneralInfoID,
                IsMaster = value.IsMaster,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.FpmanufacturingRecipe.Add(fpmanufacturingRecipe);
            _context.SaveChanges();
            value.FPManufacturingRecipeID = fpmanufacturingRecipe.FpmanufacturingRecipeId;
            value.ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ManufacturingSiteID).Select(a => a.Value).SingleOrDefault() : "";


            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFPManufacturingRecipe")]
        public FPManufacturingRecipeModel Put(FPManufacturingRecipeModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var fpmanufacturingRecipe = _context.FpmanufacturingRecipe.SingleOrDefault(p => p.FpmanufacturingRecipeId == value.FPManufacturingRecipeID);
            fpmanufacturingRecipe.ManufacturingSiteId = value.ManufacturingSiteID;
            fpmanufacturingRecipe.FinishProductGeneralInfoId = value.FinishProductGeneralInfoID;
            fpmanufacturingRecipe.IsMaster = value.IsMaster;
            fpmanufacturingRecipe.ModifiedUserId = value.ModifiedByUserID;
            fpmanufacturingRecipe.ModifiedDate = DateTime.Now;
            fpmanufacturingRecipe.StatusCodeId = value.StatusCodeID;

            _context.SaveChanges();
            value.ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ManufacturingSiteID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFPManufacturingRecipe")]
        public void Delete(int id)
        {
            var FPManufacturingRecipe = _context.FpmanufacturingRecipe.SingleOrDefault(p => p.FpmanufacturingRecipeId == id);
            if (FPManufacturingRecipe != null)
            {
                _context.FpmanufacturingRecipe.Remove(FPManufacturingRecipe);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("GenerateFPManufacturingRecipes")]
        public List<FPManufacturingRecipeModel> GenerateFPManufacturingRecipes(ManufacturingRecipeParam manufacturingRecipeParam)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var applicationmasterId = _context.ApplicationMaster.FirstOrDefault(m => m.ApplicationMasterCodeId == 126).ApplicationMasterId;
            var fPCommonFieldLines = _context.FpcommonFieldLine.Include(f => f.FpcommonFiled).Where(s => s.FpcommonFiledId == manufacturingRecipeParam.ProductID).AsNoTracking().ToList();
            List<FinishProductLineModel> FinishProductLineModel = new List<FinishProductLineModel>();
            fPCommonFieldLines.ForEach(s =>
            {
                FinishProductLineModel FinishProductLineModels = new FinishProductLineModel
                {
                    MaterialId = s.MaterialNoId,
                    MaterialName = s.MaterialName,
                    DosageForm = s.DosageNo,
                    Uom = s.DosageUnitId,
                    PerDosage = s.PerDosageId,
                    Overage = s.Overage == "0" ? string.Empty : s.Overage,
                    DosageUnits = s.OverageDosageUnitsId,
                    OverageUnits = s.OverageUnitsId,
                    DosageFormID = s.FpcommonFiled != null ? s.FpcommonFiled.DosageFormId : 0
                };
                FinishProductLineModel.Add(FinishProductLineModels);
            });
            var itemIds = FinishProductLineModel.Where(line => line.DosageFormID != null && line.DosageFormID == manufacturingRecipeParam.DosageFormID).Select(m => m.MaterialId).ToList();
            var materialIDs = _context.ItemClassificationHeader.Where(h => itemIds.Contains(h.ItemClassificationHeaderId)).AsNoTracking().Select(mat => mat.MaterialId).ToList();
            var query = _context.FinishProductLine.Where(c => materialIDs.Contains(c.MaterialId.Value)).AsNoTracking().ToList();

            List<FinishProductLine> finishProductLines = new List<FinishProductLine>();

            query.ForEach(fpl =>
            {
                FinishProductLineModel.ForEach(cl =>
                {
                    if ((fpl.DosageForm != null && fpl.DosageForm == cl.DosageForm) && (fpl.Uom != null && fpl.Uom == cl.Uom) && (fpl.PerDosage != null && fpl.PerDosage == cl.PerDosage))
                    {
                        fpl.Overage = fpl.Overage == "0" ? string.Empty : fpl.Overage;
                        if ((!string.IsNullOrEmpty(fpl.Overage) && fpl.Overage == cl.Overage) || (fpl.DosageUnits != null && fpl.DosageUnits == cl.DosageUnits) || (fpl.OverageUnits != null && fpl.OverageUnits == cl.OverageUnits))
                        {
                            if (!finishProductLines.Any(f => f.FinishProductLineId == fpl.FinishProductLineId))
                            {
                                finishProductLines.Add(fpl);
                            }
                        }
                        else if (string.IsNullOrEmpty(fpl.Overage) && fpl.DosageUnits == null && fpl.OverageUnits == null)
                        {
                            if (!finishProductLines.Any(f => f.FinishProductLineId == fpl.FinishProductLineId))
                            {
                                finishProductLines.Add(fpl);
                            }
                        }
                    }
                });
            });

            var finishProductIds = finishProductLines.Select(s => s.FinishProductId).ToList();
            var finishProducts = _context.FinishProduct.Include(f => f.FinishProductLine).AsNoTracking().ToList();
            List<Product> products = new List<Product>();
            //List<FPManufacturingRecipeModel> fpManufacturingRecipeModels = new List<FPManufacturingRecipeModel>();

            var fPManufacturingRecipeModels = _context.FinishProductGeneralInfo.Include(f => f.FinishProduct).AsNoTracking().ToList();
            List<FPManufacturingRecipeModel> fPManufacturingRecipeModel = new List<FPManufacturingRecipeModel>();
            fPManufacturingRecipeModels.ForEach(g =>
            {
                FPManufacturingRecipeModel FPManufacturingRecipeModels = new FPManufacturingRecipeModel
                {
                    FinishProductId = g.FinishProductId,
                    ManufacturingSiteID = g.FinishProduct != null ? g.FinishProduct.ManufacturingSiteId : null,
                    FinishProductGeneralInfoID = g.FinishProductGeneralInfoId,
                    ProductName = g.FinishProduct != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == g.FinishProduct.ProductId).Value : "",
                };
                fPManufacturingRecipeModel.Add(FPManufacturingRecipeModels);
            });
            fPManufacturingRecipeModel = fPManufacturingRecipeModel.Where(r => finishProductIds.Contains(r.FinishProductId)).ToList();

            List<FpmanufacturingRecipe> fpmanufacturingRecipes = new List<FpmanufacturingRecipe>();
            var ManufacturingSiteIDs = fPManufacturingRecipeModel.Select(m => m.ManufacturingSiteID).ToList();
            var FinishProductGeneralInfoIDs = fPManufacturingRecipeModel.Select(m => m.FinishProductGeneralInfoID).ToList();
            var existingRecipes = _context.FpmanufacturingRecipe.Where(c => (ManufacturingSiteIDs.Contains(c.ManufacturingSiteId) && FinishProductGeneralInfoIDs.Contains(c.FinishProductGeneralInfoId)));

            var ExistingManufacturingSiteIDs = existingRecipes.Select(m => m.ManufacturingSiteId).ToList();
            var ExistingFinishProductGeneralInfoIDs = existingRecipes.Select(m => m.FinishProductGeneralInfoId).ToList();

            var notExistManufacturingModels = fPManufacturingRecipeModel.Where(r => !(ExistingManufacturingSiteIDs.Contains(r.ManufacturingSiteID) && ExistingFinishProductGeneralInfoIDs.Contains(r.FinishProductGeneralInfoID))).ToList();
            existingRecipes.ToList().ForEach(f =>
            {
                string profileNo = string.Empty;
                if (manufacturingRecipeParam.ProfileID != null)
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = manufacturingRecipeParam.ProfileID ,AddedByUserID = manufacturingRecipeParam.UserID,StatusCodeID=710, Title=manufacturingRecipeParam.ProductName});
                }
                f.ProfileReferenceNo = profileNo;
                f.LinkProfileReferenceNo = manufacturingRecipeParam.LinkProfileReferenceNo;
                f.MasterProfileReferenceNo = string.IsNullOrEmpty(manufacturingRecipeParam.MasterProfileReferenceNo) ? profileNo : manufacturingRecipeParam.MasterProfileReferenceNo;
            });


            notExistManufacturingModels.ForEach(m =>
             {
                 string profileNo = string.Empty;
                 if (manufacturingRecipeParam.ProfileID != null)
                 {
                     profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = manufacturingRecipeParam.ProfileID, AddedByUserID = manufacturingRecipeParam.UserID, StatusCodeID=710, Title =manufacturingRecipeParam.ProductName });
                 }
                 FpmanufacturingRecipe fpmanufacturingRecipe = new FpmanufacturingRecipe
                 {
                     ManufacturingSiteId = m.ManufacturingSiteID,
                     FinishProductGeneralInfoId = m.FinishProductGeneralInfoID,
                     ItemClassificationHeaderId = manufacturingRecipeParam.SiteID,
                     IsMaster = false,
                     ProfileReferenceNo = profileNo,
                     LinkProfileReferenceNo = manufacturingRecipeParam.LinkProfileReferenceNo,
                     AddedDate = DateTime.Now,
                     AddedByUserId=manufacturingRecipeParam.UserID,
                     MasterProfileReferenceNo = string.IsNullOrEmpty(manufacturingRecipeParam.MasterProfileReferenceNo) ? profileNo : manufacturingRecipeParam.MasterProfileReferenceNo,
                 };
                 fpmanufacturingRecipes.Add(fpmanufacturingRecipe);
             });
            _context.FpmanufacturingRecipe.AddRange(fpmanufacturingRecipes);
            _context.SaveChanges();
            var result = new List<FPManufacturingRecipeModel>();
            var generalInfos = _context.FpmanufacturingRecipe.Include("FinishProductGeneralInfo.FinishProduct").Where(r => FinishProductGeneralInfoIDs.Contains(r.FinishProductGeneralInfoId)).AsNoTracking().ToList();

            generalInfos.ForEach(f =>
            {
                result.Add(new FPManufacturingRecipeModel
                {
                    ManufacturingSiteID = f.ManufacturingSiteId,
                    FinishProductGeneralInfoID = f.FinishProductGeneralInfoId,
                    RegisterHolderName = f.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.ProductRegistrationHolderId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.ProductRegistrationHolderId).Value : string.Empty,
                    ManufacturingSite = f.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId).Value : string.Empty,
                    RegisterCountry = f.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.RegisterCountry) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.RegisterCountry).Value : string.Empty,
                    ProductName = f.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.FinishProduct.ProductId).Value : string.Empty,
                    PRHSpecificProductName = f.FinishProductGeneralInfo != null && applicationmasterdetail != null && applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.PrhspecificProductId) != null ? applicationmasterdetail.SingleOrDefault(a => a.ApplicationMasterDetailId == f.FinishProductGeneralInfo.PrhspecificProductId).Value : string.Empty,
                    FPManufacturingRecipeID = f.FpmanufacturingRecipeId,
                    IsMaster = f.IsMaster,
                    MasterProfileReferenceNo = f.MasterProfileReferenceNo,
                    ProfileReferenceNo = f.ProfileReferenceNo,
                    LinkProfileReferenceNo = f.LinkProfileReferenceNo,
                    AddedDate =f.AddedDate,
                    AddedByUserID = f.AddedByUserId,
                });

            });
            return result;
        }

        [HttpPut]
        [Route("UpdateFPManufacturingRecipeMaster")]
        public FPManufacturingRecipeModel UpdateFPManufacturingRecipeMaster(FPManufacturingRecipeModel value)
        {
            var fpmanufacturingRecipe = _context.FpmanufacturingRecipe.SingleOrDefault(p => p.FpmanufacturingRecipeId == value.FPManufacturingRecipeID);
            fpmanufacturingRecipe.IsMaster = value.IsMaster;
            _context.SaveChanges();
            return value;
        }
    }

    public class Product
    {
        public long? ProductID { get; set; }
        public string ProductName { get; set; }
    }
}