﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyCalendarController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CompanyCalendarController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetCompanyCalendarByEvent")]
        public List<CompanyCalendarModel> Get(DateTime StartDate, DateTime EndDate)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            EndDate = EndDate.Date.AddDays(15);
            var companyCalendar = _context.CompanyCalendar.Where(w => w.CalendarEventDate.Value.Date >= StartDate.Date && w.CalendarEventDate.Value.Date <= EndDate.Date)
                .AsNoTracking().ToList();
            List<CompanyCalendarModel> CompanyCalendarModels = new List<CompanyCalendarModel>();
            List<DateTime> lists = new List<DateTime>();
            companyCalendar.ForEach(s =>
            {
                CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                {
                    CompanyCalendarId = s.CompanyCalendarId,
                    CalenderEventId = s.CalenderEventId,
                    CalendarEventDate = s.CalendarEventDate,
                    CalenderEventName = s.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "",
                };
                lists.Add(s.CalendarEventDate.Value.Date);
                CompanyCalendarModels.Add(CompanyCalendarModel);
            });
            List<CompanyCalendarModel> CompanyCalendarModellists = new List<CompanyCalendarModel>();
            if (lists.Count > 0)
            {
                lists.ForEach(h =>
                {
                    CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                    {
                        Start = h.Date,
                        Title = CompanyCalendarModels != null ? string.Join(",", CompanyCalendarModels.Where(b => b.CalendarEventDate.Value.Date == h).Select(s => s.CalenderEventName).ToList()) : "",
                    };
                    CompanyCalendarModellists.Add(CompanyCalendarModel);
                });
            }
            return CompanyCalendarModellists.ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyCalendarModel> GetData(SearchModel searchModel)
        {
            var companyCalendarModel = new CompanyCalendar();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).FirstOrDefault();
                        break;
                    case "Last":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).LastOrDefault();
                        break;
                    case "Next":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).LastOrDefault();
                        break;
                    case "Previous":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).FirstOrDefault();
                        break;
                    case "Last":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).LastOrDefault();
                        break;
                    case "Next":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderBy(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).FirstOrDefault(s => s.CompanyCalendarId > searchModel.Id);
                        break;
                    case "Previous":
                        companyCalendarModel = _context.CompanyCalendar.Include(a => a.CompanyCalendarAssistance).OrderByDescending(o => o.CompanyCalendarId).Where(a => a.AddedByUserId == searchModel.UserID).FirstOrDefault(s => s.CompanyCalendarId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyCalendarModel>(companyCalendarModel);
            if (result != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
                var assistanceFormIds = companyCalendarModel.CompanyCalendarAssistance != null ? companyCalendarModel.CompanyCalendarAssistance.Select(q => q.UserId).ToList() : new List<long?>();
                result.RequireAssistanceFlag = result.RequireAssistance == true ? "Yes" : "No";
                result.AssistanceFromIds = assistanceFormIds;
                result.AssistanceFrom = assistanceFormIds != null && appUsers != null ? string.Join(",", appUsers.Where(w => assistanceFormIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "";
            }
            return result;
        }
        [HttpGet]
        [Route("GetCompanyCalendarEvents")]
        public List<CompanyCalendarLineModel> GetCompanyCalendarEvents(long? id)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 278, 279, 280, 281, 282, 283 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var companyCalendarLines = _context.CompanyCalendarLine
                                        .Include(c => c.CompanyCalendar)
                                        .Include(c => c.Vendor)
                                        .Include(c => c.CompanyCalendar.Company)
                                        .Include(m => m.Machine)
                                        .Include(m => m.CompanyCalendarLineLocation).AsNoTracking().ToList();
            List<CompanyCalendarLineModel> companyCalendarLineModels = new List<CompanyCalendarLineModel>();
            companyCalendarLines.ForEach(s =>
            {
                CompanyCalendarLineModel companyCalendarLineModel = new CompanyCalendarLineModel();
                companyCalendarLineModel.Id = s.CompanyCalendarLineId;
                companyCalendarLineModel.CompanyCalendarId = s.CompanyCalendarId;
                companyCalendarLineModel.CompanyCalendarLineId = s.CompanyCalendarLineId;
                companyCalendarLineModel.CompanyName = s.CompanyCalendar?.Company?.PlantCode;
                companyCalendarLineModel.EventDate = s.EventDate;
                companyCalendarLineModel.Start = s.EventDate;
                companyCalendarLineModel.End = s.EventDate;
                companyCalendarLineModel.CompanyId = s.CompanyCalendar.CompanyId;
                companyCalendarLineModel.CalenderEventId = s.CompanyCalendar.CalenderEventId;
                companyCalendarLineModel.FromTime = s.FromTime;
                companyCalendarLineModel.ToTime = s.ToTime;
                companyCalendarLineModel.LinkDocComment = s.LinkDocComment;
                var locations = _context.Ictmaster.Where(i => i.MasterType == 572).ToList();
                companyCalendarLineModel.Notes = s.Notes;
                companyCalendarLineModel.CalenderEventName = s.CompanyCalendar.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendar.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "";

                if (companyCalendarLineModel.CalenderEventName == "Yearly Holiday")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "green";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.IsYearly = true;
                }
                if (companyCalendarLineModel.CalenderEventName == "External Service")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "blue";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.VendorId = s.VendorId;
                    companyCalendarLineModel.VendorName = s.Vendor?.VendorName;
                    companyCalendarLineModel.IsService = true;
                }
                if (companyCalendarLineModel.CalenderEventName == "In house Service Machine related")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "yellow";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.MachineId = s.MachineId;
                    companyCalendarLineModel.MachineName = s.Machine?.NameOfTheMachine;
                    companyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                    companyCalendarLineModel.IsMachine = true;
                }
                if (companyCalendarLineModel.CalenderEventName == "In house Service location related")
                {
                    companyCalendarLineModel.TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "";
                    companyCalendarLineModel.cssClass = "grey";
                    companyCalendarLineModel.Title = companyCalendarLineModel.TypeOfServiceName + " | " + s.CompanyCalendar?.Company.PlantCode;
                    companyCalendarLineModel.LocationIds = s.CompanyCalendarLineLocation != null ? s.CompanyCalendarLineLocation.Where(w => w.CompanyCalendarLineId == s.CompanyCalendarLineId).Select(a => a.LocationId).ToList() : new List<long?>();
                    companyCalendarLineModel.LocationName = locations.Count > 0 ? string.Join(',', locations.Where(l => companyCalendarLineModel.LocationIds.Contains(l.IctmasterId)).Select(c => c.Name)) : "";
                    companyCalendarLineModel.NoOfHoursRequire = s.NoOfHoursRequire;
                    companyCalendarLineModel.IsLocation = true;
                }
                if (companyCalendarLineModel.LinkDocComment != "" && companyCalendarLineModel.LinkDocComment != null)
                {
                    companyCalendarLineModel.IsLinkDocument = true;
                }
                companyCalendarLineModels.Add(companyCalendarLineModel);
            });
            companyCalendarLineModels = companyCalendarLineModels.ToList();
            if (id > 0)
            {
                companyCalendarLineModels = companyCalendarLineModels.Where(w => w.CompanyId == id).ToList();
            }
            return companyCalendarLineModels.ToList();
        }


        [HttpGet]
        [Route("GetCompanyCalendar")]
        public List<CompanyCalendarModel> Get(DateTime? CalenderDate)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var companyCalendar = _context.CompanyCalendar
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(z => z.OnBehalf)
                .Include(a => a.CompanyCalendarAssistance)
                .Include(p => p.Site).Where(w => w.CalendarEventDate.Value.Date == CalenderDate.Value.Date)
                .AsNoTracking().ToList();
            List<CompanyCalendarModel> CompanyCalendarModels = new List<CompanyCalendarModel>();
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            companyCalendar.ForEach(s =>
            {
                var assistanceFormIds = s.CompanyCalendarAssistance != null ? s.CompanyCalendarAssistance.Select(q => q.UserId).ToList() : new List<long?>();
                CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                {
                    CompanyCalendarId = s.CompanyCalendarId,
                    CalendarEventDate = s.CalendarEventDate,
                    CalenderEventId = s.CalenderEventId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId = s.SessionId,
                    CalenderEventName = s.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "",
                    SiteName = s.Site != null ? s.Site.Name : "",
                    CompanyName = s.Company != null ? s.Company.Description : "",
                    CompanyId = s.CompanyId,
                    SiteId = s.SiteId,
                    OnBehalfId = s.OnBehalfId,
                    OnBehalfName = s.OnBehalf?.UserName,
                    AssistanceFromIds = assistanceFormIds,
                    AssistanceFrom = assistanceFormIds != null && appUsers != null ? string.Join(",", appUsers.Where(w => assistanceFormIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "",
                };
                CompanyCalendarModels.Add(CompanyCalendarModel);
            });
            return CompanyCalendarModels.OrderByDescending(a => a.CompanyCalendarId).ToList();
        }
        private List<long> GetHeaderUserIds(long id)
        {
            List<long> userIds = new List<long>();
            var companycalendar = _context.CompanyCalendar.Include(s => s.CompanyCalendarAssistance).SingleOrDefault(w => w.CompanyCalendarId == id);
            if (companycalendar != null)
            {
                if (companycalendar != null)
                {
                    userIds.Add(companycalendar.OwnerId.GetValueOrDefault(0));
                    if (companycalendar.RequireAssistance == true && companycalendar.CompanyCalendarAssistance != null && companycalendar.CompanyCalendarAssistance.Count > 0)
                    {
                        userIds.AddRange(companycalendar.CompanyCalendarAssistance?.Select(s => s.UserId.GetValueOrDefault(0)).ToList());
                    }
                    userIds.Add(companycalendar.AddedByUserId.GetValueOrDefault(0));
                    userIds.Add(companycalendar.OnBehalfId.GetValueOrDefault(0));
                }
            }
            return userIds.Where(w => w > 0).Distinct().ToList();
        }
        [HttpPost]
        [Route("GetCompanyCalendarAssistance")]
        public List<CompanyCalendarModel> GetCompanyCalendarAssistance(SearchModel searchModel)
        {
            List<CompanyCalendarModel> CompanyCalendarModels = new List<CompanyCalendarModel>();
            var calendarAssistance = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMaster.ApplicationMasterCodeId == 278 && w.Value.ToLower() == "calender assistance").FirstOrDefault()?.ApplicationMasterDetailId;
            var exits = "";
            if (calendarAssistance != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.ToList();
                var companyCalendars = _context.CompanyCalendarLine
                    .Include(a => a.AddedByUser)
                    .Include(m => m.ModifiedByUser)
                    .Include(s => s.StatusCode)
                    .Include(a => a.CompanyCalendar)
                    .Include(a => a.CompanyCalendar.Site)
                    .Include(a => a.CompanyCalendar.Company)
                    .Include(a => a.CompanyCalendar.Owner)
                    .Include(a => a.CompanyCalendar.OnBehalf)
                    .Include(a => a.CompanyCalendar.CompanyCalendarAssistance)
                    .Where(w => w.CompanyCalendar.CalenderEventId == calendarAssistance);
                if (searchModel.TypeOfServiceId != null)
                {
                    companyCalendars = companyCalendars.Where(w => w.TypeOfServiceId == searchModel.TypeOfServiceId);
                    exits = "Yes";
                }
                if (searchModel.TypeOfEventIds != null)
                {
                    companyCalendars = companyCalendars.Where(w => w.TypeOfEventId == searchModel.TypeOfEventIds);
                    exits = "Yes";
                }
                if (searchModel.CalenderStatusIds != null)
                {
                    companyCalendars = companyCalendars.Where(w => w.CalenderStatusId == searchModel.CalenderStatusIds);
                    exits = "Yes";
                }
                if (searchModel.EventDate != null)
                {
                    companyCalendars = companyCalendars.Where(w => w.EventDate == searchModel.EventDate);
                    exits = "Yes";
                }
                if (searchModel.DueDate != null)
                {
                    companyCalendars = companyCalendars.Where(w => w.DueDate == searchModel.DueDate);
                    exits = "Yes";
                }
                if (!String.IsNullOrEmpty(searchModel.Subject))
                {
                    companyCalendars = companyCalendars.Where(w => w.Subject.Contains(searchModel.Subject));
                    exits = "Yes";
                }
                var companyCalendar = companyCalendars.AsNoTracking().ToList();
                companyCalendar.ForEach(s =>
                {
                    var userExits = GetHeaderUserIds(s.CompanyCalendarId.Value).Contains(searchModel.UserID);
                    if (userExits == true)
                    {
                        var assistanceFormIds = s.CompanyCalendar?.CompanyCalendarAssistance != null ? s.CompanyCalendar.CompanyCalendarAssistance.Select(q => q.UserId).ToList() : new List<long?>();
                        CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                        {
                            CompanyCalendarLineId=s.CompanyCalendarLineId,
                            CompanyCalendarId = s.CompanyCalendarId.Value,
                            CalendarEventDate = s.CompanyCalendar?.CalendarEventDate,
                            CalenderEventId = s.CompanyCalendar?.CalenderEventId,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            SessionId = s.SessionId,
                            CalenderEventName = s.CompanyCalendar?.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CompanyCalendar?.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "",
                            SiteName = s.CompanyCalendar?.Site != null ? s.CompanyCalendar?.Site.Name : "",
                            CompanyName = s.CompanyCalendar?.Company != null ? s.CompanyCalendar?.Company.Description : "",
                            CompanyId = s.CompanyCalendar?.CompanyId,
                            SiteId = s.CompanyCalendar?.SiteId,
                            EventDate = s.EventDate,
                            FromTime = s.FromTime,
                            ToTime = s.ToTime,
                            DueDate = s.DueDate,
                            Subject = s.Subject,
                            OwnerId = s.CompanyCalendar?.OwnerId,
                            OwnerName = s.CompanyCalendar?.Owner?.UserName,
                            OnBehalfId = s.CompanyCalendar?.OnBehalfId,
                            OnBehalfName = s.CompanyCalendar?.OnBehalf?.UserName,
                            CalenderStatusId = s.CalenderStatusId,
                            CalenderStatus = s.CalenderStatusId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CalenderStatusId).Select(m => m.Value).FirstOrDefault() : "",
                            RequireAssistance = s.CompanyCalendar?.RequireAssistance,
                            RequireAssistanceFlag = s.CompanyCalendar?.RequireAssistance == true ? "Yes" : "No",
                            AssistanceFromIds = assistanceFormIds,
                            TypeOfServiceId = s.TypeOfServiceId,
                            AssistanceFrom = assistanceFormIds != null && appUsers != null ? string.Join(",", appUsers.Where(w => assistanceFormIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "",
                            TypeOfServiceName = s.TypeOfServiceId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfServiceId).Select(m => m.Value).FirstOrDefault() : "",
                            TypeOfEventName = s.TypeOfEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfEventId).Select(m => m.Value).FirstOrDefault() : "",
                        };
                        CompanyCalendarModels.Add(CompanyCalendarModel);
                    }
                });
                if (exits == "")
                {
                    var companyCalendarss = _context.CompanyCalendar
                   .Include(a => a.AddedByUser)
                   .Include(m => m.ModifiedByUser)
                   .Include(s => s.StatusCode)
                   .Include(l => l.Company)
                   .Include(p => p.Site)
                   .Include(c => c.Owner)
                   .Include(v => v.OnBehalf)
                   .Include(a => a.CompanyCalendarAssistance)
                    .Include(a => a.CompanyCalendarLine).Where(w => w.CompanyCalendarLine == null || w.CompanyCalendarLine.Count == 0)
                   .AsNoTracking().ToList();
                    companyCalendarss.ForEach(s =>
                    {
                        var userExits = GetHeaderUserIds(s.CompanyCalendarId).Contains(searchModel.UserID);
                        if (userExits == true)
                        {
                            var assistanceFormIds = s.CompanyCalendarAssistance != null ? s.CompanyCalendarAssistance.Select(q => q.UserId).ToList() : new List<long?>();

                            CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                            {
                                CompanyCalendarId = s.CompanyCalendarId,
                                CalendarEventDate = s.CalendarEventDate,
                                CalenderEventId = s.CalenderEventId,
                                StatusCodeID = s.StatusCodeId,
                                AddedByUserID = s.AddedByUserId,
                                ModifiedByUserID = s.ModifiedByUserId,
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,
                                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                SessionId = s.SessionId,
                                CalenderEventName = s.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "",
                                SiteName = s.Site != null ? s.Site.Name : "",
                                CompanyName = s.Company != null ? s.Company.Description : "",
                                CompanyId = s.CompanyId,
                                SiteId = s.SiteId,
                                RequireAssistance = s.RequireAssistance,
                                RequireAssistanceFlag = s.RequireAssistance == true ? "Yes" : "No",
                                OwnerId = s.OwnerId,
                                OwnerName = s.Owner?.UserName,
                                OnBehalfId = s.OnBehalfId,
                                OnBehalfName = s.OnBehalf?.UserName,
                                AssistanceFromIds = assistanceFormIds,
                                AssistanceFrom = assistanceFormIds != null && appUsers != null ? string.Join(",", appUsers.Where(w => assistanceFormIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "",
                            };
                            CompanyCalendarModels.Add(CompanyCalendarModel);
                        }
                    });
                }
            }
            return CompanyCalendarModels.OrderByDescending(a => a.CompanyCalendarId).ToList();
        }
        [HttpGet]
        [Route("GetCompanyCalendarItems")]
        public List<CompanyCalendarModel> GetCompanyCalendarItems()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var companyCalendar = _context.CompanyCalendar
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(p => p.Site)
                .Include(c => c.Owner)
                .Include(v => v.OnBehalf)
                .Include(a => a.CompanyCalendarAssistance)
                .AsNoTracking().ToList();
            List<CompanyCalendarModel> CompanyCalendarModels = new List<CompanyCalendarModel>();
            var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            companyCalendar.ForEach(s =>
            {
                var assistanceFormIds = s.CompanyCalendarAssistance != null ? s.CompanyCalendarAssistance.Select(q => q.UserId).ToList() : new List<long?>();
                CompanyCalendarModel CompanyCalendarModel = new CompanyCalendarModel
                {
                    CompanyCalendarId = s.CompanyCalendarId,
                    CalendarEventDate = s.CalendarEventDate,
                    CalenderEventId = s.CalenderEventId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId = s.SessionId,
                    CalenderEventName = s.CalenderEventId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CalenderEventId).Select(m => m.Value).FirstOrDefault() : "",
                    SiteName = s.Site != null ? s.Site.Name : "",
                    CompanyName = s.Company != null ? s.Company.Description : "",
                    CompanyId = s.CompanyId,
                    SiteId = s.SiteId,
                    OwnerId = s.OwnerId,
                    OwnerName = s.Owner?.UserName,
                    OnBehalfId = s.OnBehalfId,
                    OnBehalfName = s.OnBehalf?.UserName,
                    AssistanceFromIds = assistanceFormIds,
                    AssistanceFrom = assistanceFormIds != null && appUsers != null ? string.Join(",", appUsers.Where(w => assistanceFormIds.Contains(w.UserId)).Select(s => s.UserName).ToList()) : "",
                };
                CompanyCalendarModels.Add(CompanyCalendarModel);
            });
            return CompanyCalendarModels.OrderByDescending(a => a.CompanyCalendarId).ToList();
        }
        [HttpPost]
        [Route("InsertCompanyCalendar")]
        public CompanyCalendarModel Post(CompanyCalendarModel value)
        {
            var SessionId = Guid.NewGuid();
            var CompanyCalendar = new CompanyCalendar
            {
                CalendarEventDate = value.CalendarEventDate,
                CalenderEventId = value.CalenderEventId,
                SiteId = value.SiteId,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                CompanyId = value.CompanyId,
                UserId = value.UserId,
                RequireAssistance = value.RequireAssistanceFlag == "Yes" ? true : false,
                OwnerId = value.OwnerId,
                OnBehalfId = value.OnBehalfId,
                AssistanceFromId = value.AssistanceFromId,
            };
            _context.CompanyCalendar.Add(CompanyCalendar);
            _context.SaveChanges();
            value.CompanyCalendarId = CompanyCalendar.CompanyCalendarId;
            value.SessionId = CompanyCalendar.SessionId;
            if (value.AssistanceFromIds != null && value.AssistanceFromIds.Count > 0)
            {
                value.AssistanceFromIds.ForEach(h =>
                {
                    var companyCalendarAssistance = new CompanyCalendarAssistance
                    {
                        UserId = h,
                        CompanyCalendarId = CompanyCalendar.CompanyCalendarId,
                    };
                    _context.CompanyCalendarAssistance.Add(companyCalendarAssistance);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyCalendar")]
        public CompanyCalendarModel Put(CompanyCalendarModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var CompanyCalendar = _context.CompanyCalendar.SingleOrDefault(p => p.CompanyCalendarId == value.CompanyCalendarId);
            CompanyCalendar.CalendarEventDate = value.CalendarEventDate;
            CompanyCalendar.SiteId = value.SiteId;
            CompanyCalendar.CalenderEventId = value.CalenderEventId;
            CompanyCalendar.SessionId = value.SessionId;
            CompanyCalendar.CompanyId = value.CompanyId;
            CompanyCalendar.StatusCodeId = value.StatusCodeID.Value;
            CompanyCalendar.ModifiedByUserId = value.ModifiedByUserID;
            CompanyCalendar.ModifiedDate = DateTime.Now;
            CompanyCalendar.UserId = value.UserId;
            CompanyCalendar.RequireAssistance = value.RequireAssistanceFlag == "Yes" ? true : false;
            CompanyCalendar.OwnerId = value.OwnerId;
            CompanyCalendar.OnBehalfId = value.OnBehalfId;
            CompanyCalendar.AssistanceFromId = value.AssistanceFromId;
            _context.SaveChanges();
            var companyCalendarAssistance = _context.CompanyCalendarAssistance.Where(w => w.CompanyCalendarId == value.CompanyCalendarId).ToList();
            if (companyCalendarAssistance != null)
            {
                _context.CompanyCalendarAssistance.RemoveRange(companyCalendarAssistance);
                _context.SaveChanges();
            }
            if (value.AssistanceFromIds != null && value.AssistanceFromIds.Count > 0)
            {
                value.AssistanceFromIds.ForEach(h =>
                {
                    var companyCalendarAssistance = new CompanyCalendarAssistance
                    {
                        UserId = h,
                        CompanyCalendarId = CompanyCalendar.CompanyCalendarId,
                    };
                    _context.CompanyCalendarAssistance.Add(companyCalendarAssistance);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteCompanyCalendar")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var CompanyCalendar = _context.CompanyCalendar.Where(p => p.CompanyCalendarId == id).FirstOrDefault();
                if (CompanyCalendar != null)
                {
                    var CompanyCalendarLine = _context.CompanyCalendarLine.Where(b => b.CompanyCalendarId == id).ToList();
                    var companyCalendarAssistance = _context.CompanyCalendarAssistance.Where(w => w.CompanyCalendarId == CompanyCalendar.CompanyCalendarId).ToList();
                    if (companyCalendarAssistance != null)
                    {
                        _context.CompanyCalendarAssistance.RemoveRange(companyCalendarAssistance);
                        _context.SaveChanges();
                    }
                    if (CompanyCalendarLine != null)
                    {
                        CompanyCalendarLine.ForEach(h =>
                        {
                            var CompanyCalendarLineLocation = _context.CompanyCalendarLineLocation.Where(b => b.CompanyCalendarLineId == h.CompanyCalendarLineId).ToList();
                            _context.CompanyCalendarLineLocation.RemoveRange(CompanyCalendarLineLocation);
                            _context.SaveChanges();
                            var companyCalendarLineNotes = _context.CompanyCalendarLineNotes.Where(b => b.CompanyCalendarLineId == h.CompanyCalendarLineId).ToList();
                            _context.CompanyCalendarLineNotes.RemoveRange(companyCalendarLineNotes);
                            _context.SaveChanges();
                            var companyCalendarLineLink = _context.CompanyCalendarLineLink.Where(b => b.CompanyCalendarLineId == h.CompanyCalendarLineId).ToList();
                            _context.CompanyCalendarLineLink.RemoveRange(companyCalendarLineLink);
                            _context.SaveChanges();
                        });
                        _context.CompanyCalendarLine.RemoveRange(CompanyCalendarLine);
                        _context.SaveChanges();
                    }
                    _context.CompanyCalendar.Remove(CompanyCalendar);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
