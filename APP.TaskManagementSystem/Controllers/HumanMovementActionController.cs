﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HumanMovementActionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public HumanMovementActionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetHumanMovementActionByMovement")]
        public HumanMovementActionModel GetHumanMovementActionByMovement(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var humanMovementAction = _context.HumanMovementAction.Include(s => s.Pharmacist).Where(w => w.HumanMovementId == id).FirstOrDefault();
            HumanMovementActionModel humanMovementActionModel = new HumanMovementActionModel();
            if (humanMovementAction != null)
            {
                humanMovementActionModel.HumanMovementActionId = humanMovementAction.HumanMovementActionId;
                humanMovementActionModel.HumanMovementId = humanMovementAction.HumanMovementId;
                humanMovementActionModel.PharmacistId = humanMovementAction.PharmacistId;
                humanMovementActionModel.LogInTime = humanMovementAction.LogInTime;
                humanMovementActionModel.ActionToTakeId = humanMovementAction.ActionToTakeId;
                humanMovementActionModel.Description = humanMovementAction.Description;
                humanMovementActionModel.Comment = humanMovementAction.Comment;
                humanMovementActionModel.StatusCodeID = humanMovementAction.StatusCodeId;
                humanMovementActionModel.PharmacistName = humanMovementAction.Pharmacist != null ? (humanMovementAction.Pharmacist.FirstName + (humanMovementAction.Pharmacist.LastName != null ? (" | " + humanMovementAction.Pharmacist.LastName) : "")) : "";
                humanMovementActionModel.ActionToTakeName = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == humanMovementAction.ActionToTakeId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == humanMovementAction.ActionToTakeId).Value : "";
                humanMovementActionModel.HrloginId = humanMovementAction.HrloginId;
                humanMovementActionModel.Hrdescription = humanMovementAction.Hrdescription;
                humanMovementActionModel.Hrcomment = humanMovementAction.Hrcomment;
                humanMovementActionModel.HrstatusCodeId = humanMovementAction.HrstatusCodeId;
                humanMovementActionModel.HractionToTakeId = humanMovementAction.HractionToTakeId;
                humanMovementActionModel.HrLogInTime = humanMovementAction.HrLogInTime;
            }
            return humanMovementActionModel;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertHumanMovementAction")]
        public HumanMovementActionModel Post(HumanMovementActionModel value)
        {
            var humanMovementAction = new HumanMovementAction
            {
                HumanMovementId = value.HumanMovementId,
                PharmacistId = value.PharmacistId,
                LogInTime = value.LogInTime,
                ActionToTakeId = value.ActionToTakeId,
                Description = value.Description,
                Comment = value.Comment,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                HrloginId = value.HrloginId,
                HractionToTakeId = value.HractionToTakeId,
                Hrdescription = value.Hrdescription,
                Hrcomment = value.Hrcomment,
                HrstatusCodeId = value.HrstatusCodeId,
                HrLogInTime = value.HrLogInTime,
            };
            _context.HumanMovementAction.Add(humanMovementAction);
            _context.SaveChanges();
            value.HumanMovementActionId = humanMovementAction.HumanMovementActionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateHumanMovementAction")]
        public HumanMovementActionModel Put(HumanMovementActionModel value)
        {
            var humanMovementAction = _context.HumanMovementAction.SingleOrDefault(p => p.HumanMovementId == value.HumanMovementId);
            humanMovementAction.PharmacistId = value.PharmacistId;
            humanMovementAction.LogInTime = value.LogInTime;
            humanMovementAction.ActionToTakeId = value.ActionToTakeId;
            humanMovementAction.Comment = value.Comment;
            humanMovementAction.Description = value.Description;
            humanMovementAction.ModifiedByUserId = value.ModifiedByUserID;
            humanMovementAction.ModifiedDate = DateTime.Now;
            humanMovementAction.HrloginId = value.HrloginId;
            humanMovementAction.Hrdescription = value.Hrdescription;
            humanMovementAction.Hrcomment = value.Hrcomment;
            humanMovementAction.HrstatusCodeId = value.HrstatusCodeId;
            humanMovementAction.HractionToTakeId = value.HractionToTakeId;
            humanMovementAction.HrLogInTime = value.HrLogInTime;
            _context.SaveChanges();
            return value;
        }
    }
}