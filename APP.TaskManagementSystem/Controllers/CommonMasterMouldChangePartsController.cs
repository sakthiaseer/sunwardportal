﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonMasterMouldChangePartsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonMasterMouldChangePartsController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonMasterMouldChangeParts")]
        public List<CommonMasterMouldChangePartsModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFiledsProductionMachineList = _context.PunchesMachine.ToList();
            var commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(b => b.Bcutter)
                .Include(bf => bf.BformingMould)
                .Include(bs => bs.Bsealing)
                .Include(bt => bt.BtrackMould)
                .Include(pa => pa.PallowInterchangeMachine)
                .Include(p => p.PinterCompanyMachine)
                .Include(pc => pc.PinterCompanyShare)
                .Include(t => t.Typeoftooling)
                .AsNoTracking().ToList();
            List<CommonMasterMouldChangePartsModel> commonMasterMouldChangePartsModel = new List<CommonMasterMouldChangePartsModel>();
            commonMasterMouldChangeParts.ForEach(s =>
            {
                CommonMasterMouldChangePartsModel commonMasterMouldChangePartsModels = new CommonMasterMouldChangePartsModel
                {
                    CommonMasterMouldChangePartsId = s.CommonMasterMouldChangePartsId,
                    MachineGroupingId = s.MachineGroupingId,
                    BformingMouldId = s.BformingMouldId,
                    BtrackMouldId = s.BtrackMouldId,
                    BsealingId = s.BsealingId,
                    BcutterId = s.BcutterId,
                    BnameOfTheMouldSet = s.BnameOfTheMouldSet,
                    BformingMouldName = s.BformingMould != null ? s.BformingMould.NameOfMould : "",
                    BsealingName = s.Bsealing != null ? s.Bsealing.NameOfMould : "",
                    BcutterName = s.Bcutter != null ? s.Bcutter.NameOfMould : "",
                    BtrackMouldName = s.BtrackMould != null ? s.BtrackMould.NameOfMould : "",
                    PsizeLength = s.PsizeLength,
                    PsizeWidth = s.PsizeWidth,
                    PshapeId = s.PshapeId,
                    PmarkingId = s.PmarkingId,
                    PshapeName = s.PshapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.PshapeId).Value : "",
                    PmarkingName = s.PmarkingId != 0 && masterDetailList.Count > 0 ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.PmarkingId).Value : "",
                    PmarkingOn = s.PmarkingOn,
                    PallowInterchangeMachineId = s.PallowInterchangeMachineId,
                    PinterCompanyMachineId = s.PinterCompanyMachineId,
                    PinterCompanyShareId = s.PinterCompanyShareId,
                    PallowInterchangeMachineName = s.PallowInterchangeMachine != null ? s.PallowInterchangeMachine.NameOfTheMachine : "",
                    PinterCompanyMachineName = s.PinterCompanyMachine != null ? s.PinterCompanyMachine.NameOfTheMachine : "",
                    PinterCompanyShareName = s.PinterCompanyShare != null ? s.PinterCompanyShare.CompanyName : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    ProfileRefernceNo = s.ProfileRefernceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    PallowInterchangeMachineIds = commonFiledsProductionMachineList != null ? commonFiledsProductionMachineList.Where(l => l.CommonMasterMouldChangePartsId == s.CommonMasterMouldChangePartsId).Select(l => l.PunchesCommonMachineId.Value).ToList() : new List<long>(),
                    UpperTipSizeStandard = s.UpperTipSizeStandard,
                    UpperTipSizeVariation = s.UpperTipSizeVariation,
                    LowerTipSizeStandardValue = s.LowerTipSizeStandardValue,
                    LowerTipSizeVariationValue = s.LowerTipSizeVariationValue,
                    UpperTipStraigthStandard = s.UpperTipStraigthStandard,
                    UpperTipStraigthVariation = s.UpperTipStraigthVariation,
                    LowerTipStraigthStandardValue = s.LowerTipStraigthStandardValue,
                    LowerTipStraigthVariationValue = s.LowerTipStraigthVariationValue,
                    UpperTipDepthStandard = s.UpperTipDepthStandard,
                    UpperTipDepthVariation = s.UpperTipDepthVariation,
                    LowerTipDepthStandardValue = s.LowerTipDepthStandardValue,
                    LowerTipDepthVariationValue = s.LowerTipDepthVariationValue,
                    UpperBarrelDiameterStandard = s.UpperBarrelDiameterStandard,
                    UpperBarrelDiameterVariation = s.UpperBarrelDiameterVariation,
                    LowerBarrelDiameterStandardValue = s.LowerBarrelDiameterStandardValue,
                    LowerBarrelDiameterVariationValue = s.LowerBarrelDiameterVariationValue,
                    UpperOverallLengthStandard = s.UpperOverallLengthStandard,
                    UpperOverallLengthVariation = s.UpperOverallLengthVariation,
                    LowerOverallLengthStandardValue = s.LowerOverallLengthStandardValue,
                    LowerOverallLengthVariationValue = s.LowerOverallLengthVariationValue,
                    UpperCriticalLengthStandard = s.UpperCriticalLengthStandard,
                    UpperCriticalLengthVariation = s.UpperCriticalLengthVariation,
                    LowerCriticalLengthStandardValue = s.LowerCriticalLengthStandardValue,
                    LowerCriticalLengthVariationValue = s.LowerCriticalLengthVariationValue,
                    UpperCupRadiusStandard = s.UpperCupRadiusStandard,
                    UpperCupRadiusVariation = s.UpperCupRadiusVariation,
                    LowerCupRadiusStandardValue = s.LowerCupRadiusStandardValue,
                    LowerCupRadiusVariationValue = s.LowerCupRadiusVariationValue,
                    UpperCupDepthStandard = s.UpperCupDepthStandard,
                    UpperCupDepthVariation = s.UpperCupDepthVariation,
                    LowerCupDepthStandardValue = s.LowerCupDepthStandardValue,
                    LowerCupDepthVariationValue = s.LowerCupDepthVariationValue,
                    UpperLandStandard = s.UpperLandStandard,
                    UpperLandVariation = s.UpperLandVariation,
                    LowerLandStandardValue = s.LowerLandStandardValue,
                    LowerLandVariationValue = s.LowerLandVariationValue,
                    UpperBevelEdgeAngelStandard = s.UpperBevelEdgeAngelStandard,
                    UpperBevelEdgeAngelVariation = s.UpperBevelEdgeAngelVariation,
                    LowerBevelEdgeAngelStandardValue = s.LowerBevelEdgeAngelStandardValue,
                    LowerBevelEdgeAngelVariationValue = s.LowerBevelEdgeAngelVariationValue,
                    UpperBevelEdgeStandard = s.UpperBevelEdgeStandard,
                    UpperBevelEdgeVariation = s.UpperBevelEdgeVariation,
                    LowerBevelEdgeStandardValue = s.LowerBevelEdgeStandardValue,
                    LowerBevelEdgeVariationValue = s.LowerBevelEdgeVariationValue,
                    UpperWidthOfScoreStandard = s.UpperWidthOfScoreStandard,
                    UpperWidthOfScoreVariation = s.UpperWidthOfScoreVariation,
                    LowerWidthOfScoreStandardValue = s.LowerWidthOfScoreStandardValue,
                    LowerWidthOfScoreVariationValue = s.LowerWidthOfScoreVariationValue,
                    UpperDepthOfScoreStandard = s.UpperDepthOfScoreStandard,
                    UpperDepthOfScoreVariation = s.UpperDepthOfScoreVariation,
                    LowerDepthOfScoreStandardValue = s.LowerDepthOfScoreStandardValue,
                    BoreIdmm = s.BoreIdmm,
                    Odmm = s.Odmm,
                    Heightmm = s.Heightmm,
                    EmbossementFlag = s.Embossement == true ? "Yes" : "No",
                    PurchaseDate = s.PurchaseDate,
                    TypeoftoolingId = s.TypeoftoolingId,
                    Seriesno = s.Seriesno,
                    Totalnumbers = s.Totalnumbers,
                    Typeoftooling = s.Typeoftooling != null ? s.Typeoftooling.CodeValue : null,
                    SessionId=s.SessionId,
                };
                commonMasterMouldChangePartsModel.Add(commonMasterMouldChangePartsModels);
            });
            return commonMasterMouldChangePartsModel.OrderByDescending(a => a.CommonMasterMouldChangePartsId).ToList();
        }

        [HttpPost]
        [Route("GetCommonMasterMouldChangePartsByRefNo")]
        public List<CommonMasterMouldChangePartsModel> GetCommonMasterMouldChangePartsByRefNo(RefSearchModel refSearchModel)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFiledsProductionMachineList = _context.PunchesMachine.ToList();
            var commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(b => b.Bcutter)
                .Include(bf => bf.BformingMould)
                .Include(bs => bs.Bsealing)
                .Include(bt => bt.BtrackMould)
                .Include(pa => pa.PallowInterchangeMachine)
                .Include(p => p.PinterCompanyMachine)
                .Include(pc => pc.PinterCompanyShare)
                .Include(t => t.Typeoftooling)
                .AsNoTracking().ToList();
            List<CommonMasterMouldChangePartsModel> commonMasterMouldChangePartsModel = new List<CommonMasterMouldChangePartsModel>();
            commonMasterMouldChangeParts.ForEach(s =>
            {
                CommonMasterMouldChangePartsModel commonMasterMouldChangePartsModels = new CommonMasterMouldChangePartsModel();
                commonMasterMouldChangePartsModels.CommonMasterMouldChangePartsId = s.CommonMasterMouldChangePartsId;
                commonMasterMouldChangePartsModels.MachineGroupingId = s.MachineGroupingId;
                commonMasterMouldChangePartsModels.BformingMouldId = s.BformingMouldId;
                commonMasterMouldChangePartsModels.BtrackMouldId = s.BtrackMouldId;
                commonMasterMouldChangePartsModels.BsealingId = s.BsealingId;
                commonMasterMouldChangePartsModels.BcutterId = s.BcutterId;
                commonMasterMouldChangePartsModels.MachineGroupingName = s.MachineGroupingId != null && masterDetailList.Count > 0 ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Value : "";
                commonMasterMouldChangePartsModels.BnameOfTheMouldSet = s.BnameOfTheMouldSet;
                commonMasterMouldChangePartsModels.BformingMouldName = s.BformingMould != null ? s.BformingMould.NameOfMould : "";
                commonMasterMouldChangePartsModels.BsealingName = s.Bsealing != null ? s.Bsealing.NameOfMould : "";
                commonMasterMouldChangePartsModels.BcutterName = s.Bcutter != null ? s.Bcutter.NameOfMould : "";
                commonMasterMouldChangePartsModels.BtrackMouldName = s.BtrackMould != null ? s.BtrackMould.NameOfMould : "";
                commonMasterMouldChangePartsModels.PsizeLength = s.PsizeLength;
                commonMasterMouldChangePartsModels.PsizeWidth = s.PsizeWidth;
                commonMasterMouldChangePartsModels.PshapeId = s.PshapeId;
                commonMasterMouldChangePartsModels.PmarkingId = s.PmarkingId;
                commonMasterMouldChangePartsModels.PshapeName = s.PshapeId != null && masterDetailList.Count > 0 ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.PshapeId).Value : "";
                commonMasterMouldChangePartsModels.PmarkingName = s.PmarkingId != null && masterDetailList.Count > 0 ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.PmarkingId).Value : "";
                commonMasterMouldChangePartsModels.PmarkingOn = s.PmarkingOn;
                commonMasterMouldChangePartsModels.PallowInterchangeMachineId = s.PallowInterchangeMachineId;
                commonMasterMouldChangePartsModels.PinterCompanyMachineId = s.PinterCompanyMachineId;
                commonMasterMouldChangePartsModels.PinterCompanyShareId = s.PinterCompanyShareId;
                commonMasterMouldChangePartsModels.PallowInterchangeMachineName = s.PallowInterchangeMachine != null ? s.PallowInterchangeMachine.NameOfTheMachine : "";
                commonMasterMouldChangePartsModels.PinterCompanyMachineName = s.PinterCompanyMachine != null ? s.PinterCompanyMachine.NameOfTheMachine : "";
                commonMasterMouldChangePartsModels.PinterCompanyShareName = s.PinterCompanyShare != null ? s.PinterCompanyShare.CompanyName : "";
                commonMasterMouldChangePartsModels.StatusCodeID = s.StatusCodeId;
                commonMasterMouldChangePartsModels.AddedByUserID = s.AddedByUserId;
                commonMasterMouldChangePartsModels.ModifiedByUserID = s.ModifiedByUserId;
                commonMasterMouldChangePartsModels.AddedDate = s.AddedDate;
                commonMasterMouldChangePartsModels.ModifiedDate = s.ModifiedDate;
                commonMasterMouldChangePartsModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                commonMasterMouldChangePartsModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                commonMasterMouldChangePartsModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                commonMasterMouldChangePartsModels.ProfileRefernceNo = s.ProfileRefernceNo;
                commonMasterMouldChangePartsModels.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                commonMasterMouldChangePartsModels.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                commonMasterMouldChangePartsModels.PallowInterchangeMachineIds = commonFiledsProductionMachineList != null ? commonFiledsProductionMachineList.Where(l => l.CommonMasterMouldChangePartsId == s.CommonMasterMouldChangePartsId).Select(l => l.PunchesCommonMachineId.Value).ToList() : new List<long>();
                commonMasterMouldChangePartsModels.UpperTipSizeStandard = s.UpperTipSizeStandard;
                commonMasterMouldChangePartsModels.UpperTipSizeVariation = s.UpperTipSizeVariation;
                commonMasterMouldChangePartsModels.LowerTipSizeStandardValue = s.LowerTipSizeStandardValue;
                commonMasterMouldChangePartsModels.LowerTipSizeVariationValue = s.LowerTipSizeVariationValue;
                commonMasterMouldChangePartsModels.UpperTipStraigthStandard = s.UpperTipStraigthStandard;
                commonMasterMouldChangePartsModels.UpperTipStraigthVariation = s.UpperTipStraigthVariation;
                commonMasterMouldChangePartsModels.LowerTipStraigthStandardValue = s.LowerTipStraigthStandardValue;
                commonMasterMouldChangePartsModels.LowerTipStraigthVariationValue = s.LowerTipStraigthVariationValue;
                commonMasterMouldChangePartsModels.UpperTipDepthStandard = s.UpperTipDepthStandard;
                commonMasterMouldChangePartsModels.UpperTipDepthVariation = s.UpperTipDepthVariation;
                commonMasterMouldChangePartsModels.LowerTipDepthStandardValue = s.LowerTipDepthStandardValue;
                commonMasterMouldChangePartsModels.LowerTipDepthVariationValue = s.LowerTipDepthVariationValue;
                commonMasterMouldChangePartsModels.UpperBarrelDiameterStandard = s.UpperBarrelDiameterStandard;
                commonMasterMouldChangePartsModels.UpperBarrelDiameterVariation = s.UpperBarrelDiameterVariation;
                commonMasterMouldChangePartsModels.LowerBarrelDiameterStandardValue = s.LowerBarrelDiameterStandardValue;
                commonMasterMouldChangePartsModels.LowerBarrelDiameterVariationValue = s.LowerBarrelDiameterVariationValue;
                commonMasterMouldChangePartsModels.UpperOverallLengthStandard = s.UpperOverallLengthStandard;
                commonMasterMouldChangePartsModels.UpperOverallLengthVariation = s.UpperOverallLengthVariation;
                commonMasterMouldChangePartsModels.LowerOverallLengthStandardValue = s.LowerOverallLengthStandardValue;
                commonMasterMouldChangePartsModels.LowerOverallLengthVariationValue = s.LowerOverallLengthVariationValue;
                commonMasterMouldChangePartsModels.UpperCriticalLengthStandard = s.UpperCriticalLengthStandard;
                commonMasterMouldChangePartsModels.UpperCriticalLengthVariation = s.UpperCriticalLengthVariation;
                commonMasterMouldChangePartsModels.LowerCriticalLengthStandardValue = s.LowerCriticalLengthStandardValue;
                commonMasterMouldChangePartsModels.LowerCriticalLengthVariationValue = s.LowerCriticalLengthVariationValue;
                commonMasterMouldChangePartsModels.UpperCupRadiusStandard = s.UpperCupRadiusStandard;
                commonMasterMouldChangePartsModels.UpperCupRadiusVariation = s.UpperCupRadiusVariation;
                commonMasterMouldChangePartsModels.LowerCupRadiusStandardValue = s.LowerCupRadiusStandardValue;
                commonMasterMouldChangePartsModels.LowerCupRadiusVariationValue = s.LowerCupRadiusVariationValue;
                commonMasterMouldChangePartsModels.UpperCupDepthStandard = s.UpperCupDepthStandard;
                commonMasterMouldChangePartsModels.UpperCupDepthVariation = s.UpperCupDepthVariation;
                commonMasterMouldChangePartsModels.LowerCupDepthStandardValue = s.LowerCupDepthStandardValue;
                commonMasterMouldChangePartsModels.LowerCupDepthVariationValue = s.LowerCupDepthVariationValue;
                commonMasterMouldChangePartsModels.UpperLandStandard = s.UpperLandStandard;
                commonMasterMouldChangePartsModels.UpperLandVariation = s.UpperLandVariation;
                commonMasterMouldChangePartsModels.LowerLandStandardValue = s.LowerLandStandardValue;
                commonMasterMouldChangePartsModels.LowerLandVariationValue = s.LowerLandVariationValue;
                commonMasterMouldChangePartsModels.UpperBevelEdgeAngelStandard = s.UpperBevelEdgeAngelStandard;
                commonMasterMouldChangePartsModels.UpperBevelEdgeAngelVariation = s.UpperBevelEdgeAngelVariation;
                commonMasterMouldChangePartsModels.LowerBevelEdgeAngelStandardValue = s.LowerBevelEdgeAngelStandardValue;
                commonMasterMouldChangePartsModels.LowerBevelEdgeAngelVariationValue = s.LowerBevelEdgeAngelVariationValue;
                commonMasterMouldChangePartsModels.UpperBevelEdgeStandard = s.UpperBevelEdgeStandard;
                commonMasterMouldChangePartsModels.UpperBevelEdgeVariation = s.UpperBevelEdgeVariation;
                commonMasterMouldChangePartsModels.LowerBevelEdgeStandardValue = s.LowerBevelEdgeStandardValue;
                commonMasterMouldChangePartsModels.LowerBevelEdgeVariationValue = s.LowerBevelEdgeVariationValue;
                commonMasterMouldChangePartsModels.UpperWidthOfScoreStandard = s.UpperWidthOfScoreStandard;
                commonMasterMouldChangePartsModels.UpperWidthOfScoreVariation = s.UpperWidthOfScoreVariation;
                commonMasterMouldChangePartsModels.LowerWidthOfScoreStandardValue = s.LowerWidthOfScoreStandardValue;
                commonMasterMouldChangePartsModels.LowerWidthOfScoreVariationValue = s.LowerWidthOfScoreVariationValue;
                commonMasterMouldChangePartsModels.UpperDepthOfScoreStandard = s.UpperDepthOfScoreStandard;
                commonMasterMouldChangePartsModels.UpperDepthOfScoreVariation = s.UpperDepthOfScoreVariation;
                commonMasterMouldChangePartsModels.LowerDepthOfScoreStandardValue = s.LowerDepthOfScoreStandardValue;
                commonMasterMouldChangePartsModels.LowerDepthOfScoreVariationValue = s.LowerDepthOfScoreVariationValue;
                commonMasterMouldChangePartsModels.Odmm = s.Odmm;
                commonMasterMouldChangePartsModels.Heightmm = s.Heightmm;
                commonMasterMouldChangePartsModels.EmbossementFlag = s.Embossement == true ? "Yes" : "No";
                commonMasterMouldChangePartsModels.BoreIdmm = s.BoreIdmm;
                commonMasterMouldChangePartsModels.PurchaseDate = s.PurchaseDate;
                commonMasterMouldChangePartsModels.TypeoftoolingId = s.TypeoftoolingId;
                commonMasterMouldChangePartsModels.Seriesno = s.Seriesno;
                commonMasterMouldChangePartsModels.Totalnumbers = s.Totalnumbers;
                commonMasterMouldChangePartsModels.SessionId = s.SessionId;
                commonMasterMouldChangePartsModels.Typeoftooling = s.Typeoftooling != null ? s.Typeoftooling.CodeValue : null;
                commonMasterMouldChangePartsModel.Add(commonMasterMouldChangePartsModels);
            });
            if (refSearchModel.IsHeader)
            {
                return commonMasterMouldChangePartsModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.CommonMasterMouldChangePartsId).ToList();
            }
            return commonMasterMouldChangePartsModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.CommonMasterMouldChangePartsId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonMasterMouldChangePartsModel> GetData(SearchModel searchModel)
        {
            var commonMasterMouldChangeParts = new CommonMasterMouldChangeParts();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).FirstOrDefault();
                        break;
                    case "Last":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).LastOrDefault();
                        break;
                    case "Next":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).LastOrDefault();
                        break;
                    case "Previous":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).FirstOrDefault();
                        break;
                    case "Last":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).LastOrDefault();
                        break;
                    case "Next":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderBy(o => o.CommonMasterMouldChangePartsId).FirstOrDefault(s => s.CommonMasterMouldChangePartsId > searchModel.Id);
                        break;
                    case "Previous":
                        commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.OrderByDescending(o => o.CommonMasterMouldChangePartsId).FirstOrDefault(s => s.CommonMasterMouldChangePartsId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonMasterMouldChangePartsModel>(commonMasterMouldChangeParts);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonMasterMouldChangeParts")]
        public CommonMasterMouldChangePartsModel Post(CommonMasterMouldChangePartsModel value)
        {
            var SessionId = Guid.NewGuid();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonMasterMouldChangeParts" });
            var commonMasterMouldChangeParts = new CommonMasterMouldChangeParts
            {
                MachineGroupingId = value.MachineGroupingId,
                BformingMouldId = value.BformingMouldId,
                BtrackMouldId = value.BtrackMouldId,
                BsealingId = value.BsealingId,
                BcutterId = value.BcutterId,
                BnameOfTheMouldSet = UpdateCommonPackage(value),
                PsizeLength = value.PsizeLength,
                PsizeWidth = value.PsizeWidth,
                PshapeId = value.PshapeId,
                PmarkingId = value.PmarkingId,
                PmarkingOn = value.PmarkingOn,
                PallowInterchangeMachineId = value.PallowInterchangeMachineId,
                PinterCompanyMachineId = value.PinterCompanyMachineId,
                PinterCompanyShareId = value.PinterCompanyShareId,
                ProfileRefernceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                UpperTipSizeStandard = value.UpperTipSizeStandard,
                UpperTipSizeVariation = value.UpperTipSizeVariation,
                LowerTipSizeStandardValue = value.LowerTipSizeStandardValue,
                LowerTipSizeVariationValue = value.LowerTipSizeVariationValue,
                UpperTipStraigthStandard = value.UpperTipStraigthStandard,
                UpperTipStraigthVariation = value.UpperTipStraigthVariation,
                LowerTipStraigthStandardValue = value.LowerTipStraigthStandardValue,
                LowerTipStraigthVariationValue = value.LowerTipStraigthVariationValue,
                UpperTipDepthStandard = value.UpperTipDepthStandard,
                UpperTipDepthVariation = value.UpperTipDepthVariation,
                LowerTipDepthStandardValue = value.LowerTipDepthStandardValue,
                LowerTipDepthVariationValue = value.LowerTipDepthVariationValue,
                UpperBarrelDiameterStandard = value.UpperBarrelDiameterStandard,
                UpperBarrelDiameterVariation = value.UpperBarrelDiameterVariation,
                LowerBarrelDiameterStandardValue = value.LowerBarrelDiameterStandardValue,
                LowerBarrelDiameterVariationValue = value.LowerBarrelDiameterVariationValue,
                UpperOverallLengthStandard = value.UpperOverallLengthStandard,
                UpperOverallLengthVariation = value.UpperOverallLengthVariation,
                LowerOverallLengthStandardValue = value.LowerOverallLengthStandardValue,
                LowerOverallLengthVariationValue = value.LowerOverallLengthVariationValue,
                UpperCriticalLengthStandard = value.UpperCriticalLengthStandard,
                UpperCriticalLengthVariation = value.UpperCriticalLengthVariation,
                LowerCriticalLengthStandardValue = value.LowerCriticalLengthStandardValue,
                LowerCriticalLengthVariationValue = value.LowerCriticalLengthVariationValue,
                UpperCupRadiusStandard = value.UpperCupRadiusStandard,
                UpperCupRadiusVariation = value.UpperCupRadiusVariation,
                LowerCupRadiusStandardValue = value.LowerCupRadiusStandardValue,
                LowerCupRadiusVariationValue = value.LowerCupRadiusVariationValue,
                UpperCupDepthStandard = value.UpperCupDepthStandard,
                UpperCupDepthVariation = value.UpperCupDepthVariation,
                LowerCupDepthStandardValue = value.LowerCupDepthStandardValue,
                LowerCupDepthVariationValue = value.LowerCupDepthVariationValue,
                UpperLandStandard = value.UpperLandStandard,
                UpperLandVariation = value.UpperLandVariation,
                LowerLandStandardValue = value.LowerLandStandardValue,
                LowerLandVariationValue = value.LowerLandVariationValue,
                UpperBevelEdgeAngelStandard = value.UpperBevelEdgeAngelStandard,
                UpperBevelEdgeAngelVariation = value.UpperBevelEdgeAngelVariation,
                LowerBevelEdgeAngelStandardValue = value.LowerBevelEdgeAngelStandardValue,
                LowerBevelEdgeAngelVariationValue = value.LowerBevelEdgeAngelVariationValue,
                UpperBevelEdgeStandard = value.UpperBevelEdgeStandard,
                UpperBevelEdgeVariation = value.UpperBevelEdgeVariation,
                LowerBevelEdgeStandardValue = value.LowerBevelEdgeStandardValue,
                LowerBevelEdgeVariationValue = value.LowerBevelEdgeVariationValue,
                UpperWidthOfScoreStandard = value.UpperWidthOfScoreStandard,
                UpperWidthOfScoreVariation = value.UpperWidthOfScoreVariation,
                LowerWidthOfScoreStandardValue = value.LowerWidthOfScoreStandardValue,
                LowerWidthOfScoreVariationValue = value.LowerWidthOfScoreVariationValue,
                UpperDepthOfScoreStandard = value.UpperDepthOfScoreStandard,
                UpperDepthOfScoreVariation = value.UpperDepthOfScoreVariation,
                LowerDepthOfScoreStandardValue = value.LowerDepthOfScoreStandardValue,
                LowerDepthOfScoreVariationValue = value.LowerDepthOfScoreVariationValue,
                BoreIdmm = value.BoreIdmm,
                Odmm = value.Odmm,
                Heightmm = value.Heightmm,
                Embossement = value.EmbossementFlag == "Yes" ? true : false,
                PurchaseDate = value.PurchaseDate,
                TypeoftoolingId = value.TypeoftoolingId,
                Seriesno = value.Seriesno,
                Totalnumbers = value.Totalnumbers,
                SessionId=SessionId,
            };
            _context.CommonMasterMouldChangeParts.Add(commonMasterMouldChangeParts);
            if (value.PallowInterchangeMachineIds != null)
            {
                value.PallowInterchangeMachineIds.ForEach(c =>
                {
                    var machineCommonFieldMultipleItems = new PunchesMachine
                    {
                        PunchesCommonMachineId = c,
                    };
                    commonMasterMouldChangeParts.PunchesMachine.Add(machineCommonFieldMultipleItems);
                });
            }
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.MasterProfileReferenceNo = commonMasterMouldChangeParts.MasterProfileReferenceNo;
            value.ProfileRefernceNo = commonMasterMouldChangeParts.ProfileRefernceNo;
            value.CommonMasterMouldChangePartsId = commonMasterMouldChangeParts.CommonMasterMouldChangePartsId;
            return value;
        }
        private string UpdateCommonPackage(CommonMasterMouldChangePartsModel value)
        {
            value.BnameOfTheMouldSet = "";
            var machineItems = _context.BlisterMouldInformation.ToList();
            if (machineItems != null)
            {
                var BformingMould = value.BformingMouldId != 0 && machineItems.Count > 0 ? machineItems.Where(m => m.BlisterMouldInformationId == value.BformingMouldId).Select(m => m.NameOfMould).FirstOrDefault() : "";
                var BtrackMould = value.BformingMouldId != 0 && machineItems.Count > 0 ? machineItems.Where(m => m.BlisterMouldInformationId == value.BtrackMouldId).Select(m => m.NameOfMould).FirstOrDefault() : "";
                var Bsealing = value.BformingMouldId != 0 && machineItems.Count > 0 ? machineItems.Where(m => m.BlisterMouldInformationId == value.BsealingId).Select(m => m.NameOfMould).FirstOrDefault() : "";
                var Bcutter = value.BformingMouldId != 0 && machineItems.Count > 0 ? machineItems.Where(m => m.BlisterMouldInformationId == value.BcutterId).Select(m => m.NameOfMould).FirstOrDefault() : "";
                var itemName = "[" + BformingMould + "][" + BtrackMould + "][" + Bsealing + "][" + Bcutter + "]" + " " + "Bilster Set";
                value.BnameOfTheMouldSet = itemName;
            }
            return value.BnameOfTheMouldSet;
        }
        [HttpPut]
        [Route("UpdateCommonMasterMouldChangeParts")]
        public CommonMasterMouldChangePartsModel Put(CommonMasterMouldChangePartsModel value)
        {
            if(value.SessionId==null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.SingleOrDefault(p => p.CommonMasterMouldChangePartsId == value.CommonMasterMouldChangePartsId);
            commonMasterMouldChangeParts.MachineGroupingId = value.MachineGroupingId;
            commonMasterMouldChangeParts.BformingMouldId = value.BformingMouldId;
            commonMasterMouldChangeParts.BtrackMouldId = value.BtrackMouldId;
            commonMasterMouldChangeParts.BsealingId = value.BsealingId;
            commonMasterMouldChangeParts.BcutterId = value.BcutterId;
            commonMasterMouldChangeParts.BnameOfTheMouldSet = UpdateCommonPackage(value);
            commonMasterMouldChangeParts.PsizeLength = value.PsizeLength;
            commonMasterMouldChangeParts.PsizeWidth = value.PsizeWidth;
            commonMasterMouldChangeParts.PshapeId = value.PshapeId;
            commonMasterMouldChangeParts.PmarkingId = value.PmarkingId;
            commonMasterMouldChangeParts.PmarkingOn = value.PmarkingOn;
            commonMasterMouldChangeParts.PallowInterchangeMachineId = value.PallowInterchangeMachineId;
            commonMasterMouldChangeParts.PinterCompanyMachineId = value.PinterCompanyMachineId;
            commonMasterMouldChangeParts.PinterCompanyShareId = value.PinterCompanyShareId;
            commonMasterMouldChangeParts.StatusCodeId = value.StatusCodeID.Value;
            commonMasterMouldChangeParts.ModifiedByUserId = value.ModifiedByUserID;
            commonMasterMouldChangeParts.ModifiedDate = DateTime.Now;
            commonMasterMouldChangeParts.UpperTipSizeStandard = value.UpperTipSizeStandard;
            commonMasterMouldChangeParts.UpperTipSizeVariation = value.UpperTipSizeVariation;
            commonMasterMouldChangeParts.LowerTipSizeStandardValue = value.LowerTipSizeStandardValue;
            commonMasterMouldChangeParts.LowerTipSizeVariationValue = value.LowerTipSizeVariationValue;
            commonMasterMouldChangeParts.UpperTipStraigthStandard = value.UpperTipStraigthStandard;
            commonMasterMouldChangeParts.UpperTipStraigthVariation = value.UpperTipStraigthVariation;
            commonMasterMouldChangeParts.LowerTipStraigthStandardValue = value.LowerTipStraigthStandardValue;
            commonMasterMouldChangeParts.LowerTipStraigthVariationValue = value.LowerTipStraigthVariationValue;
            commonMasterMouldChangeParts.UpperTipDepthStandard = value.UpperTipDepthStandard;
            commonMasterMouldChangeParts.UpperTipDepthVariation = value.UpperTipDepthVariation;
            commonMasterMouldChangeParts.LowerTipDepthStandardValue = value.LowerTipDepthStandardValue;
            commonMasterMouldChangeParts.LowerTipDepthVariationValue = value.LowerTipDepthVariationValue;
            commonMasterMouldChangeParts.UpperBarrelDiameterStandard = value.UpperBarrelDiameterStandard;
            commonMasterMouldChangeParts.UpperBarrelDiameterVariation = value.UpperBarrelDiameterVariation;
            commonMasterMouldChangeParts.LowerBarrelDiameterStandardValue = value.LowerBarrelDiameterStandardValue;
            commonMasterMouldChangeParts.LowerBarrelDiameterVariationValue = value.LowerBarrelDiameterVariationValue;
            commonMasterMouldChangeParts.UpperOverallLengthStandard = value.UpperOverallLengthStandard;
            commonMasterMouldChangeParts.UpperOverallLengthVariation = value.UpperOverallLengthVariation;
            commonMasterMouldChangeParts.LowerOverallLengthStandardValue = value.LowerOverallLengthStandardValue;
            commonMasterMouldChangeParts.LowerOverallLengthVariationValue = value.LowerOverallLengthVariationValue;
            commonMasterMouldChangeParts.UpperCriticalLengthStandard = value.UpperCriticalLengthStandard;
            commonMasterMouldChangeParts.UpperCriticalLengthVariation = value.UpperCriticalLengthVariation;
            commonMasterMouldChangeParts.LowerCriticalLengthStandardValue = value.LowerCriticalLengthStandardValue;
            commonMasterMouldChangeParts.LowerCriticalLengthVariationValue = value.LowerCriticalLengthVariationValue;
            commonMasterMouldChangeParts.UpperCupRadiusStandard = value.UpperCupRadiusStandard;
            commonMasterMouldChangeParts.UpperCupRadiusVariation = value.UpperCupRadiusVariation;
            commonMasterMouldChangeParts.LowerCupRadiusStandardValue = value.LowerCupRadiusStandardValue;
            commonMasterMouldChangeParts.LowerCupRadiusVariationValue = value.LowerCupRadiusVariationValue;
            commonMasterMouldChangeParts.UpperCupDepthStandard = value.UpperCupDepthStandard;
            commonMasterMouldChangeParts.UpperCupDepthVariation = value.UpperCupDepthVariation;
            commonMasterMouldChangeParts.LowerCupDepthStandardValue = value.LowerCupDepthStandardValue;
            commonMasterMouldChangeParts.LowerCupDepthVariationValue = value.LowerCupDepthVariationValue;
            commonMasterMouldChangeParts.UpperLandStandard = value.UpperLandStandard;
            commonMasterMouldChangeParts.UpperLandVariation = value.UpperLandVariation;
            commonMasterMouldChangeParts.LowerLandStandardValue = value.LowerLandStandardValue;
            commonMasterMouldChangeParts.LowerLandVariationValue = value.LowerLandVariationValue;
            commonMasterMouldChangeParts.UpperBevelEdgeAngelStandard = value.UpperBevelEdgeAngelStandard;
            commonMasterMouldChangeParts.UpperBevelEdgeAngelVariation = value.UpperBevelEdgeAngelVariation;
            commonMasterMouldChangeParts.LowerBevelEdgeAngelStandardValue = value.LowerBevelEdgeAngelStandardValue;
            commonMasterMouldChangeParts.LowerBevelEdgeAngelVariationValue = value.LowerBevelEdgeAngelVariationValue;
            commonMasterMouldChangeParts.UpperBevelEdgeStandard = value.UpperBevelEdgeStandard;
            commonMasterMouldChangeParts.UpperBevelEdgeVariation = value.UpperBevelEdgeVariation;
            commonMasterMouldChangeParts.LowerBevelEdgeStandardValue = value.LowerBevelEdgeStandardValue;
            commonMasterMouldChangeParts.LowerBevelEdgeVariationValue = value.LowerBevelEdgeVariationValue;
            commonMasterMouldChangeParts.UpperWidthOfScoreStandard = value.UpperWidthOfScoreStandard;
            commonMasterMouldChangeParts.UpperWidthOfScoreVariation = value.UpperWidthOfScoreVariation;
            commonMasterMouldChangeParts.LowerWidthOfScoreStandardValue = value.LowerWidthOfScoreStandardValue;
            commonMasterMouldChangeParts.LowerWidthOfScoreVariationValue = value.LowerWidthOfScoreVariationValue;
            commonMasterMouldChangeParts.UpperDepthOfScoreStandard = value.UpperDepthOfScoreStandard;
            commonMasterMouldChangeParts.UpperDepthOfScoreVariation = value.UpperDepthOfScoreVariation;
            commonMasterMouldChangeParts.LowerDepthOfScoreStandardValue = value.LowerDepthOfScoreStandardValue;
            commonMasterMouldChangeParts.LowerDepthOfScoreVariationValue = value.LowerDepthOfScoreVariationValue;
            commonMasterMouldChangeParts.BoreIdmm = value.BoreIdmm;
            commonMasterMouldChangeParts.Odmm = value.Odmm;
            commonMasterMouldChangeParts.Heightmm = value.Heightmm;
            commonMasterMouldChangeParts.Embossement = value.EmbossementFlag == "Yes" ? true : false;
            commonMasterMouldChangeParts.PurchaseDate = value.PurchaseDate;
            commonMasterMouldChangeParts.TypeoftoolingId = value.TypeoftoolingId;
            commonMasterMouldChangeParts.Seriesno = value.Seriesno;
            commonMasterMouldChangeParts.Totalnumbers = value.Totalnumbers;
            var machineCommonFieldMultipleRemove = _context.PunchesMachine.Where(l => l.CommonMasterMouldChangePartsId == value.CommonMasterMouldChangePartsId).ToList();
            if (machineCommonFieldMultipleRemove.Count > 0)
            {
                _context.PunchesMachine.RemoveRange(machineCommonFieldMultipleRemove);
            }
            if (value.PallowInterchangeMachineIds != null)
            {
                value.PallowInterchangeMachineIds.ForEach(c =>
                {
                    var machineCommonFieldMultipleItems = new PunchesMachine
                    {
                        PunchesCommonMachineId = c,
                    };
                    commonMasterMouldChangeParts.PunchesMachine.Add(machineCommonFieldMultipleItems);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonMasterMouldChangeParts")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonMasterMouldChangeParts = _context.CommonMasterMouldChangeParts.Where(p => p.CommonMasterMouldChangePartsId == id).FirstOrDefault();
                if (commonMasterMouldChangeParts != null)
                {
                    var machineCommonFieldMultipleRemove = _context.PunchesMachine.Where(l => l.CommonMasterMouldChangePartsId == id).ToList();
                    if (machineCommonFieldMultipleRemove.Count > 0)
                    {
                        _context.PunchesMachine.RemoveRange(machineCommonFieldMultipleRemove);
                    }
                    var Documentbandlistline = _context.CommonMasterMouldChangePartsDocument.Where(p => p.SessionId == commonMasterMouldChangeParts.SessionId).Select(s => s.CommonMasterMouldChangePartsDocumentId).ToList();
                    if (Documentbandlistline != null)
                    {
                        Documentbandlistline.ForEach(s =>
                        {
                            var Documentbanditemline = _context.CommonMasterMouldChangePartsDocument.SingleOrDefault(p => p.CommonMasterMouldChangePartsDocumentId == s);
                            _context.CommonMasterMouldChangePartsDocument.Remove(Documentbanditemline);
                            _context.SaveChanges();
                        });

                    }
                    _context.CommonMasterMouldChangeParts.Remove(commonMasterMouldChangeParts);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId,string DocumentType)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new CommonMasterMouldChangePartsDocument
                {
                    DocumentType= DocumentType,
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.CommonMasterMouldChangePartsDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());
        }
        [HttpGet]
        [Route("GetDocument")]
        public List<CommonMasterMouldChangePartsDocumentModel> GetDocument(Guid? SessionId,string DocumentType)
        {
            var query = _context.CommonMasterMouldChangePartsDocument.Select(s => new CommonMasterMouldChangePartsDocumentModel
            {
                SessionId = s.SessionId,
                CommonMasterMouldChangePartsDocumentId = s.CommonMasterMouldChangePartsDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                DocumentType=s.DocumentType,
                UploadDate=s.UploadDate,
            }).Where(l => l.SessionId == SessionId && l.DocumentType== DocumentType).OrderByDescending(o => o.CommonMasterMouldChangePartsDocumentId).ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadDocument")]
        public IActionResult DownLoadDocument(long id)
        {
            var document = _context.CommonMasterMouldChangePartsDocument.SingleOrDefault(t => t.CommonMasterMouldChangePartsDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteDocument")]
        public void MachineDocuments(int id)
        {

            var query = string.Format("delete from CommonMasterMouldChangePartsDocument Where CommonMasterMouldChangePartsDocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
            //var document = _context.CommonMasterMouldChangePartsDocument.SingleOrDefault(p => p.CommonMasterMouldChangePartsDocumentId == id);
            //if (document != null)
            //{
            //    _context.CommonMasterMouldChangePartsDocument.Remove(document);
            //    _context.SaveChanges();
            //}
        }
    }
}