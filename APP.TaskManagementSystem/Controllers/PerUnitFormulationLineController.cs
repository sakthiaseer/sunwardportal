﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PerUnitFormulationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PerUnitFormulationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("PerUnitFormulationLineByID")]
        public List<PerUnitFormulationLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var items = _context.Navitems.ToList();
            List<PerUnitFormulationLineModel> perUnitFormulationLineModels = new List<PerUnitFormulationLineModel>();
            var perUnitFormulationLine = _context.PerUnitFormulationLine.Include("AddedByUser").Include("ModifiedByUser").Where(t => t.FinishProductExcipintId == id).OrderByDescending(o => o.PerUnitFormulationLineId).AsNoTracking().ToList();
            if(perUnitFormulationLine!=null && perUnitFormulationLine.Count > 0)
            {
                perUnitFormulationLine.ForEach(s =>
                {
                    PerUnitFormulationLineModel perUnitFormulationLineModel = new PerUnitFormulationLineModel
                    {
                        PerUnitFormulationLineID = s.PerUnitFormulationLineId,
                        FinishProductExcipintID = s.FinishProductExcipintId,
                        ManufacturingProcessID = s.ManufacturingProcessId,
                        ManufacturingStepsID = s.ManufacturingStepsId,
                        ManufacturingSteps = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(a => a.Value).SingleOrDefault() : "",
                        ManufacturingProcess = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    perUnitFormulationLineModels.Add(perUnitFormulationLineModel);

                });
            }
            return perUnitFormulationLineModels;
        }



        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PerUnitFormulationLineModel> GetData(SearchModel searchModel)
        {
            var perUnitFormulationLine = new PerUnitFormulationLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).LastOrDefault();
                        break;
                    case "Next":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).LastOrDefault();
                        break;
                    case "Previous":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).LastOrDefault();
                        break;
                    case "Next":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderBy(o => o.PerUnitFormulationLineId).FirstOrDefault(s => s.PerUnitFormulationLineId > searchModel.Id);
                        break;
                    case "Previous":
                        perUnitFormulationLine = _context.PerUnitFormulationLine.OrderByDescending(o => o.PerUnitFormulationLineId).FirstOrDefault(s => s.PerUnitFormulationLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PerUnitFormulationLineModel>(perUnitFormulationLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPerUnitFormulationLine")]
        public PerUnitFormulationLineModel Post(PerUnitFormulationLineModel value)
        {
            var items = _context.Navitems.AsNoTracking().ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var perUnitFormulationLine = new PerUnitFormulationLine
            {
                FinishProductExcipintId = value.FinishProductExcipintID,
                ManufacturingProcessId = value.ManufacturingProcessID,
                ManufacturingStepsId = value.ManufacturingStepsID,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,




            };
            _context.PerUnitFormulationLine.Add(perUnitFormulationLine);
            _context.SaveChanges();
            value.PerUnitFormulationLineID = perUnitFormulationLine.PerUnitFormulationLineId;
            value.ManufacturingProcess = value.ManufacturingProcessID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturingProcessID).Select(m => m.Value).FirstOrDefault() : "";
            value.ManufacturingSteps = value.ManufacturingStepsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturingStepsID).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePerUnitFormulationLine")]
        public PerUnitFormulationLineModel Put(PerUnitFormulationLineModel value)
        {
            var items = _context.Navitems.AsNoTracking().ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var perUnitFormulationLine = _context.PerUnitFormulationLine.SingleOrDefault(p => p.PerUnitFormulationLineId == value.PerUnitFormulationLineID);
            perUnitFormulationLine.PerUnitFormulationLineId = value.PerUnitFormulationLineID;
            perUnitFormulationLine.FinishProductExcipintId = value.FinishProductExcipintID;
            perUnitFormulationLine.ManufacturingProcessId = value.ManufacturingProcessID;
            perUnitFormulationLine.ManufacturingStepsId = value.ManufacturingStepsID;
            perUnitFormulationLine.ModifiedByUserId = value.ModifiedByUserID;
            perUnitFormulationLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.ManufacturingProcess = value.ManufacturingProcessID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturingProcessID).Select(m => m.Value).FirstOrDefault() : "";
            value.ManufacturingSteps = value.ManufacturingStepsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturingStepsID).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePerUnitFormulationLine")]
        public void Delete(int id)
        {
            var perUnitFormulationLine = _context.PerUnitFormulationLine.SingleOrDefault(p => p.PerUnitFormulationLineId == id);
            if (perUnitFormulationLine != null)
            {
                _context.PerUnitFormulationLine.Remove(perUnitFormulationLine);
                _context.SaveChanges();
            }
        }
    }
}