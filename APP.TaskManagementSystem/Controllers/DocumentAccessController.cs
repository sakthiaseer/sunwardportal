﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentAccessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentAccessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentAccess")]
        public List<long?> Get(int id)
        {
            var documentRights = _context.DocumentRights.Where(d => d.DocumentId == id).Select(s => s.UserId).ToList();
            return documentRights;
        }
        [HttpGet]
        [Route("GetDocumentAccessUsers")]
        public List<DocumentRightsModel> Getusers(int id)
        {
            var documentRights = _context.DocumentRights.Where(u => u.DocumentId == id).ToList();
            List<DocumentRightsModel> documentRightsModel = new List<DocumentRightsModel>();
            documentRights.ForEach(s =>
            {
                DocumentRightsModel documentRightsModels = new DocumentRightsModel
                {
                    DocumentID = s.DocumentId,
                    IsRead = s.IsRead,
                    IsReadWrite = s.IsReadWrite,
                    UserID = s.UserId,
                };
                documentRightsModel.Add(documentRightsModels);
            });
            //documentRightsModel= documentRightsModel.OrderByDescending(a => a.DocumentID).Where(u => u.DocumentID == id).Distinct().ToList();
            return documentRightsModel.ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get DocumentRights")]
        [HttpGet("GetDocumentAccess/{id:int}")]
        public ActionResult<DocumentAccessModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentRights = _context.DocumentRights.SingleOrDefault(p => p.DocumentAccessId == id.Value);
            var result = _mapper.Map<DocumentAccessModel>(documentRights);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentAccessModel> GetData(SearchModel searchModel)
        {
            var documentRights = new DocumentRights();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).LastOrDefault();
                        break;
                    case "Next":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).LastOrDefault();
                        break;
                    case "Previous":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).LastOrDefault();
                        break;
                    case "Next":
                        documentRights = _context.DocumentRights.OrderBy(o => o.DocumentAccessId).FirstOrDefault(s => s.DocumentAccessId > searchModel.Id);
                        break;
                    case "Previous":
                        documentRights = _context.DocumentRights.OrderByDescending(o => o.DocumentAccessId).FirstOrDefault(s => s.DocumentAccessId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentAccessModel>(documentRights);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentAccess")]
        public DocumentAccessModel Post(DocumentAccessModel value)
        {
            var documentRights = new DocumentRights();
            if (value.UserIDs != null)
            {
                value.UserIDs.ForEach(u =>
                {
                    documentRights = new DocumentRights
                    {


                        UserId = u,
                        DocumentId = value.DocumentID,

                    };
                    _context.DocumentRights.Add(documentRights);
                });
            }
            _context.SaveChanges();
            value.DocumentAccessID = documentRights.DocumentAccessId;
            return value;
        }
        [HttpPut]
        [Route("UpdateDocumentRights")]
        public DocumentsModel UpdateDocumentRights(DocumentsModel value)
        {

            var taskAssignedList = _context.TaskAssigned.Where(t => t.TaskId == value.LinkID).ToList();
            var taskmaster = _context.TaskMaster.Where(t => t.TaskId == value.LinkID).FirstOrDefault();
            var taskassigned = _context.TaskAssigned.Where(t => t.TaskId == value.LinkID).FirstOrDefault();
            if (value.AssignedToIds != null)
            {
                value.AssignedToIds.ForEach(c =>
                {
                    var existingUser = _context.TaskAssigned.Where(t => t.TaskId == value.LinkID && t.UserId == c).FirstOrDefault();
                    if (existingUser == null)
                    {
                        var assigned = new TaskAssigned
                        {
                            UserId = c,
                            StatusCodeId = taskmaster.StatusCodeId,
                            DueDate = taskmaster.DueDate,
                            AssignedDate = DateTime.Now,
                            TaskOwnerId = taskmaster.OnBehalf.HasValue ? taskmaster.FollowUp == "true" ? taskmaster.AddedByUserId : taskmaster.OnBehalf : taskmaster.AddedByUserId,
                            IsRead = false,
                            StartDate = taskassigned.StartDate,
                            OnBehalfId = value.OnBehalfID != null ? value.OnBehalfID : taskmaster.OnBehalf,
                            TaskId = value.LinkID,
                        };
                        _context.TaskAssigned.Add(assigned);
                        _context.SaveChanges();
                    }
                });
            }

            if (value.AssignedCCIds != null)
            {
                value.AssignedCCIds.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var existingUser = _context.TaskMembers.Where(t => t.TaskId == value.LinkID && t.AssignedCc == c).FirstOrDefault();
                        if (existingUser == null)
                        {
                            var member = new TaskMembers
                            {
                                AssignedCc = c,
                                IsRead = false,
                                DueDate = taskmaster.DueDate,
                                StatusCodeId = taskmaster.StatusCodeId,
                                TaskId = value.LinkID,
                            };
                            _context.TaskMembers.Add(member);
                            _context.SaveChanges();
                        }
                    }
                });
            }
            if (value.DocumentList != null && value.DocumentList.Count > 0)
            {

                value.DocumentList.ForEach(d =>
                {
                    if (value.AssignedCCIds != null)
                    {
                        value.AssignedCCIds.ForEach(i =>
                        {
                            var existingUser = _context.DocumentRights.Where(t => t.DocumentId == value.LinkID && t.UserId == i).FirstOrDefault();
                            if (existingUser == null)
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = d.IsRead,
                                    IsReadWrite = d.IsEdit,
                                    UserId = i,
                                    DocumentId = d.DocumentID,

                                };
                                _context.DocumentRights.Add(docAccess);
                                _context.SaveChanges();
                            }

                        });
                    }
                    if (value.AssignedToIds != null)
                    {
                        value.AssignedToIds.ForEach(i =>
                        {
                            var existingUser = _context.DocumentRights.Where(t => t.DocumentId == value.LinkID && t.UserId == i).FirstOrDefault();
                            if (existingUser == null)
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = d.IsRead,
                                    IsReadWrite = d.IsEdit,
                                    UserId = i,
                                    DocumentId = d.DocumentID,
                                };
                                _context.DocumentRights.Add(docAccess);
                                _context.SaveChanges();
                            }
                        });
                    }

                });
            }
            _context.SaveChanges();
            return value;
        }
        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateDocumentAccess")]
        public DocumentAccessModel Put(DocumentAccessModel value)
        {
            var documentRights = _context.DocumentRights.SingleOrDefault(p => p.DocumentAccessId == value.DocumentAccessID);
            documentRights.UserId = value.UserID;
            documentRights.DocumentId = value.DocumentID;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteDocumentAccess")]
        public void Delete(int id)
        {
            var documentRights = _context.DocumentRights.SingleOrDefault(p => p.DocumentAccessId == id);
            if (documentRights != null)
            {
                _context.DocumentRights.Remove(documentRights);
                _context.SaveChanges();
            }
        }
    }
}