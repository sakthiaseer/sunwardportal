﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesItemPackingInforLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SalesItemPackingInforLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSalesItemPackingInfoLine")]
        public List<SalesItemPackingInfoLineModel> Get(int? id)
        {
            List<SalesItemPackingInfoLineModel> salesItemPackingInfoLineModels = new List<SalesItemPackingInfoLineModel>();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var saleItemPackInfoLine = _context.SalesItemPackingInforLine.Include("AddedByUser").Include("ModifiedByUser").Include(l => l.SalesItemPackingRequirement).Include(s => s.StatusCode).Where(l => l.SalesItemPackingInfoId == id).OrderByDescending(o => o.SalesItemPackingInfoLineId).AsNoTracking().ToList();
            if (saleItemPackInfoLine != null && saleItemPackInfoLine.Count > 0)
            {
                var masterDetailIds = saleItemPackInfoLine?.Where(s=>s.SalesPerPack!=null).Select(s => s.SalesPerPack).ToList();
                if (masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }

                saleItemPackInfoLine.ForEach(s =>
                {
                    SalesItemPackingInfoLineModel salesItemPackingInfoLineModel = new SalesItemPackingInfoLineModel
                    {
                        Fpname = s.Fpname,
                        SalesFactor = s.SalesFactor,
                        SalesItemPackingInfoId = s.SalesItemPackingInfoId,
                        SalesItemPackingInfoLineId = s.SalesItemPackingInfoLineId,
                        SalesPerPack = s.SalesPerPack,
                        SalesPerPackName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SalesPerPack) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SalesPerPack).Value : "",
                        PackageRequirementIds = s.SalesItemPackingRequirement.Where(l => l.SalesItemPackingInfoLineId == s.SalesItemPackingInfoLineId).Select(r => r.PackingRequirementId).ToList(),
                        StatusCode = s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser.UserName,
                        ModifiedByUser = s.ModifiedByUser.UserName,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    salesItemPackingInfoLineModels.Add(salesItemPackingInfoLineModel);
                });
            }
            return salesItemPackingInfoLineModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SalesItemPackingInfoLineModel> GetData(SearchModel searchModel)
        {
            var salesItemPackingInfoLine = new SalesItemPackingInforLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).FirstOrDefault();
                        break;
                    case "Last":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).LastOrDefault();
                        break;
                    case "Next":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).LastOrDefault();
                        break;
                    case "Previous":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).FirstOrDefault();
                        break;
                    case "Last":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).LastOrDefault();
                        break;
                    case "Next":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderBy(o => o.SalesItemPackingInfoLineId).FirstOrDefault(s => s.SalesItemPackingInfoLineId > searchModel.Id);
                        break;
                    case "Previous":
                        salesItemPackingInfoLine = _context.SalesItemPackingInforLine.OrderByDescending(o => o.SalesItemPackingInfoLineId).FirstOrDefault(s => s.SalesItemPackingInfoLineId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SalesItemPackingInfoLineModel>(salesItemPackingInfoLine);
            if (result != null)
            {
                result.PackageRequirementIds = _context.SalesItemPackingRequirement.Where(s => s.SalesItemPackingInfoLineId == result.SalesItemPackingInfoLineId).AsNoTracking().Select(l => l.PackingRequirementId).ToList();
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSalesItemPackInfoLine")]
        public SalesItemPackingInfoLineModel Post(SalesItemPackingInfoLineModel value)
        {
            var salesItemPackingInfoLine = new SalesItemPackingInforLine
            {
                Fpname = value.Fpname,
                SalesFactor = value.SalesFactor,
                SalesItemPackingInfoId = value.SalesItemPackingInfoId,
                SalesPerPack = value.SalesPerPack,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            value.PackageRequirementIds.ForEach(l =>
            {
                SalesItemPackingRequirement salesItemPackingRequirement = new SalesItemPackingRequirement
                {
                    PackingRequirementId = l,
                    SalesItemPackingInfoLineId = value.SalesItemPackingInfoLineId,
                };
                salesItemPackingInfoLine.SalesItemPackingRequirement.Add(salesItemPackingRequirement);
            });
            _context.SalesItemPackingInforLine.Add(salesItemPackingInfoLine);
            _context.SaveChanges();
            value.SalesItemPackingInfoLineId = salesItemPackingInfoLine.SalesItemPackingInfoLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSalesItemPackingInfoLine")]
        public SalesItemPackingInfoLineModel Put(SalesItemPackingInfoLineModel value)
        {
            var salesItemPackingInfoLine = _context.SalesItemPackingInforLine.Include(l => l.SalesItemPackingRequirement).SingleOrDefault(p => p.SalesItemPackingInfoLineId == value.SalesItemPackingInfoLineId);
            salesItemPackingInfoLine.Fpname = value.Fpname;
            salesItemPackingInfoLine.SalesFactor = value.SalesFactor;
            salesItemPackingInfoLine.SalesItemPackingInfoId = value.SalesItemPackingInfoId;
            salesItemPackingInfoLine.SalesPerPack = value.SalesPerPack;
            salesItemPackingInfoLine.ModifiedByUserId = value.ModifiedByUserID;
            salesItemPackingInfoLine.ModifiedDate = DateTime.Now;
            salesItemPackingInfoLine.StatusCodeId = value.StatusCodeID.Value;
            if (salesItemPackingInfoLine.SalesItemPackingRequirement != null)
            {
                _context.SalesItemPackingRequirement.RemoveRange(salesItemPackingInfoLine.SalesItemPackingRequirement);
                _context.SaveChanges();
            }
            value.PackageRequirementIds.ForEach(l =>
            {
                SalesItemPackingRequirement salesItemPackingRequirement = new SalesItemPackingRequirement
                {
                    PackingRequirementId = l,
                    SalesItemPackingInfoLineId = value.SalesItemPackingInfoLineId
                };
                _context.SalesItemPackingRequirement.Add(salesItemPackingRequirement);
            });
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSalesItemPackingInfoLine")]
        public void Delete(int id)
        {
            var salesItemPackingInforLine = _context.SalesItemPackingInforLine.SingleOrDefault(p => p.SalesItemPackingInfoLineId == id);
            if (salesItemPackingInforLine != null)
            {
                var salesItemPackingRequirement = _context.SalesItemPackingRequirement.Where(s => s.SalesItemPackingInfoLineId == id).AsNoTracking().ToList();
                if (salesItemPackingRequirement != null)
                {
                    _context.SalesItemPackingRequirement.RemoveRange(salesItemPackingRequirement);
                    _context.SaveChanges();
                }
                _context.SalesItemPackingInforLine.Remove(salesItemPackingInforLine);
                _context.SaveChanges();
            }
        }
    }
}