﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ClassificationBmrLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ClassificationBmrLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetClassificationBmrLine")]
        public List<ClassificationBmrLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var classificationBmrLine = _context.ClassificationBmrLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Where(s => s.ClassificationBmrId == id)
                .AsNoTracking()
                .ToList();
            List<ClassificationBmrLineModel> ClassificationBmrLineModels = new List<ClassificationBmrLineModel>();
            classificationBmrLine.ForEach(s =>
            {
                ClassificationBmrLineModel classificationBmrLineModel = new ClassificationBmrLineModel
                {
                    ClassificationBmrLineId = s.ClassificationBmrLineId,
                    ClassificationBmrId = s.ClassificationBmrId,
                    BatchNo = s.BatchNo,
                    SupplyTo = s.SupplyTo,
                    ProductNo = s.ProductNo,
                    ProductName = s.ProductName,
                    ProductUom=s.ProductUom,
                    OutputQty = s.OutputQty,
                    ExpiryDate = s.ExpiryDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                ClassificationBmrLineModels.Add(classificationBmrLineModel);
            });
            return ClassificationBmrLineModels.OrderByDescending(a => a.ClassificationBmrLineId).ToList();
        }


        [HttpPost]
        [Route("InsertClassificationBmrLine")]
        public ClassificationBmrLineModel Post(ClassificationBmrLineModel value)
        {
            var classificationBmrLine = new ClassificationBmrLine
            {
                ClassificationBmrId = value.ClassificationBmrId,
                BatchNo = value.BatchNo,
                SupplyTo = value.SupplyTo,
                ProductNo = value.ProductNo,
                ProductName = value.ProductName,
                OutputQty = value.OutputQty,
                ExpiryDate = value.ExpiryDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                ProductUom=value.ProductUom,
            };
            _context.ClassificationBmrLine.Add(classificationBmrLine);
            _context.SaveChanges();
            value.ClassificationBmrLineId = classificationBmrLine.ClassificationBmrLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateClassificationBmrLine")]
        public ClassificationBmrLineModel Put(ClassificationBmrLineModel value)
        {
            var classificationBmrLine = _context.ClassificationBmrLine.SingleOrDefault(p => p.ClassificationBmrLineId == value.ClassificationBmrLineId);
            classificationBmrLine.ClassificationBmrId = value.ClassificationBmrId;
            classificationBmrLine.BatchNo = value.BatchNo;
            classificationBmrLine.SupplyTo = value.SupplyTo;
            classificationBmrLine.ProductNo = value.ProductNo;
            classificationBmrLine.ProductName = value.ProductName;
            classificationBmrLine.OutputQty = value.OutputQty;
            classificationBmrLine.ExpiryDate = value.ExpiryDate;
            classificationBmrLine.StatusCodeId = value.StatusCodeID.Value;
            classificationBmrLine.ModifiedByUserId = value.ModifiedByUserID;
            classificationBmrLine.ModifiedDate = DateTime.Now;
            classificationBmrLine.ProductUom = value.ProductUom;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteClassificationBmrLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var classificationBmrLine = _context.ClassificationBmrLine.Where(p => p.ClassificationBmrLineId == id).FirstOrDefault();
                if (classificationBmrLine != null)
                {
                    _context.ClassificationBmrLine.Remove(classificationBmrLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}