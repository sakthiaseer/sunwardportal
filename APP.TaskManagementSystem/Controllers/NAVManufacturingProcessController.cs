﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NAVManufacturingProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHubContext<ChatHub> _hub;
        private readonly IHostingEnvironment _hostingEnvironment;

        public NAVManufacturingProcessController(CRT_TMSContext context, IMapper mapper, IHubContext<ChatHub> hub, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _hub = hub;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetNavManufacturingProcess")]
        public List<NAVManufacturingProcessModel> Get(int id)
        {
            var navManufacturingprocess = _context.NavManufacturingProcess
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.Company)
                .Include(i => i.Item).Where(t => t.CompanyId == id)
               .AsNoTracking()
                .ToList();
            List<NAVManufacturingProcessModel> nAVManufacturingProcessModels = new List<NAVManufacturingProcessModel>();
            navManufacturingprocess.ForEach(s =>
            {
                NAVManufacturingProcessModel nAVManufacturingProcessModel = new NAVManufacturingProcessModel
                {
                    ManufacturingProcessID = s.ManufacturingProcessId,
                    PigeonHoleVersion = s.PigeonHoleVersion,
                    CompanyId = s.CompanyId,
                    Process = s.Process,
                    NoChangeinDate = s.NoChangeinDate,
                    ReplanRefNo = s.ReplanRefNo,
                    ProdOrderNo = s.ProdOrderNo,
                    ItemId = s.ItemId,
                    ItemNo = s.Item?.No,
                    Description = s.Description,
                    Description2 = s.Description2,
                    StartingDate = s.StartingDate,
                    CompletionDate = s.CompletionDate,
                    ReleaseFromPlanner = s.ReleaseFromPlanner,
                    Substatus = s.Substatus,
                    Remarks = s.Remarks,


                };
                nAVManufacturingProcessModels.Add(nAVManufacturingProcessModel);
            });
            return nAVManufacturingProcessModels;
        }
        [HttpGet]
        [Route("GetNavManufacturingProcessAllItems")]
        public List<NAVManufacturingProcessModel> GetNavManufacturingProcessAllItems()
        {
            var navManufacturingprocess = _context.NavManufacturingProcess.AsNoTracking().ToList();
            List<NAVManufacturingProcessModel> nAVManufacturingProcessModels = new List<NAVManufacturingProcessModel>();
            navManufacturingprocess.ForEach(s =>
            {
                NAVManufacturingProcessModel nAVManufacturingProcessModel = new NAVManufacturingProcessModel
                {
                    ManufacturingProcessID = s.ManufacturingProcessId,
                    CompanyId = s.CompanyId,
                    Process = s.Process,
                    ReplanRefNo = s.ReplanRefNo,
                    ProdOrderNo = s.ProdOrderNo,
                    ItemNo = s.ItemNo,
                    Description = s.Description,
                    StartingDate = s.StartingDate,
                    CompletionDate = s.CompletionDate,
                    Description2 = s.Description2,
                    DescriptionDetails = s.Description + "|" + s.Description2,
                };
                nAVManufacturingProcessModels.Add(nAVManufacturingProcessModel);
            });
            return nAVManufacturingProcessModels;
        }
        [HttpPost]
        [Route("GetNavManufacturingProcessByIds")]
        public List<NAVManufacturingProcessModel> GetNavManufacturingProcessAll(List<string> ids)
        {
            var navManufacturingprocess = _context.NavManufacturingProcess.Where(c => ids.Contains(c.ReplanRefNo)).AsNoTracking().ToList();
            List<NAVManufacturingProcessModel> nAVManufacturingProcessModels = new List<NAVManufacturingProcessModel>();
            navManufacturingprocess.ForEach(s =>
            {
                if (!nAVManufacturingProcessModels.Any(a => a.Process == s.Process))
                {
                    NAVManufacturingProcessModel nAVManufacturingProcessModel = new NAVManufacturingProcessModel
                    {
                        ManufacturingProcessID = s.ManufacturingProcessId,
                        CompanyId = s.CompanyId,
                        Process = s.Process,
                    };
                    nAVManufacturingProcessModels.Add(nAVManufacturingProcessModel);
                }
            });
            return nAVManufacturingProcessModels;
        }

        [HttpGet]
        [Route("GetProductionOrderByCompany")]
        public List<NAVManufacturingProcessModel> GetProductionOrderByCompany(int id)
        {
            var navManufacturingprocess = _context.NavManufacturingProcess.Where(c => c.CompanyId == id).AsNoTracking().ToList();
            List<NAVManufacturingProcessModel> nAVManufacturingProcessModels = new List<NAVManufacturingProcessModel>();
            navManufacturingprocess.ForEach(s =>
            {
                if (!nAVManufacturingProcessModels.Any(a => a.Process == s.Process))
                {
                    NAVManufacturingProcessModel nAVManufacturingProcessModel = new NAVManufacturingProcessModel
                    {
                        ManufacturingProcessID = s.ManufacturingProcessId,
                        ProdOrderNo = s.ProdOrderNo
                    };
                    nAVManufacturingProcessModels.Add(nAVManufacturingProcessModel);
                }
            });
            return nAVManufacturingProcessModels;
        }

        [HttpGet]
        [Route("GetReplanRefNoByCompany")]
        public List<NAVManufacturingProcessModel> GetReplanRefNoByCompany(int id)
        {
            var navManufacturingprocess = _context.NavManufacturingProcess.Where(c => c.CompanyId == id).AsNoTracking().ToList();
            List<NAVManufacturingProcessModel> nAVManufacturingProcessModels = new List<NAVManufacturingProcessModel>();
            navManufacturingprocess.ForEach(s =>
            {
                if (!nAVManufacturingProcessModels.Any(a => a.Process == s.Process))
                {
                    NAVManufacturingProcessModel nAVManufacturingProcessModel = new NAVManufacturingProcessModel
                    {
                        ManufacturingProcessID = s.ManufacturingProcessId,
                        ProdOrderNo = s.ProdOrderNo,
                        ReplanRefNo = s.ReplanRefNo
                    };
                    nAVManufacturingProcessModels.Add(nAVManufacturingProcessModel);
                }
            });
            return nAVManufacturingProcessModels;
        }

        [HttpPost]
        [Route("GetProcessAvailablity")]
        public NavManufacturingProcessItem GetProcessAvailablity(ProcessAvailabilityParam processAvailabilityParam)
        {
            List<string> processes = new List<string>();
            NavManufacturingProcessItem navManufacturingProcessItem = new NavManufacturingProcessItem();
            navManufacturingProcessItem.AvailableDates = new List<NavManufacturingDateRange>();
            var navManufacturingprocess = _context.NavManufacturingProcess.Where(t => t.CompanyId == processAvailabilityParam.CompanyId).AsNoTracking().ToList();
            if (processAvailabilityParam.ReplanNos.Count > 0)
            {
                navManufacturingprocess = navManufacturingprocess.Where(p => processAvailabilityParam.ReplanNos.Contains(p.ReplanRefNo)).ToList();
                processes = navManufacturingprocess.Select(r => r.Process).ToList();
                if (!string.IsNullOrEmpty(processAvailabilityParam.Process))
                {
                    processes.Clear();
                    processes.Add(processAvailabilityParam.Process);
                }
            }

            var processGrouping = navManufacturingprocess.GroupBy(n => n.Process).ToList();

            if (processes.Count > 0)
            {
                processes = processes.Distinct().ToList();
                processGrouping = processGrouping.Where(c => processes.Contains(c.Key)).ToList();
                var navManufacturingProcesses = processGrouping.SelectMany(i => i.ToList());
                List<long?> itemsIds = navManufacturingProcesses.Select(i => i.ItemId).ToList();
                var startDate = navManufacturingProcesses.Min(s => s.StartingDate);
                var endDate = navManufacturingProcesses.Max(s => s.CompletionDate);
                List<DateTime> allDates = new List<DateTime>();
                List<DateTime> processDates = new List<DateTime>();
                var navItems = _context.Navitems.Where(n => itemsIds.Contains(n.ItemId)).ToList();
                for (DateTime i = startDate.Value; i <= endDate.Value; i = i.AddDays(1))
                {
                    allDates.Add(i.Date);
                }
                foreach (var groupItem in processGrouping)
                {
                    List<DateTime> currentProcessDates = new List<DateTime>();
                    foreach (var item in groupItem.ToList())
                    {
                        var navItem = navItems.FirstOrDefault(n => n.ItemId == item.ItemId);
                        for (DateTime i = item.StartingDate.Value; i <= item.CompletionDate.Value; i = i.AddDays(1))
                        {
                            processDates.Add(i.Date);
                            if (!currentProcessDates.Any(s => s == i.Date))
                            {
                                currentProcessDates.Add(i.Date);
                            }
                        }

                        currentProcessDates.ForEach(p =>
                        {
                            navManufacturingProcessItem.AvailableDates.Add(new NavManufacturingDateRange
                            {
                                Start = p.ToString("yyyy-MM-dd"),
                                End = p.ToString("yyyy-MM-dd"),
                                Date = p.ToString("yyyy-MM-dd"),
                                CurrentDate = p,
                                Title = item.ProdOrderNo,
                                ItemNo = navItem.No,
                                Process = item.Process,
                                ItemDetail = navItem.Description + "|" + navItem.BaseUnitofMeasure,
                                CssClass = "red",
                                StartDate = p,
                                EndDate = p,
                                CompanyId = item.CompanyId,
                            });

                        });
                    }
                }

                if (processAvailabilityParam.StartDate != null && processAvailabilityParam.EndDate != null)
                {
                    navManufacturingProcessItem.AvailableDates = navManufacturingProcessItem.AvailableDates.Where(a => (a.StartDate >= processAvailabilityParam.StartDate && a.StartDate <= processAvailabilityParam.EndDate) || (a.EndDate >= processAvailabilityParam.StartDate && a.EndDate <= processAvailabilityParam.EndDate)).ToList();
                }
            }



            return navManufacturingProcessItem;
        }

        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult Upload(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            var plant = files["plantId"].ToString();
            var user = files["userId"].ToString();
            long userId = !string.IsNullOrEmpty(user) ? long.Parse(user) : -1;
            var companyId = !string.IsNullOrEmpty(plant) ? long.Parse(plant) : -1;
            DataTable dt = new DataTable();
            files.Files.ToList().ForEach(f =>
            {
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Importing excel file data.");

                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var fileName = file.FileName;

                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + fileName;
                System.IO.File.WriteAllBytes(serverPath, document);
                var fileInfo = new FileInfo(serverPath);
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Reading excel file information.");
                using (XLWorkbook workbook = new XLWorkbook(serverPath))
                {
                    IXLWorksheet worksheet = workbook.Worksheet(1);
                    bool FirstRow = true;
                    //Range for reading the cells based on the last cell used.  
                    string readRange = "1:1";
                    _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Processing excel file data.");
                    foreach (IXLRow row in worksheet.RowsUsed())
                    {
                        //If Reading the First Row (used) then add them as column name  
                        if (FirstRow)
                        {
                            //Checking the Last cellused for column generation in datatable  
                            readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                            foreach (IXLCell cell in row.Cells(readRange))
                            {
                                dt.Columns.Add(cell.Value.ToString());
                            }
                            FirstRow = false;
                        }
                        else
                        {
                            //Adding a Row in datatable  
                            dt.Rows.Add();
                            int cellIndex = 0;
                            //Updating the values of datatable  
                            foreach (IXLCell cell in row.Cells(readRange))
                            {
                                dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                cellIndex++;
                            }
                        }
                    }
                    //If no data in Excel file  
                    if (FirstRow)
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                    }
                }
            });

            var navManufacturingProcessItems = (from DataRow row in dt.Rows

                                                select new NavManufacturingProcess
                                                {
                                                    PigeonHoleVersion = row["PigeonHoleVersion"].ToString(),
                                                    Process = row["Process"].ToString(),
                                                    NoChangeinDate = row["NoChangeinDate"].ToString() == "No" ? false : true,
                                                    ReplanRefNo = row["ReplanRefNo"].ToString(),
                                                    ProdOrderNo = row["ProdOrderNo"].ToString(),
                                                    ItemNo = row["ItemNo"].ToString(),
                                                    Description = row["Description"].ToString(),
                                                    Description2 = row["Description2"].ToString(),
                                                    StartingDate = GetDate(row["StartingDate"].ToString()),
                                                    CompletionDate = GetDate(row["CompletionDate"].ToString()),
                                                    ReleaseFromPlanner = row["ReleaseFromPlanner"].ToString() == "No" ? false : true,
                                                    Substatus = row["Substatus"].ToString(),
                                                    Remarks = row["Remarks"].ToString(),
                                                }).ToList();

            if (navManufacturingProcessItems.Count > 0)
            {
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Clearing exsiting tender information.");
                var items = _context.Navitems.ToList();
                int count = 0;
                int totalItems = navManufacturingProcessItems.Count();

                navManufacturingProcessItems.ForEach(f =>
                {
                    var itemCount = count + " of " + totalItems;
                    //notify client progress update
                    var itemName = string.Format("{0} {1}-{2}  {3}", itemCount, f.ItemNo, f.Description, "tender items");
                    _hub.Clients.Group(userId.ToString()).SendAsync("progress", itemName);
                    var navManufacturingProcessExist = _context.NavManufacturingProcess.FirstOrDefault(c => c.ReplanRefNo == f.ReplanRefNo && c.ProdOrderNo == f.ProdOrderNo && c.ItemNo == f.ItemNo);

                    if (navManufacturingProcessExist != null)
                    {
                        var itemMas = _context.Navitems.FirstOrDefault(f => f.No == navManufacturingProcessExist.ItemNo && f.CompanyId == companyId);
                        navManufacturingProcessExist.PigeonHoleVersion = f.PigeonHoleVersion;
                        navManufacturingProcessExist.Process = f.Process;
                        navManufacturingProcessExist.NoChangeinDate = f.NoChangeinDate;
                        navManufacturingProcessExist.ReplanRefNo = f.ReplanRefNo;
                        navManufacturingProcessExist.ProdOrderNo = f.ProdOrderNo;
                        navManufacturingProcessExist.ItemNo = f.ItemNo;
                        navManufacturingProcessExist.Description = f.Description;
                        navManufacturingProcessExist.Description2 = f.Description2;
                        navManufacturingProcessExist.StartingDate = f.StartingDate;
                        navManufacturingProcessExist.CompletionDate = f.CompletionDate;
                        navManufacturingProcessExist.ReleaseFromPlanner = f.ReleaseFromPlanner;
                        navManufacturingProcessExist.Substatus = f.Substatus;
                        navManufacturingProcessExist.Remarks = f.Remarks;
                        navManufacturingProcessExist.ItemId = itemMas?.ItemId;
                        navManufacturingProcessExist.CompanyId = companyId;
                    }
                    else
                    {
                        var itemMas = _context.Navitems.FirstOrDefault(f => f.No == navManufacturingProcessExist.ItemNo && f.CompanyId == companyId);
                        f.CompanyId = companyId;
                        f.ItemId = itemMas?.ItemId;
                        _context.NavManufacturingProcess.Add(f);
                    }
                    count++;
                });
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Submit information to portal database");
                _context.SaveChanges();

                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Completed");
            }
            return Content(SessionId.ToString());
        }

        private DateTime? GetDate(string dateString)
        {
            if (!string.IsNullOrEmpty(dateString))
            {
                DateTime date = DateTime.Parse(dateString);
                return date;
            }
            return null;
        }
    }
}