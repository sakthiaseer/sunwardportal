﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingPvcFoilController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingPvcFoilController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingPvcFoils")]
        public List<PackagingPvcfoilModel> Get()
        {
            List<PackagingPvcfoilModel> packagingPvcfoilModels = new List<PackagingPvcfoilModel>();
            var packagingPvcFoils = _context.PackagingPvcfoil.Include(a=>a.AddedByUser).
                Include(b=>b.ModifiedByUser).
                Include(d=>d.PackagingPvcfoilBlister).
                Include(c=>c.PvcFoilType).OrderByDescending(o => o.PvcFoilId).AsNoTracking().ToList();
            packagingPvcFoils.ForEach(s =>
            {
                List<long> masterIds = packagingPvcFoils.Where(w => w.PvcTypeId != null).Select(a => a.PvcTypeId.Value).Distinct().ToList();
                var packagingPvcfoilIds = packagingPvcFoils.Select(s => s.PvcFoilId).ToList();
                List<long> PackagingPvcfoilBlisterIds = _context.PackagingPvcfoilBlister.Where(w => packagingPvcfoilIds.Contains(w.PvcFoilId.Value)).Select(s => s.PvcFoilBlisterTypeId.Value).Distinct().ToList();
                masterIds.AddRange(PackagingPvcfoilBlisterIds);
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                PackagingPvcfoilModel packagingPvcfoilModel = new PackagingPvcfoilModel
                {
                    FoilThickness = s.FoilThickness,
                    FoilWidth = s.FoilWidth,
                    PvcTypeId = s.PvcTypeId,
                    PvcFoilId = s.PvcFoilId,
                    IsVersion = s.IsVersion,                   
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    PvcFoilTypeId = s.PvcFoilTypeId,
                    PvcFoilTypeName =s.PvcFoilType !=null? s.PvcFoilType.CodeValue : "",
                    PvcFoilBlisterTypeIds = s.PackagingPvcfoilBlister.Where(a => a.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).ToList(),
                    BlisterType = s.PackagingPvcfoilBlister.Where(a => a.PvcFoilId == s.PvcFoilId) != null && applicationmasterdetail.Where(a => s.PackagingPvcfoilBlister.Where(f => f.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).Contains(a.ApplicationMasterDetailId)) != null ? string.Join(',', applicationmasterdetail.Where(a => s.PackagingPvcfoilBlister.Where(f => f.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).Contains(a.ApplicationMasterDetailId)).Select(m => m.Value).ToList()) : string.Empty,
                    PvcType = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PvcTypeId).Select(a => a.Value).SingleOrDefault() : "",
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    ProfileLinkReferenceNo = s.ProfileReferenceNo,
                };
                packagingPvcfoilModels.Add(packagingPvcfoilModel);
            });
         
            return packagingPvcfoilModels;
        }

        [HttpPost]
        [Route("GetPackagingPvcFoilsByRefNo")]
        public List<PackagingPvcfoilModel> GetPackagingPvcFoilsByRefNo(RefSearchModel refSearchModel)
        {
            List<PackagingPvcfoilModel> packagingPvcfoilModels = new List<PackagingPvcfoilModel>();
            List<PackagingPvcfoil> packagingPvcfoil = new List<PackagingPvcfoil>();
            if (refSearchModel.IsHeader)
            {
                packagingPvcfoil = _context.PackagingPvcfoil.Include(a=>a.AddedByUser).Include(b=>b.ModifiedByUser).Include(c=>c.PvcFoilType).Include(d=>d.PackagingPvcfoilBlister).Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.PvcFoilTypeId == refSearchModel.TypeID).OrderByDescending(o => o.PvcFoilId).AsNoTracking().ToList();
            }
            else

            {
                packagingPvcfoil = _context.PackagingPvcfoil.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.PvcFoilType).Include(d => d.PackagingPvcfoilBlister).Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.PvcFoilTypeId == refSearchModel.TypeID).OrderByDescending(o => o.PvcFoilId).AsNoTracking().ToList();

            }
            if (packagingPvcfoil != null && packagingPvcfoil.Count > 0)
            {
                List<long> masterIds = packagingPvcfoil.Where(w => w.PvcTypeId != null).Select(a => a.PvcTypeId.Value).Distinct().ToList();
                var packagingPvcfoilIds = packagingPvcfoil.Select(s => s.PvcFoilId).ToList();
                List<long> PackagingPvcfoilBlisterIds = _context.PackagingPvcfoilBlister.Where(w => packagingPvcfoilIds.Contains(w.PvcFoilId.Value)).Select(s => s.PvcFoilBlisterTypeId.Value).Distinct().ToList();
                masterIds.AddRange(PackagingPvcfoilBlisterIds); 
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingPvcfoil.ForEach(s =>
                {
                    PackagingPvcfoilModel packagingPvcfoilModel = new PackagingPvcfoilModel
                    {
                        FoilThickness = s.FoilThickness,
                        FoilWidth = s.FoilWidth,
                        PvcTypeId = s.PvcTypeId,
                        PvcFoilId = s.PvcFoilId,
                        IsVersion = s.IsVersion,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        PvcFoilTypeId = s.PvcFoilTypeId,
                        PvcFoilTypeName = s.PvcFoilType != null ? s.PvcFoilType.CodeValue : "",
                        PvcFoilBlisterTypeIds = s.PackagingPvcfoilBlister.Where(a => a.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).ToList(),
                        BlisterType = s.PackagingPvcfoilBlister!= null && applicationmasterdetail.Where(a => s.PackagingPvcfoilBlister.Where(f => f.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).Contains(a.ApplicationMasterDetailId)) != null ? string.Join(',', applicationmasterdetail.Where(a => s.PackagingPvcfoilBlister.Where(f => f.PvcFoilId == s.PvcFoilId).Select(l => l.PvcFoilBlisterTypeId.Value).Contains(a.ApplicationMasterDetailId)).Select(m => m.Value).ToList()) : string.Empty,
                        PvcType = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PvcTypeId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo,
                    };
                    packagingPvcfoilModels.Add(packagingPvcfoilModel);
                });
            }
          
            return packagingPvcfoilModels;


        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackagingPvcfoilModel> GetData(SearchModel searchModel)
        {
            var packagingPvcfoil = new PackagingPvcfoil();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).LastOrDefault();
                        break;
                    case "Next":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).LastOrDefault();
                        break;
                    case "Next":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderBy(o => o.PvcFoilId).FirstOrDefault(s => s.PvcFoilId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingPvcfoil = _context.PackagingPvcfoil.OrderByDescending(o => o.PvcFoilId).FirstOrDefault(s => s.PvcFoilId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingPvcfoilModel>(packagingPvcfoil);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingPvcFoil")]
        public PackagingPvcfoilModel Post(PackagingPvcfoilModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "PackagingPvcFoil" });
            var packagingPvcfoil = new PackagingPvcfoil
            {
                FoilThickness = value.FoilThickness,
                FoilWidth = value.FoilWidth,
                PvcTypeId = value.PvcTypeId,
                IsVersion=value.IsVersion,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                PvcFoilTypeId = value.PvcFoilTypeId,
            };
            value.PvcFoilBlisterTypeIds.ForEach(l =>
            {
                PackagingPvcfoilBlister packagingPvcfoilBlister = new PackagingPvcfoilBlister
                {
                    PvcFoilBlisterTypeId = l,
                };
                packagingPvcfoil.PackagingPvcfoilBlister.Add(packagingPvcfoilBlister);
            });
            _context.PackagingPvcfoil.Add(packagingPvcfoil);
            _context.SaveChanges();
            value.PvcFoilId = packagingPvcfoil.PvcFoilId;
            value.MasterProfileReferenceNo = packagingPvcfoil.MasterProfileReferenceNo;
            value.PvcType = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PvcTypeId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackagingPvcfoilModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            value.PackagingItemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                string blisterTypes = string.Empty;
                List<string> blister = new List<string>();
                if (value.PvcFoilBlisterTypeIds != null && value.PvcFoilBlisterTypeIds.Count > 0)
                {
                    blister = applicationmasterdetail != null ? applicationmasterdetail.Where(a => value.PvcFoilBlisterTypeIds.Contains(a.ApplicationMasterDetailId)).Select(a => a.Value).ToList() : blister;
                    blisterTypes = string.Join(',', blister);
                }
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                var itemName = "";
                if (itemHeader != null)
                {
                    // [PVC Type] [Thickness] mm X [Foil Width]mm Foil
                    var masterDetailList = _context.ApplicationMasterDetail.ToList();
                    if(value.FoilWidth!=null && value.FoilWidth > 0)
                    {
                        itemName += value.FoilWidth + "mm" + "(W) ";
                    }
                    if (value.FoilThickness != null && value.FoilThickness > 0)
                    {
                        itemName += value.FoilThickness + "mm" + "(T) ";
                    }
                    if (!string.IsNullOrWhiteSpace(blisterTypes))
                    {
                        itemName += blisterTypes;
                    }
                    // var itemName = blisterTypes + " " + value.FoilThickness + "mm" + "(Thickness)" + " " + "X" + " " + value.FoilWidth + "mm" + " (width)" + " " + "Foil";
                    itemName += " " +  "PVC Foil";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingPvcFoil")]
        public PackagingPvcfoilModel Put(PackagingPvcfoilModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingPvcfoil = _context.PackagingPvcfoil.SingleOrDefault(p => p.PvcFoilId == value.PvcFoilId);
            packagingPvcfoil.FoilThickness = value.FoilThickness;
            packagingPvcfoil.FoilWidth = value.FoilWidth;
            packagingPvcfoil.PvcTypeId = value.PvcTypeId;
            packagingPvcfoil.IsVersion=value.IsVersion;
            packagingPvcfoil.ModifiedByUserId = value.ModifiedByUserID;
            packagingPvcfoil.ModifiedDate = DateTime.Now;
            packagingPvcfoil.StatusCodeId = value.StatusCodeID.Value;
            packagingPvcfoil.PvcFoilTypeId = value.PvcFoilTypeId;
            _context.SaveChanges();
            var pvcFoilBlisterTypeIdlist = _context.PackagingPvcfoilBlister.Where(s => s.PvcFoilId == value.PvcFoilId).AsNoTracking().ToList();
            if (pvcFoilBlisterTypeIdlist != null)
            {
                _context.PackagingPvcfoilBlister.RemoveRange(pvcFoilBlisterTypeIdlist);
                _context.SaveChanges();
            }
            value.PvcFoilBlisterTypeIds.ForEach(l =>
            {
                PackagingPvcfoilBlister packagingPvcfoilBlister = new PackagingPvcfoilBlister
                {
                    PvcFoilBlisterTypeId = l,
                };
                packagingPvcfoil.PackagingPvcfoilBlister.Add(packagingPvcfoilBlister);
            });
            value.PvcType = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PvcTypeId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingPvcFoil")]
        public void Delete(int id)
        {
            var packagingPvcfoil = _context.PackagingPvcfoil.SingleOrDefault(p => p.PvcFoilId == id);
            if (packagingPvcfoil != null)
            {
                var pvcFoilBlisterTypeIdlist = _context.PackagingPvcfoilBlister.Where(s => s.PvcFoilId == id).AsNoTracking().ToList();
                if (pvcFoilBlisterTypeIdlist != null)
                {
                    _context.PackagingPvcfoilBlister.RemoveRange(pvcFoilBlisterTypeIdlist);
                    _context.SaveChanges();
                }
                _context.PackagingPvcfoil.Remove(packagingPvcfoil);
                _context.SaveChanges();
            }
        }
    }
}