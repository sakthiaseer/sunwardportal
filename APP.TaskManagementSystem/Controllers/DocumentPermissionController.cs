﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentPermissionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentPermissionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentPermissions")]
        public List<DocumentPermissionModel> Get()
        {
            var documentPermission = _context.DocumentPermission.Include(d => d.DocumentRole).Include(dr => dr.DocumentRole.StatusCode).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DocumentPermissionModel> documentPermissionModel = new List<DocumentPermissionModel>();
            documentPermission.ForEach(s =>
            {
                DocumentPermissionModel documentPermissionModels = new DocumentPermissionModel
                {
                    DocumentPermissionID = s.DocumentPermissionId,
                    DocumentID = s.DocumentId,
                    DocumentName = s.Document != null ? s.Document.FileName : string.Empty,
                    DocumentRoleID = s.DocumentRoleId,
                    DocumentRoleDescription = s.DocumentRole != null ? s.DocumentRole.DocumentRoleDescription : string.Empty,
                    DocumentRoleName = s.DocumentRole != null ? s.DocumentRole.DocumentRoleName : string.Empty,
                    IsRead = s.IsRead,
                    IsCreateFolder = s.IsCreateFolder,
                    IsCreateDocument = s.IsCreateDocument,
                    IsSetAlert = s.IsSetAlert,
                    IsEditIndex = s.IsEditIndex,
                    IsRename = s.IsRename,
                    IsUpdateDocument = s.IsUpdateDocument,
                    IsCopy = s.IsCopy,
                    IsMove = s.IsMove,
                    IsDelete = s.IsDelete,
                    IsRelationship = s.IsRelationship,
                    IsListVersion = s.IsListVersion,
                    IsInvitation = s.IsInvitation,
                    IsSendEmail = s.IsSendEmail,
                    IsDiscussion = s.IsDiscussion,
                    IsAccessControl = s.IsAccessControl,
                    IsAuditTrail = s.IsAuditTrail,
                    IsRequired = s.IsRequired,
                    IsEdit = s.IsEdit,
                    IsFileDelete = s.IsFileDelete,
                    IsGrantAdminPermission = s.IsGrantAdminPermission,
                    IsDocumentAccess = s.IsDocumentAccess,                   
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.DocumentRole != null ? s.DocumentRole.StatusCodeId : new int?(),
                    StatusCode = s.DocumentRole != null ? s.DocumentRole.StatusCode != null ? s.DocumentRole.StatusCode.CodeValue : string.Empty : string.Empty,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsCreateTask = s.IsCreateTask,
                    IsEnableProfileTypeInfo = s.IsEnableProfileTypeInfo,
                    IsCloseDocument = s.IsCloseDocument,

                };
                documentPermissionModel.Add(documentPermissionModels);
            });
            return documentPermissionModel.OrderByDescending(a => a.DocumentPermissionID).ToList();
        }
        [HttpGet]
        [Route("GetDocumentPermissionByRoleID")]
        public DocumentPermissionModel GetDocumentPermissionByRoll(int id)
        {
            var documentPermission = _context.DocumentPermission.Include(d => d.DocumentRole).Include(dr => dr.DocumentRole.StatusCode).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Where(r => r.DocumentRoleId == id).AsNoTracking().ToList();
            List<DocumentPermissionModel> documentPermissionModel = new List<DocumentPermissionModel>();
            documentPermission.ForEach(s =>
            {
                DocumentPermissionModel documentPermissionModels = new DocumentPermissionModel
                {
                    DocumentPermissionID = s.DocumentPermissionId,
                    DocumentID = s.DocumentId,
                    DocumentName = s.Document != null ? s.Document.FileName : null,
                    DocumentRoleID = s.DocumentRoleId,
                    DocumentRoleDescription = s.DocumentRole != null ? s.DocumentRole.DocumentRoleDescription : null,
                    DocumentRoleName = s.DocumentRole != null ? s.DocumentRole.DocumentRoleName : null,
                    IsRead = s.IsRead,
                    IsCreateFolder = s.IsCreateFolder,
                    IsCreateDocument = s.IsCreateDocument.GetValueOrDefault(false),
                    IsSetAlert = s.IsSetAlert,
                    IsEditIndex = s.IsEditIndex,
                    IsRename = s.IsRename,
                    IsUpdateDocument = s.IsUpdateDocument,
                    IsCopy = s.IsCopy,
                    IsMove = s.IsMove,
                    IsDelete = s.IsDelete,
                    IsRelationship = s.IsRelationship,
                    IsListVersion = s.IsListVersion,
                    IsInvitation = s.IsInvitation,
                    IsSendEmail = s.IsSendEmail,
                    IsDiscussion = s.IsDiscussion,
                    IsAccessControl = s.IsAccessControl,
                    IsAuditTrail = s.IsAuditTrail,
                    IsRequired = s.IsRequired,
                    IsEdit = s.IsEdit,
                    IsFileDelete = s.IsFileDelete,
                    IsGrantAdminPermission = s.IsGrantAdminPermission,
                    IsDocumentAccess = s.IsDocumentAccess,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.DocumentRole != null ? s.DocumentRole.StatusCodeId : 1,
                    StatusCode = s.DocumentRole != null && s.DocumentRole.StatusCode != null ? s.DocumentRole.StatusCode.CodeValue : "",
                    ModifiedDate = s.ModifiedDate,
                    IsCreateTask = s.IsCreateTask,
                    IsEnableProfileTypeInfo = s.IsEnableProfileTypeInfo,
                    IsCloseDocument = s.IsCloseDocument,
                    IsShare = s.IsShare,
                   

                };
                documentPermissionModel.Add(documentPermissionModels);
            });
            return documentPermissionModel.OrderByDescending(a => a.DocumentPermissionID).FirstOrDefault();
        }

        public List<DocumentPermissionModel> GetDocumentPermissionByRoleIDs(List<long> ids)
        {
            var documentPermission = _context.DocumentPermission.Include(d => d.DocumentRole).Include(dr => dr.DocumentRole.StatusCode).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Where(r => ids.Contains(r.DocumentRoleId.Value)).AsNoTracking().ToList();
            List<DocumentPermissionModel> documentPermissionModels = new List<DocumentPermissionModel>();
            documentPermission.ForEach(s =>
            {
                DocumentPermissionModel documentPermissionModel = new DocumentPermissionModel
                {
                    DocumentPermissionID = s.DocumentPermissionId,
                    DocumentID = s.DocumentId,
                    DocumentRoleID = s.DocumentRoleId,
                    IsRead = s.IsRead,
                    IsCreateFolder = s.IsCreateFolder,
                    IsCreateDocument = s.IsCreateDocument.GetValueOrDefault(false),
                    IsSetAlert = s.IsSetAlert,
                    IsEditIndex = s.IsEditIndex,
                    IsRename = s.IsRename,
                    IsUpdateDocument = s.IsUpdateDocument,
                    IsCopy = s.IsCopy,
                    IsMove = s.IsMove,
                    IsDelete = s.IsDelete,
                    IsRelationship = s.IsRelationship,
                    IsListVersion = s.IsListVersion,
                    IsInvitation = s.IsInvitation,
                    IsSendEmail = s.IsSendEmail,
                    IsDiscussion = s.IsDiscussion,
                    IsAccessControl = s.IsAccessControl,
                    IsAuditTrail = s.IsAuditTrail,
                    IsRequired = s.IsRequired,
                    IsEdit = s.IsEdit,
                    IsFileDelete = s.IsFileDelete,
                    IsGrantAdminPermission = s.IsGrantAdminPermission,
                    IsDocumentAccess = s.IsDocumentAccess,
                    IsCreateTask = s.IsCreateTask,
                    IsEnableProfileTypeInfo = s.IsEnableProfileTypeInfo,
                    IsShare=s.IsShare,
                    IsCloseDocument = s.IsCloseDocument,

                };
                documentPermissionModels.Add(documentPermissionModel);
            });
            return documentPermissionModels;
        }

        [HttpGet("GetDocuments/{id:int}")]
        public ActionResult<DocumentPermissionModel> GetDocument(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var document = _context.DocumentPermission.SingleOrDefault(p => p.DocumentId == id.Value);
            var result = _mapper.Map<DocumentPermissionModel>(document);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get DocumentPermission")]
        [HttpGet("GetDocumentPermissions/{id:int}")]
        public ActionResult<DocumentPermissionModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentPermission = _context.DocumentPermission.SingleOrDefault(p => p.DocumentPermissionId == id.Value);
            var result = _mapper.Map<DocumentPermissionModel>(documentPermission);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentPermissionModel> GetData(SearchModel searchModel)
        {
            var documentPermission = new DocumentPermission();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).LastOrDefault();
                        break;
                    case "Next":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).LastOrDefault();
                        break;
                    case "Previous":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).LastOrDefault();
                        break;
                    case "Next":
                        documentPermission = _context.DocumentPermission.OrderBy(o => o.DocumentPermissionId).FirstOrDefault(s => s.DocumentPermissionId > searchModel.Id);
                        break;
                    case "Previous":
                        documentPermission = _context.DocumentPermission.OrderByDescending(o => o.DocumentPermissionId).FirstOrDefault(s => s.DocumentPermissionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentPermissionModel>(documentPermission);
            if (result != null)
            {
                result.DocumentRoleDescription = _context.DocumentRole.Where(t => t.DocumentRoleId == result.DocumentRoleID).Select(t => t.DocumentRoleDescription).ToString();
            }
            return result;
        }

        // POST: api/DocumentPermission
        [HttpPost]
        [Route("InsertDocumentPermission")]
        public DocumentPermissionModel Post(DocumentPermissionModel value)
        {
            var documentRoleID = _context.DocumentPermission.SingleOrDefault(p => p.DocumentRoleId == value.DocumentRoleID);
            if (documentRoleID == null)
            {
                var documentPermission = new DocumentPermission
                {

                    DocumentRoleId = value.DocumentRoleID,
                    IsRead = value.IsRead,
                    IsCreateFolder = value.IsCreateFolder,
                    IsCreateDocument = value.IsCreateDocument,
                    IsSetAlert = value.IsSetAlert,
                    IsEditIndex = value.IsEditIndex,
                    IsRename = value.IsRename,
                    IsUpdateDocument = value.IsUpdateDocument,
                    IsCopy = value.IsCopy,
                    IsMove = value.IsMove,
                    IsDelete = value.IsDelete,
                    IsRelationship = value.IsRelationship,
                    IsListVersion = value.IsListVersion,
                    IsInvitation = value.IsInvitation,
                    IsSendEmail = value.IsSendEmail,
                    IsDiscussion = value.IsDiscussion,
                    IsAccessControl = value.IsAccessControl,
                    IsAuditTrail = value.IsAuditTrail,
                    IsRequired = value.IsRequired,
                    IsEdit = value.IsEdit,
                    IsFileDelete = value.IsFileDelete,
                    IsGrantAdminPermission = value.IsGrantAdminPermission,
                    AddedByUserId = value.AddedByUserID,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    IsDocumentAccess = value.IsDocumentAccess,
                    IsCreateTask = value.IsCreateTask,
                    IsEnableProfileTypeInfo = value.IsEnableProfileTypeInfo,
                    IsCloseDocument = value.IsCloseDocument,


                };
                _context.DocumentPermission.Add(documentPermission);
                _context.SaveChanges();
                value.DocumentPermissionID = documentPermission.DocumentPermissionId;
            }
            else
            {
                var documentPermission = _context.DocumentPermission.SingleOrDefault(p => p.DocumentPermissionId == documentRoleID.DocumentPermissionId);
                documentPermission.DocumentId = value.DocumentID;
                documentPermission.DocumentRoleId = value.DocumentRoleID;
                documentPermission.IsRead = value.IsRead;
                documentPermission.IsCreateFolder = value.IsCreateFolder;
                documentPermission.IsCreateDocument = value.IsCreateDocument;
                documentPermission.IsSetAlert = value.IsSetAlert;
                documentPermission.IsEditIndex = value.IsEditIndex;
                documentPermission.IsRename = value.IsRename;
                documentPermission.IsUpdateDocument = value.IsUpdateDocument;
                documentPermission.IsCopy = value.IsCopy;
                documentPermission.IsMove = value.IsMove;
                documentPermission.IsDelete = value.IsDelete;
                documentPermission.IsRelationship = value.IsRelationship;
                documentPermission.IsListVersion = value.IsListVersion;
                documentPermission.IsInvitation = value.IsInvitation;
                documentPermission.IsSendEmail = value.IsSendEmail;
                documentPermission.IsDiscussion = value.IsDiscussion;
                documentPermission.IsAccessControl = value.IsAccessControl;
                documentPermission.IsAuditTrail = value.IsAuditTrail;
                documentPermission.IsRequired = value.IsRequired;
                documentPermission.IsEdit = value.IsEdit;
                documentPermission.IsFileDelete = value.IsFileDelete;
                documentPermission.IsGrantAdminPermission = value.IsGrantAdminPermission;
                documentPermission.AddedByUserId = value.AddedByUserID;
                documentPermission.AddedDate = DateTime.Now;
                documentPermission.ModifiedByUserId = value.ModifiedByUserID;
                documentPermission.StatusCodeId = value.StatusCodeID.Value;
                documentPermission.IsDocumentAccess = value.IsDocumentAccess;
                documentPermission.ModifiedDate = DateTime.Now;
                documentPermission.IsCreateTask = value.IsCreateTask;
                documentPermission.IsEnableProfileTypeInfo = value.IsEnableProfileTypeInfo;
                documentPermission.IsCloseDocument = value.IsCloseDocument;

                _context.SaveChanges();
            }
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateDocumentPermission")]
        public DocumentPermissionModel Put(DocumentPermissionModel value)
        {
            var documentPermission = _context.DocumentPermission.SingleOrDefault(p => p.DocumentPermissionId == value.DocumentPermissionID);
            if (documentPermission != null)
            {
                documentPermission.DocumentId = value.DocumentID;
                documentPermission.DocumentRoleId = value.DocumentRoleID;
                documentPermission.IsRead = value.IsRead;
                documentPermission.IsCreateFolder = value.IsCreateFolder;
                documentPermission.IsCreateDocument = value.IsCreateDocument;
                documentPermission.IsSetAlert = value.IsSetAlert;
                documentPermission.IsEditIndex = value.IsEditIndex;
                documentPermission.IsRename = value.IsRename;
                documentPermission.IsUpdateDocument = value.IsUpdateDocument;
                documentPermission.IsCopy = value.IsCopy;
                documentPermission.IsMove = value.IsMove;
                documentPermission.IsDelete = value.IsDelete;
                documentPermission.IsRelationship = value.IsRelationship;
                documentPermission.IsListVersion = value.IsListVersion;
                documentPermission.IsInvitation = value.IsInvitation;
                documentPermission.IsSendEmail = value.IsSendEmail;
                documentPermission.IsDiscussion = value.IsDiscussion;
                documentPermission.IsAccessControl = value.IsAccessControl;
                documentPermission.IsAuditTrail = value.IsAuditTrail;
                documentPermission.IsRequired = value.IsRequired;
                documentPermission.IsEdit = value.IsEdit;
                documentPermission.IsFileDelete = value.IsFileDelete;
                documentPermission.IsGrantAdminPermission = value.IsGrantAdminPermission;
                documentPermission.AddedByUserId = value.AddedByUserID;
                documentPermission.AddedDate = DateTime.Now;
                documentPermission.ModifiedByUserId = value.ModifiedByUserID;
                documentPermission.StatusCodeId = value.StatusCodeID.Value;
                documentPermission.IsDocumentAccess = value.IsDocumentAccess;
                documentPermission.ModifiedDate = DateTime.Now;
                documentPermission.IsCreateTask = value.IsCreateTask;
                documentPermission.IsEnableProfileTypeInfo = value.IsEnableProfileTypeInfo;
                documentPermission.IsCloseDocument = value.IsCloseDocument;
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteDocumentPermission")]
        public bool Delete(int id)
        {
            bool _result = false;
            try
            {
                var documentPermission = _context.DocumentPermission.SingleOrDefault(p => p.DocumentPermissionId == id);
                if (documentPermission != null)
                {
                    var documentuserRole = _context.DocumentUserRole.Where(p => p.RoleId == documentPermission.DocumentRoleId).ToList();
                    if (documentuserRole.Count == 0)
                    {
                        _context.DocumentPermission.Remove(documentPermission);
                        _context.SaveChanges();
                        _result = true;

                    }
                    else
                    {
                        _result = false;
                    }

                }
                return _result;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}