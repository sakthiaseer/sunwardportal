﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FileProfileTypeDynamicFormController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public FileProfileTypeDynamicFormController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
        }
        [HttpGet]
        [Route("GetFileProfileTypeDynamicForm")]
        public FileProfileTypeDynamicFormLoadModel Get(long? id, long? fileprofiletypeid)
        {
            FileProfileTypeDynamicFormLoadModel fileProfileTypeDynamicFormLoadModel = new FileProfileTypeDynamicFormLoadModel();
            FileProfileTypeDynamicFormModel FileProfileSetupFormModels = new FileProfileTypeDynamicFormModel();
            var FileProfileSetupFormlist = _context.FileProfileTypeDynamicForm
                .Include(s => s.AddedByUser).Include(s => s.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.FileProfileType).Where(w => w.DocumentId == id).OrderBy(o => o.FileProfileTypeDynamicFormId).AsNoTracking().ToList();
            if (FileProfileSetupFormlist != null && FileProfileSetupFormlist.Count > 0)
            {
                FileProfileSetupFormlist.ForEach(s =>
                {
                    FileProfileTypeDynamicFormModel FileProfileSetupFormModel = new FileProfileTypeDynamicFormModel
                    {
                        FileProfileTypeDynamicFormId = s.FileProfileTypeDynamicFormId,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode?.CodeValue,
                        FileProfileTypeId = s.FileProfileTypeId,
                        DynamicFormData = s.DynamicFormData,
                        ProfileNo = s.ProfileNo,
                        FileProfileTypeName = s.FileProfileType?.Name,
                        DocumentId = s.DocumentId,
                    };
                    FileProfileSetupFormModels = FileProfileSetupFormModel;
                });
            }
            fileProfileTypeDynamicFormLoadModel.FileProfileTypeDynamicFormModel = FileProfileSetupFormModels;
            FileProfileSetupFormController fileProfileSetupFormController = new FileProfileSetupFormController(_context, _mapper);
            fileProfileTypeDynamicFormLoadModel.FileProfileSetupFormModel = fileProfileSetupFormController.Get(fileprofiletypeid);

            return fileProfileTypeDynamicFormLoadModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeDynamicFormByFileProfile")]
        public List<FileProfileTypeDynamicFormModel> GetFileProfileTypeDynamicFormByFileProfile(long? id)
        {
            List<FileProfileTypeDynamicFormModel> fileProfileTypeDynamicFormModel = new List<FileProfileTypeDynamicFormModel>();
            var fileProfileSetupFormlist = _context.FileProfileTypeDynamicForm
                .Where(w => w.FileProfileTypeId == id && w.DocumentId != null).OrderBy(o => o.FileProfileTypeDynamicFormId).AsNoTracking().ToList();
            if (fileProfileSetupFormlist != null && fileProfileSetupFormlist.Count > 0)
            {
                var docuIds = fileProfileSetupFormlist.Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
                var docs = _context.Documents.Select(s => new { s.FileName, s.ContentType, s.DocumentId }).Where(w => docuIds.Contains(w.DocumentId)).ToList();
                fileProfileSetupFormlist.ForEach(s =>
                {
                    FileProfileTypeDynamicFormModel fileProfileSetupFormModel = new FileProfileTypeDynamicFormModel
                    {
                        FileProfileTypeDynamicFormId = s.FileProfileTypeDynamicFormId,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        FileProfileTypeId = s.FileProfileTypeId,
                        DynamicFormData = s.DynamicFormData,
                        ProfileNo = s.ProfileNo,
                        DocumentId = s.DocumentId,
                        DocumentID = s.DocumentId,
                        FileName = docs.FirstOrDefault(f => f.DocumentId == s.DocumentId)?.FileName,
                        ContentType = docs.FirstOrDefault(f => f.DocumentId == s.DocumentId)?.ContentType,
                        Type = "Document",
                    };
                    fileProfileTypeDynamicFormModel.Add(fileProfileSetupFormModel);
                });
            }
            return fileProfileTypeDynamicFormModel;
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocumentsSize(IFormCollection files)
        {
            var documentId = "";
            
                var filterProfileTypeId = Convert.ToInt32(files["filterProfileTypeId"].ToString());
                var fileProfileType = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == filterProfileTypeId);
                var userId = Convert.ToInt32(files["userId"].ToString());
                var plantID = Convert.ToInt64(files["plantID"].ToString());
                long? departmentId = null;
                var departmentIds = Convert.ToInt64(files["departmentId"].ToString());
                if (departmentIds > 0)
                {
                    departmentId = departmentIds;
                }
                long? sectionId = null;
                var sectionIds = Convert.ToInt64(files["sectionId"].ToString());
                if (sectionIds > 0)
                {
                    sectionId = departmentIds;
                }
                long? subSectionId = null;
                var subSectionIds = Convert.ToInt64(files["subSectionId"].ToString());
                if (subSectionIds > 0)
                {
                    subSectionId = subSectionIds;
                }
                long? divisionId = null;
                var divisionIds = Convert.ToInt64(files["divisionId"].ToString());
                if (divisionIds > 0)
                {
                    divisionId = divisionIds;
                }

                var profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
                {
                    ProfileID = fileProfileType.ProfileId,
                    AddedByUserID = userId,
                    StatusCodeID = 710,
                    DepartmentName = files["departmentName"].ToString(),
                    CompanyCode = files["companyCode"].ToString(),
                    SectionName = files["sectionName"].ToString(),
                    SubSectionName = files["subSectionName"].ToString(),
                    DepartmentId = departmentId,
                    PlantID = plantID,
                    SectionId = sectionId,
                    SubSectionId = subSectionId,
                    ScreenAutoNumberId = fileProfileType.FileProfileTypeId,
                });
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    var sessionID = Guid.NewGuid();
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = f.FileName,
                        ContentType = f.ContentType,
                        FileData = null,
                        FileSize = f.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = filterProfileTypeId,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        IsVideoFile = isVideoFiles,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    documentId = documents.DocumentId.ToString();


                });
            
            return Content(documentId.ToString());
        }
        [HttpPost]
        [Route("InsertFileProfileTypeDynamicForm")]
        public FileProfileTypeDynamicFormModel Post(FileProfileTypeDynamicFormModel value)
        {
            var fileProfileTypeDynamicForms = _context.FileProfileTypeDynamicForm.SingleOrDefault(s => s.FileProfileTypeDynamicFormId == value.FileProfileTypeDynamicFormId);
            if (fileProfileTypeDynamicForms == null)
            {
                var fileProfileTypeDynamicForm = new FileProfileTypeDynamicForm
                {
                    FileProfileTypeId = value.FileProfileTypeId,
                    DynamicFormData = value.DynamicFormData,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    AddedByUserId = value.AddedByUserID.Value,
                    ProfileNo = value.ProfileNo,
                    DocumentId = value.DocumentId,
                };
                _context.FileProfileTypeDynamicForm.Add(fileProfileTypeDynamicForm);
            }
            else
            {
                fileProfileTypeDynamicForms.DynamicFormData = value.DynamicFormData;
                fileProfileTypeDynamicForms.ModifiedByUserId = value.AddedByUserID.Value;
                fileProfileTypeDynamicForms.ModifiedDate = DateTime.Now;
            }
            _context.SaveChanges();
            return value;
        }
    }
}
