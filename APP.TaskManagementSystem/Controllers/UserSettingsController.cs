﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserSettingsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public UserSettingsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/UserSettings
        [HttpGet]
        [Route("GetUserSettings")]
        public UserSettingsModel Get(int id)
        {
            var userSettings = _context.UserSettings.Select(s => new UserSettingsModel
            {
               UserSettingsID = s.UserSettingsId,
               UserID = s.UserId,
               PageSize = s.PageSize,
               SearchQuery = s.SearchQuery,
               UserTheme = s.UserTheme,
               TitleCaption = s.TitleCaption,
               IsEnableFolderPermission= s.IsEnableFolderPermission,
                 
            }).OrderByDescending(o => o.UserID).AsNoTracking().Where(s=>s.UserID == id).FirstOrDefault();
            //var result = _mapper.Map<List<CountryModel>>(Country);
            return userSettings;
           
                 
        }
       

        
      
        

       
        // POST: api/User
        [HttpPost]
        [Route("InsertUserSettings")]
        public UserSettingsModel Post(UserSettingsModel value)
        {
            var userSettings = _context.UserSettings.SingleOrDefault(p => p.UserId == value.UserID);
            if (userSettings == null)
            {
                 userSettings = new UserSettings
                {                    
                    UserId = value.UserID,
                    PageSize = value.PageSize,
                    UserTheme = value.UserTheme,
                    SearchQuery = value.SearchQuery,
                    
                };
                _context.UserSettings.Add(userSettings);
                _context.SaveChanges();
            }
            else
            {
                userSettings.UserId = value.UserID;
                userSettings.PageSize = value.PageSize;
                userSettings.UserTheme = value.UserTheme;
                userSettings.SearchQuery = value.SearchQuery;
                if (value.UserID == 47 && value.TitleCaption !="")
                {
                    userSettings.TitleCaption = value.TitleCaption;
                }
                _context.SaveChanges();
            }
            
            return value;
        }

        [HttpPut]
        [Route("UpdateUserSettings")]
        public UserSettingsModel Put(UserSettingsModel value)
        {
            var userSettings = _context.UserSettings.SingleOrDefault(p => p.UserId == value.UserID);
            if (userSettings != null)
            {             
           
                userSettings.UserId = value.UserID;              
                if (value.UserID == 47 || value.UserID==1)
                {
                    userSettings.IsEnableFolderPermission = value.IsEnableFolderPermission;
                }
                _context.SaveChanges();
            }

            return value;
        }

        // PUT: api/User/5

    }
}