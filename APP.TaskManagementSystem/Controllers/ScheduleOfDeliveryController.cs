using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ScheduleOfDeliveryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ScheduleOfDeliveryController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetScheduleOfDeliverys")]
        public List<ScheduleOfDeliveryModel> Get()
        {

            List<ScheduleOfDeliveryModel> scheduleOfDeliverys = new List<ScheduleOfDeliveryModel>();
            var scheduleOfDeliveryslist = _context.ScheduleOfDelivery
                                           .Include(a => a.AddedByUser).Include(s => s.StatusCode)
                                            .Include(m => m.ModifiedByUser).OrderByDescending(o => o.ScheduleOfDeliveryId).AsNoTracking()
                                            .ToList();

            if (scheduleOfDeliveryslist != null && scheduleOfDeliveryslist.Count > 0)
            {
                scheduleOfDeliveryslist.ForEach(s =>
                {
                    ScheduleOfDeliveryModel ScheduleOfDelivery = new ScheduleOfDeliveryModel
                    {
                        ScheduleOfDeliveryId = s.ScheduleOfDeliveryId,
                        DeliveryCompanyId = s.DeliveryCompanyId,
                        ShipmentNo = s.ShipmentNo,
                        PlanweekId = s.PlanweekId,
                        DayOfTheWeekId = s.DayOfTheWeekId,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    scheduleOfDeliverys.Add(ScheduleOfDelivery);
                });

            }
            return scheduleOfDeliverys;
        }
        [HttpPost]
        [Route("GetScheduleOfDeliverysByRefNo")]
        public List<ScheduleOfDeliveryModel> GetProductionColorsByRefNo(RefSearchModel refSearchModel)
        {

            List<ScheduleOfDeliveryModel> scheduleOfDeliverys = new List<ScheduleOfDeliveryModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var scheduleOfDeliverylist = _context.ScheduleOfDelivery.Include(d=>d.DeliveryCompany).Include(s=>s.ShipmentNoNavigation).Include(d => d.ScheduleOfDeliveryPlanOfWeek)
                                           .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                            .OrderByDescending(o => o.ScheduleOfDeliveryId).AsNoTracking().ToList();
            if (scheduleOfDeliverylist != null && scheduleOfDeliverylist.Count > 0)
            {
                scheduleOfDeliverylist.ForEach(s =>
                {
                    ScheduleOfDeliveryModel ScheduleOfDelivery = new ScheduleOfDeliveryModel
                    {
                        ScheduleOfDeliveryId = s.ScheduleOfDeliveryId,
                        DeliveryCompanyId = s.DeliveryCompanyId,
                        DeliveryCompany=s.DeliveryCompany!=null?s.DeliveryCompany.CompanyName:"",
                        ShipmentNo = s.ShipmentNo,
                        PlanweekId = s.PlanweekId,
                        DayOfTheWeekId = s.DayOfTheWeekId,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        Planweek = s.PlanweekId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.PlanweekId).Select(m => m.Value).FirstOrDefault() : "",
                        ShipmentDetails = s.ShipmentNoNavigation!=null?(s.ShipmentNoNavigation.LocationNo+"|"+ s.ShipmentNoNavigation.LocationName) :"",
                        DayOfTheWeekIds = s.ScheduleOfDeliveryPlanOfWeek.Where(w => w.ScheduleOfDeliveryId == s.ScheduleOfDeliveryId).Select(d => d.DayOfTheWeekId.Value).ToList(),
                    };
                    scheduleOfDeliverys.Add(ScheduleOfDelivery);
                });

            }

            if (refSearchModel.IsHeader)
            {
                return scheduleOfDeliverys.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ScheduleOfDeliveryId).ToList();
            }
            return scheduleOfDeliverys.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ScheduleOfDeliveryId).ToList();
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertScheduleOfDelivery")]
        public ScheduleOfDeliveryModel Post(ScheduleOfDeliveryModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "ScheduleOfDelivery" });
            var ScheduleOfDelivery = new ScheduleOfDelivery
            {
                DeliveryCompanyId = value.DeliveryCompanyId,
                ShipmentNo = value.ShipmentNo,
                PlanweekId = value.PlanweekId,
                DayOfTheWeekId = value.DayOfTheWeekId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.ScheduleOfDelivery.Add(ScheduleOfDelivery);
            _context.SaveChanges();
            value.ScheduleOfDeliveryId = ScheduleOfDelivery.ScheduleOfDeliveryId;
            value.MasterProfileReferenceNo = ScheduleOfDelivery.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = ScheduleOfDelivery.ProfileLinkReferenceNo;
            if (value.DayOfTheWeekIds != null && value.DayOfTheWeekIds.Count > 0)
            {
                value.DayOfTheWeekIds.ForEach(f =>
                {
                    var scheduleOfDeliveryPlanOfWeek = new ScheduleOfDeliveryPlanOfWeek
                    {
                        ScheduleOfDeliveryId = value.ScheduleOfDeliveryId,
                        DayOfTheWeekId = f,

                    };
                    _context.ScheduleOfDeliveryPlanOfWeek.Add(scheduleOfDeliveryPlanOfWeek);
                });
                _context.SaveChanges();
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateScheduleOfDelivery")]
        public ScheduleOfDeliveryModel Put(ScheduleOfDeliveryModel value)
        {
            var scheduleOfDelivery = _context.ScheduleOfDelivery.SingleOrDefault(p => p.ScheduleOfDeliveryId == value.ScheduleOfDeliveryId);
            scheduleOfDelivery.DeliveryCompanyId = value.DeliveryCompanyId;
            scheduleOfDelivery.ShipmentNo = value.ShipmentNo;
            scheduleOfDelivery.PlanweekId = value.PlanweekId;
            scheduleOfDelivery.DayOfTheWeekId = value.DayOfTheWeekId;
            scheduleOfDelivery.StatusCodeId = value.StatusCodeID;
            scheduleOfDelivery.ShipmentNo = value.ShipmentNo;
            scheduleOfDelivery.ModifiedByUserId = value.ModifiedByUserID;
            scheduleOfDelivery.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            var scheduleOfDeliveryPlanOfWeekRemove = _context.ScheduleOfDeliveryPlanOfWeek.Where(s => s.ScheduleOfDeliveryId == value.ScheduleOfDeliveryId).ToList();
            if (scheduleOfDeliveryPlanOfWeekRemove.Count > 0)
            {
                _context.ScheduleOfDeliveryPlanOfWeek.RemoveRange(scheduleOfDeliveryPlanOfWeekRemove);
                _context.SaveChanges();
            }
            if (value.DayOfTheWeekIds != null && value.DayOfTheWeekIds.Count > 0)
            {
                value.DayOfTheWeekIds.ForEach(f =>
                {
                    var scheduleOfDeliveryPlanOfWeek = new ScheduleOfDeliveryPlanOfWeek
                    {
                        ScheduleOfDeliveryId = value.ScheduleOfDeliveryId,
                        DayOfTheWeekId = f,

                    };
                    _context.ScheduleOfDeliveryPlanOfWeek.Add(scheduleOfDeliveryPlanOfWeek);
                });
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteScheduleOfDelivery")]
        public void Delete(int id)
        {
            var ScheduleOfDelivery = _context.ScheduleOfDelivery.SingleOrDefault(p => p.ScheduleOfDeliveryId == id);
            if (ScheduleOfDelivery != null)
            {
                var scheduleOfDeliveryPlanOfWeekRemove = _context.ScheduleOfDeliveryPlanOfWeek.Where(t => t.ScheduleOfDeliveryId == id).ToList();
                if (scheduleOfDeliveryPlanOfWeekRemove != null)
                {
                    _context.ScheduleOfDeliveryPlanOfWeek.RemoveRange(scheduleOfDeliveryPlanOfWeekRemove);
                    _context.SaveChanges();
                }
                _context.ScheduleOfDelivery.Remove(ScheduleOfDelivery);
                _context.SaveChanges();
            }
        }
    }
}