﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using ClosedXML.Excel;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TenderOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHubContext<ChatHub> _hub;


        public TenderOrderController(CRT_TMSContext context, IMapper mapper, IConfiguration config, IHostingEnvironment host, IHubContext<ChatHub> hub)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hostingEnvironment = host;
            _hub = hub;

        }
        [HttpGet]
        [Route("GetTenderOrder")]
        public List<TenderOrderModel> Get()
        {

            List<TenderOrderModel> TenderOrderModels = new List<TenderOrderModel>();
            var TenderOrder = _context.SimulationAddhoc

               .Include(i => i.Item).OrderByDescending(o => o.SimualtionAddhocId).AsNoTracking().ToList();
            if (TenderOrder != null && TenderOrder.Count > 0)
            {
                TenderOrder.ForEach(s =>
                {
                    TenderOrderModel TenderOrderModel = new TenderOrderModel
                    {
                        SimualtionAddhocID = s.SimualtionAddhocId,
                        DocumantType = s.DocumantType,
                        SelltoCustomerNo = s.SelltoCustomerNo,
                        CustomerName = s.CustomerName,
                        Categories = s.Categories,
                        DocumentNo = s.DocumentNo,
                        ExternalDocNo = s.ExternalDocNo,
                        ItemId = s.ItemId,
                        ItemNo = s.ItemNo,
                        Description = s.Description,
                        Description1 = s.Description1,
                        OutstandingQty = s.OutstandingQty,
                        PromisedDate = s.PromisedDate,
                        ShipmentDate = s.ShipmentDate,
                        UOMCode = s.Uomcode,


                    };
                    TenderOrderModels.Add(TenderOrderModel);

                });

            }

            return TenderOrderModels;
        }

        [HttpPost]
        [Route("UploadDocuments")]
        public async Task<IActionResult> Upload(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            var plant = files["plantId"].ToString();
            var user = files["userId"].ToString();
            long userId = !string.IsNullOrEmpty(user) ? long.Parse(user) : -1;
            var companyId = !string.IsNullOrEmpty(plant) ? long.Parse(plant) : -1;
            DataTable dt = new DataTable();
            files.Files.ToList().ForEach(f =>
            {
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Importing excel file data.");

                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var fileName = file.FileName;

                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + fileName;
                System.IO.File.WriteAllBytes(serverPath, document);
                var fileInfo = new FileInfo(serverPath);
                _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Reading excel file information.");
                using (XLWorkbook workbook = new XLWorkbook(serverPath))
                {
                    IXLWorksheet worksheet = workbook.Worksheet(1);
                    bool FirstRow = true;
                    //Range for reading the cells based on the last cell used.  
                    string readRange = "1:1";
                    _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Processing excel file data.");
                    foreach (IXLRow row in worksheet.RowsUsed())
                    {
                        //If Reading the First Row (used) then add them as column name  
                        if (FirstRow)
                        {
                            //Checking the Last cellused for column generation in datatable  
                            readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                            foreach (IXLCell cell in row.Cells(readRange))
                            {
                                dt.Columns.Add(cell.Value.ToString());
                            }
                            FirstRow = false;
                        }
                        else
                        {
                            //Adding a Row in datatable  
                            dt.Rows.Add();
                            int cellIndex = 0;
                            //Updating the values of datatable  
                            foreach (IXLCell cell in row.Cells(readRange))
                            {
                                dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                cellIndex++;
                            }
                        }
                    }
                    //If no data in Excel file  
                    if (FirstRow)
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                    }
                }
            });

            var tenderOrders = (from DataRow row in dt.Rows

                                select new TenderOrderModel
                                {
                                    Categories = row["Catogories"].ToString(),
                                    CustomerName = row["CustomerName"].ToString(),
                                    Description = row["Description"].ToString(),
                                    Description1 = row["Description2"].ToString(),
                                    DocumantType = row["DocumentType"].ToString(),
                                    DocumentNo = row["DocumentNo"].ToString(),
                                    ExternalDocNo = row["ExternalDocumentNo"].ToString(),
                                    ItemNo = row["ItemNo"].ToString(),
                                    OutstandingQty = decimal.Parse(row["OutstandingQuantity"].ToString()),
                                    PromisedDate = GetDate(row["PromisedDeliveryDate"].ToString()),
                                    SelltoCustomerNo = row["SelltoCustomerNo"].ToString(),
                                    ShipmentDate = GetDate(row["ShipmentDate"]?.ToString()),
                                    UOMCode = row["UnitofMeasureCode"].ToString(),
                                    Company = row["Company"].ToString(),
                                }).ToList();

            if (tenderOrders.Count > 0)
            {
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Clearing exsiting tender information.");
                //var removeExist = _context.SimulationAddhoc.ToList();
                //_context.BulkDelete(removeExist);

                  await _context.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM SimulationAddhoc");

                var items = _context.Navitems.Select(s=>new { s.ItemId,s.No,s.CompanyId}).AsNoTracking().ToList();
                int count = 0;
                int totalItems = tenderOrders.Count();
                var addhocList = new List<SimulationAddhoc>();
                tenderOrders.ForEach(f =>
                {
                    var itemCount = count + " of " + totalItems;
                    //notify client progress update
                    var itemName = string.Format("{0} {1}-{2}  {3}", itemCount, f.ItemNo, f.Description, "tender items");
                    _hub.Clients.Group(userId.ToString()).SendAsync("progress", itemName);
                    var companyId = f.Company == "SWJB" ? 1 : 2;
                    var simAddhoc = new SimulationAddhoc
                    {
                        Categories = f.Categories,
                        CustomerName = f.CustomerName,
                        Description = f.Description,
                        Description1 = f.Description1,
                        DocumantType = f.DocumantType,
                        DocumentNo = f.DocumentNo,
                        ExternalDocNo = f.ExternalDocNo,
                        ItemId = items.FirstOrDefault(i => i.No == f.ItemNo && i.CompanyId == companyId)?.ItemId,
                        ItemNo = f.ItemNo,
                        OutstandingQty = f.OutstandingQty,
                        PromisedDate = f.PromisedDate,
                        SelltoCustomerNo = f.SelltoCustomerNo,
                        ShipmentDate = f.ShipmentDate,
                        Uomcode = f.UOMCode,
                    };
                    //_context.SimulationAddhoc.Add(simAddhoc);
                    addhocList.Add(simAddhoc);
                    count++;
                });
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Submit information to portal database");
                //_context.BulkInsert(addhocList);  

                SqlBulkUpload objBulkInsert = new SqlBulkUpload(_context, _config);
                objBulkInsert.BulkInsert(addhocList, "SimulationAddhoc", new List<string>()
                {
                    "SimualtionAddhocId","DocumantType","SelltoCustomerNo","CustomerName","Categories","DocumentNo","ExternalDocNo","ItemId","ItemNo","Description","Description1","OutstandingQty","PromisedDate","ShipmentDate","Uomcode"
                });



                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", "Completed");
            }
            return Content(SessionId.ToString());
        }

        private DateTime? GetDate(string dateString)
        {
            if (!string.IsNullOrEmpty(dateString))
            {
                DateTime date = DateTime.Parse(dateString);
                //double d = double.Parse(dateString);
                //DateTime date = DateTime.FromOADate(d);
                return date;
            }
            return null;
        }
        [HttpPut]
        [Route("UpdateTenderOrder")]
        public TenderOrderModel Put(TenderOrderModel value)
        {
            var tenderOrder = _context.SimulationAddhoc.SingleOrDefault(p => p.SimualtionAddhocId == value.SimualtionAddhocID);
            tenderOrder.DocumentNo = value.DocumentNo;
            tenderOrder.ExternalDocNo = value.ExternalDocNo;
            tenderOrder.OutstandingQty = value.OutstandingQty;
            tenderOrder.PromisedDate = value.PromisedDate;
            tenderOrder.ShipmentDate = value.ShipmentDate;

            _context.SaveChanges();
            return value;
        }


        [HttpDelete]
        [Route("DeleteTenderOrder")]
        public ActionResult<string> DeleteTenderOrder(int id)
        {
            try
            {
                var deleteTenderOrder = _context.SimulationAddhoc.Where(p => p.SimualtionAddhocId == id).FirstOrDefault();
                if (deleteTenderOrder != null)
                {
                    _context.SimulationAddhoc.Remove(deleteTenderOrder);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}