﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingUnitBoxController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingUnitBoxController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingUnitBox")]
        public List<PackagingUnitBoxModel> Get()
        {
            List<PackagingUnitBoxModel> packagingUnitBoxModels = new List<PackagingUnitBoxModel>();
            var packagingUnitBox = _context.PackagingUnitBox.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.Finishing).OrderByDescending(o => o.UnitBoxId).AsNoTracking().ToList();
            if (packagingUnitBox != null && packagingUnitBox.Count > 0)
            {
                List<long?> masterIds = packagingUnitBox.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList();
                masterIds.AddRange(packagingUnitBox.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.BgPantoneColorId != null).Select(a => a.BgPantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.BgColorId != null).Select(a => a.BgColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingUnitBox.ForEach(s =>
                {
                    PackagingUnitBoxModel packagingUnitBoxModel = new PackagingUnitBoxModel
                    {
                        ColorId = s.ColorId,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Non-Printed",
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PantoneColorId = s.PantoneColorId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        SizeHeight = s.SizeHeight,
                        SizeLength = s.SizeLength,
                        SizeWidth = s.SizeWidth,
                        UnitBoxId = s.UnitBoxId,
                        PackingUnitId = s.PackingUnitId,
                        IsVersion = s.IsVersion,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Grammage = s.Grammage,
                        FinishingId = s.FinishingId,
                        FinishingName = s.Finishing != null ? s.Finishing.CodeValue : "",
                        UnitBoxTypeID = s.UnitBoxTypeId,
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingUnitBoxModels.Add(packagingUnitBoxModel);
                });


            }

            return packagingUnitBoxModels;
        }

        [HttpPost]
        [Route("GetPackagingUnitBoxByRefNo")]
        public List<PackagingUnitBoxModel> GetPackagingUnitBoxByRefNo(RefSearchModel refSearchModel)
        {
            List<PackagingUnitBoxModel> packagingUnitBoxModels = new List<PackagingUnitBoxModel>();
            List<PackagingUnitBox> packagingUnitBox = new List<PackagingUnitBox>();
            if (refSearchModel.IsHeader)
            {
                packagingUnitBox = _context.PackagingUnitBox.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.Finishing).Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.UnitBoxTypeId == refSearchModel.TypeID).OrderByDescending(o => o.UnitBoxId).AsNoTracking().ToList();
            }
            else
            {
                packagingUnitBox = _context.PackagingUnitBox.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.Finishing).Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.UnitBoxTypeId == refSearchModel.TypeID).OrderByDescending(o => o.UnitBoxId).AsNoTracking().ToList();

            }
            if (packagingUnitBox != null && packagingUnitBox.Count > 0)
            {
                List<long?> masterIds = packagingUnitBox.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList();
                masterIds.AddRange(packagingUnitBox.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.BgPantoneColorId != null).Select(a => a.BgPantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.BgColorId != null).Select(a => a.BgColorId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingUnitBox.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingUnitBox.ForEach(s =>
                {
                    PackagingUnitBoxModel packagingUnitBoxModel = new PackagingUnitBoxModel
                    {
                        ColorId = s.ColorId,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Non-Printed",
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PantoneColorId = s.PantoneColorId,
                        BgColorId = s.BgColorId,
                        BgPantoneColorId = s.BgPantoneColorId,
                        SizeHeight = s.SizeHeight,
                        SizeLength = s.SizeLength,
                        SizeWidth = s.SizeWidth,
                        UnitBoxId = s.UnitBoxId,
                        PackingUnitId = s.PackingUnitId,
                        IsVersion = s.IsVersion,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Grammage = s.Grammage,
                        FinishingId = s.FinishingId,
                        FinishingName = s.Finishing != null ? s.Finishing.CodeValue : "",
                        UnitBoxTypeID = s.UnitBoxTypeId,
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BgColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo
                    };
                    packagingUnitBoxModels.Add(packagingUnitBoxModel);
                });


            }

            return packagingUnitBoxModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetPackagingUnitBox/{id:int}")]
        public ActionResult<PackagingUnitBoxModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var packagingUnitBox = _context.PackagingUnitBox.SingleOrDefault(p => p.UnitBoxId == id);
            var result = _mapper.Map<PackagingUnitBoxModel>(packagingUnitBox);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackagingUnitBoxModel> GetData(SearchModel searchModel)
        {
            var packagingUnitBox = new PackagingUnitBox();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).LastOrDefault();
                        break;
                    case "Next":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).LastOrDefault();
                        break;
                    case "Next":
                        packagingUnitBox = _context.PackagingUnitBox.OrderBy(o => o.UnitBoxId).FirstOrDefault(s => s.UnitBoxId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingUnitBox = _context.PackagingUnitBox.OrderByDescending(o => o.UnitBoxId).FirstOrDefault(s => s.UnitBoxId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingUnitBoxModel>(packagingUnitBox);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingUnitBox")]
        public PackagingUnitBoxModel Post(PackagingUnitBoxModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "PackagingUnitBox" });
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingUnitBox = new PackagingUnitBox
            {

                SizeHeight = value.SizeHeight,
                SizeWidth = value.SizeWidth,
                SizeLength = value.SizeLength,
                BgColorId = value.BgColorId,
                BgPantoneColorId = value.BgPantoneColorId,
                ColorId = value.ColorId,
                IsPrinted = value.IsPrinted,
                LetteringId = value.LetteringId,
                NoOfColor = value.NoOfColor,
                PackagingItemId = value.PackagingItemId,
                PackagingMaterialId = value.PackagingMaterialId,
                PantoneColorId = value.PantoneColorId,
                PackingUnitId = value.PackingUnitId,
                AddedByUserId = value.AddedByUserID,
                UnitBoxTypeId = value.UnitBoxTypeID,
                IsVersion = value.IsVersion,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                Grammage = value.Grammage,
                FinishingId = value.FinishingId,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.PackagingUnitBox.Add(packagingUnitBox);
            _context.SaveChanges();
            value.Printed = value.IsPrinted == true ? "Printed" : "Not-Printed";
            value.MasterProfileReferenceNo = packagingUnitBox.MasterProfileReferenceNo;
            value.UnitBoxId = packagingUnitBox.UnitBoxId;
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackagingUnitBoxModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.PackagingItemName = "";
            //if (value.FormTab == true)
            //{
            if (value.UnitBoxTypeID == 1086)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        //[No of color] Color [Size(L)] mm X [Size(W)]mm X[Size(H)mm]  [Packaging Material] Unit Box

                        var PackagingMaterial = value.PackagingMaterialId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialId).Select(m => m.Value).FirstOrDefault() : "";
                        var itemName = string.Empty;
                        if (value.IsPrinted == true)
                        {
                            itemName = "Printed" + " ";
                        }
                        if (value.IsPrinted == false)
                        {
                            itemName = "Non- Printed" + " ";
                        }
                        if (value.NoOfColor != null && value.IsPrinted == true)
                        {
                            itemName = itemName + value.NoOfColor + " " + "Color" + " ";
                        }
                        if (value.SizeLength != null)
                        {
                            itemName = itemName + value.SizeLength + "mm" + " ";
                        }
                        if (value.SizeWidth != null)
                        {
                            itemName = itemName + "X" + " " + value.SizeWidth + "mm" + " ";
                        }
                        if (value.SizeHeight != null)
                        {
                            itemName = itemName + "X" + " " + value.SizeHeight + "mm" + " ";
                        }
                        if (!string.IsNullOrEmpty(PackagingMaterial))
                        {
                            itemName = itemName + PackagingMaterial + " ";
                        }
                        itemName = itemName + "Unit Box";
                        //itemName = value.NoOfColor + " " + "Color" + " " + value.SizeLength  + "mm" + " " + "X" + " " + value.SizeWidth + "mm" + " " + "X" + " " + value.SizeHeight + "mm" + " " + PackagingMaterial + " " + "Unit Box";

                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            if (value.UnitBoxTypeID == 1087)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        //[No of color] Color [Size(L)] mm X [Size(W)]mm X[Size(H)mm]  [Packaging Material] Unit Box


                        var PackagingMaterial = value.PackagingMaterialId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialId).Select(m => m.Value).FirstOrDefault() : "";
                        var itemName = string.Empty;
                        if (value.IsPrinted == true)
                        {
                            itemName = "Printed" + " ";
                        }
                        if (value.IsPrinted == false)
                        {
                            itemName = "Non- Printed" + " ";
                        }
                        if (value.NoOfColor != null && value.IsPrinted == true)
                        {
                            itemName = itemName + value.NoOfColor + " " + "Color" + " ";
                        }
                        if (value.SizeLength != null)
                        {
                            itemName = itemName + value.SizeLength + "mm" + " " + "(L)" + " " + "X" + " ";
                        }
                        if (value.SizeWidth != null)
                        {
                            itemName = itemName + value.SizeWidth + "mm" + " " + "(W)" + " " + "X" + " ";
                        }
                        if (value.SizeHeight != null)
                        {
                            itemName = itemName + value.SizeHeight + "mm" + " " + "(H)" + " ";
                        }
                        if (value.Grammage != null)
                        {
                            itemName = itemName + value.Grammage + "mm" + " " + "Gram" + " ";
                        }
                        if (PackagingMaterial != null)
                        {
                            itemName = itemName + PackagingMaterial + " ";
                        }
                        if (value.FinishingName != null || value.FinishingName != "")
                        {
                            itemName = itemName + "Finishing" + " " + value.FinishingName + " ";
                        }
                        itemName = itemName + "Unit Box";
                        //itemName = value.NoOfColor + " " + "Color" + " " + value.SizeLength + "mm" + " " + "(L)" + " " + "X" + " " + value.SizeWidth + "mm" + " " + "(W)" + " " + "X" + " " + value.SizeHeight + "mm" + " " + "(H)" + " " + value.Grammage + "mm" + " " + "Gram" + " " + PackagingMaterial + " " + "Finishing" + " " + value.FinishingName + " " + "Unit Box";


                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }

            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingUnitBox")]
        public PackagingUnitBoxModel Put(PackagingUnitBoxModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingUnitBox = _context.PackagingUnitBox.SingleOrDefault(p => p.UnitBoxId == value.UnitBoxId);
            packagingUnitBox.ColorId = value.ColorId;
            packagingUnitBox.BgColorId = value.BgColorId;
            packagingUnitBox.IsPrinted = value.IsPrinted;
            packagingUnitBox.BgPantoneColorId = value.BgPantoneColorId;
            packagingUnitBox.IsPrinted = value.IsPrinted;
            packagingUnitBox.LetteringId = value.LetteringId;
            packagingUnitBox.NoOfColor = value.NoOfColor;
            packagingUnitBox.PackagingItemId = value.PackagingItemId;
            packagingUnitBox.PackagingMaterialId = value.PackagingMaterialId;
            packagingUnitBox.PantoneColorId = value.PantoneColorId;
            packagingUnitBox.NoOfColor = value.NoOfColor;
            packagingUnitBox.SizeHeight = value.SizeHeight;
            packagingUnitBox.SizeLength = value.SizeLength;
            packagingUnitBox.SizeWidth = value.SizeWidth;
            packagingUnitBox.IsVersion = value.IsVersion;
            packagingUnitBox.PackingUnitId = value.PackingUnitId;
            packagingUnitBox.ModifiedByUserId = value.ModifiedByUserID;
            packagingUnitBox.ModifiedDate = DateTime.Now;
            packagingUnitBox.StatusCodeId = value.StatusCodeID.Value;
            packagingUnitBox.Grammage = value.Grammage;
            packagingUnitBox.FinishingId = value.FinishingId;
            packagingUnitBox.UnitBoxTypeId = value.UnitBoxTypeID;
            _context.SaveChanges();
            value.FinishingName = _context.CodeMaster.Where(w => w.CodeId == value.FinishingId).Select(s => s.CodeValue).SingleOrDefault();
            value.PackagingItemName = UpdateCommonPackage(value);
            value.Printed = value.IsPrinted == true ? "Printed" : "Not-Printed";
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgPantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgPantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.BgColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.BgColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingUnitBox")]
        public void Delete(int id)
        {
            var packagingUnitBox = _context.PackagingUnitBox.SingleOrDefault(p => p.UnitBoxId == id);
            if (packagingUnitBox != null)
            {
                _context.PackagingUnitBox.Remove(packagingUnitBox);
                _context.SaveChanges();
            }
        }
    }
}