﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppIPIREntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AppIPIREntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetAppIPIREntrys")]
        public List<AppIPIREntryModel> Get()
        {
            var appIPIREntry = _context.AppIpirentry.Include("AddedByUser").Include("ModifiedByUser").Select(s => new AppIPIREntryModel
            {
               
                IPIRNo = s.Ipirno,
                IPIRUploadType = s.IpiruploadType,
                IPIRUploadName = s.IpiruploadName,
                AdditionalProcessID = s.AdditionalProcessId,
                AdditionalProcessCode = s.AdditionalProcess.Code,
                NoOfDrums = s.NoOfDrums,
                NoOfManpower = s.NoOfManpower,
                NoOfHours = s.NoOfHours,
                LocationID = s.LocationId,
                LocationName = s.Location.Name,
                DocumentID = s.DocumentId,
                Document = s.Document.FileName,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                //CertificateList = s.IctcontactDetails.Where(c=>c.SourceListId == SourceListId).ToList(),
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                

            }).OrderByDescending(o => o.IPIREntryID).AsNoTracking().ToList();
           
            return appIPIREntry;
        }

       
       

       
        [HttpGet]
        [Route("GetActiveAppIPIREntry")]
        public List<AppIPIREntryModel> GetActiveIPIREntry()
        {
            var appIPIREntry = _context.AppIpirentry.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).Select(s => new AppIPIREntryModel
            {
                IPIRNo = s.Ipirno,
                IPIRUploadType = s.IpiruploadType,
                IPIRUploadName = s.IpiruploadName,
                AdditionalProcessID = s.AdditionalProcessId,
                AdditionalProcessCode = s.AdditionalProcess.Code,
                NoOfDrums = s.NoOfDrums,
                NoOfManpower = s.NoOfManpower,
                NoOfHours = s.NoOfHours,
                LocationID = s.LocationId,
                LocationName = s.Location.Name,
                DocumentID = s.DocumentId,
                Document = s.Document.FileName,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
               

            }).OrderByDescending(o=>o.IPIREntryID).Where(i=>i.StatusCodeID==1).AsNoTracking().ToList();
            //var result = _mapper.Map<List<SourceListModel>>(AppIPIREntry);
            return appIPIREntry;
        }

        // GET: api/Project/2


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppIPIREntryModel> GetData(SearchModel searchModel)
        {
            var appIPIREntry = new AppIpirentry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).FirstOrDefault();
                        break;
                    case "Last":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).LastOrDefault();
                        break;
                    case "Next":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).LastOrDefault();
                        break;
                    case "Previous":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).FirstOrDefault();
                        break;
                    case "Last":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).LastOrDefault();
                        break;
                    case "Next":
                        appIPIREntry = _context.AppIpirentry.OrderBy(o => o.IpirentryId).FirstOrDefault(s => s.IpirentryId > searchModel.Id);
                        break;
                    case "Previous":
                        appIPIREntry = _context.AppIpirentry.OrderByDescending(o => o.IpirentryId).FirstOrDefault(s => s.IpirentryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppIPIREntryModel>(appIPIREntry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppIPIREntry")]
        public AppIPIREntryModel Post(AppIPIREntryModel value)
        {        
          
           
            var appIPIREntry = new AppIpirentry
            {     
                Ipirno = value.IPIRNo,
                IpiruploadType = value.IPIRUploadType,
                IpiruploadName = value.IPIRUploadName,
                NoOfDrums = value.NoOfDrums,
                NoOfManpower = value.NoOfManpower,
                NoOfHours = value.NoOfHours,
                AdditionalProcessId = value.AdditionalProcessID,
                LocationId = value.LocationID,
                DocumentId = value.DocumentID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
               

            };
            _context.AppIpirentry.Add(appIPIREntry);
            _context.SaveChanges();
            value.IPIREntryID = appIPIREntry.IpirentryId;     

            return value;
        }







        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppIPIREntry")]
        public AppIPIREntryModel Put(AppIPIREntryModel value)
        {
            var appIPIREntry = _context.AppIpirentry.SingleOrDefault(p => p.IpirentryId == value.IPIREntryID);
            appIPIREntry.Ipirno = value.IPIRNo;
            appIPIREntry.IpiruploadName = value.IPIRUploadName;
            appIPIREntry.IpiruploadType = value.IPIRUploadType;
            appIPIREntry.AdditionalProcessId = value.AdditionalProcessID;
            appIPIREntry.LocationId = value.LocationID;
            appIPIREntry.NoOfDrums = value.NoOfDrums;
            appIPIREntry.NoOfManpower = value.NoOfManpower;
            appIPIREntry.NoOfHours = value.NoOfHours;
            appIPIREntry.DocumentId = value.DocumentID;
            appIPIREntry.ModifiedByUserId = value.ModifiedByUserID;
            appIPIREntry.ModifiedDate = DateTime.Now;           
            appIPIREntry.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
           
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppIPIREntry")]
        public void Delete(int id)
        {

            var appIPIREntry = _context.AppIpirentry.SingleOrDefault(p => p.IpirentryId == id);
            if (appIPIREntry != null)
            {
               
                _context.AppIpirentry.Remove(appIPIREntry);
                _context.SaveChanges();
            }
        }
    }
}