﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class WorkOrderCommentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public WorkOrderCommentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region WorkOrder

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetWorkOrderComments")]
        public List<WorkOrderCommentModel> Get(long? id, long? userId)
        {
            List<long?> workOrderCommentIds = new List<long?>();
            var applicationUsersList = _context.Employee.Include(a => a.User).Where(s => s.User.StatusCodeId == 1).AsNoTracking().ToList();
            var workOrderComment = _context.WorkOrderComment.Include(a => a.CommentedByNavigation).Include(a => a.WorkOrderCommentUser).Where(s => s.WorkorderId == id).AsNoTracking().ToList();
            var AssignedcommentIds = _context.WorkOrderCommentUser.Where(w => w.WorkOrderLineId == id).Select(s => s.WorkOrderCommentId).Distinct().ToList();
            var commentdIds = _context.WorkOrderCommentUser.Where(w => w.IsAssignedTo == false && w.WorkOrderLineId == id || w.IsAssignedTo == null).Select(s => s.WorkOrderCommentId).Distinct().ToList();
            if (AssignedcommentIds.Count > 0)
            {
                workOrderCommentIds.AddRange(AssignedcommentIds);
            }
            if (commentdIds.Count > 0)
            {
                workOrderCommentIds.AddRange(commentdIds);
            }
            var workOrders = workOrderComment
                               .Where(a => (workOrderCommentIds.Contains(a.WorkOrderCommentId) && a.ParentCommentId == null)).ToList();
            List<WorkOrderCommentModel> WorkOrderCommentModels = new List<WorkOrderCommentModel>();
            workOrders.ForEach(s =>
            {
                WorkOrderCommentModel WorkOrderCommentModel = new WorkOrderCommentModel
                {
                    WorkOrderCommentId = s.WorkOrderCommentId,
                    WorkorderId = s.WorkorderId,
                    Comment = s.Comment,
                    CommentedBy = s.CommentedBy,
                    ParentCommentId = s.ParentCommentId,
                    IsRead = s.IsRead,
                    CommentedDate = s.CommentedDate,
                    AddedByUser = s.CommentedByNavigation?.UserName,
                    Subject = s.Subject,
                    IsEdit = s.IsEdit,
                    EditedBy = s.EditedBy,
                    EditedByName = s.EditedByNavigation?.UserName,
                    AssignTo = s.WorkOrderCommentUser?.Where(w => w.WorkOrderCommentId == s.WorkOrderCommentId).FirstOrDefault()?.UserId,
                    IsClosed = s.IsClosed,
                    AssignUser = applicationUsersList?.Where(a => a.UserId == s.WorkOrderCommentUser?.Where(w => w.WorkOrderCommentId == s.WorkOrderCommentId).FirstOrDefault()?.UserId).FirstOrDefault()?.FirstName,
                };
                WorkOrderCommentModels.Add(WorkOrderCommentModel);
            });
            WorkOrderCommentModels.ForEach(sc =>
            {
                List<WorkOrderCommentModel> subMessageModels = new List<WorkOrderCommentModel>();

                var subMessages = _context.WorkOrderComment
                                    .Include(t => t.CommentedByNavigation)
                                    .Where(t => t.ParentCommentId == sc.WorkOrderCommentId).AsNoTracking().ToList();
                subMessages.ForEach(reply =>
                {
                    WorkOrderCommentModel subMessageModel = new WorkOrderCommentModel();

                    subMessageModel.WorkOrderCommentId = reply.WorkOrderCommentId;
                    //subMessageModel.TaskSubject = reply.TaskSubject;
                    subMessageModel.WorkorderId = reply.WorkorderId;
                    subMessageModel.Comment = reply.Comment;
                    subMessageModel.CommentedBy = reply.CommentedBy;
                    subMessageModel.AddedByUser = reply.CommentedByNavigation?.UserName;
                    subMessageModel.CommentedDate = reply.CommentedDate;
                    subMessageModel.ParentCommentId = reply.ParentCommentId;
                    subMessageModel.AddedByUser = reply.CommentedByNavigation?.UserName;
                    subMessageModel.IsEdit = reply.IsEdit;
                    subMessageModel.EditedByName = reply?.EditedByNavigation?.UserName;
                    subMessageModel.EditedBy = reply.EditedBy;
                    subMessageModel.Subject = reply.Subject;
                    subMessageModel.IsRead = reply.IsRead;
                    subMessageModel.IsClosed = reply.IsClosed;

                    //subMessageModel.IsRead = reply.CommentedBy == userId ? false : reply.TaskCommentUser.Where(u => u.UserId == userId).Any(a => a.IsRead == false);

                    //sc.WorkOrderCommentModels.Add(subMessageModel);
                    subMessageModels.Add(subMessageModel);
                });
                sc.WorkOrderCommentModels=subMessageModels;


            });
            return WorkOrderCommentModels.Where(s => s.IsClosed == false || s.IsClosed == null).ToList();
        }


        [HttpGet]
        [Route("GetUnReadWorkOrderComments")]
        public List<WorkOrderModel> GetUnReadWorkOrderComments(long? id)
        {
            List<WorkOrderModel> workOrderModels = new List<WorkOrderModel>();
            var workorderIds = _context.WorkOrder.Where(s => s.AssignTo == id || s.AddedByUserId == id).Select(s => s.WorkOrderId).ToList();

            if (workorderIds.Count > 0)
            {
                var workorderlineIds = _context.WorkOrderLine.Where(s => workorderIds.Contains(s.WorkOrderId.Value) && s.CrtstatusId != 2373).Select(s => s.WorkOrderLineId).ToList();
                var workOrderComments = _context.WorkOrderComment
                                    .AsNoTracking().OrderByDescending(o => o.WorkOrderCommentId).Include(a => a.Workorder).Where(a => workorderlineIds.Contains(a.WorkorderId.Value) && a.ParentCommentId == null && (a.IsClosed == false || a.IsClosed == null)).ToList();
                if (workOrderComments.Count > 0)
                {
                    List<long?> unreadworkorder = workOrderComments.Select(s => s.Workorder.WorkOrderId).ToList();
                    var workOrders = _context.WorkOrder
                         .Include(a => a.Assignment)
                                .Include(r => r.Permission)
                                .Include(m => m.Module)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)

                        .Where(w => unreadworkorder.Contains(w.WorkOrderId)).ToList();
                    if (workOrders.Count > 0)
                    {
                        workOrders.ForEach(s =>
                        {
                            WorkOrderModel workOrderModel = new WorkOrderModel
                            {
                                WorkOrderId = s.WorkOrderId,
                                AssignmentId = s.AssignmentId,
                                AssignmentName = s.Assignment?.CodeValue,
                                RequirementId = s.RequirementId,
                                RequirementName = s.Requirement?.Value,
                                ModuleId = s.ModuleId,
                                ModuleName = s.Permission?.Name,
                                PermissionID = s.PermissionId,
                                AddedByUserID = s.AddedByUserId,
                                ModifiedByUserID = s.ModifiedByUserId,
                                StatusCodeID = s.StatusCodeId,
                                AddedByUser = s.AddedByUser?.UserName,
                                ModifiedByUser = s.ModifiedByUser?.UserName,
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,
                                StatusCode = s.StatusCode?.CodeValue,
                                TypeId = s.TypeId,
                                AssignTo = s.AssignTo,

                            };
                            workOrderModels.Add(workOrderModel);
                        });
                    }
                }
            }
            return workOrderModels;
        }

        // POST: api/WorkOrder
        [HttpPost]
        [Route("InsertWorkOrderComment")]
        public WorkOrderCommentModel Post(WorkOrderCommentModel value)
        {

            var workOrderComment = new WorkOrderComment
            {
                WorkorderId = value.WorkorderId,
                Comment = value.Comment,
                CommentedBy = value.CommentedBy,
                CommentedDate = value.CommentedDate,
                ParentCommentId = value.ParentCommentId,
                IsRead = value.IsRead,
                Subject = value.Subject,



            };
            _context.WorkOrderComment.Add(workOrderComment);



            _context.SaveChanges();
            value.WorkOrderCommentId = workOrderComment.WorkOrderCommentId;
            if (value.AssignTo != null)
            {
                WorkOrderCommentUser workOrderCommentUser = new WorkOrderCommentUser
                {
                    IsAssignedTo = true,
                    UserId = value.AssignTo,
                   
                    WorkOrderLineId = value.WorkorderId,
                    WorkOrderCommentId = value.WorkOrderCommentId,
                    IsRead = false,
                    IsClosed = false,
                };
                _context.WorkOrderCommentUser.Add(workOrderCommentUser);
                _context.SaveChanges();
                if (value.ParentCommentId == null)
                {
                    var workOrderLine = _context.WorkOrderLine.SingleOrDefault(w => w.WorkOrderLineId == value.WorkorderId);
                    workOrderLine.SunwardStatusId = 2354;
                    _context.SaveChanges();
                }
            }
            else
            {
                WorkOrderCommentUser workOrderCommentUser = new WorkOrderCommentUser
                {
                    IsAssignedTo = true,
                    UserId = value.CommentedBy,
                    WorkOrderLineId = value.WorkorderId,
                    WorkOrderCommentId = value.WorkOrderCommentId,
                    IsRead = false,
                    IsClosed = false,
                };
                _context.WorkOrderCommentUser.Add(workOrderCommentUser);
                _context.SaveChanges();
                if (value.ParentCommentId == null)
                {
                    var workOrderLine = _context.WorkOrderLine.SingleOrDefault(w => w.WorkOrderLineId == value.WorkorderId);
                    workOrderLine.SunwardStatusId = 2354;
                    _context.SaveChanges();
                }
            }
            if (value.ParentCommentId != null)
            {
                var workOrderLine = _context.WorkOrderLine.Include(a => a.WorkOrderCommentUser).SingleOrDefault(w => w.WorkOrderLineId == value.WorkorderId);
                if (workOrderLine.SunwardStatusId == 2354 && workOrderLine.WorkOrderCommentUser != null)
                {
                    var userid = workOrderLine.WorkOrderCommentUser.FirstOrDefault().UserId;
                    if (userid != null && userid == value.CommentedBy)
                        workOrderLine.SunwardStatusId = 2355;
                    _context.SaveChanges();
                }
            }
            if (value.CommentedBy > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.CommentedBy).FirstOrDefault().UserName;
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateWorkOrderComment")]
        public WorkOrderCommentModel Put(WorkOrderCommentModel value)
        {
            var workOrderComment = _context.WorkOrderComment.SingleOrDefault(p => p.WorkOrderCommentId == value.WorkOrderCommentId);


            workOrderComment.WorkorderId = value.WorkorderId;
            workOrderComment.Comment = value.Comment;
            workOrderComment.CommentedBy = value.CommentedBy;
            workOrderComment.CommentedDate = value.CommentedDate;
            workOrderComment.IsRead = value.IsRead;
            workOrderComment.ParentCommentId = value.ParentCommentId;
            workOrderComment.Subject = value.Subject;
            workOrderComment.IsEdit = true;
            workOrderComment.EditedBy = value.EditedBy;
            _context.SaveChanges();
            if (value.AssignTo != null)
            {
                var existing = _context.WorkOrderCommentUser.Where(c => c.WorkOrderLineId == value.WorkorderId && c.WorkOrderCommentId == value.WorkOrderCommentId && c.UserId == value.AssignTo).ToList();
                if (existing == null)
                {
                    WorkOrderCommentUser workOrderCommentUser = new WorkOrderCommentUser
                    {
                        IsAssignedTo = true,
                        UserId = value.AssignTo,
                        WorkOrderLineId = value.WorkorderId,
                        WorkOrderCommentId = value.WorkOrderCommentId,
                        IsRead = false,
                        IsClosed = false,
                    };
                    _context.WorkOrderCommentUser.Add(workOrderCommentUser);
                    _context.SaveChanges();
                    if (value.ParentCommentId == null)
                    {
                        var workOrderLine = _context.WorkOrderLine.SingleOrDefault(w => w.WorkOrderLineId == value.WorkorderId);
                        workOrderLine.SunwardStatusId = 2354;
                        _context.SaveChanges();
                    }
                }
            }
            if (value.ParentCommentId != null)
            {
                var workOrderLine = _context.WorkOrderLine.Include(a => a.WorkOrderCommentUser).SingleOrDefault(w => w.WorkOrderLineId == value.WorkorderId);
                if (workOrderLine.SunwardStatusId == 2354 && workOrderLine.WorkOrderCommentUser != null)
                {
                    var userid = workOrderLine.WorkOrderCommentUser.FirstOrDefault().UserId;
                    if (userid != null && userid == value.CommentedBy)
                        workOrderLine.SunwardStatusId = 2355;
                    _context.SaveChanges();
                }
            }
            return value;
        }

        [HttpPut]
        [Route("UpdateReadComment")]
        public WorkOrderCommentModel UpdateReadComment(WorkOrderCommentModel value)
        {
            var workOrderComment = _context.WorkOrderComment.Where(p => p.WorkorderId == value.WorkorderId && p.IsRead == false && p.CommentedBy != value.CommentedBy).ToList();


            if (workOrderComment != null && workOrderComment.Count > 0)
            {
                workOrderComment.ForEach(w =>
                {
                    w.IsRead = true;

                });
                _context.SaveChanges();
            }



            return value;
        }

        [HttpPut]
        [Route("CloseComment")]
        public WorkOrderCommentModel CloseComment(WorkOrderCommentModel value)
        {
            var workOrderComment = _context.WorkOrderComment.Where(p => p.WorkOrderCommentId == value.WorkOrderCommentId).FirstOrDefault();


            if (workOrderComment != null)
            {
                workOrderComment.IsClosed = true;
                //workOrderComment.IsClosed = true;


                _context.SaveChanges();
            }



            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteWorkOrderComment")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var workOrderComment = _context.WorkOrderComment.SingleOrDefault(p => p.WorkOrderCommentId == id);
                if (workOrderComment != null)
                {
                    var workordercommentUser = _context.WorkOrderCommentUser.Where(s => s.WorkOrderCommentId == id).ToList();
                    if (workordercommentUser.Count > 0)
                    {
                        _context.WorkOrderCommentUser.RemoveRange(workordercommentUser);
                        _context.SaveChanges();
                    }
                    _context.WorkOrderComment.Remove(workOrderComment);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Workorder Comment Cannot be delete! work order Reference to others");
            }
        }

        #endregion
    }
}
