﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class InterCompanyPricingSummaryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public InterCompanyPricingSummaryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpPost]
        [Route("GetInterCompanyPricingSummary")]
        public List<SellingPriceInformations> InterCompanyPricingSummary(InterCompanyPricingSummaryModel searchModel)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var exchangeRate = _context.ExchangeRateLine.Include(e => e.ExchangeRate).Where(s => s.ExchangeRate.ValidPeriodFrom.Value.Date >= DateTime.Today.Date && s.ExchangeRate.ValidPeriodFrom.Value.Date >= DateTime.Today.Date).SingleOrDefault();
            List<SellingPriceInformations> SellingPriceInformations = new List<SellingPriceInformations>();
            var MasterInterCompanyPricingLineItems = _context.MasterInterCompanyPricingLine.Include(b => b.MasterInterCompanyPricing).Where(w => w.MasterInterCompanyPricing.FromCompanyId == searchModel.FromCompanyId && w.MasterInterCompanyPricing.ToCompanyId == searchModel.ToCompanyId && w.SellingToId == searchModel.CompanyId).FirstOrDefault();
            MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel();
            if (MasterInterCompanyPricingLineItems != null)
            {
                masterInterCompanyPricingLineModel.SellingToId = MasterInterCompanyPricingLineItems.SellingToId;
                masterInterCompanyPricingLineModel.BillingPercentage = MasterInterCompanyPricingLineItems.BillingPercentage;
                masterInterCompanyPricingLineModel.CurrencyId = MasterInterCompanyPricingLineItems.CurrencyId;
                masterInterCompanyPricingLineModel.Currency = masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == MasterInterCompanyPricingLineItems.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == MasterInterCompanyPricingLineItems.CurrencyId).Value : "";
            }
            if (searchModel.SpecificOrderNo == null || searchModel.SpecificOrderNo == "")
            {
                MarginInformationSearchModel MarginInformationSearchModel = new MarginInformationSearchModel();
                MarginInformationSearchModel.BusinessCategoryId = searchModel.BusinessCategoryId;
                MarginInformationSearchModel.CompanyId = searchModel.CompanyId;
                SellingPriceInformationController SellingPriceInformation = new SellingPriceInformationController(_context, _mapper);
                var sellingPriceInformationItems = SellingPriceInformation.GetSellingPriceInformationsByCompanyID(MarginInformationSearchModel);
                if (sellingPriceInformationItems.Count > 0)
                {
                    sellingPriceInformationItems.ForEach(h =>
                    {
                        if (h.SellingPriceInformations.Count > 0)
                        {
                            h.SellingPriceInformations.ForEach(s =>
                            {
                                SellingPriceInformations SellingPriceInformationModel = new SellingPriceInformations();
                                s.SummeryCurrency = s.Currency;
                                s.SummerySellingPrice = s.MinSellingPrice - (s.MinSellingPrice * (masterInterCompanyPricingLineModel.BillingPercentage / 100));
                                if (exchangeRate!=null && exchangeRate.EquivalentUnitId>0)
                                {
                                    if(exchangeRate.CurrencyId!=s.CurrencyId && exchangeRate.EquivalebtCurrencyId!= masterInterCompanyPricingLineModel.CurrencyId)
                                    {
                                        s.SummeryCurrency= masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == exchangeRate.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == exchangeRate.CurrencyId).Value : "";
                                        s.SummerySellingPrice = (s.MinSellingPrice - (s.MinSellingPrice * (masterInterCompanyPricingLineModel.BillingPercentage / 100)))* exchangeRate.EquivalentUnitId;
                                    }
                                }
                                SellingPriceInformations.Add(s);
                            });
                        }
                    });
                }
            }
            else
            {
                SalesOrderEntryController SalesOrderEntryController = new SalesOrderEntryController(_context, _mapper, _generateDocumentNoSeries);
                SearchModel ContractNoSearch = new SearchModel();
                ContractNoSearch.SearchString = searchModel.SpecificOrderNo;
                ContractNoSearch.UserID = searchModel.CompanyId.Value;
                var GetContractNoSearch = SalesOrderEntryController.GetContractNoSearch(ContractNoSearch);
                if (GetContractNoSearch != null && GetContractNoSearch.SalesEntryProducts.Count > 0)
                {
                    GetContractNoSearch.SalesEntryProducts.ForEach(h =>
                    {
                        SellingPriceInformations SellingPriceInformationModel = new SellingPriceInformations();
                        SellingPriceInformationModel.No = h.ProductName;
                        SellingPriceInformationModel.Description = h.Description;
                        SellingPriceInformationModel.Currency = h.OrderCurrency;
                        SellingPriceInformationModel.MinSellingPrice = h.SellingPrice;
                        SellingPriceInformationModel.Description2 = h.Description2;
                        SellingPriceInformationModel.BaseUnitofMeasure = h.BUOM;
                        SellingPriceInformationModel.SummerySellingPrice = h.SellingPrice - (h.SellingPrice * (masterInterCompanyPricingLineModel.BillingPercentage / 100));
                        SellingPriceInformationModel.SummeryCurrency = h.OrderCurrency;
                        if (exchangeRate != null && exchangeRate.EquivalentUnitId > 0)
                        {
                            if (exchangeRate.CurrencyId != h.OrderCurrencyId && exchangeRate.EquivalebtCurrencyId != masterInterCompanyPricingLineModel.CurrencyId)
                            {
                                SellingPriceInformationModel.SummeryCurrency = masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == exchangeRate.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == exchangeRate.CurrencyId).Value : "";
                                SellingPriceInformationModel.SummerySellingPrice = (h.SellingPrice - (h.SellingPrice * (masterInterCompanyPricingLineModel.BillingPercentage / 100))) * exchangeRate.EquivalentUnitId;
                            }
                        }
                        SellingPriceInformations.Add(SellingPriceInformationModel);
                    });
                }
            }
            return SellingPriceInformations;
        }
    }
}