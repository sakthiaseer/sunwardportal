﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PlantMaintenanceEntryMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PlantMaintenanceEntryMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPlantMaintenanceEntryMasters")]
        public List<PlantMaintenanceEntryMasterModel> Get()
        {
            List<PlantMaintenanceEntryMasterModel> plantMaintenanceEntryMasterModels = new List<PlantMaintenanceEntryMasterModel>();
            var plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.PlantMaintenanceEntryMasterId).AsNoTracking().ToList();
            if (plantMaintenanceEntryMaster != null && plantMaintenanceEntryMaster.Count > 0)
            {
                plantMaintenanceEntryMaster.ForEach(s =>
                {
                    PlantMaintenanceEntryMasterModel plantMaintenanceEntryMasterModel = new PlantMaintenanceEntryMasterModel
                    {
                        PlantMaintenanceEntryMasterID = s.PlantMaintenanceEntryMasterId,
                        Name = s.Name,
                        MasterType = s.MasterType.Value,
                        //StatusCode=s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantMaintenanceEntryMasterModels.Add(plantMaintenanceEntryMasterModel);
                });
            }

            return plantMaintenanceEntryMasterModels;
        }
        [HttpGet]
        [Route("GetPlantMaintenanceEntryMasterByID")]
        public List<PlantMaintenanceEntryMasterModel> GetICT(int id)
        {
            List<PlantMaintenanceEntryMasterModel> plantMaintenanceEntryMasterModels = new List<PlantMaintenanceEntryMasterModel>();
            var plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.Include("AddedByUser").Include("ModifiedByUser").Include("IctlayoutPlanTypes").OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(t => t.MasterType == id).AsNoTracking().ToList();
            if (plantMaintenanceEntryMaster != null && plantMaintenanceEntryMaster.Count > 0)
            {
                plantMaintenanceEntryMaster.ForEach(s =>
                {
                    PlantMaintenanceEntryMasterModel plantMaintenanceEntryMasterModel = new PlantMaintenanceEntryMasterModel
                    {
                        PlantMaintenanceEntryMasterID = s.PlantMaintenanceEntryMasterId,
                        Name = s.Name,
                        MasterType = s.MasterType.Value,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantMaintenanceEntryMasterModels.Add(plantMaintenanceEntryMasterModel);
                });
            }

            return plantMaintenanceEntryMasterModels;
        }

        [HttpGet]
        [Route("GetPlantMaintenanceEntryMasterActive")]
        public List<PlantMaintenanceEntryMasterModel> GetICTActive(int id)
        {
            List<PlantMaintenanceEntryMasterModel> plantMaintenanceEntryMasterModels = new List<PlantMaintenanceEntryMasterModel>();
            var plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.Include("AddedByUser").Include("ModifiedByUser").Include("IctlayoutPlanTypes").OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(t => t.MasterType == id && t.StatusCodeId == 1).AsNoTracking().ToList();
            if (plantMaintenanceEntryMaster != null && plantMaintenanceEntryMaster.Count > 0)
            {
                plantMaintenanceEntryMaster.ForEach(s =>
                {
                    PlantMaintenanceEntryMasterModel plantMaintenanceEntryMasterModel = new PlantMaintenanceEntryMasterModel
                    {
                        PlantMaintenanceEntryMasterID = s.PlantMaintenanceEntryMasterId,
                        Name = s.Name,
                        MasterType = s.MasterType.Value,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantMaintenanceEntryMasterModels.Add(plantMaintenanceEntryMasterModel);
                });
            }

            return plantMaintenanceEntryMasterModels;
        }




        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PlantMaintenanceEntryMasterModel> GetData(SearchModel searchModel)
        {
            var plantMaintenanceEntryMaster = new PlantMaintenanceEntryMaster();
            var plantMaintenanceEntryMastermodel = new PlantMaintenanceEntryMasterModel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Previous":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderBy(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault(s => s.PlantMaintenanceEntryMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.OrderByDescending(o => o.PlantMaintenanceEntryMasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault(s => s.PlantMaintenanceEntryMasterId < searchModel.Id);

                        break;
                }
            }
            var result = _mapper.Map<PlantMaintenanceEntryMasterModel>(plantMaintenanceEntryMaster);
            //PlantMaintenanceEntryMastermodel.LayoutPlanTypeID = _context.IctlayoutPlanTypes.Where(t => t.PlantMaintenanceEntryMasterId == searchModel.Id).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault();
            //if (PlantMaintenanceEntryMastermodel.LayoutPlanTypeID > 0 && result !=null)
            //{
            //    result.LayoutPlanTypeID = PlantMaintenanceEntryMastermodel.LayoutPlanTypeID;
            //}
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPlantMaintenanceEntryMaster")]
        public PlantMaintenanceEntryMasterModel Post(PlantMaintenanceEntryMasterModel value)
        {

            var plantMaintenanceEntryMaster = new PlantMaintenanceEntryMaster()
            {

                Name = value.Name,
                MasterType = value.MasterType,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };


            _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceEntryMaster);
            _context.SaveChanges();
            value.PlantMaintenanceEntryMasterID = plantMaintenanceEntryMaster.PlantMaintenanceEntryMasterId;


            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePlantMaintenanceEntryMaster")]
        public PlantMaintenanceEntryMasterModel Put(PlantMaintenanceEntryMasterModel value)
        {

            var plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.SingleOrDefault(p => p.PlantMaintenanceEntryMasterId == value.PlantMaintenanceEntryMasterID);
            if (plantMaintenanceEntryMaster != null)
            {

                plantMaintenanceEntryMaster.ModifiedByUserId = value.ModifiedByUserID;
                plantMaintenanceEntryMaster.ModifiedDate = DateTime.Now;
                plantMaintenanceEntryMaster.Name = value.Name;
                plantMaintenanceEntryMaster.MasterType = value.MasterType;
                plantMaintenanceEntryMaster.StatusCodeId = value.StatusCodeID.Value;

                _context.SaveChanges();

            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePlantMaintenanceEntryMaster")]
        public void Delete(int id)
        {
            try
            {
                var plantMaintenanceEntryMaster = _context.PlantMaintenanceEntryMaster.SingleOrDefault(p => p.PlantMaintenanceEntryMasterId == id);
                if (plantMaintenanceEntryMaster != null)
                {
                    _context.PlantMaintenanceEntryMaster.Remove(plantMaintenanceEntryMaster);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}