﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SelfTestController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public SelfTestController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }
        [HttpGet]
        [Route("GetHumanMovement")]
        public List<SelfTestModel> GetHumanMovement()
        {
            List<SelfTestModel> humanMovementModels = new List<SelfTestModel>();
            var humanMovementCompanyRelated = _context.SelfTestCompanyRelated.Include(e => e.Employee).ToList();
            var humanMovementPrivate = _context.SelfTestPrivate.Include(e => e.Employee).ToList();
            var humanMovement = _context.SelfTest.OrderByDescending(o => o.SelfTestId).AsNoTracking().ToList();
            if (humanMovement != null && humanMovement.Count > 0)
            {
                var employeeIds = humanMovement.Select(s => s.EmployeeId.GetValueOrDefault(0)).ToList();
                var compnayIds = humanMovement.Select(s => s.CompanyId.GetValueOrDefault(0)).ToList();
                var company = _context.Company.Select(s => new { s.CompanyId, s.CompanyName }).Where(w => compnayIds.Contains(w.CompanyId)).ToList();
                var employee = _context.Employee.Select(s => new { s.EmployeeId, s.FirstName, s.LastName, s.NickName, s.Nricno }).Where(w => employeeIds.Contains(w.EmployeeId)).ToList();
                var sessionIds = humanMovement.Where(s => s.SessionId != null).Select(s => s.SessionId).ToList();
                var documnts = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.ContentType, s.FileName }).Where(w => sessionIds.Contains(w.SessionId)).ToList();
                humanMovement.ForEach(s =>
                {
                    SelfTestModel humanMovementModel = new SelfTestModel();
                    humanMovementModel.SelfTestId = s.SelfTestId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = employee != null ? employee.FirstOrDefault(a => a.EmployeeId == s.EmployeeId)?.FirstName : "";
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (humanMovementModel.SunEmployeeName);
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyName = company != null ? company.FirstOrDefault(a => a.CompanyId == s.CompanyId)?.CompanyName : "";
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.SelfTestDate = s.SelfTestDate;
                    humanMovementModel.TimeOfSelfTest = s.TimeOfSelfTest;
                    humanMovementModel.NricNo = s.NricNo;
                    humanMovementModel.SelfTestKitLotNo = s.SelfTestKitLotNo;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.SelfTestId == s.SelfTestId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.SelfTestId == s.SelfTestId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.SelfTestId == s.SelfTestId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.SelfTestStatusId = s.SelfTestStatusId;
                    humanMovementModel.DocumentID = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.DocumentId : 0;
                    humanMovementModel.ContentType = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.ContentType : "";
                    humanMovementModel.FileName = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.FileName : "";

                    humanMovementModel.Type = "Document";
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels;
        }
        [HttpPost]
        [Route("GetSelfTestDocument")]
        public List<SelfTestDocuments> GetSelfTestDocument(SelfTestDocuments searchModel)
        {
            List<SelfTestDocuments> SelfTestModel = new List<SelfTestDocuments>();
            var documents = _context.Documents.Where(t => searchModel.SessionIds.Contains(t.SessionId)).ToList();
            if (documents != null)
            {
                documents.ForEach(s =>
                {
                    SelfTestDocuments SelfTestDocuments = new SelfTestDocuments();
                    string imageData = "";
                    SelfTestDocuments.SessionId = s.SessionId;
                    if (s.ContentType.Contains("image"))
                    {
                        var fileData = s.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(s.FileData) : s.FileData;
                        var base64String = Convert.ToBase64String(fileData, 0, s.FileData.Length);
                        imageData = "data:" + s.ContentType + ";base64," + base64String;
                    }
                    SelfTestDocuments.ImageData = imageData;
                    SelfTestModel.Add(SelfTestDocuments);

                });
            }
            return SelfTestModel;
        }
        [HttpPost]
        [Route("GetSelfTestSearch")]
        public List<SelfTestModel> GetSelfTestSearch(HumanMovementSearchModel searchModel)
        {
            List<SelfTestModel> humanMovementModels = new List<SelfTestModel>();
            var humanMovement = _context.SelfTest.Include(a => a.Employee).Include(p => p.Employee.Plant).Include(p => p.Employee.Plant).Include(c => c.Company).Include(d=>d.Employee.Designation).Include(e=>e.Employee.DepartmentNavigation).Where(s => s.SelfTestId > 0 && s.IsVoidStaus == null);
            if (searchModel.StartDate != null && searchModel.EndDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime.Value.Date >= searchModel.StartDate.Value.Date);
            }
            if (searchModel.EndDate != null && searchModel.StartDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime.Value.Date <= searchModel.EndDate.Value.Date);
            }
            if (searchModel.StartDate != null && searchModel.EndDate != null)
            {
                humanMovement = humanMovement.Where(s => (s.DateAndTime.Value.Date >= searchModel.StartDate.Value.Date) && (s.DateAndTime.Value.Date <= searchModel.EndDate.Value.Date));
            }
            if (searchModel.CompanyId.Count > 0 && searchModel.AllVisitorCompany == false)
            {
                humanMovement = humanMovement.Where(s => searchModel.CompanyId.Contains(s.CompanyId));
                if (searchModel.IsSwstaff == "")
                {
                    humanMovement = humanMovement.Where(o => o.IsSwstaff != null);
                }
            }
            if (searchModel.TesKitResult!=null)
            {
                humanMovement = humanMovement.Where(o => o.TesKitResult == searchModel.TesKitResult);
            }
            if (searchModel.AllVisitorCompany == true)
            {
                humanMovement = humanMovement.Where(o => o.IsSwstaff == false);
            }
            if (searchModel.LocationId.Count > 0 && searchModel.AllLocation == false)
            {
                humanMovement = humanMovement.Where(s => searchModel.LocationId.Contains(s.Location));
            }
            var humanMovements = humanMovement.OrderByDescending(o => o.SelfTestId).AsNoTracking().ToList();
            List<SelfTest> companyFilters = new List<SelfTest>();
            if (searchModel.IsSwstaff != null)
            {
                if (searchModel.IsSwstaff == "Yes")
                {
                    humanMovements = humanMovements.Where(o => o.IsSwstaff == true).ToList();
                }
                else if (searchModel.IsSwstaff == "No")
                {
                    humanMovements = humanMovement.Where(o => o.IsSwstaff == false).ToList();
                }
                else
                {
                    /*else if (searchModel.IsSwstaff == "No")
                    {
                        humanMovements = humanMovements.Where(o => o.IsSwstaff == false).ToList();
                    }*/
                    if (searchModel.IsSwstaff == "SWGP")
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == "SWMY" || h.Employee.Plant.PlantCode.Trim() == "SWSG")
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                    else
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == searchModel.IsSwstaff)
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                }
            }

            if (humanMovements != null && humanMovements.Count > 0)
            {
                var sessionIds = humanMovements.Where(s => s.SessionId != null).Select(s => s.SessionId).ToList();
                var documnts = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.ContentType, s.FileName, s.IsCompressed }).Where(w => sessionIds.Contains(w.SessionId)).ToList();

                var humanMovementCompanyRelated = _context.SelfTestCompanyRelated.Include(e => e.Employee).ToList();
                var humanMovementPrivate = _context.SelfTestPrivate.Include(e => e.Employee).ToList();
                string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\SelfTest\";
                humanMovements.ForEach(s =>
                {
                    SelfTestModel humanMovementModel = new SelfTestModel();
                    humanMovementModel.SelfTestId = s.SelfTestId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null;
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null);
                    humanMovementModel.SageId = s.Employee != null ? s.Employee.SageId : null;
                    humanMovementModel.Department = s.Employee?.DepartmentNavigation?.Name;
                    humanMovementModel.Designation = s.Employee?.Designation?.Name;
                    //humanMovementModel.Department = s.Employee != null && s.Employee.DepartmentNavigation != null ? s.Employee.DepartmentNavigation.Name : "";
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.CompanyName = s.IsSwstaff == false ? (s.Company != null ? s.Company.CompanyName : s.NameOfCompany) : (s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null);
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.PlantCode = s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null;
                    humanMovementModel.SelfTestDate = s.SelfTestDate;
                    humanMovementModel.TesKitResult = s.TesKitResult;
                    humanMovementModel.SelfTestKitLotNo = s.SelfTestKitLotNo;
                    humanMovementModel.TimeOfSelfTest = s.TimeOfSelfTest;
                    humanMovementModel.NricNo = s.NricNo;
                    humanMovementModel.SessionId = s.SessionId;
                    humanMovementModel.IsVoidStaus = s.IsVoidStaus;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.SelfTestId == s.SelfTestId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        humanMovementModel.PurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.SelfTestId == s.SelfTestId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.SelfTestId == s.SelfTestId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.SelfTestStatusId = s.SelfTestStatusId;
                    humanMovementModel.DocumentID = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.DocumentId : 0;
                    humanMovementModel.ContentType = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.ContentType : "";
                    humanMovementModel.FileName = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.FileName : "";
                    humanMovementModel.Type = "Document";
                    if (humanMovementModel.ContentType != null && humanMovementModel.ContentType.Contains("image"))
                    {
                        int lastIndex = humanMovementModel.FileName.LastIndexOf('.');
                        var ext = humanMovementModel.FileName.Substring(lastIndex + 1);
                        var fileName = s.SessionId + "." + ext.ToString();
                        if (System.IO.File.Exists(serverPath + fileName))
                        {
                            humanMovementModel.ImageType = humanMovementModel.ContentType;
                            humanMovementModel.ImageData = "SelfTest/" + fileName;
                        }

                    }
                    /*humanMovementModel.ImageData = "";
                    if (humanMovementModel.ContentType!=null && humanMovementModel.ContentType.Contains("image"))
                    {
                        var IsCompressed=documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.IsCompressed : false;
                        var FileData=documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.FileData;
                        if (FileData != null)
                        {
                            humanMovementModel.ImageType = humanMovementModel.ContentType;
                            var fileData = IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(FileData) : FileData;
                            var base64String = Convert.ToBase64String(fileData, 0, FileData.Length);
                            humanMovementModel.ImageData = "data:" + humanMovementModel.ContentType + ";base64," + base64String;
                        }
                    }*/
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels.OrderBy(w => w.PlantCode).ToList();
        }
        [HttpPost]
        [Route("GetSelfTestPdfSearch")]
        public List<SelfTestModel> GetSelfTestPdfSearch(HumanMovementSearchModel searchModel)
        {
            List<SelfTestModel> humanMovementModels = new List<SelfTestModel>();
            var humanMovement = _context.SelfTest.Include(a => a.Employee).Include(p => p.Employee.Plant).Include(d => d.Employee.DepartmentNavigation).Include(p => p.Employee.Plant).Include(c => c.Company).Where(s => s.SelfTestId > 0);
            if (searchModel.StartDate != null && searchModel.EndDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime >= searchModel.StartDate);
            }
            if (searchModel.EndDate != null && searchModel.StartDate == null)
            {
                humanMovement = humanMovement.Where(s => s.DateAndTime <= searchModel.EndDate);
            }
            if (searchModel.StartDate != null && searchModel.EndDate != null)
            {
                humanMovement = humanMovement.Where(s => (s.DateAndTime >= searchModel.StartDate) && (s.DateAndTime <= searchModel.EndDate));
            }
            if (searchModel.CompanyId.Count > 0 && searchModel.AllVisitorCompany == false)
            {
                humanMovement = humanMovement.Where(s => searchModel.CompanyId.Contains(s.CompanyId));
                if (searchModel.IsSwstaff == "")
                {
                    humanMovement = humanMovement.Where(o => o.IsSwstaff != null);
                }
            }
            if (searchModel.AllVisitorCompany == true)
            {
                humanMovement = humanMovement.Where(o => o.IsSwstaff == false);
            }
            var humanMovements = humanMovement.OrderByDescending(o => o.SelfTestId).AsNoTracking().ToList();
            List<SelfTest> companyFilters = new List<SelfTest>();
            if (searchModel.IsSwstaff != null)
            {
                if (searchModel.IsSwstaff == "Yes")
                {
                    humanMovements = humanMovements.Where(o => o.IsSwstaff == true).ToList();
                }
                else
                {
                    /*else if (searchModel.IsSwstaff == "No")
                    {
                        humanMovements = humanMovements.Where(o => o.IsSwstaff == false).ToList();
                    }*/
                    if (searchModel.IsSwstaff == "SWGP")
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == "SWMY" || h.Employee.Plant.PlantCode.Trim() == "SWSG")
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                    else
                    {
                        humanMovements.ForEach(h =>
                        {
                            if (h.Employee != null && h.Employee.Plant != null)
                            {
                                if (h.Employee.Plant.PlantCode.Trim() == searchModel.IsSwstaff)
                                {
                                    companyFilters.Add(h);
                                }
                            }
                        });
                        humanMovements = companyFilters;
                    }
                }
            }

            if (humanMovements != null && humanMovements.Count > 0)
            {
                var sessionIds = humanMovements.Where(s => s.SessionId != null).Select(s => s.SessionId).ToList();
                var documnts = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.ContentType, s.FileName, s.IsCompressed, s.FileData }).Where(w => sessionIds.Contains(w.SessionId)).ToList();

                var humanMovementCompanyRelated = _context.HumanMovementCompanyRelated.Include(e => e.Employee).ToList();
                var humanMovementPrivate = _context.HumanMovementPrivate.Include(e => e.Employee).ToList();
                humanMovements.ForEach(s =>
                {
                    SelfTestModel humanMovementModel = new SelfTestModel();
                    humanMovementModel.SelfTestId = s.SelfTestId;
                    humanMovementModel.DateAndTime = s.DateAndTime;
                    humanMovementModel.Time = (Convert.ToDateTime(s.DateAndTime)).ToString("HH:mm:ss");
                    humanMovementModel.Location = s.Location;
                    humanMovementModel.Language = s.Language;
                    humanMovementModel.IsSwstaffFlag = s.IsSwstaff == null ? "Inter Company" : (s.IsSwstaff == true ? "Yes" : "No");
                    humanMovementModel.EmployeeId = s.EmployeeId;
                    humanMovementModel.SunEmployeeName = s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null;
                    humanMovementModel.EmployeeName = s.IsSwstaff == false ? s.EmployeeName : (s.Employee != null ? (s.Employee.FirstName + " " + s.Employee.LastName) : null);
                    humanMovementModel.SageId = s.Employee != null ? s.Employee.SageId : null;
                    humanMovementModel.Department = s.Employee != null && s.Employee.DepartmentNavigation != null ? s.Employee.DepartmentNavigation.Name : "";
                    humanMovementModel.ContactNo = s.ContactNo;
                    humanMovementModel.VisitingPurpose = s.VisitingPurpose;
                    humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                    humanMovementModel.CompanyId = s.CompanyId;
                    humanMovementModel.CompanyName = s.IsSwstaff == false ? (s.Company != null ? s.Company.CompanyName : s.NameOfCompany) : (s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null);
                    humanMovementModel.NameOfCompany = s.NameOfCompany;
                    humanMovementModel.CompanyRelatedpurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                    humanMovementModel.PlantCode = s.Employee != null && s.Employee.Plant != null ? s.Employee.Plant.PlantCode : null;
                    humanMovementModel.SelfTestDate = s.SelfTestDate;
                    humanMovementModel.TesKitResult = s.TesKitResult;
                    humanMovementModel.SelfTestKitLotNo = s.SelfTestKitLotNo;
                    humanMovementModel.TimeOfSelfTest = s.TimeOfSelfTest;
                    humanMovementModel.NricNo = s.NricNo;
                    humanMovementModel.SessionId = s.SessionId;
                    if (s.VisitingPurpose == "Private")
                    {
                        humanMovementModel.PurposeOfVisit = s.PurposeOfVisit;
                        humanMovementModel.Persontosee = humanMovementPrivate != null ? (string.Join(',', humanMovementPrivate.Where(a => a.HumanMovementId == s.SelfTestId).Select(a => a.Employee.FirstName).ToList())) : "";

                    }
                    if (s.VisitingPurpose == "Company related")
                    {
                        humanMovementModel.PurposeOfVisit = s.CompanyRelatedpurposeOfVisit;
                        var humanMovementCompanyRelatedNot = humanMovementCompanyRelated.Where(a => a.EmployeeId == null && a.HumanMovementId == s.SelfTestId).Count();
                        humanMovementModel.Persontosee = (humanMovementCompanyRelatedNot == 1 ? "Not Sure, " : "") + (humanMovementCompanyRelated != null ? (string.Join(',', humanMovementCompanyRelated.Where(a => a.HumanMovementId == s.SelfTestId && a.EmployeeId != null).Select(a => a.Employee.FirstName).ToList())) : "");
                    }
                    humanMovementModel.SelfTestStatusId = s.SelfTestStatusId;
                    var humanmovementAtion = _context.HumanMovementAction.SingleOrDefault(ww => ww.HumanMovementId == s.SelfTestId);
                    humanMovementModel.Description = humanmovementAtion != null ? humanmovementAtion.Description : null;
                    humanMovementModel.Hrdescription = humanmovementAtion != null ? humanmovementAtion.Hrdescription : null;
                    humanMovementModel.DocumentID = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.DocumentId : 0;
                    humanMovementModel.ContentType = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.ContentType : "";
                    humanMovementModel.FileName = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.FileName : "";
                    humanMovementModel.Type = "Document";
                    humanMovementModel.ImageData = "";
                    if (humanMovementModel.ContentType != null && humanMovementModel.ContentType.Contains("image"))
                    {
                        var IsCompressed = documnts != null ? documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.IsCompressed : false;
                        var FileData = documnts.FirstOrDefault(a => a.SessionId == s.SessionId)?.FileData;
                        if (FileData != null)
                        {
                            humanMovementModel.ImageType = humanMovementModel.ContentType;
                            var fileData = IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(FileData) : FileData;
                            var base64String = Convert.ToBase64String(fileData, 0, FileData.Length);
                            humanMovementModel.ImageData = "data:" + humanMovementModel.ContentType + ";base64," + base64String;
                        }
                    }
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels.OrderBy(w => w.PlantCode).ToList();
        }

        [HttpPost]
        [Route("InsertSelfTest")]
        public SelfTestModel Post(SelfTestModel value)
        {
            var SessionId = Guid.NewGuid();
            var humanMovement = new SelfTest
            {
                DateAndTime = value.DateAndTime,
                Language = value.Language,
                Location = value.Location,
                IsSwstaff = value.IsSwstaffFlag == "interCompany" ? default(bool?) : (value.IsSwstaffFlag == "Yes" ? true : false),
                EmployeeId = value.EmployeeId,
                EmployeeName = value.EmployeeName,
                ContactNo = value.ContactNo,
                VisitingPurpose = value.VisitingPurpose,
                PurposeOfVisit = value.PurposeOfVisit,
                CompanyId = value.CompanyId == -1 ? null : value.CompanyId,
                NameOfCompany = value.NameOfCompany,
                CompanyRelatedpurposeOfVisit = value.CompanyRelatedpurposeOfVisit,
                HignFever = value.HignFeverFlag == "Yes" ? true : false,
                MildFever = value.MildFeverFlag == "Yes" ? true : false,
                SoreThroat = value.SoreThroatFlag == "Yes" ? true : false,
                MuscleJointPain = value.MuscleJointPainFlag == "Yes" ? true : false,
                Headache = value.HeadacheFlag == "Yes" ? true : false,
                ShortnessOfBread = value.ShortnessOfBreadFlag == "Yes" ? true : false,
                Diarrhea = value.DiarrheaFlag == "Yes" ? true : false,
                LossOfVoice = value.LossOfVoiceFlag == "Yes" ? true : false,
                Phlegm = value.PhlegmFlag == "Yes" ? true : false,
                Flu = value.FluFlag == "Yes" ? true : false,
                Gathering = value.GatheringFlag == "Yes" ? true : false,
                GatheringDetails = value.GatheringDetails,
                TravellingCountry = value.TravellingCountryFlag == "Yes" ? true : false,
                TravellingCountryDetails = value.TravellingCountryDetails,
                ContactPatients = value.ContactPatientsFlag == "Yes" ? true : false,
                ContactPatientsDetails = value.ContactPatientsDetails,
                Temperature = value.Temperature,
                SessionId = SessionId,
                NauseaVomiting = value.NauseaVomitingFlag == "Yes" ? true : false,
                Sneezing = value.SneezingFlag == "Yes" ? true : false,
                Fatigue = value.FatigueFlag == "Yes" ? true : false,
                Conjunctivits = value.ConjunctivitsFlag == "Yes" ? true : false,
                LossOfTasteSmell = value.LossOfTasteSmellFlag == "Yes" ? true : false,
                LossOfApetite = value.LossOfApetiteFlag == "Yes" ? true : false,
                RunnyStuffyNose = value.RunnyStuffyNoseFlag == "Yes" ? true : false,
                Chills = value.ChillsFlag == "Yes" ? true : false,
                NoForAllAbove = value.NoForAllAbove,
                Coughing = value.CoughingFlag == "Yes" ? true : false,
                SelfTestStatusId = value.SelfTestStatusId,
                SelfTestDate = value.SelfTestDate,
                SelfTestKitLotNo = value.SelfTestKitLotNo,
                TimeOfSelfTest = value.TimeOfSelfTest,
                TesKitResult = value.TesKitResult,
                NricNo = value.NricNo,
            };
            if (value.PersonToSeeIds != null)
            {
                value.PersonToSeeIds.ForEach(c =>
                {
                    var PersonToSeeItems = new SelfTestPrivate
                    {
                        EmployeeId = c,
                    };
                    humanMovement.SelfTestPrivate.Add(PersonToSeeItems);
                });
            }
            if (value.CompanyRelatedpersonToSeeIds != null)
            {
                value.CompanyRelatedpersonToSeeIds.ForEach(c =>
                {
                    if (c != 0)
                    {
                        var cPersonToSeeItems = new SelfTestCompanyRelated
                        {
                            EmployeeId = c == -1 ? default(long?) : c,
                        };
                        humanMovement.SelfTestCompanyRelated.Add(cPersonToSeeItems);
                    }
                });
            }
            _context.SelfTest.Add(humanMovement);
            _context.SaveChanges();
            if (value.EmployeeId != null && !string.IsNullOrEmpty(value.NricNo))
            {
                var employee = _context.Employee.SingleOrDefault(e => e.EmployeeId == value.EmployeeId);
                if (employee != null)
                {
                    employee.Nricno = value.NricNo;
                    _context.SaveChanges();

                }
            }
            value.SelfTestId = humanMovement.SelfTestId;
            value.SessionId = SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSelfTest")]
        public SelfTestModel UpdateSelfTest(SelfTestModel value)
        {
            var selfTest = _context.SelfTest.Where(i => i.SelfTestId == value.SelfTestId).FirstOrDefault();
            if (selfTest != null)
            {
                selfTest.NricNo = value.NricNo;
                selfTest.TesKitResult = value.TesKitResult;
                _context.SaveChanges();
                if (value.EmployeeId != null && !string.IsNullOrEmpty(value.NricNo))
                {
                    var employee = _context.Employee.SingleOrDefault(e => e.EmployeeId == value.EmployeeId);
                    if (employee != null)
                    {
                        employee.Nricno = value.NricNo;
                        _context.SaveChanges();

                    }
                }
            }
            return value;
        }
        [HttpPost]
        [Route("UploadSelfTestDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var contentType = file.ContentType.Split("/");
                if (contentType[0] == "image")
                {
                    var fs = file.OpenReadStream();
                    var br = new BinaryReader(fs);
                    Byte[] document = br.ReadBytes((Int32)fs.Length);
                    string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
                    string SelfTest = "SelfTest";
                    string serverPath = System.IO.Path.Combine(folderName, SelfTest);
                    if (!System.IO.Directory.Exists(serverPath))
                    {
                        System.IO.Directory.CreateDirectory(serverPath);
                    }
                    int lastIndex = file.FileName.LastIndexOf('.');
                    var ext = file.FileName.Substring(lastIndex + 1);
                    var fileName = SessionId + "." + ext.ToString();
                    serverPath = folderName + @"\" + SelfTest + @"\" + fileName;

                    GenerateThumbnails(0.2, fs, serverPath);
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        //FileData = document,
                        FileSize = fs.Length,
                        SessionId = SessionId,
                    };
                    _context.Documents.Add(documents);
                }
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpPost]
        [Route("UpdateVoidStatusSelfTest")]
        public SelfTestModel UpdateVoidStatusSelfTest(SelfTestModel selfTestModel)
        {
            var selfTest = _context.SelfTest.Where(i => i.SelfTestId == selfTestModel.SelfTestId).FirstOrDefault();
            if (selfTest != null)
            {
                selfTest.IsVoidStaus = selfTestModel.IsVoidStaus==true? selfTestModel.IsVoidStaus:null;
                _context.SaveChanges();
            }
            return selfTestModel;
        }
        [HttpGet]
        [Route("GetSelfTestDocumentsList")]
        public void GetSelfTestDocumentsList()
        {
            var humanMovement = _context.SelfTest.ToList();
            if (humanMovement != null)
            {
                string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
                string SelfTest = "SelfTest";
                string serverPath = System.IO.Path.Combine(folderName, SelfTest);
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                var sessionIds = humanMovement.Where(s => s.SessionId != null).Select(s => s.SessionId).ToList();
                var documnts = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.ContentType, s.FileName, s.IsCompressed, s.FileData }).Where(w => sessionIds.Contains(w.SessionId)).ToList();
                humanMovement.ForEach(s =>
                {
                    var documnt = documnts.FirstOrDefault(a => a.SessionId == s.SessionId);
                    if (documnt != null && documnt.ContentType != null && documnt.ContentType.Contains("image"))
                    {
                        int lastIndex = documnt.FileName.LastIndexOf('.');
                        var ext = documnt.FileName.Substring(lastIndex + 1);
                        var fileName = s.SessionId + "." + ext.ToString();
                        serverPath = folderName + @"\" + SelfTest + @"\" + fileName;
                        if (documnt.FileData != null)
                        {
                            var file = documnt.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(documnt.FileData) : documnt.FileData;
                            Stream stream = new MemoryStream(file);
                            GenerateThumbnails(0.2, stream, serverPath);
                            stream.Close();
                        }
                    }
                });
            }
        }
        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                // can given width of image as we want  
                var newWidth = (int)(image.Width * scaleFactor);
                // can given height of image as we want  
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }
        [HttpPost]
        [Route("GetLocationCompany")]
        public SearchModel GetLocationCompany(SearchModel searchModel)
        {
            var locationCompany = _context.Ictmaster.Include(c => c.Company).Where(s => s.Name == searchModel.SearchString).FirstOrDefault();
            SearchModel searchItem = new SearchModel();
            if (locationCompany != null)
            {
                searchItem.MasterTypeID = locationCompany.IctmasterId;
                searchItem.SearchString = locationCompany.Company != null ? locationCompany.Company.PlantCode : null;
            };
            return searchItem;
        }

        [HttpGet]
        [Route("GetSelfTestLocation")]
        public List<HumanMovementModel> GetSelfTestLocation()
        {
            List<HumanMovementModel> humanMovementModels = new List<HumanMovementModel>();
            var humanMovement = _context.SelfTest.Where(l => l.Location != null && l.Location != "").OrderByDescending(o => o.SelfTestId).AsNoTracking().Select(s => s.Location).Distinct().ToList();
            if (humanMovement != null && humanMovement.Count > 0)
            {
                humanMovement.ForEach(s =>
                {
                    HumanMovementModel humanMovementModel = new HumanMovementModel();
                    humanMovementModel.Location = s;
                    humanMovementModels.Add(humanMovementModel);
                });
            }
            return humanMovementModels;
        }
    }
}