﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AssetMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IHubContext<ChatHub> _hub;
        public AssetMasterController(CRT_TMSContext context, IMapper mapper, IConfiguration configuration, IHubContext<ChatHub> hub)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _hub = hub;
        }

        #region AssetMaster



        // GET: api/AssetMasters
        [HttpGet]
        [Route("GetAssetMasters")]
        public List<AssetMasterModel> Get()
        {
            var applicationUserList = _context.ApplicationUser.Where(a => a.StatusCodeId == 1).ToList();
            var assetMasters = _context.AssetMaster
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .AsNoTracking().OrderByDescending(o => o.AssetMasterId).ToList();
            List<AssetMasterModel> AssetMasterModels = new List<AssetMasterModel>();
            assetMasters.ForEach(s =>
            {
                AssetMasterModel AssetMasterModel = new AssetMasterModel
                {
                    AssetMasterId = s.AssetMasterId,
                    InventoryTypeId = s.InventoryTypeId,
                    Name = s.Name,
                    Description = s.Description,
                    AssetNo = s.AssetNo,
                    LocationId = s.LocationId,
                    CategoryId = s.CategoryId,
                    SubCategoryId = s.SubCategoryId,
                    VendorId = s.VendorId,
                    MakeId = s.MakeId,
                    SerialNo = s.SerialNo,
                    ModelNo = s.ModelNo,
                    AcquiredDate = s.AcquiredDate,
                    DisposedDate = s.DisposedDate,
                    PurchasePrice = s.PurchasePrice,
                    DepriciatedToDate = s.DepriciatedToDate,
                    ExpiryDate = s.ExpiryDate,
                    DepriciationBasePrice = s.DepriciationBasePrice,
                    DepriciableLife = s.DepriciableLife,
                    DepricationMethod = s.DepricationMethod,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    SessionId = s.SessionId,
                };
                AssetMasterModels.Add(AssetMasterModel);
            });

            return AssetMasterModels.ToList();
        }


        [HttpGet]
        [Route("GetAssetMaintenanceById")]
        public List<AssetMaintenanceModel> GetAssetMaintenanceById(long? id)
        {
            var applicationUserList = _context.ApplicationUser.Where(a => a.StatusCodeId == 1).ToList();
            var assetMaintenance = _context.AssetMaintenance
                                 .Include(s => s.AddedByUser)
                                 .Include(s => s.ModifiedByUser)
                                 .Include(s => s.StatusCode)
                                 .Include(s=>s.PerformedByNavigation)
                                .AsNoTracking().Where(s => s.AssetId == id).ToList();
            List<AssetMaintenanceModel> assetMaintenanceModels = new List<AssetMaintenanceModel>();
            assetMaintenance.ForEach(s =>
            {
                AssetMaintenanceModel assetMaintenanceModel = new AssetMaintenanceModel
                {
                    AssetMaintenanceId = s.AssetMaintenanceId,
                    Description = s.Description,
                    MaintenanceDate = s.MaintenanceDate,
                    NextMaintenanceDate = s.NextMaintenanceDate,
                    AssetId = s.AssetId,
                    PerformedBy = s.PerformedBy,
                    Cost = s.Cost,
                    DownDuration = s.DownDuration,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    Performed = s.PerformedByNavigation?.UserName,

                };
                assetMaintenanceModels.Add(assetMaintenanceModel);
            });

            return assetMaintenanceModels.ToList();
        }

        [HttpGet]
        [Route("GetAssetAssignmentById")]
        public List<AssetAssignmentModel> GetAssetAssignmentById(long? id)
        {

            var assetAssignment = _context.AssetAssignment
                                  .Include(s => s.AddedByUser)                                 
                                  .Include(a => a.AssetManagedByNavigation)
                                  .Include(a => a.AssignedToNavigation)
                                  .Include(a => a.AssetStatusNavigation)
                                .AsNoTracking().Where(s => s.AssetId == id).ToList();
            List<AssetAssignmentModel> assetAssignmentModels = new List<AssetAssignmentModel>();
            assetAssignment.ForEach(s =>
            {
                AssetAssignmentModel assetAssignmentModel = new AssetAssignmentModel
                {

                    
                    AssetId = s.AssetId,
                    AssetAssignId = s.AssetAssignId,
                    AssetManagedBy = s.AssetManagedBy,
                    ManagedBy = s.AssetManagedByNavigation?.UserName,
                    AssetStatus = s.AssetStatus,
                    AssignedOn = s.AssignedOn,
                    AssignedTo = s.AssignedTo,
                    AssignedToName = s.AssignedToNavigation.UserName,
                    IsLatest = s.IsLatest,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    AssetStatusName = s.AssetStatusNavigation?.CodeValue,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                    StatusCodeID = s.StatusCodeId,
                };
                assetAssignmentModels.Add(assetAssignmentModel);
            });

            return assetAssignmentModels.ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AssetMasterModel> GetData(SearchModel searchModel)
        {
            var AssetMaster = new AssetMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).LastOrDefault();
                        break;
                    case "Next":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).LastOrDefault();
                        break;
                    case "Next":
                        AssetMaster = _context.AssetMaster.OrderBy(o => o.AssetMasterId).FirstOrDefault(s => s.AssetMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        AssetMaster = _context.AssetMaster.OrderByDescending(o => o.AssetMasterId).FirstOrDefault(s => s.AssetMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AssetMasterModel>(AssetMaster);
            return result;
        }
        // POST: api/AssetMaster
        [HttpPost]
        [Route("InsertAssetMaster")]
        public AssetMasterModel Post(AssetMasterModel value)
        {
            var sessionId = Guid.NewGuid();


            var AssetMaster = new AssetMaster
            {

                InventoryTypeId = value.InventoryTypeId,
                Name = value.Name,
                Description = value.Description,
                AssetNo = value.AssetNo,
                LocationId = value.LocationId,
                CategoryId = value.CategoryId,
                SubCategoryId = value.SubCategoryId,
                VendorId = value.VendorId,
                MakeId = value.MakeId,
                SerialNo = value.SerialNo,
                ModelNo = value.ModelNo,
                AcquiredDate = value.AcquiredDate,
                DisposedDate = value.DisposedDate,
                PurchasePrice = value.PurchasePrice,
                DepriciatedToDate = value.DepriciatedToDate,
                ExpiryDate = value.ExpiryDate,
                DepriciationBasePrice = value.DepriciationBasePrice,
                DepriciableLife = value.DepriciableLife,
                DepricationMethod = value.DepricationMethod,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,


            };
            _context.AssetMaster.Add(AssetMaster);
            _context.SaveChanges();
            value.SessionId = sessionId;

            value.AssetMasterId = AssetMaster.AssetMasterId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAssetMaster")]
        public AssetMasterModel Put(AssetMasterModel value)
        {
            var assetMaster = _context.AssetMaster.SingleOrDefault(p => p.AssetMasterId == value.AssetMasterId);

            assetMaster.InventoryTypeId = value.InventoryTypeId;
            assetMaster.Name = value.Name;
            assetMaster.Description = value.Description;
            assetMaster.AssetNo = value.AssetNo;
            assetMaster.LocationId = value.LocationId;
            assetMaster.CategoryId = value.CategoryId;
            assetMaster.SubCategoryId = value.SubCategoryId;
            assetMaster.VendorId = value.VendorId;
            assetMaster.MakeId = value.MakeId;
            assetMaster.SerialNo = value.SerialNo;
            assetMaster.ModelNo = value.ModelNo;
            assetMaster.AcquiredDate = value.AcquiredDate;
            assetMaster.DisposedDate = value.DisposedDate;
            assetMaster.PurchasePrice = value.PurchasePrice;
            assetMaster.DepriciatedToDate = value.DepriciatedToDate;
            assetMaster.ExpiryDate = value.ExpiryDate;
            assetMaster.DepriciationBasePrice = value.DepriciationBasePrice;
            assetMaster.DepriciableLife = value.DepriciableLife;
            assetMaster.DepricationMethod = value.DepricationMethod;

            assetMaster.ModifiedByUserId = value.ModifiedByUserID;
            assetMaster.ModifiedDate = DateTime.Now;
            assetMaster.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAssetMaster")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var assetMaster = _context.AssetMaster.SingleOrDefault(p => p.AssetMasterId == id);
                if (assetMaster != null)
                {
                    _context.AssetMaster.Remove(assetMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("AssetMaster Cannot be delete! work order Reference to others");
            }
        }


        [HttpPost]
        [Route("InsertAssetMaintenance")]
        public AssetMaintenanceModel InsertAssetMaintenance(AssetMaintenanceModel value)
        {

            var assetmaintenance = new AssetMaintenance
            {

                Description = value.Description,
                MaintenanceDate = value.MaintenanceDate,
                NextMaintenanceDate = value.NextMaintenanceDate,
                DownDuration = value.DownDuration,
                Cost = value.Cost,
                PerformedBy = value.PerformedBy,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AssetId = value.AssetId,
            };
            _context.AssetMaintenance.Add(assetmaintenance);
            _context.SaveChanges();


            value.AssetMaintenanceId = assetmaintenance.AssetMaintenanceId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAssetMaintenance")]
        public AssetMaintenanceModel UpdateAssetMaintenance(AssetMaintenanceModel value)
        {
            var assetMaintenance = _context.AssetMaintenance.SingleOrDefault(p => p.AssetMaintenanceId == value.AssetMaintenanceId);

            assetMaintenance.Description = value.Description;
            assetMaintenance.MaintenanceDate = value.MaintenanceDate;
            assetMaintenance.NextMaintenanceDate = value.NextMaintenanceDate;
            assetMaintenance.DownDuration = value.DownDuration;
            assetMaintenance.Cost = value.Cost;
            assetMaintenance.PerformedBy = value.PerformedBy;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAssetMaintenance")]
        public ActionResult<string> DeleteAssetMaintenance(int id)
        {
            try
            {
                var assetMaintenace = _context.AssetMaintenance.SingleOrDefault(p => p.AssetMaintenanceId == id);
                if (assetMaintenace != null)
                {
                    _context.AssetMaintenance.Remove(assetMaintenace);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("AssetMaster Cannot be delete! work order Reference to others");
            }
        }


        [HttpPost]
        [Route("InsertAssetAssignment")]
        public AssetAssignmentModel InsertAssetAssignment(AssetAssignmentModel value)
        {

            var assetAssignment = new AssetAssignment
            {

                AssetId = value.AssetId,

                AssetManagedBy = value.AssetManagedBy,
                AssetStatus = value.AssetStatus,
                AssignedOn = value.AssignedOn,
                AssignedTo = value.AssignedTo,
                IsLatest = value.IsLatest,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.AssetAssignment.Add(assetAssignment);
            _context.SaveChanges();


            value.AssetAssignId = assetAssignment.AssetAssignId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAssetAssignment")]
        public AssetAssignmentModel UpdateAssetAssignment(AssetAssignmentModel value)
        {
            var assetAssignment = _context.AssetAssignment.SingleOrDefault(p => p.AssetAssignId == value.AssetAssignId);

            assetAssignment.AssetId = value.AssetId;

            assetAssignment.AssetManagedBy = value.AssetManagedBy;
            assetAssignment.AssetStatus = value.AssetStatus;
            assetAssignment.AssignedOn = value.AssignedOn;
            assetAssignment.AssignedTo = value.AssignedTo;
            assetAssignment.IsLatest = value.IsLatest;
            assetAssignment.ModifiedByUserId = value.ModifiedByUserID;
            assetAssignment.ModifiedDate = DateTime.Now;
            assetAssignment.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAssetAssignment")]
        public ActionResult<string> DeleteAssetAssignment(int id)
        {
            try
            {
                var assetAssignment = _context.AssetAssignment.SingleOrDefault(p => p.AssetAssignId == id);
                if (assetAssignment != null)
                {
                    _context.AssetAssignment.Remove(assetAssignment);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Asset Assignment Cannot be delete! work order Reference to others");
            }
        }
        #endregion




    }
}
