﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IssueReportIPIRController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _config;

        public IssueReportIPIRController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment hostingEnvironment, GenerateDocumentNoSeries generateDocumentNoSeries, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _config = config;
        }

        [HttpGet]
        [Route("GetIssueReportIPIRItems")]
        public List<IssueReportIPIRModel> GetIssueReportIPIRItems()
        {
            List<IssueReportIPIRModel> issueReportIPIRModels = new List<IssueReportIPIRModel>();
            var issueReportIPIRItems = _context.IssueReportIpir
                                    .Include(s => s.IssueReportAssignTo)
                                    .Include(s => s.IssueReportCcto)
                                    .Include(s => s.StatusCode)
                                    .Include(s => s.AddedByUser)
                                    .Include(s => s.ModifiedByUser)
                                    .OrderByDescending(o => o.Ipirid).AsNoTracking().ToList();
            issueReportIPIRItems.ForEach(s =>
            {
                IssueReportIPIRModel issueReportIPIRModel = new IssueReportIPIRModel
                {
                    Ipirid = s.Ipirid,
                    Description = s.Description,
                    BatchSize = s.BatchSize,
                    Location = s.Location,
                    ProdOrderNo = s.ProdOrderNo,
                    Uom = s.Uom,
                    ProfileId = s.ProfileId,
                    ProfileNo = s.ProfileNo,
                    CompanyId = s.CompanyId,
                    BatchNo = s.BatchNo,
                    IssueTitle = s.IssueTitle,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DocSessionId = s.DocSessionId,
                };
                foreach (var item in s.IssueReportAssignTo)
                {
                    issueReportIPIRModel.AssignTos = new List<long>();
                    issueReportIPIRModel.AssignTos.Add(item.AssignToId.Value);
                }
                foreach (var item in s.IssueReportCcto)
                {
                    issueReportIPIRModel.CCTos = new List<long>();
                    issueReportIPIRModel.CCTos.Add(item.CctoId.Value);
                }
                issueReportIPIRModels.Add(issueReportIPIRModel);

            });
            return issueReportIPIRModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<IssueReportIPIRModel> GetData(SearchModel searchModel)
        {
            var issueReportIpir = new IssueReportIpir();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                    case "Last":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Next":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Previous":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).FirstOrDefault();
                        break;
                    case "Last":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).LastOrDefault();
                        break;
                    case "Next":
                        issueReportIpir = _context.IssueReportIpir.OrderBy(o => o.Ipirid).FirstOrDefault(s => s.Ipirid > searchModel.Id);
                        break;
                    case "Previous":
                        issueReportIpir = _context.IssueReportIpir.OrderByDescending(o => o.Ipirid).FirstOrDefault(s => s.Ipirid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<IssueReportIPIRModel>(issueReportIpir);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertIssueReportIpir")]
        public IssueReportIPIRModel Post(IssueReportIPIRModel value)
        {
            try
            {
                var plant = _context.Plant.FirstOrDefault(p => p.NavCompanyName == value.CompanyName);
                var reportNo = "";
                if (value.ProfileId > 0 && value.DocSessionId != null)
                {
                    reportNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "IPIR Mobile" });
                }
                var sessionId = value.DocSessionId ?? Guid.NewGuid();
                var issueReportIpir = new IssueReportIpir
                {
                    Location = value.Location,
                    ProdOrderNo = value.ProdOrderNo,
                    Description = value.Description,
                    BatchSize = value.BatchSize,
                    Uom = value.Uom,
                    BatchNo = value.BatchNo,
                    IssueTitle = value.IssueTitle,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                    DocSessionId = sessionId,
                    ProfileId = value.ProfileId,
                    CompanyId = plant.PlantId,
                    ProfileNo = reportNo,
                };

                value.AssignTos.ForEach(a =>
                {
                    issueReportIpir.IssueReportAssignTo.Add(new IssueReportAssignTo { AssignToId = a });
                });
                value.CCTos.ForEach(a =>
                {
                    issueReportIpir.IssueReportCcto.Add(new IssueReportCcto { CctoId = a });
                });

                _context.IssueReportIpir.Add(issueReportIpir);
                _context.SaveChanges();
                value.IssueTitle = issueReportIpir.IssueTitle;
                value.ProfileNo = issueReportIpir.ProfileNo;
                value.Ipirid = issueReportIpir.Ipirid;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateIssueReportIPIR")]
        public IssueReportIPIRModel Put(IssueReportIPIRModel value)
        {
            var ipirItem = _context.IssueReportIpir.Include(i => i.IssueReportAssignTo).Include(i => i.IssueReportCcto).FirstOrDefault(f => f.Ipirid == value.Ipirid);
            ipirItem.Location = value.Location;
            ipirItem.IssueTitle = value.IssueTitle;
            ipirItem.BatchNo = value.BatchNo;
            ipirItem.ProdOrderNo = value.ProdOrderNo;
            ipirItem.Description = value.Description;
            ipirItem.BatchNo = value.BatchNo;
            ipirItem.ModifiedDate = DateTime.Now;
            ipirItem.StatusCodeId = value.StatusCodeID.Value;
            ipirItem.DocSessionId = value.DocSessionId ?? Guid.NewGuid();
            if (ipirItem.IssueReportAssignTo.Any())
            {
                _context.IssueReportAssignTo.RemoveRange(ipirItem.IssueReportAssignTo);
            }
            value.AssignTos.ForEach(a =>
            {
                ipirItem.IssueReportAssignTo.Add(new IssueReportAssignTo { AssignToId = a, Ipirid = ipirItem.Ipirid });
            });
            if (ipirItem.IssueReportCcto.Any())
            {
                _context.IssueReportCcto.RemoveRange(ipirItem.IssueReportCcto);
            }
            value.CCTos.ForEach(a =>
            {
                ipirItem.IssueReportCcto.Add(new IssueReportCcto { CctoId = a, Ipirid = ipirItem.Ipirid });
            });
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteIssueReportIPIR")]
        public void Delete(int id)
        {
            try
            {
                var issueReportIpir = _context.IssueReportIpir.Include(a => a.IssueReportAssignTo).Include(c => c.IssueReportCcto).SingleOrDefault(p => p.Ipirid == id);
                if (issueReportIpir != null)
                {
                    if (issueReportIpir.IssueReportAssignTo.Any())
                    {
                        _context.IssueReportAssignTo.RemoveRange(issueReportIpir.IssueReportAssignTo);
                    }
                    if (issueReportIpir.IssueReportCcto.Any())
                    {
                        _context.IssueReportCcto.RemoveRange(issueReportIpir.IssueReportCcto);
                    }
                    _context.IssueReportIpir.Remove(issueReportIpir);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Issue Report IPIR Cannot be Delete! Reference to Others", ex);
            }
        }

    }
}
