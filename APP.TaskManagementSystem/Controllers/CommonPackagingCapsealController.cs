﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingCapsealController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingCapsealController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingCapseal")]
        public List<CommonPackagingCapsealModel> Get()
        {
            var commonPackagingCapseal = _context.CommonPackagingCapseal
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingCapsealModel> commonPackagingCapsealModel = new List<CommonPackagingCapsealModel>();
            if (commonPackagingCapseal.Count > 0)
            {
                List<long?> masterIds = commonPackagingCapseal.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingCapseal.ForEach(s =>
                {
                    CommonPackagingCapsealModel commonPackagingCapsealModels = new CommonPackagingCapsealModel
                    {
                        CapsealSpecificationId = s.CapsealSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        MeasurementThickness = s.MeasurementThickness != 0 ? s.MeasurementThickness : null,
                        Thickness = s.Thickness != 0 ? s.Thickness : null,
                        Printing = s.Printing,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.CapsealSpecificationId == s.CapsealSpecificationId).Select(b => b.UseForItemId).ToList(),
                        StatusCodeID = s.StatusCodeId,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    };
                    commonPackagingCapsealModel.Add(commonPackagingCapsealModels);
                });
            }
            return commonPackagingCapsealModel.OrderByDescending(a => a.CapsealSpecificationId).ToList();
        }

        [HttpPost()]
        [Route("GetCommonPackagingCapsealByRefByNo")]
        public List<CommonPackagingCapsealModel> GetCommonPackagingCapsealByRefByNo(RefSearchModel refSearchModel)
        {
            var commonPackagingCapseal = _context.CommonPackagingCapseal
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).Where(w => w.CapsealSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingCapseal = commonPackagingCapseal.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingCapseal = commonPackagingCapseal.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingCapseals = commonPackagingCapseal.AsNoTracking().ToList();
            List<CommonPackagingCapsealModel> commonPackagingCapsealModel = new List<CommonPackagingCapsealModel>();
            if (commonPackagingCapseals.Count > 0)
            {
                List<long?> masterIds = commonPackagingCapseals.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingCapseals.ForEach(s =>
                {
                    CommonPackagingCapsealModel commonPackagingCapsealModels = new CommonPackagingCapsealModel
                    {
                        CapsealSpecificationId = s.CapsealSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        MeasurementThickness = s.MeasurementThickness != 0 ? s.MeasurementThickness : null,
                        Thickness = s.Thickness != 0 ? s.Thickness : null,
                        Printing = s.Printing,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.CapsealSpecificationId == s.CapsealSpecificationId).Select(b => b.UseForItemId).ToList(),
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingCapsealModel.Add(commonPackagingCapsealModels);
                });
            }
            return commonPackagingCapsealModel.OrderByDescending(o => o.CapsealSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingCapsealModel> GetData(SearchModel searchModel)
        {
            var commonPackagingCapseal = new CommonPackagingCapseal();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderBy(o => o.CapsealSpecificationId).FirstOrDefault(s => s.CapsealSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingCapseal = _context.CommonPackagingCapseal.OrderByDescending(o => o.CapsealSpecificationId).FirstOrDefault(s => s.CapsealSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingCapsealModel>(commonPackagingCapseal);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingCapseal")]
        public CommonPackagingCapsealModel Post(CommonPackagingCapsealModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingCapseal" });
            var commonPackagingCapseal = new CommonPackagingCapseal
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                MeasurementWidth = value.MeasurementWidth,
                MeasurementLength = value.MeasurementLength,
                MeasurementThickness = value.MeasurementThickness,
                Thickness = value.Thickness,
                Printing = value.Printing,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingCapseal.Add(commonPackagingCapseal);
            _context.SaveChanges();

            value.MasterProfileReferenceNo = commonPackagingCapseal.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingCapseal.ProfileLinkReferenceNo;
            value.CapsealSpecificationId = commonPackagingCapseal.CapsealSpecificationId;
            value.PackagingItemName = UpdateCommonPackage(value);
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.CapsealSpecificationId == value.CapsealSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        CapsealSpecificationId = value.CapsealSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingCapsealModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    if (value.MeasurementLength != null && value.MeasurementLength > 0)
                    {
                        itemName = value.MeasurementLength + "mm" + "(L)" + " ";
                    }
                    if (value.MeasurementWidth != null && value.MeasurementWidth > 0)
                    {
                        itemName = itemName + "X" + " " + value.MeasurementWidth + "mm" + "(W)" + " ";
                    }
                    if (value.MeasurementThickness != null && value.MeasurementThickness > 0)
                    {
                        itemName = itemName + "X" + " " + value.MeasurementThickness + "mm";
                    }
                    if (value.Thickness != null && value.Thickness > 0)
                    {
                        itemName = itemName + "±" + value.Thickness + "mm" + "(T)";
                    }



                    itemName = itemName + " " + (value.Printing == true ? "With" : "Without") + " " + "Printing Capseal";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingCapseal")]
        public CommonPackagingCapsealModel Put(CommonPackagingCapsealModel value)
        {
            var commonPackagingCapseal = _context.CommonPackagingCapseal.SingleOrDefault(p => p.CapsealSpecificationId == value.CapsealSpecificationId);
            commonPackagingCapseal.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingCapseal.Set = value.Set;
            commonPackagingCapseal.MeasurementWidth = value.MeasurementWidth;
            commonPackagingCapseal.MeasurementLength = value.MeasurementLength;
            commonPackagingCapseal.MeasurementThickness = value.MeasurementThickness;
            commonPackagingCapseal.Thickness = value.Thickness;
            commonPackagingCapseal.Printing = value.Printing;
            commonPackagingCapseal.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingCapseal.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingCapseal.VersionControl = value.VersionControl;
            commonPackagingCapseal.MeasurementThickness = value.MeasurementThickness;
            commonPackagingCapseal.Thickness = value.Thickness;
            commonPackagingCapseal.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingCapseal.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingCapseal.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.CapsealSpecificationId == value.CapsealSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.CapsealSpecificationId == value.CapsealSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        CapsealSpecificationId = value.CapsealSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingCapseal")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingCapseal = _context.CommonPackagingCapseal.Where(p => p.CapsealSpecificationId == id).FirstOrDefault();
                if (commonPackagingCapseal != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.CapsealSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingCapseal.Remove(commonPackagingCapseal);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}