﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonFieldsProductionMachineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonFieldsProductionMachineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonFieldsProductionMachine")]
        public List<CommonFieldsProductionMachineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.LocationNavigation)
                .Include(p => p.Profile)
                .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModel = new List<CommonFieldsProductionMachineModel>();
            commonFieldsProductionMachine.ForEach(s =>
            {
                CommonFieldsProductionMachineModel commonFieldsProductionMachineModels = new CommonFieldsProductionMachineModel
                {
                    CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingProcessId = s.ManufacturingProcessId,
                    ManufacturingStepsId = s.ManufacturingStepsId,
                    MachineGroupingId = s.MachineGroupingId,
                    ManufacturingSiteName = s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingStepsName = s.ManufacturingStepsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineGroupingName = s.MachineGroupingId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineNo = s.MachineNo,
                    NameOfTheMachine = s.NameOfTheMachine,
                    Location = s.Location,
                    LocationName = s.LocationNavigation != null ? s.LocationNavigation.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    SessionId = s.SessionId,
                };
                commonFieldsProductionMachineModel.Add(commonFieldsProductionMachineModels);
            });
            return commonFieldsProductionMachineModel.Where(w => w.ItemClassificationMasterId == id).OrderByDescending(a => a.CommonFieldsProductionMachineId).ToList();
        }

        [HttpGet]
        [Route("GetCommonFieldsProductionMachineAll")]
        public List<CommonFieldsProductionMachineModel> GetCommonFieldsProductionMachineAll()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.LocationNavigation)
                .Include(p => p.Profile)
                .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModel = new List<CommonFieldsProductionMachineModel>();
            commonFieldsProductionMachine.ForEach(s =>
            {
                CommonFieldsProductionMachineModel commonFieldsProductionMachineModels = new CommonFieldsProductionMachineModel();
                commonFieldsProductionMachineModels.CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId;
                commonFieldsProductionMachineModels.ManufacturingSiteId = s.ManufacturingSiteId;
                commonFieldsProductionMachineModels.ManufacturingProcessId = s.ManufacturingProcessId;
                commonFieldsProductionMachineModels.ManufacturingStepsId = s.ManufacturingStepsId;
                commonFieldsProductionMachineModels.MachineGroupingId = s.MachineGroupingId;
                commonFieldsProductionMachineModels.ManufacturingSiteName = s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "";
                commonFieldsProductionMachineModels.ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "";
                commonFieldsProductionMachineModels.ManufacturingStepsName = s.ManufacturingStepsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "";
                commonFieldsProductionMachineModels.MachineGroupingName = s.MachineGroupingId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "";
                commonFieldsProductionMachineModels.MachineNo = s.MachineNo;
                commonFieldsProductionMachineModels.NameOfTheMachine = s.NameOfTheMachine;
                commonFieldsProductionMachineModels.Location = s.Location;
                commonFieldsProductionMachineModels.LocationName = s.LocationNavigation != null ? s.LocationNavigation.Name : "";
                commonFieldsProductionMachineModels.StatusCodeID = s.StatusCodeId;
                commonFieldsProductionMachineModels.AddedByUserID = s.AddedByUserId;
                commonFieldsProductionMachineModels.ModifiedByUserID = s.ModifiedByUserId;
                commonFieldsProductionMachineModels.AddedDate = s.AddedDate;
                commonFieldsProductionMachineModels.ModifiedDate = s.ModifiedDate;
                commonFieldsProductionMachineModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                commonFieldsProductionMachineModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                commonFieldsProductionMachineModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                commonFieldsProductionMachineModels.ProfileReferenceNo = s.ProfileReferenceNo;
                commonFieldsProductionMachineModels.ItemClassificationMasterId = s.ItemClassificationMasterId;
                commonFieldsProductionMachineModels.ProfileId = s.ProfileId;
                commonFieldsProductionMachineModels.ProfileName = s.Profile != null ? s.Profile.Name : "";
                commonFieldsProductionMachineModels.SessionId = s.SessionId;
                commonFieldsProductionMachineModels.MachineNameList = commonFieldsProductionMachineModels.ManufacturingSiteName + "|" + s.NameOfTheMachine;
                commonFieldsProductionMachineModel.Add(commonFieldsProductionMachineModels);
            });
            return commonFieldsProductionMachineModel.OrderByDescending(a => a.CommonFieldsProductionMachineId).ToList();
        }
        [Route("GetCommonFieldsProductionMachineManufactureSite")]
        public List<CommonFieldsProductionMachineModel> CommonFieldsProductionMachineManufactureSite(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.LocationNavigation)
                .Include(p => p.Profile)
                .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModel = new List<CommonFieldsProductionMachineModel>();
            commonFieldsProductionMachine.ForEach(s =>
            {
                CommonFieldsProductionMachineModel commonFieldsProductionMachineModels = new CommonFieldsProductionMachineModel
                {
                    CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingProcessId = s.ManufacturingProcessId,
                    ManufacturingStepsId = s.ManufacturingStepsId,
                    MachineGroupingId = s.MachineGroupingId,
                    ManufacturingSiteName = s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingStepsName = s.ManufacturingStepsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineGroupingName = s.MachineGroupingId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineNo = s.MachineNo,
                    NameOfTheMachine = s.NameOfTheMachine,
                    Location = s.Location,
                    LocationName = s.LocationNavigation != null ? s.LocationNavigation.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    MachineName = s.MachineNo + " | " + s.NameOfTheMachine,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    SessionId = s.SessionId,
                };
                commonFieldsProductionMachineModel.Add(commonFieldsProductionMachineModels);
            });
            return commonFieldsProductionMachineModel.Where(w => w.ManufacturingSiteId == id).OrderByDescending(a => a.CommonFieldsProductionMachineId).ToList();
        }
        [Route("GetCommonFieldsProductionMachineName")]
        public List<CommonFieldsProductionMachineModel> GetCommonFieldsProductionMachineName(string type)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.LocationNavigation)
                .Include(p => p.Profile)
                .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineModel> commonFieldsProductionMachineModel = new List<CommonFieldsProductionMachineModel>();
            commonFieldsProductionMachine.ForEach(s =>
            {
                CommonFieldsProductionMachineModel commonFieldsProductionMachineModels = new CommonFieldsProductionMachineModel
                {
                    CommonFieldsProductionMachineId = s.CommonFieldsProductionMachineId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingProcessId = s.ManufacturingProcessId,
                    ManufacturingStepsId = s.ManufacturingStepsId,
                    MachineGroupingId = s.MachineGroupingId,
                    ManufacturingSiteName = s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingStepsName = s.ManufacturingStepsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingStepsId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineGroupingName = s.MachineGroupingId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.MachineGroupingId).Select(m => m.Value).FirstOrDefault() : "",
                    MachineNo = s.MachineNo,
                    NameOfTheMachine = s.NameOfTheMachine,
                    Location = s.Location,
                    LocationName = s.LocationNavigation != null ? s.LocationNavigation.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    MachineName = s.MachineNo + " | " + s.NameOfTheMachine,
                    SessionId = s.SessionId,
                    MachinesName = (s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "") + " | " + s.MachineNo + " | " + s.NameOfTheMachine,

                };
                commonFieldsProductionMachineModel.Add(commonFieldsProductionMachineModels);
            });
            return commonFieldsProductionMachineModel.Where(w => w.MachineGroupingName == type).OrderByDescending(a => a.CommonFieldsProductionMachineId).ToList();
        }
        [HttpPost]
        [Route("InsertCommonFieldsProductionMachine")]
        public CommonFieldsProductionMachineModel Post(CommonFieldsProductionMachineModel value)
        {
            var SessionId = Guid.NewGuid();
            var profileNo = "";
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MachineNo });
            }
            var commonFieldsProductionMachine = new CommonFieldsProductionMachine
            {
                ManufacturingSiteId = value.ManufacturingSiteId,
                ManufacturingProcessId = value.ManufacturingProcessId,
                ManufacturingStepsId = value.ManufacturingStepsId,
                MachineGroupingId = value.MachineGroupingId,
                MachineNo = value.MachineNo,
                NameOfTheMachine = value.NameOfTheMachine,
                Location = value.Location,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
                SessionId = SessionId,
            };
            _context.CommonFieldsProductionMachine.Add(commonFieldsProductionMachine);
            _context.SaveChanges();
            value.CommonFieldsProductionMachineId = commonFieldsProductionMachine.CommonFieldsProductionMachineId;
            value.ProfileReferenceNo = commonFieldsProductionMachine.ProfileReferenceNo;
            value.SessionId = commonFieldsProductionMachine.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonFieldsProductionMachine")]
        public CommonFieldsProductionMachineModel Put(CommonFieldsProductionMachineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine.SingleOrDefault(p => p.CommonFieldsProductionMachineId == value.CommonFieldsProductionMachineId);
            commonFieldsProductionMachine.ManufacturingSiteId = value.ManufacturingSiteId;
            commonFieldsProductionMachine.ManufacturingProcessId = value.ManufacturingProcessId;
            commonFieldsProductionMachine.ManufacturingStepsId = value.ManufacturingStepsId;
            commonFieldsProductionMachine.MachineGroupingId = value.MachineGroupingId;
            commonFieldsProductionMachine.MachineNo = value.MachineNo;
            commonFieldsProductionMachine.NameOfTheMachine = value.NameOfTheMachine;
            commonFieldsProductionMachine.Location = value.Location;
            commonFieldsProductionMachine.StatusCodeId = value.StatusCodeID.Value;
            commonFieldsProductionMachine.ModifiedByUserId = value.ModifiedByUserID;
            commonFieldsProductionMachine.ModifiedDate = DateTime.Now;
            commonFieldsProductionMachine.ProfileReferenceNo = value.ProfileReferenceNo;
            commonFieldsProductionMachine.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonFieldsProductionMachine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonFieldsProductionMachine = _context.CommonFieldsProductionMachine.Where(p => p.CommonFieldsProductionMachineId == id).FirstOrDefault();
                if (commonFieldsProductionMachine != null)
                {
                    var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLine.Where(p => p.CommonFieldsProductionMachineId == id).ToList();
                    if (commonFieldsProductionMachineLine != null)
                    {
                        commonFieldsProductionMachineLine.ForEach(h =>
                        {
                            var machineDocumentbandlist = _context.CommonFieldsProductionMachineDocument.Where(p => p.SessionId == h.SessionId).Select(s => s.MachineDocumentId).ToList();
                            if (machineDocumentbandlist != null)
                            {
                                machineDocumentbandlist.ForEach(m =>
                                {
                                    var machineDocumentbanditem = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == m);
                                    _context.CommonFieldsProductionMachineDocument.Remove(machineDocumentbanditem);
                                    _context.SaveChanges();
                                });

                            }
                        });
                        _context.CommonFieldsProductionMachineLine.RemoveRange(commonFieldsProductionMachineLine);
                        _context.SaveChanges();
                    }
                    _context.CommonFieldsProductionMachine.Remove(commonFieldsProductionMachine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}