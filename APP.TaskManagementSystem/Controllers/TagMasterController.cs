﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TagMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TagMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetTagMaster")]
        public List<TagMasterModel> Get()
        {
            List<TagMasterModel> tagMasterModels = new List<TagMasterModel>();
            var tagMaster = _context.TagMaster.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.TagMasterId).AsNoTracking().ToList();
            if(tagMaster!=null && tagMaster.Count>0)
            {
                tagMaster.ForEach(s =>
                {
                    TagMasterModel tagMasterModel = new TagMasterModel();
                    tagMasterModel.TagMasterID = s.TagMasterId;
                    tagMasterModel.AddedByUser = s.AddedByUser.UserName;
                    tagMasterModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    tagMasterModel.AddedByUserID = s.AddedByUserId;
                    tagMasterModel.AddedDate = DateTime.Now;
                    tagMasterModel.StatusCodeID = s.StatusCodeId;
                    tagMasterModel.ModifiedByUserID = s.ModifiedByUserId;
                    tagMasterModel.ModifiedDate = s.ModifiedDate;
                    tagMasterModel.Name = s.Name;
                    tagMasterModels.Add(tagMasterModel);

                });                

            }
          
            return tagMasterModels;
        }
        [HttpGet]
        [Route("GetActiveTagMaster")]
        public List<TagMasterModel> GetActiveTagMaster()
        {
            List<TagMasterModel> tagMasterModels = new List<TagMasterModel>();
            var tagMaster = _context.TagMaster.Include("AddedByUser").Include("ModifiedByUser").Where(s=>s.StatusCodeId == 1).OrderByDescending(o => o.TagMasterId).AsNoTracking().ToList();
            if (tagMaster != null && tagMaster.Count > 0)
            {
                tagMaster.ForEach(s =>
                {
                    TagMasterModel tagMasterModel = new TagMasterModel();
                    tagMasterModel.TagMasterID = s.TagMasterId;
                    tagMasterModel.AddedByUser = s.AddedByUser.UserName;
                    tagMasterModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    tagMasterModel.AddedByUserID = s.AddedByUserId;
                    tagMasterModel.AddedDate = DateTime.Now;
                    tagMasterModel.StatusCodeID = s.StatusCodeId;
                    tagMasterModel.ModifiedByUserID = s.ModifiedByUserId;
                    tagMasterModel.ModifiedDate = s.ModifiedDate;
                    tagMasterModel.Name = s.Name;
                    tagMasterModels.Add(tagMasterModel);

                });

            }

            return tagMasterModels;
        }
        [HttpGet]
        [Route("GetTasksByTag")]
        public List<TaskTagModel> GetTasksByTag(int id)
        {

            var tagMaster = _context.TaskTag.Include("Tag").Include("TaskMaster").Select(s => new TaskTagModel
            {
                TaskMasterID = s.TaskMasterId,
                TagID = s.TagId,
                TaskTagID = s.TaskTagId,
                TagName = s.Tag.Name,
                TaskName = s.TaskMaster.Title,

            }).Where(o => o.TagID == id).AsNoTracking().ToList();
            return tagMaster;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<TagMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var tagMaster = _context.TagMaster.SingleOrDefault(p => p.TagMasterId == id.Value);
            var result = _mapper.Map<TagMasterModel>(tagMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TagMasterModel> GetData(SearchModel searchModel)
        {
            var tagMaster = new TagMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).LastOrDefault();
                        break;
                    case "Next":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).LastOrDefault();
                        break;
                    case "Next":
                        tagMaster = _context.TagMaster.OrderBy(o => o.TagMasterId).FirstOrDefault(s => s.TagMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        tagMaster = _context.TagMaster.OrderByDescending(o => o.TagMasterId).FirstOrDefault(s => s.TagMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TagMasterModel>(tagMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTagMaster")]
        public TagMasterModel Post(TagMasterModel value)
        {
            var tagMaster = new TagMaster
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Name = value.Name,

            };
            _context.TagMaster.Add(tagMaster);
            _context.SaveChanges();
            value.TagMasterID = tagMaster.TagMasterId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTagMaster")]
        public TagMasterModel Put(TagMasterModel value)
        {
            var tagMaster = _context.TagMaster.SingleOrDefault(p => p.TagMasterId == value.TagMasterID);
            tagMaster.ModifiedByUserId = value.ModifiedByUserID;
            tagMaster.ModifiedDate = DateTime.Now;
            tagMaster.Name = value.Name;
            tagMaster.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateTaskTag")]
        public TaskTagModel UpdateTaskTag(TaskTagModel value)
        {
            var tagmaster = value.TaskTagList;
            if (tagmaster != null && tagmaster.Count != 0)
            {
                tagmaster.ForEach(t =>
                {
                    if (t.IsSelected == true)
                    {
                        var taskTag = _context.TaskTag.SingleOrDefault(p => p.TaskTagId == t.TaskTagID);
                        taskTag.TagId = value.TagID;
                        taskTag.TaskMasterId = t.TaskMasterID;
                        _context.SaveChanges();
                    }

                });
            }
            else
            {
                if (value.TaskTagID > 0)
                {
                    var taskTag = _context.TaskTag.SingleOrDefault(p => p.TaskTagId == value.TaskTagID);
                    taskTag.TagId = value.TagID;
                    taskTag.TaskMasterId = value.TaskMasterID;
                    _context.SaveChanges();
                }
            }

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTagMaster")]
        public void Delete(int id)
        {
            try
            {
                var tagMaster = _context.TagMaster.SingleOrDefault(p => p.TagMasterId == id);
                if (tagMaster != null)
                {
                    _context.TagMaster.Remove(tagMaster);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Tag assigned to some other records, User unable to delete this tag!", null);
            }
        }
    }
}