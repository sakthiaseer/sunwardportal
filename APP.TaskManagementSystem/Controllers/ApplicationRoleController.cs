﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationRoleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationRoleController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApplicationRoles")]
        public List<ApplicationRoleModel> Get()
        {
            //var ApplicationRole = _context.ApplicationRole.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new ApplicationRoleModel
            var applicationRole = _context.ApplicationRole.Select(s => new ApplicationRoleModel
            {
                RoleID = s.RoleId,
                RoleName = s.RoleName,
                RoleDescription = s.RoleDescription,            
               
                ModifiedByUserID = s.AddedByUserId,
                AddedByUserID = s.ModifiedByUserId,
                StatusCodeID = s.StatusCodeId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                StatusCode = s.StatusCode.CodeValue,


            }).OrderByDescending(o => o.RoleID).ToList();
            return applicationRole;
        }

      
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationRoleModel> GetData(SearchModel searchModel)
        {
            var ApplicationRole = new ApplicationRole();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).LastOrDefault();
                        break;
                    case "Previous":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationRole = _context.ApplicationRole.OrderBy(o => o.RoleId).FirstOrDefault(s => s.RoleId > searchModel.Id);
                        break;
                    case "Previous":
                        ApplicationRole = _context.ApplicationRole.OrderByDescending(o => o.RoleId).FirstOrDefault(s => s.RoleId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationRoleModel>(ApplicationRole);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationRole")]
        public ApplicationRoleModel Post(ApplicationRoleModel value)
        {
            var applicationRole = new ApplicationRole
            {
                //ApplicationRoleID = value.ApplicationRoleId,
                RoleName = value.RoleName,
                RoleDescription = value.RoleDescription,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };
            _context.ApplicationRole.Add(applicationRole);

           

            _context.SaveChanges();
            value.RoleID = applicationRole.RoleId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationRole")]
        public ApplicationRoleModel Put(ApplicationRoleModel value)
        {
            var applicationRole = _context.ApplicationRole.SingleOrDefault(p => p.RoleId == value.RoleID);


            applicationRole.RoleName = value.RoleName;
            applicationRole.RoleDescription = value.RoleDescription;
            applicationRole.ModifiedByUserId = value.ModifiedByUserID;
            applicationRole.ModifiedDate = DateTime.Now;
            applicationRole.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationRole")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var ApplicationRole = _context.ApplicationRole.SingleOrDefault(p => p.RoleId == id);
                if (ApplicationRole != null)
                {
                    _context.ApplicationRole.Remove(ApplicationRole);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Role Cannot be delete! Role Reference to others");
            }
        }
    }
}