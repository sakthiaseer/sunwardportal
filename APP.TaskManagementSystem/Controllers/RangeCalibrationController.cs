﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RangeCalibrationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public RangeCalibrationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetRangeCalibrations")]
        public List<RangeCalibrationModel> Get()
        {
            var rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).AsNoTracking().ToList();
            List<RangeCalibrationModel> rangeCalibrationModels = new List<RangeCalibrationModel>();
            rangeCalibration.ForEach(s =>
            {
                RangeCalibrationModel rangeCalibrationModel = new RangeCalibrationModel
                {
                    RangeCalibrationID = s.RangeCalibrationId,
                    ParameterRange = s.ParameterRange,
                    Max = s.Max,
                    Min = s.Min,
                    UnitOfMeasure = s.UnitOfMeasure,
                    Remarks = s.Remarks,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,

                };
                rangeCalibrationModels.Add(rangeCalibrationModel);
            });

            return rangeCalibrationModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetRangeCalibration/{id:int}")]
        public ActionResult<RangeCalibrationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var rangeCalibration = _context.RangeCalibration.SingleOrDefault(p => p.RangeCalibrationId == id.Value);
            var result = _mapper.Map<RangeCalibrationModel>(rangeCalibration);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RangeCalibrationModel> GetData(SearchModel searchModel)
        {
            var rangeCalibration = new RangeCalibration();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).FirstOrDefault();
                        break;
                    case "Last":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).LastOrDefault();
                        break;
                    case "Next":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).LastOrDefault();
                        break;
                    case "Previous":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).FirstOrDefault();
                        break;
                    case "Last":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).LastOrDefault();
                        break;
                    case "Next":
                        rangeCalibration = _context.RangeCalibration.OrderBy(o => o.RangeCalibrationId).FirstOrDefault(s => s.RangeCalibrationId > searchModel.Id);
                        break;
                    case "Previous":
                        rangeCalibration = _context.RangeCalibration.OrderByDescending(o => o.RangeCalibrationId).FirstOrDefault(s => s.RangeCalibrationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<RangeCalibrationModel>(rangeCalibration);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertRangeCalibration")]
        public RangeCalibrationModel Post(RangeCalibrationModel value)
        {
            var rangeCalibration = new RangeCalibration
            {
                //RangeCalibrationID = value.RangeCalibrationId,
                ParameterRange = value.ParameterRange,
                Max = value.Max,
                Min = value.Min,
                UnitOfMeasure = value.UnitOfMeasure,
                Remarks = value.Remarks,
                CalibrationServiceInformationId = value.CalibrationServiceInformationID,

            };


            _context.RangeCalibration.Add(rangeCalibration);
            _context.SaveChanges();
            value.RangeCalibrationID = rangeCalibration.RangeCalibrationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateRangeCalibration")]
        public RangeCalibrationModel Put(RangeCalibrationModel value)
        {
            var rangeCalibration = _context.RangeCalibration.SingleOrDefault(p => p.RangeCalibrationId == value.RangeCalibrationID);

            rangeCalibration.ParameterRange = value.ParameterRange;
            rangeCalibration.Max = value.Max;
            rangeCalibration.Min = value.Min;
            rangeCalibration.UnitOfMeasure = value.UnitOfMeasure;
            rangeCalibration.Remarks = value.Remarks;
            rangeCalibration.CalibrationServiceInformationId = value.CalibrationServiceInformationID;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteRangeCalibration")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var rangeCalibration = _context.RangeCalibration.SingleOrDefault(p => p.RangeCalibrationId == id);
                if (rangeCalibration != null)
                {
                    _context.RangeCalibration.Remove(rangeCalibration);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}