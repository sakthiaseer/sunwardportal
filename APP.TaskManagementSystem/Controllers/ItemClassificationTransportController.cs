using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemClassificationTransportController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ItemClassificationTransportController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetItemClassificationTransports")]
        public List<ItemClassificationTransPortModel> Get()
        {
            List<ItemClassificationTransPortModel> itemClassificationTransports = new List<ItemClassificationTransPortModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var itemClassificationTransportslist = _context.ItemClassificationTransport
                                           .Include(a => a.AddedByUser).Include(s => s.StatusCode).Include(c=>c.Company).Include(p => p.Profile)
                                            .Include(m => m.ModifiedByUser).OrderByDescending(o => o.TransportId).AsNoTracking()
                                            .ToList();

            if (itemClassificationTransportslist != null && itemClassificationTransportslist.Count > 0)
            {
                itemClassificationTransportslist.ForEach(s =>
                {
                    ItemClassificationTransPortModel itemClassificationTransport = new ItemClassificationTransPortModel
                    {
                        TransportId = s.TransportId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileID = s.ProfileId,
                        CompanyId = s.CompanyId,
                        NameOfTransportation = s.NameOfTransportation,
                        Purpose = s.Purpose,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        TransportCompanyName = s.Company != null ? s.Company.Description : null,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                    };
                    itemClassificationTransports.Add(itemClassificationTransport);
                });

            }
            return itemClassificationTransports;
        }

        [HttpGet]
        [Route("GetItemClassificationTransportsByRefNo")]
        public List<ItemClassificationTransPortModel> GetItemClassificationTransportsByRefNo(int? id)
        {

            List<ItemClassificationTransPortModel> itemClassificationTransports = new List<ItemClassificationTransPortModel>();

            var ItemClassificationTransportlist = _context.ItemClassificationTransport
                                            .Include(a => a.AddedByUser).Include(s => s.StatusCode).Include(c => c.Company).Include(p => p.Profile)
                                             .Include(m => m.ModifiedByUser).Where(s=>s.ItemClassificationMasterId==id).OrderByDescending(o => o.TransportId).AsNoTracking()
                                             .ToList();
            if (ItemClassificationTransportlist != null && ItemClassificationTransportlist.Count > 0)
            {
                ItemClassificationTransportlist.ForEach(s =>
                {
                    ItemClassificationTransPortModel ItemClassificationTransport = new ItemClassificationTransPortModel
                    {
                        TransportId = s.TransportId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileID = s.ProfileId,
                        CompanyId = s.CompanyId,
                        NameOfTransportation = s.NameOfTransportation,
                        Purpose = s.Purpose,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        TransportCompanyName = s.Company != null ? s.Company.Description : null,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                    };
                    itemClassificationTransports.Add(ItemClassificationTransport);
                });

            }
            return itemClassificationTransports;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertItemClassificationTransport")]
        public ItemClassificationTransPortModel Post(ItemClassificationTransPortModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title=value.NameOfTransportation });

            var itemClassificationTransport = new ItemClassificationTransport
            {
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileId = value.ProfileID,
                CompanyId = value.CompanyId,
                NameOfTransportation = value.NameOfTransportation,
                Purpose = value.Purpose,
                ProfileReferenceNo = profileNo,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                
            };
            _context.ItemClassificationTransport.Add(itemClassificationTransport);
            _context.SaveChanges();
            value.TransportId = itemClassificationTransport.TransportId;
            value.ProfileReferenceNo = itemClassificationTransport.ProfileReferenceNo;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemClassificationTransport")]
        public ItemClassificationTransPortModel Put(ItemClassificationTransPortModel value)
        {
            var itemClassificationTransport = _context.ItemClassificationTransport.SingleOrDefault(p => p.TransportId == value.TransportId);
            itemClassificationTransport.ItemClassificationMasterId = value.ItemClassificationMasterId;
            itemClassificationTransport.CompanyId = value.CompanyId;
            itemClassificationTransport.NameOfTransportation = value.NameOfTransportation;
            itemClassificationTransport.Purpose = value.Purpose;
            itemClassificationTransport.StatusCodeId = value.StatusCodeID.Value;
            itemClassificationTransport.ModifiedByUserId = value.ModifiedByUserID.Value;
            itemClassificationTransport.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemClassificationTransport")]
        public void Delete(int id)
        {
            var itemClassificationTransport = _context.ItemClassificationTransport.SingleOrDefault(p => p.TransportId == id);
            if (itemClassificationTransport != null)
            {
                _context.ItemClassificationTransport.Remove(itemClassificationTransport);
                _context.SaveChanges();
            }
        }
    }
}