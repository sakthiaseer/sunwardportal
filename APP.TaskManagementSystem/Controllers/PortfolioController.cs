﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PortfolioController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PortfolioController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #region Portfolio
        [HttpGet]
        [Route("GetPortfolio")]
        public List<PortfolioModel> Get()
        {
            List<PortfolioModel> PortfolioModels = new List<PortfolioModel>();
            var Portfolio = _context.Portfolio
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(p => p.Company)
                                           .Include(m => m.ModifiedByUser)
                                           .Include(a => a.OnBehalf)
                                           .Include(a => a.PortfolioOnBehalf)
                                           .OrderByDescending(o => o.PortfolioId).AsNoTracking()
                                           .ToList();

            if (Portfolio != null && Portfolio.Count > 0)
            {
                var PortfolioIds = Portfolio.Select(a => a.PortfolioId).ToList();
                var userIds = _context.PortfolioOnBehalf.Where(w => PortfolioIds.Contains(w.PortfolioId.Value)).Select(s => s.OnBehalfId.GetValueOrDefault(0)).ToList();
                var users = _context.ApplicationUser.Where(a => userIds.Contains(a.UserId)).ToList();
                Portfolio.ForEach(s =>
                {
                    PortfolioModel PortfolioModel = new PortfolioModel();

                    PortfolioModel.PortfolioId = s.PortfolioId;
                    PortfolioModel.OnBehalfId = s.OnBehalfId;
                    PortfolioModel.CompanyId = s.CompanyId;
                    PortfolioModel.CompanyName = s.Company?.Description;
                    PortfolioModel.PortfolioTypeId = s.PortfolioTypeId;
                    PortfolioModel.AddedByUserID = s.AddedByUserId;
                    PortfolioModel.ModifiedByUserID = s.ModifiedByUserId;
                    PortfolioModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                    PortfolioModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    PortfolioModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                    PortfolioModel.StatusCodeID = s.StatusCodeId;
                    PortfolioModel.AddedDate = s.AddedDate;
                    PortfolioModel.ModifiedDate = s.ModifiedDate;
                    PortfolioModel.PortfolioType = s.PortfolioType?.Value;
                    PortfolioModel.SessionId = s.SessionId;
                    PortfolioModel.OnBehalfIds = s.PortfolioOnBehalf != null ? s.PortfolioOnBehalf.Select(s => s.OnBehalfId).ToList() : new List<long?>();
                    PortfolioModel.OnBehalfUser = users != null && PortfolioModel.OnBehalfIds.Count > 0 ? string.Join(",", users.Where(u => PortfolioModel.OnBehalfIds.Contains(u.UserId)).Select(ss => ss.UserName).ToList()) : "";
                    PortfolioModels.Add(PortfolioModel);
                });

            }
            return PortfolioModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PortfolioModel> GetData(SearchModel searchModel)
        {
            var Portfolio = new Portfolio();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).FirstOrDefault();
                        break;
                    case "Last":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).LastOrDefault();
                        break;
                    case "Next":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).LastOrDefault();
                        break;
                    case "Previous":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).FirstOrDefault();
                        break;
                    case "Last":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).LastOrDefault();
                        break;
                    case "Next":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderBy(o => o.PortfolioId).FirstOrDefault(s => s.PortfolioId > searchModel.Id);
                        break;
                    case "Previous":
                        Portfolio = _context.Portfolio.Include(a => a.PortfolioOnBehalf).OrderByDescending(o => o.PortfolioId).FirstOrDefault(s => s.PortfolioId < searchModel.Id);
                        break;
                }
            }


            var result = _mapper.Map<PortfolioModel>(Portfolio);
            if (Portfolio != null)
            {
                result.OnBehalfIds = Portfolio.PortfolioOnBehalf != null ? Portfolio.PortfolioOnBehalf.Select(s => s.OnBehalfId).ToList() : new List<long?>();
            }
            return result;
        }
        [HttpPost]
        [Route("InsertPortfolio")]
        public PortfolioModel Post(PortfolioModel value)
        {
            var sessionId = Guid.NewGuid();
            var Portfolio = new Portfolio
            {

                OnBehalfId = value.OnBehalfId,
                CompanyId = value.CompanyId,
                PortfolioTypeId = value.PortfolioTypeId,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,

                AddedDate = DateTime.Now,
                SessionId = sessionId,
            };
            if (value.OnBehalfIds != null && value.OnBehalfIds.Count > 0)
            {
                value.OnBehalfIds.ForEach(a =>
                {
                    var onBehalf = new PortfolioOnBehalf
                    {
                        OnBehalfId = a,
                    };
                    Portfolio.PortfolioOnBehalf.Add(onBehalf);
                });
            }
            _context.Portfolio.Add(Portfolio);
            _context.SaveChanges();

            value.PortfolioId = Portfolio.PortfolioId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePortfolio")]
        public PortfolioModel Put(PortfolioModel value)
        {
            var Portfolio = _context.Portfolio.SingleOrDefault(p => p.PortfolioId == value.PortfolioId);

            Portfolio.StatusCodeId = value.StatusCodeID.Value;
            Portfolio.ModifiedByUserId = value.ModifiedByUserID.Value;
            Portfolio.ModifiedDate = DateTime.Now;
            Portfolio.OnBehalfId = value.OnBehalfId;
            Portfolio.PortfolioTypeId = value.PortfolioTypeId;
            Portfolio.CompanyId = value.CompanyId;
            var PortfolioOnBehalf = _context.PortfolioOnBehalf.Where(p => p.PortfolioId == value.PortfolioId).ToList();
            if (PortfolioOnBehalf != null)
            {
                _context.PortfolioOnBehalf.RemoveRange(PortfolioOnBehalf);
                _context.SaveChanges();
            }
            if (value.OnBehalfIds != null && value.OnBehalfIds.Count > 0)
            {
                value.OnBehalfIds.ForEach(a =>
                {
                    var onBehalf = new PortfolioOnBehalf
                    {
                        PortfolioId = value.PortfolioId,
                        OnBehalfId = a,
                    };
                    _context.PortfolioOnBehalf.Add(onBehalf);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeletePortfolio")]
        public void Delete(int id)
        {
            try
            {
                var Portfolio = _context.Portfolio.SingleOrDefault(p => p.PortfolioId == id);
                if (Portfolio != null)
                {
                    var PortfolioOnBehalf = _context.PortfolioOnBehalf.Where(p => p.PortfolioId == id).ToList();
                    if (PortfolioOnBehalf != null)
                    {
                        _context.PortfolioOnBehalf.RemoveRange();
                        _context.SaveChanges();
                    }
                    _context.Portfolio.Remove(Portfolio);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("This Portfolio Reference to others Cannot delete");
            }
        }
        #endregion
        #region PortfolioLine
        [HttpGet]
        [Route("GetPortfolioLinesByID")]
        public List<PortfolioLineModel> GetPortfolioLine(long? id)
        {
            List<PortfolioLineModel> PortfolioLineModels = new List<PortfolioLineModel>();
            List<ApplicationWiki> applicationWikis = new List<ApplicationWiki>();
            var PortfolioLine = _context.PortfolioLine
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(m => m.ModifiedByUser)
                                           .Include(a => a.ChileMaster)
                                           .Where(w => w.PortfolioId == id)
                                           .OrderByDescending(o => o.PortfolioLineId).AsNoTracking()
                                           .ToList();
            if (PortfolioLine != null && PortfolioLine.Count > 0)
            {

                var wikireferenceIds = PortfolioLine.Select(s => s.WikiReferenceId).ToList();
                if (wikireferenceIds != null && wikireferenceIds.Count > 0)
                {
                    applicationWikis = _context.ApplicationWiki.Include(a => a.WikiOwner).Include(a => a.WikiType).Where(a => a.StatusCodeId == 841 && wikireferenceIds.Contains(a.ApplicationWikiId)).AsNoTracking().ToList();
                }
            }
            if (PortfolioLine != null && PortfolioLine.Count > 0)
            {
                PortfolioLine.ForEach(s =>
                {
                    PortfolioLineModel portfolioLineModel = new PortfolioLineModel();
                    var appwiki = applicationWikis.FirstOrDefault(w => w.ApplicationWikiId == s.WikiReferenceId);
                    portfolioLineModel.PortfolioLineId = s.PortfolioLineId;
                    portfolioLineModel.PortfolioId = s.PortfolioId;
                    portfolioLineModel.WikiReferenceId = s.WikiReferenceId;
                    portfolioLineModel.Remarks = s.Remarks;
                    portfolioLineModel.AddedByUserID = s.AddedByUserId;
                    portfolioLineModel.ModifiedByUserID = s.ModifiedByUserId;
                    if (appwiki != null)
                    {
                        portfolioLineModel.WikiReference = appwiki.WikiType?.Value + " | " + appwiki.WikiOwner?.Name + " | " + appwiki.Title;
                    }
                    portfolioLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                    portfolioLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    portfolioLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                    portfolioLineModel.StatusCodeID = s.StatusCodeId;
                    portfolioLineModel.AddedDate = s.AddedDate;
                    portfolioLineModel.ModifiedDate = s.ModifiedDate;
                    portfolioLineModel.SessionId = s.SessionId;
                    portfolioLineModel.VersionNo = s.VersionNo;
                    portfolioLineModel.NameOfReferenceType = s.NameOfReferenceType;
                    portfolioLineModel.ChileMasterId = s.ChileMasterId;
                    portfolioLineModel.ChileMaster = s.ChileMaster?.Value;
                    PortfolioLineModels.Add(portfolioLineModel);
                });

            }
            return PortfolioLineModels;
        }
        [HttpPost]
        [Route("InsertPortfolioLine")]
        public PortfolioLineModel Post(PortfolioLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var PortfolioLine = new PortfolioLine
            {
                PortfolioId = value.PortfolioId,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
                Remarks = value.Remarks,
                WikiReferenceId = value.WikiReferenceId,
                VersionNo = value.VersionNo,
                NameOfReferenceType = value.NameOfReferenceType,
                ChileMasterId = value.ChileMasterId,
            };
            _context.PortfolioLine.Add(PortfolioLine);
            _context.SaveChanges();
            value.PortfolioLineId = PortfolioLine.PortfolioLineId;
            value.SessionId = sessionId;

            return value;
        }
        [HttpPut]
        [Route("UpdatePortfolioLine")]
        public PortfolioLineModel UpdatePortfolioLine(PortfolioLineModel value)
        {
            var PortfolioLine = _context.PortfolioLine.SingleOrDefault(p => p.PortfolioLineId == value.PortfolioLineId);
            PortfolioLine.PortfolioId = value.PortfolioId;

            PortfolioLine.StatusCodeId = value.StatusCodeID.Value;
            PortfolioLine.ModifiedByUserId = value.ModifiedByUserID.Value;
            PortfolioLine.ModifiedDate = DateTime.Now;
            PortfolioLine.Remarks = value.Remarks;
            PortfolioLine.VersionNo = value.VersionNo;
            PortfolioLine.WikiReferenceId = value.WikiReferenceId;
            PortfolioLine.NameOfReferenceType = value.NameOfReferenceType;
            PortfolioLine.ChileMasterId = value.ChileMasterId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeletePortfolioLine")]
        public void DeletePortfolioLine(int id)
        {
            var PortfolioLine = _context.PortfolioLine.SingleOrDefault(p => p.PortfolioLineId == id);
            if (PortfolioLine != null)
            {
                _context.PortfolioLine.Remove(PortfolioLine);
                _context.SaveChanges();
            }
        }

        [HttpPut]
        [Route("CheckOutDoc")]
        public PortfolioAttachmentModel CheckOutDoc(PortfolioAttachmentModel value)
        {
            if (value.DocumentID > 0)
            {
                var attachment = _context.PortfolioAttachment.FirstOrDefault(l => l.DocumentId == value.DocumentID);
                //var attachments = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
                //var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();


                if (attachment.UploadedByUserId != value.AddedByUserID)
                {
                    throw new AppException("Required permission to read this file.!", null);
                }
                else
                {
                    if (attachment != null)
                    {
                        attachment.IsLocked = true;
                        attachment.LockedDate = DateTime.Now;
                        attachment.LockedByUserId = value.AddedByUserID;

                    }
                }
                var documents = _context.Documents.FirstOrDefault(f => f.DocumentId == value.DocumentID);
                if (documents != null)
                {

                    documents.IsLocked = true;
                    documents.LockedByUserId = value.AddedByUserID;
                    documents.LockedDate = DateTime.Now;
                }
                _context.SaveChanges();
            }
            return value;
        }

        [HttpPut]
        [Route("CheckInDoc")]
        public PortfolioAttachmentModel CheckInDoc(PortfolioAttachmentModel value)
        {
            //try
            //{ 
            //var doclist = _context.Documents.AsNoTracking().ToList();
            if (value.DocumentID > 0)
            {

                var attachment = _context.PortfolioAttachment.FirstOrDefault(l => l.DocumentId == value.DocumentID && l.PortfolioLineId == value.PortfolioLineId);



                if (attachment != null)
                {
                    attachment.IsLocked = true;
                    attachment.LockedDate = DateTime.Now;
                    attachment.LockedByUserId = value.AddedByUserID;
                    attachment.IsLatest = false;
                    _context.SaveChanges();
                }

                var docu = _context.Documents.FirstOrDefault(f => f.DocumentId == value.DocumentID);
                if (docu != null)
                {
                    docu.IsLatest = false;
                }

                var portfolio = _context.PortfolioLine.Where(f => f.PortfolioLineId == value.PortfolioLineId).FirstOrDefault();
                var sageNo = "";
                var companyName = "";
                var typeofportfolio = "";
                var versionNo = "";

                if (portfolio != null)
                {
                    if (portfolio.PortfolioId != null)
                    {
                        var portfoliodata = _context.Portfolio.Include(f => f.Company).Include(e => e.OnBehalf).Include(e => e.OnBehalf.Employe).Include(t => t.PortfolioType).FirstOrDefault(f => f.PortfolioId == portfolio.PortfolioId);
                        if (portfoliodata != null)
                        {
                            sageNo = _context.Employee.FirstOrDefault(f => f.UserId == portfoliodata.OnBehalfId)?.SageId;
                            companyName = portfoliodata?.Company?.PlantCode;
                            typeofportfolio = portfoliodata?.PortfolioType?.Value;
                            versionNo = portfolio?.VersionNo;
                        }
                    }
                }

                if (value.SessionID != null)
                {


                    var documents = _context.Documents
                        .Select(s => new Documents
                        {
                            DocumentId = s.DocumentId,
                            IsTemp = s.IsTemp,
                            Description = s.Description,
                            SessionId = s.SessionId,
                            FileName = s.FileName,
                        }).Where(d => d.SessionId == value.SessionID).ToList();
                    if (documents != null && documents.Count > 0)
                    {
                        documents.ForEach(file =>
                        {
                            var fileindex = "";
                            var FileName = typeofportfolio + "|" + companyName + "|" + sageNo + "|" + DateTime.Now.Date.ToString("dd/MM/yyyy") + "|" + versionNo + "|" + file.FileName;

                            var existdocument = _context.Documents.FirstOrDefault(f => f.FileName == FileName);
                            file.IsTemp = false;

                            PortfolioAttachment portfolioAttachment = new PortfolioAttachment();

                            portfolioAttachment.DocumentId = file.DocumentId;
                            portfolioAttachment.IsLatest = true;
                            portfolioAttachment.IsLocked = false;
                            portfolioAttachment.PortfolioLineId = value.PortfolioLineId;
                            portfolioAttachment.UploadedDate = DateTime.Now;
                            portfolioAttachment.UploadedByUserId = value.UploadedByUserId;

                            portfolioAttachment.FileName = FileName;
                            portfolioAttachment.PreviousDocumentId = attachment.PreviousDocumentId != null ? attachment.PreviousDocumentId : attachment.PortfolioAttachmentId;
                            _context.PortfolioAttachment.Add(portfolioAttachment);

                            _context.SaveChanges();

                            var docu = _context.Documents.FirstOrDefault(f => f.DocumentId == file.DocumentId);
                            if (docu != null)
                            {
                                docu.FileName = FileName;
                            }

                        });

                    }
                }

                _context.SaveChanges();
            }

            //}
            //catch (Exception ex)
            //{



            //}
            return value;
        }
        #endregion

    }
}
