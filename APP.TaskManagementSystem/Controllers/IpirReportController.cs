﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IpirReportController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly MailService _mailService;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public IpirReportController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host, MailService mailService, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _mailService = mailService;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        #region IPIR
        [HttpGet]
        [Route("GetIpirReportByForum")]
        public List<ProductActivityAppModel> GetIpirReportByForum(long? id)
        {
            var productActivityAppQuery = _context.IpirReport
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcess)
                .Include(a => a.Company)
                .Include(a => a.StatusCode)
                .Where(w => w.IpirReportId == id);
            var productActivityApps = productActivityAppQuery.ToList();
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();

                productActivityApps.ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    if (s.ProductActivityCaseId > 0)
                    {
                        var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                        var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                        var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                        var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                        var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                        var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                        var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                        var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                        if (appUserss != null && appUserss.Count > 0)
                        {
                            var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                            if (usersIdsBy != null && usersIdsBy.Count > 0)
                            {
                                responsibilityUsers.AddRange(usersIdsBy);
                            }
                        }
                    }
                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    productActivityApp.Type = "Ipir";
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityAppLineId = s.IpirReportId;
                    productActivityApp.Comment = s.Description;
                    productActivityApp.LineComment = s.Description;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.CompanyId;
                    productActivityApp.CompanyName = s.Company?.PlantCode;
                    productActivityApp.ProdOrderNo = s.ProdOrder;
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessId;
                    productActivityApp.TopicId = s.TopicId;

                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.ModifiedDate = counts.UploadDate;
                            productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                        }
                        productActivityAppModels.Add(productActivityApp);
                    }
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetIpirReportLine")]
        public List<ProductActivityAppModel> GetIpirReportLine(long? id, string type, long? userId, long? locationId)
        {
            var productActivityAppQuery = _context.IpirReport
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcess)
                .Include(a => a.Company)
                .Include(a => a.StatusCode)
                .Where(w => w.CompanyId == id);
            
            if (locationId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.LocationId == locationId);
            }
            if (!string.IsNullOrEmpty(type))
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ProdOrder == type);
            }

                var productActivityApps = productActivityAppQuery.ToList();
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityAppLineDoc.Where(w => w.Type == "Ipir").ToList();

                var addedIds = productActivityApps.ToList().Select(s => s.AddedByUserId).ToList();
                addedIds.Add(userId);
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", addedIds.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.UserID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.DepartmentName,
                    s.PlantID,
                    s.DepartmentID
                }).ToList();
                var loginUser = employee.FirstOrDefault(w => w.UserID == userId)?.DepartmentID;
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();

                productActivityApps.ForEach(s =>
                {
                    var checkSameDept = false;

                    if (s.AddedByUserId == userId)
                    {
                        checkSameDept = true;
                    }
                    else
                    {
                        var employeeDep = employee.FirstOrDefault(w => w.UserID == s.AddedByUserId)?.DepartmentID;
                        if (loginUser != null && employeeDep != null && loginUser == employeeDep)
                        {
                            checkSameDept = true;
                        }
                    }
                    if (checkSameDept == true)
                    {
                        List<long> responsibilityUsers = new List<long>();
                        if (s.ProductActivityCaseId > 0)
                        {
                            var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                            var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                            var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                            var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                            var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                            var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                            var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                            if (appUserss != null && appUserss.Count > 0)
                            {
                                var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                                if (usersIdsBy != null && usersIdsBy.Count > 0)
                                {
                                    responsibilityUsers.AddRange(usersIdsBy);
                                }
                            }
                        }
                        ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                        productActivityApp.Type = "Ipir";
                        productActivityApp.ResponsibilityUsers = responsibilityUsers;
                        productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                        productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Ipir" && w.IpirReportId == s.IpirReportId).Count();
                        productActivityApp.ProductionActivityAppLineId = s.IpirReportId;
                        productActivityApp.Comment = s.Description;
                        productActivityApp.LineComment = s.Description;
                        productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                        productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessId;
                        productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                        productActivityApp.ManufacturingProcessChild = s.ManufacturingProcess?.Value;
                        productActivityApp.CompanyId = s.CompanyId;
                        productActivityApp.CompanyName = s.Company?.PlantCode;
                        productActivityApp.ProdOrderNo = s.ProdOrder;
                        productActivityApp.StatusCodeID = s.StatusCodeId;
                        productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                        productActivityApp.ModifiedDate = s.ModifiedDate;
                        productActivityApp.SessionId = s.SessionId;
                        productActivityApp.LineSessionId = s.SessionId;
                        productActivityApp.AddedByUserID = s.AddedByUserId;
                        productActivityApp.AddedDate = s.AddedDate;
                        productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                        productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                        productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessId;
                        productActivityApp.TopicId = s.TopicId;
                        productActivityApp.LocationId = s.LocationId;
                        productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                        if (documents != null && s.SessionId != null)
                        {
                            var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                            if (counts != null)
                            {
                                productActivityApp.DocumentId = counts.DocumentId;
                                productActivityApp.DocumentID = counts.DocumentId;
                                productActivityApp.DocumentParentId = counts.DocumentParentId;
                                productActivityApp.FileName = counts.FileName;
                                productActivityApp.FilePath = counts.FilePath;
                                productActivityApp.ContentType = counts.ContentType;
                                productActivityApp.IsLocked = counts.IsLocked;
                                productActivityApp.LockedByUserId = counts.LockedByUserId;
                                productActivityApp.ModifiedDate = counts.UploadDate;
                                productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                                productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                                productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                                productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                                long? roleId = 0;
                                roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == userId)?.RoleId;
                                if (roleId != null)
                                {
                                    var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                    if (documentPermission != null)
                                    {
                                        productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                        productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                        productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                        productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                        productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                        productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                        productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                        productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                        productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                        productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                        productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                        productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                        productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                        productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                        productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                        productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                        productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                        productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                        productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                        productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                        productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                        productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                    }
                                    else
                                    {
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                        productActivityApp.DocumentPermissionData.IsRead = false;
                                        productActivityApp.DocumentPermissionData.IsDelete = false;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                    }
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = true;
                                    productActivityApp.DocumentPermissionData.IsDelete = true;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                                }
                            }
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                });
            }
            return productActivityAppModels;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetIpirReport")]
        public List<IpirReportModel> Get()
        {
            List<IpirReportModel> ipirReportModels = new List<IpirReportModel>();

            var ipirReport = _context.IpirReport.Include(s => s.AddedByUser).Include(a => a.Company).Include(s => s.ModifiedByUser).Include(a => a.Ipirissue).Include(s => s.ManufacturingProcess).OrderByDescending(o => o.IpirReportId).AsNoTracking().ToList();

            if (ipirReport != null && ipirReport.Count > 0)
            {
                ipirReport.ForEach(s =>
                {
                    IpirReportModel ipirReportModel = new IpirReportModel
                    {
                        IpirReportId = s.IpirReportId,
                        Description = s.Description,
                        IssueTitle = s.IssueTitle,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ManufacturingProcess = s.ManufacturingProcess?.Value,
                        ProdOrder = s.ProdOrder,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        SessionId = s.SessionId,
                        Location = s.Location,
                        CompanyId = s.CompanyId,
                        ProdOrderDescription = s.ProdOrderDescription,
                        ProdOrderData = s.ProdOrderData,
                        CompanyName = s.Company?.PlantCode,
                        IsOthersOptions = s.IsOthersOptions,
                        Ipirissue = s.Ipirissue?.Value,
                        IpirissueDescription = s.IpirissueDescription,
                        IpirissueId = s.IpirissueId,
                        LocationId = s.LocationId,
                        ProductActivityCaseId=s.ProductActivityCaseId,
                    };
                    ipirReportModels.Add(ipirReportModel);
                });
            }
            return ipirReportModels;
        }
        [HttpPost]
        [Route("InsertIpirReport")]
        public IpirReportModel InsertIpirReport(IpirReportModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var ipirReport = new IpirReport
            {
                Description = value.Description,
                IssueTitle = value.IssueTitle,
                ManufacturingProcessId = value.ManufacturingProcessId,
                ProdOrder = value.ProdOrder,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.SessionId,
                Location = value.Location,
                CompanyId = value.CompanyId,
                ProdOrderDescription = value.ProdOrderDescription,
                ProdOrderData = value.ProdOrderData,
                Subject = value.Subject,
                IsOthersOptions = value.IsOthersOptions,
                IpirissueId = value.IpirissueId,
                IpirissueDescription = value.IpirissueDescription,
                LocationId = value.LocationId,
                ProductActivityCaseId=value.ProductActivityCaseId,
            };
            _context.IpirReport.Add(ipirReport);
            _context.SaveChanges();
            value.IpirReportId = ipirReport.IpirReportId;
            value.SessionId = value.SessionId;
            GenerateIpriNo(value);
            return value;
        }
        private void GenerateIpriNo(IpirReportModel value)
        {
            var ipirReport = _context.IpirReport.SingleOrDefault(p => p.IpirReportId == value.IpirReportId);
            if (ipirReport != null && ipirReport.IpirReportNo == null)
            {
                var companyId = _context.Plant.FirstOrDefault(f => f.PlantId == value.CompanyId);
                if (companyId != null)
                {
                    ipirReport.IpirReportNo = companyId.PlantCode + "/" + DateTime.Now.Year + "/" + value.IpirReportId;
                    _context.SaveChanges();
                }
            }
        }
        [HttpPost]
        [Route("GetIpirReportLocationPlant")]
        public ICTMasterModel GetIpirReportLocationPlant(SearchModel name)
        {
            ICTMasterModel iCTMasterModel = new ICTMasterModel();
            if (!string.IsNullOrEmpty(name.SearchString))
            {
                List<long?> parentIctids = new List<long?>() { 231, 3511, 195 };
                var companyId = _context.Ictmaster.FirstOrDefault(f => f.Name.ToLower() == name.SearchString.ToLower() && f.MasterType == 572);
                if (companyId != null)
                {
                    iCTMasterModel.ICTMasterID = companyId.IctmasterId;
                    iCTMasterModel.CompanyID = companyId.CompanyId;
                    iCTMasterModel.Name = companyId.Name;
                    iCTMasterModel.LocationDescription = companyId.Name + "||" + companyId.Description;
                    iCTMasterModel.Description = companyId.Description;
                }
            }
            return iCTMasterModel;
        }
        [HttpPut]
        [Route("UpdateIpirReport")]
        public IpirReportModel UpdateIpirReport(IpirReportModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var ipirReport = _context.IpirReport.SingleOrDefault(p => p.IpirReportId == value.IpirReportId);
            ipirReport.ProdOrder = value.ProdOrder;
            ipirReport.Description = value.Description;
            ipirReport.IssueTitle = value.IssueTitle;
            ipirReport.ManufacturingProcessId = value.ManufacturingProcessId;
            ipirReport.Location = value.Location;
            ipirReport.StatusCodeId = value.StatusCodeID;
            ipirReport.SessionId = value.SessionId;
            ipirReport.ModifiedByUserId = value.ModifiedByUserID;
            ipirReport.ModifiedDate = DateTime.Now;
            ipirReport.IsOthersOptions = value.IsOthersOptions;
            ipirReport.IpirissueId = value.IpirissueId;
            ipirReport.IpirissueDescription = value.IpirissueDescription;
            ipirReport.LocationId = value.LocationId;
            ipirReport.ProductActivityCaseId = value.ProductActivityCaseId;
            if (value.CompanyId == null)
            {
                ipirReport.CompanyId = value.CompanyId;
            }
            ipirReport.ProdOrderDescription = value.ProdOrderDescription;
            ipirReport.ProdOrderData = value.ProdOrderData;
            ipirReport.Subject = value.Subject;
            _context.SaveChanges();
            GenerateIpriNo(value);
            return value;
        }
        [HttpPut]
        [Route("UpdateIpirReportClose")]
        public IpirReportModel UpdateIpirReportClose(IpirReportModel value)
        {
            var ipirReport = _context.IpirReport.SingleOrDefault(p => p.IpirReportId == value.IpirReportId);
            ipirReport.StatusCodeId = 162;
            ipirReport.IpirissueDescription = value.IpirissueDescription;
            ipirReport.IpirissueId = value.IpirissueId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateIpirReportTopic")]
        public IpirReportModel UpdateIpirReportTopic(IpirReportModel value)
        {
            var ipirReport = _context.IpirReport.SingleOrDefault(p => p.IpirReportId == value.IpirReportId);
            ipirReport.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {
            var SessionId = new Guid(files["sessionId"].ToString());
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
                var filePath = files["filePath"].ToString();
                if (filePath == "filePath")
                {
                    UploadFileAsByPath(files, SessionId);
                }
                else
                {
                    var userId = new long?();
                    var user = files["userID"].ToString();
                    if (user != "" && user != "undefined" && user != null)
                    {
                        userId = Convert.ToInt32(files["userID"].ToString());
                    }

                    var videoFiles = files["isVideoFile"].ToString();

                    var videoFile = false;
                    if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                    {
                        videoFile = Convert.ToBoolean(videoFiles);
                    }
                    files.Files.ToList().ForEach(f =>
                    {
                        var file = f;
                        var ext = "";

                        if (videoFile == true)
                        {
                            ext = f.FileName.Split(".").Last(); 
                            var fileName1 = SessionId + "." + ext;
                            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                            using (var targetStream = System.IO.File.Create(serverPath))
                            {
                                file.CopyTo(targetStream);
                                targetStream.Flush();
                            }
                            var documents = new Documents
                            {
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FileData = null,
                                FileSize = file.Length,
                                UploadDate = DateTime.Now,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                IsVideoFile = true,
                                IsLatest = true,
                            };
                            _context.Documents.Add(documents);
                        }
                        else
                        {
                            var fs = file.OpenReadStream();
                            var br = new BinaryReader(fs);
                            Byte[] document = br.ReadBytes((Int32)fs.Length);
                            var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        var documents = new Documents
                            {
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FileData = compressedData,
                                FileSize = fs.Length,
                                UploadDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                IsLatest = true,
                            };
                            _context.Documents.Add(documents);
                        }

                    });
                    _context.SaveChanges();
                //}
            }
            return Content(SessionId.ToString());

        }
        private void UploadFileAsByPath(IFormCollection files, Guid SessionId)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + newFile;
                var filePath = getNextFileName(serverPath);
                newFile = filePath.Replace(serverPaths + @"\", "");
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = newFile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = true,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                }
                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
            });
            _context.SaveChanges();
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }
            return fileName;
        }
        [HttpDelete]
        [Route("DeleteIpirReport")]
        public ActionResult<string> DeleteIpirReport(int id)
        {
            try
            {
                var ipirReport = _context.IpirReport.SingleOrDefault(p => p.IpirReportId == id);
                if (ipirReport != null)
                {
                    _context.IpirReport.Remove(ipirReport);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("IpirReport Cannot be delete! Reference to others");
            }
        }
        #endregion
        #region IpirAssignment
        [HttpGet]
        [Route("GetIpirReportAssignment")]
        public List<IpirReportAssignmentModel> GetIpirReportAssignment(long? id)
        {
            List<IpirReportAssignmentModel> ipirReportModels = new List<IpirReportAssignmentModel>();

            var ipirReport = _context.IpirReportAssignment.Include(s => s.AddedByUser).Include(a => a.IntendAction).Include(s => s.ModifiedByUser).Include(s => s.StatusCode).Include(a => a.IpirReportAssignmentUser).OrderByDescending(o => o.IpirReportAssignmentId).Where(w => w.IpirReportId == id).AsNoTracking().ToList();
            var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName, s.SessionId }).ToList();
            var userGroup = _context.UserGroup.Select(s => new { s.UserGroupId, s.Name }).ToList();
            if (ipirReport != null && ipirReport.Count > 0)
            {
                ipirReport.ForEach(s =>
                {
                    IpirReportAssignmentModel ipirReportModel = new IpirReportAssignmentModel();
                    ipirReportModel.IpirReportAssignmentId = s.IpirReportAssignmentId;
                    ipirReportModel.IpirReportId = s.IpirReportId;
                    ipirReportModel.Description = s.Description;
                    ipirReportModel.Urgent = s.Urgent;
                    ipirReportModel.IsUrgentFlag = s.Urgent == true ? "Yes" : "No";
                    ipirReportModel.IntendActionId = s.IntendActionId;
                    ipirReportModel.IntendAction = s.IntendAction?.CodeValue;
                    ipirReportModel.ModifiedByUserID = s.AddedByUserId;
                    ipirReportModel.AddedByUserID = s.ModifiedByUserId;
                    ipirReportModel.StatusCodeID = s.StatusCodeId;
                    ipirReportModel.StatusCode = s.StatusCode?.CodeValue;
                    ipirReportModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    ipirReportModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    ipirReportModel.AddedDate = s.AddedDate;
                    ipirReportModel.ModifiedDate = s.ModifiedDate;
                    ipirReportModel.TopicId = s.TopicId;
                    ipirReportModel.SessionId = s.SessionId;
                    ipirReportModel.OthersDescription = s.OthersDescription;
                    ipirReportModel.UserGroupIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "UserGroup" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserGroupId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcuserGroupIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "CCUserGroup" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserGroupId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcuserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "CCUser" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).ToList() : new List<long?>();
                    ipirReportModel.UserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "User" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).ToList() : new List<long?>();
                    ipirReportModel.UserType = "User";
                    ipirReportModel.CcuserType = "CCUser";
                    var userlists = new List<string> { "User", "UserGroup" };
                    var ccuserlists = new List<string> { "CCUser", "CCUserGroup" };
                    var userIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => userlists.Contains(w.Type) && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.ToUser = string.Join(",", appUser.Where(w => userIDs.Contains(w.UserId)).Select(s => s.UserName).ToList());
                    var ccuserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => ccuserlists.Contains(w.Type) && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcUser = string.Join(",", appUser.Where(w => ccuserIDs.Contains(w.UserId)).Select(s => s.UserName).ToList());
                    ipirReportModel.CcUserSessions = string.Join(",", appUser.Where(w => ccuserIDs.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList());
                    ipirReportModel.ToUserSessions = string.Join(",", appUser.Where(w => userIDs.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList());
                    var userSess = new List<long?>();
                    userSess.AddRange(ccuserIDs); userSess.AddRange(userIDs);
                    ipirReportModel.SessionsItems = string.Join(",", appUser.Where(w => userSess.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList());
                    if (ipirReportModel.UserIDs != null && ipirReportModel.UserIDs.Count > 0)
                    {
                        ipirReportModel.UserType = "User";
                    }
                    if (ipirReportModel.CcuserIDs != null && ipirReportModel.CcuserIDs.Count > 0)
                    {
                        ipirReportModel.CcuserType = "CCUser";
                    }
                    if (ipirReportModel.UserGroupIDs != null && ipirReportModel.UserGroupIDs.Count > 0)
                    {
                        ipirReportModel.UserType = "UserGroup";

                    }
                    if (ipirReportModel.CcuserGroupIDs != null && ipirReportModel.CcuserGroupIDs.Count > 0)
                    {
                        ipirReportModel.CcuserType = "CCUserGroup";
                    }
                    ipirReportModels.Add(ipirReportModel);
                });
            }
            return ipirReportModels;
        }
        [HttpGet]
        [Route("GetIpirReportAssignmentTopicList")]
        public List<IpirReportAssignmentModel> GetIpirReportAssignmentTopicList(long? id)
        {
            List<IpirReportAssignmentModel> ipirReportModels = new List<IpirReportAssignmentModel>();

            var ipirReport = _context.IpirReportAssignment.Include(s => s.AddedByUser).Include(a => a.IntendAction).Include(s => s.ModifiedByUser).Include(s => s.StatusCode).Include(a => a.IpirReportAssignmentUser).OrderByDescending(o => o.IpirReportAssignmentId).Where(w => w.IpirReportAssignmentId == id).AsNoTracking().ToList();
            var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName, s.SessionId }).ToList();
            var userGroup = _context.UserGroup.Select(s => new { s.UserGroupId, s.Name }).ToList();
            if (ipirReport != null && ipirReport.Count > 0)
            {
                ipirReport.ForEach(s =>
                {
                    IpirReportAssignmentModel ipirReportModel = new IpirReportAssignmentModel();
                    ipirReportModel.IpirReportAssignmentId = s.IpirReportAssignmentId;
                    ipirReportModel.IpirReportId = s.IpirReportId;
                    ipirReportModel.Description = s.Description;
                    ipirReportModel.Urgent = s.Urgent;
                    ipirReportModel.IsUrgentFlag = s.Urgent == true ? "Yes" : "No";
                    ipirReportModel.IntendActionId = s.IntendActionId;
                    ipirReportModel.IntendAction = s.IntendAction?.CodeValue;
                    ipirReportModel.ModifiedByUserID = s.AddedByUserId;
                    ipirReportModel.AddedByUserID = s.ModifiedByUserId;
                    ipirReportModel.StatusCodeID = s.StatusCodeId;
                    ipirReportModel.StatusCode = s.StatusCode?.CodeValue;
                    ipirReportModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    ipirReportModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    ipirReportModel.AddedDate = s.AddedDate;
                    ipirReportModel.ModifiedDate = s.ModifiedDate;
                    ipirReportModel.SessionId = s.SessionId;
                    ipirReportModel.OthersDescription = s.OthersDescription;
                    ipirReportModel.UserGroupIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "UserGroup" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserGroupId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcuserGroupIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "CCUserGroup" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserGroupId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcuserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "CCUser" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).ToList() : new List<long?>();
                    ipirReportModel.UserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => w.Type == "User" && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).ToList() : new List<long?>();
                    ipirReportModel.UserType = "User";
                    ipirReportModel.CcuserType = "CCUser";
                    var userlists = new List<string> { "User", "UserGroup" };
                    var ccuserlists = new List<string> { "CCUser", "CCUserGroup" };
                    var userIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => userlists.Contains(w.Type) && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.ToUser = string.Join(",", appUser.Where(w => userIDs.Contains(w.UserId)).Select(s => s.UserName).ToList());
                    var ccuserIDs = s.IpirReportAssignmentUser != null ? s.IpirReportAssignmentUser.Where(w => ccuserlists.Contains(w.Type) && w.IpirReportAssignmentId == s.IpirReportAssignmentId).Select(s => s.UserId).Distinct().ToList() : new List<long?>();
                    ipirReportModel.CcUser = string.Join(",", appUser.Where(w => ccuserIDs.Contains(w.UserId)).Select(s => s.UserName).ToList());
                    ipirReportModel.CcUserSessions = string.Join(",", appUser.Where(w => ccuserIDs.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList());
                    ipirReportModel.ToUserSessions = string.Join(",", appUser.Where(w => userIDs.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList());
                    if (ipirReportModel.UserIDs != null && ipirReportModel.UserIDs.Count > 0)
                    {
                        ipirReportModel.UserType = "User";
                    }
                    if (ipirReportModel.CcuserIDs != null && ipirReportModel.CcuserIDs.Count > 0)
                    {
                        ipirReportModel.CcuserType = "CCUser";
                    }
                    if (ipirReportModel.UserGroupIDs != null && ipirReportModel.UserGroupIDs.Count > 0)
                    {
                        ipirReportModel.UserType = "UserGroup";

                    }
                    if (ipirReportModel.CcuserGroupIDs != null && ipirReportModel.CcuserGroupIDs.Count > 0)
                    {
                        ipirReportModel.CcuserType = "CCUserGroup";
                    }
                    ipirReportModels.Add(ipirReportModel);
                });
            }
            return ipirReportModels;
        }
        [HttpPost]
        [Route("InsertIpirAssignment")]
        public IpirReportAssignmentModel InsertIpirAssignment(IpirReportAssignmentModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var ipirReport = new IpirReportAssignment
            {
                IpirReportId = value.IpirReportId,
                Description = value.Description,
                Urgent = value.IsUrgentFlag == "Yes" ? true : false,
                IntendActionId = value.IntendActionId,
                OthersDescription = value.OthersDescription,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.SessionId,
            };
            _context.IpirReportAssignment.Add(ipirReport);
            _context.SaveChanges();
            value.IpirReportAssignmentId = ipirReport.IpirReportAssignmentId;
            value.SessionId = value.SessionId;
            if (value.UserType == "User")
            {
                if (value.UserIDs != null && value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
            }
            if (value.UserType == "UserGroup" && value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                var userGroupUserId = _context.UserGroupUser.Where(w => value.UserGroupIDs.Contains(w.UserGroupId) && w.UserId != null).ToList();
                if (userGroupUserId != null && userGroupUserId.Count > 0)
                {
                    userGroupUserId.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a.UserId,
                            UserGroupId = a.UserGroupId,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
            }
            if (value.CcuserType == "CCUser")
            {
                if (value.CcuserIDs != null && value.CcuserIDs.Count > 0)
                {
                    value.CcuserIDs.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.CcuserType,
                            UserId = a,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);

                    });
                }
            }
            if (value.CcuserType == "CCUserGroup" && value.CcuserGroupIDs != null && value.CcuserGroupIDs.Count > 0)
            {
                var userGroupUserId = _context.UserGroupUser.Where(w => value.CcuserGroupIDs.Contains(w.UserGroupId) && w.UserId != null).ToList();
                if (userGroupUserId != null && userGroupUserId.Count > 0)
                {
                    userGroupUserId.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.CcuserType,
                            UserId = a.UserId,
                            UserGroupId = a.UserGroupId,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
            }
            _context.SaveChanges();
            /*NotifyDocumentController notifyDocumentController = new NotifyDocumentController(_context, _mapper, _mailService, _hostingEnvironment);
            var ipirAssignment = _context.IpirReportAssignment.Include(a => a.IpirReport).FirstOrDefault(f => f.IpirReportAssignmentId == value.IpirReportAssignmentId)?.SessionId;

            var documentId = _context.Documents.FirstOrDefault(f => f.SessionId == ipirAssignment && f.IsLatest == true)?.DocumentId;

            var notifyDocument = new NotifyDocumentModel
            {
                DocumentId = documentId,
                UserId = value.AddedByUserID,
                AddedByUserID = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeID = 1,
                IsSenderClose = false,
                NotifyType = "IpirReportAssignment",
                IpirReportAssignmentId = value.IpirReportAssignmentId,
                UserGroupIDs = value.UserGroupIDs,
                UserIDs = value.UserIDs,
                CcuserGroupIDs = value.CcuserGroupIDs,
                CcuserIDs = value.CcuserIDs,
            };
            notifyDocumentController.Post(notifyDocument);*/
            return value;
        }
        [HttpPut]
        [Route("UpdateIpirAssignmentTopic")]
        public IpirReportAssignmentModel UpdateIpirAssignmentTopic(IpirReportAssignmentModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var ipirReport = _context.IpirReportAssignment.SingleOrDefault(p => p.IpirReportAssignmentId == value.IpirReportAssignmentId);
            ipirReport.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateIpirAssignment")]
        public IpirReportAssignmentModel UpdateIpirAssignment(IpirReportAssignmentModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var ipirReport = _context.IpirReportAssignment.SingleOrDefault(p => p.IpirReportAssignmentId == value.IpirReportAssignmentId);

            ipirReport.IpirReportId = value.IpirReportId;
            ipirReport.Description = value.Description;
            ipirReport.Urgent = value.IsUrgentFlag == "Yes" ? true : false;
            ipirReport.IntendActionId = value.IntendActionId;
            ipirReport.OthersDescription = value.OthersDescription;
            ipirReport.StatusCodeId = value.StatusCodeID.Value;
            ipirReport.ModifiedByUserId = value.AddedByUserID;
            ipirReport.ModifiedDate = DateTime.Now;
            ipirReport.SessionId = value.SessionId;
            _context.SaveChanges();
            value.IpirReportAssignmentId = ipirReport.IpirReportAssignmentId;
            value.SessionId = value.SessionId;
            var ipirReportAssignmentUserRemove = _context.IpirReportAssignmentUser.Where(w => w.IpirReportAssignmentId == value.IpirReportAssignmentId).ToList();
            if (ipirReportAssignmentUserRemove != null && ipirReportAssignmentUserRemove.Count > 0)
            {
                _context.IpirReportAssignmentUser.RemoveRange(ipirReportAssignmentUserRemove);
                _context.SaveChanges();
            }
            if (value.UserType == "User")
            {
                if (value.UserIDs != null && value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
                _context.SaveChanges();
            }
            if (value.UserType == "UserGroup" && value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                var userGroupUserId = _context.UserGroupUser.Where(w => value.UserGroupIDs.Contains(w.UserGroupId) && w.UserId != null).ToList();
                if (userGroupUserId != null && userGroupUserId.Count > 0)
                {
                    userGroupUserId.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a.UserId,
                            UserGroupId = a.UserGroupId,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
                _context.SaveChanges();
            }
            if (value.CcuserType == "CCUser")
            {
                if (value.CcuserIDs != null && value.CcuserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                        _context.SaveChanges();
                    });
                }
            }
            if (value.CcuserType == "CCUserGroup" && value.CcuserGroupIDs != null && value.CcuserGroupIDs.Count > 0)
            {
                var userGroupUserId = _context.UserGroupUser.Where(w => value.CcuserGroupIDs.Contains(w.UserGroupId) && w.UserId != null).ToList();
                if (userGroupUserId != null && userGroupUserId.Count > 0)
                {
                    userGroupUserId.ForEach(a =>
                    {
                        var users = new IpirReportAssignmentUser
                        {
                            Type = value.UserType,
                            UserId = a.UserId,
                            UserGroupId = a.UserId,
                            IpirReportAssignmentId = value.IpirReportAssignmentId,
                        };
                        _context.IpirReportAssignmentUser.Add(users);
                    });
                }
                _context.SaveChanges();
            }
            return value;
        }

        [HttpDelete]
        [Route("DeleteIpirReportAssignment")]
        public ActionResult<string> DeleteIpirReportAssignment(int id)
        {
            try
            {
                var ipirReport = _context.IpirReportAssignment.SingleOrDefault(p => p.IpirReportAssignmentId == id);
                if (ipirReport != null)
                {
                    var ipirReportAssignmentUserRemove = _context.IpirReportAssignmentUser.Where(w => w.IpirReportAssignmentId == ipirReport.IpirReportAssignmentId).ToList();
                    if (ipirReportAssignmentUserRemove != null && ipirReportAssignmentUserRemove.Count > 0)
                    {
                        _context.IpirReportAssignmentUser.RemoveRange(ipirReportAssignmentUserRemove);
                        _context.SaveChanges();
                    }
                    _context.IpirReportAssignment.Remove(ipirReport);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("IpirReport Cannot be delete! Reference to others");
            }
        }
        #endregion
    }
}
