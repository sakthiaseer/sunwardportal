﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DisposalItemController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DisposalItemController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDisposalItems")]
        public async Task<List<DisposalItemModel>> Get()
        {
            var disposalItems = await _context.DisposalItem
                .Include(s => s.InventoryType)
                .Include(s => s.Item)
                .Include(s => s.ModifiedByUser)
                .Include(s => s.AddedByUser)
                .Include(s => s.StatusCode).AsNoTracking().ToListAsync();
            var disposalItemModel = disposalItems.Select(s => new DisposalItemModel
            {
                DisposalItemId = s.DisposalItemId,
                InventoryTypeId = s.InventoryTypeId,
                InventoryType = s.InventoryType?.Name,
                ItemId = s.ItemId,
                ItemNo = s.Item?.No,
                BatchNo = s.BatchNo,
                DisposalQty = s.DisposalQty,
                Remarks = s.Remarks,
                ModifiedByUserID = s.AddedByUserId,
                AddedByUserID = s.ModifiedByUserId,
                StatusCodeID = s.StatusCodeId,
                AddedByUser = s.AddedByUser?.UserName,
                ModifiedByUser = s.ModifiedByUser?.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                StatusCode = s.StatusCode?.CodeValue,


            }).OrderByDescending(o => o.DisposalItemId).ToList();
            return disposalItemModel;
        }


        [HttpPost()]
        [Route("GetDisposalItemData")]
        public ActionResult<DisposalItemModel> GetData(SearchModel searchModel)
        {
            var disposalItem = new DisposalItem();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).FirstOrDefault();
                        break;
                    case "Last":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).LastOrDefault();
                        break;
                    case "Next":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).LastOrDefault();
                        break;
                    case "Previous":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).FirstOrDefault();
                        break;
                    case "Last":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).LastOrDefault();
                        break;
                    case "Next":
                        disposalItem = _context.DisposalItem.OrderBy(o => o.DisposalItemId).FirstOrDefault(s => s.DisposalItemId > searchModel.Id);
                        break;
                    case "Previous":
                        disposalItem = _context.DisposalItem.OrderByDescending(o => o.DisposalItemId).FirstOrDefault(s => s.DisposalItemId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DisposalItemModel>(disposalItem);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDisposalItem")]
        public DisposalItemModel Post(DisposalItemModel value)
        {
           
          
            var disposalItem = new DisposalItem
            {
                InventoryTypeId = value.InventoryTypeId,
                BatchNo = value.BatchNo,
                DisposalQty = value.DisposalQty,
                ItemId = value.ItemId,
                Remarks = value.Remarks,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.DisposalItem.Add(disposalItem);
            if (value.ItemId > 0)
            {
                var itemStockInfo = _context.ItemStockInfo.FirstOrDefault(s => s.ItemId == value.ItemId);
                if (itemStockInfo != null)
                {
                    itemStockInfo.QuantityOnHand -= value.DisposalQty;
                }
            }
            if (value.ItemId !=null && value.BatchNo != null)
            {
                var itemBatchInfo = _context.ItemBatchInfo.FirstOrDefault(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo);
                if (itemBatchInfo != null)
                {
                    itemBatchInfo.QuantityOnHand -= value.DisposalQty;
                }
            }
           
            _context.SaveChanges();
            value.DisposalItemId = disposalItem.DisposalItemId;
            if (value.ItemId > 0)
            {
                value.ItemNo = _context.Navitems.Where(a => a.ItemId == value.ItemId)?.FirstOrDefault()?.No;
            }
            if (value.InventoryTypeId > 0)
            {
                value.InventoryType = _context.InventoryType.Where(d => d.InventoryTypeId == value.InventoryTypeId)?.FirstOrDefault()?.Name;
            }

            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteDisposalItem")]
        public void Delete(int id)
        {
            var DisposalItem = _context.DisposalItem.SingleOrDefault(p => p.DisposalItemId == id);
            if (DisposalItem != null)
            {
                _context.DisposalItem.Remove(DisposalItem);
                _context.SaveChanges();
            }
        }
    }
}
