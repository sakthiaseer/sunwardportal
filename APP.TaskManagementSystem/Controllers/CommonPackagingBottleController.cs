﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingBottleController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingBottleController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingBottle")]
        public List<CommonPackagingBottleModel> Get()
        {
            var commonPackagingBottle = _context.CommonPackagingBottle
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingBottleModel> commonPackagingBottleModel = new List<CommonPackagingBottleModel>();
            if (commonPackagingBottle.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottle.Where(w => w.PackSizeunitsId != null).Select(a => a.PackSizeunitsId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottle.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottle.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottle.Where(w => w.ShapeId != null).Select(a => a.ShapeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottle.Where(w => w.TypeId != null).Select(a => a.TypeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottle.ForEach(s =>
                {
                    CommonPackagingBottleModel commonPackagingBottleModels = new CommonPackagingBottleModel
                    {
                        BottleSpecificationId = s.BottleSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        Volume = s.Volume,
                        Code = s.Code,
                        ColorId = s.ColorId,
                        ShapeId = s.ShapeId,
                        TypeId = s.TypeId,
                        PackSizeunitsId = s.PackSizeunitsId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleSpecificationId == s.BottleSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackSizeunitsName = s.PackSizeunitsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackSizeunitsId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        ColorName = s.ColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ColorId).Select(m => m.Value).FirstOrDefault() : "",
                        ShapeName = s.ShapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ShapeId).Select(m => m.Value).FirstOrDefault() : "",
                        TypeName = s.TypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingBottleModel.Add(commonPackagingBottleModels);
                });
            }
            return commonPackagingBottleModel.OrderByDescending(a => a.BottleSpecificationId).ToList();
        }

        [HttpPost]
        [Route("GetCommonPackagingBottleByRefNo")]
        public List<CommonPackagingBottleModel> GetCommonPackagingBottleByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingBottle = _context.CommonPackagingBottle
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).Where(w => w.BottleSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingBottle = commonPackagingBottle.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingBottle = commonPackagingBottle.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingBottles = commonPackagingBottle.AsNoTracking().ToList();
            List<CommonPackagingBottleModel> commonPackagingBottleModel = new List<CommonPackagingBottleModel>();
            if (commonPackagingBottles.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottles.Where(w => w.PackSizeunitsId != null).Select(a => a.PackSizeunitsId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottles.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottles.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottles.Where(w => w.ShapeId != null).Select(a => a.ShapeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottles.Where(w => w.TypeId != null).Select(a => a.TypeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottles.ForEach(s =>
                {
                    CommonPackagingBottleModel commonPackagingBottleModels = new CommonPackagingBottleModel
                    {
                        BottleSpecificationId = s.BottleSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        Volume = s.Volume,
                        Code = s.Code,
                        ColorId = s.ColorId,
                        ShapeId = s.ShapeId,
                        TypeId = s.TypeId,
                        PackSizeunitsId = s.PackSizeunitsId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleSpecificationId == s.BottleSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackSizeunitsName = s.PackSizeunitsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackSizeunitsId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        ColorName = s.ColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ColorId).Select(m => m.Value).FirstOrDefault() : "",
                        ShapeName = s.ShapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ShapeId).Select(m => m.Value).FirstOrDefault() : "",
                        TypeName = s.TypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingBottleModel.Add(commonPackagingBottleModels);
                });
            }
            return commonPackagingBottleModel.OrderByDescending(o => o.BottleSpecificationId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingBottleModel> GetData(SearchModel searchModel)
        {
            var commonPackagingBottle = new CommonPackagingBottle();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderBy(o => o.BottleSpecificationId).FirstOrDefault(s => s.BottleSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingBottle = _context.CommonPackagingBottle.OrderByDescending(o => o.BottleSpecificationId).FirstOrDefault(s => s.BottleSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingBottleModel>(commonPackagingBottle);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingBottle")]
        public CommonPackagingBottleModel Post(CommonPackagingBottleModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Code });
            var commonPackagingBottle = new CommonPackagingBottle
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                Volume = value.Volume,
                Code = value.Code,
                ColorId = value.ColorId,
                ShapeId = value.ShapeId,
                TypeId = value.TypeId,
                PackSizeunitsId = value.PackSizeunitsId,
                VersionControl = value.VersionControl,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingBottle.Add(commonPackagingBottle);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingBottle.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingBottle.ProfileLinkReferenceNo;
            value.BottleSpecificationId = commonPackagingBottle.BottleSpecificationId;
            value.PackagingItemName = UpdateCommonPackage(value);
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.BottleSpecificationId == value.BottleSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        BottleSpecificationId = value.BottleSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingBottleModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    var masterDetailList = _context.ApplicationMasterDetail.ToList();
                    var PackSizeunitsName = value.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackSizeunitsId).Select(m => m.Value).FirstOrDefault() : "";
                    var ColorName = value.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ColorId).Select(m => m.Value).FirstOrDefault() : "";
                    var ShapeName = value.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ShapeId).Select(m => m.Value).FirstOrDefault() : "";
                    var TypeName = value.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TypeId).Select(m => m.Value).FirstOrDefault() : "";
                    if (value.Volume != null && value.Volume > 0)
                    {
                        itemName = value.Volume + " ";
                    }
                    if (!string.IsNullOrEmpty(PackSizeunitsName))
                    {
                        itemName = itemName + PackSizeunitsName + " ";
                    }
                    if (!string.IsNullOrEmpty(value.Code))
                    {
                        itemName = itemName + " (" + value.Code + ") " + " ";
                    }
                    if (!string.IsNullOrEmpty(ColorName))
                    {
                        itemName = itemName + ColorName + " ";
                    }
                    if (!string.IsNullOrEmpty(ShapeName))
                    {
                        itemName = itemName + ShapeName + " ";
                    }
                    if (!string.IsNullOrEmpty(TypeName))
                    {
                        itemName = itemName + TypeName + " ";
                    }
                    itemName = itemName + "Bottle";
                    //itemName = value.Volume + " " + PackSizeunitsName + " (" + value.Code + ") " + ColorName + " " + ShapeName + " " + TypeName + " " + "Bottle";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingBottle")]
        public CommonPackagingBottleModel Put(CommonPackagingBottleModel value)
        {
            var commonPackagingBottle = _context.CommonPackagingBottle.SingleOrDefault(p => p.BottleSpecificationId == value.BottleSpecificationId);
            commonPackagingBottle.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingBottle.Set = value.Set;
            commonPackagingBottle.Volume = value.Volume;
            commonPackagingBottle.Code = value.Code;
            commonPackagingBottle.ColorId = value.ColorId;
            commonPackagingBottle.ShapeId = value.ShapeId;
            commonPackagingBottle.TypeId = value.TypeId;
            commonPackagingBottle.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingBottle.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingBottle.PackSizeunitsId = value.PackSizeunitsId;
            commonPackagingBottle.VersionControl = value.VersionControl;
            commonPackagingBottle.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingBottle.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingBottle.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.BottleSpecificationId == value.BottleSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.BottleSpecificationId == value.BottleSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        BottleSpecificationId = value.BottleSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingBottle")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingBottle = _context.CommonPackagingBottle.Where(p => p.BottleSpecificationId == id).FirstOrDefault();
                if (commonPackagingBottle != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.BottleSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingBottle.Remove(commonPackagingBottle);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}