﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using APP.EntityModel.Response;
using AutoMapper;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FoldersController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public FoldersController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFolders")]
        public List<FoldersModel> Get()
        {
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            var folders = _context.Folders.Include("AddedByUser").Include("DocumentFolder").Include("ModifiedByUser").Include("FolderType").Include("StatusCode").Include("ParentFolder").AsNoTracking().OrderByDescending(o => o.FolderId).ToList();
            if (folders != null && folders.Count > 0)
            {
                folders.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderId,
                        ParentFolderID = s.ParentFolderId,
                        ParentFolderName = s.ParentFolder != null ? s.ParentFolder.Name : string.Empty,
                        MainFolderID = s.MainFolderId,
                        FolderTypeID = s.FolderTypeId,
                        Id = s.FolderId,
                        //CodeID = s.StatusCode.CodeId,
                        //CodeType = s.StatusCode.CodeType,
                        FolderTypeName = s.FolderType != null ? s.FolderType.CodeValue : string.Empty,
                        Name = s.Name,
                        Description = s.Description,
                        IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Attachments = s.DocumentFolder != null ? s.DocumentFolder.Select(sa => sa.DocumentId).ToList() : new List<long?>(),
                        AttachmentIds = s.DocumentFolder != null ? s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList() : new List<long>(),

                    };
                    foldersModels.Add(foldersModel);
                });

            }

            return foldersModels;

        }
        [HttpGet]
        [Route("GetFoldersDropDown")]
        public List<FoldersModel> GetFoldersDropDown()
        {
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            var folders = _context.Folders.AsNoTracking().OrderByDescending(o => o.FolderId).ToList();
            if (folders != null && folders.Count > 0)
            {
                folders.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderId,
                        Name = s.Name,
                        Description = s.Description,
                    };
                    foldersModels.Add(foldersModel);
                });

            }

            return foldersModels;

        }
        [HttpGet]
        [Route("GetUserGroups")]
        public List<UserGroupModel> GetUserGroup()
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();
            var userGroup = _context.UserGroup.Include("AddedByUser").Include("ModifiedByUser").Include("UserGroupUser").AsNoTracking().OrderByDescending(o => o.UserGroupId).ToList();
            if (userGroup != null && userGroup.Count > 0)
            {
                userGroup.ForEach(s =>
                {
                    UserGroupModel userGroupModel = new UserGroupModel
                    {
                        UserGroupID = s.UserGroupId,
                        UserIDs = s.UserGroupUser != null ? s.UserGroupUser.Select(c => c.UserId).ToList() : new List<long?>(),
                        Name = s.Name,
                        Description = s.Description,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    userGroupModels.Add(userGroupModel);
                });
            }
            return userGroupModels;
        }
        [HttpGet]
        [Route("GetDocumentUserGroup")]
        public List<UserGroupModel> GetDocumentUserGroup()
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();
            var userGroup = _context.UserGroup.Include("AddedByUser").Include("ModifiedByUser").Include("UserGroupUser").AsNoTracking().OrderByDescending(o => o.UserGroupId).ToList();
            if (userGroup != null && userGroup.Count > 0)
            {
                userGroup.ForEach(s =>
                {
                    UserGroupModel userGroupModel = new UserGroupModel
                    {
                        UserGroupID = s.UserGroupId,
                        UserIDs = s.UserGroupUser != null ? s.UserGroupUser.Select(c => c.UserId).ToList() : new List<long?>(),
                        Name = s.Name,
                        Description = s.Description,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    userGroupModels.Add(userGroupModel);
                });
            }
            return userGroupModels;
        }

        private List<FoldersModel> GetAllPublicFolders(long id)
        {
            var folders = new List<FoldersModel>();
            List<DocumentPermission> documentpermission = null;
            List<DocumentUserRole> documentRoleList = null;
            List<UserGroupUser> userGroupList = null;
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            documentRoleList = _context.DocumentUserRole.AsNoTracking().ToList();
            userGroupList = _context.UserGroupUser.AsNoTracking().ToList();
            var filterFolders = _context.Folders.Include("AddedByUser").Include("DocumentFolder").Include("ModifiedByUser").Include("StatusCode").Where(t => t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().OrderByDescending(o => o.FolderId).ToList();
            if (filterFolders.Count > 0)
            {
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();
                if (filterFolders != null && filterFolders.Count > 0)
                {
                    filterFolders.ForEach(s =>
                    {
                        FoldersModel foldersModel = new FoldersModel
                        {
                            FolderID = s.FolderId,
                            MainFolderID = s.MainFolderId,
                            ParentFolderID = s.ParentFolderId,
                            FolderTypeID = s.FolderTypeId,
                            Name = s.Name,
                            Description = s.Description,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUserID = s.AddedByUserId,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                            StatusCodeID = s.StatusCodeId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            Attachments = s.DocumentFolder != null ? s.DocumentFolder.Select(sa => sa.DocumentId).ToList() : new List<long?>(),
                            AttachmentIds = s.DocumentFolder != null ? s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList() : new List<long>(),
                        };
                        folders.Add(foldersModel);
                    });
                }

                if (documentUserRole.Count > 0)
                {
                    List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                    folders.ForEach(folder =>
                    {
                        documentUserRole.ForEach(document =>
                        {
                            if (document.FolderID == folder.FolderID && folder.AddedByUserID != id)
                            {
                                if (document.UserID == id)
                                {
                                    var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                                    if (userpermission != null)
                                    {
                                        folder.DocumentRoleId = userpermission.DocumentRoleId;
                                        folder.UserId = document.UserID;
                                        folder.UserGroupId = document.UserGroupID;
                                        folder.IsDelete = userpermission.IsDelete;
                                        folder.IsRead = userpermission.IsRead;
                                        folder.IsEdit = userpermission.IsEdit;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                    else
                                    {
                                        folder.IsDelete = folder.IsDelete;
                                        folder.IsRead = folder.IsRead;
                                        folder.IsEdit = folder.IsEdit;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                }
                                else if (document.UserGroupID != null && document.UserGroupID > 0)
                                {
                                    var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                                    if (userGroupRole != null)
                                    {
                                        var userGroup = userGroupList.Where(u => u.UserId == id).LastOrDefault();
                                        if (userGroup != null)
                                        {
                                            if (document.UserGroupID == userGroup.UserGroupId)
                                            {
                                                var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                                if (userGrouppermission != null)
                                                {

                                                    folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                                                    folder.UserId = document.UserID;
                                                    folder.UserGroupId = document.UserGroupID;
                                                    folder.IsDelete = userGrouppermission.IsDelete;
                                                    folder.IsRead = userGrouppermission.IsRead;
                                                    folder.IsEdit = userGrouppermission.IsEdit;
                                                    permissionAppliedFolders.Add(folder);

                                                }
                                                else
                                                {
                                                    folder.IsDelete = false;
                                                    folder.IsRead = false;
                                                    folder.IsEdit = false;
                                                    permissionAppliedFolders.Add(folder);
                                                }


                                            }
                                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                            {
                                                folder.IsDelete = false;
                                                folder.IsRead = false;
                                                folder.IsEdit = false;
                                                permissionAppliedFolders.Add(folder);

                                            }
                                        }
                                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                        {

                                            folder.IsDelete = false;
                                            folder.IsRead = false;
                                            folder.IsEdit = false;
                                            permissionAppliedFolders.Add(folder);


                                        }
                                    }
                                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                    {
                                        folder.IsDelete = false;
                                        folder.IsRead = false;
                                        folder.IsEdit = false;
                                        permissionAppliedFolders.Add(folder);

                                    }
                                }
                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                {

                                    folder.IsDelete = false;
                                    folder.IsRead = false;
                                    folder.IsEdit = false;
                                    permissionAppliedFolders.Add(folder);

                                }

                            }
                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == id)
                            {

                                folder.IsDelete = true;
                                folder.IsRead = true;
                                folder.IsEdit = true;
                                permissionAppliedFolders.Add(folder);

                            }

                        });
                    });
                }
            }

            return folders;
        }

        [HttpGet]
        [Route("GetPublicFolders")]
        public List<FoldersModel> GetPublicFolders(int id)
        {
            var folders = GetAllPublicFolders(id);

            var lookup = folders.ToLookup(x => x.ParentFolderID);

            Func<long?, List<FoldersModel>> build = null;
            int index = 0;
            build = pid =>
                lookup[pid]
                    .Select(x => new FoldersModel
                    {
                        Id = ++index,
                        FolderID = x.FolderID,
                        MainFolderID = x.MainFolderID,
                        ParentFolderID = x.ParentFolderID,
                        FolderTypeID = x.FolderTypeID,
                        FolderTypeName = x.FolderTypeName,
                        Name = x.Name,
                        Description = x.Description,
                        ModifiedByUserID = x.ModifiedByUserID,
                        AddedByUserID = x.AddedByUserID,
                        AddedByUser = x.AddedByUser,

                        ModifiedByUser = x.ModifiedByUser,
                        StatusCodeID = x.StatusCodeID,
                        StatusCode = x.StatusCode,
                        AddedDate = x.AddedDate,
                        ModifiedDate = x.ModifiedDate,
                        Attachments = x.Attachments,
                        AttachmentIds = x.AttachmentIds,
                        DocumentRoleId = x.DocumentRoleId,
                        UserId = x.UserId,
                        UserGroupId = x.UserGroupId,
                        IsDelete = x.IsDelete,
                        IsRead = x.IsRead,
                        IsEdit = x.IsEdit,
                        Children = build(x.FolderID)
                    })
                    .ToList();

            List<FoldersModel> allPageComponents = new List<FoldersModel>();
            var nestedlist = build(null).ToList();
            List<FoldersModel> pathFoldersModels = new List<FoldersModel>();
            pathFoldersModels = ParentPath(folders);
            folders.ForEach(f =>
            {
                f.ParentFolderPathName = pathFoldersModels.FirstOrDefault(t => t.FolderID == f.FolderID)?.ParentFolderPathName;
                f.FolderPath = pathFoldersModels.FirstOrDefault(t => t.FolderID == f.FolderID)?.FolderPath;
            });
            return folders;
        }

        [HttpGet]
        [Route("GetFolderLists")]
        public List<FoldersModel> GetFolderLists(int? id, int userId)
        {
            var folders = GetAllPublicFolders(userId).ToList();
            List<FoldersModel> listFolders = new List<FoldersModel>();
            List<FoldersModel> folderLocation = new List<FoldersModel>();
            var emptyFolderIds = new List<long>();
            var selectfolder = folders.Where(t => t.ParentFolderID == id).ToList();
            foreach (var sel in selectfolder)
            {
                FoldersModel f = new FoldersModel();
                if (sel.ParentFolderID > 0)
                {
                    f.FolderPath = "";
                    f.FolderID = sel.FolderID;
                    f.Name = sel.Name;
                    f.ParentFolderID = sel.ParentFolderID;
                    folderLocation.Add(f);
                    GenerateAllFolder(folderLocation, f, folders, emptyFolderIds);



                }
                else if (sel.ParentFolderID == null)
                {
                    //f.FolderPath = fn.Name;
                    f.FolderPath = sel.Name;
                    f.FolderID = sel.FolderID;
                    f.Name = sel.Name;
                    f.ParentFolderID = sel.ParentFolderID;
                    folderLocation.Add(f);
                }
            };


            var folderItems = folderLocation.Select(s => new FoldersModel
            {
                FolderID = s.FolderID,
                ParentFolderID = s.ParentFolderID,
                MainFolderID = s.MainFolderID,
                Name = s.Name,

                Description = s.Description,
                StatusCodeID = s.StatusCodeID,


            }).Distinct().ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return folderItems;
            // listFolders= folders.Where(f=>f.ParentFolderID == id && f.IsRead == true && f.IsEdit == true).ToList();
            //GenerateLocation(folders)
            //  return listFolders;

        }
        [HttpPost]
        [Route("GetShowonlyOrPermissionFolders")]
        public List<FoldersModel> GetShowonlyOrPermissionFolders(FolderParam folderParam)
        {
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            foldersModels = GetAllPublicFolders(folderParam.UserId);

            if (folderParam.ShowOnly)
            {
                foldersModels = foldersModels.Where(f => f.IsRead.GetValueOrDefault(false)).ToList();
            }

            var lookup = foldersModels.ToLookup(x => x.ParentFolderID);
            Func<long?, List<FoldersModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new FoldersModel
                    {
                        FolderID = x.FolderID,
                        MainFolderID = x.MainFolderID,
                        ParentFolderID = x.ParentFolderID,
                        FolderTypeID = x.FolderTypeID,
                        FolderTypeName = x.FolderTypeName,
                        Name = x.Name,
                        Description = x.Description,
                        ModifiedByUserID = x.ModifiedByUserID,
                        AddedByUserID = x.AddedByUserID,
                        AddedByUser = x.AddedByUser,
                        ModifiedByUser = x.ModifiedByUser,
                        StatusCodeID = x.StatusCodeID,
                        StatusCode = x.StatusCode,
                        AddedDate = x.AddedDate,
                        ModifiedDate = x.ModifiedDate,
                        Attachments = x.Attachments,
                        AttachmentIds = x.AttachmentIds,
                        DocumentRoleId = x.DocumentRoleId,
                        UserId = x.UserId,
                        UserGroupId = x.UserGroupId,
                        IsDelete = x.IsDelete,
                        IsRead = x.IsRead,
                        IsEdit = x.IsEdit,
                        Children = build(x.FolderID)
                    })
                    .ToList();

            List<FoldersModel> allPageComponents = new List<FoldersModel>();
            var nestedlist = build(null).ToList();
            return nestedlist;
        }
        [HttpGet]
        [Route("GetDocumentLists")]

        public List<DocumentFolderModel> GetDocumentLists(int id)
        {
            var folders = GetAllPublicFolders(id).ToList();
            List<long> folderIDs = new List<long>();
            folderIDs = folders.Where(f => f.IsRead == true || f.IsEdit == true).Select(f => f.FolderID).ToList();
            List<DocumentFolderModel> documentFolderModels = new List<DocumentFolderModel>();
            var documentFolder = _context.DocumentFolder.Where(d => folderIDs.Contains(d.FolderId.Value) && d.IsLatest == true).OrderByDescending(df => df.DocumentFolderId).Distinct().ToList();
            if (documentFolder != null && documentFolder.Count > 0)
            {
                documentFolder.ForEach(s =>
                {
                    DocumentFolderModel documentFolderModel = new DocumentFolderModel
                    {
                        DocumentFolderID = s.DocumentFolderId,
                        FolderID = s.FolderId.Value,
                        FolderTitle = s.Folder != null ? s.Folder.Name : string.Empty,
                        FileName = s.Document != null ? s.Document.FileName : string.Empty,
                        DocumentID = s.DocumentId.Value,
                        AddedByUserID = s.Document != null ? s.Document.AddedByUserId : null,

                    };
                    documentFolderModels.Add(documentFolderModel);
                });

            }

            return documentFolderModels;
        }
        [HttpGet]
        [Route("GetInActivePublicFolders")]
        public List<FoldersModel> GetInActivePublicFolders(int id)
        {
            var folders = new List<FoldersModel>();
            List<DocumentPermission> documentpermission = null;
            List<DocumentUserRole> documentRoleList = null;
            List<UserGroupUser> userGroupList = null;
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            documentRoleList = _context.DocumentUserRole.AsNoTracking().ToList();
            userGroupList = _context.UserGroupUser.AsNoTracking().ToList();
            var filterFolders = _context.Folders.Include("AddedByUser").Include("DocumentFolder").Include("ModifiedByUser").Include("FolderType").Include("StatusCode").OrderByDescending(o => o.FolderId).Where(t => t.FolderTypeId == 524 && t.StatusCodeId == 2 && t.AddedByUserId == id && t.ParentFolderId == null).AsNoTracking().ToList();
            if (filterFolders.Count >= 0)
            {
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();
                if (filterFolders != null && filterFolders.Count > 0)
                {
                    filterFolders.ForEach(s =>
                    {
                        FoldersModel foldersModel = new FoldersModel
                        {
                            FolderID = s.FolderId,
                            MainFolderID = s.MainFolderId,
                            ParentFolderID = s.ParentFolderId,
                            FolderTypeID = s.FolderTypeId,
                            FolderTypeName = s.FolderType != null ? s.FolderType.CodeValue : "",
                            Name = s.Name,
                            Description = s.Description,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUserID = s.AddedByUserId,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                            StatusCodeID = s.StatusCodeId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            Attachments = s.DocumentFolder != null ? s.DocumentFolder.Select(sa => sa.DocumentId).ToList() : new List<long?>(),
                            AttachmentIds = s.DocumentFolder != null ? s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList() : new List<long>(),

                        };
                        folders.Add(foldersModel);
                    });
                }

                if (documentUserRole.Count >= 0)
                {
                    List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                    folders.ForEach(folder =>
                    {
                        documentUserRole.ForEach(document =>
                        {
                            if (document.FolderID == folder.FolderID && folder.AddedByUserID != id)
                            {
                                if (document.UserID == id)
                                {
                                    var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                                    if (documentpermission != null)
                                    {
                                        folder.DocumentRoleId = userpermission.DocumentRoleId;
                                        folder.UserId = document.UserID;
                                        folder.UserGroupId = document.UserGroupID;
                                        folder.IsDelete = userpermission.IsDelete;
                                        folder.IsRead = userpermission.IsRead;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                    else
                                    {
                                        folder.IsDelete = folder.IsDelete;
                                        folder.IsRead = folder.IsRead;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                }
                                else if (document.UserGroupID != null && document.UserGroupID > 0)
                                {
                                    var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                                    if (userGroupRole != null)
                                    {
                                        var userGroup = userGroupList.Where(u => u.UserId == id).LastOrDefault();
                                        if (userGroup != null)
                                        {
                                            if (document.UserGroupID == userGroup.UserGroupId)
                                            {
                                                var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                                if (userGrouppermission != null)
                                                {

                                                    folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                                                    folder.UserId = document.UserID;
                                                    folder.UserGroupId = document.UserGroupID;
                                                    folder.IsDelete = userGrouppermission.IsDelete;
                                                    folder.IsRead = userGrouppermission.IsRead;
                                                    permissionAppliedFolders.Add(folder);

                                                }
                                                else
                                                {
                                                    folder.IsDelete = false;
                                                    folder.IsRead = false;
                                                    permissionAppliedFolders.Add(folder);
                                                }


                                            }
                                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                            {
                                                folder.IsDelete = false;
                                                folder.IsRead = false;
                                                permissionAppliedFolders.Add(folder);

                                            }
                                        }
                                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                        {
                                            folder.IsDelete = false;
                                            folder.IsRead = false;
                                            permissionAppliedFolders.Add(folder);

                                        }
                                    }
                                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                    {
                                        folder.IsDelete = false;
                                        folder.IsRead = false;
                                        permissionAppliedFolders.Add(folder);

                                    }
                                }
                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                {

                                    folder.IsDelete = false;
                                    folder.IsRead = false;
                                    permissionAppliedFolders.Add(folder);

                                }

                            }
                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == id)
                            {

                                folder.IsDelete = true;
                                folder.IsRead = true;
                                permissionAppliedFolders.Add(folder);

                            }

                        });
                    });
                }



            }
            return folders;

        }

        [HttpGet]
        [Route("GetPersonalFolders")]
        public List<FoldersModel> GetPersonalFolders(int id)
        {
            var folderTypeIds = _context.Folders.Where(t => t.AddedByUserId == id && t.FolderTypeId == 525).AsNoTracking().ToList();
            var folderIds = folderTypeIds.Select(s => s.FolderId).ToList();
            var folders = _context.Folders.Include("AddedByUser").Include("DocumentFolder").Include("ModifiedByUser").Include("Name").Include("FolderType").Include("StatusCode").AsNoTracking().Select(s => new FoldersModel
            {
                FolderID = s.FolderId,
                ParentFolderID = s.ParentFolderId,
                ParentFolderName = s.ParentFolder.Name,
                MainFolderID = s.MainFolderId,
                FolderTypeID = s.FolderTypeId,
                //CodeID = s.StatusCode.CodeId,
                //CodeType = s.StatusCode.CodeType,
                FolderTypeName = s.FolderType.CodeValue,
                Name = s.Name,
                Description = s.Description,
                IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUserID = s.AddedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                StatusCodeID = s.StatusCodeId,
                StatusCode = s.StatusCode.CodeValue,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                Attachments = s.DocumentFolder.Select(sa => sa.DocumentId).ToList(),
                AttachmentIds = s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList(),

            }).OrderByDescending(o => o.FolderID).Where(t => folderIds.Contains(t.FolderID) && t.ParentFolderID == null).ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return folders;

        }

        [HttpGet]
        [Route("DownloadDocument")]
        public IActionResult DownloadDocument(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("GetSubFoldersId")]
        public List<FoldersModel> GetSubFoldersId(int? id, int userId)
        {

            var subFolderId = _context.Folders.Where(t => t.AddedByUserId == userId || t.ModifiedByUserId == userId).AsNoTracking().Select(s => s.FolderId).ToList();
            //var folderId = _context.TaskMembers.Where(t => t.AssignedCc == userId || t.OnBehalfId == userId).Select(s => s.FolderId.Value).ToList();
            //subFolderId.AddRange(folderId);

            var folderMaster = _context.Folders.Select(s => new FoldersModel
            {
                FolderID = s.FolderId,
                Id = s.FolderId,
                MainFolderID = s.MainFolderId,
                ParentFolderID = s.ParentFolderId,
                Name = s.Name,
                Description = s.Description,
                SessionID = s.SessionId,
                FolderTypeID = s.FolderTypeId,
                //AddedByUser = s.AddedByUser.UserName,
                //ModifiedByUser = s.ModifiedByUser.UserName,
                StatusCodeID = s.StatusCodeId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                HasPermission = subFolderId.Any(t => t == s.FolderId),

                Children = new List<FoldersModel>(),
            }).OrderByDescending(o => o.FolderID).Where(i => i.MainFolderID == id).ToList();

            return folderMaster;
        }

        [HttpGet]
        [Route("GetSubFolders")]
        public List<FoldersModel> GetSubFolders(int? id, int userId)
        {
            List<FoldersModel> foldersModels = new List<FoldersModel>();

            var subFolderId = _context.Folders.Where(t => t.AddedByUserId == userId || t.ModifiedByUserId == userId).AsNoTracking().Select(s => s.FolderId).ToList();
            var folderMaster = _context.Folders.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").OrderByDescending(o => o.FolderId).Where(i => i.MainFolderId == id).AsNoTracking().ToList();
            //var folderId = _context.TaskMembers.Where(t => t.AssignedCc == userId || t.OnBehalfId == userId).Select(s => s.FolderId.Value).ToList();
            //subFolderId.AddRange(folderId);
            if (folderMaster != null && folderMaster.Count > 0)
            {
                folderMaster.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderId,
                        MainFolderID = s.MainFolderId,
                        ParentFolderID = s.ParentFolderId,
                        Name = s.Name,
                        Description = s.Description,
                        SessionID = s.SessionId,
                        FolderTypeID = s.FolderTypeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        HasPermission = subFolderId.Any(t => t == s.FolderId),

                        Children = new List<FoldersModel>(),
                    };
                    foldersModels.Add(foldersModel);
                });

            }
            return foldersModels;
        }

        [HttpGet]
        [Route("GetSubNestedFoldersId")]
        public TreeResponse GetSubNestedFoldersId(int? id, int userId)
        {
            //var foldertree = _context.Folders
            // .OrderByDescending(o => o.FolderId).Where(i => i.MainFolderId == id).ToList();
            if (id != -1)
            {
                var foldertree = GetSubNestedPublicFolders(id.Value, userId);

                var lookup = foldertree.ToLookup(x => x.ParentFolderID);
                int treelevel = 0;
                Func<long?, List<FoldersModel>> build = null;
                build = pid =>
                    lookup[pid]
                        .Select(x => new FoldersModel()
                        {
                            FolderID = x.FolderID,
                            //MainFolderID = x.MainFolderId,
                            Id = x.FolderID,
                            Name = x.Name,
                            ParentFolderID = x.ParentFolderID,
                            Description = x.Description,
                            IsRead = x.IsRead,
                            IsDelete = x.IsDelete,
                            TreeLevel = treelevel + 1,
                            cssTitle = x.IsRead.GetValueOrDefault(false) ? "read" : "noPermis",
                            Children = build(x.FolderID)
                        })
                        .ToList();
                var nestedlist = build(null).ToList();

                return new TreeResponse { FlatFolders = foldertree, HierarchicalFolders = nestedlist };
            }
            else
            {
                var foldertree = GetSubNestedPublicFolders(0, userId);

                var lookup = foldertree.ToLookup(x => x.ParentFolderID);

                Func<long?, List<FoldersModel>> build = null;
                build = pid =>
                    lookup[pid]
                        .Select(x => new FoldersModel()
                        {
                            FolderID = x.FolderID,
                            //MainFolderID = x.MainFolderId,
                            Id = x.FolderID,
                            Name = x.Name,
                            ParentFolderID = x.ParentFolderID,
                            Description = x.Description,
                            IsRead = x.IsRead,
                            IsDelete = x.IsDelete,
                            cssTitle = x.IsRead.GetValueOrDefault(false) ? "read" : "noPermis",
                            Children = build(x.FolderID)
                        })
                        .ToList();
                var nestedlist = build(null).ToList();

                return new TreeResponse { FlatFolders = foldertree, HierarchicalFolders = nestedlist }; ;
            }
        }


        public List<FoldersModel> GetSubNestedPublicFolders(int mainFolderId, int id)
        {
            var folders = new List<FoldersModel>();
            List<Folders> filterFolders = new List<Folders>();
            List<DocumentPermission> documentpermission = null;
            List<DocumentUserRole> documentRoleList = null;
            List<UserGroupUser> userGroupList = null;
            // documentpermission = _context.DocumentPermission.ToList();
            // documentRoleList = _context.DocumentUserRole.ToList();
            userGroupList = _context.UserGroupUser.AsNoTracking().ToList();


            if (mainFolderId > 0)
            {
                filterFolders = _context.Folders.Where(t => t.MainFolderId == mainFolderId).AsNoTracking().ToList();
            }
            else
            {
                filterFolders = _context.Folders.Where(t => t.FolderTypeId == 524 || t.FolderTypeId == 525).AsNoTracking().ToList();
            }
            var folderIds = filterFolders.Select(s => s.FolderId).ToList();
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            documentRoleList = _context.DocumentUserRole.Where(f => folderIds.Contains(f.FolderId.Value)).AsNoTracking().ToList();

            if (filterFolders.Count >= 0)
            {
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();

                folders = filterFolders.Select(s => new FoldersModel
                {
                    FolderID = s.FolderId,
                    MainFolderID = s.MainFolderId,
                    ParentFolderID = s.ParentFolderId,
                    FolderTypeID = s.FolderTypeId,
                    //FolderTypeName = s.FolderType.CodeValue,
                    Name = s.Name,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    // AddedByUser = s.AddedByUser.UserName,
                    //ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCodeID = s.StatusCodeId,
                    // StatusCode = s.StatusCode.CodeValue,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    //Attachments = s.DocumentFolder.Select(sa => sa.DocumentId).ToList(),
                    //AttachmentIds = s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList(),

                }).OrderByDescending(o => o.FolderID).Where(t => t.FolderTypeID == 524 && t.StatusCodeID == 1).ToList();
                bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;

                if (isfolderPermission == true)
                {
                    if (documentUserRole.Count >= 0)
                    {
                        List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                        folders.ForEach(folder =>
                        {
                            var docuRoles = documentUserRole.Where(f => f.FolderID == folder.FolderID && folder.AddedByUserID != id).ToList();
                            if (docuRoles.Count == 0)
                            {
                                if (!permissionAppliedFolders.Any(f => f.FolderID == folder.FolderID) && folder.FolderID == folder.FolderID && folder.AddedByUserID == id)
                                {

                                    folder.IsDelete = true;
                                    folder.IsRead = true;
                                    folder.IsEdit = true;
                                    permissionAppliedFolders.Add(folder);

                                }
                            }

                            docuRoles.ForEach(document =>
                            {
                                if (document.FolderID == folder.FolderID && folder.AddedByUserID != id)
                                {
                                    if (document.UserID == id)
                                    {
                                        var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                                        if (userpermission != null)
                                        {
                                            folder.DocumentRoleId = userpermission.DocumentRoleId;
                                            folder.UserId = document.UserID;
                                            folder.UserGroupId = document.UserGroupID;
                                            folder.IsDelete = userpermission.IsDelete;
                                            folder.IsRead = userpermission.IsRead;
                                            permissionAppliedFolders.Add(folder);
                                        }
                                        else
                                        {
                                            folder.IsDelete = folder.IsDelete;
                                            folder.IsRead = folder.IsRead;
                                            permissionAppliedFolders.Add(folder);
                                        }
                                    }
                                    else if (document.UserGroupID != null && document.UserGroupID > 0)
                                    {
                                        var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                                        if (userGroupRole != null)
                                        {
                                            var userGroup = userGroupList.Where(u => u.UserId == id).LastOrDefault();
                                            if (userGroup != null)
                                            {
                                                if (document.UserGroupID == userGroup.UserGroupId)
                                                {
                                                    var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                                    if (userGrouppermission != null)
                                                    {

                                                        folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                                                        folder.UserId = document.UserID;
                                                        folder.UserGroupId = document.UserGroupID;
                                                        folder.IsDelete = userGrouppermission.IsDelete;
                                                        folder.IsRead = userGrouppermission.IsRead;
                                                        permissionAppliedFolders.Add(folder);

                                                    }
                                                    else
                                                    {
                                                        folder.IsDelete = false;
                                                        folder.IsRead = false;
                                                        permissionAppliedFolders.Add(folder);
                                                    }


                                                }
                                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                                {
                                                    folder.IsDelete = false;
                                                    folder.IsRead = false;
                                                    permissionAppliedFolders.Add(folder);

                                                }
                                            }
                                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                            {

                                                folder.IsDelete = false;
                                                folder.IsRead = false;
                                                permissionAppliedFolders.Add(folder);


                                            }
                                        }
                                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                        {
                                            folder.IsDelete = false;
                                            folder.IsRead = false;
                                            permissionAppliedFolders.Add(folder);

                                        }
                                    }
                                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                    {

                                        folder.IsDelete = false;
                                        folder.IsRead = false;
                                        permissionAppliedFolders.Add(folder);

                                    }

                                }
                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == id)
                                {

                                    folder.IsDelete = true;
                                    folder.IsRead = true;
                                    permissionAppliedFolders.Add(folder);

                                }

                            });
                        });
                    }

                }

            }
            return folders;

        }

        [HttpGet]
        [Route("GetFolderFiles")]
        public List<DocumentsModel> GetFolderFiles(int id, int userId)
        {
            var applicationUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId
            }).AsNoTracking().ToList();

            var FolderAddedByUserID = _context.Folders.Where(p => p.FolderId == id).Select(t => t.AddedByUserId).SingleOrDefault();
            List<Folders> childFolders = _context.Folders.Where(f => f.ParentFolderId == id).AsNoTracking().ToList();
            var folderIds = new List<long>
            {
                id
            };
            childFolders.ForEach(cf =>
            {
                folderIds.Add(cf.FolderId);
            });
            var folderDiscussion = new List<long?>();
            folderDiscussion = _context.FolderDiscussion.Where(f => folderIds.Contains(f.FolderId.Value)).AsNoTracking().Select(s => s.DocumentId).ToList();
            var fileNameList = new List<string>();
            var documentList = _context.Documents.Select(s => new
            {

                s.AddedByUserId,
                s.DocumentType,
                s.CategoryId,
                s.ContentType,
                s.DepartmentId,
                s.DisplayName,
                s.DocumentId,
                s.Extension,
                s.FileName,
            }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var docIds = _context.DocumentFolder.Where(d => d.FolderId == id && d.IsLatest.Value == true).AsNoTracking().ToList();
            if (docIds != null && docIds.Count > 0)
            {
                docIds.ForEach(s =>
                {
                    DocumentsModel documentsModel = new DocumentsModel();

                    //DocumentType = s.Document.DocumentType,
                    //CategoryID = s.Document.CategoryId,
                    //ContentType = s.Document.ContentType,
                    //DepartmentID = s.Document.DepartmentId,
                    //Description = s.Document.Description,
                    //DisplayName = s.Document.DisplayName,
                    //DocumentID = s.Document.DocumentId,
                    //Extension = s.Document.Extension,
                    //FileName = s.Document.FileName,
                    //FileSize = s.Document.FileSize,
                    //ReferenceNumber = s.Document.ReferenceNumber,
                    //SessionID = s.Document.SessionId,
                    //ReadOnlyUserId = s.Document.DocumentRights.Where(w => w.IsRead == true).Select(r => r.UserId.Value).ToList(),
                    //ReadWriteUserId = s.Document.DocumentRights.Where(w => w.IsReadWrite == true).Select(r => r.UserId.Value).ToList(),
                    documentsModel.DocumentID = s.DocumentId.Value;
                    documentsModel.LinkedFolder = s.Folder != null ? s.Folder.Name : string.Empty;
                    documentsModel.LinkID = s.FolderId;
                    documentsModel.UploadDate = s.UploadedDate;
                    documentsModel.DocumentFolderId = s.DocumentFolderId;
                    documentsModel.VersionNo = s.VersionNo;
                    documentsModel.ActualVersionNo = s.ActualVersionNo;
                    documentsModel.DraftingVersionNo = s.DraftingVersionNo;
                    documentsModel.IsLocked = s.IsLocked;
                    documentsModel.IsMajorChange = s.IsMajorChange;
                    documentsModel.IsReleaseVersion = s.IsReleaseVersion;
                    documentsModel.IsNoChange = s.IsNoChange;
                    documentsModel.LockedByUserId = s.LockedByUserId;
                    documentsModel.LockedDate = s.LockedDate;
                    documentsModel.FolderCreatedBy = FolderAddedByUserID;
                    documentsModel.Description = s.CheckInDescription;
                    documentsModel.Type = "Document";
                    //IsSubTask = s.IsSubTask,
                    documentsModel.PreviousDocumentId = s.PreviousDocumentId;
                    documentsModel.UploadedByUser = applicationUsers != null && s.UploadedByUserId != null ? applicationUsers.FirstOrDefault(f => f.UserId == s.UploadedByUserId).UserName : string.Empty;
                    documentsModel.LockedByUser = applicationUsers != null && s.LockedByUserId != null ? applicationUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId).UserName : string.Empty;
                    documentsModel.UploadedByUserId = s.UploadedByUserId;
                    documentsModel.IsDiscussionNotes = folderDiscussion?.Any(a => a == s.DocumentId);
                    documentsModel.FileName = documentList?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FileName;
                    documentsModels.Add(documentsModel);


                });
            }
            var documentIds = documentsModels.Select(s => s.DocumentID).ToList();
            var documents = _context.Documents.Include("DocumentRights").Select(s => new DocumentsModel
            {
                AddedByUserID = s.AddedByUserId,
                DocumentType = s.DocumentType,
                CategoryID = s.CategoryId,
                ContentType = s.ContentType,
                DepartmentID = s.DepartmentId,
                DisplayName = s.DisplayName,
                DocumentID = s.DocumentId,
                Extension = s.Extension,
                FileName = s.FileName,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                IsVideoFile = s.IsVideoFile,
                //FileData = s.FileData,
                FileIndex = s.FileIndex,
                ReferenceNumber = s.ReferenceNumber,
                SessionID = s.SessionId,
                Type = "Document",
                ReadOnlyUserId = s.DocumentRights != null ? s.DocumentRights.Where(w => w.IsRead == true).Select(r => r.UserId.Value).ToList() : new List<long>(),
                ReadWriteUserId = s.DocumentRights != null ? s.DocumentRights.Where(w => w.IsReadWrite == true).Select(r => r.UserId.Value).ToList() : new List<long>(),
            }).Where(f => documentIds.Contains(f.DocumentID));
            var documentsIds = documents.Select(s => s.DocumentID).ToList();
            var setAccess = _context.FileProfileTypeSetAccess.Where(s => documentsIds.Contains(s.DocumentId.Value)).ToList();
            documentsModels.ForEach(f =>
            {
                var setAccessdocExits = setAccess.Where(w => w.DocumentId == f.DocumentID).Count();
                var setAccessExits = true;
                if (f.AddedByUserID != userId)
                {
                    if (setAccessdocExits > 0)
                    {
                        setAccessExits = false;
                        var userExits = setAccess.Where(w => w.UserId == userId && w.DocumentId == f.DocumentID).Count();
                        if (userExits > 0)
                        {
                            setAccessExits = true;
                        }
                    }
                }
                if (setAccessExits == true && f.LinkID > 0)
                {
                    var s = documents.FirstOrDefault(d => d.DocumentID == f.DocumentID);

                    f.DocumentType = s.DocumentType;
                    f.CategoryID = s.CategoryID;
                    f.ContentType = s.ContentType;
                    f.DepartmentID = s.DepartmentID;
                    f.DisplayName = s.DisplayName;
                    f.DocumentID = s.DocumentID;
                    f.Extension = s.Extension;
                    f.FileName = s.FileName;

                    f.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                    f.ReferenceNumber = s.ReferenceNumber;
                    f.SessionID = s.SessionID;
                    f.ReadOnlyUserId = s.ReadOnlyUserId;
                    f.ReadWriteUserId = s.ReadOnlyUserId;
                    f.IsVideoFile = s.IsVideoFile;

                }
            });

            childFolders.ForEach(cf =>
            {
                DocumentPermissionModel documentPermissionModel = new DocumentPermissionModel();
                DocumentsModel documentsModel = new DocumentsModel();
                documentsModel.FileName = cf.Name;
                documentsModel.Description = cf.Description;
                documentsModel.FolderID = cf.FolderId;
                documentsModel.ParentFolderID = cf.ParentFolderId;
                documentsModel.MainFolderID = cf.MainFolderId;
                int folderid = Convert.ToInt32(cf.FolderId);
                documentPermissionModel = GetDocumentUserRoleByFolderIDDocumentID(folderid, userId);
                if (documentPermissionModel != null)
                {
                    documentsModel.IsEdit = documentPermissionModel.IsEdit;
                    documentsModel.IsRead = documentPermissionModel.IsRead;
                }
                documentsModels.Add(documentsModel);
            });

            var folders = GetAllPublicFolders(id);
            if (documentsModels.Count > 0)
            {
                documentsModels.ForEach(d =>
                {
                    DocumentsModel documentsModel = new DocumentsModel();
                    List<FoldersModel> foldersModels = new List<FoldersModel>();
                    foldersModels = ParentPath(folders);

                    d.FullFolderPath = foldersModels.FirstOrDefault(f => f.FolderID == d.FolderID)?.FolderPath;
                    d.DocumentPath = foldersModels.FirstOrDefault(f => f.FolderID == d.FolderID)?.ParentFolderPathName;
                    if (d.FolderID == null)
                    {
                        d.FullFolderPath = foldersModels.FirstOrDefault(f => f.FolderID == id)?.FolderPath;
                        d.DocumentPath = foldersModels.FirstOrDefault(f => f.FolderID == id)?.ParentFolderPathName;

                    }

                });
            }
            return documentsModels;

        }
        // GET: api/Project
        [HttpGet]
        [Route("GetCodeMaster")]
        public List<CodeMasterModel> GetCodeMaster()
        {
            var codeMasterDetails = _context.CodeMaster.AsNoTracking().Select(s => new CodeMasterModel
            {
                CodeID = s.CodeId,
                CodeType = s.CodeType,
                CodeValue = s.CodeValue,
                CodeDescription = s.CodeDescription,

            }).Where(o => o.CodeType == "FolderType").ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return codeMasterDetails;

        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Folders")]
        [HttpGet("GetFolders/{id:int}")]
        public ActionResult<FoldersModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var folders = _context.Folders.SingleOrDefault(p => p.FolderId == id.Value);
            var result = _mapper.Map<FoldersModel>(folders);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet("GetFolderDetail")]
        public FoldersModel GetFolderDetail(int? id)
        {

            var folders = _context.Folders.Select(s => new FoldersModel
            {
                FolderID = s.FolderId,
                Name = s.Name,
                Description = s.Description,
                ParentFolderID = s.ParentFolderId,
                MainFolderID = s.MainFolderId,

            }).Where(o => o.FolderID == id).FirstOrDefault();

            return folders;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FoldersModel> GetData(SearchModel searchModel)
        {
            var folders = new Folders();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).FirstOrDefault();
                        break;
                    case "Last":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).LastOrDefault();
                        break;
                    case "Next":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).LastOrDefault();
                        break;
                    case "Previous":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).FirstOrDefault();
                        break;
                    case "Last":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).LastOrDefault();
                        break;
                    case "Next":
                        folders = _context.Folders.OrderBy(o => o.FolderId).FirstOrDefault(s => s.FolderId > searchModel.Id);
                        break;
                    case "Previous":
                        folders = _context.Folders.OrderByDescending(o => o.FolderId).FirstOrDefault(s => s.FolderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FoldersModel>(folders);
            return result;
        }

        // GET: api/Project/2
        [HttpGet("GetFoldersbyID")]
        public ActionResult<FoldersModel> GetFolderById(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var folder = _context.Folders.SingleOrDefault(p => p.FolderId == id.Value);
            var result = _mapper.Map<FoldersModel>(folder);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpGet]
        [Route("GetSubNestedUserFolders")]
        public List<FoldersModel> GetSubNestedUserFolders(int? id, int userId)
        {
            var userfolderId = _context.Folders.Where(t => t.AddedByUserId == userId || t.ModifiedByUserId == userId).AsNoTracking().Select(s => s.FolderId).ToList();

            var foldertree = _context.Folders
             .OrderByDescending(o => o.FolderId).Where(i => i.MainFolderId == id && userfolderId.Contains(i.FolderId)).AsNoTracking().ToList();

            var lookup = foldertree.ToLookup(x => x.ParentFolderId);
            int TreeLevel = 0;
            Func<long?, List<FoldersModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new FoldersModel()
                    {
                        FolderID = x.FolderId,
                        Id = x.FolderId,
                        Name = x.Name,
                        ParentFolderID = x.ParentFolderId,
                        //MainFolderID = x.MainFolderId,
                        FolderTypeID = x.FolderTypeId,
                        Description = x.Description,
                        TreeLevel = TreeLevel + 1,
                        Children = build(x.FolderId)
                    })
                    .ToList();

            List<FoldersModel> allPageComponents = new List<FoldersModel>();

            var nestedlist = build(null).ToList();
            allPageComponents = nestedlist
       .Where(myObject => myObject.Id == id)
       .Flatten((myObject, objectsBeingFlattened) =>
                  myObject.Children.Except(objectsBeingFlattened))
       .ToList();

            return allPageComponents.ToList(); ;
        }

        [HttpGet]
        [Route("GetMovePublicFolders")]
        public List<FoldersModel> GetMovePublicFolders(int id)
        {
            List<FoldersModel> foldersList = GetAllPublicFolders(id);
            List<FoldersModel> folders = new List<FoldersModel>();
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            folders = FindDocumentLocation(foldersList);

            var folderItems = foldersList.Where(t => t.IsEdit == true || t.AddedByUserID == id).Distinct().ToList();
            if (folderItems != null && folderItems.Count > 0)
            {
                folderItems.ForEach(s =>
                {

                    var folderNames = folders.Where(f => f.FolderID == s.FolderID).SingleOrDefault()?.FolderPath;
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderID,
                        ParentFolderID = s.ParentFolderID,
                        MainFolderID = s.MainFolderID,
                        Name = s.Name,
                        FolderPath = folderNames != null && folderNames != "" ? folderNames : s.Name,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeID,

                    };
                    foldersModels.Add(foldersModel);
                });
            }
            //if(foldersModels.Count>0)
            //{
            //    foldersModels.ForEach(f =>
            //    {
            //        int foderid = Convert.ToInt32(f.FolderID);
            //        folders = GetPublicFolderPath(foderid, id);
            //        if (folders.Count > 0)
            //        {
            //            f.FolderPath = folders.FirstOrDefault(t => t.FolderID == f.FolderID)?.FolderPath;
            //        }
            //    });
            //}
            foldersModels = foldersModels.Where(f => f.FolderPath != null && f.FolderPath != "").ToList();
            return foldersModels;

        }
        [HttpGet]
        [Route("GetPublicFoldersTreeItems")]
        public List<FoldersModel> GetPublicFoldersTreeItems(int id)
        {
            var foldersListItems = _context.Folders.Where(t => t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();

            List<FoldersModel> foldersList = new List<FoldersModel>();
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                foldersListItems.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderId,
                        ParentFolderID = s.ParentFolderId,
                        MainFolderID = s.MainFolderId,
                        Name = s.Name,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,

                    };

                    foldersList.Add(foldersModel);
                });
            }
            // List<FoldersModel> foldersList = GetAllPublicFolders(id);
            List<FoldersModel> folders = new List<FoldersModel>();
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            folders = FindDocumentLocation(foldersList);
            var folderItems = foldersList.Where(t => t.IsEdit == true || t.AddedByUserID == id).Distinct().ToList();
            if (folderItems != null && folderItems.Count > 0)
            {
                folderItems.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel
                    {
                        FolderID = s.FolderID,
                        ParentFolderID = s.ParentFolderID,
                        MainFolderID = s.MainFolderID,
                        Name = s.Name,
                        FolderPath = folders != null ? folders.Where(f => f.FolderID == s.FolderID).Select(f => f.FolderPath).SingleOrDefault() : string.Empty,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeID,

                    };
                    foldersModels.Add(foldersModel);
                });
            }

            return foldersModels;

        }
        [HttpGet]
        [Route("GetMovePersonalFolders")]
        public List<FoldersModel> GetMovePersonalFolders()
        {
            var folders = _context.Folders.Include("AddedByUser").Include("DocumentFolder").Include("ModifiedByUser").Include("Name").Include("FolderType").Include("StatusCode").Where(t => t.FolderTypeId == 525).AsNoTracking().Select(s => new FoldersModel
            {
                FolderID = s.FolderId,
                ParentFolderID = s.ParentFolderId,
                ParentFolderName = s.ParentFolder.Name,
                MainFolderID = s.MainFolderId,
                FolderTypeID = s.FolderTypeId,
                Id = s.FolderId,
                //CodeID = s.StatusCode.CodeId,
                //CodeType = s.StatusCode.CodeType,
                FolderTypeName = s.FolderType.CodeValue,
                Name = s.Name,
                Description = s.Description,
                ModifiedByUserID = s.AddedByUserId,
                AddedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                StatusCodeID = s.StatusCodeId,
                StatusCode = s.StatusCode.CodeValue,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                Attachments = s.DocumentFolder.Select(sa => sa.DocumentId).ToList(),
                AttachmentIds = s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList(),

            }).OrderByDescending(o => o.FolderID).ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return folders;

        }

        [HttpGet]
        [Route("GetDocumentRoles")]
        public List<DocumentRoleModel> GetDocumentRoles()
        {
            var documentRole = _context.DocumentRole.AsNoTracking().Select(s => new DocumentRoleModel
            {
                DocumentRoleID = s.DocumentRoleId,
                DocumentRoleName = s.DocumentRoleName,
                StatusCodeID = s.StatusCodeId,
                //RoleName = s.Role.RoleName,

            }).OrderByDescending(o => o.DocumentRoleID).ToList();
            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);
            return documentRole;
        }

        [HttpGet]
        [Route("GetDocumentUserRoleByFolderID")]
        public List<DocumentUserRoleModel> GetDocumentRolesByID(int id)
        {
            var documentUserRoles = _context.DocumentUserRole.Include("User").Include("Role").Where(f => f.FolderId == id && f.DocumentId == null).AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();

            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            List<long> userIds = documentUserRoles != null && documentUserRoles.Count > 0 ? documentUserRoles.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList() : new List<long>();
            List<long> userGroupIds = documentUserRoles?.Where(u => u.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
            List<UserGroup> userGroups = userGroupItems?.Where(g => userGroupIds.Contains(g.UserGroupId)).ToList();
            List<UserGroupUser> userGroupUsers = userGroupUserItems?.Where(g => g.UserGroupId != null && userGroupIds.Contains(g.UserGroupId.Value)).ToList();
            var employees = _context.Employee.Include("Department").Include("Designation").Where(e => e.UserId != null && userIds.Distinct().ToList().Contains(e.UserId.Value)).AsNoTracking().ToList();
            var departmentlist = _context.Department.AsNoTracking().ToList();
            var applicationUser = _context.ApplicationUser.AsNoTracking().ToList();
            List<DocumentUserRoleModel> documentUserRoleModels = new List<DocumentUserRoleModel>();

            List<DocumentUserRole> documentUserRolesItems = new List<DocumentUserRole>();
            if (userGroupUsers != null && userGroupUsers.Count > 0)
            {
                userGroupUsers.ForEach(g =>
                {
                    if (!documentUserRoleModels.Any(r => r.UserID == g.UserId))
                    {
                        var userGroupRole = documentUserRoles?.Where(s => s.UserGroupId != null).FirstOrDefault(r => r.UserGroupId == g.UserGroupId);
                        var userUserRole = documentUserRoles?.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == g.UserId);
                        var userGroup = userGroups?.FirstOrDefault(ug => ug.UserGroupId == g.UserGroupId);
                        var user = applicationUser?.FirstOrDefault(a => a.UserId == g.UserId);
                        var employee = employees.FirstOrDefault(e => e.UserId == user.UserId);
                        if (userUserRole == null && userGroupRole != null)
                        {
                            if (!documentUserRoles.Any(r => r.UserId == g.UserId && r.FolderId == id && r.RoleId == userUserRole.RoleId))
                            {
                                DocumentUserRole documentUserRole = new DocumentUserRole();
                                documentUserRole.DocumentUserRoleId = 0;
                                documentUserRole.FolderId = id;
                                documentUserRole.UserId = user?.UserId;
                                documentUserRole.RoleId = userGroupRole?.RoleId;
                                _context.DocumentUserRole.AddRange(documentUserRole);
                                _context.SaveChanges();
                                DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel();
                                documentUserRoleModel.DocumentUserRoleID = documentUserRole.DocumentUserRoleId;
                                documentUserRoleModel.UserID = user.UserId;
                                documentUserRoleModel.RoleID = documentUserRole?.RoleId;
                                documentUserRoleModel.FolderID = documentUserRole?.FolderId;
                                documentUserRoleModel.RoleName = documentUserRole?.Role?.DocumentRoleName;
                                documentUserRoleModel.UserName = user != null ? user.UserName : string.Empty;
                                documentUserRoleModel.UserGroupName = userGroup.Name;
                                documentUserRoleModel.UserGroupID = userGroup.UserGroupId;
                                documentUserRoleModel.NickName = employee != null ? employee.NickName : string.Empty;
                                // documentUserRoleModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                                documentUserRoleModel.DepartmentName = departmentlist != null && employee?.DepartmentId > 0 ? departmentlist.Where(d => d.DepartmentId == employee?.DepartmentId).Select(d => d.Name)?.FirstOrDefault() : "";
                                documentUserRoleModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                                documentUserRoleModel.DocumentID = documentUserRole?.DocumentId;
                                documentUserRoleModel.UserGroupIDs = new List<long?> { documentUserRole.UserGroupId };
                                documentUserRoleModels.Add(documentUserRoleModel);
                            }
                        }
                        else if (userUserRole != null)
                        {
                            DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel();
                            documentUserRoleModel.DocumentUserRoleID = userUserRole.DocumentUserRoleId;
                            documentUserRoleModel.UserID = user?.UserId;
                            documentUserRoleModel.RoleID = userUserRole.RoleId;
                            documentUserRoleModel.FolderID = userUserRole.FolderId;
                            documentUserRoleModel.RoleName = userUserRole.Role?.DocumentRoleName;
                            documentUserRoleModel.UserName = user != null ? user.UserName : string.Empty;
                            documentUserRoleModel.UserGroupName = userGroup.Name;
                            documentUserRoleModel.UserGroupID = userGroup.UserGroupId;
                            documentUserRoleModel.NickName = employee != null ? employee.NickName : string.Empty;
                            documentUserRoleModel.DepartmentName = departmentlist != null && employee.DepartmentId > 0 ? departmentlist.Where(d => d.DepartmentId == employee.DepartmentId).Select(d => d.Name).FirstOrDefault() : "";
                            // documentUserRoleModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                            documentUserRoleModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                            documentUserRoleModel.DocumentID = userUserRole.DocumentId;
                            documentUserRoleModel.UserGroupIDs = new List<long?> { userUserRole.UserGroupId };
                            documentUserRoleModels.Add(documentUserRoleModel);
                        }
                    }
                });
            }
            if (userIds != null && userIds.Count > 0)
            {
                userIds.ForEach(u =>
                {
                    if (!documentUserRoleModels.Any(r => r.UserID == u))
                    {
                        var employee = employees?.FirstOrDefault(e => e.UserId == u);
                        var userUserRole = documentUserRoles?.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                        var userGroupRole = documentUserRoles?.FirstOrDefault(ug => ug.FolderId == userUserRole.FolderId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                        // List<long?> currentUserGroupIds = userGroupUserItems?.Where(ug => ug.UserId == u).Select(s => s.UserGroupId).ToList();
                        DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel();
                        documentUserRoleModel.DocumentUserRoleID = userUserRole != null ? userUserRole.DocumentUserRoleId : new long();
                        documentUserRoleModel.UserID = userUserRole?.UserId;
                        documentUserRoleModel.RoleID = userUserRole?.RoleId;
                        documentUserRoleModel.FolderID = userUserRole?.FolderId;
                        documentUserRoleModel.RoleName = userUserRole?.Role?.DocumentRoleName;
                        documentUserRoleModel.UserName = userUserRole?.User?.UserName;
                        documentUserRoleModel.NickName = employee?.NickName;
                        documentUserRoleModel.UserGroupName = userUserRole != null ? userUserRole.ReferencedGroupId.HasValue && userUserRole.ReferencedGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.ReferencedGroupId).Name : userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty : string.Empty;
                        // documentUserRoleModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                        documentUserRoleModel.DepartmentName = departmentlist != null && employee.DepartmentId > 0 ? departmentlist.Where(d => d.DepartmentId == employee.DepartmentId).Select(d => d.Name).FirstOrDefault() : "";
                        documentUserRoleModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                        documentUserRoleModel.DocumentID = userUserRole.DocumentId;
                        documentUserRoleModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                        documentUserRoleModels.Add(documentUserRoleModel);
                    }
                });
            }

            documentUserRoleModels = documentUserRoleModels.OrderByDescending(o => o.DocumentUserRoleID).Where(s => s.FolderID == id && s.DocumentID == null).ToList();
            return documentUserRoleModels;
        }

        [HttpGet]
        [Route("GetDocumentUserRoleByDocumentID")]
        public List<DocumentUserRoleModel> GetDocumentRolesByDocumentID(int id)
        {
            List<DocumentUserRoleModel> documentUserRoleModels = new List<DocumentUserRoleModel>();
            var documentRolelist = _context.DocumentUserRole.AsNoTracking().ToList();
            var applicationUserList = _context.ApplicationUser.AsNoTracking().ToList();
            var documentRole = _context.DocumentUserRole.Include("Role").Include("User").Include("UserGroup").OrderByDescending(o => o.DocumentUserRoleId).Where(s => s.DocumentId == id && s.UserId != null).Distinct().AsNoTracking().ToList();
            if (documentRole != null && documentRole.Count > 0)
            {
                documentRole.ForEach(s =>
                {
                    DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel
                    {
                        DocumentUserRoleID = s.DocumentUserRoleId,
                        UserID = s.UserId,
                        RoleID = s.RoleId,
                        FolderID = s.FolderId,
                        RoleName = s.Role != null ? s.Role.DocumentRoleName : string.Empty,
                        UserName = s.User != null ? s.User.UserName : string.Empty,
                        UserGroupName = s.UserGroup != null ? s.UserGroup.Name : string.Empty,
                        DocumentID = s.DocumentId,
                        UserGroupID = s.UserGroupId,
                        UserGroupIDs = documentRolelist != null ? documentRolelist.Where(d => d.DocumentId == id).Select(d => d.UserGroupId).ToList() : new List<long?>(),
                        UserIDs = documentRolelist != null ? documentRolelist.Where(d => d.DocumentId == id).Select(d => d.UserId).ToList() : new List<long?>(),
                    };
                    documentUserRoleModels.Add(documentUserRoleModel);
                });

            }
            var setAccess = _context.FileProfileTypeSetAccess.Where(s => s.DocumentId == id).ToList();
            if (setAccess != null && setAccess.Count > 0)
            {
                setAccess.ForEach(su =>
                {
                    DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel();

                    documentUserRoleModel.RoleName = "Set Access";
                    documentUserRoleModel.UserName = applicationUserList.FirstOrDefault(f => f.UserId == su.UserId)?.UserName;
                    //documentUserRoleModel.UserGroupName = applicationUserList.FirstOrDefault(f => f.UserId == su.UserId)?.UserName;
                    documentUserRoleModel.DocumentID = id;
                    documentUserRoleModels.Add(documentUserRoleModel);

                });
            }
            return documentUserRoleModels;
        }
        [HttpGet]
        [Route("GetuserTreefolderPermission")]
        public DocumentPermissionModel GetPermissionByFolder(int id, int userId)
        {
            DocumentPermissionModel documentPermission = new DocumentPermissionModel();
            List<DocumentPermission> documentPermissionList = new List<DocumentPermission>();
            documentPermissionList = _context.DocumentPermission.AsNoTracking().ToList();
            var userGroupList = _context.UserGroupUser.AsNoTracking().ToList();
            var documentRoleList = _context.DocumentUserRole.Where(p => p.FolderId == id).AsNoTracking().ToList();
            var documentUserRole = documentRoleList.FirstOrDefault(p => p.FolderId == id && p.UserId == userId);

            bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;

            if (isfolderPermission == true)
            {

                if (documentUserRole != null)
                {
                    var userpermission = documentPermissionList.Where(d => d.DocumentRoleId == documentUserRole.RoleId).FirstOrDefault();
                    if (userpermission != null)
                    {
                        documentPermission = new DocumentPermissionModel();
                        documentPermission.IsDelete = userpermission.IsDelete;
                        documentPermission.IsRead = userpermission.IsRead;

                    }
                    else
                    {
                        documentPermission = new DocumentPermissionModel();
                        documentPermission.IsDelete = true;
                        documentPermission.IsRead = true;
                    }
                }
                else
                {
                    var userGroup = userGroupList.Where(u => u.UserId == userId).LastOrDefault();
                    if (userGroup != null)
                    {
                        var userGroupRole = documentRoleList.Where(d => d.UserGroupId == userGroup.UserGroupId).FirstOrDefault();
                        if (userGroupRole != null)
                        {
                            if (userGroupRole.UserGroupId == userGroup.UserGroupId)
                            {
                                var userpermission = documentPermissionList.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                if (userpermission != null)
                                {
                                    documentPermission = new DocumentPermissionModel();
                                    documentPermission.IsDelete = userpermission.IsDelete;
                                    documentPermission.IsRead = userpermission.IsRead;

                                }
                                else
                                {
                                    documentPermission = new DocumentPermissionModel();
                                    documentPermission.IsDelete = true;
                                    documentPermission.IsRead = true;
                                }

                            }
                            else
                            {
                                documentPermission = new DocumentPermissionModel();
                                documentPermission.IsDelete = true;
                                documentPermission.IsRead = true;
                            }
                        }
                        else
                        {
                            documentPermission = new DocumentPermissionModel();
                            documentPermission.IsDelete = true;
                            documentPermission.IsRead = true;
                        }


                    }
                    else
                    {
                        documentPermission = new DocumentPermissionModel();
                        documentPermission.IsDelete = false;
                        documentPermission.IsRead = false;

                    }

                }
            }



            return documentPermission;
        }

        [HttpGet]
        [Route("GetDocumentUserRoleByFolderIDDocumentID")]
        public DocumentPermissionModel GetDocumentUserRoleByFolderIDDocumentID(int id, int userId)
        {

            bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;
            DocumentPermissionModel documentPermissionModel = new DocumentPermissionModel();
            var documentUserRoleAll = _context.DocumentUserRole.Where(p => p.FolderId == id).AsNoTracking().ToList();
            var userFolder = _context.Folders.Where(p => p.FolderId == id && p.AddedByUserId == userId).AsNoTracking().ToList();
            var documentUserRole = _context.DocumentUserRole.FirstOrDefault(p => p.FolderId == id && p.UserId == userId);
            var Folder = _context.Folders.Where(p => p.FolderId == id).FirstOrDefault();
            var FilelockededBy = _context.DocumentFolder.Where(p => p.DocumentId == id).Select(t => t.LockedByUserId).FirstOrDefault();
            if (isfolderPermission == true)
            {
                var userdesignation = _context.Employee.Where(e => e.UserId == userId)?.FirstOrDefault()?.DesignationId;
                if (userdesignation != null)
                {
                    var inActiveDesignation = _context.Designation.Where(d => d.DesignationId == userdesignation).FirstOrDefault()?.StatusCodeId;
                    if (inActiveDesignation == 2)
                    {
                        var documentPermission = _context.DocumentPermission.Select(b => new DocumentPermissionModel
                        {
                            DocumentPermissionID = b.DocumentPermissionId,
                            IsRead = false,
                            IsCreateFolder = false,
                            IsCreateDocument = false,
                            IsSetAlert = false,
                            IsEditIndex = false,
                            IsRename = false,
                            IsUpdateDocument = false,
                            IsCopy = false,
                            IsMove = false,
                            IsDelete = false,
                            IsRelationship = false,
                            IsListVersion = false,
                            IsInvitation = false,
                            IsSendEmail = false,
                            IsDiscussion = false,
                            IsAccessControl = false,
                            IsAuditTrail = false,
                            IsEdit = false,
                            IsFileDelete = false,
                            IsGrantAdminPermission = false,
                            FolderCreatedBy = Folder != null ? Folder.AddedByUserId : new long?(),
                            FolderName = Folder != null ? Folder.Name : "",
                            FolderID = Folder != null ? Folder.FolderId : new long?(),
                            RoleID = 1,
                            //DocumentUserRole Details

                        }).OrderByDescending(a => a.RoleID).FirstOrDefault();
                        return documentPermission;
                    }
                }

                if (Folder != null)
                {
                    if (Folder.AddedByUserId != userId)
                    {
                        if (documentUserRole == null)
                        {
                            var dpUserGroupIds = _context.UserGroupUser.Where(p => p.UserId == userId).AsNoTracking().Select(s => s.UserGroupId).ToList();

                            if (dpUserGroupIds != null)
                            {
                                var documentUserGroupRole = _context.DocumentUserRole.FirstOrDefault(p => p.FolderId == id && dpUserGroupIds.Contains(p.UserGroupId));
                                if (documentUserGroupRole != null)
                                {
                                    List<DocumentPermissionModel> documentPermissionModels = new List<DocumentPermissionModel>();
                                    var documentPermissions = _context.DocumentPermission.Where(a => a.DocumentRoleId == documentUserGroupRole.RoleId).ToList();
                                    documentPermissions.ForEach(b =>
                                    {
                                        DocumentPermissionModel permissionModel = new DocumentPermissionModel
                                        {
                                            DocumentPermissionID = b.DocumentPermissionId,
                                            IsRead = b.IsRead,
                                            IsCreateFolder = b.IsCreateFolder,
                                            IsCreateDocument = b.IsCreateDocument,
                                            IsSetAlert = b.IsSetAlert,
                                            IsEditIndex = b.IsEditIndex,
                                            IsRename = b.IsRename,
                                            IsUpdateDocument = b.IsUpdateDocument,
                                            IsCopy = b.IsCopy,
                                            IsMove = b.IsMove,
                                            IsDelete = b.IsDelete,
                                            IsRelationship = b.IsRelationship,
                                            IsListVersion = b.IsListVersion,
                                            IsInvitation = b.IsInvitation,
                                            IsSendEmail = b.IsSendEmail,
                                            IsDiscussion = b.IsDiscussion,
                                            IsAccessControl = b.IsAccessControl,
                                            IsAuditTrail = b.IsAuditTrail,
                                            IsEdit = b.IsEdit,
                                            IsFileDelete = b.IsFileDelete,
                                            IsGrantAdminPermission = b.IsGrantAdminPermission,
                                            FolderCreatedBy = Folder?.AddedByUserId,
                                            FolderName = Folder?.Name,
                                            //DocumentUserRole Details
                                            DocumentUserRoleID = documentUserRole != null ? documentUserRole.DocumentUserRoleId : new long(),
                                            UserID = documentUserRole?.UserId,
                                            RoleID = documentUserRole?.RoleId,
                                            UserGroupID = documentUserRole?.UserGroupId,
                                            FolderID = documentUserRole?.FolderId,
                                            DocumentID = documentUserRole?.DocumentId,
                                            FolderAddedByUserID = userFolder?.Where(u => u.FolderId == id).Select(u => u.AddedByUserId).FirstOrDefault(),
                                            UserGroupIDs = documentUserRoleAll?.Where(d => d.FolderId == documentUserGroupRole?.FolderId).Select(d => d.UserGroupId).ToList(),
                                            UserIDs = documentUserRoleAll?.Where(d => d.FolderId == documentUserRole?.FolderId).Select(d => d.UserId).ToList(),
                                        };
                                        documentPermissionModels.Add(permissionModel);
                                    });

                                    return documentPermissionModels.OrderByDescending(r => r.RoleID).FirstOrDefault();
                                }
                                else if (documentUserRoleAll.Count != 0)
                                {
                                    var documentPermission = _context.DocumentPermission.Select(b => new DocumentPermissionModel
                                    {
                                        DocumentPermissionID = b.DocumentPermissionId,
                                        IsRead = false,
                                        IsCreateFolder = false,
                                        IsCreateDocument = false,
                                        IsSetAlert = false,
                                        IsEditIndex = false,
                                        IsRename = false,
                                        IsUpdateDocument = false,
                                        IsCopy = false,
                                        IsMove = false,
                                        IsDelete = false,
                                        IsRelationship = false,
                                        IsListVersion = false,
                                        IsInvitation = false,
                                        IsSendEmail = false,
                                        IsDiscussion = false,
                                        IsAccessControl = false,
                                        IsAuditTrail = false,
                                        IsEdit = false,
                                        IsFileDelete = false,
                                        IsGrantAdminPermission = false,
                                        FolderCreatedBy = Folder != null ? Folder.AddedByUserId : new long?(),
                                        FolderName = Folder != null ? Folder.Name : "",
                                        FolderID = Folder != null ? Folder.FolderId : new long?(),
                                        RoleID = 1,
                                        //DocumentUserRole Details

                                    }).OrderByDescending(a => a.RoleID).FirstOrDefault();
                                    return documentPermission;

                                }

                            }

                        }
                        else
                        {
                            List<DocumentPermissionModel> documentPermissionModels = new List<DocumentPermissionModel>();
                            var documentPermissions = _context.DocumentPermission.Where(a => a.DocumentRoleId == documentUserRole.RoleId).ToList();
                            documentPermissions.ForEach(b =>
                            {
                                DocumentPermissionModel permissionModel = new DocumentPermissionModel
                                {
                                    DocumentPermissionID = b.DocumentPermissionId,
                                    IsRead = b.IsRead,
                                    IsCreateFolder = b.IsCreateFolder,
                                    IsCreateDocument = b.IsCreateDocument,
                                    IsSetAlert = b.IsSetAlert,
                                    IsEditIndex = b.IsEditIndex,
                                    IsRename = b.IsRename,
                                    IsUpdateDocument = b.IsUpdateDocument,
                                    IsCopy = b.IsCopy,
                                    IsMove = b.IsMove,
                                    IsDelete = b.IsDelete,
                                    IsRelationship = b.IsRelationship,
                                    IsListVersion = b.IsListVersion,
                                    IsInvitation = b.IsInvitation,
                                    IsSendEmail = b.IsSendEmail,
                                    IsDiscussion = b.IsDiscussion,
                                    IsAccessControl = b.IsAccessControl,
                                    IsAuditTrail = b.IsAuditTrail,
                                    IsEdit = b.IsEdit,
                                    IsFileDelete = b.IsFileDelete,
                                    IsGrantAdminPermission = b.IsGrantAdminPermission,
                                    FolderCreatedBy = Folder?.AddedByUserId,
                                    FolderName = Folder?.Name,
                                    //DocumentUserRole Details
                                    DocumentUserRoleID = documentUserRole != null ? documentUserRole.DocumentUserRoleId : new long(),
                                    UserID = documentUserRole?.UserId,
                                    RoleID = documentUserRole?.RoleId,
                                    UserGroupID = documentUserRole?.UserGroupId,
                                    FolderID = documentUserRole?.FolderId,
                                    DocumentID = documentUserRole?.DocumentId,
                                    FolderAddedByUserID = userFolder?.Where(u => u.FolderId == id).Select(u => u.AddedByUserId).FirstOrDefault(),
                                    UserGroupIDs = documentUserRoleAll?.Where(d => d.FolderId == documentUserRole?.FolderId).Select(d => d.UserGroupId).ToList(),
                                    UserIDs = documentUserRoleAll?.Where(d => d.FolderId == documentUserRole?.FolderId).Select(d => d.UserId).ToList(),
                                };
                                documentPermissionModels.Add(permissionModel);
                            });

                            return documentPermissionModels.OrderByDescending(c => c.RoleID).FirstOrDefault();
                        }
                    }
                }

                if (Folder != null)
                {
                    documentPermissionModel.FolderName = Folder?.Name;
                    documentPermissionModel.FolderID = Folder?.FolderId;
                    documentPermissionModel.FolderAddedByUserID = userFolder?.Where(u => u.FolderId == id).Select(u => u.AddedByUserId).FirstOrDefault();
                }
            }
            return documentPermissionModel;
        }

        [HttpGet]
        [Route("GetUserFolderRole")]
        public DocumentUserRoleModel GetFolderLoginId(int id, int userId)
        {
            var userRolePermission = new DocumentUserRoleModel();
            var documentuserroleList = _context.DocumentUserRole.AsNoTracking().ToList();
            var userFolder = _context.Folders.Where(p => p.FolderId == id && p.AddedByUserId == userId).AsNoTracking().ToList();
            if (userFolder.Count == 0)
            {

                var documentUserRole = documentuserroleList.FirstOrDefault(p => p.FolderId == id && p.DocumentId == null);
                if (documentUserRole != null)
                {
                    if (documentUserRole.UserId != null)
                    {

                        userRolePermission.RoleID = documentUserRole.RoleId;

                        userRolePermission.UserIDs = documentuserroleList.Where(d => d.FolderId == documentUserRole.FolderId).Select(d => d.UserId).ToList();
                        // userRole.FolderID = documentUserRole.FolderId;
                    }
                    else
                    {
                        if (documentUserRole.UserGroupId != null)
                        {
                            userRolePermission.RoleID = documentUserRole.RoleId;
                            userRolePermission.UserGroupIDs = documentuserroleList.Where(d => d.FolderId == documentUserRole.FolderId).Select(d => d.UserGroupId).ToList();


                        }
                    }

                }

                return userRolePermission;

            }
            else
            {

                userRolePermission = new DocumentUserRoleModel();
                userRolePermission.FolderAddedByUserID = userFolder.Where(u => u.FolderId == id).Select(u => u.AddedByUserId).FirstOrDefault();
                return userRolePermission;
            }

        }

        [HttpGet]
        [Route("GetSubFolderPermission")]
        public DocumentUserRoleModel GetSubFolderPermission(int id, int userId)
        {
            var userRolePermission = new DocumentUserRoleModel();
            var userFolder = _context.Folders.Where(p => p.FolderId == id && p.AddedByUserId == userId).AsNoTracking().ToList();
            if (userFolder.Count == 0)
            {

                var documentUserRole = _context.DocumentUserRole.FirstOrDefault(p => p.FolderId == id && p.DocumentId == null);
                if (documentUserRole != null)
                {
                    if (documentUserRole.UserId != null)
                    {

                        userRolePermission.RoleID = documentUserRole.RoleId;

                        userRolePermission.UserIDs = _context.DocumentUserRole.Where(d => d.FolderId == documentUserRole.FolderId).Select(d => d.UserId).ToList();
                        // userRole.FolderID = documentUserRole.FolderId;
                    }
                    else
                    {
                        if (documentUserRole.UserGroupId != null)
                        {
                            userRolePermission.RoleID = documentUserRole.RoleId;
                            userRolePermission.UserGroupIDs = _context.DocumentUserRole.Where(d => d.FolderId == documentUserRole.FolderId).Select(d => d.UserGroupId).ToList();


                        }
                    }

                }

                return userRolePermission;

            }
            else
            {

                userRolePermission = new DocumentUserRoleModel();
                userRolePermission.FolderAddedByUserID = userFolder.Where(u => u.FolderId == id).Select(u => u.AddedByUserId).FirstOrDefault();
                return userRolePermission;
            }

        }
        [HttpGet]
        [Route("GetEditFolderByFolderID")]
        public ActionResult<FoldersModel> GetEditFolderByFolderID(int id)
        {
            var userGroupIds = _context.DocumentUserRole.Where(p => p.FolderId == id && p.UserGroupId != null).AsNoTracking().Select(u => u.UserGroupId).ToList();
            var userIds = _context.DocumentUserRole.Where(p => p.FolderId == id && p.UserId != null).AsNoTracking().Select(u => u.UserId).ToList();
            DocumentUserRoleModel docUserModel = new DocumentUserRoleModel();
            var documentRoleForSubFolder = _context.DocumentUserRole.Where(p => p.FolderId == id).AsNoTracking().Select(s => new DocumentUserRoleModel
            {
                DocumentUserRoleID = s.DocumentUserRoleId,
                UserID = s.UserId,
                UserGroupID = s.UserGroupId,
                RoleID = s.RoleId,
                UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>(),
                UserIDs = userIds != null ? userIds : new List<long?>(),
                FolderID = s.FolderId,
                DocumentID = s.DocumentId


            }).OrderByDescending(o => o.DocumentUserRoleID).ToList();

            var docRoleID = documentRoleForSubFolder.Select(a => a.RoleID).LastOrDefault();
            if (docRoleID == null)
            {
                docRoleID = documentRoleForSubFolder.Select(a => a.RoleID).FirstOrDefault();
            }
            var docUserID = documentRoleForSubFolder.Select(a => a.UserID).LastOrDefault();
            if (docUserID == null)
            {
                docUserID = documentRoleForSubFolder.Select(a => a.UserID).FirstOrDefault();
            }
            var docUserGroupID = documentRoleForSubFolder.Select(a => a.UserGroupID).LastOrDefault();
            if (docUserGroupID == null)
            {
                docUserGroupID = documentRoleForSubFolder.Select(a => a.UserGroupID).FirstOrDefault();
            }

            var documentRole = _context.DocumentUserRole.FirstOrDefault(p => p.FolderId == id);
            FoldersModel foldersModel = new FoldersModel();
            if (documentRole != null && documentRole.DocumentId != null)
            {
                var folderByID = _context.Folders.Include("DocumentFolder").Include("StatusCode").OrderByDescending(o => o.FolderId).Where(s => s.FolderId == id).SingleOrDefault();
                if (folderByID != null)
                {
                    foldersModel = new FoldersModel
                    {
                        DocumentUserRoleId = documentRole.DocumentUserRoleId,
                        UserId = documentRole.UserId,
                        DocumentRoleId = documentRole.RoleId,
                        FolderID = folderByID.FolderId,
                        UserGroupId = documentRole.UserGroupId,
                        UserIDs = userIds != null ? userIds : new List<long?>(),
                        UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>(),
                        Name = folderByID.Name,
                        //FileNames = folderByID.DocumentFolder.Where(t => t.FolderId == folderByID.FolderId).Select(t => t.Document.FileName).ToList(),
                        Description = folderByID.Description,
                        StatusCodeID = folderByID.StatusCodeId,
                        StatusCode = folderByID.StatusCode.CodeValue,
                        IsFolderLevel = documentRole.IsFolderLevel

                    };

                }

                return foldersModel;
            }
            else if (documentRoleForSubFolder != null && (docRoleID > 0 || docUserID > 0 || docUserGroupID > 0))
            {
                var folderByID = _context.Folders.OrderByDescending(o => o.FolderId).Where(s => s.FolderId == id).SingleOrDefault();
                FoldersModel foldersModel1 = new FoldersModel();
                if (folderByID != null)
                {
                    foldersModel1.DocumentUserRoleId = documentRole.DocumentUserRoleId;
                    foldersModel1.UserId = docUserID;
                    foldersModel1.DocumentRoleId = docRoleID;
                    foldersModel1.FolderID = folderByID.FolderId;
                    foldersModel1.UserGroupId = docUserGroupID;
                    foldersModel1.Name = folderByID.Name;
                    foldersModel1.UserIDs = userIds != null ? userIds : new List<long?>();
                    foldersModel1.UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>();
                    //foldersModel1.FileNames = folderByID.DocumentFolder?.Where(t => t.FolderId == folderByID.FolderId).Select(t => t.Document.FileName).ToList();
                    foldersModel1.Description = folderByID.Description;
                    foldersModel1.StatusCodeID = folderByID.StatusCodeId;
                    foldersModel1.StatusCode = folderByID.StatusCode?.CodeValue;

                }

                return foldersModel1;
            }
            else if (documentRole != null && (documentRole.RoleId > 0 || documentRole.UserId > 0 || documentRole.UserGroupId > 0))
            {
                var folderByID = _context.Folders.OrderByDescending(o => o.FolderId).Where(s => s.FolderId == id).SingleOrDefault();
                FoldersModel foldersModel1 = new FoldersModel();
                if (folderByID != null)
                {
                    foldersModel1.DocumentUserRoleId = documentRole.DocumentUserRoleId;
                    foldersModel1.UserId = documentRole.UserId;
                    foldersModel1.DocumentRoleId = documentRole.RoleId;
                    foldersModel1.FolderID = folderByID.FolderId;
                    foldersModel1.UserIDs = userIds != null ? userIds : new List<long?>();
                    foldersModel1.UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>();
                    foldersModel1.UserGroupId = documentRole.UserGroupId;
                    foldersModel1.Name = folderByID.Name;
                    foldersModel1.Description = folderByID.Description;
                    foldersModel1.StatusCodeID = folderByID.StatusCodeId;
                    foldersModel1.StatusCode = folderByID.StatusCode.CodeValue;
                    foldersModel1.IsFolderLevel = documentRole.IsFolderLevel;
                }

                return foldersModel1;
            }
            else
            {
                FoldersModel foldersModel1 = new FoldersModel();
                var folderByID = _context.Folders.OrderByDescending(o => o.FolderId).Where(s => s.FolderId == id).SingleOrDefault();
                if (folderByID != null)
                {
                    foldersModel1.FolderID = folderByID.FolderId;
                    foldersModel1.Name = folderByID.Name;
                    //foldersModel1.FileNames = folderByID.DocumentFolder?.Where(t => t.FolderId == folderByID.FolderId).Select(t => t.Document.FileName).ToList();
                    foldersModel1.Description = folderByID.Description;
                    foldersModel1.StatusCodeID = folderByID.StatusCodeId;
                    foldersModel1.StatusCode = folderByID.StatusCode?.CodeValue;
                }

                return foldersModel1;
            }

        }

        [HttpGet]
        [Route("GetFileUploadByFolderID")]
        public ActionResult<FoldersModel> GetFileUploadByFolderID(int id)
        {
            var userGroupIds = _context.DocumentUserRole.Where(p => p.FolderId == id && p.UserGroupId != null).AsNoTracking().Select(u => u.UserGroupId).ToList();
            var userIds = _context.DocumentUserRole.Where(p => p.FolderId == id && p.UserId != null).AsNoTracking().Select(u => u.UserId).ToList();
            var documentRole = _context.DocumentUserRole.FirstOrDefault(p => p.FolderId == id);
            if (documentRole != null && documentRole.DocumentId != null)
            {
                var folderByID = _context.Folders.Select(s => new FoldersModel
                {
                    DocumentUserRoleId = documentRole.DocumentUserRoleId,
                    UserId = documentRole.UserId,
                    DocumentRoleId = documentRole.RoleId,
                    FolderID = s.FolderId,
                    UserIDs = userIds != null ? userIds : new List<long?>(),
                    UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>(),
                    UserGroupId = documentRole.UserGroupId,
                    Name = s.Name,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty,
                    IsFolderLevel = documentRole.IsFolderLevel,


                }).OrderByDescending(o => o.FolderID).Where(s => s.FolderID == id).SingleOrDefault();
                return folderByID;
            }
            else if (documentRole != null && (documentRole.RoleId > 0 || documentRole.UserId > 0 || documentRole.UserGroupId > 0))
            {
                var folderByID = _context.Folders.OrderByDescending(o => o.FolderId).Where(s => s.FolderId == id).SingleOrDefault();
                FoldersModel folders = new FoldersModel();
                if (folderByID != null)
                {
                    folders = new FoldersModel();
                    folders.DocumentUserRoleId = documentRole.DocumentUserRoleId;
                    folders.UserId = documentRole.UserId;
                    folders.DocumentRoleId = documentRole.RoleId;
                    folders.FolderID = folderByID.FolderId;
                    folders.UserGroupIDs = userGroupIds != null ? userGroupIds : new List<long?>();
                    folders.UserIDs = userIds != null ? userIds : new List<long?>();
                    folders.UserGroupId = documentRole.UserGroupId;
                    folders.Name = folderByID.Name;
                    folders.Description = folderByID.Description;
                    folders.StatusCodeID = folderByID.StatusCodeId;
                    folders.StatusCode = folderByID.StatusCode?.CodeValue;
                    folders.IsFolderLevel = documentRole.IsFolderLevel;
                }

                return folders;
            }
            else
            {
                var folderByID = _context.Folders.Select(s => new FoldersModel
                {
                    FolderID = s.FolderId,
                    Name = s.Name,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty,

                }).OrderByDescending(o => o.FolderID).Where(s => s.FolderID == id).SingleOrDefault();
                return folderByID;
            }

        }

        // POST: api/User
        [HttpPost]
        [Route("InsertFolders")]
        public FoldersModel Post(FoldersModel value)
        {
            if (value.Name != "" && value.Name != null && !string.IsNullOrEmpty(value.Name.Trim()))
            {

                var existingfolderName = "";
                if (value.ParentFolderID == null)
                {
                    existingfolderName = _context.Folders.Where(f => f.ParentFolderId == null && f.Name.ToLower().Trim() == value.Name.ToLower().Trim()).Select(f => f.Name).FirstOrDefault();

                }
                if (existingfolderName == null || existingfolderName == "")
                {
                    var folders = new Folders
                    {
                        //FolderId = value.FolderID,
                        ParentFolderId = value.ParentFolderID,
                        MainFolderId = value.MainFolderID,
                        FolderTypeId = value.FolderTypeID,
                        Name = value.Name,
                        Description = value.Description,
                        SessionId = value.SessionID,
                        StatusCodeId = value.StatusCodeID,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,

                        DocumentFolder = new List<DocumentFolder>(),

                    };
                    _context.Folders.Add(folders);
                    _context.SaveChanges();
                    value.FolderID = folders.FolderId;

                    if (value.SessionID != null)
                    {
                        var docu = _context.Documents.Select(s => new Documents
                        {
                            DocumentId = s.DocumentId,
                            IsTemp = s.IsTemp,
                            Description = s.Description,
                            SessionId = s.SessionId
                        }).Where(d => d.SessionId == value.SessionID).AsNoTracking().ToList();
                        docu.ForEach(file =>
                        {
                            //file.Description = value.Description;
                            file.IsTemp = false;
                            if (docu != null)
                            {
                                file.Description = value.DocumentDescription;
                                var folderAttach = new DocumentFolder
                                {
                                    DocumentId = file.DocumentId,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = value.AddedByUserID,
                                    VersionNo = "1",
                                    IsReleaseVersion = true,
                                    IsNoChange = false,
                                    CheckInDescription = value.Description,
                                };
                                folders.DocumentFolder.Add(folderAttach);
                            }
                        });
                    }


                    if (value.ParentFolderID == null && value.MainFolderID == null)
                    {
                        // to build tree view with master FolderID..
                        var mainfolder = _context.Folders.FirstOrDefault(id => id.FolderId == folders.FolderId);
                        mainfolder.MainFolderId = folders.FolderId;
                        _context.SaveChanges();

                        value.MainFolderID = mainfolder.MainFolderId;
                    }
                }

                else
                {
                    throw new Exception("Folder Name " + value.Name + " Already Exist! Please Create New Different folder Name ");
                }
            }
            else
            {
                throw new Exception("Folder Name is Required field ! Please Enter Folder Name");
            }
            return value;


        }

        [HttpGet]
        [Route("CheckfolderIds")]
        public bool CheckfolderIds(long? id, long? userId)
        {
            bool _result = false;
            try
            {
                var existingTaskCreated = _context.Folders.SingleOrDefault(p => p.FolderId == id && p.MainFolderId == userId);
                if (existingTaskCreated != null)
                {
                    _result = true;

                }
                else
                {
                    _result = false;
                }

                return _result;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        [HttpPost]
        [Route("InsertDocumentUserRole")]
        public FoldersModel PostRole(FoldersModel value)
        {
            var errormessage = "";
            var folderlistIDs = value.FolderList;
            CreateDocumentUserRole(value);
            _context.SaveChanges();
            updateDocumentRole(value.FolderID);
            if (folderlistIDs != null && folderlistIDs.Any())
            {
                folderlistIDs.ForEach(f =>
                {
                    value.FolderID = f.Value;
                    CreateDocumentUserRole(value);
                    _context.SaveChanges();
                    updateDocumentRole(value.FolderID);
                });
            }
            _context.SaveChanges();
            return value;
        }



        [HttpPost]
        [Route("InsertSubFolder")]
        public FoldersModel PostSubFolder(FoldersModel value)
        {
            if (value.Name != "" && value.Name != null && !string.IsNullOrEmpty(value.Name.Trim()))
            {
                var existingfolderName = "";
                if (value.ParentFolderID != null)
                {
                    existingfolderName = _context.Folders.Where(f => f.ParentFolderId == value.ParentFolderID && f.Name.ToLower().Trim() == value.Name.ToLower().Trim()).Select(f => f.Name).FirstOrDefault();

                }
                if (value.ParentFolderID == null)
                {
                    existingfolderName = _context.Folders.Where(f => f.ParentFolderId == null && f.Name.ToLower().Trim() == value.Name.ToLower().Trim()).Select(f => f.Name).FirstOrDefault();

                }
                if (existingfolderName == null || existingfolderName == "")
                {
                    var documentUserRole = new DocumentUserRole();
                    //var exist = _context.Folders.Where(t => value.Name.ToLower().Trim() == t.Name.ToLower().Trim()).FirstOrDefault();
                    //if (exist != null) throw new AppException("There is Folder exist with this Name. please check the Folder or change new Name.!", null);
                    var documentExistingRole = _context.DocumentUserRole.Where(d => d.FolderId == value.MainFolderID && d.DocumentId == null).AsNoTracking().ToList();
                    value.UserIDs = documentExistingRole?.Where(d => d.FolderId == value.MainFolderID && d.UserId != null).Select(u => u.UserId).ToList();
                    value.UserGroupIDs = documentExistingRole?.Where(d => d.FolderId == value.MainFolderID && d.UserGroupId != null).Select(u => u.UserGroupId).ToList();
                    var userList = value.UserIDs;
                    var userGroupList = value.UserGroupIDs;
                    var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).AsNoTracking().ToList();
                    //userList.AddRange(userGroupUserList);
                    //userList.Distinct();
                    var folder = new Folders
                    {

                        //FolderId=value.TaskID,
                        ParentFolderId = value.ParentFolderID,
                        MainFolderId = value.MainFolderID,
                        FolderTypeId = value.FolderTypeID,
                        Name = value.Name,
                        Description = value.Description,
                        SessionId = value.SessionID,
                        StatusCodeId = value.StatusCodeID,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now
                    };

                    _context.Folders.Add(folder);
                    _context.SaveChanges();
                    value.FolderID = folder.FolderId;
                    //var userExistingRole = documentExistingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
                    //var userGroupExistingRole = documentExistingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
                    if (documentExistingRole.Count > 0)
                    {
                        documentExistingRole.ForEach(d =>
                        {
                            if (userList.Any())
                            {
                                userList.ForEach(u =>
                                {
                                    var userExistingRole = _context.DocumentUserRole.Where(du => du.FolderId == folder.FolderId && du.UserId == u && du.DocumentId == null).AsNoTracking().ToList();
                                    if (userExistingRole.Count == 0)
                                    {
                                        documentUserRole = new DocumentUserRole
                                        {
                                            UserId = u,
                                            RoleId = d.RoleId,
                                            FolderId = folder.FolderId,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                });


                            }
                            else if (userGroupList.Any())
                            {
                                userGroupList.ForEach(ug =>
                                {
                                    var usergroupExistingRole = _context.DocumentUserRole.Where(du => du.FolderId == folder.FolderId && du.UserGroupId == ug && du.DocumentId == null).AsNoTracking().ToList();
                                    if (usergroupExistingRole.Count == 0)
                                    {
                                        documentUserRole = new DocumentUserRole
                                        {
                                            UserGroupId = ug,
                                            RoleId = d.RoleId,
                                            FolderId = folder.FolderId,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        List<UserGroupUser> currentGroupUsers = userGroupUserList?.Where(u => u.UserGroupId == ug).ToList();

                                        currentGroupUsers.ForEach(c =>
                                        {
                                            var userExist = _context.DocumentUserRole.Where(du => du.FolderId == folder.FolderId && du.UserId == c.UserId && du.DocumentId == null).AsNoTracking().ToList();
                                            if (userExist.Count == 0)
                                            {
                                                documentUserRole = new DocumentUserRole
                                                {
                                                    RoleId = value.DocumentRoleId,
                                                    UserId = c.UserId,
                                                    ReferencedGroupId = c.UserGroupId,
                                                    FolderId = value.FolderID,
                                                    IsFolderLevel = value.IsFolderLevel
                                                };
                                                _context.DocumentUserRole.Add(documentUserRole);
                                            }
                                        });
                                        _context.SaveChanges();
                                    }

                                });
                            }

                        });

                    }

                    if (value.ParentFolderID == null && value.MainFolderID == null)
                    {
                        // to build tree view with master FolderID..
                        var mainfolder = _context.Folders.FirstOrDefault(id => id.FolderId == folder.FolderId);
                        mainfolder.MainFolderId = folder.FolderId;
                        _context.SaveChanges();

                        value.MainFolderID = mainfolder.MainFolderId;
                    }
                    _context.SaveChanges();
                }
                else
                {
                    throw new Exception("Folder Name " + value.Name + " Already Exist! Please Create New Different folder Name ");
                }
            }
            else
            {
                throw new Exception("Please Enter Folder Name");
            }
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateFolders")]
        public FoldersModel Put(FoldersModel value)
        {
            var folders = _context.Folders.SingleOrDefault(p => p.FolderId == value.FolderID);
            //var existingfolderName = "";
            //if (value.ParentFolderID == null)
            //{
            //    existingfolderName = _context.Folders.Where(f => f.ParentFolderId == null && f.FolderId != value.FolderID && f.Name.ToLower().Trim() == value.Name.ToLower().Trim()).Select(f => f.Name).FirstOrDefault();

            //}
            //if (existingfolderName == null || existingfolderName == "")
            //{
            folders.Name = value.Name;
            value.FolderTypeID = folders.FolderTypeId;
            if (value.MainFolderID == null)
            {
                value.MainFolderID = folders.MainFolderId;
            }

            folders.Description = value.Description;
            folders.SessionId = value.SessionID;
            folders.StatusCodeId = value.StatusCodeID;
            folders.ModifiedByUserId = value.ModifiedByUserID;
            folders.ModifiedDate = DateTime.Now;
            var documentFolders = _context.DocumentFolder.Where(f => f.FolderId == value.FolderID).AsNoTracking().ToList();
            var FolderRoles = _context.DocumentUserRole.Where(d => d.FolderId == value.FolderID && d.DocumentId == null).AsNoTracking().ToList();
            // var usergroupIds = _context.DocumentUserRole.Where(d => d.FolderId == value.FolderID && d.DocumentId == null).ToList();
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new Documents
                {
                    DocumentId = s.DocumentId,
                    IsTemp = s.IsTemp,
                    Description = s.Description,
                    SessionId = s.SessionId
                }).Where(d => d.SessionId == value.SessionID).ToList();

                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null && !documentFolders.Any(d => d.DocumentId == file.DocumentId))
                    {
                        file.Description = value.DocumentDescription;
                        var folderAttach = new DocumentFolder
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            IsReleaseVersion = true,
                            IsNoChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            CheckInDescription = value.DocumentDescription,
                        };
                        folders.DocumentFolder.Add(folderAttach);
                        _context.SaveChanges();
                    }
                });

                if (documentFolders.Count != 0 && documentFolders != null)
                {
                    documentFolders.ForEach(doc =>
                    {
                        if (FolderRoles.Count != 0 && FolderRoles != null)
                        {
                            FolderRoles.ForEach(u =>
                            {
                                var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserId == u.UserId && du.FolderId == u.FolderId).FirstOrDefault();
                                if (existingDocumentRole == null)
                                {
                                    if (u.UserId != null)
                                    {
                                        DocumentUserRole documentUserRole = new DocumentUserRole
                                        {

                                            UserId = u.UserId,
                                            RoleId = u.RoleId,
                                            DocumentId = doc.DocumentId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }

                                }
                                var existingUserGroupRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserGroupId == u.UserGroupId).FirstOrDefault();
                                if (existingUserGroupRole == null)
                                {
                                    if (u.UserGroupId != null)
                                    {
                                        DocumentUserRole documentUserRole = new DocumentUserRole
                                        {
                                            RoleId = u.RoleId,
                                            UserGroupId = u.UserGroupId,
                                            DocumentId = doc.DocumentId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                }
                            });
                        }
                    });
                }
            }
            else
            {
                if (documentFolders.Count != 0 && documentFolders != null)
                {
                    documentFolders.ForEach(doc =>
                    {
                        if (FolderRoles.Count != 0 && FolderRoles != null)
                        {
                            FolderRoles.ForEach(u =>
                            {
                                var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserId == u.UserId && du.FolderId == u.FolderId).FirstOrDefault();
                                if (existingDocumentRole == null)
                                {
                                    if (u.UserId != null)
                                    {
                                        DocumentUserRole documentUserRole = new DocumentUserRole
                                        {
                                            UserId = u.UserId,
                                            RoleId = u.RoleId,
                                            DocumentId = doc.DocumentId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                }
                                var existingUserGroupRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserGroupId == u.UserGroupId).FirstOrDefault();
                                if (existingUserGroupRole == null)
                                {
                                    if (u.UserGroupId != null)
                                    {
                                        DocumentUserRole documentUserRole = new DocumentUserRole
                                        {
                                            RoleId = u.RoleId,
                                            UserGroupId = u.UserGroupId,
                                            DocumentId = doc.DocumentId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                }
                            });
                        }

                    });

                }

            }
            _context.SaveChanges();

            if (value.ParentFolderID == null && value.MainFolderID == null)
            {
                // to build tree view with master FolderID..
                var mainfolder = _context.Folders.FirstOrDefault(id => id.FolderId == folders.FolderId);
                mainfolder.MainFolderId = folders.FolderId;
                _context.SaveChanges();

                value.MainFolderID = mainfolder.MainFolderId;
            }
            //}
            //else
            //{
            //    throw new Exception("Folder Name " + value.Name + " Already Exist! Please Create New Different folder Name ");
            //}
            return value;
        }

        [HttpPut]
        [Route("UpdateSubFolder")]
        public FoldersModel PutSubFolder(FoldersModel value)
        {
            var folder = _context.Folders.SingleOrDefault(p => p.FolderId == value.FolderID);
            //var existingfolderName = "";
            //if (value.ParentFolderID != null)
            //{
            //    existingfolderName = _context.Folders.Where(f => f.ParentFolderId == value.ParentFolderID && f.FolderId != value.FolderID && f.Name.ToLower().Trim() == value.Name.ToLower().Trim()).Select(f => f.Name).FirstOrDefault();

            //}
            //if (existingfolderName == null || existingfolderName == "")
            //{
            var documentUserRoleUpdate = _context.DocumentUserRole.SingleOrDefault(p => p.DocumentUserRoleId == value.DocumentUserRoleId);
            folder.Name = value.Name;
            if (value.MainFolderID == null)
            {
                value.MainFolderID = folder.MainFolderId;
            }
            folder.Name = value.Name;
            folder.Description = value.Description;
            value.FolderTypeID = folder.FolderTypeId;
            folder.SessionId = value.SessionID;
            folder.StatusCodeId = value.StatusCodeID;
            folder.ModifiedByUserId = value.ModifiedByUserID;
            folder.ModifiedDate = DateTime.Now;
            var documentFolders = _context.DocumentFolder.Where(f => f.FolderId == value.FolderID).AsNoTracking().ToList();
            var FolderRoles = _context.DocumentUserRole.Where(d => d.FolderId == value.FolderID && d.DocumentId == null).AsNoTracking().ToList();
            // var usergroupIds = _context.DocumentUserRole.Where(d => d.FolderId == value.FolderID && d.DocumentId == null).ToList();
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new Documents
                {
                    DocumentId = s.DocumentId,
                    IsTemp = s.IsTemp,
                    Description = s.Description,
                    SessionId = s.SessionId
                }).Where(d => d.SessionId == value.SessionID).ToList();

                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null && !documentFolders.Any(d => d.DocumentId == file.DocumentId))
                    {
                        file.Description = value.DocumentDescription;
                        var folderAttach = new DocumentFolder
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            IsReleaseVersion = true,
                            IsNoChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            CheckInDescription = value.DocumentDescription,
                        };
                        folder.DocumentFolder.Add(folderAttach);
                        _context.SaveChanges();
                    }
                });

                if (documentFolders.Count != 0 && documentFolders != null)
                {
                    documentFolders.ForEach(doc =>
                    {
                        if (FolderRoles.Count != 0 && FolderRoles != null)
                        {
                            FolderRoles.ForEach(u =>
                            {
                                var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserId == u.UserId).FirstOrDefault();
                                if (existingDocumentRole == null)
                                {

                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {

                                        UserId = u.UserId,
                                        RoleId = u.RoleId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = value.FolderID,
                                        IsFolderLevel = value.IsFolderLevel
                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();

                                }
                                var existingUserGroupRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserGroupId == u.UserGroupId).FirstOrDefault();
                                if (existingUserGroupRole == null)
                                {

                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {
                                        RoleId = u.RoleId,
                                        UserGroupId = u.UserGroupId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = value.FolderID,
                                        IsFolderLevel = value.IsFolderLevel
                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();

                                }
                            });
                        }
                    });
                }
            }
            else
            {
                if (documentFolders.Count != 0 && documentFolders != null)
                {
                    documentFolders.ForEach(doc =>
                    {
                        if (FolderRoles.Count != 0 && FolderRoles != null)
                        {
                            FolderRoles.ForEach(u =>
                            {
                                var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserId == u.UserId).FirstOrDefault();
                                if (existingDocumentRole == null)
                                {

                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {
                                        UserId = u.UserId,
                                        RoleId = u.RoleId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = value.FolderID,
                                        IsFolderLevel = value.IsFolderLevel
                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();
                                }
                                var existingUserGroupRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserGroupId == u.UserGroupId).FirstOrDefault();
                                if (existingUserGroupRole == null)
                                {
                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {
                                        RoleId = u.RoleId,
                                        UserGroupId = u.UserGroupId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = value.FolderID,
                                        IsFolderLevel = value.IsFolderLevel
                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();


                                }
                            });
                        }

                    });

                }

            }

            if (value.ParentFolderID == null && value.MainFolderID == null)
            {
                // to build tree view with master FolderID..
                var mainfolder = _context.Folders.FirstOrDefault(id => id.FolderId == folder.FolderId);
                mainfolder.MainFolderId = folder.FolderId;
                _context.SaveChanges();

                value.MainFolderID = mainfolder.MainFolderId;
            }
            //}
            //else
            //{
            //    throw new Exception("Folder Name " + value.Name + " Already Exist! Please Create New Different folder Name ");
            //}
            return value;
        }


        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteFolders")]
        public ActionResult Delete(int id)
        {
            //try
            //{
            List<Documents> document = new List<Documents>();
            var folders = _context.Folders.SingleOrDefault(p => p.FolderId == id);
            if (folders != null)
            {
                if (folders.SessionId != null)
                {
                    document = _context.Documents.Select(s => new Documents
                    {
                        DocumentId = s.DocumentId,
                        IsTemp = s.IsTemp,
                        Description = s.Description,
                        SessionId = s.SessionId,
                    }).Where(d => d.SessionId == folders.SessionId).ToList();
                }

                var subFolders = _context.Folders.Where(p => p.ParentFolderId == id);
                if (subFolders.Any())
                {
                    foreach (var subFolder in subFolders)
                    {
                        if (subFolder.SessionId != null)
                        {
                            var subFolderDocuments = _context.Documents.Select(s => new Documents
                            {
                                DocumentId = s.DocumentId,
                                IsTemp = s.IsTemp,
                                Description = s.Description,
                                SessionId = s.SessionId,
                            }).Where(d => d.SessionId == subFolder.SessionId).ToList();

                            if (subFolderDocuments.Count == 0)
                            {
                                var documentUserRolefolder = _context.DocumentUserRole.Where(d => d.FolderId != null && d.FolderId.Value == subFolder.FolderId).ToList();
                                if (documentUserRolefolder.Count > 0)
                                {
                                    _context.DocumentUserRole.RemoveRange(documentUserRolefolder);
                                    _context.SaveChanges();
                                }

                                var parentfolder = _context.Folders.Where(d => d.ParentFolderId != null && d.ParentFolderId == subFolder.FolderId).ToList();
                                if (parentfolder.Count > 0)
                                {
                                    _context.Folders.RemoveRange(parentfolder);
                                    _context.SaveChanges();
                                }

                                _context.Folders.RemoveRange(subFolders);
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                if (folders != null)
                {
                    if (folders.FolderId > 0 && document.Count == 0)
                    {
                        var documentFolder = _context.DocumentFolder.Where(d => d.FolderId == folders.FolderId).ToList();
                        if (documentFolder.Count > 0)
                        {
                            _context.DocumentFolder.RemoveRange(documentFolder);
                            _context.SaveChanges();
                        }

                        var docDiscussionuser = _context.FolderDiscussionUser.Where(d => d.FolderId == folders.FolderId).ToList();
                        if (docDiscussionuser.Count > 0)
                        {
                            _context.FolderDiscussionUser.RemoveRange(docDiscussionuser);
                            _context.SaveChanges();
                        }

                        var docDiscussion = _context.FolderDiscussion.Where(d => d.FolderId == folders.FolderId).ToList();
                        if (docDiscussion.Count > 0)
                        {
                            _context.FolderDiscussion.RemoveRange(docDiscussion);
                            _context.SaveChanges();
                        }

                        var documentUserRolefolder = _context.DocumentUserRole.Where(d => d.FolderId == folders.FolderId).ToList();
                        if (documentUserRolefolder.Count > 0)
                        {
                            _context.DocumentUserRole.RemoveRange(documentUserRolefolder);
                            _context.SaveChanges();
                        }

                        var parentfolder = _context.Folders.Where(d => d.ParentFolderId == folders.FolderId).ToList();
                        if (parentfolder.Count > 0)
                        {
                            throw new AppException("There is an Sub Folder inside folder cannot be deleted!", null);
                        }
                        else
                        {
                            _context.Folders.RemoveRange(parentfolder);
                            _context.SaveChanges();
                        }

                        _context.Folders.Remove(folders);
                        _context.SaveChanges();

                        return Ok("Record Deleted.");
                    }
                }
                throw new AppException("There is file inside, folder cannot be deleted!", null);
            }
            return Ok("Record Deleted.");
        }

        [HttpDelete]
        [Route("DeleteSubFolder")]
        public void DeleteSubFolder(int id)
        {
            var folders = _context.Folders.SingleOrDefault(t => t.FolderId == id);
            if (folders != null)
            {
                if (folders.FolderId > 0)
                {
                    var documentfolder = _context.DocumentFolder.Where(d => d.FolderId == folders.FolderId).ToList();
                    if (documentfolder.Count > 0)
                    {
                        _context.DocumentFolder.RemoveRange(documentfolder);
                        _context.SaveChanges();
                    }
                    //var parentfolder = _context.Folders.Where(d => d.ParentFolderId == folders.FolderId).ToList();
                    //if (parentfolder.Count > 0)
                    //{
                    //    _context.Folders.RemoveRange(parentfolder);
                    //    _context.SaveChanges();
                    //}
                    //_context.Folders.Remove(folders);

                    //var document = _context.Documents.Where(d => d.SessionId == folders.SessionId).ToList();
                    //if (document.Count > 0)
                    //{
                    //    _context.Documents.RemoveRange(document);
                    //    _context.SaveChanges();
                    //}
                    _context.Folders.Remove(folders);
                    _context.SaveChanges();

                }
            }
        }

        [HttpPut]
        [Route("CheckOutDoc")]
        public DocumentFolderModel CheckOutDoc(DocumentFolderModel value)
        {
            if (value.DocumentFolderID > 0)
            {
                var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentFolderId == value.DocumentFolderID);
                //var attachments = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
                //var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();

                if (attachment != null)
                {
                    attachment.IsLocked = true;
                    attachment.LockedDate = DateTime.Now;
                    attachment.LockedByUserId = value.AddedByUserID;

                    var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DocumentId == attachment.DocumentId);
                    if (folderDiscussion != null)
                    {
                        var folderDiscussionAttachment = _context.FolderDiscussionAttachment.SingleOrDefault(p => p.FolderDiscussionId == folderDiscussion.DiscussionNotesId);
                        if (folderDiscussionAttachment != null)
                        {
                            _context.FolderDiscussionAttachment.Remove(folderDiscussionAttachment);
                            _context.SaveChanges();
                        }

                        var folderDiscussionUser = _context.FolderDiscussionUser.SingleOrDefault(p => p.FolderDiscussionNotesId == folderDiscussion.DiscussionNotesId);
                        if (folderDiscussionUser != null)
                        {
                            _context.FolderDiscussionUser.Remove(folderDiscussionUser);
                            _context.SaveChanges();
                        }

                        _context.FolderDiscussion.Remove(folderDiscussion);
                        _context.SaveChanges();
                    }

                    //_context.FolderDiscussionAttachment
                    //_context.FolderDiscussionUser
                }



                else if (attachment.UploadedByUserId != value.AddedByUserID)
                {
                    throw new AppException("Required permission to read this file.!", null);
                }
                else
                {
                    if (attachment != null)
                    {
                        attachment.IsLocked = true;
                        attachment.LockedDate = DateTime.Now;
                        attachment.LockedByUserId = value.AddedByUserID;
                        var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DocumentId == attachment.DocumentId);
                        if (folderDiscussion != null)
                        {
                            _context.FolderDiscussion.Remove(folderDiscussion);
                            _context.SaveChanges();
                        }
                    }
                }
                var taskattachment = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == attachment.DocumentId);
                if (taskattachment != null)
                {
                    var isfromCreateTask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskattachment.TaskMasterId && t.IsFromProfileDocument != null)?.IsFromProfileDocument;
                    if (isfromCreateTask != null && isfromCreateTask == true)
                    {
                        taskattachment.IsLocked = true;
                        taskattachment.LockedDate = DateTime.Now;
                        taskattachment.LockedByUserId = value.AddedByUserID;

                    }
                }


                _context.SaveChanges();
            }
            return value;
        }

        [HttpPut]
        [Route("CheckInDoc")]
        public DocumentFolderModel CheckInDoc(DocumentFolderModel value)
        {
            //try
            //{ 
            //var doclist = _context.Documents.AsNoTracking().ToList();
            if (value.DocumentFolderID > 0)
            {
                double draftingVersionNo = 0;
                double versionNo = 0;
                var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentFolderId == value.DocumentFolderID);


                if (!value.IsNoChange.GetValueOrDefault(false))
                {
                    if (attachment != null)
                    {
                        attachment.IsLocked = true;
                        attachment.LockedDate = DateTime.Now;
                        attachment.LockedByUserId = value.AddedByUserID;
                        attachment.IsLatest = false;
                        attachment.IsNoChange = value.IsNoChange;
                    }
                    if (attachment.VersionNo != null)
                    {
                        versionNo = double.Parse(attachment.VersionNo) + 0.1;
                    }
                    if (value.IsOverwrite == false && attachment.VersionNo != null)
                    {
                        versionNo = double.Parse(attachment.VersionNo) + 0.1;
                    }
                    else
                    {
                        if (attachment.VersionNo != null)
                        {
                            versionNo = double.Parse(attachment.VersionNo);
                        }
                    }
                    if (value.IsMajorChange.GetValueOrDefault(false) && value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {
                        var number = attachment.VersionNo.Split('.')[0];
                        if (value.IsOverwrite != true)
                        {
                            versionNo = double.Parse(number) + 1;

                        }
                        if (attachment.DraftingVersionNo != null)
                        {
                            draftingVersionNo = 0;
                        }
                    }
                    else if (value.IsMajorChange.GetValueOrDefault(false) && !value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {
                        if (attachment.DraftingVersionNo != null)
                        {
                            var number = attachment.DraftingVersionNo != null ? attachment.DraftingVersionNo.Split('.')[0] : string.Empty;
                            draftingVersionNo = number != string.Empty ? double.Parse(number) + 1 : 0;
                        }
                        else
                        {
                            draftingVersionNo = 1;
                        }
                        versionNo = double.Parse(attachment.VersionNo);
                    }
                    else if (!value.IsMajorChange.GetValueOrDefault(false) && !value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {

                        if (attachment.DraftingVersionNo != null)
                        {
                            draftingVersionNo = 0;
                            versionNo = !string.IsNullOrEmpty(attachment.VersionNo) ? double.Parse(attachment.VersionNo) : 0;


                        }
                        else
                        {
                            //var number = attachment.DraftingVersionNo.Split('.')[0];
                            draftingVersionNo = 0;
                            versionNo = !string.IsNullOrEmpty(attachment.VersionNo) ? double.Parse(attachment.VersionNo) : 0;
                        }

                        // draftingVersionNo = 0;
                    }
                    else if (value.IsOverwrite == true && value.IsReleaseVersion == true)
                    {
                        if (value.OverwriteVersionNo != null)
                        {
                            versionNo = !string.IsNullOrEmpty(value.OverwriteVersionNo) ? double.Parse(value.OverwriteVersionNo) : 0;
                        }

                    }
                    else if (value.IsOverwrite == true && value.IsReleaseVersion == false)
                    {
                        if (value.OverwriteVersionNo != null && value.OverwriteVersionNo != "")
                        {
                            draftingVersionNo = double.Parse(value.OverwriteVersionNo);
                        }
                        else if (attachment.DraftingVersionNo != null)
                        {
                            draftingVersionNo = double.Parse(attachment.DraftingVersionNo);
                        }

                    }
                }
                else
                {
                    attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentFolderId == value.DocumentFolderID);
                    if (attachment != null)
                    {
                        attachment.IsLatest = true;
                        attachment.IsLocked = false;
                        attachment.IsNoChange = value.IsNoChange;
                    }


                }
                if (value.SessionID != null)
                {
                    versionNo = Math.Round(versionNo, 2);
                    //var docu = _context.Documents.FirstOrDefault(d => d.SessionId == value.SessionID);
                    var documents = _context.Documents
                        .Select(s => new Documents
                        {
                            DocumentId = s.DocumentId,
                            IsTemp = s.IsTemp,
                            Description = s.Description,
                            SessionId = s.SessionId
                        }).Where(d => d.SessionId == value.SessionID).ToList();
                    if (documents != null && documents.Count > 0)
                    {
                        documents.ForEach(file =>
                        {
                            file.IsTemp = false;
                            file.Description = value.Description;
                            DocumentFolder folderAttach = new DocumentFolder();
                            folderAttach.CheckInDescription = value.Description;
                            folderAttach.DocumentId = file.DocumentId;
                            folderAttach.FolderId = attachment?.FolderId;
                            folderAttach.IsLatest = true;
                            folderAttach.IsLocked = false;
                            folderAttach.IsMajorChange = value.IsMajorChange;
                            folderAttach.UploadedDate = DateTime.Now;
                            folderAttach.UploadedByUserId = value.AddedByUserID;
                            folderAttach.VersionNo = versionNo.ToString().Trim();
                            folderAttach.DraftingVersionNo = draftingVersionNo == 0 && value.IsReleaseVersion == true ? null : draftingVersionNo.ToString().Trim();
                            folderAttach.ActualVersionNo = value.ActualVersionNo?.Trim();
                            folderAttach.PreviousDocumentId = attachment?.PreviousDocumentId == null ? attachment?.DocumentFolderId : attachment?.PreviousDocumentId;
                            folderAttach.IsReleaseVersion = value.IsReleaseVersion;
                            folderAttach.IsNoChange = value.IsNoChange;

                            _context.DocumentFolder.Add(folderAttach);

                            _context.SaveChanges();

                            var documentRights = _context.DocumentRights.Where(u => u.DocumentId == attachment.DocumentId).AsNoTracking().ToList();
                            if (documentRights != null && documentRights.Count > 0)
                            {
                                documentRights.ForEach(dr =>
                                {
                                    var docRights = new DocumentRights
                                    {
                                        DocumentId = dr.DocumentId,
                                        IsRead = dr.IsRead,
                                        IsReadWrite = dr.IsReadWrite,
                                        UserId = dr.UserId,
                                    };
                                    file.DocumentRights.Add(docRights);
                                });
                            }

                        });
                    }
                }
                var documentlinklist = _context.DocumentLink.Where(d => d.LinkDocumentId == attachment.DocumentId).AsNoTracking().ToList();
                if (documentlinklist != null && documentlinklist.Count > 0)
                {
                    var docu = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FilterProfileTypeId }).FirstOrDefault(d => d.SessionId == value.SessionID);
                    documentlinklist.ForEach(d =>
                    {
                        var doclinkexist = _context.DocumentLink.Where(r => r.LinkDocumentId == d.LinkDocumentId)?.FirstOrDefault();
                        if (doclinkexist != null)
                        {
                            var linkparentdocId = doclinkexist.DocumentId;
                            _context.DocumentLink.Remove(doclinkexist);
                            _context.SaveChanges();


                            var DocumentLink = new DocumentLink
                            {
                                DocumentId = linkparentdocId,
                                AddedByUserId = value.AddedByUserID.Value,
                                AddedDate = DateTime.Now,
                                StatusCodeId = 1,
                                LinkDocumentId = docu.DocumentId,
                                FolderId = attachment?.FolderId,
                                FileProfieTypeId = docu.FilterProfileTypeId,
                                DocumentPath = "Document Link From Task checkin",

                            };
                            _context.DocumentLink.Add(DocumentLink);
                            _context.SaveChanges();
                        }

                    });

                }
                var taskId = _context.Documents.Select(s => new { s.DocumentId, s.TaskId }).Where(w => w.DocumentId == attachment.DocumentId).FirstOrDefault()?.TaskId;
                if (taskId != null)
                {
                    var taskName = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskId)?.Title;
                    var taskAttachmentLatest = _context.TaskAttachment.Where(w => w.DocumentId == attachment.DocumentId && w.TaskMasterId == taskId && w.IsLatest == true).FirstOrDefault();
                    if (taskAttachmentLatest != null)
                    {
                        var docu = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FilterProfileTypeId, s.FileName }).FirstOrDefault(d => d.SessionId == value.SessionID);
                        var query = string.Format("Update TaskAttachment Set IsLatest={0} Where TaskAttachmentId= {1}", 0, taskAttachmentLatest.TaskAttachmentId);
                        var rowaffected = _context.Database.ExecuteSqlRaw(query);
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == attachment.DocumentId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            var checkin = "Link From Task CheckIn";
                            var query1 = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}, DocumentPath='{4}'  Where DocumentId= {0}", attachment.DocumentId, docu.DocumentId, DateTime.Now, value.AddedByUserID, checkin);
                            var rowaffected1 = _context.Database.ExecuteSqlRaw(query1);
                            if (rowaffected <= 0)
                            {
                                throw new Exception("Failed to update document");
                            }

                        }
                        var taskAttach = new TaskAttachment
                        {
                            TaskMasterId = taskId,
                            DocumentId = docu?.DocumentId,
                            PreviousDocumentId = taskAttachmentLatest.PreviousDocumentId == null ? taskAttachmentLatest.TaskAttachmentId : taskAttachmentLatest.PreviousDocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            VersionNo = "1",
                            IsMajorChange = false,
                            UploadedByUserId = value.AddedByUserID,
                            UploadedDate = DateTime.Now,
                            IsMeetingNotes = false,
                            IsDiscussionNotes = false,
                            IsSubTask = false

                        };
                        _context.TaskAttachment.Add(taskAttach);
                        _context.SaveChanges();
                        var notes = new Notes
                        {
                            Notes1 = taskName,
                            Link = value.Link + "id=" + taskId + "&taskid=" + taskId,
                            DocumentId = docu?.DocumentId,
                            SessionId = value.SessionID,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,


                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();
                    }
                }
                else
                {
                    var taskAttachmentLatest = _context.TaskAttachment.Where(w => w.DocumentId == attachment.DocumentId && w.IsLatest == true).FirstOrDefault();
                    if (taskAttachmentLatest != null)
                    {
                        var docu = _context.Documents.Select(s => new { s.SessionId, s.DocumentId, s.FilterProfileTypeId }).FirstOrDefault(d => d.SessionId == value.SessionID);
                        var query = string.Format("Update TaskAttachment Set IsLatest={0} Where TaskAttachmentId= {1}", 0, taskAttachmentLatest.TaskAttachmentId);
                        var rowaffected = _context.Database.ExecuteSqlRaw(query);
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == attachment.DocumentId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            var checkin = "Link From Task CheckIn";
                            var query1 = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}, DocumentPath='{4}'  Where DocumentId= {0}", attachment.DocumentId, docu.DocumentId, DateTime.Now, value.AddedByUserID, checkin);
                            var rowaffected1 = _context.Database.ExecuteSqlRaw(query1);
                            if (rowaffected <= 0)
                            {
                                throw new Exception("Failed to update document");
                            }

                        }
                        var taskAttach = new TaskAttachment
                        {
                            TaskMasterId = taskAttachmentLatest.TaskMasterId,
                            DocumentId = docu?.DocumentId,
                            PreviousDocumentId = taskAttachmentLatest.PreviousDocumentId == null ? taskAttachmentLatest.TaskAttachmentId : taskAttachmentLatest.PreviousDocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            VersionNo = "1",
                            IsMajorChange = false,
                            UploadedByUserId = value.AddedByUserID,
                            UploadedDate = DateTime.Now,
                            IsMeetingNotes = false,
                            IsDiscussionNotes = false,
                            IsSubTask = false

                        };
                        _context.TaskAttachment.Add(taskAttach);
                        _context.SaveChanges();
                        var docufileName = _context.Documents.FirstOrDefault(d => d.DocumentId == attachment.DocumentId)?.FileName;
                        var taskName = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskAttachmentLatest.TaskMasterId)?.Title;
                        var notes = new Notes
                        {
                            Notes1 = taskName,
                            Link = value.Link + "id=" + taskAttachmentLatest.TaskMasterId + "&taskid=" + taskAttachmentLatest.TaskMasterId,
                            DocumentId = docu?.DocumentId,
                            SessionId = value.SessionID,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,


                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();
                    }
                }
                var documentSess = _context.Documents
                       .Select(s => new Documents
                       {
                           DocumentId = s.DocumentId,
                           IsTemp = s.IsTemp,
                           Description = s.Description,
                           SessionId = s.SessionId
                       }).Where(d => d.SessionId == value.SessionID).FirstOrDefault();
                if (documentSess != null)
                {
                    var query = string.Format("Update LinkFileProfileTypeDocument Set DocumentID={0} Where DocumentID= {1}", documentSess.DocumentId, attachment.DocumentId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);

                }
                _context.SaveChanges();
            }

            //}
            //catch (Exception ex)
            //{



            //}
            return value;
        }

        [HttpPut]
        [Route("FileUploadDoc")]
        public DocumentFolderModel FileUploadDoc(DocumentFolderModel value)
        {
            if (value.DocumentFolderID > 0)
            {
                //var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentFolderId == value.DocumentFolderID);
                //if (attachment != null)
                //{
                //    attachment.IsLocked = true;
                //    attachment.LockedDate = DateTime.Now;
                //    attachment.LockedByUserId = value.AddedByUserID;
                //    attachment.IsLatest = false;
                //}

                //var version = value.IsMajorChange == true ? (double.Parse(attachment.VersionNo) + 1) : double.Parse(attachment.VersionNo) + 0.1;
                if (value.SessionID != null)
                {
                    var docu = _context.Documents.FirstOrDefault(d => d.SessionId == value.SessionID);
                    if (docu != null)
                    {
                        docu.IsTemp = false;
                        if (docu != null)
                        {
                            var folderAttach = new DocumentFolder
                            {
                                DocumentId = docu.DocumentId,
                                //FolderId = attachment.FolderId,
                                FolderId = value.FolderID,
                                IsLatest = true,
                                IsLocked = false,
                                IsMajorChange = value.IsMajorChange,
                                IsReleaseVersion = value.IsReleaseVersion,
                                IsNoChange = value.IsNoChange,
                                UploadedDate = DateTime.Now,
                                UploadedByUserId = value.AddedByUserID,
                                VersionNo = "1.0",
                                PreviousDocumentId = null,
                                //VersionNo = version.ToString(),
                                //PreviousDocumentId = attachment.PreviousDocumentId == null ? attachment.DocumentFolderId : attachment.PreviousDocumentId,
                            };
                            _context.DocumentFolder.Add(folderAttach);

                            if (value.ReadonlyUserID != null && value.ReadonlyUserID.Count > 0)
                            {
                                value.ReadonlyUserID.ForEach(id =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = true,
                                        IsReadWrite = false,
                                        UserId = id,
                                    };
                                    docu.DocumentRights.Add(docAccess);
                                });
                            }

                            if (value.ReadWriteUserID != null && value.ReadWriteUserID.Count > 0)
                            {
                                value.ReadWriteUserID.ForEach(id =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = false,
                                        IsReadWrite = true,
                                        UserId = id,
                                    };
                                    docu.DocumentRights.Add(docAccess);
                                });
                            }
                        }
                    }
                }
                _context.SaveChanges();
            }
            return value;
        }

        [HttpGet]
        [Route("GetAccessControl")]
        public List<DocumentRightsModel> GetAccessControl(int id)
        {
            var accessControl = _context.DocumentRights.Where(d => d.DocumentId == id).AsNoTracking().Select(s => new DocumentRightsModel
            {
                DocumentID = s.DocumentId,
                DocumentAccessID = s.DocumentAccessId,
                UserID = s.UserId,
                IsReadWrite = s.IsReadWrite,
                IsRead = s.IsRead,

            }).OrderByDescending(o => o.DocumentID).ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return accessControl;

        }

        [HttpGet]
        [Route("GetDocumentPermissionControl")]
        public DocumentPermissionModel GetDocumentPermissionControl(int id)
        {

            var documentUserRole = _context.DocumentUserRole.FirstOrDefault(p => p.UserId == id);
            if (documentUserRole == null)
            {
                var documentpermissionUserGroup = _context.UserGroupUser.FirstOrDefault(p => p.UserId == id);

                if (documentpermissionUserGroup != null)
                {
                    documentUserRole = _context.DocumentUserRole.FirstOrDefault(p => p.UserGroupId == documentpermissionUserGroup.UserGroupId);
                    if (documentUserRole != null)
                    {
                        var documentPermission = _context.DocumentPermission.Where(a => a.DocumentRoleId == documentUserRole.RoleId).Select(b => new DocumentPermissionModel
                        {
                            DocumentPermissionID = b.DocumentPermissionId,
                            IsRead = b.IsRead,
                            IsCreateFolder = b.IsCreateFolder,
                            IsCreateDocument = b.IsCreateDocument,
                            IsSetAlert = b.IsSetAlert,
                            IsEditIndex = b.IsEditIndex,
                            IsRename = b.IsRename,
                            IsUpdateDocument = b.IsUpdateDocument,
                            IsCopy = b.IsCopy,
                            IsMove = b.IsMove,
                            IsDelete = b.IsDelete,
                            IsRelationship = b.IsRelationship,
                            IsListVersion = b.IsListVersion,
                            IsInvitation = b.IsInvitation,
                            IsSendEmail = b.IsSendEmail,
                            IsDiscussion = b.IsDiscussion,
                            IsAccessControl = b.IsAccessControl,
                            IsAuditTrail = b.IsAuditTrail,
                            IsEdit = b.IsEdit,
                            IsFileDelete = b.IsFileDelete,
                            IsGrantAdminPermission = b.IsGrantAdminPermission,
                            //DocumentUserRole Details
                            DocumentUserRoleID = documentUserRole.DocumentUserRoleId,
                            UserID = documentUserRole.UserId,
                            RoleID = documentUserRole.RoleId,
                            UserGroupID = documentUserRole.UserGroupId,
                            FolderID = documentUserRole.FolderId,
                            DocumentID = documentUserRole.DocumentId

                        }).OrderByDescending(a => a.RoleID).FirstOrDefault();
                        return documentPermission;
                    }
                }
            }
            else
            {
                var documentPermission = _context.DocumentPermission.Where(a => a.DocumentRoleId == documentUserRole.RoleId).Select(b => new DocumentPermissionModel
                {
                    DocumentPermissionID = b.DocumentPermissionId,
                    IsRead = b.IsRead,
                    IsCreateFolder = b.IsCreateFolder,
                    IsCreateDocument = b.IsCreateDocument,
                    IsSetAlert = b.IsSetAlert,
                    IsEditIndex = b.IsEditIndex,
                    IsRename = b.IsRename,
                    IsUpdateDocument = b.IsUpdateDocument,
                    IsCopy = b.IsCopy,
                    IsMove = b.IsMove,
                    IsDelete = b.IsDelete,
                    IsRelationship = b.IsRelationship,
                    IsListVersion = b.IsListVersion,
                    IsInvitation = b.IsInvitation,
                    IsSendEmail = b.IsSendEmail,
                    IsDiscussion = b.IsDiscussion,
                    IsAccessControl = b.IsAccessControl,
                    IsAuditTrail = b.IsAuditTrail,
                    IsEdit = b.IsEdit,
                    IsFileDelete = b.IsFileDelete,
                    IsGrantAdminPermission = b.IsGrantAdminPermission,
                    //DocumentUserRole Details
                    DocumentUserRoleID = documentUserRole.DocumentUserRoleId,
                    UserID = documentUserRole.UserId,
                    RoleID = documentUserRole.RoleId,
                    UserGroupID = documentUserRole.UserGroupId,
                    FolderID = documentUserRole.FolderId,
                    DocumentID = documentUserRole.DocumentId

                }).OrderByDescending(a => a.RoleID).FirstOrDefault();
                return documentPermission;
            }

            return new DocumentPermissionModel();
        }

        [HttpGet]
        [Route("GetInvitationControl")]
        public List<DocumentRightsModel> GetInvitationControl(int id)
        {
            var invitationControl = _context.DocumentRights.Where(d => d.DocumentId == id).AsNoTracking().Select(s => new DocumentRightsModel
            {
                DocumentID = s.DocumentId,
                DocumentAccessID = s.DocumentAccessId,
                UserID = s.UserId,
                IsReadWrite = s.IsReadWrite,
                IsRead = s.IsRead,

            }).ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return invitationControl;

        }
        // PUT: api/User/5
        [HttpPut]
        [Route("SetDocAccess")]
        public DocumentFolderModel SetDocAccess(DocumentFolderModel value)
        {
            if (value.DocumentID != 0)
            {
                var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
                var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (fileaccess.Count() > 0)
                {
                    var rightss = fileaccess.Where(a => a.UserId == value.AddedByUserID);
                    if (rightss.Count() > 0)
                    {
                        if (attachment.UploadedByUserId != value.AddedByUserID)
                        {
                            throw new AppException("Required permission to Document Access Control this file.!", null);
                        }
                    }
                }
                var rights = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();

                if (rights.Count() > 0)
                {
                    _context.DocumentRights.RemoveRange(rights);
                    _context.SaveChanges();
                }

                var file = _context.Documents.FirstOrDefault(d => d.DocumentId == value.DocumentID);

                if (value.ReadonlyUserID != null)
                {
                    value.ReadonlyUserID.ForEach(i =>
                    {
                        var exist = _context.DocumentRights.FirstOrDefault(d => d.DocumentId == value.DocumentID && d.UserId == i);
                        if (exist == null)
                        {
                            var docAccess = new DocumentRights
                            {
                                DocumentId = value.DocumentID,
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        }
                    });
                }
                if (value.ReadWriteUserID != null)
                {
                    value.ReadWriteUserID.ForEach(i =>
                    {
                        var exist = _context.DocumentRights.FirstOrDefault(d => d.DocumentId == value.DocumentID && d.UserId == i);
                        if (exist == null)
                        {
                            var docAccess = new DocumentRights
                            {
                                DocumentId = value.DocumentID,
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        }
                    });
                }

            }
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("SetDocInvitation")]
        public DocumentFolderModel SetDocInvitation(DocumentFolderModel value)
        {
            if (value.DocumentID != 0)
            {
                var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
                var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (fileaccess.Count() > 0)
                {
                    var rightss = fileaccess.Where(a => a.UserId == value.AddedByUserID);
                    if (rightss.Count() > 0)
                    {
                        if (attachment.UploadedByUserId != value.AddedByUserID)
                        {
                            throw new AppException("Required permission to Document Invitation this file.!", null);
                        }
                    }
                }

                var rights = fileaccess.Where(d => d.DocumentId == value.DocumentID).ToList();

                if (rights.Count() > 0)
                {
                    _context.DocumentRights.RemoveRange(rights);
                    _context.SaveChanges();
                }

                var file = _context.Documents.FirstOrDefault(d => d.DocumentId == value.DocumentID);

                if (value.ReadonlyUserID != null)
                {
                    value.ReadonlyUserID.ForEach(i =>
                    {
                        var docAccess = new DocumentRights
                        {
                            DocumentId = value.DocumentID,
                            IsRead = true,
                            IsReadWrite = false,
                            UserId = i,
                        };
                        file.DocumentRights.Add(docAccess);
                    });
                }
                if (value.ReadWriteUserID != null)
                {
                    value.ReadWriteUserID.ForEach(i =>
                    {
                        var docAccess = new DocumentRights
                        {
                            DocumentId = value.DocumentID,
                            IsRead = false,
                            IsReadWrite = true,
                            UserId = i,
                        };
                        file.DocumentRights.Add(docAccess);
                    });
                }
            }
            _context.SaveChanges();
            return value;
        }

        [HttpGet]
        [Route("GetFilesHistory")]
        public List<DocumentsModel> GetFilesHistory(int id)
        {
            //var folderIds = _context.DocumentFolder.Where(i => i.PreviousDocumentId == id).AsNoTracking().Select(s => s.DocumentFolderId).ToList();
            //folderIds.Add(id);
            var applicationUserList = _context.ApplicationUser.AsNoTracking().ToList();
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var documentFolder = _context.DocumentFolder.Include(I => I.Folder).Where(d => d.PreviousDocumentId == id && d.IsLatest == false || (d.DocumentFolderId == id && d.PreviousDocumentId == null)).OrderByDescending(d => d.DocumentFolderId).AsNoTracking().ToList();
            if (documentFolder != null && documentFolder.Count > 0)
            {
                var documentIds = documentFolder.Select(d => d.DocumentId).ToList();
                var documents = _context.Documents.Select(s => new
                {
                    s.DocumentType,
                    s.CategoryId,
                    s.ContentType,
                    s.DepartmentId,
                    s.DisplayName,
                    s.DocumentId,
                    s.Extension,
                    s.FileName,
                    s.FileSize,
                    s.ReferenceNumber,
                    s.SessionId,
                    s.DocumentRights,
                }).Where(d => documentIds.Contains(d.DocumentId)).ToList();

                documentFolder.ForEach(s =>
                {
                    var document = documents.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                    DocumentsModel documentsModel = new DocumentsModel();
                    {
                        documentsModel.DocumentType = document?.DocumentType;
                        documentsModel.CategoryID = document?.CategoryId;
                        documentsModel.ContentType = document?.ContentType;
                        documentsModel.DepartmentID = document?.DepartmentId;
                        documentsModel.Description = s.CheckInDescription;
                        documentsModel.DisplayName = document?.DisplayName;
                        documentsModel.DocumentID = document != null ? document.DocumentId : new long();
                        documentsModel.Extension = document?.Extension;
                        documentsModel.FileName = document?.FileName;
                        documentsModel.FileSize = document != null && document.FileSize != null ? (long)Math.Round(Convert.ToDouble(document.FileSize / 1024)) : 0;
                        documentsModel.LinkedFolder = s.Folder?.Name;
                        documentsModel.LinkID = s.FolderId;
                        documentsModel.ReferenceNumber = document?.ReferenceNumber;
                        documentsModel.SessionID = document?.SessionId;
                        documentsModel.UploadDate = s.UploadedDate;
                        documentsModel.DocumentFolderId = s.DocumentFolderId;
                        documentsModel.VersionNo = s.VersionNo;
                        documentsModel.DraftingVersionNo = s.DraftingVersionNo;
                        documentsModel.IsLocked = s.IsLocked;
                        documentsModel.IsMajorChange = s.IsMajorChange;
                        documentsModel.IsReleaseVersion = s.IsReleaseVersion;
                        documentsModel.IsNoChange = s.IsNoChange;
                        documentsModel.LockedByUserId = s.LockedByUserId;
                        documentsModel.LockedDate = s.LockedDate;
                        documentsModel.IsMeetingNotes = s.IsMeetingNotes;
                        documentsModel.IsDiscussionNotes = s.IsDiscussionNotes;
                        //IsSubTask = s.IsSubTask,
                        documentsModel.PreviousDocumentId = s.PreviousDocumentId;
                        documentsModel.UploadedByUser = applicationUserList != null && s.UploadedByUserId != null ? applicationUserList.FirstOrDefault(f => f.UserId == s.UploadedByUserId).UserName : string.Empty;
                        documentsModel.LockedByUser = applicationUserList != null && s.LockedByUserId != null ? applicationUserList.FirstOrDefault(f => f.UserId == s.LockedByUserId).UserName : string.Empty;
                        documentsModel.UploadedByUserId = s.UploadedByUserId;
                        //UploadedByUser = s.UploadedByUser.UserName,
                        documentsModel.ReadOnlyUserId = document.DocumentRights?.Where(w => w.IsRead == true).Select(r => r.UserId.Value).ToList();
                        documentsModel.ReadWriteUserId = document.DocumentRights?.Where(w => w.IsReadWrite == true).Select(r => r.UserId.Value).ToList();

                    };
                    documentsModels.Add(documentsModel);
                });

            }
            return documentsModels.OrderByDescending(s => s.DocumentFolderId).ToList();
        }

        [HttpPut]
        [Route("UpdateRenameFileName")]
        public DocumentsModel UpdateRenameFileName(DocumentsModel value)
        {
            var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
            var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
            if (fileaccess.Count() > 0)
            {
                var rightss = fileaccess.Where(a => a.UserId == value.AddedByUserID);
                if (rightss.Count() > 0)
                {
                    if (attachment.UploadedByUserId != value.AddedByUserID)
                    {
                        throw new AppException("Required permission to Rename this file.!", null);
                    }
                }
            }

            //var documentFolder = _context.Documents.SingleOrDefault(p => p.DocumentId == value.DocumentID);
            //var folderIds = _context.DocumentFolder.Where(i => i.FolderId == id).Select(s => s.DocumentFolderId).ToList();
            //folderIds.Add(id);
            // documentFolder.DocumentId = value.DocumentID;
            //documentFolder.DepartmentId = value.DepartmentID;
            //documentFolder.WikiId = value.WikiID;
            //documentFolder.CategoryId = value.CategoryID;
            //documentFolder.FileName = value.FileName;
            //documentFolder.DisplayName = value.DisplayName;
            //documentFolder.Extension = value.Extension;
            //documentFolder.ContentType = value.ContentType;
            //documentFolder.DocumentType = value.DocumentType;
            //documentFolder.FileSize = value.FileSize;
            //documentFolder.UploadDate = DateTime.Now;
            //documentFolder.LinkId = value.LinkID;
            //documentFolder.SessionId = value.SessionID;
            //documentFolder.IsSpecialFile = value.IsSpecialFile;
            //documentFolder.IsTemp = true;

            //Changes Done by Aravinth
            //documentFolder.ReferenceNumber = value.ReferenceNumber;
            //documentFolder.Description = value.Description;
            //documentFolder.AddedByUserId = value.AddedByUserID;
            //documentFolder.AddedDate = DateTime.Now;
            //documentFolder.ModifiedByUserId = value.ModifiedByUserID;
            //documentFolder.StatusCodeId = value.StatusCodeID.Value;
            //documentFolder.ModifiedDate = DateTime.Now;

            // _context.SaveChanges();
            //value.DocumentID = documentFolder.DocumentId;


            var query = string.Format("Update Documents Set FileName='{1}', ContentType='{2}', FileSize='{3}', UploadDate={4:d}, ModifiedByUserId={5}, ModifiedDate={4:d}  Where DocumentId= {0}", value.DocumentID, value.FileName, value.ContentType, value.FileSize, DateTime.Now, value.ModifiedByUserID);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }
            return value;
        }

        [HttpPut]
        [Route("UpdateMoveFolder")]
        public DocumentsModel UpdateMoveFolder(DocumentsModel value)
        {
            var attachment = _context.DocumentFolder.FirstOrDefault(l => l.DocumentId == value.DocumentID);
            var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
            if (fileaccess.Count() > 0)
            {
                var rightss = fileaccess.Where(a => a.UserId == value.AddedByUserID);
                if (rightss.Count() > 0)
                {
                    if (attachment.UploadedByUserId != value.AddedByUserID)
                    {
                        throw new AppException("Required permission to Move this file.!", null);
                    }
                }
            }



            var documentFolder = _context.DocumentFolder.SingleOrDefault(p => p.DocumentId == value.DocumentID);

            var folders = _context.Folders.SingleOrDefault(p => p.FolderId == value.DocumentID);

            folders.FolderId = documentFolder.FolderId.Value;
            documentFolder.FolderId = value.SelectedFolderID;
            documentFolder.DocumentFolderId = value.DocumentFolderId.Value;
            documentFolder.DocumentId = value.DocumentID;
            documentFolder.UploadedByUserId = value.ModifiedByUserID;
            documentFolder.UploadedDate = DateTime.Now;
            //document.FileName = value.FileName;
            folders.ParentFolderId = documentFolder.Folder.ParentFolderId;
            folders.MainFolderId = documentFolder.Folder.MainFolderId;
            folders.FolderTypeId = documentFolder.Folder.FolderTypeId;
            folders.Name = documentFolder.Folder.Name;
            folders.Description = documentFolder.Folder.Description;
            folders.SessionId = value.SessionID;
            folders.StatusCodeId = value.StatusCodeID;
            folders.ModifiedByUserId = value.ModifiedByUserID;
            folders.ModifiedDate = DateTime.Now;

            //var document = _context.Documents.SingleOrDefault(p => p.DocumentId == value.DocumentID);
            var query = string.Format("Update Documents Set FileName='{1}', ModifiedByUserId={2}, ModifiedDate={3:d}  Where DocumentId= {0}", value.DocumentID, value.FileName, DateTime.Now, value.ModifiedByUserID);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }
            _context.SaveChanges();

            return value;
        }

        [HttpGet]
        [Route("GetMoveDocumentFolderID")]
        public List<DocumentFolderModel> GetMoveDocumentFolderID(DocumentFolderModel value)
        {

            //var documentId = _context.Documents.Where(t => t.DocumentId == DocumentID).Select(s => s.DocumentId).ToList();
            //var folderId = _context.Folders.Where(t => t.FolderId == FolderID).Select(s => s.FolderId).ToList();
            //var documentFolderId = _context.DocumentFolder.Where(t => t.FolderId == FolderID && t.DocumentId == DocumentID).Select(s => s.DocumentFolderId).ToList();
            ////var folderId = _context.TaskMembers.Where(t => t.AssignedCc == userId || t.OnBehalfId == userId).Select(s => s.FolderId.Value).ToList();
            ////subFolderId.AddRange(folderId);

            var folderMaster = _context.DocumentFolder.Select(s => new DocumentFolderModel
            {
                FolderID = s.FolderId.Value,
                DocumentFolderID = s.DocumentFolderId,
                DocumentID = s.DocumentId.Value,
            }).OrderByDescending(o => o.DocumentFolderID).Where(t => t.FolderID == value.FolderID && t.DocumentID == value.DocumentID).ToList();

            return folderMaster;
        }

        [HttpPut]
        [Route("UpdateDescriptionField")]
        public DocumentsModel UpdateDescriptionField(DocumentsModel value)
        {
            var documentFolder = _context.DocumentFolder.SingleOrDefault(p => p.DocumentFolderId == value.DocumentFolderId);

            documentFolder.CheckInDescription = value.Description;
            _context.SaveChanges();
            var query = string.Format("Update Documents Set Description='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where DocumentId= {0}", value.DocumentID, value.Description, DateTime.Now, value.ModifiedByUserID);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }

            return value;
        }

        [HttpPut]
        [Route("UpdateDocumentUserRole")]
        public void UpdateDocumentUserRole(DocumentUserRoleModel value)
        {
            var documentUserRole = _context.DocumentUserRole.FirstOrDefault(p => p.DocumentUserRoleId == value.DocumentUserRoleID);
            if (documentUserRole != null)
            {
                var documentUserRoles = _context.DocumentUserRole.Where(p => p.UserId == documentUserRole.UserId && p.FolderId == documentUserRole.FolderId).ToList();
                documentUserRoles.ForEach(d =>
                {
                    d.RoleId = value.RoleID;
                });
            }
            _context.SaveChanges();
        }

        [HttpPut]
        [Route("DragDropFolder")]
        public FoldersModel DragDropFolder(FoldersModel value)
        {
            var folderMaster = _context.Folders.SingleOrDefault(p => p.FolderId == value.FolderID);
            if (folderMaster != null)
            {
                if (value.ParentFolderID != null && value.MainFolderID != null)
                {
                    var parentFolder = _context.Folders.OrderByDescending(o => o.ParentFolderId).FirstOrDefault(id => id.ParentFolderId == value.ParentFolderID);
                    if (parentFolder == null)
                    {
                        parentFolder = _context.Folders.OrderByDescending(o => o.ParentFolderId).FirstOrDefault(id => id.ParentFolderId == value.ParentFolderID);
                    }

                    folderMaster.ParentFolderId = parentFolder.ParentFolderId + 1;
                }
                else if (value.ParentFolderID == null && value.MainFolderID != null)
                {
                    var folderlevel = _context.Folders.OrderByDescending(o => o.ParentFolderId).FirstOrDefault(id => id.MainFolderId == value.MainFolderID);
                    var level = folderlevel.ParentFolderId + 1;
                    folderMaster.ParentFolderId = level;
                }


                folderMaster.ParentFolderId = value.ParentFolderID;
                folderMaster.ModifiedByUserId = value.ModifiedByUserID;
                folderMaster.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
                if (value.Children.Count > 0)
                {
                    var childerns = ChildrenOf(value);
                    int ignoreFirst = 0;
                    childerns.ToList().ForEach(folder =>
                    {
                        if (ignoreFirst > 0)
                        {
                            DragDropFolderTreeLevel(folder);
                        }
                        ignoreFirst++;
                    });
                }
            }
            //}
            return value;
        }
        private FoldersModel DragDropFolderTreeLevel(FoldersModel value)
        {
            var foldermaster = _context.Folders.SingleOrDefault(p => p.FolderId == value.FolderID);
            if (foldermaster != null)
            {
                if (value.ParentFolderID != null && value.MainFolderID != null)
                {
                    var folderlevel = _context.Folders.OrderByDescending(o => o.ParentFolderId).FirstOrDefault(id => id.FolderId == value.ParentFolderID);

                    foldermaster.ParentFolderId = folderlevel.ParentFolderId + 1;
                }
                else if (value.ParentFolderID == null && value.MainFolderID != null)
                {
                    var folderlevel = _context.Folders.OrderByDescending(o => o.ParentFolderId).FirstOrDefault(id => id.MainFolderId == value.MainFolderID);
                    var level = folderlevel.ParentFolderId + 1;
                    foldermaster.ParentFolderId = level;
                }


                foldermaster.ParentFolderId = value.ParentFolderID;
                foldermaster.ModifiedByUserId = value.ModifiedByUserID;
                foldermaster.ModifiedDate = DateTime.Now;
                _context.SaveChanges();


            }
            return value;
        }
        private List<FoldersModel> GetAllPublicFolders(int id)
        {
            var folders = new List<FoldersModel>();
            var documentpermission = new List<DocumentPermission>();
            var documentRoleList = new List<DocumentUserRole>();
            var userGroupList = new List<UserGroupUser>();

            userGroupList = _context.UserGroupUser.Where(f => f.UserId == id).AsNoTracking().ToList();
            var filterFolders = _context.Folders.Include("AddedByUser").Where(t => t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();
            var folderIds = filterFolders.Select(s => s.FolderId).ToList();
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            documentRoleList = _context.DocumentUserRole.Where(f => folderIds.Contains(f.FolderId.Value) && f.DocumentId == null).AsNoTracking().Distinct().ToList();

            if (filterFolders.Count > 0)
            {
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();
                int idx = 0;
                filterFolders.OrderBy(f => f.FolderId).ToList().ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel();
                    foldersModel.Id = ++idx;
                    if (folders.Any(a => a.FolderID == s.ParentFolderId))
                    {
                        var folder = folders.FirstOrDefault(g => g.FolderID == s.ParentFolderId);
                        //foldersModel.ParentId = folder.Id;
                    }
                    foldersModel.FolderID = s.FolderId;
                    foldersModel.MainFolderID = s.MainFolderId;
                    foldersModel.ParentFolderID = s.ParentFolderId;
                    foldersModel.FolderTypeID = s.FolderTypeId;
                    foldersModel.Name = s.Name;
                    foldersModel.Description = s.Description;
                    foldersModel.ModifiedByUserID = s.ModifiedByUserId;
                    foldersModel.AddedByUserID = s.AddedByUserId;
                    foldersModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    foldersModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    foldersModel.StatusCodeID = s.StatusCodeId;
                    foldersModel.AddedDate = s.AddedDate;
                    foldersModel.ModifiedDate = s.ModifiedDate;
                    folders.Add(foldersModel);
                });

                bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;

                if (isfolderPermission == true)
                {
                    folders = filterFolders.Select(s => new FoldersModel
                    {
                        FolderID = s.FolderId,
                        MainFolderID = s.MainFolderId,
                        ParentFolderID = s.ParentFolderId,
                        FolderTypeID = s.FolderTypeId,
                        // FolderTypeName = s.FolderType.CodeValue,
                        Name = s.Name,
                        Description = s.Description,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUserID = s.AddedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        //StatusCode = s.StatusCode.CodeValue,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        // Attachments = s.DocumentFolder.Select(sa => sa.DocumentId).ToList(),
                        //AttachmentIds = s.DocumentFolder.Where(sa => sa.FolderId == s.FolderId).Select(t => t.DocumentFolderId).ToList(),

                    }).OrderByDescending(o => o.FolderID).ToList();

                    if (documentUserRole.Count > 0)
                    {
                        List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                        folders.ForEach(folder =>
                        {
                            var docuRoles = documentUserRole.Where(f => f.FolderID == folder.FolderID && folder.AddedByUserID != id).ToList();
                            if (docuRoles.Count == 0)
                            {
                                if (!permissionAppliedFolders.Any(f => f.FolderID == folder.FolderID) && folder.FolderID == folder.FolderID && folder.AddedByUserID == id)
                                {

                                    folder.IsDelete = true;
                                    folder.IsRead = true;
                                    folder.IsEdit = true;
                                    permissionAppliedFolders.Add(folder);

                                }
                            }
                            docuRoles.ForEach(document =>
                            {
                                if (document.FolderID == folder.FolderID && folder.AddedByUserID != id)
                                {
                                    if (document.UserID == id)
                                    {
                                        var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                                        if (userpermission != null)
                                        {
                                            folder.DocumentRoleId = userpermission.DocumentRoleId;
                                            folder.UserId = document.UserID;
                                            folder.UserGroupId = document.UserGroupID;
                                            folder.IsDelete = userpermission.IsDelete;
                                            folder.IsRead = userpermission.IsRead;
                                            folder.IsEdit = userpermission.IsEdit;
                                            permissionAppliedFolders.Add(folder);
                                        }
                                        else
                                        {
                                            folder.IsDelete = folder.IsDelete;
                                            folder.IsRead = folder.IsRead;
                                            folder.IsEdit = folder.IsEdit;
                                            permissionAppliedFolders.Add(folder);
                                        }
                                    }
                                    else if (document.UserGroupID != null && document.UserGroupID > 0)
                                    {
                                        var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                                        if (userGroupRole != null)
                                        {
                                            var userGroup = userGroupList.Where(u => u.UserId == id).LastOrDefault();
                                            if (userGroup != null)
                                            {
                                                if (document.UserGroupID == userGroup.UserGroupId)
                                                {
                                                    var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                                    if (userGrouppermission != null)
                                                    {

                                                        folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                                                        folder.UserId = document.UserID;
                                                        folder.UserGroupId = document.UserGroupID;
                                                        folder.IsDelete = userGrouppermission.IsDelete;
                                                        folder.IsRead = userGrouppermission.IsRead;
                                                        folder.IsEdit = userGrouppermission.IsEdit;
                                                        permissionAppliedFolders.Add(folder);

                                                    }
                                                    else
                                                    {
                                                        folder.IsDelete = false;
                                                        folder.IsRead = false;
                                                        folder.IsEdit = false;
                                                        permissionAppliedFolders.Add(folder);
                                                    }


                                                }
                                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                                {
                                                    folder.IsDelete = false;
                                                    folder.IsRead = false;
                                                    folder.IsEdit = false;
                                                    permissionAppliedFolders.Add(folder);

                                                }
                                            }
                                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                            {

                                                folder.IsDelete = false;
                                                folder.IsRead = false;
                                                folder.IsEdit = false;
                                                permissionAppliedFolders.Add(folder);


                                            }
                                        }
                                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                        {
                                            folder.IsDelete = false;
                                            folder.IsRead = false;
                                            folder.IsEdit = false;
                                            permissionAppliedFolders.Add(folder);

                                        }
                                    }
                                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                    {

                                        folder.IsDelete = false;
                                        folder.IsRead = false;
                                        folder.IsEdit = false;
                                        permissionAppliedFolders.Add(folder);

                                    }

                                }
                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == id)
                                {

                                    folder.IsDelete = true;
                                    folder.IsRead = true;
                                    folder.IsEdit = true;
                                    permissionAppliedFolders.Add(folder);

                                }

                            });
                        });
                    }

                }

            }
            return folders;

        }
        public IEnumerable<FoldersModel> ChildrenOf(FoldersModel root)
        {
            yield return root;
            foreach (var c in root.Children)
                foreach (var cc in ChildrenOf(c))
                    yield return cc;
        }



        private List<FoldersModel> FindDocumentLocation(List<FoldersModel> folderslist)
        {
            List<FoldersModel> folderLocation = new List<FoldersModel>();
            var emptyFolderIds = new List<long>();
            foreach (var fn in folderslist)
            {
                FoldersModel f = new FoldersModel();

                if (fn.ParentFolderID > 0)
                {
                    f.FolderPath = "";
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;

                    f.FolderPaths.Add(fn.Name);
                    GenerateLocation(f, fn, folderslist, emptyFolderIds);
                    f.FolderPaths.Reverse();
                    foreach (var item in f.FolderPaths)
                    {
                        if (f.FolderPaths[f.FolderPaths.Count - 1] != item)
                        {
                            f.FolderPath += item + " / ";
                        }
                        else
                        {
                            f.FolderPath += item;
                        }
                    }



                    folderLocation.Add(f);

                }
                else if (fn.ParentFolderID == null)
                {
                    //f.FolderPath = fn.Name;
                    f.FolderPath = fn.Name;
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;
                    folderLocation.Add(f);
                }



            }
            return folderLocation.Distinct().ToList();
        }

        private void GenerateLocation(FoldersModel foldersModel, FoldersModel parentFolderModel, List<FoldersModel> foldersModels, List<long> emptyIds)
        {
            if (!emptyIds.Contains(parentFolderModel.FolderID))
            {
                emptyIds.Add(parentFolderModel.FolderID);
                parentFolderModel = foldersModels.FirstOrDefault(s => s.FolderID == parentFolderModel.ParentFolderID);

                if (parentFolderModel != null)
                {
                    foldersModel.FolderPaths.Add(parentFolderModel.Name);
                    if (parentFolderModel.ParentFolderID != null)
                    {
                        GenerateLocation(foldersModel, parentFolderModel, foldersModels, emptyIds);
                    }
                }
            }

        }

        private void GenerateDocumentLocation(long? id, FoldersModel foldersModel)
        {

            var folders = _context.Folders.Where(t => t.FolderId == id).FirstOrDefault();
            if (folders.ParentFolderId != null)
            {
                if (foldersModel.FolderPaths != null)
                {
                    foldersModel.FolderPaths.Add(folders.Name);
                }

                id = folders.ParentFolderId;
                GenerateDocumentLocation(id, foldersModel);
            }
            else if (folders.ParentFolderId == null)
            {
                if (foldersModel.FolderPaths != null)
                {
                    foldersModel.FolderPaths.Add(folders.Name);
                }
            }


        }
        private void GenerateAllFolder(List<FoldersModel> folderLocations, FoldersModel parentFolderModel, List<FoldersModel> foldersModels, List<long> emptyIds)
        {
            if (!emptyIds.Contains(parentFolderModel.FolderID))
            {
                emptyIds.Add(parentFolderModel.FolderID);
                List<FoldersModel> models = new List<FoldersModel>();
                models = foldersModels.Where(s => s.ParentFolderID == parentFolderModel.FolderID).ToList();
                models.ForEach(f =>
                {
                    if (f != null)
                    {

                        if (f.ParentFolderID != null)
                        {
                            folderLocations.Add(f);
                            GenerateAllFolder(folderLocations, f, foldersModels, emptyIds);

                        }
                    }

                });


            }

        }
        private void GenerateFilePath(long parenfolderID, FoldersModel foldersModel)
        {

            //foldersModel.FolderPath += parenfolderID + "//";
            if (parenfolderID > 0)
            {
                GenerateFilePath(parenfolderID, foldersModel);
            }

        }
        private void CreateDocumentUserRole(FoldersModel value)
        {
            var errormessage = "";
            var documentUserRole = new DocumentUserRole();
            var documentExistingRole = _context.DocumentUserRole.Where(d => d.FolderId == value.FolderID && d.DocumentId == null).ToList();
            var userExistingRole = documentExistingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = documentExistingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();

            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            var documentFolderlist = _context.DocumentFolder.Where(t => t.FolderId == value.FolderID).ToList();
            userList.AddRange(userGroupUserList.Select(s => s.UserId).ToList());
            userList.Distinct();
            if ((userList != null) && (userList.Count > 0))
            {
                if (userExistingRole == null)
                {
                    if (value.SessionID != null)
                    {
                        userList.ForEach(u =>
                        {
                            var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FolderId == value.FolderID && d.DocumentId == null).FirstOrDefault();
                            if (userExisting == null)
                            {
                                documentUserRole = new DocumentUserRole
                                {
                                    UserId = u,
                                    RoleId = value.DocumentRoleId,
                                    UserGroupId = value.UserGroupId,
                                    FolderId = value.FolderID,
                                    IsFolderLevel = value.IsFolderLevel,
                                };
                                _context.DocumentUserRole.Add(documentUserRole);
                                _context.SaveChanges();
                            }

                        });
                        var docu = _context.Documents
                            .Select(s => new Documents
                            {
                                DocumentId = s.DocumentId,
                                IsTemp = s.IsTemp,
                                Description = s.Description,
                                SessionId = s.SessionId
                            }).Where(d => d.SessionId == value.SessionID).ToList();
                        docu.ForEach(file =>
                        {
                            file.IsTemp = false;
                            if (docu != null)
                            {
                                if (value.FolderID > 0 && value.DocumentRoleId > 0)
                                {
                                    userList.ForEach(u =>
                                    {
                                        var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FolderId == value.FolderID && d.DocumentId == null).FirstOrDefault();
                                        if (userExisting == null)
                                        {
                                            documentUserRole = new DocumentUserRole
                                            {

                                                UserId = u,
                                                RoleId = value.DocumentRoleId,
                                                UserGroupId = value.UserGroupId,
                                                DocumentId = file.DocumentId,
                                                FolderId = value.FolderID,
                                                IsFolderLevel = value.IsFolderLevel,
                                            };
                                            _context.DocumentUserRole.Add(documentUserRole);
                                            _context.SaveChanges();
                                        }

                                    });

                                }
                            }
                        });
                    }
                    else
                    {
                        if (value.FolderID > 0)
                        {

                            if ((userList != null) && (userList.Count > 0))
                            {
                                userList.ForEach(u =>
                                {
                                    var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FolderId == value.FolderID && d.DocumentId == null).FirstOrDefault();
                                    if (userExisting == null)
                                    {
                                        documentUserRole = new DocumentUserRole
                                        {
                                            UserId = u,
                                            RoleId = value.DocumentRoleId,
                                            UserGroupId = value.UserGroupId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel,
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                });
                            }

                            if (documentFolderlist != null && documentFolderlist.Count > 0)
                            {
                                documentFolderlist.ForEach(file =>
                                {

                                    if (value.FolderID > 0 && file.DocumentId > 0)
                                    {
                                        userList.ForEach(u =>
                                        {
                                            var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == file.DocumentId && du.UserId == u && du.FolderId == value.FolderID).FirstOrDefault();
                                            if (existingDocumentRole == null)
                                            {
                                                documentUserRole = new DocumentUserRole
                                                {
                                                    UserId = u,
                                                    RoleId = value.DocumentRoleId,
                                                    DocumentId = file.DocumentId,
                                                    FolderId = value.FolderID,
                                                    IsFolderLevel = value.IsFolderLevel,
                                                };
                                                _context.DocumentUserRole.Add(documentUserRole);
                                                _context.SaveChanges();
                                            }
                                        });

                                    }

                                });
                            }

                        }
                    }
                }
                else
                {
                    throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                }
            }
            else if ((userGroupList != null) && (userGroupList.Count > 0))
            {
                if (userGroupExistingRole == null)
                {
                    if (value.SessionID != null)
                    {
                        userGroupList.ForEach(ug =>
                        {
                            documentUserRole = new DocumentUserRole
                            {
                                RoleId = value.DocumentRoleId,
                                UserGroupId = ug,
                                FolderId = value.FolderID,
                                IsFolderLevel = value.IsFolderLevel
                            };
                            _context.DocumentUserRole.Add(documentUserRole);
                            List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();

                            currentGroupUsers.ForEach(c =>
                            {
                                var userExist = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                if (userExist.Count == 0)
                                {
                                    documentUserRole = new DocumentUserRole
                                    {
                                        RoleId = value.DocumentRoleId,
                                        UserId = c.UserId,
                                        ReferencedGroupId = c.UserGroupId,
                                        FolderId = value.FolderID,
                                        IsFolderLevel = value.IsFolderLevel
                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();
                                }
                            });

                        });
                        var docu = _context.Documents.Where(d => d.SessionId == value.SessionID).Select(s => new Documents
                        {
                            DocumentId = s.DocumentId,
                            IsTemp = s.IsTemp,
                            Description = s.Description,
                            SessionId = s.SessionId
                        }).ToList();
                        docu.ForEach(file =>
                        {
                            file.IsTemp = false;
                            if (docu != null)
                            {
                                if (value.FolderID > 0 && value.DocumentRoleId > 0)
                                {
                                    userGroupList.ForEach(ug =>
                                    {
                                        documentUserRole = new DocumentUserRole
                                        {
                                            RoleId = value.DocumentRoleId,
                                            UserGroupId = ug,
                                            DocumentId = file.DocumentId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                        List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();

                                        currentGroupUsers.ForEach(c =>
                                        {
                                            var userExist = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                            if (userExist.Count == 0)
                                            {
                                                documentUserRole = new DocumentUserRole
                                                {
                                                    RoleId = value.DocumentRoleId,
                                                    UserId = c.UserId,
                                                    ReferencedGroupId = c.UserGroupId,
                                                    FolderId = value.FolderID,
                                                    IsFolderLevel = value.IsFolderLevel
                                                };
                                                _context.DocumentUserRole.Add(documentUserRole);
                                                _context.SaveChanges();
                                            }
                                        });

                                    });


                                }
                            }
                        });
                    }
                    else
                    {
                        if (value.FolderID > 0)
                        {
                            userGroupList.ForEach(ug =>
                            {
                                documentUserRole = new DocumentUserRole
                                {
                                    RoleId = value.DocumentRoleId,
                                    UserGroupId = ug,
                                    FolderId = value.FolderID,
                                    IsFolderLevel = value.IsFolderLevel
                                };
                                _context.DocumentUserRole.Add(documentUserRole);

                                List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();

                                currentGroupUsers.ForEach(c =>
                                {
                                    var userExist = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                    if (userExist.Count == 0)
                                    {
                                        documentUserRole = new DocumentUserRole
                                        {
                                            RoleId = value.DocumentRoleId,
                                            UserId = c.UserId,
                                            ReferencedGroupId = c.UserGroupId,
                                            FolderId = value.FolderID,
                                            IsFolderLevel = value.IsFolderLevel
                                        };
                                        _context.DocumentUserRole.Add(documentUserRole);
                                        _context.SaveChanges();
                                    }
                                });
                            });
                            if (documentFolderlist != null && documentFolderlist.Count > 0)
                            {
                                documentFolderlist.ForEach(file =>
                                {
                                    if ((userGroupList != null) && (userGroupList.Count > 0))
                                    {
                                        userGroupList.ForEach(ug =>
                                        {
                                            var existingDocumentRole = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.DocumentId == file.DocumentId && du.UserGroupId == ug).FirstOrDefault();
                                            if (existingDocumentRole == null)
                                            {
                                                documentUserRole = new DocumentUserRole
                                                {
                                                    RoleId = value.DocumentRoleId,
                                                    UserGroupId = ug,
                                                    FolderId = value.FolderID,
                                                    IsFolderLevel = value.IsFolderLevel,
                                                    ReferencedGroupId = ug
                                                };
                                                _context.DocumentUserRole.Add(documentUserRole);
                                                _context.SaveChanges();
                                                List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();

                                                currentGroupUsers.ForEach(c =>
                                                {
                                                    var userExist = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                                    if (userExist.Count == 0)
                                                    {
                                                        documentUserRole = new DocumentUserRole
                                                        {
                                                            RoleId = value.DocumentRoleId,
                                                            UserId = c.UserId,
                                                            ReferencedGroupId = c.UserGroupId,
                                                            FolderId = value.FolderID,
                                                            IsFolderLevel = value.IsFolderLevel
                                                        };
                                                        _context.DocumentUserRole.Add(documentUserRole);
                                                        _context.SaveChanges();
                                                    }
                                                });
                                                _context.SaveChanges();
                                            }
                                        });

                                    }

                                });
                            }

                        }
                    }
                }
                else
                {
                    throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                }
            }
            else if ((value.LevelID != null) && (value.LevelID > 0))
            {
                List<long?> userIds = new List<long?>();
                var designationList = _context.Designation.Where(d => d.LevelId == value.LevelID).Select(e => e.DesignationId).ToList();
                userIds = _context.Employee.Where(e => designationList.Contains(e.DesignationId.Value)).Select(e => e.UserId).ToList();

                if (userIds != null && userIds.Count > 0)
                {
                    userIds.ForEach(u =>
                    {

                        var userExist = _context.DocumentUserRole.Where(du => du.FolderId == value.FolderID && du.UserId == u && du.DocumentId == null).ToList();
                        if (userExist.Count == 0)
                        {
                            documentUserRole = new DocumentUserRole
                            {
                                RoleId = value.DocumentRoleId,
                                LevelId = value.LevelID,
                                FolderId = value.FolderID,
                                IsFolderLevel = value.IsFolderLevel,
                                UserId = u,
                            };
                            _context.DocumentUserRole.Add(documentUserRole);
                            _context.SaveChanges();
                        }
                    });
                }

            }
        }

        private void updateDocumentRole(long folderID)
        {
            var documentFolders = _context.DocumentFolder.Where(f => f.FolderId == folderID).ToList();
            var FolderRoles = _context.DocumentUserRole.Where(d => d.FolderId == folderID && d.DocumentId == null).ToList();
            if (documentFolders.Count != 0 && documentFolders != null)
            {
                documentFolders.ForEach(doc =>
                {
                    if (FolderRoles.Count != 0 && FolderRoles != null)
                    {
                        FolderRoles.ForEach(u =>
                        {
                            var existingDocumentRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserId == u.UserId && du.FolderId == u.FolderId).FirstOrDefault();
                            if (existingDocumentRole == null)
                            {
                                if (u.UserId != null)
                                {
                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {

                                        UserId = u.UserId,
                                        RoleId = u.RoleId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = folderID,

                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();
                                }

                            }
                            var existingUserGroupRole = _context.DocumentUserRole.Where(du => du.DocumentId == doc.DocumentId && du.UserGroupId == u.UserGroupId).FirstOrDefault();
                            if (existingUserGroupRole == null)
                            {
                                if (u.UserGroupId != null)
                                {
                                    DocumentUserRole documentUserRole = new DocumentUserRole
                                    {
                                        RoleId = u.RoleId,
                                        UserGroupId = u.UserGroupId,
                                        DocumentId = doc.DocumentId,
                                        FolderId = folderID,

                                    };
                                    _context.DocumentUserRole.Add(documentUserRole);
                                    _context.SaveChanges();
                                }
                            }
                        });
                    }
                });
            }
        }

        [HttpGet]
        [Route("GetDocumentUserRoleByGroupFolderIDs")]
        public void GetDocumentUserRoleByGroupFolderIDs()
        {
            var documentUserRoles = _context.DocumentUserRole.Where(f => f.UserGroupId > 0 && f.UserId == null).AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            List<long?> groupIds = documentUserRoles.Select(s => s.UserGroupId).Distinct().ToList();
            //List<long?> folderIds = documentUserRoles.Select(s => s.FolderId).ToList();

            List<UserGroup> userGroups = userGroupItems.Where(g => groupIds.Contains(g.UserGroupId)).ToList();
            List<UserGroupUser> userGroupUsers = userGroupUserItems.Where(g => g.UserGroupId != null && groupIds.Contains(g.UserGroupId.Value)).ToList();
            var folderid = 231;
            // var deleteDocumentUserRoles = _context.DocumentUserRole.Where(r => folderIds.Contains(r.FolderId) && documentUserRoles.Select(s => s.RoleId).Contains(r.RoleId) && userGroupUsers.Select(u => u.UserId).Contains(r.UserId)).ToList();
            var deleteDocumentUserRoles = _context.DocumentUserRole.Where(r => r.FolderId == folderid && documentUserRoles.Select(s => s.RoleId).Contains(r.RoleId) && userGroupUsers.Select(u => u.UserId).Contains(r.UserId) && r.DocumentId == null).AsNoTracking().ToList();
            _context.DocumentUserRole.RemoveRange(deleteDocumentUserRoles);
            _context.SaveChanges();
            //folderIds.ForEach(f =>
            //{
            var foldergroupIds = documentUserRoles.Where(c => c.FolderId == 231).Select(c => c.UserGroupId).Distinct().ToList();
            userGroupUsers.Where(gu => foldergroupIds.Contains(gu.UserGroupId)).ToList().ForEach(g =>
                {
                    DocumentUserRole documentUserRole = new DocumentUserRole();
                    documentUserRole.FolderId = folderid;
                    documentUserRole.UserId = g.UserId;
                    documentUserRole.ReferencedGroupId = g.UserGroupId;
                    documentUserRole.RoleId = documentUserRoles.FirstOrDefault(u => u.UserGroupId == g.UserGroupId).RoleId;
                    _context.DocumentUserRole.Add(documentUserRole);
                });
            //});

            _context.SaveChanges();

        }

        [HttpGet]
        [Route("GetParentPublicFolders")]
        public List<FoldersModel> GetParentPublicFolders(long id)
        {
            var folders = new List<FoldersModel>();
            List<DocumentPermission> documentpermission = null;
            List<DocumentUserRole> documentRoleList = null;
            List<UserGroupUser> userGroupList = null;
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            userGroupList = _context.UserGroupUser.AsNoTracking().ToList();
            var filterFolders = _context.Folders.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").Where(t => t.FolderTypeId == 524 && t.StatusCodeId == 1 && t.ParentFolderId == null).AsNoTracking().OrderByDescending(o => o.FolderId).ToList();
            var folderIds = filterFolders.Select(f => f.FolderId).ToList();
            documentRoleList = _context.DocumentUserRole.Where(f => f.FolderId != null && folderIds.Contains(f.FolderId.Value)).AsNoTracking().ToList();
            if (filterFolders.Count > 0)
            {
                bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;

              
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();
                if (filterFolders != null && filterFolders.Count > 0)
                {
                    if (isfolderPermission == true)
                    {
                        // bool? isfolderPermission = _context.UserSettings.SingleOrDefault(p => (p.UserId == 47 || p.UserId == 1) && p.IsEnableFolderPermission != null)?.IsEnableFolderPermission;
                        var withPermissionFolderIds = documentUserRole.Where(d => folderIds.Contains(d.FolderID.Value)).Select(s => s.FolderID).Distinct().ToList();
                        var withoutPermissionFolderIds = filterFolders.Where(f => !withPermissionFolderIds.Contains(f.FolderId)).Select(s => s.FolderId).Distinct().ToList();
                        var documentUserRoles = _context.DocumentUserRole.Where(w => w.UserId == id && folderIds.Contains(w.FolderId.Value)).ToList();
                        var roleIds = documentUserRoles.Select(s => s.RoleId.GetValueOrDefault(0)).Distinct().ToList();
                        var documentPermission = _context.DocumentPermission.Select(s => new { s.DocumentRoleId, s.IsRead }).Where(w => roleIds.Contains(w.DocumentRoleId.Value)).ToList();
                        filterFolders.ForEach(s =>
                        {
                            if (withPermissionFolderIds.Contains(s.FolderId))
                            {
                                bool? isRead = false;
                                var documentUserRoleId = documentUserRoles.FirstOrDefault(w => w.UserId == id && w.FolderId == s.FolderId)?.RoleId;
                                var isReads = documentPermission.FirstOrDefault(f => f.DocumentRoleId == documentUserRoleId)?.IsRead;
                                if (s.AddedByUserId == id || isReads == true)
                                {
                                    isRead = true;
                                }
                                if (isRead == true)
                                {
                                    FoldersModel foldersModel = new FoldersModel
                                    {
                                        FolderID = s.FolderId,
                                        MainFolderID = s.MainFolderId,
                                        ParentFolderID = s.ParentFolderId,
                                        FolderTypeID = s.FolderTypeId,
                                        Name = s.Name,
                                        Description = s.Description,
                                        ModifiedByUserID = s.ModifiedByUserId,
                                        AddedByUserID = s.AddedByUserId,
                                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                                        StatusCodeID = s.StatusCodeId,
                                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                        AddedDate = s.AddedDate,
                                        ModifiedDate = s.ModifiedDate,
                                    };
                                    folders.Add(foldersModel);
                                }
                            }
                            else if (withoutPermissionFolderIds.Contains(s.FolderId))

                            {
                                FoldersModel foldersModel = new FoldersModel
                                {
                                    FolderID = s.FolderId,
                                    MainFolderID = s.MainFolderId,
                                    ParentFolderID = s.ParentFolderId,
                                    FolderTypeID = s.FolderTypeId,
                                    Name = s.Name,
                                    Description = s.Description,
                                    ModifiedByUserID = s.ModifiedByUserId,
                                    AddedByUserID = s.AddedByUserId,
                                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                                    StatusCodeID = s.StatusCodeId,
                                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                    AddedDate = s.AddedDate,
                                    ModifiedDate = s.ModifiedDate,
                                };
                                folders.Add(foldersModel);
                            }
                        });
                    }
                    else
                    {
                        filterFolders.ForEach(s =>
                        {
                            FoldersModel foldersModel = new FoldersModel
                            {
                                FolderID = s.FolderId,
                                MainFolderID = s.MainFolderId,
                                ParentFolderID = s.ParentFolderId,
                                FolderTypeID = s.FolderTypeId,
                                Name = s.Name,
                                Description = s.Description,
                                ModifiedByUserID = s.ModifiedByUserId,
                                AddedByUserID = s.AddedByUserId,
                                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                                StatusCodeID = s.StatusCodeId,
                                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,
                            };
                            folders.Add(foldersModel);

                        });
                    }

                }

                if (documentUserRole.Count > 0)
                {
                    List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                    folders.ForEach(folder =>
                    {
                        documentUserRole.ForEach(document =>
                        {
                            if (document.FolderID == folder.FolderID && folder.AddedByUserID != id)
                            {
                                if (document.UserID == id)
                                {
                                    var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                                    if (userpermission != null)
                                    {
                                        folder.DocumentRoleId = userpermission.DocumentRoleId;
                                        folder.UserId = document.UserID;
                                        folder.UserGroupId = document.UserGroupID;
                                        folder.IsDelete = userpermission.IsDelete;
                                        folder.IsRead = userpermission.IsRead;
                                        folder.IsEdit = userpermission.IsEdit;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                    else
                                    {
                                        folder.IsDelete = folder.IsDelete;
                                        folder.IsRead = folder.IsRead;
                                        folder.IsEdit = folder.IsEdit;
                                        permissionAppliedFolders.Add(folder);
                                    }
                                }
                                else if (document.UserGroupID != null && document.UserGroupID > 0)
                                {
                                    var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                                    if (userGroupRole != null)
                                    {
                                        var userGroup = userGroupList.Where(u => u.UserId == id).LastOrDefault();
                                        if (userGroup != null)
                                        {
                                            if (document.UserGroupID == userGroup.UserGroupId)
                                            {
                                                var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                                                if (userGrouppermission != null)
                                                {

                                                    folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                                                    folder.UserId = document.UserID;
                                                    folder.UserGroupId = document.UserGroupID;
                                                    folder.IsDelete = userGrouppermission.IsDelete;
                                                    folder.IsRead = userGrouppermission.IsRead;
                                                    folder.IsEdit = userGrouppermission.IsEdit;
                                                    permissionAppliedFolders.Add(folder);

                                                }
                                                else
                                                {
                                                    folder.IsDelete = false;
                                                    folder.IsRead = false;
                                                    folder.IsEdit = false;
                                                    permissionAppliedFolders.Add(folder);
                                                }


                                            }
                                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                            {
                                                folder.IsDelete = false;
                                                folder.IsRead = false;
                                                folder.IsEdit = false;
                                                permissionAppliedFolders.Add(folder);

                                            }
                                        }
                                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                        {

                                            folder.IsDelete = false;
                                            folder.IsRead = false;
                                            folder.IsEdit = false;
                                            permissionAppliedFolders.Add(folder);


                                        }
                                    }
                                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                    {
                                        folder.IsDelete = false;
                                        folder.IsRead = false;
                                        folder.IsEdit = false;
                                        permissionAppliedFolders.Add(folder);

                                    }
                                }
                                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                                {

                                    folder.IsDelete = false;
                                    folder.IsRead = false;
                                    folder.IsEdit = false;
                                    permissionAppliedFolders.Add(folder);

                                }

                            }
                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == id)
                            {

                                folder.IsDelete = true;
                                folder.IsRead = true;
                                folder.IsEdit = true;
                                permissionAppliedFolders.Add(folder);

                            }

                        });
                    });
                }

            }

            return folders;
        }

        [HttpGet]
        [Route("GetDocumentListByFolder")]
        public List<DocumentsModel> GetDocumentListByFolder(int id, int userId)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            List<long> folderIds = new List<long>();
            var applicationUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId
            }).AsNoTracking().ToList();
            List<Folders> childFolders = _context.Folders.Where(f => f.ParentFolderId == id).AsNoTracking().ToList();
            childFolders.ForEach(c =>
            {
                folderIds.Add(c.FolderId);
                if (c.ParentFolderId > 0)
                {
                    GetAllFolderIDs(c.ParentFolderId.Value, folderIds);
                }
            });
            List<DocumentFolderModel> documentFolderModels = new List<DocumentFolderModel>();
            var documentFolder = _context.DocumentFolder.Where(d => folderIds.Contains(d.FolderId.Value) && d.IsLatest == true).OrderByDescending(df => df.DocumentFolderId).Distinct().ToList();
            var documentIds = documentFolder.Select(d => d.DocumentId).ToList();
            var documents = _context.Documents.Include(f => f.FilterProfileType)
                                .Include(f => f.AddedByUser)
                                .Include(f => f.ModifiedByUser)
                            .Select(d => new
                            {
                                d.DocumentId,
                                d.FileName,
                                d.FilterProfileTypeId,
                                FileProfileTypeName = d.FilterProfileType != null ? d.FilterProfileType.Name : "",
                                d.AddedByUserId,
                                d.AddedDate,
                                AddedUser = d.AddedByUser != null ? d.AddedByUser.UserName : "",
                            }).Where(d => documentIds.Contains(d.DocumentId)).ToList();
            if (documentFolder != null && documentFolder.Count > 0)
            {
                documentFolder.ForEach(s =>
                {
                    var documentItem = documents.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                    DocumentsModel documentModel = new DocumentsModel
                    {
                        FolderID = s.FolderId.Value,
                        FileName = documentItem.FileName,
                        DocumentID = s.DocumentId.Value,
                        AddedByUserID = s.Document != null ? s.Document.AddedByUserId : null,
                    };
                    documentsModels.Add(documentModel);
                });

            }
            return documentsModels;
        }

        private List<long> GetAllFolderIDs(long folderId, List<long> folderIds)
        {
            var childFolders = _context.Folders.Where(c => c.ParentFolderId == folderId).ToList();
            childFolders.ForEach(c =>
            {
                folderIds.Add(c.FolderId);
                if (c.ParentFolderId > 0)
                {
                    GetAllFolderIDs(c.ParentFolderId.Value, folderIds);
                }
            });
            return folderIds;
        }

        [HttpGet]
        [Route("GetDocumentsByFolderID")]
        public List<DocumentsModel> GetFoldersByFolderID(int id, int userId)
        {
            var foldersList = _context.Folders.Where(t => t.FolderId == id).AsNoTracking().ToList();
            var folderName = _context.Folders.Where(t => t.FolderId == id).FirstOrDefault()?.Name;
            var folderIds = foldersList.Select(f => f.FolderId).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var documentFolder = _context.DocumentFolder.Where(d => folderIds.Contains(d.FolderId.Value) && d.IsLatest == true).OrderByDescending(df => df.DocumentFolderId).Distinct().ToList();
            var documentIds = documentFolder.Select(d => d.DocumentId).ToList();
            var setAccess = _context.FileProfileTypeSetAccess.Where(s => documentIds.Contains(s.DocumentId.Value)).ToList();
            var documents = _context.Documents.Include(f => f.FilterProfileType)
                                .Include(f => f.AddedByUser)
                                .Include(f => f.ModifiedByUser)
                            .Select(d => new
                            {
                                DocumentID = d.DocumentId,
                                FileName = d.FileName,
                                FilterProfileTypeId = d.FilterProfileTypeId,
                                FileProfileTypeName = d.FilterProfileType != null ? d.FilterProfileType.Name : "",
                                AddedByUserID = d.AddedByUserId,
                                AddedDate = d.AddedDate,
                                AddedByUser = d.AddedByUser != null ? d.AddedByUser.UserName : "",
                                SessionID = d.SessionId,
                                UploadDate = d.UploadDate,

                            }).Where(d => documentIds.Contains(d.DocumentID)).ToList();
            documents.ForEach(d =>
            {
                var setAccessdocExits = setAccess.Where(w => w.DocumentId == d.DocumentID).Count();
                var setAccessExits = true;
                if (d.AddedByUserID != userId)
                {
                    if (setAccessdocExits > 0)
                    {
                        setAccessExits = false;
                        var userExits = setAccess.Where(w => w.UserId == userId && w.DocumentId == d.DocumentID).Count();
                        if (userExits > 0)
                        {
                            setAccessExits = true;
                        }
                    }
                }
                if (setAccessExits == true)
                {
                    DocumentsModel DocumentsModel = new DocumentsModel();
                    DocumentsModel.DocumentID = d.DocumentID;
                    DocumentsModel.FileName = d.FileName;
                    DocumentsModel.FilterProfileTypeId = d.FilterProfileTypeId;
                    DocumentsModel.FileProfileTypeName = d.FileProfileTypeName;
                    DocumentsModel.AddedByUserID = d.AddedByUserID;
                    DocumentsModel.AddedDate = d.AddedDate != null ? d.AddedDate : d.UploadDate;
                    DocumentsModel.AddedByUser = d.AddedByUser != null && d.AddedByUser != "" ? d.AddedByUser : documentFolder?.FirstOrDefault(f => f.FolderId == id && f.DocumentId == d.DocumentID)?.UploadedByUserId != null ? appUsers?.FirstOrDefault(u => u.UserId == documentFolder?.FirstOrDefault(f => f.FolderId == id && f.DocumentId == d.DocumentID)?.UploadedByUserId)?.UserName : "";
                    DocumentsModel.SessionID = d.SessionID;
                    DocumentsModel.FolderId = id;
                    DocumentsModel.FolderID = id;
                    DocumentsModel.FolderName = folderName;
                    documentsModels.Add(DocumentsModel);
                }
            });
            return documentsModels;
        }
        [HttpGet]
        [Route("GetPublicFolderPath")]
        public List<FoldersModel> GetPublicFolderPath(int id, int userId)
        {
            var mainFolderId = _context.Folders.FirstOrDefault(t => t.FolderId == id)?.MainFolderId;
            var foldersListItems = _context.Folders.Where(t => t.MainFolderId == mainFolderId).AsNoTracking().ToList();

            List<FoldersModel> foldersList = new List<FoldersModel>();
            var documentuserList = _context.DocumentUserRole.Where(d => d.FolderId != null).AsNoTracking().ToList();
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                var folderIds = foldersListItems.Select(s => s.FolderId).ToList();
                var withPermissionFolderIds = documentuserList.Where(d => folderIds.Contains(d.FolderId.Value)).Select(s => s.FolderId).Distinct().ToList();
                var withoutPermissionFolderIds = foldersListItems.Where(f => !withPermissionFolderIds.Contains(f.FolderId)).Select(s => s.FolderId).Distinct().ToList();
                var documentUserRole = documentuserList?.Where(w => w.UserId == userId && withPermissionFolderIds.Contains(w.FolderId.Value)).ToList();
                var roleIds = documentUserRole.Select(s => s.RoleId.GetValueOrDefault(0)).Distinct().ToList();

                var documentPermission = _context.DocumentPermission.Select(s => new { s.DocumentRoleId, s.IsRead }).Where(w => roleIds.Contains(w.DocumentRoleId.Value)).ToList();
                foldersListItems.ForEach(s =>
                {
                    if (withPermissionFolderIds.Contains(s.FolderId))
                    {
                        bool? isRead = true;

                        var documentUserRoleId = documentUserRole.FirstOrDefault(w => w.UserId == userId && w.FolderId == s.FolderId)?.RoleId;
                        var isReads = documentPermission.FirstOrDefault(f => f.DocumentRoleId == documentUserRoleId)?.IsRead != null ? documentPermission.FirstOrDefault(f => f.DocumentRoleId == documentUserRoleId)?.IsRead : true;
                        if (s.AddedByUserId == userId)
                        {
                            isRead = true;
                        }
                        else if (s.AddedByUserId != userId || isReads == false)
                        {
                            isRead = false;
                        }
                        if (isReads == true)
                        {
                            isRead = true;
                        }
                        if (isRead == true)
                        {
                            List<string> FolderPathLists = new List<string>();
                            FoldersModel foldersModel = new FoldersModel();
                            foldersModel.FolderID = s.FolderId;
                            foldersModel.ParentFolderID = s.ParentFolderId;
                            foldersModel.MainFolderID = s.MainFolderId;
                            foldersModel.Name = s.Name;
                            foldersModel.Description = s.Description;
                            foldersModel.StatusCodeID = s.StatusCodeId;
                            foldersModel.AddedByUserID = s.AddedByUserId;
                            FolderPathLists = GetAllFolderName(foldersListItems, s, FolderPathLists);
                            FolderPathLists.Add(s.Name);
                            foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                            foldersList.Add(foldersModel);
                        }
                    }
                    else if (withoutPermissionFolderIds.Contains(s.FolderId))
                    {
                        List<string> FolderPathLists = new List<string>();
                        FoldersModel foldersModel = new FoldersModel();
                        foldersModel.FolderID = s.FolderId;
                        foldersModel.ParentFolderID = s.ParentFolderId;
                        foldersModel.MainFolderID = s.MainFolderId;
                        foldersModel.Name = s.Name;
                        foldersModel.Description = s.Description;
                        foldersModel.StatusCodeID = s.StatusCodeId;
                        foldersModel.AddedByUserID = s.AddedByUserId;
                        FolderPathLists = GetAllFolderName(foldersListItems, s, FolderPathLists);
                        FolderPathLists.Add(s.Name);
                        foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                        foldersList.Add(foldersModel);
                    }
                });
            }
            return foldersList;
        }
        [HttpGet]
        [Route("GetPublicFolderPathAll")]
        public List<FoldersModel> GetPublicFolderPathAll(int id)
        {

            var foldersListItems = _context.Folders.Select(s => new Folders
            {
                FolderId = s.FolderId,
                MainFolderId = s.MainFolderId,
                ParentFolderId = s.ParentFolderId,
                Name = s.Name,
                Description = s.Description,
                AddedByUserId = s.AddedByUserId,
                StatusCodeId = s.StatusCodeId

            }).AsNoTracking().ToList();
            List<FoldersModel> foldersList = new List<FoldersModel>();
            List<FoldersModel> folderLocation = new List<FoldersModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in foldersListItems)
            {
                FoldersModel f = new FoldersModel();

                if (fn.ParentFolderId > 0)
                {
                    f.FolderID = fn.FolderId;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderId;
                    f.ParentFolderPathName = foldersListItems.FirstOrDefault(t => t.FolderId == fn.ParentFolderId)?.Name + "/" + f.Name;
                    folderPath += folderPath != "" ? "/" + f.Name : f.Name;
                    f.FolderPath = folderPath;
                    folderLocation.Add(f);

                }
                else if (fn.ParentFolderId == null)
                {
                    folderPath = fn.Name;
                    f.FolderPath = folderPath;
                    f.FolderID = fn.FolderId;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderId;
                    folderLocation.Add(f);
                }
            }
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                var folderIds = foldersListItems.Select(s => s.FolderId).ToList();
                var documentUserRole = _context.DocumentUserRole.Where(w => w.UserId == id && folderIds.Contains(w.FolderId.Value)).ToList();
                var roleIds = documentUserRole.Select(s => s.RoleId.GetValueOrDefault(0)).ToList();
                var documentPermission = _context.DocumentPermission.Select(s => new { s.DocumentRoleId, s.IsRead }).Where(w => roleIds.Contains(w.DocumentRoleId.Value)).ToList();
                foldersListItems.ForEach(s =>
                {
                    bool? isRead = true;
                    var documentUserRoleId = documentUserRole.FirstOrDefault(w => w.UserId == id && w.FolderId == s.FolderId)?.RoleId;
                    var isReads = documentPermission.FirstOrDefault(f => f.DocumentRoleId == documentUserRoleId)?.IsRead;
                    if (s.AddedByUserId != id)
                    {
                        isRead = false;
                    }
                    if (isReads == true)
                    {
                        isRead = true;
                    }
                    if (isRead == true)
                    {
                        List<string> FolderPathLists = new List<string>();
                        FoldersModel foldersModel = new FoldersModel();
                        foldersModel.FolderID = s.FolderId;
                        foldersModel.ParentFolderID = s.ParentFolderId;
                        foldersModel.MainFolderID = s.MainFolderId;
                        foldersModel.Name = s.Name;
                        foldersModel.Description = s.Description;
                        foldersModel.StatusCodeID = s.StatusCodeId;
                        foldersModel.AddedByUserID = s.AddedByUserId;
                        foldersModel.FolderPath = folderLocation?.FirstOrDefault(f => f.FolderID == s.FolderId)?.FolderPath;
                        if (s.ParentFolderId != null)
                        {
                            //FolderPathLists = GetAllFolderName(foldersListItems, s, FolderPathLists);
                        }
                        //FolderPathLists.Add(s.Name);
                        //foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                        foldersList.Add(foldersModel);
                    }
                });
            }
            return foldersList;
        }
        [HttpPost]
        [Route("GetPublicFolderMainPathAll")]
        public List<FoldersModel> GetPublicFolderMainPathAll(SearchModel searchModel)
        {
            var foldersListItem = _context.Folders.Select(s => new FoldersModel
            {
                FolderID = s.FolderId,
                Name = s.Name,
                ParentFolderID = s.ParentFolderId,
                AddedByUserID = s.AddedByUserId
            }).Where(w => w.FolderID > 0 && w.Name != "").ToList();
            var foldersListItems = foldersListItem.ToList();
            if (!string.IsNullOrWhiteSpace(searchModel.SearchString))
            {
                foldersListItems = foldersListItems.Where(w => w.Name.ToLower().Trim().Contains(searchModel.SearchString.ToLower().Trim())).ToList();
            }

            List<FoldersModel> foldersList = new List<FoldersModel>();
            if (foldersListItems != null)
            {
                foldersListItems.ForEach(s =>
                {
                    List<string> FolderPathLists = new List<string>();
                    FoldersModel foldersModel = new FoldersModel();
                    foldersModel.FolderID = s.FolderID;
                    foldersModel.ParentFolderID = s.ParentFolderID;
                    foldersModel.Name = s.Name;
                    foldersModel.AddedByUserID = s.AddedByUserID;
                    if (s.ParentFolderID != null)
                    {
                        FolderPathLists = GetAllFolderNames(foldersListItem, s, FolderPathLists);
                    }
                    FolderPathLists.Add(s.Name);
                    foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                    foldersList.Add(foldersModel);
                });
            }
            return foldersList;
        }
        private List<string> GetAllFolderNames(List<FoldersModel> foldersListItems, FoldersModel s, List<string> foldersModel)
        {
            if (s.ParentFolderID != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderID == s.ParentFolderID);
                GetAllFolderNames(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        [HttpGet]
        [Route("GetPublicFolderMainPath")]
        public List<FoldersModel> GetPublicFolderMainPath(int id)
        {
            var foldersListItem = _context.Folders.AsNoTracking().ToList();
            var foldersListItems = foldersListItem.Where(t => t.FolderId == id).ToList();
            List<FoldersModel> foldersList = new List<FoldersModel>();
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                foldersListItems.ForEach(s =>
                {
                    List<string> FolderPathLists = new List<string>();
                    FoldersModel foldersModel = new FoldersModel();
                    foldersModel.FolderID = s.FolderId;
                    foldersModel.ParentFolderID = s.ParentFolderId;
                    foldersModel.MainFolderID = s.MainFolderId;
                    foldersModel.Name = s.Name;
                    foldersModel.Description = s.Description;
                    foldersModel.StatusCodeID = s.StatusCodeId;
                    foldersModel.AddedByUserID = s.AddedByUserId;
                    if (s.ParentFolderId != null)
                    {
                        FolderPathLists = GetAllFolderName(foldersListItem, s, FolderPathLists);
                    }
                    FolderPathLists.Add(s.Name);
                    foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                    foldersList.Add(foldersModel);
                });
            }
            return foldersList;
        }
        private List<string> GetAllFolderName(List<Folders> foldersListItems, Folders s, List<string> foldersModel)
        {
            if (s.ParentFolderId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderId == s.ParentFolderId);
                GetAllFolderName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        private List<FoldersModel> GetCurrentPublicFolders(int id, int userId)
        {
            var folders = new List<FoldersModel>();
            var documentpermission = new List<DocumentPermission>();
            var documentRoleList = new List<DocumentUserRole>();
            var userGroupList = new List<UserGroupUser>();

            userGroupList = _context.UserGroupUser.Where(f => f.UserId == userId).AsNoTracking().ToList();
            var filterFolders = _context.Folders.Include("AddedByUser").Where(t => (t.FolderId == id || t.ParentFolderId == id) && t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();
            var folderIds = filterFolders.Select(s => s.FolderId).ToList();
            GetChildFolders(filterFolders, folderIds);
            var hierarchyFolders = _context.Folders.Include("AddedByUser").Where(t => (folderIds.Contains(t.FolderId) || (t.ParentFolderId != null && folderIds.Contains(t.ParentFolderId.Value))) && t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();
            documentpermission = _context.DocumentPermission.AsNoTracking().ToList();
            documentRoleList = _context.DocumentUserRole.Where(f => folderIds.Contains(f.FolderId.Value) && f.DocumentId == null).AsNoTracking().Distinct().ToList();

            if (hierarchyFolders.Count > 0)
            {
                var documentUserRole = documentRoleList.Where(p => p.DocumentId == null).GroupBy(d => new { d.RoleId, d.FolderId, d.DocumentUserRoleId, d.UserId, d.UserGroupId }).Select(s => new DocumentUserRoleModel
                {
                    RoleID = s.Key.RoleId,
                    FolderID = s.Key.FolderId,
                    DocumentUserRoleID = s.Key.DocumentUserRoleId,
                    UserID = s.Key.UserId,
                    UserGroupID = s.Key.UserGroupId
                }).ToList();
                int idx = 0;
                hierarchyFolders.OrderBy(f => f.FolderId).ToList().ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel();
                    foldersModel.Id = ++idx;
                    if (folders.Any(a => a.FolderID == s.ParentFolderId))
                    {
                        var folder = folders.FirstOrDefault(g => g.FolderID == s.ParentFolderId);
                        //foldersModel.ParentId = folder.Id;
                    }
                    foldersModel.FolderID = s.FolderId;
                    foldersModel.MainFolderID = s.MainFolderId;
                    foldersModel.ParentFolderID = s.ParentFolderId;
                    foldersModel.FolderTypeID = s.FolderTypeId;
                    foldersModel.Name = s.Name;
                    foldersModel.Description = s.Description;
                    foldersModel.ModifiedByUserID = s.ModifiedByUserId;
                    foldersModel.AddedByUserID = s.AddedByUserId;
                    foldersModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    foldersModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                    foldersModel.StatusCodeID = s.StatusCodeId;
                    foldersModel.AddedDate = s.AddedDate;
                    foldersModel.ModifiedDate = s.ModifiedDate;
                    folders.Add(foldersModel);
                });

                //if (documentUserRole.Count > 0)
                //{
                //    List<FoldersModel> permissionAppliedFolders = new List<FoldersModel>();

                //    folders.ForEach(folder =>
                //    {
                //        var docuRoles = documentUserRole.Where(f => f.FolderID == folder.FolderID && folder.AddedByUserID != userId).ToList();
                //        if (docuRoles.Count == 0)
                //        {
                //            if (!permissionAppliedFolders.Any(f => f.FolderID == folder.FolderID) && folder.FolderID == folder.FolderID && folder.AddedByUserID == userId)
                //            {

                //                folder.IsDelete = true;
                //                folder.IsRead = true;
                //                folder.IsEdit = true;
                //                permissionAppliedFolders.Add(folder);

                //            }
                //        }
                //        docuRoles.ForEach(document =>
                //        {
                //            if (document.FolderID == folder.FolderID && folder.AddedByUserID != userId)
                //            {
                //                if (document.UserID == userId)
                //                {
                //                    var userpermission = documentpermission.Where(d => d.DocumentRoleId == document.RoleID).FirstOrDefault();
                //                    if (userpermission != null)
                //                    {
                //                        folder.DocumentRoleId = userpermission.DocumentRoleId;
                //                        folder.UserId = document.UserID;
                //                        folder.UserGroupId = document.UserGroupID;
                //                        folder.IsDelete = userpermission.IsDelete;
                //                        folder.IsRead = userpermission.IsRead;
                //                        folder.IsEdit = userpermission.IsEdit;
                //                        permissionAppliedFolders.Add(folder);
                //                    }
                //                    else
                //                    {
                //                        folder.IsDelete = folder.IsDelete;
                //                        folder.IsRead = folder.IsRead;
                //                        folder.IsEdit = folder.IsEdit;
                //                        permissionAppliedFolders.Add(folder);
                //                    }
                //                }
                //                else if (document.UserGroupID != null && document.UserGroupID > 0)
                //                {
                //                    var userGroupRole = documentRoleList.Where(r => r.UserGroupId == document.UserGroupID && r.FolderId == document.FolderID).FirstOrDefault();
                //                    if (userGroupRole != null)
                //                    {
                //                        var userGroup = userGroupList.Where(u => u.UserId == userId).LastOrDefault();
                //                        if (userGroup != null)
                //                        {
                //                            if (document.UserGroupID == userGroup.UserGroupId)
                //                            {
                //                                var userGrouppermission = documentpermission.Where(d => d.DocumentRoleId == userGroupRole.RoleId).FirstOrDefault();
                //                                if (userGrouppermission != null)
                //                                {

                //                                    folder.DocumentRoleId = userGrouppermission.DocumentRoleId;
                //                                    folder.UserId = document.UserID;
                //                                    folder.UserGroupId = document.UserGroupID;
                //                                    folder.IsDelete = userGrouppermission.IsDelete;
                //                                    folder.IsRead = userGrouppermission.IsRead;
                //                                    folder.IsEdit = userGrouppermission.IsEdit;
                //                                    permissionAppliedFolders.Add(folder);

                //                                }
                //                                else
                //                                {
                //                                    folder.IsDelete = false;
                //                                    folder.IsRead = false;
                //                                    folder.IsEdit = false;
                //                                    permissionAppliedFolders.Add(folder);
                //                                }


                //                            }
                //                            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                //                            {
                //                                folder.IsDelete = false;
                //                                folder.IsRead = false;
                //                                folder.IsEdit = false;
                //                                permissionAppliedFolders.Add(folder);

                //                            }
                //                        }
                //                        else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                //                        {

                //                            folder.IsDelete = false;
                //                            folder.IsRead = false;
                //                            folder.IsEdit = false;
                //                            permissionAppliedFolders.Add(folder);


                //                        }
                //                    }
                //                    else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                //                    {
                //                        folder.IsDelete = false;
                //                        folder.IsRead = false;
                //                        folder.IsEdit = false;
                //                        permissionAppliedFolders.Add(folder);

                //                    }
                //                }
                //                else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID)
                //                {

                //                    folder.IsDelete = false;
                //                    folder.IsRead = false;
                //                    folder.IsEdit = false;
                //                    permissionAppliedFolders.Add(folder);

                //                }

                //            }
                //            else if (!permissionAppliedFolders.Any(f => f.FolderID == document.FolderID) && folder.FolderID == document.FolderID && folder.AddedByUserID == userId)
                //            {

                //                folder.IsDelete = true;
                //                folder.IsRead = true;
                //                folder.IsEdit = true;
                //                permissionAppliedFolders.Add(folder);

                //            }

                //        });
                //    });
                //}



            }
            return folders;

        }

        private List<long> GetChildFolders(List<Folders> filterFolders, List<long> folderIds)
        {
            List<Folders> folders = new List<Folders>();
            filterFolders.ForEach(f =>
            {
                var folderItems = _context.Folders.Where(t => (t.ParentFolderId == f.FolderId) && t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();

                if (folderItems.Any())
                {
                    folderItems.ForEach(f =>
                    {
                        folderIds.Add(f.FolderId);
                    });
                    GetChildFolders(folderItems, folderIds);
                }
            });

            return folderIds.Distinct().ToList();
        }

        private List<long> GetChildFoldersDocPath(List<Folders> filterFolders, List<long> folderIds)
        {
            List<Folders> folders = new List<Folders>();
            filterFolders.ForEach(f =>
            {
                if (f.ParentFolderId != null)
                {
                    var folderItems = _context.Folders.Where(t => (t.FolderId == f.ParentFolderId) && t.FolderTypeId == 524 && t.StatusCodeId == 1).AsNoTracking().ToList();

                    if (folderItems.Any())
                    {
                        folderItems.ForEach(f =>
                        {
                            folderIds.Add(f.FolderId);
                        });
                        GetChildFoldersDocPath(folderItems, folderIds);
                    }
                }
            });

            return folderIds.Distinct().ToList();
        }
        private List<FoldersModel> GenerateFoldePath(List<FoldersModel> folderslist)
        {
            List<FoldersModel> folderLocation = new List<FoldersModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in folderslist)
            {
                FoldersModel f = new FoldersModel();

                if (fn.ParentFolderID > 0)
                {
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;
                    folderPath += folderPath != "" ? "/" + f.Name : f.Name;
                    f.FolderPath = folderPath;
                    folderLocation.Add(f);

                }
                else if (fn.ParentFolderID == null)
                {
                    folderPath = fn.Name;
                    f.FolderPath = folderPath;
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;
                    folderLocation.Add(f);
                }
            }
            return folderLocation.Distinct().ToList();
        }

        private List<FoldersModel> ParentPath(List<FoldersModel> folderslist)
        {
            List<FoldersModel> folderLocation = new List<FoldersModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in folderslist)
            {
                FoldersModel f = new FoldersModel();

                if (fn.ParentFolderID > 0)
                {
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;
                    f.ParentFolderPathName = folderslist.FirstOrDefault(t => t.FolderID == fn.ParentFolderID)?.Name + "/" + f.Name;
                    folderPath += folderPath != "" ? "/" + f.Name : f.Name;
                    f.FolderPath = folderPath;
                    folderLocation.Add(f);

                }
                else if (fn.ParentFolderID == null)
                {
                    folderPath = fn.Name;
                    f.FolderPath = folderPath;
                    f.FolderID = fn.FolderID;
                    f.Name = fn.Name;
                    f.ParentFolderID = fn.ParentFolderID;
                    folderLocation.Add(f);
                }
            }
            return folderLocation.Distinct().ToList();
        }
        [HttpGet]
        [Route("GetFolderDocumentPath")]
        public FoldersModel GetFolderDocumentPath(long? id)
        {
            //FoldersModel folderModel = new FoldersModel();
            //FoldersModel folder = new FoldersModel();
            var folder = _context.Folders.FirstOrDefault(f => f.FolderId == id);
            FoldersModel folderModel = new FoldersModel();
            if (folder != null)
            {
                folderModel.FolderID = folder.FolderId;
                folderModel.Name = folder.Name;
                folderModel.ParentFolderID = folder.ParentFolderId;
                folderModel.FolderTypeID = folder.FolderTypeId;
                folderModel.MainFolderID = folder.MainFolderId;
                folderModel.AddedByUserID = folder.AddedByUserId;
            }
            folderModel = GenerateDocumentPath(id, folderModel);

            return folderModel;
        }


        private FoldersModel GenerateDocumentPath(long? id, FoldersModel foldersModel)
        {
            FoldersModel folderLocation = new FoldersModel();
            var emptyFolderIds = new List<long>();
            GenerateDocumentLocation(id, foldersModel);
            if (foldersModel.FolderPaths != null)
            {
                foldersModel.FolderPaths.Reverse();
            }
            foldersModel.FolderPath = "";
            if (foldersModel.FolderPaths != null)
            {
                foreach (var item in foldersModel.FolderPaths)
                {
                    if (foldersModel.FolderPaths[foldersModel.FolderPaths.Count - 1] != item)
                    {
                        foldersModel.FolderPath += item + " / ";
                    }
                    else
                    {
                        foldersModel.FolderPath += item;
                    }
                }
            }
            folderLocation.FolderPath = foldersModel.FolderPath;

            return folderLocation;
        }
        private List<FoldersModel> GetFoldersIds(long? id)
        {
            List<FoldersModel> ids = new List<FoldersModel>();
            var folders = _context.Folders.Select(s => new FoldersModel { Name = s.Name, FolderID = s.FolderId, ParentFolderID = s.ParentFolderId }).ToList();
            var folderName = folders.FirstOrDefault(f => f.FolderID == id);
            if (folderName! != null)
            {
                ids.Add(folderName);
                ids = GetAllFolderIds(folders, folderName, ids);
            }
            return ids;
        }
        private List<FoldersModel> GetAllFolderIds(List<FoldersModel> foldersListItems, FoldersModel s, List<FoldersModel> foldersModel)
        {
            var items = foldersListItems.Where(f => f.ParentFolderID == s.FolderID).ToList();
            if (items != null)
            {
                items.ForEach(s =>
                {
                    GetAllFolderIds(foldersListItems, s, foldersModel);
                    foldersModel.Add(s);
                });
            }
            return foldersModel;
        }
        private List<string> GetAllFoldersName(List<FoldersModel> foldersListItems, FoldersModel s, List<string> path)
        {
            if (s.ParentFolderID != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderID == s.ParentFolderID);
                GetAllFoldersName(foldersListItems, items, path);
                path.Add(items.Name);
            }
            return path;
        }
        [HttpPost]
        [Route("GetExcel")]
        public IActionResult GetExcel(SearchModel searchModel)
        {
            /* var foldersList = _context.Folders.Select(s => new FoldersModel { Name = s.Name, FolderID = s.FolderId, ParentFolderID = s.ParentFolderId }).ToList();
             List<FoldersModel> foldersModels = new List<FoldersModel>();
             foldersList.ForEach(f =>
             {
                 FoldersModel foldersModel = new FoldersModel();
                 List<string> FolderPathLists = new List<string>();
                 FolderPathLists.Add(f.Name);
                 FolderPathLists =GetAllFoldersName(foldersList, f, FolderPathLists);
                 foldersModel.FolderID = f.FolderID;
                 foldersModel.Name = string.Join("/", FolderPathLists);
                 foldersModels.Add(foldersModel);
             });
             var foldersModela = foldersModels;*/
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("File Name");
            headers.Add("Document Path");
            headers.Add("Description");
            headers.Add("Upload Date");
            headers.Add("Upload By");
            headers.Add("Is Notes");
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();

            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var sessionId = Guid.NewGuid();
            string FromLocation = folderName + @"\" + newFolderName + @"\" + sessionId + ".xlsx";
            var fileProfileTypes = GetFoldersIds(searchModel.FolderId).ToList();
            var fileProfileTypeIds = fileProfileTypes.Select(s => s.FolderID).ToList();

            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                int j = 1;
                fileProfileTypes.ForEach(i =>
                {
                    WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                    var sheetName = j.ToString() + ") " + (i.Name.Length > 25 ? i.Name.Substring(0, 25) : i.Name);
                    Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = sheetName };
                    sheets.Append(sheet);
                    SheetData sheetData = new SheetData();
                    wSheetPart.Worksheet = new Worksheet(sheetData);

                    Row headerRow = new Row();
                    Cell cellHeader = new Cell() { CellReference = "C1" };
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(i.Name));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    DocumentFormat.OpenXml.Spreadsheet.FontSize fontSize1 = new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 16 };
                    run1Properties.Append(fontSize1);
                    inlineString.Append(run1);

                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                    sheetData.AppendChild(headerRow);

                    MergeCells mergeCells = new MergeCells();
                    mergeCells.Append(new MergeCell() { Reference = new StringValue("C1:P1") });
                    wSheetPart.Worksheet.InsertAfter(mergeCells, wSheetPart.Worksheet.Elements<SheetData>().First());

                    Row headerSecondRow = new Row();
                    foreach (string column in headers)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.InlineString;
                        Run run2 = new Run();
                        run2.Append(new Text(column));
                        RunProperties run2Properties = new RunProperties();
                        run2Properties.Append(new Bold());
                        run2.RunProperties = run2Properties;
                        InlineString inlineStrings = new InlineString();
                        inlineStrings.Append(run2);
                        cell.Append(inlineStrings);
                        headerSecondRow.AppendChild(cell);
                    }
                    sheetData.AppendChild(headerSecondRow);
                    var folderDocuments = GetFolderFiles((int)i.FolderID, (int)searchModel.UserID);
                    folderDocuments.ForEach(d =>
                    {
                        Row row = new Row();
                        row.AppendChild(AddCellColumn(d.FileName));
                        row.AppendChild(AddCellColumn(d.DocumentPath));
                        row.AppendChild(AddCellColumn(d.Description));
                        row.AppendChild(AddCellColumn(d.LinkID > 0 ? d.UploadDate?.ToString("dd-MMM-yyyy hh:mm tt") : d.ModifiedDate?.ToString("dd-MMM-yyyy hh:mm tt")));
                        row.AppendChild(AddCellColumn(d.UploadedByUser));
                        row.AppendChild(AddCellColumn(d.IsDiscussionNotes == true ? "Yes" : "No"));
                        sheetData.AppendChild(row);
                    });
                    sheetId++;
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        private Cell AddCellColumn(string value)
        {
            Cell fileNamecell = new Cell();
            fileNamecell.DataType = CellValues.String;
            fileNamecell.CellValue = new CellValue(value);
            return fileNamecell;
        }
        [HttpPost]
        [Route("SaveFolderImage")]
        public IActionResult SaveFolderImage(IFormCollection files)
        {
            var documentId = Convert.ToInt32(files["Id"].ToString());
            var sessionId = Guid.NewGuid();
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            string FromLocation = folderName + @"\" + newFolderName + @"\" + sessionId + ".png";
            string ExcelLocation = folderName + @"\" + newFolderName + @"\" + sessionId + ".xlsx";
            var folders = _context.Folders.FirstOrDefault(a => a.FolderId == documentId);
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                using (var targetStream = System.IO.File.Create(FromLocation))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                fs.Close();
            });
            using (var excel = new ExcelPackage())
            {
                var wks = excel.Workbook.Worksheets.Add(folders?.Name);
                var pic = wks.Drawings.AddPicture(folders?.Name, new FileInfo(FromLocation));
                pic.SetPosition(2, 0, 1, 0);
                excel.SaveAs(new FileInfo(ExcelLocation));
            }
            System.IO.File.Delete(FromLocation);
            FileStream stream = System.IO.File.OpenRead(ExcelLocation);
            var brs = new BinaryReader(stream);
            Byte[] documentStream = brs.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete(ExcelLocation);
            return Ok(streams);
        }
    }
}
