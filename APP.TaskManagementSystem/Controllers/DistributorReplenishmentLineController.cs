﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.BussinessObject;
using System.Text.Json;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DistributorReplenishmentLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public DistributorReplenishmentLineController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]
        [Route("GetDistributorReplenishmentVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<DistributorReplenishmentLineModel>>> GetDistributorReplenishmentVersion(string sessionID)
        {
            return await _repository.GetList<DistributorReplenishmentLineModel>(sessionID);
        }

        [HttpGet]
        [Route("GetDistributorReplenishmentLineById")]
        public List<DistributorReplenishmentLineModel> Get(long? id)
        {
            var distributorReplenishment = _context.DistributorReplenishmentLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)   
                .Include(s=>s.ProductName)
                .Where(s=>s.DistributorReplenishmentId == id)
                .AsNoTracking().ToList();
            List<DistributorReplenishmentLineModel> DistributorReplenishmentLineModel = new List<DistributorReplenishmentLineModel>();
            distributorReplenishment.ForEach(s =>
            {
                DistributorReplenishmentLineModel DistributorReplenishmentLineModels = new DistributorReplenishmentLineModel
                {
                    DistributorReplenishmentId = s.DistributorReplenishmentId,
                    DistributorReplenishmentLineId = s.DistributorReplenishmentLineId,
                    IsExceptionalItem = s.IsExceptionalItem,
                    ReplenishmentTiming = s.ReplenishmentTiming,
                    NeedVersionChecking = s.NeedVersionChecking,
                    NoOfMonthToReplenish = s.NoOfMonthToReplenish,
                    NeedVersionCheckingFlag = s.NeedVersionChecking == true ? "Yes" : "No",
                    MaxHoldingStock = s.MaxHoldingStock,
                    ProductNameId = s.ProductNameId,
                    ProductName = s.ProductName?.Code,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    SessionId = s.SessionId,
                };
                DistributorReplenishmentLineModel.Add(DistributorReplenishmentLineModels);
            });
            return DistributorReplenishmentLineModel.OrderByDescending(a => a.DistributorReplenishmentId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DistributorReplenishmentLineModel> GetData(SearchModel searchModel)
        {
            var distributorReplenishment = new DistributorReplenishmentLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).LastOrDefault();
                        break;
                    case "Next":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).LastOrDefault();
                        break;
                    case "Previous":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).LastOrDefault();
                        break;
                    case "Next":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderBy(o => o.DistributorReplenishmentLineId).FirstOrDefault(s => s.DistributorReplenishmentLineId > searchModel.Id);
                        break;
                    case "Previous":
                        distributorReplenishment = _context.DistributorReplenishmentLine.OrderByDescending(o => o.DistributorReplenishmentLineId).FirstOrDefault(s => s.DistributorReplenishmentLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DistributorReplenishmentLineModel>(distributorReplenishment);
            if (result != null)
            {
                result.NeedVersionCheckingFlag = result.NeedVersionChecking == true ? "Yes" : "No";
            }
            return result;
        }
        [HttpPost]
        [Route("InsertDistributorReplenishmentLine")]
        public DistributorReplenishmentLineModel Post(DistributorReplenishmentLineModel value)
        {
            var SessionId = Guid.NewGuid();
            //var distributorReplenishments = _context.DistributorReplenishmentLine.ToList();
           
            var distributorReplenishment = new DistributorReplenishmentLine
            {
                DistributorReplenishmentId = value.DistributorReplenishmentId,
                MaxHoldingStock = value.MaxHoldingStock,
                IsExceptionalItem = value.IsExceptionalItem,
                ProductNameId = value.ProductNameId,
                ReplenishmentTiming = value.ReplenishmentTiming,
                NoOfMonthToReplenish = value.NoOfMonthToReplenish,
                NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            _context.DistributorReplenishmentLine.Add(distributorReplenishment);
            _context.SaveChanges();
            value.DistributorReplenishmentLineId = distributorReplenishment.DistributorReplenishmentLineId;
            return value;
        }



        [HttpPut]
        [Route("UpdateDistributorReplenishmentLine")]
        public async Task<DistributorReplenishmentLineModel> Put(DistributorReplenishmentLineModel value)
        {

            value.SessionId ??= Guid.NewGuid();
            var distributorReplenishment = _context.DistributorReplenishmentLine.SingleOrDefault(p => p.DistributorReplenishmentLineId == value.DistributorReplenishmentLineId);
            
            distributorReplenishment.ReplenishmentTiming = value.ReplenishmentTiming;
            distributorReplenishment.NoOfMonthToReplenish = value.NoOfMonthToReplenish;
            distributorReplenishment.NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false;
            distributorReplenishment.IsExceptionalItem = value.IsExceptionalItem;
            distributorReplenishment.ProductNameId = value.ProductNameId;
            distributorReplenishment.MaxHoldingStock = value.MaxHoldingStock;
            distributorReplenishment.StatusCodeId = value.StatusCodeID.Value;
            distributorReplenishment.ModifiedByUserId = value.AddedByUserID;
            distributorReplenishment.ModifiedDate = DateTime.Now;
            distributorReplenishment.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteDistributorReplenishmentLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var distributorReplenishment = _context.DistributorReplenishmentLine.Where(p => p.DistributorReplenishmentLineId == id).FirstOrDefault();
                if (distributorReplenishment != null)
                {
                    //if (distributorReplenishment.SessionId != null)
                    //{
                    //    var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == distributorReplenishment.SessionId).ToList();
                    //    _context.TableDataVersionInfo.RemoveRange(versions);

                    //}
                    _context.DistributorReplenishmentLine.Remove(distributorReplenishment);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetGenericCodesDistributorReplenishment")]
        public List<GenericCodesModel> GetGenericCodesDistributorReplenishment(DistributorGenericCodeModel distributorGenericCodeModel)
        {
            List<ApplicationMasterDetail> applicationMasterDetails = new List<ApplicationMasterDetail>();
            List<long?> applicationMasterCodeids = new List<long?> { 103, 234, 235 };
            var genericcodeManufacturerIds = _context.ProductGroupingManufacture.Where(p =>p.DosageFormId!=null && p.DosageFormId == distributorGenericCodeModel.DosageFormId && p.DrugClassificationId!=null && p.DrugClassificationId == distributorGenericCodeModel.DrugClassificationId).Select(s => s.ProductGroupingId).ToList();
            var applicationMasterIds = _context.ApplicationMaster.Where(a => applicationMasterCodeids.Contains(a.ApplicationMasterCodeId)).Select(a => a.ApplicationMasterId).ToList();
            if (applicationMasterIds != null && applicationMasterIds.Count > 0)
            {
                applicationMasterDetails = _context.ApplicationMasterDetail.Where(a => applicationMasterIds.Contains(a.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var genericCodes = _context.GenericCodes.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(g => g.StatusCodeId == 1 && genericcodeManufacturerIds.Contains(g.GenericCodeId)).ToList();
            List<GenericCodesModel> GenericCodesModel = new List<GenericCodesModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodesModel GenericCodesModels = new GenericCodesModel
                {
                    GenericCodeID = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    ProfileNo = s.ProfileNo,
                    PackingCode = s.PackingCode,
                    Uom = s.Uom,
                    Name = s.Code + "|" + s.Description,
                    UomName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null,
                };
                GenericCodesModel.Add(GenericCodesModels);
            });

            return GenericCodesModel.OrderByDescending(o => o.GenericCodeID).ToList();
        }

    }
}
