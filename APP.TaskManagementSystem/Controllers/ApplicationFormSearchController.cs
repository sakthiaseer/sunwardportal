﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationFormSearchController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationFormSearchController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        private dynamic ExportHeadersList(int ? id)
        {
            dynamic jsonList = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)jsonList;
            var applicationformsearch = _context.ApplicationFormSearch.Where(s => s.MainFormId == id).AsNoTracking().ToList();
            if (applicationformsearch.Count > 0)
            {
                applicationformsearch.ForEach(h => {
                    dictionary.Add(h.DisplayName, Char.ToLowerInvariant(h.ColumnName[0]) + h.ColumnName.Substring(1));
                });
            }
            return jsonList;
        }
        private List<ItemHeaderModel> GenerateCodeHeaders(int? id)
        {
            var applicationformsearch = _context.ApplicationFormSearch.Where(s=>s.MainFormId==id).AsNoTracking().ToList();
            List<ItemHeaderModel> itemHeaders = new List<ItemHeaderModel>();
            if(applicationformsearch.Count>0)
            {
                applicationformsearch.ForEach(h=>{
                    itemHeaders.Add(new ItemHeaderModel { Text =h.DisplayName, Value = Char.ToLowerInvariant(h.ColumnName[0]) + h.ColumnName.Substring(1), Align = "left", Sortable = true });
                });
            }
            return itemHeaders;
        }
        [HttpGet]
        [Route("GetSearchDetail")]
        public ProductionBatchInformationDetailList GetSearchDetail(int? id)
        {
            var productionBatchInformation = _context.ProductionBatchInformation.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.ProductionBatchInformationLine).Include("ProductionBatchInformationLine.Item").AsNoTracking().ToList();
            ProductionBatchInformationDetailList productionBatchInfoList = new ProductionBatchInformationDetailList();
            productionBatchInfoList.ItemHeaderModels = GenerateCodeHeaders(id);
            productionBatchInfoList.ItemJsonHeaders = ExportHeadersList(id);
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            if (productionBatchInformation.Count > 0)
            {
                productionBatchInformation.ForEach(s =>
                {
                    if (s.ProductionBatchInformationLine.Count > 0)
                    {
                        s.ProductionBatchInformationLine.ToList().ForEach(p => {
                            var infoItems = new ProductionBatchInformationDetailModel
                            {
                                ItemName = p.Item?.No + "|" + p.Item?.Description + "|" + p.Item?.ProductionBomno,
                                TicketNo = s.TicketNo,
                                ProductionOrderNo = s.ProductionOrderNo,
                                ManufacturingStartDate = String.Format("{0:dd-MM-yyyy}", s.ManufacturingStartDate),
                                BatchSizeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BatchSizeId).Select(a => a.Value).SingleOrDefault() : "",
                                UnitsofBatchSizeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.UnitsofBatchSize).Select(a => a.Value).SingleOrDefault() : "",
                                BatchNo = p.BatchNo,
                                QtyPack = p.QtyPack,
                                QtyPackUnitsName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == p.QtyPackUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                                ExpiryDate = String.Format("{0:dd-MM-yyyy}", p.ExpiryDate),
                        };
                            productionBatchInfoList.ProductionBatchInformationDetail.Add(infoItems);
                        });
                    }
                    else
                    {
                        var infoItems = new ProductionBatchInformationDetailModel
                        {
                            TicketNo = s.TicketNo,
                            ProductionOrderNo = s.ProductionOrderNo,
                            ManufacturingStartDate = String.Format("{0:dd-MM-yyyy}", s.ManufacturingStartDate),
                            BatchSizeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BatchSizeId).Select(a => a.Value).SingleOrDefault() : "",
                            UnitsofBatchSizeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.UnitsofBatchSize).Select(a => a.Value).SingleOrDefault() : "",
                        };
                        productionBatchInfoList.ProductionBatchInformationDetail.Add(infoItems);
                    }
                });
            }
            return productionBatchInfoList;
        }
    }
}