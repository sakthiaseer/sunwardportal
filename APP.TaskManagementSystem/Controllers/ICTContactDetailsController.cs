﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ICTContactDetailsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ICTContactDetailsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetICTContactDetails")]
        public List<ICTContactDetailsModel> Get()
        {
            var contactDetails = _context.IctcontactDetails.Include("AddedByUser").Include("ModifiedByUser").AsNoTracking().Select(s => new ICTContactDetailsModel
            {
               
                ContactDetailsID = s.ContactDetailsId,
                Department = s.Department,
                Salutation = s.Salutation,
                ContactName = s.ContactName,
                Phone = s.Phone.Value,
                VendorsListID = s.VendorsListId.Value,
                SourceListID = s.SourceListId.Value,
                ContactTypeID = s.ContactTypeId.Value,
                Email = s.Email,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
               

            }).OrderByDescending(o => o.ContactDetailsID).ToList();
            //var result = _mapper.Map<List<ICTContactDetailsModel>>(ICTContactDetails);
            return contactDetails;
        }
        [HttpGet]
        [Route("GetActiveICTContactDetails")]
        public List<ICTContactDetailsModel> GetActiveICTContactDetails()
        {
            var contactDetails = _context.IctcontactDetails.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).AsNoTracking().Select(s => new ICTContactDetailsModel
            {
                ContactDetailsID = s.ContactDetailsId,
                Department = s.Department,
                Salutation = s.Salutation,
                ContactName = s.ContactName,
                Phone = s.Phone.Value,
                VendorsListID = s.VendorsListId.Value,
                SourceListID = s.SourceListId.Value,
                ContactTypeID = s.ContactTypeId.Value,
                Email = s.Email,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,

            }).OrderByDescending(o => o.ContactDetailsID).ToList();
            //var result = _mapper.Map<List<ICTContactDetailsModel>>(ICTContactDetails);
            return contactDetails;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<ICTContactDetailsModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == id.Value);
            var result = _mapper.Map<ICTContactDetailsModel>(contactDetails);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ICTContactDetailsModel> GetData(SearchModel searchModel)
        {
            var contactDetails = new IctcontactDetails();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).FirstOrDefault();
                        break;
                    case "Last":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).LastOrDefault();
                        break;
                    case "Next":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).LastOrDefault();
                        break;
                    case "Previous":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).FirstOrDefault();
                        break;
                    case "Last":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).LastOrDefault();
                        break;
                    case "Next":
                        contactDetails = _context.IctcontactDetails.OrderBy(o => o.ContactDetailsId).FirstOrDefault(s => s.ContactDetailsId > searchModel.Id);
                        break;
                    case "Previous":
                        contactDetails = _context.IctcontactDetails.OrderByDescending(o => o.ContactDetailsId).FirstOrDefault(s => s.ContactDetailsId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ICTContactDetailsModel>(contactDetails);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertICTContactDetails")]
        public ICTContactDetailsModel Post(ICTContactDetailsModel value)
        {
            var contactDetails = new IctcontactDetails
            {

                Department = value.Department,
                Salutation = value.Salutation,
                ContactName = value.ContactName,
                Phone = value.Phone,
                VendorsListId = value.VendorsListID,
                SourceListId = value.SourceListID,
                Email = value.Email,
                //ContactTypeId = value.ContactTypeID
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
               

            };
            _context.IctcontactDetails.Add(contactDetails);
            _context.SaveChanges();
            value.ContactDetailsID = contactDetails.ContactDetailsId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateICTContactDetails")]
        public ICTContactDetailsModel Put(ICTContactDetailsModel value)
        {
            var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == value.ContactDetailsID);
            contactDetails.Department = value.Department;
            contactDetails.Salutation = value.Salutation;
            // ICTContactDetails.ContactDetailsId = value.ContactDetailsID;
            contactDetails.ContactName = value.ContactName;
            contactDetails.Phone = value.Phone;
            contactDetails.VendorsListId = value.VendorsListID;
            contactDetails.SourceListId = value.SourceListID;
            contactDetails.Email = value.Email;
            //ICTContactDetails.ContactTypeId = value.ContactTypeID;
            contactDetails.ModifiedByUserId = value.ModifiedByUserID;
            contactDetails.ModifiedDate = DateTime.Now;

            contactDetails.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteICTContactDetails")]
        public void Delete(int id)
        {
            var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == id);
            if (contactDetails != null)
            {
                _context.IctcontactDetails.Remove(contactDetails);
                _context.SaveChanges();
            }
        }
    }
}