﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data.Services.Client;
using NAV;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SampleRequestFormController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IHubContext<ChatHub> _hub;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public SampleRequestFormController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IHubContext<ChatHub> hub, IConfiguration config, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hub = hub;
            _config = config;
            _hostingEnvironment = host;
        }

        #region SampleRequestForm

        [HttpGet]
        [Route("GetSampleRequestForm")]
        public List<SampleRequestFormModel> Get()
        {

            List<SampleRequestFormModel> sampleRequestFormModels = new List<SampleRequestFormModel>();
            List<ApplicationMasterDetail> applicationMasterDetatil = new List<ApplicationMasterDetail>();

            var sampleRequestForm = _context.SampleRequestForm.Include("AddedByUser").Include(s => s.TypeOfRequest).Include(s => s.InventoryType).Include(i => i.Person).Include(c => c.Company).Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.SampleRequestFormId).AsNoTracking().ToList();
            if (sampleRequestForm != null && sampleRequestForm.Count > 0)
            {
                var masterDetailIds = sampleRequestForm.Where(s => s.PurposeSampleRequestId != null).Select(s => s.PurposeSampleRequestId).ToList();
                if (masterDetailIds.Count > 0)
                {
                    applicationMasterDetatil = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                sampleRequestForm.ForEach(s =>
                {
                    SampleRequestFormModel sampleRequestFormModel = new SampleRequestFormModel
                    {
                        SampleRequestFormId = s.SampleRequestFormId,
                        CompanyId = s.CompanyId,
                        TypeOfRequestId = s.TypeOfRequestId,
                        TypeOfRequest = s.TypeOfRequest?.Value,
                        InventoryTypeId = s.InventoryTypeId,
                        InventoryType = s.InventoryType?.Name,
                        CompanyName = s.Company?.PlantCode,
                        ProfileNo = s.ProfileNo,
                        PersonId = s.PersonId,
                        Person = s.Person != null ? s.Person.NickName : "",
                        PurposeSampleRequestId = s.PurposeSampleRequestId,
                        PurposeSampleRequest = applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeSampleRequestId) != null ? applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeSampleRequestId).Value : string.Empty,
                        AdditionalRequirement = s.AdditionalRequirement,
                        DueDate = s.DueDate,
                        SessionId = s.SessionId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    sampleRequestFormModels.Add(sampleRequestFormModel);
                });
            }

            return sampleRequestFormModels;
        }

        [HttpGet]
        [Route("GetRequestForPersonById")]
        public List<SampleRequestFormModel> GetRequestForPersonById(int id)
        {
            List<SampleRequestFormModel> sampleRequestFormModels = new List<SampleRequestFormModel>();
            List<ApplicationMasterDetail> applicationMasterDetatil = new List<ApplicationMasterDetail>();
            var employeeId = _context.Employee.FirstOrDefault(e => e.UserId == id)?.EmployeeId;
            var sampleRequestForm = _context.SampleRequestForm.Include("AddedByUser").Include(i => i.Person).Include(c => c.Company).Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.SampleRequestFormId).Where(u => u.PersonId == employeeId).AsNoTracking().ToList();

            if (sampleRequestForm != null && sampleRequestForm.Count > 0)
            {
                var sampleRequestIds = sampleRequestForm.Select(s => s.SampleRequestFormId).ToList();
                if (sampleRequestIds != null && sampleRequestIds.Count > 0)
                {
                    var samplelinelists = _context.SampleRequestFormLine.Where(t => sampleRequestIds.Contains(t.SampleRequestFormId.Value)).AsNoTracking().ToList();
                    var masterDetailIds = sampleRequestForm.Where(s => s.PurposeSampleRequestId != null).Select(s => s.PurposeSampleRequestId).ToList();
                    if (masterDetailIds.Count > 0)
                    {
                        applicationMasterDetatil = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                    }
                    sampleRequestForm.ForEach(s =>
                    {
                        SampleRequestFormModel sampleRequestFormModel = new SampleRequestFormModel
                        {
                            SampleRequestFormId = s.SampleRequestFormId,
                            CompanyId = s.CompanyId,
                            CompanyName = s.Company?.PlantCode,
                            TypeOfRequestId = s.TypeOfRequestId,
                            TypeOfRequest = s.TypeOfRequest?.Value,
                            InventoryTypeId = s.InventoryTypeId,
                            InventoryType = s.InventoryType?.Name,
                            ProfileNo = s.ProfileNo,
                            PersonId = s.PersonId,
                            Person = s.Person != null ? s.Person.NickName : "",
                            PurposeSampleRequestId = s.PurposeSampleRequestId,
                            PurposeSampleRequest = applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeSampleRequestId) != null ? applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeSampleRequestId).Value : string.Empty,
                            AdditionalRequirement = s.AdditionalRequirement,
                            DueDate = s.DueDate,
                            SessionId = s.SessionId,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                        };
                        var sampleline = samplelinelists.Where(l => l.SampleRequestFormId == sampleRequestFormModel.SampleRequestFormId).ToList();
                        if (sampleline != null && sampleline.Count > 0)
                        {
                            sampleRequestFormModels.Add(sampleRequestFormModel);
                        }

                    });
                }
            }

            return sampleRequestFormModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SampleRequestFormModel> GetData(SearchModel searchModel)
        {
            var sampleRequestForm = new SampleRequestForm();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).FirstOrDefault();
                        break;
                    case "Last":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).LastOrDefault();
                        break;
                    case "Next":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).LastOrDefault();
                        break;
                    case "Previous":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).FirstOrDefault();
                        break;
                    case "Last":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).LastOrDefault();
                        break;
                    case "Next":
                        sampleRequestForm = _context.SampleRequestForm.OrderBy(o => o.SampleRequestFormId).FirstOrDefault(s => s.SampleRequestFormId > searchModel.Id);
                        break;
                    case "Previous":
                        sampleRequestForm = _context.SampleRequestForm.OrderByDescending(o => o.SampleRequestFormId).FirstOrDefault(s => s.SampleRequestFormId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SampleRequestFormModel>(sampleRequestForm);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSampleRequestForm")]
        public async Task<SampleRequestFormModel> Post(SampleRequestFormModel value)
        {
            var SessionId = Guid.NewGuid();
            value.ProfileId = _context.ApplicationForm.FirstOrDefault(a => a.ScreenId == value.ScreenId).ProfileId;
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "SampleRequestForm" });
            var sampleRequestForm = new SampleRequestForm
            {
                CompanyId = value.CompanyId,
                ProfileNo = profileNo,
                PersonId = value.PersonId,
                PurposeSampleRequestId = value.PurposeSampleRequestId,
                AdditionalRequirement = value.AdditionalRequirement,
                DueDate = value.DueDate,
                SessionId = SessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                NavLocationId = value.NavLocationId,
                TypeOfRequestId = value.TypeOfRequestId,
                InventoryTypeId = value.InventoryTypeId,

            };
            _context.SampleRequestForm.Add(sampleRequestForm);
            _context.SaveChanges();
            value.SampleRequestFormId = sampleRequestForm.SampleRequestFormId;
            value.SessionId = SessionId;
            value.ProfileNo = profileNo;

            var userId = _context.Employee.FirstOrDefault(f => f.EmployeeId == value.PersonId).UserId;
            await _hub.Clients.Group(userId.ToString()).SendAsync("requestFormNotification", "Completed");
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSampleRequestForm")]
        public async Task<SampleRequestFormModel> Put(SampleRequestFormModel value)
        {
            var sampleRequestForm = _context.SampleRequestForm.SingleOrDefault(p => p.SampleRequestFormId == value.SampleRequestFormId);
            value.SessionId ??= Guid.NewGuid();
            sampleRequestForm.CompanyId = value.CompanyId;
            sampleRequestForm.PersonId = value.PersonId;
            sampleRequestForm.PurposeSampleRequestId = value.PurposeSampleRequestId;
            sampleRequestForm.AdditionalRequirement = value.AdditionalRequirement;
            sampleRequestForm.DueDate = value.DueDate;
            sampleRequestForm.SessionId = value.SessionId;
            sampleRequestForm.ModifiedByUserId = value.ModifiedByUserID;
            sampleRequestForm.ModifiedDate = DateTime.Now;
            sampleRequestForm.StatusCodeId = value.StatusCodeID.Value;
            sampleRequestForm.NavLocationId = value.NavLocationId;
            sampleRequestForm.InventoryTypeId = value.InventoryTypeId;
            sampleRequestForm.TypeOfRequestId = value.TypeOfRequestId;
            _context.SaveChanges();

            var userId = _context.Employee.FirstOrDefault(f => f.EmployeeId == value.PersonId).UserId;

            await _hub.Clients.Group(userId.ToString()).SendAsync("requestFormNotification", "Completed");
            if (value.SampleRequestFormId > 0)
            {
                var sampleRequestFormlinslist = _context.SampleRequestFormLine.Where(s => s.SampleRequestFormId == value.SampleRequestFormId).AsNoTracking().ToList();
                var sampleRequestlinesStaus = sampleRequestFormlinslist?.Where(s => s.StatusCodeId == 2301).ToList();

                if (sampleRequestFormlinslist.Count > 0 && sampleRequestlinesStaus.Count > 0)
                {
                    if (sampleRequestFormlinslist.Count == sampleRequestlinesStaus.Count)
                    {
                        var exitsampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (exitsampleRequestForm != null)
                        {
                            exitsampleRequestForm.StatusCodeId = 2302;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        var exitsampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (exitsampleRequestForm != null)
                        {
                            exitsampleRequestForm.StatusCodeId = 2301;
                            _context.SaveChanges();
                        }
                    }
                }

            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSampleRequestForm")]
        public void Delete(int id)
        {
            var sampleRequestForm = _context.SampleRequestForm.Include(c => c.SampleRequestFormLine).SingleOrDefault(p => p.SampleRequestFormId == id);
            if (sampleRequestForm != null)
            {
                _context.SampleRequestForm.Remove(sampleRequestForm);
                _context.SaveChanges();
            }
        }

        #endregion

        #region SampleRequestFormLine

        [HttpGet]
        [Route("GetSampleRequestFormLinesById")]
        public List<SampleRequestFormLineModel> GetSampleRequestFormLinesById(int id)
        {
            var sampleRequestFormLines = _context.SampleRequestFormLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Item)
                .Include(i => i.IssueStatus)
                .Include(i => i.NavCompany)
                .Include(h => h.IssueRequestSampleLine)
                .Where(s => s.SampleRequestFormId == id)
                .AsNoTracking()
                .ToList();
            var issueSamplelist = _context.IssueRequestSampleLine.Include(s => s.AddedByUser).Include(s => s.NavCompany).AsNoTracking().ToList();
            List<IssueRequestSampleLine> issueRequestSampleLines = new List<IssueRequestSampleLine>();
            List<SampleRequestFormLineModel> sampleRequestFormLineModels = new List<SampleRequestFormLineModel>();
            sampleRequestFormLines.ForEach(s =>
            {
                decimal? issueRequestLineIssueQty = s.IssueRequestSampleLine != null ? s.IssueRequestSampleLine.Where(s => s.SampleRequestLineId == s.SampleRequestLineId && s.IssueQuantity != null).Sum(s => s.IssueQuantity).Value : 0;
                decimal? issueBalance = s.QtyRequire - issueRequestLineIssueQty;
                SampleRequestFormLineModel sampleRequestFormLineModel = new SampleRequestFormLineModel
                {
                    SampleRequestFormId = s.SampleRequestFormId,
                    SampleRequestFormLineId = s.SampleRequestFormLineId,
                    BatchNo = s.BatchNo,
                    ItemId = s.ItemId,
                    Item = s.Item != null ? s.Item.No : "",
                    Description = s.Item != null ? s.Item.Description : "",
                    QtyRequire = s.QtyRequire,
                    QtyIssue = s.QtyIssue,

                    MfgDate = s.MfgDate,
                    ExpiryDate = s.ExpiryDate,
                    Uom = s.Item?.BaseUnitofMeasure,
                    SpecialNotes = s.SpecialNotes,
                    SpecialNotesIssue = s.SpecialNotesIssue,
                    StatusCodeID = issueBalance > 0 ? s.StatusCodeId : 3102,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = issueBalance > 0 ? (s.StatusCode != null ? s.StatusCode.CodeValue : "") : "Issued",
                    IsSpecificBatch = s.IsSpecificBatch,
                    InStockSameItem = s.InStockSameItem,
                    Factor = s.Factor,
                    InventoryItemId = s.InventoryItemId,
                    TotalIssueQty = s.TotalIssueQty,
                    NavCompanyId = s.NavCompanyId,
                    IssueStatusId = s.IssueStatusId,
                    NavLocation = s.NavCompany?.Name,
                    IssueStatus = s.IssueStatus?.CodeValue,
                };
                issueRequestSampleLines = issueSamplelist?.Where(i => i.SampleRequestLineId == s.SampleRequestFormLineId).ToList();
                List<IssueRequestSampleLineModel> issueRequestSampleLineModels = new List<IssueRequestSampleLineModel>();
                if (issueRequestSampleLines != null && issueRequestSampleLines.Count > 0)
                {

                    issueRequestSampleLines.ForEach(l =>
                    {
                        IssueRequestSampleLineModel issueRequestSampleLineModel = new IssueRequestSampleLineModel();

                        issueRequestSampleLineModel.SampleRequestLineId = l.SampleRequestLineId;
                        issueRequestSampleLineModel.IssueRequestSampleLineId = l.IssueRequestSampleLineId;
                        issueRequestSampleLineModel.IssueQuantity = l.IssueQuantity;
                        issueRequestSampleLineModel.BatchNo = l.BatchNo;
                        issueRequestSampleLineModel.SpecialNotes = l.SpecialNotes;
                        issueRequestSampleLineModel.AddedByUserID = l.AddedByUserId;
                        issueRequestSampleLineModel.AddedByUser = l.AddedByUser?.UserName;
                        issueRequestSampleLineModel.AddedDate = l.AddedDate;
                        issueRequestSampleLineModel.NavcompanyId = l.NavCompanyId;
                        issueRequestSampleLineModel.NavLocation = l.NavCompany?.Name;
                        issueRequestSampleLineModels.Add(issueRequestSampleLineModel);
                    });
                    sampleRequestFormLineModel.IssueRequestSampleLineModels = new List<IssueRequestSampleLineModel>();
                    if (issueRequestSampleLineModels != null && issueRequestSampleLineModels.Count > 0)
                    {
                        sampleRequestFormLineModel.IssueRequestSampleLineModels.AddRange(issueRequestSampleLineModels);
                    }
                }
                sampleRequestFormLineModels.Add(sampleRequestFormLineModel);
            });
            return sampleRequestFormLineModels.OrderByDescending(a => a.SampleRequestFormLineId).ToList();
        }

        [HttpGet]
        [Route("GetIssueLinesById")]
        public List<IssueRequestSampleLineModel> GetIssueLinesById(int id)
        {
            List<IssueRequestSampleLineModel> issueRequestSampleLineModels = new List<IssueRequestSampleLineModel>();
            var issueSamplelist = _context.IssueRequestSampleLine.Include(s => s.AddedByUser).Include(s => s.NavCompany).Where(a => a.SampleRequestLineId == id).AsNoTracking().ToList();
            if (issueSamplelist != null && issueSamplelist.Count > 0)
            {

                issueSamplelist.ForEach(l =>
                {
                    IssueRequestSampleLineModel issueRequestSampleLineModel = new IssueRequestSampleLineModel();

                    issueRequestSampleLineModel.SampleRequestLineId = l.SampleRequestLineId;
                    issueRequestSampleLineModel.IssueRequestSampleLineId = l.IssueRequestSampleLineId;
                    issueRequestSampleLineModel.IssueQuantity = l.IssueQuantity;
                    issueRequestSampleLineModel.BatchNo = l.BatchNo;
                    issueRequestSampleLineModel.SpecialNotes = l.SpecialNotes;
                    issueRequestSampleLineModel.AddedByUserID = l.AddedByUserId;
                    issueRequestSampleLineModel.AddedByUser = l.AddedByUser?.UserName;
                    issueRequestSampleLineModel.AddedDate = l.AddedDate;
                    issueRequestSampleLineModel.NavcompanyId = l.NavCompanyId;
                    issueRequestSampleLineModel.NavLocation = l.NavCompany?.Name;
                    issueRequestSampleLineModels.Add(issueRequestSampleLineModel);
                });

            }
            return issueRequestSampleLineModels.OrderByDescending(a => a.IssueRequestSampleLineId).ToList();
        }
        [HttpPost]
        [Route("InsertSampleRequestFormLine")]
        public SampleRequestFormLineModel Post(SampleRequestFormLineModel value)
        {
            var sampleRequestFormlineExits = _context.SampleRequestFormLine
               .Where(w => w.BatchNo == value.BatchNo && w.ItemId == value.ItemId && w.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
            if (sampleRequestFormlineExits == null)
            {
                var sampleRequestFormLine = new SampleRequestFormLine
                {
                    SampleRequestFormId = value.SampleRequestFormId,
                    BatchNo = value.BatchNo,
                    ItemId = value.ItemId,
                    QtyRequire = value.QtyRequire,
                    SpecialNotes = value.SpecialNotes,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    IsSpecificBatch = value.IsSpecificBatch,
                    InStockSameItem = value.InStockSameItem,
                    Factor = value.Factor,
                    MfgDate = value.MfgDate,
                    ExpiryDate = value.ExpiryDate,
                    InventoryItemId = value.InventoryItemId,
                };
                _context.SampleRequestFormLine.Add(sampleRequestFormLine);
                _context.SaveChanges();
                value.SampleRequestFormLineId = sampleRequestFormLine.SampleRequestFormLineId;
            }
            else
            {
                sampleRequestFormlineExits.QtyRequire += value.QtyRequire;
                _context.SaveChanges();
            }
            if (value.SampleRequestFormId > 0)
            {
                var sampleRequestFormlinslist = _context.SampleRequestFormLine.Where(s => s.SampleRequestFormId == value.SampleRequestFormId).AsNoTracking().ToList();
                var sampleRequestlinesStaus = sampleRequestFormlinslist?.Where(s => s.StatusCodeId == 2301).ToList();

                if (sampleRequestFormlinslist.Count > 0 && sampleRequestlinesStaus.Count > 0)
                {
                    if (sampleRequestFormlinslist.Count == sampleRequestlinesStaus.Count)
                    {
                        var sampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (sampleRequestForm != null)
                        {
                            sampleRequestForm.StatusCodeId = 2302;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        var sampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (sampleRequestForm != null)
                        {
                            sampleRequestForm.StatusCodeId = 2301;
                            _context.SaveChanges();
                        }
                    }
                }

            }
            //if (value.ItemId != null && value.BatchNo != null)
            //{
            //    var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
            //    if (itemBatchinfo != null)
            //    {
            //        itemBatchinfo.QuantityOnHand = itemBatchinfo.QuantityOnHand + value.QtyRequire;
            //        _context.SaveChanges();
            //    }
            //    else if (itemBatchinfo == null)
            //    {
            //        var itembatchinfo = new ItemBatchInfo
            //        {
            //            ItemId = value.ItemId.Value,
            //            QuantityOnHand = value.QtyRequire,
            //            AddedByUserId = value.AddedByUserID,
            //            AddedDate = DateTime.Now,
            //            StatusCodeId = value.StatusCodeID.Value,
            //            BatchNo = value.BatchNo,
            //        };
            //        _context.ItemBatchInfo.Add(itembatchinfo);
            //        _context.SaveChanges();
            //    }
            //    var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();

            //    if (itemStockinfo == null)
            //    {
            //        var itemstockinfo = new ItemStockInfo
            //        {
            //            ItemId = value.ItemId.Value,
            //            QuantityOnHand = value.QtyRequire,
            //            AddedByUserId = value.AddedByUserID,
            //            AddedDate = DateTime.Now,
            //            StatusCodeId = value.StatusCodeID.Value,
            //        };
            //        _context.ItemStockInfo.Add(itemstockinfo);

            //    }
            //    else if (itemStockinfo != null)
            //    {
            //        itemStockinfo.QuantityOnHand = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId).Sum(t => t.QuantityOnHand);
            //    }

            //    _context.SaveChanges();
            //}
            value.HeaderStatus = _context.SampleRequestForm.Where(s => s.SampleRequestFormId == value.SampleRequestFormId)?.FirstOrDefault()?.StatusCodeId;

            return value;
        }
        [HttpPut]
        [Route("UpdateSampleRequestFormLine")]
        public SampleRequestFormLineModel Put(SampleRequestFormLineModel value)
        {
            /* if (value.IsRequestForPerson)
             {
                 var itemBatchInfo = _context.ItemBatchInfo.FirstOrDefault(b => b.BatchNo == value.BatchNo && b.ItemId == value.ItemId);
                 if (itemBatchInfo.IssueQuantity == null || itemBatchInfo.IssueQuantity == 0)
                 {
                     itemBatchInfo.IssueQuantity += value.QtyIssue;
                     itemBatchInfo.BalanceQuantity = itemBatchInfo.QuantityOnHand - itemBatchInfo.IssueQuantity;
                 }
                 else if (itemBatchInfo.IssueQuantity > 0)
                 {
                     var quantity = value.QtyIssue - itemBatchInfo.IssueQuantity;
                     if (quantity > 0)
                     {
                         itemBatchInfo.IssueQuantity += quantity;
                     }
                     else
                     {
                         itemBatchInfo.IssueQuantity += quantity;
                     }

                     itemBatchInfo.BalanceQuantity = itemBatchInfo.QuantityOnHand - itemBatchInfo.IssueQuantity;
                 }
                 var itemStockInfo = _context.ItemStockInfo.FirstOrDefault(b => b.ItemId == value.ItemId);
                 if (itemStockInfo.IssueQuantity == null || itemStockInfo.IssueQuantity == 0)
                 {
                     itemStockInfo.IssueQuantity += value.QtyIssue;
                     itemStockInfo.BalanceQuantity = itemStockInfo.QuantityOnHand - itemStockInfo.IssueQuantity;
                 }
                 else if (itemStockInfo.IssueQuantity > 0)
                 {
                     var quantity = value.QtyIssue - itemStockInfo.IssueQuantity;
                     if (quantity > 0)
                     {
                         itemStockInfo.IssueQuantity += quantity;
                     }
                     else
                     {
                         itemStockInfo.IssueQuantity += quantity;
                     }

                     itemStockInfo.BalanceQuantity = itemStockInfo.QuantityOnHand - itemStockInfo.IssueQuantity;
                 }
             }*/

            //var sampleRequestFormLine = new SampleRequestFormLine
            //{
            //    SampleRequestFormId = value.SampleRequestFormId,
            //    BatchNo = value.BatchNo,
            //    ItemId = value.ItemId,
            //    QtyRequire = value.QtyRequire,
            //    SpecialNotes = value.SpecialNotes,
            //    StatusCodeId = value.StatusCodeID.Value,
            //    AddedByUserId = value.AddedByUserID,
            //    AddedDate = DateTime.Now,
            //    IsSpecificBatch = value.IsSpecificBatch,
            //    InStockSameItem = value.InStockSameItem,
            //    Factor = value.Factor,
            //    MfgDate = value.MfgDate,
            //    ExpiryDate = value.ExpiryDate,
            //    InventoryItemId = value.InventoryItemId,
            //};
            //_context.SampleRequestFormLine.Add(sampleRequestFormLine);
            //_context.SaveChanges();
            var sampleRequestFormLine = _context.SampleRequestFormLine.SingleOrDefault(p => p.SampleRequestFormLineId == value.SampleRequestFormLineId);
            var differQty = value.QtyRequire - sampleRequestFormLine.QtyRequire;
            sampleRequestFormLine.SampleRequestFormId = value.SampleRequestFormId;
            sampleRequestFormLine.BatchNo = value.BatchNo;
            sampleRequestFormLine.ItemId = value.ItemId;
            sampleRequestFormLine.QtyRequire = value.QtyRequire;
            sampleRequestFormLine.QtyIssue = value.QtyIssue;
            sampleRequestFormLine.BalanceQty = (value.QtyRequire - value.QtyIssue);
            sampleRequestFormLine.MfgDate = value.MfgDate;
            sampleRequestFormLine.ExpiryDate = value.ExpiryDate;
            sampleRequestFormLine.SpecialNotes = value.SpecialNotes;
            sampleRequestFormLine.SpecialNotesIssue = value.SpecialNotesIssue;
            sampleRequestFormLine.StatusCodeId = value.StatusCodeID.Value;
            sampleRequestFormLine.ModifiedByUserId = value.ModifiedByUserID;
            sampleRequestFormLine.ModifiedDate = DateTime.Now;
            sampleRequestFormLine.IsSpecificBatch = value.IsSpecificBatch;
            sampleRequestFormLine.InStockSameItem = value.InStockSameItem;
            sampleRequestFormLine.Factor = value.Factor;
            sampleRequestFormLine.InventoryItemId = value.InventoryItemId;

            _context.SaveChanges();
            if (value.SampleRequestFormId > 0)
            {
                var sampleRequestFormlinslist = _context.SampleRequestFormLine.Where(s => s.SampleRequestFormId == value.SampleRequestFormId).AsNoTracking().ToList();
                var sampleRequestlinesStaus = sampleRequestFormlinslist?.Where(s => s.StatusCodeId == 2301).ToList();

                if (sampleRequestFormlinslist.Count > 0 && sampleRequestlinesStaus.Count > 0)
                {
                    if (sampleRequestFormlinslist.Count == sampleRequestlinesStaus.Count)
                    {
                        var sampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (sampleRequestForm != null)
                        {
                            sampleRequestForm.StatusCodeId = 2302;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        var sampleRequestForm = _context.SampleRequestForm.Where(a => a.SampleRequestFormId == value.SampleRequestFormId).FirstOrDefault();
                        if (sampleRequestForm != null)
                        {
                            sampleRequestForm.StatusCodeId = 2301;
                            _context.SaveChanges();
                        }
                    }
                }

            }
            value.HeaderStatus = _context.SampleRequestForm.Where(s => s.SampleRequestFormId == value.SampleRequestFormId)?.FirstOrDefault()?.StatusCodeId;
            return value;
        }


        [HttpPut]
        [Route("UpdateSampleRequestFormLineByRequest")]
        public async Task<SampleRequestFormLineModel> UpdateSampleRequestFormLineByRequest(SampleRequestFormLineModel value)
        {

            var sampleRequestFormLine = _context.SampleRequestFormLine.SingleOrDefault(p => p.SampleRequestFormLineId == value.SampleRequestFormLineId);

            sampleRequestFormLine.SampleRequestFormId = value.SampleRequestFormId;
            //sampleRequestFormLine.QtyIssue = value.QtyIssue;

            sampleRequestFormLine.SpecialNotes = value.SpecialNotes;
            sampleRequestFormLine.ModifiedByUserId = value.ModifiedByUserID;
            sampleRequestFormLine.ModifiedDate = DateTime.Now;
            sampleRequestFormLine.NavCompanyId = value.NavCompanyId;
            // sampleRequestFormLine.BatchNo = value.BatchNo;

            var issueRequestSampleline = new IssueRequestSampleLine
            {
                IssueQuantity = value.QtyIssue,
                BatchNo = value.BatchNo,
                SpecialNotes = value.SpecialNotes,
                SampleRequestLineId = value.SampleRequestFormLineId,
                AddedByUserId = value.ModifiedByUserID,
                AddedDate = DateTime.Now,
                NavCompanyId = value.NavCompanyId,
            };
            _context.IssueRequestSampleLine.Add(issueRequestSampleline);
            _context.SaveChanges();
            decimal? issueRequestLineIssueQty = _context.IssueRequestSampleLine.Where(s => s.SampleRequestLineId == value.SampleRequestFormLineId).Sum(s => s.IssueQuantity).Value;
            if (issueRequestLineIssueQty != null)
            {
                sampleRequestFormLine.TotalIssueQty = issueRequestLineIssueQty;
                sampleRequestFormLine.BalanceQty = (sampleRequestFormLine.QtyRequire - issueRequestLineIssueQty);
            }
            else if (issueRequestLineIssueQty == null)
            {
                sampleRequestFormLine.TotalIssueQty = value.QtyIssue;
                sampleRequestFormLine.BalanceQty = (sampleRequestFormLine.QtyRequire - value.QtyIssue);
            }
            if (sampleRequestFormLine.IsSpecificBatch == true)
            {

                if (sampleRequestFormLine.ItemId != null && sampleRequestFormLine.BatchNo != null)
                {
                    var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId && s.BatchNo == sampleRequestFormLine.BatchNo).FirstOrDefault();
                    if (itemBatchinfo != null)
                    {
                        itemBatchinfo.QuantityOnHand += value.QtyIssue;
                        _context.SaveChanges();
                    }
                    else if (itemBatchinfo == null)
                    {
                        var itembatchinfo = new DataAccess.Models.ItemBatchInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            BatchNo = sampleRequestFormLine.BatchNo,
                        };
                        _context.ItemBatchInfo.Add(itembatchinfo);
                        _context.SaveChanges();
                    }
                    var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId).FirstOrDefault();

                    if (itemStockinfo == null)
                    {
                        var itemstockinfo = new ItemStockInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,

                        };
                        _context.ItemStockInfo.Add(itemstockinfo);

                    }
                    else if (itemStockinfo != null)
                    {
                        itemStockinfo.QuantityOnHand += value.QtyIssue;
                    }

                }
                else if (sampleRequestFormLine.BatchNo == null)
                {
                    var batchNo = _context.InventoryTypeOpeningStockBalanceLine.FirstOrDefault(s => s.ItemId == sampleRequestFormLine.ItemId)?.BatchNo;
                    var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId && s.BatchNo == batchNo).FirstOrDefault();
                    if (itemBatchinfo != null)
                    {
                        itemBatchinfo.QuantityOnHand += value.QtyIssue;
                        _context.SaveChanges();
                    }
                    else if (itemBatchinfo == null)
                    {
                        var itembatchinfo = new DataAccess.Models.ItemBatchInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            BatchNo = batchNo,
                        };
                        _context.ItemBatchInfo.Add(itembatchinfo);
                        _context.SaveChanges();
                    }
                    var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId).FirstOrDefault();

                    if (itemStockinfo == null)
                    {
                        var itemstockinfo = new ItemStockInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,

                        };
                        _context.ItemStockInfo.Add(itemstockinfo);

                    }
                    else if (itemStockinfo != null)
                    {
                        itemStockinfo.QuantityOnHand += value.QtyIssue;
                    }
                }
            }
            else if (sampleRequestFormLine.IsSpecificBatch == false)
            {
                if (sampleRequestFormLine.ItemId != null && value.BatchNo != null)
                {
                    var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
                    if (itemBatchinfo != null)
                    {
                        itemBatchinfo.QuantityOnHand += value.QtyIssue;
                        _context.SaveChanges();
                    }
                    else if (itemBatchinfo == null)
                    {
                        var itembatchinfo = new DataAccess.Models.ItemBatchInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            BatchNo = value.BatchNo,
                        };
                        _context.ItemBatchInfo.Add(itembatchinfo);
                        _context.SaveChanges();
                    }
                    var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId).FirstOrDefault();

                    if (itemStockinfo == null)
                    {
                        var itemstockinfo = new ItemStockInfo
                        {
                            ItemId = sampleRequestFormLine.ItemId.Value,
                            QuantityOnHand = value.QtyIssue,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,

                        };
                        _context.ItemStockInfo.Add(itemstockinfo);

                    }
                    else if (itemStockinfo != null)
                    {
                        itemStockinfo.QuantityOnHand += value.QtyIssue;
                    }

                }
                sampleRequestFormLine.BatchNo = value.BatchNo;
                sampleRequestFormLine.QtyIssue = value.QtyIssue;
                _context.SaveChanges();

            }
            if (sampleRequestFormLine.QtyRequire != null && sampleRequestFormLine.TotalIssueQty != null)
            {
                if (sampleRequestFormLine.QtyRequire == sampleRequestFormLine.TotalIssueQty)
                {
                    sampleRequestFormLine.IssueStatusId = 3102;
                }
                else if (sampleRequestFormLine.QtyRequire > sampleRequestFormLine.TotalIssueQty)
                {
                    sampleRequestFormLine.IssueStatusId = 3101;
                }
            }
            else
            {
                sampleRequestFormLine.IssueStatusId = 3100;
            }
            _context.SaveChanges();
            var sampleRequestForm = _context.SampleRequestForm.Include(s => s.SampleRequestFormLine).FirstOrDefault(s => s.SampleRequestFormId == sampleRequestFormLine.SampleRequestFormId);
            if (sampleRequestForm.SampleRequestFormLine.All(l => l.IssueStatusId == 3102))
            {
                sampleRequestForm.StatusCodeId = 2302;
            }
            else if (sampleRequestForm.SampleRequestFormLine.Any(l => l.IssueStatusId == 3102))
            {
                sampleRequestForm.StatusCodeId = 2301;
            }
            else
            {
                sampleRequestForm.StatusCodeId = 2300;
            }
            _context.SaveChanges();
            value.IssueStatusId = sampleRequestFormLine.IssueStatusId;
            value.TotalIssueQty = sampleRequestFormLine.TotalIssueQty;
            if (value.IssueStatusId != null)
            {
                value.IssueStatus = _context.CodeMaster.Where(c => c.CodeId == value.IssueStatusId).FirstOrDefault()?.CodeValue;
            }
            value.HeaderStatus = _context.SampleRequestForm.Where(s => s.SampleRequestFormId == value.SampleRequestFormId)?.FirstOrDefault()?.StatusCodeId;
            var issueRequestsamplelines = _context.IssueRequestSampleLine.Include(s => s.AddedByUser).Include(s => s.NavCompany).Where(s => s.SampleRequestLineId == value.SampleRequestFormLineId).AsNoTracking().OrderByDescending(a => a.AddedDate).ToList();
            List<IssueRequestSampleLineModel> issueRequestSampleLineModels = new List<IssueRequestSampleLineModel>();
            if (issueRequestsamplelines != null && issueRequestsamplelines.Count > 0)
            {

                issueRequestsamplelines.ForEach(s =>
                {
                    IssueRequestSampleLineModel issueRequestSampleLineModel = new IssueRequestSampleLineModel();

                    issueRequestSampleLineModel.SampleRequestLineId = s.SampleRequestLineId;
                    issueRequestSampleLineModel.IssueRequestSampleLineId = s.IssueRequestSampleLineId;
                    issueRequestSampleLineModel.IssueQuantity = s.IssueQuantity;
                    issueRequestSampleLineModel.BatchNo = s.BatchNo;
                    issueRequestSampleLineModel.SpecialNotes = s.SpecialNotes;
                    issueRequestSampleLineModel.AddedByUserID = s.AddedByUserId;
                    issueRequestSampleLineModel.AddedByUser = s.AddedByUser?.UserName;
                    issueRequestSampleLineModel.AddedDate = s.AddedDate;
                    issueRequestSampleLineModel.NavcompanyId = s.NavCompanyId;
                    issueRequestSampleLineModel.NavLocation = s.NavCompany?.Name;
                    issueRequestSampleLineModels.Add(issueRequestSampleLineModel);
                });
            }
            value.IssueRequestSampleLineModels = new List<IssueRequestSampleLineModel>();
            if (issueRequestSampleLineModels != null && issueRequestSampleLineModels.Count > 0)
            {
                value.IssueRequestSampleLineModels.AddRange(issueRequestSampleLineModels);
            }

            var navstatus = await PostStockAdjuestment(value);
            if (!navstatus)
            {
                throw new Exception(exceptionMessage);
            }

            return value;
        }

        [HttpDelete]
        [Route("DeleteSampleRequestFormLine")]
        public ActionResult<string> DeleteSampleRequestFormLine(int id)
        {
            try
            {
                var sampleRequestFormLine = _context.SampleRequestFormLine.Where(p => p.SampleRequestFormLineId == id).FirstOrDefault();
                if (sampleRequestFormLine != null)
                {
                    decimal? totalissueQuantity = _context.IssueRequestSampleLine.Where(s => s.SampleRequestLineId == id).Sum(s => s.IssueQuantity);
                    if (sampleRequestFormLine.ItemId != null && sampleRequestFormLine.BatchNo != null)
                    {
                        var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId && s.BatchNo == sampleRequestFormLine.BatchNo).FirstOrDefault();
                        if (itemBatchinfo != null)
                        {
                            if (totalissueQuantity > 0)
                            {
                                itemBatchinfo.QuantityOnHand -= totalissueQuantity;
                                _context.SaveChanges();
                            }
                        }
                        var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == sampleRequestFormLine.ItemId).FirstOrDefault();
                        if (itemStockinfo != null)
                        {
                            if (totalissueQuantity > 0)
                            {
                                itemStockinfo.QuantityOnHand -= totalissueQuantity;
                            }
                        }
                    }
                    var issuesampleline = _context.IssueRequestSampleLine.Where(s => s.SampleRequestLineId == id).ToList();
                    if (issuesampleline != null)
                    {
                        _context.IssueRequestSampleLine.RemoveRange(issuesampleline);
                        _context.SaveChanges();
                    }
                    _context.SampleRequestFormLine.Remove(sampleRequestFormLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteIssueRequestSampleLine")]
        public ActionResult<string> DeleteIssueRequestSampleLine(int id)
        {
            try
            {
                var issuesampleline = _context.SampleRequestFormLine.Where(s => s.SampleRequestFormLineId == id).FirstOrDefault();
                if (issuesampleline != null)
                {
                    var issuesampleRequestFormLine = _context.IssueRequestSampleLine.Where(p => p.IssueRequestSampleLineId == id).FirstOrDefault();
                    if (issuesampleRequestFormLine != null)
                    {
                        if (issuesampleline.ItemId != null && issuesampleline.BatchNo != null)
                        {
                            var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == issuesampleline.ItemId && s.BatchNo == issuesampleline.BatchNo).FirstOrDefault();
                            if (itemBatchinfo != null)
                            {
                                itemBatchinfo.QuantityOnHand -= issuesampleRequestFormLine.IssueQuantity;
                                _context.SaveChanges();
                            }
                            var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == issuesampleline.ItemId).FirstOrDefault();
                            if (itemStockinfo != null)
                            {
                                itemStockinfo.QuantityOnHand -= issuesampleRequestFormLine.IssueQuantity;
                            }
                        }
                        _context.IssueRequestSampleLine.Remove(issuesampleRequestFormLine);
                        _context.SaveChanges();
                    }
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion


        #region post nav function
        string exceptionMessage = "Stock Adjustment Failed.";
        private async Task<bool> PostStockAdjuestment(SampleRequestFormLineModel model)
        {
            StringBuilder stringBuilder = new StringBuilder();
            try
            {

                var companyId = _context.Navitems.FirstOrDefault(i => i.ItemId == model.ItemId).CompanyId;
                model.CompanyName = _context.Plant.FirstOrDefault(p => p.PlantId == companyId).NavCompanyName;
                var docNo = _context.SampleRequestForm.FirstOrDefault(d => d.SampleRequestFormId == model.SampleRequestFormId).ProfileNo;
                stringBuilder.AppendLine(model.CompanyName);
                var context = new DataService(_config, model.CompanyName);
                var Company = model.CompanyName;
                stringBuilder.AppendLine(context.Context.ToString());
                stringBuilder.AppendLine(context.Context.BaseUri.AbsoluteUri);
                stringBuilder.AppendLine(context.Context.BaseUri.AbsolutePath);
                stringBuilder.AppendLine(context.Context.BaseUri.Query);
                string entry_no = DateTime.Now.Ticks.ToString();
                entry_no = entry_no.Substring(entry_no.Length - 5);
                var entryNo = int.Parse(entry_no);
                var userAcc = _context.ApplicationUser.FirstOrDefault(f => f.UserId == model.AddedByUserID);
                var NavItem = _context.Navitems.Where(i => i.ItemId == model.ItemId).FirstOrDefault();
                var lotNo = "";

                var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == model.ItemId && s.BatchNo == model.BatchNo && s.LocationCode == model.NavLocation).FirstOrDefault();
                if (itemBatchinfo != null)
                {
                    lotNo = itemBatchinfo.LotNo;
                }

                //var nquery1 = context.Context.ProdOrderOutputLine;
                //DataServiceQuery<NAV.ProdOrderOutputLine> query1 = (DataServiceQuery<NAV.ProdOrderOutputLine>)nquery1;
                //TaskFactory<IEnumerable<NAV.ProdOrderOutputLine>> taskFactory2 = new TaskFactory<IEnumerable<NAV.ProdOrderOutputLine>>();
                //var prodEntryUpdate = await taskFactory2.FromAsync(query1.BeginExecute(null, null), iar => query1.EndExecute(iar));
                //var objeToUpdate = prodEntryUpdate.FirstOrDefault();
                stringBuilder.AppendLine(context.Context.StockAdjustmentOdata.RequestUri.AbsoluteUri);
                var prodEntry = new StockAdjustmentOdata()
                {
                    Entry_No = entryNo,
                    Document_No = docNo,
                    Document_Type = 3,
                    Entry_Type = "Stock Adjustment",
                    Posting_Date = DateTime.Now,
                    Location_Code = model.NavLocation,
                    // Posted_on = DateTime.Now,
                    Quantity = model.QtyIssue,
                    Created_by = userAcc.LoginId,
                    Created_on = DateTime.Now,
                    Item_No = NavItem.No,
                    Unit_of_Measure_Code = NavItem.BaseUnitofMeasure,
                    Reason_Code = "CODE 45",
                    Lot_No = lotNo,

                };

                context.Context.AddToStockAdjustmentOdata(prodEntry);

                stringBuilder.AppendLine(context.Context.StockAdjustmentOdata.RequestUri.AbsoluteUri);
                TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));
                stringBuilder.AppendLine(response.ToString());

                //var nquery1 = context.Context.SWDWebIntegrationEntry.Where(i => i.Entry_No == entryNo);
                //DataServiceQuery<NAV.SWDWebIntegrationEntry> query1 = (DataServiceQuery<NAV.SWDWebIntegrationEntry>)nquery1;
                //TaskFactory<IEnumerable<NAV.SWDWebIntegrationEntry>> taskFactory2 = new TaskFactory<IEnumerable<NAV.SWDWebIntegrationEntry>>();
                //var prodEntryUpdate = await taskFactory2.FromAsync(query1.BeginExecute(null, null), iar => query1.EndExecute(iar));

                //var objeToUpdate = prodEntryUpdate.FirstOrDefault();


                var post = new StockAdjustmentService.SWDWebIntegration_PortClient();

                post.Endpoint.Address =
                new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/SWDWebIntegration"),
                new DnsEndpointIdentity(""));

                post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                post.ClientCredentials.Windows.AllowedImpersonationLevel =
                System.Security.Principal.TokenImpersonationLevel.Impersonation;

                stringBuilder.AppendLine(post.Endpoint.Address.Uri.AbsoluteUri);
                var result = await post.FnPostItemJournalAsync(entryNo);
                //var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + Guid.NewGuid().ToString() + ".txt";
                //System.IO.File.WriteAllText(serverPath, stringBuilder.ToString());

                return true;
            }
            catch (Exception ex)
            {
                var error = ex.InnerException != null ? ex.InnerException.Message : "";
                exceptionMessage = ex.Message + " " + error;
                stringBuilder.AppendLine(exceptionMessage);

                //  var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + Guid.NewGuid().ToString() + ".txt";
                // System.IO.File.WriteAllText(serverPath, stringBuilder.ToString());

                return false;
            }
        }
        #endregion
    }
}