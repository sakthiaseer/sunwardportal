﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NpraformulationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NpraformulationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #region Npraformulation
        [HttpGet]
        [Route("GetNpraformulation")]
        public List<NpraformulationModel> GetNpraformulation()
        {
            var npraformulation = _context.Npraformulation
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.RegistrationReference)
                 .Include(p => p.ProductName)
                 .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<NpraformulationModel> npraformulationModels = new List<NpraformulationModel>();
            npraformulation.ForEach(s =>
            {
                NpraformulationModel npraformulationModel = new NpraformulationModel
                {
                    NpraformulationId = s.NpraformulationId,
                    RegistrationReferenceId = s.RegistrationReferenceId,
                    ProductNameId = s.ProductNameId,
                    RegistrationReference = s.RegistrationReference?.CodeValue,
                    ProductName = s.ProductName?.Value,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId = s.SessionId,
                };
                npraformulationModels.Add(npraformulationModel);
            });
            return npraformulationModels.OrderByDescending(a => a.NpraformulationId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NpraformulationModel> GetData(SearchModel searchModel)
        {
            var npraformulation = new Npraformulation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).FirstOrDefault();
                        break;
                    case "Last":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).LastOrDefault();
                        break;
                    case "Next":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).LastOrDefault();
                        break;
                    case "Previous":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).FirstOrDefault();
                        break;
                    case "Last":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).LastOrDefault();
                        break;
                    case "Next":
                        npraformulation = _context.Npraformulation.OrderBy(o => o.NpraformulationId).FirstOrDefault(s => s.NpraformulationId > searchModel.Id);
                        break;
                    case "Previous":
                        npraformulation = _context.Npraformulation.OrderByDescending(o => o.NpraformulationId).FirstOrDefault(s => s.NpraformulationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NpraformulationModel>(npraformulation);
            return result;
        }
        [HttpPost]
        [Route("InsertNpraformulation")]
        public NpraformulationModel Post(NpraformulationModel value)
        {
            var SessionId = Guid.NewGuid();
            var npraformulation = new Npraformulation
            {
                RegistrationReferenceId = value.RegistrationReferenceId,
                ProductNameId = value.ProductNameId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            _context.Npraformulation.Add(npraformulation);
            _context.SaveChanges();
            value.NpraformulationId = npraformulation.NpraformulationId;
            value.SessionId = npraformulation.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNpraformulation")]
        public NpraformulationModel Put(NpraformulationModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var npraformulation = _context.Npraformulation.SingleOrDefault(p => p.NpraformulationId == value.NpraformulationId);
            npraformulation.RegistrationReferenceId = value.RegistrationReferenceId;
            npraformulation.ProductNameId = value.ProductNameId;
            npraformulation.StatusCodeId = value.StatusCodeID.Value;
            npraformulation.ModifiedByUserId = value.ModifiedByUserID;
            npraformulation.ModifiedDate = DateTime.Now;
            npraformulation.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteNpraformulation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var npraformulation = _context.Npraformulation.Where(p => p.NpraformulationId == id).FirstOrDefault();
                if (npraformulation != null)
                {
                    var npraformulationActiveIngredient = _context.NpraformulationActiveIngredient.Where(b => b.NpraformulationId == id).ToList();
                    if (npraformulationActiveIngredient != null)
                    {
                        _context.NpraformulationActiveIngredient.RemoveRange(npraformulationActiveIngredient);
                        _context.SaveChanges();
                    }
                    var npraformulationExcipient = _context.NpraformulationExcipient.Where(b => b.NpraformulationId == id).ToList();
                    if (npraformulationExcipient != null)
                    {
                        _context.NpraformulationExcipient.RemoveRange(npraformulationExcipient);
                        _context.SaveChanges();
                    }
                    _context.Npraformulation.Remove(npraformulation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region NpraformulationActiveIngredient
        [HttpGet]
        [Route("GetNpraformulationActiveIngredient")]
        public List<NpraformulationActiveIngredientModel> GetNpraformulationActiveIngredient(long? id)
        {
            var npraformulationActiveIngredient = _context.NpraformulationActiveIngredient
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.FormOfSubstance)
                 .Include(p => p.ActiveIngredient)
                 .Include(p => p.SaltForm)
                 .Include(p => p.Source)
                 .Include(p => p.StrengthSaltFree)
                 .Include(p => p.Units)
                 .Include(s => s.StatusCode).Where(w => w.NpraformulationId == id).AsNoTracking().ToList();
            List<NpraformulationActiveIngredientModel> npraformulationActiveIngredientModels = new List<NpraformulationActiveIngredientModel>();
            npraformulationActiveIngredient.ForEach(s =>
            {
                NpraformulationActiveIngredientModel npraformulationActiveIngredientModel = new NpraformulationActiveIngredientModel
                {
                    NpraformulationActiveIngredientId = s.NpraformulationActiveIngredientId,
                    NpraformulationId = s.NpraformulationId,
                    ActiveIngredientId = s.ActiveIngredientId,
                    SaltFormId = s.SaltFormId,
                    Strength = s.Strength,
                    UnitsId = s.UnitsId,
                    StrengthSaltFreeId = s.StrengthSaltFreeId,
                    SourceId = s.SourceId,
                    FormOfSubstanceId = s.FormOfSubstanceId,
                    Remarks = s.Remarks,
                    ActiveIngredient = s.ActiveIngredient?.Value,
                    SaltForm = s.SaltForm?.Value,
                    Units = s.Units?.Value,
                    StrengthSaltFree = s.StrengthSaltFree?.Value,
                    Source = s.Source?.Value,
                    FormOfSubstance = s.FormOfSubstance?.Value,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId=s.SessionId,
                };
                npraformulationActiveIngredientModels.Add(npraformulationActiveIngredientModel);
            });
            return npraformulationActiveIngredientModels.OrderByDescending(a => a.NpraformulationActiveIngredientId).ToList();
        }
        [HttpPost]
        [Route("InsertNpraformulationActiveIngredient")]
        public NpraformulationActiveIngredientModel InsertNpraformulationActiveIngredient(NpraformulationActiveIngredientModel value)
        {
            var SessionId = Guid.NewGuid();
            var npraformulationActiveIngredient = new NpraformulationActiveIngredient
            {
                NpraformulationId = value.NpraformulationId,
                ActiveIngredientId = value.ActiveIngredientId,
                SaltFormId = value.SaltFormId,
                Strength = value.Strength,
                UnitsId = value.UnitsId,
                StrengthSaltFreeId = value.StrengthSaltFreeId,
                SourceId = value.SourceId,
                FormOfSubstanceId = value.FormOfSubstanceId,
                Remarks = value.Remarks,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                SessionId=SessionId,
            };
            _context.NpraformulationActiveIngredient.Add(npraformulationActiveIngredient);
            _context.SaveChanges();
            value.NpraformulationActiveIngredientId = npraformulationActiveIngredient.NpraformulationActiveIngredientId;
            value.SessionId = npraformulationActiveIngredient.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNpraformulationActiveIngredient")]
        public NpraformulationActiveIngredientModel UpdateNpraformulationActiveIngredient(NpraformulationActiveIngredientModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var npraformulationActiveIngredient = _context.NpraformulationActiveIngredient.SingleOrDefault(p => p.NpraformulationActiveIngredientId == value.NpraformulationActiveIngredientId);
            npraformulationActiveIngredient.NpraformulationId = value.NpraformulationId;
            npraformulationActiveIngredient.ActiveIngredientId = value.ActiveIngredientId;
            npraformulationActiveIngredient.SaltFormId = value.SaltFormId;
            npraformulationActiveIngredient.Strength = value.Strength;
            npraformulationActiveIngredient.UnitsId = value.UnitsId;
            npraformulationActiveIngredient.StrengthSaltFreeId = value.StrengthSaltFreeId;
            npraformulationActiveIngredient.SourceId = value.SourceId;
            npraformulationActiveIngredient.FormOfSubstanceId = value.FormOfSubstanceId;
            npraformulationActiveIngredient.Remarks = value.Remarks;
            npraformulationActiveIngredient.StatusCodeId = value.StatusCodeID.Value;
            npraformulationActiveIngredient.ModifiedByUserId = value.ModifiedByUserID;
            npraformulationActiveIngredient.ModifiedDate = DateTime.Now;
            npraformulationActiveIngredient.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteNpraformulationActiveIngredient")]
        public ActionResult<string> DeleteNpraformulationActiveIngredient(int id)
        {
            try
            {
                var npraformulationActiveIngredient = _context.NpraformulationActiveIngredient.Where(p => p.NpraformulationActiveIngredientId == id).FirstOrDefault();
                if (npraformulationActiveIngredient != null)
                {
                    _context.NpraformulationActiveIngredient.Remove(npraformulationActiveIngredient);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region NpraformulationExcipient
        [HttpGet]
        [Route("GetNpraformulationExcipient")]
        public List<NpraformulationExcipientModel> GetNpraformulationExcipient(long? id)
        {
            var npraformulationExcipient = _context.NpraformulationExcipient
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.Excipient)
                 .Include(p => p.Function)
                 .Include(p => p.RemarksFunction)
                 .Include(p => p.Source)
                 .Include(p => p.Units)
                 .Include(s => s.StatusCode).Where(w => w.NpraformulationId == id).AsNoTracking().ToList();
            List<NpraformulationExcipientModel> npraformulationExcipientModels = new List<NpraformulationExcipientModel>();
            npraformulationExcipient.ForEach(s =>
            {
                NpraformulationExcipientModel npraformulationExcipientModel = new NpraformulationExcipientModel
                {
                    NpraformulationExcipientId = s.NpraformulationExcipientId,
                    NpraformulationId = s.NpraformulationId,
                    ExcipientId = s.ExcipientId,
                    Strength = s.Strength,
                    UnitsId = s.UnitsId,
                    FunctionId = s.FunctionId,
                    SourceId = s.SourceId,
                    RemarksFunctionId = s.RemarksFunctionId,
                    Remarks = s.Remarks,
                    Excipient = s.Excipient?.Value,
                    Units = s.Units?.Value,
                    Function = s.Function?.Value,
                    Source = s.Source?.Value,
                    RemarksFunction = s.RemarksFunction?.Value,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    SessionId=s.SessionId,
                };
                npraformulationExcipientModels.Add(npraformulationExcipientModel);
            });
            return npraformulationExcipientModels.OrderByDescending(a => a.NpraformulationExcipientId).ToList();
        }
        [HttpPost]
        [Route("InsertNpraformulationExcipient")]
        public NpraformulationExcipientModel InsertNpraformulationExcipient(NpraformulationExcipientModel value)
        {
            var SessionId = Guid.NewGuid();
            var npraformulationExcipient = new NpraformulationExcipient
            {
                NpraformulationId = value.NpraformulationId,
                ExcipientId = value.ExcipientId,
                Strength = value.Strength,
                UnitsId = value.UnitsId,
                FunctionId = value.FunctionId,
                SourceId = value.SourceId,
                RemarksFunctionId = value.RemarksFunctionId,
                Remarks = value.Remarks,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                SessionId=SessionId,
            };
            _context.NpraformulationExcipient.Add(npraformulationExcipient);
            _context.SaveChanges();
            value.NpraformulationExcipientId = npraformulationExcipient.NpraformulationExcipientId;
            value.SessionId = npraformulationExcipient.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNpraformulationExcipient")]
        public NpraformulationExcipientModel UpdateNpraformulationExcipient(NpraformulationExcipientModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var npraformulationExcipient = _context.NpraformulationExcipient.SingleOrDefault(p => p.NpraformulationExcipientId == value.NpraformulationExcipientId);
            npraformulationExcipient.NpraformulationId = value.NpraformulationId;
            npraformulationExcipient.ExcipientId = value.ExcipientId;
            npraformulationExcipient.Strength = value.Strength;
            npraformulationExcipient.UnitsId = value.UnitsId;
            npraformulationExcipient.FunctionId = value.FunctionId;
            npraformulationExcipient.SourceId = value.SourceId;
            npraformulationExcipient.RemarksFunctionId = value.RemarksFunctionId;
            npraformulationExcipient.Remarks = value.Remarks;
            npraformulationExcipient.StatusCodeId = value.StatusCodeID.Value;
            npraformulationExcipient.ModifiedByUserId = value.ModifiedByUserID;
            npraformulationExcipient.ModifiedDate = DateTime.Now;
            npraformulationExcipient.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteNpraformulationExcipient")]
        public ActionResult<string> DeleteNpraformulationExcipient(int id)
        {
            try
            {
                var npraformulationExcipient = _context.NpraformulationExcipient.Where(p => p.NpraformulationExcipientId == id).FirstOrDefault();
                if (npraformulationExcipient != null)
                {
                    _context.NpraformulationExcipient.Remove(npraformulationExcipient);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
