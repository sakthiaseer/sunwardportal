﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class JobProgressTemplateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IHubContext<ChatHub> _hub;
        public JobProgressTemplateController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository, GenerateDocumentNoSeries generate, IHubContext<ChatHub> hub)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
            _hub = hub;
            _generateDocumentNoSeries = generate;
        }

        #region JobProgressTemplate

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetJobProgressTemplateItems")]
        public List<JobProgressTemplateModel> GetJobProgressTemplateItems()
        {
            var jobProgressTemplates = _context.JobProgressTemplate
                                .Include(r => r.JobCategory)
                                .Include(m => m.JobType)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(s => s.Module)
                                .Include(s => s.PicNavigation)
                                .Include(s => s.Software)
                                .Include(s => s.Item)
                                .Include(u => u.JobProgressTemplateNotify)
                                .Include(n => n.JobProgressTemplateNavItem)
                                .Include(p => p.Profile)
                                .Include(p => p.Country)
                                .AsNoTracking().OrderByDescending(o => o.JobProgressTemplateId).ToList();
            List<JobProgressTemplateModel> jobProgressTemplateModels = new List<JobProgressTemplateModel>();
            jobProgressTemplates.ForEach(s =>
            {
                JobProgressTemplateModel jobProgressTemplateModel = new JobProgressTemplateModel();

                jobProgressTemplateModel.JobProgressTemplateId = s.JobProgressTemplateId;
                jobProgressTemplateModel.TemplateId = s.TemplateId;
                jobProgressTemplateModel.TemplateName = s.TemplateName;
                jobProgressTemplateModel.CountryId = s.CountryId;
                jobProgressTemplateModel.JobOrderNo = s.JobOrderNo;
                jobProgressTemplateModel.CountryName = s.Country?.Description;
                jobProgressTemplateModel.ProfileCode = s.ProfileCode;
                jobProgressTemplateModel.IsUpdatePersonalCalandar = s.IsUpdatePersonalCalandar;
                jobProgressTemplateModel.IsUpdatePersonalCalandarFlag = s.IsUpdatePersonalCalandar == true ? "Yes" : "No";
                jobProgressTemplateModel.JobTypeId = s.JobTypeId;
                jobProgressTemplateModel.JobCategoryId = s.JobCategoryId;
                jobProgressTemplateModel.JobCategory = s.JobCategory?.Value;
                jobProgressTemplateModel.JobType = s.JobType?.Value;
                jobProgressTemplateModel.SessionId = s.SessionId;
                jobProgressTemplateModel.Objective = s.Objective;
                jobProgressTemplateModel.Pic = s.Pic;
                jobProgressTemplateModel.PersonIncharge = s.PicNavigation?.UserName;
                jobProgressTemplateModel.NotificationId = s.NotificationId;
                jobProgressTemplateModel.SoftwareId = s.SoftwareId;
                jobProgressTemplateModel.Software = s.Software?.CodeValue;
                jobProgressTemplateModel.ModuleId = s.ModuleId;
                jobProgressTemplateModel.Module = s.Module?.Description;
                jobProgressTemplateModel.ItemId = s.ItemId;
                jobProgressTemplateModel.ItemNo = s.Item?.No;
                jobProgressTemplateModel.ModifiedByUserID = s.AddedByUserId;
                jobProgressTemplateModel.AddedByUserID = s.ModifiedByUserId;
                jobProgressTemplateModel.StatusCodeID = s.StatusCodeId;
                jobProgressTemplateModel.AddedByUser = s.AddedByUser.UserName;
                jobProgressTemplateModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                jobProgressTemplateModel.AddedDate = s.AddedDate;
                jobProgressTemplateModel.ModifiedDate = s.ModifiedDate;
                jobProgressTemplateModel.StatusCode = s.StatusCode.CodeValue;
                jobProgressTemplateModel.ProfileId = s.ProfileId;
                jobProgressTemplateModel.ProfileNo = s.ProfileNo;
                jobProgressTemplateModel.ProfileName = s.Profile?.Name;
                jobProgressTemplateModel.ItemIds = s.JobProgressTemplateNavItem != null ? s.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == s.JobProgressTemplateId).Select(a => a.ItemId).ToList() : new List<long?>();
                jobProgressTemplateModel.UserType = s.JobProgressTemplateNotify != null ? s.JobProgressTemplateNotify.Select(a => a.UserType).Distinct().FirstOrDefault() : "user";
                jobProgressTemplateModel.EmployeeIDs = new List<long?>();
                jobProgressTemplateModel.UserGroupIDs = new List<long?>();
                jobProgressTemplateModels.Add(jobProgressTemplateModel);
            });

            return jobProgressTemplateModels;
        }
        [HttpGet]
        [Route("GetJobProgressTemplateNotify")]
        public List<JobProgressTemplateNotifyModel> GetJobProgressTemplateNotify(long? id)
        {
            List<JobProgressTemplateNotifyModel> jobProgressTemplateNotifyModels = new List<JobProgressTemplateNotifyModel>();
            var jobProgressTemplateNotify = _context.JobProgressTemplateNotify.Include(e => e.Employee).Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.UserGroup).Where(w => w.JobProgressTemplateId == id).ToList();
            jobProgressTemplateNotify.ForEach(s =>
            {
                JobProgressTemplateNotifyModel jobProgressTemplateNotifyModel = new JobProgressTemplateNotifyModel();
                jobProgressTemplateNotifyModel.JobProgressTemplateNotifyId = s.JobProgressTemplateNotifyId;
                jobProgressTemplateNotifyModel.JobProgressTemplateId = s.JobProgressTemplateId;
                jobProgressTemplateNotifyModel.UserGroupId = s.UserGroupId;
                jobProgressTemplateNotifyModel.EmployeeId = s.EmployeeId;
                jobProgressTemplateNotifyModel.UserType = s.UserType;
                jobProgressTemplateNotifyModel.EmployeeName = s.Employee?.FirstName;
                jobProgressTemplateNotifyModel.DepartmentName = s.Employee?.DepartmentNavigation?.Name;
                jobProgressTemplateNotifyModel.DesignationName = s.Employee?.Designation?.Name;
                jobProgressTemplateNotifyModel.HeadCount = s.Employee?.HeadCount;
                jobProgressTemplateNotifyModel.JobProgressTemplateNotifyIds = new List<long?>();
                jobProgressTemplateNotifyModels.Add(jobProgressTemplateNotifyModel);
            });
            return jobProgressTemplateNotifyModels;
        }

        [HttpGet]
        [Route("IsTemplateExist")]
        public ActionResult<bool> IsTemplateExist(int id)
        {
            var jobProgressTemplateItem = _context.JobProgressTemplate.FirstOrDefault(i => i.TemplateId == id);
            return jobProgressTemplateItem != null;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<JobProgressTemplateModel> GetData(SearchModel searchModel)
        {
            var jobProgressTemplate = new JobProgressTemplate();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).FirstOrDefault();
                        break;
                    case "Last":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).LastOrDefault();
                        break;
                    case "Next":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).LastOrDefault();
                        break;
                    case "Previous":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).FirstOrDefault();
                        break;
                    case "Last":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).LastOrDefault();
                        break;
                    case "Next":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderBy(o => o.JobProgressTemplateId).FirstOrDefault(s => s.JobProgressTemplateId > searchModel.Id);
                        break;
                    case "Previous":
                        jobProgressTemplate = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).OrderByDescending(o => o.JobProgressTemplateId).FirstOrDefault(s => s.JobProgressTemplateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<JobProgressTemplateModel>(jobProgressTemplate);
            if (result != null)
            {
                result.EmployeeIDs = new List<long?>();
                result.UserGroupIDs = new List<long?>();
                result.ItemIds = jobProgressTemplate.JobProgressTemplateNavItem != null ? jobProgressTemplate.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == jobProgressTemplate.JobProgressTemplateId).Select(a => a.ItemId).ToList() : new List<long?>();

            }
            return result;
        }
        // POST: api/WorkOrder
        [HttpPost]
        [Route("InsertJobProgressTemplate")]
        public JobProgressTemplateModel Post(JobProgressTemplateModel value)
        {
            var sessionId = Guid.NewGuid();
            var profileNo = "";

            if (value.ProfileId != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID });
            }
            var jobProgressTemplate = new JobProgressTemplate
            {
                TemplateId = value.TemplateId,
                JobTypeId = value.JobTypeId,
                SessionId = sessionId,
                JobCategoryId = value.JobCategoryId,
                Objective = value.Objective,
                Pic = value.Pic,
                NotificationId = value.NotificationId,
                SoftwareId = value.SoftwareId,
                ModuleId = value.ModuleId,
                ItemId = value.ItemId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProfileId = value.ProfileId,
                ProfileNo = profileNo,
                TemplateName = value.TemplateName,
                ProfileCode = GenerteCaseNo(value),
                IsUpdatePersonalCalandar = value.IsUpdatePersonalCalandarFlag == "Yes" ? true : false,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                CountryId=value.CountryId,
                JobOrderNo=value.JobOrderNo,
            };
            if (value.ItemIds.Count > 0)
            {
                value.ItemIds.ForEach(h =>
                {
                    var jobProgressTemplateNavItem = new JobProgressTemplateNavItem
                    {
                        ItemId = h,
                    };
                    jobProgressTemplate.JobProgressTemplateNavItem.Add(jobProgressTemplateNavItem);
                });
            }
            _context.JobProgressTemplate.Add(jobProgressTemplate);
            _context.SaveChanges();
            value.ProfileNo = jobProgressTemplate.ProfileNo;
            value.JobProgressTemplateId = jobProgressTemplate.JobProgressTemplateId;
            value.ProfileCode = jobProgressTemplate.ProfileCode;
            value.SessionId = sessionId;
            UserAndUserGroup(value, jobProgressTemplate.JobProgressTemplateId, value.JobProgressTemplateId);
            return value;
        }
        private string GenerteCaseNo(JobProgressTemplateModel value)
        {
            var caseNo = string.Empty;
            var templateTestCaseForm = _context.JobProgressTemplate.Count() + 1;
            var templatCode = _context.JobProgressTemplate.FirstOrDefault(f => f.JobProgressTemplateId == value.JobProgressTemplateId)?.ProfileCode;
            var plantCode = _context.Plant.FirstOrDefault(w => w.PlantId == value.CountryId)?.PlantCode;
            if (!string.IsNullOrEmpty(plantCode))
            {
                caseNo += plantCode.Substring(2, plantCode.Length - 2) + "-";
            }
            if (!string.IsNullOrEmpty(caseNo) && !string.IsNullOrEmpty(templatCode))
            {
                caseNo += templatCode + "-";
            }
            if (!string.IsNullOrEmpty(caseNo))
            {
                caseNo += DateTime.Now.ToString("yy") + "-";
            }
            if (!string.IsNullOrEmpty(caseNo))
            {
                caseNo += string.Format("{0:0000}", templateTestCaseForm);
            }
            return caseNo;
        }
        private async void UserAndUserGroup(JobProgressTemplateModel value, long jobProgressTemplateId, long jobProgressId)
        {
            var notificationHandelerRemove = _context.NotificationHandler.Where(w => w.SessionId == value.SessionId && w.ScreenId == value.ScreenID).ToList();
            if (notificationHandelerRemove != null)
            {
                _context.NotificationHandler.RemoveRange(notificationHandelerRemove);
                _context.SaveChanges();
            }
            var jobProgressTemplateNotifyList = _context.JobProgressTemplateNotify.Where(w => w.JobProgressTemplateId == jobProgressTemplateId).Select(s => s.EmployeeId).ToList();
            if (value.UserType == "user" && value.EmployeeIDs.Count > 0)
            {
                var employeeList = _context.Employee.Select(x => new { x.EmployeeId, x.UserId }).ToList();
                value.EmployeeIDs.ForEach(async e =>
                {
                    if (!jobProgressTemplateNotifyList.Contains(e))
                    {
                        var jobProgressTemplateNotify = new JobProgressTemplateNotify
                        {
                            UserType = value.UserType,
                            EmployeeId = e,
                            JobProgressTemplateId = jobProgressTemplateId,
                        };
                        _context.JobProgressTemplateNotify.Add(jobProgressTemplateNotify);

                        var notificationHandler = new NotificationHandler
                        {
                            SessionId = value.SessionId,
                            NextNotifyDate = DateTime.Now,
                            NotifyStatusId = 1,
                            AddedDays = 0,
                            Title = "Job Progress",
                            Message = jobProgressId > 0 ? "Job Progress Updated" : "New Job Progress Assigned",
                            ScreenId = value.ScreenID,
                            NotifyTo = employeeList != null ? employeeList.FirstOrDefault(w => w.EmployeeId == e).UserId : null,
                        };
                        _context.NotificationHandler.Add(notificationHandler);
                        if (jobProgressId > 0)
                        {
                            await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("jobProgressTemplate", "New");
                        }
                        else
                        {
                            await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("jobProgressTemplate", "Update");
                        }
                    }
                });
                _context.SaveChanges();
            }
            if (value.UserType == "user-group" && value.UserGroupIDs.Count > 0)
            {
                var employeeList = _context.Employee.Select(s => new { s.EmployeeId, s.UserId }).ToList();
                var employeeIds = _context.UserGroupUser.Include(u => u.User).Where(w => w.User != null && value.UserGroupIDs.Contains(w.UserGroupId)).Select(s => s.User.UserId).ToList();
                if (employeeIds != null && employeeList.Count > 0)
                {
                    employeeIds.ForEach(async e =>
                    {
                        if (!jobProgressTemplateNotifyList.Contains(e))
                        {
                            var jobProgressTemplateNotify = new JobProgressTemplateNotify
                            {
                                UserType = value.UserType,
                                EmployeeId = employeeList.FirstOrDefault(f => f.UserId == e)?.EmployeeId,
                                JobProgressTemplateId = jobProgressTemplateId,
                            };
                            _context.JobProgressTemplateNotify.Add(jobProgressTemplateNotify);

                            var notificationHandler = new NotificationHandler
                            {
                                SessionId = value.SessionId,
                                NextNotifyDate = DateTime.Now,
                                NotifyStatusId = 1,
                                AddedDays = 0,
                                ScreenId = value.ScreenID,
                                NotifyTo = e,
                                Title = "Job Progress",
                                Message = jobProgressId > 0 ? "Job Progress Updated" : "New Job Progress Assigned",
                            };
                            _context.NotificationHandler.Add(notificationHandler);
                            if (jobProgressId > 0)
                            {
                                await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("jobProgressTemplate", "New");
                            }
                            else
                            {
                                await _hub.Clients.Group(notificationHandler.NotifyTo.ToString()).SendAsync("jobProgressTemplate", "Update");
                            }
                        }
                    });
                    _context.SaveChanges();
                }
            }
        }
        [HttpPost]
        [Route("InsertJobProgressTemplateRecurrence")]
        public JobProgressTemplateRecurrenceModel InsertJobProgressTemplateRecurrence(JobProgressTemplateRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.JobProgressTemplateRecurrence.Where(p => p.JobProgressTemplateRecurrenceId == value.JobProgressTemplateRecurrenceId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.JobProgressTemplateRecurrence.SingleOrDefault(p => p.JobProgressTemplateRecurrenceId == value.JobProgressTemplateRecurrenceId);
                ApplicationWikiRecurrences.JobProgressTemplateId = value.JobProgressTemplateId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new JobProgressTemplateRecurrence
                {
                    JobProgressTemplateId = value.JobProgressTemplateId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.JobProgressTemplateRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.JobProgressTemplateRecurrenceId = ApplicationWikiRecurrence.JobProgressTemplateRecurrenceId;
                return value;
            }
        }
        [HttpGet]
        [Route("GetJobProgressTemplateRecurrence")]
        public List<JobProgressTemplateRecurrenceModel> ApplicationWikiRecurrence(long id)
        {
            var ApplicationWikiRecurrence = _context.JobProgressTemplateRecurrence
                 .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Type")
                .Include("OccurenceOption")
               .Select(s => new JobProgressTemplateRecurrenceModel
               {
                   JobProgressTemplateId = s.JobProgressTemplateId,
                   JobProgressTemplateRecurrenceId = s.JobProgressTemplateRecurrenceId,
                   TypeId = s.TypeId,
                   RepeatNos = s.RepeatNos,
                   OccurenceOptionId = s.OccurenceOptionId,
                   NoOfOccurences = s.NoOfOccurences,
                   Sunday = s.Sunday,
                   Monday = s.Monday,
                   Tuesday = s.Tuesday,
                   Wednesday = s.Wednesday,
                   Thursday = s.Thursday,
                   Friday = s.Friday,
                   Saturyday = s.Saturyday,
                   StartDate = s.StartDate,
                   EndDate = s.EndDate,
                   StatusCodeID = s.StatusCodeId,
                   AddedByUserID = s.AddedByUserId,
                   ModifiedByUserID = s.ModifiedByUserId,
                   AddedDate = s.AddedDate,
                   ModifiedDate = s.ModifiedDate,
                   AddedByUser = s.AddedByUser.UserName,
                   ModifiedByUser = s.ModifiedByUser.UserName,
                   StatusCode = s.StatusCode.CodeValue,
                   TypeName = s.Type.CodeValue,
                   OccurenceOptionName = s.OccurenceOption.CodeValue,
               }).Where(w => w.JobProgressTemplateId == id).OrderByDescending(o => o.JobProgressTemplateRecurrenceId).AsNoTracking().ToList();
            return ApplicationWikiRecurrence;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateJobProgressTemplate")]
        public JobProgressTemplateModel Put(JobProgressTemplateModel value)
        {
            var jobProgressTemplate = _context.JobProgressTemplate.SingleOrDefault(p => p.JobProgressTemplateId == value.JobProgressTemplateId);
            if (value.ProfileId != null || value.ProfileId != jobProgressTemplate.ProfileId)
            {
                value.ProfileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID });
            }
            jobProgressTemplate.TemplateId = value.TemplateId;
            jobProgressTemplate.JobTypeId = value.JobTypeId;
            jobProgressTemplate.SessionId = value.SessionId ?? Guid.NewGuid();
            jobProgressTemplate.Objective = value.Objective;
            jobProgressTemplate.JobCategoryId = value.JobCategoryId;
            jobProgressTemplate.Pic = value.Pic;
            jobProgressTemplate.NotificationId = value.NotificationId;
            jobProgressTemplate.SoftwareId = value.SoftwareId;
            jobProgressTemplate.ModuleId = value.ModuleId;
            jobProgressTemplate.ModifiedByUserId = value.ModifiedByUserID;
            jobProgressTemplate.ModifiedDate = DateTime.Now;
            jobProgressTemplate.StatusCodeId = value.StatusCodeID.Value;
            jobProgressTemplate.ItemId = value.ItemId;
            jobProgressTemplate.ProfileId = value.ProfileId;
            jobProgressTemplate.ProfileNo = value.ProfileNo;
            jobProgressTemplate.TemplateName = value.TemplateName;
            jobProgressTemplate.ProfileCode = value.ProfileCode;
            jobProgressTemplate.CountryId = value.CountryId;
            jobProgressTemplate.JobOrderNo = value.JobOrderNo;
            jobProgressTemplate.IsUpdatePersonalCalandar = value.IsUpdatePersonalCalandarFlag == "Yes" ? true : false;
            _context.SaveChanges();
            var jobProgressTemplateNavItemRemove = _context.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == jobProgressTemplate.JobProgressTemplateId).ToList();
            if (jobProgressTemplateNavItemRemove.Count > 0)
            {
                _context.JobProgressTemplateNavItem.RemoveRange(jobProgressTemplateNavItemRemove);
                _context.SaveChanges();
            }
            if (value.ItemIds.Count > 0)
            {
                value.ItemIds.ForEach(h =>
                {
                    var jobProgressTemplateNavItem = new JobProgressTemplateNavItem
                    {
                        ItemId = h,
                        JobProgressTemplateId = value.JobProgressTemplateId,
                    };
                    _context.JobProgressTemplateNavItem.Add(jobProgressTemplateNavItem);
                });
                _context.SaveChanges();
            }
            UserAndUserGroup(value, jobProgressTemplate.JobProgressTemplateId, value.JobProgressTemplateId);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteJobProgressTemplate")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var jobProgressTemplate = _context.JobProgressTemplate.Include(l => l.JobProgressTempletateLine).Include(s => s.JobProgressTemplateNotify).Include(n => n.JobProgressTemplateNavItem).SingleOrDefault(p => p.JobProgressTemplateId == id);
                if (jobProgressTemplate != null)
                {
                    _context.JobProgressTemplate.Remove(jobProgressTemplate);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("JobProgressTemplate Cannot be delete! JobProgressTemplate Reference to others");
            }
        }
        [HttpPost]
        [Route("DeleteJobProgressTemplateNotify")]
        public JobProgressTemplateNotifyModel DeleteJobProgressTemplateNotify(JobProgressTemplateNotifyModel value)
        {
            var selectDocumentRoleIds = value.JobProgressTemplateNotifyIds;
            if (value.JobProgressTemplateNotifyIds.Count > 0)
            {
                value.JobProgressTemplateNotifyIds.ForEach(sel =>
                {
                    var jobProgressTemplateNotify = _context.JobProgressTemplateNotify.Where(p => p.JobProgressTemplateNotifyId == sel).FirstOrDefault();
                    if (jobProgressTemplateNotify != null)
                    {
                        _context.JobProgressTemplateNotify.Remove(jobProgressTemplateNotify);
                        _context.SaveChanges();
                    }
                });
                _context.SaveChanges();
            }


            return value;
        }

        [HttpGet]
        [Route("GetJobProgressTemplateItemsByID")]
        public JobProgressTemplateModel GetJobProgressTemplateItemsByID(int id)
        {
            var s = _context.JobProgressTemplate
                                .Include(a => a.Template)
                                .Include(r => r.JobCategory)
                                .Include(m => m.JobType)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(s => s.Module)
                                .Include(s => s.PicNavigation)
                                .Include(s => s.Software)
                                .Include(s => s.Item)
                                .Include(u => u.JobProgressTemplateNotify)
                                .Include(n => n.JobProgressTemplateNavItem)
                                .Include(p => p.Profile)
                                .FirstOrDefault(t => t.JobProgressTemplateId == id);
            JobProgressTemplateModel jobProgressTemplateModel = new JobProgressTemplateModel();

            jobProgressTemplateModel.JobProgressTemplateId = s.JobProgressTemplateId;
            jobProgressTemplateModel.TemplateId = s.TemplateId;
            jobProgressTemplateModel.TemplateName = s.Template?.Value;
            jobProgressTemplateModel.JobTypeId = s.JobTypeId;
            jobProgressTemplateModel.JobCategoryId = s.JobCategoryId;
            jobProgressTemplateModel.JobCategory = s.JobCategory?.Value;
            jobProgressTemplateModel.JobType = s.JobType?.Value;
            jobProgressTemplateModel.SessionId = s.SessionId;
            jobProgressTemplateModel.Objective = s.Objective;
            jobProgressTemplateModel.Pic = s.Pic;
            jobProgressTemplateModel.PersonIncharge = s.PicNavigation?.UserName;
            jobProgressTemplateModel.NotificationId = s.NotificationId;
            jobProgressTemplateModel.SoftwareId = s.SoftwareId;
            jobProgressTemplateModel.Software = s.Software?.CodeValue;
            jobProgressTemplateModel.ModuleId = s.ModuleId;
            jobProgressTemplateModel.Module = s.Module?.Description;
            jobProgressTemplateModel.ItemId = s.ItemId;
            jobProgressTemplateModel.ItemNo = s.Item?.No;
            jobProgressTemplateModel.ModifiedByUserID = s.AddedByUserId;
            jobProgressTemplateModel.AddedByUserID = s.ModifiedByUserId;
            jobProgressTemplateModel.StatusCodeID = s.StatusCodeId;
            jobProgressTemplateModel.AddedByUser = s.AddedByUser.UserName;
            jobProgressTemplateModel.ModifiedByUser = s.ModifiedByUser?.UserName;
            jobProgressTemplateModel.AddedDate = s.AddedDate;
            jobProgressTemplateModel.ModifiedDate = s.ModifiedDate;
            jobProgressTemplateModel.StatusCode = s.StatusCode.CodeValue;
            jobProgressTemplateModel.ProfileId = s.ProfileId;
            jobProgressTemplateModel.ProfileNo = s.ProfileNo;
            jobProgressTemplateModel.ProfileName = s.Profile?.Name;
            jobProgressTemplateModel.ItemIds = s.JobProgressTemplateNavItem != null ? s.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == s.JobProgressTemplateId).Select(a => a.ItemId).ToList() : new List<long?>();
            jobProgressTemplateModel.UserType = s.JobProgressTemplateNotify != null ? s.JobProgressTemplateNotify.Select(a => a.UserType).Distinct().FirstOrDefault() : "user";
            jobProgressTemplateModel.EmployeeIDs = new List<long?>();
            jobProgressTemplateModel.UserGroupIDs = new List<long?>();

            List<JobProgressTemplateLineModel> jobProgressTemplateLineModels = GetJobProgressTemplateLines(jobProgressTemplateModel.JobProgressTemplateId);

            jobProgressTemplateModel.JobProgressTemplateLineModels = new List<JobProgressTemplateLineModel>();
            jobProgressTemplateModel.JobProgressTemplateLineModels = jobProgressTemplateLineModels;

            return jobProgressTemplateModel;
        }


        [HttpPost]
        [Route("GetJobProgressTemplateItemsByTemplate")]
        public JobProgressTemplateModel GetJobProgressTemplateItemsByTemplate(SearchModel searchModel)
        {
            var s = _context.JobProgressTemplate
                                .Include(a => a.Template)
                                .Include(r => r.JobCategory)
                                .Include(m => m.JobType)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(s => s.Module)
                                .Include(s => s.PicNavigation)
                                .Include(s => s.Software)
                                .Include(s => s.Item)
                                .Include(u => u.JobProgressTemplateNotify)
                                .Include(n => n.JobProgressTemplateNavItem)
                                .Include(p => p.Profile)
                                .FirstOrDefault(t => t.JobProgressTemplateId == searchModel.Id);
            JobProgressTemplateModel jobProgressTemplateModel = new JobProgressTemplateModel();

            jobProgressTemplateModel.JobProgressTemplateId = s.JobProgressTemplateId;
            jobProgressTemplateModel.TemplateId = s.TemplateId;
            jobProgressTemplateModel.TemplateName = s.Template?.Value;
            jobProgressTemplateModel.JobTypeId = s.JobTypeId;
            jobProgressTemplateModel.JobCategoryId = s.JobCategoryId;
            jobProgressTemplateModel.JobCategory = s.JobCategory?.Value;
            jobProgressTemplateModel.JobType = s.JobType?.Value;
            jobProgressTemplateModel.SessionId = s.SessionId;
            jobProgressTemplateModel.Objective = s.Objective;
            jobProgressTemplateModel.Pic = s.Pic;
            jobProgressTemplateModel.PersonIncharge = s.PicNavigation?.UserName;
            jobProgressTemplateModel.NotificationId = s.NotificationId;
            jobProgressTemplateModel.SoftwareId = s.SoftwareId;
            jobProgressTemplateModel.Software = s.Software?.CodeValue;
            jobProgressTemplateModel.ModuleId = s.ModuleId;
            jobProgressTemplateModel.Module = s.Module?.Description;
            jobProgressTemplateModel.ItemId = s.ItemId;
            jobProgressTemplateModel.ItemNo = s.Item?.No;
            jobProgressTemplateModel.ModifiedByUserID = s.AddedByUserId;
            jobProgressTemplateModel.AddedByUserID = s.ModifiedByUserId;
            jobProgressTemplateModel.StatusCodeID = s.StatusCodeId;
            jobProgressTemplateModel.AddedByUser = s.AddedByUser.UserName;
            jobProgressTemplateModel.ModifiedByUser = s.ModifiedByUser?.UserName;
            jobProgressTemplateModel.AddedDate = s.AddedDate;
            jobProgressTemplateModel.ModifiedDate = s.ModifiedDate;
            jobProgressTemplateModel.StatusCode = s.StatusCode.CodeValue;
            jobProgressTemplateModel.ProfileId = s.ProfileId;
            jobProgressTemplateModel.ProfileNo = s.ProfileNo;
            jobProgressTemplateModel.ProfileName = s.Profile?.Name;
            jobProgressTemplateModel.ItemIds = s.JobProgressTemplateNavItem != null ? s.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == s.JobProgressTemplateId).Select(a => a.ItemId).ToList() : new List<long?>();
            jobProgressTemplateModel.UserType = s.JobProgressTemplateNotify != null ? s.JobProgressTemplateNotify.Select(a => a.UserType).Distinct().FirstOrDefault() : "user";
            jobProgressTemplateModel.EmployeeIDs = new List<long?>();
            jobProgressTemplateModel.UserGroupIDs = new List<long?>();

            List<JobProgressTemplateLineModel> jobProgressTemplateLineModels = GetJobProgressTemplateLinesByTemplate(searchModel);

            jobProgressTemplateModel.JobProgressTemplateLineModels = new List<JobProgressTemplateLineModel>();
            jobProgressTemplateModel.JobProgressTemplateLineModels = jobProgressTemplateLineModels;

            return jobProgressTemplateModel;
        }

        #endregion

        #region JobProgressTemplateLine

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetJobProgressTemplateLines")]
        public List<JobProgressTemplateLineModel> GetJobProgressTemplateLines(long? id)
        {
            var jobProgressTempletateLines = _context.JobProgressTempletateLine
                                 .Include(c => c.Profile)
                                 .Include(a => a.AssignedToNavigation)
                                 .Include(p => p.PiaNavigation).Include(j => j.JobProgressTemplate)
                                 .Include(j => j.JobProgressTemplate.Template)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                .AsNoTracking().Where(o => o.JobProgressTemplateId == id).ToList();
            List<JobProgressTemplateLineModel> jobProgressTemplateLineModels = new List<JobProgressTemplateLineModel>();
            jobProgressTempletateLines.ForEach(s =>
            {
                JobProgressTemplateLineModel jobProgressTemplateLineModel = new JobProgressTemplateLineModel
                {
                    JobProgressTemplateId = s.JobProgressTemplateId,
                    JobProgressTemplateLineId = s.JobProgressTemplateLineId,
                    ProfileId = s.JobProgressTemplate?.ProfileId,
                    ProfileName = s.JobProgressTemplate?.Profile?.Name,
                    Title = s.JobProgressTemplate?.Template?.Value,

                    ActionNo = s.ActionNo,
                    SectionNo = s.SectionNo,
                    SectionDescription = s.SectionDescription,
                    SubSectionNo = s.SubSectionNo,
                    SubSectionDescription = s.SubSectionDescription,
                    Pia = s.Pia,
                    PersonInAction = s.PiaNavigation?.UserName,
                    StartDate = s.StartDate,
                    CompletionDate = s.CompletionDate,
                    Message = s.Message,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                    NoOfWorkingDays = s.NoOfWorkingDays,
                    SpecificDateOfEachMonth = s.SpecificDateOfEachMonth,
                    JobDescription = s.JobDescription,
                    AssignedTo = s.AssignedTo,
                    AssignedToName = s.AssignedToNavigation?.UserName,
                    JobProgressTemplateLineLineProcessModels = GetJobProgressTemplateLineLine(s.JobProgressTemplateLineId),

                };
                jobProgressTemplateLineModels.Add(jobProgressTemplateLineModel);
            });
            if (jobProgressTemplateLineModels.Any())
            {
                var templateLineIds = jobProgressTemplateLineModels.Select(s => s.JobProgressTemplateLineId).ToList();

                var templateLineProcessItems = _context.JobProgressTemplateLineProcess.Include(c => c.JobProgressTemplate).Include(c => c.JobProgressTemplate.Template).Include(s => s.StatusCode).Where(s => s.JobProgressTemplateLineId != null && templateLineIds.Contains(s.JobProgressTemplateLineId.Value)).ToList();
                if (templateLineProcessItems.Any())
                {
                    var sessionIds = templateLineProcessItems.Where(p => p.SessionId != null).Select(s => s.SessionId).ToList();
                    var processStatusItems = _context.JobProgressTemplateLineProcessStatus.Include(p => p.ProcessStatus).Where(s => sessionIds.Contains(s.SessionId)).ToList();
                    templateLineProcessItems.ForEach(p =>
                     {
                         var jobProgressTemplateLine = jobProgressTemplateLineModels.FirstOrDefault(s => s.JobProgressTemplateLineId == p.JobProgressTemplateLineId);
                         if (jobProgressTemplateLine != null)
                         {
                             var processStatus = processStatusItems.FirstOrDefault(f => f.TemplateId == jobProgressTemplateLine.JobProgressTemplateId && f.TemplateLineId == jobProgressTemplateLine.JobProgressTemplateLineId && f.SessionId == p.SessionId);
                             jobProgressTemplateLine.ProcessStatusID = processStatus?.ProcessStatusId;
                             jobProgressTemplateLine.SessionId = processStatus?.SessionId;
                             jobProgressTemplateLine.ProcessStatus = processStatus?.ProcessStatus?.CodeValue;
                             jobProgressTemplateLine.ActualStartDate = p.ActualStartDate;
                             jobProgressTemplateLine.ActualCompletionDate = p.ActualCompletionDate;
                             jobProgressTemplateLine.StartDate = p.StartDate;
                             jobProgressTemplateLine.CompletionDate = p.CompletionDate;
                             jobProgressTemplateLine.Pia = p.Pia;
                             jobProgressTemplateLine.From = p.StartDate;
                             jobProgressTemplateLine.Title = p.JobProgressTemplate.Template.Value;
                             jobProgressTemplateLine.Description = p.JobDescription;
                             jobProgressTemplateLine.JobProgressTemplateLineProcessId = p.JobProgressTemplateLineProcessId;
                         }
                     });
                }
            }
            return jobProgressTemplateLineModels.OrderBy(s => s.JobProgressTemplateLineId).ToList();
        }

        [HttpPost]
        [Route("GetJobProgressTemplateLinesByTemplate")]
        public List<JobProgressTemplateLineModel> GetJobProgressTemplateLinesByTemplate(SearchModel searchModel)
        {
            var jobProgressTempletateLines = _context.JobProgressTempletateLine
                                 .Include(c => c.Profile)
                                 .Include(a => a.AssignedToNavigation)
                                 .Include(p => p.PiaNavigation).Include(j => j.JobProgressTemplate)
                                 .Include(j => j.JobProgressTemplate.Template)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                .AsNoTracking().Where(o => o.JobProgressTemplateId == searchModel.Id).ToList();
            List<JobProgressTemplateLineModel> jobProgressTemplateLineModels = new List<JobProgressTemplateLineModel>();
            jobProgressTempletateLines.ForEach(s =>
            {
                JobProgressTemplateLineModel jobProgressTemplateLineModel = new JobProgressTemplateLineModel
                {
                    JobProgressTemplateId = s.JobProgressTemplateId,
                    JobProgressTemplateLineId = s.JobProgressTemplateLineId,
                    ProfileId = s.JobProgressTemplate?.ProfileId,
                    ProfileName = s.JobProgressTemplate?.Profile?.Name,
                    Title = s.JobProgressTemplate.Template.Value,

                    ActionNo = s.ActionNo,
                    SectionNo = s.SectionNo,
                    SectionDescription = s.SectionDescription,
                    SubSectionNo = s.SubSectionNo,
                    SubSectionDescription = s.SubSectionDescription,
                    Pia = s.Pia,
                    PersonInAction = s.PiaNavigation?.UserName,
                    StartDate = s.StartDate,
                    CompletionDate = s.CompletionDate,
                    Message = s.Message,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                    NoOfWorkingDays = s.NoOfWorkingDays,
                    SpecificDateOfEachMonth = s.SpecificDateOfEachMonth,
                    JobDescription = s.JobDescription,
                    AssignedTo = s.AssignedTo,
                    AssignedToName = s.AssignedToNavigation?.UserName,
                };
                jobProgressTemplateLineModels.Add(jobProgressTemplateLineModel);
            });
            if (jobProgressTemplateLineModels.Any())
            {
                var templateLineIds = jobProgressTemplateLineModels.Select(s => s.JobProgressTemplateLineId).ToList();

                if (searchModel.Action == "Edit")
                {
                    var templateLineProcessItems = _context.JobProgressTemplateLineProcess.Include(c => c.JobProgressTemplate)
                                                                                            .Include(c => c.JobProgressTemplate.Template)
                                                                                            .Include(s => s.StatusCode)
                                                                                            .Include(s => s.PiaNavigation)
                                                                                            .Where(s => s.SessionId == searchModel.SessionID).ToList();
                    if (templateLineProcessItems.Any())
                    {
                        var processStatusItems = _context.JobProgressTemplateLineProcessStatus.Include(p => p.ProcessStatus).Where(s => s.SessionId == searchModel.SessionID).ToList();
                        templateLineProcessItems.ForEach(p =>
                        {
                            var jobProgressTemplateLine = jobProgressTemplateLineModels.FirstOrDefault(s => s.JobProgressTemplateLineId == p.JobProgressTemplateLineId);
                            if (jobProgressTemplateLine != null)
                            {
                                var processStatus = processStatusItems.FirstOrDefault(f => f.TemplateId == jobProgressTemplateLine.JobProgressTemplateId && f.TemplateLineId == jobProgressTemplateLine.JobProgressTemplateLineId && f.SessionId == p.SessionId);
                                jobProgressTemplateLine.ProcessStatusID = processStatus?.ProcessStatusId;
                                jobProgressTemplateLine.SessionId = processStatus?.SessionId;
                                jobProgressTemplateLine.ProcessStatus = processStatus?.ProcessStatus?.CodeValue;
                                jobProgressTemplateLine.ActualStartDate = p.ActualStartDate;
                                jobProgressTemplateLine.ActualCompletionDate = p.ActualCompletionDate;
                                jobProgressTemplateLine.StartDate = p.StartDate;
                                jobProgressTemplateLine.CompletionDate = p.CompletionDate;
                                jobProgressTemplateLine.From = p.StartDate;
                                jobProgressTemplateLine.Title = p.JobProgressTemplate.Template.Value;
                                jobProgressTemplateLine.Description = p.JobDescription;
                                jobProgressTemplateLine.Pia = p.Pia;
                                jobProgressTemplateLine.JobProgressTemplateLineProcessId = p.JobProgressTemplateLineProcessId;
                            }
                        });
                    }
                }

            }
            return jobProgressTemplateLineModels.OrderBy(s => s.JobProgressTemplateLineId).ToList();
        }


        [HttpGet]
        [Route("GetJobProgressTemplateProcessLines")]
        public List<JobProgressTemplateLineModel> GetJobProgressTemplateProcessLines(long? id)
        {
            var jobProgressTempletateLines = _context.JobProgressTempletateLine
                                 .Include(c => c.Profile)
                                 .Include(a => a.AssignedToNavigation)
                                 .Include(p => p.PiaNavigation).Include(j => j.JobProgressTemplate)
                                 .Include(j => j.JobProgressTemplate.Template)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                .AsNoTracking().Where(o => o.JobProgressTemplateId == id).ToList();
            List<JobProgressTemplateLineModel> jobProgressTemplateLineModels = new List<JobProgressTemplateLineModel>();
            jobProgressTempletateLines.ForEach(s =>
            {
                JobProgressTemplateLineModel jobProgressTemplateLineModel = new JobProgressTemplateLineModel
                {
                    JobProgressTemplateId = s.JobProgressTemplateId,
                    JobProgressTemplateLineId = s.JobProgressTemplateLineId,
                    ProfileId = s.JobProgressTemplate?.ProfileId,
                    ProfileName = s.JobProgressTemplate?.Profile?.Name,
                    TemplateName = s.JobProgressTemplate.Template.Value,
                    ActionNo = s.ActionNo,
                    SectionNo = s.SectionNo,
                    SectionDescription = s.SectionDescription,
                    SubSectionNo = s.SubSectionNo,
                    SubSectionDescription = s.SubSectionDescription,
                    Pia = s.Pia,
                    PersonInAction = s.PiaNavigation?.UserName,
                    StartDate = DateTime.Now,
                    From = DateTime.Now,
                    Description = s.JobDescription,
                    CompletionDate = s.CompletionDate,
                    Message = s.Message,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                    NoOfWorkingDays = s.NoOfWorkingDays,
                    SpecificDateOfEachMonth = s.SpecificDateOfEachMonth,
                    JobDescription = s.JobDescription,
                    AssignedTo = s.AssignedTo,
                    AssignedToName = s.AssignedToNavigation?.UserName,
                };
                jobProgressTemplateLineModels.Add(jobProgressTemplateLineModel);
            });
            List<JobProgressTemplateLineModel> jobProgressTemplateProcessLineModels = new List<JobProgressTemplateLineModel>();
            if (jobProgressTemplateLineModels.Any())
            {
                var templateLineIds = jobProgressTemplateLineModels.Select(s => s.JobProgressTemplateLineId).ToList();

                var templateLineProcessItems = _context.JobProgressTemplateLineProcess.Include(c => c.JobProgressTemplate).Include(c => c.JobProgressTemplate.Template).Include(s => s.StatusCode).Where(s => s.JobProgressTemplateLineId != null && templateLineIds.Contains(s.JobProgressTemplateLineId.Value)).ToList();
                if (templateLineProcessItems.Any())
                {
                    templateLineProcessItems.ForEach(p =>
                    {
                        var jobProgressTemplateLine = jobProgressTemplateLineModels.FirstOrDefault(s => s.JobProgressTemplateLineId == p.JobProgressTemplateLineId);
                        if (jobProgressTemplateLine != null)
                        {
                            jobProgressTemplateLine.ProcessStatusID = p.StatusCodeId.Value;
                            jobProgressTemplateLine.ProcessStatus = p.StatusCode?.CodeValue;
                            jobProgressTemplateLine.ActualStartDate = p.ActualStartDate;
                            jobProgressTemplateLine.ActualCompletionDate = p.ActualCompletionDate;
                            jobProgressTemplateLine.StartDate = p.StartDate;
                            jobProgressTemplateLine.CompletionDate = p.CompletionDate;
                            jobProgressTemplateLine.Pia = p.Pia;
                            jobProgressTemplateLine.From = p.StartDate;
                            jobProgressTemplateLine.Title = p.StatusCode?.CodeValue;
                            jobProgressTemplateLine.Description = p.JobDescription;
                            jobProgressTemplateLine.JobProgressTemplateLineProcessId = p.JobProgressTemplateLineProcessId;
                        }
                    });
                }
            }
            return jobProgressTemplateLineModels.OrderBy(s => s.JobProgressTemplateLineId).ToList();
        }
        private JobProgressTemplateLineActionModel GenerateActionSectionNo()
        {
            JobProgressTemplateLineActionModel JobProgressTemplateLineActionModel = new JobProgressTemplateLineActionModel();
            var jobProgressTempletateLine = _context.JobProgressTempletateLine.OrderByDescending(a => a.JobProgressTemplateLineId).FirstOrDefault();
            var jobProgressTempletateLineCount = _context.JobProgressTempletateLine.Count();
            if (jobProgressTempletateLine != null)
            {
                JobProgressTemplateLineActionModel.SectionNo = jobProgressTempletateLine.SectionNo != null ? IncrementAlphabet(jobProgressTempletateLine.SectionNo) : "A";
                JobProgressTemplateLineActionModel.ActionNo = (jobProgressTempletateLineCount + 1).ToString();
            }
            else
            {
                JobProgressTemplateLineActionModel.SectionNo = "A";
                JobProgressTemplateLineActionModel.ActionNo = (jobProgressTempletateLineCount + 1).ToString();
            }
            return JobProgressTemplateLineActionModel;
        }
        // POST: api/WorkOrder
        [HttpPost]
        [Route("InsertJobProgressTemplateLine")]
        public JobProgressTemplateLineModel InsertJobProgressTemplateLine(JobProgressTemplateLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var section = GenerateActionSectionNo();
            var jobProgressTempletateLine = new JobProgressTempletateLine
            {
                JobProgressTemplateId = value.JobProgressTemplateId,
                ProfileId = value.ProfileId,
                ActionNo = section.ActionNo,
                SectionNo = section.SectionNo,
                SectionDescription = value.SectionDescription,
                SubSectionNo = value.SubSectionNo,
                SubSectionDescription = value.SubSectionDescription,
                Pia = value.Pia,
                StartDate = value.StartDate,
                CompletionDate = value.CompletionDate,
                Message = value.Message,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,
                NoOfWorkingDays = value.NoOfWorkingDays,
                SpecificDateOfEachMonth = value.SpecificDateOfEachMonth,
                JobDescription = value.JobDescription,
                AssignedTo = value.AssignedTo,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
            };
            _context.JobProgressTempletateLine.Add(jobProgressTempletateLine);
            _context.SaveChanges();
            value.JobProgressTemplateLineId = jobProgressTempletateLine.JobProgressTemplateLineId;
            value.SectionNo = jobProgressTempletateLine.SectionNo;
            value.ActionNo = jobProgressTempletateLine.ActionNo;
            //if (value.Pia != null)
            //{
            //    InsertJobProgressTemplateLineProcessPia(value);
            //}
            return value;
        }
        private void InsertJobProgressTemplateLineProcessPia(JobProgressTemplateLineModel value)
        {
            var jobProgressTemplate = _context.JobProgressTemplate.Where(w => w.JobProgressTemplateId == value.JobProgressTemplateId).FirstOrDefault();
            if (jobProgressTemplate != null && jobProgressTemplate.ProfileId != null)
            {
                value.ProfileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = jobProgressTemplate.ProfileId, AddedByUserID = value.AddedByUserID });
            }
            var jobProgressTemplateLineProcess = new JobProgressTemplateLineProcess
            {
                JobProgressTemplateId = value.JobProgressTemplateId,
                ActionNo = value.ActionNo,
                SectionNo = value.SectionNo,
                SectionDescription = value.SectionDescription,
                SubSectionNo = value.SubSectionNo,
                SubSectionDescription = value.SubSectionDescription,
                Pia = value.Pia,
                Pic = jobProgressTemplate?.Pic,
                StartDate = value.StartDate,
                CompletionDate = value.CompletionDate,
                Message = value.Message,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SpecificDateOfEachMonth = value.SpecificDateOfEachMonth,
                NoOfWorkingDays = value.NoOfWorkingDays,
                JobDescription = value.JobDescription,
                ProfileNo = value.ProfileNo,
                AssignedTo = value.AssignedTo,
            };
            _context.JobProgressTemplateLineProcess.Add(jobProgressTemplateLineProcess);
            _context.SaveChanges();
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateJobProgressTemplateLine")]
        public JobProgressTemplateLineModel UpdateJobProgressTemplateLine(JobProgressTemplateLineModel value)
        {
            var jobProgressTempletateLine = _context.JobProgressTempletateLine.SingleOrDefault(p => p.JobProgressTemplateLineId == value.JobProgressTemplateLineId);
            var pia = jobProgressTempletateLine.Pia;
            jobProgressTempletateLine.JobProgressTemplateId = value.JobProgressTemplateId;
            jobProgressTempletateLine.ProfileId = value.ProfileId;
            jobProgressTempletateLine.ActionNo = value.ActionNo;
            jobProgressTempletateLine.SectionNo = value.SectionNo;
            jobProgressTempletateLine.SectionDescription = value.SectionDescription;
            jobProgressTempletateLine.SubSectionNo = value.SubSectionNo;
            jobProgressTempletateLine.SubSectionDescription = value.SubSectionDescription;

            jobProgressTempletateLine.StartDate = value.StartDate;

            jobProgressTempletateLine.Message = value.Message;
            jobProgressTempletateLine.ModifiedByUserId = value.ModifiedByUserID;
            jobProgressTempletateLine.ModifiedDate = DateTime.Now;
            jobProgressTempletateLine.StatusCodeId = value.StatusCodeID.Value;
            jobProgressTempletateLine.NoOfWorkingDays = value.NoOfWorkingDays;
            jobProgressTempletateLine.SpecificDateOfEachMonth = value.SpecificDateOfEachMonth;
            jobProgressTempletateLine.JobDescription = value.JobDescription;
            jobProgressTempletateLine.AssignedTo = value.AssignedTo;
            if (value.StatusCodeID == 1420)
            {
                if (value.StartDate != null && value.NoOfWorkingDays > 0)
                {
                    jobProgressTempletateLine.CompletionDate = AddWorkingDays(value.StartDate, value.NoOfWorkingDays);
                    value.CompletionDate = jobProgressTempletateLine.CompletionDate;
                }
                jobProgressTempletateLine.StatusCodeId = 213;
                //jobProgressTempletateLine.Pia = value.Pia;
            }
            if (value.StatusCodeID == 213)
            {
                jobProgressTempletateLine.StatusCodeId = 1421;
                //jobProgressTempletateLine.Pia = value.Pia;
                var nextjobProgressTempletateLine = _context.JobProgressTempletateLine.OrderBy(o => o.JobProgressTemplateLineId).FirstOrDefault(s => s.JobProgressTemplateLineId > value.JobProgressTemplateLineId);
                if (nextjobProgressTempletateLine != null)
                {
                    nextjobProgressTempletateLine.Pia = value.Pia;
                    _context.SaveChanges();
                }
            }
            value.StatusCodeID = jobProgressTempletateLine.StatusCodeId;
            _context.SaveChanges();
            if (value.Pia != pia)
            {
                InsertJobProgressTemplateLineProcessPia(value);
            }
            return value;
        }
        public static DateTime AddWorkingDays(DateTime? date, int? daysToAdd)
        {
            while (daysToAdd > 0)
            {
                date = date.Value.AddDays(1);

                if (date.Value.DayOfWeek != DayOfWeek.Saturday && date.Value.DayOfWeek != DayOfWeek.Sunday)
                {
                    daysToAdd -= 1;
                }
            }

            return date.Value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteJobProgressTemplateLine")]
        public ActionResult<string> DeleteJobProgressTemplateLine(int id)
        {
            try
            {
                var jobProgressTempletateLine = _context.JobProgressTempletateLine.SingleOrDefault(p => p.JobProgressTemplateLineId == id);
                if (jobProgressTempletateLine != null)
                {
                    var jobProgressTempletateLineProcess = _context.JobProgressTemplateLineLineProcess.Where(p => p.JobProgressTemplateLineLineProcessId == id).ToList();
                    if (jobProgressTempletateLineProcess != null)
                    {
                        jobProgressTempletateLineProcess.ForEach(s =>
                        {
                            var appWikiTaskLinkRemove = _context.JobProgressTemplateLineLineTaskLink.Where(w => w.JobProgressTemplateLineLineId == s.JobProgressTemplateLineLineProcessId).ToList();
                            if (appWikiTaskLinkRemove.Count > 0)
                            {
                                _context.JobProgressTemplateLineLineTaskLink.RemoveRange(appWikiTaskLinkRemove);
                                _context.SaveChanges();
                            }
                        });

                        _context.JobProgressTemplateLineLineProcess.RemoveRange(jobProgressTempletateLineProcess);
                        _context.SaveChanges();
                    }
                    _context.JobProgressTempletateLine.Remove(jobProgressTempletateLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot be delete!.line Reference to others");
            }
        }
        #endregion
        #region Versions
        [HttpGet]
        [Route("GetJobProgressTemplateVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<JobProgressTemplateModel>>> GetJobProgressTemplateVersion(string sessionID)
        {
            return await _repository.GetList<JobProgressTemplateModel>(sessionID);
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<JobProgressTemplateModel> CreateVersion(JobProgressTemplateModel value)
        {
            try
            {
                var versionData = _context.JobProgressTemplate.Include(a => a.JobProgressTempletateLine).SingleOrDefault(p => p.JobProgressTemplateId == value.JobProgressTemplateId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<JobProgressTemplateModel>(versionData);
                    verObject.JobProgressTemplateLineModels = GetJobProgressTemplateLines(versionData.JobProgressTemplateId);
                    var versionInfo = new TableDataVersionInfoModel<JobProgressTemplateModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "JobProgressTemplate",
                        PrimaryKey = value.JobProgressTemplateId,
                        ReferenceInfo = value.ReferenceInfo,
                        StatusCodeID = value.StatusCodeID.Value,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<JobProgressTemplateModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<JobProgressTemplateModel>(tableData.JsonData);

                    var versionData = _context.JobProgressTemplate.Include(a => a.JobProgressTemplateNotify).Include(n => n.JobProgressTemplateNavItem).Include(m => m.JobProgressTempletateLine).SingleOrDefault(p => p.JobProgressTemplateId == verObject.JobProgressTemplateId);


                    if (verObject != null && versionData != null)
                    {
                        versionData.TemplateId = verObject.TemplateId;
                        versionData.JobTypeId = verObject.JobTypeId;
                        versionData.SessionId = verObject.SessionId;
                        versionData.JobCategoryId = verObject.JobCategoryId;
                        versionData.Objective = verObject.Objective;
                        versionData.Pic = verObject.Pic;
                        versionData.NotificationId = verObject.NotificationId;
                        versionData.SoftwareId = verObject.SoftwareId;
                        versionData.ModuleId = verObject.ModuleId;
                        versionData.ItemId = verObject.ItemId;
                        versionData.StatusCodeId = verObject.StatusCodeID.Value;
                        versionData.ModifiedByUserId = verObject.ModifiedByUserID;
                        versionData.ModifiedDate = DateTime.Now;
                        versionData.SessionId = verObject.SessionId;
                        versionData.ProfileId = verObject.ProfileId;
                        versionData.ProfileNo = verObject.ProfileNo;
                        var JobProgressTempletateLine = _context.JobProgressTempletateLine.Where(w => w.JobProgressTemplateId == versionData.JobProgressTemplateId).ToList();
                        var ApplicationWikiRecurrences = _context.JobProgressTemplateRecurrence.FirstOrDefault(p => p.JobProgressTemplateId == versionData.JobProgressTemplateId);
                        if (JobProgressTempletateLine != null)
                        {
                            var ApplicationWikiRecurrenceData = _context.JobProgressTemplateRecurrence.Where(p => p.JobProgressTemplateId == versionData.JobProgressTemplateId).ToList();
                            _context.JobProgressTemplateRecurrence.RemoveRange(ApplicationWikiRecurrenceData);
                            _context.SaveChanges();
                            JobProgressTempletateLine.ForEach(a =>
                            {

                                var JobProgressTemplateLineProcess = _context.JobProgressTemplateLineProcess.Where(w => w.JobProgressTemplateLineId == a.JobProgressTemplateLineId).ToList();
                                _context.JobProgressTemplateLineProcess.RemoveRange(JobProgressTemplateLineProcess);
                                _context.SaveChanges();
                                var JobProgressTemplateLineProcessStatus = _context.JobProgressTemplateLineProcessStatus.Where(w => w.TemplateLineId == a.JobProgressTemplateLineId).ToList();
                                _context.JobProgressTemplateLineProcessStatus.RemoveRange(JobProgressTemplateLineProcessStatus);
                                _context.SaveChanges();
                                var JobProgressTemplateLineLineProcess = _context.JobProgressTemplateLineLineProcess.Where(w => w.JobProgressTemplateLineProcessId == a.JobProgressTemplateLineId).ToList();
                                if (JobProgressTemplateLineLineProcess != null)
                                {
                                    JobProgressTemplateLineLineProcess.ForEach(b =>
                                    {
                                        var JobProgressTemplateLineLineTaskLink = _context.JobProgressTemplateLineLineTaskLink.Where(w => w.JobProgressTemplateLineLineId == b.JobProgressTemplateLineLineProcessId).ToList();
                                        _context.JobProgressTemplateLineLineTaskLink.RemoveRange(JobProgressTemplateLineLineTaskLink);
                                        _context.SaveChanges();
                                        var JobProgressTemplateLineProcessPia = _context.JobProgressTemplateLineProcessPia.Where(w => w.JobProgressTemplateLineLineProcessId == b.JobProgressTemplateLineLineProcessId).ToList();
                                        _context.JobProgressTemplateLineProcessPia.RemoveRange(JobProgressTemplateLineProcessPia);
                                        _context.SaveChanges();
                                        var query = string.Format("delete from  JobProgressTemplateLineLineProcess  Where JobProgressTemplateLineLineProcessId= {0}", b.JobProgressTemplateLineLineProcessId);
                                        _context.Database.ExecuteSqlRaw(query);
                                    });
                                }
                            });
                            var query = string.Format("delete from  JobProgressTempletateLine  Where JobProgressTemplateId= {0}", versionData.JobProgressTemplateId);
                            _context.Database.ExecuteSqlRaw(query);
                        }
                        if (ApplicationWikiRecurrences != null)
                        {
                            var ApplicationWikiRecurrence = new JobProgressTemplateRecurrence
                            {
                                JobProgressTemplateId = versionData.JobProgressTemplateId,
                                TypeId = ApplicationWikiRecurrences.TypeId,
                                RepeatNos = ApplicationWikiRecurrences.RepeatNos,
                                OccurenceOptionId = ApplicationWikiRecurrences.OccurenceOptionId,
                                NoOfOccurences = ApplicationWikiRecurrences.NoOfOccurences,
                                AddedByUserId = ApplicationWikiRecurrences.AddedByUserId,
                                AddedDate = DateTime.Now,
                                StatusCodeId = ApplicationWikiRecurrences.StatusCodeId.Value,
                                Sunday = ApplicationWikiRecurrences.Sunday,
                                Monday = ApplicationWikiRecurrences.Monday,
                                Tuesday = ApplicationWikiRecurrences.Tuesday,
                                Wednesday = ApplicationWikiRecurrences.Wednesday,
                                Thursday = ApplicationWikiRecurrences.Thursday,
                                Friday = ApplicationWikiRecurrences.Friday,
                                Saturyday = ApplicationWikiRecurrences.Saturyday,
                                StartDate = ApplicationWikiRecurrences.StartDate,
                                EndDate = ApplicationWikiRecurrences.EndDate,
                            };
                            _context.JobProgressTemplateRecurrence.Add(ApplicationWikiRecurrence);
                            _context.SaveChanges();
                        }
                        if (verObject.JobProgressTemplateLineModels != null)
                        {
                            verObject.JobProgressTemplateLineModels.ForEach(f =>
                            {
                                var jobProgressTempletateLine = new JobProgressTempletateLine
                                {
                                    JobProgressTemplateId = versionData.JobProgressTemplateId,
                                    ProfileId = f.ProfileId,
                                    ActionNo = f.ActionNo,
                                    SectionNo = f.SectionNo,
                                    SectionDescription = f.SectionDescription,
                                    SubSectionNo = f.SubSectionNo,
                                    SubSectionDescription = f.SubSectionDescription,
                                    Pia = f.Pia,
                                    StartDate = f.StartDate,
                                    CompletionDate = f.CompletionDate,
                                    Message = f.Message,
                                    StatusCodeId = f.StatusCodeID.Value,
                                    AddedByUserId = verObject.ModifiedByUserID,
                                    AddedDate = DateTime.Now,
                                    NoOfWorkingDays = f.NoOfWorkingDays,
                                    JobDescription = f.JobDescription,
                                    SpecificDateOfEachMonth = f.SpecificDateOfEachMonth,
                                    //ModifiedByUserId = value.ModifiedByUserID,
                                    //ModifiedDate = DateTime.Now,
                                };
                                _context.JobProgressTempletateLine.Add(jobProgressTempletateLine);
                                _context.SaveChanges();
                                if (f.JobProgressTemplateLineLineProcessModels != null)
                                {
                                    f.JobProgressTemplateLineLineProcessModels.ForEach(j =>
                                    {
                                        var jobProgressTemplateLineLineProcess = new JobProgressTemplateLineLineProcess
                                        {
                                            JobProgressTemplateLineProcessId = jobProgressTempletateLine.JobProgressTemplateLineId,
                                            SubSectionNo = j.SubSectionNo,
                                            SubSectionDescription = j.SubSectionDescription,
                                            Message = j.Message,
                                            AddedByUserId = j.AddedByUserID,
                                            AddedDate = DateTime.Now,
                                            StatusCodeId = j.StatusCodeID.Value,
                                            //SessionId = sessionId,
                                            NoOfWorkingDays = j.NoOfWorkingDays,
                                            ModifiedByUserId = j.AddedByUserID,
                                            ModifiedDate = DateTime.Now,
                                            MonitorTypeOfJobActionId = j.MonitorTypeOfJobActionId,
                                            UserType = j.UserType,
                                            UserGroupId = j.UserGroupId,
                                            PlantId = j.PlantId,
                                        };
                                        _context.JobProgressTemplateLineLineProcess.Add(jobProgressTemplateLineLineProcess);
                                        _context.SaveChanges();
                                        if (j.PiaIds != null && j.PiaIds.Count > 0)
                                        {
                                            j.PiaIds.ForEach(h =>
                                            {
                                                JobProgressTemplateLineProcessPia jobProgressTemplateLineProcessPia = new JobProgressTemplateLineProcessPia
                                                {
                                                    JobProgressTemplateLineLineProcessId = jobProgressTemplateLineLineProcess.JobProgressTemplateLineLineProcessId,
                                                    Pia = h,
                                                };
                                                _context.JobProgressTemplateLineProcessPia.Add(jobProgressTemplateLineProcessPia);
                                                _context.SaveChanges();
                                            });
                                        }
                                        if (j.AppWikiTaskLinkModel != null && j.AppWikiTaskLinkModel.Count > 0)
                                        {
                                            j.AppWikiTaskLinkModel.ForEach(h =>
                                            {
                                                JobProgressTemplateLineLineTaskLink appWikiTaskLink = new JobProgressTemplateLineLineTaskLink
                                                {
                                                    JobProgressTemplateLineLineId = jobProgressTemplateLineLineProcess.JobProgressTemplateLineLineProcessId,
                                                    TaskLink = h.TaskLink,
                                                    Subject = h.Subject,
                                                    IsLatest = h.IsLatest,
                                                    IsWiki = h.IsWiki,
                                                    IsRequireUploadFolder = h.IsRequireUploadFolder,
                                                    IsWithCompleteCheck = h.IsWithCompleteCheck,
                                                    IsEachSubjectHeadingUpload = h.IsEachSubjectHeadingUpload,
                                                };
                                                _context.JobProgressTemplateLineLineTaskLink.Add(appWikiTaskLink);
                                                _context.SaveChanges();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        _context.JobProgressTemplateNavItem.RemoveRange(versionData.JobProgressTemplateNavItem);
                        _context.SaveChanges();
                        if (verObject.ItemIds != null)
                        {
                            verObject.ItemIds.ForEach(h =>
                            {
                                var JobProgressTemplateNavItem = new JobProgressTemplateNavItem
                                {
                                    ItemId = h,
                                    JobProgressTemplateId = versionData.JobProgressTemplateId,
                                };
                                _context.JobProgressTemplateNavItem.Add(JobProgressTemplateNavItem);
                            });
                        }
                        _context.JobProgressTemplateNotify.RemoveRange(versionData.JobProgressTemplateNotify);
                        _context.SaveChanges();
                        if (verObject.JobProgressTemplateNotifyModels != null)
                        {
                            verObject.JobProgressTemplateNotifyModels.ForEach(h =>
                            {
                                var JobProgressTemplateNotify = new JobProgressTemplateNotify
                                {
                                    JobProgressTemplateId = h.JobProgressTemplateId,
                                    UserGroupId = h.UserGroupId,
                                    UserType = h.UserType,
                                    EmployeeId = h.EmployeeId,
                                };
                                _context.JobProgressTemplateNotify.Add(JobProgressTemplateNotify);
                            });
                        }
                        _context.SaveChanges();
                    }
                    return verObject;
                }
                return new JobProgressTemplateModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<JobProgressTemplateModel> ApplyVersion(JobProgressTemplateModel value)
        {
             try
              {
            var versionData = _context.JobProgressTemplate.Include(a => a.JobProgressTemplateNotify).Include(n => n.JobProgressTemplateNavItem).Include(d => d.JobProgressTempletateLine).SingleOrDefault(p => p.JobProgressTemplateId == value.JobProgressTemplateId);

            if (versionData != null)
            {

                var JobProgressTempletateLine = _context.JobProgressTempletateLine.Where(w => w.JobProgressTemplateId == versionData.JobProgressTemplateId).ToList();
                var ApplicationWikiRecurrences = _context.JobProgressTemplateRecurrence.FirstOrDefault(p => p.JobProgressTemplateId == versionData.JobProgressTemplateId);

                if (JobProgressTempletateLine != null)
                {
                    var ApplicationWikiRecurrenceData = _context.JobProgressTemplateRecurrence.Where(p => p.JobProgressTemplateId == versionData.JobProgressTemplateId).ToList();
                    _context.JobProgressTemplateRecurrence.RemoveRange(ApplicationWikiRecurrenceData);
                    _context.SaveChanges();
                    JobProgressTempletateLine.ForEach(a =>
                    {

                        var JobProgressTemplateLineProcess = _context.JobProgressTemplateLineProcess.Where(w => w.JobProgressTemplateLineId == a.JobProgressTemplateLineId).ToList();
                        _context.JobProgressTemplateLineProcess.RemoveRange(JobProgressTemplateLineProcess);
                        _context.SaveChanges();
                        var JobProgressTemplateLineProcessStatus = _context.JobProgressTemplateLineProcessStatus.Where(w => w.TemplateLineId == a.JobProgressTemplateLineId).ToList();
                        _context.JobProgressTemplateLineProcessStatus.RemoveRange(JobProgressTemplateLineProcessStatus);
                        _context.SaveChanges();
                        var JobProgressTemplateLineLineProcess = _context.JobProgressTemplateLineLineProcess.Where(w => w.JobProgressTemplateLineProcessId == a.JobProgressTemplateLineId).ToList();
                        if (JobProgressTemplateLineLineProcess != null)
                        {
                            JobProgressTemplateLineLineProcess.ForEach(b =>
                            {
                                var JobProgressTemplateLineLineTaskLink = _context.JobProgressTemplateLineLineTaskLink.Where(w => w.JobProgressTemplateLineLineId == b.JobProgressTemplateLineLineProcessId).ToList();
                                _context.JobProgressTemplateLineLineTaskLink.RemoveRange(JobProgressTemplateLineLineTaskLink);
                                _context.SaveChanges();
                                var JobProgressTemplateLineProcessPia = _context.JobProgressTemplateLineProcessPia.Where(w => w.JobProgressTemplateLineLineProcessId == b.JobProgressTemplateLineLineProcessId).ToList();
                                _context.JobProgressTemplateLineProcessPia.RemoveRange(JobProgressTemplateLineProcessPia);
                                _context.SaveChanges();
                                var query = string.Format("delete from  JobProgressTemplateLineLineProcess  Where JobProgressTemplateLineLineProcessId= {0}", b.JobProgressTemplateLineLineProcessId);
                                _context.Database.ExecuteSqlRaw(query);
                            });
                        }
                    });
                    var query = string.Format("delete from  JobProgressTempletateLine  Where JobProgressTemplateId= {0}", versionData.JobProgressTemplateId);
                    _context.Database.ExecuteSqlRaw(query);
                }
                versionData.TemplateId = value.TemplateId;
                versionData.JobTypeId = value.JobTypeId;
                versionData.SessionId = value.SessionId;
                versionData.JobCategoryId = value.JobCategoryId;
                versionData.Objective = value.Objective;
                versionData.Pic = value.Pic;
                versionData.NotificationId = value.NotificationId;
                versionData.SoftwareId = value.SoftwareId;
                versionData.ModuleId = value.ModuleId;
                versionData.ItemId = value.ItemId;
                versionData.ModifiedByUserId = value.ModifiedByUserID;
                versionData.ModifiedDate = DateTime.Now;
                versionData.StatusCodeId = value.StatusCodeID.Value;
                versionData.ProfileId = value.ProfileId;
                versionData.ProfileNo = value.ProfileNo;
                if (ApplicationWikiRecurrences != null)
                {
                    var ApplicationWikiRecurrence = new JobProgressTemplateRecurrence
                    {
                        JobProgressTemplateId = versionData.JobProgressTemplateId,
                        TypeId = ApplicationWikiRecurrences.TypeId,
                        RepeatNos = ApplicationWikiRecurrences.RepeatNos,
                        OccurenceOptionId = ApplicationWikiRecurrences.OccurenceOptionId,
                        NoOfOccurences = ApplicationWikiRecurrences.NoOfOccurences,
                        AddedByUserId = ApplicationWikiRecurrences.AddedByUserId,
                        AddedDate = DateTime.Now,
                        StatusCodeId = ApplicationWikiRecurrences.StatusCodeId.Value,
                        Sunday = ApplicationWikiRecurrences.Sunday,
                        Monday = ApplicationWikiRecurrences.Monday,
                        Tuesday = ApplicationWikiRecurrences.Tuesday,
                        Wednesday = ApplicationWikiRecurrences.Wednesday,
                        Thursday = ApplicationWikiRecurrences.Thursday,
                        Friday = ApplicationWikiRecurrences.Friday,
                        Saturyday = ApplicationWikiRecurrences.Saturyday,
                        StartDate = ApplicationWikiRecurrences.StartDate,
                        EndDate = ApplicationWikiRecurrences.EndDate,
                    };
                    _context.JobProgressTemplateRecurrence.Add(ApplicationWikiRecurrence);
                    _context.SaveChanges();
                }
                if (value.JobProgressTemplateLineModels != null)
                {
                    value.JobProgressTemplateLineModels.ForEach(f =>
                    {
                        var jobProgressTempletateLine = new JobProgressTempletateLine
                        {
                            JobProgressTemplateId = versionData.JobProgressTemplateId,
                            ProfileId = f.ProfileId,
                            ActionNo = f.ActionNo,
                            SectionNo = f.SectionNo,
                            SectionDescription = f.SectionDescription,
                            SubSectionNo = f.SubSectionNo,
                            SubSectionDescription = f.SubSectionDescription,
                            Pia = f.Pia,
                            StartDate = f.StartDate,
                            CompletionDate = f.CompletionDate,
                            Message = f.Message,
                            StatusCodeId = f.StatusCodeID.Value,
                            AddedByUserId = value.ModifiedByUserID,
                            AddedDate = DateTime.Now,
                            NoOfWorkingDays = f.NoOfWorkingDays,
                            JobDescription = f.JobDescription,
                            SpecificDateOfEachMonth = f.SpecificDateOfEachMonth,
                                //ModifiedByUserId = value.ModifiedByUserID,
                                //ModifiedDate = DateTime.Now,
                            };
                        _context.JobProgressTempletateLine.Add(jobProgressTempletateLine);
                        _context.SaveChanges();
                        if (f.JobProgressTemplateLineLineProcessModels != null)
                        {
                            f.JobProgressTemplateLineLineProcessModels.ForEach(j =>
                            {
                                var jobProgressTemplateLineLineProcess = new JobProgressTemplateLineLineProcess
                                {
                                    JobProgressTemplateLineProcessId = jobProgressTempletateLine.JobProgressTemplateLineId,
                                    SubSectionNo = j.SubSectionNo,
                                    SubSectionDescription = j.SubSectionDescription,
                                    Message = j.Message,
                                    AddedByUserId = j.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = j.StatusCodeID.Value,
                                        //SessionId = sessionId,
                                        NoOfWorkingDays = j.NoOfWorkingDays,
                                    ModifiedByUserId = j.AddedByUserID,
                                    ModifiedDate = DateTime.Now,
                                    MonitorTypeOfJobActionId = j.MonitorTypeOfJobActionId,
                                    UserType = j.UserType,
                                    UserGroupId = j.UserGroupId,
                                    PlantId = j.PlantId,
                                };
                                _context.JobProgressTemplateLineLineProcess.Add(jobProgressTemplateLineLineProcess);
                                _context.SaveChanges();
                                if (j.PiaIds != null && j.PiaIds.Count > 0)
                                {
                                    j.PiaIds.ForEach(h =>
                                    {
                                        JobProgressTemplateLineProcessPia jobProgressTemplateLineProcessPia = new JobProgressTemplateLineProcessPia
                                        {
                                            JobProgressTemplateLineLineProcessId = jobProgressTemplateLineLineProcess.JobProgressTemplateLineLineProcessId,
                                            Pia = h,
                                        };
                                        _context.JobProgressTemplateLineProcessPia.Add(jobProgressTemplateLineProcessPia);
                                        _context.SaveChanges();
                                    });
                                }
                                if (j.AppWikiTaskLinkModel != null && j.AppWikiTaskLinkModel.Count > 0)
                                {
                                    j.AppWikiTaskLinkModel.ForEach(h =>
                                    {
                                        JobProgressTemplateLineLineTaskLink appWikiTaskLink = new JobProgressTemplateLineLineTaskLink
                                        {
                                            JobProgressTemplateLineLineId = jobProgressTemplateLineLineProcess.JobProgressTemplateLineLineProcessId,
                                            TaskLink = h.TaskLink,
                                            Subject = h.Subject,
                                            IsLatest = h.IsLatest,
                                            IsWiki = h.IsWiki,
                                            IsRequireUploadFolder = h.IsRequireUploadFolder,
                                            IsWithCompleteCheck = h.IsWithCompleteCheck,
                                            IsEachSubjectHeadingUpload = h.IsEachSubjectHeadingUpload,
                                        };
                                        _context.JobProgressTemplateLineLineTaskLink.Add(appWikiTaskLink);
                                        _context.SaveChanges();
                                    });
                                }
                            });
                        }

                    });
                }
                _context.JobProgressTemplateNavItem.RemoveRange(versionData.JobProgressTemplateNavItem);
                _context.SaveChanges();
                if (value.ItemIds != null)
                {
                    value.ItemIds.ForEach(h =>
                    {
                        var JobProgressTemplateNavItem = new JobProgressTemplateNavItem
                        {
                            ItemId = h,
                            JobProgressTemplateId = versionData.JobProgressTemplateId,
                        };
                        _context.JobProgressTemplateNavItem.Add(JobProgressTemplateNavItem);
                    });
                }
                _context.JobProgressTemplateNotify.RemoveRange(versionData.JobProgressTemplateNotify);
                _context.SaveChanges();
                if (value.JobProgressTemplateNotifyModels != null)
                {
                    value.JobProgressTemplateNotifyModels.ForEach(h =>
                    {
                        var JobProgressTemplateNotify = new JobProgressTemplateNotify
                        {
                            JobProgressTemplateId = h.JobProgressTemplateId,
                            UserGroupId = h.UserGroupId,
                            UserType = h.UserType,
                            EmployeeId = h.EmployeeId,
                        };
                        _context.JobProgressTemplateNotify.Add(JobProgressTemplateNotify);

                    });
                }
                _context.SaveChanges();
            }
            return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(JobProgressTemplateModel value)
        {
            var versionData = _context.JobProgressTemplate.Include(n => n.JobProgressTemplateNavItem).Include(n => n.JobProgressTemplateNotify).Include(a => a.JobProgressTempletateLine).SingleOrDefault(p => p.JobProgressTemplateId == value.JobProgressTemplateId);

            if (versionData != null)
            {
                var verObject = _mapper.Map<JobProgressTemplateModel>(versionData);
                verObject.ItemIds = versionData.JobProgressTemplateNavItem != null ? versionData.JobProgressTemplateNavItem.Where(w => w.JobProgressTemplateId == versionData.JobProgressTemplateId).Select(a => a.ItemId).ToList() : new List<long?>();
                verObject.UserType = versionData.JobProgressTemplateNotify != null ? versionData.JobProgressTemplateNotify.Select(a => a.UserType).Distinct().FirstOrDefault() : "user";
                verObject.EmployeeIDs = new List<long?>();
                verObject.UserGroupIDs = new List<long?>();
                verObject.JobProgressTemplateLineModels = GetJobProgressTemplateLines(versionData.JobProgressTemplateId);
                if (versionData.JobProgressTemplateNotify != null)
                {
                    List<JobProgressTemplateNotifyModel> JobProgressTemplateNotifyModelss = new List<JobProgressTemplateNotifyModel>();
                    versionData.JobProgressTemplateNotify.ToList().ForEach(h =>
                    {
                        JobProgressTemplateNotifyModel JobProgressTemplateNotifyModel = new JobProgressTemplateNotifyModel();
                        JobProgressTemplateNotifyModel.JobProgressTemplateNotifyId = h.JobProgressTemplateNotifyId;
                        JobProgressTemplateNotifyModel.JobProgressTemplateId = h.JobProgressTemplateId;
                        JobProgressTemplateNotifyModel.UserGroupId = h.UserGroupId;
                        JobProgressTemplateNotifyModel.UserType = h.UserType;
                        JobProgressTemplateNotifyModel.EmployeeId = h.EmployeeId;
                        JobProgressTemplateNotifyModelss.Add(JobProgressTemplateNotifyModel);
                    });
                    verObject.JobProgressTemplateNotifyModels = JobProgressTemplateNotifyModelss;
                }
                var versionInfo = new TableDataVersionInfoModel<JobProgressTemplateModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "JobProgressTemplate",
                    PrimaryKey = value.JobProgressTemplateId,
                    ReferenceInfo = "Temp Version - undo",
                    StatusCodeID = value.StatusCodeID.Value,
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                var TempVersion = _context.TempVersion.Where(w => w.VersionId == tableData.VersionTableId).ToList();
                if (TempVersion != null)
                {
                    _context.TempVersion.RemoveRange(TempVersion);
                    _context.SaveChanges();
                }
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        #endregion
        #region JobProgressTemplateLineProcess
        [HttpGet]
        [Route("GetJobProgressTemplateLineProcess")]
        public List<JobProgressTemplateLineProcessModel> GetJobProgressTemplateLineProcess(long? id)
        {
            var jobProgressTempletateLineProcess = _context.JobProgressTemplateLineProcess
                                 .Include(c => c.PicNavigation)
                                 .Include(p => p.PiaNavigation)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                .AsNoTracking().Where(o => o.JobProgressTemplateLineProcessId == id).ToList();
            List<JobProgressTemplateLineProcessModel> jobProgressTemplateLineProcessModels = new List<JobProgressTemplateLineProcessModel>();
            jobProgressTempletateLineProcess.ForEach(s =>
            {
                JobProgressTemplateLineProcessModel jobProgressTemplateLineProcessModel = new JobProgressTemplateLineProcessModel
                {
                    JobProgressTemplateId = s.JobProgressTemplateId,
                    JobProgressTemplateLineProcessId = s.JobProgressTemplateLineProcessId,
                    ActionNo = s.ActionNo,
                    SectionNo = s.SectionNo,
                    SectionDescription = s.SectionDescription,
                    SubSectionNo = s.SubSectionNo,
                    SubSectionDescription = s.SubSectionDescription,
                    Pia = s.Pia,
                    Pic = s.Pic,
                    PersonInAction = s.PiaNavigation?.UserName,
                    PersonInCharge = s.PicNavigation?.UserName,
                    StartDate = s.StartDate,
                    CompletionDate = s.CompletionDate,
                    Message = s.Message,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                    SpecificDateOfEachMonth = s.SpecificDateOfEachMonth,
                    NoOfWorkingDays = s.NoOfWorkingDays,
                    JobDescription = s.JobDescription,
                    AssignedTo = s.AssignedTo,
                };
                jobProgressTemplateLineProcessModels.Add(jobProgressTemplateLineProcessModel);
            });

            return jobProgressTemplateLineProcessModels;
        }

        [HttpGet]
        [Route("GetJobProgressTemplateLineProcessByPIC")]
        public List<JobProgressTemplateLineProcessModel> GetJobProgressTemplateLineProcessByPIC(long? id)
        {
            var jobProgressTempletateLineProcess = _context.JobProgressTemplateLineProcess
                                 .Include(c => c.PicNavigation)
                                 .Include(p => p.PiaNavigation)
                                 .Include(p => p.JobProgressTemplate)
                                  .Include(p => p.JobProgressTemplate.Template)
                                  .Include(p => p.JobProgressTemplateLine)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode)
                                .AsNoTracking().Where(o => o.Pic == id).ToList();
            List<JobProgressTemplateLineProcessModel> jobProgressTemplateLineProcessModels = new List<JobProgressTemplateLineProcessModel>();
            jobProgressTempletateLineProcess.ForEach(s =>
            {
                JobProgressTemplateLineProcessModel jobProgressTemplateLineProcessModel = new JobProgressTemplateLineProcessModel
                {
                    JobProgressTemplateId = s.JobProgressTemplateId,
                    JobProgressTemplateLineProcessId = s.JobProgressTemplateLineProcessId,
                    TemplateName = s.JobProgressTemplate.Template.Value,
                    ActionNo = s.ActionNo,
                    SectionNo = s.SectionNo,
                    SectionDescription = s.SectionDescription,
                    SubSectionNo = s.SubSectionNo,
                    SubSectionDescription = s.SubSectionDescription,
                    Pia = s.Pia,
                    Pic = s.Pic,
                    PersonInAction = s.PiaNavigation?.UserName,
                    PersonInCharge = s.PicNavigation?.UserName,
                    StartDate = s.StartDate,
                    CompletionDate = s.CompletionDate,
                    ActualStartDate = s.ActualStartDate,
                    ActualCompletionDate = s.ActualCompletionDate,
                    Message = s.Message,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                    SpecificDateOfEachMonth = s.SpecificDateOfEachMonth,
                    NoOfWorkingDays = s.NoOfWorkingDays,
                    JobDescription = s.JobDescription,
                    AssignedTo = s.AssignedTo,
                };
                jobProgressTemplateLineProcessModels.Add(jobProgressTemplateLineProcessModel);
            });

            return jobProgressTemplateLineProcessModels;
        }
        [HttpPost]
        [Route("InsertJobProgressTemplateLineProcess")]
        public JobProgressTemplateLineProcessModel InsertJobProgressTemplateLineProcess(JobProgressTemplateLineProcessModel value)
        {
            var templateLineProcess = _context.JobProgressTemplateLineProcess.Where(s => s.JobProgressTemplateId == value.JobProgressTemplateId).ToList();
            var progressTemplate = _context.JobProgressTemplate.Include(j => j.JobProgressTempletateLine).FirstOrDefault(w => w.JobProgressTemplateId == value.JobProgressTemplateId);
            var today = DateTime.Now;
            var StartDate = new DateTime(today.Year, today.Month, 1);
            if (progressTemplate != null && progressTemplate.ProfileId != null)
            {
                value.ProfileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = progressTemplate.ProfileId, AddedByUserID = value.AddedByUserID });
            }
            var startDate = value.SpecificDateOfEachMonth != null ? StartDate.AddDays(value.SpecificDateOfEachMonth.Value - 1) : today;
            var firstLineItem = progressTemplate.JobProgressTempletateLine.OrderBy(j => j.JobProgressTemplateLineId).FirstOrDefault();
            var sessionId = Guid.NewGuid();

            if (progressTemplate.JobProgressTempletateLine.Any())
            {
                var jobProgressTemplateLineProcessItem = new JobProgressTemplateLineProcessStatus
                {
                    ProcessStatusId = 213,
                    SessionId = sessionId,
                    TemplateId = progressTemplate.JobProgressTemplateId,
                    TemplateLineId = firstLineItem.JobProgressTemplateLineId,
                };
                _context.JobProgressTemplateLineProcessStatus.Add(jobProgressTemplateLineProcessItem);
                progressTemplate.JobProgressTempletateLine.OrderBy(t => t.JobProgressTemplateLineId)
                    .Where(l => l.JobProgressTemplateLineId != jobProgressTemplateLineProcessItem.TemplateLineId).ToList()
                    .ForEach(t =>
                     {
                         _context.JobProgressTemplateLineProcessStatus.Add(new JobProgressTemplateLineProcessStatus
                         {
                             SessionId = sessionId,
                             TemplateId = t.JobProgressTemplateId,
                             TemplateLineId = t.JobProgressTemplateLineId,
                         });
                     });
            }

            var jobProgressTemplateLineProcess = new JobProgressTemplateLineProcess
            {
                JobProgressTemplateId = value.JobProgressTemplateId,
                JobProgressTemplateLineId = value.JobProgressTemplateLineId,
                ActionNo = value.ActionNo,
                SectionNo = value.SectionNo,
                SectionDescription = value.SectionDescription,
                SubSectionNo = value.SubSectionNo,
                SubSectionDescription = value.SubSectionDescription,
                Pia = templateLineProcess.Any() ? value.Pia : value.AddedByUserID,
                Pic = progressTemplate.Pic,
                StartDate = value.SpecificDateOfEachMonth > 0 ? StartDate.AddDays(value.SpecificDateOfEachMonth.Value - 1) : DateTime.Now,
                ActualStartDate = DateTime.Now,
                CompletionDate = startDate.AddDays(value.NoOfWorkingDays.Value),
                Message = value.Message,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SpecificDateOfEachMonth = value.SpecificDateOfEachMonth,
                NoOfWorkingDays = value.NoOfWorkingDays,
                JobDescription = value.JobDescription,
                ProfileNo = value.ProfileNo,
                AssignedTo = value.AssignedTo,
                SessionId = sessionId,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
            };
            _context.JobProgressTemplateLineProcess.Add(jobProgressTemplateLineProcess);
            _context.SaveChanges();
            value.JobProgressTemplateLineProcessId = jobProgressTemplateLineProcess.JobProgressTemplateLineProcessId;
            value.SessionId = jobProgressTemplateLineProcess.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateJobProgressTemplateLineProcess")]
        public JobProgressTemplateLineProcessModel UpdateJobProgressTemplateLineProcess(JobProgressTemplateLineProcessModel value)
        {
            JobProgressTempletateLine jobProgressTempletateLine;
            var template = _context.JobProgressTemplate.FirstOrDefault(w => w.JobProgressTemplateId == value.JobProgressTemplateId);
            if (value.StatusCodeID == 211)
            {
                var today = DateTime.Now;
                var StartDate = new DateTime(today.Year, today.Month, 1);
                var templateLines = _context.JobProgressTempletateLine.Where(j => j.JobProgressTemplateId == value.JobProgressTemplateId).OrderBy(j => j.JobProgressTemplateLineId);
                var currenttemplateLineItem = templateLines.FirstOrDefault(f => f.JobProgressTemplateLineId == value.JobProgressTemplateLineId);
                jobProgressTempletateLine = _context.JobProgressTempletateLine.Where(j => j.JobProgressTemplateId == value.JobProgressTemplateId).OrderBy(j => j.JobProgressTemplateLineId).FirstOrDefault(j => j.JobProgressTemplateLineId > value.JobProgressTemplateLineId);
                if (jobProgressTempletateLine != null)
                {
                    if (template != null && template.ProfileId != null)
                    {
                        value.ProfileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = template.ProfileId, AddedByUserID = value.AddedByUserID });
                    }
                    var startDate = jobProgressTempletateLine.SpecificDateOfEachMonth != null ? StartDate.AddDays(jobProgressTempletateLine.SpecificDateOfEachMonth.Value - 1) : today;
                    var jobPTemplateLineProcessStatus = _context.JobProgressTemplateLineProcessStatus.FirstOrDefault(c => c.TemplateId == jobProgressTempletateLine.JobProgressTemplateId && c.TemplateLineId == jobProgressTempletateLine.JobProgressTemplateLineId && c.SessionId == value.SessionId);
                    JobProgressTemplateLineProcess jobProgressTemplateLineProcessItem = new JobProgressTemplateLineProcess
                    {
                        JobProgressTemplateId = jobProgressTempletateLine.JobProgressTemplateId,
                        JobProgressTemplateLineId = jobProgressTempletateLine.JobProgressTemplateLineId,
                        ProfileNo = value.ProfileNo,
                        ActionNo = jobProgressTempletateLine.ActionNo,
                        SectionNo = jobProgressTempletateLine.SectionNo,
                        SectionDescription = jobProgressTempletateLine.SectionDescription,
                        SubSectionNo = jobProgressTempletateLine.SubSectionNo,
                        SubSectionDescription = jobProgressTempletateLine.SubSectionDescription,
                        Pia = currenttemplateLineItem.Pia,
                        Pic = template.Pic,
                        StatusCodeId = 213,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = value.AddedDate,
                        ActualStartDate = DateTime.Now,
                        StartDate = startDate,
                        CompletionDate = jobProgressTempletateLine.NoOfWorkingDays > 0 ? startDate.AddDays(jobProgressTempletateLine.NoOfWorkingDays.Value) : DateTime.Now,
                        Message = jobProgressTempletateLine.Message,
                        NoOfWorkingDays = jobProgressTempletateLine.NoOfWorkingDays,
                        SpecificDateOfEachMonth = jobProgressTempletateLine.SpecificDateOfEachMonth,
                        JobDescription = jobProgressTempletateLine.JobDescription,
                        SessionId = jobPTemplateLineProcessStatus.SessionId,
                    };
                    jobPTemplateLineProcessStatus.ProcessStatusId = 213;
                    _context.JobProgressTemplateLineProcess.Add(jobProgressTemplateLineProcessItem);
                }

            }
            var jobProgressTempletateLineProcess = _context.JobProgressTemplateLineProcess.FirstOrDefault(p => p.JobProgressTemplateLineProcessId == value.JobProgressTemplateLineProcessId);
            jobProgressTempletateLineProcess.JobProgressTemplateId = value.JobProgressTemplateId;
            jobProgressTempletateLineProcess.ActionNo = value.ActionNo;
            jobProgressTempletateLineProcess.SectionNo = value.SectionNo;
            jobProgressTempletateLineProcess.SectionDescription = value.SectionDescription;
            jobProgressTempletateLineProcess.SubSectionNo = value.SubSectionNo;
            jobProgressTempletateLineProcess.SubSectionDescription = value.SubSectionDescription;
            jobProgressTempletateLineProcess.StartDate = value.StartDate;
            jobProgressTempletateLineProcess.ActualStartDate = value.ActualStartDate;
            jobProgressTempletateLineProcess.ActualCompletionDate = DateTime.Now;
            jobProgressTempletateLineProcess.CompletionDate = value.CompletionDate;
            jobProgressTempletateLineProcess.Message = value.Message;
            jobProgressTempletateLineProcess.ModifiedByUserId = value.ModifiedByUserID;
            jobProgressTempletateLineProcess.ModifiedDate = DateTime.Now;
            jobProgressTempletateLineProcess.StatusCodeId = value.StatusCodeID.Value;
            jobProgressTempletateLineProcess.Pic = template.Pic;
            jobProgressTempletateLineProcess.SpecificDateOfEachMonth = value.SpecificDateOfEachMonth;
            jobProgressTempletateLineProcess.NoOfWorkingDays = value.NoOfWorkingDays;
            jobProgressTempletateLineProcess.JobDescription = value.JobDescription;
            jobProgressTempletateLineProcess.AssignedTo = value.AssignedTo;
            var jobProgressTemplateLineProcessStatus = _context.JobProgressTemplateLineProcessStatus.FirstOrDefault(c => c.TemplateId == jobProgressTempletateLineProcess.JobProgressTemplateId && c.TemplateLineId == jobProgressTempletateLineProcess.JobProgressTemplateLineId && c.SessionId == value.SessionId);
            jobProgressTemplateLineProcessStatus.ProcessStatusId = 211;
            _context.SaveChanges();
            value.SessionId = jobProgressTempletateLineProcess.SessionId;
            return value;
        }
        #endregion
        #region JobProgressTempletateLineLine
        private JobProgressTemplateLineActionModel GenerateSubSectionNo()
        {
            JobProgressTemplateLineActionModel JobProgressTemplateLineActionModel = new JobProgressTemplateLineActionModel();
            var jobProgressTempletateLineCount = _context.JobProgressTemplateLineLineProcess.Count();
            JobProgressTemplateLineActionModel.SubSectionNo = ToRoman(Convert.ToUInt32(jobProgressTempletateLineCount + 1));
            return JobProgressTemplateLineActionModel;
        }
        [HttpGet]
        [Route("GetJobProgressTemplateLineLine")]
        public List<JobProgressTemplateLineLineProcessModel> GetJobProgressTemplateLineLine(long? id)
        {
            var jobProgressTempletateLineProcess = _context.JobProgressTemplateLineLineProcess
                .Include(a => a.JobProgressTemplateLineProcessPia)
                                 .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(s => s.MonitorTypeOfJobAction)
                                .AsNoTracking().Where(o => o.JobProgressTemplateLineProcessId == id).ToList();
            List<JobProgressTemplateLineLineProcessModel> jobProgressTemplateLineProcessModels = new List<JobProgressTemplateLineLineProcessModel>();
            if (jobProgressTempletateLineProcess != null && jobProgressTempletateLineProcess.Count > 0)
            {
                var jobProgressTempletateLineProcessIds = jobProgressTempletateLineProcess.Select(s => s.JobProgressTemplateLineLineProcessId).ToList();
                var appWikiTaskLink = _context.JobProgressTemplateLineLineTaskLink.Select(n => new JobProgressTemplateLineLineTaskLinkModel { JobProgressTemplateLineLineId = n.JobProgressTemplateLineLineId, JobProgressTemplateLineLineTaskLinkId = n.JobProgressTemplateLineLineTaskLinkId, IsLatest = n.IsLatest, IsWiki = n.IsWiki, TaskLink = n.TaskLink, Subject = n.Subject, IsEachSubjectHeadingUpload = n.IsEachSubjectHeadingUpload, IsRequireUploadFolder = n.IsRequireUploadFolder, IsWithCompleteCheck = n.IsWithCompleteCheck }).Where(w => jobProgressTempletateLineProcessIds.Contains(w.JobProgressTemplateLineLineId.Value)).ToList();

                jobProgressTempletateLineProcess.ForEach(s =>
                {
                    JobProgressTemplateLineLineProcessModel jobProgressTemplateLineProcessModel = new JobProgressTemplateLineLineProcessModel
                    {
                        JobProgressTemplateLineLineProcessId = s.JobProgressTemplateLineLineProcessId,
                        JobProgressTemplateLineProcessId = s.JobProgressTemplateLineProcessId,
                        SubSectionNo = s.SubSectionNo,
                        SubSectionDescription = s.SubSectionDescription,
                        Message = s.Message,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser?.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode?.CodeValue,
                        NoOfWorkingDays = s.NoOfWorkingDays,
                        MonitorTypeOfJobActionId = s.MonitorTypeOfJobActionId,
                        MonitorTypeOfJobAction = s.MonitorTypeOfJobAction?.Value,
                        PiaIds = s.JobProgressTemplateLineProcessPia != null ? s.JobProgressTemplateLineProcessPia.Select(a => a.Pia).ToList() : new List<long?>(),
                        UserGroupId = s.UserGroupId,
                        UserType = s.UserType,
                        PlantId = s.PlantId,
                        AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.JobProgressTemplateLineLineId == s.JobProgressTemplateLineLineProcessId).ToList(),
                    };
                    jobProgressTemplateLineProcessModels.Add(jobProgressTemplateLineProcessModel);
                });
            }
            return jobProgressTemplateLineProcessModels;
        }
        [HttpPost]
        [Route("InsertJobProgressTemplateLineLine")]
        public JobProgressTemplateLineLineProcessModel InsertJobProgressTemplateLineLine(JobProgressTemplateLineLineProcessModel value)
        {
            //var sessionId = Guid.NewGuid();
            var jobProgressTempletateLine = new JobProgressTemplateLineLineProcess
            {
                JobProgressTemplateLineProcessId = value.JobProgressTemplateLineProcessId,
                SubSectionNo = GenerateSubSectionNo().SubSectionNo,
                SubSectionDescription = value.SubSectionDescription,
                Message = value.Message,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //SessionId = sessionId,
                NoOfWorkingDays = value.NoOfWorkingDays,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                MonitorTypeOfJobActionId = value.MonitorTypeOfJobActionId,
                UserType = value.UserType,
                UserGroupId = value.UserGroupId,
                PlantId = value.PlantId,
            };
            if (value.PiaIds != null && value.PiaIds.Count > 0)
            {
                value.PiaIds.ForEach(h =>
                {
                    JobProgressTemplateLineProcessPia jobProgressTemplateLineProcessPia = new JobProgressTemplateLineProcessPia
                    {
                        Pia = h,
                    };
                    jobProgressTempletateLine.JobProgressTemplateLineProcessPia.Add(jobProgressTemplateLineProcessPia);
                });
            }
            if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
            {
                value.AppWikiTaskLinkModel.ForEach(h =>
                {
                    JobProgressTemplateLineLineTaskLink appWikiTaskLink = new JobProgressTemplateLineLineTaskLink
                    {
                        TaskLink = h.TaskLink,
                        Subject = h.Subject,
                        IsLatest = h.IsLatest,
                        IsWiki = h.IsWiki,
                        IsRequireUploadFolder = h.IsRequireUploadFolder,
                        IsWithCompleteCheck = h.IsWithCompleteCheck,
                        IsEachSubjectHeadingUpload = h.IsEachSubjectHeadingUpload,
                    };
                    jobProgressTempletateLine.JobProgressTemplateLineLineTaskLink.Add(appWikiTaskLink);
                });
            }
            _context.JobProgressTemplateLineLineProcess.Add(jobProgressTempletateLine);
            _context.SaveChanges();
            value.JobProgressTemplateLineLineProcessId = jobProgressTempletateLine.JobProgressTemplateLineLineProcessId;
            value.SubSectionNo = jobProgressTempletateLine.SubSectionNo;
            return value;
        }
        [Route("UpdateJobProgressTemplateLineLine")]
        public JobProgressTemplateLineLineProcessModel UpdateJobProgressTemplateLineLine(JobProgressTemplateLineLineProcessModel value)
        {
            var jobProgressTempletateLine = _context.JobProgressTemplateLineLineProcess.SingleOrDefault(p => p.JobProgressTemplateLineLineProcessId == value.JobProgressTemplateLineLineProcessId);
            jobProgressTempletateLine.JobProgressTemplateLineProcessId = value.JobProgressTemplateLineProcessId;
            jobProgressTempletateLine.SubSectionNo = value.SubSectionNo;
            jobProgressTempletateLine.SubSectionDescription = value.SubSectionDescription;
            jobProgressTempletateLine.Message = value.Message;
            jobProgressTempletateLine.StatusCodeId = value.StatusCodeID.Value;
            jobProgressTempletateLine.NoOfWorkingDays = value.NoOfWorkingDays;
            jobProgressTempletateLine.ModifiedByUserId = value.AddedByUserID;
            jobProgressTempletateLine.ModifiedDate = DateTime.Now;
            jobProgressTempletateLine.MonitorTypeOfJobActionId = value.MonitorTypeOfJobActionId;
            jobProgressTempletateLine.UserType = value.UserType;
            jobProgressTempletateLine.UserGroupId = value.UserGroupId;
            jobProgressTempletateLine.PlantId = value.PlantId;
            _context.SaveChanges();
            var appWikiTaskLinkRemove = _context.JobProgressTemplateLineLineTaskLink.Where(w => w.JobProgressTemplateLineLineId == value.JobProgressTemplateLineLineProcessId).ToList();
            if (appWikiTaskLinkRemove.Count > 0)
            {
                _context.JobProgressTemplateLineLineTaskLink.RemoveRange(appWikiTaskLinkRemove);
                _context.SaveChanges();
            }
            var jobProgressTemplateLineProcessPia = _context.JobProgressTemplateLineProcessPia.Where(w => w.JobProgressTemplateLineLineProcessId == value.JobProgressTemplateLineLineProcessId).ToList();
            if (jobProgressTemplateLineProcessPia.Count > 0)
            {
                _context.JobProgressTemplateLineProcessPia.RemoveRange(jobProgressTemplateLineProcessPia);
                _context.SaveChanges();
            }
            if (value.PiaIds != null && value.PiaIds.Count > 0)
            {
                value.PiaIds.ForEach(h =>
                {
                    JobProgressTemplateLineProcessPia jobProgressTemplateLineProcessPia = new JobProgressTemplateLineProcessPia
                    {
                        Pia = h,
                        JobProgressTemplateLineLineProcessId = jobProgressTempletateLine.JobProgressTemplateLineLineProcessId,
                    };
                    _context.JobProgressTemplateLineProcessPia.Add(jobProgressTemplateLineProcessPia);
                });
                _context.SaveChanges();
            }
            if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
            {
                value.AppWikiTaskLinkModel.ForEach(h =>
                {
                    JobProgressTemplateLineLineTaskLink appWikiTaskLink = new JobProgressTemplateLineLineTaskLink
                    {
                        JobProgressTemplateLineLineId = jobProgressTempletateLine.JobProgressTemplateLineLineProcessId,
                        TaskLink = h.TaskLink,
                        Subject = h.Subject,
                        IsLatest = h.IsLatest,
                        IsWiki = h.IsWiki,
                        IsRequireUploadFolder = h.IsRequireUploadFolder,
                        IsWithCompleteCheck = h.IsWithCompleteCheck,
                        IsEachSubjectHeadingUpload = h.IsEachSubjectHeadingUpload,
                    };
                    _context.JobProgressTemplateLineLineTaskLink.Add(appWikiTaskLink);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteJobProgressTemplateLineLine")]
        public ActionResult<string> DeleteJobProgressTemplateLineLine(int id)
        {
            try
            {
                var jobProgressTempletateLine = _context.JobProgressTemplateLineLineProcess.SingleOrDefault(p => p.JobProgressTemplateLineLineProcessId == id);
                if (jobProgressTempletateLine != null)
                {
                    var appWikiTaskLinkRemove = _context.JobProgressTemplateLineLineTaskLink.Where(w => w.JobProgressTemplateLineLineId == jobProgressTempletateLine.JobProgressTemplateLineLineProcessId).ToList();
                    if (appWikiTaskLinkRemove.Count > 0)
                    {
                        _context.JobProgressTemplateLineLineTaskLink.RemoveRange(appWikiTaskLinkRemove);
                        _context.SaveChanges();
                    }
                    _context.JobProgressTemplateLineLineProcess.Remove(jobProgressTempletateLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot be delete!.Reference to others");
            }
        }
        #endregion
        #region IncrementAlphabets
        [HttpGet]
        [Route("IncrementAlphabets")]
        public string IncrementAlphabets(string input)
        {
            return IncrementAlphabet(input);
        }
        private static string IncrementAlphabet(string input)
        {
            List<char> chars = input.ToUpper().ToList();
            for (int i = chars.Count - 1; i >= 0; i--)
            {
                if (chars[i] < 'A' || chars[i] > 'Z')
                {
                    throw new ArgumentException("Input must contain only A-Z", nameof(input));
                }
                chars[i]++;
                if (chars[i] > 'Z')
                {
                    chars[i] = 'A';
                    if (i == 0)
                    {
                        chars.Add('A');
                    }
                }
                else
                {
                    break;
                }
            }
            return string.Concat(chars);
        }
        #endregion
        #region RomanLetters
        [HttpGet]
        [Route("IncrementRomanLetters")]
        public string IncrementRomanLetters(uint input)
        {
            return ToRoman(input);
        }
        private static Dictionary<uint, string> RomanNumbers =
            new Dictionary<uint, string>
            {
                { 1000000, "M̅" },
                { 900000, "C̅M̅" },

                { 500000, "D̅" },
                { 400000, "C̅D̅" },

                { 100000, "C̅" },
                { 90000, "X̅C̅" },

                { 50000, "L̅" },
                { 40000, "X̅L̅" },

                { 10000, "X̅" },
                { 9000, "I̅X̅" },

                { 5000, "V̅" },
                { 4000, "I̅V̅" },

                { 1000, "M" },
                { 900, "DM" },

                { 500, "D" },
                { 400, "CD" },

                { 100, "C" },
                { 90, "XC" },

                { 50, "L" },
                { 40, "XL" },

                { 10, "X" },
                { 9, "IX" },

                { 5, "V" },
                { 4, "IV" },

                { 1, "I" },
            };
        private static string ToRoman(uint number)
        {
            var romanNum = string.Empty;

            while (number > 0)
            {
                var item = RomanNumbers
                    .OrderByDescending(x => x.Key)
                    .First(x => x.Key <= number);
                romanNum += item.Value;
                number -= item.Key;
            }

            return romanNum;
        }
        #endregion
        [HttpGet]
        [Route("GetProcessMonitoringSequence")]
        public List<JobProgressTemplateLineLineProcessModel> GetProcessMonitoringSequence(long?id)
        {
            var jbProgressTemplateLineLineProcess = _context.JobProgressTemplateLineLineProcess
                                .Include(r => r.JobProgressTemplateLineProcess)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Where(w => w.JobProgressTemplateLineProcess.JobProgressTemplateId == id).ToList();
                               
            List<JobProgressTemplateLineLineProcessModel> jobProgressTemplateLineLineProcessModels = new List<JobProgressTemplateLineLineProcessModel>();
            if (jbProgressTemplateLineLineProcess != null)
            {
                jbProgressTemplateLineLineProcess.ToList().ForEach(s =>
                {
                    JobProgressTemplateLineLineProcessModel templateTestCaseModel = new JobProgressTemplateLineLineProcessModel();
                    templateTestCaseModel.JobProgressTemplateLineLineProcessId = s.JobProgressTemplateLineLineProcessId;
                    templateTestCaseModel.JobProgressTemplateLineProcessId = s.JobProgressTemplateLineProcessId;
                    templateTestCaseModel.SubSectionNo = s.SubSectionNo;
                    templateTestCaseModel.SubSectionDescription = s.SubSectionDescription;
                    templateTestCaseModel.NoOfWorkingDays = s.NoOfWorkingDays;
                    templateTestCaseModel.SectionDescription = s.JobProgressTemplateLineProcess?.SectionDescription;
                    templateTestCaseModel.SectionNo = s.JobProgressTemplateLineProcess?.SectionNo;
                    templateTestCaseModel.ModifiedByUserID = s.AddedByUserId;
                    templateTestCaseModel.AddedByUserID = s.ModifiedByUserId;
                    templateTestCaseModel.StatusCodeID = s.StatusCodeId;
                    templateTestCaseModel.AddedByUser = s.AddedByUser.UserName;
                    templateTestCaseModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    templateTestCaseModel.AddedDate = s.AddedDate;
                    templateTestCaseModel.ModifiedDate = s.ModifiedDate;
                    templateTestCaseModel.StatusCode = s.StatusCode.CodeValue;
                    jobProgressTemplateLineLineProcessModels.Add(templateTestCaseModel);
                });
            }
            return jobProgressTemplateLineLineProcessModels.OrderBy(o=>o.JobProgressTemplateLineLineProcessId).ToList();
        }
    }
}

