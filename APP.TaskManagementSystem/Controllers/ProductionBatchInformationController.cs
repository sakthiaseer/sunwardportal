﻿using System;
using System.Collections.Generic;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionBatchInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionBatchInformationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        [HttpGet]
        [Route("GetProductionBatchSize")]
        public List<ProductionBatchSizeModel> GetNavProductCode()
        {
            List<ProductionBatchSizeModel> productionBatchSizeModels = new List<ProductionBatchSizeModel>();
            var productionBatchSizeModellist = _context.ProductionBatchSize.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ProductionBatchSizeId).AsNoTracking().ToList();
            if (productionBatchSizeModellist != null && productionBatchSizeModellist.Count > 0)
            {
                productionBatchSizeModellist.ForEach(s =>
                {
                    ProductionBatchSizeModel productionBatchSizeModel = new ProductionBatchSizeModel
                    {
                        ProductionBatchSizeId = s.ProductionBatchSizeId,
                        ProductionBatchSizeName = s.ProductionBatchSizeName,
                        ProductionBatchSizeDescription = s.ProductionBatchSizeDescription
                    };
                    productionBatchSizeModels.Add(productionBatchSizeModel);
                });

            }

            return productionBatchSizeModels;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionBatchInformations")]
        public List<ProductionBatchInformationModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ProductionBatchInformationModel> productionBatchInformationModels = new List<ProductionBatchInformationModel>();
            var productionBatchInformation = _context.ProductionBatchInformation.Include("Plant").Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ProductionBatchInformationId).AsNoTracking().ToList();
            var batchInformationIds = productionBatchInformation.Select(b => b.ProductionBatchInformationId).ToList();
            var productionBatchInformationLines = _context.ProductionBatchInformationLine.Include(b => b.Item).AsNoTracking().ToList();
            if (productionBatchInformation != null && productionBatchInformation.Count > 0)
            {
                productionBatchInformation.ForEach(s =>
                {
                    ProductionBatchInformationModel productionBatchInformationModel = new ProductionBatchInformationModel
                    {
                        ProductionBatchInformationID = s.ProductionBatchInformationId,
                        PlantID = s.PlantId,
                        CompanyName = s.Plant != null ? s.Plant.PlantCode : "",
                        TicketNo = s.TicketNo,
                        BatchNo = productionBatchInformationLines?.FirstOrDefault(b => b.ProductionBatchInformationId == s.ProductionBatchInformationId)?.BatchNo,
                        ItemNo = productionBatchInformationLines?.FirstOrDefault(b => b.ProductionBatchInformationId == s.ProductionBatchInformationId)?.Item?.No,
                        ManufacturingStartDate = s.ManufacturingStartDate,
                        BatchSizeId = s.BatchSizeId,
                        BatchSize = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BatchSizeId).Select(a => a.Value).SingleOrDefault() : "",
                        ProductionOrderNo = s.ProductionOrderNo,
                        UnitsofBatchSize = s.UnitsofBatchSize,
                        PackingUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.UnitsofBatchSize).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionBatchInformationModels.Add(productionBatchInformationModel);
                });
            }

            return productionBatchInformationModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionBatchInformationModel> GetData(SearchModel searchModel)
        {
            var productionBatchInformation = new ProductionBatchInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).LastOrDefault();
                        break;
                    case "Next":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).LastOrDefault();
                        break;
                    case "Next":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderBy(o => o.ProductionBatchInformationId).FirstOrDefault(s => s.ProductionBatchInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        productionBatchInformation = _context.ProductionBatchInformation.OrderByDescending(o => o.ProductionBatchInformationId).FirstOrDefault(s => s.ProductionBatchInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionBatchInformationModel>(productionBatchInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionBatchInformation")]
        public ProductionBatchInformationModel Post(ProductionBatchInformationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionBatchInformation = new ProductionBatchInformation
            {
                ManufacturingStartDate = value.ManufacturingStartDate,
                PlantId = value.PlantID,
                TicketNo = value.TicketNo,
                BatchSizeId = value.BatchSizeId,
                ProductionOrderNo = value.ProductionOrderNo,
                UnitsofBatchSize = value.UnitsofBatchSize,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,

            };
            _context.ProductionBatchInformation.Add(productionBatchInformation);
            _context.SaveChanges();
            value.ProductionBatchInformationID = productionBatchInformation.ProductionBatchInformationId;
            value.BatchSize = value.BatchSizeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.BatchSizeId).Select(m => m.Value).FirstOrDefault() : "";
            value.PackingUnits = value.UnitsofBatchSize != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.UnitsofBatchSize).Select(m => m.Value).FirstOrDefault() : "";

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionBatchInformation")]
        public ProductionBatchInformationModel Put(ProductionBatchInformationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionBatchInformation = _context.ProductionBatchInformation.SingleOrDefault(p => p.ProductionBatchInformationId == value.ProductionBatchInformationID);
            productionBatchInformation.TicketNo = value.TicketNo;
            productionBatchInformation.BatchSizeId = value.BatchSizeId;
            productionBatchInformation.ProductionOrderNo = value.ProductionOrderNo;
            productionBatchInformation.PlantId = value.PlantID;
            productionBatchInformation.ManufacturingStartDate = value.ManufacturingStartDate;
            productionBatchInformation.UnitsofBatchSize = value.UnitsofBatchSize;
            productionBatchInformation.ModifiedByUserId = value.ModifiedByUserID;
            productionBatchInformation.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.BatchSize = value.BatchSizeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.BatchSizeId).Select(m => m.Value).FirstOrDefault() : "";
            value.PackingUnits = value.UnitsofBatchSize != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.UnitsofBatchSize).Select(m => m.Value).FirstOrDefault() : "";

            return value;

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionBatchInformation")]
        public void Delete(int id)
        {
            var ProductionBatchInformation = _context.ProductionBatchInformation.SingleOrDefault(p => p.ProductionBatchInformationId == id);
            if (ProductionBatchInformation != null)
            {
                var ProductionBatchInformationline = _context.ProductionBatchInformationLine.Where(p => p.ProductionBatchInformationId == id).AsNoTracking().ToList();
                if (ProductionBatchInformationline.Count > 0)
                {
                    _context.ProductionBatchInformationLine.RemoveRange(ProductionBatchInformationline);
                    _context.SaveChanges();
                }
                _context.ProductionBatchInformation.Remove(ProductionBatchInformation);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("ProductionBatchInformationSyncBMR")]
        public void ProductionBatchInformationSyncBMR()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var ProductionBatchInformation = _context.ProductionBatchInformation.Include(p => p.ProductionBatchInformationLine).Include("ProductionBatchInformationLine.Item").Where(w => w.IsBmr != true).ToList();
            var itemclassificationmaster = _context.ItemClassificationMaster.Where(w => w.ClassificationTypeId == 1503 && w.IsForm == true && w.ParentId != null).FirstOrDefault();
            if (itemclassificationmaster != null)
            {
                var StandardId = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => w.ApplicationMaster.ApplicationMasterCodeId == 275 && w.Value.ToLower() == "standard").Select(a => a.ApplicationMasterDetailId).FirstOrDefault();
                if (ProductionBatchInformation != null)
                {
                    ProductionBatchInformation.ForEach(s =>
                    {
                        var classificationBmr = new ClassificationBmr
                        {
                            CompanyId = s.PlantId,
                            BatchSize = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BatchSizeId).Select(a => a.Value).SingleOrDefault() : "",
                            TypeOfProductionId = StandardId,
                            ProfileLinkReferenceNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = itemclassificationmaster.ProfileId, AddedByUserID = s.AddedByUserId, StatusCodeID = 710, Title = s.ProductionOrderNo }),
                            StatusCodeId = 1,
                            AddedByUserId = s.AddedByUserId.Value,
                            AddedDate = DateTime.Now,
                            ItemClassificationMasterId = itemclassificationmaster.ItemClassificationId,
                            ProfileId = itemclassificationmaster.ProfileId,
                            ProdOrderNo = s.TicketNo,
                            SessionId = Guid.NewGuid(),
                            ProductionStatusId = 1821,
                            ProductionSimulationDate = s.ManufacturingStartDate,
                            IsProductionBatch = true,
                        };
                        _context.ClassificationBmr.Add(classificationBmr);
                        _context.SaveChanges();
                        if (s.ProductionBatchInformationLine != null)
                        {
                            s.ProductionBatchInformationLine.ToList().ForEach(h =>
                            {
                                var classificationBmrLine = new ClassificationBmrLine
                                {
                                    ClassificationBmrId = classificationBmr.ClassificationBmrId,
                                    BatchNo = h.BatchNo,
                                    SupplyTo = h.Item != null ? h.Item.InternalRef : "",
                                    ProductNo = h.Item != null ? h.Item.No : "",
                                    ProductName = h.Item != null ? h.Item.Description : "",
                                    OutputQty = h.QtyPack,
                                    ExpiryDate = h.ExpiryDate,
                                    StatusCodeId = h.StatusCodeId,
                                    AddedByUserId = h.AddedByUserId.Value,
                                    AddedDate = h.AddedDate,
                                    ProductUom = h.Item != null ? h.Item.BaseUnitofMeasure : "",
                                };
                                _context.ClassificationBmrLine.Add(classificationBmrLine);

                            });
                        }
                        var ProductionBatchInformations = _context.ProductionBatchInformation.SingleOrDefault(a => a.ProductionBatchInformationId == s.ProductionBatchInformationId);
                        ProductionBatchInformations.IsBmr = true;
                        _context.SaveChanges();
                    });

                }
            }

        }
    }
}