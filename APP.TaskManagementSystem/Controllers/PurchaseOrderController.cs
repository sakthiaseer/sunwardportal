﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PurchaseOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;


        public PurchaseOrderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetPurchaseOrder")]
        public List<PurchaseOrderModel> Get()
        {
            var purchaseOrder = _context.PurchaseOrder.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Include(c => c.VendorName).Include(c => c.FromCompany).AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<PurchaseOrderModel> purchaseOrderModel = new List<PurchaseOrderModel>();
            purchaseOrder.ForEach(s =>
            {
                PurchaseOrderModel purchaseOrderModels = new PurchaseOrderModel
                {
                    PurchaseOrderId = s.PurchaseOrderId,
                    TypeId = s.TypeId,
                    DateOfPo = s.DateOfPo,
                    TypeOfPo = applicationMasterDetails != null && applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TypeId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TypeId).Value : "",
                    FromCompanyId = s.FromCompanyId,
                    FromCompany = s.FromCompany != null ? s.FromCompany.Description : "",
                    VendorNameId = s.VendorNameId,
                    VendorName = s.VendorName != null ? s.VendorName.CompanyName : "",
                    VersionNo = s.VersionNo,
                    Ponumber = s.Ponumber,
                    SessionId = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                purchaseOrderModel.Add(purchaseOrderModels);
            });
            return purchaseOrderModel.OrderByDescending(a => a.PurchaseOrderId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PurchaseOrderModel> GetData(SearchModel searchModel)
        {
            var purchaseOrder = new PurchaseOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrder = _context.PurchaseOrder.OrderBy(o => o.PurchaseOrderId).FirstOrDefault(s => s.PurchaseOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        purchaseOrder = _context.PurchaseOrder.OrderByDescending(o => o.PurchaseOrderId).FirstOrDefault(s => s.PurchaseOrderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PurchaseOrderModel>(purchaseOrder);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPurchaseOrder")]
        public PurchaseOrderModel Post(PurchaseOrderModel value)
        {
            var SessionId = Guid.NewGuid();
            var purchaseOrder = new PurchaseOrder
            {

                TypeId = value.TypeId,
                FromCompanyId = value.FromCompanyId,
                VendorNameId = value.VendorNameId,
                DateOfPo = value.DateOfPo,
                VersionNo = value.VersionNo,
                Ponumber = value.Ponumber,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.PurchaseOrder.Add(purchaseOrder);
            _context.SaveChanges();
            value.PurchaseOrderId = purchaseOrder.PurchaseOrderId;
            value.SessionId = purchaseOrder.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePurchaseOrder")]
        public PurchaseOrderModel Put(PurchaseOrderModel value)
        {
            var SessionId = Guid.NewGuid();


            var purchaseOrder = _context.PurchaseOrder.SingleOrDefault(p => p.PurchaseOrderId == value.PurchaseOrderId);
            purchaseOrder.TypeId = value.TypeId;
            purchaseOrder.FromCompanyId = value.FromCompanyId;
            purchaseOrder.VendorNameId = value.VendorNameId;
            purchaseOrder.DateOfPo = value.DateOfPo;
            purchaseOrder.VersionNo = value.VersionNo;
            purchaseOrder.Ponumber = value.Ponumber;
            if (value.SessionId != null)
            {
                purchaseOrder.SessionId = value.SessionId;


            }
            else
            {
                purchaseOrder.SessionId = SessionId;

            }
            purchaseOrder.ModifiedByUserId = value.ModifiedByUserID;
            purchaseOrder.ModifiedDate = DateTime.Now;
            purchaseOrder.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePurchaseOrder")]
        public void Delete(int id)
        {
            var purchaseOrder = _context.PurchaseOrder.SingleOrDefault(p => p.PurchaseOrderId == id);
            if (purchaseOrder != null)
            {
                var purchaseOrderLine = _context.PurchaseOrderLine.Where(t => t.PurchaseOrderId == purchaseOrder.PurchaseOrderId).AsNoTracking().ToList();
                if (purchaseOrderLine != null)
                {
                    _context.PurchaseOrderLine.RemoveRange(purchaseOrderLine);
                    _context.SaveChanges();
                }

                var purchaseOrder1 = _context.PurchaseOrder.Where(p => p.PurchaseOrderId == id).Select(s => s.SessionId).SingleOrDefault();

                var purchaseOrderDocument = _context.PurchaseOrderDocument.Select(p=>new { p.PurchaseOrderDocumentId, p.SessionId}).Where(p => p.SessionId == purchaseOrder1).AsNoTracking().ToList();
                if (purchaseOrderDocument != null)
                {
                    var query = string.Format("delete from PurchaseOrderDocument Where PurchaseOrderDocumentId in" + '(' + "{0}" + ')', string.Join(',', purchaseOrderDocument.Select(s => s.PurchaseOrderDocumentId)));
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to update document");
                    }
                }
                _context.PurchaseOrder.Remove(purchaseOrder);
                _context.SaveChanges();
            }
        }



        [HttpPost]
        [Route("UploadPurchaseOrderDocuments")]
        public IActionResult UploadPurchaseOrderDocuments(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new PurchaseOrderDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.PurchaseOrderDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetPurchaseOrderDocument")]
        public List<PurchaseOrderDocumentModel> GetPurchaseOrderDocument(int? id)
        {
            var purchaseOrder = _context.PurchaseOrder.Where(w => w.PurchaseOrderId == id).Select(s => s.SessionId).FirstOrDefault();
            var query = _context.PurchaseOrderDocument.Select(s => new PurchaseOrderDocumentModel
            {
                SessionId = s.SessionId,
                PurchaseOrderDocumentId = s.PurchaseOrderDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == purchaseOrder).OrderByDescending(o => o.PurchaseOrderDocumentId).AsNoTracking().ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadPurchaseOrderDocument")]
        public IActionResult DownLoadPurchaseOrderDocument(long id)
        {
            var document = _context.PurchaseOrderDocument.SingleOrDefault(t => t.PurchaseOrderDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeletePurchaseOrderDocument")]
        public void DeletePurchaseOrderDocument(int id)
        {
            var query = string.Format("delete from PurchaseOrderDocument Where PurchaseOrderDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }





        [HttpPost]
        [Route("UploadPurchaseOrderAttachments")]
        public IActionResult UploadPurchaseOrderAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID=710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new PurchaseOrderDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId
                };

                _context.PurchaseOrderDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetPurchaseOrderAttachmentBySessionID")]
        public List<DocumentsModel> GetPurchaseOrderAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.PurchaseOrderDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.PurchaseOrderDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadPurchaseOrderAttachment")]
        public IActionResult DownLoadPurchaseOrderAttachment(long id)
        {
            var document = _context.PurchaseOrderDocument.SingleOrDefault(t => t.PurchaseOrderDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeletePurchaseOrderAttachment")]
        public void DeletePurchaseOrderAttachment(int id)
        {
            var query = _context.PurchaseOrderDocument.SingleOrDefault(p => p.PurchaseOrderDocumentId == id);
            if (query != null)
            {
                _context.PurchaseOrderDocument.Remove(query);
                _context.SaveChanges();
            }
        }


    }
}