﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterBlanketOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public MasterBlanketOrderController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }
        [HttpGet]
        [Route("GetMasterBlanketOrder")]
        public async Task<IEnumerable<TableDataVersionInfoModel<MasterBlanketOrderModel>>> GetMasterBlanketOrders(string sessionID)
        {
            return await _repository.GetList<MasterBlanketOrderModel>(sessionID);
        }
        [HttpGet]
        [Route("GetMasterBlanketOrders")]
        public List<MasterBlanketOrderModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var MasterBlanketOrders = _context.MasterBlanketOrder
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(l => l.Customer)
                .AsNoTracking()
                .ToList();
            List<MasterBlanketOrderModel> MasterBlanketOrderModels = new List<MasterBlanketOrderModel>();
            MasterBlanketOrders.ForEach(s =>
            {
                MasterBlanketOrderModel MasterBlanketOrderModel = new MasterBlanketOrderModel
                {
                    MasterBlanketOrderId = s.MasterBlanketOrderId,
                    CompanyID = s.CompanyId,
                    CustomerID = s.CustomerId,
                    CompanyName = s.Company?.PlantCode,
                    FromPeriod = s.FromPeriod,
                    ToPeriod = s.ToPeriod,
                    IsRequireVersionInformation = s.IsRequireVersionInformation,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    CustomerName = s.Customer?.CompanyName,
                    VersionSessionId = s.VersionSessionId

                };
                MasterBlanketOrderModels.Add(MasterBlanketOrderModel);
            });
            return MasterBlanketOrderModels.OrderByDescending(a => a.MasterBlanketOrderId).ToList();
        }

        [HttpPost()]
        [Route("GetMasterBlanketOrderData")]
        public ActionResult<MasterBlanketOrderModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var MasterBlanketOrder = new MasterBlanketOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderBy(o => o.MasterBlanketOrderId).FirstOrDefault(s => s.MasterBlanketOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        MasterBlanketOrder = _context.MasterBlanketOrder.OrderByDescending(o => o.MasterBlanketOrderId).FirstOrDefault(s => s.MasterBlanketOrderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MasterBlanketOrderModel>(MasterBlanketOrder);
            List<MasterBlanketOrderLineModel> masterBlanketOrderLineModels = new List<MasterBlanketOrderLineModel>();
            if (result != null)
            {
                if (result.MasterBlanketOrderId > 0)
                {
                    var masterBlanketOrderLine = _context.MasterBlanketOrderLine.Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode).Where(s => s.MasterBlanketOrderId == result.MasterBlanketOrderId).ToList();
                    if (masterBlanketOrderLine != null && masterBlanketOrderLine.Count > 0)
                    {
                        masterBlanketOrderLine.ForEach(s =>
                        {
                            MasterBlanketOrderLineModel masterBlanketOrderLineModel = new MasterBlanketOrderLineModel();

                            masterBlanketOrderLineModel.MasterBlanketOrderId = s.MasterBlanketOrderId;
                            masterBlanketOrderLineModel.MasterBlanketOrderLineID = s.MasterBlanketOrderLineId;
                            masterBlanketOrderLineModel.NoOfWeeksPerMonth = s.NoOfWeeksPerMonth;
                            masterBlanketOrderLineModel.WeekOneStartDate = s.WeekOneStartDate;
                            masterBlanketOrderLineModel.WeekOneLargestOrder = s.WeekOneLargestOrder;
                            masterBlanketOrderLineModel.WeekTwoStartDate = s.WeekTwoStartDate;
                            masterBlanketOrderLineModel.WeekTwoLargestOrder = s.WeekTwoLargestOrder;
                            masterBlanketOrderLineModel.WeekThreeStartDate = s.WeekThreeStartDate;
                            masterBlanketOrderLineModel.WeekThreeLargestOrder = s.WeekThreeLargestOrder;
                            masterBlanketOrderLineModel.WeekFourStartDate = s.WeekFourStartDate;
                            masterBlanketOrderLineModel.WeekFourLargestOrder = s.WeekFourLargestOrder;
                            masterBlanketOrderLineModel.StatusCodeID = s.StatusCodeId;
                            masterBlanketOrderLineModel.AddedByUserID = s.AddedByUserId;
                            masterBlanketOrderLineModel.ModifiedByUserID = s.ModifiedByUserId;
                            masterBlanketOrderLineModel.AddedDate = s.AddedDate;
                            masterBlanketOrderLineModel.ModifiedDate = s.ModifiedDate;


                            masterBlanketOrderLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                            masterBlanketOrderLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                            masterBlanketOrderLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                            masterBlanketOrderLineModels.Add(masterBlanketOrderLineModel);

                        });

                    }
                }
            }
            if (masterBlanketOrderLineModels != null && masterBlanketOrderLineModels.Count > 0)
            {
                result.masterBlanketOrderLineModels = masterBlanketOrderLineModels;
            }
            return result;
        }

        [HttpPost]
        [Route("InsertMasterBlanketOrder")]
        public MasterBlanketOrderModel Post(MasterBlanketOrderModel value)
        {
            var SessionId = Guid.NewGuid();
            var masterBlanketOrder = new MasterBlanketOrder
            {
                CompanyId = value.CompanyID,
                CustomerId = value.CustomerID,
                FromPeriod = value.FromPeriod,
                ToPeriod = value.ToPeriod,
                IsRequireVersionInformation = value.IsRequireVersionInformation,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                VersionSessionId = SessionId,
            };
            _context.MasterBlanketOrder.Add(masterBlanketOrder);
            _context.SaveChanges();
            value.VersionSessionId = masterBlanketOrder.VersionSessionId;
            value.MasterBlanketOrderId = masterBlanketOrder.MasterBlanketOrderId;
            if (value.CustomerID > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerID).Select(s => s.CompanyName).FirstOrDefault();
            }
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == masterBlanketOrder.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }
        [HttpPut]
        [Route("UpdateMasterBlanketOrder")]
        public async Task<MasterBlanketOrderModel> Put(MasterBlanketOrderModel value)
        {
            value.VersionSessionId ??= Guid.NewGuid();
            var masterBlanketOrder = _context.MasterBlanketOrder.SingleOrDefault(p => p.MasterBlanketOrderId == value.MasterBlanketOrderId);
            if (masterBlanketOrder != null)
            {
                if (value.SaveVersionData)
                {
                    var versionInfo = new TableDataVersionInfoModel<MasterBlanketOrderModel>
                    {
                        JsonData = JsonSerializer.Serialize(value),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.VersionSessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "MasterBlanketOrder",
                        PrimaryKey = value.MasterBlanketOrderId,
                        ReferenceInfo = value.ReferenceInfo
                    };
                    await _repository.Insert(versionInfo);
                }
                masterBlanketOrder.CompanyId = value.CompanyID;
                masterBlanketOrder.CustomerId = value.CustomerID;
                masterBlanketOrder.FromPeriod = value.FromPeriod;
                masterBlanketOrder.VersionSessionId = value.VersionSessionId;
                masterBlanketOrder.ToPeriod = value.ToPeriod;
                masterBlanketOrder.IsRequireVersionInformation = value.IsRequireVersionInformation;
                masterBlanketOrder.StatusCodeId = value.StatusCodeID.Value;
                masterBlanketOrder.ModifiedByUserId = value.ModifiedByUserID;
                masterBlanketOrder.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }

            return value;
        }


        [HttpPut]
        [Route("UpdateMasterBlanketOrderVersion")]
        public MasterBlanketOrderModel UpdateMasterBlanketOrderVersion(MasterBlanketOrderModel value)
        {
            var masterBlanketOrder = _context.MasterBlanketOrder.Include(m => m.MasterBlanketOrderLine).SingleOrDefault(p => p.MasterBlanketOrderId == value.MasterBlanketOrderId);
            if (masterBlanketOrder != null)
            {
                masterBlanketOrder.CompanyId = value.CompanyID;
                masterBlanketOrder.CustomerId = value.CustomerID;
                masterBlanketOrder.FromPeriod = value.FromPeriod;
                masterBlanketOrder.ToPeriod = value.ToPeriod;
                masterBlanketOrder.IsRequireVersionInformation = value.IsRequireVersionInformation;
                masterBlanketOrder.StatusCodeId = value.StatusCodeID.Value;
                masterBlanketOrder.ModifiedByUserId = value.ModifiedByUserID;
                masterBlanketOrder.ModifiedDate = DateTime.Now;
                _context.SaveChanges();

                if (masterBlanketOrder.MasterBlanketOrderLine != null)
                {
                    _context.MasterBlanketOrderLine.RemoveRange(masterBlanketOrder.MasterBlanketOrderLine);
                    _context.SaveChanges();
                }
                if (value.masterBlanketOrderLineModels != null && value.masterBlanketOrderLineModels.Count > 0)
                {
                    value.masterBlanketOrderLineModels.ForEach(value =>
                    {
                        var masterBlanketOrderLine = new MasterBlanketOrderLine
                        {
                            MasterBlanketOrderId = value.MasterBlanketOrderId,
                            NoOfWeeksPerMonth = value.NoOfWeeksPerMonth,
                            WeekOneStartDate = value.WeekOneStartDate,
                            WeekOneLargestOrder = value.WeekOneLargestOrder,
                            WeekTwoStartDate = value.WeekTwoStartDate,
                            WeekTwoLargestOrder = value.WeekTwoLargestOrder,
                            WeekThreeStartDate = value.WeekThreeStartDate,
                            WeekThreeLargestOrder = value.WeekThreeLargestOrder,
                            WeekFourStartDate = value.WeekFourStartDate,
                            WeekFourLargestOrder = value.WeekFourLargestOrder,
                            BalanceQtyForCalculate = value.BalanceQtyForCalculate,
                            StatusCodeId = value.StatusCodeID.Value,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.MasterBlanketOrderLine.Add(masterBlanketOrderLine);
                    });
                }
                _context.SaveChanges();
            }

            return value;
        }
        [HttpDelete]
        [Route("DeleteMasterBlanketOrder")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var masterBlanketOrder = _context.MasterBlanketOrder.Include(l => l.MasterBlanketOrderLine).Where(p => p.MasterBlanketOrderId == id).FirstOrDefault();
                if (masterBlanketOrder != null)
                {
                    if (masterBlanketOrder.VersionSessionId != null)
                    {
                        var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == masterBlanketOrder.VersionSessionId).ToList();
                        _context.TableDataVersionInfo.RemoveRange(versions);
                    }
                    _context.MasterBlanketOrder.Remove(masterBlanketOrder);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<MasterBlanketOrderModel> CreateVersion(MasterBlanketOrderModel value)
        {
            try
            {
                value.SessionId = value.VersionSessionId.Value;
                var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var versionData = _context.MasterBlanketOrder.Include(s => s.MasterBlanketOrderLine).SingleOrDefault(p => p.MasterBlanketOrderId == value.MasterBlanketOrderId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<MasterBlanketOrderModel>(versionData);
                    verObject.masterBlanketOrderLineModels = new List<MasterBlanketOrderLineModel>();
                    versionData.MasterBlanketOrderLine.ToList().ForEach(s =>
                    {
                        var masterBlanketOrderLineModel = new MasterBlanketOrderLineModel
                        {
                            MasterBlanketOrderId = s.MasterBlanketOrderId,
                            NoOfWeeksPerMonth = s.NoOfWeeksPerMonth,
                            WeekOneStartDate = s.WeekOneStartDate,
                            WeekOneLargestOrder = s.WeekOneLargestOrder,
                            WeekTwoStartDate = s.WeekTwoStartDate,
                            WeekTwoLargestOrder = s.WeekTwoLargestOrder,
                            WeekThreeStartDate = s.WeekThreeStartDate,
                            WeekThreeLargestOrder = s.WeekThreeLargestOrder,
                            WeekFourStartDate = s.WeekFourStartDate,
                            WeekFourLargestOrder = s.WeekFourLargestOrder,
                            BalanceQtyForCalculate = s.BalanceQtyForCalculate,
                            StatusCodeID = value.StatusCodeID.Value,
                            AddedByUserID = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        verObject.masterBlanketOrderLineModels.Add(masterBlanketOrderLineModel);
                    });


                    var versionInfo = new TableDataVersionInfoModel<MasterBlanketOrderModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "MasterBlanketOrder",
                        PrimaryKey = value.MasterBlanketOrderId,
                        ReferenceInfo = value.ReferenceInfo,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<MasterBlanketOrderModel> ApplyVersion(MasterBlanketOrderModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            try
            {
                var versionData = _context.MasterBlanketOrder.Include(s => s.MasterBlanketOrderLine).SingleOrDefault(p => p.MasterBlanketOrderId == value.MasterBlanketOrderId);

                if (versionData != null)
                {

                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.VersionSessionId = value.VersionSessionId;

                    _context.MasterBlanketOrderLine.RemoveRange(versionData.MasterBlanketOrderLine);
                    _context.SaveChanges();

                    value.masterBlanketOrderLineModels.ForEach(s =>
                    {
                        var masterBlanketOrderLine = new MasterBlanketOrderLine
                        {
                            MasterBlanketOrderId = s.MasterBlanketOrderId,
                            NoOfWeeksPerMonth = s.NoOfWeeksPerMonth,
                            WeekOneStartDate = s.WeekOneStartDate,
                            WeekOneLargestOrder = s.WeekOneLargestOrder,
                            WeekTwoStartDate = s.WeekTwoStartDate,
                            WeekTwoLargestOrder = s.WeekTwoLargestOrder,
                            WeekThreeStartDate = s.WeekThreeStartDate,
                            WeekThreeLargestOrder = s.WeekThreeLargestOrder,
                            WeekFourStartDate = s.WeekFourStartDate,
                            WeekFourLargestOrder = s.WeekFourLargestOrder,
                            BalanceQtyForCalculate = s.BalanceQtyForCalculate,
                            StatusCodeId = value.StatusCodeID.Value,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.MasterBlanketOrderLine.Add(masterBlanketOrderLine);
                    });

                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }

        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(MasterBlanketOrderModel value)
        {
            value.SessionId = value.VersionSessionId.Value;
            var masterBlanketOrder = _context.MasterBlanketOrder.SingleOrDefault(p => p.MasterBlanketOrderId == value.MasterBlanketOrderId);

            if (masterBlanketOrder != null)
            {
                var verObject = _mapper.Map<MasterBlanketOrderModel>(masterBlanketOrder);               
                if (verObject.masterBlanketOrderLineModels != null && verObject.masterBlanketOrderLineModels.Count > 0)
                {
                    verObject.masterBlanketOrderLineModels.ForEach(s =>
                    {
                        var masterBlanketOrderLine = new MasterBlanketOrderLine
                        {
                            MasterBlanketOrderId = s.MasterBlanketOrderId,
                            NoOfWeeksPerMonth = s.NoOfWeeksPerMonth,
                            WeekOneStartDate = s.WeekOneStartDate,
                            WeekOneLargestOrder = s.WeekOneLargestOrder,
                            WeekTwoStartDate = s.WeekTwoStartDate,
                            WeekTwoLargestOrder = s.WeekTwoLargestOrder,
                            WeekThreeStartDate = s.WeekThreeStartDate,
                            WeekThreeLargestOrder = s.WeekThreeLargestOrder,
                            WeekFourStartDate = s.WeekFourStartDate,
                            WeekFourLargestOrder = s.WeekFourLargestOrder,
                            BalanceQtyForCalculate = s.BalanceQtyForCalculate,
                            StatusCodeId = s.StatusCodeID.Value,
                            AddedByUserId = s.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.MasterBlanketOrderLine.Add(masterBlanketOrderLine);
                    });
                }
                var versionInfo = new TableDataVersionInfoModel<MasterBlanketOrderModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "MasterBlanketOrder",
                    PrimaryKey = value.MasterBlanketOrderId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<MasterBlanketOrderModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<MasterBlanketOrderModel>(tableData.JsonData);

                    var masterBlanketOrder = _context.MasterBlanketOrder.Include(s=>s.MasterBlanketOrderLine).SingleOrDefault(p => p.MasterBlanketOrderId == verObject.MasterBlanketOrderId);


                    if (verObject != null && masterBlanketOrder != null)
                    {
                        masterBlanketOrder.FromPeriod = verObject.FromPeriod;
                        masterBlanketOrder.ToPeriod = verObject.ToPeriod;
                        masterBlanketOrder.CompanyId = verObject.CompanyID;
                        masterBlanketOrder.CustomerId = verObject.CustomerID;

                        masterBlanketOrder.StatusCodeId = verObject.StatusCodeID.Value;
                        masterBlanketOrder.ModifiedByUserId = verObject.AddedByUserID;
                        masterBlanketOrder.ModifiedDate = DateTime.Now;
                        if (masterBlanketOrder.MasterBlanketOrderLine != null && masterBlanketOrder.MasterBlanketOrderLine.Count > 0)
                        {
                            _context.MasterBlanketOrderLine.RemoveRange(masterBlanketOrder.MasterBlanketOrderLine);
                            _context.SaveChanges();
                        }
                        if (verObject.masterBlanketOrderLineModels != null && verObject.masterBlanketOrderLineModels.Count > 0)
                        {
                            verObject.masterBlanketOrderLineModels.ForEach(s =>
                            {
                                var masterBlanketOrderLine = new MasterBlanketOrderLine
                                {
                                    MasterBlanketOrderId = s.MasterBlanketOrderId,
                                    NoOfWeeksPerMonth = s.NoOfWeeksPerMonth,
                                    WeekOneStartDate = s.WeekOneStartDate,
                                    WeekOneLargestOrder = s.WeekOneLargestOrder,
                                    WeekTwoStartDate = s.WeekTwoStartDate,
                                    WeekTwoLargestOrder = s.WeekTwoLargestOrder,
                                    WeekThreeStartDate = s.WeekThreeStartDate,
                                    WeekThreeLargestOrder = s.WeekThreeLargestOrder,
                                    WeekFourStartDate = s.WeekFourStartDate,
                                    WeekFourLargestOrder = s.WeekFourLargestOrder,
                                    BalanceQtyForCalculate = s.BalanceQtyForCalculate,
                                    StatusCodeId = s.StatusCodeID.Value,
                                    AddedByUserId = s.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                };
                                _context.MasterBlanketOrderLine.Add(masterBlanketOrderLine);
                            });
                        }
                        _context.SaveChanges();
                    }

                    return verObject;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Undo Version failed.");
            }
            return new MasterBlanketOrderModel();
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }

    }
}