﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Hosting;


using APP.Common;
using APP.BussinessObject;
using Microsoft.OData.Edm;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.IO;


namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductActivityCaseController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly TableVersionRepository _repository;
        public ProductActivityCaseController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _repository = repository;

        }

        [HttpGet]
        [Route("GetProductActivityCaseVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ProductActivityCaseModel>>> GetVersion(string sessionID)
        {
            return await _repository.GetList<ProductActivityCaseModel>(sessionID);
        }
        #region ProductActivityCase
        [HttpGet]
        [Route("GetProductActivityCaseAll")]
        public List<ProductActivityCaseModel> GetProductActivityCaseAll(long id, long masterId, string type)
        {
            var productActivityCases = _context.ProductActivityCase
                                .Include(r => r.ManufacturingProcessChild)
                                .Include(a => a.Action)
                                .Include(a => a.CategoryAction)
                                .Include(a => a.Company)
                                .Include(a => a.VersionCodeStatus)
                                .Include(a=>a.ProductActivityCaseCategoryMultiple)
                                .Include(a=>a.ProductActivityCaseActionMultiple)
                                .Where(a => a.ProductionTypeId == masterId);
            if (id > 0)
            {
                productActivityCases = productActivityCases.Where(w => w.ManufacturingProcessChildId == id);
            }
            List<long?> applicationMasterCodeIds = new List<long?> { 340, 324, 325 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var productActivityCase = productActivityCases.AsNoTracking().ToList();
            List<ProductActivityCaseModel> templateTestCaseModels = new List<ProductActivityCaseModel>();
            if (productActivityCase != null)
            {
                var codeIds = productActivityCase.Select(a => a.ProductionTypeId.GetValueOrDefault(-1)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(a => new { a.CodeId, a.CodeValue }).Where(w => codeIds.Contains(w.CodeId)).ToList();
                List<long> listIds = new List<long>();
                listIds.AddRange(productActivityCase.Select(a => a.ManufacturingProcessChildId.GetValueOrDefault(-1)).ToList());
                listIds.AddRange(productActivityCase.Select(a => a.CategoryActionId.GetValueOrDefault(-1)).ToList());
                listIds.AddRange(productActivityCase.Select(a => a.ActionId.GetValueOrDefault(-1)).ToList());
                listIds = listIds.Distinct().ToList();
                var appMaster = _context.ApplicationMasterChild.ToList();
                productActivityCase.ForEach(s =>
                {
                    ProductActivityCaseModel templateTestCaseModel = new ProductActivityCaseModel();
                    templateTestCaseModel.ProductActivityCaseId = s.ProductActivityCaseId;
                    templateTestCaseModel.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    templateTestCaseModel.ManufacturingProcess = appMaster != null && s.ManufacturingProcessChildId > 0 ? appMaster.FirstOrDefault(f => f.ApplicationMasterChildId == s.ManufacturingProcessChildId)?.Value : "";
                    templateTestCaseModel.Instruction = s.Instruction;
                    templateTestCaseModel.SessionId = s.SessionId;
                    templateTestCaseModel.CategoryActionId = s.CategoryActionId;
                    templateTestCaseModel.CategoryActionIds = s.ProductActivityCaseCategoryMultiple.Where(t=>t.ProductActivityCaseId == s.ProductActivityCaseId).Select(t=>t.CategoryActionId).ToList();
                    templateTestCaseModel.ActionIds = s.ProductActivityCaseActionMultiple.Where(t => t.ProductActivityCaseId == s.ProductActivityCaseId).Select(t => t.ActionId).ToList();
                    templateTestCaseModel.ActionId = s.ActionId;
                    templateTestCaseModel.CategoryAction = appMaster != null && s.CategoryActionId > 0 ? appMaster.FirstOrDefault(f => f.ApplicationMasterChildId == s.CategoryActionId)?.Value : "";
                    templateTestCaseModel.ProductionTypeId = s.ProductionTypeId;
                    templateTestCaseModel.ProductionType = codemaster != null && s.CategoryActionId > 0 ? codemaster.FirstOrDefault(f => f.CodeId == s.CategoryActionId)?.CodeValue : "";
                    templateTestCaseModel.Action = appMaster != null && s.ActionId > 0 ? appMaster.FirstOrDefault(f => f.ApplicationMasterChildId == s.ActionId)?.Value : "";
                    templateTestCaseModel.IsLanguage = s.IsLanguage == null ? "English" : s.IsLanguage;
                    templateTestCaseModel.CompanyId = s.CompanyId;
                    templateTestCaseModel.CompanyName = s.Company?.PlantCode;
                    templateTestCaseModel.ImplementationDate = s.ImplementationDate;
                    templateTestCaseModel.VersionCodeStatusId = s.VersionCodeStatusId;
                    templateTestCaseModel.VersionNo = s.VersionNo;
                    templateTestCaseModel.VersionCodeStatus = s.VersionCodeStatus?.Value;
                    templateTestCaseModel.ProfileId = s.ProfileId;

                    if (type == "English")
                    {
                        templateTestCaseModel.Reference = s.Reference;
                        templateTestCaseModel.CheckerNotes = s.CheckerNotes;
                        templateTestCaseModel.Upload = s.Upload;
                    }
                    if (type == "Malay")
                    {

                        templateTestCaseModel.Reference = s.ReferenceMalay != null && s.ReferenceMalay != "" ? s.ReferenceMalay : s.Reference;
                        templateTestCaseModel.CheckerNotes = s.CheckerNotesMalay != null && s.CheckerNotesMalay != "" ? s.CheckerNotesMalay : s.CheckerNotes;
                        templateTestCaseModel.Upload = s.UploadMalay != null && s.UploadMalay != "" ? s.UploadMalay : s.Upload;
                    }
                    if (type == "Chinese")
                    {
                        templateTestCaseModel.Reference = s.ReferenceChinese != null && s.ReferenceChinese != "" ? s.ReferenceChinese : s.Reference;
                        templateTestCaseModel.CheckerNotes = s.CheckerNotesChinese != null && s.CheckerNotesChinese != "" ? s.CheckerNotesChinese : s.CheckerNotes;
                        templateTestCaseModel.Upload = s.UploadChinese != null && s.UploadChinese != "" ? s.UploadChinese : s.Upload;
                    }
                    if (templateTestCaseModel.CategoryActionIds != null)
                    {
                        templateTestCaseModel.CategoryAction = templateTestCaseModel.CategoryActionIds != null ? string.Join(",", appMaster.Where(w => templateTestCaseModel.CategoryActionIds.Contains(w.ApplicationMasterChildId)).Select(w=>w.Value).ToList()) : "";
                        templateTestCaseModel.CategoryActionId = templateTestCaseModel.CategoryActionIds.FirstOrDefault();
                    }
                    if (templateTestCaseModel.ActionIds != null && templateTestCaseModel.ActionIds.Count > 0)
                    {
                        templateTestCaseModel.Action = templateTestCaseModel.ActionIds != null ? string.Join(",", appMaster.Where(w => templateTestCaseModel.ActionIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                        templateTestCaseModel.ActionId = templateTestCaseModel.ActionIds.FirstOrDefault();
                    }
                    templateTestCaseModels.Add(templateTestCaseModel);
                });
            }
            return templateTestCaseModels;
        }
        [HttpGet]
        [Route("GetProductActivityCaseByCategory")]
        public ProductActivityCaseModel GetProductActivityCaseByCategory(string id)
        {
            ProductActivityCaseModel templateTestCaseModels = new ProductActivityCaseModel();
            if (id != null)
            {
                List<long> Ids = id.Split(',').Select(long.Parse).ToList();
                var productActivityCase = _context.ProductActivityCase.Where(w => Ids.Contains(w.CategoryActionId.Value)).AsNoTracking().Select(o => o.ActionId).ToList();

                if (productActivityCase != null && productActivityCase.Count > 0)
                {
                    templateTestCaseModels.ProductActivityCaseId = 1;
                    templateTestCaseModels.ActionIds = productActivityCase;
                }
            }
            return templateTestCaseModels;
        }
        [HttpGet]
        [Route("GetProductActivityCase")]
        public List<ProductActivityCaseModel> GetTemplateTestCase()
        {
            var productActivityCase = _context.ProductActivityCase
                                .Include(r => r.ManufacturingProcessChild)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(a => a.ProductionType)
                                .Include(a => a.Action)
                                .Include(a => a.CategoryAction)
                                .Include(s => s.StatusCode)
                                .Include(a => a.Company)
                                .Include(a => a.VersionCodeStatus)
                                .AsNoTracking().OrderByDescending(o => o.ProductActivityCaseId).ToList();
            List<ProductActivityCaseModel> templateTestCaseModels = new List<ProductActivityCaseModel>();
            if (productActivityCase != null && productActivityCase.Count > 0)
            {

                productActivityCase.ForEach(s =>
                {
                    ProductActivityCaseModel templateTestCaseModel = new ProductActivityCaseModel();
                    templateTestCaseModel.ProductActivityCaseId = s.ProductActivityCaseId;
                    templateTestCaseModel.ManufacturingProcessId = s.ManufacturingProcessChildId;
                    templateTestCaseModel.ManufacturingProcess = s.ManufacturingProcessChild?.Value;
                    templateTestCaseModel.Instruction = s.Instruction;
                    templateTestCaseModel.ModifiedByUserID = s.AddedByUserId;
                    templateTestCaseModel.AddedByUserID = s.ModifiedByUserId;
                    templateTestCaseModel.StatusCodeID = s.StatusCodeId;
                    templateTestCaseModel.AddedByUser = s.AddedByUser.UserName;
                    templateTestCaseModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    templateTestCaseModel.AddedDate = s.AddedDate;
                    templateTestCaseModel.ModifiedDate = s.ModifiedDate;
                    templateTestCaseModel.StatusCode = s.StatusCode.CodeValue;
                    templateTestCaseModel.SessionId = s.SessionId;
                    templateTestCaseModel.CategoryActionId = s.CategoryActionId;
                    templateTestCaseModel.ActionId = s.ActionId;
                    templateTestCaseModel.CategoryAction = s.CategoryAction?.Value;
                    templateTestCaseModel.ProductionType = s.ProductionType?.CodeValue;
                    templateTestCaseModel.Action = s.Action?.Value;
                    templateTestCaseModel.Reference = s.Reference;
                    templateTestCaseModel.CheckerNotes = s.CheckerNotes;
                    templateTestCaseModel.Upload = s.Upload;
                    templateTestCaseModel.IsLanguage = s.IsLanguage == null ? "English" : s.IsLanguage;
                    templateTestCaseModel.CompanyId = s.CompanyId;
                    templateTestCaseModel.CompanyName = s.Company?.PlantCode;
                    templateTestCaseModel.ImplementationDate = s.ImplementationDate;
                    templateTestCaseModel.ReferenceMalay = s.ReferenceMalay;
                    templateTestCaseModel.CheckerNotesMalay = s.CheckerNotesMalay;
                    templateTestCaseModel.UploadMalay = s.UploadMalay;
                    templateTestCaseModel.ReferenceChinese = s.ReferenceChinese;
                    templateTestCaseModel.CheckerNotesChinese = s.CheckerNotesChinese;
                    templateTestCaseModel.UploadChinese = s.UploadChinese;
                    templateTestCaseModel.VersionCodeStatusId = s.VersionCodeStatusId;
                    templateTestCaseModel.VersionNo = s.VersionNo;
                    templateTestCaseModel.VersionCodeStatus = s.VersionCodeStatus?.Value;
                    templateTestCaseModel.IsEnglishLanguage = s.IsEnglishLanguage;
                    templateTestCaseModel.IsMalayLanguage = s.IsMalayLanguage;
                    templateTestCaseModel.IsChineseLanguage = s.IsChineseLanguage;
                    templateTestCaseModel.ProfileId = s.ProfileId;
                    templateTestCaseModels.Add(templateTestCaseModel);
                });
            }
            return templateTestCaseModels;
        }
        [HttpGet]
        [Route("GetWikiResponsiblesByExits")]
        public WikiResponsibilityModel GetWikiResponsiblesByExits(long? id, long? caseFormId)
        {
            WikiResponsibilityModel wikiResponsibilityModel = new WikiResponsibilityModel();
            var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Where(w => w.ProductActivityCaseId == caseFormId).Select(s => s.ProductActivityCaseResponsId).ToList();

            var templateTestCaseCheckListResponseDutyIds = _context.ProductActivityCaseResponsDuty.Where(w => w.CompanyId == id && templateTestCaseCheckListResponse.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => s.ProductActivityCaseResponsDutyId).ToList();
            var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).ToList();
            wikiResponsibilityModel.CompanyId = id;
            wikiResponsibilityModel.WikiResponsibilityID = 0;
            var userGroupIds = templateTestCaseCheckListResponseResponsible.Where(w => w.UserGroupId != null).Select(u => u.UserGroupId).Distinct().ToList();
            wikiResponsibilityModel.UserGroupIDs = userGroupIds != null && userGroupIds.Count > 0 ? userGroupIds : new List<long?>();
            var userIds = templateTestCaseCheckListResponseResponsible.Where(w => w.EmployeeId != null).Select(u => u.EmployeeId).Distinct().ToList();
            wikiResponsibilityModel.UserIDs = userIds != null && userIds.Count > 0 ? userIds : new List<long?>();
            return wikiResponsibilityModel;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductActivityCaseModel> GetData(SearchModel searchModel)
        {
            var templateTestCase = new ProductActivityCase();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).FirstOrDefault();
                        break;
                    case "Last":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).LastOrDefault();
                        break;
                    case "Next":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).LastOrDefault();
                        break;
                    case "Previous":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).FirstOrDefault();
                        break;
                    case "Last":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).LastOrDefault();
                        break;
                    case "Next":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderBy(o => o.ProductActivityCaseId).FirstOrDefault(s => s.ProductActivityCaseId > searchModel.Id);
                        break;
                    case "Previous":
                        templateTestCase = _context.ProductActivityCase.Include(a => a.ManufacturingProcess).OrderByDescending(o => o.ProductActivityCaseId).FirstOrDefault(s => s.ProductActivityCaseId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductActivityCaseModel>(templateTestCase);
            return result;
        }
        [HttpPost]
        [Route("InsertProductActivityCase")]
        public ProductActivityCaseModel InsertProductActivityCase(ProductActivityCaseModel value)
        {
            if (value.ActionIds != null && value.ActionIds.Count > 0)
            {
                value.ActionIds.ForEach(c =>
                {
                    var sessionId = Guid.NewGuid();
            var productActivityCase = new ProductActivityCase
            {
                ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                Instruction = value.Instruction,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = sessionId,
                CategoryActionId = value.CategoryActionId,
                ActionId = value.ActionId,
                Reference = value.Reference,
                CheckerNotes = value.CheckerNotes,
                Upload = value.Upload,
                ProductionTypeId = value.ProductionTypeId,
                IsLanguage = value.IsLanguage,
                CompanyId = value.CompanyId,
                ImplementationDate = value.ImplementationDate,
                ReferenceMalay = value.ReferenceMalay,
                CheckerNotesMalay = value.CheckerNotesMalay,
                UploadMalay = value.UploadMalay,
                ReferenceChinese = value.ReferenceChinese,
                CheckerNotesChinese = value.CheckerNotesChinese,
                UploadChinese = value.UploadChinese,
                VersionCodeStatusId = value.VersionCodeStatusId,
                VersionNo = value.VersionNo,
                IsEnglishLanguage = value.IsEnglishLanguage,
                IsMalayLanguage = value.IsMalayLanguage,
                IsChineseLanguage = value.IsChineseLanguage,
                ProfileId = value.ProfileId,
                FileProfileTypeId = value.FileProfileTypeId,
            };
            _context.ProductActivityCase.Add(productActivityCase);
            _context.SaveChanges();
                    value.ProductActivityCaseId = productActivityCase.ProductActivityCaseId;
                    value.SessionId = sessionId;
                    value.AddedDate = productActivityCase.AddedDate;
                    value.AddedByUser = _context.ApplicationUser.FirstOrDefault(f => f.UserId == productActivityCase.AddedByUserId)?.UserName;

                    if (value.CompanyIds != null && value.CompanyIds.Count > 0)
                    {
                        value.CompanyIds.ForEach(s =>
                        {
                            var company = new ProductActivityCaseCompany
                            {
                                CompanyId = s,
                                ProductActivityCaseId = value.ProductActivityCaseId,
                                ImplementationDate = value.ImplementationDate,
                                StatusCodeId = value.StatusCodeID.Value,
                                VersionCodeStatusId = value.VersionCodeStatusId,
                                VersionNo = value.VersionNo,
                            };
                            _context.ProductActivityCaseCompany.Add(company);
                            _context.SaveChanges();
                        });
                    }
                    if (value.CompanyMultipleIds != null && value.CompanyMultipleIds.Count > 0)
                    {
                        value.CompanyMultipleIds.ForEach(c =>
                        {
                            var companymultiple = new ProductActivityCaseCompanyMultiple
                            {
                                CompanyId = c,
                                ProductActivityCaseId = value.ProductActivityCaseId,
                            };
                            _context.ProductActivityCaseCompanyMultiple.Add(companymultiple);
                            _context.SaveChanges();
                        });
                    }
                    if (value.ActionIds != null && value.ActionIds.Count > 0)
                    {
                        value.ActionIds.ForEach(c =>
                        {
                            var actionmultiple = new ProductActivityCaseActionMultiple
                            {
                                ActionId = c,
                                ProductActivityCaseId = value.ProductActivityCaseId,
                            };
                            _context.ProductActivityCaseActionMultiple.Add(actionmultiple);
                            _context.SaveChanges();
                        });
                    }
                    if (value.CategoryActionMultipleIds != null && value.CategoryActionMultipleIds.Count > 0)
                    {
                        value.CategoryActionMultipleIds.ForEach(c =>
                        {
                            var categoryactionMultiple = new ProductActivityCaseCategoryMultiple
                            {
                                CategoryActionId = c,
                                ProductActivityCaseId = value.ProductActivityCaseId,
                            };
                            _context.ProductActivityCaseCategoryMultiple.Add(categoryactionMultiple);
                            _context.SaveChanges();
                        });
                    }

                });
            }
          
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityCase")]
        public ProductActivityCaseModel UpdateProductActivityCase(ProductActivityCaseModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.ProductActivityCase.SingleOrDefault(p => p.ProductActivityCaseId == value.ProductActivityCaseId);
            templateTestCase.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
            templateTestCase.SessionId = value.SessionId;
            templateTestCase.ModifiedByUserId = value.ModifiedByUserID;
            templateTestCase.ModifiedDate = DateTime.Now;
            templateTestCase.Instruction = value.Instruction;
            templateTestCase.CategoryActionId = value.CategoryActionId;
            templateTestCase.ActionId = value.ActionId;
            templateTestCase.ManufacturingProcessId = value.ManufacturingProcessId;
            templateTestCase.Reference = value.Reference;
            templateTestCase.CheckerNotes = value.CheckerNotes;
            templateTestCase.Upload = value.Upload;
            templateTestCase.ProductionTypeId = value.ProductionTypeId;
            templateTestCase.IsLanguage = value.IsLanguage;
            templateTestCase.CompanyId = value.CompanyId;
            templateTestCase.ImplementationDate = value.ImplementationDate;
            templateTestCase.ReferenceMalay = value.ReferenceMalay;
            templateTestCase.CheckerNotesMalay = value.CheckerNotesMalay;
            templateTestCase.UploadMalay = value.UploadMalay;
            templateTestCase.ReferenceChinese = value.ReferenceChinese;
            templateTestCase.CheckerNotesChinese = value.CheckerNotesChinese;
            templateTestCase.UploadChinese = value.UploadChinese;
            templateTestCase.VersionCodeStatusId = value.VersionCodeStatusId;
            templateTestCase.VersionNo = value.VersionNo;
            templateTestCase.IsEnglishLanguage = value.IsEnglishLanguage;
            templateTestCase.IsMalayLanguage = value.IsMalayLanguage;
            templateTestCase.IsChineseLanguage = value.IsChineseLanguage;
            templateTestCase.ProfileId = value.ProfileId;
            templateTestCase.FileProfileTypeId = value.FileProfileTypeId;
            _context.SaveChanges();
            value.AddedDate = templateTestCase.AddedDate;
            value.AddedByUser = _context.ApplicationUser.FirstOrDefault(f => f.UserId == templateTestCase.AddedByUserId)?.UserName;
            var ProductActivityCaseCompanyRemove = _context.ProductActivityCaseCompany.Where(w => w.ProductActivityCaseId == value.ProductActivityCaseId).ToList();
            if (ProductActivityCaseCompanyRemove != null)
            {
                _context.ProductActivityCaseCompany.RemoveRange(ProductActivityCaseCompanyRemove);
                _context.SaveChanges();
            }
            if (value.CompanyIds != null && value.CompanyIds.Count > 0)
            {
                value.CompanyIds.ForEach(s =>
                {
                    var company = new ProductActivityCaseCompany
                    {
                        CompanyId = s,
                        ProductActivityCaseId = value.ProductActivityCaseId,
                        ImplementationDate = value.ImplementationDate,
                        StatusCodeId = value.StatusCodeID.Value,
                        VersionCodeStatusId = value.VersionCodeStatusId,
                        VersionNo = value.VersionNo,

                    };
                    _context.ProductActivityCaseCompany.Add(company);
                    _context.SaveChanges();
                });
            }

            var productActivityCaseCompanyMultiple = _context.ProductActivityCaseCompanyMultiple.Where(w => w.ProductActivityCaseId == value.ProductActivityCaseId).ToList();
            if (productActivityCaseCompanyMultiple != null)
            {
                _context.ProductActivityCaseCompanyMultiple.RemoveRange(productActivityCaseCompanyMultiple);
                _context.SaveChanges();
            }
            if (value.CompanyMultipleIds != null && value.CompanyMultipleIds.Count > 0)
            {
                value.CompanyMultipleIds.ForEach(c =>
                {
                    var companymultiple = new ProductActivityCaseCompanyMultiple
                    {
                        CompanyId = c,
                        ProductActivityCaseId = value.ProductActivityCaseId,
                    };
                    _context.ProductActivityCaseCompanyMultiple.Add(companymultiple);
                    _context.SaveChanges();
                });
            }
            var productActivityCaseActionMultiple = _context.ProductActivityCaseActionMultiple.Where(w => w.ProductActivityCaseId == value.ProductActivityCaseId).ToList();
            if (productActivityCaseActionMultiple != null)
            {
                _context.ProductActivityCaseActionMultiple.RemoveRange(productActivityCaseActionMultiple);
                _context.SaveChanges();
            }
            if (value.ActionIds != null && value.ActionIds.Count > 0)
            {
                value.ActionIds.ForEach(c =>
                {
                    var actionmultiple = new ProductActivityCaseActionMultiple
                    {
                        ActionId = c,
                        ProductActivityCaseId = value.ProductActivityCaseId,
                    };
                    _context.ProductActivityCaseActionMultiple.Add(actionmultiple);
                    _context.SaveChanges();
                });
            }
            var productActivityCaseCategoryMultiple = _context.ProductActivityCaseCategoryMultiple.Where(w => w.ProductActivityCaseId == value.ProductActivityCaseId).ToList();
            if (productActivityCaseCategoryMultiple != null)
            {
                _context.ProductActivityCaseCategoryMultiple.RemoveRange(productActivityCaseCategoryMultiple);
                _context.SaveChanges();
            }
            if (value.CategoryActionMultipleIds != null && value.CategoryActionMultipleIds.Count > 0)
            {
                value.CategoryActionMultipleIds.ForEach(c =>
                {
                    var categoryactionMultiple = new ProductActivityCaseCategoryMultiple
                    {
                        CategoryActionId = c,
                        ProductActivityCaseId = value.ProductActivityCaseId,
                    };
                    _context.ProductActivityCaseCategoryMultiple.Add(categoryactionMultiple);
                    _context.SaveChanges();
                });
            }

            return value;
        }
        [HttpDelete]
        [Route("DeleteProductActivityCase")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var templateTestCase = _context.ProductActivityCase.SingleOrDefault(p => p.ProductActivityCaseId == id);
                if (templateTestCase != null)
                {
                    var templateTestCaseLink = _context.ProductActivityCaseLine.Where(w => w.ProductActivityCaseId == templateTestCase.ProductActivityCaseId).ToList();
                    if (templateTestCaseLink != null)
                    {
                        _context.ProductActivityCaseLine.RemoveRange(templateTestCaseLink);
                        _context.SaveChanges();
                    }
                    var productionActivityMasterAll = _context.ProductActivityCaseRespons.Where(p => p.ProductActivityCaseId == id).ToList();
                    if (productionActivityMasterAll != null)
                    {
                        productionActivityMasterAll.ForEach(s =>
                        {
                            var productionActivityMaster = _context.ProductActivityCaseRespons.SingleOrDefault(p => p.ProductActivityCaseResponsId == s.ProductActivityCaseResponsId);
                            if (productionActivityMaster != null)
                            {
                                var applicationWikiWeeklyRemove = _context.ProductActivityCaseResponsWeekly.Where(p => p.ProductActivityCaseResponsId == s.ProductActivityCaseResponsId).ToList();
                                if (applicationWikiWeeklyRemove != null)
                                {
                                    _context.ProductActivityCaseResponsWeekly.RemoveRange(applicationWikiWeeklyRemove);
                                    _context.SaveChanges();
                                }
                                var productActivityCaseResponsRecurrenceReomve = _context.ProductActivityCaseResponsRecurrence.Where(p => p.ProductActivityCaseResponsId == id).ToList();
                                if (applicationWikiWeeklyRemove != null)
                                {
                                    _context.ProductActivityCaseResponsRecurrence.RemoveRange(productActivityCaseResponsRecurrenceReomve);
                                    _context.SaveChanges();
                                }
                                _context.ProductActivityCaseRespons.Remove(productionActivityMaster);
                                _context.SaveChanges();
                            }
                        });
                    }
                    _context.ProductActivityCase.Remove(templateTestCase);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductActivityCase Cannot be delete! Reference to others");
            }
        }
        #endregion
        #region ProductActivityCaseLine
        [HttpGet]
        [Route("GetProductActivityCaseLine")]
        public List<ProductActivityCaseLineModel> GetProductActivityCaseLine(long? id)
        {
            List<ProductActivityCaseLineModel> templateTestCaseLinkModels = new List<ProductActivityCaseLineModel>();
            var templateTestCaseLink = _context.ProductActivityCaseLine.Include(a => a.ProductActivityCase).Include(a => a.ProductActivityCase.ManufacturingProcessChild).Include(a => a.TemplateProfile).Include(a => a.ProdActivityCategoryChild).Include(a => a.ProdActivityActionChild).Where(w => w.ProductActivityCaseId == id).ToList();
            templateTestCaseLink.ForEach(s =>
            {
                ProductActivityCaseLineModel templateTestCaseModel = new ProductActivityCaseLineModel();
                templateTestCaseModel.ManufacturingProcessChilds = s.ProductActivityCase?.ManufacturingProcessChild?.Value;
                templateTestCaseModel.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                templateTestCaseModel.ProductActivityCaseId = s.ProductActivityCaseId;
                templateTestCaseModel.NameOfTemplate = s.NameOfTemplate;
                templateTestCaseModel.Subject = s.Subject;
                templateTestCaseModel.Link = s.Link;
                templateTestCaseModel.LocationName = s.LocationName;
                templateTestCaseModel.Naming = s.Naming;
                templateTestCaseModel.IsAutoNumbering = s.IsAutoNumbering;
                templateTestCaseModel.AutoNumbering = s.IsAutoNumbering == true ? "Yes" : "No";
                templateTestCaseModel.TemplateProfileId = s.TemplateProfileId;
                templateTestCaseModel.TemplateProfileName = s.TemplateProfile?.Name;
                templateTestCaseModel.ProdActivityActionChildId = s.ProdActivityActionChildId;
                templateTestCaseModel.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                templateTestCaseModel.ProdActivityCategory = s.ProdActivityCategoryChild?.Value;
                templateTestCaseModel.ProdActivityAction = s.ProdActivityActionChild?.Value;
                templateTestCaseModel.DocumentNo = s.DocumentNo;
                templateTestCaseModel.LocationToSaveId = s.LocationToSaveId;
                templateTestCaseModel.TopicId = s.TopicId;
                templateTestCaseLinkModels.Add(templateTestCaseModel);
            });
            return templateTestCaseLinkModels;
        }
        [HttpGet]
        [Route("GetProductActivityCaseCategory")]
        public List<ProductActivityCaseLineModel> GetProductActivityCaseCategory(long? mprocessId, long? id)
        {
            List<ProductActivityCaseLineModel> templateTestCaseLinkModels = new List<ProductActivityCaseLineModel>();
            var categoryMultipleCaseIds = _context.ProductActivityCaseCategoryMultiple.Where(s => s.CategoryActionId == id).Select(s=>s.ProductActivityCaseId).ToList();
            var lineIds = _context.ProductActivityCaseLine.Where(l=> categoryMultipleCaseIds.Contains(l.ProductActivityCaseId)).Select(s=>s.ProductActivityCaseLineId).ToList();
            var templateTestCaseLink = _context.ProductActivityCaseLine.Include(a => a.ProductActivityCase)
                .Include(a => a.TemplateProfile)
                .Include(a => a.ProdActivityActionChild)
                .Include(a => a.ProdActivityCategoryChild)
                .Where(w => lineIds.Contains(w.ProductActivityCaseLineId) && w.ProductActivityCase.ManufacturingProcessChildId == mprocessId).ToList();
            templateTestCaseLink.ForEach(s =>
            {
                ProductActivityCaseLineModel templateTestCaseModel = new ProductActivityCaseLineModel();
                templateTestCaseModel.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                templateTestCaseModel.ProductActivityCaseId = s.ProductActivityCaseId;
                templateTestCaseModel.NameOfTemplate = s.NameOfTemplate;
                templateTestCaseModel.Subject = s.Subject;
                templateTestCaseModel.Link = s.Link;
                templateTestCaseModel.LocationName = s.LocationName;
                templateTestCaseModel.Naming = s.Naming;
                templateTestCaseModel.IsAutoNumbering = s.IsAutoNumbering;
                templateTestCaseModel.AutoNumbering = s.IsAutoNumbering == true ? "Yes" : "No";
                templateTestCaseModel.TemplateProfileId = s.TemplateProfileId;
                templateTestCaseModel.TemplateProfileName = s.TemplateProfile?.Name;
                templateTestCaseModel.ProdActivityActionChildId = s.ProdActivityActionChildId;
                templateTestCaseModel.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                templateTestCaseModel.ProdActivityCategory = s.ProdActivityCategoryChild?.Value;
                templateTestCaseModel.ProdActivityAction = s.ProdActivityActionChild?.Value;
                templateTestCaseModel.DocumentNo = s.DocumentNo;
                templateTestCaseModel.LocationToSaveId = s.LocationToSaveId;
                templateTestCaseLinkModels.Add(templateTestCaseModel);
            });
            return templateTestCaseLinkModels;
        }
        [HttpPost]
        [Route("InsertProductActivityCaseLine")]
        public ProductActivityCaseLineModel InsertProductActivityCaseLine(ProductActivityCaseLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCaseLink = new ProductActivityCaseLine
            {
                ProductActivityCaseId = value.ProductActivityCaseId,
                Link = value.Link,
                Subject = value.Subject,
                NameOfTemplate = value.NameOfTemplate,
                LocationName = value.LocationName,
                ProdActivityCategoryChildId = value.ProdActivityCategoryChildId,
                ProdActivityActionChildId = value.ProdActivityActionChildId,
                IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false,
                TemplateProfileId = value.TemplateProfileId,
                Naming = value.Naming,
                SessionId = value.SessionId,
                LocationToSaveId = value.LocationToSaveId,
            };
            _context.ProductActivityCaseLine.Add(templateTestCaseLink);
            _context.SaveChanges();
            value.ProductActivityCaseLineId = templateTestCaseLink.ProductActivityCaseLineId;
            value.SessionId = value.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityCaseLine")]
        public ProductActivityCaseLineModel UpdateProductActivityCaseLine(ProductActivityCaseLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.ProductActivityCaseLine.SingleOrDefault(p => p.ProductActivityCaseLineId == value.ProductActivityCaseLineId);
            templateTestCase.ProductActivityCaseId = value.ProductActivityCaseId;
            templateTestCase.Link = value.Link;
            templateTestCase.Subject = value.Subject;
            templateTestCase.NameOfTemplate = value.NameOfTemplate;
            templateTestCase.IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.LocationName = value.LocationName;
            templateTestCase.ProdActivityCategoryChildId = value.ProdActivityCategoryChildId;
            templateTestCase.ProdActivityActionChildId = value.ProdActivityActionChildId;
            templateTestCase.Naming = value.Naming;
            templateTestCase.SessionId = value.SessionId;
            templateTestCase.LocationToSaveId = value.LocationToSaveId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityCaseTopicLine")]
        public ProductActivityCaseLineModel UpdateProductActivityCaseTopicLine(ProductActivityCaseLineModel value)
        {
            var templateTestCase = _context.ProductActivityCaseLine.SingleOrDefault(p => p.ProductActivityCaseLineId == value.ProductActivityCaseLineId);
            templateTestCase.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductActivityCaseLine")]
        public ActionResult<string> DeleteProductActivityCaseLine(int id)
        {
            try
            {
                var templateTestCase = _context.ProductActivityCaseLine.SingleOrDefault(p => p.ProductActivityCaseLineId == id);
                if (templateTestCase != null)
                {
                    _context.ProductActivityCaseLine.Remove(templateTestCase);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductActivityCaseLine Cannot be delete! Reference to others");
            }
        }
        #endregion
        #region Product Activity Response
        [HttpGet]
        [Route("GetProductionActivityMasterRespons")]
        public List<ProductActivityCaseResponsModel> GetProductionActivityMasterRespons(long id)
        {
            List<ProductActivityCaseResponsModel> productionActivityMasterModels = new List<ProductActivityCaseResponsModel>();

            var productionActivityMaster = _context.ProductActivityCaseRespons.Include(s => s.AddedByUser).Include(a => a.PageLinkNavigation).Include(a => a.FunctionLinkNavigation).Include(a => a.Duty).Include(s => s.ProductActivityCaseResponsWeekly).Include(s => s.ModifiedByUser).Include(a => a.StatusCode).Where(w => w.ProductActivityCaseId == id && w.ScreenId == "ByLine").OrderByDescending(o => o.ProductActivityCaseResponsId).AsNoTracking().ToList();

            if (productionActivityMaster != null && productionActivityMaster.Count > 0)
            {
                productionActivityMaster.ForEach(s =>
                {
                    ProductActivityCaseResponsModel productionActivityMasterModel = new ProductActivityCaseResponsModel
                    {
                        ProductActivityCaseId = s.ProductActivityCaseId,
                        ProductActivityCaseResponsId = s.ProductActivityCaseResponsId,
                        DutyId = s.DutyId,
                        Responsibility = s.Responsibility,
                        FunctionLink = s.FunctionLink,
                        PageLink = s.PageLink,
                        AddedByUserId = s.AddedByUserId,
                        AddedByUser = s.AddedByUser?.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode?.CodeValue,
                        AddedDate = s.AddedDate,
                        StatusCodeId = s.StatusCodeId,
                        NotificationAdvice = s.NotificationAdvice,
                        NotificationAdviceFlag = s.NotificationAdvice == true ? "Yes" : "No",
                        NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                        RepeatId = s.RepeatId,
                        CustomId = s.CustomId,
                        DueDate = s.DueDate,
                        Monthly = s.Monthly,
                        Yearly = s.Yearly,
                        EventDescription = s.EventDescription,
                        DaysOfWeek = s.DaysOfWeek,
                        // SessionId = SessionId,
                        NotificationStatusId = s.NotificationStatusId,
                        Title = s.Title,
                        Message = s.Message,
                        ScreenId = s.ScreenId,
                        NotifyEndDate = s.NotifyEndDate,
                        IsAllowDocAccess = s.IsAllowDocAccess,
                        WeeklyIds = s.ProductActivityCaseResponsWeekly != null ? s.ProductActivityCaseResponsWeekly.Where(c => c.ProductActivityCaseResponsId == s.ProductActivityCaseResponsId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        DaysOfWeekIds = s.ProductActivityCaseResponsWeekly != null ? s.ProductActivityCaseResponsWeekly.Where(c => c.ProductActivityCaseResponsId == s.ProductActivityCaseResponsId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        PageLinkName = s.PageLinkNavigation?.PermissionName,
                        FunctionLinkName = s.FunctionLinkNavigation?.PermissionName,
                        DutyName = s.Duty?.Value,
                    };
                    productionActivityMasterModels.Add(productionActivityMasterModel);
                });
            }
            return productionActivityMasterModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityMasterResponsByLine")]
        public ProductActivityCaseResponsModel InsertProductionActivityMasterResponsByLine(ProductActivityCaseResponsModel value)
        {
            //var SessionId = Guid.NewGuid();
            var productionActivityMaster = new ProductActivityCaseRespons
            {
                ProductActivityCaseId = value.ProductActivityCaseId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeId.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                //SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = "ByLine",
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess = value.IsAllowDocAccess,
            };
            _context.ProductActivityCaseRespons.Add(productionActivityMaster);
            _context.SaveChanges();
            value.ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                    {
                        ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                    {
                        ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            // value.SessionId = SessionId;
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
            return value;
        }
        [HttpPost]
        [Route("InsertProductionActivityMasterRespons")]
        public ProductActivityCaseResponsModel InsertProductionActivityMasterRespons(ProductActivityCaseResponsModel value)
        {
            //var SessionId = Guid.NewGuid();
            var productionActivityMaster = new ProductActivityCaseRespons
            {
                ProductActivityCaseId = value.ProductActivityCaseId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeId.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                // SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = value.ScreenId,
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess = value.IsAllowDocAccess,
            };
            _context.ProductActivityCaseRespons.Add(productionActivityMaster);
            _context.SaveChanges();
            value.ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
            /* if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
             {
                 value.WeeklyIds.ForEach(u =>
                 {
                     var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                     {
                         ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                         WeeklyId = u,
                         CustomType = "Weekly",
                     };
                     _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                     _context.SaveChanges();
                 });
             }
             if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
             {
                 value.DaysOfWeekIds.ForEach(u =>
                 {
                     var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                     {
                         ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                         WeeklyId = u,
                         CustomType = "Yearly",
                     };
                     _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                     _context.SaveChanges();
                 });
             }*/
            //value.SessionId = SessionId;
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityMasterRespons")]
        public ProductActivityCaseResponsModel UpdateProductionActivityMasterRespons(ProductActivityCaseResponsModel value)
        {
            var productionActivityMaster = _context.ProductActivityCaseRespons.SingleOrDefault(p => p.ProductActivityCaseResponsId == value.ProductActivityCaseResponsId);

            productionActivityMaster.ProductActivityCaseId = value.ProductActivityCaseId;
            productionActivityMaster.DutyId = value.DutyId;
            productionActivityMaster.Responsibility = value.Responsibility;
            productionActivityMaster.FunctionLink = value.FunctionLink;
            productionActivityMaster.PageLink = value.PageLink;
            productionActivityMaster.ModifiedByUserId = value.AddedByUserID;
            productionActivityMaster.ModifiedDate = DateTime.Now;
            productionActivityMaster.StatusCodeId = value.StatusCodeId.Value;
            productionActivityMaster.NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false;
            productionActivityMaster.NotificationAdviceTypeId = value.NotificationAdviceTypeId;
            productionActivityMaster.RepeatId = value.RepeatId;
            productionActivityMaster.CustomId = value.CustomId;
            productionActivityMaster.DueDate = value.DueDate == null ? DateTime.Now : value.DueDate;
            productionActivityMaster.Monthly = value.Monthly;
            productionActivityMaster.Yearly = value.Yearly;
            productionActivityMaster.EventDescription = value.EventDescription;
            productionActivityMaster.DaysOfWeek = value.DaysOfWeek;
            // SessionId = SessionId,
            productionActivityMaster.NotificationStatusId = value.NotificationStatusId;
            productionActivityMaster.Title = value.Title;
            productionActivityMaster.Message = value.Message;
            productionActivityMaster.ScreenId = value.ScreenId;
            productionActivityMaster.NotifyEndDate = value.NotifyEndDate;
            productionActivityMaster.IsAllowDocAccess = value.IsAllowDocAccess;
            _context.SaveChanges();
            var applicationWikiWeeklyRemove = _context.ProductActivityCaseResponsWeekly.Where(p => p.ProductActivityCaseResponsId == value.ProductActivityCaseResponsId).ToList();
            if (applicationWikiWeeklyRemove != null)
            {
                _context.ProductActivityCaseResponsWeekly.RemoveRange(applicationWikiWeeklyRemove);
                _context.SaveChanges();
            }
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                    {
                        ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ProductActivityCaseResponsWeekly()
                    {
                        ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.ProductActivityCaseResponsWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.ProductActivityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityMasterRespons")]
        public ActionResult<string> DeleteProductionActivityMasterRespons(int id)
        {
            try
            {
                var productionActivityMaster = _context.ProductActivityCaseRespons.SingleOrDefault(p => p.ProductActivityCaseResponsId == id);
                if (productionActivityMaster != null)
                {
                    var applicationWikiWeeklyRemove = _context.ProductActivityCaseResponsWeekly.Where(p => p.ProductActivityCaseResponsId == id).ToList();
                    if (applicationWikiWeeklyRemove != null)
                    {
                        _context.ProductActivityCaseResponsWeekly.RemoveRange(applicationWikiWeeklyRemove);
                        _context.SaveChanges();
                    }
                    var productActivityCaseResponsRecurrenceReomve = _context.ProductActivityCaseResponsRecurrence.Where(p => p.ProductActivityCaseResponsId == id).ToList();
                    if (applicationWikiWeeklyRemove != null)
                    {
                        _context.ProductActivityCaseResponsRecurrence.RemoveRange(productActivityCaseResponsRecurrenceReomve);
                        _context.SaveChanges();
                    }
                    _context.ProductActivityCaseRespons.Remove(productionActivityMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductActivityCaseRespons Cannot be delete! Reference to others");
            }
        }
        [HttpGet]
        [Route("GetRecurrence")]
        public List<ProductActivityCaseResponsRecurrenceModel> GetRecurrence(long id)
        {
            List<ProductActivityCaseResponsRecurrenceModel> productionActivityMasterResponsRecurrenceModels = new List<ProductActivityCaseResponsRecurrenceModel>();
            var recurrence = _context.ProductActivityCaseResponsRecurrence
                 .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(b => b.StatusCode)
                .Include(c => c.Type)
                .Include(d => d.OccurenceOption)
                .AsNoTracking().Where(w => w.ProductActivityCaseResponsId == id).OrderByDescending(o => o.ProductActivityCaseResponsRecurrenceId).AsNoTracking().ToList();
            recurrence.ForEach(s =>
            {
                ProductActivityCaseResponsRecurrenceModel productionActivityMasterResponsRecurrenceModel = new ProductActivityCaseResponsRecurrenceModel
                {
                    ProductActivityCaseResponsRecurrenceId = s.ProductActivityCaseResponsRecurrenceId,
                    ProductActivityCaseResponsId = s.ProductActivityCaseResponsId,
                    TypeId = s.TypeId,
                    RepeatNos = s.RepeatNos,
                    OccurenceOptionId = s.OccurenceOptionId,
                    NoOfOccurences = s.NoOfOccurences,
                    Sunday = s.Sunday,
                    Monday = s.Monday,
                    Tuesday = s.Tuesday,
                    Wednesday = s.Wednesday,
                    Thursday = s.Thursday,
                    Friday = s.Friday,
                    Saturyday = s.Saturyday,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    TypeName = s.Type.CodeValue,
                    OccurenceOptionName = s.OccurenceOption?.CodeValue,
                };
                productionActivityMasterResponsRecurrenceModels.Add(productionActivityMasterResponsRecurrenceModel);
            });
            return productionActivityMasterResponsRecurrenceModels;
        }
        [HttpPost]
        [Route("InsertRecurrence")]
        public ProductActivityCaseResponsRecurrenceModel InsertRecurrence(ProductActivityCaseResponsRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.ProductActivityCaseResponsRecurrence.Where(p => p.ProductActivityCaseResponsId == value.ProductActivityCaseResponsId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.ProductActivityCaseResponsRecurrence.SingleOrDefault(p => p.ProductActivityCaseResponsId == value.ProductActivityCaseResponsId);
                ApplicationWikiRecurrences.ProductActivityCaseResponsId = value.ProductActivityCaseResponsId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new ProductActivityCaseResponsRecurrence
                {
                    ProductActivityCaseResponsId = value.ProductActivityCaseResponsId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.ProductActivityCaseResponsRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.ProductActivityCaseResponsRecurrenceId = ApplicationWikiRecurrence.ProductActivityCaseResponsRecurrenceId;
                return value;
            }
        }
        #endregion
        #region ProductActivityCaseResponsDuty
        [HttpGet]
        [Route("GetApplicationWikiLineDuty")]
        public List<ProductActivityCaseResponsDutyModel> ApplicationWikiLineDuty(long id)
        {
            var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Where(w => w.ProductActivityCaseId == id).Select(s => s.ProductActivityCaseResponsId).ToList();

            List<ProductActivityCaseResponsDutyModel> applicationWikiLineDutyModels = new List<ProductActivityCaseResponsDutyModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.ProductActivityCaseResponsDuty
                                .Include("Departmant")
                                .Include("Designation")
                                .Include("Company")
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("Employee")
                                .Where(w => templateTestCaseCheckListResponse.Contains(w.ProductActivityCaseResponsId.Value)).OrderByDescending(o => o.ProductActivityCaseResponsDutyId).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                ProductActivityCaseResponsDutyModel applicationWikiLineDutyModel = new ProductActivityCaseResponsDutyModel
                {
                    ProductActivityCaseResponsId = s.ProductActivityCaseResponsId,
                    ProductActivityCaseResponsDutyId = s.ProductActivityCaseResponsDutyId,
                    DepartmentId = s.DepartmantId,
                    EmployeeId = s.EmployeeId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Departmant?.Name,
                    DesignationNumber = s.DesignationNumber,
                    CompanyId = s.CompanyId,
                    PlantCompanyName = s.Company?.Description,
                    DesignationId = s.DesignationId,
                    DesignationName = s.Designation?.Name,
                    DutyNo = s.DutyNo,
                    DutyNoName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    Type = s.Type,
                    Description = s.Description,
                    wikiResponsibilityModels = GetWikiResponsibleByID((int)s.ProductActivityCaseResponsDutyId)
                    //EmployeeIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.EmployeeId).ToList(),
                    //UserGroupIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.UserGroupId).Distinct().ToList(),
                };
                applicationWikiLineDutyModels.Add(applicationWikiLineDutyModel);
            });

            return applicationWikiLineDutyModels;
        }
        [HttpGet]
        [Route("GetWikiResponsibleByID")]
        public List<ProductActivityCaseResponsResponsibleModel> GetWikiResponsibleByID(int? id)
        {
            List<ProductActivityCaseResponsResponsibleModel> wikiResponsibilityModels = new List<ProductActivityCaseResponsResponsibleModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var plantList = _context.Plant.ToList();
            var appuser = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
                
            }).ToList();
            var emp = _context.Employee.Select(s => new
            {
                s.EmployeeId,
                s.UserId,

            }).ToList();
            var usergroup = _context.UserGroupUser.Include(u => u.UserGroup).ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.Where(p => p.ProductActivityCaseResponsDutyId == id).ToList();
            var wikiResponsible = _context.ProductActivityCaseResponsResponsible

                                .Include(o => o.Employee)
                                .Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.User)
                                .Where(s => s.ProductActivityCaseResponsDutyId == id.Value).AsNoTracking().ToList();
            ProductActivityPermissionModel productActivityPermissionM = new ProductActivityPermissionModel();
            var userIds= wikiResponsible.Select(s=>s.UserId).ToList();
            var productActivityPermission = productActivityPermissionList.Where(w => w.ProductActivityCaseResponsDutyId == id).FirstOrDefault();
            if (productActivityPermission != null)
            {

                productActivityPermissionM.ProductActivityPermissionId = productActivityPermission.ProductActivityPermissionId;
                productActivityPermissionM.IsActivityInfo = productActivityPermission.IsActivityInfo;
                productActivityPermissionM.IsChecker = productActivityPermission.IsChecker;
                productActivityPermissionM.IsCheckOut = productActivityPermission.IsCheckOut;
                productActivityPermissionM.IsCopyLink = productActivityPermission.IsCopyLink;
                productActivityPermissionM.IsViewHistory = productActivityPermission.IsViewHistory;
                productActivityPermissionM.IsMail = productActivityPermission.IsMail;
                productActivityPermissionM.IsNonCompliance = productActivityPermission.IsNonCompliance;
                productActivityPermissionM.IsSupportDocuments = productActivityPermission.IsSupportDocuments;
                productActivityPermissionM.IsUpdateStatus = productActivityPermission.IsUpdateStatus;
                productActivityPermissionM.IsViewFile = productActivityPermission.IsViewFile;
                productActivityPermissionM.ProductActivityCaseResponsDutyId = id;
              

            }
            wikiResponsible.ForEach(s =>
            {
                ProductActivityCaseResponsResponsibleModel wikiResponsibilityModel = new ProductActivityCaseResponsResponsibleModel
                {
                    WikiResponsibilityID = s.WikiResponsibilityId,
                    ProductActivityCaseResponsDutyId = s.ProductActivityCaseResponsDutyId,
                    EmployeeID = s.EmployeeId,
                    UserGroupID = s.UserGroupId,
                    UserID = emp.Where(e=>e.EmployeeId==s.EmployeeId).FirstOrDefault()?.UserId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Employee?.DepartmentNavigation?.Name,
                    DesignationName = s.Employee?.Designation?.Name,
                    HeadCount = s.Employee?.HeadCount,
                    PlantCompanyName = plantList.Where(p => p.PlantId == s.Employee?.PlantId).FirstOrDefault()?.PlantCode,
                    UserGroupName = usergroup != null && s.Employee != null ? (usergroup.Where(w => w.UserId == s.Employee.UserId && w.UserGroupId == s.UserGroupId).Select(g => g.UserGroup?.Name).FirstOrDefault()) : "",
                  
              
                productActivityPermissionModel = productActivityPermissionM,
            };
                
               
                wikiResponsibilityModels.Add(wikiResponsibilityModel);
            });

            return wikiResponsibilityModels;
        }
        [HttpPost]
        [Route("InsertApplicationWikiLineDuty")]
        public ProductActivityCaseResponsDutyModel InsertApplicationWikiLineDuty(ProductActivityCaseResponsDutyModel value)
        {

            ProductActivityCaseResponsModel templateTestCaseCheckListResponseModel = new ProductActivityCaseResponsModel();
            templateTestCaseCheckListResponseModel.ProductActivityCaseId = value.ProductActivityCaseId;
            
            templateTestCaseCheckListResponseModel.AddedByUserId = value.AddedByUserID;
            templateTestCaseCheckListResponseModel.AddedByUserID = value.AddedByUserID;
            templateTestCaseCheckListResponseModel.StatusCodeID = value.StatusCodeID;
            templateTestCaseCheckListResponseModel.StatusCodeId = value.StatusCodeID;
            long? productActvityCaseResponsId = null;
            
            var templateTestCaseCheckListResponseExit = _context.ProductActivityCaseRespons.FirstOrDefault(f => f.ProductActivityCaseId == value.ProductActivityCaseId && f.ProductActivityCaseResponsId == value.ProductActivityCaseResponsId)?.ProductActivityCaseResponsId;
            if (templateTestCaseCheckListResponseExit == null)
            {
                templateTestCaseCheckListResponseModel.ProductActivityCaseResponsId = -1;
                var productionActivityMaster = new ProductActivityCaseRespons
                {
                    ProductActivityCaseId = value.ProductActivityCaseId,
                    DutyId = templateTestCaseCheckListResponseModel.DutyId,
                    Responsibility = templateTestCaseCheckListResponseModel.Responsibility,
                    FunctionLink = templateTestCaseCheckListResponseModel.FunctionLink,
                    PageLink = templateTestCaseCheckListResponseModel.PageLink,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    StatusCodeId = templateTestCaseCheckListResponseModel.StatusCodeId.Value,
                    NotificationAdvice = templateTestCaseCheckListResponseModel.NotificationAdviceFlag == "Yes" ? true : false,
                    NotificationAdviceTypeId = templateTestCaseCheckListResponseModel.NotificationAdviceTypeId,
                    RepeatId = templateTestCaseCheckListResponseModel.RepeatId,
                    CustomId = templateTestCaseCheckListResponseModel.CustomId,
                    Monthly = templateTestCaseCheckListResponseModel.Monthly,
                    Yearly = templateTestCaseCheckListResponseModel.Yearly,
                    EventDescription = templateTestCaseCheckListResponseModel.EventDescription,
                    DaysOfWeek = templateTestCaseCheckListResponseModel.DaysOfWeek,
                    // SessionId = SessionId,
                    NotificationStatusId = templateTestCaseCheckListResponseModel.NotificationStatusId,
                    Title = templateTestCaseCheckListResponseModel.Title,
                    Message = templateTestCaseCheckListResponseModel.Message,
                    ScreenId = templateTestCaseCheckListResponseModel.ScreenId,
                    NotifyEndDate = templateTestCaseCheckListResponseModel.NotifyEndDate,
                    IsAllowDocAccess = templateTestCaseCheckListResponseModel.IsAllowDocAccess,
                };
                _context.ProductActivityCaseRespons.Add(productionActivityMaster);
                _context.SaveChanges();
                productActvityCaseResponsId = productionActivityMaster.ProductActivityCaseResponsId;
              
            }

            var templateTestCaseCheckListResponseDutyExit = _context.ProductActivityCaseResponsDuty.FirstOrDefault(f => f.ProductActivityCaseResponsId == productActvityCaseResponsId && f.CompanyId == value.CompanyId)?.ProductActivityCaseResponsDutyId;
            if (templateTestCaseCheckListResponseDutyExit == null)
            {
                var ApplicationWikiLineDuty = new ProductActivityCaseResponsDuty
                {
                    ProductActivityCaseResponsId = productActvityCaseResponsId,
                  
                    DepartmantId = value.DepartmentId,
                    DesignationId = value.DesignationId,
                    EmployeeId = value.EmployeeId,
                    CompanyId = value.CompanyId,
                    DesignationNumber = value.DesignationNumber,
                    DutyNo = value.DutyNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Type = value.Type,
                    Description = value.Description,
                };
                _context.ProductActivityCaseResponsDuty.Add(ApplicationWikiLineDuty);
                _context.SaveChanges();
                value.ProductActivityCaseResponsDutyId = ApplicationWikiLineDuty.ProductActivityCaseResponsDutyId;
            }
           
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
         
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {
                    var existing = _context.ProductActivityCaseResponsResponsible.Where(e => e.EmployeeId == s && e.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new ProductActivityCaseResponsResponsible
                        {
                            EmployeeId = s,
                            ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                        };
                        _context.ProductActivityCaseResponsResponsible.Add(wikiResponsible);
                        _context.SaveChanges();

                    }
                    if (value.productActivityPermissionModel != null)
                    {


                        if (value.productActivityPermissionModel.ProductActivityPermissionId > 0)
                        {
                            var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityPermissionId == value.productActivityPermissionModel.ProductActivityPermissionId);
                            // productActivityPermission.ProductActivityCaseId = value.ProductActivityCaseResponsId;
                            productActivityPermission.IsChecker = value.productActivityPermissionModel.IsChecker;
                            productActivityPermission.IsCheckOut = value.productActivityPermissionModel.IsCheckOut;
                            productActivityPermission.IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus;
                            productActivityPermission.IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo;
                            productActivityPermission.IsCopyLink = value.productActivityPermissionModel.IsCopyLink;
                            productActivityPermission.IsMail = value.productActivityPermissionModel.IsMail;
                            productActivityPermission.IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance;
                            productActivityPermission.IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments;
                            productActivityPermission.IsViewFile = value.productActivityPermissionModel.IsViewFile;
                            productActivityPermission.IsViewHistory = value.productActivityPermissionModel?.IsViewHistory;
                            productActivityPermission.UserId = s;
                            _context.SaveChanges();
                        }
                        else
                        {
                            var templateTestCaseLink = new ProductActivityPermission
                            {
                                ProductActivityCaseId = value.ProductActivityCaseResponsId,
                                ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                IsChecker = value.productActivityPermissionModel.IsChecker,
                                IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus,
                                IsCheckOut = value.productActivityPermissionModel.IsCheckOut,
                                IsMail = value.productActivityPermissionModel.IsMail,
                                IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo,
                                IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance,
                                IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments,
                                IsViewFile = value.productActivityPermissionModel.IsViewFile,

                                IsViewHistory = value.productActivityPermissionModel.IsViewHistory,
                                IsCopyLink = value.productActivityPermissionModel.IsCopyLink,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID,
                                UserId = s,

                            };
                            _context.ProductActivityPermission.Add(templateTestCaseLink);
                            _context.SaveChanges();

                            value.productActivityPermissionModel.ProductActivityPermissionId = templateTestCaseLink.ProductActivityPermissionId;
                        }
                    }
                });
            }

            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                    if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                    {
                        var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                        if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                        {
                            userGroupEmpIds.ForEach(emp =>
                            {
                                var existing = _context.ProductActivityCaseResponsResponsible.Where(e => e.EmployeeId == emp && e.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId).FirstOrDefault();
                                if (existing == null)
                                {
                                    var wikiResponsible = new ProductActivityCaseResponsResponsible
                                    {
                                        EmployeeId = emp,
                                        UserGroupId = s,
                                        ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                    };
                                    _context.ProductActivityCaseResponsResponsible.Add(wikiResponsible);
                                    _context.SaveChanges();
                                }
                                if (value.productActivityPermissionModel != null)
                                {


                                    var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.UserId == s && p.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId);
                                    if (productActivityPermission != null)
                                    {
                                       // var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityPermissionId == value.productActivityPermissionModel.ProductActivityPermissionId);
                                        // productActivityPermission.ProductActivityCaseId = value.ProductActivityCaseResponsId;
                                        productActivityPermission.IsChecker = value.productActivityPermissionModel.IsChecker;
                                        productActivityPermission.IsCheckOut = value.productActivityPermissionModel.IsCheckOut;
                                        productActivityPermission.IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus;
                                        productActivityPermission.IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo;
                                        productActivityPermission.IsCopyLink = value.productActivityPermissionModel.IsCopyLink;
                                        productActivityPermission.IsMail = value.productActivityPermissionModel.IsMail;
                                        productActivityPermission.IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance;
                                        productActivityPermission.IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments;
                                        productActivityPermission.IsViewFile = value.productActivityPermissionModel.IsViewFile;
                                        productActivityPermission.IsViewHistory = value.productActivityPermissionModel?.IsViewHistory;
                                        productActivityPermission.UserId = emp;
                                        productActivityPermission.UserGroupId = s;
                                        _context.SaveChanges();
                                    }
                                    else
                                    {
                                        var templateTestCaseLink = new ProductActivityPermission
                                        {
                                            ProductActivityCaseId = value.ProductActivityCaseResponsId,
                                            ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                            IsChecker = value.productActivityPermissionModel.IsChecker,
                                            IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus,
                                            IsCheckOut = value.productActivityPermissionModel.IsCheckOut,
                                            IsMail = value.productActivityPermissionModel.IsMail,
                                            IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo,
                                            IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance,
                                            IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments,
                                            IsViewFile = value.productActivityPermissionModel.IsViewFile,

                                            IsViewHistory = value.productActivityPermissionModel.IsViewHistory,
                                            IsCopyLink = value.productActivityPermissionModel.IsCopyLink,
                                            AddedByUserId = value.AddedByUserID,
                                            AddedDate = DateTime.Now,
                                            StatusCodeId = value.StatusCodeID,
                                            UserId = emp,
                                            UserGroupId = s,

                                        };
                                        _context.ProductActivityPermission.Add(templateTestCaseLink);
                                        _context.SaveChanges();

                                        value.productActivityPermissionModel.ProductActivityPermissionId = templateTestCaseLink.ProductActivityPermissionId;
                                    }
                                }
                            });
                        }
                    }
                });
            }

            return value;
        }
        [HttpPut]
        [Route("UpdateApplicationWikiLineDuty")]
        public ProductActivityCaseResponsDutyModel UpdateApplicationWikiLineDuty(ProductActivityCaseResponsDutyModel value)
        {
            var ApplicationWikiLineDuty = _context.ProductActivityCaseResponsDuty.SingleOrDefault(p => p.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId);
            //ApplicationWikiLineDuty.ProductActivityCaseResponsId = value.ProductActivityCaseResponsId;
            ApplicationWikiLineDuty.DepartmantId = value.DepartmentId;
            ApplicationWikiLineDuty.DesignationId = value.DesignationId;
            ApplicationWikiLineDuty.CompanyId = value.CompanyId;
            ApplicationWikiLineDuty.EmployeeId = value.EmployeeId;
            ApplicationWikiLineDuty.DesignationNumber = value.DesignationNumber;
            ApplicationWikiLineDuty.DutyNo = value.DutyNo;
            ApplicationWikiLineDuty.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLineDuty.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLineDuty.ModifiedDate = DateTime.Now;
            ApplicationWikiLineDuty.Type = value.Type;
            ApplicationWikiLineDuty.Description = value.Description;  
            var employeeList = _context.Employee.ToList();
            var userGroupList = _context.UserGroupUser.ToList();
            if (value.productActivityPermissionModel != null)        
            {
                              
            }
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.ProductActivityCaseResponsResponsible.Where(e => e.EmployeeId == s && e.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new ProductActivityCaseResponsResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i => i.UserGroupId).FirstOrDefault() : null,
                            ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                        };
                        _context.ProductActivityCaseResponsResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                    if (value.productActivityPermissionModel != null)
                    {

                        var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.UserId == s && p.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId);
                        if (productActivityPermission != null)
                        {
                            //var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityPermissionId == value.productActivityPermissionModel.ProductActivityPermissionId);
                            // productActivityPermission.ProductActivityCaseId = value.ProductActivityCaseResponsId;
                            productActivityPermission.IsChecker = value.productActivityPermissionModel.IsChecker;
                            productActivityPermission.IsCheckOut = value.productActivityPermissionModel.IsCheckOut;
                            productActivityPermission.IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus;
                            productActivityPermission.IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo;
                            productActivityPermission.IsCopyLink = value.productActivityPermissionModel.IsCopyLink;
                            productActivityPermission.IsMail = value.productActivityPermissionModel.IsMail;
                            productActivityPermission.IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance;
                            productActivityPermission.IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments;
                            productActivityPermission.IsViewFile = value.productActivityPermissionModel.IsViewFile;
                            productActivityPermission.IsViewHistory = value.productActivityPermissionModel?.IsViewHistory;
                            productActivityPermission.UserId = s;
                        }
                        else
                        {
                            var templateTestCaseLink = new ProductActivityPermission
                            {
                                ProductActivityCaseId = value.ProductActivityCaseResponsId,
                                ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                IsChecker = value.productActivityPermissionModel.IsChecker,
                                IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus,
                                IsCheckOut = value.productActivityPermissionModel.IsCheckOut,
                                IsMail = value.productActivityPermissionModel.IsMail,
                                IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo,
                                IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance,
                                IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments,
                                IsViewFile = value.productActivityPermissionModel.IsViewFile,

                                IsViewHistory = value.productActivityPermissionModel.IsViewHistory,
                                IsCopyLink = value.productActivityPermissionModel.IsCopyLink,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID,
                                UserId = s,

                            };
                            _context.ProductActivityPermission.Add(templateTestCaseLink);
                            _context.SaveChanges();

                            value.productActivityPermissionModel.ProductActivityPermissionId = templateTestCaseLink.ProductActivityPermissionId;
                        }
                    }


                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.ProductActivityCaseResponsResponsible.Where(e => e.EmployeeId == emp && e.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new ProductActivityCaseResponsResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                        };
                                        _context.ProductActivityCaseResponsResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                    if (value.productActivityPermissionModel != null)
                                    {

                                        var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.UserId == s && p.ProductActivityCaseResponsDutyId == value.ProductActivityCaseResponsDutyId);
                                        if (productActivityPermission != null)
                                        {
                                           // var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityPermissionId == value.productActivityPermissionModel.ProductActivityPermissionId);
                                            // productActivityPermission.ProductActivityCaseId = value.ProductActivityCaseResponsId;
                                            productActivityPermission.IsChecker = value.productActivityPermissionModel.IsChecker;
                                            productActivityPermission.IsCheckOut = value.productActivityPermissionModel.IsCheckOut;
                                            productActivityPermission.IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus;
                                            productActivityPermission.IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo;
                                            productActivityPermission.IsCopyLink = value.productActivityPermissionModel.IsCopyLink;
                                            productActivityPermission.IsMail = value.productActivityPermissionModel.IsMail;
                                            productActivityPermission.IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance;
                                            productActivityPermission.IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments;
                                            productActivityPermission.IsViewFile = value.productActivityPermissionModel.IsViewFile;
                                            productActivityPermission.IsViewHistory = value.productActivityPermissionModel?.IsViewHistory;
                                            productActivityPermission.UserId = emp;
                                            productActivityPermission.UserGroupId = s;
                                        }
                                        else
                                        {
                                            var templateTestCaseLink = new ProductActivityPermission
                                            {
                                                ProductActivityCaseId = value.ProductActivityCaseResponsId,
                                                ProductActivityCaseResponsDutyId = value.ProductActivityCaseResponsDutyId,
                                                IsChecker = value.productActivityPermissionModel.IsChecker,
                                                IsUpdateStatus = value.productActivityPermissionModel.IsUpdateStatus,
                                                IsCheckOut = value.productActivityPermissionModel.IsCheckOut,
                                                IsMail = value.productActivityPermissionModel.IsMail,
                                                IsActivityInfo = value.productActivityPermissionModel.IsActivityInfo,
                                                IsNonCompliance = value.productActivityPermissionModel.IsNonCompliance,
                                                IsSupportDocuments = value.productActivityPermissionModel.IsSupportDocuments,
                                                IsViewFile = value.productActivityPermissionModel.IsViewFile,

                                                IsViewHistory = value.productActivityPermissionModel.IsViewHistory,
                                                IsCopyLink = value.productActivityPermissionModel.IsCopyLink,
                                                AddedByUserId = value.AddedByUserID,
                                                AddedDate = DateTime.Now,
                                                StatusCodeId = value.StatusCodeID,
                                                UserId = emp,
                                                UserGroupId = s,

                                            };
                                            _context.ProductActivityPermission.Add(templateTestCaseLink);
                                            _context.SaveChanges();

                                            value.productActivityPermissionModel.ProductActivityPermissionId = templateTestCaseLink.ProductActivityPermissionId;
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLineDuty")]
        public void DeleteApplicationWikiLineDuty(int id)
        {
            var ApplicationWikiLineDuty = _context.ProductActivityCaseResponsDuty.SingleOrDefault(p => p.ProductActivityCaseResponsDutyId == id);
            var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityCaseResponsDutyId == id);
            if (productActivityPermission != null)
            {
                _context.ProductActivityPermission.Remove(productActivityPermission);
                _context.SaveChanges();
            }


            var wikiresponsible = _context.ProductActivityCaseResponsResponsible.Where(p => p.ProductActivityCaseResponsDutyId == id).ToList();
            if (wikiresponsible != null)
            {
                _context.ProductActivityCaseResponsResponsible.RemoveRange(wikiresponsible);
                _context.SaveChanges();
            }
            _context.ProductActivityCaseResponsDuty.Remove(ApplicationWikiLineDuty);
            _context.SaveChanges();
            if (ApplicationWikiLineDuty.ProductActivityCaseResponsId != null)
            {
                var wikirespon = _context.ProductActivityCaseRespons.Where(p => p.ProductActivityCaseResponsId == ApplicationWikiLineDuty.ProductActivityCaseResponsId).FirstOrDefault();
                if (wikirespon != null)
                {
                    _context.ProductActivityCaseRespons.Remove(wikirespon);
                    _context.SaveChanges();
                }
            }
        
        }
        [HttpPut]
        [Route("DeleteWikiResponsible")]
        public void DeleteWikiResponsible(ProductActivityCaseResponsResponsibleModel productActivityCaseResponsResponsibleModel)
        {
           
          
            var wikiresponsible = _context.ProductActivityCaseResponsResponsible.SingleOrDefault(p => p.WikiResponsibilityId == productActivityCaseResponsResponsibleModel.WikiResponsibilityID);
            if (wikiresponsible != null)
            {
                var productActivityPermission = _context.ProductActivityPermission.Where(p => p.ProductActivityCaseResponsDutyId == productActivityCaseResponsResponsibleModel.ProductActivityCaseResponsDutyId && p.UserId == productActivityCaseResponsResponsibleModel.UserID).FirstOrDefault();
                if (productActivityPermission != null)
                {
                    _context.ProductActivityPermission.Remove(productActivityPermission);
                    _context.SaveChanges();
                }
                _context.ProductActivityCaseResponsResponsible.Remove(wikiresponsible);
                _context.SaveChanges();
            }
          
        }
        [HttpPut]
        [Route("DeleteWikiResponsibles")]
        public ProductActivityCaseResponsResponsibleModel DeleteWikiResponsibles(ProductActivityCaseResponsResponsibleModel value)
        {
            var selectDocumentRoleIds = value.WikiResponsibilityIDs;
            if (value.WikiResponsibilityIDs.Count > 0)
            {
                value.WikiResponsibilityIDs.ForEach(sel =>
                {
                    var wikiresponsible = _context.ProductActivityCaseResponsResponsible.Where(p => p.WikiResponsibilityId == sel).FirstOrDefault();
                    if (wikiresponsible != null)
                    {


                        _context.ProductActivityCaseResponsResponsible.Remove(wikiresponsible);
                        _context.SaveChanges();

                    }
                });

                _context.SaveChanges();
            }
            return value;
        }
        #endregion

        [HttpPut]
        [Route("CreateVersion")]
        public async Task<ProductActivityCaseModel> CreateVersion(ProductActivityCaseModel value)
        {
            try
            {
                value.SessionId = value.SessionId.Value;
                var versionData = _context.ProductActivityCase.Include(s => s.ProductActivityCaseLine).Include(s=>s.ProductActivityCaseRespons).Include("ProductActivityCaseRespons.ProductActivityCaseResponsDuty").SingleOrDefault(p => p.ProductActivityCaseId == value.ProductActivityCaseId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<ProductActivityCaseModel>(versionData);
                    verObject.ProductActivityCaseLineModels = new List<ProductActivityCaseLineModel>();
                    verObject.ProductActivityCaseResponsDutyModels = new List<ProductActivityCaseResponsDutyModel>();
                    verObject.ProductActivityCaseResponsModels = new List<ProductActivityCaseResponsModel>();
                    versionData.ProductActivityCaseLine.ToList().ForEach(f =>
                    {
                        var priceInfo = _mapper.Map<ProductActivityCaseLineModel>(f);
                        verObject.ProductActivityCaseLineModels.Add(priceInfo);
                    });
                    versionData.ProductActivityCaseRespons.ToList().ForEach(f =>
                    {
                        var priceInfo = _mapper.Map<ProductActivityCaseResponsModel>(f);
                        verObject.ProductActivityCaseResponsModels.Add(priceInfo);
                    });
                 
                    var versionInfo = new TableDataVersionInfoModel<ProductActivityCaseModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "ProductActivityCase",
                        PrimaryKey = value.ProductActivityCaseId,
                        ReferenceInfo = value.ReferenceInfo,
                        StatusCodeID = value.StatusCodeID.Value,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }
        [HttpPut]
        [Route("CheckExitDataVersion")]
        public bool CheckExitDataVersion(ProductActivityCaseModel value)
        {

            return false;
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<ProductActivityCaseModel> ApplyVersion(ProductActivityCaseModel value)
        {
            try
            {
                var versionData = _context.ProductActivityCase.Include(q => q.ProductActivityCaseLine).SingleOrDefault(p => p.ProductActivityCaseId == value.ProductActivityCaseId);

                if (versionData != null)
                {
                    
                    versionData.Instruction = value.Instruction;
                    versionData.Reference = value.Reference;
                    versionData.CheckerNotes = value.CheckerNotes;
                    versionData.Upload = value.Upload;
                    versionData.ImplementationDate = value.ImplementationDate;
                    versionData.IsLanguage = value.IsLanguage;
                    versionData.ReferenceMalay = value.ReferenceMalay;
                    versionData.CheckerNotesMalay = value.CheckerNotesMalay;  
                    versionData.UploadMalay= value.UploadMalay;
                    versionData.ReferenceChinese = value.ReferenceChinese;
                    versionData.CheckerNotesChinese = value.CheckerNotesChinese;
                    versionData.UploadChinese = value.UploadChinese;
                    versionData.VersionNo = value.VersionNo;    
                    versionData.IsEnglishLanguage = value.IsEnglishLanguage;    
                    versionData.IsMalayLanguage = value.IsMalayLanguage;
                    versionData.IsChineseLanguage = value.IsChineseLanguage;
                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.SessionId = value.SessionId;

                    _context.ProductActivityCaseLine.RemoveRange(versionData.ProductActivityCaseLine);
                    _context.SaveChanges();
                    if (value.ProductActivityCaseLineModels != null)
                    {
                        value.ProductActivityCaseLineModels.ForEach(f =>
                        {
                            var quatationawardLine = new ProductActivityCaseLine
                            {
                                ProductActivityCaseId = versionData.ProductActivityCaseId,
                                ProductActivityCaseLineId = f.ProductActivityCaseLineId,
                                NameOfTemplate = f.NameOfTemplate,                              
                                Subject = f.Subject,
                                Link = f.Link,
                                LocationName = f.LocationName,
                                Naming = f.Naming,
                                IsAutoNumbering = f.IsAutoNumbering,
                                DocumentNo = f.DocumentNo,
                                SessionId = f.SessionId,
                                TopicId = f.TopicId,
                                StatusCodeId = f.StatusCodeID.Value,
                                AddedByUserId = value.ModifiedByUserID,
                                AddedDate = DateTime.Now,
                                ModifiedByUserId = value.ModifiedByUserID,
                                ModifiedDate = DateTime.Now,

                            };

                            _context.ProductActivityCaseLine.Add(quatationawardLine);
                        });
                    }
                    if (value.ProductActivityCaseResponsDutyModels != null)
                    {
                        value.ProductActivityCaseResponsDutyModels.ForEach(f =>
                        {
                            var ApplicationWikiLineDuty = new ProductActivityCaseResponsDuty
                            {
                                ProductActivityCaseResponsId =f.ProductActivityCaseResponsId ,
                                DepartmantId = f.DepartmentId,
                                DesignationId = f.DesignationId,
                                EmployeeId = f.EmployeeId,
                                CompanyId = value.CompanyId,
                                DesignationNumber = f.DesignationNumber,
                                DutyNo = f.DutyNo,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID.Value,
                                Type = f.Type,
                                Description = f.Description,
                            };
                            _context.ProductActivityCaseResponsDuty.Add(ApplicationWikiLineDuty);
                        });
                    }
                    if (value.ProductActivityCaseResponsModels != null)
                    {
                        value.ProductActivityCaseResponsModels.ForEach(f =>
                        {
                            var productionActivityMaster = new ProductActivityCaseRespons
                            {
                                ProductActivityCaseId = f.ProductActivityCaseId,
                                DutyId = f.DutyId,
                                Responsibility = f.Responsibility,
                                FunctionLink = f.FunctionLink,
                                PageLink = f.PageLink,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                ModifiedByUserId = value.AddedByUserID,
                                ModifiedDate = DateTime.Now,
                                StatusCodeId = f.StatusCodeId.Value,
                                NotificationAdvice = f.NotificationAdviceFlag == "Yes" ? true : false,
                                NotificationAdviceTypeId = f.NotificationAdviceTypeId,
                                RepeatId = f.RepeatId,
                                CustomId = f.CustomId,
                                DueDate = f.DueDate == null ? DateTime.Now : f.DueDate,
                                Monthly = f.Monthly,
                                Yearly = f.Yearly,
                                EventDescription = f.EventDescription,
                                DaysOfWeek = f.DaysOfWeek,
                                //SessionId = SessionId,
                                NotificationStatusId = f.NotificationStatusId,
                                Title = f.Title,
                                Message = f.Message,
                                ScreenId = "ByLine",
                                NotifyEndDate = f.NotifyEndDate,
                                IsAllowDocAccess = f.IsAllowDocAccess,
                            };
                            _context.ProductActivityCaseRespons.Add(productionActivityMaster);
                        });
                    }
                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(ProductActivityCaseModel value)
        {

            var versionData = _context.ProductActivityCase.Include(s => s.ProductActivityCaseLine).SingleOrDefault(p => p.ProductActivityCaseId == value.ProductActivityCaseId);
            if (versionData != null)
            {
                var verObject = _mapper.Map<ProductActivityCaseModel>(versionData);
                verObject.ProductActivityCaseLineModels = new List<ProductActivityCaseLineModel>();
                versionData.ProductActivityCaseLine.ToList().ForEach(f =>
                {
                    var priceInfo = _mapper.Map<ProductActivityCaseLineModel>(f);
                    verObject.ProductActivityCaseLineModels.Add(priceInfo);
                });
                versionData.ProductActivityCaseRespons.ToList().ForEach(f =>
                {
                    var priceInfo = _mapper.Map<ProductActivityCaseResponsModel>(f);
                    verObject.ProductActivityCaseResponsModels.Add(priceInfo);
                });
              
                var versionInfo = new TableDataVersionInfoModel<ProductActivityCaseModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ProductActivityCase",
                    PrimaryKey = value.ProductActivityCaseId,
                    ReferenceInfo = "Temp Version - undo",
                    StatusCodeID = value.StatusCodeID.Value,
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }

        [HttpGet]
        [Route("UndoVersion")]
        public async Task<ProductActivityCaseModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<ProductActivityCaseModel>(tableData.JsonData);

                    var versionData = _context.ProductActivityCase.Include(m => m.ProductActivityCaseLine).SingleOrDefault(p => p.ProductActivityCaseId == verObject.ProductActivityCaseId);


                    if (verObject != null && versionData != null)
                    {
                        versionData.Instruction = verObject.Instruction;
                        versionData.Reference = verObject.Reference;
                        versionData.CheckerNotes = verObject.CheckerNotes;
                        versionData.Upload = verObject.Upload;
                        versionData.ImplementationDate = verObject.ImplementationDate;
                        versionData.IsLanguage = verObject.IsLanguage;
                        versionData.ReferenceMalay = verObject.ReferenceMalay;
                        versionData.CheckerNotesMalay = verObject.CheckerNotesMalay;
                        versionData.UploadMalay = verObject.UploadMalay;
                        versionData.ReferenceChinese = verObject.ReferenceChinese;
                        versionData.CheckerNotesChinese = verObject.CheckerNotesChinese;
                        versionData.UploadChinese = verObject.UploadChinese;
                        versionData.VersionNo = verObject.VersionNo;
                        versionData.IsEnglishLanguage = verObject.IsEnglishLanguage;
                        versionData.IsMalayLanguage = verObject.IsMalayLanguage;
                        versionData.IsChineseLanguage = verObject.IsChineseLanguage;
                        versionData.StatusCodeId = verObject.StatusCodeID.Value;
                        versionData.ModifiedByUserId = verObject.ModifiedByUserID;
                        versionData.ModifiedDate = DateTime.Now;
                        versionData.SessionId = verObject.SessionId;

                        _context.ProductActivityCaseLine.RemoveRange(versionData.ProductActivityCaseLine);
                        _context.SaveChanges();
                        if (verObject.ProductActivityCaseLineModels != null)
                        {
                            verObject.ProductActivityCaseLineModels.ForEach(f =>
                            {
                                var ProductActivityline = new ProductActivityCaseLine
                                {
                                    ProductActivityCaseId = versionData.ProductActivityCaseId,
                                    NameOfTemplate = f.NameOfTemplate,
                                    ProductActivityCaseLineId = f.ProductActivityCaseLineId,
                                    Subject = f.Subject,
                                    Link = f.Link,
                                    LocationName = f.LocationName,
                                    Naming  =   f.Naming,   
                                    IsAutoNumbering = f.IsAutoNumbering,    
                                    DocumentNo = f.DocumentNo,  
                                    SessionId = f.SessionId,    
                                    TopicId = f.TopicId,
                                    StatusCodeId = f.StatusCodeID.Value,
                                    AddedByUserId = f.ModifiedByUserID,
                                    AddedDate = DateTime.Now,
                                    ModifiedByUserId = f.ModifiedByUserID,
                                    ModifiedDate = DateTime.Now,

                                };

                                _context.ProductActivityCaseLine.Add(ProductActivityline);

                            });
                        }
                        _context.SaveChanges();
                    }
                    return verObject;
                }
                return new ProductActivityCaseModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                var TempVersion = _context.TempVersion.Where(w => w.VersionId == tableData.VersionTableId).ToList();
                if (TempVersion != null)
                {
                    _context.TempVersion.RemoveRange(TempVersion);
                    _context.SaveChanges();
                }
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }

        [HttpGet]
        [Route("GetProductActivityPermissionById")]
        public ProductActivityPermissionModel GetProductActivityPermissionById(int id)
        {
           // List<ProductActivityCasePermissionModel> templateTestCaseLinkModels = new List<ProductActivityCaseLineModel>();
            var productActivityPermission = _context.ProductActivityPermission.Where(w => w.ProductActivityCaseId == id).FirstOrDefault();
            ProductActivityPermissionModel templateTestCaseModel = new ProductActivityPermissionModel();
            if (productActivityPermission != null)
            {
               
                templateTestCaseModel.ProductActivityPermissionId = productActivityPermission.ProductActivityPermissionId;
                templateTestCaseModel.ProductActivityCaseId = productActivityPermission.ProductActivityCaseId;
                templateTestCaseModel.IsChecker = productActivityPermission.IsChecker;
                templateTestCaseModel.IsCheckOut = productActivityPermission.IsCheckOut;
                templateTestCaseModel.IsUpdateStatus = productActivityPermission.IsUpdateStatus;

            }
               
            
            return templateTestCaseModel;
        }

        [HttpPost]
        [Route("GetProductActivityPermissionByUserId")]
        public ProductActivityPermissionModel GetProductActivityPermissionByUserId(ProductActivityCaseResponsResponsibleModel productActivityCaseResponsResponsibleModel)
        {
            var appuser = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,

            }).ToList();
            // List<ProductActivityCasePermissionModel> templateTestCaseLinkModels = new List<ProductActivityCaseLineModel>();
            var productActivityPermission = _context.ProductActivityPermission.Where(w => w.UserId == productActivityCaseResponsResponsibleModel.UserID && w.ProductActivityCaseResponsDutyId == productActivityCaseResponsResponsibleModel.ProductActivityCaseResponsDutyId).FirstOrDefault();
            ProductActivityPermissionModel templateTestCaseModel = new ProductActivityPermissionModel();
            if (productActivityPermission != null)
            {

                templateTestCaseModel.ProductActivityPermissionId = productActivityPermission.ProductActivityPermissionId;
                templateTestCaseModel.ProductActivityCaseId = productActivityPermission.ProductActivityCaseId;
                templateTestCaseModel.IsChecker = productActivityPermission.IsChecker;
                templateTestCaseModel.IsCheckOut = productActivityPermission.IsCheckOut;
                templateTestCaseModel.IsUpdateStatus = productActivityPermission.IsUpdateStatus;
                templateTestCaseModel.IsActivityInfo = productActivityPermission.IsActivityInfo;
                templateTestCaseModel.IsCopyLink = productActivityPermission.IsCopyLink;
                templateTestCaseModel.IsViewHistory = productActivityPermission.IsViewHistory;  
                templateTestCaseModel.IsMail =  productActivityPermission.IsMail;
                templateTestCaseModel.IsSupportDocuments = productActivityPermission.IsSupportDocuments;    
                templateTestCaseModel.IsViewFile = productActivityPermission?.IsViewFile;   
                templateTestCaseModel.IsNonCompliance = productActivityPermission?.IsNonCompliance;
                templateTestCaseModel.UserID = productActivityPermission?.UserId;
                templateTestCaseModel.UserGroupID = productActivityPermission.UserGroupId;
                templateTestCaseModel.UserName = appuser.Where(a => a.UserId == productActivityPermission?.UserId).FirstOrDefault()?.UserName;


            }


            return templateTestCaseModel;
        }
        [HttpPost]
        [Route("InsertProductActivityPermission")]
        public ProductActivityPermissionModel InsertProductActivityPermission(ProductActivityPermissionModel value)
        {
           
            var templateTestCaseLink = new ProductActivityPermission
            {
                ProductActivityCaseId = value.ProductActivityCaseId,
              
                IsChecker = value.IsChecker,
                IsUpdateStatus = value.IsUpdateStatus,
                IsCheckOut = value.IsCheckOut,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
              
            };
            _context.ProductActivityPermission.Add(templateTestCaseLink);
            _context.SaveChanges();
            value.ProductActivityPermissionId = templateTestCaseLink.ProductActivityPermissionId;
            value.SessionId = value.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityPermission")]
        public ProductActivityPermissionModel UpdateProductActivityPermission(ProductActivityPermissionModel value)
        {
          
            var productActivityPermission = _context.ProductActivityPermission.SingleOrDefault(p => p.ProductActivityPermissionId == value.ProductActivityPermissionId);
            productActivityPermission.ProductActivityCaseId = value.ProductActivityCaseId;
            productActivityPermission.IsChecker = value.IsChecker;
            productActivityPermission.IsCheckOut = value.IsCheckOut;
            productActivityPermission.IsUpdateStatus = value.IsUpdateStatus;
            productActivityPermission.IsActivityInfo = value.IsActivityInfo;
            productActivityPermission.IsCopyLink = value.IsCopyLink;
            productActivityPermission.IsViewHistory = value.IsViewHistory;
            productActivityPermission.IsViewFile = value.IsViewFile;
            productActivityPermission.IsMail = value.IsMail;
            productActivityPermission.IsSupportDocuments = value.IsSupportDocuments;
            productActivityPermission.IsNonCompliance = value.IsNonCompliance;
                      _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("GetResponsibilityUsersExcel")]
        public IActionResult GetResponsibilityUsersExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Company");
            headers.Add("User Group");
            headers.Add("Employee");
            headers.Add("Department");
            headers.Add("Designation");
            headers.Add("Designation HeadCount");
            headers.Add("Checker");
            headers.Add("UpdateStatus");
            headers.Add("Mail");
            headers.Add("Non Compliance");
            headers.Add("Activity Info");
          
           

            List<ProductActivityCaseResponsResponsibleModel> wikiResponsibilityModels = new List<ProductActivityCaseResponsResponsibleModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var plantList = _context.Plant.ToList();
            var appuser = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,

            }).ToList();
            var emp = _context.Employee.Select(s => new
            {
                s.EmployeeId,
                s.UserId,

            }).ToList();
            var usergroup = _context.UserGroupUser.Include(u => u.UserGroup).ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.Where(p => p.ProductActivityCaseResponsDutyId == searchModel.Id).ToList();
            var wikiResponsible = _context.ProductActivityCaseResponsResponsible

                                .Include(o => o.Employee)
                                .Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.User)
                                .Where(s => s.ProductActivityCaseResponsDutyId == searchModel.Id).AsNoTracking().ToList();
            ProductActivityPermissionModel productActivityPermissionM = new ProductActivityPermissionModel();
            var userIds = wikiResponsible.Select(s => s.UserId).ToList();
            var productActivityPermission = productActivityPermissionList.Where(w => w.ProductActivityCaseResponsDutyId == searchModel.Id).FirstOrDefault();
            if (productActivityPermission != null)
            {

                productActivityPermissionM.ProductActivityPermissionId = productActivityPermission.ProductActivityPermissionId;
                productActivityPermissionM.IsActivityInfo = productActivityPermission.IsActivityInfo;
                productActivityPermissionM.IsChecker = productActivityPermission.IsChecker;
                productActivityPermissionM.IsCheckOut = productActivityPermission.IsCheckOut;
                productActivityPermissionM.IsCopyLink = productActivityPermission.IsCopyLink;
                productActivityPermissionM.IsViewHistory = productActivityPermission.IsViewHistory;
                productActivityPermissionM.IsMail = productActivityPermission.IsMail;
                productActivityPermissionM.IsNonCompliance = productActivityPermission.IsNonCompliance;
                productActivityPermissionM.IsSupportDocuments = productActivityPermission.IsSupportDocuments;
                productActivityPermissionM.IsUpdateStatus = productActivityPermission.IsUpdateStatus;
                productActivityPermissionM.IsViewFile = productActivityPermission.IsViewFile;
                productActivityPermissionM.ProductActivityCaseResponsDutyId = searchModel.Id;


            }
            wikiResponsible.ForEach(s =>
            {
                ProductActivityCaseResponsResponsibleModel wikiResponsibilityModel = new ProductActivityCaseResponsResponsibleModel();
                
                    wikiResponsibilityModel.WikiResponsibilityID = s.WikiResponsibilityId;
                wikiResponsibilityModel.ProductActivityCaseResponsDutyId = s.ProductActivityCaseResponsDutyId;
                wikiResponsibilityModel.EmployeeID = s.EmployeeId;
                wikiResponsibilityModel.UserGroupID = s.UserGroupId;
                wikiResponsibilityModel.UserID = emp.Where(e => e.EmployeeId == s.EmployeeId).FirstOrDefault()?.UserId;
                wikiResponsibilityModel.EmployeeName = s.Employee?.FirstName;
                wikiResponsibilityModel.DepartmentName = s.Employee?.DepartmentNavigation?.Name;
                wikiResponsibilityModel.DesignationName = s.Employee?.Designation?.Name;
                wikiResponsibilityModel.HeadCount = s.Employee?.HeadCount;
                wikiResponsibilityModel.PlantCompanyName = plantList.Where(p => p.PlantId == s.Employee?.PlantId).FirstOrDefault()?.PlantCode;
                wikiResponsibilityModel.UserGroupName = usergroup != null && s.Employee != null ? (usergroup.Where(w => w.UserId == s.Employee.UserId && w.UserGroupId == s.UserGroupId).Select(g => g.UserGroup?.Name).FirstOrDefault()) : "";
                    // PermissionList = string.Join(",", productActivityPermissionList.Where(p => p.ProductActivityCaseResponsDutyId == searchModel.Id && p.UserId == s.UserId)?.Select(s=>s.IsChecker == true),  "Checker" :  "", 

                    var permissionlist = productActivityPermissionList.FirstOrDefault(d => d.UserId == s.EmployeeId);
                if (permissionlist != null)
                {
                    wikiResponsibilityModel.PermissionList = new List<string>();
                    if (permissionlist.IsChecker == true)
                    {
                        wikiResponsibilityModel.PermissionList.Add("Yes");
                    }
                    else
                    {
                        wikiResponsibilityModel.PermissionList.Add("No");
                    }
                  
                    if (permissionlist.IsUpdateStatus == true)
                    {
                        wikiResponsibilityModel.PermissionList.Add("Yes");
                    }
                    else
                    {
                        wikiResponsibilityModel.PermissionList.Add("No");
                    }
                   
                    if (permissionlist.IsMail == true)
                    {
                        wikiResponsibilityModel.PermissionList.Add("Yes");
                    }
                    else
                    {
                        wikiResponsibilityModel.PermissionList.Add("No");
                    }
                   
                    if (permissionlist.IsNonCompliance == true)
                    {
                        wikiResponsibilityModel.PermissionList.Add("Yes");
                    }
                    else
                    {
                        wikiResponsibilityModel.PermissionList.Add("No");
                    }
                  
                    if (permissionlist.IsActivityInfo == true)
                    {
                        wikiResponsibilityModel.PermissionList.Add("Yes");
                    }
                    else
                    {
                        wikiResponsibilityModel.PermissionList.Add("No");
                    }
                    if (wikiResponsibilityModel.PermissionList != null && wikiResponsibilityModel.PermissionList.Count > 0)
                    {
                        wikiResponsibilityModel.Permissions = string.Join(",  ", wikiResponsibilityModel.PermissionList);
                    }
                   
                }
                
                wikiResponsibilityModels.Add(wikiResponsibilityModel);
            });
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            string FromLocation = folderName + @"\" + newFolderName + @"\" + searchModel.UserID + ".xlsx";
            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                int j = 1;

                List<string> headersBy = new List<string>();
                headersBy.AddRange(headers);
                List<string> Permissionheaders = new List<string>();
               

                List<string> PermissionheadersBy = new List<string>();
                PermissionheadersBy.AddRange(Permissionheaders);
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                var sheetName = "Responsibility Users";
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = sheetName };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);

                Row headerRow = new Row();
                foreach (string column in headersBy)
                {
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                if (wikiResponsibilityModels != null && wikiResponsibilityModels.Count > 0)
                {
                    wikiResponsibilityModels.ForEach(s =>
                    {
                        Row row = new Row();
                        Cell companyNameCell = new Cell();
                        companyNameCell.DataType = CellValues.String;
                        companyNameCell.CellValue = new CellValue(s.PlantCompanyName?.ToString());
                        row.AppendChild(companyNameCell);

                        Cell userGroupName = new Cell();
                        userGroupName.DataType = CellValues.String;
                        userGroupName.CellValue = new CellValue(s.UserGroupName?.ToString());
                        row.AppendChild(userGroupName);

                        Cell employeenameCell = new Cell();
                        employeenameCell.DataType = CellValues.String;
                        employeenameCell.CellValue = new CellValue(s.EmployeeName.ToString());
                        row.AppendChild(employeenameCell);

                        Cell department = new Cell();
                        department.DataType = CellValues.String;
                        department.CellValue = new CellValue(s.DepartmentName?.ToString());
                        row.AppendChild(department);

                        Cell designationName = new Cell();
                        designationName.DataType = CellValues.String;
                        designationName.CellValue = new CellValue(s.DesignationName?.ToString());
                        row.AppendChild(designationName);                      

                       
                        Cell headcount = new Cell();
                        headcount.DataType = CellValues.String;
                        headcount.CellValue = new CellValue(s.HeadCount.ToString());
                        row.AppendChild(headcount);


                        headersBy.AddRange(Permissionheaders);

                        s.PermissionList.ForEach(p =>
                        {
                            //Row row1 = new Row();

                            // row1.AppendChild(permissionList);
                            Cell permissionList = new Cell();
                            permissionList.DataType = CellValues.String;
                            permissionList.CellValue = new CellValue(p);
                            row.AppendChild(permissionList);

                        });
                       






                        sheetData.AppendChild(row);
                    });
                }
                j++;
                sheetId++;
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }

    }
}
