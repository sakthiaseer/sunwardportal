﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MethodTemplateRoutineLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MethodTemplateRoutineLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetMethodTemplateRoutineLine")]
        public List<MethodTemplateRoutineLineModel> Get(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var icmasterOperation = _context.IcmasterOperation.AsNoTracking().ToList();
            List<MethodTemplateRoutineLineModel> methodTemplateRoutineLineModels = new List<MethodTemplateRoutineLineModel>();
            var methodTemplateRoutineLine = _context.MethodTemplateRoutineLine
                                                .Include("AddedByUser").Include("ModifiedByUser")
                                                .Include(s => s.StatusCode).Where(l => l.MethodTemplateRoutineId == id)
                                                .OrderByDescending(o => o.MethodTemplateRoutineLineId).AsNoTracking().ToList();
            if(methodTemplateRoutineLine!=null && methodTemplateRoutineLine.Count > 0)
            {
                methodTemplateRoutineLine.ForEach(s =>
                {
                    MethodTemplateRoutineLineModel methodTemplateRoutineLineModel = new MethodTemplateRoutineLineModel
                    {
                        MethodTemplateRoutineLineId = s.MethodTemplateRoutineLineId,
                        MethodTemplateRoutineId = s.MethodTemplateRoutineId,
                        DetermineFactorId = s.DetermineFactorId,
                        FixTimingorMin = s.FixTimingorMin,
                        No = s.No,
                        OperationId = s.OperationId,
                        ProcesssStepId = s.ProcesssStepId,
                        DetermineFactorName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DetermineFactorId).Select(a => a.Value).SingleOrDefault() : "",
                        ProcesssStepName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ProcesssStepId).Select(a => a.Value).SingleOrDefault() : "",
                        OperationName = icmasterOperation != null && s.OperationId != null ? icmasterOperation.Where(l => l.IcmasterOperationId == s.OperationId).Select(a => a.MasterOperation).SingleOrDefault() : "",
                        
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate

                    };
                    methodTemplateRoutineLineModels.Add(methodTemplateRoutineLineModel);
                });
            }
            return methodTemplateRoutineLineModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        /*[HttpGet("GetMethodTemplateRoutineLine/{id:int}")]
        public ActionResult<MethodTemplateRoutineLineModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.SingleOrDefault(p => p.MethodTemplateRoutineLineId == id.Value);
            var result = _mapper.Map<MethodTemplateRoutineLineModel>(methodTemplateRoutineLine);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }*/
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MethodTemplateRoutineLineModel> GetData(SearchModel searchModel)
        {
            var methodTemplateRoutineLine = new MethodTemplateRoutineLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).FirstOrDefault();
                        break;
                    case "Last":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).LastOrDefault();
                        break;
                    case "Next":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).LastOrDefault();
                        break;
                    case "Previous":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).FirstOrDefault();
                        break;
                    case "Last":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).LastOrDefault();
                        break;
                    case "Next":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderBy(o => o.MethodTemplateRoutineLineId).FirstOrDefault(s => s.MethodTemplateRoutineLineId > searchModel.Id);
                        break;
                    case "Previous":
                        methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.OrderByDescending(o => o.MethodTemplateRoutineLineId).FirstOrDefault(s => s.MethodTemplateRoutineLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MethodTemplateRoutineLineModel>(methodTemplateRoutineLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertMethodTemplateRoutineLine")]
        public MethodTemplateRoutineLineModel Post(MethodTemplateRoutineLineModel value)
        {
            var methodTemplateRoutineLine = new MethodTemplateRoutineLine
            {
                //MethodTemplateRoutineLineId = value.MethodTemplateRoutineLineId,
                MethodTemplateRoutineId = value.MethodTemplateRoutineId,
                DetermineFactorId = value.DetermineFactorId,
                FixTimingorMin = value.FixTimingorMin,
                No = value.No,
                OperationId = value.OperationId,
                ProcesssStepId = value.ProcesssStepId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.MethodTemplateRoutineLine.Add(methodTemplateRoutineLine);
            _context.SaveChanges();
            value.MethodTemplateRoutineLineId = methodTemplateRoutineLine.MethodTemplateRoutineLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMethodTemplateRoutineLine")]
        public MethodTemplateRoutineLineModel Put(MethodTemplateRoutineLineModel value)
        {
            var methodTemplateRoutineLineModel = _context.MethodTemplateRoutineLine.SingleOrDefault(p => p.MethodTemplateRoutineLineId == value.MethodTemplateRoutineLineId);

            methodTemplateRoutineLineModel.DetermineFactorId = value.DetermineFactorId;
            methodTemplateRoutineLineModel.FixTimingorMin = value.FixTimingorMin;
            methodTemplateRoutineLineModel.No = value.No;
            methodTemplateRoutineLineModel.OperationId = value.OperationId;
            methodTemplateRoutineLineModel.ProcesssStepId = value.ProcesssStepId;
            methodTemplateRoutineLineModel.ModifiedByUserId = value.ModifiedByUserID;
            methodTemplateRoutineLineModel.ModifiedDate = DateTime.Now;
            methodTemplateRoutineLineModel.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMethodTemplateRoutineLine")]
        public void Delete(int id)
        {
            var methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.SingleOrDefault(p => p.MethodTemplateRoutineLineId == id);
            if (methodTemplateRoutineLine != null)
            {
                _context.MethodTemplateRoutineLine.Remove(methodTemplateRoutineLine);
                _context.SaveChanges();
            }
        }
    }
}