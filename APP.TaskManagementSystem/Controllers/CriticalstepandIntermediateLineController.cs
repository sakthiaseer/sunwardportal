﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CriticalstepandIntermediateLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CriticalstepandIntermediateLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCriticalstepandIntermediateLines")]
        public List<CriticalstepandIntermediateLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine
                                                    .Include(a => a.AddedByUser).Include(m => m.ModifiedByUser)
                                                    .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CriticalstepandIntermediateLineModel> criticalstepandIntermediateLineModel = new List<CriticalstepandIntermediateLineModel>();
            criticalstepandIntermediateLine.ForEach(s =>
            {
                CriticalstepandIntermediateLineModel criticalstepandIntermediateLineModels = new CriticalstepandIntermediateLineModel
                {
                    CriticalstepandIntermediateLineID = s.CriticalstepandIntermediateLineId,
                    CriticalstepandIntermediateID = s.CriticalstepandIntermediateId,
                    TestID = s.TestId,
                    TestStageID = s.TestStageId,
                    SamplingFrequencyIDs = s.CriticalSamplingFrequency.Where(t => t.CriticalstepandIntermediateLineId == s.CriticalstepandIntermediateLineId).Select(t => t.SamplingFrequencyId).ToList(),
                    SamplingFrequncyList = s.CriticalSamplingFrequency.Where(t => t.CriticalstepandIntermediateLineId == s.CriticalstepandIntermediateLineId).Select(c =>
                        new CriticalSamplingFrequencyModel
                        {
                            SamplingFrequencyID = c.SamplingFrequencyId,
                            CriticalSamplingFrequencyID = c.CriticalSamplingFrequencyId,
                            CriticalstepandIntermediateLineID = c.CriticalstepandIntermediateLineId,
                            SampleFrequencyValue = c.SampleFrequencyValue,



                        }).Where(t => t.CriticalstepandIntermediateLineID == s.CriticalstepandIntermediateLineId).ToList(),
                    UnitsID = s.UnitsId,
                    SpecificationLimitsID = s.SpecificationLimitsId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                    Test = s.TestId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TestId).Select(m => m.Value).FirstOrDefault() : "",
                    TestStage = s.TestStageId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TestStageId).Select(m => m.Value).FirstOrDefault() : "",
                    Units = s.UnitsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UnitsId).Select(m => m.Value).FirstOrDefault() : "",
                    SpecificationLimits = s.SpecificationLimitsId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SpecificationLimitsId).Select(m => m.Value).FirstOrDefault() : "",
                };
                criticalstepandIntermediateLineModel.Add(criticalstepandIntermediateLineModels);
            });
            return criticalstepandIntermediateLineModel.Where(t => t.CriticalstepandIntermediateID == id).OrderByDescending(a => a.CriticalstepandIntermediateLineID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CriticalstepandIntermediateLineModel> GetData(SearchModel searchModel)
        {
            var criticalstepandIntermediateLine = new CriticalstepandIntermediateLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).FirstOrDefault();
                        break;
                    case "Last":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).LastOrDefault();
                        break;
                    case "Next":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).LastOrDefault();
                        break;
                    case "Previous":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).FirstOrDefault();
                        break;
                    case "Last":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).LastOrDefault();
                        break;
                    case "Next":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderBy(o => o.CriticalstepandIntermediateLineId).FirstOrDefault(s => s.CriticalstepandIntermediateLineId > searchModel.Id);
                        break;
                    case "Previous":
                        criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.OrderByDescending(o => o.CriticalstepandIntermediateLineId).FirstOrDefault(s => s.CriticalstepandIntermediateLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CriticalstepandIntermediateLineModel>(criticalstepandIntermediateLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCriticalstepandIntermediateLine")]
        public CriticalstepandIntermediateLineModel Post(CriticalstepandIntermediateLineModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var criticalstepandIntermediateLine = new CriticalstepandIntermediateLine
            {

                CriticalstepandIntermediateId = value.CriticalstepandIntermediateID,
                TestId = value.TestID,
                TestStageId = value.TestStageID,

                //SamplingFrequencyId = value.SamplingFrequencyID,
                SpecificationLimitsId = value.SpecificationLimitsID,
                UnitsId = value.UnitsID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.CriticalstepandIntermediateLine.Add(criticalstepandIntermediateLine);
            _context.SaveChanges();
            value.CriticalstepandIntermediateLineID = criticalstepandIntermediateLine.CriticalstepandIntermediateLineId;
            if (value.SamplingFrequencyIDs.Count > 0 && value.SamplingFrequencyIDs != null)
            {

                var samplingFrequency = value.ApplicationMasterList;
                samplingFrequency.ForEach(sa =>
                {
                    value.SampleFrequencyValue = 0;
                    if (sa.Value.ToLower() == "batch size")
                    {
                        value.SampleFrequencyValue = value.BatchSizeValue;
                    }
                    if (sa.Value.ToLower() == "timeline")
                    {
                        value.SampleFrequencyValue = value.TimelineValue;
                    }
                    if (sa.Value.ToLower() == "timepoint")
                    {
                        value.SampleFrequencyValue = value.TimepointValue;
                    }
                    var criticalsamplevalue = new CriticalSamplingFrequency
                    {
                        CriticalstepandIntermediateLineId = value.CriticalstepandIntermediateLineID,
                        SamplingFrequencyId = sa.ApplicationMasterDetailId,
                        SampleFrequencyValue = value.SampleFrequencyValue,

                    };
                    _context.CriticalSamplingFrequency.Add(criticalsamplevalue);


                });

            }

            _context.SaveChanges();
            value.Test = value.TestID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TestID).Select(m => m.Value).FirstOrDefault() : "";
            value.TestStage = value.TestStageID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TestStageID).Select(m => m.Value).FirstOrDefault() : "";
            value.Units = value.UnitsID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.UnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.SpecificationLimits = value.SpecificationLimitsID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.SpecificationLimitsID).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCriticalstepandIntermediateLine")]
        public CriticalstepandIntermediateLineModel Put(CriticalstepandIntermediateLineModel value)
        {
            var criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.SingleOrDefault(p => p.CriticalstepandIntermediateLineId == value.CriticalstepandIntermediateLineID);


            criticalstepandIntermediateLine.CriticalstepandIntermediateId = value.CriticalstepandIntermediateID;
            criticalstepandIntermediateLine.TestId = value.TestID;
            criticalstepandIntermediateLine.TestStageId = value.TestStageID;

            //criticalstepandIntermediateLine.SamplingFrequencyId = value.SamplingFrequencyID;
            criticalstepandIntermediateLine.UnitsId = value.UnitsID;
            criticalstepandIntermediateLine.SpecificationLimitsId = value.SpecificationLimitsID;
            criticalstepandIntermediateLine.ModifiedByUserId = value.ModifiedByUserID;
            criticalstepandIntermediateLine.ModifiedDate = DateTime.Now;
            criticalstepandIntermediateLine.StatusCodeId = value.StatusCodeID.Value;
            if (value.SamplingFrequencyIDs.Count > 0 && value.SamplingFrequencyIDs != null)
            {

                var samplingFrequency = value.ApplicationMasterList;
                samplingFrequency.ForEach(sa =>
                {
                    value.SampleFrequencyValue = 0;
                    if (sa.Value.ToLower() == "batch size")
                    {
                        value.SampleFrequencyValue = value.BatchSizeValue;
                    }
                    if (sa.Value.ToLower() == "timeline")
                    {
                        value.SampleFrequencyValue = value.TimelineValue;
                    }
                    if (sa.Value.ToLower() == "timepoint")
                    {
                        value.SampleFrequencyValue = value.TimepointValue;
                    }
                    var existingsampling = _context.CriticalSamplingFrequency.Where(t => t.CriticalstepandIntermediateLineId == value.CriticalstepandIntermediateLineID && t.SamplingFrequencyId == sa.ApplicationMasterDetailId).FirstOrDefault();
                    if (existingsampling == null)
                    {
                        var criticalsamplevalue = new CriticalSamplingFrequency
                        {
                            CriticalstepandIntermediateLineId = value.CriticalstepandIntermediateLineID,
                            SamplingFrequencyId = sa.ApplicationMasterDetailId,
                            SampleFrequencyValue = value.SampleFrequencyValue,

                        };
                        _context.CriticalSamplingFrequency.Add(criticalsamplevalue);
                    }



                });

            }
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCriticalstepandIntermediateLine")]
        public void Delete(int id)
        {
            var criticalstepandIntermediateLine = _context.CriticalstepandIntermediateLine.SingleOrDefault(p => p.CriticalstepandIntermediateLineId == id);
            if (criticalstepandIntermediateLine != null)
            {
                var CriticalSamplingFrequency = _context.CriticalSamplingFrequency.Where(a => a.CriticalstepandIntermediateLineId == id).ToList();
                if (CriticalSamplingFrequency.Count > 0)
                {
                    _context.CriticalSamplingFrequency.RemoveRange(CriticalSamplingFrequency);
                    _context.SaveChanges();
                }
                _context.CriticalstepandIntermediateLine.Remove(criticalstepandIntermediateLine);
                _context.SaveChanges();
            }
        }
    }
}