﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationMasterChildController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationMasterChildController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetApplicationMasterChildtWithChild")]
        public List<ApplicationMasterChildModel> Get(string id)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            if (id != null)
            {
                List<long> Ids = id.Split(',').Select(long.Parse).ToList();
                var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => Ids.Contains(w.ParentId.Value)).AsNoTracking().ToList();
                if (applicationmasterChild != null)
                {
                    applicationmasterChild.ForEach(s =>
                    {
                        ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                        applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                        applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                        applicationMasterChildModel.Value = s.Value;
                        applicationMasterChildModel.Description = s.Description;
                        applicationMasterChildModel.ParentId = s.ParentId;
                        applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                        applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                        applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                        applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                        applicationMasterChildModel.ParentName = s.Parent?.Value;
                        applicationMasterChildModel.DisplayName = applicationMasterChildModel.ParentName + "||" + s.Value;
                        applicationMasterChildModels.Add(applicationMasterChildModel);
                    });
                }
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }

        [HttpPost]
        [Route("GetApplicationMasterChildByMultipleSelection")]
        public List<ApplicationMasterChildModel> GetApplicationMasterChildByMultipleSelection(ProductActivityCaseModel value)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            if (value.CategoryActionMultipleIds != null && value.CategoryActionMultipleIds.Count>0)
            {

                var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => (value.CategoryActionMultipleIds.Contains(w.ParentId))).ToList();
                if (applicationmasterChild != null)
                {
                    applicationmasterChild.ForEach(s =>
                    {
                        ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                        applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                        applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                        applicationMasterChildModel.Value = s.Value;
                        applicationMasterChildModel.Description = s.Description;
                        applicationMasterChildModel.ParentId = s.ParentId;
                        applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                        applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                        applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                        applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                        applicationMasterChildModel.ParentName = s.Parent?.Value;
                        applicationMasterChildModel.DisplayName = applicationMasterChildModel.ParentName + "||" + s.Value;
                        applicationMasterChildModels.Add(applicationMasterChildModel);
                    });
                }
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterChildtWithChildWithouCat")]
        public List<ApplicationMasterChildModel> GetApplicationMasterChildtWithChildWithouCat(string id, long actionId)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            if (id != null)
            {
                List<long> Ids = id.Split(',').Select(long.Parse).ToList();
                var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => Ids.Contains(w.ParentId.Value)).AsNoTracking().ToList();
                var productActivityCase = _context.ProductActivityCase.Where(w => Ids.Contains(w.CategoryActionId.Value)).AsNoTracking().Select(o => o.ActionId).ToList();
                applicationmasterChild = applicationmasterChild.Where(c => !productActivityCase.Contains(c.ApplicationMasterChildId)).ToList();
                if (actionId > 0)
                {
                    var productActivityCases = _context.ProductActivityCase.Where(w => w.ProductActivityCaseId == actionId).AsNoTracking().FirstOrDefault()?.ActionId;
                    if (productActivityCases != null)
                    {
                        var lists = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => w.ApplicationMasterChildId == productActivityCases).AsNoTracking().FirstOrDefault();
                        applicationmasterChild.Add(lists);
                    }
                }
                if (applicationmasterChild != null)
                {
                    applicationmasterChild.ForEach(s =>
                    {
                        ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                        applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                        applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                        applicationMasterChildModel.Value = s.Value;
                        applicationMasterChildModel.Description = s.Description;
                        applicationMasterChildModel.ParentId = s.ParentId;
                        applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                        applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                        applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                        applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                        applicationMasterChildModel.ParentName = s.Parent?.Value;
                        applicationMasterChildModel.DisplayName = applicationMasterChildModel.ParentName + "||" + s.Value;
                        applicationMasterChildModels.Add(applicationMasterChildModel);
                    });
                }
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterChildByParent")]
        public List<ApplicationMasterChildModel> GetApplicationMasterChildByParent(long? id)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            var parentId = _context.ApplicationMasterParent.SingleOrDefault(w => w.ApplicationMasterParentCodeId == id).ParentId;
            if (parentId != null)
            {
                var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => w.ApplicationMasterParentId == parentId).AsNoTracking().ToList();
                if (applicationmasterChild != null)
                {
                    applicationmasterChild.ForEach(s =>
                    {
                        ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                        applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                        applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                        applicationMasterChildModel.Value = s.Parent?.Value + (s.Parent?.Value == null ? "" : "|") + s.Value;
                        applicationMasterChildModel.Description = s.Description;
                        applicationMasterChildModel.ParentId = s.ParentId;
                        applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                        applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                        applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                        applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                        applicationMasterChildModel.ParentName = s.Parent?.Value;
                        applicationMasterChildModels.Add(applicationMasterChildModel);
                    });
                }
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterChildByParentID")]
        public List<ApplicationMasterChildModel> GetApplicationMasterChildByParentID(long id)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).AsNoTracking().Where(w => w.ApplicationMasterParentId == id).ToList();
            if (applicationmasterChild != null)
            {
                applicationmasterChild.ForEach(s =>
                {
                    ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                    applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                    applicationMasterChildModel.Value = s.Value;
                    applicationMasterChildModel.Description = s.Description;
                    applicationMasterChildModel.ParentId = s.ParentId;
                    applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterChildModel.ParentName = s.Parent?.Value;
                    applicationMasterChildModels.Add(applicationMasterChildModel);
                });
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterChildListID")]
        public List<ApplicationMasterChildModel> ApplicationMasterChildListID(long? id)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).AsNoTracking().Where(w => w.ApplicationMasterChildId == id).ToList();
            if (applicationmasterChild != null)
            {
                applicationmasterChild.ForEach(s =>
                {
                    ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                    applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                    applicationMasterChildModel.Value = s.Value;
                    applicationMasterChildModel.Description = s.Description;
                    applicationMasterChildModel.ParentId = s.ParentId;
                    applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterChildModel.ParentName = s.Parent?.Value;
                    applicationMasterChildModel.ApplicationNameList = s.Parent?.Value + " | " + s.Value;
                    applicationMasterChildModels.Add(applicationMasterChildModel);
                });
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }
        [HttpPost]
        [Route("ApplicationMasterChildWikiTopicList")]
        public List<ApplicationMasterChildModel> ApplicationMasterChildWikiTopicList(SearchModel searchModel)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            var applicationmasterChild = _context.ApplicationMasterChild.Include(p => p.Parent).AsNoTracking().Where(w => searchModel.Ids.Contains(w.ParentId)).ToList();
            if (applicationmasterChild != null)
            {
                applicationmasterChild.ForEach(s =>
                {
                    ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                    applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                    applicationMasterChildModel.Value = s.Value;
                    applicationMasterChildModel.Description = s.Description;
                    applicationMasterChildModel.ParentId = s.ParentId;
                    applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterChildModel.ParentName = s.Parent?.Value;
                    applicationMasterChildModel.ApplicationNameList = s.Parent?.Value + " | " + s.Value;
                    applicationMasterChildModels.Add(applicationMasterChildModel);
                });
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }

        [HttpPost]
        [Route("SearchApplicationMasterChildWikiTopicList")]
        public List<ApplicationMasterChildModel> SearchApplicationMasterChildWikiTopicList(SearchModel searchModel)
        {
            List<ApplicationMasterChildModel> applicationMasterChildModels = new List<ApplicationMasterChildModel>();
            var applicationmasterChilds = _context.ApplicationMasterChild.Include(p => p.Parent).Where(w => w.ApplicationMasterParentId == 102);
            if (searchModel.WikiCategoryId != null)
            {
                applicationmasterChilds = applicationmasterChilds.Where(w => w.ParentId == searchModel.WikiCategoryId);
            }
            var applicationmasterChild = applicationmasterChilds.AsNoTracking().ToList();
            if (applicationmasterChild != null)
            {
                applicationmasterChild.ForEach(s =>
                {
                    ApplicationMasterChildModel applicationMasterChildModel = new ApplicationMasterChildModel();
                    applicationMasterChildModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterChildModel.ApplicationMasterChildId = s.ApplicationMasterChildId;
                    applicationMasterChildModel.Value = s.Value;
                    applicationMasterChildModel.Description = s.Description;
                    applicationMasterChildModel.ParentId = s.ParentId;
                    applicationMasterChildModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterChildModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterChildModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterChildModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterChildModel.ParentName = s.Parent?.Value;
                    applicationMasterChildModel.ApplicationNameList = s.Parent?.Value + " | " + s.Value;
                    applicationMasterChildModels.Add(applicationMasterChildModel);
                });
            }
            return applicationMasterChildModels.OrderByDescending(o => o.ApplicationMasterChildId).ToList();
        }

        [HttpPost]
        [Route("InsertApplicationMasterChild")]
        public ApplicationMasterChildModel Post(ApplicationMasterChildModel value)
        {
            var sessionId = Guid.NewGuid();
            var applicationmasterChild = new ApplicationMasterChild
            {
                ApplicationMasterParentId = value.ApplicationMasterParentId,
                Value = value.Value,
                Description = value.Description,
                ParentId = value.ParentId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
            };
            _context.ApplicationMasterChild.Add(applicationmasterChild);
            _context.SaveChanges();
            value.ApplicationMasterChildId = applicationmasterChild.ApplicationMasterChildId;
            if (value.ParentId != null)
            {
                value.ParentName = _context.ApplicationMasterChild.SingleOrDefault(w => w.ApplicationMasterChildId == value.ParentId).Value;
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateApplicationMasterChild")]
        public ApplicationMasterChildModel Put(ApplicationMasterChildModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var applicationmasterChild = _context.ApplicationMasterChild.SingleOrDefault(p => p.ApplicationMasterChildId == value.ApplicationMasterChildId);
            applicationmasterChild.ApplicationMasterParentId = value.ApplicationMasterParentId;
            applicationmasterChild.Value = value.Value;
            applicationmasterChild.Description = value.Description;
            applicationmasterChild.ParentId = value.ParentId;
            applicationmasterChild.ModifiedByUserId = value.ModifiedByUserID;
            applicationmasterChild.ModifiedDate = DateTime.Now;
            applicationmasterChild.SessionId = value.SessionId;
            _context.SaveChanges();
            if (value.ParentId != null)
            {
                value.ParentName = _context.ApplicationMasterChild.SingleOrDefault(w => w.ApplicationMasterChildId == value.ParentId).Value;
            }
            return value;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationMasterChildModel> GetData(SearchModel searchModel)
        {
            var applicationmasterChild = new ApplicationMasterChild();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderBy(o => o.ApplicationMasterChildId).FirstOrDefault(s => s.ApplicationMasterChildId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationmasterChild = _context.ApplicationMasterChild.Where(o => o.ParentId == searchModel.ParentID).OrderByDescending(o => o.ApplicationMasterChildId).FirstOrDefault(s => s.ApplicationMasterChildId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationMasterChildModel>(applicationmasterChild);
            return result;
        }
        [HttpDelete]
        [Route("DeleteApplicationMasterChild")]
        public void Delete(int id)
        {
            var applicationmasterChild = _context.ApplicationMasterChild.SingleOrDefault(p => p.ApplicationMasterChildId == id);
            if (applicationmasterChild != null)
            {
                _context.ApplicationMasterChild.Remove(applicationmasterChild);
                _context.SaveChanges();
            }
        }
    }
}
