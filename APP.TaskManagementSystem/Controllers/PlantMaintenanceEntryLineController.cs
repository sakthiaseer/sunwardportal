﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PlantMaintenanceEntryLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        private readonly IHostingEnvironment _hostingEnvironment;

        public PlantMaintenanceEntryLineController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;


        }

        // GET: api/PlantMaintenanceEntryLine
        [HttpGet]
        [Route("GetPlantMaintenanceEntryLines")]
        public List<PlantMaintenanceEntryLineModel> Get()
        {
            List<PlantMaintenanceEntryLineModel> plantMaintenanceEntryLineModels = new List<PlantMaintenanceEntryLineModel>();
            var plantMaintenanceEntrys = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).AsNoTracking().ToList();
            if(plantMaintenanceEntrys!=null && plantMaintenanceEntrys.Count>0)
            {
                plantMaintenanceEntrys.ForEach(s =>
                {
                    PlantMaintenanceEntryLineModel plantMaintenanceEntryLineModel = new PlantMaintenanceEntryLineModel
                    {
                        PlantEntryLineID = s.PlantEntryLineId,
                        PlantEntryId = s.PlantEntryId,
                        ItemGroupId = s.ItemGroupId,
                        EntryNumber = s.EntryNumber,
                        Manufacture = s.Manufacture,
                        Brand = s.Brand,
                        ModelNumber = s.ModelNumber,
                        ManufactureIdNo = s.ManufactureIdNo,
                        ManufactureSerialNo = s.ManufactureSerialNo,
                        SWExistingId = s.SwexistingId,
                        FrontPhoto = s.FrontPhoto,
                        BackPhoto = s.BackPhoto,
                        LocationVideo = s.LocationVideo,
                        Condition = s.Condition,
                        Recommendation = s.Recommendation,
                        CategoryID = s.CategoryId,
                        NameGroupID = s.NameGroupId,
                        PlantItemGroupID = s.PlantItemGroupId,
                        PartGroupID = s.PartGroupId,
                        SubPartGroupID = s.SubPartGroupId,
                        SpecificItemName = s.SpecificItemName,
                        

                    };
                    plantMaintenanceEntryLineModels.Add(plantMaintenanceEntryLineModel);
                });
            }
         
            return plantMaintenanceEntryLineModels;
        }

        [HttpGet]
        [Route("GetPlantMaintenanceEntryLineByID")]
        public List<PlantMaintenanceEntryLineModel> GetPlantMaintenanceEntryLineByID(int id)
        {
            List<PlantMaintenanceEntryLineModel> plantMaintenanceEntryLineModels = new List<PlantMaintenanceEntryLineModel>();
            var plantMaintenanceEntryLines = _context.PlantMaintenanceEntryLine.Include(p => p.PlantMaintenanceMasterDetail).OrderByDescending(o => o.PlantEntryLineId).Where(t => t.PlantEntryId == id).AsNoTracking().ToList();
            var plantMaintenanceMasterDetailList = _context.PlantMaintenanceMasterDetail.AsNoTracking().ToList();
            if (plantMaintenanceEntryLines!=null && plantMaintenanceEntryLines.Count>0)
            {
                plantMaintenanceEntryLines.ForEach(s =>
                {
                    PlantMaintenanceEntryLineModel plantMaintenanceEntryLineModel = new PlantMaintenanceEntryLineModel
                    {
                        PlantEntryLineID = s.PlantEntryLineId,
                        PlantEntryId = s.PlantEntryId,
                        ItemGroupId = s.ItemGroupId,
                        EntryNumber = s.EntryNumber,
                        Manufacture = s.Manufacture,
                        Brand = s.Brand,
                        ModelNumber = s.ModelNumber,
                        ManufactureIdNo = s.ManufactureIdNo,
                        ManufactureSerialNo = s.ManufactureSerialNo,
                        SWExistingId = s.SwexistingId,
                        //FrontPhoto = s.FrontPhoto,
                        //BackPhoto = s.BackPhoto,
                        //LocationVideo = s.LocationVideo,
                        FrontPhotoName = s.FrontPhotoName,
                        BackPhotoName = s.BackPhotoName,
                        Condition = s.Condition,
                        Recommendation = s.Recommendation,
                        VideoFileName = s.VideoFileName,
                        CategoryID = s.CategoryId,
                        NameGroupID = s.NameGroupId,
                        PlantItemGroupID = s.PlantItemGroupId,
                        PartGroupID = s.PartGroupId,
                        SubPartGroupID = s.SubPartGroupId,
                        SpecificItemName = s.SpecificItemName,
                        CategoryNames = plantMaintenanceMasterDetailList.Where(p => p.PlantEntryLineId == s.PlantEntryLineId && p.PlantMaintenanceEntryMaster.MasterType == 640).Select(p => p.PlantMaintenanceEntryMaster.Name).Distinct().ToList(),
                        PlantItemGroupNames = plantMaintenanceMasterDetailList.Where(p => p.PlantEntryLineId == s.PlantEntryLineId && p.PlantMaintenanceEntryMaster.MasterType == 641).Select(p => p.PlantMaintenanceEntryMaster.Name).Distinct().ToList(),
                        NameGroupNames = plantMaintenanceMasterDetailList.Where(p => p.PlantEntryLineId == s.PlantEntryLineId && p.PlantMaintenanceEntryMaster.MasterType == 642).Select(p => p.PlantMaintenanceEntryMaster.Name).Distinct().ToList(),
                        PartGroupNames = plantMaintenanceMasterDetailList.Where(p => p.PlantEntryLineId == s.PlantEntryLineId && p.PlantMaintenanceEntryMaster.MasterType == 643).Select(p => p.PlantMaintenanceEntryMaster.Name).Distinct().ToList(),
                        SubPartGroupNames = plantMaintenanceMasterDetailList.Where(p => p.PlantEntryLineId == s.PlantEntryLineId && p.PlantMaintenanceEntryMaster.MasterType == 644).Select(p => p.PlantMaintenanceEntryMaster.Name).Distinct().ToList(),
                        Category =s.Category!=null? s.Category.Name : "",
                        NameGroup =s.NameGroup!=null? s.NameGroup.Name : "",
                        PlantItemGroup =s.PlantItemGroup!=null? s.PlantItemGroup.Name : "",
                        PartGroup = s.PartGroup!=null? s.PartGroup.Name : "",
                        SubPartGroup = s.SubPartGroup!=null? s.SubPartGroup.Name :"",

                    };
                    plantMaintenanceEntryLineModels.Add(plantMaintenanceEntryLineModel);
                });
               
            }
          
            return plantMaintenanceEntryLineModels;

        }

        // GET: api/PlantMaintenanceEntryLine/2
        //[HttpGet("{id}", Name = "GetPlantMaintenanceEntry")]
        [HttpGet("GetPlantMaintenanceEntryLines/{id:int}")]
        public ActionResult<PlantMaintenanceEntryLineModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.Where(p => p.PlantEntryLineId == id.Value).Select(s => new PlantMaintenanceEntryLineModel
            {
                PlantEntryLineID = s.PlantEntryLineId,
                PlantEntryId = s.PlantEntryId,
                ItemGroupId = s.ItemGroupId,
                EntryNumber = s.EntryNumber,
                Manufacture = s.Manufacture,
                Brand = s.Brand,
                ModelNumber = s.ModelNumber,
                ManufactureIdNo = s.ManufactureIdNo,
                ManufactureSerialNo = s.ManufactureSerialNo,
                SWExistingId = s.SwexistingId,
                //FrontPhoto = s.FrontPhoto,
                // BackPhoto = s.BackPhoto,
                //LocationVideo = s.LocationVideo,
                Condition = s.Condition,
                Recommendation = s.Recommendation,
                VideoFileName = s.VideoFileName,
                FrontPhotoName = s.FrontPhotoName,
                BackPhotoName = s.BackPhotoName,
                CategoryID = s.CategoryId,
                NameGroupID = s.NameGroupId,
                PlantItemGroupID = s.PlantItemGroupId,
                PartGroupID = s.PartGroupId,
                SubPartGroupID = s.SubPartGroupId,
                SpecificItemName = s.SpecificItemName,

                Category = s.Category!=null? s.Category.Name : "",
                NameGroup = s.NameGroup!=null? s.NameGroup.Name : "",
                PlantItemGroup = s.PlantItemGroup!=null? s.PlantItemGroup.Name : "",
                PartGroup = s.PartGroup!=null? s.PartGroup.Name : "",
                SubPartGroup = s.SubPartGroup!=null? s.SubPartGroup.Name : "",
            }).FirstOrDefault();

            return plantMaintenanceEntryLine;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PlantMaintenanceEntryLineModel> GetData(SearchModel searchModel)
        {
            var plantMaintenanceEntryLine = new PlantMaintenanceEntryLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).LastOrDefault();
                        break;
                    case "Previous":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderBy(o => o.PlantEntryLineId).FirstOrDefault(s => s.PlantEntryLineId > searchModel.Id);
                        break;
                    case "Previous":
                        plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.OrderByDescending(o => o.PlantEntryLineId).FirstOrDefault(s => s.PlantEntryLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PlantMaintenanceEntryLineModel>(plantMaintenanceEntryLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPlantMaintenanceEntryLines")]
        public PlantMaintenanceEntryLineModel Post(PlantMaintenanceEntryLineModel value)
        {
            var plantMaintenanceEntryLine = new PlantMaintenanceEntryLine
            {

                PlantEntryId = value.PlantEntryId,
                ItemGroupId = value.ItemGroupId,
                EntryNumber = String.IsNullOrEmpty(value.EntryNumber) ? "no-information" : value.EntryNumber,
                Manufacture = String.IsNullOrEmpty(value.Manufacture) ? "no-information" : value.Manufacture,
                Brand = String.IsNullOrEmpty(value.Brand) ? "no-information" : value.Brand,
                ModelNumber = String.IsNullOrEmpty(value.ModelNumber) ? "no-information" : value.ModelNumber,
                ManufactureIdNo = String.IsNullOrEmpty(value.ManufactureIdNo) ? "no-information" : value.ModelNumber,
                ManufactureSerialNo = String.IsNullOrEmpty(value.ManufactureSerialNo) ? "no-information" : value.ModelNumber,
                SwexistingId = String.IsNullOrEmpty(value.SWExistingId) ? "no-information" : value.ModelNumber,
                //FrontPhoto = value.FrontPhoto,
                //BackPhoto = value.BackPhoto,
                FrontPhotoName = value.FrontPhotoName,
                BackPhotoName = value.BackPhotoName,
                //LocationVideo = value.LocationVideo,
                VideoFileName = value.VideoFileName,
                CategoryId = value.CategoryID,
                NameGroupId = value.NameGroupID,
                PlantItemGroupId = value.PlantItemGroupID,
                PartGroupId = value.PartGroupID,
                SubPartGroupId = value.SubPartGroupID,
                SpecificItemName = value.SpecificItemName,
                Condition = String.IsNullOrEmpty(value.Condition) ? "no-information" : value.ModelNumber,
                Recommendation = String.IsNullOrEmpty(value.Recommendation) ? "no-information" : value.ModelNumber,

            };
            _context.PlantMaintenanceEntryLine.Add(plantMaintenanceEntryLine);


            if (value.LocationVideo != null && value.LocationVideo.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.VideoFileName;
                System.IO.File.WriteAllBytes(serverPath, value.LocationVideo);
            }

            if (value.FrontPhoto != null && value.FrontPhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.FrontPhotoName;
                System.IO.File.WriteAllBytes(serverPath, value.FrontPhoto);
            }

            if (value.BackPhoto != null && value.BackPhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.BackPhotoName;
                System.IO.File.WriteAllBytes(serverPath, value.BackPhoto);
            }
            _context.SaveChanges();
            value.PlantEntryLineID = plantMaintenanceEntryLine.PlantEntryLineId;



            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdatePlantMaintenanceEntryLines")]
        public PlantMaintenanceEntryLineModel Put(PlantMaintenanceEntryLineModel value)
        {
            var plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.SingleOrDefault(p => p.PlantEntryLineId == value.PlantEntryLineID);

            plantMaintenanceEntryLine.PlantEntryId = value.PlantEntryId;
            plantMaintenanceEntryLine.ItemGroupId = value.ItemGroupId;
            plantMaintenanceEntryLine.EntryNumber = String.IsNullOrEmpty(value.EntryNumber) ? "no-information" : value.EntryNumber;
            plantMaintenanceEntryLine.Manufacture = String.IsNullOrEmpty(value.Manufacture) ? "no-information" : value.Manufacture;
            plantMaintenanceEntryLine.Brand = String.IsNullOrEmpty(value.Brand) ? "no-information" : value.Brand;
            plantMaintenanceEntryLine.ModelNumber = String.IsNullOrEmpty(value.ModelNumber) ? "no-information" : value.ModelNumber;
            plantMaintenanceEntryLine.ManufactureIdNo = String.IsNullOrEmpty(value.ManufactureIdNo) ? "no-information" : value.ManufactureIdNo;
            plantMaintenanceEntryLine.ManufactureSerialNo = String.IsNullOrEmpty(value.ManufactureSerialNo) ? "no-information" : value.ManufactureSerialNo;
            plantMaintenanceEntryLine.SwexistingId = String.IsNullOrEmpty(value.SWExistingId) ? "no-information" : value.SWExistingId;
            //plantMaintenanceEntryLine.FrontPhoto = value.FrontPhoto;
            // plantMaintenanceEntryLine.BackPhoto = value.BackPhoto;
            plantMaintenanceEntryLine.FrontPhotoName = value.FrontPhotoName;
            plantMaintenanceEntryLine.BackPhotoName = value.BackPhotoName;
            //plantMaintenanceEntryLine.LocationVideo = value.LocationVideo;
            plantMaintenanceEntryLine.VideoFileName = value.VideoFileName;
            plantMaintenanceEntryLine.Condition = String.IsNullOrEmpty(value.Condition) ? "no-information" : value.Condition;
            plantMaintenanceEntryLine.Recommendation = String.IsNullOrEmpty(value.Recommendation) ? "no-information" : value.Recommendation;

            if (value.LocationVideo != null && value.LocationVideo.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.VideoFileName;
                System.IO.File.WriteAllBytes(serverPath, value.LocationVideo);
            }
            if (value.FrontPhoto != null && value.FrontPhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.FrontPhotoName;
                System.IO.File.WriteAllBytes(serverPath, value.FrontPhoto);
            }

            if (value.BackPhoto != null && value.BackPhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.BackPhotoName;
                System.IO.File.WriteAllBytes(serverPath, value.BackPhoto);
            }

            _context.SaveChanges();

            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdatePlantMaintenance")]
        public PlantMaintenanceEntryLineModel PutPlantMaintenace(PlantMaintenanceEntryLineModel value)
        {
            var plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.SingleOrDefault(p => p.PlantEntryLineId == value.PlantEntryLineID);

            plantMaintenanceEntryLine.PlantEntryId = value.PlantEntryId;
            plantMaintenanceEntryLine.ItemGroupId = value.ItemGroupId;
            plantMaintenanceEntryLine.EntryNumber = String.IsNullOrEmpty(value.EntryNumber) ? "no-information" : value.EntryNumber;
            plantMaintenanceEntryLine.Manufacture = String.IsNullOrEmpty(value.Manufacture) ? "no-information" : value.Manufacture;
            plantMaintenanceEntryLine.Brand = String.IsNullOrEmpty(value.Brand) ? "no-information" : value.Brand;
            plantMaintenanceEntryLine.ModelNumber = String.IsNullOrEmpty(value.ModelNumber) ? "no-information" : value.ModelNumber;
            plantMaintenanceEntryLine.ManufactureIdNo = String.IsNullOrEmpty(value.ManufactureIdNo) ? "no-information" : value.ManufactureIdNo;
            plantMaintenanceEntryLine.ManufactureSerialNo = String.IsNullOrEmpty(value.ManufactureSerialNo) ? "no-information" : value.ManufactureSerialNo;
            plantMaintenanceEntryLine.SwexistingId = String.IsNullOrEmpty(value.SWExistingId) ? "no-information" : value.SWExistingId;
            plantMaintenanceEntryLine.Condition = String.IsNullOrEmpty(value.Condition) ? "no-information" : value.Condition;
            plantMaintenanceEntryLine.Recommendation = String.IsNullOrEmpty(value.Recommendation) ? "no-information" : value.Recommendation;


            _context.SaveChanges();

            return value;
        }

        [HttpPut]
        [Route("UpdatePlantMaintenanceLine")]
        public PlantMaintenanceEntryLineModel PutPlantMaintenaceEntryLine(PlantMaintenanceEntryLineModel value)
        {
            var plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.SingleOrDefault(p => p.PlantEntryLineId == value.PlantEntryLineID);
           
            plantMaintenanceEntryLine.PlantEntryId = value.PlantEntryId;
            plantMaintenanceEntryLine.ItemGroupId = value.ItemGroupId;
            plantMaintenanceEntryLine.EntryNumber = String.IsNullOrEmpty(value.EntryNumber) ? "no-information" : value.EntryNumber;
            plantMaintenanceEntryLine.Manufacture = String.IsNullOrEmpty(value.Manufacture) ? "no-information" : value.Manufacture;
            plantMaintenanceEntryLine.Brand = String.IsNullOrEmpty(value.Brand) ? "no-information" : value.Brand;
            plantMaintenanceEntryLine.ModelNumber = String.IsNullOrEmpty(value.ModelNumber) ? "no-information" : value.ModelNumber;
            plantMaintenanceEntryLine.ManufactureIdNo = String.IsNullOrEmpty(value.ManufactureIdNo) ? "no-information" : value.ManufactureIdNo;
            plantMaintenanceEntryLine.ManufactureSerialNo = String.IsNullOrEmpty(value.ManufactureSerialNo) ? "no-information" : value.ManufactureSerialNo;
            plantMaintenanceEntryLine.SwexistingId = String.IsNullOrEmpty(value.SWExistingId) ? "no-information" : value.SWExistingId;
            plantMaintenanceEntryLine.Condition = String.IsNullOrEmpty(value.Condition) ? "no-information" : value.Condition;
            plantMaintenanceEntryLine.Recommendation = String.IsNullOrEmpty(value.Recommendation) ? "no-information" : value.Recommendation;
            plantMaintenanceEntryLine.SpecificItemName = value.SpecificItemName;
            var categoryNames = value.CategoryNames;
            if (categoryNames.Count != 0)
            {
                categoryNames.ForEach(category =>
                {
                    var categoryExist = _context.PlantMaintenanceEntryMaster.Where(p => p.Name == category).FirstOrDefault();
                    if (categoryExist == null)
                    {

                        var plantMaintenanceMaster = new PlantMaintenanceEntryMaster
                        {

                            Name = category,
                            MasterType = 640,
                            AddedByUserId = value.PlantMaintenanceMaster.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.PlantMaintenanceMaster.StatusCodeID.Value,



                        };
                        _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceMaster);
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantMaintenanceMaster.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);
                    }
                    else
                    {                       
                            var plantmasterDetail = new PlantMaintenanceMasterDetail
                            {
                                PlantEntryLineId = value.PlantEntryLineID,
                                PlantMaintenanceEntryMasterId = categoryExist.PlantMaintenanceEntryMasterId,

                            };
                            _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);
                       
                    }



                }

                );
            }
            var plantItemGroupNames = value.PlantItemGroupNames;
            if (plantItemGroupNames.Count != 0)
            {
                plantItemGroupNames.ForEach(plantItemGroup =>
                {
                    var plantItemGroupNamesExist = _context.PlantMaintenanceEntryMaster.Where(p => p.Name == plantItemGroup).FirstOrDefault();
                    if (plantItemGroupNamesExist == null)
                    {

                        var plantMaintenanceMaster = new PlantMaintenanceEntryMaster
                        {

                            Name = plantItemGroup,
                            MasterType = 641,

                            AddedByUserId = value.PlantMaintenanceMaster.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.PlantMaintenanceMaster.StatusCodeID.Value,



                        };
                        _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceMaster);
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantMaintenanceMaster.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }
                    else
                    {
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantItemGroupNamesExist.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }


                }

                );
            }
            var nameGroupNames = value.NameGroupNames;
            if (nameGroupNames.Count != 0)
            {
                nameGroupNames.ForEach(nameGroup =>
                {
                    var nameGroupNamesExist = _context.PlantMaintenanceEntryMaster.Where(p => p.Name == nameGroup).FirstOrDefault();
                    if (nameGroupNamesExist == null)
                    {

                        var plantMaintenanceMaster = new PlantMaintenanceEntryMaster
                        {

                            Name = nameGroup,
                            MasterType = 642,
                            AddedByUserId = value.PlantMaintenanceMaster.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.PlantMaintenanceMaster.StatusCodeID.Value,



                        };
                        _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceMaster);
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantMaintenanceMaster.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }
                    else
                    {
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = nameGroupNamesExist.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }


                }

                );
            }

            var partGroupNames = value.PartGroupNames;
            if (partGroupNames.Count != 0)
            {
                partGroupNames.ForEach(partGroup =>
                {
                    var partGroupNamesExist = _context.PlantMaintenanceEntryMaster.Where(p => p.Name == partGroup).FirstOrDefault();
                    if (partGroupNamesExist == null)
                    {

                        var plantMaintenanceMaster = new PlantMaintenanceEntryMaster
                        {

                            Name = partGroup,
                            MasterType = 643,
                            AddedByUserId = value.PlantMaintenanceMaster.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.PlantMaintenanceMaster.StatusCodeID.Value,



                        };
                        _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceMaster);
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantMaintenanceMaster.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }
                    else
                    {
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = partGroupNamesExist.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }


                }

                );
            }
            var subPartGroupNames = value.SubPartGroupNames;
            if (subPartGroupNames.Count != 0)
            {
                subPartGroupNames.ForEach(subPartGroup =>
                {
                    var subPartGroupNamesExist = _context.PlantMaintenanceEntryMaster.Where(p => p.Name == subPartGroup).FirstOrDefault();
                    if (subPartGroupNamesExist == null)
                    {

                        var plantMaintenanceMaster = new PlantMaintenanceEntryMaster
                        {

                            Name = subPartGroup,
                            MasterType = 644,
                            AddedByUserId = value.PlantMaintenanceMaster.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.PlantMaintenanceMaster.StatusCodeID.Value,



                        };
                        _context.PlantMaintenanceEntryMaster.Add(plantMaintenanceMaster);
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = plantMaintenanceMaster.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }
                    else
                    {
                        var plantmasterDetail = new PlantMaintenanceMasterDetail
                        {
                            PlantEntryLineId = value.PlantEntryLineID,
                            PlantMaintenanceEntryMasterId = subPartGroupNamesExist.PlantMaintenanceEntryMasterId,

                        };
                        _context.PlantMaintenanceMasterDetail.Add(plantmasterDetail);

                    }


                }

                );
            }
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeletePlantMaintenanceEntryLines")]
        public void Delete(int id)
        {
            var plantMaintenanceEntryLine = _context.PlantMaintenanceEntryLine.SingleOrDefault(p => p.PlantEntryLineId == id);
            if (plantMaintenanceEntryLine != null)
            {
                _context.PlantMaintenanceEntryLine.Remove(plantMaintenanceEntryLine);
                _context.SaveChanges();
            }
        }
    }
}