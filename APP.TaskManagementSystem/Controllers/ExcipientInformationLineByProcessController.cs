﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ExcipientInformationLineByProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ExcipientInformationLineByProcessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetExcipientInformationLineByProcessByID")]
        public List<ExcipientInformationLineByProcessModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var items = _context.Navitems.AsNoTracking().ToList();

            var excipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.Include(m => m.MaterialName).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<ExcipientInformationLineByProcessModel> excipientInformationLineByProcessModel = new List<ExcipientInformationLineByProcessModel>();
            excipientInformationLineByProcess.ForEach(s =>
            {
                ExcipientInformationLineByProcessModel excipientInformationLineByProcessModels = new ExcipientInformationLineByProcessModel
                {
                    ExcipientInformationLineByProcessID = s.ExcipientInformationLineByProcessId,
                    PerUnitFormulationLineID = s.PerUnitFormulationLineId,
                    MaterialFunctionID = s.MaterialFunctionId,
                    MaterialNameID = s.MaterialNameId,
                    MaterialName = s.MaterialName != null ? s.MaterialName.MaterialName : null,
                    Overage = s.Overage,
                    PerDosageID = s.PerDosageId,
                    Dosage = s.Dosage,
                    DosageInformationDosageUnitsID = s.DosageInformationDosageUnitsId,
                    OverageInformationDosageUnitsID = s.OverageInformationDosageUnitsId,
                    OverageUnitsID = s.OverageUnitsId,
                    MaterialFunction = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialFunctionId).Select(a => a.Value).SingleOrDefault() : "",
                    OverageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.OverageUnitsId).Select(a => a.Value).SingleOrDefault() : "",

                    DosageInformationDosageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageInformationDosageUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                    OverageInformationDosageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.OverageInformationDosageUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                    PerDosage = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PerDosageId).Select(a => a.Value).SingleOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    NavisionSpecificationId = s.NavisionSpecificationId,
                    TypeOfIngredientId = s.TypeOfIngredientId,
                    TypeOfIngredientName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TypeOfIngredientId).Select(a => a.Value).SingleOrDefault() : "",
                };
                excipientInformationLineByProcessModel.Add(excipientInformationLineByProcessModels);
            });
            return excipientInformationLineByProcessModel.Where(t => t.PerUnitFormulationLineID == id).OrderByDescending(a => a.ExcipientInformationLineByProcessID).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ExcipientInformationLineByProcessModel> GetData(SearchModel searchModel)
        {
            var ExcipientInformationLineByProcess = new ExcipientInformationLineByProcess();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).LastOrDefault();
                        break;
                    case "Next":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).LastOrDefault();
                        break;
                    case "Previous":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).LastOrDefault();
                        break;
                    case "Next":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderBy(o => o.ExcipientInformationLineByProcessId).FirstOrDefault(s => s.ExcipientInformationLineByProcessId > searchModel.Id);
                        break;
                    case "Previous":
                        ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.OrderByDescending(o => o.ExcipientInformationLineByProcessId).FirstOrDefault(s => s.ExcipientInformationLineByProcessId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ExcipientInformationLineByProcessModel>(ExcipientInformationLineByProcess);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertExcipientInformationLineByProcess")]
        public ExcipientInformationLineByProcessModel Post(ExcipientInformationLineByProcessModel value)
        {
            var items = _context.Navitems.ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var ExcipientInformationLineByProcess = new ExcipientInformationLineByProcess
            {
                MaterialFunctionId = value.MaterialFunctionID,
                MaterialNameId = value.MaterialNameID,
                PerUnitFormulationLineId = value.PerUnitFormulationLineID,
                Overage = value.Overage,
                Dosage = value.Dosage,
                PerDosageId = value.PerDosageID,
                DosageInformationDosageUnitsId = value.DosageInformationDosageUnitsID,
                OverageUnitsId = value.OverageUnitsID,
                OverageInformationDosageUnitsId = value.OverageInformationDosageUnitsID,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                NavisionSpecificationId = value.NavisionSpecificationId,
                TypeOfIngredientId = value.TypeOfIngredientId,



            };
            _context.ExcipientInformationLineByProcess.Add(ExcipientInformationLineByProcess);
            _context.SaveChanges();
            value.ExcipientInformationLineByProcessID = ExcipientInformationLineByProcess.ExcipientInformationLineByProcessId;
            value.MaterialFunction = value.MaterialFunctionID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.MaterialFunctionID).Select(m => m.Value).FirstOrDefault() : "";
            value.DosageInformationDosageUnits = value.DosageInformationDosageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.DosageInformationDosageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.OverageInformationDosageUnits = value.OverageInformationDosageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.OverageInformationDosageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.OverageUnits = value.OverageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.OverageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.PerDosage = value.PerDosageID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.PerDosageID).Select(m => m.Value).FirstOrDefault() : "";
            value.TypeOfIngredientName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.TypeOfIngredientId).Select(a => a.Value).SingleOrDefault() : "";

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateExcipientInformationLineByProcess")]
        public ExcipientInformationLineByProcessModel Put(ExcipientInformationLineByProcessModel value)
        {
            var items = _context.Navitems.ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var excipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.SingleOrDefault(p => p.ExcipientInformationLineByProcessId == value.ExcipientInformationLineByProcessID);

            excipientInformationLineByProcess.PerUnitFormulationLineId = value.PerUnitFormulationLineID;
            excipientInformationLineByProcess.MaterialFunctionId = value.MaterialFunctionID;
            excipientInformationLineByProcess.MaterialNameId = value.MaterialNameID;
            excipientInformationLineByProcess.Overage = value.Overage;
            excipientInformationLineByProcess.OverageUnitsId = value.OverageUnitsID;
            excipientInformationLineByProcess.Dosage = value.Dosage;
            excipientInformationLineByProcess.DosageInformationDosageUnitsId = value.DosageInformationDosageUnitsID;
            excipientInformationLineByProcess.OverageInformationDosageUnitsId = value.OverageInformationDosageUnitsID;
            excipientInformationLineByProcess.PerDosageId = value.PerDosageID;
            excipientInformationLineByProcess.ModifiedByUserId = value.ModifiedByUserID;
            excipientInformationLineByProcess.ModifiedDate = DateTime.Now;
            excipientInformationLineByProcess.NavisionSpecificationId = value.NavisionSpecificationId;
            excipientInformationLineByProcess.TypeOfIngredientId = value.TypeOfIngredientId;
            _context.SaveChanges();
            value.ExcipientInformationLineByProcessID = excipientInformationLineByProcess.ExcipientInformationLineByProcessId;
            value.MaterialFunction = value.MaterialFunctionID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.MaterialFunctionID).Select(m => m.Value).FirstOrDefault() : "";
            value.DosageInformationDosageUnits = value.DosageInformationDosageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.DosageInformationDosageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.OverageInformationDosageUnits = value.OverageInformationDosageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.OverageInformationDosageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.OverageUnits = value.OverageUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.OverageUnitsID).Select(m => m.Value).FirstOrDefault() : "";
            value.PerDosage = value.PerDosageID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.PerDosageID).Select(m => m.Value).FirstOrDefault() : "";
            value.TypeOfIngredientName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.TypeOfIngredientId).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteExcipientInformationLineByProcess")]
        public void Delete(int id)
        {
            var ExcipientInformationLineByProcess = _context.ExcipientInformationLineByProcess.SingleOrDefault(p => p.ExcipientInformationLineByProcessId == id);
            if (ExcipientInformationLineByProcess != null)
            {
                _context.ExcipientInformationLineByProcess.Remove(ExcipientInformationLineByProcess);
                _context.SaveChanges();
            }
        }
    }
}