﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MobileShiftController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MobileShiftController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetMobileShift")]
        public List<MobileShiftModel> Get()
        {
            List<MobileShiftModel> mobileShiftModels = new List<MobileShiftModel>();
            var mobileShift = _context.MobileShift
              .Include(a => a.AddedByUser)
              .Include(b => b.ModifiedByUser)
              .Include(c => c.StatusCode)
              .Include(d => d.Plant)
              .OrderByDescending(o => o.MobileShiftId).AsNoTracking().ToList();
            mobileShift.ForEach(s =>
            {
                MobileShiftModel mobileShiftModel = new MobileShiftModel
                {
                    MobileShiftId = s.MobileShiftId,
                    Code = s.Code,
                    PlantId = s.PlantId,
                    Description = s.Description,
                    CompanyName = s.Plant?.PlantCode,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                mobileShiftModels.Add(mobileShiftModel);

            });
            return mobileShiftModels;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetMobileShiftByPlants")]
        public List<MobileShiftModel> GetMobileShiftByPlants(string Id)
        {
            int plantId = 1;
            if (Id == "NAV_SG")
            {
                plantId = 2;
            }
            List<MobileShiftModel> mobileShiftModels = new List<MobileShiftModel>();
            var mobileShift = _context.MobileShift
              .Include(a => a.AddedByUser)
              .Include(b => b.ModifiedByUser)
              .Include(c => c.StatusCode)
              .Include(d => d.Plant)
              .Where(p => p.PlantId == plantId)
              .OrderByDescending(o => o.MobileShiftId).AsNoTracking().ToList();
            mobileShift.ForEach(s =>
            {
                MobileShiftModel mobileShiftModel = new MobileShiftModel
                {
                    MobileShiftId = s.MobileShiftId,
                    Code = s.Code,
                    PlantId = s.PlantId,
                    Description = s.Description,
                    CompanyName = s.Plant?.PlantCode,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate
                };
                mobileShiftModels.Add(mobileShiftModel);

            });
            return mobileShiftModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MobileShiftModel> GetData(SearchModel searchModel)
        {
            var mobileShift = new MobileShift();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).FirstOrDefault();
                        break;
                    case "Last":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).LastOrDefault();
                        break;
                    case "Next":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).LastOrDefault();
                        break;
                    case "Previous":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).FirstOrDefault();
                        break;
                    case "Last":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).LastOrDefault();
                        break;
                    case "Next":
                        mobileShift = _context.MobileShift.OrderBy(o => o.MobileShiftId).FirstOrDefault(s => s.MobileShiftId > searchModel.Id);
                        break;
                    case "Previous":
                        mobileShift = _context.MobileShift.OrderByDescending(o => o.MobileShiftId).FirstOrDefault(s => s.MobileShiftId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MobileShiftModel>(mobileShift);
            return result;
        }
        [HttpPost]
        [Route("InsertMobileShift")]
        public MobileShiftModel Post(MobileShiftModel value)
        {
            var mobileShift = new MobileShift
            {
                Code = value.Code,
                Description = value.Description,
                PlantId = value.PlantId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value
            };
            _context.MobileShift.Add(mobileShift);
            _context.SaveChanges();
            value.MobileShiftId = mobileShift.MobileShiftId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMobileShift")]
        public MobileShiftModel Put(MobileShiftModel value)
        {
            var mobileShift = _context.MobileShift.SingleOrDefault(p => p.MobileShiftId == value.MobileShiftId);
            mobileShift.PlantId = value.PlantId;
            mobileShift.Code = value.Code;
            mobileShift.Description = value.Description;
            mobileShift.ModifiedByUserId = value.ModifiedByUserID;
            mobileShift.ModifiedDate = DateTime.Now;
            mobileShift.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSection")]
        public void Delete(int id)
        {
            try
            {
                var mobileShift = _context.MobileShift.SingleOrDefault(p => p.MobileShiftId == id);
                if (mobileShift != null)
                {
                    _context.MobileShift.Remove(mobileShift);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Mobile Shift Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
