﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductGroupingManufactureController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductGroupingManufactureController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductGroupingManufactures")]
        public List<ProductGroupingManufactureModel> GetProductGroupingManufactures(int id)
        {
            List<ProductGroupingManufactureModel> productGroupingManufactureModels = new List<ProductGroupingManufactureModel>();
            var productGroupingManufactures = _context.ProductGroupingManufacture.Include("AddedByUser").Include(s=>s.SupplyTo).Include(m => m.ManufactureBy).Include("ModifiedByUser").Include(d=>d.DosageForm).Include(u=>u.DrugClassification).Where(p => p.ProductGroupingId == id).OrderByDescending(o => o.ProductGroupingManufactureId).AsNoTracking().ToList();
            if (productGroupingManufactures != null && productGroupingManufactures.Count > 0)
            {
                productGroupingManufactures.ForEach(s =>
                {
                    ProductGroupingManufactureModel productGroupingManufactureModel = new ProductGroupingManufactureModel();

                    productGroupingManufactureModel.ProductGroupingManufactureId = s.ProductGroupingManufactureId;
                    productGroupingManufactureModel.ProductGroupingId = s.ProductGroupingId;
                    productGroupingManufactureModel.ManufactureById = s.ManufactureById;
                    productGroupingManufactureModel.ManufactureBy = s.ManufactureBy?.CompanyName;
                    productGroupingManufactureModel.SupplyToId = s.SupplyToId;
                    productGroupingManufactureModel.SupplyTo = s.SupplyTo?.CompanyName; ;
                    productGroupingManufactureModel.AddedByUserID = s.AddedByUserId;
                    productGroupingManufactureModel.ModifiedByUserID = s.ModifiedByUserId;
                    productGroupingManufactureModel.StatusCodeID = s.StatusCodeId;
                    productGroupingManufactureModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    productGroupingManufactureModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    productGroupingManufactureModel.AddedDate = s.AddedDate;
                    productGroupingManufactureModel.ModifiedDate = s.ModifiedDate;
                    productGroupingManufactureModel.DosageFormId = s.DosageFormId;
                    productGroupingManufactureModel.DrugClassificationId = s.DrugClassificationId;
                    productGroupingManufactureModel.DosageFormName = s.DosageForm?.Value;
                    productGroupingManufactureModel.DrugClassificationName = s.DrugClassification?.Value;
                    productGroupingManufactureModels.Add(productGroupingManufactureModel);


                });
            }

            return productGroupingManufactureModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertProductGroupingManufacture")]
        public ProductGroupingManufactureModel Post(ProductGroupingManufactureModel value)
        {
            var productGroupingManufacture = new ProductGroupingManufacture
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProductGroupingId = value.ProductGroupingId,
                ManufactureById = value.ManufactureById,
                SupplyToId = value.SupplyToId,
                DosageFormId=value.DosageFormId,
                DrugClassificationId=value.DrugClassificationId,
        };
            _context.ProductGroupingManufacture.Add(productGroupingManufacture);
            _context.SaveChanges();
            value.ProductGroupingManufactureId = productGroupingManufacture.ProductGroupingManufactureId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateProductGroupingManufacture")]
        public ProductGroupingManufactureModel Put(ProductGroupingManufactureModel value)
        {
            var productGroupingManufacture = _context.ProductGroupingManufacture.SingleOrDefault(p => p.ProductGroupingManufactureId == value.ProductGroupingManufactureId);
            productGroupingManufacture.ModifiedByUserId = value.ModifiedByUserID;
            productGroupingManufacture.ModifiedDate = DateTime.Now;
            productGroupingManufacture.StatusCodeId = value.StatusCodeID.Value;
            productGroupingManufacture.ProductGroupingId = value.ProductGroupingId;
            productGroupingManufacture.ManufactureById = value.ManufactureById;
            productGroupingManufacture.SupplyToId = value.SupplyToId;
            productGroupingManufacture.DrugClassificationId = value.DrugClassificationId;
            productGroupingManufacture.DosageFormId = value.DosageFormId;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteProductGroupingManufacture")]
        public void Delete(int id)
        {
            try
            {

                var productGroupingManufacture = _context.ProductGroupingManufacture.SingleOrDefault(p => p.ProductGroupingManufactureId == id);
                if (productGroupingManufacture != null)
                {
                    _context.ProductGroupingManufacture.Remove(productGroupingManufacture);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Record Cannot be delete! Reference To Others", ex);
            }
        }
    }
}
