﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesOrderProfileController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SalesOrderProfileController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetSalesOrderProfile")]
        public List<SalesOrderProfileModel> Get()
        {
            List<SalesOrderProfileModel> salesOrderProfileModels = new List<SalesOrderProfileModel>();
            var salesOrderProfile = _context.SalesOrderProfile.Include(a => a.AddedByUser).Include(a => a.ModifiedByUser).Include(s => s.OrderBy)
                .Include(s => s.StatusCode).Include(s => s.Profile).OrderByDescending(o => o.SalesOrderProfileId).AsNoTracking().ToList();
            if (salesOrderProfile != null && salesOrderProfile.Count > 0)
            {
                salesOrderProfile.ForEach(s =>
                {
                    SalesOrderProfileModel salesOrderProfileModel = new SalesOrderProfileModel();

                    salesOrderProfileModel.SalesOrderProfileID = s.SalesOrderProfileId;
                    salesOrderProfileModel.OrderByID = s.OrderById;
                    if (s.OrderById == 1403)
                    {
                        salesOrderProfileModel.TenderProfileID = s.ProfileId;
                    }
                    if (s.OrderById == 1402)
                    {
                        salesOrderProfileModel.CustomerProfileID = s.ProfileId;
                    }
                    if (s.OrderById == 1651)
                    {
                        salesOrderProfileModel.BlanketOrderProfileID = s.ProfileId;
                    }
                    salesOrderProfileModel.ProfileID = s.ProfileId;
                    salesOrderProfileModel.OrderBy = s.OrderBy?.CodeValue;
                    salesOrderProfileModel.ProfileName = s.Profile?.Name;
                    salesOrderProfileModel.AddedByUser = s.AddedByUser?.UserName;
                    salesOrderProfileModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    salesOrderProfileModel.StatusCode = s.StatusCode?.CodeValue;
                    salesOrderProfileModel.OrderBy = s.OrderBy?.CodeValue;
                    salesOrderProfileModel.ProfileName = s.Profile?.Name;
                    salesOrderProfileModels.Add(salesOrderProfileModel);
                });
            }


            return salesOrderProfileModels;
        }



        [HttpPost]
        [Route("InsertSalesOrderProfile")]
        public SalesOrderProfileModel Post(SalesOrderProfileModel value)
        {
            var SalesOrderProfile = new SalesOrderProfile
            {

                OrderById = value.OrderByID,
                ProfileId = value.ProfileID,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                ModifiedByUserId = value.ModifiedByUserID,
                AddedDate = DateTime.Now,

            };
            _context.SalesOrderProfile.Add(SalesOrderProfile);
            _context.SaveChanges();
            value.SalesOrderProfileID = SalesOrderProfile.SalesOrderProfileId;
            return value;
        }

        // PUT: api/User/5
        [HttpPost]
        [Route("UpdateSalesOrderProfile")]
        public SalesOrderProfileModel Put(SalesOrderProfileModel value)
        {
            var salesOrderProfile = _context.SalesOrderProfile.ToList();

            if (salesOrderProfile != null && salesOrderProfile.Count > 0)
            {
                salesOrderProfile.ForEach(s =>
                {
                    s.ModifiedByUserId = value.ModifiedByUserID;
                    s.ModifiedDate = DateTime.Now;
                    s.StatusCodeId = value.StatusCodeID;
                    if (s.OrderById == 1403)
                    {
                        s.ProfileId = value.TenderProfileID;
                    }
                    if (s.OrderById == 1402)
                    {
                        s.ProfileId = value.CustomerProfileID;
                    }
                    if (s.OrderById == 1651)
                    {
                        s.ProfileId = value.BlanketOrderProfileID;
                    }
                    _context.SaveChanges();
                });
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSalesOrderProfile")]
        public void Delete(int id)
        {
            var SalesOrderProfile = _context.SalesOrderProfile.SingleOrDefault(p => p.SalesOrderProfileId == id);
            if (SalesOrderProfile != null)
            {
                _context.SalesOrderProfile.Remove(SalesOrderProfile);
                _context.SaveChanges();
            }
        }
    }
}