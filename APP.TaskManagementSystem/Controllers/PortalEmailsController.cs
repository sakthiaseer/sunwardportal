﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PortalEmailsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public PortalEmailsController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetEmails")]
        public List<EmailsModel> Get()
        {
            List<EmailsModel> emailModels = new List<EmailsModel>();
            var email = _context.Emails.ToList();
            var emailAttachment = _context.EmailAttachment.ToList();
            email.ForEach(s =>
            {
                var emailAttachments = emailAttachment.Where(w => w.EmailsId == s.EmailsId).Count();
                EmailsModel emailModel = new EmailsModel();
                emailModel.EmailsId = s.EmailsId;
                emailModel.FormId = s.FormId;
                emailModel.Subject = s.Subject;
                emailModel.Message = s.Message;
                emailModel.ToMails = s.ToMails;
                emailModel.Ccmails = s.Ccmails;
                emailModel.Bccmails = s.Bccmails;
                emailModel.AddedByUserID = s.AddedByUserId;
                emailModel.AddedDate = s.AddedDate;
                emailModel.ModifiedByUserID = s.ModifiedByUserId;
                emailModel.ModifiedDate = s.ModifiedDate;
                emailModel.StatusCodeID = s.StatusCodeId;
                emailModel.SessionId = s.SessionId;
                emailModel.FromEmails = s.FromMails;
                emailModel.isAttachment = emailAttachments > 0 ? true : false;
                emailModels.Add(emailModel);
            });
            return emailModels.OrderByDescending(o => o.EmailsId).ToList();
        }
        [HttpGet]
        [Route("GetEmailsAttachment")]
        public List<DocumentsModel> GetEmailsAttachment(long? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var documents = _context.EmailAttachment.Include(s => s.Document)
                .Select(s => new
                {
                    s.EmailsId,
                    s.EmailAttachmentId,
                    s.Document.SessionId,
                    s.Document.DocumentId,
                    s.Document.FileName,
                    s.Document.ContentType,
                    s.Document.FileSize,
                    s.Document.UploadDate,
                    s.Document.FilterProfileTypeId,
                    s.Document.DocumentParentId,
                    s.Document.TableName,
                    s.Document.ExpiryDate,
                    s.Document.AddedByUserId,
                    s.Document.ModifiedByUserId,
                    s.Document.ModifiedDate,
                    IsLocked = s.Document.IsLocked,
                    LockedByUserId = s.Document.LockedByUserId,
                    LockedDate = s.Document.LockedDate,
                    s.Document.AddedDate,
                    s.Document.DepartmentId,
                    s.Document.WikiId,
                    s.Document.Extension,
                    s.Document.CategoryId,
                    s.Document.DocumentType,
                    s.Document.DisplayName,
                    s.Document.LinkId,
                    s.Document.IsSpecialFile,
                    s.Document.IsTemp,
                    s.Document.ReferenceNumber,
                    s.Document.Description,
                    s.Document.StatusCodeId,
                    s.Document.IsCompressed,
                    s.Document.IsVideoFile,
                }).Where(w => w.EmailsId == id).AsNoTracking().ToList();
            documents.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.DocumentId,
                    DepartmentID = s.DepartmentId,
                    WikiID = s.WikiId,
                    FileName = s.FileName,
                    Extension = s.Extension,
                    ContentType = s.ContentType,
                    CategoryID = s.CategoryId,
                    DocumentType = s.DocumentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    DisplayName = s.DisplayName,
                    SessionID = s.SessionId,
                    LinkID = s.LinkId,
                    IsSpecialFile = s.IsSpecialFile,
                    IsTemp = s.IsTemp,
                    IsCompressed = s.IsCompressed,
                    ReferenceNumber = s.ReferenceNumber,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = "Active",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsVideoFile = s.IsVideoFile
                };
                documentsModel.Add(documentsModels);
            });
            return documentsModel;
        }
        [HttpPost]
        [Route("InsertEmails")]
        public EmailsModel Post(EmailsModel value)
        {
            var SessionId = Guid.NewGuid();
            var formMaster = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).FirstOrDefault(w => w.ApplicationMaster.ApplicationMasterCodeId==313 && w.Value.ToLower().Trim() == value.FormId.ToLower().Trim());
            var profileId = formMaster?.ProfileId;
            if (formMaster != null && formMaster?.Value!=null)
            {
                var emailData = _context.Emails.Select(s => new { s.EmailsId, s.SessionId, s.FormId }).FirstOrDefault(f => f.FormId.ToLower().Trim() == value.FormId.ToLower().Trim());
                SessionId = emailData != null ? emailData.SessionId.Value : SessionId;
            }
            var emails = new Emails
            {
                Message = value.Message,
                Subject = value.Subject,
                ToMails = value.ToMails,
                Ccmails = value.Ccmails,
                Bccmails = value.Bccmails,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = 1,
                SessionId = SessionId,
                FromMails = value.FromEmails,
                FormId = value.FormId,
            };
            _context.Emails.Add(emails);
            _context.SaveChanges();
            value.EmailsId = emails.EmailsId;
            value.SessionId = emails.SessionId;
            
            if (value.emailAttachmentModels!=null && value.emailAttachmentModels.Count > 0)
            {
                value.emailAttachmentModels.ForEach(a =>
                {
                    var documentParentId = new long?();
                    var compressedData = DocumentZipUnZip.Zip(a.FileData);//Compress(document);
                    if (value.FormId != null)
                    {
                        var emailData = _context.Emails.Select(s => new { s.EmailsId, s.SessionId, s.FormId }).Where(f => f.FormId.ToLower().Trim() == value.FormId.ToLower().Trim()).ToList();
                        var emailIds = emailData.Select(s => s.EmailsId).ToList();
                        var documentsId = _context.EmailAttachment.Select(e => new { e.EmailsId, e.DocumentId }).Where(w => emailIds.Contains(w.EmailsId.Value)).Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
                        if (a.FileData.Length > 0)
                        {
                            if (emailData != null && emailData.Count > 0)
                            {
                                if (documentsId.Count > 0)
                                {
                                    documentParentId = _context.Documents.Select(d=>new { d.DocumentId,d.SessionId,d.IsLatest}).FirstOrDefault(z => z.SessionId == emailData[0].SessionId && z.IsLatest==true)?.DocumentId;
                                    var query = string.Format("Update Documents Set IsLatest = {0} Where DocumentId in" + '(' + "{1}" + ')', 0, string.Join(',', documentsId));
                                    _context.Database.ExecuteSqlRaw(query);
                                }
                            }
                        }
                    }
                    var profileNo = "";
                    if (profileId != null)
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Subject });
                    }
                    var documents = new Documents
                    {
                        FileName = a.FileName,
                        ContentType = a.ContentType,
                        FileData = compressedData,
                        FileSize = a.FileSize,
                        UploadDate = DateTime.Now,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsLatest = true,
                        AddedByUserId = value.AddedByUserID,
                        IsCompressed = true,
                        FileIndex = 0,
                        IsMainTask = false,
                        ProfileNo = profileNo,
                        DocumentParentId= documentParentId
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    var emailAttachment = new EmailAttachment
                    {
                        DocumentId = documents.DocumentId,
                        EmailsId = value.EmailsId,
                    };
                    _context.EmailAttachment.Add(emailAttachment);
                    _context.SaveChanges();

                });
            }
            return value;
        }
    }
}
