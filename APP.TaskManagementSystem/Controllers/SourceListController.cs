﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SourceListController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SourceListController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetSourceLists")]
        public List<SourceListModel> Get()
        {
            var sourceList = _context.SourceList.Include("AddedByUser").Include("ModifiedByUser").Select(s => new SourceListModel
            {
                SourceListID = s.SourceListId,
                SourceNo = s.SourceNo,
                OfficeAddressID = s.OfficeAddressId,
                SiteAddressID = s.SiteAddressId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                //CertificateList = s.IctcontactDetails.Where(c=>c.SourceListId == SourceListId).ToList(),
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,

            }).OrderByDescending(o => o.SourceListID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<SourceListModel>>(SourceList);
            return sourceList;
        }

        [HttpGet]
        [Route("GetMobileSourceLists1")]
        public List<SourceListMobile> GetSourceLists1()
        {
            var sourceList = _context.SourceList.Where(i => i.StatusCodeId == 1).Select(s => new SourceListMobile
            {
                SourceListID = s.SourceListId,
                Name = s.Name,

            }).OrderByDescending(o => o.SourceListID).AsNoTracking().ToList();
            return sourceList;
        }
        [HttpGet]
        [Route("GetMobileSourceLists")]
        public List<SourceListMobile> GetSourceLists()
        {
            var sourceList = _context.DeviceGroupList.Where(i => i.StatusCodeId == 1).Select(s => new SourceListMobile
            {
                SourceListID = s.DeviceGroupListId,
                Name = s.DeviceGroupName,

            }).OrderByDescending(o => o.SourceListID).AsNoTracking().ToList();
            return sourceList;
        }

        [HttpGet("GetSourceByID")]
        public ActionResult<SourceListModel> GetSource(int? id)
        {
            List<ICTCertificateModel> certificateList = new List<ICTCertificateModel>();
            List<ICTContactDetailsModel> contactList = new List<ICTContactDetailsModel>();
            if (id == null)
            {
                return NotFound();
            }
            var sourceList = _context.SourceList.SingleOrDefault(p => p.SourceListId == id.Value);
            if (sourceList != null)
            {
                certificateList = _context.Ictcertificate.Select(tn => new ICTCertificateModel
                {
                    ICTCertificateID = tn.IctcertificateId,
                    Name = tn.Name,
                    CertificateType = tn.CertificateType.Value,
                    SourceListID = tn.SourceListId,
                    VendorsListID = tn.VendorsListId,
                    //CertificateType = s.certificatety
                    Link = tn.Link,
                    IsExpired = tn.IsExpired.Value,
                    ExpiryDate = tn.ExpiryDate.Value,
                    AddedByUser = tn.AddedByUser.UserName,
                    ModifiedByUser = tn.ModifiedByUser.UserName,
                    AddedByUserID = tn.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = tn.StatusCodeId,
                    ModifiedByUserID = tn.ModifiedByUserId,
                    ModifiedDate = tn.ModifiedDate,
                }).Where(a => a.SourceListID == id).AsNoTracking().ToList();

                contactList = _context.IctcontactDetails.Select(s => new ICTContactDetailsModel
                {

                    ContactDetailsID = s.ContactDetailsId,
                    Department = s.Department,
                    Salutation = s.Salutation,
                    ContactName = s.ContactName,
                    Phone = s.Phone.Value,
                    VendorsListID = s.VendorsListId.Value,
                    SourceListID = s.SourceListId.Value,
                    ContactTypeID = s.ContactTypeId.Value,
                    Email = s.Email,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,


                }).Where(a => a.SourceListID == id).AsNoTracking().ToList();

            }
            var result = _mapper.Map<SourceListModel>(sourceList);
            if (certificateList.Count > 0)
            {
                result.CertificateList = certificateList;
            }
            if (contactList.Count > 0)
            {
                result.ContactList = contactList;
            }
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet]
        [Route("GetActiveSourceList")]
        public List<SourceListModel> GetActiveSourceList()
        {
            var sourceList = _context.SourceList.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).Select(s => new SourceListModel
            {
                SourceListID = s.SourceListId,
                SourceNo = s.SourceNo,
                OfficeAddressID = s.OfficeAddressId,
                SiteAddressID = s.SiteAddressId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUserID = s.AddedByUserId,
                AddedDate = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                ModifiedByUserID = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                Name = s.Name,

            }).OrderByDescending(o => o.SourceListID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<SourceListModel>>(SourceList);
            return sourceList;
        }

        // GET: api/Project/2


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SourceListModel> GetData(SearchModel searchModel)
        {
            var sourceList = new SourceList();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).FirstOrDefault();
                        break;
                    case "Last":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).LastOrDefault();
                        break;
                    case "Next":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).LastOrDefault();
                        break;
                    case "Previous":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).FirstOrDefault();
                        break;
                    case "Last":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).LastOrDefault();
                        break;
                    case "Next":
                        sourceList = _context.SourceList.OrderBy(o => o.SourceListId).FirstOrDefault(s => s.SourceListId > searchModel.Id);
                        break;
                    case "Previous":
                        sourceList = _context.SourceList.OrderByDescending(o => o.SourceListId).FirstOrDefault(s => s.SourceListId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SourceListModel>(sourceList);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSourceList")]
        public SourceListModel Post(SourceListModel value)
        {
            var address = new Address
            {
                AddressType = value.OfficeAddress.AddressType,
                PostCode = value.OfficeAddress.PostCode,
                Address1 = value.OfficeAddress.Address1,
                Address2 = value.OfficeAddress.Address2,
                City = value.OfficeAddress.City,
                CountryCode = value.OfficeAddress.CountryCode,
                Country = value.OfficeAddress.Country,
                OfficePhone = value.OfficeAddress.OfficePhone,
                Email = value.OfficeAddress.Email,
                Website = value.OfficeAddress.Website,

            };
            _context.Address.Add(address);
            _context.SaveChanges();
            value.OfficeAddress.AddressID = address.AddressId;
            var siteaddress = new Address
            {
                AddressType = value.SiteAddress.AddressType,
                PostCode = value.SiteAddress.PostCode,
                Address1 = value.SiteAddress.Address1,
                Address2 = value.SiteAddress.Address2,
                City = value.SiteAddress.City,
                CountryCode = value.SiteAddress.CountryCode,
                Country = value.SiteAddress.Country,
                OfficePhone = value.SiteAddress.OfficePhone,
                Email = value.SiteAddress.Email,
                Website = value.SiteAddress.Website,

            };
            _context.Address.Add(siteaddress);
            _context.SaveChanges();
            value.SiteAddress.AddressID = address.AddressId;
            var sourceList = new SourceList
            {

                SourceNo = value.SourceNo,
                OfficeAddressId = value.OfficeAddress.AddressID,
                SiteAddressId = value.SiteAddress.AddressID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Name = value.Name,

            };
            _context.SourceList.Add(sourceList);
            _context.SaveChanges();
            value.SourceListID = sourceList.SourceListId;

            var contactList = value.ContactList;
            if (contactList.Count != 0)
            {
                contactList.ForEach(contact =>
                {
                    if (contact.ContactDetailsID < 0)
                    {
                        var contactDetails = new IctcontactDetails
                        {

                            Department = contact.Department,
                            Salutation = contact.Salutation,
                            ContactName = contact.ContactName,
                            Phone = contact.Phone,
                            ContactTypeId = contact.ContactTypeID,
                            // VendorsListId = contact.VendorsListID,
                            SourceListId = value.SourceListID,
                            Email = contact.Email,
                            //ContactTypeId = value.ContactTypeID
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,



                        };
                        _context.IctcontactDetails.Add(contactDetails);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == contact.ContactDetailsID);
                        contactDetails.Department = contact.Department;
                        contactDetails.Salutation = contact.Salutation;
                        contactDetails.ContactTypeId = contact.ContactTypeID;
                        // ICTContactDetails.ContactDetailsId = value.ContactDetailsID;
                        contactDetails.ContactName = contact.ContactName;
                        contactDetails.Phone = contact.Phone;
                        contactDetails.VendorsListId = contact.VendorsListID;
                        contactDetails.SourceListId = contact.SourceListID;
                        contactDetails.Email = contact.Email;
                        //ICTContactDetails.ContactTypeId = value.ContactTypeID;
                        contactDetails.ModifiedByUserId = contact.ModifiedByUserID;
                        contactDetails.ModifiedDate = DateTime.Now;

                        contactDetails.StatusCodeId = contact.StatusCodeID.Value;
                        _context.SaveChanges();

                    }

                }


                );
            }


            var certificatelist = value.CertificateList;
            if (certificatelist.Count != 0)
            {
                certificatelist.ForEach(certificate =>
                {
                    var iCTCertificate = new Ictcertificate
                    {


                        CertificateType = certificate.CertificateType,
                        Name = certificate.Name,
                        Link = certificate.Link,
                        IsExpired = certificate.IsExpired,
                        IssueDate = certificate.IssueDate,
                        ExpiryDate = certificate.ExpiryDate,
                        AddedByUserId = certificate.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = certificate.StatusCodeID.Value,
                        SourceListId = value.SourceListID,

                    };
                    _context.Ictcertificate.Add(iCTCertificate);
                    _context.SaveChanges();
                    //value.ICTCertificateID = iCTCertificate.IctcertificateId;
                }
                 );
            }

            return value;
        }







        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSourceList")]
        public SourceListModel Put(SourceListModel value)
        {
            var sourceList = _context.SourceList.SingleOrDefault(p => p.SourceListId == value.SourceListID);
            sourceList.SourceNo = value.SourceNo;
            sourceList.OfficeAddressId = value.OfficeAddressID;
            sourceList.SiteAddressId = value.SiteAddressID;
            sourceList.ModifiedByUserId = value.ModifiedByUserID;
            sourceList.ModifiedDate = DateTime.Now;
            sourceList.Name = value.Name;
            sourceList.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            var contactList = value.ContactList;
            if (contactList.Count != 0)
            {
                contactList.ForEach(contact =>
                {
                    if (contact.ContactDetailsID < 0)
                    {
                        var contactDetails = new IctcontactDetails
                        {

                            Department = contact.Department,
                            Salutation = contact.Salutation,
                            ContactName = contact.ContactName,
                            Phone = contact.Phone,
                            ContactTypeId = contact.ContactTypeID,
                            // VendorsListId = contact.VendorsListID,
                            SourceListId = value.SourceListID,
                            Email = contact.Email,
                            //ContactTypeId = value.ContactTypeID
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,



                        };
                        _context.IctcontactDetails.Add(contactDetails);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var contactDetails = _context.IctcontactDetails.SingleOrDefault(p => p.ContactDetailsId == contact.ContactDetailsID);
                        contactDetails.Department = contact.Department;
                        contactDetails.Salutation = contact.Salutation;
                        // ICTContactDetails.ContactDetailsId = value.ContactDetailsID;
                        contactDetails.ContactName = contact.ContactName;
                        contactDetails.Phone = contact.Phone;
                        contactDetails.ContactTypeId = contact.ContactTypeID;
                        contactDetails.VendorsListId = contact.VendorsListID;
                        contactDetails.SourceListId = contact.SourceListID;
                        contactDetails.Email = contact.Email;
                        //ICTContactDetails.ContactTypeId = value.ContactTypeID;
                        contactDetails.ModifiedByUserId = contact.ModifiedByUserID;
                        contactDetails.ModifiedDate = DateTime.Now;

                        contactDetails.StatusCodeId = contact.StatusCodeID.Value;
                        _context.SaveChanges();

                    }

                }


                );
            }


            var certificatelist = value.CertificateList;
            if (certificatelist.Count != 0)
            {
                certificatelist.ForEach(certificate =>
                {
                    if (certificate.ICTCertificateID < 0)
                    {
                        var iCTCertificate = new Ictcertificate
                        {


                            CertificateType = certificate.CertificateType,
                            Name = certificate.Name,
                            Link = certificate.Link,
                            IsExpired = certificate.IsExpired,
                            IssueDate = certificate.IssueDate,
                            ExpiryDate = certificate.ExpiryDate,
                            AddedByUserId = certificate.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = certificate.StatusCodeID.Value,
                            SourceListId = value.SourceListID,

                        };
                        _context.Ictcertificate.Add(iCTCertificate);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var iCTCertificate = _context.Ictcertificate.SingleOrDefault(p => p.IctcertificateId == certificate.ICTCertificateID);
                        iCTCertificate.Name = certificate.Name;
                        iCTCertificate.CertificateType = certificate.CertificateType;
                        iCTCertificate.Link = certificate.Link;
                        iCTCertificate.IsExpired = certificate.IsExpired;
                        iCTCertificate.ExpiryDate = certificate.ExpiryDate;
                        iCTCertificate.ModifiedByUserId = certificate.ModifiedByUserID;
                        iCTCertificate.ModifiedDate = DateTime.Now;
                        iCTCertificate.StatusCodeId = certificate.StatusCodeID.Value;
                        iCTCertificate.SourceListId = certificate.SourceListID.Value;
                        iCTCertificate.VendorsListId = certificate.VendorsListID;
                        _context.SaveChanges();
                    }
                    //value.ICTCertificateID = iCTCertificate.IctcertificateId;
                }
                 );
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSourceList")]
        public void Delete(int id)
        {

            var sourceList = _context.SourceList.SingleOrDefault(p => p.SourceListId == id);
            if (sourceList != null)
            {
                var officeaddress = _context.Address.SingleOrDefault(p => p.AddressId == sourceList.OfficeAddressId);
                if (officeaddress != null)
                {
                    _context.Address.Remove(officeaddress);
                    _context.SaveChanges();
                }
                var siteaddress = _context.Address.SingleOrDefault(p => p.AddressId == sourceList.SiteAddressId);
                if (siteaddress != null)
                {
                    _context.Address.Remove(siteaddress);
                    _context.SaveChanges();
                }
                var certificate = _context.Ictcertificate.SingleOrDefault(p => p.SourceListId == sourceList.SourceListId);
                if (certificate != null)
                {
                    _context.Ictcertificate.Remove(certificate);
                    _context.SaveChanges();
                }
                var contact = _context.IctcontactDetails.SingleOrDefault(p => p.SourceListId == sourceList.SourceListId);
                if (contact != null)
                {
                    _context.IctcontactDetails.Remove(contact);
                    _context.SaveChanges();
                }
                _context.SourceList.Remove(sourceList);
                _context.SaveChanges();
            }
        }
    }
}