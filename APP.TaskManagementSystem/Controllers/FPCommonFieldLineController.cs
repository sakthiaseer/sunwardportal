﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FPCommonFieldLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FPCommonFieldLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetFPCommonFieldLines")]
        public List<FPCommonFieldLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var FpcommonFieldLine = _context.FpcommonFieldLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<FPCommonFieldLineModel> FPCommonFieldLineModel = new List<FPCommonFieldLineModel>();
            FpcommonFieldLine.ForEach(s =>
            {
                FPCommonFieldLineModel FPCommonFieldLineModels = new FPCommonFieldLineModel
                {
                    FPCommonFieldLineID = s.FpcommonFieldLineId,
                    FPCommonFiledID = s.FpcommonFiledId,
                    MaterialNoID = s.MaterialNoId,
                    MaterialName = s.MaterialName,
                    DosageNo = s.DosageNo,
                    DosageUnitID = s.DosageUnitId,
                    PerDosageID = s.PerDosageId,
                    Overage = s.Overage,
                    OverageUnitsID = s.OverageUnitsId,
                    OverageDosageUnitsID = s.OverageDosageUnitsId,
                    DosageUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DosageUnitId).Select(a => a.Value).SingleOrDefault() : "",
                    OverageDosageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.OverageDosageUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                    PerDosage = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PerDosageId).Select(a => a.Value).SingleOrDefault() : "",
                    OverageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.OverageUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                };
                FPCommonFieldLineModel.Add(FPCommonFieldLineModels);
            });
            return FPCommonFieldLineModel.Where(f => f.FPCommonFiledID == id).OrderByDescending(o => o.FPCommonFieldLineID).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPCommonFieldLineModel> GetData(SearchModel searchModel)
        {
            var FpcommonFieldLine = new FpcommonFieldLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).LastOrDefault();
                        break;
                    case "Next":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).LastOrDefault();
                        break;
                    case "Previous":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).LastOrDefault();
                        break;
                    case "Next":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderBy(o => o.FpcommonFieldLineId).FirstOrDefault(s => s.FpcommonFieldLineId > searchModel.Id);
                        break;
                    case "Previous":
                        FpcommonFieldLine = _context.FpcommonFieldLine.OrderByDescending(o => o.FpcommonFieldLineId).FirstOrDefault(s => s.FpcommonFieldLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPCommonFieldLineModel>(FpcommonFieldLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFPCommonFieldLine")]
        public FPCommonFieldLineModel Post(FPCommonFieldLineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var codeMaster = _context.CodeMaster.ToList();
            var userlist = _context.ApplicationUser.ToList();
            var FpcommonFieldLine = new FpcommonFieldLine
            {

                FpcommonFiledId = value.FPCommonFiledID,
                MaterialNoId = value.MaterialNoID,
                MaterialName = value.MaterialName,
                DosageNo = value.DosageNo,
                DosageUnitId = value.DosageUnitID,
                OverageDosageUnitsId = value.OverageDosageUnitsID,
                OverageUnitsId = value.OverageUnitsID,
                Overage = value.Overage,
                PerDosageId = value.PerDosageID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.FpcommonFieldLine.Add(FpcommonFieldLine);
            _context.SaveChanges();
            value.AddedByUser = userlist.Where(u => u.UserId == value.AddedByUserID).Select(u => u.UserName).FirstOrDefault();
            value.StatusCode = codeMaster.Where(c => c.CodeId == value.StatusCodeID).Select(c => c.CodeValue).FirstOrDefault();
            value.FPCommonFieldLineID = FpcommonFieldLine.FpcommonFieldLineId;
            value.DosageUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.DosageUnitID).Select(a => a.Value).SingleOrDefault() : "";
            value.OverageDosageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.OverageDosageUnitsID).Select(a => a.Value).SingleOrDefault() : "";
            value.PerDosage = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PerDosageID).Select(a => a.Value).SingleOrDefault() : "";
            value.OverageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.OverageUnitsID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFPCommonFieldLine")]
        public FPCommonFieldLineModel Put(FPCommonFieldLineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var codeMaster = _context.CodeMaster.ToList();
            var userlist = _context.ApplicationUser.ToList();
            var FpcommonFieldLine = _context.FpcommonFieldLine.SingleOrDefault(p => p.FpcommonFieldLineId == value.FPCommonFieldLineID);
            FpcommonFieldLine.FpcommonFiledId = value.FPCommonFiledID;
            FpcommonFieldLine.DosageNo = value.DosageNo;
            FpcommonFieldLine.MaterialNoId = value.MaterialNoID;
            FpcommonFieldLine.MaterialName = value.MaterialName;
            FpcommonFieldLine.DosageUnitId = value.DosageUnitID;
            FpcommonFieldLine.PerDosageId = value.PerDosageID;
            FpcommonFieldLine.Overage = value.Overage;
            FpcommonFieldLine.OverageDosageUnitsId = value.OverageDosageUnitsID;
            FpcommonFieldLine.OverageUnitsId = value.OverageUnitsID;
            FpcommonFieldLine.ModifiedByUserId = value.ModifiedByUserID;
            FpcommonFieldLine.ModifiedDate = DateTime.Now;
            FpcommonFieldLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            value.AddedByUser = userlist.Where(u => u.UserId == value.AddedByUserID).Select(u => u.UserName).FirstOrDefault();
            value.StatusCode = codeMaster.Where(c => c.CodeId == value.StatusCodeID).Select(c => c.CodeValue).FirstOrDefault();
            value.DosageUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.DosageUnitID).Select(a => a.Value).SingleOrDefault() : "";
            value.OverageDosageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.OverageDosageUnitsID).Select(a => a.Value).SingleOrDefault() : "";
            value.PerDosage = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PerDosageID).Select(a => a.Value).SingleOrDefault() : "";
            value.OverageUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.OverageUnitsID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFPCommonFieldLine")]
        public void Delete(int id)
        {
            var FpcommonFieldLine = _context.FpcommonFieldLine.SingleOrDefault(p => p.FpcommonFieldLineId == id);
            if (FpcommonFieldLine != null)
            {
                _context.FpcommonFieldLine.Remove(FpcommonFieldLine);
                _context.SaveChanges();
            }
        }
    }
}