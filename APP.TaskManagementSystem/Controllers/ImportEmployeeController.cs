﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportEmployeeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private Microsoft.Extensions.Configuration.IConfiguration _config;

        public ImportEmployeeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        [HttpPost]
        [Route("ImportuploadDocuments")]
        public void ImportuploadDocuments(IFormCollection files)
        {
            var count = 0;
            List<HeaderList> headerList = new List<HeaderList>();
            List<JsonList> JsonList = new List<JsonList>();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            JsonList Json3 = new JsonList();
            Json3.EntityName = "SageID";
            Json3.ColumnId = 1;
            JsonList.Add(Json3);
            JsonList Json = new JsonList();
            Json.EntityName = "FirstName";
            Json.ColumnId = 2;
            JsonList.Add(Json);
            JsonList Json1 = new JsonList();
            Json1.EntityName = "NickName";
            Json1.ColumnId = 3;
            JsonList.Add(Json1);
            JsonList Json2 = new JsonList();
            Json2.EntityName = "LastName";
            Json2.ColumnId = 4;
            JsonList.Add(Json2);
            files.Files.ToList().ForEach(f =>
            {
                if (count == 0)
                {
                    var file = f;
                    var extension = Path.GetExtension(file.FileName);
                    if (extension == ".xlsx")
                    {
                        var fs = file.OpenReadStream();
                        using (var package = new ExcelPackage(fs))
                        {
                            var worksheets = package.Workbook.Worksheets.Select(x => x.Name).ToList();
                            var count = worksheets.Count();
                            if (count > 0)
                            {
                                var oSheet = package.Workbook.Worksheets[worksheets[0]];
                                if (oSheet != null)
                                {
                                    ExcelWorksheet worksheet = oSheet;
                                    var rowCount = worksheet.Dimension.Rows;
                                    for (int i = 1; i <= worksheet.Dimension.End.Column; i++)
                                    {
                                        headerList.Add(new HeaderList
                                        {
                                            Text = worksheet.Cells[1, i].Value.ToString(),
                                            ID = i
                                        });
                                    }
                                    for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                                    {
                                        EmployeeModel employeeModel = new EmployeeModel();
                                        employeeModel.EmployeeID = 1;
                                        JsonList.ForEach(j =>
                                        {
                                            if (j.EntityName.ToLower() == "SageID".ToLower())
                                            {
                                                employeeModel.SageID = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                            }
                                            if (j.EntityName.ToLower() == "FirstName".ToLower())
                                            {
                                                employeeModel.FirstName = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                            }
                                            if (j.EntityName.ToLower() == "LastName".ToLower())
                                            {
                                                employeeModel.LastName = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                            }
                                            if (j.EntityName.ToLower() == "NickName".ToLower())
                                            {
                                                employeeModel.NickName = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                            }
                                        });
                                        employeeModels.Add(employeeModel);
                                    }
                                }
                            }
                            else
                            {
                                throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                            }
                        }
                    }
                    else
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!.It  will support only (.xlsx) Extension", null);
                    }
                }
                count++;
            });
            int i = 1;
            var password = "1234";
            employeeModels.ForEach(s =>
            {
                ApplicationUser applicationUser = new ApplicationUser
                {
                    UserName = s.FirstName,
                    UserCode = s.SageID,
                    LoginId = s.SageID,
                    IsPasswordChanged = false,
                    LoginPassword = EncryptDecryptPassword.Encrypt(password),
                    SessionId = Guid.NewGuid(),
                    StatusCodeId = 1,
                    AddedByUserId = 1,
                    AddedDate = DateTime.Now,
                };
                _context.ApplicationUser.Add(applicationUser);
                _context.SaveChanges();
                Employee historicalDetail = new Employee
                {
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    SageId = s.SageID,
                    NickName = s.NickName,
                    AddedByUserId = 1,
                    AddedDate = DateTime.Now,
                    UserId = applicationUser.UserId,
                    StatusCodeId = 1,
                    DepartmentId=1,
                    DesignationId=1,
                    PlantId=1,
                };
                _context.Employee.Add(historicalDetail);
                _context.SaveChanges();
                i++;
            });
        }
    }
}
