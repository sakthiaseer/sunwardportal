﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppSamplingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public AppSamplingController(CRT_TMSContext context, IMapper mapper, IConfiguration config, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAppSamplings")]
        public List<AppSamplingModel> Get()
        {
            List<AppSamplingModel> appSamplingModels = new List<AppSamplingModel>();
            var appSamplings = _context.AppSampling.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.SamplingId).AsNoTracking().ToList();

            appSamplings.ForEach(s =>
            {
                var appSamplingModel = new AppSamplingModel
                {
                    SamplingId = s.SamplingId,
                    ScanDocument = s.ScanDocument,
                    TicketNo = s.TicketNo,
                    SublotNo = s.SublotNo,
                    BatchNo = s.BatchNo,
                    ItemCode = s.ItemCode,
                    ItemDescription = s.ItemDescription,
                    StartDate = s.StartDate,
                    ActualStartDate = s.ActualStartDate,
                    EndDate = s.EndDate,
                    CompanyName = s.Company,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                appSamplingModels.Add(appSamplingModel);
            });
            return appSamplingModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppSamplingModel> GetData(SearchModel searchModel)
        {
            var appSampling = new AppSampling();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).FirstOrDefault();
                        break;
                    case "Last":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).LastOrDefault();
                        break;
                    case "Next":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).LastOrDefault();
                        break;
                    case "Previous":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).FirstOrDefault();
                        break;
                    case "Last":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).LastOrDefault();
                        break;
                    case "Next":
                        appSampling = _context.AppSampling.OrderBy(o => o.SamplingId).FirstOrDefault(s => s.SamplingId > searchModel.Id);
                        break;
                    case "Previous":
                        appSampling = _context.AppSampling.OrderByDescending(o => o.SamplingId).FirstOrDefault(s => s.SamplingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppSamplingModel>(appSampling);
            return result;
        }

        [HttpPost]
        [Route("GetAppSamplingItem")]
        public async Task<AppSamplingModel> GetAppSamplingItem(AppSamplingModel samplingModel)
        {

            var s = _context.AppSampling.FirstOrDefault(c => c.TicketNo == samplingModel.TicketNo && c.SublotNo == samplingModel.SublotNo && c.BatchNo == samplingModel.BatchNo);

            if (s != null)
            {
                var productionOutputModelItem = new AppSamplingModel
                {
                    ScanDocument = s.ScanDocument,
                    TicketNo = s.TicketNo,
                    SublotNo = s.SublotNo,
                    BatchNo = s.BatchNo,
                    ItemCode = s.ItemCode,
                    ItemDescription = s.ItemDescription,
                    StartDate = s.StartDate,
                    ActualStartDate = s.ActualStartDate,
                    EndDate = s.EndDate,
                    CompanyName = s.Company,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };

                return productionOutputModelItem;
            }
            return new AppSamplingModel();
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertAppSampling")]
        public AppSamplingModel Post(AppSamplingModel value)
        {
            try
            {
                if (_context.AppSampling.Any(c => c.TicketNo == value.TicketNo && c.SublotNo == value.SublotNo && c.BatchNo == value.BatchNo))
                {
                    var samplingItem = _context.AppSampling.FirstOrDefault(c => c.TicketNo == value.TicketNo && c.SublotNo == value.SublotNo && c.BatchNo == value.BatchNo);
                    value.SamplingId = samplingItem.SamplingId;
                    return Put(value);
                }


                var appSampling = new AppSampling
                {
                    ScanDocument = value.ScanDocument,
                    TicketNo = value.TicketNo,
                    SublotNo = value.SublotNo,
                    BatchNo = value.BatchNo,
                    ItemCode = value.ItemCode,
                    ItemDescription = value.ItemDescription,
                    StartDate = value.StartDate,
                    ActualStartDate = value.ActualStartDate,
                    EndDate = value.EndDate,
                    Company = value.Company,
                    SessionId = value.SessionId,
                    ModifiedByUserId = value.AddedByUserID,
                    AddedByUserId = value.AddedByUserID,
                    StatusCodeId = value.StatusCodeID,
                    AddedDate = value.AddedDate,
                    ModifiedDate = value.ModifiedDate,
                };

                _context.AppSampling.Add(appSampling);
                _context.SaveChanges();
                value.SamplingId = appSampling.SamplingId;
                return value;
            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppSampling")]
        public AppSamplingModel Put(AppSamplingModel value)
        {
            var appSampling = _context.AppSampling.SingleOrDefault(p => p.SamplingId == value.SamplingId);
            appSampling.ScanDocument = value.ScanDocument;
            appSampling.TicketNo = value.TicketNo;
            appSampling.SublotNo = value.SublotNo;
            appSampling.BatchNo = value.BatchNo;
            appSampling.ItemCode = value.ItemCode;
            appSampling.ItemDescription = value.ItemDescription;
            appSampling.StartDate = value.StartDate;
            appSampling.ActualStartDate = value.ActualStartDate;
            appSampling.EndDate = value.EndDate;
            appSampling.Company = value.Company;
            appSampling.SessionId = value.SessionId;
            appSampling.ModifiedByUserId = value.AddedByUserID;
            appSampling.AddedByUserId = value.AddedByUserID;
            appSampling.StatusCodeId = value.StatusCodeID;
            appSampling.AddedDate = value.AddedDate;
            appSampling.ModifiedDate = value.ModifiedDate;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppSampling")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var appSampling = _context.AppSampling.Include(s => s.AppSamplingLine).SingleOrDefault(p => p.SamplingId == id);
                if (appSampling != null)
                {
                    if (appSampling.AppSamplingLine != null)
                    {
                        _context.AppSamplingLine.RemoveRange(appSampling.AppSamplingLine);
                        _context.SaveChanges();
                    }
                    _context.AppSampling.Remove(appSampling);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
