﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppSamplingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        private readonly IHostingEnvironment _hostingEnvironment;
        //public NAV.NAV Context { get; private set; }
        private readonly IConfiguration _config;

        public AppSamplingLineController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;

            _hostingEnvironment = host;

            _config = config;

        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAppSamplingLines")]
        public List<AppSamplingLineModel> Get()
        {
            var appSampleLines = _context.AppSamplingLine.Include(a => a.AddedByUser).Include(p => p.SamplingPurpose).Select(s => new AppSamplingLineModel
            {
                AppSamplingId = s.AppSamplingId,
                AppSamplingLineId = s.AppSamplingLineId,
                SamplingPurposeId = s.SamplingPurposeId,
                SamplingPurpose = s.SamplingPurpose != null ? s.SamplingPurpose.Description : string.Empty,
                DrumNo = s.DrumNo,
                QcsampleNo = s.QcsampleNo,
                Qty = s.Qty,
                Uom = s.Uom,
                AddedByUserID = s.AddedByUserId,
                AddedDate = s.AddedDate,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty

            }).OrderByDescending(o => o.AppSamplingLineId).AsNoTracking().ToList();

            return appSampleLines;
        }



        [HttpGet]
        [Route("GetAppSamplingLinesById")]
        public List<AppSamplingLineModel> GetAppSamplingLinesById(int id)
        {
            List<AppSamplingLineModel> appSamplingLineModels = new List<AppSamplingLineModel>();
            var appSamplingLines = _context.AppSamplingLine.Include(a => a.AddedByUser).Include(o => o.SamplingPurpose).OrderByDescending(o => o.AppSamplingLineId).Where(t => t.AppSamplingId == id).AsNoTracking().ToList();
            appSamplingLines.ForEach(s =>
            {
                AppSamplingLineModel appSamplingLineModel = new AppSamplingLineModel()
                {
                    AppSamplingId = s.AppSamplingId,
                    AppSamplingLineId = s.AppSamplingLineId,
                    DrumNo = s.DrumNo,
                    SamplingPurposeId = s.SamplingPurposeId,
                    SamplingPurpose = s.SamplingPurpose != null ? s.SamplingPurpose.Description : string.Empty,
                    QcsampleNo = s.QcsampleNo,
                    Qty = s.Qty,
                    Uom = s.Uom,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty,
                };
                appSamplingLineModels.Add(appSamplingLineModel);
            });

            return appSamplingLineModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppSamplingLineModel> GetData(SearchModel searchModel)
        {
            var appSamplingLine = new AppSamplingLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).LastOrDefault();
                        break;
                    case "Next":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).LastOrDefault();
                        break;
                    case "Previous":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).LastOrDefault();
                        break;
                    case "Next":
                        appSamplingLine = _context.AppSamplingLine.OrderBy(o => o.AppSamplingLineId).FirstOrDefault(s => s.AppSamplingLineId > searchModel.Id);
                        break;
                    case "Previous":
                        appSamplingLine = _context.AppSamplingLine.OrderByDescending(o => o.AppSamplingLineId).FirstOrDefault(s => s.AppSamplingLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppSamplingLineModel>(appSamplingLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppSamplingLine")]
        public async Task<AppSamplingLineModel> Post(AppSamplingLineModel value)
        {
            if (value.AddedByUserID == null)
            {
                var consumptionEntry = _context.AppSampling.FirstOrDefault(c => c.SamplingId == value.AppSamplingId);
                value.AddedByUserID = consumptionEntry.AddedByUserId;
            }
            var samplingLines = _context.AppSamplingLine.Where(p => p.AppSamplingLineId == value.AppSamplingId).ToList();
            string date = DateTime.Now.Date.ToString();
            string time = DateTime.Now.TimeOfDay.ToString();
            var appSamplingLine = new AppSamplingLine
            {
                AppSamplingId = value.AppSamplingId,
                SamplingPurposeId = value.SamplingPurposeId,
                DrumNo = value.DrumNo,
                QcsampleNo = date + "/" + time + "/" + samplingLines.Count() + 1,
                Qty = value.Qty,
                Uom = value.Uom,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.AppSamplingLine.Add(appSamplingLine);
            _context.SaveChanges();
            value.AppSamplingLineId = appSamplingLine.AppSamplingLineId;
            value.QcsampleNo = appSamplingLine.QcsampleNo;
            return value;

        }





        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppSamplingLine")]
        public AppSamplingLineModel Put(AppSamplingLineModel value)
        {
            var samplingLine = _context.AppSamplingLine.SingleOrDefault(p => p.AppSamplingLineId == value.AppSamplingLineId);

            samplingLine.AppSamplingId = value.AppSamplingId;
            samplingLine.DrumNo = value.DrumNo;
            samplingLine.QcsampleNo = value.QcsampleNo;
            samplingLine.Qty = value.Qty;
            samplingLine.Uom = value.Uom;
            samplingLine.SamplingPurposeId = value.SamplingPurposeId;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppSamplingLine")]
        public void Delete(int id)
        {
            var appSamplingLine = _context.AppSamplingLine.SingleOrDefault(p => p.AppSamplingLineId == id);
            var errorMessage = "";
            try
            {
                if (appSamplingLine != null)
                {
                    _context.AppSamplingLine.Remove(appSamplingLine);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}