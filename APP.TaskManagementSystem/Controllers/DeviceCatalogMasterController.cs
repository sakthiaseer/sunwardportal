﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DeviceCatalogMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public DeviceCatalogMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDeviceCatalogMasters")]
        public List<DeviceCatalogMasterModel> Get()
        {
            //var deviceCatalogMaster = _context.DeviceCatalogMaster.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new DeviceCatalogMasterModel
            var deviceCatalogMaster = _context.DeviceCatalogMaster.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DeviceCatalogMasterModel> deviceCatalogMasterModel = new List<DeviceCatalogMasterModel>();
            deviceCatalogMaster.ForEach(s =>
            {
                DeviceCatalogMasterModel deviceCatalogMasterModels = new DeviceCatalogMasterModel
                {
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    DeviceGroupListID = s.DeviceGroupListId,
                    DeviceName = s.DeviceGroupList.DeviceGroupName,
                    ModelName = s.ModelName,
                    SourceListID = s.SourceListId,
                    ManufacturerName = s.SourceList.Name,
                    CalibrationTypeID = s.CalibrationTypeId,
                    CalibrationTypeName = s.CalibrationType.Name,
                    Cataloglink = s.Cataloglink,
                    Status = s.StatusCode.CodeValue,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                };
                deviceCatalogMasterModel.Add(deviceCatalogMasterModels);
            });
            return deviceCatalogMasterModel.OrderByDescending(a => a.DeviceCatalogMasterID).ToList();
        }
        [HttpGet]
        [Route("GetDeviceCatalogByID")]
        public ActionResult<DeviceCatalogMasterModel> GetById(int id)
        {
            var deviceCatalogMastermodel = new DeviceCatalogMasterModel();
            var deviceCatalogMaster = _context.DeviceCatalogMaster.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DeviceCatalogMasterModel> DeviceCatalogMasterModel = new List<DeviceCatalogMasterModel>();
            deviceCatalogMaster.ForEach(s =>
            {
                DeviceCatalogMasterModel DeviceCatalogMasterModels = new DeviceCatalogMasterModel
                {
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    DeviceGroupListID = s.DeviceGroupListId,
                    DeviceName = s.DeviceGroupList.DeviceGroupName,
                    ModelName = s.ModelName,
                    SourceListID = s.SourceListId,
                    ManufacturerName = s.SourceList.Name,
                    CalibrationTypeID = s.CalibrationTypeId,
                    CalibrationTypeName = s.CalibrationType.Name,
                    Cataloglink = s.Cataloglink,
                    Status = s.StatusCode.CodeValue,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                };
                DeviceCatalogMasterModel.Add(DeviceCatalogMasterModels);
            });
            DeviceCatalogMasterModel.OrderByDescending(o => o.DeviceCatalogMasterID).Where(t => t.DeviceCatalogMasterID == id).FirstOrDefault();
            var DeviceinstructionType = _context.InstructionType.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<InstructionTypeModel> InstructionTypeModel = new List<InstructionTypeModel>();
            DeviceinstructionType.ForEach(s =>
            {
                InstructionTypeModel InstructionTypeModels = new InstructionTypeModel
                {
                    InstructionTypeID = s.InstructionTypeId,
                    DocumentName = s.DocumentName,
                    TypeID = s.TypeId,
                    No = s.No,
                    InstructionNo = s.InstructionNo,
                    VersionNo = s.VersionNo,
                    EffectiveDate = s.EffectiveDate,
                    Link = s.Link,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,

                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                InstructionTypeModel.Add(InstructionTypeModels);
            });
            InstructionTypeModel.OrderByDescending(o => o.InstructionTypeID).Where(t => t.DeviceCatalogMasterID == id && t.TypeID == 620).ToList();
            var CalibrationServiceinstructionType = _context.InstructionType.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<InstructionTypeModel> CalibrationServiceinstructionTypes = new List<InstructionTypeModel>();
            DeviceinstructionType.ForEach(s =>
            {
                InstructionTypeModel CalibrationServiceinstructionTypemodels = new InstructionTypeModel
                {
                    InstructionTypeID = s.InstructionTypeId,
                    DocumentName = s.DocumentName,
                    TypeID = s.TypeId,
                    No = s.No,
                    InstructionNo = s.InstructionNo,
                    VersionNo = s.VersionNo,
                    EffectiveDate = s.EffectiveDate,
                    Link = s.Link,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,

                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                CalibrationServiceinstructionTypes.Add(CalibrationServiceinstructionTypemodels);
            });
            CalibrationServiceinstructionTypes.OrderByDescending(o => o.InstructionTypeID).Where(t => t.DeviceCatalogMasterID == id && t.TypeID == 621).ToList();
            var rangeCalibration = _context.RangeCalibration.AsNoTracking().ToList();
            List<RangeCalibrationModel> RangeCalibrationModel = new List<RangeCalibrationModel>();
            rangeCalibration.ForEach(s =>
            {
                RangeCalibrationModel RangeCalibrationModels = new RangeCalibrationModel
                {
                    RangeCalibrationID = s.RangeCalibrationId,
                    ParameterRange = s.ParameterRange,
                    Max = s.Max,
                    Min = s.Min,
                    UnitOfMeasure = s.UnitOfMeasure,
                    Remarks = s.Remarks,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                };
                RangeCalibrationModel.Add(RangeCalibrationModels);
            });
            RangeCalibrationModel.OrderByDescending(o => o.RangeCalibrationID).Where(t => t.DeviceCatalogMasterID == id).ToList();
            var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.AsNoTracking().ToList();
            List<AcceptableCalibrationInformationModel> AcceptableCalibrationInformationModel = new List<AcceptableCalibrationInformationModel>();
            acceptableCalibrationInformation.ForEach(s =>
            {
                AcceptableCalibrationInformationModel AcceptableCalibrationInformationModels = new AcceptableCalibrationInformationModel
                {
                    AcceptableCalibrationInformationID = s.AcceptableCalibrationInformationId,
                    Parameter = s.Parameter,
                    StandardReference = s.StandardReference,
                    Max = s.Max,
                    Min = s.Min,
                    UnitOfMeasure = s.UnitOfMeasure,
                    Remarks = s.Remarks,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                };
                AcceptableCalibrationInformationModel.Add(AcceptableCalibrationInformationModels);
            });
            AcceptableCalibrationInformationModel.OrderByDescending(o => o.AcceptableCalibrationInformationID).Where(t => t.DeviceCatalogMasterID == id).ToList();
            var calExpireCalculation = _context.CalibrationExpireCalculation.OrderByDescending(o => o.CalibrationExpireCalculationId).Where(t => t.DeviceCatalogMasterId == id).FirstOrDefault();
            CalibrationExpireCalculationModel CalibrationExpireCalculationModels = new CalibrationExpireCalculationModel
            {
                CalibrationExpireCalculationID = calExpireCalculation.CalibrationExpireCalculationId,
                Months = calExpireCalculation.Months,
                CalibrationServiceInformationID = calExpireCalculation.CalibrationServiceInformationId,
                DeviceCatalogMasterID = calExpireCalculation.DeviceCatalogMasterId,
            };
            var calibrationVendorInformation = _context.CalibrationVendorInfo.AsNoTracking().ToList();
            List<CalibrationVendorInformationModel> CalibrationVendorInformationModel = new List<CalibrationVendorInformationModel>();
            calibrationVendorInformation.ForEach(s =>
            {
                CalibrationVendorInformationModel CalibrationVendorInformationModels = new CalibrationVendorInformationModel
                {
                    CalibrationVendorInformationID = s.CalibrationVendorInformationId,
                    VendorName = s.VendorName,
                    MethodOfCalibration = s.MethodOfCalibration,
                    CalibrationLeadTime = s.CalibrationLeadTime,
                    MethodOfQuotation = s.MethodOfQuotation,
                    Rating = s.Rating,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,
                    Status = s.Status,
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                };
                CalibrationVendorInformationModel.Add(CalibrationVendorInformationModels);
            });
            CalibrationVendorInformationModel.OrderByDescending(o => o.CalibrationVendorInformationID).Where(t => t.DeviceCatalogMasterID == id).ToList();
            deviceCatalogMastermodel.DeviceInstructionTypeList = InstructionTypeModel;
            deviceCatalogMastermodel.CalibrationInstructionTypeList = CalibrationServiceinstructionTypes;
            deviceCatalogMastermodel.RangeCalibrationList = RangeCalibrationModel;
            deviceCatalogMastermodel.CalibrationExpireCal = CalibrationExpireCalculationModels;
            deviceCatalogMastermodel.CalibrationVendorInfoList = CalibrationVendorInformationModel;
            deviceCatalogMastermodel.AcceptableCalibrationInfoList = AcceptableCalibrationInformationModel;
            return deviceCatalogMastermodel;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetDeviceCatalogMaster/{id:int}")]
        public ActionResult<DeviceCatalogMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == id.Value);
            var result = _mapper.Map<DeviceCatalogMasterModel>(deviceCatalogMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DeviceCatalogMasterModel> GetData(SearchModel searchModel)
        {
            var deviceCatalogMaster = new DeviceCatalogMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Next":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Next":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderBy(o => o.DeviceCatalogMasterId).FirstOrDefault(s => s.DeviceCatalogMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault(s => s.DeviceCatalogMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DeviceCatalogMasterModel>(deviceCatalogMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDeviceCatalogMaster")]
        public DeviceCatalogMasterModel Post(DeviceCatalogMasterModel value)
        {
            var deviceCatalogMaster = new DeviceCatalogMaster
            {
                //DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                DeviceGroupListId = value.DeviceGroupListID,
                ModelName = value.ModelName,
                CalibrationTypeId = value.CalibrationTypeID,
                SourceListId = value.SourceListID,
                Cataloglink = value.Cataloglink,
                Status = value.Status,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,

            };
            _context.DeviceCatalogMaster.Add(deviceCatalogMaster);

            value.DeviceCatalogMasterID = deviceCatalogMaster.DeviceCatalogMasterId;
            var deviceInstructionType = value.DeviceInstructionTypeList;
            if (deviceInstructionType != null)
            {
                if (deviceInstructionType.Count != 0)
                {
                    deviceInstructionType.ForEach(device =>
                    {

                        var deviceType = new InstructionType()
                        {
                            DocumentName = device.DocumentName,
                            TypeId = 620,
                            No = device.No,
                            InstructionNo = device.InstructionNo,
                            VersionNo = device.VersionNo,
                            EffectiveDate = device.EffectiveDate,
                            Link = device.Link,
                            DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                            CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,


                        };
                        _context.InstructionType.Add(deviceType);

                    }

                    );
                }
            }
            var rangeofcalibration = value.RangeCalibrationList;
            if (rangeofcalibration != null)
            {
                if (rangeofcalibration.Count != 0)
                {
                    rangeofcalibration.ForEach(range =>
                    {

                        var rangeofcalibrationType = new RangeCalibration()
                        {
                            ParameterRange = range.ParameterRange,
                            Max = range.Max,
                            Min = range.Min,
                            UnitOfMeasure = range.UnitOfMeasure,
                            Remarks = range.Remarks,
                            CalibrationServiceInformationId = range.CalibrationServiceInformationID,
                            DeviceCatalogMasterId = value.DeviceCatalogMasterID,


                        };
                        _context.RangeCalibration.Add(rangeofcalibrationType);

                    }

                    );
                }
            }
            var acceptablecalibration = value.AcceptableCalibrationInfoList;
            if (acceptablecalibration != null)
            {
                if (acceptablecalibration.Count != 0)
                {
                    acceptablecalibration.ForEach(accept =>
                    {

                        var acceptableCalibrationInformation = new AcceptableCalibrationInfo
                        {
                            Parameter = accept.Parameter,
                            StandardReference = accept.StandardReference,
                            Max = accept.Max,
                            Min = accept.Min,
                            UnitOfMeasure = accept.UnitOfMeasure,
                            Remarks = accept.Remarks,
                            CalibrationServiceInformationId = accept.CalibrationServiceInformationID,
                            DeviceCatalogMasterId = value.DeviceCatalogMasterID,

                            //ContactActivities = new List<ContactActivities>(),

                        };
                        _context.AcceptableCalibrationInfo.Add(acceptableCalibrationInformation);

                    }

                    );
                }
            }
            var CalibrationExpire = new CalibrationExpireCalculation
            {

                Months = value.CalibrationExpireCal.Months,
                CalibrationServiceInformationId = value.CalibrationExpireCal.CalibrationServiceInformationID,
                DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                //ContactActivities = new List<ContactActivities>(),

            };
            _context.CalibrationExpireCalculation.Add(CalibrationExpire);

            var CalibrationServiceInstructionType = value.CalibrationInstructionTypeList;
            if (CalibrationServiceInstructionType != null)
            {
                if (CalibrationServiceInstructionType.Count != 0)
                {
                    CalibrationServiceInstructionType.ForEach(device =>
                    {

                        var deviceType = new InstructionType()
                        {
                            DocumentName = device.DocumentName,
                            TypeId = 621,
                            No = device.No,
                            InstructionNo = device.InstructionNo,
                            VersionNo = device.VersionNo,
                            EffectiveDate = device.EffectiveDate,
                            Link = device.Link,
                            DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                            CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,

                        };
                        _context.InstructionType.Add(deviceType);

                    }

                    );
                }
            }
            var CalibrationvendorInformation = value.CalibrationVendorInfoList;
            if (CalibrationvendorInformation != null)
            {
                if (CalibrationvendorInformation.Count != 0)
                {
                    CalibrationvendorInformation.ForEach(vendor =>
                    {
                        var calibrationVendorInformation = new CalibrationVendorInfo
                        {
                            VendorName = vendor.VendorName,
                            MethodOfCalibration = vendor.MethodOfCalibration,
                            CalibrationLeadTime = vendor.CalibrationLeadTime,
                            MethodOfQuotation = vendor.MethodOfQuotation,
                            Rating = vendor.Rating,
                            CalibrationServiceInformationId = vendor.CalibrationServiceInformationID,
                            Status = vendor.Status,
                            DeviceCatalogMasterId = value.DeviceCatalogMasterID,

                        };

                        _context.CalibrationVendorInfo.Add(calibrationVendorInformation);

                    }




                    );
                }
            }

            _context.SaveChanges();
            return value;
        }



        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDeviceCatalogMaster")]
        public DeviceCatalogMasterModel Put(DeviceCatalogMasterModel value)
        {
            var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == value.DeviceCatalogMasterID);

            deviceCatalogMaster.DeviceGroupListId = value.DeviceGroupListID;
            deviceCatalogMaster.ModelName = value.ModelName;
            deviceCatalogMaster.CalibrationTypeId = value.CalibrationTypeID;
            deviceCatalogMaster.Cataloglink = value.Cataloglink;
            deviceCatalogMaster.Status = value.Status;
            deviceCatalogMaster.ModifiedByUserId = value.ModifiedByUserID;
            deviceCatalogMaster.ModifiedDate = DateTime.Now;

            var deviceInstructionType = value.DeviceInstructionTypeList;
            if (deviceInstructionType != null)
            {
                if (deviceInstructionType.Count != 0)
                {
                    deviceInstructionType.ForEach(device =>
                    {
                        var instructionType = _context.InstructionType.SingleOrDefault(p => p.InstructionTypeId == device.InstructionTypeID);
                        if (instructionType != null)
                        {
                            instructionType.DocumentName = device.DocumentName;
                            instructionType.TypeId = device.TypeID;
                            instructionType.No = device.No;
                            instructionType.InstructionNo = device.InstructionNo;
                            instructionType.VersionNo = device.VersionNo;
                            instructionType.EffectiveDate = device.EffectiveDate;
                            instructionType.Link = device.Link;
                            instructionType.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
                            instructionType.CalibrationServiceInformationId = device.CalibrationServiceInformationID;

                            instructionType.ModifiedByUserId = value.ModifiedByUserID;
                            instructionType.ModifiedDate = DateTime.Now;
                            instructionType.StatusCodeId = value.StatusCodeID.Value;

                        }
                        else
                        {
                            var deviceType = new InstructionType()
                            {
                                DocumentName = device.DocumentName,
                                TypeId = 620,
                                No = device.No,
                                InstructionNo = device.InstructionNo,
                                VersionNo = device.VersionNo,
                                EffectiveDate = device.EffectiveDate,
                                Link = device.Link,
                                DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                                CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID.Value,
                            };
                            _context.InstructionType.Add(deviceType);

                        }
                    }

                    );
                }
            }
            var rangeofcalibration = value.RangeCalibrationList;
            if (rangeofcalibration != null)
            {
                if (rangeofcalibration.Count != 0)
                {
                    rangeofcalibration.ForEach(range =>
                    {


                        var rangeCalibration = _context.RangeCalibration.SingleOrDefault(p => p.RangeCalibrationId == range.RangeCalibrationID);
                        {
                            if (rangeCalibration != null)
                            {
                                rangeCalibration.ParameterRange = range.ParameterRange;
                                rangeCalibration.Max = range.Max;
                                rangeCalibration.Min = range.Min;
                                rangeCalibration.UnitOfMeasure = range.UnitOfMeasure;
                                rangeCalibration.Remarks = range.Remarks;
                                rangeCalibration.CalibrationServiceInformationId = range.CalibrationServiceInformationID;
                                rangeCalibration.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
                            }
                            else
                            {
                                var rangeofcalibrationType = new RangeCalibration()
                                {
                                    ParameterRange = range.ParameterRange,
                                    Max = range.Max,
                                    Min = range.Min,
                                    UnitOfMeasure = range.UnitOfMeasure,
                                    Remarks = range.Remarks,
                                    CalibrationServiceInformationId = range.CalibrationServiceInformationID,
                                    DeviceCatalogMasterId = value.DeviceCatalogMasterID,


                                };
                                _context.RangeCalibration.Add(rangeofcalibrationType);
                            }
                        };


                    }

                    );
                }
            }
            var acceptablecalibration = value.AcceptableCalibrationInfoList;
            if (acceptablecalibration != null)
            {
                if (acceptablecalibration.Count != 0)
                {
                    acceptablecalibration.ForEach(accept =>
                    {


                        var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.SingleOrDefault(p => p.AcceptableCalibrationInformationId == accept.AcceptableCalibrationInformationID);
                        {
                            if (acceptableCalibrationInformation != null)
                            {
                                acceptableCalibrationInformation.Parameter = accept.Parameter;
                                acceptableCalibrationInformation.StandardReference = accept.StandardReference;
                                acceptableCalibrationInformation.Max = accept.Max;
                                acceptableCalibrationInformation.Min = accept.Min;
                                acceptableCalibrationInformation.UnitOfMeasure = accept.UnitOfMeasure;
                                acceptableCalibrationInformation.Remarks = accept.Remarks;
                                acceptableCalibrationInformation.CalibrationServiceInformationId = accept.CalibrationServiceInformationID;
                                acceptableCalibrationInformation.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
                            }
                            else
                            {
                                acceptableCalibrationInformation = new AcceptableCalibrationInfo
                                {
                                    Parameter = accept.Parameter,
                                    StandardReference = accept.StandardReference,
                                    Max = accept.Max,
                                    Min = accept.Min,
                                    UnitOfMeasure = accept.UnitOfMeasure,
                                    Remarks = accept.Remarks,
                                    CalibrationServiceInformationId = accept.CalibrationServiceInformationID,
                                    DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                                    //ContactActivities = new List<ContactActivities>(),

                                };
                                _context.AcceptableCalibrationInfo.Add(acceptableCalibrationInformation);

                            }
                        };

                    }

                    );
                }
            }

            var calExpire = _context.CalibrationExpireCalculation.SingleOrDefault(p => p.CalibrationExpireCalculationId == value.CalibrationExpireCal.CalibrationExpireCalculationID);
            {
                calExpire.Months = value.CalibrationExpireCal.Months;
                calExpire.CalibrationServiceInformationId = value.CalibrationExpireCal.CalibrationServiceInformationID;
                calExpire.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
                //ContactActivities = new List<ContactActivities>(),
            }

            var CalibrationServiceInstructionType = value.CalibrationInstructionTypeList;
            if (CalibrationServiceInstructionType != null)
            {
                if (CalibrationServiceInstructionType.Count != 0)
                {
                    CalibrationServiceInstructionType.ForEach(device =>
                    {

                        var instructionType = _context.InstructionType.SingleOrDefault(p => p.InstructionTypeId == device.InstructionTypeID);
                        if (instructionType != null)
                        {
                            instructionType.DocumentName = device.DocumentName;
                            instructionType.TypeId = device.TypeID;
                            instructionType.No = device.No;
                            instructionType.InstructionNo = device.InstructionNo;
                            instructionType.VersionNo = device.VersionNo;
                            instructionType.EffectiveDate = device.EffectiveDate;
                            instructionType.Link = device.Link;
                            instructionType.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
                            instructionType.CalibrationServiceInformationId = device.CalibrationServiceInformationID;

                            instructionType.ModifiedByUserId = value.ModifiedByUserID;
                            instructionType.ModifiedDate = DateTime.Now;
                            instructionType.StatusCodeId = value.StatusCodeID.Value;
                            instructionType.DeviceCatalogMasterId = value.DeviceCatalogMasterID;

                        }
                        else
                        {
                            var deviceType = new InstructionType()
                            {
                                DocumentName = device.DocumentName,
                                TypeId = 620,
                                No = device.No,
                                InstructionNo = device.InstructionNo,
                                VersionNo = device.VersionNo,
                                EffectiveDate = device.EffectiveDate,
                                Link = device.Link,
                                DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                                CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                                StatusCodeId = value.StatusCodeID.Value,

                            };
                            _context.InstructionType.Add(deviceType);

                        }

                    }



                    );
                }

            }
            var CalibrationVendor = value.CalibrationVendorInfoList;
            if (CalibrationVendor != null)
            {
                if (CalibrationVendor.Count != 0)
                {
                    CalibrationVendor.ForEach(vendor =>
                    {

                        var calibrationVendorInformation = _context.CalibrationVendorInfo.SingleOrDefault(p => p.CalibrationVendorInformationId == vendor.CalibrationVendorInformationID);

                        if (calibrationVendorInformation != null)
                        {


                            calibrationVendorInformation.VendorName = vendor.VendorName;
                            calibrationVendorInformation.MethodOfCalibration = vendor.MethodOfCalibration;
                            calibrationVendorInformation.CalibrationLeadTime = vendor.CalibrationLeadTime;
                            calibrationVendorInformation.MethodOfQuotation = vendor.MethodOfQuotation;
                            calibrationVendorInformation.Rating = vendor.Rating;
                            calibrationVendorInformation.CalibrationServiceInformationId = vendor.CalibrationServiceInformationID;
                            calibrationVendorInformation.Status = vendor.Status;
                            calibrationVendorInformation.DeviceCatalogMasterId = vendor.DeviceCatalogMasterID;
                        }
                        else
                        {
                            var calibrationVendorAdd = new CalibrationVendorInfo
                            {
                                VendorName = vendor.VendorName,
                                MethodOfCalibration = vendor.MethodOfCalibration,
                                CalibrationLeadTime = vendor.CalibrationLeadTime,
                                MethodOfQuotation = vendor.MethodOfQuotation,
                                Rating = vendor.Rating,
                                CalibrationServiceInformationId = vendor.CalibrationServiceInformationID,
                                Status = vendor.Status,
                                DeviceCatalogMasterId = value.DeviceCatalogMasterID,

                            };

                            _context.CalibrationVendorInfo.Add(calibrationVendorAdd);


                        }

                    }



                    );
                }

            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDeviceCatalogMaster")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == id);
                if (deviceCatalogMaster != null)
                {
                    var deviceinstruction = _context.InstructionType.Where(p => p.DeviceCatalogMasterId == id).ToList();
                    if (deviceinstruction != null)
                    {
                        _context.InstructionType.RemoveRange(deviceinstruction);
                        _context.SaveChanges();
                    }
                    var acceptable = _context.AcceptableCalibrationInfo.Where(p => p.DeviceCatalogMasterId == id).ToList();
                    if (acceptable != null)
                    {
                        _context.AcceptableCalibrationInfo.RemoveRange(acceptable);
                        _context.SaveChanges();
                    }
                    var rangecalibration = _context.RangeCalibration.Where(p => p.DeviceCatalogMasterId == id).ToList();
                    if (rangecalibration != null)
                    {
                        _context.RangeCalibration.RemoveRange(rangecalibration);
                        _context.SaveChanges();
                    }
                    var vendorinformation = _context.CalibrationVendorInfo.Where(p => p.DeviceCatalogMasterId == id).ToList();
                    if (vendorinformation != null)
                    {
                        _context.CalibrationVendorInfo.RemoveRange(vendorinformation);
                        _context.SaveChanges();
                    }
                    var expirecal = _context.CalibrationExpireCalculation.SingleOrDefault(p => p.DeviceCatalogMasterId == id);
                    if (expirecal != null)
                    {
                        _context.CalibrationExpireCalculation.Remove(expirecal);
                        _context.SaveChanges();
                    }

                    _context.DeviceCatalogMaster.Remove(deviceCatalogMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}