﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SOLotInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public SOLotInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetSOLotInformations")]
        public List<SOLotInformationModel> Get(int id)
        {
           
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
            var solotInformations = _context.SolotInformation
                .Include(a => a.AddedByUser)
                .Include(c => c.StatusCode)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.SowithoutBlanketOrder)
                .Include(a=>a.SowithoutBlanketOrder.Soproduct)
                .Where(p => p.SowithoutBlanketOrderId == id)
                .AsNoTracking()
                .ToList();
            List<SOLotInformationModel> sOLotInformationModels = new List<SOLotInformationModel>();
            if(solotInformations!=null && solotInformations.Count>0)
            {
                var uomIds = solotInformations.Where(s => s.SowithoutBlanketOrder?.Soproduct?.PerUomId != null).Select(s => s.SowithoutBlanketOrder?.Soproduct?.PerUomId).ToList();
                var orderCurrencyIds = solotInformations.Where(s => s.OrderCurrencyId != null).Select(s => s.OrderCurrencyId).ToList();
                var priceUnitIds = solotInformations.Where(s => s.PriceUnitId != null).Select(s => s.PriceUnitId).ToList();
                if(uomIds.Count>0)
                {
                    masterDetailIds.AddRange(uomIds);
                }
                if(orderCurrencyIds.Count>0)
                {
                    masterDetailIds.AddRange(orderCurrencyIds);
                }
                if(priceUnitIds.Count>0)
                {
                    masterDetailIds.AddRange(priceUnitIds);
                }
            }
            if(masterDetailIds.Count>0)
            {
                 masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
            }
            solotInformations.ForEach(s =>
            {
                SOLotInformationModel sOLotInformationModel = new SOLotInformationModel();

                sOLotInformationModel.SowithoutBlanketOrderId = s.SowithoutBlanketOrderId;
                sOLotInformationModel.SolotInformationId = s.SolotInformationId;
                sOLotInformationModel.OrderQty = s.OrderQty;
                sOLotInformationModel.OrderCurrencyId = s.OrderCurrencyId;
                sOLotInformationModel.SellingPrice = s.SellingPrice;
                sOLotInformationModel.PriceUnitId = s.PriceUnitId;
                sOLotInformationModel.IsFoc = s.IsFoc;
                sOLotInformationModel.RequestShipmentDate = s.RequestShipmentDate;
                sOLotInformationModel.StatusCodeID = s.StatusCodeId;
                sOLotInformationModel.StatusCode = s.StatusCode?.CodeValue;
                sOLotInformationModel.AddedByUserID = s.AddedByUserId;
                sOLotInformationModel.AddedByUser = s.AddedByUser?.UserName;
                sOLotInformationModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                sOLotInformationModel.AddedDate = s.AddedDate;
                sOLotInformationModel.Foc = s.Foc;
                sOLotInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                sOLotInformationModel.ModifiedDate = s.ModifiedDate;
               sOLotInformationModel.UOM  = masterDetailList != null && s.SowithoutBlanketOrder?.Soproduct?.PerUomId>0? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.SowithoutBlanketOrder?.Soproduct?.PerUomId).Select(a => a.Value).SingleOrDefault() : "";
                sOLotInformationModel.OrderCurrency = masterDetailList != null && s.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                sOLotInformationModel.PriceUnit = masterDetailList != null && s.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
                sOLotInformationModels.Add(sOLotInformationModel);
            });
            return sOLotInformationModels.OrderByDescending(a => a.SolotInformationId).ToList();
        }

        [HttpPost()]
        [Route("GetSOLotInformationData")]
        public ActionResult<SOLotInformationModel> GetSOLotInformationData(SearchModel searchModel)
        {
            var solotInformation = new SolotInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).LastOrDefault();
                        break;
                    case "Next":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).LastOrDefault();
                        break;
                    case "Next":
                        solotInformation = _context.SolotInformation.OrderBy(o => o.SolotInformationId).FirstOrDefault(s => s.SolotInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        solotInformation = _context.SolotInformation.OrderByDescending(o => o.SolotInformationId).FirstOrDefault(s => s.SolotInformationId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SOLotInformationModel>(solotInformation);
            return result;
        }

        [HttpPost]
        [Route("InsertSoLotInformation")]
        public SOLotInformationModel Post(SOLotInformationModel value)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var sOLotInformation = new SolotInformation();
            sOLotInformation.SowithoutBlanketOrderId = value.SowithoutBlanketOrderId;
            sOLotInformation.OrderQty = value.OrderQty;
            sOLotInformation.OrderCurrencyId = value.OrderCurrencyId;
            sOLotInformation.SellingPrice = value.SellingPrice;
            sOLotInformation.PriceUnitId = value.PriceUnitId;
            sOLotInformation.IsFoc = value.IsFoc;
            sOLotInformation.RequestShipmentDate = value.RequestShipmentDate;
            sOLotInformation.StatusCodeId = value.StatusCodeID.Value;
            sOLotInformation.AddedByUserId = value.AddedByUserID;
            sOLotInformation.AddedDate = DateTime.Now;
            sOLotInformation.Foc = value.Foc;
            _context.SolotInformation.Add(sOLotInformation);
            _context.SaveChanges();
            if(value.OrderCurrencyId>0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(a=>a.ApplicationMasterDetailId == value.OrderCurrencyId).AsNoTracking().ToList();
                value.OrderCurrency = masterDetailList != null && value.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
            }
            if (value.PriceUnitId > 0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterDetailId == value.PriceUnitId).AsNoTracking().ToList();
                value.PriceUnit = masterDetailList != null && value.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
            }
            value.SolotInformationId = sOLotInformation.SolotInformationId;
            value.AddedDate = sOLotInformation.AddedDate;
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == sOLotInformation.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }
        [HttpPut]
        [Route("UpdateSOLotInformation")]
        public SOLotInformationModel Put(SOLotInformationModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var sOLotInformation = _context.SolotInformation.SingleOrDefault(p => p.SolotInformationId == value.SolotInformationId);
            sOLotInformation.SowithoutBlanketOrderId = value.SowithoutBlanketOrderId;
            sOLotInformation.OrderQty = value.OrderQty;
            sOLotInformation.OrderCurrencyId = value.OrderCurrencyId;
            sOLotInformation.SellingPrice = value.SellingPrice;
            sOLotInformation.PriceUnitId = value.PriceUnitId;
            sOLotInformation.IsFoc = value.IsFoc;
            sOLotInformation.RequestShipmentDate = value.RequestShipmentDate;
            sOLotInformation.StatusCodeId = value.StatusCodeID.Value;
            sOLotInformation.AddedByUserId = value.AddedByUserID;
            sOLotInformation.AddedDate = value.AddedDate.Value;
            sOLotInformation.StatusCodeId = value.StatusCodeID.Value;
            sOLotInformation.ModifiedByUserId = value.ModifiedByUserID;
            sOLotInformation.ModifiedDate = value.ModifiedDate.Value;
            sOLotInformation.Foc = value.Foc;
            _context.SaveChanges();
            value.OrderCurrency = masterDetailList != null && value.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
            value.PriceUnit = masterDetailList != null && value.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.ModifiedDate = sOLotInformation.ModifiedDate;
            value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == sOLotInformation.ModifiedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSoLotInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var solotInformation = _context.SolotInformation.Where(p => p.SolotInformationId == id).FirstOrDefault();
                if (solotInformation != null)
                {
                    _context.SolotInformation.Remove(solotInformation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}


