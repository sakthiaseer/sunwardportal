﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ReAssignmentJobController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ReAssignmentJobController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetReAssignmentJob")]
        public List<ReAssignmentJobModel> Get()
        {
            List<ReAssignmentJobModel> reAssignmentJobModels = new List<ReAssignmentJobModel>();
            var plants = _context.Plant.ToListAsync();
            var reAssignmentJob = _context.ReAssignmentJob
              .Include(a => a.AddedByUser)
              .Include(b => b.ModifiedByUser)
              .Include(c => c.StatusCode)
              .Include(e => e.ProdTask)
              .Include(f => f.ReAssignementManpowerMultiple)
              .Include(g => g.SpecificProcedure)
              .Include(h=>h.MobileShift)
              .OrderByDescending(o => o.ReassignmentJobId).AsNoTracking().ToList();
            reAssignmentJob.ForEach(s =>
            {
                var plant = plants.Result.FirstOrDefault(s => s.PlantId == s.PlantId).Description;
                ReAssignmentJobModel reAssignmentJobModel = new ReAssignmentJobModel
                {
                    ReassignmentJobId = s.ReassignmentJobId,
                    CompanyDbid = s.CompanyDbid,
                    ProdTaskId = s.ProdTaskId,
                    MobileShiftId=s.MobileShiftId,
                    MobileShift=s.MobileShift?.Code,
                    SpecificProcedureId = s.SpecificProcedureId,
                    StartDateTime = s.StartTime,
                    EndDateTime = s.EndTime,
                    CompanyDb = plant,
                    ProdTask = s.ProdTask?.Description,
                    SpecificProcedure = s.SpecificProcedure?.Value,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ReAssignementManpowerMultipleIds = s.ReAssignementManpowerMultiple != null ? s.ReAssignementManpowerMultiple.Where(w => w.ReassignmentId == s.ReassignmentJobId).Select(s => s.ManPowerId).ToList() : new List<long?>(),
                };
                reAssignmentJobModels.Add(reAssignmentJobModel);

            });
            return reAssignmentJobModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ReAssignmentJobModel> GetData(SearchModel searchModel)
        {
            var reAssignmentJob = new ReAssignmentJob();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).FirstOrDefault();
                        break;
                    case "Last":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).LastOrDefault();
                        break;
                    case "Next":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).LastOrDefault();
                        break;
                    case "Previous":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).FirstOrDefault();
                        break;
                    case "Last":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).LastOrDefault();
                        break;
                    case "Next":
                        reAssignmentJob = _context.ReAssignmentJob.OrderBy(o => o.ReassignmentJobId).FirstOrDefault(s => s.ReassignmentJobId > searchModel.Id);
                        break;
                    case "Previous":
                        reAssignmentJob = _context.ReAssignmentJob.OrderByDescending(o => o.ReassignmentJobId).FirstOrDefault(s => s.ReassignmentJobId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ReAssignmentJobModel>(reAssignmentJob);
            return result;
        }
        [HttpPost]
        [Route("InsertReAssignmentJob")]
        public ReAssignmentJobModel Post(ReAssignmentJobModel value)
        {
            int plantId = 1;
            if (value.CompanyName == "NAV_SG")
            {
                plantId = 2;
            }
            var reAssignmentJob = new ReAssignmentJob
            {
                MobileShiftId = value.MobileShiftId,
                CompanyDbid = plantId,
                ProdTaskId = value.ProdTaskId,
                SpecificProcedureId = value.SpecificProcedureId,
                StartTime = value.StartDateTime,
                EndTime = value.EndDateTime,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = 1
            };
            if (value.ReAssignementManpowerMultipleIds.Count > 0)
            {
                value.ReAssignementManpowerMultipleIds.ForEach(h =>
                {
                    var reAssignementManpowerMultiple = new ReAssignementManpowerMultiple
                    {
                        ManPowerId = h
                    };
                    reAssignmentJob.ReAssignementManpowerMultiple.Add(reAssignementManpowerMultiple);
                });

            }
            _context.ReAssignmentJob.Add(reAssignmentJob);
            _context.SaveChanges();
            value.ReassignmentJobId = reAssignmentJob.ReassignmentJobId;

            return value;
        }
        [HttpPut]
        [Route("UpdateReAssignmentJob")]
        public ReAssignmentJobModel Put(ReAssignmentJobModel value)
        {
            var reAssignmentJob = _context.ReAssignmentJob.SingleOrDefault(p => p.ReassignmentJobId == value.ReassignmentJobId);
            reAssignmentJob.CompanyDbid = value.CompanyDbid;
            reAssignmentJob.ProdTaskId = value.ProdTaskId;
            reAssignmentJob.SpecificProcedureId = value.SpecificProcedureId;
            reAssignmentJob.StartTime = value.StartDateTime;
            reAssignmentJob.EndTime = value.EndDateTime;
            reAssignmentJob.ModifiedByUserId = value.ModifiedByUserID;
            reAssignmentJob.ModifiedDate = DateTime.Now;
            reAssignmentJob.StatusCodeId = value.StatusCodeID.Value;
            reAssignmentJob.MobileShiftId = value.MobileShiftId;
            _context.SaveChanges();
            var reAssignementManpowerMultipleRemove = _context.ReAssignementManpowerMultiple.Where(w => w.ReassignmentId == reAssignmentJob.ReassignmentJobId).ToList();
            if (reAssignementManpowerMultipleRemove != null)
            {
                _context.ReAssignementManpowerMultiple.RemoveRange(reAssignementManpowerMultipleRemove);
                _context.SaveChanges();
            }
            if (value.ReAssignementManpowerMultipleIds.Count > 0)
            {
                value.ReAssignementManpowerMultipleIds.ForEach(h =>
                {
                    var reAssignementManpowerMultiple = new ReAssignementManpowerMultiple
                    {
                        ManPowerId = h
                    };
                    reAssignmentJob.ReAssignementManpowerMultiple.Add(reAssignementManpowerMultiple);
                });

            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteReAssignmentJob")]
        public void Delete(int id)
        {
            try
            {
                var reAssignmentJob = _context.ReAssignmentJob.SingleOrDefault(p => p.ReassignmentJobId == id);
                if (reAssignmentJob != null)
                {
                    var reAssignementManpowerMultipleRemove = _context.ReAssignementManpowerMultiple.Where(w => w.ReassignmentId == id).ToList();
                    if (reAssignementManpowerMultipleRemove != null)
                    {
                        _context.ReAssignementManpowerMultiple.RemoveRange(reAssignementManpowerMultipleRemove);
                        _context.SaveChanges();
                    }
                    _context.ReAssignmentJob.Remove(reAssignmentJob);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Mobile Shift Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
