﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterFormController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MasterFormController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetMasterForms")]
        public List<MasterFormModel> Get()
        {
            var masterForms = _context.MasterForm.Include(m => m.AddedByUser)
                                                .Include(m => m.ModifiedByUser)
                                                .Include(m => m.StatusCode)
                                                .Select(s => new MasterFormModel
                                                {
                                                    MasterFormId = s.MasterFormId,
                                                    FormName = s.FormName,
                                                    FormModel = s.FormModel,
                                                    FormSchema = s.FormSchema,
                                                    FormItems = s.FormItems,
                                                    SessionId = s.SessionId,
                                                    AddedDate = s.AddedDate,
                                                    ModifiedDate = s.ModifiedDate,
                                                    AddedByUserID = s.AddedByUserId,
                                                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                                    ModifiedByUserID = s.ModifiedByUserId,
                                                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                                                    StatusCodeID = s.StatusCodeId,
                                                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                                }).OrderByDescending(o => o.MasterFormId).AsNoTracking().ToList();
            return masterForms;
        }


        [HttpGet]
        [Route("GetMasterFormBySessionId")]
        public MasterFormModel GetMasterFormBySessionID(Guid sessionId)
        {
            var masterForm = _context.MasterForm.Include(m => m.AddedByUser)
                                                .Include(m => m.ModifiedByUser)
                                                .Include(m => m.StatusCode)
                                                .Select(s => new MasterFormModel
                                                {
                                                    MasterFormId = s.MasterFormId,
                                                    FormName = s.FormName,
                                                    FormModel = s.FormModel,
                                                    FormSchema = s.FormSchema,
                                                    FormItems = s.FormItems,
                                                    SessionId = s.SessionId,
                                                    AddedDate = s.AddedDate,
                                                    ModifiedDate = s.ModifiedDate,
                                                    AddedByUserID = s.AddedByUserId,
                                                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                                                    ModifiedByUserID = s.ModifiedByUserId,
                                                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                                                    StatusCodeID = s.StatusCodeId,
                                                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                                                }).FirstOrDefault(s => s.SessionId == sessionId);
            return masterForm;
        }

        [HttpGet("GetMasterFormLines/{id:int}")]
        public List<MasterFormLineModel> GetMasterFormLines(int? id)
        {
            var masterformLines = _context.MasterFormDetail.Include(p => p.User).Select(s => new MasterFormLineModel
            {
                MasterFormDetailId = s.MasterFormDetailId,
                MasterFormId = s.MasterFormId,
                MasterFormData = s.MasterFormData,
                UserId = s.UserId,
            }).Where(s => s.MasterFormId == id).OrderByDescending(o => o.MasterFormDetailId).AsNoTracking().ToList();
            return masterformLines;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertMasterForm")]
        public MasterFormModel Post(MasterFormModel value)
        {
            var sessionId = Guid.NewGuid();
            var masterForm = new MasterForm
            {
                FormName = value.FormName,
                FormSchema = value.FormSchema,
                FormModel = value.FormModel,
                FormItems = value.FormItems,
                SessionId = sessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = 1
            };
            _context.MasterForm.Add(masterForm);
            _context.SaveChanges();
            value.MasterFormId = masterForm.MasterFormId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMasterForm")]
        public MasterFormModel Put(MasterFormModel value)
        {
            var masterForm = _context.MasterForm.FirstOrDefault(p => p.MasterFormId == value.MasterFormId);
            masterForm.FormName = value.FormName;
            masterForm.FormSchema = value.FormSchema;
            masterForm.FormModel = value.FormModel;
            masterForm.FormItems = value.FormItems;
            masterForm.ModifiedByUserId = value.ModifiedByUserID;
            masterForm.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMasterForm")]
        public void Delete(int id)
        {
            var masterForm = _context.MasterForm.Include(m => m.MasterFormDetail).FirstOrDefault(p => p.MasterFormId == id);
            if (masterForm != null)
            {
                _context.MasterForm.Remove(masterForm);
                _context.SaveChanges();
            }
        }


        [HttpGet("GetMasterFormLineItem")]
        public MasterFormLineModel GetMasterFormLineItem(Guid sessionID, int userId)
        {
            var masterformLineItem = _context.MasterFormDetail.Select(s => new MasterFormLineModel
            {
                MasterFormDetailId = s.MasterFormDetailId,
                MasterFormId = s.MasterFormId,
                MasterFormData = s.MasterFormData,
                UserId = s.UserId,
                SessionId = s.SessionId
            }).Where(s => s.SessionId == sessionID && s.UserId == userId).FirstOrDefault();
            return masterformLineItem;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertMasterFormLine")]
        public MasterFormLineModel Post(MasterFormLineModel value)
        {
            var masterFormDetail = new MasterFormDetail
            {
                MasterFormId = value.MasterFormId,
                MasterFormData = value.MasterFormData,
                UserId = value.UserId,
                SessionId = value.SessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = 1
            };
            _context.MasterFormDetail.Add(masterFormDetail);
            _context.SaveChanges();
            value.MasterFormDetailId = masterFormDetail.MasterFormDetailId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMasterFormLine")]
        public MasterFormLineModel Put(MasterFormLineModel value)
        {
            var masterFormDetail = _context.MasterFormDetail.FirstOrDefault(p => p.MasterFormDetailId == value.MasterFormDetailId);
            masterFormDetail.MasterFormData = value.MasterFormData;
            masterFormDetail.UserId = value.UserId;
            masterFormDetail.ModifiedByUserId = value.ModifiedByUserID;
            masterFormDetail.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMasterFormLine")]
        public void DeleteMasterFormLine(int id)
        {
            var masterFormDetail = _context.MasterFormDetail.FirstOrDefault(p => p.MasterFormDetailId == id);
            if (masterFormDetail != null)
            {
                _context.MasterFormDetail.Remove(masterFormDetail);
                _context.SaveChanges();
            }
        }

    }
}