﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DeviceCatalogController : Controller
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DeviceCatalogController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDeviceCatalogMasters")]
        public List<DeviceCatalogMasterModel> Get()
        {
            var deviceCatalogMaster = _context.DeviceCatalogMaster.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DeviceCatalogMasterModel> deviceCatalogMasterModel = new List<DeviceCatalogMasterModel>();
            deviceCatalogMaster.ForEach(s =>
            {
                DeviceCatalogMasterModel deviceCatalogMasterModels = new DeviceCatalogMasterModel
                {
                    DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                    DeviceGroupListID = s.DeviceGroupListId,
                    DeviceName = s.DeviceGroupList.DeviceGroupName,
                    ModelName = s.ModelName,
                    SourceListID = s.SourceListId,
                    ManufacturerName = s.SourceList.Name,
                    CalibrationTypeID = s.CalibrationTypeId,
                    CalibrationTypeName = s.CalibrationType.Name,
                    Cataloglink = s.Cataloglink,
                    Status = s.Status,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                };
                deviceCatalogMasterModel.Add(deviceCatalogMasterModels);
            });
            return deviceCatalogMasterModel.OrderByDescending(a => a.DeviceCatalogMasterID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetDeviceCatalogMaster/{id:int}")]
        public ActionResult<DeviceCatalogMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == id.Value);
            var result = _mapper.Map<DeviceCatalogMasterModel>(deviceCatalogMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DeviceCatalogMasterModel> GetData(SearchModel searchModel)
        {
            var deviceCatalogMaster = new DeviceCatalogMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Next":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).LastOrDefault();
                        break;
                    case "Next":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderBy(o => o.DeviceCatalogMasterId).FirstOrDefault(s => s.DeviceCatalogMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        deviceCatalogMaster = _context.DeviceCatalogMaster.OrderByDescending(o => o.DeviceCatalogMasterId).FirstOrDefault(s => s.DeviceCatalogMasterId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DeviceCatalogMasterModel>(deviceCatalogMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDevice")]
        public DeviceCatalogMasterModel Post(DeviceCatalogMasterModel value)
        {
            var deviceCatalogMaster = new DeviceCatalogMaster
            {
                DeviceGroupListId = value.DeviceGroupListID,
                ModelName = value.ModelName,
                CalibrationTypeId = value.CalibrationTypeID,
                SourceListId = value.SourceListID,
                Cataloglink = value.Cataloglink,
                Status = value.Status,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,

            };

            _context.DeviceCatalogMaster.Add(deviceCatalogMaster);
            _context.SaveChanges();
            value.DeviceCatalogMasterID = deviceCatalogMaster.DeviceCatalogMasterId;
            var deviceInstructionType = value.DeviceInstructionTypeList;
            if (deviceInstructionType.Count != 0)
            {
                deviceInstructionType.ForEach(device =>
                {

                    var deviceType = new InstructionType()
                    {
                        DocumentName = device.DocumentName,
                        TypeId = 620,
                        No = device.No,
                        InstructionNo = device.InstructionNo,
                        VersionNo = device.VersionNo,
                        EffectiveDate = device.EffectiveDate,
                        Link = device.Link,
                        DeviceCatalogMasterId = device.DeviceCatalogMasterID,
                        CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,


                    };
                    _context.InstructionType.Add(deviceType);
                }

                );
            }
            var rangeofcalibration = value.RangeCalibrationList;
            if (rangeofcalibration.Count != 0)
            {
                rangeofcalibration.ForEach(range =>
                {

                    var rangeofcalibrationType = new RangeCalibration()
                    {
                        ParameterRange = range.ParameterRange,
                        Max = range.Max,
                        Min = range.Min,
                        UnitOfMeasure = range.UnitOfMeasure,
                        Remarks = range.Remarks,
                        CalibrationServiceInformationId = range.CalibrationServiceInformationID,



                    };
                    _context.RangeCalibration.Add(rangeofcalibrationType);
                }

                );
            }
            var acceptablecalibration = value.AcceptableCalibrationInfoList;
            if (acceptablecalibration.Count != 0)
            {
                acceptablecalibration.ForEach(accept =>
                {

                    var acceptableCalibrationInformation = new AcceptableCalibrationInfo
                    {
                        Parameter = accept.Parameter,
                        StandardReference = accept.StandardReference,
                        Max = accept.Max,
                        Min = accept.Min,
                        UnitOfMeasure = accept.UnitOfMeasure,
                        Remarks = accept.Remarks,
                        CalibrationServiceInformationId = accept.CalibrationServiceInformationID,

                    };
                    _context.AcceptableCalibrationInfo.Add(acceptableCalibrationInformation);
                }

                );
            }
            var CalibrationExpire = new CalibrationExpireCalculation
            {

                Months = value.CalibrationExpireCal.Months,
                CalibrationServiceInformationId = value.CalibrationExpireCal.CalibrationServiceInformationID,

            };
            _context.CalibrationExpireCalculation.Add(CalibrationExpire);
            var CalibrationServiceInstructionType = value.CalibrationInstructionTypeList;
            if (CalibrationServiceInstructionType.Count != 0)
            {
                CalibrationServiceInstructionType.ForEach(device =>
                {

                    var deviceType = new InstructionType()
                    {
                        DocumentName = device.DocumentName,
                        TypeId = 621,
                        No = device.No,
                        InstructionNo = device.InstructionNo,
                        VersionNo = device.VersionNo,
                        EffectiveDate = device.EffectiveDate,
                        Link = device.Link,
                        DeviceCatalogMasterId = device.DeviceCatalogMasterID,
                        CalibrationServiceInformationId = device.CalibrationServiceInformationID,

                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,


                    };
                    _context.InstructionType.Add(deviceType);
                }

                );
            }
            return value;
        }



        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDeviceCatalogMaster")]
        public DeviceCatalogMasterModel Put(DeviceCatalogMasterModel value)
        {
            var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == value.DeviceCatalogMasterID);

            deviceCatalogMaster.DeviceGroupListId = value.DeviceGroupListID;
            deviceCatalogMaster.ModelName = value.ModelName;
            deviceCatalogMaster.CalibrationTypeId = value.CalibrationTypeID;
            deviceCatalogMaster.Cataloglink = value.Cataloglink;
            deviceCatalogMaster.Status = value.Status;
            deviceCatalogMaster.ModifiedByUserId = value.ModifiedByUserID;
            deviceCatalogMaster.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDeviceCatalogMaster")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var deviceCatalogMaster = _context.DeviceCatalogMaster.SingleOrDefault(p => p.DeviceCatalogMasterId == id);
                if (deviceCatalogMaster != null)
                {
                    _context.DeviceCatalogMaster.Remove(deviceCatalogMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}