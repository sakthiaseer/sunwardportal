﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NoticeLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NoticeLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpPost]
        [Route("GetNoticesLineByType")]
        public List<NoticeLineModel> GetNoticesLineByType(NoticeModel searchData)
        {
            var notices = _context.NoticeLine.Include(a=>a.Notice).Include(a=>a.Notice.NoticeUser).Include(n=>n.Notice.Type).Include(a => a.AddedByUser).Include(c => c.Company).Include(m => m.Module).Include(m => m.ModifiedByUser).Include("StatusCode").Include(s => s.Frequency).Include(a => a.NoticeLineWeekly).Where(w=>w.StatusCodeId==1 && w.Notice.TypeId!=null && w.Notice.TypeId==searchData.TypeId && w.Notice.StatusCodeId==1);
            List<NoticeLineModel> noticeModels = new List<NoticeLineModel>();
            if (!string.IsNullOrWhiteSpace(searchData.Name))
            {
                notices = notices.Where(w => w.Notice.Name.Contains(searchData.Name) || w.Notice.Description.Contains(searchData.Name) || w.Module.CodeValue.Contains(searchData.Name));
            }
            var notice = notices.AsNoTracking().ToList();
            if (notice != null)
            {
                var noticeIds = notice.Select(s => s.NoticeId).ToList();
                notice.ForEach(s =>
                {
                    var show = true;
                    if (s.Notice != null && s.Notice.AddedByUserId != searchData.AddedByUserID)
                    {
                        if (s.Notice.NoticeUser != null && s.Notice.NoticeUser.Count > 0)
                        {
                            var useIds = s.Notice.NoticeUser.Where(a => a.UserId == searchData.AddedByUserID).Count();
                            show = useIds > 0 ? true : false;
                        }
                    }
                    if (show == true)
                    {
                        NoticeLineModel noticeModel = new NoticeLineModel
                        {
                            Name = s.Notice?.Name,
                            Description = s.Notice?.Description,
                            TypeId = s.Notice?.TypeId,
                            TypeName = s.Notice?.Type?.Value,
                            NoticeId = s.NoticeId,
                            NoticeLineId = s.NoticeLineId,
                            Link = s.Link,
                            Wilink = s.Wilink,
                            StatusCodeID = s.StatusCodeId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            ModuleId = s.ModuleId,
                            ModuleName = s.Module?.CodeValue,
                            FrequencyId = s.FrequencyId,
                            FrequencyName = s.Frequency?.CodeValue,
                            StartDate = s.StartDate,
                            EndDate = s.EndDate,
                            MonthlyDay = s.MonthlyDay,
                            DaysOfWeek = s.DaysOfWeek,
                            CompanyId = s.CompanyId,
                            CompanyName = s.Company?.PlantCode,
                            NoticeWeeklyIds = s.NoticeLineWeekly != null ? s.NoticeLineWeekly.Where(c => c.NoticeLineId == s.NoticeLineId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                            DaysOfWeekIds = s.NoticeLineWeekly != null ? s.NoticeLineWeekly.Where(c => c.NoticeLineId == s.NoticeLineId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        };
                        noticeModels.Add(noticeModel);
                    }
                });

            }
            return noticeModels.OrderByDescending(a => a.NoticeLineId).ToList();
        }
        [HttpGet]
        [Route("GetNoticesLine")]
        public List<NoticeLineModel> GetNoticesLine(long? id)
        {
            var notice = _context.NoticeLine.Include(a => a.AddedByUser).Include(c=>c.Company).Include(m => m.Module).Include(m => m.ModifiedByUser).Include("StatusCode").Include(s => s.Frequency).Include(a=>a.NoticeLineWeekly).Where(w => w.NoticeId == id).AsNoTracking().ToList();
            List<NoticeLineModel> noticeModels = new List<NoticeLineModel>();
            if (notice != null)
            {
                var noticeIds = notice.Select(s => s.NoticeId).ToList();
                notice.ForEach(s =>
                {
                    NoticeLineModel noticeModel = new NoticeLineModel
                    {
                        NoticeId = s.NoticeId,
                        NoticeLineId = s.NoticeLineId,
                        Link = s.Link,
                        Wilink=s.Wilink,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ModuleId = s.ModuleId,
                        ModuleName = s.Module?.CodeValue,
                        FrequencyId = s.FrequencyId,
                        FrequencyName = s.Frequency?.CodeValue,
                        StartDate = s.StartDate,
                        EndDate = s.EndDate,
                        MonthlyDay = s.MonthlyDay,
                        DaysOfWeek = s.DaysOfWeek,
                        CompanyId = s.CompanyId,
                        CompanyName = s.Company?.PlantCode,
                        NoticeWeeklyIds = s.NoticeLineWeekly != null ? s.NoticeLineWeekly.Where(c => c.NoticeLineId == s.NoticeLineId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                        DaysOfWeekIds = s.NoticeLineWeekly != null ? s.NoticeLineWeekly.Where(c => c.NoticeLineId == s.NoticeLineId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    };
                    noticeModels.Add(noticeModel);
                });
            }
            return noticeModels.OrderByDescending(a => a.NoticeId).ToList();
        }
        [HttpPost]
        [Route("InsertNoticeLine")]
        public NoticeLineModel Post(NoticeLineModel value)
        {
            var notice = new NoticeLine
            {
                NoticeId=value.NoticeId,
                Link = value.Link,
                Wilink = value.Wilink,
                ModuleId = value.ModuleId,
                FrequencyId=value.FrequencyId,
                StartDate=value.StartDate,
                EndDate=value.EndDate,
                MonthlyDay=value.MonthlyDay,
                DaysOfWeek=value.DaysOfWeek,
                CompanyId=value.CompanyId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.NoticeLine.Add(notice);
            _context.SaveChanges();
            value.NoticeLineId = notice.NoticeLineId;
            if (value.NoticeWeeklyIds.Count > 0)
            {
                value.NoticeWeeklyIds.ForEach(s =>
                {
                    if (value.FrequencyId == 2132)
                    {
                        var noticeUsers = new NoticeLineWeekly
                        {
                            WeeklyId = s,
                            CustomType = "Weekly",
                            NoticeLineId=value.NoticeLineId,
                        };
                        _context.NoticeLineWeekly.Add(noticeUsers);
                        _context.SaveChanges();
                    }
                });
            }
            if (value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(s =>
                {
                    if (value.DaysOfWeek == true && value.FrequencyId == 2134)
                    {
                        var noticeUsers = new NoticeLineWeekly
                        {
                            WeeklyId = s,
                            CustomType = "Yearly",
                            NoticeLineId = value.NoticeLineId,
                        };
                        _context.NoticeLineWeekly.Add(noticeUsers);
                        _context.SaveChanges();
                    }
                });
            }
           
            return value;
        }
        [HttpPut]
        [Route("UpdateNoticeLine")]
        public NoticeLineModel Put(NoticeLineModel value)
        {
            var notice = _context.NoticeLine.SingleOrDefault(p => p.NoticeLineId == value.NoticeLineId);
            notice.NoticeId = value.NoticeId;
            notice.Link = value.Link;
            notice.Wilink = value.Wilink;
            notice.ModuleId = value.ModuleId;
            notice.FrequencyId = value.FrequencyId;
            notice.StartDate = value.StartDate;
            notice.EndDate = value.EndDate;
            notice.MonthlyDay = value.MonthlyDay;
            notice.DaysOfWeek = value.DaysOfWeek;
            notice.CompanyId = value.CompanyId;
            notice.Link = value.Link;
            notice.ModifiedByUserId = value.ModifiedByUserID;
            notice.ModifiedDate = DateTime.Now;
            notice.StatusCodeId = value.StatusCodeID.Value;
            var noticeUserRemove = _context.NoticeLineWeekly.Where(w => w.NoticeLineId == value.NoticeLineId).ToList();
            if (noticeUserRemove != null)
            {
                _context.NoticeLineWeekly.RemoveRange(noticeUserRemove);
                _context.SaveChanges();
            }
            if (value.NoticeWeeklyIds.Count > 0)
            {
                value.NoticeWeeklyIds.ForEach(s =>
                {
                    if (value.FrequencyId == 2132)
                    {
                        var noticeUsers = new NoticeLineWeekly
                        {
                            NoticeLineId=value.NoticeLineId,
                            WeeklyId = s,
                            CustomType = "Weekly",
                        };
                        _context.NoticeLineWeekly.Add(noticeUsers);
                    }
                });
            }
            if (value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(s =>
                {
                    if (value.DaysOfWeek == true && value.FrequencyId == 2134)
                    {
                        var noticeUsers = new NoticeLineWeekly
                        {
                            WeeklyId = s,
                            CustomType = "Yearly",
                            NoticeLineId = value.NoticeLineId,
                        };
                        _context.NoticeLineWeekly.Add(noticeUsers);
                    }
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteNoticeLine")]
        public void Delete(int id)
        {
            try
            {
                var division = _context.NoticeLine.FirstOrDefault(p => p.NoticeLineId == id);
                if (division != null)
                {
                    var noticeUserRemove = _context.NoticeLineWeekly.Where(w => w.NoticeLineId == division.NoticeLineId).ToList();
                    if (noticeUserRemove != null)
                    {
                        _context.NoticeLineWeekly.RemoveRange(noticeUserRemove);
                        _context.SaveChanges();
                    }
                    _context.NoticeLine.Remove(division);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Notice Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
