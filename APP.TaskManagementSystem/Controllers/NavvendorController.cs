﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavvendorController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavvendorController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNavVendors")]
        public List<NavvendorModel> GetCustomer()
        {
            var navVendor = _context.Navvendor.Select(s => new NavvendorModel
            {
                VendorId = s.VendorId,
                VendorName = s.VendorName,
                VendorNo = s.VendorNo,
                VendorItemNo = s.VendorItemNo,
                VendorNames=s.VendorNo+"|"+s.VendorName,
            }).OrderByDescending(o => o.VendorId).AsNoTracking().ToList();
            return navVendor;
        }
        [HttpGet]
        [Route("GetNavVendorsByCountry")]
        public List<NavvendorModel> GetNavVendorsByCountry(long? id)
        {
            var navVendor = _context.Navvendor.Select(s => new NavvendorModel
            {
                VendorId = s.VendorId,
                VendorName = s.VendorName,
                VendorNo = s.VendorNo,
                VendorItemNo = s.VendorItemNo,
                VendorNames = s.VendorNo + "|" + s.VendorName,
                CompanyId=s.CompanyId,
            }).Where(w=>w.CompanyId==id).OrderByDescending(o => o.VendorId).AsNoTracking().ToList();
            return navVendor;
        }
    }
}