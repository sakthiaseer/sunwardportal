﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActivityController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public ProductionActivityController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host, IConfiguration config, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _config = config;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionActivities")]
        public List<ProductionActivityModel> Get()
        {
            List<ProductionActivityModel> productionActivityModels = new List<ProductionActivityModel>();
            var productionActivities = _context.ProductionActivity
                .Include(a => a.ActivityDocument)
                .Include(a => a.Process)
                .Include(a => a.AddedByUser)
                .Include(b => b.StatusCode).Include(c => c.ModifiedByUser)
                .AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.ToList();
            productionActivities.ForEach(s =>
            {
                ProductionActivityModel productionActivityModel = new ProductionActivityModel();
                productionActivityModel.ProductionActivityId = s.ProductionActivityId;
                productionActivityModel.Company = s.Company;
                productionActivityModel.TicketNo = s.TicketNo;
                productionActivityModel.FileProfileTypeId = s.FileProfileTypeId;
                productionActivityModel.BatchNo = s.BatchNo;
                productionActivityModel.IsManualEntry = s.IsManualEntry;
                productionActivityModel.SubLot = s.SubLot;
                productionActivityModel.Comment = s.Comment;
                productionActivityModel.IsNotification = s.IsNotification;
                productionActivityModel.ProcessId = s.ProcessId;
                productionActivityModel.ProcessName = s.Process?.Value;
                productionActivityModel.ActivityDocumentId = s.ActivityDocumentId;
                productionActivityModel.ActivityDocumentName = s.ActivityDocument?.Value;
                productionActivityModel.SessionId = s.SessionId;
                productionActivityModel.StatusCodeID = s.StatusCodeId;
                productionActivityModel.AddedByUserID = s.AddedByUserId;
                productionActivityModel.AddedByUser = s.AddedByUser?.UserName;
                productionActivityModel.ModifiedByUserID = s.ModifiedByUserId;
                productionActivityModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                productionActivityModel.FileProfileTypeId = s.FileProfileTypeId;
                productionActivityModel.IsFinalProcess = s.IsFinalProcess;
                productionActivityModel.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                productionActivityModel.ProfileReferenceNo = s.ProfileReferenceNo;
                productionActivityModel.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                productionActivityModels.Add(productionActivityModel);
            });
            return productionActivityModels.OrderByDescending(o => o.ProductionActivityId).ToList();
        }
        [HttpPost]
        [Route("GetProductionActivitiesRefByNo")]
        public List<ProductionActivityModel> GetProductionActivitiesRefByNo(RefSearchModel refSearchModel)
        {
            List<ProductionActivityModel> productionActivityModels = new List<ProductionActivityModel>();
            var productionActivity = _context.ProductionActivity
                .Include(a => a.ActivityDocument)
                .Include(a => a.Process)
                .Include(a => a.AddedByUser)
                .Include(b => b.StatusCode).Include(c => c.ModifiedByUser)
                .Where(w => w.ProductionActivityId > 0);
            if (refSearchModel.IsHeader)
            {
                productionActivity = productionActivity.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                productionActivity = productionActivity.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var productionActivities = productionActivity.AsNoTracking().ToList();
            productionActivities.ForEach(s =>
            {
                ProductionActivityModel productionActivityModel = new ProductionActivityModel();
                productionActivityModel.ProductionActivityId = s.ProductionActivityId;
                productionActivityModel.Company = s.Company;
                productionActivityModel.TicketNo = s.TicketNo;
                productionActivityModel.FileProfileTypeId = s.FileProfileTypeId;
                productionActivityModel.BatchNo = s.BatchNo;
                productionActivityModel.IsManualEntry = s.IsManualEntry;
                productionActivityModel.SubLot = s.SubLot;
                productionActivityModel.Comment = s.Comment;
                productionActivityModel.IsNotification = s.IsNotification;
                productionActivityModel.ProcessId = s.ProcessId;
                productionActivityModel.ProcessName = s.Process?.Value;
                productionActivityModel.ActivityDocumentId = s.ActivityDocumentId;
                productionActivityModel.ActivityDocumentName = s.ActivityDocument?.Value;
                productionActivityModel.SessionId = s.SessionId;
                productionActivityModel.StatusCodeID = s.StatusCodeId;
                productionActivityModel.AddedByUserID = s.AddedByUserId;
                productionActivityModel.AddedByUser = s.AddedByUser?.UserName;
                productionActivityModel.ModifiedByUserID = s.ModifiedByUserId;
                productionActivityModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                productionActivityModel.FileProfileTypeId = s.FileProfileTypeId;
                productionActivityModel.IsFinalProcess = s.IsFinalProcess;
                productionActivityModel.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                productionActivityModel.ProfileReferenceNo = s.ProfileReferenceNo;
                productionActivityModel.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                productionActivityModels.Add(productionActivityModel);
            });
            return productionActivityModels.OrderByDescending(o => o.ProductionActivityId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionActivityModel> GetData(SearchModel searchModel)
        {
            var productionActivity = new ProductionActivity();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).FirstOrDefault();
                        break;
                    case "Last":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).LastOrDefault();
                        break;
                    case "Next":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).LastOrDefault();
                        break;
                    case "Previous":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).FirstOrDefault();
                        break;
                    case "Last":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).LastOrDefault();
                        break;
                    case "Next":
                        productionActivity = _context.ProductionActivity.OrderBy(o => o.ProductionActivityId).FirstOrDefault(s => s.ProductionActivityId > searchModel.Id);
                        break;
                    case "Previous":
                        productionActivity = _context.ProductionActivity.OrderByDescending(o => o.ProductionActivityId).FirstOrDefault(s => s.ProductionActivityId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionActivityModel>(productionActivity);
            return result;
        }

        [HttpPost]
        [Route("InsertProductionActivity")]
        public async Task<ProductionActivityModel> Post(ProductionActivityModel value)
        {
            try
            {
                var isFinalProcessExist = _context.ProductionActivity.Any(s => s.IsFinalProcess == true && s.TicketNo == value.TicketNo);
                if (value.IsFinalProcess == true && isFinalProcessExist)
                {
                    value.IsError = true;
                    value.Errormessage = "Final Process already exist";
                    return value;
                }
                else
                {
                    var profileNo = "";
                    if (value.ProfileID != null)
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.TicketNo });
                    }
                    var sessionId = value.SessionId ?? Guid.NewGuid();
                    var productionActivity = new ProductionActivity
                    {
                        TicketNo = value.TicketNo,
                        BatchNo = value.BatchNo,
                        ProcessId = value.ProcessId,
                        ActivityDocumentId = value.ActivityDocumentId,
                        FileProfileTypeId = value.FileProfileTypeId,
                        AddedDate = DateTime.Now,
                        Company = value.CompanyName,
                        StatusCodeId = 1,
                        SessionId = sessionId,
                        AddedByUserId = value.AddedByUserId,
                        IsFinalProcess = value.IsFinalProcess,
                        IsManualEntry = value.IsManualEntry,
                        SubLot = value.SubLot,
                        Comment = value.Comment,
                        IsNotification = value.IsNotification,
                        ProfileReferenceNo = profileNo,
                        LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,

                    };
                    _context.ProductionActivity.Add(productionActivity);

                    //var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + sessionId.ToString() + ".png";
                    //System.IO.File.WriteAllBytes(serverPath, value.UploadPhoto);

                    //var documents = new Documents
                    //{
                    //    FileName = sessionId.ToString() + ".png",
                    //    ContentType = "image/png",
                    //    FileData = null,
                    //    FileSize = value.UploadPhoto.Length,
                    //    //Description = files.desc
                    //    UploadDate = DateTime.Now,
                    //    SessionId = sessionId,
                    //    IsTemp = true,
                    //    IsCompressed = true,
                    //    IsMobileUpload = true,

                    //};
                    //_context.Documents.Add(documents);
                    _context.SaveChanges();
                    value.ProductionActivityId = productionActivity.ProductionActivityId;
                    value.MasterProfileReferenceNo = productionActivity.MasterProfileReferenceNo;
                    var documents = _context.Documents.Select(d => new DocumentsModel { DocumentID = d.DocumentId, SessionId = d.SessionId }).FirstOrDefault(c => c.SessionId == productionActivity.SessionId);

                    if (isFinalProcessExist && value.IsFinalProcess == false)
                    {
                        //Get the records from linkdocument and link the document
                        var prodActivity = _context.ProductionActivity.FirstOrDefault(f => f.TicketNo == value.TicketNo && f.IsFinalProcess == true);
                        var document = _context.Documents.Select(d => new { DocumentId = d.DocumentId, SessionId = d.SessionId }).FirstOrDefault(s => s.SessionId == prodActivity.SessionId);
                        LinkActivityDocument(documents.DocumentID, document.DocumentId, value);
                    }
                    else if (!isFinalProcessExist && value.IsFinalProcess == true)
                    {
                        // Create final document, check the ticket linked document and create documents 
                        var prodActivities = _context.ProductionActivity.Where(f => f.TicketNo == value.TicketNo).Select(s => s.SessionId).ToList();
                        var documentItems = _context.Documents.Select(d => new { DocumentId = d.DocumentId, SessionId = d.SessionId }).Where(s => prodActivities.Contains(s.SessionId)).ToList();
                        foreach (var item in documentItems)
                        {
                            LinkActivityDocument(documents.DocumentID, item.DocumentId, value);
                        }
                    }

                    return value;
                }
            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        private void LinkActivityDocument(long? documentId, long? linkDocumentId, ProductionActivityModel productionActivityModel)
        {
            if (documentId != linkDocumentId)
            {
                var DocumentLink = new DocumentLink
                {
                    DocumentId = documentId,
                    AddedByUserId = productionActivityModel.AddedByUserId.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                    LinkDocumentId = linkDocumentId,
                    FileProfieTypeId = productionActivityModel.FileProfileTypeId,
                };
                _context.DocumentLink.Add(DocumentLink);
                _context.SaveChanges();
            }
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionActivity")]
        public ProductionActivityModel Put(ProductionActivityModel value)
        {
            var productionActivity = _context.ProductionActivity.SingleOrDefault(p => p.ProductionActivityId == value.ProductionActivityId);
            var sessionId = value.SessionId ?? Guid.NewGuid();
            productionActivity.TicketNo = value.TicketNo;
            productionActivity.BatchNo = value.BatchNo;
            productionActivity.ProcessId = value.ProcessId;
            productionActivity.ActivityDocumentId = value.ActivityDocumentId;
            productionActivity.FileProfileTypeId = value.FileProfileTypeId;
            productionActivity.ModifiedDate = DateTime.Now;
            productionActivity.Company = value.CompanyName;
            productionActivity.StatusCodeId = value.StatusCodeID;
            productionActivity.SessionId = sessionId;
            productionActivity.ModifiedByUserId = value.ModifiedByUserID;
            productionActivity.FileProfileTypeId = value.FileProfileTypeId;
            productionActivity.IsFinalProcess = value.IsFinalProcess;
            productionActivity.IsManualEntry = value.IsManualEntry;
            productionActivity.SubLot = value.SubLot;
            productionActivity.Comment = value.Comment;
            productionActivity.IsNotification = value.IsNotification;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionActivity")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var productionActivity = _context.ProductionActivity.SingleOrDefault(p => p.ProductionActivityId == id);
                if (productionActivity != null)
                {
                    _context.ProductionActivity.Remove(productionActivity);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
