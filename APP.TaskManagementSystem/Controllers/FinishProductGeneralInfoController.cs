﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FinishProductGeneralInfoController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FinishProductGeneralInfoController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFinishProductGeneralInfos")]
        public List<FinishProductGeneralInfoModel> Get()
        {
           var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishProductGeneralInfo = _context.FinishProductGeneralInfo.
                Include(a => a.AddedByUser)
                .Include(m => m.RegistrationDocument)
                .Include(c => c.RegisterationCode)
                .Include(s => s.StatusCode)
                .Include(r => r.FinishProduct)
                .Include(g => g.ModifiedUser).AsNoTracking().ToList();
            List<FinishProductGeneralInfoModel> finishProductGeneralInfoModel = new List<FinishProductGeneralInfoModel>();
            finishProductGeneralInfo.ForEach(s =>
            {
                FinishProductGeneralInfoModel finishProductGeneralInfoModels = new FinishProductGeneralInfoModel
                {
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    FinishProductID = s.FinishProductId,
                    ProductName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.ProductId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.ProductId).Value : "",
                    DosageFormId = s.FinishProduct.DosageFormId.Value,
                    DosageFormName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.DosageFormId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.DosageFormId).Value : "",
                    ManufacturingSite = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct?.ManufacturingSiteId).Value : "",
                    RegisterCountry = s.RegisterCountry,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    RegisterCountryProductName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegisterCountry) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegisterCountry).Value : "",
                    RegisterProductOwnerID = s.RegisterProductOwnerId,
                    DrugClassificationID = s.DrugClassificationId,
                    CallNo = s.CallNo,
                    RegistrationReferenceNo = s.RegistrationReferenceNo,
                    RegistrationNo = s.RegistrationNo,
                    RegistrationDateOfIssue = s.RegistrationDateOfIssue,
                    RegistrationDateOfApproval = s.RegistrationDateOfApproval,
                    RegistrationDateOfExpiry = s.RegistrationDateOfExpiry,
                    RegistrationDocumentID = s.RegistrationDocumentId,
                    RegistrationDateOfSubmission = s.RegistrationDateOfSubmission,

                    RegistrationProductCategoryId = s.RegistrationProductCategoryId,
                    RegistrationCategoryClassId = s.RegistrationCategoryClassId,

                    RegistrationCategoryClassName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCategoryClassId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCategoryClassId).Value : "",
                    RegistrationProductCategoryName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationProductCategoryId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationProductCategoryId).Value : "",
                    ModifiedByUserID = s.ModifiedUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionID = s.SessionId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedUser != null ? s.ModifiedUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    linkComment = s.LinkComment,
                    LinkDmscomment = s.LinkDmscomment,
                    RegistrationDocument = s.RegistrationDocument != null ? s.RegistrationDocument.FileName : "",
                    ProductRegistrationHolderId = s.ProductRegistrationHolderId,
                    PrhspecificProductId = s.PrhspecificProductId,
                    ProductionRegistrationHolderId = s.ProductionRegistrationHolderId,
                    ProductionRegistrationHolderName = s.ProductionRegistrationHolder?.CompanyName,
                    PrhspecificProductName = s.PrhspecificProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "",
                    ProductRegistrationHolderName = s.ProductRegistrationHolderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "",
                    CountryName = s.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "",
                    ProductOwner = s.RegisterProductOwnerId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : "",
                    DrugClassification = s.DrugClassificationId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.DrugClassificationId).Select(m => m.Value).FirstOrDefault() : "",
                };
                finishProductGeneralInfoModel.Add(finishProductGeneralInfoModels);
            });
            if (finishProductGeneralInfoModel.Count > 0 && finishProductGeneralInfoModel != null)
            {
                List<long> generalInfoIds = finishProductGeneralInfoModel.Select(g => g.FinishProductGeneralInfoID).ToList();

                var countryInformationStatus = _context.CountryInformationStatus.Where(s => generalInfoIds.Contains(s.FinishProductGeneralInfoId.Value)).ToList();

                finishProductGeneralInfoModel.ForEach(f =>
                {
                    var countryInformation = countryInformationStatus.LastOrDefault(c => c.FinishProductGeneralInfoId == f.FinishProductGeneralInfoID);
                    f.FPProductName = f.ProductName + '|' + f.ManufacturingSite;
                    if (countryInformation != null)
                    {
                        f.Remarks = countryInformation.Remarks;
                        f.StatusCodeID = countryInformation.StatusCodeId;
                    }
                });
            }
            return finishProductGeneralInfoModel.OrderByDescending(o => o.FinishProductGeneralInfoID).ToList();
        }

        [HttpGet]
        [Route("GetFinishProductGeneralInfoByManufacuringId")]
        public List<FinishProductGeneralInfoModel> GetFinishProductGeneralInfoByManufacuringId(int id)
        {
            var finishproductIds = _context.FinishProduct.Where(f => f.ManufacturingSiteId == id).AsNoTracking().Select(t => t.FinishProductId).ToList();
            //var  FinishProductGeneralInfo = _context. FinishProductGeneralInfo.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new  FinishProductGeneralInfoModel
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishProductGeneralInfo = _context.FinishProductGeneralInfo.
                    Include(a => a.AddedByUser)
                    .Include(m => m.RegistrationDocument)
                    .Include(c => c.RegisterationCode)
                    .Include(s => s.StatusCode)
                    .Include(r => r.FinishProduct)
                    .Include(g => g.ModifiedUser).AsNoTracking().ToList();
            List<FinishProductGeneralInfoModel> finishProductGeneralInfoModel = new List<FinishProductGeneralInfoModel>();
            finishProductGeneralInfo.ForEach(s =>
            {
                FinishProductGeneralInfoModel finishProductGeneralInfoModels = new FinishProductGeneralInfoModel
                {
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    FinishProductID = s.FinishProductId,
                    ProductName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId).Value : "",
                    DosageFormId = s.FinishProduct.DosageFormId.Value,
                    DosageFormName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.DosageFormId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.DosageFormId).Value : "",
                    ManufacturingSite = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId).Value : "",

                    RegisterCountry = s.RegisterCountry,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                    RegisterCountryProductName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegisterCountry) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegisterCountry).Value : "",
                    RegisterProductOwnerID = s.RegisterProductOwnerId,
                    DrugClassificationID = s.DrugClassificationId,
                    CallNo = s.CallNo,
                    RegistrationReferenceNo = s.RegistrationReferenceNo,
                    RegistrationNo = s.RegistrationNo,
                    RegistrationDateOfIssue = s.RegistrationDateOfIssue,
                    RegistrationDateOfApproval = s.RegistrationDateOfApproval,
                    RegistrationDateOfExpiry = s.RegistrationDateOfExpiry,
                    RegistrationDocumentID = s.RegistrationDocumentId,
                    RegistrationDateOfSubmission = s.RegistrationDateOfSubmission,

                    RegistrationProductCategoryId = s.RegistrationProductCategoryId,
                    RegistrationCategoryClassId = s.RegistrationCategoryClassId,

                    RegistrationCategoryClassName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCategoryClassId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCategoryClassId).Value : "",
                    RegistrationProductCategoryName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationProductCategoryId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationProductCategoryId).Value : "",
                    ModifiedByUserID = s.ModifiedUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedUser != null ? s.ModifiedUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    linkComment = s.LinkComment,
                    LinkDmscomment = s.LinkDmscomment,
                    RegistrationDocument = s.RegistrationDocument != null ? s.RegistrationDocument.FileName : "",
                    ProductRegistrationHolderId = s.ProductRegistrationHolderId,
                    PrhspecificProductId = s.PrhspecificProductId,
                    ProductionRegistrationHolderId = s.ProductionRegistrationHolderId,
                    ProductionRegistrationHolderName = s.ProductionRegistrationHolder?.CompanyName,
                    PrhspecificProductName = s.PrhspecificProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "",
                    ProductRegistrationHolderName = s.ProductRegistrationHolderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "",
                    CountryName = s.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "",
                    ProductOwner = s.RegisterProductOwnerId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : "",
                    DrugClassification = s.DrugClassificationId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.DrugClassificationId).Select(m => m.Value).FirstOrDefault() : "",
                };
                finishProductGeneralInfoModel.Add(finishProductGeneralInfoModels);
            });
            if (finishProductGeneralInfoModel.Count > 0 && finishProductGeneralInfoModel != null)
            {
                finishProductGeneralInfoModel.ForEach(f =>
                {
                    f.FPProductName = f.ProductName + '|' + f.ManufacturingSite;
                });
            }
            return finishProductGeneralInfoModel.Where(t => finishproductIds.Contains(t.FinishProductID.Value)).OrderByDescending(o => o.FinishProductGeneralInfoID).ToList();

        }
        [HttpGet]
        [Route("GetFinishProductGeneralInfoLines")]
        public List<FinishProductGeneralInfoLineModel> GetFinishProductGeneralInfoLines(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var specialInstruction = _context.FinishProductGeneralInfoSpecialIns.AsNoTracking().ToList();
            var finishProductGeneralInfoLine = _context.FinishProdcutGeneralInfoLine.
                   Include(a => a.RegistrationPackingStatus).AsNoTracking().ToList();
            List<FinishProductGeneralInfoLineModel> finishProductGeneralInfoLineModel = new List<FinishProductGeneralInfoLineModel>();
            finishProductGeneralInfoLine.ForEach(s =>
            {
                FinishProductGeneralInfoLineModel finishProductGeneralInfoLineModels = new FinishProductGeneralInfoLineModel
                {
                    FinishProductGeneralInfoID = s.FinishProductGeneralInfoId,
                    FinishProductGeneralInfoLineID = s.FinishProductGeneralInfoLineId,
                    PackagingTypeID = s.PackagingTypeId,
                    PackTypeID = s.PackTypeId,
                    PackSize = s.PackSize,
                    PerUnitQTY = s.PerUnitQty,
                    ShelfLife = s.ShelfLife,
                    StorageCondition = s.StorageCondition,
                    PackType = s.PackTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackTypeId).Select(m => m.Value).FirstOrDefault() : "",
                    PackagingType = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                    Size = s.PackSize != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackSize).Select(m => m.Value).FirstOrDefault() : "",
                    PackQty = s.PackQty,
                    PackQtyunitId = s.PackQtyunitId,
                    PackQtyunitName = s.PackQtyunitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackQtyunitId).Select(m => m.Value).FirstOrDefault() : "",
                    PerPackId = s.PerPackId,
                    PerPackName = s.PerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerPackId).Select(m => m.Value).FirstOrDefault() : "",
                    EquvalentSmallestQty = s.EquvalentSmallestQty,
                    EquvalentSmallestUnitId = s.EquvalentSmallestUnitId,
                    EquvalentSmallestUnitName = s.EquvalentSmallestUnitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.EquvalentSmallestUnitId).Select(m => m.Value).FirstOrDefault() : "",
                    FactorOfSmallestProductionPack = s.FactorOfSmallestProductionPack,
                    SalesPerPackId = s.SalesPerPackId,
                    SalesPerPackName = s.SalesPerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "",
                    TemparatureConditionId = s.TemparatureConditionId,
                    TemparatureConditionName = s.TemparatureConditionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TemparatureConditionId).Select(m => m.Value).FirstOrDefault() : "",
                    Temparature = s.Temparature,
                    IsProtectFromLight = s.IsProtectFromLight,
                    IsProtectFromLightStatus = s.IsProtectFromLight == true ? "1" : "0",
                    RegistrationSalesId = s.RegistrationSalesId,
                    StorageConditionId = specialInstruction.Where(w => w.FinishProductGeneralInfoLineId == s.FinishProductGeneralInfoLineId).Select(w => w.SpecialInstructionId.Value).ToList(),
                    RegistrationPackingStatusId = s.RegistrationPackingStatusId,
                    RegistrationSalesName = s.RegistrationSalesId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegistrationSalesId).Select(m => m.Value).FirstOrDefault() : "",
                    StorageConditionName = s.StorageConditionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.StorageConditionId).Select(m => m.Value).FirstOrDefault() : "",
                    RegistrationPackingStatusName = s.RegistrationPackingStatus != null ? s.RegistrationPackingStatus.CodeValue : "",
                    CapacityId = s.CapacityId,
                    ClosureId = s.ClosureId,
                    LinerId = s.LinerId,
                    CapacityName = s.CapacityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CapacityId).Select(m => m.Value).FirstOrDefault() : "",
                    ClosureName = s.ClosureId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ClosureId).Select(m => m.Value).FirstOrDefault() : "",
                    LinerName = s.LinerId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.LinerId).Select(m => m.Value).FirstOrDefault() : "",





                };
                finishProductGeneralInfoLineModel.Add(finishProductGeneralInfoLineModels);
            });
            if (finishProductGeneralInfoLineModel != null && finishProductGeneralInfoLineModel.Count > 0)
            {
                finishProductGeneralInfoLineModel.ForEach(f =>
                {
                    f.FPProductInfoLines = f.PackagingType + '|' + f.PackQty + '|' + f.PerPackName + '|' + f.FactorOfSmallestProductionPack;
                });
            }
            return finishProductGeneralInfoLineModel.Where(o => o.FinishProductGeneralInfoID == id).OrderByDescending(t => t.FinishProductGeneralInfoLineID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FinishProductGeneralInfoModel> GetData(SearchModel searchModel)
        {
            var finishProductGeneralInfo = new FinishProductGeneralInfo();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).FirstOrDefault();
                        break;
                    case "Last":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).LastOrDefault();
                        break;
                    case "Next":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).LastOrDefault();
                        break;
                    case "Previous":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).FirstOrDefault();
                        break;
                    case "Last":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).LastOrDefault();
                        break;
                    case "Next":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderBy(o => o.FinishProductGeneralInfoId).FirstOrDefault(s => s.FinishProductGeneralInfoId > searchModel.Id);
                        break;
                    case "Previous":
                        finishProductGeneralInfo = _context.FinishProductGeneralInfo.OrderByDescending(o => o.FinishProductGeneralInfoId).FirstOrDefault(s => s.FinishProductGeneralInfoId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<FinishProductGeneralInfoModel>(finishProductGeneralInfo);
            if (result != null)
            {
                if (result.RegistrationDocumentID != null)
                {
                    result.RegistrationDocument = _context.Documents.Select(s=>new { s.DocumentId, s.SessionId,s.FileName }).FirstOrDefault(s => s.DocumentId == result.RegistrationDocumentID)?.FileName;
                }

                result.PrhspecificProductName = result.PrhspecificProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == result.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "";
                result.ProductRegistrationHolderName = result.ProductRegistrationHolderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == result.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "";
                result.ProductOwner = result.RegisterProductOwnerID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == result.RegisterProductOwnerID).Select(m => m.Value).FirstOrDefault() : "";
                result.CountryName = result.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == result.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "";
                result.DrugClassification = result.DrugClassificationID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == result.DrugClassificationID).Select(m => m.Value).FirstOrDefault() : "";
                var finishproduct = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == result.FinishProductID);
                if (finishproduct != null)
                {
                    result.ProductName = finishproduct.ProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ProductId).Select(m => m.Value).FirstOrDefault() : "";
                    result.ManufacturingSite = finishproduct.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "";
                }
            }
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertFinishProductGeneralInfo")]
        public FinishProductGeneralInfoModel Post(FinishProductGeneralInfoModel value)
        {
            var SessionId = Guid.NewGuid();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            Documents docu = new Documents();
            if (value.SessionID != null)
            {
                docu = _context.Documents.Select(s=>new Documents {SessionId = s.SessionId,DocumentId=s.DocumentId,FileName=s.FileName}).FirstOrDefault(d => d.SessionId == value.SessionID);
            }
            var finishProductGeneralInfo = new FinishProductGeneralInfo
            {
                // FinishProductGeneralInfoID = value. FinishProductGeneralInfoId,
                FinishProductId = value.FinishProductID,
                RegisterCountry = value.RegisterCountry.Value,
                RegisterationCodeId = value.RegisterationCodeId,
                RegisterCountryProductName = value.RegisterCountryProductName,
                RegisterProductOwnerId = value.RegisterProductOwnerID,
                DrugClassificationId = value.DrugClassificationID,
                CallNo = value.CallNo,
                RegistrationReferenceNo = value.RegistrationReferenceNo,
                RegistrationNo = value.RegistrationNo,
                RegistrationDateOfIssue = value.RegistrationDateOfIssue,
                RegistrationDateOfApproval = value.RegistrationDateOfApproval,
                RegistrationDateOfExpiry = value.RegistrationDateOfExpiry,
                RegistrationDateOfSubmission = value.RegistrationDateOfSubmission,
                ProductRegistrationHolderId = value.ProductRegistrationHolderId,
                ProductionRegistrationHolderId = value.ProductionRegistrationHolderId,
                PrhspecificProductId = value.PrhspecificProductId,
                SessionId = SessionId,
                RegistrationCategoryClassId = value.RegistrationCategoryClassId,
                RegistrationProductCategoryId = value.RegistrationProductCategoryId,
                LinkComment = value.linkComment,
                LinkDmscomment = value.LinkDmscomment,
                AddedByUserId = value.AddedByUserID,
                ModifiedUserId = value.ModifiedByUserID,
                StatusCodeId = value.StatusCodeID,
                AddedDate = DateTime.Now,
                RegistrationDocumentId = docu.DocumentId > 0 ? docu.DocumentId : value.RegistrationDocumentID > 0 ? value.RegistrationDocumentID : default(long?),


            };
            value.PrhspecificProductName = value.PrhspecificProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "";
            value.ProductRegistrationHolderName = value.ProductRegistrationHolderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "";
            value.ProductOwner = value.RegisterProductOwnerID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisterProductOwnerID).Select(m => m.Value).FirstOrDefault() : "";
            value.CountryName = value.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "";
            value.DrugClassification = value.DrugClassificationID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.DrugClassificationID).Select(m => m.Value).FirstOrDefault() : "";
            _context.FinishProductGeneralInfo.Add(finishProductGeneralInfo);
            _context.SaveChanges();
            value.FinishProductGeneralInfoID = finishProductGeneralInfo.FinishProductGeneralInfoId;
            value.RegistrationDocumentID = finishProductGeneralInfo.RegistrationDocumentId;
            var finishproduct = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == value.FinishProductID);
            value.ProductName = finishproduct.ProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ProductId).Select(m => m.Value).FirstOrDefault() : "";
            value.ManufacturingSite = finishproduct.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "";
            value.SessionID = finishProductGeneralInfo.SessionId;
            return value;
        }
        [HttpPost]
        [Route("InsertFinishProductGeneralInfoLine")]
        public FinishProductGeneralInfoLineModel InsertFinishProductGeneralInfoLine(FinishProductGeneralInfoLineModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();

            var finishProductGeneralInfoLine = new FinishProdcutGeneralInfoLine
            {
                FinishProductGeneralInfoId = value.FinishProductGeneralInfoID,
                PackagingTypeId = value.PackagingTypeID,
                PackTypeId = value.PackTypeID,
                PackSize = value.PackSize,
                ShelfLife = value.ShelfLife,
                PerUnitQty = value.PerUnitQTY,
                StorageCondition = value.StorageCondition,
                PackQty = value.PackQty,
                PackQtyunitId = value.PackQtyunitId,
                PerPackId = value.PerPackId,
                EquvalentSmallestQty = value.EquvalentSmallestQty,
                EquvalentSmallestUnitId = value.EquvalentSmallestUnitId,
                FactorOfSmallestProductionPack = value.FactorOfSmallestProductionPack,
                SalesPerPackId = value.SalesPerPackId,
                TemparatureConditionId = value.TemparatureConditionId,
                Temparature = value.Temparature,
                IsProtectFromLight = value.IsProtectFromLightStatus == "1" ? true : false,
                RegistrationSalesId = value.RegistrationSalesId,
                //StorageConditionId=value.StorageConditionId,
                RegistrationPackingStatusId = value.RegistrationPackingStatusId,
                ClosureId = value.ClosureId,
                CapacityId =    value.CapacityId,   
                LinerId = value.LinerId,    


            };
            if (value.StorageConditionId != null)
            {
                value.StorageConditionId.ForEach(c =>
                {
                    var specialInstruction = new FinishProductGeneralInfoSpecialIns
                    {
                        SpecialInstructionId = c,
                    };
                    finishProductGeneralInfoLine.FinishProductGeneralInfoSpecialIns.Add(specialInstruction);
                });
            }
            _context.FinishProdcutGeneralInfoLine.Add(finishProductGeneralInfoLine);
            _context.SaveChanges();
            value.PackType = value.PackTypeID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackTypeID).Select(m => m.Value).FirstOrDefault() : "";
            value.PackagingType = value.PackagingTypeID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingTypeID).Select(m => m.Value).FirstOrDefault() : "";
            value.Size = value.PackSize != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackSize).Select(m => m.Value).FirstOrDefault() : "";
            value.PackQtyunitName = value.PackQtyunitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackQtyunitId).Select(m => m.Value).FirstOrDefault() : "";
            value.PerPackName = value.PerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PerPackId).Select(m => m.Value).FirstOrDefault() : "";
            value.EquvalentSmallestUnitName = value.EquvalentSmallestUnitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.EquvalentSmallestUnitId).Select(m => m.Value).FirstOrDefault() : "";
            value.SalesPerPackName = value.SalesPerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "";
            value.TemparatureConditionName = value.TemparatureConditionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.TemparatureConditionId).Select(m => m.Value).FirstOrDefault() : "";
            value.FinishProductGeneralInfoLineID = finishProductGeneralInfoLine.FinishProductGeneralInfoLineId;
            value.CapacityName = value.CapacityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.CapacityId).Select(m => m.Value).FirstOrDefault() : "";
            value.ClosureName = value.ClosureId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ClosureId).Select(m => m.Value).FirstOrDefault() : "";
            value.LinerName = value.LinerId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.LinerId).Select(m => m.Value).FirstOrDefault() : "";

            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFinishProductGeneralInfo")]
        public FinishProductGeneralInfoModel Put(FinishProductGeneralInfoModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var finishProductGeneralInfo = _context.FinishProductGeneralInfo.SingleOrDefault(p => p.FinishProductGeneralInfoId == value.FinishProductGeneralInfoID);
            Documents docu = new Documents();
            if (value.SessionID != null)
            {
                docu = _context.Documents.Select(s => new Documents { SessionId = s.SessionId, DocumentId = s.DocumentId, FileName = s.FileName }).FirstOrDefault(d => d.SessionId == value.SessionID);

            }
            finishProductGeneralInfo.FinishProductId = value.FinishProductID;
            finishProductGeneralInfo.RegisterationCodeId = value.RegisterationCodeId;
            finishProductGeneralInfo.RegisterCountry = value.RegisterCountry.Value;
            finishProductGeneralInfo.RegisterCountryProductName = value.RegisterCountryProductName;
            finishProductGeneralInfo.RegisterProductOwnerId = value.RegisterProductOwnerID;
            finishProductGeneralInfo.DrugClassificationId = value.DrugClassificationID;
            finishProductGeneralInfo.CallNo = value.CallNo;
            finishProductGeneralInfo.RegistrationReferenceNo = value.RegistrationReferenceNo;
            finishProductGeneralInfo.RegistrationNo = value.RegistrationNo;
            finishProductGeneralInfo.RegistrationDateOfIssue = value.RegistrationDateOfIssue;
            finishProductGeneralInfo.RegistrationDateOfApproval = value.RegistrationDateOfApproval;
            finishProductGeneralInfo.RegistrationDateOfExpiry = value.RegistrationDateOfExpiry;
            finishProductGeneralInfo.RegistrationDateOfSubmission = value.RegistrationDateOfSubmission;
            finishProductGeneralInfo.RegistrationProductCategoryId = value.RegistrationProductCategoryId;
            finishProductGeneralInfo.RegistrationCategoryClassId = value.RegistrationCategoryClassId;
            finishProductGeneralInfo.LinkComment = value.linkComment;
            finishProductGeneralInfo.LinkDmscomment = value.LinkDmscomment;
            finishProductGeneralInfo.AddedByUserId = value.AddedByUserID;
            finishProductGeneralInfo.ModifiedUserId = value.ModifiedByUserID;
            finishProductGeneralInfo.StatusCodeId = value.StatusCodeID;
            finishProductGeneralInfo.AddedDate = value.AddedDate;
            finishProductGeneralInfo.ModifiedDate = DateTime.Now;
            if (finishProductGeneralInfo.SessionId == null && value.SessionID == null)
            {
                var SessionId = Guid.NewGuid();
                finishProductGeneralInfo.SessionId = SessionId;
            }
            else
            {
                
                finishProductGeneralInfo.SessionId = value.SessionID;
            }
            if (docu != null && docu.DocumentId>0)
            {
                finishProductGeneralInfo.RegistrationDocumentId = docu?.DocumentId;
            }
            else
            {
                finishProductGeneralInfo.RegistrationDocumentId = value.RegistrationDocumentID;
            }
            finishProductGeneralInfo.ProductRegistrationHolderId = value.ProductRegistrationHolderId;
            finishProductGeneralInfo.ProductionRegistrationHolderId = value.ProductionRegistrationHolderId;
            finishProductGeneralInfo.PrhspecificProductId = value.PrhspecificProductId;
            _context.SaveChanges();
            value.SessionID = finishProductGeneralInfo.SessionId;
            value.RegistrationDocumentID = finishProductGeneralInfo.RegistrationDocumentId;
            value.PrhspecificProductName = value.PrhspecificProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "";
            value.ProductRegistrationHolderName = value.ProductRegistrationHolderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : "";
            value.ProductOwner = value.RegisterProductOwnerID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisterProductOwnerID).Select(m => m.Value).FirstOrDefault() : "";
            value.CountryName = value.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "";
            value.DrugClassification = value.DrugClassificationID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.DrugClassificationID).Select(m => m.Value).FirstOrDefault() : "";
            var finishproduct = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == value.FinishProductID);
            value.ProductName = finishproduct.ProductId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ProductId).Select(m => m.Value).FirstOrDefault() : "";
            value.ManufacturingSite = finishproduct.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == finishproduct.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpPut]
        [Route("UpdateFinishProductGeneralInfoLine")]
        public FinishProductGeneralInfoLineModel UpdateFinishProductGeneralInfoLine(FinishProductGeneralInfoLineModel value)
        {
            var finishProductGeneralInfoline = _context.FinishProdcutGeneralInfoLine.Where(p => p.FinishProductGeneralInfoLineId == value.FinishProductGeneralInfoLineID).FirstOrDefault();

            finishProductGeneralInfoline.PackagingTypeId = value.PackagingTypeID;
            finishProductGeneralInfoline.PackTypeId = value.PackTypeID;
            finishProductGeneralInfoline.PackSize = value.PackSize;
            finishProductGeneralInfoline.PerUnitQty = value.PerUnitQTY;
            finishProductGeneralInfoline.ShelfLife = value.ShelfLife;
            finishProductGeneralInfoline.StorageCondition = value.StorageCondition;
            finishProductGeneralInfoline.PackQty = value.PackQty;
            finishProductGeneralInfoline.PackQtyunitId = value.PackQtyunitId;
            finishProductGeneralInfoline.PerPackId = value.PerPackId;
            finishProductGeneralInfoline.EquvalentSmallestQty = value.EquvalentSmallestQty;
            finishProductGeneralInfoline.EquvalentSmallestUnitId = value.EquvalentSmallestUnitId;
            finishProductGeneralInfoline.FactorOfSmallestProductionPack = value.FactorOfSmallestProductionPack;
            finishProductGeneralInfoline.SalesPerPackId = value.SalesPerPackId;
            finishProductGeneralInfoline.TemparatureConditionId = value.TemparatureConditionId;
            finishProductGeneralInfoline.Temparature = value.Temparature;
            finishProductGeneralInfoline.IsProtectFromLight = value.IsProtectFromLightStatus == "1" ? true : false;
            finishProductGeneralInfoline.RegistrationSalesId = value.RegistrationSalesId;
            finishProductGeneralInfoline.RegistrationPackingStatusId = value.RegistrationPackingStatusId;
            finishProductGeneralInfoline.CapacityId = value.CapacityId; 
            finishProductGeneralInfoline.ClosureId = value.ClosureId; 
            finishProductGeneralInfoline.LinerId = value.LinerId;   
            var specialInstructionList = _context.FinishProductGeneralInfoSpecialIns.Where(l => l.FinishProductGeneralInfoLineId == value.FinishProductGeneralInfoLineID).ToList();
            if (specialInstructionList.Count > 0)
            {
                _context.FinishProductGeneralInfoSpecialIns.RemoveRange(specialInstructionList);
            }
            if (value.StorageConditionId != null)
            {
                value.StorageConditionId.ForEach(c =>
                {
                    var specialInstruction = new FinishProductGeneralInfoSpecialIns
                    {
                        SpecialInstructionId = c,
                    };
                    finishProductGeneralInfoline.FinishProductGeneralInfoSpecialIns.Add(specialInstruction);
                });
            }
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFinishProductGeneralInfo")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var finishProductGeneralInfo = _context.FinishProductGeneralInfo.SingleOrDefault(p => p.FinishProductGeneralInfoId == id);
                if (finishProductGeneralInfo != null)
                {
                    var finishProductGeneralInfoline = _context.FinishProdcutGeneralInfoLine.Where(s => s.FinishProductGeneralInfoId == id).ToList();
                    if (finishProductGeneralInfoline != null)
                    {
                        finishProductGeneralInfoline.ForEach(c =>
                        {
                            var FinishProductGeneralInfoSpecialIns = _context.FinishProductGeneralInfoSpecialIns.Where(f => f.FinishProductGeneralInfoLineId == c.FinishProductGeneralInfoLineId).ToList();
                            _context.FinishProductGeneralInfoSpecialIns.RemoveRange(FinishProductGeneralInfoSpecialIns);
                            _context.SaveChanges();
                        });
                        _context.FinishProdcutGeneralInfoLine.RemoveRange(finishProductGeneralInfoline);
                        _context.SaveChanges();
                    }
                    _context.FinishProductGeneralInfo.Remove(finishProductGeneralInfo);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteFinishProductGeneralInfoLine")]
        public ActionResult<string> DeleteFinishProductGeneralInfoLine(int id)
        {
            try
            {
                var finishProductGeneralInfoline = _context.FinishProdcutGeneralInfoLine.Where(p => p.FinishProductGeneralInfoLineId == id).FirstOrDefault();
                if (finishProductGeneralInfoline != null)
                {
                    var specialInstructionList = _context.FinishProductGeneralInfoSpecialIns.Where(l => l.FinishProductGeneralInfoLineId == id).ToList();
                    if (specialInstructionList.Count > 0)
                    {
                        _context.FinishProductGeneralInfoSpecialIns.RemoveRange(specialInstructionList);
                    }
                    _context.FinishProdcutGeneralInfoLine.Remove(finishProductGeneralInfoline);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        [Route("GetPacksizeSearch")]
        public List<FinishProductGeneralInfoLineModel> GetPacksizeSearch(GeneralInfoSearchModel searchModel)
        {
            List<FinishProductGeneralInfoLineModel> generalInfoLineModels = new List<FinishProductGeneralInfoLineModel>();

            List<ApplicationMasterDetail> applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var query = _context.FinishProductGeneralInfo.Include("FinishProdcutGeneralInfoLine").Where(s => s.FinishProductId == searchModel.ProductID);

            if (searchModel.OwnerID > 0)
            {
                query = query.Where(o => o.RegisterProductOwnerId == searchModel.OwnerID);
            }
            if (searchModel.HolderID > 0)
            {
                query = query.Where(o => o.ProductRegistrationHolderId == searchModel.HolderID);
            }
            if (searchModel.CountryID > 0)
            {
                query = query.Where(o => o.RegisterCountry == searchModel.CountryID);
            }

            var generalInfolines = query.SelectMany(g => g.FinishProdcutGeneralInfoLine).ToList();
            generalInfoLineModels = generalInfolines.Select(line => new FinishProductGeneralInfoLineModel
            {
                PackType = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackTypeId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackTypeId).Value : string.Empty,
                PackagingType = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackagingTypeId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackagingTypeId).Value : string.Empty,
                PackSize = line.PackSize,
                PerUnitQTY = line.PerUnitQty,
                ShelfLife = line.ShelfLife,
                StorageCondition = line.StorageCondition,
                PackQty = line.PackQty,
                PackQtyunitName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackQtyunitId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PackQtyunitId).Value : string.Empty,
                PerPackName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PerPackId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.PerPackId).Value : string.Empty,
                EquvalentSmallestQty = line.EquvalentSmallestQty,
                EquvalentSmallestUnitName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.EquvalentSmallestUnitId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.EquvalentSmallestUnitId).Value : string.Empty,
                FactorOfSmallestProductionPack = line.FactorOfSmallestProductionPack,
                SalesPerPackName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.SalesPerPackId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.SalesPerPackId).Value : string.Empty,
                TemparatureConditionName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.TemparatureConditionId) != null ? applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == line.TemparatureConditionId).Value : string.Empty,
                Temparature = line.Temparature,
                IsProtectFromLightStatus = line.IsProtectFromLight == true ? "1" : "0",
            }).ToList();

            return generalInfoLineModels;
        }

        [HttpPost]
        [Route("InsertFinishProductGeneralInfoStatus")]
        public void InsertFinishProductGeneralInfoStatus(GeneralInfoParam generalInfoParam)
        {
            var countryInformationStatus = new CountryInformationStatus
            {
                FinishProductGeneralInfoId = generalInfoParam.FinishProductGeneralInfoID,
                StatusCodeId = generalInfoParam.StatusCodeId,
                ModifiedByUserId = generalInfoParam.ModifiedByUserId,
                Remarks = generalInfoParam.Remarks,
                ModifiedDate = DateTime.Now
            };
            var generalInfo = _context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == generalInfoParam.FinishProductGeneralInfoID);
            generalInfo.StatusCodeId = generalInfoParam.StatusCodeId;
            _context.CountryInformationStatus.Add(countryInformationStatus);
            _context.SaveChanges();
        }



        [HttpGet]
        [Route("GetFinishProductGeneralInfoReport")]
        public List<FinishProductGeneraIInfoReportModel> GetFinishProductGeneralInfoReport()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var query = _context.FinishProdcutGeneralInfoLine
                .Include(r => r.FinishProductGeneralInfo.StatusCode)
                .Include(r => r.FinishProductGeneralInfo.RegisterationCode)
                .Include(r => r.FinishProductGeneralInfo.FinishProduct)
                .Include(r => r.FinishProductGeneralInfo.ProductionRegistrationHolder)
                .Include(f => f.FinishProductGeneralInfo).AsNoTracking().ToList();
            List<FinishProductGeneraIInfoReportModel> finishProductGeneraIInfoReportModel = new List<FinishProductGeneraIInfoReportModel>();
            query.ForEach(s =>
            {
                FinishProductGeneraIInfoReportModel finishProductGeneraIInfoReportModels = new FinishProductGeneraIInfoReportModel();

                finishProductGeneraIInfoReportModels.FinishProductGeneralInfoLineID = s.FinishProductGeneralInfoLineId;
                finishProductGeneraIInfoReportModels.FinishProductGeneralInfoID = s.FinishProductGeneralInfoId != null ? s.FinishProductGeneralInfoId.Value : 0;
                finishProductGeneraIInfoReportModels.ManufacturingSite = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId).Value : "";
                finishProductGeneraIInfoReportModels.CallNo = s.FinishProductGeneralInfo != null ? s.FinishProductGeneralInfo.CallNo : "";
                finishProductGeneraIInfoReportModels.ProductName = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId).Value : "";
                finishProductGeneraIInfoReportModels.PrhspecificProductName = s.FinishProductGeneralInfo?.PrhspecificProductId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId).FirstOrDefault()?.Value : "";
                finishProductGeneraIInfoReportModels.ProductRegistrationHolderName = s.FinishProductGeneralInfo?.ProductRegistrationHolderId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.ProductRegistrationHolderId).FirstOrDefault()?.Value : "";
                finishProductGeneraIInfoReportModels.ProductionRegistrationHolderName = s.FinishProductGeneralInfo?.ProductionRegistrationHolderId != null ? s.FinishProductGeneralInfo?.ProductionRegistrationHolder?.CompanyName : "";
                finishProductGeneraIInfoReportModels.CountryName = s.FinishProductGeneralInfo?.RegisterCountry != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry).FirstOrDefault()?.Value : "";
                finishProductGeneraIInfoReportModels.ProductOwner = s.FinishProductGeneralInfo?.RegisterProductOwnerId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId).FirstOrDefault()?.Value : "";
                finishProductGeneraIInfoReportModels.RegisterationCodeName = s.FinishProductGeneralInfo?.RegisterationCode != null ? s.FinishProductGeneralInfo.RegisterationCode.CodeValue : "";
                finishProductGeneraIInfoReportModels.RegisterCountryProductName = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry)?.Value : "";
                finishProductGeneraIInfoReportModels.RegistrationReferenceNo = s.FinishProductGeneralInfo != null ? s.FinishProductGeneralInfo.RegistrationReferenceNo : "";
                finishProductGeneraIInfoReportModels.RegistrationNo = s.FinishProductGeneralInfo != null ? s.FinishProductGeneralInfo.RegistrationNo : "";
                finishProductGeneraIInfoReportModels.RegistrationCategoryClassName = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegistrationCategoryClassId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegistrationCategoryClassId)?.Value : "";
                finishProductGeneraIInfoReportModels.RegistrationProductCategoryName = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegistrationProductCategoryId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegistrationProductCategoryId)?.Value : "";
                finishProductGeneraIInfoReportModels.PerUnitQTY = s.PerUnitQty;
                finishProductGeneraIInfoReportModels.ShelfLife = s.ShelfLife;
                finishProductGeneraIInfoReportModels.StorageCondition = s.StorageCondition;
                finishProductGeneraIInfoReportModels.PackType = s.PackTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackTypeId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.PackagingType = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.Size = s.PackSize != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackSize).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.PackQty = s.PackQty;
                finishProductGeneraIInfoReportModels.PackQtyunitName = s.PackQtyunitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackQtyunitId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.PerPackName = s.PerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerPackId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.EquvalentSmallestQty = s.EquvalentSmallestQty;
                finishProductGeneraIInfoReportModels.EquvalentSmallestUnitName = s.EquvalentSmallestUnitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.EquvalentSmallestUnitId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.FactorOfSmallestProductionPack = s.FactorOfSmallestProductionPack;
                finishProductGeneraIInfoReportModels.SalesPerPackName = s.SalesPerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.TemparatureConditionName = s.TemparatureConditionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TemparatureConditionId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.Temparature = s.Temparature;
                finishProductGeneraIInfoReportModels.IsProtectFromLight = s.IsProtectFromLight;
                finishProductGeneraIInfoReportModels.IsProtectFromLightStatus = s.IsProtectFromLight == true ? "1" : "0";
                finishProductGeneraIInfoReportModels.RegistrationSalesName = s.RegistrationSalesId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegistrationSalesId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.StorageConditionName = s.StorageConditionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.StorageConditionId).Select(m => m.Value).FirstOrDefault() : "";
                finishProductGeneraIInfoReportModels.RegistrationPackingStatusName = s.RegistrationPackingStatus?.CodeValue;
                finishProductGeneraIInfoReportModels.StatusCode = s.FinishProductGeneralInfo?.StatusCode != null ? s.FinishProductGeneralInfo.StatusCode.CodeValue : "";

                finishProductGeneraIInfoReportModel.Add(finishProductGeneraIInfoReportModels);
            });
            return finishProductGeneraIInfoReportModel.OrderByDescending(f => f.FinishProductGeneralInfoID).ThenBy(l => l.FinishProductGeneralInfoLineID).ToList();

        }


        [HttpPost]
        [Route("UploadGeneralInfoDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {

            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new FinishProductGeneralInfoDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    //Description = files.desc
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,

                };
                _context.FinishProductGeneralInfoDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetGeneralInfoDocument")]
        public List<FinishProductGeneralInfoDocumentModel> Get(Guid? SessionId)
        {
            List<FinishProductGeneralInfoDocumentModel> finishProductGeneralInfoDocumentModels = new List<FinishProductGeneralInfoDocumentModel>();
            List<FinishProductGeneralInfoDocument> finishProductGeneralInfoDocument = new List<FinishProductGeneralInfoDocument>();



            finishProductGeneralInfoDocument = _context.FinishProductGeneralInfoDocument.Where(f => f.SessionId == SessionId).AsNoTracking().ToList();
            if (finishProductGeneralInfoDocument != null && finishProductGeneralInfoDocument.Count > 0)
            {
                finishProductGeneralInfoDocument.ForEach(s =>
                {
                    FinishProductGeneralInfoDocumentModel finishProductGeneralInfoDocument = new FinishProductGeneralInfoDocumentModel
                    {
                        FinishProductGeneralInfoDocumentID = s.FinishProductGeneralInfoDocumentId,
                        FileName = s.FileName,
                        ContentType = s.ContentType,
                        FileSize = s.FileSize,
                        UploadDate = s.UploadDate,
                        SessionId = s.SessionId,

                    };
                    finishProductGeneralInfoDocumentModels.Add(finishProductGeneralInfoDocument);
                });

            }


            return finishProductGeneralInfoDocumentModels;
        }

        [HttpDelete]
        [Route("DeleteGeneralInfoDocuments")]
        public void DeleteDocuments(int id)
        {
            try
            {
                var query = string.Format("delete from FinishProductGeneralInfoDocument Where FinishProductGeneralInfoDocumentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
                //var finishProductGeneralInfoDocument = _context.FinishProductGeneralInfoDocument.FirstOrDefault(f => f.FinishProductGeneralInfoDocumentId == id);
                //if (finishProductGeneralInfoDocument != null)
                //{
                //    _context.FinishProductGeneralInfoDocument.Remove(finishProductGeneralInfoDocument);
                //    _context.SaveChanges();

                //}
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpGet]
        [Route("DownLoadGeneralInfoDocument")]
        public IActionResult DownLoadGeneralInfoDocument(long id)
        {
            var document = _context.FinishProductGeneralInfoDocument.SingleOrDefault(t => t.FinishProductGeneralInfoDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }

    }
}