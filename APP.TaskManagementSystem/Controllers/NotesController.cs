﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NotesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public NotesController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetNotesBySessionId")]
        public List<NotesModel> GetNotesBySessionId(Guid? sessionId)
        {

            List<NotesModel> notesModels = new List<NotesModel>();
            NotesModel notesModel = new NotesModel();
            var notes = _context.Notes.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.SessionId == sessionId).AsNoTracking().ToList();
            var notesDocumentId = notes.Select(n => n.DocumentId).ToList();
            var notesDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, ContentType = e.ContentType, FileIndex=e.FileIndex }).Where(n => notesDocumentId.Contains(n.DocumentId)).ToList();

            if (notes != null && notes.Count > 0)
            {
                notes.ForEach(s =>
                {
                    var document = notesDocumentItems.FirstOrDefault(n => n.DocumentId == s.DocumentId);
                    var fileName = document?.FileName.Split('.');
                    notesModel = new NotesModel();
                    notesModel.SessionId = s.SessionId;
                    notesModel.Notes = s.Notes1;
                    notesModel.Link = s.Link;
                    notesModel.DocumentId = s.DocumentId;
                    notesModel.DocumentName = document?.FileIndex > 0 ? fileName[0] + "_V0" + document?.FileIndex + "." + fileName[1] : document.FileName;
                    notesModel.NotesId = s.NotesId;
                    notesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    notesModel.StatusCode = s.StatusCode?.CodeValue;
                    notesModel.AddedByUserID = s.AddedByUserId;
                    notesModel.ModifiedByUserID = s.ModifiedByUserId;
                    notesModel.StatusCodeID = s.StatusCodeId;
                    notesModel.AddedDate = s.AddedDate;
                    notesModel.ModifiedDate = s.ModifiedDate;
                    notesModel.AddedByUser = s.AddedByUser?.UserName;
                    notesModel.Type = "Document";
                    notesModel.ContentType = document?.ContentType;
                    notesModels.Add(notesModel);
                });

            }
            notesModels = notesModels.OrderByDescending(s => s.AddedDate).ToList();
            return notesModels;
        }

        [HttpGet]
        [Route("GetNotesByDocumentId")]
        public List<NotesModel> GetNotesByDocumentId(int id)
        {

            List<NotesModel> notesModels = new List<NotesModel>();
            NotesModel notesModel = new NotesModel();
            var notes = _context.Notes.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.DocumentId == id).AsNoTracking().ToList();
            var notesDocumentId = notes.Select(n => n.DocumentId).ToList();
            var notesDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, ContentType = e.ContentType, FileIndex = e.FileIndex }).Where(n => notesDocumentId.Contains(n.DocumentId)).ToList();

            if (notes != null && notes.Count > 0)
            {
                notes.ForEach(s =>
                {
                    var document = notesDocumentItems.FirstOrDefault(n => n.DocumentId == s.DocumentId);
                    var fileName = document?.FileName.Split('.');
                    notesModel = new NotesModel();
                    notesModel.SessionId = s.SessionId;
                    notesModel.Notes = s.Notes1;
                    notesModel.Link = s.Link;
                    notesModel.DocumentId = s.DocumentId;
                    notesModel.DocumentName = document?.FileIndex > 0 ? fileName[0] + "_V0" + document?.FileIndex + "." + fileName[1] : document.FileName;
                    notesModel.NotesId = s.NotesId;
                    notesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    notesModel.StatusCode = s.StatusCode?.CodeValue;
                    notesModel.AddedByUserID = s.AddedByUserId;
                    notesModel.ModifiedByUserID = s.ModifiedByUserId;
                    notesModel.StatusCodeID = s.StatusCodeId;
                    notesModel.AddedDate = s.AddedDate;
                    notesModel.ModifiedDate = s.ModifiedDate;
                    notesModel.AddedByUser = s.AddedByUser?.UserName;
                    notesModel.Type = "Document";
                    notesModel.ContentType = document?.ContentType;
                    notesModels.Add(notesModel);
                });

            }
            return notesModels;
        }

        [HttpPost]
        [Route("GetPersonalNotes")]
        public List<NotesModel> GetPersonalNotes(NotesModel value)
        {

            List<NotesModel> notesModels = new List<NotesModel>();
            NotesModel notesModel = new NotesModel();
            var notes = _context.Notes.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.DocumentId == value.DocumentId && s.IsPersonalNotes == true && s.AddedByUserId == value.AddedByUserID).AsNoTracking().ToList();
            var notesDocumentId = notes.Select(n => n.DocumentId).ToList();
            var notesDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, ContentType = e.ContentType, FileIndex = e.FileIndex }).Where(n => notesDocumentId.Contains(n.DocumentId)).ToList();

            if (notes != null && notes.Count > 0)
            {
                notes.ForEach(s =>
                {
                    var document = notesDocumentItems.FirstOrDefault(n => n.DocumentId == s.DocumentId);
                    var fileName = document?.FileName.Split('.');
                    notesModel = new NotesModel();
                    notesModel.SessionId = s.SessionId;
                    notesModel.Notes = s.Notes1;
                    notesModel.Link = s.Link;
                    notesModel.DocumentId = s.DocumentId;
                    notesModel.DocumentName = document?.FileIndex > 0 ? fileName[0] + "_V0" + document?.FileIndex + "." + fileName[1] : document.FileName;
                    notesModel.NotesId = s.NotesId;
                    notesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    notesModel.StatusCode = s.StatusCode?.CodeValue;
                    notesModel.AddedByUserID = s.AddedByUserId;
                    notesModel.ModifiedByUserID = s.ModifiedByUserId;
                    notesModel.StatusCodeID = s.StatusCodeId;
                    notesModel.AddedDate = s.AddedDate;
                    notesModel.ModifiedDate = s.ModifiedDate;
                    notesModel.AddedByUser = s.AddedByUser?.UserName;
                    notesModel.Type = "Document";
                    notesModel.ContentType = document?.ContentType;
                    notesModels.Add(notesModel);
                });

            }
            return notesModels;
        }



        // POST: api/User
        [HttpPost]
        [Route("InsertNotes")]
        public NotesModel Post(NotesModel value)
        {

            var notes = new Notes
            {
                Notes1 = value.Notes,
                Link = value.Link,
                DocumentId = value.DocumentId,
                SessionId = value.SessionId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                IsPersonalNotes = false,

            };
            _context.Notes.Add(notes);
            value.AddedDate = notes.AddedDate;
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
            }
            _context.SaveChanges();
            value.NotesId = notes.NotesId;
            return value;
        }

        [HttpPost]
        [Route("InsertPersonalNotes")]
        public NotesModel InsertPersonalNotes(NotesModel value)
        {
            var exist = _context.Notes.FirstOrDefault(f => f.IsPersonalNotes == true && f.DocumentId == value.DocumentId && f.AddedByUserId == value.AddedByUserID);
            if (exist == null)
            {
                var notes = new Notes
                {
                    Notes1 = value.Notes,
                    Link = value.Link,
                    DocumentId = value.DocumentId,
                    SessionId = value.SessionId,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    IsPersonalNotes = true,


                };
                _context.Notes.Add(notes);
                value.AddedDate = notes.AddedDate;
                _context.SaveChanges();
                value.NotesId = notes.NotesId;
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
            }
         
            return value;
        }
        [HttpPut]
        [Route("UpdatePersonalNotes")]
        public NotesModel UpdatePersonalNotes(NotesModel value)
        {

            var notes = _context.Notes.SingleOrDefault(p => p.NotesId == value.NotesId);
            if (notes != null)
            {

                notes.DocumentId = value.DocumentId;
                notes.Link = value.Link;
                notes.ModifiedByUserId = value.ModifiedByUserID;
                notes.ModifiedDate = DateTime.Now;
                notes.StatusCodeId = value.StatusCodeID.Value;
                notes.Notes1 = value.Notes;
                notes.StatusCodeId = value.StatusCodeID.Value;
                notes.IsPersonalNotes = true;
            }
            _context.SaveChanges();
            value.ModifiedDate = notes.ModifiedDate;
            if (value.ModifiedByUserID > 0)
            {
                value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == value.ModifiedByUserID)?.FirstOrDefault().UserName;
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNotes")]
        public NotesModel Put(NotesModel value)
        {

            var notes = _context.Notes.SingleOrDefault(p => p.NotesId == value.NotesId);
            if (notes != null)
            {

                notes.DocumentId = value.DocumentId;
                notes.Link = value.Link;
                notes.ModifiedByUserId = value.ModifiedByUserID;
                notes.ModifiedDate = DateTime.Now;
                notes.StatusCodeId = value.StatusCodeID.Value;
                notes.Notes1 = value.Notes;
                notes.StatusCodeId = value.StatusCodeID.Value;
                notes.IsPersonalNotes = false;

            }
            _context.SaveChanges();
            value.ModifiedDate = notes.ModifiedDate;
            if (value.ModifiedByUserID > 0)
            {
                value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == value.ModifiedByUserID)?.FirstOrDefault().UserName;
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNotes")]
        public void Delete(int id)
        {
            var notes = _context.Notes.SingleOrDefault(p => p.NotesId == id);
            if (notes != null)
            {
                _context.Notes.Remove(notes);
                _context.SaveChanges();
            }
        }

    }
}
