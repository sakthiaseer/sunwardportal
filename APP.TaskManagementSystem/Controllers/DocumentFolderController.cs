﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentFolderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentFolderController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/DocumentFolder
        [HttpGet]
        [Route("GetDocumentFolders")]
        public List<DocumentFolderModel> Get(long id)
        {
            var documentFolder = _context.DocumentFolder.Include(d => d.Document).AsNoTracking().ToList();
            List<DocumentFolderModel> documentFolderModel = new List<DocumentFolderModel>();
            documentFolder.ForEach(s =>
            {
                DocumentFolderModel documentFolderModels = new DocumentFolderModel
                {
                    DocumentFolderID = s.DocumentFolderId,
                    FolderID = s.FolderId.Value,
                    DocumentID = s.DocumentId.Value,
                    FileName = s.Document != null ? s.Document.FileName : "",
                    ContentType = s.Document != null ? s.Document.ContentType : "",
                    FileSize = (long)Math.Round(Convert.ToDouble(s.Document.FileSize / 1024)),
                };
                documentFolderModel.Add(documentFolderModels);
            });
            return documentFolderModel.OrderByDescending(a => a.DocumentFolderID).Where(u => u.FolderID == id).ToList();
        }

        // GET: api/DocumentFolder/2
        //[HttpGet("{id}", Name = "Get DocumentFolder")]
        [HttpGet("GetDocumentFolders/{id:int}")]
        public ActionResult<DocumentFolderModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentFolder = _context.DocumentFolder.SingleOrDefault(p => p.DocumentFolderId == id.Value);
            var result = _mapper.Map<DocumentFolderModel>(documentFolder);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentFolderModel> GetData(SearchModel searchModel)
        {
            var documentFolder = new DocumentFolder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).FirstOrDefault();
                        break;
                    case "Last":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).LastOrDefault();
                        break;
                    case "Next":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).LastOrDefault();
                        break;
                    case "Previous":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).FirstOrDefault();
                        break;
                    case "Last":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).LastOrDefault();
                        break;
                    case "Next":
                        documentFolder = _context.DocumentFolder.OrderBy(o => o.DocumentFolderId).FirstOrDefault(s => s.DocumentFolderId > searchModel.Id);
                        break;
                    case "Previous":
                        documentFolder = _context.DocumentFolder.OrderByDescending(o => o.DocumentFolderId).FirstOrDefault(s => s.DocumentFolderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentFolderModel>(documentFolder);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentFolder")]
        public DocumentFolderModel Post(DocumentFolderModel value)
        {
            var documentFolder = new DocumentFolder
            {
                DocumentFolderId = value.DocumentFolderID,
                FolderId = value.FolderID,
                DocumentId = value.DocumentID,
            };
            _context.DocumentFolder.Add(documentFolder);
            _context.SaveChanges();
            value.DocumentFolderID = documentFolder.DocumentFolderId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentFolder")]
        public DocumentFolderModel Put(DocumentFolderModel value)
        {
            var documentFolder = _context.DocumentFolder.SingleOrDefault(p => p.DocumentFolderId == value.DocumentFolderID);
            //DocumentFolder.DocumentFolderId = value.DocumentFolderID;
            documentFolder.FolderId = value.SelectedFolderID;
            documentFolder.DocumentId = value.DocumentID;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentFolder")]
        public void Delete(int id)
        {
            var documentFolder = _context.DocumentFolder.SingleOrDefault(p => p.DocumentFolderId == id);
            if (documentFolder != null)
            {
                _context.DocumentFolder.Remove(documentFolder);
                _context.SaveChanges();
            }
        }
    }
}