﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ICBMPLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
       

        public ICBMPLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
           
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetICBMPLinesByID")]
        public List<ICBMPLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var Icbmpline = _context.Icbmpline.Include("AddedByUser").Include("ModifiedByUser").AsNoTracking().Select(s => new ICBMPLineModel
            {
                ICBMPLineID = s.IcbmplineId,
                ICBMPID = s.Icbmpid,
                ManufacturingStepsID = s.ManufacturingStepsId,
                MachineGroupingID = s.MachineGroupingId,
                MachineID = s.MachineId,               
              
                AddedByUserID = s.AddedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                StatusCodeID = s.StatusCodeId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,

            }).Where(t=>t.ICBMPID ==id).OrderByDescending(o => o.ICBMPLineID).ToList();
            //var result = _mapper.Map<List<ICBMPLineModel>>(Icbmpline);
            return Icbmpline;
        }


      



        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ICBMPLineModel> GetData(SearchModel searchModel)
        {
            var Icbmpline = new Icbmpline();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).FirstOrDefault();
                        break;
                    case "Last":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).LastOrDefault();
                        break;
                    case "Next":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).LastOrDefault();
                        break;
                    case "Previous":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).FirstOrDefault();
                        break;
                    case "Last":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).LastOrDefault();
                        break;
                    case "Next":
                        Icbmpline = _context.Icbmpline.OrderBy(o => o.IcbmplineId).FirstOrDefault(s => s.IcbmplineId > searchModel.Id);
                        break;
                    case "Previous":
                        Icbmpline = _context.Icbmpline.OrderByDescending(o => o.IcbmplineId).FirstOrDefault(s => s.IcbmplineId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<ICBMPLineModel>(Icbmpline);
         
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertICBMPLine")]
        public ICBMPLineModel Post(ICBMPLineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
           
            var Icbmpline = new Icbmpline
            {
                Icbmpid =value.ICBMPID,
                ManufacturingStepsId = value.ManufacturingStepsID,
                MachineGroupingId = value.MachineGroupingID,
                MachineId = value.MachineID,
                Wilink = value.WILink,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
               
            };
            _context.Icbmpline.Add(Icbmpline);
            _context.SaveChanges();
            value.ICBMPLineID = Icbmpline.IcbmplineId;
            if (value.DetermineFactorIDs != null && value.DetermineFactorIDs.Any())
            {
                value.DetermineFactorIDs.ForEach(o =>
                {
                    var icbmplineDetermineFactor = new IcbmplineDetermineFactor
                    {
                        IcbmplineId = value.ICBMPLineID,
                        IcbmpdetermineFactorId = o,
                    };
                    _context.IcbmplineDetermineFactor.Add(icbmplineDetermineFactor);
                });
                _context.SaveChanges();
            }
           
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateICBMPLine")]
        public ICBMPLineModel Put(ICBMPLineModel value)
        {
            var Icbmpline = _context.Icbmpline.SingleOrDefault(p => p.IcbmplineId == value.ICBMPLineID);
            Icbmpline.Icbmpid = value.ICBMPID;
            Icbmpline.MachineGroupingId = value.MachineGroupingID;
            Icbmpline.ManufacturingStepsId = value.ManufacturingStepsID;
            Icbmpline.MachineId = value.MachineID;
            Icbmpline.Wilink = value.WILink;           
            Icbmpline.ModifiedByUserId = value.ModifiedByUserID;
            Icbmpline.ModifiedDate = DateTime.Now;
            var icbmplineDetermineFactorlist = _context.IcbmplineDetermineFactor.Where(m => m.IcbmplineId == value.ICBMPLineID).ToList();
            if (icbmplineDetermineFactorlist.Count() > 0)
            {
                _context.IcbmplineDetermineFactor.RemoveRange(icbmplineDetermineFactorlist);
                _context.SaveChanges();
            }
            if (value.DetermineFactorIDs != null && value.DetermineFactorIDs.Any())
            {
                value.DetermineFactorIDs.ForEach(o =>
                {
                    var icbmplineDetermineFactor = new IcbmplineDetermineFactor
                    {
                        IcbmplineId = value.ICBMPLineID,
                        IcbmpdetermineFactorId = o,
                    };
                    _context.IcbmplineDetermineFactor.Add(icbmplineDetermineFactor);
                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
         
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteICBMPLine")]
        public void Delete(int id)
        {
            var Icbmpline = _context.Icbmpline.SingleOrDefault(p => p.IcbmplineId == id);
            if (Icbmpline != null)
            {
                var IcbmplineDetermineFactor = _context.IcbmplineDetermineFactor.Where(m => m.IcbmplineId == id).ToList();
                if (IcbmplineDetermineFactor.Count() > 0)
                {
                    _context.IcbmplineDetermineFactor.RemoveRange(IcbmplineDetermineFactor);
                    _context.SaveChanges();
                }
                _context.Icbmpline.Remove(Icbmpline);
                _context.SaveChanges();
            }
        }
    }
}