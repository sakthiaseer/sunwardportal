﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
 
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   // [Authorize(Roles = "Admin")]
    public class FulfillmentofAdhocController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IHubContext<ChatHub> _hub;
        
        private readonly IBackgroundJobClient _backgroundJobClient;
        public FulfillmentofAdhocController(CRT_TMSContext context, IMapper mapper, 
            IConfiguration config, IHubContext<ChatHub> hub,  IBackgroundJobClient backgroundJobClient)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hub = hub;           
            _backgroundJobClient = backgroundJobClient;
        }

        async Task<bool> DeleteSaleOrder(long companyId)
        {
            var deleteexisting = _context.NavSalesOrderLine.ToList();
            if (deleteexisting.Count > 0)
            {
                _context.NavSalesOrderLine.RemoveRange(deleteexisting);
                _context.SaveChanges();
            }
            return true;
        }
        async Task<bool> DeleteProdOrder(long companyId)
        {
            var deleteexisting = _context.NavprodOrderLine.ToList();
            if (deleteexisting.Count > 0)
            {
                _context.NavprodOrderLine.RemoveRange(deleteexisting);
                _context.SaveChanges();
            }
            return true;
        }

        [HttpPost]
        [Route("SyncNAVOrders")]
        public async Task<string> SyncNAVOrders(FulfillmentSearchParam fulfillmentSearch)
        {
            NAVFuncController navFunc = new NAVFuncController(_context, _mapper, _config,_hub,_backgroundJobClient);
            var company = "NAV_JB";
            if (fulfillmentSearch.CompanyId == 2)
            {
                company = "NAV_SG";
            }
            IEnumerable<Task> tasks = new Task[]
            {
                DeleteSaleOrder(fulfillmentSearch.CompanyId.Value),
                DeleteProdOrder(fulfillmentSearch.CompanyId.Value),

             };
            await Task.WhenAll(tasks);
            var search = new StockBalanceSearch
            {
                CompanyId = fulfillmentSearch.CompanyId,
                UserId = fulfillmentSearch.UserId,
            };
            IEnumerable<Task> navtasks = new Task[]
            {
                navFunc.GetOutstandingProdOrderLine(search),
                navFunc.GetOutstandingSalesOrderLine(search),

             };
            await Task.WhenAll(navtasks);


            return "Nav Sale & Prod orders sync successfully.";
        }

        [HttpPost]
        [Route("GetFulfillmentofAdhocBySearch")]
        public List<FulfillmentofAdhocModel> Get(FulfillmentSearchParam fulfillmentSearch)
        {
            if (fulfillmentSearch.MethodCodeIds.Count() <= 0)
            {
                var methodcodeIds = _context.NavMethodCode.AsNoTracking().Select(s => s.MethodCodeId).ToList();
                methodcodeIds.ForEach(f => { fulfillmentSearch.MethodCodeIds.Add(f); });
            }
            var firstDayCurrentMonth = new DateTime(fulfillmentSearch.FromDate.Value.Year, fulfillmentSearch.FromDate.Value.Month, 1);
            var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);

            var month = lastDayLastMonth.Month;
            var year = lastDayLastMonth.Year;

            var fulfillmentadhocList = new List<FulfillmentofAdhocModel>();
            var itemIds = _context.NavMethodCodeLines.Where(f => fulfillmentSearch.MethodCodeIds.Contains(f.MethodCodeId)).AsNoTracking().Select(s => s.ItemId).ToList();
            var navItems = _context.Navitems.Include("GenericCode").Include("NavMethodCodeLines.MethodCode").Include("NavItemCitemList.NavItemCustomerItem").Where(f => f.CompanyId == fulfillmentSearch.CompanyId && itemIds.Contains(f.ItemId) && f.StatusCodeId == 1).AsNoTracking().ToList();
            var navStkbalance = _context.NavitemStockBalance.Include("Item").Where(f => f.StockBalMonth.Value.Month == month && f.StockBalMonth.Value.Year == year && itemIds.Contains(f.ItemId)).AsNoTracking().ToList();
            navStkbalance.ForEach(f =>
            {
                f.Quantity *= f.Item.PackQty;
            });
            var prodyctionTickets = _context.ProductionSimulation.Where(f => f.StartingDate <= fulfillmentSearch.ToDate && f.StartingDate >= fulfillmentSearch.FromDate).OrderByDescending(o => o.StartingDate).AsNoTracking().ToList();
            var saleOrderList = _context.NavSalesOrderLine.Where(f => f.ShipmentDate <= fulfillmentSearch.ToDate && f.ShipmentDate >= fulfillmentSearch.FromDate).OrderByDescending(o => o.ShipmentDate).AsNoTracking().ToList();
            var prodOrderList = _context.NavprodOrderLine.Where(f => f.CompletionDate <= fulfillmentSearch.ToDate && f.CompletionDate >= fulfillmentSearch.FromDate).OrderByDescending(o => o.CompletionDate).AsNoTracking().ToList();

            var monthsDate = Enumerable.Range(0, 13).Select(a => fulfillmentSearch.FromDate.Value.AddMonths(a))
           .TakeWhile(a => a <= fulfillmentSearch.ToDate)
           .Select(a => a.Month).ToList();

            var nacCustomer = _context.Navcustomer.AsNoTracking().ToList();

            navItems.ForEach(f =>
            {

                monthsDate.ForEach(m =>
                {
                    DateTime date = new DateTime(year, m, 1);
                    var prodBalance = prodyctionTickets.Where(p => p.ItemNo == f.No && p.StartingDate.Month == m).Sum(s => s.OutputQty);
                    fulfillmentadhocList.Add(new FulfillmentofAdhocModel
                    {
                        MethodCode = f.NavMethodCodeLines.First().MethodCode.MethodName,
                        ItemName = f.No,
                        ItemNo = f.No,
                        Description = f.Description + " " + f.Description2,
                        InternalRef = f.InternalRef,
                        BUOM = f.BaseUnitofMeasure,
                        StockBalance = navStkbalance.Where(n => n.ItemId.Value == f.ItemId && n.StockBalMonth.Value.Month == m).Sum(s => s.Quantity),
                        ProductionOrder = prodBalance, //prodyctionTickets.Where(p => p.ItemNo == f.No).Sum(s => s.OutputQty),
                        Date = date,
                        OrderType = "STOCK",//SaleProdOrders = fulfillment.SaleProdOrders,
                        IsMonthLastRecord = false,
                        IsPromiseDate = false,
                    });

                    var orders = saleOrderList.Where(sale => sale.ItemNo == f.No && sale.ShipmentDate.Value.Month == m).ToList();
                    orders.ForEach(o =>
                    {
                        var cust = nacCustomer.FirstOrDefault(cf => cf.Code == o.SelltoCustomerNo);
                        fulfillmentadhocList.Add(new FulfillmentofAdhocModel
                        {
                            MethodCode = f.NavMethodCodeLines.First().MethodCode.MethodName,
                            ItemName = "",
                            ItemNo = f.No,
                            InternalRef = "",
                            BUOM = "",
                            DocumentNo = o.DocumentNo,
                            Date = o.ShipmentDate,
                            SourceName = cust != null ? cust.Name : o.SelltoCustomerNo,
                            Quantity = o.OutstandingQuantity,
                            ProductionOrder = null,
                            StockBalance = null,
                            OrderType = "SO",
                            IsMonthLastRecord = false,
                            IsPromiseDate = o.PromisedDeliveryDate == DateTime.MinValue ? false : true,
                            // Balance = prodBalance - o.OutstandingQuantity,
                        });
                    });
                    var prodorders = prodOrderList.Where(sale => sale.ItemNo == f.No && sale.CompletionDate.Value.Month == m).ToList();
                    prodorders.ForEach(o =>
                    {
                        fulfillmentadhocList.Add(new FulfillmentofAdhocModel
                        {
                            MethodCode = f.NavMethodCodeLines.First().MethodCode.MethodName,
                            ItemName = "",
                            ItemNo = f.No,
                            InternalRef = "",
                            BUOM = "",
                            DocumentNo = o.ProdOrderNo,
                            Date = o.CompletionDate,
                            SourceName = "",
                            Quantity = o.RemainingQuantity,
                            ProductionOrder = null,
                            StockBalance = null,
                            OrderType = "Prod",
                            IsMonthLastRecord = false,
                            IsPromiseDate = false,
                            // Balance = prodBalance + o.RemainingQuantity,
                        });
                    });
                });
            });

            var endresult = fulfillmentadhocList.OrderBy(o => o.Date).GroupBy(g => new { g.Date.Value.Month, g.ItemNo }).ToList();
            var reportData = new List<FulfillmentofAdhocModel>();
            endresult.ForEach(f =>
            {

                f.Last().IsMonthLastRecord = true;
                //var stockBalance = f.First().StockBalance;
                f.ToList().ForEach(o =>
                {
                    var rptData = new FulfillmentofAdhocModel
                    {
                        MethodCode = o.MethodCode,
                        ItemName = o.ItemName,
                        ItemNo = o.ItemNo,
                        Description =o.Description,
                        InternalRef = o.InternalRef,
                        BUOM = o.BUOM,
                        StockBalance = o.StockBalance,
                        ProductionOrder = o.ProductionOrder,
                        OrderType = o.OrderType,
                        IsMonthLastRecord = o.IsMonthLastRecord,
                        DocumentNo = o.DocumentNo,
                        Date = o.Date,
                        SourceName = o.SourceName,
                        Quantity = o.Quantity,
                        Balance = o.Balance,
                    };

                    reportData.Add(rptData);
                });
            });
            var groupbyItem = reportData.GroupBy(g => g.ItemNo).ToList();
            decimal? stockBalance = 0;
            var reportDatabyItem = new List<FulfillmentofAdhocModel>();
            groupbyItem.ForEach(f =>
            {
                stockBalance = 0;
                f.ToList().ForEach(o =>
                {
                    var rptData = new FulfillmentofAdhocModel
                    {
                        MethodCode = o.MethodCode,
                        ItemName = o.ItemName,
                        ItemNo = o.ItemNo,
                        Description =o.Description,
                        InternalRef = o.InternalRef,
                        BUOM = o.BUOM,
                        StockBalance = o.StockBalance,
                        ProductionOrder = o.ProductionOrder,
                        OrderType = o.OrderType,
                        IsMonthLastRecord = o.IsMonthLastRecord,
                        DocumentNo = o.DocumentNo,
                        Date = o.Date,
                        SourceName = o.SourceName,
                        Quantity = o.Quantity,

                    };
                    if (o.OrderType == "SO")
                    {
                        stockBalance -= o.Quantity;
                        rptData.Balance = stockBalance;
                    }
                    if (o.OrderType == "Prod")
                    {
                        stockBalance += o.Quantity;
                        rptData.Balance = stockBalance;
                    }
                    if (o.OrderType == "STOCK")
                    {
                        stockBalance += o.StockBalance;
                        rptData.Balance = stockBalance;
                    }
                    reportDatabyItem.Add(rptData);
                });
            });

            return reportDatabyItem;
        }


    }
}