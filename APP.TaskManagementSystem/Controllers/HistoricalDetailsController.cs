﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HistoricalDetailsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private Microsoft.Extensions.Configuration.IConfiguration _config;

        public HistoricalDetailsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        [HttpPost]
        [Route("GetHistoricalDetailsGraph")]
        public HistoricalDetailsChartModel GetHistoricalDetailsGraph(DateRangeModel dateRangeModel)
        {
            List<string> Yeardatelist = new List<string>();
            List<string> Monthdatelist = new List<string>();
            List<decimal> MonthSaleslist = new List<decimal>();
            List<decimal> YearSaleslist = new List<decimal>();
            SortedList monthDataList = new SortedList();
            HistoricalDetailsChartModel historicalDetailsModels = new HistoricalDetailsChartModel();
            if (dateRangeModel.StartDate != null && dateRangeModel.EndDate != null)
            {
                // int Months = (dateRangeModel.EndDate.Value.Year - dateRangeModel.StartDate.Value.Year) * 12 + dateRangeModel.EndDate.Value.Month - dateRangeModel.StartDate.Value.Month;
                var dateMonth = DateTime.Parse(dateRangeModel.EndDate.Value.Year + "-01-01");
                Monthdatelist = Enumerable.Range(0, 12).Select(a => dateMonth.AddMonths(a))
                           .TakeWhile(a => a <= dateRangeModel.EndDate)
                           .Select(a => String.Concat(a.ToString("MMM") + "-" + a.Year)).ToList();
                for (int i = dateRangeModel.StartDate.Value.Year; i <= dateRangeModel.EndDate.Value.Year; i++)
                {
                    Yeardatelist.Add(i.ToString());
                }
                var startDate = dateRangeModel.StartDate.Value.ToString("yyy-MM-01");
                var endDate = dateRangeModel.EndDate.Value.ToString("yyy-MM-" + DateTime.DaysInMonth(dateRangeModel.EndDate.Value.Year, dateRangeModel.EndDate.Value.Month));
                //var sqlQuery = "select FORMAT(Date,'yyyy') as Date,COUNT(*) as TotalSales,SUM(Gross) as Gross,  SUM(Qty) as Qty,SUM(Bouns) as Bouns,SUM(Nett) as Nett from HistoricalDetails where NavCustomerId="+dateRangeModel.CustomerId+ " and Date BETWEEN '"+startDate+ "' AND '" + endDate + "' group by FORMAT(date, 'yyyy')";
                var rep = "";
                var cusId = "";
                if (dateRangeModel.CustomerId != null)
                {
                    cusId = "NavCustomerId='" + dateRangeModel.CustomerId + "' and ";
                }
                if (dateRangeModel.Ref != null)
                {
                    rep = "ref='" + dateRangeModel.Ref + "' and ";
                }
                var sqlQuery = "select FORMAT(Date,'yyyy') as Date,COUNT(*) as TotalSales,SUM(Gross) as Gross,  SUM(Qty) as Qty,SUM(Bouns) as Bouns,SUM(Nett) as Nett from HistoricalDetails where " + rep + cusId + " YEAR(Date) >=" + dateRangeModel.StartDate.Value.Year + "  AND YEAR(Date) <=" + dateRangeModel.EndDate.Value.Year + " group by FORMAT(date, 'yyyy')";
                var yearSalesResult = _context.Set<HistoricalDetailsGraph>().FromSqlRaw(sqlQuery).ToList();

                if (Yeardatelist.Count > 0)
                {
                    Yeardatelist.ForEach(s =>
                    {
                        var salesSum = yearSalesResult.FirstOrDefault(w => w.Date == s)?.Gross;
                        YearSaleslist.Add(Math.Floor((salesSum == null ? 0 : salesSum.Value) / 1000));
                    });
                }
                var sqlQuery1 = "select FORMAT(Date,'MMM-yyyy') as Date,COUNT(*) as TotalSales,SUM(Gross) as Gross,  SUM(Qty) as Qty,SUM(Bouns) as Bouns,SUM(Nett) as Nett from HistoricalDetails where " + rep + cusId + " Date BETWEEN '" + startDate + "' AND '" + endDate + "' group by FORMAT(date, 'MMM-yyyy')";

                var MonthSalesResult = _context.Set<HistoricalDetailsGraph>().FromSqlRaw(sqlQuery1).ToList();

                if (Monthdatelist.Count > 0)
                {
                    Monthdatelist.ForEach(a =>
                    {
                        var salesSum = MonthSalesResult.FirstOrDefault(w => w.Date == a)?.Gross;
                        MonthSaleslist.Add(Math.Floor((salesSum == null ? 0 : salesSum.Value) / 1000));
                    });
                }
                var months = Enumerable.Range(1, 12).Select(i =>
                 new
                 {
                     Index = i,
                     MonthName = new CultureInfo("en-US").DateTimeFormat.GetAbbreviatedMonthName(i)
                 }).ToDictionary(x => x.Index, x => x.MonthName).ToList();
                Yeardatelist.ForEach(y =>
                {
                    var YearName = "Year";
                    var MonthName = "Total";
                    SortedList monthDataList1 = new SortedList();
                    monthDataList1.Add(YearName, y);
                    decimal TotalSums = 0;
                    months.ForEach(m =>
                    {
                        var monthYear = m.Value + '-' + y;
                        var salesSum = MonthSalesResult.FirstOrDefault(w => w.Date == monthYear)?.Gross;
                        var TotalSum = salesSum == null ? 0 : salesSum.Value;
                        monthDataList1.Add(m.Value, TotalSum);
                        TotalSums += TotalSum;
                        if (m.Key == 12)
                        {
                            monthDataList1.Add(MonthName, TotalSums);
                            monthDataList.Add(y, monthDataList1);
                        }
                    });
                });
            }
            historicalDetailsModels.Monthwisesales = monthDataList;
            historicalDetailsModels.Yearsales = YearSaleslist;
            historicalDetailsModels.Yeardate = Yeardatelist;
            historicalDetailsModels.Monthsales = MonthSaleslist;
            historicalDetailsModels.Monthdate = Monthdatelist;
            return historicalDetailsModels;

        }
        [HttpGet]
        [Route("GetHistoricalDetails")]
        public List<HistoricalDetailsModel> GetHistoricalDetails()
        {
            List<HistoricalDetailsModel> historicalDetailsModels = new List<HistoricalDetailsModel>();
            var historicalDetails = _context.HistoricalDetails.ToList();
            if (historicalDetails != null)
            {
                var navCustomerIds = historicalDetails.Select(s => s.NavCustomerId.GetValueOrDefault(0)).ToList();
                var navcustomers = _context.CompanyListing.Select(s => new { s.CompanyListingId, s.CompanyName }).Where(w => navCustomerIds.Contains(w.CompanyListingId)).ToList();
                historicalDetails.ForEach(s =>
                {
                    HistoricalDetailsModel historicalDetailsModel = new HistoricalDetailsModel();
                    historicalDetailsModel.HistoricalDetailsId = s.HistoricalDetailsId;
                    historicalDetailsModel.NavCustomerId = s.NavCustomerId;
                    historicalDetailsModel.NavCustomer = navcustomers != null ? navcustomers.FirstOrDefault(a => a.CompanyListingId == s.NavCustomerId)?.CompanyName : "";
                    historicalDetailsModel.Date = s.Date;
                    historicalDetailsModel.Invoice = s.Invoice;
                    historicalDetailsModel.CustNo = s.CustNo;
                    historicalDetailsModel.Company = s.Company;
                    historicalDetailsModel.BranchCode = s.BranchCode;
                    historicalDetailsModel.Branch = s.Branch;
                    historicalDetailsModel.Item = s.Item;
                    historicalDetailsModel.Description = s.Description;
                    historicalDetailsModel.TranType = s.TranType;
                    historicalDetailsModel.Batch = s.Batch;
                    historicalDetailsModel.Expiry = s.Expiry;
                    historicalDetailsModel.Ref = s.Ref;
                    historicalDetailsModel.Price = s.Price;
                    historicalDetailsModel.Discount = s.Discount;
                    historicalDetailsModel.Qty = s.Qty;
                    historicalDetailsModel.Bouns = s.Bouns;
                    historicalDetailsModel.Nett = s.Nett;
                    historicalDetailsModel.Gross = s.Gross;
                    historicalDetailsModel.StatusCodeID = s.StatusCodeId;
                    historicalDetailsModel.AddedByUserID = s.AddedByUserId;
                    historicalDetailsModel.AddedDate = s.AddedDate;
                    historicalDetailsModel.ModifiedByUserID = s.ModifiedByUserId;
                    historicalDetailsModel.ModifiedDate = s.ModifiedDate;

                    historicalDetailsModels.Add(historicalDetailsModel);
                });
            }
            return historicalDetailsModels;
        }
        [HttpGet]
        [Route("GetHistoricalDetailsRef")]
        public List<HistoricalDetailsModel> GetHistoricalDetailsRef()
        {
            List<HistoricalDetailsModel> historicalDetailsModels = new List<HistoricalDetailsModel>();
            var historicalDetails = _context.HistoricalDetails.Where(w => w.Ref!=null).Select(s=>s.Ref).Distinct().ToList();
            if (historicalDetails != null)
            {
                historicalDetails.ForEach(s =>
                {
                    HistoricalDetailsModel historicalDetailsModel = new HistoricalDetailsModel();
                    historicalDetailsModel.HistoricalDetailsId = 1;
                    historicalDetailsModel.Ref = s;
                    historicalDetailsModels.Add(historicalDetailsModel);
                });
            }
            return historicalDetailsModels;
        }
        [HttpPost]
        [Route("ImportuploadDocuments")]
        public List<HeaderList> ImportuploadDocuments(IFormCollection files)
        {
            var navCustomerIds = files["NavCustomerId"].ToString();
            var navCustomerId = !string.IsNullOrEmpty(navCustomerIds) ? long.Parse(navCustomerIds) : -1;
            var type = files["Type"].ToString();
            var SessionId = Guid.NewGuid();
            var plant = files["plantId"].ToString();
            var user = files["userId"].ToString();
            long userId = !string.IsNullOrEmpty(user) ? long.Parse(user) : -1;
            var companyId = !string.IsNullOrEmpty(plant) ? long.Parse(plant) : -1;
            List<JsonList> JsonList = new List<JsonList>();
            if (type == "Insert")
            {
                if (!string.IsNullOrWhiteSpace(files["Items"].ToString()))
                {
                    dynamic ItemsList = JsonConvert.DeserializeObject(files["Items"], typeof(List<JsonList>));
                    foreach (var item in ItemsList)
                    {
                        JsonList Json = new JsonList();
                        Json.EntityName = item.EntityName;
                        Json.ColumnId = item.ColumnId;
                        JsonList.Add(Json);
                    }
                }
            }
            List<HeaderList> columnPropertyList = new List<HeaderList>();
            List<HeaderList> headerList = new List<HeaderList>();
            ACImportExcelModel ACImportExcelModel = new ACImportExcelModel();
            var model = new HistoricalDetails();
            string[] stringArray = {
                        "HistoricalDetailsId","NavCustomerId","StatusCodeId","AddedByUserId","AddedDate","ModifiedByUserId",
                        "ModifiedDate","AddedByUser","ModifiedByUser","NavCustomer","StatusCode"

                    };
            foreach (var property in model.GetType().GetProperties())
            {
                int index = Array.IndexOf(stringArray, property.Name);
                if (index > -1)
                {
                }
                else
                {
                    HeaderList HistoricalDetailsModel = new HeaderList();
                    HistoricalDetailsModel.Text = property.Name;
                    HistoricalDetailsModel.Value = property.Name;
                    columnPropertyList.Add(HistoricalDetailsModel);
                }
            }
            var acItems = new List<HistoricalDetailsModel>();
            var count = 0;
            files.Files.ToList().ForEach(f =>
            {
                if (count == 0)
                {
                    var file = f;
                    var extension = Path.GetExtension(file.FileName);
                    if (extension == ".xlsx")
                    {
                        var fs = file.OpenReadStream();
                        using (var package = new ExcelPackage(fs))
                        {
                            var worksheets = package.Workbook.Worksheets.Select(x => x.Name).ToList();
                            var count = worksheets.Count();
                            if (count > 0)
                            {
                                var oSheet = package.Workbook.Worksheets[worksheets[0]];
                                if (oSheet != null)
                                {
                                    ExcelWorksheet worksheet = oSheet;
                                    var rowCount = worksheet.Dimension.Rows;
                                    for (int i = 1; i <= worksheet.Dimension.End.Column; i++)
                                    {
                                        headerList.Add(new HeaderList
                                        {
                                            Text = worksheet.Cells[1, i].Value.ToString(),
                                            ID = i
                                        });
                                    }
                                    if (JsonList.Count > 0)
                                    {
                                        for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                                        {
                                            HistoricalDetailsModel ACImportModel = new HistoricalDetailsModel();
                                            JsonList.ForEach(j =>
                                            {
                                                if (j.EntityName.ToLower() == "Date".ToLower())
                                                {
                                                     
                                                    var dt = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                    if (dt != null)
                                                    {
                                                        string s2 = "/";
                                                        if (dt.Contains(s2))
                                                        {
                                                            string[] dateString = dt.Split('/');
                                                            if (dateString.Count() > 2)
                                                            {
                                                                DateTime d = Convert.ToDateTime(dateString[1] + "/" + dateString[0] + "/" + dateString[2]);
                                                                ACImportModel.Date = d;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            double d = double.Parse(dt);
                                                            DateTime conv = DateTime.FromOADate(d);
                                                            ACImportModel.Date = conv;
                                                            /*string[] dateStrings = dt.Split('-');
                                                            if (dateStrings.Count() > 2)
                                                            {
                                                                DateTime d = Convert.ToDateTime(dateStrings[1] + "/" + dateStrings[0] + "/" + dateStrings[2]);
                                                                ACImportModel.Date = d;
                                                            }*/
                                                        }
                                                    }
                                                }
                                                else if (j.EntityName.ToLower() == "Invoice".ToLower())
                                                {
                                                    ACImportModel.Invoice = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "CustNo".ToLower())
                                                {
                                                    ACImportModel.CustNo = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Company".ToLower())
                                                {
                                                    ACImportModel.Company = worksheet.Cells[row, j.ColumnId.Value]?.Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "BranchCode".ToLower())
                                                {
                                                    ACImportModel.BranchCode = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Branch".ToLower())
                                                {
                                                    ACImportModel.Branch = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Item".ToLower())
                                                {
                                                    ACImportModel.Item = worksheet.Cells[row, j.ColumnId.Value]?.Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Description".ToLower())
                                                {
                                                    ACImportModel.Description = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Po".ToLower())
                                                {
                                                    ACImportModel.Po = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "TranType".ToLower())
                                                {
                                                    ACImportModel.TranType = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Batch".ToLower())
                                                {
                                                    ACImportModel.Batch = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Expiry".ToLower())
                                                {
                                                    var dt = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                    if (dt != null)
                                                    {
                                                        string s2 = "/";
                                                        if (dt.Contains(s2))
                                                        {
                                                            string[] dateString = dt.Split('/');
                                                            if (dateString.Count() > 2)
                                                            {
                                                                DateTime d = Convert.ToDateTime(dateString[1] + "/" + dateString[0] + "/" + dateString[2]);
                                                                ACImportModel.Expiry = d;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string[] dateStrings = dt.Split('-');
                                                            if (dateStrings.Count() > 2)
                                                            {
                                                                DateTime d = Convert.ToDateTime(dateStrings[1] + "/" + dateStrings[0] + "/" + dateStrings[2]);
                                                                ACImportModel.Expiry = d;
                                                            }
                                                            else
                                                            {
                                                                double ds = double.Parse(dt);
                                                                DateTime conv = DateTime.FromOADate(ds);
                                                                ACImportModel.Expiry = conv;
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (j.EntityName.ToLower() == "Ref".ToLower())
                                                {
                                                    ACImportModel.Ref = worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim();
                                                }
                                                else if (j.EntityName.ToLower() == "Price".ToLower())
                                                {
                                                    ACImportModel.Price = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                                else if (j.EntityName.ToLower() == "Discount".ToLower())
                                                {
                                                    ACImportModel.Discount = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                                else if (j.EntityName.ToLower() == "Qty".ToLower())
                                                {
                                                    ACImportModel.Qty = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                                else if (j.EntityName.ToLower() == "Bouns".ToLower())
                                                {
                                                    ACImportModel.Bouns = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                                else if (j.EntityName.ToLower() == "Nett".ToLower())
                                                {
                                                    ACImportModel.Nett = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                                else if (j.EntityName.ToLower() == "Gross".ToLower())
                                                {
                                                    ACImportModel.Gross = decimal.Parse(worksheet.Cells[row, j.ColumnId.Value].Value?.ToString().Trim());
                                                }
                                            });
                                            acItems.Add(ACImportModel);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                            }
                        }
                    }
                    else
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!.It  will support only (.xlsx) Extension", null);
                    }
                }
                count++;
            });
            columnPropertyList.ForEach(s =>
            {
                List<HeaderList> headerLists = new List<HeaderList>();
                headerList.ForEach(h =>
                {
                    HeaderList HeaderList = new HeaderList();
                    HeaderList.Text = h.Text;
                    HeaderList.ID = h.ID;
                    HeaderList.Value = s.Value;
                    headerLists.Add(HeaderList);
                });
                s.ColumnList = headerLists;
            });
            if (type == "Insert")
            {
                List<HistoricalDetails> addHistoricalDetails = new List<HistoricalDetails>();
                acItems.ForEach(a =>
                {
                    long? custId = null;
                    if (navCustomerId > 0)
                    {
                        custId = navCustomerId;
                    }
                    HistoricalDetails historicalDetail = new HistoricalDetails
                    {
                        NavCustomerId = custId,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 1,
                        Date = a.Date,
                        Invoice = a.Invoice,
                        CustNo = a.CustNo,
                        Company = a.Company,
                        BranchCode = a.BranchCode,
                        Branch = a.Branch,
                        Item = a.Item,
                        Description = a.Description,
                        Po = a.Po,
                        TranType = a.TranType,
                        Batch = a.Batch,
                        Expiry = a.Expiry,
                        Ref = a.Ref,
                        Price = a.Price,
                        Discount = a.Discount,
                        Qty = a.Qty,
                        Bouns = a.Bouns,
                        Nett = a.Nett,
                        Gross = a.Gross,
                    };
                    addHistoricalDetails.Add(historicalDetail);
                });
                _context.BulkInsert(addHistoricalDetails);          

               
            }
            return columnPropertyList;
        }
    }
}
