﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.BussinessObject;
using System.Text.Json;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DistributorReplenishmentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public DistributorReplenishmentController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]
        [Route("GetDistributorReplenishmentVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<DistributorReplenishmentModel>>> GetDistributorReplenishmentVersion(string sessionID)
        {
            return await _repository.GetList<DistributorReplenishmentModel>(sessionID);
        }

        [HttpGet]
        [Route("GetDistributorReplenishment")]
        public List<DistributorReplenishmentModel> Get()
        {
            var distributorReplenishment = _context.DistributorReplenishment
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.Company)
                 .Include(c => c.Customer)
                .AsNoTracking().ToList();
            List<DistributorReplenishmentModel> distributorReplenishmentModel = new List<DistributorReplenishmentModel>();
            distributorReplenishment.ForEach(s =>
            {
                DistributorReplenishmentModel distributorReplenishmentModels = new DistributorReplenishmentModel
                {
                    DistributorReplenishmentId = s.DistributorReplenishmentId,
                    CompanyId = s.CompanyId,
                    CompanyListingName = s.Company != null ? s.Company.PlantCode : null,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer != null ? s.Customer.Name : null,
                    FromPeriod = s.FromPeriod,
                    ToPeriod = s.ToPeriod,
                    ReplenishmentTiming = s.ReplenishmentTiming,
                    NeedVersionChecking = s.NeedVersionChecking,
                    NoOfMonthToReplenish = s.NoOfMonthToReplenish,
                    NeedVersionCheckingFlag = s.NeedVersionChecking == true ? "Yes" : "No",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    SessionId = s.SessionId,
                    MaxHoldingStock = s.MaxHoldingStock,
                    DosageFormId = s.DosageFormId,
                    DrugClassificationId = s.DrugClassificationId,
                    DosageForm = s.DosageForm?.Value,
                    DrugClassification = s.DrugClassification?.Value,
                };
                distributorReplenishmentModel.Add(distributorReplenishmentModels);
            });
            return distributorReplenishmentModel.OrderByDescending(a => a.DistributorReplenishmentId).ToList();
        }

        [HttpGet]
        [Route("GetDistributorReplenishmentLineById")]
        public List<DistributorReplenishmentLineModel> GetDistributorReplenishmentLineById(long? id)
        {
            var distributorReplenishment = _context.DistributorReplenishmentLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.ProductName)
                .Where(s => s.DistributorReplenishmentId == id)
                .AsNoTracking().ToList();
            List<DistributorReplenishmentLineModel> DistributorReplenishmentLineModel = new List<DistributorReplenishmentLineModel>();
            distributorReplenishment.ForEach(s =>
            {
                DistributorReplenishmentLineModel DistributorReplenishmentLineModels = new DistributorReplenishmentLineModel
                {
                    DistributorReplenishmentId = s.DistributorReplenishmentId,
                    DistributorReplenishmentLineId = s.DistributorReplenishmentLineId,
                    IsExceptionalItem = s.IsExceptionalItem,
                    ReplenishmentTiming = s.ReplenishmentTiming,
                    NeedVersionChecking = s.NeedVersionChecking,
                    NoOfMonthToReplenish = s.NoOfMonthToReplenish,
                    NeedVersionCheckingFlag = s.NeedVersionChecking == true ? "Yes" : "No",
                    MaxHoldingStock = s.MaxHoldingStock,
                    ProductNameId = s.ProductNameId,
                    ProductName = s.ProductName?.Code,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    SessionId = s.SessionId,
                };
                DistributorReplenishmentLineModel.Add(DistributorReplenishmentLineModels);
            });
            return DistributorReplenishmentLineModel.OrderByDescending(a => a.DistributorReplenishmentId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DistributorReplenishmentModel> GetData(SearchModel searchModel)
        {
            var distributorReplenishment = new DistributorReplenishment();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).LastOrDefault();
                        break;
                    case "Next":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).LastOrDefault();
                        break;
                    case "Previous":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).LastOrDefault();
                        break;
                    case "Next":
                        distributorReplenishment = _context.DistributorReplenishment.OrderBy(o => o.DistributorReplenishmentId).FirstOrDefault(s => s.DistributorReplenishmentId > searchModel.Id);
                        break;
                    case "Previous":
                        distributorReplenishment = _context.DistributorReplenishment.OrderByDescending(o => o.DistributorReplenishmentId).FirstOrDefault(s => s.DistributorReplenishmentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DistributorReplenishmentModel>(distributorReplenishment);
            if (result != null)
            {
                result.NeedVersionCheckingFlag = result.NeedVersionChecking == true ? "Yes" : "No";
            }
            return result;
        }
        [HttpPost]
        [Route("InsertDistributorReplenishment")]
        public DistributorReplenishmentModel Post(DistributorReplenishmentModel value)
        {
            var SessionId = Guid.NewGuid();
            var distributorReplenishments = _context.DistributorReplenishment.ToList();
            if (distributorReplenishments.Any())
            {
                var dist = distributorReplenishments.Where(c =>c.CustomerId!=null && c.CustomerId == value.CustomerId && c.DosageFormId==value.DosageFormId && c.StatusCodeId == value.StatusCodeID).ToList();

                var isCompanyExist = dist.Any(d => (d.FromPeriod <= value.FromPeriod && d.ToPeriod >= value.FromPeriod) || (d.FromPeriod <= value.ToPeriod && d.ToPeriod >= value.ToPeriod));

                if (isCompanyExist)
                {
                    throw new Exception("Customer Already Exist!");
                }
            }
            var distributorReplenishment = new DistributorReplenishment
            {
                FromPeriod = value.FromPeriod,
                ToPeriod = value.ToPeriod,
                CompanyId = value.CompanyId,
                CustomerId = value.CustomerId,
                ReplenishmentTiming = value.ReplenishmentTiming,
                NoOfMonthToReplenish = value.NoOfMonthToReplenish,
                NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
                MaxHoldingStock = value.MaxHoldingStock,
                DosageFormId = value.DosageFormId,
                DrugClassificationId = value.DrugClassificationId,
            };
            _context.DistributorReplenishment.Add(distributorReplenishment);
            _context.SaveChanges();
            value.DistributorReplenishmentId = distributorReplenishment.DistributorReplenishmentId;
            if (value.DosageFormId != null && value.DrugClassificationId != null)
            {
                var genericcodeManufacturerIds = _context.ProductGroupingManufacture.Where(p => p.DosageFormId != null && p.DosageFormId == value.DosageFormId && p.DrugClassificationId != null && p.DrugClassificationId == value.DrugClassificationId).Select(s => s.ProductGroupingId).Distinct().ToList();
                if (genericcodeManufacturerIds.Count > 0)
                {
                    //var SessionId = Guid.NewGuid();
                    //var distributorReplenishments = _context.DistributorReplenishmentLine.ToList();
                    genericcodeManufacturerIds.ForEach(g =>
                    {
                        var exist = _context.DistributorReplenishmentLine.Where(s => s.DistributorReplenishmentId == value.DistributorReplenishmentId && s.ProductNameId == g)?.FirstOrDefault();
                        if (exist == null)
                        {
                            var distributorReplenishmentline = new DistributorReplenishmentLine
                            {
                                DistributorReplenishmentId = value.DistributorReplenishmentId,
                                MaxHoldingStock = value.MaxHoldingStock,
                                IsExceptionalItem = false,
                                ProductNameId = g,
                                ReplenishmentTiming = value.ReplenishmentTiming,
                                NoOfMonthToReplenish = value.NoOfMonthToReplenish,
                                NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false,
                                StatusCodeId = value.StatusCodeID.Value,
                                AddedByUserId = value.AddedByUserID,
                                AddedDate = DateTime.Now,
                            // SessionId = SessionId,
                        };
                            _context.DistributorReplenishmentLine.Add(distributorReplenishmentline);
                            _context.SaveChanges();
                        }
                    });
                }
            }
            return value;
        }



        [HttpPut]
        [Route("UpdateDistributorReplenishment")]
        public async Task<DistributorReplenishmentModel> Put(DistributorReplenishmentModel value)
        {

            value.SessionId ??= Guid.NewGuid();
            var distributorReplenishment = _context.DistributorReplenishment.SingleOrDefault(p => p.DistributorReplenishmentId == value.DistributorReplenishmentId);
            distributorReplenishment.FromPeriod = value.FromPeriod;
            distributorReplenishment.ToPeriod = value.ToPeriod;
            distributorReplenishment.CompanyId = value.CompanyId;
            distributorReplenishment.CustomerId = value.CustomerId;
            distributorReplenishment.ReplenishmentTiming = value.ReplenishmentTiming;
            distributorReplenishment.NoOfMonthToReplenish = value.NoOfMonthToReplenish;
            distributorReplenishment.NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false;
            distributorReplenishment.StatusCodeId = value.StatusCodeID.Value;
            distributorReplenishment.ModifiedByUserId = value.AddedByUserID;
            distributorReplenishment.ModifiedDate = DateTime.Now;
            distributorReplenishment.SessionId = value.SessionId;
            distributorReplenishment.MaxHoldingStock = value.MaxHoldingStock;
            distributorReplenishment.DosageFormId = value.DosageFormId;
            distributorReplenishment.DrugClassificationId = value.DrugClassificationId;
            var distributorline = _context.DistributorReplenishmentLine.Where(s => s.DistributorReplenishmentId == value.DistributorReplenishmentId && s.IsExceptionalItem == false).ToList();
            if(distributorline.Count>0)
            {
                distributorline.ForEach(l=>{
                    l.ReplenishmentTiming = value.ReplenishmentTiming;
                    l.NoOfMonthToReplenish = value.NoOfMonthToReplenish;
                    l.MaxHoldingStock = value.MaxHoldingStock;
                    l.ModifiedByUserId = value.ModifiedByUserID;
                    l.ModifiedDate = DateTime.Now;
                    l.StatusCodeId = value.StatusCodeID.Value;
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteDistributorReplenishment")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var distributorReplenishment = _context.DistributorReplenishment.Include(a=>a.DistributorReplenishmentLine).Where(p => p.DistributorReplenishmentId == id).FirstOrDefault();
                if (distributorReplenishment != null)
                {
                    if (distributorReplenishment.SessionId != null)
                    {
                        var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == distributorReplenishment.SessionId).ToList();
                        _context.TableDataVersionInfo.RemoveRange(versions);

                    }
                    _context.DistributorReplenishment.Remove(distributorReplenishment);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("CreateVersion")]
        public async Task<DistributorReplenishmentModel> CreateVersion(DistributorReplenishmentModel value)
        {
            value.SessionId = value.SessionId.Value;
            var distributorReplenishment = _context.DistributorReplenishment.SingleOrDefault(p => p.DistributorReplenishmentId == value.DistributorReplenishmentId);

            if (distributorReplenishment != null)
            {
                var verObject = _mapper.Map<DistributorReplenishmentModel>(distributorReplenishment);
                verObject.DistributorReplenishmentLine = GetDistributorReplenishmentLineById(value.DistributorReplenishmentId);
                var versionInfo = new TableDataVersionInfoModel<DistributorReplenishmentModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "DistributorReplenishment",
                    PrimaryKey = value.DistributorReplenishmentId,
                    ReferenceInfo = value.ReferenceInfo,
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<DistributorReplenishmentModel> ApplyVersion(DistributorReplenishmentModel value)
        {
            value.SessionId = value.SessionId.Value;
            var distributorReplenishment = _context.DistributorReplenishment.SingleOrDefault(p => p.DistributorReplenishmentId == value.DistributorReplenishmentId);

            if (distributorReplenishment != null)
            {
                distributorReplenishment.FromPeriod = value.FromPeriod;
                distributorReplenishment.ToPeriod = value.ToPeriod;
                distributorReplenishment.CompanyId = value.CompanyId;
                distributorReplenishment.CustomerId = value.CustomerId;
                distributorReplenishment.ReplenishmentTiming = value.ReplenishmentTiming;
                distributorReplenishment.NoOfMonthToReplenish = value.NoOfMonthToReplenish;
                distributorReplenishment.NeedVersionChecking = value.NeedVersionCheckingFlag == "Yes" ? true : false;
                distributorReplenishment.StatusCodeId = value.StatusCodeID.Value;
                distributorReplenishment.ModifiedByUserId = value.AddedByUserID;
                distributorReplenishment.ModifiedDate = DateTime.Now;
                distributorReplenishment.SessionId = value.SessionId;
                _context.SaveChanges();
            }
            var distributorReplenishmentLine = _context.DistributorReplenishmentLine.Where(prc => prc.DistributorReplenishmentId == distributorReplenishment.DistributorReplenishmentId).ToList();
            if (distributorReplenishmentLine != null)
            {
                _context.DistributorReplenishmentLine.RemoveRange(distributorReplenishmentLine);
                _context.SaveChanges();
            }
            if (value.DistributorReplenishmentLine != null)
            {
                value.DistributorReplenishmentLine.ForEach(ec =>
                {
                    var distributorReplenishment = new DistributorReplenishmentLine
                    {
                        DistributorReplenishmentId = ec.DistributorReplenishmentId,
                        MaxHoldingStock = ec.MaxHoldingStock,
                        IsExceptionalItem = ec.IsExceptionalItem,
                        ProductNameId = ec.ProductNameId,
                        ReplenishmentTiming = ec.ReplenishmentTiming,
                        NoOfMonthToReplenish = ec.NoOfMonthToReplenish,
                        NeedVersionChecking = ec.NeedVersionCheckingFlag == "Yes" ? true : false,
                        StatusCodeId = ec.StatusCodeID.Value,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        
                    };
                    _context.DistributorReplenishmentLine.Add(distributorReplenishment);


                });
            }
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(DistributorReplenishmentModel value)
        {
            value.SessionId = value.SessionId.Value;
            var distributorReplenishment = _context.DistributorReplenishment.SingleOrDefault(p => p.DistributorReplenishmentId == value.DistributorReplenishmentId);

            if (distributorReplenishment != null)
            {
                var verObject = _mapper.Map<DistributorReplenishmentModel>(distributorReplenishment);
                verObject.DistributorReplenishmentLine = GetDistributorReplenishmentLineById(value.DistributorReplenishmentId);
                var versionInfo = new TableDataVersionInfoModel<DistributorReplenishmentModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "DistributorReplenishment",
                    PrimaryKey = value.DistributorReplenishmentId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<DistributorReplenishmentModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<DistributorReplenishmentModel>(tableData.JsonData);

                    var distributorReplenishment = _context.DistributorReplenishment.Include(a=>a.DistributorReplenishmentLine).SingleOrDefault(p => p.DistributorReplenishmentId == verObject.DistributorReplenishmentId);


                    if (verObject != null && distributorReplenishment != null)
                    {
                        distributorReplenishment.FromPeriod = verObject.FromPeriod;
                        distributorReplenishment.ToPeriod = verObject.ToPeriod;
                        distributorReplenishment.CompanyId = verObject.CompanyId;
                        distributorReplenishment.CustomerId = verObject.CustomerId;
                        distributorReplenishment.ReplenishmentTiming = verObject.ReplenishmentTiming;
                        distributorReplenishment.NoOfMonthToReplenish = verObject.NoOfMonthToReplenish;
                        distributorReplenishment.NeedVersionChecking = verObject.NeedVersionCheckingFlag == "Yes" ? true : false;
                        distributorReplenishment.StatusCodeId = verObject.StatusCodeID.Value;
                        distributorReplenishment.ModifiedByUserId = verObject.AddedByUserID;
                        distributorReplenishment.ModifiedDate = DateTime.Now;

                        _context.SaveChanges();
                        //var distributorReplenishmentLine = _context.DistributorReplenishmentLine.Where(prc => prc.DistributorReplenishmentId == distributorReplenishment.DistributorReplenishmentId).ToList();
                        if (verObject.DistributorReplenishmentLine != null)
                        {
                            _context.DistributorReplenishmentLine.RemoveRange(distributorReplenishment.DistributorReplenishmentLine);
                            _context.SaveChanges();
                        }
                        if (verObject.DistributorReplenishmentLine != null)
                        {
                            verObject.DistributorReplenishmentLine.ForEach(ec =>
                            {
                                var distributorReplenishment = new DistributorReplenishmentLine
                                {
                                    DistributorReplenishmentId = ec.DistributorReplenishmentId,
                                    MaxHoldingStock = ec.MaxHoldingStock,
                                    IsExceptionalItem = ec.IsExceptionalItem,
                                    ProductNameId = ec.ProductNameId,
                                    ReplenishmentTiming = ec.ReplenishmentTiming,
                                    NoOfMonthToReplenish = ec.NoOfMonthToReplenish,
                                    NeedVersionChecking = ec.NeedVersionCheckingFlag == "Yes" ? true : false,
                                    StatusCodeId = ec.StatusCodeID.Value,
                                    AddedByUserId = ec.AddedByUserID,
                                    AddedDate = DateTime.Now,

                                };
                                _context.DistributorReplenishmentLine.Add(distributorReplenishment);


                            });
                        }
                    }

                    return verObject;
                }
                return new DistributorReplenishmentModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
    }
}