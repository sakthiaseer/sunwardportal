﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentRolePermissionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DocumentRolePermissionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentRolePermission")]
        public List<DocumentRolePermissionModel> Get()
        {
            var documentRolePermission = _context.DocumentRolePermission.Include(dp => dp.DocumentPermission).Include(d => d.DocumentRole).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<DocumentRolePermissionModel> documentRolePermissionModel = new List<DocumentRolePermissionModel>();
            documentRolePermission.ForEach(s =>
            {
                DocumentRolePermissionModel documentRolePermissionModels = new DocumentRolePermissionModel
                {
                    DocumentRolePermissionID = s.DocumentRolePermissionId,
                    //DocumentID = s.DocumentId,
                    //FileName = s.Document.FileName,
                    DocumentPermissionID = s.DocumentPermissionId,
                    //CodeID = s.StatusCode.CodeId,
                    //CodeType = s.StatusCode.CodeType,

                    DocumentRoleID = s.DocumentRoleId,
                    DocumentRoleName = s.DocumentRole != null ? s.DocumentRole.DocumentRoleName : "",
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                    //Document Permission
                    IsRead = s.DocumentPermission.IsRead,
                    IsCreateFolder = s.DocumentPermission.IsCreateFolder,
                    IsCreateDocument = s.DocumentPermission.IsCreateDocument,
                    IsSetAlert = s.DocumentPermission.IsSetAlert,
                    IsEditIndex = s.DocumentPermission.IsEditIndex,
                    IsRename = s.DocumentPermission.IsRename,
                    IsUpdateDocument = s.DocumentPermission.IsUpdateDocument,
                    IsCopy = s.DocumentPermission.IsCopy,
                    IsMove = s.DocumentPermission.IsMove,
                    IsDelete = s.DocumentPermission.IsDelete,
                    IsRelationship = s.DocumentPermission.IsRelationship,
                    IsListVersion = s.DocumentPermission.IsListVersion,
                    IsInvitation = s.DocumentPermission.IsInvitation,
                    IsSendEmail = s.DocumentPermission.IsSendEmail,
                    IsDiscussion = s.DocumentPermission.IsDiscussion,
                    IsAccessControl = s.DocumentPermission.IsAccessControl,
                    IsAuditTrail = s.DocumentPermission.IsAuditTrail,
                    IsRequired = s.DocumentPermission.IsRequired

                };
                documentRolePermissionModel.Add(documentRolePermissionModels);
            });
            return documentRolePermissionModel.OrderByDescending(a => a.DocumentRolePermissionID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get DocumentRolePermission")]
        [HttpGet("GetDocumentRolePermission/{id:int}")]
        public ActionResult<DocumentRolePermissionModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documentRolePermission = _context.DocumentRolePermission.SingleOrDefault(p => p.DocumentRolePermissionId == id.Value);
            var result = _mapper.Map<DocumentRolePermissionModel>(documentRolePermission);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentRolePermissionModel> GetData(SearchModel searchModel)
        {
            var documentRolePermission = new DocumentRolePermission();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).LastOrDefault();
                        break;
                    case "Next":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).LastOrDefault();
                        break;
                    case "Previous":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).FirstOrDefault();
                        break;
                    case "Last":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).LastOrDefault();
                        break;
                    case "Next":
                        documentRolePermission = _context.DocumentRolePermission.OrderBy(o => o.DocumentRolePermissionId).FirstOrDefault(s => s.DocumentRolePermissionId > searchModel.Id);
                        break;
                    case "Previous":
                        documentRolePermission = _context.DocumentRolePermission.OrderByDescending(o => o.DocumentRolePermissionId).FirstOrDefault(s => s.DocumentRolePermissionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentRolePermissionModel>(documentRolePermission);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentRolePermission")]
        public DocumentRolePermissionModel Post(DocumentRolePermissionModel value)
        {
            var documentRolePermission = new DocumentRolePermission
            {
                //DocumentRolePermissionId = value.FolderID,
                //DocumentRolePermissionId = value.DocumentRolePermissionID,
                //DocumentId = value.DocumentID,
                DocumentPermissionId = value.DocumentPermissionID,
                DocumentRoleId = value.DocumentRoleID,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now
            };
            _context.DocumentRolePermission.Add(documentRolePermission);
            _context.SaveChanges();

            var documentPermission = new DocumentPermission
            {
                DocumentId = value.DocumentID,

                IsRead = value.IsRead,
                IsCreateFolder = value.IsCreateFolder,
                IsCreateDocument = value.IsCreateDocument,
                IsSetAlert = value.IsSetAlert,
                IsEditIndex = value.IsEditIndex,
                IsRename = value.IsRename,
                IsUpdateDocument = value.IsUpdateDocument,
                IsCopy = value.IsCopy,
                IsMove = value.IsMove,
                IsDelete = value.IsDelete,
                IsRelationship = value.IsRelationship,
                IsListVersion = value.IsListVersion,
                IsInvitation = value.IsInvitation,
                IsSendEmail = value.IsSendEmail,
                IsDiscussion = value.IsDiscussion,
                IsAccessControl = value.IsAccessControl,
                IsAuditTrail = value.IsAuditTrail,
                IsRequired = value.IsRequired,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now
            };
            _context.DocumentPermission.Add(documentPermission);
            _context.SaveChanges();
            value.DocumentPermissionID = documentPermission.DocumentPermissionId;

            value.DocumentRolePermissionID = documentRolePermission.DocumentRolePermissionId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateDocumentRolePermission")]
        public DocumentRolePermissionModel Put(DocumentRolePermissionModel value)
        {
            var documentRolePermission = _context.DocumentRolePermission.SingleOrDefault(p => p.DocumentRolePermissionId == value.DocumentRolePermissionID);

            var documentPermission = _context.DocumentPermission.SingleOrDefault(p => p.DocumentPermissionId == value.DocumentPermissionID);

            //documentRolePermission.DocumentId = value.DocumentID;
            documentRolePermission.DocumentPermissionId = value.DocumentPermissionID;
            documentRolePermission.DocumentRoleId = value.DocumentRoleID;
            documentRolePermission.StatusCodeId = value.StatusCodeID;
            documentRolePermission.ModifiedByUserId = value.ModifiedByUserID;
            documentRolePermission.ModifiedDate = DateTime.Now;

            //Document Permission

            //documentPermission.DocumentId = documentRolePermission.DocumentId;

            documentPermission.IsRead = value.IsRead;
            documentPermission.IsCreateFolder = value.IsCreateFolder;
            documentPermission.IsCreateDocument = value.IsCreateDocument;
            documentPermission.IsSetAlert = value.IsSetAlert;
            documentPermission.IsEditIndex = value.IsEditIndex;
            documentPermission.IsRename = value.IsRename;
            documentPermission.IsUpdateDocument = value.IsUpdateDocument;
            documentPermission.IsCopy = value.IsCopy;
            documentPermission.IsMove = value.IsMove;
            documentPermission.IsDelete = value.IsDelete;
            documentPermission.IsRelationship = value.IsRelationship;
            documentPermission.IsListVersion = value.IsListVersion;
            documentPermission.IsInvitation = value.IsInvitation;
            documentPermission.IsSendEmail = value.IsSendEmail;
            documentPermission.IsDiscussion = value.IsDiscussion;
            documentPermission.IsAccessControl = value.IsAccessControl;
            documentPermission.IsAuditTrail = value.IsAuditTrail;
            documentPermission.IsRequired = value.IsRequired;
            documentPermission.StatusCodeId = value.StatusCodeID;
            documentPermission.AddedByUserId = value.AddedByUserID;
            documentPermission.AddedDate = DateTime.Now;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteDocumentRolePermission")]
        public void Delete(int id)
        {
            var documentRolePermission = _context.DocumentRolePermission.SingleOrDefault(p => p.DocumentRolePermissionId == id);
            if (documentRolePermission != null)
            {
                _context.DocumentRolePermission.Remove(documentRolePermission);
                _context.SaveChanges();
            }
        }
    }
}