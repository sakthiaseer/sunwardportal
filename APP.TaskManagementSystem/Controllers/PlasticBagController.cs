﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PlasticBagController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PlasticBagController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
       
        [HttpGet]
        [Route("GetPlasticBags")]
        public List<PlasticBagModel> Get()
        {
            List<PlasticBagModel> plasticBagModels = new List<PlasticBagModel>();
            var plasticBag = _context.PlasticBag.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.PlasticBagId).AsNoTracking().ToList();
            if(plasticBag!=null && plasticBag.Count>0)
            {
                plasticBag.ForEach(s =>
                {
                    PlasticBagModel plasticBagModel = new PlasticBagModel
                    {
                        PlasticBagID = s.PlasticBagId,
                        Name = s.Name,
                        Description = s.Description,
                        TarWeight = s.TarWeight,                        
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate

                    };
                    plasticBagModels.Add(plasticBagModel);
                });
            }
           
            return plasticBagModels;
        }

        
        //[HttpGet("{id}", Name = "Get PlasticBag")]
        [HttpGet("GetPlasticBags/{id:int}")]
        public ActionResult<PlasticBagModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var PlasticBag = _context.PlasticBag.SingleOrDefault(p => p.PlasticBagId == id.Value);
            var result = _mapper.Map<PlasticBagModel>(PlasticBag);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PlasticBagModel> GetData(SearchModel searchModel)
        {
            var PlasticBag = new PlasticBag();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).FirstOrDefault();
                        break;
                    case "Last":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).LastOrDefault();
                        break;
                    case "Next":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).LastOrDefault();
                        break;
                    case "Previous":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).FirstOrDefault();
                        break;
                    case "Last":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).LastOrDefault();
                        break;
                    case "Next":
                        PlasticBag = _context.PlasticBag.OrderBy(o => o.PlasticBagId).FirstOrDefault(s => s.PlasticBagId > searchModel.Id);
                        break;
                    case "Previous":
                        PlasticBag = _context.PlasticBag.OrderByDescending(o => o.PlasticBagId).FirstOrDefault(s => s.PlasticBagId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PlasticBagModel>(PlasticBag);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPlasticBag")]
        public PlasticBagModel Post(PlasticBagModel value)
        {
            var PlasticBag = new PlasticBag
            {
                
                Name = value.Name,
                Description = value.Description,
                TarWeight = value.TarWeight,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,                

            };
            _context.PlasticBag.Add(PlasticBag);
            _context.SaveChanges();
            value.PlasticBagID = PlasticBag.PlasticBagId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePlasticBag")]
        public PlasticBagModel Put(PlasticBagModel value)
        {
            var PlasticBag = _context.PlasticBag.SingleOrDefault(p => p.PlasticBagId == value.PlasticBagID);
           
            PlasticBag.ModifiedByUserId = value.ModifiedByUserID;
            PlasticBag.ModifiedDate = DateTime.Now;
            PlasticBag.Name = value.Name;
            PlasticBag.Description = value.Description;
            PlasticBag.TarWeight = value.TarWeight;           
            PlasticBag.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePlasticBag")]
        public void Delete(int id)
        {
            var PlasticBag = _context.PlasticBag.SingleOrDefault(p => p.PlasticBagId == id);
            if (PlasticBag != null)
            {
                _context.PlasticBag.Remove(PlasticBag);
                _context.SaveChanges();
            }
        }
    }
}