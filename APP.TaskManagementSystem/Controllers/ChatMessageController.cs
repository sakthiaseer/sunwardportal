﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ChatMessageController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ChatMessageController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }

        [HttpGet]
        [Route("GetUserNotificationMessages")]
        public List<ChatMessageModel> GetUserNotificationMessages(int Id, int userId)
        {
            var result = _context.ChatMessage.Include(r => r.ReceiveUser).Include(s => s.SentUser).OrderByDescending(item => item.CreatedDateTime).GroupBy(s => s.SentUserId).Select(s => s.FirstOrDefault(g => g.IsDelivered == false)).Where(w => w.ReceiveUserId == Id && w.SentUserId != userId).ToList();
            UpdateDeliveredMessage(Id, userId);
            var chatMessageUser = new List<ChatMessageModel>();
            result.ForEach(s =>
            {
                if (s.SentUserId != userId)
                {
                    var message = new ChatMessageModel
                    {
                        ChatMessageId = s.ChatMessageId,
                        ChatGroupId = s.ChatGroupId,
                        SentUserId = s.SentUserId,
                        SenderName = s.SentUser.UserName,
                        ReceiveUserId = s.ReceiveUserId,
                        ReceiverName = s.ReceiveUser.UserName,
                        Message = s.Message,
                        CreatedDateTime = s.CreatedDateTime,
                        FileName = s.FileName,
                        FilePath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Chat\ChatImage\" + s.FileName
                    };
                    chatMessageUser.Add(message);
                }
            });
            return chatMessageUser;
        }

        private void UpdateDeliveredMessage(int id, int userId)
        {
            var result = _context.ChatMessage.Where(w => w.ReceiveUserId == id && w.SentUserId != userId && w.IsDelivered == false).ToList();
            result.ForEach(c =>
            {
                c.IsDelivered = true;
                _context.ChatMessage.Update(c);
            });
            _context.SaveChanges();
        }

        [HttpGet]
        [Route("GetUsers")]
        public List<ChatUserModel> GetUsers(int Id)
        {
            var chatUsers = _context.ApplicationUser.Include(a => a.Employe).ToList();
            List<ChatUserModel> ChatUserModel = new List<ChatUserModel>();
            chatUsers.ForEach(s =>
            {
                ChatUserModel ChatUserModels = new ChatUserModel
                {
                    UserId = s.UserId,
                    ChatUserId = s.Employe != null ? s.Employe.EmployeeId : 0,
                    UserName = s.LoginId,
                    Name = s.UserName,
                    ImageURL = s.Employe != null ? s.Employe.ImageUrl : string.Empty,
                };
                ChatUserModel.Add(ChatUserModels);
            });
            return ChatUserModel.Where(u => u.UserId != Id).OrderByDescending(a => a.ChatUserId).ToList();
        }

        [HttpGet]
        [Route("GetChatUsers")]
        public List<ChatUserModel> GetChatUsers()
        {
            var chats = _context.ChatUser.Include("User").Include("User.Employe").ToList();
            var chatUsers = _context.ChatUser.Include(a => a.User).Include(u => u.User.Employe).ToList();
            List<ChatUserModel> ChatUserModel = new List<ChatUserModel>();
            chatUsers.ForEach(s =>
            {
                ChatUserModel ChatUserModels = new ChatUserModel
                {
                    UserId = s.UserId,
                    ChatUserId = s.User.Employe != null ? s.User.Employe.EmployeeId : 0,
                    UserName = s.User.LoginId,
                    Name = s.User.UserName,
                    ImageURL = s.User.Employe != null ? s.User.Employe.ImageUrl : string.Empty,
                    IsOnline = s.IsOnline.GetValueOrDefault(),
                    ConnectionId = s.ConnectionId,
                    LastMessage = _context.ChatMessage.OrderBy(d => d.CreatedDateTime).LastOrDefault(m => m.SentUserId == s.UserId || m.ReceiveUserId == s.UserId).Message
                };
                ChatUserModel.Add(ChatUserModels);
            });
            ChatUserModel.ForEach(c =>
            {
                var chatMessageInfo = _context.ChatMessage.OrderBy(d => d.CreatedDateTime).LastOrDefault(m => m.SentUserId == c.UserId || m.ReceiveUserId == c.UserId);
                c.LastMessage = chatMessageInfo != null ? chatMessageInfo.Message : string.Empty;
                c.LastChattedDate = chatMessageInfo != null ? chatMessageInfo.CreatedDateTime : null;
            });
            return ChatUserModel.OrderByDescending(a => a.ChatUserId).ToList();
        }

        [HttpGet("GetMessageByUser")]
        public List<ChatMessageModel> GetMessageByUser(int Id, int itemId)
        {
            var chatMessages = _context.ChatMessage.Include(a => a.ReceiveUser).Include(u => u.SentUser).Where(t => (t.SentUserId == Id && t.ReceiveUserId == itemId) || (t.SentUserId == itemId && t.ReceiveUserId == Id)).ToList();
            List<ChatMessageModel> ChatMessageModel = new List<ChatMessageModel>();
            chatMessages.ForEach(s =>
            {
                ChatMessageModel ChatMessageModels = new ChatMessageModel
                {
                    ChatMessageId = s.ChatMessageId,
                    ChatGroupId = s.ChatGroupId,
                    SentUserId = s.SentUserId,
                    SenderName = s.SentUser.UserName,
                    ReceiveUserId = s.ReceiveUserId,
                    ReceiverName = s.ReceiveUser.UserName,
                    Message = s.Message,
                    CreatedDateTime = s.CreatedDateTime,
                    FileName = s.FileName,
                    MessageType = s.MessageType.Value,
                    FilePath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Chat\ChatImage\" + s.FileName
                };
                ChatMessageModel.Add(ChatMessageModels);
            });

            ChatMessageModel.ForEach(c =>
            {
                if (c.SentUserId == Id)
                {
                    c.IsIncoming = true;
                }
            });

            return ChatMessageModel;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteChatMessage")]
        public void Delete(int id)
        {
            var chatMessage = _context.ChatMessage.SingleOrDefault(p => p.ChatMessageId == id);
            if (chatMessage != null)
            {
                _context.ChatMessage.Remove(chatMessage);
                _context.SaveChanges();
            }
        }

        // POST: api/User
        [HttpPost]
        [Route("UploadChatImage")]
        public ChatMessageAttachmentModel UploadChatImage(ChatMessageAttachmentModel value)
        {
            if (value.UploadFile != null && value.UploadFile.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Chat\ChatImage\" + value.FileName;
                System.IO.File.WriteAllBytes(serverPath, value.UploadFile);
                value.UploadedFilePath = serverPath;
            }
            return value;
        }

        [HttpPost]
        [Route("UploadChatAudio")]
        public ChatMessageAttachmentModel UploadChatAudio(ChatMessageAttachmentModel value)
        {
            if (value.UploadFile != null && value.UploadFile.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Chat\ChatAudio\" + value.FileName;
                System.IO.File.WriteAllBytes(serverPath, value.UploadFile);
                value.UploadedFilePath = serverPath;
            }
            return value;
        }

    }
}