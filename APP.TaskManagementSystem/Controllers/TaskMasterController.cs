﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using APP.EntityModel.Param;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<TaskMasterController> _logger;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public TaskMasterController(CRT_TMSContext context, IMapper mapper, ILogger<TaskMasterController> logger, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskMasters")]
        public List<TaskMasterModel> Get(int id)
        {
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == id && t.StatusCodeId == 512).AsNoTracking().ToList();
            var taskIds = taskAssigned.Select(s => s.TaskId).ToList();
            var taskMaster = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation").Include("TaskAttachment")
                .Include("TaskMembers").Include("TaskTag").Include("TaskAssigned")
                .Include("TaskLinksTask").Include("ModifiedByUser")
                .OrderByDescending(o => o.TaskId).Where(t => taskIds.Contains(t.TaskId)).AsNoTracking().ToList();
            taskMaster.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Title = s.Title,
                    ParentTaskID = s.ParentTaskId,
                    MainTaskId = s.MainTaskId,
                    AssignedTo = s.TaskAssigned.Select(c => c.UserId).ToList(),
                    DueDate = s.DueDate,
                    Description = s.Description,
                    ProjectID = s.ProjectId,
                    SessionID = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    OwnerID = s.TaskAssigned.FirstOrDefault().TaskOwnerId,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                    IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false,
                    IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false,
                    Start = s.DueDate.Value,
                    End = s.DueDate.Value,
                    Id = s.TaskId,
                    OnBehalfName = s.OnBehalfNavigation.UserName,
                    OnBehalfID = s.OnBehalf,
                    OverDueAllowed = s.OverDueAllowed,
                    FollowUp = s.FollowUp,
                    TagNames = s.TaskTag.Select(t => t.Tag.Name).ToList(),
                    cssClass = s.AssignedTo == id ? s.DueDate < DateTime.Today ? s.StatusCodeId == 512 ? "red" : "blue" : "green" : "blue",
                    TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList(),
                    OrderNo = s.OrderNo,
                    IsNoDueDate = s.IsNoDueDate,
                    AssignedCC = s.TaskMembers.Where(t => t.TaskId == s.TaskId).Select(c => c.AssignedCc).ToList(),
                    UserNames = _context.ApplicationUser.Where(a => a.UserId == s.AssignedTo).Select(tn => tn.UserName).ToList(),
                    TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                    Attachments = s.TaskAttachment.Select(sa => sa.DocumentId.Value).ToList(),
                    AttachmentIds = s.TaskAttachment.Where(sa => sa.TaskMasterId == s.TaskId).Select(t => t.TaskAttachmentId).ToList(),
                    // WorkType = "Task",
                    WorkType = s.ParentTaskId == null ? "Task" : "SubTask",
                    TaskNote = _context.TaskNotes.Where(u => u.TaskUserId == id && u.TaskId == s.TaskId).Select(tn => new TaskNotesModel { Notes = tn.Notes, RemainderDate = tn.RemainderDate }).FirstOrDefault(),
                    RequestType = "Task Assigned",
                    StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault(),
                    IsNotifyByMessage = s.IsNotifyByMessage,
                    IsEmail = s.IsEmail,
                    NewDueDate = s.NewDueDate,

                };
                taskMasterModels.Add(taskMasterModel);
            });
            return taskMasterModels;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskLinksByID")]
        public List<TaskMasterModel> GetTaskLinksByID(long id)
        {
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskLinks = _context.TaskLinks
                            .Include(l => l.LinkedTask)
                            .Include(t => t.Task)
                            .Include(a => a.LinkedTask.AddedByUser)
                            .Include(a => a.LinkedTask.ModifiedByUser)
                            .OrderByDescending(o => o.TaskId)
                            .Where(t => t.TaskId == id).AsNoTracking().ToList();

            taskLinks.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.LinkedTask.TaskId,
                    Title = s.LinkedTask.Title,
                    ParentTaskID = s.LinkedTask.ParentTaskId,
                    MainTaskId = s.LinkedTask.MainTaskId,
                    DueDate = s.LinkedTask.DueDate,
                    TaskLinkDescription = s.Description,
                    TaskLinkID = s.TaskLinkId,
                    Id = s.LinkedTask.TaskId,
                    AddedByUserID = s.LinkedTask.AddedByUserId,
                    AddedByUser = s.LinkedTask.AddedByUser.UserName,
                    ModifiedByUser = s.LinkedTask.ModifiedByUser != null ? s.LinkedTask.ModifiedByUser.UserName : "",
                    StatusCodeID = s.LinkedTask.StatusCodeId,
                    AddedDate = s.LinkedTask.AddedDate,
                    ModifiedDate = s.LinkedTask.ModifiedDate,
                    // WorkType = "Task",
                    WorkType = s.LinkedTask.ParentTaskId == null ? "Task" : "SubTask"

                };
                taskMasterModels.Add(taskMasterModel);
            });

            return taskMasterModels;
        }

        [HttpGet]
        [Route("GetTaskListByUser")]
        public List<TaskMasterModel> GetTaskListByUser(int id)
        {
            var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == id || t.OnBehalfId == id && t.StatusCodeId == 512).AsNoTracking().ToList();
            var assignedCC = _context.TaskMembers.Where(t => t.AssignedCc == id && t.StatusCodeId == 512).AsNoTracking().ToList();
            var taskIds = taskAssigned.Select(s => s.TaskId).ToList();
            var taskccIds = assignedCC.Select(s => s.TaskId).ToList();
            var applicationUserList = _context.ApplicationUser.AsNoTracking().ToList();
            var taskNote = _context.TaskNotes.AsNoTracking().ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskMaster = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation").Include("TaskAttachment")
                .Include("TaskMembers").Include("TaskTag").Include("TaskAssigned")
                .Include("TaskLinksTask").Include("ModifiedByUser")
                .OrderByDescending(o => o.TaskId)
                .Where(t => taskIds.Contains(t.TaskId) || taskccIds.Contains(t.TaskId)).AsNoTracking().ToList();
            taskMaster.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Title = s.Title,
                    ParentTaskID = s.ParentTaskId,
                    MainTaskId = s.MainTaskId,
                    AssignedTo = s.TaskAssigned != null ? s.TaskAssigned.Select(c => c.UserId).ToList() : new List<long?>(),
                    DueDate = s.DueDate,
                    Description = s.Description,
                    ProjectID = s.ProjectId,
                    SessionID = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    OwnerID = s.TaskAssigned != null ? s.TaskAssigned.FirstOrDefault().TaskOwnerId : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                    IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false,
                    IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false,
                    Start = s.DueDate.Value,
                    End = s.DueDate.Value,
                    Id = s.TaskId,
                    OnBehalfName = s.OnBehalfNavigation != null ? s.OnBehalfNavigation.UserName : string.Empty,
                    OnBehalfID = s.OnBehalf,
                    OverDueAllowed = s.OverDueAllowed,
                    FollowUp = s.FollowUp,
                    TagNames = s.TaskTag != null ? s.TaskTag.Where(tag => tag.Tag != null).Select(t => t.Tag.Name).ToList() : new List<string>(),
                    cssClass = s.AssignedTo == id ? s.DueDate < DateTime.Today ? s.StatusCodeId == 512 ? "red" : "blue" : "green" : "blue",
                    TagIds = s.TaskTag != null ? s.TaskTag.Select(t => t.TagId.Value).ToList() : new List<long>(),
                    OrderNo = s.OrderNo,
                    IsNoDueDate = s.IsNoDueDate,
                    AssignedCC = s.TaskMembers.Where(t => t.TaskId == s.TaskId).Select(c => c.AssignedCc).ToList(),
                    UserNames = applicationUserList != null ? applicationUserList.Where(a => a.UserId == s.AssignedTo).Select(tn => tn.UserName).ToList() : new List<string>(),
                    TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                    Attachments = s.TaskAttachment.Select(sa => sa.DocumentId.Value).ToList(),
                    AttachmentIds = s.TaskAttachment.Where(sa => sa.TaskMasterId == s.TaskId).Select(t => t.TaskAttachmentId).ToList(),
                    // WorkType = "Task",
                    WorkType = s.ParentTaskId == null ? "Task" : "SubTask",
                    TaskNote = taskNote.Where(u => u.TaskUserId == id && u.TaskId == s.TaskId).Select(tn => new TaskNotesModel { Notes = tn.Notes, RemainderDate = tn.RemainderDate }).FirstOrDefault(),
                    RequestType = "Task Assigned",
                    StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault(),
                    IsNotifyByMessage = s.IsNotifyByMessage,
                    IsEmail = s.IsEmail,
                    NewDueDate = s.NewDueDate,
                };
                taskMasterModels.Add(taskMasterModel);
            });

            return taskMasterModels;
        }

        [HttpGet]
        [Route("GetUsersByTaskID")]
        public TaskUsers GetUsersByTaskID(int id)
        {
            TaskUsers taskUsers = new TaskUsers();
            var assignedTo = _context.TaskAssigned.Where(t => t.TaskId == id && t.StatusCodeId == 512).AsNoTracking().Select(s => s.UserId).ToList();
            var assignedCC = _context.TaskMembers.Where(t => t.TaskId == id && t.StatusCodeId == 512).AsNoTracking().Select(s => s.AssignedCc).ToList();
            var onbehalf = _context.TaskAssigned.Where(t => t.TaskId == id && t.StatusCodeId == 512).AsNoTracking().Select(s => s.OnBehalfId).FirstOrDefault();


            if (assignedTo != null && assignedTo.Count != 0)
            {
                taskUsers.AssignedToUserIDs = assignedTo;
            }
            if (assignedCC != null && assignedCC.Count != 0)
            {
                taskUsers.AssignedCCUserIDs = assignedCC;
            }
            if (onbehalf != null)
            {
                taskUsers.Onbehalf = onbehalf;
            }
            return taskUsers;
        }
        [HttpGet]
        [Route("GetUserGroups")]
        public List<UserGroupModel> GetUserGroup()
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();
            var userGroup = _context.UserGroup
                            .Include("AddedByUser")
                            .Include("ModifiedByUser")
                            .OrderByDescending(o => o.UserGroupId).Where(u => u.IsTms == true && u.StatusCodeId == 1).AsNoTracking().ToList();
            userGroup.ForEach(s =>
            {

                UserGroupModel userGroupModel = new UserGroupModel
                {
                    UserGroupID = s.UserGroupId,
                    UserIDs = s.UserGroupUser.Select(c => c.UserId).ToList(),
                    Name = s.Name,
                    Description = s.Description,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsTMS = s.IsTms.Value,
                };
                userGroupModels.Add(userGroupModel);
            });

            return userGroupModels;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskAsignee")]
        public List<TaskMasterModel> GetTaskAsignee(int id, int userId)
        {
            var taskId = new List<long>();
            if (id == 611)
            {
                taskId = _context.TaskAssigned.Where(t => t.TaskOwnerId == userId).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            }
            else if (id == 612)
            {
                taskId = _context.TaskMembers.Where(t => t.AssignedCc == userId).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            }
            else if (id == 615)
            {
                taskId = _context.TaskAssigned.Where(t => t.UserId == userId).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            }
            else
            {
                taskId = _context.TaskMaster.Where(t => t.OnBehalf == userId).AsNoTracking().Select(s => s.TaskId).ToList();
            }

            var taskMaster = _context.TaskMaster
                                .Include("AddedByUser")
                                .Include("TaskAssigned")
                                .Include("TaskAttachment")
                                .Include("TaskMembers")
                                .Include("TaskTag")
                                .Include("TaskLinksTask")
                                .Include("ModifiedByUser")
                                .Include("OnBehalfNavigation")
                                .OrderByDescending(o => o.TaskId)
                                .Where(t => taskId.Contains(t.TaskId)).AsNoTracking().ToList();

            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            taskMaster.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Title = s.Title,
                    ParentTaskID = s.ParentTaskId,
                    MainTaskId = s.MainTaskId,
                    AssignedTo = s.TaskAssigned.Select(t => t.UserId).ToList(),
                    AddedByUserID = s.AddedByUserId,
                    OwnerID = s.TaskAssigned.FirstOrDefault()?.TaskOwnerId,
                    DueDate = s.DueDate,
                    Description = s.Description,
                    ProjectID = s.ProjectId,
                    SessionID = s.SessionId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                    IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false,
                    IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false,
                    Start = s.DueDate.Value,
                    End = s.DueDate.Value,
                    Id = s.TaskId,
                    cssClass = s.AddedByUserId == userId ? s.DueDate < DateTime.Today ? s.StatusCodeId == 512 ? "red" : "blue" : "green" : "blue",
                    TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList(),
                    AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList(),
                    OrderNo = s.OrderNo,
                    OnBehalfName = s.OnBehalfNavigation?.UserName,
                    OnBehalfID = s.OnBehalf,
                    OverDueAllowed = s.OverDueAllowed,
                    FollowUp = s.FollowUp,
                    TagNames = s.TaskTag.Select(t => t.Tag.Name).ToList(),
                    TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                    Attachments = s.TaskAttachment.Select(sa => sa.DocumentId.Value).ToList(),
                    // WorkType = "Task",
                    WorkType = s.ParentTaskId == null ? "Task" : "SubTask",
                    IsNoDueDate = s.IsNoDueDate,
                    RequestType = "Task Assigned",
                    StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault(),
                    TaskNote = _context.TaskNotes.Where(u => u.TaskUserId == userId && u.TaskId == s.TaskId).Select(tn => new TaskNotesModel { Notes = tn.Notes, RemainderDate = tn.RemainderDate }).FirstOrDefault(),
                    IsNotifyByMessage = s.IsNotifyByMessage,
                    IsEmail = s.IsEmail,
                    NewDueDate = s.NewDueDate,
                };
                taskMasterModels.Add(taskMasterModel);
            });
            return taskMasterModels;
        }

        [HttpPost]
        [Route("GetRemainderTaskList")]
        public List<TaskMasterModel> GetRemainderTaskList(TaskSearchModel value)
        {
            var taskMessage = _context.TaskCommentUser.Where(c => c.UserId == value.UserId && c.IsRead == false).AsNoTracking().ToList();

            var taskIds = _context.TaskNotes.Where(w => w.TaskUserId == value.UserId && w.IsNoReminderDate == false).AsNoTracking().Select(s => s.TaskId).ToList();

            var query = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation")
                .Include("TaskMembers").Include("TaskTag")
                .Include("TaskLinksTask")
                .Include("TaskAssigned").Include("TaskNotes")
                .OrderByDescending(o => o.TaskId).Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.ParentTaskId,
                    s.MainTaskId,
                    s.TaskAssigned,
                    s.AddedByUserId,
                    s.DueDate,
                    //s.Description,
                    s.ProjectId,
                    s.SessionId,
                    s.AddedByUser.UserName,
                    s.StatusCodeId,
                    s.StatusCode.CodeValue,
                    s.AddedDate,
                    s.ModifiedDate,
                    s.Version,
                    s.OnBehalf,
                    s.IsUrgent,
                    s.IsAutoClose,
                    OnBehalfName = s.OnBehalfNavigation.UserName,
                    s.OverDueAllowed,
                    s.FollowUp,
                    s.TaskTag,
                    s.OrderNo,
                    s.IsNoDueDate,
                    s.TaskMembers,
                    s.TaskLinksTask,
                    s.IsNotifyByMessage,
                    s.IsEmail,
                    s.TaskNotes,
                    s.NewDueDate,
                }).Where(t => taskIds.Contains(t.TaskId) && t.StatusCodeId == 512).ToList();
            var taskdocuments = _context.TaskAttachment.Where(f => taskIds.Contains(f.TaskMasterId.Value)).Select(s => new
            {
                s.TaskMasterId,
                s.DocumentId
            }).ToList();

            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            query.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel();
                taskMasterModel.TaskID = s.TaskId;
                taskMasterModel.Title = "Reminder - " + s.Title;
                taskMasterModel.ParentTaskID = s.ParentTaskId;
                taskMasterModel.MainTaskId = s.MainTaskId;
                taskMasterModel.AssignedTo = s.TaskAssigned.Select(t => t.UserId).ToList();
                taskMasterModel.AddedByUserID = s.AddedByUserId;
                taskMasterModel.OwnerID = s.TaskAssigned.FirstOrDefault()?.TaskOwnerId;
                taskMasterModel.DueDate = s.DueDate;
                //taskMasterModel.Description = s.Description;
                taskMasterModel.ProjectID = s.ProjectId;
                taskMasterModel.SessionID = s.SessionId;
                taskMasterModel.AddedByUser = s.UserName;
                //taskMasterModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                taskMasterModel.StatusCodeID = s.StatusCodeId;
                taskMasterModel.AddedDate = s.AddedDate;
                taskMasterModel.ModifiedDate = s.ModifiedDate;
                taskMasterModel.Version = s.Version;
                taskMasterModel.IsRead = s.TaskAssigned.Where(t => t.TaskId == s.TaskId && t.UserId == value.UserId).FirstOrDefault()?.IsRead;
                taskMasterModel.UnreadMessage = taskMessage.Any(r => r.TaskMasterId == s.TaskId);
                taskMasterModel.IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false;
                taskMasterModel.IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false;
                taskMasterModel.Start = s.TaskNotes.Where(w => w.TaskUserId == value.UserId).FirstOrDefault()?.RemainderDate.Value;
                taskMasterModel.End = s.TaskNotes.Where(w => w.TaskUserId == value.UserId).FirstOrDefault()?.RemainderDate.Value;
                taskMasterModel.Id = s.TaskId;
                taskMasterModel.OnBehalfName = s.OnBehalfName;
                taskMasterModel.OnBehalfID = s.OnBehalf;
                taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                taskMasterModel.FollowUp = s.FollowUp;
                taskMasterModel.TagNames = s.TaskTag.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList();
                taskMasterModel.cssClass = "orange";
                taskMasterModel.TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList();
                taskMasterModel.OrderNo = s.OrderNo;
                taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                taskMasterModel.AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList();
                taskMasterModel.TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList();
                taskMasterModel.TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty;
                taskMasterModel.Attachments = taskdocuments.Where(td => td.TaskMasterId.Value == s.TaskId).Select(sa => sa.DocumentId.Value).ToList();
                taskMasterModel.WorkType = "Reminder";
                taskMasterModel.RequestType = "Task Assigned";
                taskMasterModel.IsNotifyByMessage = s.IsNotifyByMessage;
                taskMasterModel.IsEmail = s.IsEmail;
                taskMasterModel.NewDueDate = s.NewDueDate;
                taskMasterModels.Add(taskMasterModel);

            });

            return taskMasterModels.ToList();
        }
        [HttpPost]
        [Route("GetTaskList")]
        public List<TaskMasterModel> GetTaskList(TaskSearchModel value)
        {
            var taskIds = new List<long>();

            var taskMessage = _context.TaskCommentUser.Where(c => c.UserId == value.UserId && c.IsRead == false).AsNoTracking().ToList();

            var query = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation")
                .Include("TaskMembers").Include("TaskTag").Include("StatusCode")
                .Include("TaskLinksTask")
                .Include("TaskAssigned").AsNoTracking().AsQueryable();

            if (value.TaskAssigneeIds.Count > 0)
            {
                value.TaskAssigneeIds.ForEach(id =>
                {
                    var taskId = new List<long>();
                    if (id == 611)
                    {
                        if (value.TaskStatusIds.Count > 0)
                        {
                            taskId = new List<long>();
                            taskId = _context.TaskAssigned.Where(t => (t.TaskOwnerId == value.UserId) && value.TaskStatusIds.Contains(t.StatusCodeId.Value)).Select(s => s.TaskId.Value).Distinct().ToList();
                            taskIds.AddRange(taskId);

                            if (value.TaskStatusIds.Contains(515))
                            {
                                var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512);
                                taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                                taskIds.AddRange(taskId);
                            }
                        }
                        else
                        {
                            taskId = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId).Select(s => s.TaskId.Value).ToList();
                            taskIds.AddRange(taskId);
                        }
                    }
                    else if (id == 612)
                    {
                        taskId = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.StatusCodeId != 511).Select(s => s.TaskId.Value).ToList();
                        taskIds.AddRange(taskId);
                    }
                    else if (id == 615)
                    {
                        if (value.TaskStatusIds.Count > 0)
                        {
                            taskId = _context.TaskAssigned.Where(t => t.UserId == value.UserId && value.TaskStatusIds.Contains(t.StatusCodeId.Value)).Select(s => s.TaskId.Value).ToList();
                            taskIds.AddRange(taskId);

                            if (value.TaskStatusIds.Contains(515))
                            {
                                var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512);
                                taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                                taskIds.AddRange(taskId);
                            }

                        }
                        else
                        {
                            taskId = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.StatusCodeId.Value == 512).Select(s => s.TaskId.Value).ToList();
                            taskIds.AddRange(taskId);
                        }
                    }
                    else
                    {
                        if (value.TaskStatusIds.Count > 0)
                        {
                            taskId = _context.TaskMaster.Where(t => t.OnBehalf == value.UserId && value.TaskStatusIds.Contains(t.StatusCodeId.Value)).Select(s => s.TaskId).ToList();
                            taskIds.AddRange(taskId);
                            if (value.TaskStatusIds.Contains(515))
                            {
                                var taskAssigned = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512);
                                taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                                taskIds.AddRange(taskId);
                            }
                        }
                        else
                        {
                            taskId = _context.TaskMaster.Where(t => t.OnBehalf == value.UserId && t.StatusCodeId != 511).Select(s => s.TaskId).ToList();
                        }
                        taskIds.AddRange(taskId);
                    }
                });
            }
            else
            {
                if (value.TaskStatusIds.Count > 0)
                {
                    value.TaskStatusIds.Remove(511);


                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && value.TaskStatusIds.Contains(t.StatusCodeId.Value));
                    var taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskId);

                    if (value.TaskStatusIds.Contains(517))
                    {
                        var taskMasters = _context.TaskMaster.Where(t => t.StatusCodeId == 517);
                        taskAssigned = _context.TaskAssigned.Where(t => taskMasters.Select(m => m.TaskId).Contains(t.TaskId.Value) && t.UserId == value.UserId);
                        taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                        taskIds.AddRange(taskId);
                    }

                    if (value.TaskStatusIds.Contains(515))
                    {
                        taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512);
                        taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                        taskIds.AddRange(taskId);
                    }
                    //query = query.Where(t => taskIds.Contains(t.TaskID));
                }
                else
                {
                    if (value.TaskAssigneeIds.Count == 0)
                    {
                        var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.StatusCodeId == 512);
                        var taskId = taskAssigned.Select(s => s.TaskId.Value).ToList();
                        taskIds.AddRange(taskId);
                        //query = query.Where(t => taskIds.Contains(t.TaskID));
                    }
                }
            }
            var taskdocuments = _context.TaskAttachment.Where(f => taskIds.Contains(f.TaskMasterId.Value)).Select(s => new
            {
                s.TaskMasterId,
                s.DocumentId
            }).ToList();

            var taskMasterItems = query.OrderByDescending(o => o.TaskId).Where(t => taskIds.Contains(t.TaskId))
                .Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.ParentTaskId,
                    s.MainTaskId,
                    s.TaskAssigned,
                    s.AddedByUserId,
                    s.DueDate,
                    s.Description,
                    s.ProjectId,
                    s.SessionId,
                    s.AddedByUser.UserName,
                    s.StatusCodeId,
                    s.StatusCode.CodeValue,
                    s.AddedDate,
                    s.ModifiedDate,
                    s.Version,
                    s.OnBehalf,
                    s.IsUrgent,
                    s.IsAutoClose,
                    OnBehalfName = s.OnBehalfNavigation.UserName,
                    s.OverDueAllowed,
                    s.FollowUp,
                    s.TaskTag,
                    s.OrderNo,
                    s.IsNoDueDate,
                    s.TaskMembers,
                    s.TaskLinksTask,
                    s.IsNotifyByMessage,
                    s.IsEmail,
                    s.VersionSessionId,
                    s.NewDueDate,
                })
                .ToList();

            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            taskMasterItems.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel();
                taskMasterModel.TaskID = s.TaskId;
                taskMasterModel.Title = s.Title;
                taskMasterModel.ParentTaskID = s.ParentTaskId;
                taskMasterModel.MainTaskId = s.MainTaskId;
                taskMasterModel.AssignedTo = s.TaskAssigned != null ? s.TaskAssigned.Select(t => t.UserId).ToList() : new List<long?>();
                taskMasterModel.AddedByUserID = s.AddedByUserId;
                taskMasterModel.OwnerID = s.TaskAssigned != null ? s.TaskAssigned.FirstOrDefault().TaskOwnerId : null;
                taskMasterModel.DueDate = s.DueDate;
                taskMasterModel.Description = s.Description;
                taskMasterModel.ProjectID = s.ProjectId;
                taskMasterModel.SessionID = s.SessionId;
                taskMasterModel.AddedByUser = s.UserName;
                //taskMasterModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                taskMasterModel.StatusCodeID = s.StatusCodeId;
                taskMasterModel.StatusCode = s.CodeValue;
                taskMasterModel.AddedDate = s.AddedDate;
                taskMasterModel.ModifiedDate = s.ModifiedDate;
                taskMasterModel.Version = s.Version;
                taskMasterModel.IsRead = s.AddedByUserId == value.UserId || s.OnBehalf == value.UserId ? true : s.TaskAssigned.Where(t => t.TaskId == s.TaskId && t.UserId == value.UserId).FirstOrDefault() != null ? s.TaskAssigned.Where(t => t.TaskId == s.TaskId && t.UserId == value.UserId).FirstOrDefault().IsRead : s.TaskMembers.Where(t => t.TaskId == s.TaskId && t.AssignedCc == value.UserId).FirstOrDefault().IsRead;
                taskMasterModel.UnreadMessage = taskMessage.Any(r => r.TaskMasterId == s.TaskId);
                taskMasterModel.IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false;
                taskMasterModel.IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false;
                taskMasterModel.Start = s.DueDate.Value;
                taskMasterModel.End = s.DueDate.Value;
                taskMasterModel.Id = s.TaskId;
                taskMasterModel.OnBehalfName = s.OnBehalfName;
                taskMasterModel.OnBehalfID = s.OnBehalf;
                taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                taskMasterModel.FollowUp = s.FollowUp;
                taskMasterModel.TagNames = s.TaskTag != null ? s.TaskTag.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList() : new List<string>();
                taskMasterModel.cssClass = s.DueDate < DateTime.Today ? "red" : "blue";
                taskMasterModel.TagIds = s.TaskTag != null ? s.TaskTag.Where(t => t.Tag != null).Select(t => t.TagId.Value).ToList() : new List<long>();
                taskMasterModel.OrderNo = s.OrderNo;
                taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                taskMasterModel.AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList();
                taskMasterModel.TaskLinkedIds = s.TaskLinksTask != null ? s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList() : new List<long>();
                taskMasterModel.TaskLinkDescription = s.TaskLinksTask.Count > 0 ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty;
                taskMasterModel.Attachments = taskdocuments.Where(td => td.TaskMasterId.Value == s.TaskId).Select(sa => sa.DocumentId.Value).ToList();

                taskMasterModel.WorkType = s.ParentTaskId == null ? "Task" : "SubTask";
                taskMasterModel.RequestType = "Task Assigned";
                taskMasterModel.StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId) != null ? s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault() : null;
                taskMasterModel.IsNotifyByMessage = s.IsNotifyByMessage;
                taskMasterModel.IsEmail = s.IsEmail;
                taskMasterModel.VersionSessionId = s.VersionSessionId;
                taskMasterModel.NewDueDate = s.NewDueDate;
                var TaskMasterDescriptionVersionLists = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == s.VersionSessionId).ToList();
                List<TaskMasterDescriptionVersionModel> TaskMasterDescriptionVersionModels = new List<TaskMasterDescriptionVersionModel>();
                if (TaskMasterDescriptionVersionLists != null)
                {
                    TaskMasterDescriptionVersionLists.ForEach(h =>
                    {
                        TaskMasterDescriptionVersionModel TaskMasterDescriptionVersionModel = new TaskMasterDescriptionVersionModel();
                        TaskMasterDescriptionVersionModel.TaskMasterDescriptionVersionId = h.TaskMasterDescriptionVersionId;
                        TaskMasterDescriptionVersionModel.VersionNo = h.VersionNo;
                        TaskMasterDescriptionVersionModel.Description = h.Description;
                        TaskMasterDescriptionVersionModel.SessionId = h.SessionId;
                        TaskMasterDescriptionVersionModel.VersionNos = "Version " + h.VersionNo;
                        TaskMasterDescriptionVersionModels.Add(TaskMasterDescriptionVersionModel);
                    });
                }
                taskMasterModel.TaskMasterDescriptionVersionList = TaskMasterDescriptionVersionModels;
                taskMasterModels.Add(taskMasterModel);
            });

            return taskMasterModels.OrderByDescending(t => t.AddedDate).ToList();
        }

        [HttpPost]
        [Route("GetTaskFilter")]
        public List<TaskMasterModel> GetTaskFilter(TaskSearchModel value)
        {

            var taskMessage = _context.TaskCommentUser.Where(c => c.UserId == value.UserId && c.IsRead == false).AsNoTracking().ToList();
            var IsSearch = false;
            var taskIdsList = new List<long?>();
            var query = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation")
                .Include("TaskMembers").Include("TaskTag")
                .Include("TaskLinksTask").Include("TaskAssigned")
                .OrderByDescending(o => o.TaskId).AsNoTracking().AsQueryable();



            if (value.AssignTypeIds > 0)
            {

                if (value.AssignTypeIds == 611)
                {
                    if (value.UserIds.Count > 0)
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && value.UserIds.Contains(t.UserId.Value) && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && value.UserIds.Contains(t.UserId.Value) && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && value.UserIds.Contains(t.UserId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                    else
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }

                }
                else if (value.AssignTypeIds == 612)
                {
                    if (value.UserIds.Count > 0)
                    {
                        var taskIDs = _context.TaskAssigned.Where(t => value.UserIds.Contains(t.TaskOwnerId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                        if (value.StatusIds > 0)
                        {
                            taskIdsList = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.StatusCodeId == value.StatusIds && taskIDs.Contains(t.TaskId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                        else
                        {
                            taskIdsList = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && taskIDs.Contains(t.TaskId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                    else
                    {
                        if (value.StatusIds > 0)
                        {
                            taskIdsList = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                        else
                        {
                            taskIdsList = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                }
                else if (value.AssignTypeIds == 615)
                {
                    if (value.UserIds.Count > 0)
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value) && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value) && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                    else
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                }
                else if (value.AssignTypeIds == 613)
                {
                    if (value.UserIds.Count > 0)
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value) && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value) && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && value.UserIds.Contains(t.TaskOwnerId.Value)).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                    else
                    {
                        if (value.StatusIds > 0)
                        {
                            if (value.StatusIds == 515)
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today))).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                            else
                            {
                                taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && t.StatusCodeId == value.StatusIds).AsNoTracking().Select(s => s.TaskId).ToList();
                                query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                            }
                        }
                        else
                        {
                            taskIdsList = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                            query = query.Where(t => taskIdsList.Contains(t.TaskId)); IsSearch = true;
                        }
                    }
                }
            }
            else
            {
                if (value.StatusIds > 0)
                {
                    taskIdsList = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                    if (value.StatusIds != 511 && value.StatusIds != 515)
                    {
                        query = query.Where(t => t.StatusCodeId == value.StatusIds && taskIdsList.Contains(t.TaskId));
                    }
                    if (value.StatusIds == 515)
                    {
                        query = query.Where(t => ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && taskIdsList.Contains(t.TaskId) && t.StatusCodeId == 512);
                    }
                    IsSearch = true;
                }
            }

            if (!string.IsNullOrEmpty(value.Title))
            {
                var titles = value.Title.Split("@").ToList();
                var listofIds = new List<long>();
                if (taskIdsList.Count == 0)
                {
                    var taskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIdsList.AddRange(taskIds);
                }
                titles.ForEach(tit =>
                {
                    var titleID = _context.TaskMaster.Where(t => t.Title.Contains(tit) && taskIdsList.Contains(t.TaskId)).AsNoTracking().Select(s => s.TaskId).ToList();
                    listofIds.AddRange(titleID);
                });
                query = query.Where(t => listofIds.Contains(t.TaskId)); IsSearch = true;

            }

            if (value.IsEmail == true)
            {

                query = query.Where(t => t.IsEmail == true); IsSearch = true;

            }
            if (value.ProjectIds > 0)
            {
                if (taskIdsList.Count == 0)
                {
                    var taskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIdsList.AddRange(taskIds);
                }
                query = query.Where(t => t.ProjectId == value.ProjectIds && taskIdsList.Contains(t.TaskId)); IsSearch = true;
            }
            if (value.WorkTypeIds > 0)
            {
                var taskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                query = query.Where(t => t.WorkTypeId == value.WorkTypeIds && taskIds.Contains(t.TaskId)); IsSearch = true;
            }
            if (value.RequestTypeIds > 0)
            {
                var taskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                query = query.Where(t => t.RequestTypeId == value.RequestTypeIds && taskIds.Contains(t.TaskId)); IsSearch = true;
            }
            if (value.TagIds.Count() > 0)
            {
                if (taskIdsList.Count == 0)
                {
                    var ustaskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId || t.TaskOwnerId == value.UserId || t.OnBehalfId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIdsList.AddRange(ustaskIds);
                }
                var taskIds = _context.TaskTag.Where(T => value.TagIds.Contains(T.TagId.Value) && taskIdsList.Contains(T.TaskMasterId)).AsNoTracking().Select(s => s.TaskMasterId).ToList();
                //ustaskIds.AddRange(taskIds);
                query = query.Where(t => taskIds.Contains(t.TaskId)); IsSearch = true;
            }
            if (value.PersonaltagIds.Count() > 0)
            {
                if (taskIdsList.Count == 0)
                {
                    var ustaskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId || t.TaskOwnerId == value.UserId || t.OnBehalfId == value.UserId).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIdsList.AddRange(ustaskIds);
                }
                var taskIds = _context.PersonalTags.Where(T => value.PersonaltagIds.Contains(T.Tag) && taskIdsList.Contains(T.TaskId)).AsNoTracking().Select(s => s.TaskId).ToList();

                query = query.Where(t => taskIds.Contains(t.TaskId)); IsSearch = true;
            }
            if (value.UnreadSubject)
            {
                var querycommentTaskIds = _context.TaskCommentUser.Where(c => c.UserId == value.UserId && c.IsRead == false).AsNoTracking().Select(s => s.TaskCommentId).ToList();
                var taskcommentby = _context.TaskComment.Where(w => w.CommentedBy != value.UserId && querycommentTaskIds.Contains(w.TaskCommentId)).AsNoTracking().Select(s => s.TaskMasterId).ToList();
                query = query.Where(t => taskcommentby.Contains(t.TaskId));
                IsSearch = true;
            }
            if (!IsSearch)
            {
                var taskIds = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).ToList();
                query = query.Where(t => taskIds.Contains(t.TaskId));
            }

            if (value.IsBefore == true)
            {
                value.CreatedToDate = null;
                if (value.AddedDate != null)
                {
                    query = query.Where(s => s.AddedDate <= value.AddedDate);
                }
            }
            else
            {
                if (value.AddedDate != null && value.CreatedToDate != null)
                {
                    query = query.Where(s => s.AddedDate >= value.AddedDate && s.AddedDate <= value.CreatedToDate);
                }
                else if (value.AddedDate != null && value.CreatedToDate == null)
                {
                    query = query.Where(s => s.AddedDate >= value.AddedDate);
                }
            }

            var taskMasterItems = query.Include(q => q.TaskAssigned).Include(q => q.TaskTag).Include(t => t.TaskMembers).Include(t => t.TaskLinksTask).Select(s => new
            {
                s.TaskId,
                s.Title,
                s.ParentTaskId,
                s.MainTaskId,
                s.TaskAssigned,
                s.AddedByUserId,
                s.DueDate,
                s.Description,
                s.ProjectId,
                s.SessionId,
                s.AddedByUser.UserName,
                s.StatusCodeId,
                s.StatusCode.CodeValue,
                s.AddedDate,
                s.ModifiedDate,
                s.Version,
                s.OnBehalf,
                s.IsUrgent,
                s.IsAutoClose,
                OnBehalfName = s.OnBehalfNavigation.UserName,
                s.OverDueAllowed,
                s.FollowUp,
                s.TaskTag,
                s.OrderNo,
                s.IsNoDueDate,
                s.TaskMembers,
                s.TaskLinksTask,
                s.IsNotifyByMessage,
                s.IsEmail,
                s.VersionSessionId,
                s.NewDueDate,
            }).ToList();
            var doctaskIds = taskMasterItems.Select(s => s.TaskId).ToList();
            var taskdocuments = _context.TaskAttachment.Where(f => doctaskIds.Contains(f.TaskMasterId.Value)).Select(s => new
            {
                s.TaskMasterId,
                s.DocumentId
            }).ToList();

            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            if (taskMasterItems != null && taskMasterItems.Count > 0)
            {
                taskMasterItems.ForEach(s =>
                {
                    TaskMasterModel taskMasterModel = new TaskMasterModel();
                    taskMasterModel.TaskID = s.TaskId;
                    taskMasterModel.Title = s.Title;
                    taskMasterModel.ParentTaskID = s.ParentTaskId;
                    taskMasterModel.MainTaskId = s.MainTaskId;
                    taskMasterModel.AssignedTo = s.TaskAssigned?.Select(t => t.UserId).ToList();
                    taskMasterModel.AddedByUserID = s.AddedByUserId;
                    taskMasterModel.OwnerID = s.TaskAssigned?.FirstOrDefault().TaskOwnerId;
                    taskMasterModel.DueDate = s.DueDate;
                    taskMasterModel.Description = s.Description;
                    taskMasterModel.ProjectID = s.ProjectId;
                    taskMasterModel.SessionID = s.SessionId;
                    taskMasterModel.AddedByUser = s.UserName;
                    //ModifiedByUser = s.ModifiedByUser?.UserName,
                    taskMasterModel.StatusCodeID = s.StatusCodeId;
                    taskMasterModel.AddedDate = s.AddedDate;
                    taskMasterModel.ModifiedDate = s.ModifiedDate;
                    if (value.UserId > 0)
                    {
                        taskMasterModel.IsRead = s.TaskAssigned?.Where(t => t.TaskId == s.TaskId && t.UserId == value.UserId).FirstOrDefault()?.IsRead;
                    }
                    taskMasterModel.UnreadMessage = taskMessage.Any(r => r.TaskMasterId == s.TaskId);
                    taskMasterModel.IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false;
                    taskMasterModel.IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false;
                    taskMasterModel.Start = s.DueDate.Value;
                    taskMasterModel.End = s.DueDate.Value;
                    taskMasterModel.Id = s.TaskId;
                    taskMasterModel.OnBehalfName = s.OnBehalfName;
                    taskMasterModel.OnBehalfID = s.OnBehalf;
                    taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                    taskMasterModel.FollowUp = s.FollowUp;
                    taskMasterModel.TagNames = s.TaskTag?.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList();
                    taskMasterModel.cssClass = s.DueDate < DateTime.Today ? "red" : "blue";
                    taskMasterModel.TagIds = s.TaskTag?.Select(t => t.TagId.Value).ToList();
                    taskMasterModel.OrderNo = s.OrderNo;
                    taskMasterModel.AssignedCC = s.TaskMembers?.Select(c => c.AssignedCc).ToList();
                    taskMasterModel.TaskLinkedIds = s.TaskLinksTask?.Select(t => t.LinkedTaskId.Value).ToList();
                    taskMasterModel.TaskLinkDescription = s.TaskLinksTask?.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty;
                    taskMasterModel.Attachments = taskdocuments.Where(td => td.TaskMasterId.Value == s.TaskId).Select(sa => sa.DocumentId.Value).ToList();
                    // WorkType = "Task",
                    taskMasterModel.WorkType = s.ParentTaskId == null ? "Task" : "SubTask";
                    taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                    taskMasterModel.RequestType = "Task Assigned";
                    taskMasterModel.StartDate = s.TaskAssigned?.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault();
                    taskMasterModel.IsNotifyByMessage = s.IsNotifyByMessage;
                    taskMasterModel.IsEmail = s.IsEmail;
                    taskMasterModel.VersionSessionId = s.VersionSessionId;
                    taskMasterModel.NewDueDate = s.NewDueDate;
                    var TaskMasterDescriptionVersionLists = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == s.VersionSessionId).ToList();
                    List<TaskMasterDescriptionVersionModel> TaskMasterDescriptionVersionModels = new List<TaskMasterDescriptionVersionModel>();
                    if (TaskMasterDescriptionVersionLists != null)
                    {
                        TaskMasterDescriptionVersionLists.ForEach(h =>
                        {
                            TaskMasterDescriptionVersionModel TaskMasterDescriptionVersionModel = new TaskMasterDescriptionVersionModel();
                            TaskMasterDescriptionVersionModel.TaskMasterDescriptionVersionId = h.TaskMasterDescriptionVersionId;
                            TaskMasterDescriptionVersionModel.VersionNo = h.VersionNo;
                            TaskMasterDescriptionVersionModel.Description = h.Description;
                            TaskMasterDescriptionVersionModel.SessionId = h.SessionId;
                            TaskMasterDescriptionVersionModel.VersionNos = "Version " + h.VersionNo;
                            TaskMasterDescriptionVersionModels.Add(TaskMasterDescriptionVersionModel);
                        });
                    }
                    taskMasterModel.TaskMasterDescriptionVersionList = TaskMasterDescriptionVersionModels;
                    taskMasterModels.Add(taskMasterModel);
                });
            }
            return taskMasterModels;
        }

        [HttpPost]
        [Route("GetDashTaskList")]
        public List<TaskMasterModel> GetDashTaskList(TaskSearchModel value)
        {
            var taskIds = new List<long>();
            var taskMessage = _context.TaskCommentUser.Where(c => c.UserId == value.UserId && c.IsRead == false).AsNoTracking().ToList();

            var query = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation")
                .Include("TaskMembers").Include("TaskTag")
                .Include("TaskLinksTask").Include("TaskAssigned")
                .OrderByDescending(o => o.TaskId).AsNoTracking().AsQueryable();


            int days = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            DateTime weekStart = DateTime.Today.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(6);

            if (value.TaskType == "MyTask")
            {
                if (value.BarType == "Unread")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.IsRead == false && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "UnreadEmail")
                {
                    var taskmasterIds = _context.TaskMaster.Where(w => w.IsEmail == true).Select(s => s.TaskId).ToList();

                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.IsRead == false && t.StatusCodeId == 512 && taskmasterIds.Contains(t.TaskId.Value)).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "Active")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "OverDue")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "1 Week")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate != null && t.NewDueDate <= weekEnd && t.NewDueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "2 Weeks")
                {
                    weekEnd = weekStart.AddDays(13);
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "Month")
                {
                    weekEnd = weekStart.AddDays(29);
                    var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else
                {
                    var assignedto = _context.TaskAssigned
              .Where(t => t.UserId == value.UserId && (t.StatusCodeId == 512 || t.StatusCodeId == 515)).AsNoTracking().Select(s => s.TaskId).ToList();
                    var taskAssigned = _context.TaskNotes.Where(t => t.TaskUserId == value.UserId && assignedto.Contains(t.TaskId)).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
            }
            else if (value.TaskType == "AssignedbyMe")
            {
                if (value.BarType == "Unread")
                {
                    var taskAssigned = _context.TaskMaster.Where(t => t.AddedByUserId == value.UserId && t.IsRead == false && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "UnreadEmail")
                {
                    var taskAssigned = _context.TaskMaster.Where(t => t.AddedByUserId == value.UserId && t.IsEmail == true && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "Active")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "OverDue")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "1 Week")
                {
                    var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate != null && t.NewDueDate <= weekEnd && t.NewDueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "2 Weeks")
                {
                    weekEnd = weekStart.AddDays(13);
                    var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate != null && t.NewDueDate <= weekEnd && t.NewDueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else if (value.BarType == "Month")
                {
                    weekEnd = weekStart.AddDays(29);
                    var taskAssigned = _context.TaskAssigned.Where(t => t.TaskOwnerId == value.UserId && ((t.NewDueDate == null && t.DueDate <= weekEnd && t.DueDate >= weekStart) || (t.NewDueDate != null && t.NewDueDate <= weekEnd && t.NewDueDate >= weekStart)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
                else
                {
                    var assignedto = _context.TaskAssigned
              .Where(t => t.TaskOwnerId == value.UserId && (t.StatusCodeId == 512 || t.StatusCodeId == 515)).AsNoTracking().Select(s => s.TaskId).ToList();
                    var taskAssigned = _context.TaskNotes.Where(t => t.TaskUserId == value.UserId && assignedto.Contains(t.TaskId)).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskAssigned);
                }
            }
            else if (value.TaskType == "OnBehalf")
            {
                if (value.BarType == "Unread")
                {
                    var taskcc = _context.TaskMaster.Where(t => t.OnBehalf == value.UserId && t.IsRead == false && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).ToList();


                    taskIds.AddRange(taskcc);
                }
                else if (value.BarType == "UnreadEmail")
                {
                    var taskcc = _context.TaskMaster.Where(w => w.IsEmail == true && w.StatusCodeId == 512 && w.OnBehalf == value.UserId && w.IsRead == false).AsNoTracking().Select(s => s.TaskId).ToList();
                    //var taskcc = _context.TaskAssigned.Where(t => t.OnBehalfId == value.UserId && t.IsRead == false && taskmasterIds.Contains(t.TaskId.Value) && t.StatusCodeId == 512).Select(s => s.TaskId.Value).ToList();


                    taskIds.AddRange(taskcc);
                }
            }
            else
            {
                if (value.BarType == "Unread")
                {
                    var taskcc = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.IsRead == false && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();

                    //var taskAssigned = _context.TaskAssigned.Where(t => taskcc.Contains(t.TaskId.Value) && t.IsRead == false).Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskcc);
                }
                else if (value.BarType == "UnreadEmail")
                {
                    var taskmasterIds = _context.TaskMaster.Where(w => w.IsEmail == true).AsNoTracking().Select(s => s.TaskId).ToList();
                    var taskcc = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.IsRead == false && t.StatusCodeId == 512 && taskmasterIds.Contains(t.TaskId.Value)).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskcc);
                }
                else if (value.BarType == "Active")
                {
                    var taskcc = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                    // var taskAssigned = _context.TaskAssigned.Where(t => taskcc.Contains(t.TaskId.Value) && t.StatusCodeId == 512).Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskcc);
                }
                else
                {
                    var taskcc = _context.TaskMembers.Where(t => t.AssignedCc == value.UserId && ((t.NewDueDate == null && t.DueDate < DateTime.Today) || (t.NewDueDate != null && t.NewDueDate < DateTime.Today)) && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();

                    //var taskAssigned = _context.TaskAssigned.Where(t => taskcc.Contains(t.TaskId.Value) && t.DueDate < DateTime.Today).Select(s => s.TaskId.Value).ToList();
                    taskIds.AddRange(taskcc);
                }
            }
            var taskMasterItems = query.Where(t => taskIds.Contains(t.TaskId))
                .Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.ParentTaskId,
                    s.MainTaskId,
                    s.TaskAssigned,
                    s.AddedByUserId,
                    s.DueDate,
                    s.Description,
                    s.ProjectId,
                    s.SessionId,
                    s.AddedByUser.UserName,
                    s.StatusCodeId,
                    s.StatusCode.CodeValue,
                    s.AddedDate,
                    s.ModifiedDate,
                    s.Version,
                    s.OnBehalf,
                    s.IsUrgent,
                    s.IsAutoClose,
                    OnBehalfName = s.OnBehalfNavigation.UserName,
                    s.OverDueAllowed,
                    s.FollowUp,
                    s.TaskTag,
                    s.OrderNo,
                    s.IsNoDueDate,
                    s.TaskMembers,
                    s.TaskLinksTask,
                    s.IsNotifyByMessage,
                    s.IsEmail,
                    s.NewDueDate
                }).ToList();
            var taskdocuments = _context.TaskAttachment.Where(f => taskIds.Contains(f.TaskMasterId.Value)).Select(s => new
            {
                s.TaskMasterId,
                s.DocumentId
            }).ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            taskMasterItems.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Title = s.Title,
                    ParentTaskID = s.ParentTaskId,
                    MainTaskId = s.MainTaskId,
                    AssignedTo = s.TaskAssigned.Select(t => t.UserId).ToList(),
                    AddedByUserID = s.AddedByUserId,
                    OwnerID = s.TaskAssigned.FirstOrDefault().TaskOwnerId,
                    DueDate = s.DueDate,
                    Description = s.Description,
                    ProjectID = s.ProjectId,
                    SessionID = s.SessionId,
                    AddedByUser = s.UserName,
                    //ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    //PersonaltagIds = _context.PersonalTags.Where(pt => pt.TaskId == s.TaskId).Select(ps => ps.Tag).ToList(),
                    Version = s.Version,
                    //WorkTypeID = s.WorkTypeId.Value,
                    //RequestTypeID = s.RequestTypeId.Value,
                    IsRead = value.BarType == "Unread" || value.BarType == "UnreadEmail" ? false : s.AddedByUserId == value.UserId || s.OnBehalf == value.UserId ? true : s.TaskAssigned.Where(t => t.TaskId == s.TaskId && t.UserId == value.UserId).FirstOrDefault().IsRead,
                    UnreadMessage = taskMessage.Any(r => r.TaskMasterId == s.TaskId),
                    IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false,
                    IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose : false,
                    Start = s.DueDate.Value,
                    End = s.DueDate.Value,
                    Id = s.TaskId,
                    OnBehalfName = s.OnBehalfName,
                    OnBehalfID = s.OnBehalf,
                    OverDueAllowed = s.OverDueAllowed,
                    FollowUp = s.FollowUp,
                    TagNames = s.TaskTag.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList(),
                    cssClass = s.NewDueDate == null && s.DueDate < DateTime.Today ? "red" : s.NewDueDate != null && s.NewDueDate < DateTime.Today ? "red" : "blue",
                    TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList(),
                    OrderNo = s.OrderNo,
                    IsNoDueDate = s.IsNoDueDate,
                    AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList(),
                    TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                    Attachments = taskdocuments.Where(td => td.TaskMasterId.Value == s.TaskId).Select(sa => sa.DocumentId.Value).ToList(),
                    // WorkType = "Task",
                    WorkType = s.ParentTaskId == null ? "Task" : "SubTask",
                    RequestType = "Task Assigned",
                    IsNotifyByMessage = s.IsNotifyByMessage,
                    IsEmail = s.IsEmail,
                    NewDueDate = s.NewDueDate,

                };

                taskMasterModels.Add(taskMasterModel);
            });
            return taskMasterModels;
        }

        [HttpGet]
        [Route("GetTaskByStatus")]
        public List<TaskMasterModel> GetTaskByStatus(int id, int userId)
        {
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskAssigned = _context.TaskAssigned.Where(t => t.UserId == userId && t.StatusCodeId == id).AsNoTracking().ToList();
            var taskIds = taskAssigned.Select(s => s.TaskId).ToList();

            var taskMaster = _context.TaskMaster
                                .Include("AddedByUser")
                                .Include("TaskAssigned")
                                .Include("TaskAttachment")
                                .Include("TaskMembers")
                                .Include("TaskTag")
                                .Include("TaskLinksTask")
                                .Include("ModifiedByUser")
                                .Include("OnBehalfNavigation")
                                .OrderByDescending(o => o.TaskId).Where(t => taskIds.Contains(t.TaskId)).AsNoTracking().ToList();
            taskMaster.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Title = s.Title,
                    ParentTaskID = s.ParentTaskId,
                    MainTaskId = s.MainTaskId,
                    AssignedTo = s.TaskAssigned.Select(c => c.UserId).ToList(),
                    AddedByUserID = s.AddedByUserId,
                    OwnerID = s.TaskAssigned.FirstOrDefault().TaskOwnerId,
                    DueDate = s.DueDate,
                    Description = s.Description,
                    ProjectID = s.ProjectId,
                    SessionID = s.SessionId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsRead = s.IsRead.HasValue ? s.IsRead.Value : false,
                    IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false,
                    IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false,
                    Start = s.DueDate.Value,
                    End = s.DueDate.Value,
                    Id = s.TaskId,
                    cssClass = s.AssignedTo == userId ? s.DueDate < DateTime.Today ? s.StatusCodeId == 512 ? "red" : "blue" : "green" : "blue",
                    TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList(),
                    OnBehalfName = s.OnBehalfNavigation.UserName,
                    OnBehalfID = s.OnBehalf,
                    OverDueAllowed = s.OverDueAllowed,
                    FollowUp = s.FollowUp,
                    TagNames = s.TaskTag.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList(),
                    AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList(),
                    OrderNo = s.OrderNo,
                    IsNoDueDate = s.IsNoDueDate,
                    TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                    Attachments = s.TaskAttachment.Select(sa => sa.DocumentId.Value).ToList(),
                    // WorkType = "Task",
                    WorkType = s.ParentTaskId == null ? "Task" : "SubTask",
                    RequestType = "Task Assigned",
                    StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault(),
                    IsNotifyByMessage = s.IsNotifyByMessage,
                    IsEmail = s.IsEmail,
                    NewDueDate = s.NewDueDate,
                };
                taskMasterModels.Add(taskMasterModel);
            });


            return taskMasterModels;
        }
        [HttpGet]
        [Route("GetTaskLinks")]
        public List<TaskMasterModel> GetTaskLinks()
        {
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskLink = _context.TaskLinks.Include(t => t.Task).Include(t => t.Task.TaskLinksTask).OrderByDescending(o => o.TaskId).AsNoTracking().ToList();
            taskLink.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskLinkID = s.TaskLinkId,
                    TaskID = s.TaskId.Value,
                    TaskLinkedIds = s.Task.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList(),
                    TaskLinkDescription = s.Task.TaskLinksTask.FirstOrDefault() != null ? s.Task.TaskLinksTask.FirstOrDefault().Description : string.Empty,
                };
                taskMasterModels.Add(taskMasterModel);
            });
            return taskMasterModels;
        }
        // GET: api/Project/2
        [HttpGet("GetTaskMastersbyID")]
        public ActionResult<TaskMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == id.Value);
            var result = _mapper.Map<TaskMasterModel>(taskMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        // GET: api/Project/2
        [HttpGet("UpadateRead")]
        public ActionResult<TaskMaster> UpadateRead(int id, int userId)
        {
            if (id <= 0)
            {
                return NotFound();
            }
            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == id);
            if (taskMaster != null)
            {
                if (taskMaster.OnBehalf.HasValue && taskMaster.OnBehalf.Value == userId)
                {
                    taskMaster.IsRead = true;
                }

                //if (userId != taskMaster.AddedByUserId)
                //    taskMaster.IsRead = true;
                //else if (taskMaster.OnBehalf.HasValue && taskMaster.OnBehalf.Value == userId)
                //    taskMaster.IsRead = true;

                bool assignedorCC = false;
                var taskassigned = _context.TaskAssigned.FirstOrDefault(a => a.UserId == userId && a.TaskId == id);
                if (taskassigned != null)
                {
                    assignedorCC = true;
                    taskassigned.IsRead = true;

                    if (taskassigned.UserId == taskassigned.TaskOwnerId)
                    {
                        taskMaster.IsRead = true;
                    }
                }
                taskassigned = _context.TaskAssigned.FirstOrDefault(a => a.OnBehalfId == userId && a.TaskId == id);
                if (taskassigned != null)
                {
                    assignedorCC = true;
                    taskassigned.IsRead = true;
                }
                var taskCC = _context.TaskMembers.FirstOrDefault(a => a.AssignedCc == userId && a.TaskId == id);
                if (taskCC != null)
                {
                    assignedorCC = true;
                    taskCC.IsRead = true;
                }
                //if (!assignedorCC)
                //{
                //    taskMaster.IsRead = true;
                //}
                _context.SaveChanges();
            }
            return taskMaster;
        }
        [HttpGet("UpadateAsUnRead")]
        public ActionResult<TaskMaster> UpadateAsUnRead(int id, int userId)
        {
            if (id <= 0)
            {
                return NotFound();
            }
            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == id);
            if (taskMaster != null)
            {
                if (taskMaster.OnBehalf.HasValue && taskMaster.OnBehalf.Value == userId)
                {
                    taskMaster.IsRead = true;
                }
                //if (userId != taskMaster.AddedByUserId)
                //    taskMaster.IsRead = true;
                //else if (taskMaster.OnBehalf.HasValue && taskMaster.OnBehalf.Value == userId)
                //    taskMaster.IsRead = true;
                bool assignedorCC = false;
                var taskassigned = _context.TaskAssigned.FirstOrDefault(a => a.UserId == userId && a.TaskId == id);
                if (taskassigned != null)
                {
                    assignedorCC = true;
                    taskassigned.IsRead = false;

                    if (taskassigned.UserId == taskassigned.TaskOwnerId)
                    {
                        taskMaster.IsRead = false;
                    }
                }
                var taskCC = _context.TaskMembers.FirstOrDefault(a => a.AssignedCc == userId && a.TaskId == id);
                if (taskCC != null)
                {
                    assignedorCC = true;
                    taskCC.IsRead = false;
                }
                //if (!assignedorCC)
                //{
                //    taskMaster.IsRead = false;
                //}
                _context.SaveChanges();
            }
            return taskMaster;
        }
        [HttpGet]
        [Route("GetTaskFiles")]
        public List<DocumentsModel> GetTaskFiles(int id, int userId)
        {
            var taskIds = new List<long>();
            var taskmaster = _context.TaskMaster.Select(s => new { s.MainTaskId, s.TaskId, s.ParentTaskId, s.Title, s.SessionId }).AsNoTracking().ToList();
            var maintaskid = taskmaster?.Select(s => new { s.MainTaskId, s.TaskId, s.ParentTaskId }).Where(f => f.TaskId == id).FirstOrDefault();
            if (maintaskid != null)
            {
                var tasks = taskmaster.Where(i => i.MainTaskId != null && i.MainTaskId == maintaskid.MainTaskId || i.TaskId == maintaskid.TaskId).ToList();
                taskIds = tasks.Select(t => t.TaskId).ToList();

                var taskAttachments = _context.TaskAttachment
                                        .Where(d => taskIds.Contains(d.TaskMasterId.Value) && d.IsLatest.Value == true)
                                        .Select(s => new
                                        {
                                            s.DocumentId,
                                            s.TaskComment,
                                            s.CheckInDescription,
                                            s.TaskMaster.Title,
                                            s.TaskMasterId,
                                            s.TaskMaster.ParentTaskId,
                                            s.UploadedDate,
                                            s.TaskAttachmentId,
                                            s.VersionNo,
                                            s.IsLocked,
                                            s.IsMajorChange,
                                            s.LockedByUserId,
                                            s.LockedDate,
                                            s.IsMeetingNotes,
                                            s.IsDiscussionNotes,
                                            s.IsSubTask,
                                            s.PreviousDocumentId,
                                            s.UploadedByUser.UserName,
                                            s.UploadedByUserId,
                                            s.ActualVersionNo,
                                            s.IsNoChange,
                                            s.DraftingVersionNo,
                                            s.IsReleaseVersion,
                                            s.FolderId,
                                            s.FileProfieTypeId

                                        }).AsNoTracking().ToList();
                var lockedbyUserId = taskAttachments.Select(s => s.LockedByUserId).Where(f => f.HasValue).ToList();
                var taskAttachmentdocumentid = taskAttachments.Where(c => c.DocumentId != null).Select(s => s.DocumentId).ToList();
                var taskSessionId = tasks.Select(s => s.SessionId).ToList();
                //var linkProfileDocument = _context.LinkFileProfileTypeDocument.Where(s => taskSessionId.Contains(s.TransactionSessionId)).Select(l => new { l.DocumentId, l.AddedDate, l.AddedByUserId, l.FileProfileTypeId, l.TransactionSessionId, l.FolderId }).Distinct().ToList();
                //var linkProfileDocumentIds = linkProfileDocument.Select(l => l.DocumentId).Distinct().ToList();
                //var documentsIdsList = taskAttachmentdocumentid;
                //documentsIdsList.AddRange(linkProfileDocumentIds);
                List<long?> linkProfileDocumentIds = new List<long?>();
                var documents = _context.Documents.
                Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.ScreenId,
                    s.ProfileNo,
                    s.IsLatest,
                    s.IsCompressed,
                    s.IsVideoFile,
                    s.CloseDocumentId,
                    s.FileIndex,
                    s.IsPublichFolder,
                    s.FolderId,
                    s.IsMainTask,
                }).Where(w => taskAttachmentdocumentid.Contains(w.DocumentId)).AsNoTracking().ToList();
                var applicationUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,

                }).AsNoTracking().ToList();
                var fileProfileType = _context.FileProfileType.Select(s => new
                {
                    s.FileProfileTypeId,
                    s.Name,
                }).AsNoTracking().ToList();
                var folders = _context.Folders.Select(s => new
                {
                    s.FolderId,
                    s.Name,
                    s.FolderTypeId
                }).Where(d => d.FolderTypeId == 524).AsNoTracking().ToList();

                var documentRights = _context.DocumentRights.Where(d => taskAttachmentdocumentid.Contains(d.DocumentId)).AsNoTracking().ToList();
                List<DocumentsModel> documentsModels = new List<DocumentsModel>();

                if (documents != null && documents.Count > 0)
                {

                    var tasktitle = taskmaster?.FirstOrDefault(t => t.TaskId == id)?.Title;
                    var selTaskid = new long?();
                    var description = "";
                    if (taskSessionId != null && taskSessionId.Count > 0)
                    {
                        var documentids = documents.Where(d => taskAttachmentdocumentid.Contains(d.DocumentId)).Select(d => d.DocumentId).Distinct().ToList();

                        if (linkProfileDocumentIds.Count > 0 || documentids.Count > 0)
                        {
                            var documentRightsNew = _context.DocumentRights.Where(t => linkProfileDocumentIds.Contains(t.DocumentId) || documentids.Contains(t.DocumentId.Value)).AsNoTracking().ToList();

                            var linkProfiledocuments = documents.Where(t => linkProfileDocumentIds.Contains(t.DocumentId) || documentids.Contains(t.DocumentId)).ToList();
                            var documentIds = linkProfiledocuments?.Select(d => d.DocumentId).Distinct().ToList();
                            if (linkProfiledocuments != null && linkProfiledocuments.Count > 0)
                            {
                                linkProfiledocuments.Where(d => documentIds.Contains(d.DocumentId)).ToList().ForEach(l =>
                                 {
                                     DocumentsModel documentsModel = new DocumentsModel();
                                     if (taskAttachmentdocumentid.Contains(l.DocumentId))
                                     {

                                         var fileProfieTypeIdAtta = taskAttachments.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.FileProfieTypeId;
                                         var fileProfieTypeNameAtta = fileProfieTypeIdAtta != null ? fileProfileType.FirstOrDefault(a => a.FileProfileTypeId == fileProfieTypeIdAtta)?.Name : null;
                                         var folderIdAtta = taskAttachments.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.FolderId;
                                         var folderNameAtta = folderIdAtta != null ? folders.FirstOrDefault(a => a.FolderId == folderIdAtta)?.Name : null;
                                         selTaskid = taskAttachments.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.TaskMasterId;
                                         tasktitle = taskmaster?.FirstOrDefault(t => t.TaskId == selTaskid)?.Title;
                                         description = taskAttachments.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.CheckInDescription;
                                         documentsModel.FilterProfileTypeId = fileProfileType?.FirstOrDefault(f => f.FileProfileTypeId == (documents.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.FilterProfileTypeId))?.FileProfileTypeId;
                                         documentsModel.FolderId = folders?.FirstOrDefault(f => f.FolderId == (documents.FirstOrDefault(d => d.DocumentId == l.DocumentId)?.FolderId))?.FolderId;
                                         documentsModel.FileProfileTypeName = fileProfileType?.FirstOrDefault(f => f.FileProfileTypeId == (documents.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.FilterProfileTypeId))?.Name;
                                         documentsModel.FolderName = folders?.FirstOrDefault(f => f.FolderId == (documents.FirstOrDefault(d => d.DocumentId == l.DocumentId)?.FolderId))?.Name;
                                         if (documentsModel.FilterProfileTypeId == null)
                                         {
                                             documentsModel.FilterProfileTypeId = fileProfieTypeIdAtta;
                                             documentsModel.FileProfileTypeName = fileProfieTypeNameAtta;
                                         }
                                         if (documentsModel.FolderId == null)
                                         {
                                             documentsModel.FolderId = folderIdAtta;
                                             documentsModel.FolderName = folderNameAtta;
                                         }
                                     }
                                     else if (linkProfileDocumentIds.Contains(l.DocumentId))
                                     {

                                         /* selTaskid = tasks?.Where(a => a.SessionId == linkProfileDocument.FirstOrDefault(p => p.DocumentId == l.DocumentId)?.TransactionSessionId)?.FirstOrDefault()?.TaskId;
                                          tasktitle = taskmaster?.FirstOrDefault(t => t.TaskId == selTaskid)?.Title;
                                          description = documents.FirstOrDefault(a => a.DocumentId == l.DocumentId)?.Description;
                                          documentsModel.FileProfileTypeName = fileProfileType?.FirstOrDefault(f => f.FileProfileTypeId == (linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.FileProfileTypeId))?.Name;
                                          documentsModel.FolderName = folders?.FirstOrDefault(f => f.FolderId == (linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.FolderId))?.Name;*/
                                     }
                                     var seldocument = documents.FirstOrDefault(d => d.DocumentId == l.DocumentId);
                                     var fileName = seldocument?.FileName.Split('.');

                                     documentsModel.DocumentType = l.DocumentType;

                                     documentsModel.CategoryID = l.CategoryId;
                                     documentsModel.ContentType = l.ContentType;
                                     documentsModel.DepartmentID = l.DepartmentId;
                                     // documentsModel.Description = l.Description;
                                     documentsModel.DisplayName = l.DisplayName;
                                     documentsModel.DocumentID = l.DocumentId;
                                     documentsModel.Extension = l.Extension;
                                     documentsModel.FileName = l.FileIndex > 0 ? fileName[0] + "_V0" + l.FileIndex + "." + fileName[1] : l.FileName;
                                     documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(l.FileSize / 1024));
                                     documentsModel.ReferenceNumber = l.ReferenceNumber;
                                     documentsModel.SessionID = l.SessionId;
                                     documentsModel.IsVideoFile = l.IsVideoFile;
                                     if (documentsModel.FilterProfileTypeId == null)
                                     {
                                         documentsModel.FilterProfileTypeId = l.FilterProfileTypeId;
                                     }
                                     if (documentsModel.FolderId == null)
                                     {
                                         documentsModel.FolderId = l.FolderId;
                                     }
                                     // documentsModel.FilterProfileTypeId = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.FileProfileTypeId : l.FilterProfileTypeId;
                                     documentsModel.ProfileNo = l.ProfileNo;
                                     documentsModel.IsLatest = l.IsLatest;
                                     documentsModel.LockedDate = l.LockedDate;
                                     documentsModel.UploadDate = l.UploadDate;
                                     documentsModel.CheckInDescription = description;
                                     documentsModel.ExpiryDate = l.ExpiryDate;
                                     documentsModel.IsLocked = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid)?.Select(s => s.IsLocked)?.FirstOrDefault();
                                     documentsModel.LockedByUserId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid)?.Select(s => s.LockedByUserId)?.FirstOrDefault();
                                     documentsModel.Type = "Document";
                                     documentsModel.AddedByUserID = l.AddedByUserId;
                                     //documentsModel.AddedByUserID = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.AddedByUserId : l.AddedByUserId;
                                     documentsModel.LinkedTask = tasktitle;
                                     documentsModel.LinkID = selTaskid;
                                     documentsModel.PreviousDocumentId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid).Select(s => s.PreviousDocumentId)?.FirstOrDefault();
                                     documentsModel.UploadedByUserId = l.AddedByUserId;
                                     // documentsModel.UploadedByUserId = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.AddedByUserId : l.AddedByUserId;
                                     documentsModel.CloseDocumentId = l.CloseDocumentId;
                                     documentsModel.TaskAttachmentId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid).Select(s => s.TaskAttachmentId)?.FirstOrDefault();
                                     documentsModel.NoAccessId = documentRightsNew?.Where(w => w.IsRead == false && w.IsReadWrite == false && w.DocumentId == l.DocumentId).Select(r => r.UserId.Value).ToList();
                                     documentsModel.ReadOnlyUserId = documentRightsNew?.Where(w => w.IsRead == true && w.IsReadWrite == false && w.DocumentId == l.DocumentId).Select(r => r.UserId.Value).ToList();
                                     documentsModel.ReadWriteUserId = documentRightsNew?.Where(w => w.IsReadWrite == true && w.IsRead == false && w.DocumentId == l.DocumentId).Select(r => r.UserId.Value).ToList();
                                     documentsModel.LockedByUser = applicationUsers.FirstOrDefault(f => f.UserId == taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid)?.Select(s => s.LockedByUserId)?.FirstOrDefault())?.UserName;
                                     documentsModel.UploadedByUser = applicationUsers.FirstOrDefault(f => f.UserId == taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == selTaskid)?.Select(s => s.UploadedByUserId)?.FirstOrDefault())?.UserName;
                                     documentsModel.ParentTaskID = tasks?.FirstOrDefault(p => p.TaskId == selTaskid)?.ParentTaskId;
                                     documentsModel.DocumentLinkType = l.IsMainTask != null ? l.IsMainTask == true ? "Main Document" : l.IsMainTask == false ? "Link Document" : "" : "";
                                     documentsModels.Add(documentsModel);
                                 });
                            }
                        }
                    }


                }

                documentsModels.Distinct().ToList().ForEach(d =>
                {
                    //if (maintaskid.TaskId == id && maintaskid.ParentTaskId > 0)
                    //{
                    if (d.ParentTaskID != null && d.ParentTaskID > 0)
                    {
                        d.IsSubTaskDocument = true;
                    }
                    else if (d.ParentTaskID == null)
                    {
                        d.IsSubTaskDocument = false;
                    }
                    //}

                    var setPers = false;
                    var userid = _context.TaskMaster
                                    .Include("TaskMembers")
                                    .Include("TaskAssigned")
                                    .FirstOrDefault(i => i.TaskId == d.LinkID);
                    var assign = userid?.TaskAssigned?.Select(s => s.UserId.Value)?.ToList();
                    var ccusers = userid?.TaskMembers?.Select(s => s.AssignedCc.Value)?.ToList();

                    if (d.ReadWriteUserId.Count > 0)
                    {
                        setPers = true;
                        if (userId != null)
                        {
                            if (userid != null)
                            {
                                d.ReadWriteUserId.Add(userid.AddedByUserId.Value);

                                if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                                {
                                    d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (assign != null)
                        {
                            d.ReadWriteUserId.AddRange(assign);
                        }
                        if (ccusers != null)
                        {
                            d.ReadWriteUserId.AddRange(ccusers);
                        }
                        if (userid != null)
                        {
                            d.ReadWriteUserId.Add(userid.AddedByUserId.Value);

                            if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                            {
                                d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                            }
                        }
                        if (d.UploadedByUserId != null)
                        {
                            d.ReadWriteUserId.Add(d.UploadedByUserId.Value);
                        }
                    }
                    if (d.ReadOnlyUserId.Count > 0)
                    {
                        //d.ReadOnlyUserId.Add(userid.AddedByUserId.Value);
                        //if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                        //{
                        //    d.ReadOnlyUserId.Add(userid.OnBehalf.Value);
                        //}
                    }
                    else
                    {
                        if (!setPers)
                        {
                            //d.ReadOnlyUserId.Add(d.UploadedByUserId.Value);
                            //d.ReadOnlyUserId.AddRange(ccusers);
                            //d.ReadOnlyUserId.Add(userid.AddedByUserId.Value);
                            //d.ReadOnlyUserId.AddRange(assign);
                            //if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                            //{
                            //    d.ReadOnlyUserId.Add(userid.OnBehalf.Value);
                            //}
                        }
                    }
                    if (d.IsExpiryDate == false || d.IsExpiryDate == null)
                    {
                        d.ExpiryDate = null;
                    }
                });
                documentsModels = documentsModels.OrderByDescending(d => d.UploadDate).Distinct().ToList();
                return documentsModels;
            }
            else
                return new List<DocumentsModel>();
        }

        [HttpGet]
        [Route("GetSelectedTaskFiles")]
        public List<DocumentsModel> GetSelectedTaskFiles(int id, int userId)
        {
            List<ApplicationUser> applicationUsers = _context.ApplicationUser.AsNoTracking().ToList();
            var documents = _context.Documents.Select(s => new
            {
                s.DocumentType,
                s.DepartmentId,
                s.ContentType,
                s.Description,
                s.DisplayName,
                s.DocumentId,
                s.Extension,
                s.FileName,
                s.SessionId,
                s.DocumentRights,
            }).ToList();

            var taskAttachments = _context.TaskAttachment
                                    .Include("UploadedByUser")
                                    .Include("TaskMaster")
                                    .Include("TaskComment")
                                    .Where(d => d.TaskMasterId == id && d.IsLatest.Value == true).AsNoTracking().ToList();
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            taskAttachments.ForEach(s =>
            {
                var documetsData = documents.FirstOrDefault(w => w.DocumentId == s.DocumentId);
                DocumentsModel documentsModel = new DocumentsModel
                {
                    Description = documetsData?.Description,
                    DocumentID = documetsData.DocumentId,
                    FileName = documetsData?.FileName,
                    LinkID = s.TaskMasterId,
                    SessionID = documetsData?.SessionId,
                    UploadDate = s.UploadedDate,
                    TaskAttachmentId = s.TaskAttachmentId,
                    IsSubTask = s.IsSubTask,
                    UploadedByUser = s.UploadedByUser.UserName,
                    AddedByUserID = s.UploadedByUserId,
                    UploadedByUserId = s.UploadedByUserId,
                    ReadOnlyUserId = documetsData?.DocumentRights?.Where(w => w.IsRead == true).Select(r => r.UserId.Value).ToList(),
                    ReadWriteUserId = documetsData?.DocumentRights?.Where(w => w.IsReadWrite == true).Select(r => r.UserId.Value).ToList(),
                    IsRead = false,
                    IsEdit = false,
                    IsNoAccess = false,

                };
                documentsModels.Add(documentsModel);
            });



            documentsModels.ForEach(d =>
            {

                var setPers = false;
                var userid = _context.TaskMaster.Include("TaskMembers").Include("TaskAssigned").FirstOrDefault(i => i.TaskId == d.LinkID);
                var assign = userid.TaskAssigned.Select(s => s.UserId.Value).ToList();
                var ccusers = userid.TaskMembers.Select(s => s.AssignedCc.Value).ToList();

                if (d.ReadWriteUserId.Count > 0)
                {
                    setPers = true;
                    d.ReadWriteUserId.Add(userid.AddedByUserId.Value);
                    if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                    {
                        d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                    }
                }
                else
                {
                    d.ReadWriteUserId.AddRange(assign);
                    d.ReadWriteUserId.AddRange(ccusers);
                    d.ReadWriteUserId.Add(userid.AddedByUserId.Value);
                    d.ReadWriteUserId.Add(d.UploadedByUserId.Value);
                    if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                    {
                        d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                    }
                }

            });
            return documentsModels;

        }

        [HttpGet]
        [Route("GetFilesHistory")]
        public List<DocumentsModel> GetFilesHistory(int id)
        {
            List<long?> historyattachmentIds = new List<long?>();
            List<ApplicationUser> applicationUsers = _context.ApplicationUser.AsNoTracking().ToList();
            var TaskMasterId = _context.TaskAttachment.Where(i => i.PreviousDocumentId == id)?.FirstOrDefault()?.TaskMasterId;
            var currentTaskattachmentid = _context.TaskAttachment.Where(i => i.PreviousDocumentId == id)?.FirstOrDefault()?.TaskAttachmentId;
            var taskIds = _context.TaskAttachment.Where(i => i.PreviousDocumentId == id || i.TaskAttachmentId == id).AsNoTracking().Select(s => s.TaskAttachmentId).ToList();
            var taskattachmentlist = _context.TaskAttachment.Where(t => t.TaskMasterId == TaskMasterId).AsNoTracking().ToList();

            //taskIds.Add(id);
            var documents = _context.Documents.Select(s => new
            {
                s.CategoryId,
                s.DocumentType,
                s.DepartmentId,
                s.ContentType,
                s.Description,
                s.DisplayName,
                s.DocumentId,
                s.Extension,
                s.FileName,
                s.FileSize,
                s.IsVideoFile,
                s.SessionId,
                s.DocumentRights,
                s.ReferenceNumber,
                s.DocumentParentId,
            }).ToList();
            var taskAttachments = _context.TaskAttachment
                            .Include("UploadedByUser")
                            .Include("TaskMaster")
                            .Where(d => taskIds.Contains(d.TaskAttachmentId) && d.IsLatest == false).AsNoTracking()
                            .ToList();
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            if (taskAttachments != null && taskAttachments.Count > 0)
            {
                taskAttachments.ForEach(s =>
                {
                    var documentsData = documents?.FirstOrDefault(w => w.DocumentId == s.DocumentId);
                    DocumentsModel documentsModel = new DocumentsModel
                    {
                        DocumentType = documentsData?.DocumentType,
                        CategoryID = documentsData?.CategoryId,
                        ContentType = documentsData?.ContentType,
                        DepartmentID = documentsData?.DepartmentId,
                        Description = documentsData?.Description,
                        CheckInDescription = s.CheckInDescription,
                        DisplayName = documentsData?.DisplayName,
                        DocumentID = documentsData.DocumentId,
                        Extension = documentsData?.Extension,
                        FileName = documentsData?.FileName,
                        FileSize = documentsData != null ? (long)Math.Round(Convert.ToDouble(documentsData?.FileSize / 1024)) : 0,
                        LinkedTask = s.TaskMaster.Title,
                        LinkID = s.TaskMasterId,
                        ReferenceNumber = documentsData?.ReferenceNumber,
                        SessionID = documentsData?.SessionId,
                        UploadDate = s.UploadedDate,
                        TaskAttachmentId = s.TaskAttachmentId,
                        VersionNo = s.VersionNo,
                        DraftingVersionNo = s.DraftingVersionNo,
                        IsLocked = s.IsLocked,
                        IsMajorChange = s.IsMajorChange,
                        LockedByUserId = s.LockedByUserId,
                        LockedByUser = applicationUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName,
                        LockedDate = s.LockedDate,
                        IsMeetingNotes = s.IsMeetingNotes,
                        IsDiscussionNotes = s.IsDiscussionNotes,
                        IsSubTask = s.IsSubTask,
                        PreviousDocumentId = s.PreviousDocumentId,
                        UploadedByUser = s.UploadedByUser.UserName,
                        AddedByUserID = s.UploadedByUserId,
                        UploadedByUserId = s.UploadedByUserId,
                        ReadOnlyUserId = documentsData?.DocumentRights?.Where(w => w.IsRead == true).Select(r => r.UserId.Value).ToList(),
                        ReadWriteUserId = documentsData?.DocumentRights?.Where(w => w.IsReadWrite == true).Select(r => r.UserId.Value).ToList(),
                    };
                    documentsModels.Add(documentsModel);
                });

            }
            var userid = _context.TaskMaster
                            .Include("TaskMembers")
                            .Include("TaskAssigned")
                            .FirstOrDefault(i => i.TaskId == TaskMasterId);
            documentsModels.ForEach(d =>
            {
                var setPers = false;
                var assign = userid?.TaskAssigned.Select(s => s.UserId.Value).ToList();
                var ccusers = userid?.TaskMembers.Select(s => s.AssignedCc.Value).ToList();
                if (d.ReadWriteUserId.Count > 0)
                {
                    setPers = true;
                    d.ReadWriteUserId.Add(userid.AddedByUserId.Value);
                    if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                    {
                        d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                    }
                }
                else
                {
                    d.ReadWriteUserId.AddRange(assign);
                    d.ReadWriteUserId.AddRange(ccusers);
                    d.ReadWriteUserId.Add(userid.AddedByUserId.Value);
                    d.ReadWriteUserId.Add(d.UploadedByUserId.Value);
                    if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                    {
                        d.ReadWriteUserId.Add(userid.OnBehalf.Value);
                    }
                }
                if (d.ReadOnlyUserId.Count > 0)
                {
                    d.ReadOnlyUserId.Add(userid.AddedByUserId.Value);
                    if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                    {
                        d.ReadOnlyUserId.Add(userid.OnBehalf.Value);
                    }
                }
                else
                {
                    if (!setPers)
                    {
                        d.ReadOnlyUserId.Add(d.UploadedByUserId.Value);
                        d.ReadOnlyUserId.AddRange(ccusers);
                        d.ReadOnlyUserId.Add(userid.AddedByUserId.Value);
                        d.ReadOnlyUserId.AddRange(assign);
                        if (userid.OnBehalf.HasValue && userid.OnBehalf > 0)
                        {
                            d.ReadOnlyUserId.Add(userid.OnBehalf.Value);
                        }
                    }
                }
            });
            return documentsModels;
        }

        [HttpGet]
        [Route("GetSubTasksId")]
        public List<TaskMasterModel> GetSubTasks(int? id, int userId)
        {
            var taskAssigned = _context.TaskAssigned.Where(t => (t.UserId == userId || t.TaskOwnerId == userId)).AsNoTracking().ToList();
            var taskIds = taskAssigned.Select(s => s.TaskId.Value).ToList();
            var inviteUsertaskList = _context.TaskInviteUser.Where(s => s.InviteUserId == userId).ToList();
            var applicationUsers = _context.Employee.Include("Designation").Include(e => e.User).Where(e => e.User.StatusCodeId == 1).AsNoTracking().ToList();
            var usertaskId = _context.TaskMaster.Where(t => t.OnBehalf == userId).AsNoTracking().Select(s => s.TaskId).ToList();
            var taskId = _context.TaskMembers.Where(t => t.AssignedCc == userId).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            if (taskId != null)
            {
                usertaskId.AddRange(taskId);
            }
            if (taskIds != null && taskIds.Count > 0)
            {
                usertaskId.AddRange(taskIds);
            }

            var maintask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == id);
            if (maintask == null)
            {
                return null;
            }

            var taskMasters = _context.TaskMaster
                                .Include(t => t.TaskAssigned)
                                .Include(t => t.StatusCode)
                                .Include(t => t.AddedByUser)
                                .Include(t => t.TaskMembers)
                                .Include(t => t.TaskTag)
                                .Include(t => t.TaskLinksTask)
                                .Include(t => t.ModifiedByUser)
                                .OrderByDescending(o => o.TaskId).Where(i => i.MainTaskId == maintask.MainTaskId).AsNoTracking().ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            if (taskMasters != null && taskMasters.Count > 0)
            {
                taskMasters.ForEach(s =>
                {
                    TaskMasterModel taskMasterModel = new TaskMasterModel();

                    taskMasterModel.TaskID = s.TaskId;
                    taskMasterModel.Title = s.Title;
                    taskMasterModel.ParentTaskID = s.ParentTaskId;
                    taskMasterModel.MainTaskId = s.MainTaskId;
                    if (s.TaskAssigned != null && s.TaskAssigned.Count > 0)
                    {
                        taskMasterModel.AssignedTo = s.TaskAssigned?.Where(w => w.TaskId == s.TaskId).Select(c => c.UserId).ToList();
                        taskMasterModel.OwnerID = s.TaskAssigned?.FirstOrDefault().TaskOwnerId;
                        taskMasterModel.StartDate = s.TaskAssigned?.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault();

                    }
                    taskMasterModel.AddedByUserID = s.AddedByUserId;
                    taskMasterModel.DueDate = s.DueDate;
                    //taskMasterModel.Description = s.Description;
                    taskMasterModel.ProjectID = s.ProjectId;
                    taskMasterModel.SessionID = s.SessionId;
                    taskMasterModel.AddedByUser = s.AddedByUser?.UserName;
                    taskMasterModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    taskMasterModel.StatusCodeID = s.StatusCodeId;
                    taskMasterModel.AddedDate = s.AddedDate;
                    taskMasterModel.ModifiedDate = s.ModifiedDate;
                    taskMasterModel.FollowUp = s.FollowUp;
                    taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                    //DiscussionDate = s.DiscussionDate.Value,
                    //Version = s.Version,
                    //WorkTypeID = s.WorkTypeId.Value,
                    //RequestTypeID = s.RequestTypeId.Value,
                    taskMasterModel.IsRead = s.IsRead.HasValue ? s.IsRead.Value : false;
                    taskMasterModel.IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false;
                    taskMasterModel.IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false;
                    // WorkType = "Task",
                    taskMasterModel.WorkType = s.ParentTaskId == null ? "Task" : "SubTask";
                    taskMasterModel.RequestType = "Task Assigned";
                    taskMasterModel.Start = s.DueDate.Value;
                    taskMasterModel.End = s.DueDate.Value;
                    taskMasterModel.Id = s.TaskId;
                    taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                    taskMasterModel.VersionSessionId = s.VersionSessionId;
                    if (s.TaskTag != null)
                    {
                        taskMasterModel.TagIds = s.TaskTag?.Select(t => t.TagId.Value).ToList();
                    }
                    if (s.TaskMembers != null && s.TaskMembers.Count > 0)
                    {
                        taskMasterModel.AssignedCC = s.TaskMembers?.Select(c => c.AssignedCc).ToList();
                    }
                    taskMasterModel.OnBehalfID = s.OnBehalf;
                    if (s.TaskLinksTask != null && s.TaskLinksTask.Count > 0)
                    {
                        taskMasterModel.TaskLinkedIds = s.TaskLinksTask?.Select(t => t.LinkedTaskId.Value).ToList();
                        taskMasterModel.TaskLinkDescription = s.TaskLinksTask?.FirstOrDefault() != null ? s.TaskLinksTask?.FirstOrDefault().Description : string.Empty;
                    }
                    taskMasterModel.Name = s.Title;
                    if (usertaskId != null && usertaskId.Count > 0)
                    {
                        taskMasterModel.HasPermission = s.StatusCodeId == 511 ? s.AddedByUserId == userId ? true : false : usertaskId.Any(t => t == s.TaskId);

                    }
                    else
                    {
                        taskMasterModel.HasPermission = s.StatusCodeId == 511 ? s.AddedByUserId == userId ? true : false : false;
                    }
                    taskMasterModel.StatusCode = s.StatusCode?.CodeValue;
                    taskMasterModel.Children = new List<TaskMasterModel>();
                    taskMasterModel.IsNotifyByMessage = s.IsNotifyByMessage;
                    taskMasterModel.IsEmail = s.IsEmail;
                    taskMasterModel.NewDueDate = s.NewDueDate;
                    taskMasterModel.IsAllowEditContent = s.IsAllowEditContent;
                    taskMasterModel.IsOwnerOnbehalfLogin = s.AddedByUserId == userId || s.OnBehalf == userId ? true : false;
                    taskMasterModel.IsOwnerLogin = s.AddedByUserId == userId ? true : false;
                    taskMasterModel.IsOnbehalfLogin = s.OnBehalf == userId ? true : false;
                    taskMasterModel.HasPermission = inviteUsertaskList.Where(i => i.TaskId == s.TaskId)?.FirstOrDefault() != null ? true : taskMasterModel.HasPermission;
                    //var TaskMasterDescriptionVersionLists = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == s.VersionSessionId).ToList();
                    //List<TaskMasterDescriptionVersionModel> TaskMasterDescriptionVersionModels = new List<TaskMasterDescriptionVersionModel>();
                    //if (TaskMasterDescriptionVersionLists != null)
                    //{
                    //    TaskMasterDescriptionVersionLists.ForEach(h =>
                    //    {
                    //        TaskMasterDescriptionVersionModel TaskMasterDescriptionVersionModel = new TaskMasterDescriptionVersionModel();
                    //        TaskMasterDescriptionVersionModel.TaskMasterDescriptionVersionId = h.TaskMasterDescriptionVersionId;
                    //        TaskMasterDescriptionVersionModel.VersionNo = h.VersionNo;
                    //        TaskMasterDescriptionVersionModel.Description = h.Description;
                    //        TaskMasterDescriptionVersionModel.SessionId = h.SessionId;
                    //        TaskMasterDescriptionVersionModel.VersionNos = "Version " + h.VersionNo;
                    //        TaskMasterDescriptionVersionModels.Add(TaskMasterDescriptionVersionModel);
                    //    });
                    //}
                    //taskMasterModel.TaskMasterDescriptionVersionList = TaskMasterDescriptionVersionModels;
                    taskMasterModels.Add(taskMasterModel);
                });
            }
            taskMasterModels.ForEach(t =>
            {
                if (t.AssignedTo != null && t.AssignedTo.Count > 0)
                {
                    t.IsAssignToLogin = t.AssignedTo.Contains(userId) && t.AddedByUserID != userId;
                }
                if (t.AssignedCC != null && t.AssignedCC.Count > 0)
                {
                    t.IsAssignCCLogin = t.AssignedCC.Contains(userId) && t.AddedByUserID != userId;
                }

                t.AssignToNames = t.AssignedTo != null ? string.Join(",", applicationUsers.Where(a => t.AssignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                t.AssignCCNames = t.AssignedCC != null ? string.Join(",", applicationUsers.Where(a => t.AssignedCC.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                t.OnBehalfName = t.OnBehalfID != null ? applicationUsers.Where(a => a.UserId == t.OnBehalfID).Select(a => a.NickName).FirstOrDefault() : "";
            });

            return taskMasterModels;
        }
        [HttpGet]
        [Route("GetSubTasksIdsList")]
        public List<TaskMasterModel> GetSubTasksIdsList(int? id, int userId)
        {
            var maintask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == id);
            if (maintask == null)
            {
                return null;
            }

            var taskMasters = _context.TaskMaster
                                .Include(t => t.TaskAssigned)
                                .Include(t => t.AddedByUser)
                                .OrderBy(o => o.TaskId).Where(i => i.MainTaskId == maintask.MainTaskId).AsNoTracking().ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            if (taskMasters != null && taskMasters.Count > 0)
            {
                taskMasters.ForEach(s =>
                {
                    TaskMasterModel taskMasterModel = new TaskMasterModel();

                    taskMasterModel.TaskID = s.TaskId;
                    taskMasterModel.Title = s.Title;
                    taskMasterModel.ParentTaskID = s.ParentTaskId;
                    taskMasterModel.MainTaskId = s.MainTaskId;
                    if (s.TaskAssigned != null && s.TaskAssigned.Count > 0)
                    {
                        taskMasterModel.StartDate = s.TaskAssigned?.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault();
                    }
                    taskMasterModel.AddedByUserID = s.AddedByUserId;
                    taskMasterModel.DueDate = s.DueDate;
                    taskMasterModel.AddedByUser = s.AddedByUser?.UserName;
                    taskMasterModel.AddedDate = s.AddedDate;
                    taskMasterModel.ModifiedDate = s.ModifiedDate;
                    taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                    taskMasterModel.WorkType = s.ParentTaskId == null ? "Task" : "SubTask";
                    taskMasterModel.RequestType = "Task Assigned";
                    taskMasterModel.Start = s.DueDate.Value;
                    taskMasterModel.End = s.DueDate.Value;
                    taskMasterModel.Id = s.TaskId;
                    taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                    taskMasterModel.VersionSessionId = s.VersionSessionId;
                    taskMasterModel.NewDueDate = s.NewDueDate;
                    taskMasterModels.Add(taskMasterModel);
                });
            }
            return taskMasterModels;
        }
        [HttpGet]
        [Route("GetTaskVersionDescription")]

        public TaskMasterModel GetTaskVersionDescription(int id)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            var taskMaster = _context.TaskMaster.FirstOrDefault(i => i.TaskId == id);
            taskMasterModel.TaskID = taskMaster.TaskId;
            taskMasterModel.VersionSessionId = taskMaster.VersionSessionId;
            taskMasterModel.Description = taskMaster.Description;
            var TaskMasterDescriptionVersionLists = _context.TaskMasterDescriptionVersion?.Where(w => w.SessionId == taskMasterModel.VersionSessionId).ToList();
            List<TaskMasterDescriptionVersionModel> TaskMasterDescriptionVersionModels = new List<TaskMasterDescriptionVersionModel>();
            if (TaskMasterDescriptionVersionLists != null)
            {
                TaskMasterDescriptionVersionLists.ForEach(h =>
                {
                    TaskMasterDescriptionVersionModel TaskMasterDescriptionVersionModel = new TaskMasterDescriptionVersionModel();
                    TaskMasterDescriptionVersionModel.TaskMasterDescriptionVersionId = h.TaskMasterDescriptionVersionId;
                    TaskMasterDescriptionVersionModel.VersionNo = h.VersionNo;
                    TaskMasterDescriptionVersionModel.Description = h.Description;
                    TaskMasterDescriptionVersionModel.SessionId = h.SessionId;
                    TaskMasterDescriptionVersionModel.VersionNos = "Version " + h.VersionNo;
                    TaskMasterDescriptionVersionModels.Add(TaskMasterDescriptionVersionModel);
                });
            }
            taskMasterModel.TaskMasterDescriptionVersionList = TaskMasterDescriptionVersionModels;

            return taskMasterModel;
        }


        [HttpGet]
        [Route("GetSubNestedTasksId")]
        public List<TaskMasterModel> GetSubNestedTasksId(int? id, int userId)
        {
            var maintask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == id);
            if (maintask == null)
            {
                return null;
            }

            var tasktree = _context.TaskMaster.Include("TaskAssigned").Include("TaskMembers")
                .Select(s => new
                {
                    s.TaskId,
                    s.TaskNumber,
                    s.Title,
                    s.TaskLevel,
                    s.MainTaskId,
                    s.ParentTaskId,
                    s.StatusCodeId,
                    s.TaskAssigned,
                    s.TaskMembers,
                    s.AddedByUserId,
                    s.OnBehalf,
                    s.IsRead

                })
             .OrderBy(o => o.TaskLevel).Where(i => i.MainTaskId == maintask.MainTaskId).AsNoTracking().ToList();

            var lookup = tasktree.ToLookup(x => x.ParentTaskId);

            Func<long?, List<TaskMasterModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new TaskMasterModel()
                    {
                        TaskID = x.TaskId,
                        Id = x.TaskId,
                        Title = x.TaskNumber + " " + x.Title,
                        Name = x.StatusCodeId == 511 ? x.TaskNumber + " " + x.Title + "(Draft)" : x.TaskNumber + " " + x.Title,
                        ParentTaskID = x.ParentTaskId,
                        MainTaskId = x.MainTaskId,
                        Children = build(x.TaskId),
                        TaskLevel = x.TaskLevel,
                        TaskNumber = x.TaskNumber,
                        TreeLevel = x.TaskNumber != null ? x.TaskNumber.Split('.')[0] : "",
                        cssTitle = x.StatusCodeId == 516 ? "close"
                        : x.AddedByUserId == userId || x.OnBehalf == userId ? x.IsRead == false ? "unread" : "read"
                        : x.TaskAssigned.Where(t => t.UserId == userId).FirstOrDefault() != null ? x.TaskAssigned.Where(t => t.UserId == userId).FirstOrDefault().IsRead == false ? "unread" : "read"
                        : x.TaskMembers.Where(t => t.AssignedCc == userId).FirstOrDefault() != null ? x.TaskMembers.Where(t => t.AssignedCc == userId).FirstOrDefault().IsRead == false ? "unread" : "read" : "noPermis",
                    })
                    .ToList();
            var nestedlist = build(null).ToList();


            //List<TaskMasterModel> allPageComponents = nestedlist.SelectMany(wps => wps.Children).ToList();

            return nestedlist;
        }


        [HttpGet]
        [Route("GetSubNestedUserTasks")]
        public List<TaskMasterModel> GetSubNestedUserTasks(int? id, int userId)
        {
            var usertaskId = _context.TaskMaster.Where(t => t.OnBehalf == userId && t.StatusCodeId != 511).AsNoTracking().Select(s => s.TaskId).ToList();
            var taskId = _context.TaskMembers.Where(t => t.AssignedCc == userId && t.StatusCodeId != 511).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            var taskassignId = _context.TaskAssigned.Where(t => (t.UserId == userId || t.TaskOwnerId == userId) && t.StatusCodeId != 511).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            usertaskId.AddRange(taskId);
            usertaskId.AddRange(taskassignId);

            var maintaskId = _context.TaskMaster.FirstOrDefault(t => t.TaskId == id).MainTaskId;
            var taskCommentUser = _context.TaskCommentUser.FirstOrDefault(t => t.TaskMasterId == id && t.UserId == userId);
            var tasktree = _context.TaskMaster.Include("StatusCode").Include("TaskComment.CommentedByNavigation").Include("AddedByUser")
                                    .OrderBy(o => o.TaskNumber)
                                    .Where(i => i.MainTaskId == maintaskId && usertaskId.Contains(i.TaskId))
                                    .OrderByDescending(t => t.TaskId).AsNoTracking().ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            tasktree.ForEach(x =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel
                {
                    TaskID = x.TaskId,
                    Id = x.TaskId,
                    Title = x.Title,
                    Name = x.Title,
                    DueDate = x.DueDate,
                    TaskNumber = x.TaskNumber,
                    TaskLevel = x.TaskLevel,
                    TaskMessages = x.TaskComment.Select(s => new TaskCommentModel { TaskSubject = s.TaskSubject, Comment = s.Comment, CommentedDate = s.CommentedDate, AddedByUser = s.CommentedByNavigation.UserName, IsEdited = s.IsEdited, TaskCommentID = s.TaskCommentId }).ToList(),
                    StatusCode = x.StatusCode.CodeValue,
                    ParentTaskID = x.ParentTaskId,
                    MainTaskId = x.MainTaskId,
                    Description = x.Description,
                    cssClass = x.ParentTaskId == null ? "green" : "yellow",
                    AddedByUser = x.AddedByUser.UserName,
                    AddedByUserID = x.AddedByUserId,
                    OnBehalfID = x.OnBehalf,
                    NewDueDate = x.NewDueDate,
                };
                taskMasterModels.Add(taskMasterModel);
            });
            return taskMasterModels.OrderBy(t => t.TaskID).ToList();
        }
        public IEnumerable<TaskMasterModel> DepthFirstTraversal(TaskMasterModel node)
        {
            Stack<TaskMasterModel> nodes = new Stack<TaskMasterModel>();
            nodes.Push(node);

            while (nodes.Count > 0)
            {
                var n = nodes.Pop();
                yield return n;

                for (int i = n.Children.Count - 1; i >= 0; i--)
                    nodes.Push(n.Children[i]);
            }
        }
        // GET: api/Project/2
        [HttpGet("GetTaskLinks/{id:int}")]
        public ActionResult<TaskMasterModel> GetTaskLinkId(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskLink = _context.TaskLinks.SingleOrDefault(p => p.TaskLinkId == id.Value);
            var result = _mapper.Map<TaskMasterModel>(taskLink);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }



        [HttpGet]
        [Route("GetTaskSorting")]
        public List<TaskMasterModel> GetTaskSorting(int id, int userId)
        {
            var taskIds = new List<long>();

            if (id != 709)
            {
                taskIds = _context.TaskAssigned.Where(t => t.UserId == userId).AsNoTracking().Select(s => s.TaskId.Value).ToList();
                if (id == 708)
                {
                    var taskDraftIDs = _context.TaskMaster.Where(w => !taskIds.Contains(w.TaskId) && w.StatusCodeId == 511).AsNoTracking().Select(t => t.TaskId).ToList();
                    taskIds.AddRange(taskDraftIDs);
                }
            }
            else
            {
                taskIds = _context.TaskAssigned.Where(t => t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId.Value).ToList();
            }

            var query = _context.TaskMaster.Include("AddedByUser")
                .Include("OnBehalfNavigation").Include("TaskAttachment")
                .Include("TaskMembers").Include("TaskTag")
                .Include("TaskLinksTask").Include("ModifiedByUser").Include("TaskAssigned")
                .Where(w => taskIds.Contains(w.TaskId)).AsNoTracking().AsQueryable();


            if (id == 700)
            {
                query = query.OrderBy(o => o.TaskId);
            }
            else if (id == 701)
            {
                query = query.OrderByDescending(o => o.TaskId);
            }
            else if (id == 702)
            {
                query = query.OrderBy(o => o.Title);
            }
            else if (id == 703)
            {
                query = query.OrderByDescending(o => o.Title);
            }
            else if (id == 704)
            {
                query = query.Where(w => w.IsRead.Value).OrderByDescending(o => o.Title);
            }
            else if (id == 705)
            {
                query = query.Where(w => w.IsRead == false).OrderByDescending(o => o.Title);
            }
            else if (id == 706)
            {
                query = query.Where(w => w.DueDate < DateTime.Today).OrderByDescending(o => o.Title);
            }
            else if (id == 707)
            {
                query = query.Where(w => w.ParentTaskId == null).OrderByDescending(o => o.TaskId);
            }
            else if (id == 708)
            {
                query = query.Where(w => w.StatusCodeId == 511 && w.AddedByUserId == userId).OrderByDescending(o => o.TaskId);
            }

            var taskMasterItems = query.ToList();
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            taskMasterItems.ForEach(s =>
            {
                TaskMasterModel taskMasterModel = new TaskMasterModel();
                taskMasterModel.TaskID = s.TaskId;
                taskMasterModel.Title = s.Title;
                taskMasterModel.ParentTaskID = s.ParentTaskId;
                taskMasterModel.MainTaskId = s.MainTaskId;
                taskMasterModel.AssignedTo = s.TaskAssigned.Select(t => t.UserId).ToList();
                taskMasterModel.AddedByUserID = s.AddedByUserId;
                taskMasterModel.OwnerID = s.TaskAssigned.FirstOrDefault()?.TaskOwnerId;
                taskMasterModel.DueDate = s.DueDate;
                taskMasterModel.Description = s.Description;
                taskMasterModel.ProjectID = s.ProjectId;
                taskMasterModel.SessionID = s.SessionId;
                taskMasterModel.AddedByUser = s.AddedByUser.UserName;
                taskMasterModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                taskMasterModel.StatusCodeID = s.StatusCodeId;
                taskMasterModel.AddedDate = s.AddedDate;
                taskMasterModel.ModifiedDate = s.ModifiedDate;
                taskMasterModel.IsRead = s.IsRead.HasValue ? s.IsRead.Value : false;
                taskMasterModel.IsUrgent = s.IsUrgent.HasValue ? s.IsUrgent.Value : false;
                taskMasterModel.IsAutoClose = s.IsAutoClose.HasValue ? s.IsAutoClose.Value : false;
                taskMasterModel.Start = s.DueDate.Value;
                taskMasterModel.End = s.DueDate.Value;
                taskMasterModel.Id = s.TaskId;
                taskMasterModel.OnBehalfName = s.OnBehalfNavigation?.UserName;
                taskMasterModel.OnBehalfID = s.OnBehalf;
                taskMasterModel.OverDueAllowed = s.OverDueAllowed;
                taskMasterModel.FollowUp = s.FollowUp;
                taskMasterModel.TagNames = s.TaskTag.Where(t => t.Tag != null).Select(t => t.Tag.Name).ToList();
                taskMasterModel.cssClass = s.AssignedTo == userId ? s.DueDate < DateTime.Today ? s.StatusCodeId == 512 ? "red" : "blue" : "green" : "blue";
                taskMasterModel.TagIds = s.TaskTag.Select(t => t.TagId.Value).ToList();
                taskMasterModel.OrderNo = s.OrderNo;
                taskMasterModel.IsNoDueDate = s.IsNoDueDate;
                taskMasterModel.AssignedCC = s.TaskMembers.Select(c => c.AssignedCc).ToList();
                taskMasterModel.TaskLinkedIds = s.TaskLinksTask.Select(t => t.LinkedTaskId.Value).ToList();
                taskMasterModel.TaskLinkDescription = s.TaskLinksTask.FirstOrDefault() != null ? s.TaskLinksTask.FirstOrDefault().Description : string.Empty;
                taskMasterModel.Attachments = s.TaskAttachment.Select(sa => sa.DocumentId.Value).ToList();
                //WorkType = "Task",
                taskMasterModel.WorkType = s.ParentTaskId == null ? "Task" : "SubTask";
                taskMasterModel.RequestType = "Task Assigned";
                taskMasterModel.StartDate = s.TaskAssigned.Where(t => t.TaskId == s.TaskId).Select(c => c.StartDate).FirstOrDefault();
                taskMasterModel.IsNotifyByMessage = s.IsNotifyByMessage;
                taskMasterModel.IsEmail = s.IsEmail;
                taskMasterModel.IsAllowEditContent = s.IsAllowEditContent;
                taskMasterModel.NewDueDate = s.NewDueDate;
                taskMasterModels.Add(taskMasterModel);
            });

            return taskMasterModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskMasterModel> GetData(SearchModel searchModel)
        {
            var taskMaster = new TaskMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).FirstOrDefault();
                        break;
                    case "Last":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).LastOrDefault();
                        break;
                    case "Previous":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).FirstOrDefault();

                        break;
                    case "Last":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskMaster.OrderBy(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).FirstOrDefault(s => s.TaskId > searchModel.Id);
                        break;
                    case "Previous":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).Where(t => t.AssignedTo == searchModel.UserID).FirstOrDefault(s => s.TaskId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskMasterModel>(taskMaster);
            return result;
        }

        [HttpPost()]
        [Route("GetDataTaskLink")]
        public ActionResult<TaskMasterModel> GetDataTaskLink(SearchModel searchModel)
        {
            var taskMaster = new TaskLinks();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).LastOrDefault();
                        break;
                    case "Previous":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskLinks.OrderBy(o => o.TaskLinkId).FirstOrDefault(s => s.TaskLinkId > searchModel.Id);
                        break;
                    case "Previous":
                        taskMaster = _context.TaskLinks.OrderByDescending(o => o.TaskLinkId).FirstOrDefault(s => s.TaskLinkId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskMasterModel>(taskMaster);
            return result;
        }
        [HttpPost()]
        [Route("GetDataSubTask")]
        public ActionResult<TaskMasterModel> GetDataSubTask(SearchModel searchModel)
        {
            var taskMaster = new TaskMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).LastOrDefault();
                        break;
                    case "Previous":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).LastOrDefault();
                        break;
                    case "Next":
                        taskMaster = _context.TaskMaster.OrderBy(o => o.TaskId).FirstOrDefault(s => s.TaskId > searchModel.Id);
                        break;
                    case "Previous":
                        taskMaster = _context.TaskMaster.OrderByDescending(o => o.TaskId).FirstOrDefault(s => s.TaskId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskMasterModel>(taskMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskMaster")]
        public TaskMasterModel Post(TaskMasterModel value)
        {
            var applicationUsers = _context.Employee.Include(s => s.Designation).Include(s => s.User).Where(e => e.User.StatusCodeId == 1).AsNoTracking().ToList();
            var exist = _context.TaskMaster.Where(t => t.Title.Trim().ToLower() == value.Title.ToLower().Trim()).FirstOrDefault();
            if (exist != null)
            {
                value.IsRecordExist = true;
                value.Title = value.Title + " " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt");
                return value;
            }
            if (value.IsNoDueDate == true)
            {
                value.DueDate = DateTime.Now.AddYears(20);
            }
            value.VersionSessionId = Guid.NewGuid();
            if (value.SessionID == null)
            {
                value.SessionID = Guid.NewGuid();
            }
            var taskMaster = new TaskMaster
            {
                //TaskId=value.TaskID,
                Title = value.Title,
                ParentTaskId = value.ParentTaskID,
                MainTaskId = value.MainTaskId,
                AssignedTo = value.AssignedTo.Count > 0 ? value.AssignedTo[0] : null,
                DueDate = value.DueDate,
                Description = value.Description,
                ProjectId = value.ProjectID,
                SessionId = value.SessionID,
                FollowUp = value.FollowUp,
                OnBehalf = value.OnBehalfID,
                OwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                // DiscussionDate = value.DiscussionDate,
                Version = "1.0",
                WorkTypeId = 37,
                RequestTypeId = 58,
                IsRead = false,
                IsNoDueDate = value.IsNoDueDate,
                IsUrgent = value.IsUrgent,
                IsAutoClose = value.IsAutoClose,
                OverDueAllowed = value.OverDueAllowed == null ? "false" : value.OverDueAllowed,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                AddedDate = DateTime.Now,
                TaskMembers = new List<TaskMembers>(),
                TaskTag = new List<TaskTag>(),
                TaskAttachment = new List<TaskAttachment>(),
                IsNotifyByMessage = value.IsNotifyByMessage,
                IsEmail = value.IsEmail,
                VersionSessionId = value.VersionSessionId,
                IsAllowEditContent = value.IsAllowEditContent,
                IsFromProfileDocument = false,
            };
            if (value.ParentTaskID != null && value.MainTaskId != null)
            {
                var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.ParentTaskID);
                var taskNumber = "1";
                if (tasklevel == null)
                {
                    tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.ParentTaskID);
                    taskNumber = tasklevel.TaskNumber + ".1";
                }
                else
                {
                    var numberarray = tasklevel.TaskNumber.Split('.');
                    var number = numberarray[numberarray.Length - 1].ToString();
                    var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                    var taskNo = tasklevel.TaskNumber.Remove(leng);
                    number = (double.Parse(number) + 1).ToString();
                    taskNumber = taskNo + "." + number;
                }
                taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                taskMaster.TaskNumber = taskNumber;
            }
            else if (value.ParentTaskID == null && value.MainTaskId != null)
            {
                var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.MainTaskId == value.MainTaskId && id.ParentTaskId == null);
                var level = tasklevel.TaskLevel + 1;
                taskMaster.TaskLevel = level;
                taskMaster.TaskNumber = level.ToString();
            }


            if (value.AssignedTo != null)
            {
                value.AssignedTo.ForEach(c =>
                {
                    var assigned = new TaskAssigned
                    {
                        UserId = c,
                        StatusCodeId = value.StatusCodeID,
                        DueDate = value.DueDate,
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                        IsRead = false,
                        StartDate = value.StartDate,
                        OnBehalfId = value.OnBehalfID,
                    };
                    taskMaster.TaskAssigned.Add(assigned);
                });
            }

            if (value.AssignedCC != null)
            {
                value.AssignedCC.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var member = new TaskMembers
                        {
                            AssignedCc = c,
                            IsRead = false,
                            DueDate = value.DueDate,
                            StatusCodeId = value.StatusCodeID,
                            //OnBehalfId = value.OnBehalfID,
                            //AssignedTo = value.AssignedTo,
                            //AssignedFrom = value.AddedByUserID.Value
                        };
                        taskMaster.TaskMembers.Add(member);
                    }
                });
            }
            if (value.TagIds != null && value.TagIds.Count > 0)
            {
                value.TagIds.ForEach(t =>
                {
                    var tag = new TaskTag
                    {
                        TagId = t,
                    };
                    taskMaster.TaskTag.Add(tag);
                });
            }


            _context.TaskMaster.Add(taskMaster);
            _context.SaveChanges();
            value.TaskID = taskMaster.TaskId;
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == value.SessionID).ToList();
                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null)
                    {
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            VersionNo = "1",
                            CheckInDescription = value.DocumentDescription
                            //TaskMasterId = value.TaskID
                        };
                        taskMaster.TaskAttachment.Add(taskAttach);
                    }
                    if (value.ReadonlyUserID != null)
                    {
                        value.ReadonlyUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                    if (value.ReadWriteUserID != null)
                    {
                        value.ReadWriteUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                });
            }
            _context.SaveChanges();

            if (value.PersonaltagIds != null && value.PersonaltagIds.Count > 0)
            {
                value.PersonaltagIds.ForEach(t =>
                {
                    var personalTagsNew = _context.PersonalTags.Where(p => p.TaskId == value.TaskID && p.Tag == t).FirstOrDefault();
                    if (personalTagsNew == null)
                    {
                        var personalTags = new PersonalTags
                        {
                            Tag = t,
                            TaskId = value.TaskID,
                            AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            AddedDate = value.AddedDate,

                        };
                        _context.PersonalTags.Add(personalTags);
                    }
                });

            }

            if (value.ParentTaskID == null && value.MainTaskId == null)
            {
                // to build tree view with master taskID..
                var maintask = _context.TaskMaster.FirstOrDefault(id => id.TaskId == taskMaster.TaskId);
                maintask.MainTaskId = taskMaster.TaskId;
                maintask.TaskLevel = 1;
                maintask.TaskNumber = "1";
                _context.SaveChanges();

                value.MainTaskId = maintask.MainTaskId;
            }

            if (value.IsEmail.GetValueOrDefault(false) && !value.IsAutoClose.GetValueOrDefault(false))
            {
                CreateDefaultConversation(value);
            }
            value.AssignToNames = value.AssignedTo != null ? string.Join(",", applicationUsers.Where(a => value.AssignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
            value.AssignCCNames = value.AssignedCC != null ? string.Join(",", applicationUsers.Where(a => value.AssignedCC.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
            value.OnBehalfName = value.OnBehalfID != null ? applicationUsers.Where(a => a.UserId == value.OnBehalfID).Select(a => a.NickName).FirstOrDefault() : "";
            value.HasPermission = true;
            var TaskMasterDescriptionVersionNo = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == value.VersionSessionId).Count();
            var TaskMasterDescriptionVersion = new TaskMasterDescriptionVersion
            {
                VersionNo = TaskMasterDescriptionVersionNo == 0 ? 1 : (TaskMasterDescriptionVersionNo + 1),
                SessionId = value.VersionSessionId,
                Description = value.Description,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now
            };
            _context.TaskMasterDescriptionVersion.Add(TaskMasterDescriptionVersion);
            _context.SaveChanges();
            return value;
        }
        private void CreateDefaultFileProfileConversation(DocumentTaskMasterModel value)
        {
            var taskComment = new TaskComment
            {
                //  TaskCommentId = value.TaskCommentID,
                TaskMasterId = value.TaskID,
                TaskSubject = "E-" + value.Title,
                Comment = value.Description,
                CommentedBy = value.AddedByUserID,
                CommentedDate = DateTime.Now,
                IsRead = false,
                IsEdited = false,
                IsDocument = false,
                TaskCommentUser = new List<TaskCommentUser>()
            };
            List<long?> userIds = new List<long?>();
            if (value.AssignedTo.Any())
            {
                userIds.AddRange(value.AssignedTo);
            }
            if (value.OnBehalfID.HasValue)
            {
                userIds.Add(value.OnBehalfID);
            }
            if (value.AssignedCC.Any())
            {
                userIds.AddRange(value.AssignedCC);
            }
            userIds.Add(value.AddedByUserID);
            userIds.ForEach(u =>
            {
                var userComment = new TaskCommentUser
                {
                    IsRead = false,
                    TaskCommentId = taskComment.TaskCommentId,
                    TaskMasterId = value.TaskID,
                    UserId = u.Value,
                    DueDate = value.DueDate,
                    IsAssignedTo = true,
                    StatusCodeId = value.StatusCodeID,
                    IsClosed = false,
                };

                taskComment.TaskCommentUser.Add(userComment);
            });
            _context.TaskComment.Add(taskComment);
            _context.SaveChanges();
        }
        private void CreateDefaultConversation(TaskMasterModel value)
        {
            var taskComment = new TaskComment
            {
                //  TaskCommentId = value.TaskCommentID,
                TaskMasterId = value.TaskID,
                TaskSubject = "E-" + value.Title,
                Comment = value.Description,
                CommentedBy = value.AddedByUserID,
                CommentedDate = DateTime.Now,
                IsRead = false,
                IsEdited = false,
                IsDocument = false,
                TaskCommentUser = new List<TaskCommentUser>()
            };
            List<long?> userIds = new List<long?>();
            if (value.AssignedTo.Any())
            {
                userIds.AddRange(value.AssignedTo);
            }
            if (value.OnBehalfID.HasValue)
            {
                userIds.Add(value.OnBehalfID);
            }
            userIds.Add(value.AddedByUserID);
            userIds.ForEach(u =>
            {
                var userComment = new TaskCommentUser
                {
                    IsRead = false,
                    TaskCommentId = taskComment.TaskCommentId,
                    TaskMasterId = value.TaskID,
                    UserId = u.Value,
                    DueDate = value.DueDate,
                    IsAssignedTo = true,
                    StatusCodeId = value.StatusCodeID,
                    IsClosed = false,
                };

                taskComment.TaskCommentUser.Add(userComment);
            });
            _context.TaskComment.Add(taskComment);
            _context.SaveChanges();
        }


        [HttpPost]
        [Route("ReOpenTask")]
        public TaskMasterModel ReOpenTask(TaskMasterModel value)
        {
            var exist = _context.TaskMaster.Include("TaskAttachment").FirstOrDefault(t => t.TaskId == value.TaskID);

            var version = double.Parse(exist.Version) + 0.1;
            var taskMaster = new TaskMaster
            {
                //TaskId=value.TaskID,
                Title = value.Title + " v" + version,
                ParentTaskId = exist.TaskId,
                MainTaskId = exist.MainTaskId,
                AssignedTo = value.AssignedTo.Count > 0 ? value.AssignedTo[0] : null,
                DueDate = value.DueDate,
                Description = value.Description,
                ProjectId = value.ProjectID,
                SessionId = value.SessionID,
                FollowUp = value.FollowUp,
                OnBehalf = value.OnBehalfID,
                OwnerId = value.AddedByUserID,
                Version = version.ToString(),
                WorkTypeId = 37,
                RequestTypeId = 58,
                IsRead = false,
                IsUrgent = value.IsUrgent ? value.IsUrgent : false,
                IsAutoClose = value.IsAutoClose,
                OverDueAllowed = value.OverDueAllowed,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                TaskMembers = new List<TaskMembers>(),
                TaskTag = new List<TaskTag>(),
                TaskAttachment = new List<TaskAttachment>(),
                IsNotifyByMessage = value.IsNotifyByMessage,
                IsEmail = value.IsEmail,
                NewDueDate = value.NewDueDate,
            };
            if (exist.TaskId > 0 && exist.MainTaskId != null)
            {
                var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == exist.TaskId);
                var taskNumber = "1";
                if (tasklevel == null)
                {
                    tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == exist.TaskId);
                    taskNumber = tasklevel.TaskNumber + ".1";
                }
                else
                {
                    var numberarray = tasklevel.TaskNumber.Split('.');
                    var number = numberarray[numberarray.Length - 1].ToString();
                    var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                    var taskNo = tasklevel.TaskNumber.Remove(leng);
                    number = (double.Parse(number) + 1).ToString();
                    taskNumber = taskNo + "." + number;
                }
                taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                taskMaster.TaskNumber = taskNumber;
            }
            if (value.AssignedTo != null)
            {
                value.AssignedTo.ForEach(c =>
                {
                    var assigned = new TaskAssigned
                    {
                        UserId = c,
                        StatusCodeId = value.StatusCodeID,
                        DueDate = value.DueDate,
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = value.AddedByUserID,
                        IsRead = false,
                        StartDate = value.StartDate,
                        OnBehalfId = value.OnBehalfID,
                    };
                    taskMaster.TaskAssigned.Add(assigned);
                });
            }

            if (value.AssignedCC != null)
            {
                value.AssignedCC.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var member = new TaskMembers
                        {
                            AssignedCc = c,
                            IsRead = false,
                            DueDate = value.DueDate,
                            StatusCodeId = value.StatusCodeID,
                        };
                        taskMaster.TaskMembers.Add(member);
                    }
                });
            }
            if (value.TagIds != null && value.TagIds.Count > 0)
            {
                value.TagIds.ForEach(t =>
                {
                    var tag = new TaskTag
                    {
                        TagId = t,
                    };
                    taskMaster.TaskTag.Add(tag);
                });
            }
            _context.TaskMaster.Add(taskMaster);
            _context.SaveChanges();
            value.TaskID = taskMaster.TaskId;
            var taskComments = _context.TaskComment.Include("TaskCommentUser").Include("CommentAttachment").Where(c => c.TaskMasterId == exist.TaskId).ToList();
            if (taskComments.Count > 0)
            {
                taskComments.ForEach(c =>
                {
                    var taskComment = new TaskComment
                    {
                        TaskMasterId = value.TaskID,
                        TaskSubject = c.TaskSubject,
                        Comment = c.Comment,
                        CommentedBy = c.CommentedBy,
                        ParentCommentId = c.ParentCommentId,
                        DocumentId = c.DocumentId,
                        IsDocument = c.IsDocument,
                        CommentedDate = c.CommentedDate,
                        IsRead = c.IsRead,
                        IsEdited = c.IsEdited,
                        SessionId = c.SessionId,
                        TaskId = c.TaskId,
                        MainTaskId = c.MainTaskId,
                        EditedBy = c.EditedBy,
                        EditedDate = c.EditedDate,
                    };
                    c.TaskCommentUser.ToList().ForEach(u =>
                    {
                        var userComment = new TaskCommentUser
                        {
                            IsRead = u.IsRead,
                            TaskMasterId = value.TaskID,
                            UserId = u.UserId,
                            StatusCodeId = u.StatusCodeId,
                        };
                        taskComment.TaskCommentUser.Add(userComment);
                    });

                    c.CommentAttachment.ToList().ForEach(a =>
                    {
                        var commentAttachement = new CommentAttachment
                        {

                            DocumentId = a.DocumentId,
                            UserId = a.UserId,
                            FileName = a.FileName
                        };
                        taskComment.CommentAttachment.Add(commentAttachement);
                    });

                    _context.TaskComment.Add(taskComment);
                });
                _context.SaveChanges();
            }

            return value;
        }

        private void AddNotification(List<long?> users, string notifi)
        {
            users.ForEach(u =>
            {
                var notification = new Notification
                {
                    IsRead = false,
                    ModuleId = 550,
                    NotifiedUserId = u,
                    UpdatedDate = DateTime.Now,
                    Notification1 = notifi,
                };
                _context.Notification.Add(notification);
            });
            _context.SaveChanges();
        }

        [HttpPost]
        [Route("InsertSubTaskMaster")]
        public TaskMasterModel PostSubTask(TaskMasterModel value)
        {
            var exist = _context.TaskMaster.Where(t => t.Title.Trim().ToLower() == value.Title.ToLower().Trim()).FirstOrDefault();
            if (exist != null) throw new AppException("There is task exist with this title. please check the task or change new title.!", null);
            if (value.SessionID == null)
            {
                value.SessionID = Guid.NewGuid();
            }
            var parentTask = _context.TaskMaster.FirstOrDefault(id => id.TaskId == value.ParentTaskID);
            var taskMaster = new TaskMaster
            {

                //TaskId=value.TaskID,
                Title = value.Title,
                ParentTaskId = value.ParentTaskID,
                AssignedTo = value.AssignedTo.Count > 0 ? value.AssignedTo[0] : null,
                DueDate = value.DueDate,
                Description = value.Description,
                ProjectId = value.ProjectID,
                SessionId = value.SessionID,
                MainTaskId = value.MainTaskId,
                OwnerId = value.AddedByUserID,
                OnBehalf = value.OnBehalfID,
                Version = "1.0",
                WorkTypeId = 37,
                RequestTypeId = 58,
                IsRead = false,
                IsUrgent = value.IsUrgent ? value.IsUrgent : false,
                IsAutoClose = value.IsAutoClose,
                OverDueAllowed = value.OverDueAllowed,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                TaskMembers = new List<TaskMembers>(),
                TaskTag = new List<TaskTag>(),
                TaskAttachment = new List<TaskAttachment>(),
                IsNotifyByMessage = value.IsNotifyByMessage,
                IsEmail = value.IsEmail,
                IsAllowEditContent = value.IsAllowEditContent,
                IsFromProfileDocument = parentTask?.IsFromProfileDocument != null && parentTask?.IsFromProfileDocument == true ? true : false,
            };
            if (value.AssignedTo != null)
            {
                value.AssignedTo.ForEach(c =>
                {
                    var assigned = new TaskAssigned
                    {
                        UserId = c,
                        StatusCodeId = value.StatusCodeID,
                        DueDate = value.DueDate,
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = value.AddedByUserID,
                        IsRead = false,
                        StartDate = value.StartDate,
                        OnBehalfId = value.OnBehalfID,
                    };
                    taskMaster.TaskAssigned.Add(assigned);
                });
            }
            if (value.AssignedCC != null)
            {
                value.AssignedCC.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var member = new TaskMembers
                        {
                            AssignedCc = c,
                            IsRead = false,
                            DueDate = value.DueDate,
                            StatusCodeId = value.StatusCodeID,
                            //OnBehalfId = value.OnBehalfID,
                            //AssignedTo = value.AssignedTo,
                            //AssignedFrom = value.AddedByUserID.Value
                        };
                        taskMaster.TaskMembers.Add(member);
                    }
                });
            }
            if (value.TagIds.Count > 0)
            {
                value.TagIds.ForEach(t =>
                {
                    var tag = new TaskTag
                    {
                        TagId = t,
                    };
                    taskMaster.TaskTag.Add(tag);
                });
            }
            _context.TaskMaster.Add(taskMaster);
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == value.SessionID).ToList();
                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null)
                    {
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            //TaskMasterId = value.TaskID
                        };
                        taskMaster.TaskAttachment.Add(taskAttach);
                    }
                    if (value.ReadonlyUserID != null)
                    {
                        value.ReadonlyUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                    if (value.ReadWriteUserID != null)
                    {
                        value.ReadWriteUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                });
            }
            _context.SaveChanges();
            value.TaskID = taskMaster.TaskId;
            if (value.ParentTaskID == null)
            {
                // to build tree view with master taskID..
                var maintask = _context.TaskMaster.FirstOrDefault(id => id.TaskId == taskMaster.TaskId);
                maintask.MainTaskId = taskMaster.TaskId;
                _context.SaveChanges();
            }

            return value;
        }
        [HttpPost]
        [Route("InsertTaskLinks")]
        public TaskMasterModel PostTaskLink(TaskMasterModel value)
        {
            var tasklink = _context.TaskLinks.Where(l => l.TaskId == value.TaskID).ToList();

            if (tasklink.Count > 0)
            {
                _context.TaskLinks.RemoveRange(tasklink);
                _context.SaveChanges();
            }

            value.TaskLinkedIds.ToList().ForEach(l =>
            {
                var tasklinkadd = new TaskLinks
                {
                    TaskLinkId = value.TaskLinkID,
                    TaskId = value.TaskID,
                    LinkedTaskId = l,
                    Description = value.TaskLinkDescription
                };
                _context.TaskLinks.Add(tasklinkadd);
            });

            _context.SaveChanges();
            return value;

        }
        [HttpPut]
        [Route("MultipleTaskUpdateAsRead")]
        public TaskMasterModel MultipleTaskUpdateAsRead(TaskMasterModel value)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            var taskmembers = _context.TaskMembers.Where(a => value.TaskIds.Contains(a.TaskId)).AsNoTracking().ToList();
            var taskassignedList = _context.TaskAssigned.Where(a => a.UserId == value.ModifiedByUserID && value.TaskIds.Contains(a.TaskId)).ToList();
            if (value.TaskIds.Count > 0)
            {
                var taskMaster = _context.TaskMaster.Where(t => value.TaskIds.Contains(t.TaskId)).AsNoTracking().ToList();
                if (taskMaster != null && taskMaster.Count > 0)
                {
                    taskMaster.ForEach(t =>
                    {
                        if (t.OnBehalf == value.ModifiedByUserID || t.OwnerId == value.ModifiedByUserID || t.AddedByUserId == value.ModifiedByUserID)
                        {
                            var task = _context.TaskMaster.FirstOrDefault(d => d.TaskId == t.TaskId);
                            if (task != null)
                            {
                                if (task.IsRead == false)
                                {
                                    task.IsRead = true;
                                }
                                _context.SaveChanges();
                            }

                        }

                        var taskassigned = _context.TaskAssigned.FirstOrDefault(a => a.TaskId == t.TaskId && a.UserId == value.ModifiedByUserID);
                        if (taskassigned != null)
                        {
                            if (taskassigned.IsRead == false)
                            {
                                taskassigned.IsRead = true;
                            }


                        }

                        var taskCC = _context.TaskMembers.FirstOrDefault(a => a.AssignedCc == value.ModifiedByUserID && a.TaskId == t.TaskId);
                        if (taskCC != null)
                        {
                            if (taskCC.IsRead == false)
                            {
                                taskCC.IsRead = true;
                            }
                        }

                        _context.SaveChanges();

                    });

                }

            }
            return taskMasterModel;
        }


        [HttpPut]
        [Route("MultipleTaskUpdateAsUnRead")]
        public TaskMasterModel MultipleTaskUpdateAsUnRead(TaskMasterModel value)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            var taskmembers = _context.TaskMembers.Where(a => value.TaskIds.Contains(a.TaskId)).AsNoTracking().ToList();
            var taskassignedList = _context.TaskAssigned.Where(a => a.UserId == value.ModifiedByUserID && value.TaskIds.Contains(a.TaskId)).ToList();
            if (value.TaskIds.Count > 0)
            {
                var taskMaster = _context.TaskMaster.Where(t => value.TaskIds.Contains(t.TaskId)).AsNoTracking().ToList();
                if (taskMaster != null && taskMaster.Count > 0)
                {
                    taskMaster.ForEach(t =>
                    {
                        if (t.OnBehalf == value.ModifiedByUserID || t.OwnerId == value.ModifiedByUserID || t.AddedByUserId == value.ModifiedByUserID)
                        {
                            var task = _context.TaskMaster.FirstOrDefault(d => d.TaskId == t.TaskId);
                            if (task != null)
                            {
                                //if (task.IsRead == true)
                                //{
                                task.IsRead = false;
                                //}
                                _context.SaveChanges();
                            }
                        }

                        var taskassigned = _context.TaskAssigned.FirstOrDefault(a => a.TaskId == t.TaskId && a.UserId == value.ModifiedByUserID);
                        if (taskassigned != null)
                        {
                            if (taskassigned.IsRead == true)
                            {
                                taskassigned.IsRead = false;
                            }
                            _context.SaveChanges();

                        }

                        var taskCC = _context.TaskMembers.FirstOrDefault(a => a.AssignedCc == value.ModifiedByUserID && a.TaskId == t.TaskId);
                        if (taskCC != null)
                        {
                            if (taskCC.IsRead == true)
                            {
                                taskCC.IsRead = false;
                            }
                        }

                        _context.SaveChanges();

                    });

                }

            }
            return taskMasterModel;
        }
        [HttpPut]
        [Route("DragDropTask")]
        public TaskMasterModel DragDropTask(TaskMasterModel value)
        {
            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == value.TaskID);
            if (taskMaster != null)
            {
                //if (taskMaster.ParentTaskId != value.ParentTaskID)
                //{
                if (value.ParentTaskID != null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.ParentTaskID);
                    var taskNumber = "1";
                    if (tasklevel == null)
                    {
                        tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.ParentTaskID);
                        taskNumber = tasklevel.TaskNumber + ".1";
                    }
                    else
                    {
                        var numberarray = tasklevel.TaskNumber.Split('.');
                        var number = numberarray[numberarray.Length - 1].ToString();
                        var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                        var taskNo = tasklevel.TaskNumber.Remove(leng);
                        number = (double.Parse(number) + 1).ToString();
                        taskNumber = taskNo + "." + number;
                    }
                    taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                    taskMaster.TaskNumber = taskNumber;
                }
                else if (value.ParentTaskID == null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.MainTaskId == value.MainTaskId);
                    var level = tasklevel.TaskLevel + 1;
                    taskMaster.TaskLevel = level;
                    taskMaster.TaskNumber = level.ToString();
                }


                taskMaster.ParentTaskId = value.ParentTaskID;
                taskMaster.ModifiedByUserId = value.ModifiedByUserID;
                taskMaster.ModifiedDate = DateTime.Now;
                taskMaster.IsNotifyByMessage = value.IsNotifyByMessage;
                taskMaster.IsEmail = value.IsEmail;
                _context.SaveChanges();
                if (value.Children.Count > 0)
                {
                    var childerns = ChildrenOf(value);
                    int ignoreFirst = 0;
                    childerns.ToList().ForEach(task =>
                    {
                        if (ignoreFirst > 0)
                        {
                            DragDropTaskTreeLevel(task);
                        }
                        ignoreFirst++;
                    });
                }
            }
            //}
            return value;
        }
        private TaskMasterModel DragDropTaskTreeLevel(TaskMasterModel value)
        {
            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == value.TaskID);
            if (taskMaster != null)
            {
                if (value.ParentTaskID != null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.ParentTaskID);
                    var taskNumber = "1";
                    taskNumber = tasklevel.TaskNumber + ".1";
                    taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                    taskMaster.TaskNumber = taskNumber;
                }
                else if (value.ParentTaskID == null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.MainTaskId == value.MainTaskId);
                    var level = tasklevel.TaskLevel + 1;
                    taskMaster.TaskLevel = level;
                    taskMaster.TaskNumber = level.ToString();
                }


                taskMaster.ParentTaskId = value.ParentTaskID;
                taskMaster.ModifiedByUserId = value.ModifiedByUserID;
                taskMaster.ModifiedDate = DateTime.Now;
                taskMaster.IsNotifyByMessage = value.IsNotifyByMessage;
                taskMaster.IsEmail = value.IsEmail;
                _context.SaveChanges();


            }
            return value;
        }

        public IEnumerable<TaskMasterModel> ChildrenOf(TaskMasterModel root)
        {
            yield return root;
            foreach (var c in root.Children)
                foreach (var cc in ChildrenOf(c))
                    yield return cc;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskMaster")]
        public TaskMasterModel Put(TaskMasterModel value)
        {
            if (value.IsNoDueDate == true)
            {
                value.DueDate = DateTime.Now.AddYears(20);
            }
            value.VersionSessionId ??= Guid.NewGuid();

            var applicationUsers = _context.Employee.Include("Designation").Include(s => s.User).Where(s => s.User.StatusCodeId == 1).ToList();
            var taskMaster = _context.TaskMaster.Where(p => p.TaskId == value.TaskID).FirstOrDefault();
            taskMaster.Title = value.Title;
            taskMaster.ParentTaskId = value.ParentTaskID;
            taskMaster.AssignedTo = value.AssignedTo[0];
            taskMaster.DueDate = value.DueDate;
            taskMaster.Description = value.Description;
            taskMaster.ProjectId = value.ProjectID;
            if (taskMaster.SessionId == null)
            {
                taskMaster.SessionId = Guid.NewGuid();
            }
            if (taskMaster.SessionId != null)
            {
                value.SessionID = taskMaster.SessionId;
            }
            taskMaster.FollowUp = value.FollowUp;
            taskMaster.IsNoDueDate = value.IsNoDueDate;
            taskMaster.VersionSessionId = value.VersionSessionId;
            // taskMaster.DiscussionDate = value.DiscussionDate;
            //taskMaster.Version = "1";
            taskMaster.WorkTypeId = 37;
            taskMaster.RequestTypeId = 58;
            taskMaster.IsRead = false;
            taskMaster.IsUrgent = value.IsUrgent;
            taskMaster.IsAutoClose = value.IsAutoClose;
            taskMaster.OverDueAllowed = value.OverDueAllowed == null ? "false" : value.OverDueAllowed;
            taskMaster.OnBehalf = value.OnBehalfID;
            // added no followup changes //
            taskMaster.OwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? taskMaster.OwnerId : value.OnBehalfID : taskMaster.OwnerId;
            taskMaster.AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? taskMaster.AddedByUserId : value.OnBehalfID : taskMaster.AddedByUserId;
            // end///
            taskMaster.StatusCodeId = value.StatusCodeID;
            taskMaster.ModifiedByUserId = value.ModifiedByUserID;
            taskMaster.ModifiedDate = DateTime.Now;
            taskMaster.IsNotifyByMessage = value.IsNotifyByMessage;
            taskMaster.IsEmail = value.IsEmail;
            taskMaster.IsAllowEditContent = value.IsAllowEditContent;
            var taskmembers = _context.TaskMembers.Where(l => l.TaskId == value.TaskID).ToList();
            if (taskmembers.Count > 0)
            {
                _context.TaskMembers.RemoveRange(taskmembers);
                _context.SaveChanges();
            }

            var taskAssinged = _context.TaskAssigned.Where(l => l.TaskId == value.TaskID).ToList();
            if (taskAssinged.Count > 0)
            {
                _context.TaskAssigned.RemoveRange(taskAssinged);
                _context.SaveChanges();
            }

            if (value.AssignedTo != null && value.AssignedTo.Count > 0)
            {
                value.AssignedTo.ForEach(c =>
                {
                    var assigned = new TaskAssigned
                    {
                        UserId = c,
                        StatusCodeId = value.StatusCodeID,
                        DueDate = value.DueDate,
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? taskMaster.OwnerId : value.OnBehalfID : taskMaster.OwnerId,
                        IsRead = false,
                        StartDate = value.StartDate,
                        OnBehalfId = value.OnBehalfID,
                    };
                    taskMaster.TaskAssigned.Add(assigned);
                });
            }
            if (value.AssignedCC != null && value.AssignedCC.Count > 0)
            {
                value.AssignedCC.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var member = new TaskMembers
                        {
                            AssignedCc = c,
                            IsRead = false,
                            DueDate = value.DueDate,
                            StatusCodeId = value.StatusCodeID,
                        };
                        taskMaster.TaskMembers.Add(member);
                    }
                });
            }
            if (value.AssignedRights != null && value.AssignedRights.Count > 0)
            {
                value.AssignedRights.ForEach(c =>
                {
                    var member = new TaskMembers
                    {
                        AssignedCc = c,
                        IsRead = false,
                        DueDate = value.DueDate,
                        StatusCodeId = value.StatusCodeID,
                    };
                    taskMaster.TaskMembers.Add(member);
                });
            }

            var tags = _context.TaskTag.Where(l => l.TaskMasterId == value.TaskID).ToList();
            if (tags.Count > 0)
            {
                _context.TaskTag.RemoveRange(tags);
                _context.SaveChanges();
            }

            if (value.TagIds != null && value.TagIds.Count > 0)
            {
                value.TagIds.ForEach(t =>
                {
                    var tag = new TaskTag
                    {
                        TagId = t,
                    };
                    taskMaster.TaskTag.Add(tag);
                });
            }
            var personalTaglist = _context.PersonalTags.Where(p => p.TaskId == value.TaskID).ToList();
            personalTaglist.ForEach(tag =>
            {
                tag.TaskId = null;
            });
            _context.SaveChanges();
            if (value.PersonaltagIds != null && value.PersonaltagIds.Count > 0)
            {
                value.PersonaltagIds.ForEach(t =>
                {
                    var personalTagsNew = _context.PersonalTags.Where(p => p.TaskId == value.TaskID && p.Tag == t).FirstOrDefault();
                    if (personalTagsNew == null)
                    {
                        var personalTags = new PersonalTags
                        {
                            Tag = t,
                            TaskId = value.TaskID,
                            AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            AddedDate = value.AddedDate,

                        };
                        _context.PersonalTags.Add(personalTags);
                    }
                    else
                    {
                        personalTagsNew.TaskId = value.TaskID;
                    }
                });

            }
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == value.SessionID).ToList();
                var taskAttachments = _context.TaskAttachment.Where(a => a.TaskMasterId == value.TaskID).ToList();
                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null && !taskAttachments.Any(a => a.DocumentId == file.DocumentId))
                    {
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            VersionNo = "1",
                            CheckInDescription = value.DocumentDescription,
                            //TaskMasterId = value.TaskID
                        };
                        taskMaster.TaskAttachment.Add(taskAttach);
                    }
                    if (value.ReadonlyUserID != null)
                    {
                        value.ReadonlyUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                    if (value.ReadWriteUserID != null)
                    {
                        value.ReadWriteUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                });
            }
            var taskComment = _context.TaskComment.Where(t => t.TaskMasterId == value.TaskID).FirstOrDefault();
            if (taskComment != null)
            {
                if (value.IsEmail.GetValueOrDefault(false))
                {
                    taskComment.Comment = value.Description;
                }
            }
            else
            {
                var taskComments = new TaskComment
                {
                    TaskMasterId = value.TaskID,
                    TaskSubject = value.Title,
                    Comment = value.Description,
                    CommentedBy = value.AddedByUserID,
                    CommentedDate = DateTime.Now,
                    IsRead = false,
                    IsEdited = false,
                    IsDocument = false,
                };
                _context.TaskComment.Add(taskComments);
                _context.SaveChanges();
                taskComment = _context.TaskComment.Where(t => t.TaskCommentId == taskComments.TaskCommentId).FirstOrDefault();
            }

            if (value.IsEmail.GetValueOrDefault(false) && !value.IsAutoClose.GetValueOrDefault(false))
            {
                bool isDefaultConversationExits = _context.TaskComment.Any(s => s.TaskMasterId == value.TaskID && s.TaskSubject.Trim().ToLower() == "E-" + value.Title.Trim().ToLower());
                if (!isDefaultConversationExits)
                {
                    CreateDefaultConversation(value);
                }
            }
            if (value.AssignedCC != null)
            {
                value.AssignedCC.ForEach(u =>
                {
                    var taskcommentExituser = _context.TaskCommentUser.Where(t => t.TaskMasterId == value.TaskID && t.UserId == u).FirstOrDefault();
                    if (taskcommentExituser == null)
                    {
                        var userComment = new TaskCommentUser
                        {
                            IsRead = false,
                            IsClosed = false,
                            TaskCommentId = taskComment.TaskCommentId,
                            TaskMasterId = value.TaskID,
                            UserId = u.Value,
                            StatusCodeId = value.StatusCodeID,
                        };
                        _context.TaskCommentUser.Add(userComment);
                    }
                    _context.SaveChanges();
                });
            }
            _context.SaveChanges();
            value.OwnerID = taskMaster.OwnerId;
            value.AddedByUserID = taskMaster.AddedByUserId;
            value.AssignToNames = value.AssignedTo != null ? string.Join(",", applicationUsers.Where(a => value.AssignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
            value.AssignCCNames = value.AssignedCC != null ? string.Join(",", applicationUsers.Where(a => value.AssignedCC.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
            value.OnBehalfName = value.OnBehalfID != null ? applicationUsers.Where(a => a.UserId == value.OnBehalfID).Select(a => a.NickName).FirstOrDefault() : "";
            var TaskMasterDescriptionVersionNo = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == value.VersionSessionId).Count();
            var TaskMasterDescriptionVersion = new TaskMasterDescriptionVersion
            {
                VersionNo = TaskMasterDescriptionVersionNo == 0 ? 1 : (TaskMasterDescriptionVersionNo + 1),
                SessionId = value.VersionSessionId,
                Description = value.Description,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now
            };
            _context.TaskMasterDescriptionVersion.Add(TaskMasterDescriptionVersion);
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateTaskAsUnread")]
        public TaskMasterModel UpdatetaskRead(TaskMasterModel value)
        {

            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("SetDocAccess")]
        public DocumentRightsModel SetDocAccess(DocumentRightsModel value)
        {
            if (value.DocumentID != null)
            {
                var rights = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();

                if (rights.Count() > 0)
                {
                    _context.DocumentRights.RemoveRange(rights);
                    _context.SaveChanges();
                }
                var file = _context.Documents.Select(s => new { s.DocumentId, s.DocumentRights }).FirstOrDefault(d => d.DocumentId == value.DocumentID);

                if (value.ReadOnlyUserID != null)
                {
                    value.ReadOnlyUserID.ForEach(i =>
                    {
                        var docAccess = new DocumentRights
                        {
                            IsRead = true,
                            IsReadWrite = false,
                            UserId = i,
                            DocumentId = value.DocumentID,
                        };
                        _context.DocumentRights.Add(docAccess);
                    });
                }
                if (value.ReadWriteUserID != null)
                {
                    value.ReadWriteUserID.ForEach(i =>
                    {
                        var docAccess = new DocumentRights
                        {
                            IsRead = false,
                            IsReadWrite = true,
                            UserId = i,
                            DocumentId = value.DocumentID,
                        };
                        _context.DocumentRights.Add(docAccess);
                    });
                }

            }
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateSubTaskMaster")]
        public TaskMasterModel PutSubTask(TaskMasterModel value)
        {
            if (value.IsNoDueDate == true)
            {
                value.DueDate = DateTime.Now.AddYears(20);
            }

            var taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == value.TaskID);
            taskMaster.Title = value.Title;
            //taskMaster.ParentTaskId = value.ParentTaskID;
            taskMaster.AssignedTo = value.AssignedTo[0];
            if (taskMaster.SessionId == null)
            {
                value.SessionID = Guid.NewGuid();
            }
            taskMaster.DueDate = value.DueDate;
            taskMaster.Description = value.Description;
            taskMaster.ProjectId = value.ProjectID;
            taskMaster.SessionId = value.SessionID;
            taskMaster.StatusCodeId = value.StatusCodeID;
            //taskMaster.DiscussionDate = value.DiscussionDate;
            taskMaster.FollowUp = value.FollowUp;
            taskMaster.Version = "1";
            taskMaster.WorkTypeId = 37;
            taskMaster.RequestTypeId = 58;
            taskMaster.OverDueAllowed = value.OverDueAllowed;
            taskMaster.OnBehalf = value.OnBehalfID;
            taskMaster.IsNoDueDate = value.IsNoDueDate;
            //taskMaster.IsRead = value.IsRead;
            taskMaster.ModifiedByUserId = value.ModifiedByUserID;
            taskMaster.ModifiedDate = DateTime.Now;
            taskMaster.IsNotifyByMessage = value.IsNotifyByMessage;
            taskMaster.IsEmail = value.IsEmail;
            taskMaster.IsAllowEditContent = value.IsAllowEditContent;
            var taskmembers = _context.TaskMembers.Where(l => l.TaskId == value.TaskID).ToList();
            if (taskmembers.Count > 0)
            {
                _context.TaskMembers.RemoveRange(taskmembers);
                _context.SaveChanges();
            }
            var taskAssinged = _context.TaskAssigned.Where(l => l.TaskId == value.TaskID).ToList();
            if (taskAssinged.Count > 0)
            {
                _context.TaskAssigned.RemoveRange(taskAssinged);
                _context.SaveChanges();
            }

            if (value.AssignedTo != null && value.AssignedTo.Count > 0)
            {
                value.AssignedTo.ForEach(c =>
                {
                    var assigned = new TaskAssigned
                    {
                        UserId = c,
                        StatusCodeId = value.StatusCodeID,
                        DueDate = value.DueDate,
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = value.AddedByUserID,
                        IsRead = false,
                        StartDate = value.StartDate,
                        OnBehalfId = value.OnBehalfID,
                    };
                    taskMaster.TaskAssigned.Add(assigned);
                });
            }

            if (value.AssignedCC != null && value.AssignedCC.Count > 0)
            {
                value.AssignedCC.ForEach(c =>
                {
                    if (c != null && c > 0)
                    {
                        var member = new TaskMembers
                        {
                            AssignedCc = c,
                            IsRead = false,
                            DueDate = value.DueDate,
                            StatusCodeId = value.StatusCodeID,
                        };
                        taskMaster.TaskMembers.Add(member);
                    }
                });
            }
            if (value.AssignedRights != null && value.AssignedRights.Count > 0)
            {
                value.AssignedRights.ForEach(c =>
                {
                    var member = new TaskMembers
                    {
                        AssignedCc = c,
                        IsRead = false,
                        DueDate = value.DueDate,
                        StatusCodeId = value.StatusCodeID,
                    };
                    taskMaster.TaskMembers.Add(member);
                });
            }
            if (value.TagIds != null && value.TagIds.Count > 0)
            {
                var tags = _context.TaskTag.Where(l => l.TaskMasterId == value.TaskID).ToList();
                if (tags.Count > 0)
                {
                    _context.TaskTag.RemoveRange(tags);
                    _context.SaveChanges();
                }

                value.TagIds.ForEach(t =>
                {
                    var tag = new TaskTag
                    {
                        TagId = t,
                    };
                    taskMaster.TaskTag.Add(tag);
                });
            }
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == value.SessionID).ToList();
                docu.ForEach(file =>
                {
                    file.IsTemp = false;
                    if (docu != null)
                    {
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = file.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            //TaskMasterId = value.TaskID
                        };
                        taskMaster.TaskAttachment.Add(taskAttach);
                    }
                    if (value.ReadonlyUserID != null && value.ReadonlyUserID.Count > 0)
                    {
                        value.ReadonlyUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                    if (value.ReadWriteUserID != null && value.ReadWriteUserID.Count > 0)
                    {
                        value.ReadWriteUserID.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                            };
                            file.DocumentRights.Add(docAccess);
                        });
                    }
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("CompleteTaskSubject")]
        public TaskMasterModel CompleteTaskSubject(TaskMasterModel value)
        {
            int status = 517;
            if (value.TaskCommentId != null)
            {
                //var completecommentedByuser = _context.TaskComment.Where(t => t.TaskCommentId == value.TaskCommentId).FirstOrDefault();
                //if (completecommentedByuser != null)
                //{
                //    var completeComment = _context.TaskCommentUser.FirstOrDefault(w => w.TaskCommentId == completecommentedByuser.TaskCommentId && w.UserId == value.ModifiedByUserID && w.IsAssignedTo == true);
                //    if(completeComment!=null)
                //    {
                //        completeComment.StatusCodeId = 513;
                //        _context.SaveChanges();
                //    }
                //}
            }
            return value;
        }
        [HttpPut]
        [Route("CompleteTask")]
        public TaskMasterModel CompleteTask(TaskMasterModel value)
        {
            int status = 517;
            if (value.TaskCommentId != null)
            {
                var completecommentedByuser = _context.TaskComment.Where(t => t.TaskCommentId == value.TaskCommentId && t.CommentedBy == value.ModifiedByUserID && t.TaskMasterId == value.TaskID).FirstOrDefault();
                if (completecommentedByuser != null)
                {
                    var compleComment = _context.TaskCommentUser.Where(w => w.TaskCommentId == completecommentedByuser.TaskCommentId).ToList();
                    compleComment.ForEach(c =>
                    {
                        c.StatusCodeId = 513;
                    });
                }
            }
            _context.SaveChanges();
            var taskCommentUsers = _context.TaskCommentUser.Where(f => f.TaskMasterId == value.TaskID && f.UserId == value.ModifiedByUserID).ToList();
            var allCompleted = taskCommentUsers.Count > 0 ? taskCommentUsers.All(c => c.StatusCodeId == 513) : true;
            if (!allCompleted)
            {
                status = 512;
                if (value.TaskCommentId == null)
                {
                    throw new AppException("User unable to complete this task, please make sure all subject is completed by assignee", null);
                }
            }
            else
            {
                var taskAssigned = _context.TaskAssigned.FirstOrDefault(a => a.UserId == value.ModifiedByUserID && a.TaskId == value.TaskID);
                if (taskAssigned != null)
                {
                    taskAssigned.StatusCodeId = 513;
                    taskAssigned.CompletedDate = DateTime.Now;
                    taskAssigned.IsRead = false;
                }

                _context.SaveChanges();

                var taskMaster = _context.TaskMaster.Include("TaskAssigned").SingleOrDefault(p => p.TaskId == value.TaskID);
                if (taskMaster.IsAutoClose.HasValue && taskMaster.IsAutoClose == true)
                {
                    var any = taskMaster.TaskAssigned.All(t => t.StatusCodeId == 513);
                    if (any)
                    {
                        var messagecount = _context.TaskComment.Count(c => c.TaskMasterId == value.TaskID);
                        status = 513;
                        if (messagecount == 0)
                        {
                            status = 516;

                            var discuss = _context.TaskAppointment.FirstOrDefault(t => t.TaskMasterId == taskMaster.TaskId);
                            if (discuss != null)
                            {
                                var disTask = _context.TaskMaster.FirstOrDefault(s => s.TaskId == discuss.SubTaskId);
                                disTask.StatusCodeId = 516;
                                disTask.ModifiedByUserId = value.ModifiedByUserID;
                                disTask.ModifiedDate = DateTime.Now;
                            }

                        }
                    }
                }
                else
                {
                    var allclose = taskMaster.TaskAssigned.All(t => t.StatusCodeId == 513);
                    if (allclose)
                    {
                        status = 513;
                        var discuss = _context.TaskAppointment.FirstOrDefault(t => t.TaskMasterId == taskMaster.TaskId);
                        if (discuss != null)
                        {
                            var disTask = _context.TaskMaster.FirstOrDefault(s => s.TaskId == discuss.SubTaskId);
                            disTask.StatusCodeId = 513;
                            disTask.ModifiedByUserId = value.ModifiedByUserID;
                            disTask.ModifiedDate = DateTime.Now;
                        }
                    }
                }
                taskMaster.StatusCodeId = status;
                taskMaster.ModifiedByUserId = value.ModifiedByUserID;
                taskMaster.ModifiedDate = DateTime.Now;
                taskMaster.IsRead = false;


                _context.SaveChanges();

                if (status == 516)
                {

                    CloseTask(value);
                }
            }
            value.StatusCode = _context.CodeMaster.FirstOrDefault(c => c.CodeId == status).CodeValue;
            return value;

        }

        [HttpPut]
        [Route("CloseTask")]
        public TaskMasterModel CloseTask(TaskMasterModel value)
        {
            var codeMaster = _context.CodeMaster.FirstOrDefault(c => c.CodeId == 516);
            var taskmas = _context.TaskMaster.FirstOrDefault(t => t.TaskId == value.TaskID);
            if (taskmas != null)
            {
                taskmas.StatusCodeId = 516;

                var taskAssigned = _context.TaskAssigned.Where(a => a.TaskId == taskmas.TaskId).ToList();
                taskAssigned.ForEach(a =>
                {
                    a.StatusCodeId = 516;
                });

                var allclose = _context.TaskCommentUser.Where(w => w.TaskMasterId == value.TaskID).ToList();
                allclose.ForEach(c =>
                {
                    c.StatusCodeId = 516;
                });
            }
            var taskapp = _context.TaskAppointment.FirstOrDefault(a => a.TaskMasterId == value.TaskID);

            if (taskapp != null)
            {
                var appoint = _context.TaskMaster.FirstOrDefault(ta => ta.TaskId == taskapp.SubTaskId);
                appoint.StatusCodeId = 516;
            }

            var taskFiles = _context.TaskAttachment.Count(c => c.TaskMasterId == value.TaskID);
            if (taskFiles > 0)
            {
                var title = "Automated task for DMS files arrangement  " + taskmas.Title;
                var taskobjexist = _context.TaskMaster.FirstOrDefault(w => w.Title == title);
                if (taskobjexist == null)
                {
                    var task = new TaskMaster()
                    {
                        Title = title,
                        Description = "Automated task created to arrange DMS files",
                        ParentTaskId = value.TaskID,
                        MainTaskId = taskmas.MainTaskId,
                        AssignedTo = taskmas.AddedByUserId,
                        DueDate = DateTime.Now.AddDays(7),
                        OwnerId = taskmas.AddedByUserId,
                        AddedByUserId = taskmas.AddedByUserId,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 512,
                        IsNotifyByMessage = value.IsNotifyByMessage,
                        IsEmail = value.IsEmail,
                    };
                    if (value.TaskID > 0 && taskmas.MainTaskId != null)
                    {
                        var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.TaskID);
                        var taskNumber = "1";
                        if (tasklevel == null)
                        {
                            tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.TaskID);
                            taskNumber = tasklevel.TaskNumber + ".1";
                        }
                        else
                        {
                            var numberarray = tasklevel.TaskNumber.Split('.');
                            var number = numberarray[numberarray.Length - 1].ToString();
                            var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                            var taskNo = tasklevel.TaskNumber.Remove(leng);
                            number = (double.Parse(number) + 1).ToString();
                            taskNumber = taskNo + "." + number;
                        }
                        task.TaskLevel = tasklevel.TaskLevel + 1;
                        task.TaskNumber = taskNumber;
                    }
                    var assigned = new TaskAssigned
                    {
                        UserId = taskmas.AddedByUserId,
                        StatusCodeId = 512,
                        DueDate = DateTime.Now.AddDays(7),
                        AssignedDate = DateTime.Now,
                        TaskOwnerId = taskmas.AddedByUserId,
                        IsRead = false,
                        StartDate = value.StartDate,
                    };
                    task.TaskAssigned.Add(assigned);
                    _context.TaskMaster.Add(task);
                }

            }

            _context.SaveChanges();
            value.StatusCodeID = codeMaster.CodeId;
            value.StatusCode = codeMaster.CodeValue;
            return value;

        }
        [HttpPut]
        [Route("DuplicateTask")]
        public TaskMaster DuplicateTask(TaskMasterModel value)
        {
            var taskMaster = new TaskMaster();
            value.TaskMergedIds.ForEach(i =>
            {
                taskMaster = _context.TaskMaster.SingleOrDefault(p => p.TaskId == i);
                taskMaster.StatusCodeId = 516;
                taskMaster.Title = "Duplicate " + taskMaster.Title;
                taskMaster.ModifiedByUserId = value.ModifiedByUserID;
                taskMaster.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            });
            return taskMaster;

        }
        [HttpPut]
        [Route("UpdateTaskLink")]
        public TaskMasterModel PutTaskLink(TaskMasterModel value)
        {
            if (value.TaskLinkedIds.Count > 0)
            {
                var tasklinks = _context.TaskLinks.Where(l => l.TaskLinkId == value.TaskID).ToList();
                if (tasklinks.Count > 0)
                {
                    _context.TaskLinks.RemoveRange(tasklinks);
                    _context.SaveChanges();
                }

            }

            _context.SaveChanges();

            return value;
        }
        [HttpPut]
        [Route("CheckOutDoc")]
        public TaskAttachmentModel CheckOutDoc(TaskAttachmentModel value)
        {
            if (value.TaskAttachmentID > 0)
            {
                var attachment = _context.TaskAttachment.FirstOrDefault(l => l.TaskAttachmentId == value.TaskAttachmentID);
                if (attachment != null)
                {
                    var isFromProfileDocument = _context.TaskMaster.FirstOrDefault(f => f.TaskId == attachment.TaskMasterId)?.IsFromProfileDocument;
                    if (isFromProfileDocument != null && isFromProfileDocument == true)
                    {
                        var fileProfileDoc = _context.Documents.Where(d => d.DocumentId == attachment.DocumentId && d.FilterProfileTypeId != null)?.FirstOrDefault();
                        if (fileProfileDoc != null)
                        {
                            fileProfileDoc.IsLocked = true;
                            fileProfileDoc.LockedDate = DateTime.Now;
                            fileProfileDoc.LockedByUserId = value.AddedByUserID;
                        }
                    }


                }

                var fileaccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (fileaccess.Count() > 0)
                {
                    var rights = fileaccess.Where(a => a.UserId == value.AddedByUserID);
                    if (rights.Count() > 0)
                    {
                        if (rights.Any(a => a.IsReadWrite.Value))
                        {
                            if (attachment != null)
                            {
                                attachment.IsLocked = true;
                                attachment.LockedDate = DateTime.Now;
                                attachment.LockedByUserId = value.AddedByUserID;
                            }
                        }
                    }
                    else
                    {
                        if (attachment.UploadedByUserId != value.AddedByUserID)
                        {
                            throw new AppException("Required permission to read this file.!", null);
                        }
                        else
                        {
                            if (attachment != null)
                            {
                                attachment.IsLocked = true;
                                attachment.LockedDate = DateTime.Now;
                                attachment.LockedByUserId = value.AddedByUserID;
                            }
                        }
                    }
                }
                else
                {
                    if (attachment != null)
                    {
                        attachment.IsLocked = true;
                        attachment.LockedDate = DateTime.Now;
                        attachment.LockedByUserId = value.AddedByUserID;
                    }
                }
                if (attachment.DocumentId > 0)
                {
                    var documentfolder = _context.DocumentFolder.FirstOrDefault(d => d.DocumentId == attachment.DocumentId);
                    if (documentfolder != null)
                    {
                        documentfolder.IsLocked = true;
                        documentfolder.LockedByUserId = value.AddedByUserID;
                        documentfolder.LockedDate = DateTime.Now;
                    }
                    var query = string.Format("Update DocumentFolder Set IsLocked = 1 Where DocumentId in" + '(' + "{0}" + ')', attachment.DocumentId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                }

                _context.SaveChanges();
            }
            return value;
        }
        [HttpPost]
        [Route("BulkTransferTask")]
        public BulkTransferTaskModel BulkTransferTask(BulkTransferTaskModel value)
        {

            var taskassigned = _context.TaskAssigned.Where(l => l.UserId == value.TransferFromUserId).ToList();

            taskassigned.ForEach(ta =>
            {
                ta.UserId = value.TransferToUserId;
            });

            var taskMember = _context.TaskMembers.Where(l => l.AssignedCc == value.TransferFromUserId).ToList();
            taskMember.ForEach(ta =>
            {
                ta.AssignedCc = value.TransferToUserId;
            });

            var taskMaster = _context.TaskMaster.Where(l => l.OnBehalf == value.TransferFromUserId).ToList();
            taskMaster.ForEach(ta =>
            {
                ta.OnBehalf = value.TransferToUserId;
                ta.AddedByUserId = value.TransferToUserId;
            });

            var DocumentRights = _context.DocumentRights.Where(l => l.UserId == value.TransferFromUserId).ToList();

            DocumentRights.ForEach(ta =>
            {
                ta.UserId = value.TransferToUserId;
            });

            _context.SaveChanges();

            var taskMasterowner = _context.TaskMaster.Where(l => l.OwnerId == value.TransferFromUserId).ToList();
            taskMasterowner.ForEach(ta =>
            {
                ta.OwnerId = value.TransferToUserId;
                ta.AddedByUserId = value.TransferToUserId;
            });

            var taskassignedowner = _context.TaskAssigned.Where(l => l.TaskOwnerId == value.TransferFromUserId).ToList();

            taskassignedowner.ForEach(ta =>
            {
                ta.TaskOwnerId = value.TransferToUserId;
            });
            var DiscussionNotes = _context.TaskDiscussion.Where(p => p.UserId == value.TransferFromUserId).ToList();
            DiscussionNotes.ForEach(d =>
            {
                d.UserId = value.TransferToUserId;
            });

            _context.SaveChanges();
            var wikiowner = _context.WikiPage.Select(s => new WikiPage { OwnerId = s.OwnerId, PageId = s.PageId }).Where(u => u.OwnerId == value.TransferFromUserId).ToList();
            wikiowner.ForEach(ta =>
            {
                ta.OwnerId = value.TransferToUserId;
                _context.WikiPage.Attach(ta);
                _context.Entry(ta).Property(x => x.OwnerId).IsModified = true;

            });
            _context.SaveChanges();

            var wikiassine = _context.WikiPage.Select(s => new WikiPage { Assignee = s.Assignee, PageId = s.PageId }).Where(u => u.Assignee == value.TransferFromUserId).ToList();
            wikiassine.ForEach(ta =>
            {
                ta.Assignee = value.TransferToUserId;
                _context.WikiPage.Attach(ta);
                _context.Entry(ta).Property(x => x.Assignee).IsModified = true;
            });
            _context.SaveChanges();
            var wikiAccess = _context.WikiAccess.Where(u => u.UserId == value.TransferFromUserId).ToList();
            wikiAccess.ForEach(ta =>
            {
                ta.UserId = value.TransferToUserId;
            });
            var wikiAccessRight = _context.WikiAccessRight.Where(u => u.UserId == value.TransferFromUserId).ToList();
            wikiAccessRight.ForEach(ta =>
            {
                ta.UserId = value.TransferToUserId;
            });
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("CheckInDoc")]
        public TaskAttachmentModel CheckInDoc(TaskAttachmentModel value)
        {
            if (value.TaskAttachmentID > 0)
            {
                double draftingVersionNo = 0;
                double versionNo = 0;
                long? sourceId = new long?();
                var attachment = _context.TaskAttachment.FirstOrDefault(l => l.TaskAttachmentId == value.TaskAttachmentID);
                var documentItem = _context.Documents.Select(s => new
              Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, FileName = s.FileName, DocumentRights = s.DocumentRights, FileIndex = s.FileIndex, FilterProfileTypeId = s.FilterProfileTypeId, DocumentParentId = s.DocumentParentId, ProfileNo = s.ProfileNo })
                  .FirstOrDefault(d => d.DocumentId == attachment.DocumentId);
                sourceId = _context.TaskMaster.FirstOrDefault(t => t.TaskId == attachment.TaskMasterId)?.SourceId;
                var version = double.Parse(attachment.VersionNo) + 0.1;
                var doclink = new DocumentLink();

                var fileProfileDocId = documentItem?.FilterProfileTypeId;
                var docu = _context.Documents.Select(s => new
               Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights, ProfileNo = s.ProfileNo })
                   .FirstOrDefault(d => d.SessionId == value.SessionID);
                var isFromProfileTask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == attachment.TaskMasterId)?.IsFromProfileDocument;
                if (isFromProfileTask != null && isFromProfileTask == true)
                {

                    var fileProfiledocu = _context.Documents.Where(d => d.SessionId == value.SessionID).FirstOrDefault();
                    if (fileProfiledocu != null)
                    {
                        fileProfiledocu.IsLatest = true;
                        fileProfiledocu.DocumentParentId = documentItem.DocumentParentId > 0 ? documentItem.DocumentParentId : documentItem.DocumentId;
                        fileProfiledocu.FilterProfileTypeId = fileProfileDocId;
                        fileProfiledocu.TableName = "Document";
                        fileProfiledocu.IsLocked = false;
                        fileProfiledocu.ModifiedByUserId = value.AddedByUserID;
                        fileProfiledocu.ModifiedDate = DateTime.Now;
                        fileProfiledocu.ProfileNo = documentItem.ProfileNo;
                        fileProfiledocu.SessionId = documentItem.SessionId;
                        // fileProfiledocu.TaskId = attachment.TaskMasterId;
                        fileProfiledocu.IsMainTask = false;

                        _context.SaveChanges();


                        var task = _context.TaskMaster.FirstOrDefault(d => d.TaskId == attachment.TaskMasterId);

                        var notes = new Notes
                        {
                            Notes1 = task.Title,
                            Link = value.Link + "id=" + attachment.TaskMasterId + "&taskid=" + attachment.TaskMasterId,
                            DocumentId = fileProfiledocu.DocumentId,
                            SessionId = fileProfiledocu.SessionId,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,


                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();

                    }
                    var documentlinklist = _context.DocumentLink.Where(d => d.LinkDocumentId == attachment.DocumentId).AsNoTracking().ToList();
                    var checkin = "Link From Task CheckIn";
                    if (documentlinklist != null && documentlinklist.Count > 0)
                    {
                        documentlinklist.ForEach(d =>
                        {
                            var doclinkexist = _context.DocumentLink.Where(r => r.LinkDocumentId == d.LinkDocumentId)?.FirstOrDefault();
                            if (doclinkexist != null)
                            {
                                var linkparentdocId = doclinkexist.DocumentId;
                                _context.DocumentLink.Remove(doclinkexist);
                                _context.SaveChanges();
                                if (docu != null)
                                {
                                    var folderid = _context.DocumentFolder.FirstOrDefault(d => d.DocumentId == attachment.DocumentId)?.FolderId;
                                    if (linkparentdocId != docu.DocumentId)
                                    {

                                        var DocumentLink = new DocumentLink
                                        {
                                            DocumentId = linkparentdocId,
                                            AddedByUserId = value.AddedByUserID.Value,
                                            AddedDate = DateTime.Now,
                                            StatusCodeId = 1,
                                            LinkDocumentId = docu.DocumentId,
                                            FolderId = folderid,
                                            FileProfieTypeId = documentItem.FilterProfileTypeId,
                                            DocumentPath = "Document Link From Task checkin",

                                        };
                                        _context.DocumentLink.Add(DocumentLink);
                                        _context.SaveChanges();
                                    }
                                }



                            }



                        });




                    }
                    if (sourceId == attachment.DocumentId)
                    {
                        long? taskId = new long?();
                        var existProfiledocu = _context.Documents.Where(d => d.DocumentId == attachment.DocumentId).FirstOrDefault();
                        if (existProfiledocu != null)
                        {
                            taskId = existProfiledocu.TaskId;
                            existProfiledocu.IsLatest = false;
                            //existProfiledocu.TaskId = attachment.TaskMasterId;
                            existProfiledocu.IsMainTask = false;
                            existProfiledocu.TaskId = null;
                            // _context.SaveChanges();
                        }
                        var sourcetask = _context.TaskMaster.FirstOrDefault(t => t.TaskId == attachment.TaskMasterId);
                        if (sourcetask != null)
                        {
                            sourcetask.SourceId = docu.DocumentId;
                        }
                        var maindocu = _context.Documents.Where(d => d.DocumentId == docu.DocumentId).FirstOrDefault();
                        if (maindocu != null)
                        {
                            //existProfiledocu.IsLatest = false;
                            //existProfiledocu.TaskId = attachment.TaskMasterId;
                            maindocu.IsMainTask = true;
                            maindocu.TaskId = taskId;
                            if (maindocu.FilterProfileTypeId == null && fileProfileDocId != null)
                            {
                                maindocu.FilterProfileTypeId = fileProfileDocId;
                            }
                            if (existProfiledocu != null)
                            {
                                if (existProfiledocu.FolderId != null)
                                {
                                    maindocu.FolderId = existProfiledocu.FolderId;

                                }
                            }
                            _context.SaveChanges();
                        }
                        // existProfiledocu.TaskId = attachment.TaskMasterId;
                        //existProfiledocu.IsMainTask = true;
                        // _context.SaveChanges();
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == sourceId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}, DocumentPath='{4}'  Where DocumentId= {0}", sourceId, docu.DocumentId, DateTime.Now, value.AddedByUserID, checkin);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            if (rowaffected <= 0)
                            {
                                throw new Exception("Failed to update document");
                            }

                        }

                    }
                    else
                    {
                        var existProfiledocu = _context.Documents.Where(d => d.DocumentId == attachment.DocumentId).FirstOrDefault();
                        if (existProfiledocu != null)
                        {

                            existProfiledocu.IsLatest = false;
                            //existProfiledocu.TaskId = attachment.TaskMasterId;
                            existProfiledocu.IsMainTask = false;
                            //existProfiledocu.TaskId = null;
                            // _context.SaveChanges();
                        }
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == attachment.DocumentId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}, DocumentPath='{4}'  Where DocumentId= {0}", attachment.DocumentId, docu.DocumentId, DateTime.Now, value.AddedByUserID, checkin);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            if (rowaffected <= 0)
                            {
                                throw new Exception("Failed to update document");
                            }

                        }
                        if (docu != null)
                        {
                            var checkindocu = _context.Documents.Where(d => d.DocumentId == docu.DocumentId).FirstOrDefault();
                            if (checkindocu != null)
                            {
                                if (checkindocu.FilterProfileTypeId == null && fileProfileDocId != null)
                                {
                                    checkindocu.FilterProfileTypeId = fileProfileDocId;
                                }
                                if (existProfiledocu != null)
                                {
                                    if (existProfiledocu.FolderId != null)
                                    {
                                        checkindocu.FolderId = existProfiledocu.FolderId;

                                    }
                                }
                                _context.SaveChanges();
                            }
                        }
                    }
                }

                var fileIndex = documentItem.FileIndex == null ? 0 : documentItem.FileIndex;

                if (!value.IsNoChange.Value)
                {

                    if (attachment != null)
                    {

                        attachment.IsLocked = true;
                        attachment.LockedDate = DateTime.Now;
                        attachment.LockedByUserId = value.AddedByUserID;
                        attachment.IsLatest = false;
                        attachment.IsNoChange = value.IsNoChange;

                    }
                    if (value.IsOverwrite == false)
                    {
                        versionNo = double.Parse(attachment.VersionNo) + 0.1;
                    }
                    else
                    {
                        versionNo = double.Parse(attachment.VersionNo);
                    }
                    if (value.IsMajorChange.Value && value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {
                        var number = attachment.VersionNo.Split('.')[0];
                        if (value.IsOverwrite != true)
                        {
                            versionNo = double.Parse(number) + 1;

                        }
                        if (attachment.DraftingVersionNo != null)
                        {
                            //draftingVersionNo = double.Parse(attachment.DraftingVersionNo);
                            draftingVersionNo = 0;
                        }
                    }
                    else if (value.IsMajorChange.Value && !value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {
                        if (attachment.DraftingVersionNo != null)
                        {
                            var number = attachment.DraftingVersionNo != null ? attachment.DraftingVersionNo.Split('.')[0] : string.Empty;
                            draftingVersionNo = number != string.Empty ? double.Parse(number) + 1 : 0;
                        }
                        else
                        {
                            draftingVersionNo = 1;
                        }
                        versionNo = double.Parse(attachment.VersionNo);
                    }
                    else if (!value.IsMajorChange.Value && !value.IsReleaseVersion.GetValueOrDefault(false) && value.IsOverwrite == false)
                    {

                        if (attachment.DraftingVersionNo != null)
                        {
                            draftingVersionNo = 0;
                            // draftingVersionNo = double.Parse(attachment.DraftingVersionNo) + 0.1;
                            versionNo = double.Parse(attachment.VersionNo);


                        }
                        else
                        {
                            //var number = attachment.DraftingVersionNo.Split('.')[0];
                            draftingVersionNo = 0;
                            versionNo = double.Parse(attachment.VersionNo);
                        }

                        // draftingVersionNo = 0;
                    }
                    else if (value.IsOverwrite == true && value.IsReleaseVersion == true)
                    {
                        if (value.OverwriteVersionNo != null)
                        {
                            versionNo = double.Parse(value.OverwriteVersionNo);
                        }

                    }
                    else if (value.IsOverwrite == true && value.IsReleaseVersion == false)
                    {
                        if (value.OverwriteVersionNo != null && value.OverwriteVersionNo != "")
                        {
                            draftingVersionNo = double.Parse(value.OverwriteVersionNo);
                        }
                        else if (attachment.DraftingVersionNo != null)
                        {
                            draftingVersionNo = double.Parse(attachment.DraftingVersionNo);
                        }

                    }

                    if (value.SessionID != null)
                    {
                        versionNo = Math.Round(versionNo, 2);

                        if (docu != null)
                        {
                            //docu.IsTemp = false;
                            var taskAttach = new TaskAttachment
                            {
                                DocumentId = docu.DocumentId,
                                CheckInDescription = value.DocumentDescription,
                                TaskMasterId = attachment.TaskMasterId,
                                IsLatest = true,
                                IsLocked = false,
                                IsMajorChange = value.IsMajorChange,
                                IsNoChange = value.IsNoChange,
                                DraftingVersionNo = draftingVersionNo == 0 && value.IsReleaseVersion == true ? null : draftingVersionNo.ToString(),
                                ActualVersionNo = value.ActualVersionNo,
                                IsReleaseVersion = value.IsReleaseVersion,
                                UploadedDate = DateTime.Now,
                                UploadedByUserId = value.AddedByUserID,
                                VersionNo = versionNo.ToString(),
                                PreviousDocumentId = attachment.PreviousDocumentId == null ? attachment.TaskAttachmentId : attachment.PreviousDocumentId,
                            };
                            _context.TaskAttachment.Add(taskAttach);

                            _context.SaveChanges();
                            var documentFolder = _context.DocumentFolder.Where(w => w.DocumentId == attachment.DocumentId).FirstOrDefault();
                            if (documentFolder != null)
                            {
                                documentFolder.IsLatest = false;
                                _context.SaveChanges();
                                DocumentFolder folderAttach = new DocumentFolder();
                                folderAttach.CheckInDescription = documentFolder.CheckInDescription;
                                folderAttach.DocumentId = docu?.DocumentId;
                                folderAttach.FolderId = documentFolder?.FolderId;
                                folderAttach.IsLatest = true;
                                folderAttach.IsLocked = false;
                                folderAttach.IsMajorChange = value.IsMajorChange;
                                folderAttach.UploadedDate = DateTime.Now;
                                folderAttach.UploadedByUserId = value.AddedByUserID;
                                folderAttach.VersionNo = versionNo.ToString().Trim();
                                folderAttach.DraftingVersionNo = draftingVersionNo == 0 && value.IsReleaseVersion == true ? null : draftingVersionNo.ToString().Trim();
                                folderAttach.ActualVersionNo = value.ActualVersionNo?.Trim();
                                folderAttach.PreviousDocumentId = documentFolder?.PreviousDocumentId == null ? documentFolder?.DocumentFolderId : documentFolder?.PreviousDocumentId;
                                folderAttach.IsReleaseVersion = value.IsReleaseVersion;
                                folderAttach.IsNoChange = value.IsNoChange;

                                _context.DocumentFolder.Add(folderAttach);
                                var checkindocu = _context.Documents.Where(d => d.DocumentId == docu.DocumentId).FirstOrDefault();
                                if (checkindocu != null)
                                {
                                    if (checkindocu.FilterProfileTypeId == null && fileProfileDocId != null)
                                    {
                                        checkindocu.FilterProfileTypeId = fileProfileDocId;
                                    }
                                    if (checkindocu.FolderId == null)
                                    {
                                        checkindocu.FolderId = documentFolder?.FolderId;
                                    }
                                    _context.SaveChanges();
                                }
                            }
                            _context.SaveChanges();
                            var documentRights = _context.DocumentRights.Where(u => u.DocumentId == attachment.DocumentId).ToList();
                            documentRights.ForEach(dr =>
                            {
                                var docRights = new DocumentRights
                                {
                                    DocumentId = dr.DocumentId,
                                    IsRead = dr.IsRead,
                                    IsReadWrite = dr.IsReadWrite,
                                    UserId = dr.UserId,
                                };
                                docu.DocumentRights.Add(docRights);
                            });

                            var meetNotes = _context.TaskMeetingNote.Where(n => n.TaskAttachmentId == value.TaskAttachmentID).ToList();
                            meetNotes.ForEach(n =>
                            {
                                n.TaskAttachmentId = taskAttach.TaskAttachmentId;

                            });

                            var taskComments = _context.TaskComment.Where(n => n.DocumentId == value.DocumentID).ToList();
                            taskComments.ForEach(n =>
                            {
                                n.DocumentId = docu.DocumentId;

                            });



                        }
                    }
                }
                else
                {
                    var taskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.DocumentId == value.DocumentID && d.TaskAttachmentId == value.TaskAttachmentID);
                    taskAttachment.IsLatest = true;
                    taskAttachment.IsLocked = false;
                    taskAttachment.IsNoChange = value.IsNoChange;
                    taskAttachment.LockedByUserId = value.AddedByUserID;
                    taskAttachment.LockedDate = DateTime.Now;
                    _context.SaveChanges();
                }

            }
            return value;
        }

        [HttpPut]
        [Route("UpdateDescriptionTaskAttachement")]
        public UpdateDescriptionModel UpdateDescriptionTaskAttachement(UpdateDescriptionModel value)
        {


            var taskAttachement = _context.TaskAttachment.Where(p => p.TaskAttachmentId == value.TaskAttachmentID).FirstOrDefault();
            if (taskAttachement != null)
            {
                taskAttachement.CheckInDescription = value.Description;
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("FileUploadDoc")]
        public TaskAttachmentModel FileUploadDoc(TaskAttachmentModel value)
        {
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                 Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, Description = s.Description, IsTemp = false, DocumentRights = s.DocumentRights })
                     .Where(d => d.SessionId == value.SessionID).ToList();
                if (docu != null && docu.Count > 0)
                {
                    docu.ForEach(d =>
                    {

                        d.IsTemp = false;
                        d.Description = value.DocumentDescription;
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = d.DocumentId,
                            CheckInDescription = value.DocumentDescription,
                            TaskMasterId = value.TaskMasterID,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            PreviousDocumentId = null,
                        };
                        _context.TaskAttachment.Add(taskAttach);
                        if (value.ReadonlyUserID != null && value.ReadonlyUserID.Count > 0)
                        {
                            value.ReadonlyUserID.ForEach(id =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = true,
                                    IsReadWrite = false,
                                    UserId = id,
                                };
                                d.DocumentRights.Add(docAccess);
                            });
                        }

                        if (value.ReadWriteUserID != null && value.ReadWriteUserID.Count > 0)
                        {
                            value.ReadWriteUserID.ForEach(id =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = false,
                                    IsReadWrite = true,
                                    UserId = id,
                                };
                                d.DocumentRights.Add(docAccess);
                            });
                        }

                    });
                }
            }
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskMaster")]
        public ActionResult Delete(int id)
        {
            var errorMessage = "";
            try
            {
                var taskMaster = _context.TaskMaster.SingleOrDefault(t => t.TaskId == id);
                if (taskMaster != null)
                {
                    var childExist = _context.TaskMaster.FirstOrDefault(p => p.ParentTaskId == id);
                    if (childExist != null)
                    {
                        errorMessage = "Task had sub task, user unable delete this task!";
                        throw new AppException("Task had sub task, user unable delete this task!", null);
                    }
                    if (taskMaster.TaskId > 0)
                    {
                        var taskattachment = _context.TaskAttachment.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                        if (taskattachment.Count > 0)
                        {
                            _context.TaskAttachment.RemoveRange(taskattachment);
                            _context.SaveChanges();
                        }
                        var taskComment = _context.TaskComment.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                        if (taskComment.Count > 0)
                        {
                            var taskCommentUser = _context.TaskCommentUser.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                            _context.TaskCommentUser.RemoveRange(taskCommentUser);
                            _context.SaveChanges();
                            _context.TaskComment.RemoveRange(taskComment);
                            _context.SaveChanges();
                        }
                        var taskLikes = _context.TaskLikes.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                        if (taskLikes.Count > 0)
                        {
                            _context.TaskLikes.RemoveRange(taskLikes);
                            _context.SaveChanges();
                        }
                        var taskassigned = _context.TaskAssigned.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                        if (taskassigned.Count > 0)
                        {
                            _context.TaskAssigned.RemoveRange(taskassigned);
                            _context.SaveChanges();
                        }
                        var taskMembers = _context.TaskMembers.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                        if (taskMembers.Count > 0)
                        {
                            _context.TaskMembers.RemoveRange(taskMembers);
                            _context.SaveChanges();
                        }
                        var taskTag = _context.TaskTag.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                        if (taskTag.Count > 0)
                        {
                            _context.TaskTag.RemoveRange(taskTag);
                            _context.SaveChanges();
                        }
                        var tasklinks = _context.TaskLinks.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                        if (tasklinks.Count > 0)
                        {
                            _context.TaskLinks.RemoveRange(tasklinks);
                            _context.SaveChanges();
                        }
                        _context.TaskMaster.Remove(taskMaster);
                        _context.SaveChanges();

                    }
                }
                return Ok("Record Deleted.");

            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "Attached file had meeting notes, user unable delete this task!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSubTask")]
        public void DeleteSubTask(int id)
        {
            var taskMaster = _context.TaskMaster.SingleOrDefault(t => t.TaskId == id);
            if (taskMaster != null)
            {
                if (taskMaster.TaskId > 0)
                {
                    var taskattachment = _context.TaskAttachment.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                    if (taskattachment.Count > 0)
                    {
                        _context.TaskAttachment.RemoveRange(taskattachment);
                        _context.SaveChanges();
                    }
                    var taskComment = _context.TaskComment.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                    if (taskComment.Count > 0)
                    {
                        _context.TaskComment.RemoveRange(taskComment);
                        _context.SaveChanges();
                    }
                    var taskLikes = _context.TaskLikes.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                    if (taskLikes.Count > 0)
                    {
                        _context.TaskLikes.RemoveRange(taskLikes);
                        _context.SaveChanges();
                    }
                    var taskMembers = _context.TaskMembers.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                    if (taskMembers.Count > 0)
                    {
                        _context.TaskMembers.RemoveRange(taskMembers);
                        _context.SaveChanges();
                    }
                    var taskassigned = _context.TaskAssigned.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                    if (taskassigned.Count > 0)
                    {
                        _context.TaskAssigned.RemoveRange(taskassigned);
                        _context.SaveChanges();
                    }

                    var taskTag = _context.TaskTag.Where(d => d.TaskMasterId == taskMaster.TaskId).ToList();
                    if (taskTag.Count > 0)
                    {
                        _context.TaskTag.RemoveRange(taskTag);
                        _context.SaveChanges();
                    }
                    var tasklinks = _context.TaskLinks.Where(d => d.TaskId == taskMaster.TaskId).ToList();
                    if (tasklinks.Count > 0)
                    {
                        _context.TaskLinks.RemoveRange(tasklinks);
                        _context.SaveChanges();
                    }
                    _context.TaskMaster.Remove(taskMaster);
                    _context.SaveChanges();

                }


            }
        }
        [HttpDelete]
        [Route("DeleteTaskLink")]
        public void DeleteTaskLink(int id)
        {
            var taskLink = _context.TaskLinks.SingleOrDefault(t => t.TaskLinkId == id);
            {
                _context.TaskLinks.Remove(taskLink);
                _context.SaveChanges();

            }
        }

        [HttpGet]
        [Route("GetDashboard")]
        public List<TaskDashboardModel> GetDashboard(int id)
        {
            //var assignedto = _context.TaskAssigned.Count(c => c.UserId == id && c.StatusCodeId == 512);

            var taskMaster = _context.TaskAssigned
                .Where(t => t.UserId == id && t.StatusCodeId == 512).AsNoTracking().ToList();

            var assignedcc = _context.TaskMembers
                .Where(t => t.AssignedCc == id && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).Distinct().Count();

            var assignedBy = _context.TaskAssigned
                .Where(t => t.TaskOwnerId == id && t.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).Distinct().Count();

            var dashItems = new List<TaskDashboardModel>();

            var item = new TaskDashboardModel
            {
                DashboardType = "My Task",
                TotalCount = taskMaster.Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardModel
            {
                DashboardType = "Assigned By Me",
                TotalCount = assignedBy.ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardModel
            {
                DashboardType = "Task Inbox",
                TotalCount = assignedcc.ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardModel
            {
                DashboardType = "Over Due",
                TotalCount = taskMaster.Where(s => (s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpGet]
        [Route("GetIsemailDashboard")]
        public List<TaskDashboardModel> GetIsemailDashboard(int id)
        {
            var taskMasterIds = _context.TaskMaster.Where(c => c.IsEmail == true).AsNoTracking().Select(s => s.TaskId).ToList();

            var assignedto = _context.TaskAssigned
             .Where(t => t.UserId == id && taskMasterIds.Contains(t.TaskId.Value)).AsNoTracking().ToList();

            var dashItems = new List<TaskDashboardModel>();

            var item = new TaskDashboardModel
            {
                DashboardType = "My Task",
                Name = "Unread",
                TotalCount = assignedto.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            //var assignedbyMe = _context.TaskMaster
            //.Where(t => t.AddedByUserId == id && t.IsEmail == true).ToList();

            //item = new TaskDashboardModel
            //{
            //    DashboardType = "Assigned by Me",
            //    Name = "Unread",
            //    TotalCount = assignedbyMe.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            //};
            //dashItems.Add(item);

            var assignedcc = _context.TaskMembers
              .Where(t => t.AssignedCc == id && t.StatusCodeId == 512 && taskMasterIds.Contains(t.TaskId.Value)).AsNoTracking().ToList();

            item = new TaskDashboardModel
            {
                DashboardType = "CC",
                Name = "Unread",
                TotalCount = assignedcc.Where(w => w.IsRead.HasValue && w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);

            var taskInbox = _context.TaskMaster
             .Where(t => t.OnBehalf == id && taskMasterIds.Contains(t.TaskId)).AsNoTracking().ToList();

            //var taskInbox = _context.TaskMaster.Where(b => b.OnBehalf == id && b.StatusCodeId == 512 && b.IsEmail == true).Select(s => s.TaskId).Distinct().Count();
            item = new TaskDashboardModel
            {
                DashboardType = "On behalf",
                Name = "Unread",
                TotalCount = taskInbox.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpGet]
        [Route("GetDueDashboard")]
        public List<TaskDashboardModel> GetDueDashboard(int id)
        {
            //var assignedto = _context.TaskAssigned.Count(c => c.UserId == id && c.StatusCodeId == 512);

            var taskMaster = _context.TaskAssigned
                .Where(t => t.UserId == id && t.StatusCodeId == 512).AsNoTracking().ToList();



            var assignedBy = _context.TaskAssigned
                .Where(t => t.TaskOwnerId == id && t.StatusCodeId == 512).AsNoTracking().ToList();

            var dashItems = new List<TaskDashboardModel>();
            var item = new TaskDashboardModel
            {
                DashboardType = "Assigned By Me",
                TotalCount = assignedBy.Where(s => (s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardModel
            {
                DashboardType = "Assigned To Me",
                TotalCount = taskMaster.Where(s => (s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpGet]
        [Route("GetUnreadDashboard")]
        public List<TaskDashboardModel> GetUnreadDashboard(int id)
        {
            var assignedto = _context.TaskAssigned
             .Where(t => t.UserId == id).AsNoTracking().ToList();

            var dashItems = new List<TaskDashboardModel>();

            var item = new TaskDashboardModel
            {
                DashboardType = "My Task",
                Name = "Unread",
                TotalCount = assignedto.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            //var assignedbyMe = _context.TaskMaster
            //.Where(t => t.AddedByUserId == id).ToList();

            //item = new TaskDashboardModel
            //{
            //    DashboardType = "Assigned by Me",
            //    Name = "Unread",
            //    TotalCount = assignedbyMe.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            //};
            //dashItems.Add(item);

            var assignedcc = _context.TaskMembers
              .Where(t => t.AssignedCc == id && t.StatusCodeId == 512).AsNoTracking().ToList();

            item = new TaskDashboardModel
            {
                DashboardType = "CC",
                Name = "Unread",
                TotalCount = assignedcc.Where(w => w.IsRead.HasValue && w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);

            var taskInbox = _context.TaskMaster.Where(b => b.OnBehalf == id && b.IsRead == false && b.StatusCodeId == 512).AsNoTracking().Select(s => s.TaskId).Distinct().Count();
            item = new TaskDashboardModel
            {
                DashboardType = "On behalf",
                Name = "Unread",
                TotalCount = taskInbox.ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }
        [HttpGet]
        [Route("GetReminderDashboard")]
        public List<TaskMasterModel> GetReminderDashboard(int id)
        {

            var reminderTasks = _context.TaskNotes.Where(w => w.TaskUserId == id && w.IsNoReminderDate == false).AsNoTracking().ToList();
            var taskIds = reminderTasks.Select(s => s.TaskId).ToList();
            var dashItems = new List<TaskMasterModel>();

            dashItems = _context.TaskMaster.Include("AddedByUser").Where(t => taskIds.Contains(t.TaskId)).AsNoTracking()
                .Select(s => new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Name = s.Title,
                    Title = s.Title,
                    DueDate = s.DueDate,
                    //RemainderDate = reminderTasks.FirstOrDefault(t => t.TaskId == s.TaskId) != null ? reminderTasks.FirstOrDefault(t => t.TaskId == s.TaskId).RemainderDate : null,
                    AddedByUser = s.AddedByUser.UserName,
                    MainTaskId = s.MainTaskId,
                    ParentTaskID = s.ParentTaskId,
                    AddedByUserID = s.AddedByUserId
                }).ToList();


            return dashItems;
        }
        [HttpGet]
        [Route("GetTaskCommentMessageDashboard")]
        public List<TaskCommentModel> GetTaskCommentMessageDashboard(int id)
        {
            int days = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            DateTime today = DateTime.Today;
            DateTime weekStart = DateTime.Today.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(29);
            var query = _context.TaskCommentUser.Where(t => t.UserId == id).AsNoTracking().ToList();
            var alltaskcommentUser = _context.TaskCommentUser.Where(t => t.UserId != id).AsNoTracking().ToList();
            var taskCommentIds = new List<long>();
            var parentCommentIds = new List<long?>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = query.Where(w => w.UserId == id).Select(s => s.TaskCommentId).ToList();
            //if (commentIds.Any())
            //{
            //    taskCommentIds = query?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo != true).Select(s => s.TaskCommentId).Distinct().ToList();
            //    //var taskCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo != true).Select(s => s.TaskCommentId).ToList();
            //    assignedCommentIds = query?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId != id).Select(s => s.TaskCommentId).Distinct().ToList();
            //    assignedUserCommentIds = query?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId == id).Select(s => s.TaskCommentId).Distinct().ToList();
            //}
            //var assignedCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo == true).Select(s => s.TaskCommentId).ToList();
            var dashItems = new List<TaskCommentModel>();
            var subCommentItems = new List<TaskCommentModel>();
            var mainComments = new List<TaskCommentModel>();
            //if (assignedUserCommentIds.Any())
            //{
            //    taskCommentIds.AddRange(assignedUserCommentIds);
            //}
            //if (assignedCommentIds.Any())
            //{
            //    assignedCommentIds.ForEach(a =>
            //    {
            //        if (taskCommentIds.Contains(a))
            //        {
            //            taskCommentIds.Remove(a);
            //        }

            //    });
            //}

            taskCommentIds = query.Where(t => (t.UserId == id && t.IsClosed == false) || (t.IsPin == true && t.PinnedBy == id)).Select(t => t.TaskCommentId).ToList();

            if (taskCommentIds.Any())
            {
                var taskComments = _context.TaskComment.Include("TaskCommentUser").
                                    Select(s => new
                                    {
                                        s.TaskCommentId,
                                        s.TaskMasterId,
                                        s.CommentedBy,
                                        s.TaskCommentUser,
                                        s.ParentCommentId,
                                        s.TaskId,
                                        s.MainTaskId,
                                        s.TaskSubject,
                                        s.CommentedDate,

                                    }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && t.CommentedBy != id).AsNoTracking().ToList();
                var taskCommentId = taskComments.Select(s => s.TaskCommentId).ToList();
                var CommentedByIds = taskComments.Select(s => s.CommentedBy).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).AsNoTracking().ToList();

                var ParentCommentIds = taskComments.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
                var ParentComment = _context.TaskComment.Select(s => new
                {
                    s.TaskCommentId,
                    s.TaskSubject,
                }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
                var taskMasterId = taskComments.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
                var taskmaster = _context.TaskMaster.Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.DueDate,
                    s.IsNoDueDate,
                }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();
                List<long> taskIds = taskComments.Select(t => t.TaskMasterId.Value).Distinct().ToList();
                bool isAssigned = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.UserId == id);
                bool isOnBehalf = false;
                bool isAssignedCC = false;
                if (!isAssigned)
                {
                    isOnBehalf = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.OnBehalfId == id);
                }
                if (!isOnBehalf)
                {
                    isAssignedCC = _context.TaskMembers.Any(t => taskIds.Contains(t.TaskId.Value) && t.AssignedCc == id);
                }
                //List<TaskComment> taskCommentItems = taskComments.ToList();
                var selectTaskCommentIds = taskComments.Where(s => s.ParentCommentId != null).Select(s => s.TaskCommentId).Distinct().ToList();
                var selcommentIds = taskComments.GroupBy(d => new { d.ParentCommentId, d.TaskCommentId }).Select(s => new TaskCommentModel
                {
                    TaskCommentID = s.Key.TaskCommentId,
                    ParentCommentId = s.Key.ParentCommentId,

                });
                foreach (var s in taskComments)
                {
                    var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                    TaskCommentModel taskCommentModel = new TaskCommentModel();
                    taskCommentModel.TaskId = s.TaskId;
                    taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                    taskCommentModel.TaskMasterID = s.TaskMasterId;
                    taskCommentModel.TaskCommentID = s.TaskCommentId;
                    // taskCommentModel.Comment = s.Comment;
                    taskCommentModel.MainTaskId = s.MainTaskId;
                    taskCommentModel.TaskName = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.Title;
                    taskCommentModel.TaskDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.DueDate;
                    taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                    taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                    taskCommentModel.CommentedByName = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.CommentedBy)?.UserName : "";
                    taskCommentModel.CommentedBy = s.CommentedBy;
                    taskCommentModel.ParentCommentId = s.ParentCommentId;
                    taskCommentModel.LastCommentedDate = s.CommentedDate;

                    //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                    taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();
                    taskCommentModel.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                    dashItems.Add(taskCommentModel);
                }
                dashItems.ForEach(d =>
                {
                    if (d.IsNoDueDate == false)
                    {
                        if (d.ParentCommentId != null)
                        {
                            d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.ParentCommentId && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                        }
                        else
                        {
                            d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.TaskCommentID && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                        }
                    }

                });
                mainComments = dashItems?.Where(d => d.ParentCommentId == null).ToList();

                subCommentItems = dashItems?.Where(d => d.ParentCommentId != null).ToList();
                //dashItems.ForEach(d =>
                //{
                //    if (d.ParentCommentId != null)
                //    {
                //        var subcomment = subCommentItems?.Where(s => s.ParentCommentId == d.ParentCommentId).ToList();
                //        if (subcomment != null && subcomment.Count > 0)
                //        {
                //            if (subcomment.Count > 1)
                //            {
                //                var selsubcomment = subcomment.LastOrDefault();
                //                if (!mainComments.Exists(m => m.ParentCommentId == d.ParentCommentId))
                //                {
                //                    var existingmain = mainComments.Where(m => m.TaskCommentID == d.ParentCommentId).FirstOrDefault();
                //                    if (existingmain == null)
                //                    {
                //                        mainComments.Add(selsubcomment);
                //                    }
                //                }

                //            }
                //            else
                //            {
                //                if (!mainComments.Exists(m => m.ParentCommentId == d.ParentCommentId))
                //                {
                //                    var existingmain = mainComments.Where(m => m.TaskCommentID == d.ParentCommentId).FirstOrDefault();
                //                    if (existingmain == null)
                //                    {
                //                        mainComments.AddRange(subcomment);
                //                    }
                //                }
                //            }
                //        }
                //    }

                //});

            }
            dashItems = dashItems.Where(t => (t.SubjectDueDate <= weekEnd && t.SubjectDueDate >= weekStart) || (t.TaskDueDate <= weekEnd && t.TaskDueDate >= weekStart)).ToList();
            if (dashItems.Count > 0)
            {
                dashItems.ForEach(d =>
                {
                    if (d.SubjectDueDate <= today.AddDays(7))
                    {
                        d.DueIn = "1 Week";
                    }
                    if (d.SubjectDueDate >= today.AddDays(7) && (d.SubjectDueDate <= today.AddDays(14)))
                    {
                        d.DueIn = "2 Weeks";
                    }
                    if (d.SubjectDueDate >= today.AddDays(14))
                    {
                        d.DueIn = "1 Month";
                    }
                });
            }
            dashItems = dashItems.OrderByDescending(o => o.LastCommentedDate).Distinct().ToList();
            return dashItems.ToList();
        }


        [HttpGet]
        [Route("GetUpComingDueMessageDashboard")]
        public List<TaskCommentModel> GetUpComingDueMessageDashboard(int id)
        {
            int days = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            DateTime today = DateTime.Today;
            DateTime weekStart = DateTime.Today.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(29);
            var query = _context.TaskCommentUser.Where(t => t.UserId == id && t.IsAssignedTo == true && t.DueDate != null).AsNoTracking().ToList();
            var taskCommentIds = query.Where(t => (t.UserId == id && t.IsClosed == false)).Select(t => t.TaskCommentId).ToList();

            if (taskCommentIds.Any())
            {
                var taskComments = _context.TaskComment.Include("TaskCommentUser").
                                    Select(s => new
                                    {
                                        s.TaskCommentId,
                                        s.TaskMasterId,
                                        s.CommentedBy,
                                        s.TaskCommentUser,
                                        s.ParentCommentId,
                                        s.TaskId,
                                        s.MainTaskId,
                                        s.TaskSubject,
                                        s.CommentedDate,

                                    }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && t.CommentedBy != id).AsNoTracking().ToList();
                var taskCommentId = taskComments.Select(s => s.TaskCommentId).ToList();
                var CommentedByIds = taskComments.Select(s => s.CommentedBy).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).AsNoTracking().ToList();

                var ParentCommentIds = taskComments.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
                var ParentComment = _context.TaskComment.Select(s => new
                {
                    s.TaskCommentId,
                    s.TaskSubject,
                }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
                var taskMasterId = taskComments.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
                var taskmaster = _context.TaskMaster.Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.DueDate,
                    s.IsNoDueDate,
                }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();
                List<long> taskIds = taskComments.Select(t => t.TaskMasterId.Value).Distinct().ToList();
                bool isAssigned = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.UserId == id);
                bool isOnBehalf = false;
                bool isAssignedCC = false;
                if (!isAssigned)
                {
                    isOnBehalf = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.OnBehalfId == id);
                }
                if (!isOnBehalf)
                {
                    isAssignedCC = _context.TaskMembers.Any(t => taskIds.Contains(t.TaskId.Value) && t.AssignedCc == id);
                }
                //List<TaskComment> taskCommentItems = taskComments.ToList();

                foreach (var s in taskComments)
                {
                    var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                    TaskCommentModel taskCommentModel = new TaskCommentModel();
                    taskCommentModel.TaskId = s.TaskId;
                    taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                    taskCommentModel.TaskMasterID = s.TaskMasterId;
                    taskCommentModel.TaskCommentID = s.TaskCommentId;
                    // taskCommentModel.Comment = s.Comment;
                    taskCommentModel.MainTaskId = s.MainTaskId;
                    taskCommentModel.TaskName = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.Title;
                    taskCommentModel.TaskDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate == false ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.DueDate : null;
                    taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                    taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.UserId == id && t.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == id && t.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault();
                    //taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                    taskCommentModel.CommentedByName = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.CommentedBy)?.UserName : "";
                    taskCommentModel.CommentedBy = s.CommentedBy;
                    taskCommentModel.ParentCommentId = s.ParentCommentId;
                    taskCommentModel.LastCommentedDate = s.CommentedDate;

                    //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                    taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();
                    taskCommentModel.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                    taskCommentModels.Add(taskCommentModel);
                }
                taskCommentModels.ForEach(d =>
                {
                    if (d.IsNoDueDate == false)
                    {
                        if (d.ParentCommentId != null)
                        {
                            d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.ParentCommentId && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                        }
                        else
                        {
                            d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.TaskCommentID && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                        }
                    }

                });


            }
            taskCommentModels = taskCommentModels.Where(t => (t.SubjectDueDate <= weekEnd && t.SubjectDueDate >= weekStart)).ToList();
            if (taskCommentModels.Count > 0)
            {
                taskCommentModels.ForEach(d =>
                {
                    if (d.SubjectDueDate <= today.AddDays(7))
                    {
                        d.DueIn = "1 Week";
                    }
                    if (d.SubjectDueDate >= today.AddDays(7) && (d.SubjectDueDate <= today.AddDays(14)))
                    {
                        d.DueIn = "2 Weeks";
                    }
                    if (d.SubjectDueDate >= today.AddDays(14))
                    {
                        d.DueIn = "1 Month";
                    }
                });
            }
            taskCommentModels = taskCommentModels.OrderByDescending(o => o.LastCommentedDate).Distinct().ToList();
            return taskCommentModels.ToList();
        }
        [HttpGet]
        [Route("GetTaskCommentDashboard")]
        public List<TaskCommentModel> GetTaskCommentDashboard(int id)
        {
            var query = _context.TaskCommentUser.Where(t => t.UserId == id).AsNoTracking().ToList();
            var alltaskcommentUser = _context.TaskCommentUser.Where(t => t.UserId != id).AsNoTracking().ToList();
            var taskCommentIds = new List<long>();
            var parentCommentIds = new List<long?>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = query.Where(w => w.UserId == id).Select(s => s.TaskCommentId).ToList();
            if (commentIds.Any())
            {
                taskCommentIds = alltaskcommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo != true).Select(s => s.TaskCommentId).Distinct().ToList();
                //var taskCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo != true).Select(s => s.TaskCommentId).ToList();
                assignedCommentIds = alltaskcommentUser?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId != id).Select(s => s.TaskCommentId).Distinct().ToList();
                assignedUserCommentIds = query?.Where(w => commentIds.Contains(w.TaskCommentId) && w.IsAssignedTo == true && w.UserId == id).Select(s => s.TaskCommentId).Distinct().ToList();
            }
            //var assignedCommentIds = _context.TaskCommentUser.Where(w => w.IsRead == false && w.UserId == id && w.IsAssignedTo == true).Select(s => s.TaskCommentId).ToList();
            var dashItems = new List<TaskCommentModel>();
            var subCommentItems = new List<TaskCommentModel>();
            var mainComments = new List<TaskCommentModel>();
            if (assignedCommentIds.Any())
            {
                assignedCommentIds.ForEach(a =>
                {
                    if (taskCommentIds.Contains(a))
                    {
                        taskCommentIds.Remove(a);
                    }

                });
            }
            if (assignedUserCommentIds.Any())
            {
                taskCommentIds.AddRange(assignedUserCommentIds);
            }
            taskCommentIds = query.Where(t => (taskCommentIds.Distinct().Contains(t.TaskCommentId) && t.UserId == id && t.IsClosed == false) || (t.IsPin == true && t.PinnedBy == id)).Select(t => t.TaskCommentId).ToList();

            if (taskCommentIds.Any())
            {
                var taskComments = _context.TaskComment.Include("TaskCommentUser").
                    Select(s => new
                    {
                        s.TaskCommentId,
                        s.TaskMasterId,
                        s.CommentedBy,
                        s.TaskCommentUser,
                        s.ParentCommentId,
                        s.TaskId,
                        s.MainTaskId,
                        s.TaskSubject,
                        s.CommentedDate
                    }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && (t.CommentedBy != id || t.TaskCommentUser.Where(tu => tu.IsPin == true && tu.PinnedBy == id).Any(tu => tu.IsPin == true))).AsNoTracking();
                var taskCommentId = taskComments.Select(s => s.TaskCommentId).ToList();
                var CommentedByIds = taskComments.Select(s => s.CommentedBy).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => CommentedByIds.Contains(w.UserId)).AsNoTracking().ToList();
                var taskCommentUser = _context.TaskCommentUser.Select(s => new
                {
                    s.TaskCommentId,
                    s.TaskCommentUserId,
                    s.TaskMasterId,
                    s.UserId,
                    s.IsRead,
                    s.IsPin,
                    s.IsClosed,
                    s.IsAssignedTo,
                    s.PinnedBy,
                }).Where(w => taskCommentIds.Contains(w.TaskCommentId)).ToList();
                var ParentCommentIds = taskComments.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
                var ParentComment = _context.TaskComment.Select(s => new
                {
                    s.TaskCommentId,
                    s.TaskSubject,
                }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
                var taskMasterId = taskComments.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
                var taskmaster = _context.TaskMaster.Select(s => new
                {
                    s.TaskId,
                    s.Title,
                    s.DueDate,
                    s.IsNoDueDate,
                }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();
                List<long> taskIds = taskComments.Select(t => t.TaskMasterId.Value).Distinct().ToList();
                bool isAssigned = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.UserId == id);
                bool isOnBehalf = false;
                bool isAssignedCC = false;
                if (!isAssigned)
                {
                    isOnBehalf = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.OnBehalfId == id);
                }
                if (!isOnBehalf)
                {
                    isAssignedCC = _context.TaskMembers.Any(t => taskIds.Contains(t.TaskId.Value) && t.AssignedCc == id);
                }
                // List<TaskComment> taskCommentItems = taskComments.ToList();
                var selectTaskCommentIds = taskComments.Where(s => s.ParentCommentId != null).Select(s => s.TaskCommentId).Distinct().ToList();
                var selcommentIds = taskComments.GroupBy(d => new { d.ParentCommentId, d.TaskCommentId }).Select(s => new TaskCommentModel
                {
                    TaskCommentID = s.Key.TaskCommentId,
                    ParentCommentId = s.Key.ParentCommentId,

                });
                foreach (var s in taskComments)
                {
                    var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                    TaskCommentModel taskCommentModel = new TaskCommentModel();
                    taskCommentModel.TaskId = s.TaskId;
                    taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                    taskCommentModel.TaskMasterID = s.TaskMasterId;
                    taskCommentModel.TaskCommentID = s.TaskCommentId;
                    // taskCommentModel.Comment = s.Comment;
                    taskCommentModel.MainTaskId = s.MainTaskId;
                    taskCommentModel.TaskName = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.Title;
                    taskCommentModel.TaskDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.DueDate;
                    taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                    taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                    taskCommentModel.CommentedByName = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.CommentedBy)?.UserName : "";
                    taskCommentModel.CommentedBy = s.CommentedBy;
                    taskCommentModel.ParentCommentId = s.ParentCommentId;
                    taskCommentModel.LastCommentedDate = s.CommentedDate;
                    //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                    taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();
                    taskCommentModel.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                    dashItems.Add(taskCommentModel);
                }
                dashItems.ForEach(d =>
                {
                    if (d.ParentCommentId != null)
                    {
                        d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.ParentCommentId && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                    }
                    else
                    {
                        d.SubjectDueDate = query.Where(t => t.TaskCommentId == d.TaskCommentID && t.TaskMasterId == d.TaskMasterID && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();
                    }

                });
                mainComments = dashItems?.Where(d => d.ParentCommentId == null).ToList();
                subCommentItems = dashItems?.Where(d => d.ParentCommentId != null).ToList();
                dashItems.ForEach(d =>
                {
                    if (d.ParentCommentId != null)
                    {
                        var subcomment = subCommentItems?.Where(s => s.ParentCommentId == d.ParentCommentId).ToList();
                        if (subcomment != null && subcomment.Count > 0)
                        {
                            if (subcomment.Count > 1)
                            {
                                var selsubcomment = subcomment.LastOrDefault();
                                if (!mainComments.Exists(m => m.ParentCommentId == d.ParentCommentId))
                                {
                                    var existingmain = mainComments.Where(m => m.TaskCommentID == d.ParentCommentId).FirstOrDefault();
                                    if (existingmain == null)
                                    {
                                        mainComments.Add(selsubcomment);
                                    }
                                }

                            }
                            else
                            {
                                if (!mainComments.Exists(m => m.ParentCommentId == d.ParentCommentId))
                                {
                                    var existingmain = mainComments.Where(m => m.TaskCommentID == d.ParentCommentId).FirstOrDefault();
                                    if (existingmain == null)
                                    {
                                        mainComments.AddRange(subcomment);
                                    }
                                }
                            }
                        }
                    }

                });

            }

            return mainComments.OrderByDescending(o => o.LastCommentedDate).ToList();
        }
        [HttpGet]
        [Route("GetUpDueDashboard")]
        public List<TaskDashboardModel> GetUpDueDashboard(int id)
        {
            int days = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            DateTime weekStart = DateTime.Today.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(6);

            var taskMaster = _context.TaskAssigned
                .Where(t => t.UserId == id && t.StatusCodeId == 512).AsNoTracking().ToList();


            var dashItems = new List<TaskDashboardModel>();
            var item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "OverDue",
                //TaskIds = taskMaster.Where(s => s.DueDate != null && s.DueDate < DateTime.Today && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = taskMaster.Where(s => ((s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today))).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "1 Week",
                //TaskIds = taskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = taskMaster.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart))).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(7);
            weekEnd = DateTime.Today.AddDays(14);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "2 Weeks",
                //TaskIds = taskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = taskMaster.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart))).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(14);
            weekEnd = DateTime.Today.AddDays(30);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "Month",
                //TaskIds = taskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = taskMaster.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart))).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);



            var assignedBy = _context.TaskAssigned
                .Where(t => t.TaskOwnerId == id && t.StatusCodeId == 512).AsNoTracking().ToList();
            item = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "OverDue",
                //TaskIds = assignedBy.Where(s => s.DueDate != null && s.DueDate < DateTime.Today && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = assignedBy.Where(s => ((s.NewDueDate == null && s.DueDate != null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(-days);
            weekEnd = DateTime.Today.AddDays(7);
            item = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "1 Week",
                //TaskIds = assignedBy.Where(s => ((s.NewDueDate==null && s.DueDate <= weekEnd && s.DueDate >= weekStart)||(s.NewDueDate!=null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = assignedBy.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(7);
            weekEnd = DateTime.Today.AddDays(14);
            item = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "2 Weeks",
                //TaskIds = assignedBy.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = assignedBy.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(14);
            weekEnd = DateTime.Today.AddDays(30);
            item = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "Month",
                //TaskIds= assignedBy.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().ToList(),
                TotalCount = assignedBy.Where(s => ((s.NewDueDate == null && s.DueDate <= weekEnd && s.DueDate >= weekStart) || (s.NewDueDate != null && s.NewDueDate <= weekEnd && s.NewDueDate >= weekStart)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);


            return dashItems;
        }
        [HttpGet]
        [Route("GetUpDueMessageDashboard")]
        public List<TaskDashboardModel> GetUpDueMessageDashboard(int id)
        {
            int days = DateTime.Today.DayOfWeek - DayOfWeek.Sunday;
            DateTime weekStart = DateTime.Today.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(7);
            List<TaskCommentModel> taskCommentModels = new List<TaskCommentModel>();
            var taskMaster = _context.TaskCommentUser.Include(a => a.TaskComment)
                .Where(t => t.UserId == id && t.DueDate != null && t.IsAssignedTo == true).AsNoTracking().ToList();

            var assigntaskMaster = _context.TaskCommentUser.Include(a => a.TaskComment)
               .Where(t => t.TaskComment.CommentedBy == id && t.DueDate != null && t.IsAssignedTo == true).AsNoTracking().ToList();
            var taskCommentIds = taskMaster?.Select(t => t.TaskCommentId).ToList();
            var dashItems = new List<TaskDashboardModel>();
            var taskComments = _context.TaskComment.Include("TaskCommentUser").
                                   Select(s => new
                                   {
                                       s.TaskCommentId,
                                       s.TaskMasterId,
                                       s.CommentedBy,
                                       s.TaskCommentUser,
                                       s.ParentCommentId,
                                       s.TaskId,
                                       s.MainTaskId,
                                       s.TaskSubject,
                                       s.CommentedDate,

                                   }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && t.CommentedBy != id).AsNoTracking().ToList();
            var ParentCommentIds = taskComments.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
            var ParentComment = _context.TaskComment.Select(s => new
            {
                s.TaskCommentId,
                s.TaskSubject,
            }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
            var taskMasterId = taskComments.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
            var taskmaster = _context.TaskMaster.Select(s => new
            {
                s.TaskId,
                s.Title,
                s.DueDate,
                s.IsNoDueDate,
            }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();

            foreach (var s in taskComments)
            {
                var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                TaskCommentModel taskCommentModel = new TaskCommentModel();
                taskCommentModel.TaskId = s.TaskId;
                taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                taskCommentModel.TaskMasterID = s.TaskMasterId;
                taskCommentModel.TaskCommentID = s.TaskCommentId;
                // taskCommentModel.Comment = s.Comment;
                taskCommentModel.MainTaskId = s.MainTaskId;
                taskCommentModel.TaskName = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.Title;
                taskCommentModel.TaskDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.DueDate;
                taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.UserId == id && t.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.UserId == id && t.IsAssignedTo == true).Select(t => t.DueDate).FirstOrDefault();
                //taskCommentModel.SubjectDueDate = s.ParentCommentId != null ? s.TaskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() : s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault();

                taskCommentModel.CommentedBy = s.CommentedBy;
                taskCommentModel.ParentCommentId = s.ParentCommentId;
                taskCommentModel.LastCommentedDate = s.CommentedDate;

                //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                taskCommentModel.IsPin = s.TaskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();

                taskCommentModels.Add(taskCommentModel);
            }
            var item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "OverDue",
                TotalCount = taskCommentModels.Where(s => s.SubjectDueDate != null && s.SubjectDueDate < DateTime.Today && s.IsClosed != true).Select(s => s.TaskCommentID).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "1 Week",
                TotalCount = taskCommentModels.Where(s => s.SubjectDueDate <= weekEnd && s.SubjectDueDate >= weekStart).Select(s => s.TaskCommentID).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(7);
            weekEnd = DateTime.Today.AddDays(14);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "2 Weeks",
                TotalCount = taskCommentModels.Where(s => s.SubjectDueDate <= weekEnd && s.SubjectDueDate >= weekStart).Select(s => s.TaskCommentID).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            weekStart = DateTime.Today.AddDays(14);
            weekEnd = DateTime.Today.AddDays(30);
            item = new TaskDashboardModel
            {
                Name = "Assigned to Me",
                DashboardType = "Month",
                TotalCount = taskCommentModels.Where(s => s.SubjectDueDate <= weekEnd && s.SubjectDueDate >= weekStart).Select(s => s.TaskCommentID).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            weekStart = DateTime.Today.AddDays(-days);
            weekEnd = DateTime.Today.AddDays(7);
            var assignByitem = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "OverDue",
                TotalCount = assigntaskMaster.Where(s => s.DueDate != null && s.DueDate < DateTime.Today && s.StatusCodeId == 512).Select(s => s.TaskCommentId).Distinct().Count().ToString(),
            };
            dashItems.Add(assignByitem);
            assignByitem = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "1 Week",
                TotalCount = assigntaskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskCommentId).Distinct().Count().ToString(),
            };
            dashItems.Add(assignByitem);
            weekStart = DateTime.Today.AddDays(7);
            weekEnd = DateTime.Today.AddDays(14);
            assignByitem = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "2 Weeks",
                TotalCount = assigntaskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskCommentId).Distinct().Count().ToString(),
            };
            dashItems.Add(assignByitem);
            weekStart = DateTime.Today.AddDays(14);
            weekEnd = DateTime.Today.AddDays(30);
            assignByitem = new TaskDashboardModel
            {
                Name = "Assigned by Me",
                DashboardType = "Month",
                TotalCount = assigntaskMaster.Where(s => s.DueDate <= weekEnd && s.DueDate >= weekStart).Select(s => s.TaskCommentId).Distinct().Count().ToString(),
            };
            dashItems.Add(assignByitem);

            return dashItems;
        }
        [HttpGet]
        [Route("GetDashAssigned")]
        public List<TaskDashboardPieModel> GetDashAssigned(int id)
        {

            var assignedto = _context.TaskAssigned
              .Where(t => t.UserId == id).AsNoTracking().ToList();


            var dashItems = new List<TaskDashboardPieModel>();

            var item = new TaskDashboardPieModel
            {
                TaskType = "My Task",
                Name = "Unread",
                Value = assignedto.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardPieModel
            {
                TaskType = "My Task",
                Name = "Active",
                Value = assignedto.Where(w => w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardPieModel
            {
                TaskType = "My Task",
                Name = "Over Due",
                Value = assignedto.Where(s => ((s.DueDate != null && s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);

            var activetaskid = assignedto.Where(s => s.StatusCodeId == 512 || s.StatusCodeId == 515).Select(s => s.TaskId).ToList();
            var taskAssigned = _context.TaskNotes.Where(t => t.TaskUserId == id && t.IsNoReminderDate == false && activetaskid.Contains(t.TaskId)).AsNoTracking().Count();
            item = new TaskDashboardPieModel
            {
                TaskType = "My Task",
                Name = "Reminder",
                Value = taskAssigned.ToString(),
            };
            dashItems.Add(item);


            // assigned by me 

            var assignedbyMe = _context.TaskAssigned
              .Where(t => t.TaskOwnerId == id).AsNoTracking().ToList();


            var taskmasassigned = _context.TaskMaster
              .Where(t => t.AddedByUserId == id).AsNoTracking().ToList();

            //var taskIds = taskmasassigned.Select(s => s.TaskId).ToList();

            //var taskassigned = _context.TaskAssigned.Where(a => taskIds.Contains(a.TaskId.Value)).ToList();

            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Unread",
                Value = taskmasassigned.Where(w => w.IsRead.HasValue && w.IsRead.Value == false && w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Active",
                Value = assignedbyMe.Where(w => w.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Over Due",
                Value = assignedbyMe.Where(s => ((s.DueDate != null && s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)) && s.StatusCodeId == 512).Select(s => s.TaskId).Distinct().Count().ToString(),
            };
            dashItems.Add(item);
            var retaskid = assignedbyMe.Where(s => s.StatusCodeId == 512 || s.StatusCodeId == 515).Select(s => s.TaskId).Distinct().ToList();
            var remainCount = _context.TaskNotes.Where(t => t.TaskUserId == id && t.IsNoReminderDate == false && retaskid.Contains(t.TaskId.Value)).AsNoTracking().Count();

            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Reminder",
                Value = remainCount.ToString(),
            };
            dashItems.Add(item);

            // inbox 
            var assignedcc = _context.TaskMembers
               .Where(t => t.AssignedCc == id && t.StatusCodeId == 512).AsNoTracking().ToList();

            //var taskMaster = _context.TaskAssigned
            //   .Where(t => assignedcc.Contains(t.TaskId) && t.StatusCodeId == 512).AsQueryable();



            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Unread",
                Value = assignedcc.Where(w => w.IsRead.HasValue && w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Active",
                Value = assignedcc.Where(s => s.StatusCodeId == 512).Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Over Due",
                Value = assignedcc.Where(s => (s.DueDate != null && s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Count().ToString(),
            };
            dashItems.Add(item);

            var remtaskid = assignedcc.Where(s => s.StatusCodeId.Value == 512 || s.StatusCodeId.Value == 515).Select(s => s.TaskId).ToList();
            remainCount = _context.TaskNotes.Where(t => t.TaskUserId == id && t.IsNoReminderDate == false && remtaskid.Contains(t.TaskId.Value)).AsNoTracking().Count();

            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Reminder",
                Value = remainCount.ToString(),
            };
            dashItems.Add(item);


            return dashItems;
        }
        [HttpGet]
        [Route("GetDashAssignedBy")]
        public List<TaskDashboardPieModel> GetDashAssignedBy(int id)
        {

            var taskMaster = _context.TaskMaster
                .Where(t => t.AddedByUserId == id || t.OwnerId == id).AsNoTracking().AsQueryable();


            var dashItems = new List<TaskDashboardPieModel>();

            var item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Unread",
                Value = taskMaster.Where(w => w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Active",
                Value = taskMaster.Where(w => w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Over Due",
                Value = taskMaster.Where(s => (s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "Assigned by Me",
                Name = "Reminder",
                Value = "0",
            };
            dashItems.Add(item);


            return dashItems;
        }
        [HttpGet]
        [Route("GetDashInbox")]
        public List<TaskDashboardPieModel> GetDashInbox(int id)
        {

            var assignedcc = _context.TaskMembers
                .Where(t => t.AssignedCc == id).AsNoTracking().Select(S => S.TaskId).ToList();

            var taskMaster = _context.TaskAssigned
                .Where(t => assignedcc.Contains(t.TaskId)).AsNoTracking().AsQueryable();

            var dashItems = new List<TaskDashboardPieModel>();

            var item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Unread",
                Value = taskMaster.Where(w => w.IsRead.Value == false).Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Active",
                Value = taskMaster.Where(w => w.StatusCodeId == 512).Count().ToString(),
            };
            dashItems.Add(item);
            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Over Due",
                Value = taskMaster.Where(s => (s.NewDueDate == null && s.DueDate < DateTime.Today) || (s.NewDueDate != null && s.NewDueDate < DateTime.Today)).Count().ToString(),
            };
            dashItems.Add(item);

            item = new TaskDashboardPieModel
            {
                TaskType = "CC",
                Name = "Reminder",
                Value = "0",
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpGet]
        [Route("GetSuperUserDashboard")]
        public List<TaskMasterModel> GetSuperUserDashboard(int id)
        {
            List<long?> ccTaskIDs = _context.TaskMembers.Where(a => !(a.AssignedCc == id)).AsNoTracking().Select(t => t.TaskId).ToList();
            List<long?> validTaskIds = _context.TaskAssigned.Where(a => (a.UserId == id || a.OnBehalfId == id || a.TaskOwnerId == id)).AsNoTracking().Select(t => t.TaskId).ToList();
            List<long?> taskIDs = _context.TaskAssigned.Where(a => !(a.UserId == id || a.OnBehalfId == id || a.TaskOwnerId == id)).AsNoTracking().Select(t => t.TaskId).ToList();
            ccTaskIDs = ccTaskIDs.Except(validTaskIds).ToList();
            if (ccTaskIDs.Any())
            {
                taskIDs.AddRange(ccTaskIDs);
            }
            var dashItems = new List<TaskMasterModel>();

            dashItems = _context.TaskMaster.Include("AddedByUser").Where(t => taskIDs.Contains(t.TaskId))
                .Select(s =>
                new TaskMasterModel
                {
                    TaskID = s.TaskId,
                    Name = s.Title,
                    DueDate = s.DueDate,
                    AddedByUser = s.AddedByUser.UserName,
                    MainTaskId = s.MainTaskId,
                    ParentTaskID = s.ParentTaskId,
                    AddedByUserID = s.AddedByUserId
                }).AsNoTracking().ToList();
            return dashItems;
        }
        [HttpGet]
        [Route("GenerateTaskSession")]
        public IActionResult GenerateTaskSession(long? id)
        {
            Guid? SessionId;
            SessionId = Guid.NewGuid();
            return Content(SessionId.ToString());
        }
        [HttpPut]
        [Route("UpdateInviteTaskSubject")]
        public TaskMasterModel UpdateInviteTaskSubject(InviteUserModel value)
        {
            TaskMasterModel taskMasterModel = new TaskMasterModel();
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assignto"))
            {
                if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                {
                    value.InviteUsers.ForEach(i =>
                    {
                        var tasAssigned = new TaskAssigned
                        {
                            TaskId = value.TaskId,
                            DueDate = value.DueDate,
                            UserId = i,
                            IsRead = false,
                            AssignedDate = DateTime.Now,
                            StatusCodeId = 512,

                        };
                        _context.TaskAssigned.Add(tasAssigned);
                    });
                }
            }
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "assigncc"))
            {
                if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                {
                    value.InviteUsers.ForEach(i =>
                    {
                        var taskMembers = new TaskMembers
                        {
                            TaskId = value.TaskId,
                            DueDate = value.DueDate,
                            AssignedCc = i,
                            IsRead = false,
                            StatusCodeId = 512,

                        };
                        _context.TaskMembers.Add(taskMembers);


                    });
                }
            }
            if (value.TaskId != null && (value.AssignTask.ToLower().Trim() == "onbehalf"))
            {
                var taskmaster = _context.TaskMaster.Where(s => s.TaskId == value.TaskId).FirstOrDefault();
                if (taskmaster != null)
                {
                    if (value.OnBehalfId != null)
                    {
                        taskmaster.OnBehalf = value.OnBehalfId;
                    }
                }
            }
            if (value.TaskCommentId != null)
            {
                if (value.InviteUsers != null && value.InviteUsers.Count > 0)
                {
                    value.InviteUsers.ForEach(i =>
                    {
                        var taskcommentUser = new TaskCommentUser
                        {
                            TaskMasterId = value.TaskId.Value,
                            DueDate = value.DueDate,
                            UserId = i.Value,
                            IsRead = false,
                            IsAssignedTo = true,
                            TaskCommentId = value.TaskCommentId.Value,
                            //StatusCodeId = 512,
                        };
                        _context.TaskCommentUser.Add(taskcommentUser);


                    });
                }

            }
            _context.SaveChanges();
            return taskMasterModel;
        }

        [HttpPost]
        [Route("InsertTaskMasterFromDocument")]
        public DocumentTaskMasterModel InsertTaskMasterFromDocument(DocumentTaskMasterModel value)
        {

            var applicationUsers = _context.Employee.Include(s => s.Designation).Include(s => s.User).Where(e => e.User.StatusCodeId == 1).AsNoTracking().ToList();
            int index = value.FileName.IndexOf('.');
            string result = "";
            if (index > 0)
            {
                result = value.FileName.Substring(0, index);
            }
            if (result != "" && value.IsConfirmDuplicate == false)
            {
                //if (value.ProfileId != null)
                //{
                //    var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Title });

                //    value.Title = profileNo + " / " + result;
                //}
                //else
                //{
                //    value.Title = result;
                //}

                value.Title = value.ProfileNo + " / " + result;
            }
            //value.DueDate = DateTime.Now.AddYears(20);
            var exist = _context.TaskMaster.Where(t => t.Title.Trim().ToLower() == value.Title.ToLower().Trim()).FirstOrDefault();
            if (exist != null)
            {
                value.IsRecordExist = true;
                value.Title = value.Title + " " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt");
                return value;
            }
            value.SessionId = Guid.NewGuid();
            value.VersionSessionId = Guid.NewGuid();
            value.SessionID = Guid.NewGuid();
            List<long?> readWriteUserIds = new List<long?>();
            List<long?> readonlyUserIds = new List<long?>();
            var existdocumentTask = _context.TaskMaster.Where(t => t.TaskId == value.TaskID && t.SourceId == value.DocumentId).FirstOrDefault();
            if (existdocumentTask == null)
            {
                var taskMaster = new TaskMaster
                {
                    //TaskId=value.TaskID,
                    Title = value.Title,
                    ParentTaskId = value.ParentTaskID,
                    MainTaskId = value.MainTaskId,
                    AssignedTo = value.AssignedTo.Count > 0 ? value.AssignedTo[0] : null,
                    DueDate = value.DueDate,
                    Description = value.Description,
                    ProjectId = value.ProjectID,
                    SessionId = value.SessionID,
                    FollowUp = value.FollowUp,
                    OnBehalf = value.OnBehalfID,
                    OwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                    // DiscussionDate = value.DiscussionDate,
                    Version = "1.0",
                    WorkTypeId = 37,
                    RequestTypeId = 58,
                    IsRead = false,
                    IsNoDueDate = value.IsNoDueDate,
                    IsUrgent = value.IsUrgent,
                    IsAutoClose = value.IsAutoClose,
                    OverDueAllowed = value.OverDueAllowed == null ? "false" : value.OverDueAllowed,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    TaskMembers = new List<TaskMembers>(),
                    TaskTag = new List<TaskTag>(),
                    TaskAttachment = new List<TaskAttachment>(),
                    IsNotifyByMessage = value.IsNotifyByMessage,
                    IsEmail = value.IsEmail,
                    VersionSessionId = value.VersionSessionId,
                    IsAllowEditContent = value.IsAllowEditContent,
                    SourceId = value.DocumentId,
                    IsFromProfileDocument = true,
                };
                if (value.ParentTaskID != null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.ParentTaskID);
                    var taskNumber = "1";
                    if (tasklevel == null)
                    {
                        tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.ParentTaskID);
                        taskNumber = tasklevel.TaskNumber + ".1";
                    }
                    else
                    {
                        var numberarray = tasklevel.TaskNumber.Split('.');
                        var number = numberarray[numberarray.Length - 1].ToString();
                        var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                        var taskNo = tasklevel.TaskNumber.Remove(leng);
                        number = (double.Parse(number) + 1).ToString();
                        taskNumber = taskNo + "." + number;
                    }
                    taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                    taskMaster.TaskNumber = taskNumber;
                }
                else if (value.ParentTaskID == null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.MainTaskId == value.MainTaskId && id.ParentTaskId == null);
                    var level = tasklevel.TaskLevel + 1;
                    taskMaster.TaskLevel = level;
                    taskMaster.TaskNumber = level.ToString();
                }


                if (value.AssignedTo != null)
                {
                    value.AssignedTo.ForEach(c =>
                    {
                        var assigned = new TaskAssigned
                        {
                            UserId = c,
                            StatusCodeId = value.StatusCodeID,
                            DueDate = value.DueDate,
                            AssignedDate = DateTime.Now,
                            TaskOwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            IsRead = false,
                            StartDate = value.StartDate,
                            OnBehalfId = value.OnBehalfID,
                        };
                        taskMaster.TaskAssigned.Add(assigned);
                    });
                }

                if (value.AssignedCC != null)
                {
                    value.AssignedCC.ForEach(c =>
                    {
                        if (c != null && c > 0)
                        {
                            var member = new TaskMembers
                            {
                                AssignedCc = c,
                                IsRead = false,
                                DueDate = value.DueDate,
                                StatusCodeId = value.StatusCodeID,
                                //OnBehalfId = value.OnBehalfID,
                                //AssignedTo = value.AssignedTo,
                                //AssignedFrom = value.AddedByUserID.Value
                            };
                            taskMaster.TaskMembers.Add(member);
                        }
                    });
                }
                if (value.TagIds != null && value.TagIds.Count > 0)
                {
                    value.TagIds.ForEach(t =>
                    {
                        var tag = new TaskTag
                        {
                            TagId = t,
                        };
                        taskMaster.TaskTag.Add(tag);
                    });
                }


                _context.TaskMaster.Add(taskMaster);
                _context.SaveChanges();

                value.TaskID = taskMaster.TaskId;
                value.OwnerID = taskMaster.OwnerId;
                var existing = _context.Documents.Where(d => d.DocumentId == value.DocumentId)?.FirstOrDefault();
                if (existing != null)
                {
                    existing.TaskId = value.TaskID;
                    existing.IsMainTask = true;
                    _context.SaveChanges();
                }
                if (value.PersonaltagIds != null && value.PersonaltagIds.Count > 0)
                {
                    value.PersonaltagIds.ForEach(t =>
                    {
                        var personalTagsNew = _context.PersonalTags.Where(p => p.TaskId == value.TaskID && p.Tag == t).FirstOrDefault();
                        if (personalTagsNew == null)
                        {
                            var personalTags = new PersonalTags
                            {
                                Tag = t,
                                TaskId = value.TaskID,
                                AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                                AddedDate = value.AddedDate,

                            };
                            _context.PersonalTags.Add(personalTags);
                        }
                    });

                }

                if (value.ParentTaskID == null && value.MainTaskId == null)
                {
                    // to build tree view with master taskID..
                    var maintask = _context.TaskMaster.FirstOrDefault(id => id.TaskId == taskMaster.TaskId);
                    maintask.MainTaskId = taskMaster.TaskId;
                    maintask.TaskLevel = 1;
                    maintask.TaskNumber = "1";
                    _context.SaveChanges();

                    value.MainTaskId = maintask.MainTaskId;
                }

                if (value.IsEmail.GetValueOrDefault(false) && !value.IsAutoClose.GetValueOrDefault(false))
                {
                    CreateDefaultFileProfileConversation(value);
                }
                value.AssignToNames = value.AssignedTo != null ? string.Join(",", applicationUsers.Where(a => value.AssignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                value.AssignCCNames = value.AssignedCC != null ? string.Join(",", applicationUsers.Where(a => value.AssignedCC.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                value.OnBehalfName = value.OnBehalfID != null ? applicationUsers.Where(a => a.UserId == value.OnBehalfID).Select(a => a.NickName).FirstOrDefault() : "";
                value.HasPermission = true;
                var TaskMasterDescriptionVersionNo = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == value.VersionSessionId).Count();
                var TaskMasterDescriptionVersion = new TaskMasterDescriptionVersion
                {
                    VersionNo = TaskMasterDescriptionVersionNo == 0 ? 1 : (TaskMasterDescriptionVersionNo + 1),
                    SessionId = value.VersionSessionId,
                    Description = value.Description,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now
                };
                _context.TaskMasterDescriptionVersion.Add(TaskMasterDescriptionVersion);
                _context.SaveChanges();
                value.TaskID = taskMaster.TaskId;
                taskMaster.MainTaskId = taskMaster.TaskId;
                readWriteUserIds = _context.TaskAssigned.Where(t => t.TaskId == value.TaskID).Select(s => s.UserId).ToList();
                readonlyUserIds = _context.TaskMembers.Where(t => t.TaskId == value.TaskID).Select(s => s.AssignedCc).ToList();
                readWriteUserIds.Add(value.OwnerID);
                var document = _context.Documents.FirstOrDefault(f => f.DocumentId == value.DocumentId);
                if (document != null)
                {
                    var notes = new Notes
                    {
                        Notes1 = value.Title,
                        Link = value.Link + "id=" + value.TaskID + "&taskid=" + value.TaskID,
                        DocumentId = value.DocumentId,
                        SessionId = document?.SessionId,
                        AddedByUserId = value.AddedByUserID.Value,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,


                    };
                    _context.Notes.Add(notes);
                    _context.SaveChanges();
                    var taskattachment = new TaskAttachment
                    {
                        TaskMasterId = value.TaskID,
                        DocumentId = value.DocumentId,
                        IsLatest = true,
                        IsLocked = false,
                        IsMajorChange = false,
                        UploadedDate = DateTime.Now,
                        UploadedByUserId = value.AddedByUserID,
                        VersionNo = "1",
                        PreviousDocumentId = document.DocumentParentId,
                    };
                    _context.TaskAttachment.Add(taskattachment);
                    _context.SaveChanges();
                    if (readonlyUserIds != null)
                    {
                        readonlyUserIds.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = i,
                                DocumentId = value.DocumentId,
                            };
                            _context.DocumentRights.Add(docAccess);
                        });
                    }
                    if (readWriteUserIds != null)
                    {
                        readWriteUserIds.ForEach(i =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = i,
                                DocumentId = value.DocumentId,

                            };
                            _context.DocumentRights.Add(docAccess);
                        });
                    }
                }
                _context.SaveChanges();

                var linkCompoundDocuments = _context.DocumentLink.Where(d => d.DocumentId == value.DocumentId).AsNoTracking().ToList();
                if (linkCompoundDocuments != null && linkCompoundDocuments.Count > 0)
                {
                    linkCompoundDocuments.ForEach(l =>
                    {
                        var document = _context.Documents.FirstOrDefault(f => f.DocumentId == l.LinkDocumentId);
                        var taskattachment = new TaskAttachment
                        {
                            TaskMasterId = value.TaskID,
                            DocumentId = l.LinkDocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            VersionNo = "1",
                            PreviousDocumentId = document?.DocumentParentId,
                        };
                        _context.TaskAttachment.Add(taskattachment);
                        _context.SaveChanges();
                        var notes = new Notes
                        {
                            Notes1 = value.Title,
                            Link = value.Link + "id=" + value.TaskID + "&taskid=" + value.TaskID,
                            DocumentId = l.LinkDocumentId,
                            SessionId = document?.SessionId,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,


                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();

                        if (readonlyUserIds != null)
                        {
                            readonlyUserIds.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = true,
                                    IsReadWrite = false,
                                    UserId = i,
                                    DocumentId = l.LinkDocumentId,
                                };
                                _context.DocumentRights.Add(docAccess);
                            });
                        }
                        if (readWriteUserIds != null)
                        {
                            readWriteUserIds.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = false,
                                    IsReadWrite = true,
                                    UserId = i,
                                    DocumentId = l.LinkDocumentId,

                                };
                                _context.DocumentRights.Add(docAccess);
                            });
                        }
                        _context.SaveChanges();
                        var existinglink = _context.Documents.Where(d => d.DocumentId == l.LinkDocumentId)?.FirstOrDefault();
                        if (existinglink != null)
                        {

                            existinglink.IsMainTask = false;
                            _context.SaveChanges();
                        }
                        var linkhistorydocuments = _context.Documents.Select(s => new
                        {
                            s.SessionId,
                            s.DocumentId,
                            s.FileName,
                            s.ContentType,
                            s.FileSize,
                            s.UploadDate,
                            s.FilterProfileTypeId,
                            s.DocumentParentId,
                            s.TableName,
                            s.ExpiryDate,
                            s.AddedByUserId,
                            s.ModifiedByUserId,
                            s.ModifiedDate,
                            IsLocked = s.IsLocked,
                            LockedByUserId = s.LockedByUserId,
                            LockedDate = s.LockedDate,
                            s.AddedDate,
                            s.StatusCodeId,
                            FilterProfileType = s.FilterProfileType,
                            s.IsLatest,
                            s.CloseDocumentId,
                            s.IsMainTask
                        }).Where(d => (d.SessionId == document.SessionId && d.FilterProfileTypeId == document.FilterProfileTypeId && d.IsLatest == false) || (d.DocumentId == document.DocumentParentId)).ToList();
                        if (linkhistorydocuments != null && linkhistorydocuments.Count > 0)
                        {
                            linkhistorydocuments.ForEach(h =>
                            {


                                var taskattachment = new TaskAttachment
                                {
                                    TaskMasterId = taskMaster.TaskId,
                                    DocumentId = h.DocumentId,
                                    IsLatest = false,
                                    IsLocked = true,

                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = value.AddedByUserID,
                                    PreviousDocumentId = document.DocumentParentId,

                                };
                                _context.TaskAttachment.Add(taskattachment);
                                _context.SaveChanges();
                            });
                        }

                    });

                }
                var historydocuments = _context.Documents.Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.StatusCodeId,
                    FilterProfileType = s.FilterProfileType,
                    s.IsLatest,
                    s.CloseDocumentId,
                    s.IsMainTask
                }).Where(d => (d.SessionId == document.SessionId && d.FilterProfileTypeId == document.FilterProfileTypeId && d.IsLatest == false) || (d.DocumentId == document.DocumentParentId)).ToList();
                if (historydocuments != null && historydocuments.Count > 0)
                {
                    historydocuments.ForEach(h =>
                    {


                        var taskattachment = new TaskAttachment
                        {
                            TaskMasterId = taskMaster.TaskId,
                            DocumentId = h.DocumentId,
                            IsLatest = false,
                            IsLocked = true,

                            UploadedDate = DateTime.Now,
                            UploadedByUserId = value.AddedByUserID,
                            PreviousDocumentId = document.DocumentParentId,

                        };
                        _context.TaskAttachment.Add(taskattachment);
                        _context.SaveChanges();
                    });

                }
            }
            else
            {
                throw new Exception("From This Document Task Already Created ");
            }
            return value;
        }
        [HttpGet]
        [Route("CheckDocumentCreatedTask")]
        public bool CheckDocumentCreatedTask(long? id)
        {
            bool _result = false;
            try
            {
                var existingTaskCreated = _context.Documents.SingleOrDefault(p => p.DocumentId == id && p.TaskId != null && p.TaskId > 0);
                if (existingTaskCreated != null)
                {
                    _result = true;

                }
                else
                {
                    _result = false;
                }

                return _result;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        [HttpPost]
        [Route("InsertTaskMasterByTemplateCase")]
        public TaskMasterModel InsertTaskMasterByTemplateCase(TaskMasterModel value)
        {
            var applicationUsers = _context.Employee.Include(s => s.Designation).Include(s => s.User).Where(e => e.User.StatusCodeId == 1).AsNoTracking().ToList();
            var exist = _context.TaskMaster.Where(t => t.Title.Trim().ToLower() == value.Title.ToLower().Trim()).FirstOrDefault();
            if (exist !=null)
            {
                value.IsRecordExist = true;
                return value;
            }
            else
            {
                if (value.IsNoDueDate == true)
                {
                    value.DueDate = DateTime.Now.AddYears(20);
                }
                value.VersionSessionId = Guid.NewGuid();
                if (value.SessionId == null)
                {
                    value.SessionId = Guid.NewGuid();
                }
                var taskMaster = new TaskMaster
                {
                    //TaskId=value.TaskID,
                    Title = value.Title,
                    ParentTaskId = value.ParentTaskID,
                    MainTaskId = value.MainTaskId,
                    AssignedTo = value.AssignedTo.Count > 0 ? value.AssignedTo[0] : null,
                    DueDate = value.DueDate,
                    Description = value.Description,
                    ProjectId = value.ProjectID,
                    SessionId = value.SessionId,
                    FollowUp = value.FollowUp,
                    OnBehalf = value.OnBehalfID,
                    OwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                    // DiscussionDate = value.DiscussionDate,
                    Version = "1.0",
                    WorkTypeId = 37,
                    RequestTypeId = 58,
                    IsRead = false,
                    IsNoDueDate = value.IsNoDueDate,
                    IsUrgent = value.IsUrgent,
                    IsAutoClose = value.IsAutoClose,
                    OverDueAllowed = value.OverDueAllowed == null ? "false" : value.OverDueAllowed,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    TaskMembers = new List<TaskMembers>(),
                    TaskTag = new List<TaskTag>(),
                    TaskAttachment = new List<TaskAttachment>(),
                    IsNotifyByMessage = value.IsNotifyByMessage,
                    IsEmail = value.IsEmail,
                    VersionSessionId = value.VersionSessionId,
                    IsAllowEditContent = value.IsAllowEditContent,
                    IsFromProfileDocument = false,
                };
                if (value.ParentTaskID != null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.ParentTaskId == value.ParentTaskID);
                    var taskNumber = "1";
                    if (tasklevel == null)
                    {
                        tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.TaskId == value.ParentTaskID);
                        taskNumber = tasklevel.TaskNumber + ".1";
                    }
                    else
                    {
                        var numberarray = tasklevel.TaskNumber.Split('.');
                        var number = numberarray[numberarray.Length - 1].ToString();
                        var leng = tasklevel.TaskNumber.Length - (number.Length + 1);
                        var taskNo = tasklevel.TaskNumber.Remove(leng);
                        number = (double.Parse(number) + 1).ToString();
                        taskNumber = taskNo + "." + number;
                    }
                    taskMaster.TaskLevel = tasklevel.TaskLevel + 1;
                    taskMaster.TaskNumber = taskNumber;
                }
                else if (value.ParentTaskID == null && value.MainTaskId != null)
                {
                    var tasklevel = _context.TaskMaster.OrderByDescending(o => o.TaskLevel).FirstOrDefault(id => id.MainTaskId == value.MainTaskId && id.ParentTaskId == null);
                    var level = tasklevel.TaskLevel + 1;
                    taskMaster.TaskLevel = level;
                    taskMaster.TaskNumber = level.ToString();
                }


                if (value.AssignedTo != null)
                {
                    value.AssignedTo.ForEach(c =>
                    {
                        var assigned = new TaskAssigned
                        {
                            UserId = c,
                            StatusCodeId = value.StatusCodeID,
                            DueDate = value.DueDate,
                            AssignedDate = DateTime.Now,
                            TaskOwnerId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                            IsRead = false,
                            StartDate = value.StartDate,
                            OnBehalfId = value.OnBehalfID,
                        };
                        taskMaster.TaskAssigned.Add(assigned);
                    });
                }

                if (value.AssignedCC != null)
                {
                    value.AssignedCC.ForEach(c =>
                    {
                        if (c != null && c > 0)
                        {
                            var member = new TaskMembers
                            {
                                AssignedCc = c,
                                IsRead = false,
                                DueDate = value.DueDate,
                                StatusCodeId = value.StatusCodeID,
                            //OnBehalfId = value.OnBehalfID,
                            //AssignedTo = value.AssignedTo,
                            //AssignedFrom = value.AddedByUserID.Value
                        };
                            taskMaster.TaskMembers.Add(member);
                        }
                    });
                }
                if (value.TagIds != null && value.TagIds.Count > 0)
                {
                    value.TagIds.ForEach(t =>
                    {
                        var tag = new TaskTag
                        {
                            TagId = t,
                        };
                        taskMaster.TaskTag.Add(tag);
                    });
                }


                _context.TaskMaster.Add(taskMaster);
                _context.SaveChanges();
                value.TaskID = taskMaster.TaskId;
                if (value.SessionId != null)
                {
                    var docu = _context.Documents.Select(s => new
                    Documents
                    { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights, IsLatest = s.IsLatest })
                        .Where(d => d.SessionId == value.SessionId && d.IsLatest == true).ToList();
                    docu.ForEach(file =>
                    {
                        file.IsTemp = false;
                        if (docu != null)
                        {
                            var taskAttach = new TaskAttachment
                            {
                                DocumentId = file.DocumentId,
                                IsLatest = true,
                                IsLocked = false,
                                IsMajorChange = false,
                                UploadedDate = DateTime.Now,
                                UploadedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                                VersionNo = "1",
                                CheckInDescription = value.DocumentDescription
                            };
                            taskMaster.TaskAttachment.Add(taskAttach);
                        }
                        if (value.ReadonlyUserID != null)
                        {
                            value.ReadonlyUserID.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = true,
                                    IsReadWrite = false,
                                    UserId = i,
                                };
                                file.DocumentRights.Add(docAccess);
                            });
                        }
                        if (value.ReadWriteUserID != null)
                        {
                            value.ReadWriteUserID.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = false,
                                    IsReadWrite = true,
                                    UserId = i,
                                };
                                file.DocumentRights.Add(docAccess);
                            });
                        }
                    });
                }
                _context.SaveChanges();

                if (value.PersonaltagIds != null && value.PersonaltagIds.Count > 0)
                {
                    value.PersonaltagIds.ForEach(t =>
                    {
                        var personalTagsNew = _context.PersonalTags.Where(p => p.TaskId == value.TaskID && p.Tag == t).FirstOrDefault();
                        if (personalTagsNew == null)
                        {
                            var personalTags = new PersonalTags
                            {
                                Tag = t,
                                TaskId = value.TaskID,
                                AddedByUserId = value.OnBehalfID.HasValue ? value.FollowUp == "true" ? value.AddedByUserID : value.OnBehalfID : value.AddedByUserID,
                                AddedDate = value.AddedDate,

                            };
                            _context.PersonalTags.Add(personalTags);
                        }
                    });

                }

                if (value.ParentTaskID == null && value.MainTaskId == null)
                {
                    // to build tree view with master taskID..
                    var maintask = _context.TaskMaster.FirstOrDefault(id => id.TaskId == taskMaster.TaskId);
                    maintask.MainTaskId = taskMaster.TaskId;
                    maintask.TaskLevel = 1;
                    maintask.TaskNumber = "1";
                    _context.SaveChanges();

                    value.MainTaskId = maintask.MainTaskId;
                }

                if (value.IsEmail.GetValueOrDefault(false) && !value.IsAutoClose.GetValueOrDefault(false))
                {
                    CreateDefaultConversation(value);
                }
                value.AssignToNames = value.AssignedTo != null ? string.Join(",", applicationUsers.Where(a => value.AssignedTo.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                value.AssignCCNames = value.AssignedCC != null ? string.Join(",", applicationUsers.Where(a => value.AssignedCC.Contains(a.UserId)).Select(a => a.NickName).ToList()) : "";
                value.OnBehalfName = value.OnBehalfID != null ? applicationUsers.Where(a => a.UserId == value.OnBehalfID).Select(a => a.NickName).FirstOrDefault() : "";
                value.HasPermission = true;
                var TaskMasterDescriptionVersionNo = _context.TaskMasterDescriptionVersion.Where(w => w.SessionId == value.VersionSessionId).Count();
                var TaskMasterDescriptionVersion = new TaskMasterDescriptionVersion
                {
                    VersionNo = TaskMasterDescriptionVersionNo == 0 ? 1 : (TaskMasterDescriptionVersionNo + 1),
                    SessionId = value.VersionSessionId,
                    Description = value.Description,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now
                };
                _context.TaskMasterDescriptionVersion.Add(TaskMasterDescriptionVersion);
                _context.SaveChanges();
                var templateTestCaseFormId = _context.TemplateTestCaseForm.FirstOrDefault(f=>f.SessionId==value.SessionId)?.TemplateTestCaseFormId;
                var notes = new TemplateCaseFormNotes
                {
                    Notes = value.Title,
                    Link = value.Link + "id=" + value.TaskID + "&taskid=" + value.TaskID,
                    TemplateTestCaseFormId = templateTestCaseFormId,
                    SessionId = value.SessionId,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                };
                _context.TemplateCaseFormNotes.Add(notes);
                _context.SaveChanges();
                return value;
            }
        }
    }
}