﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PersonalTagsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PersonalTagsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPersonalTags")]
        public List<string> Get(int id, int userId)
        {
            if(id == -1)
            return _context.PersonalTags.Where(t=>t.TaskId > id && t.AddedByUserId == userId).Select(s =>s.Tag
           ).ToList();
            else
                return _context.PersonalTags.Where(t => t.TaskId == id && t.AddedByUserId == userId).Select(s => s.Tag
           ).ToList();
            
        }
        [HttpGet("GetPersonalTagsByUser")]        
        public List<PersonalTagsModel> GetuserTags(int id)
        {
            List<PersonalTagsModel> personalTagsModels = new List<PersonalTagsModel>();
            var personalTags = _context.PersonalTags.Where(t => t.AddedByUserId == id).ToList();
              if(personalTags!=null && personalTags.Count>0)
            {
                personalTags.ForEach(s =>
                {
                    PersonalTagsModel personalTagsModel = new PersonalTagsModel
                    {
                        Tag = s.Tag,
                        TaskID = s.TaskId,
                    };
                    personalTagsModels.Add(personalTagsModel);
                });
            }             
                    
          
            return personalTagsModels;

        }

        // GET: api/Project/2
        [HttpGet("GetPersonalTags/{id:int}")]
        public ActionResult<PersonalTagsModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var personalTags = _context.PersonalTags.SingleOrDefault(p => p.PersonalTagId == id.Value);
            var result = _mapper.Map<PersonalTagsModel>(personalTags);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost]
        [Route("RemovePersonalTagsItem")]       
        public void GetPersonalTagsItem(PersonalTagsModel value)
        {
            var personalTags = _context.PersonalTags.Where(p => p.Tag == value.Tag).FirstOrDefault();
            if (personalTags != null)
            {
                _context.PersonalTags.Remove(personalTags);
                _context.SaveChanges();
            }

            //var personalTags = _context.PersonalTags.SingleOrDefault(p => p.Tag == value.Tag);
            //var result = _mapper.Map<PersonalTagsModel>(personalTags);

            //return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PersonalTagsModel> GetData(SearchModel searchModel)
        {
            var personalTags = new PersonalTags();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).FirstOrDefault();
                        break;
                    case "Last":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).LastOrDefault();
                        break;
                    case "Next":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).LastOrDefault();
                        break;
                    case "Previous":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).FirstOrDefault();
                        break;
                    case "Last":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).LastOrDefault();
                        break;
                    case "Next":
                        personalTags = _context.PersonalTags.OrderBy(o => o.PersonalTagId).FirstOrDefault(s => s.PersonalTagId > searchModel.Id);
                        break;
                    case "Previous":
                        personalTags = _context.PersonalTags.OrderByDescending(o => o.PersonalTagId).FirstOrDefault(s => s.PersonalTagId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PersonalTagsModel>(personalTags);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPersonalTags")]
        public PersonalTagsModel Post(PersonalTagsModel value)
        {
            var tags = _context.PersonalTags.Where(l => l.TaskId == value.TaskID && l.AddedByUserId == value.AddedByUserID).ToList();
            if (tags.Count > 0)
            {
                _context.PersonalTags.RemoveRange(tags);
                _context.SaveChanges();
            }
            var personalTags = new PersonalTags();
            if (value.Tags.Count != 0)
            {
                value.Tags.ForEach(t =>
                {
                    var personalTagsNew = _context.PersonalTags.Where(p => p.TaskId == value.TaskID && p.Tag == t).FirstOrDefault();
                    if(personalTagsNew == null)
                    {
                    personalTags = new PersonalTags
                    {
                        Tag = t,
                        TaskId = value.TaskID,                        
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = value.AddedDate,

                    };
                    _context.PersonalTags.Add(personalTags);
                    } 
                });
            }
            
            _context.SaveChanges();
            value.PersonalTagID = personalTags.PersonalTagId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePersonalTags")]
        public PersonalTagsModel Put(PersonalTagsModel value)
        {
            var personalTags = _context.PersonalTags.SingleOrDefault(p => p.PersonalTagId == value.PersonalTagID);
            //  folders.TaskMemberId = value.TaskMemberID;

            personalTags.TaskId = value.TaskID;
            personalTags.Tag = value.Tag;
            personalTags.AddedByUserId = value.AddedByUserID;
            personalTags.AddedDate = value.AddedDate;    
            //teamMember.MemberId = value.MemberID;
            //teamMember.TeamId = value.TeamID;
            //teamMember.ModifiedByUserId = value.ModifiedByUserID;
            //teamMember.ModifiedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            //project.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeletePersonalTags")]
        public void Delete(int id)
        {
            var personalTags = _context.PersonalTags.SingleOrDefault(p => p.PersonalTagId == id);
            if (personalTags != null)
            {
                _context.PersonalTags.Remove(personalTags);
                _context.SaveChanges();
            }
        }


    }
}