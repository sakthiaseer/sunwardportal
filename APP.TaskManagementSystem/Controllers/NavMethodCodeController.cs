﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavMethodCodeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavMethodCodeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavMethodCode")]
        public List<NavMethodCodeModel> Get()
        {
            List<NavMethodCodeModel> navMethodCodeModels = new List<NavMethodCodeModel>();
            var navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).AsNoTracking().ToList();
            if(navMethodCode!=null && navMethodCode.Count>0)
            {
                navMethodCode.ForEach(s =>
                {
                    NavMethodCodeModel navMethodCodeModel = new NavMethodCodeModel
                    {
                        MethodCodeID = s.MethodCodeId,
                        MethodName = s.MethodName,
                        CompanyId = s.CompanyId,
                        CompanyName = s.Company?.PlantCode,
                        MethodDescription = s.MethodDescription,
                        NavinpCategoryID = s.NavinpcategoryId,
                        ItemIds = s.NavMethodCodeLines!=null? s.NavMethodCodeLines.Where(al => al.MethodCodeId == s.MethodCodeId).Select(al => al.ItemId).ToList() : new List<long?>(),
                        ItemNos = String.Join("|", s.NavMethodCodeLines.Where(al => al.MethodCodeId == s.MethodCodeId).Select(al => al.Item.No)),
                        ProdFrequency = s.ProdFrequency,
                        DistReplenishHs = s.DistReplenishHs,
                        DistAcmonth = s.DistAcmonth,
                        AdhocMonthStandAlone = s.AdhocMonthStandAlone,
                        AdhocPlanQty = s.AdhocPlanQty,
                        AdhocReplenishHs = s.AdhocReplenishHs,
                        DropDownName = s.MethodName +" | " + s.MethodDescription,
                    };
                    navMethodCodeModels.Add(navMethodCodeModel);
                });
            }
           
            var methodCodeIds = navMethodCodeModels.Select(m => m.MethodCodeID).ToList();
            var navMethodCodeBatch = _context.NavmethodCodeBatch.Where(m => methodCodeIds.Contains(m.NavMethodCodeId)).AsNoTracking().ToList();
            navMethodCodeModels.ForEach(nav =>
            {
                nav.BatchSizeIds = navMethodCodeBatch.Where(b => b.NavMethodCodeId == nav.MethodCodeID).Select(bs => bs.BatchSize).ToList();
                nav.BatchSizeId = navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID) != null ? navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID).DefaultBatchSize : default(long?);
                nav.BatchSizeNo = navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID) != null ? navMethodCodeBatch.FirstOrDefault(b => b.NavMethodCodeId == nav.MethodCodeID).BatchUnitSize : default(decimal?);
            });

            return navMethodCodeModels;
        }

        [HttpGet]
        [Route("GetNavMethodCodeLine")]
        public List<NavMethodCodeLinesModel> GetMethed(int id)
        {
            List<NavMethodCodeLinesModel> navMethodCodeLinesModels = new List<NavMethodCodeLinesModel>();
            var navMethodCode = _context.NavMethodCodeLines.Include("Item").Include("Item.CompanyNavigation").OrderByDescending(o => o.MethodCodeLineId).Where(t => t.MethodCodeId == id).AsNoTracking().ToList();
            if(navMethodCode!=null && navMethodCode.Count>0)
            {
                navMethodCode.ForEach(s =>
                {
                    NavMethodCodeLinesModel navMethodCodeLinesModel = new NavMethodCodeLinesModel
                    {
                        MethodCodeID = s.MethodCodeId,
                        MethodCodeLineID = s.MethodCodeLineId,
                        ItemId = s.ItemId,
                        ItemName = s.Item!=null? s.Item.No : string.Empty,
                        Description = s.Item != null ? s.Item.Description : string.Empty,
                        Description2 = s.Item != null ? s.Item.Description2 : string.Empty,
                        PackSize = s.Item != null ? s.Item.PackSize : null,
                        PackUom = s.Item != null ? s.Item.PackUom : string.Empty,
                        BaseUnitofMeasure = s.Item != null ? s.Item.BaseUnitofMeasure : string.Empty,
                        CategoryCode = s.Item != null ? s.Item.ItemCategoryCode : string.Empty,
                        CompanyName = s.Item != null ? s.Item.CompanyNavigation!=null? s.Item.CompanyNavigation.PlantCode : string.Empty : string.Empty,
                    };
                    navMethodCodeLinesModels.Add(navMethodCodeLinesModel);
                });

            }           
            return navMethodCodeLinesModels;
        }
        [HttpGet]
        [Route("GetMethodCodeLineByDistributor")]
        public List<ACByDistributorModel> GetMethodCodeLineByDistributor(int id)
        {
            List<ACByDistributorModel> acByDistributorList = new List<ACByDistributorModel>();
            var navMethodCode = _context.NavMethodCodeLines.Include("Item.ItemSalesPrice").OrderByDescending(o => o.MethodCodeLineId).Where(t => t.MethodCodeId == id).AsNoTracking().ToList();
            if(navMethodCode!=null && navMethodCode.Count>0)
            {
                navMethodCode.ForEach(s =>
                {
                    ACByDistributorModel aCByDistributorModel = new ACByDistributorModel
                    {
                        MethodCodeID = s.MethodCodeId,
                        MethodCodeLineID = s.MethodCodeLineId,
                        ItemID = s.ItemId,
                        ItemNo = s.Item!=null? s.Item.No : string.Empty,
                        Description1 = s.Item != null ? s.Item.Description : string.Empty,
                        Description2 = s.Item != null ? s.Item.Description2 : string.Empty,
                        PackSize = s.Item != null ? s.Item.PackSize : null,
                        AcMonth = "",
                        SellingPrice = s.Item != null ? s.Item.ItemSalesPrice!=null?s.Item.ItemSalesPrice.Where(t => t.ItemId == s.ItemId).Select(t => t.SellingPrice).FirstOrDefault() : null : null,
                    };
                    acByDistributorList.Add(aCByDistributorModel);
                });

            }           
            return acByDistributorList;
        }
        [HttpGet]
        [Route("GetExistingItems")]
        public List<long?> GetExistingItems()
        {
            List<long?> ItemIds;
            ItemIds = _context.NavMethodCodeLines.AsNoTracking().Select(n => n.ItemId).ToList();
            return ItemIds;
        }
        [HttpGet]
        [Route("GetItems")]
        public List<NavItemModel> GetItems()
        {
            List<long?> ItemIds;
            ItemIds = _context.NavMethodCodeLines.AsNoTracking().Select(n => n.ItemId).ToList();
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navitems = _context.Navitems.Include(n => n.CompanyNavigation).Include(c => c.StatusCode).OrderByDescending(o => o.ItemId).Where(t => !ItemIds.Contains(t.ItemId) && t.StatusCodeId == 1).AsNoTracking().ToList();
            if(navitems!=null && navitems.Count>0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.CompanyNavigation != null ? s.No + "  |  " + s.Description + "  |  " + s.Description2 + "  |  " + s.CompanyNavigation.PlantCode : s.No + "  |  " + s.Description + "  |  " + s.Description2,
                        RelatedItemNo = s.RelatedItemNo,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        PackSize = s.PackSize,
                        PackUom = s.PackUom,
                        BaseUnitofMeasure = s.PackUom,
                        StatusCodeID = s.StatusCodeId
                    };
                    navItemModels.Add(navItemModel);
                });
            }
          
            return navItemModels;
        }
        [HttpGet]
        [Route("GetNAVINPCategoryItems")]
        public List<NAVINPCategoryModel> GetCategoryItems()
        {
            //List<long?> NavCategoryIds;
            //NavCategoryIds = _context.NavMethodCode.Select(n => n.NavinpcategoryId).ToList();
            var categoryitems = _context.Navinpcategory.Select(s => new NAVINPCategoryModel
            {
                Code = s.Code,
                Description = s.Description,
                NAVINPCategoryID = s.NavinpcategoryId,

            }).OrderByDescending(o => o.NAVINPCategoryID).AsNoTracking().ToList();
            return categoryitems;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NavMethodCodeModel> GetData(SearchModel searchModel)
        {
            var navMethodCode = new NavMethodCode();
            NavMethodCodeModel methodModel = new NavMethodCodeModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).LastOrDefault();
                        break;
                    case "Next":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).LastOrDefault();
                        break;
                    case "Previous":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).LastOrDefault();
                        break;
                    case "Next":
                        navMethodCode = _context.NavMethodCode.OrderBy(o => o.MethodCodeId).FirstOrDefault(s => s.MethodCodeId > searchModel.Id);
                        break;
                    case "Previous":
                        navMethodCode = _context.NavMethodCode.OrderByDescending(o => o.MethodCodeId).FirstOrDefault(s => s.MethodCodeId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<NavMethodCodeModel>(navMethodCode);
            if (result != null)
            {


                methodModel.MethodCodeLinesList = _context.NavMethodCodeLines.Select(s =>
                     new NavMethodCodeLinesModel
                     {

                         MethodCodeID = s.MethodCodeId,
                         MethodCodeLineID = s.MethodCodeLineId,
                         ItemId = s.ItemId,
                         ItemName = s.Item.No,
                         Description2 = s.Item.Description2,
                         BaseUnitofMeasure = s.Item.BaseUnitofMeasure,


                     }).OrderByDescending(o => o.MethodCodeLineID).Where(t => t.MethodCodeID == result.MethodCodeID).AsNoTracking().ToList();

                if (methodModel.MethodCodeLinesList.Count > 0)
                {
                    result.MethodCodeLinesList = methodModel.MethodCodeLinesList;
                }
            }

            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertNavMethodCode")]
        public NavMethodCodeModel Post(NavMethodCodeModel value)
        {

            var navMethodCode = new NavMethodCode
            {
                MethodName = value.MethodName,
                MethodDescription = value.MethodDescription,
                NavinpcategoryId = value.NavinpCategoryID,
                StatusCodeId = 1,
                CompanyId = value.CompanyId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ProdFrequency = value.ProdFrequency,
                DistReplenishHs = value.DistReplenishHs,
                DistAcmonth = value.DistAcmonth,
                AdhocMonthStandAlone = value.AdhocMonthStandAlone,
                AdhocPlanQty = value.AdhocPlanQty,
                AdhocReplenishHs = value.AdhocReplenishHs
            };
            _context.NavMethodCode.Add(navMethodCode);
            if (value.BatchSizeIds.Any())
            {
                value.BatchSizeIds.ForEach(id =>
                {
                    NavmethodCodeBatch navmethodCodeBatch = new NavmethodCodeBatch
                    {
                        NavMethodCodeId = navMethodCode.MethodCodeId,
                        DefaultBatchSize = value.BatchSizeId,
                        BatchUnitSize = value.BatchSizeNo,
                        BatchSize = id
                    };
                    navMethodCode.NavmethodCodeBatch.Add(navmethodCodeBatch);
                });
            }
            _context.SaveChanges();
            value.MethodCodeID = navMethodCode.MethodCodeId;


            return value;
        }

        [HttpPost]
        [Route("InsertNavMethodCodeLine")]
        public NavMethodCodeLinesModel InsertNavMethodCodeLine(NavMethodCodeLinesModel value)
        {
            var entryLines = new NavMethodCodeLines
            {
                MethodCodeId = value.MethodCodeID,
                ItemId = value.ItemId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,

            };
            _context.NavMethodCodeLines.Add(entryLines);
            _context.SaveChanges();
            value.MethodCodeLineID = entryLines.MethodCodeLineId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavMethodCode")]
        public NavMethodCodeModel Put(NavMethodCodeModel value)
        {
            var navMethodCode = _context.NavMethodCode.Where(p => p.MethodCodeId == value.MethodCodeID).FirstOrDefault();
            if (navMethodCode != null)
            {
                navMethodCode.MethodName = value.MethodName;
                navMethodCode.MethodDescription = value.MethodDescription;
                navMethodCode.NavinpcategoryId = value.NavinpCategoryID;
                navMethodCode.ModifiedByUserId = value.ModifiedByUserID;
                navMethodCode.ModifiedDate = DateTime.Now;
                navMethodCode.CompanyId = value.CompanyId;
                navMethodCode.ProdFrequency = value.ProdFrequency;
                navMethodCode.DistReplenishHs = value.DistReplenishHs;
                navMethodCode.DistAcmonth = value.DistAcmonth;
                navMethodCode.AdhocMonthStandAlone = value.AdhocMonthStandAlone;
                navMethodCode.AdhocPlanQty = value.AdhocPlanQty;
                navMethodCode.AdhocReplenishHs = value.AdhocReplenishHs;
            }

            var navMethodCodeBatches = _context.NavmethodCodeBatch.Where(n => n.NavMethodCodeId == navMethodCode.MethodCodeId).AsNoTracking().ToList();

            _context.NavmethodCodeBatch.RemoveRange(navMethodCodeBatches);

            if (value.BatchSizeIds.Any())
            {
                value.BatchSizeIds.ForEach(id =>
                {
                    NavmethodCodeBatch navmethodCodeBatch = new NavmethodCodeBatch
                    {
                        NavMethodCodeId = navMethodCode.MethodCodeId,
                        DefaultBatchSize = value.BatchSizeId,
                        BatchUnitSize = value.BatchSizeNo,
                        BatchSize = id
                    };
                    _context.NavmethodCodeBatch.Add(navmethodCodeBatch);
                });
            }

            _context.SaveChanges();

            return value;
        }
        [HttpPut]
        [Route("UpdateNavMethodCodeLine")]
        public NavMethodCodeLinesModel UpdateNavMethodCodeLine(NavMethodCodeLinesModel value)
        {
            var navMethodCodeLine = _context.NavMethodCodeLines.Where(p => p.MethodCodeLineId == value.MethodCodeLineID).FirstOrDefault();
            navMethodCodeLine.MethodCodeId = value.MethodCodeID;
            navMethodCodeLine.ItemId = value.ItemId;
            navMethodCodeLine.ModifiedByUserId = value.ModifiedByUserID;
            navMethodCodeLine.ModifiedDate = DateTime.Now;
            navMethodCodeLine.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNavMethodCode")]
        public void Delete(int id)
        {
            var navMethodCode = _context.NavMethodCode.SingleOrDefault(p => p.MethodCodeId == id);
            if (navMethodCode != null)
            {
                var navMethodcodeLines = _context.NavMethodCodeLines.Where(p => p.MethodCodeId == id).AsNoTracking().ToList();
                if (navMethodcodeLines != null)
                {
                    _context.NavMethodCodeLines.RemoveRange(navMethodcodeLines);
                    _context.SaveChanges();

                }

                var productionForecast = _context.ProductionForecast.Where(p => p.MethodCodeId == id).AsNoTracking().ToList();
                if (productionForecast != null)
                {
                    _context.ProductionForecast.RemoveRange(productionForecast);
                    _context.SaveChanges();

                }
                _context.NavMethodCode.Remove(navMethodCode);

                var navMethodCodeBatches = _context.NavmethodCodeBatch.Where(n => n.NavMethodCodeId == navMethodCode.MethodCodeId).AsNoTracking().ToList();
                _context.NavmethodCodeBatch.RemoveRange(navMethodCodeBatches);
                _context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("DeleteNavMethodCodeLines")]
        public void DeleteMethodLines(int id)
        {
            var navMethodCodeLines = _context.NavMethodCodeLines.SingleOrDefault(p => p.MethodCodeLineId == id);
            if (navMethodCodeLines != null)
            {
                _context.NavMethodCodeLines.Remove(navMethodCodeLines);
                _context.SaveChanges();

            }
        }
    }
}