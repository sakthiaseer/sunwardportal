﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlanketOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public BlanketOrderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [Route("GetBlanketOrder")]
        public List<BlanketOrderModel> Get()
        {
            var blanketOrder = _context.BlanketOrder.Include(p => p.Product).Include(c => c.Customer).Include(o => o.Contract).Include(e => e.ExtensionInformation).Include(t => t.TypeOfRequest).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            List<BlanketOrderModel> blanketOrderModel = new List<BlanketOrderModel>();
            blanketOrder.ForEach(s =>
            {
                BlanketOrderModel blanketOrderModels = new BlanketOrderModel
                {
                    BlanketOrderId = s.BlanketOrderId,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.CompanyName,
                    ContractId = s.ContractId,
                    ContractNo = s.Contract?.OrderNo,
                    ProductId = s.ProductId,
                    TypeOfRequestId = s.TypeOfRequestId,
                    QtyRequest = s.QtyRequest,
                    ExtensionInformationId = s.ExtensionInformationId,
                    FromMonth = s.FromMonth,
                    ToMonth = s.ToMonth,
                    NoOfMonth = s.NoOfMonth,
                    LotInformation = s.LotInformation,
                    HowManyLot = s.HowManyLot,
                    CurrencyId = s.CurrencyId,
                    ExceedQty = s.ExceedQty,
                    Price = s.Price,
                    PriceToQuoteForExceedQty = s.PriceToQuoteForExceedQty,
                    SpecialNotes = s.SpecialNotes,
                    Validity = s.Validity,
                    SessionId = s.SessionId,
                    ProductName = s.Product?.CustomerReferenceNo,
                    Uom = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).SingleOrDefault() : "",
                    Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                    TypeOfRequestName = s.TypeOfRequest != null ? s.TypeOfRequest.CodeValue : "",
                    ExtensionInformationName = s.ExtensionInformation != null ? s.ExtensionInformation.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    LotInformationFlag = s.LotInformation == true ? "Yes" : "No",
                    ExtensionStatusId = s.ExtensionStatusId,
                    QtyExtensionStatusId = s.QtyExtensionStatusId,
                    SellingPrice = s.SellingPrice,
                    SpecialRemarks = s.SpecialRemarks,
                    DateOfOrder = s.DateOfOrder,
                };
                blanketOrderModel.Add(blanketOrderModels);
            });
            return blanketOrderModel.OrderByDescending(a => a.BlanketOrderId).ToList();
        }
        [HttpGet]
        [Route("GetBlanketOrderBySalesId")]
        public List<BlanketOrderModel> GetBlanketOrderBySalesId(int id)
        {
            var blanketOrder = _context.BlanketOrder.Include(p => p.Product).Include(ss => ss.Product).Include(c => c.Customer).Include(o => o.Contract).Include(e => e.ExtensionInformation).Include(t => t.TypeOfRequest).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.ContractId == id).AsNoTracking().ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            List<BlanketOrderModel> blanketOrderModel = new List<BlanketOrderModel>();
            blanketOrder.ForEach(s =>
            {
                BlanketOrderModel blanketOrderModels = new BlanketOrderModel
                {
                    BlanketOrderId = s.BlanketOrderId,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.CompanyName,
                    ContractId = s.ContractId,
                    ContractNo = s.Contract?.OrderNo,
                    ProductId = s.ProductId,
                    TypeOfRequestId = s.TypeOfRequestId,
                    QtyRequest = s.QtyRequest,
                    ExtensionInformationId = s.ExtensionInformationId,
                    FromMonth = s.FromMonth,
                    ToMonth = s.ToMonth,
                    NoOfMonth = s.NoOfMonth,
                    LotInformation = s.LotInformation,
                    HowManyLot = s.HowManyLot,
                    CurrencyId = s.CurrencyId,
                    ExceedQty = s.ExceedQty,
                    Price = s.Price,
                    PriceToQuoteForExceedQty = s.PriceToQuoteForExceedQty,
                    SpecialNotes = s.SpecialNotes,
                    Validity = s.Validity,
                    SessionId = s.SessionId,
                    ProductName = s.Product?.CustomerReferenceNo,
                    Uom = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).SingleOrDefault() : "",
                    Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                    TypeOfRequestName = s.TypeOfRequest != null ? s.TypeOfRequest.CodeValue : "",
                    ExtensionInformationName = s.ExtensionInformation != null ? s.ExtensionInformation.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    LotInformationFlag = s.LotInformation == true ? "Yes" : "No",
                    ExtensionStatusId = s.ExtensionStatusId,
                    QtyExtensionStatusId = s.QtyExtensionStatusId,
                    SellingPrice = s.SellingPrice,
                    SpecialRemarks = s.SpecialRemarks,
                    DateOfOrder = s.DateOfOrder,
                };
                blanketOrderModel.Add(blanketOrderModels);
            });
            return blanketOrderModel.OrderByDescending(a => a.BlanketOrderId).ToList();
        }

        [HttpGet]
        [Route("GetContractDistributionCustomerBySalesId")]
        public List<CompanyListingModel> GetContractDistributionCustomerBySalesId(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var PurchaseInfoIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            var ContractDistributionSalesEntryLinesids = _context.ContractDistributionSalesEntryLine
              .Where(s => PurchaseInfoIds.Contains(s.PurchaseItemSalesEntryLineId.Value)).Select(s => s.SocustomerId)
                .ToList();
            var CompanyListingItems = _context.CompanyListing.Include(c => c.CompanyListingCompanyType).Where(c => ContractDistributionSalesEntryLinesids.Contains(c.CompanyListingId)).ToList();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();

            CompanyListingItems.ForEach(h =>
            {
                CompanyListingModel CompanyListingModel = new CompanyListingModel();
                CompanyListingModel.CompanyListingID = h.CompanyListingId;
                CompanyListingModel.CompanyListingName = h.CompanyName;
                CompanyListingModels.Add(CompanyListingModel);
            });
            return CompanyListingModels;
        }

        [HttpGet]
        [Route("GetProductBySalesId")]
        public List<SocustomersItemCrossReferenceModel> GetProductBySalesId(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var socustomerIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).Select(s => s.SobyCustomersId).ToList();

            var soCustomer = _context.SocustomersItemCrossReference.Where(c => socustomerIds.Contains(c.SocustomersItemCrossReferenceId)).ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();

            soCustomer.ForEach(h =>
            {
                SocustomersItemCrossReferenceModel socustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                socustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = h.SocustomersItemCrossReferenceId;
                socustomersItemCrossReferenceModel.CustomerReferenceNo = h.CustomerReferenceNo;
                socustomersItemCrossReferenceModel.CompanyListingName = h.CustomerReferenceNo;
                //socustomersItemCrossReferenceModel.ProductNo = h.CustomerReferenceNo +" | " +h.Description;
                socustomersItemCrossReferenceModel.PerUomName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == h.PerUomId).Select(a => a.Value).SingleOrDefault() : "";
                socustomersItemCrossReferenceModel.Description = h.Description;
                SocustomersItemCrossReferenceModels.Add(socustomersItemCrossReferenceModel);
            });
            if (SocustomersItemCrossReferenceModels != null && SocustomersItemCrossReferenceModels.Count > 0)
            {
                SocustomersItemCrossReferenceModels.ForEach(s =>
                {
                    s.ProductNo = s.CustomerReferenceNo + " | "+s.Description +" | " + s.PerUomName;
                });

            }
            return SocustomersItemCrossReferenceModels;
        }

        [HttpGet]
        [Route("GetProductReferenceByProductId")]
        public PurchaseItemSalesEntryLineModel GetProductReferenceByProductId(int id, int userId)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var purchaseline = _context.PurchaseItemSalesEntryLine.Where(s => s.SobyCustomersId == id && s.SalesOrderEntryId == userId).FirstOrDefault();
            PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();
            if (purchaseline != null)
            {
                purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = purchaseline.PurchaseItemSalesEntryLineId;
                purchaseItemSalesEntryLineModel.SellingPrice = purchaseline.SellingPrice;
                purchaseItemSalesEntryLineModel.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == purchaseline.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
            }
            return purchaseItemSalesEntryLineModel;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlanketOrderModel> GetData(SearchModel searchModel)
        {
            var blanketOrder = new BlanketOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderBy(o => o.BlanketOrderId).FirstOrDefault(s => s.BlanketOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        blanketOrder = _context.BlanketOrder.Include(c => c.Contract).OrderByDescending(o => o.BlanketOrderId).FirstOrDefault(s => s.BlanketOrderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlanketOrderModel>(blanketOrder);
            if (result != null)
            {
                result.ContractNo = blanketOrder.Contract?.OrderNo;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertBlanketOrder")]
        public BlanketOrderModel Post(BlanketOrderModel value)
        {
            var SessionId = Guid.NewGuid();
            var blanketOrder = new BlanketOrder
            {
                CustomerId = value.CustomerId,
                ContractId = value.ContractId,
                ProductId = value.ProductId,
                TypeOfRequestId = value.TypeOfRequestId,
                QtyRequest = value.QtyRequest,
                ExtensionInformationId = value.ExtensionInformationId,
                FromMonth = value.FromMonth,
                ToMonth = value.ToMonth,
                NoOfMonth = value.NoOfMonth,
                LotInformation = value.LotInformationFlag == "Yes" ? true : false,
                HowManyLot = value.HowManyLot,
                CurrencyId = value.CurrencyId,
                Price = value.Price,
                PriceToQuoteForExceedQty = value.PriceToQuoteForExceedQty,
                SpecialNotes = value.SpecialNotes,
                Validity = value.Validity,
                SessionId = SessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ExtensionStatusId = value.ExtensionStatusId,
                QtyExtensionStatusId = value.QtyExtensionStatusId,
                DateOfOrder = value.DateOfOrder,
                SellingPrice = value.SellingPrice,
                SpecialRemarks = value.SpecialRemarks,
                ExceedQty = value.ExceedQty,
            };
            _context.BlanketOrder.Add(blanketOrder);
            _context.SaveChanges();
            value.BlanketOrderId = blanketOrder.BlanketOrderId;
            if (value.ProductId > 0)
            {
                value.ProductName = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.ProductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            value.SessionId = blanketOrder.SessionId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBlanketOrder")]
        public BlanketOrderModel Put(BlanketOrderModel value)
        {
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var blanketOrder = _context.BlanketOrder.SingleOrDefault(p => p.BlanketOrderId == value.BlanketOrderId);
            blanketOrder.CustomerId = value.CustomerId;
            blanketOrder.ContractId = value.ContractId;
            blanketOrder.ProductId = value.ProductId;
            blanketOrder.TypeOfRequestId = value.TypeOfRequestId;
            blanketOrder.QtyRequest = value.QtyRequest;
            blanketOrder.ExtensionInformationId = value.ExtensionInformationId;
            blanketOrder.FromMonth = value.FromMonth;
            blanketOrder.ToMonth = value.ToMonth;
            blanketOrder.NoOfMonth = value.NoOfMonth;
            blanketOrder.LotInformation = value.LotInformationFlag == "Yes" ? true : false;
            blanketOrder.HowManyLot = value.HowManyLot;
            blanketOrder.CurrencyId = value.CurrencyId;
            blanketOrder.Price = value.Price;
            blanketOrder.PriceToQuoteForExceedQty = value.PriceToQuoteForExceedQty;
            blanketOrder.SpecialNotes = value.SpecialNotes;
            blanketOrder.Validity = value.Validity;
            blanketOrder.ModifiedDate = DateTime.Now;
            blanketOrder.ModifiedByUserId = value.ModifiedByUserID;
            blanketOrder.StatusCodeId = value.StatusCodeID.Value;
            blanketOrder.ExtensionStatusId = value.ExtensionStatusId;
            blanketOrder.QtyExtensionStatusId = value.QtyExtensionStatusId;
            blanketOrder.DateOfOrder = value.DateOfOrder;
            blanketOrder.SpecialRemarks = value.SpecialRemarks;
            blanketOrder.SellingPrice = value.SellingPrice;
            blanketOrder.ExceedQty = value.ExceedQty;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UploadBlanketOrderDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new BlanketOrderAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.BlanketOrderAttachment.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetBlanketOrderDocument")]
        public List<BlanketOrderAttachmentModel> GetMachineDocument(Guid? SessionId)
        {
            var query = _context.BlanketOrderAttachment.Select(s => new BlanketOrderAttachmentModel
            {
                SessionId = s.SessionId,
                BlanketOrderAttachmentId = s.BlanketOrderAttachmentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024))
        }).Where(l => l.SessionId == SessionId).OrderByDescending(o => o.BlanketOrderAttachmentId).ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadBlanketOrderDocument")]
        public IActionResult DownLoadBlanketOrderDocument(long id)
        {
            var document = _context.BlanketOrderAttachment.SingleOrDefault(t => t.BlanketOrderAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteBlanketOrderDocument")]
        public void BlanketOrderDocuments(int id)
        {
            var document = _context.BlanketOrderAttachment.SingleOrDefault(p => p.BlanketOrderAttachmentId == id);
            if (document != null)
            {
                _context.BlanketOrderAttachment.Remove(document);
                _context.SaveChanges();
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBlanketOrder")]
        public void Delete(int id)
        {
            var blanketOrder = _context.BlanketOrder.SingleOrDefault(p => p.BlanketOrderId == id);
            if (blanketOrder != null)
            {
                var blanketOrderAttachment = _context.BlanketOrderAttachment.Where(p => p.SessionId == blanketOrder.SessionId).ToList();
                if (blanketOrderAttachment != null)
                {
                    _context.BlanketOrderAttachment.RemoveRange(blanketOrderAttachment);
                    _context.SaveChanges();
                }
                _context.BlanketOrder.Remove(blanketOrder);
                _context.SaveChanges();
            }
        }


        //
        // [HttpGet]
        // [Route("GetBlanketOrderAttachment")]
        // public List<BlanketOrderAttachmentModel> GetBlanketOrderAttachment(int? id)
        // {
        //     var BlanketOrder = _context.BlanketOrder.Where(w => w.BlanketOrderId == id).Select(s => s.SessionId).FirstOrDefault();
        //     var query = _context.BlanketOrderAttachment.Select(s => new BlanketOrderAttachmentModel
        //     {
        //         SessionId = s.SessionId,
        //         BlanketOrderAttachmentId = s.BlanketOrderAttachmentId,
        //         FileName = s.FileName,
        //         ContentType = s.ContentType,
        //         FileSize = s.FileSize,
        //         IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
        //     }).Where(l => l.SessionId == BlanketOrder).OrderByDescending(o => o.BlanketOrderAttachmentId).AsNoTracking().ToList();
        //     return query;
        // }



        [HttpPost]
        [Route("UploadBlanketOrderAttachments")]
        public IActionResult UploadBlanketOrderAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var screenId = files["screenID"].ToString();
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new BlanketOrderAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    ScreenId = screenId,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo

                };

                _context.BlanketOrderAttachment.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetBlanketOrderAttachmentBySessionID")]
        public List<DocumentsModel> GetBlanketOrderAttachmentBySessionID(Guid? sessionId, int userId)
        {
            var query = _context.BlanketOrderAttachment.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.BlanketOrderAttachmentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                ScreenID = s.ScreenId,
                UploadDate = s.UploadDate,
                Uploaded = true,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadBlanketOrderAttachment")]
        public IActionResult DownLoadBlanketOrderAttachment(long id)
        {
            var document = _context.BlanketOrderAttachment.SingleOrDefault(t => t.BlanketOrderAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteBlanketOrderAttachment")]
        public void DeleteBlanketOrderAttachment(int id)
        {
            var BlanketOrderAttachment = _context.BlanketOrderAttachment.SingleOrDefault(p => p.BlanketOrderAttachmentId == id);
            if (BlanketOrderAttachment != null)
            {
                _context.BlanketOrderAttachment.Remove(BlanketOrderAttachment);
                _context.SaveChanges();
            }
        }


    }
}