﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ContactsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ContactsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetContacts")]
        public List<ContactsModel> Get()
        {
            var contacts = _context.Contacts.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<ContactsModel> contactsModel = new List<ContactsModel>();
            contacts.ForEach(s =>
            {
                ContactsModel contactsModels = new ContactsModel
                {
                    ContactId = s.ContactId,
                    BusinessAccount = s.BusinessAccount,
                    Owner = s.Owner,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    WorkGroup = s.WorkGroup,
                    ContactClass = s.ContactClass,
                    JobTitle = s.JobTitle,
                    StatusCodeId = s.StatusCodeId,
                    Type = s.Type,
                    Email = s.Email,
                    Phone = s.Phone,
                    Fax = s.Fax,
                    Address1 = s.Address1,
                    Address2 = s.Address2,
                    City = s.City,
                    StateId = s.StateId,
                    CountryId = s.CountryId,
                    PostalCode = s.PostalCode,
                    IsDataPrivacy = s.IsDataPrivacy,
                    DateOfConsent = s.DateOfConsent,
                    ConsentExpires = s.ConsentExpires,
                    ContactMethod = s.ContactMethod,
                    AccountReference = s.AccountReference,
                    ParentAccount = s.ParentAccount,
                    InComingActivity = s.InComingActivity,
                    OutGoingActivity = s.OutGoingActivity,
                    CompanyName = s.CompanyName,
                    Dob = s.Dob,
                    SpouseName = s.SpouseName,
                    Gender = s.Gender,
                    MaritalStatus = s.MaritalStatus,
                    LastAccessDate = s.LastAccessDate,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode.CodeValue,
                };
                contactsModel.Add(contactsModels);
            });
            return contactsModel.OrderByDescending(a => a.ContactId).ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetContacts/{id:int}")]
        public ActionResult<ContactsModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var contacts = _context.Contacts.SingleOrDefault(p => p.ContactId == id.Value);
            var result = _mapper.Map<ContactsModel>(contacts);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ContactsModel> GetData(SearchModel searchModel)
        {
            var contacts = new Contacts();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).FirstOrDefault();
                        break;
                    case "Last":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).LastOrDefault();
                        break;
                    case "Next":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).LastOrDefault();
                        break;
                    case "Previous":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).FirstOrDefault();
                        break;
                    case "Last":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).LastOrDefault();
                        break;
                    case "Next":
                        contacts = _context.Contacts.OrderBy(o => o.ContactId).FirstOrDefault(s => s.ContactId > searchModel.Id);
                        break;
                    case "Previous":
                        contacts = _context.Contacts.OrderByDescending(o => o.ContactId).FirstOrDefault(s => s.ContactId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ContactsModel>(contacts);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertContacts")]
        public ContactsModel Post(ContactsModel value)
        {
            var contacts = new Contacts
            {
                BusinessAccount = value.BusinessAccount,
                Owner = value.Owner,
                FirstName = value.FirstName,
                LastName = value.LastName,
                WorkGroup = value.WorkGroup,
                ContactClass = value.ContactClass,
                JobTitle = value.JobTitle,
                StatusCodeId = value.StatusCodeId,
                Type = value.Type,
                Email = value.Email,
                Phone = value.Phone,
                Fax = value.Fax,
                Address1 = value.Address1,
                Address2 = value.Address2,
                City = value.City,
                StateId = value.StateId,
                CountryId = value.CountryId,
                PostalCode = value.PostalCode,
                IsDataPrivacy = value.IsDataPrivacy,
                DateOfConsent = value.DateOfConsent,
                ConsentExpires = value.ConsentExpires,
                ContactMethod = value.ContactMethod,
                AccountReference = value.AccountReference,
                ParentAccount = value.ParentAccount,
                InComingActivity = value.InComingActivity,
                OutGoingActivity = value.OutGoingActivity,
                CompanyName = value.CompanyName,
                Dob = value.Dob,
                SpouseName = value.SpouseName,
                Gender = value.Gender,
                MaritalStatus = value.MaritalStatus,
                LastAccessDate = value.LastAccessDate,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ContactActivities = new List<ContactActivities>(),
            };
            value.ContactActivities.ToList().ForEach(l =>
            {
                var contactActivities = new ContactActivities
                {
                    //ContactActivitiesId = l.ContactActivitiesId,
                    ContactId = l.ContactId,
                    Type = l.Type,
                    Summary = l.Summary,
                };
                contacts.ContactActivities.Add(contactActivities);
            });

            _context.Contacts.Add(contacts);
            _context.SaveChanges();
            value.ContactId = contacts.ContactId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateContacts")]
        public ContactsModel Put(ContactsModel value)
        {
            var contacts = _context.Contacts.SingleOrDefault(p => p.ContactId == value.ContactId);

            contacts.ModifiedByUserId = value.ModifiedByUserID;
            contacts.ModifiedDate = DateTime.Now;
            contacts.BusinessAccount = value.BusinessAccount;
            contacts.Owner = value.Owner;
            contacts.FirstName = value.FirstName;
            contacts.LastName = value.LastName;
            contacts.WorkGroup = value.WorkGroup;
            contacts.ContactClass = value.ContactClass;
            contacts.JobTitle = value.JobTitle;
            contacts.StatusCodeId = value.StatusCodeId;
            contacts.Type = value.Type;
            contacts.Email = value.Email;
            contacts.Phone = value.Phone;
            contacts.Fax = value.Fax;
            contacts.Address1 = value.Address1;
            contacts.Address2 = value.Address2;
            contacts.City = value.City;
            contacts.StateId = value.StateId;
            contacts.CountryId = value.CountryId;
            contacts.PostalCode = value.PostalCode;
            contacts.IsDataPrivacy = value.IsDataPrivacy;
            contacts.DateOfConsent = value.DateOfConsent;
            contacts.ConsentExpires = value.ConsentExpires;
            contacts.ContactMethod = value.ContactMethod;
            contacts.AccountReference = value.AccountReference;
            contacts.ParentAccount = value.ParentAccount;
            contacts.InComingActivity = value.InComingActivity;
            contacts.OutGoingActivity = value.OutGoingActivity;
            contacts.CompanyName = value.CompanyName;
            contacts.Dob = value.Dob;
            contacts.SpouseName = value.SpouseName;
            contacts.Gender = value.Gender;
            contacts.MaritalStatus = value.MaritalStatus;
            contacts.LastAccessDate = value.LastAccessDate;

            var contactActivities = _context.ContactActivities.Where(l => l.ContactId == value.ContactId).ToList();
            if (contactActivities != null)
            {
                if (contactActivities.Count > 0)
                {
                    _context.ContactActivities.RemoveRange(contactActivities);
                    _context.SaveChanges();
                }

                value.ContactActivities.ToList().ForEach(l =>
                {
                    var contactActivity = new ContactActivities
                    {
                        ContactId = value.ContactId,
                        Type = l.Type,
                        Summary = l.Summary,
                    };
                    contacts.ContactActivities.Add(contactActivity);
                });
            }


            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteContacts")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var Contacts = _context.Contacts.SingleOrDefault(p => p.ContactId == id);
                if (Contacts != null)
                {
                    _context.Contacts.Remove(Contacts);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}