﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ManpowerInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ManpowerInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetManpowerInformations")]
        public List<ManpowerInformationModel> Get(int? id)
        {
            var ManpowerInformationModels = _context.ManpowerInformation.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.StatusCode).Select(s => new ManpowerInformationModel
            {
                ManpowerInformationID = s.ManpowerInformationId,
                StandardManufacturingProcessId=s.StandardManufacturingProcessId,
                ProcessFromNo = s.ProcessFromNo,
                ProcessToNo = s.ProcessToNo,
                ManpowerNoID = s.ManpowerNoId,
               
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).Where(l=>l.StandardManufacturingProcessId==id).OrderByDescending(o => o.ManpowerInformationID).AsNoTracking().ToList();
            return ManpowerInformationModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ManpowerInformationModel> GetData(SearchModel searchModel)
        {
            var ManpowerInformation = new ManpowerInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).LastOrDefault();
                        break;
                    case "Next":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).LastOrDefault();
                        break;
                    case "Next":
                        ManpowerInformation = _context.ManpowerInformation.OrderBy(o => o.ManpowerInformationId).FirstOrDefault(s => s.ManpowerInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        ManpowerInformation = _context.ManpowerInformation.OrderByDescending(o => o.ManpowerInformationId).FirstOrDefault(s => s.ManpowerInformationId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<ManpowerInformationModel>(ManpowerInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertManpowerInformation")]
        public ManpowerInformationModel Post(ManpowerInformationModel value)
        {
            var ManpowerInformation = new ManpowerInformation
            {
                ProcessFromNo = value.ProcessFromNo,
                StandardManufacturingProcessId=value.StandardManufacturingProcessId,
                ProcessToNo = value.ProcessToNo,
                ManpowerNoId = value.ManpowerNoID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.ManpowerInformation.Add(ManpowerInformation);
            _context.SaveChanges();
            value.ManpowerInformationID = ManpowerInformation.ManpowerInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateManpowerInformation")]
        public ManpowerInformationModel Put(ManpowerInformationModel value)
        {
            var manpowerInformation = _context.ManpowerInformation.SingleOrDefault(p => p.ManpowerInformationId == value.ManpowerInformationID);
            manpowerInformation.ProcessFromNo = value.ProcessFromNo;
            manpowerInformation.StandardManufacturingProcessId = value.StandardManufacturingProcessId;
            manpowerInformation.ProcessToNo = value.ProcessToNo;
            manpowerInformation.ManpowerNoId = value.ManpowerNoID;            
            manpowerInformation.ModifiedByUserId = value.ModifiedByUserID;
            manpowerInformation.ModifiedDate = DateTime.Now;
            manpowerInformation.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteManpowerInformation")]
        public void Delete(int id)
        {
            var ManpowerInformation = _context.ManpowerInformation.SingleOrDefault(p => p.ManpowerInformationId == id);
            if (ManpowerInformation != null)
            {
                _context.ManpowerInformation.Remove(ManpowerInformation);
                _context.SaveChanges();
            }
        }
    }
}