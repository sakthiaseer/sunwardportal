﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationGlossaryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationGlossaryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetGlossarys")]
        public List<ApplicationGlossaryModel> Get()
        {
            var glossary = _context.ApplicationGlossary
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Select(s => new ApplicationGlossaryModel
                {
                    ApplicationGlossaryID = s.ApplicationGlossaryId,
                    Name = s.Name,
                    Definition = s.Definition,

                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    StatusCode = s.StatusCode.CodeValue

                }).OrderByDescending(o => o.ApplicationGlossaryID).AsNoTracking().ToList();
            return glossary;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Designation")]
        [HttpGet("GetGlossarys/{id:int}")]
        public ActionResult<ApplicationGlossaryModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var glossary = _context.ApplicationGlossary.SingleOrDefault(p => p.ApplicationGlossaryId == id.Value);
            var result = _mapper.Map<ApplicationGlossaryModel>(glossary);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationGlossaryModel> GetData(SearchModel searchModel)
        {
            var glossary = new ApplicationGlossary();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).FirstOrDefault();
                        break;
                    case "Last":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).LastOrDefault();
                        break;
                    case "Next":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).LastOrDefault();
                        break;
                    case "Previous":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).FirstOrDefault();
                        break;
                    case "Last":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).LastOrDefault();
                        break;
                    case "Next":
                        glossary = _context.ApplicationGlossary.OrderBy(o => o.ApplicationGlossaryId).FirstOrDefault(s => s.ApplicationGlossaryId > searchModel.Id);
                        break;
                    case "Previous":
                        glossary = _context.ApplicationGlossary.OrderByDescending(o => o.ApplicationGlossaryId).FirstOrDefault(s => s.ApplicationGlossaryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationGlossaryModel>(glossary);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertGlossary")]
        public ApplicationGlossaryModel Post(ApplicationGlossaryModel value)
        {
            var glossary = new ApplicationGlossary
            {
                Definition = value.Definition,
                AddedByUserId = value.AddedByUserID.Value,
                Name = value.Name,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.ApplicationGlossary.Add(glossary);
            _context.SaveChanges();
            value.ApplicationGlossaryID = glossary.ApplicationGlossaryId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateGlossary")]
        public ApplicationGlossaryModel Put(ApplicationGlossaryModel value)
        {
            var glossary = _context.ApplicationGlossary.SingleOrDefault(p => p.ApplicationGlossaryId == value.ApplicationGlossaryID);

            glossary.Definition = value.Definition;
            glossary.Name = value.Name;
            glossary.ModifiedByUserId = value.ModifiedByUserID;
            glossary.ModifiedDate = DateTime.Now;
            glossary.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteGlossary")]
        public void Delete(int id)
        {
            var glossary = _context.ApplicationGlossary.SingleOrDefault(p => p.ApplicationGlossaryId == id);
            if (glossary != null)
            {
                _context.ApplicationGlossary.Remove(glossary);
                _context.SaveChanges();
            }
        }
        [HttpGet("GetApplicationGloAbbrSearch")]
        public List<ApplicationGloAbbrSearchModel> ApplicationGloAbbrSearchModel(string data, string type)
        {
            List<ApplicationGloAbbrSearchModel> applicationGloAbbrSearchModels = new List<ApplicationGloAbbrSearchModel>();
            if (type == "glossary")
            {
                var glossary = _context.ApplicationGlossary.AsNoTracking().ToList();
                if (data != null)
                {
                    glossary = glossary.Where(s => s.Name.Contains(data)).ToList();
                }
                glossary.ForEach(a =>
                {
                    ApplicationGloAbbrSearchModel gloss = new ApplicationGloAbbrSearchModel
                    {
                        ApplicationGlossaryId = a.ApplicationGlossaryId,
                        Name = a.Name,
                        Definition = a.Definition,

                    };
                    applicationGloAbbrSearchModels.Add(gloss);
                });
            }
            if (type == "abbrevation")
            {
                var glossary = _context.ApplicationGlossary.AsNoTracking().ToList();
                if (data != null)
                {
                    glossary = glossary.Where(s => s.Name.Contains(data)).ToList();
                }
                glossary.ForEach(a =>
                {
                    ApplicationGloAbbrSearchModel gloss = new ApplicationGloAbbrSearchModel
                    {
                        ApplicationGlossaryId = a.ApplicationGlossaryId,
                        Name = a.Name,
                        Definition = a.Definition,

                    };
                    applicationGloAbbrSearchModels.Add(gloss);
                });
                var abbrevation = _context.ApplicationAbbreviation.AsNoTracking().ToList();
                if (data != null)
                {
                    abbrevation = abbrevation.Where(s => s.Name.ToLower().Contains(data.ToLower()) || s.Description.ToLower().Contains(data.ToLower())).ToList();
                }
                abbrevation.ForEach(a =>
                {
                    ApplicationGloAbbrSearchModel abbr = new ApplicationGloAbbrSearchModel
                    {
                        ApplicationAbbreviationId = a.ApplicationAbbreviationId,
                        Name = a.Name,
                        Definition = glossary.Where(s => s.Name.Contains(a.Description)).OrderBy(s => s.ApplicationGlossaryId).Select(s => s.Definition).FirstOrDefault(),
                        Description = a.Description,
                        GlossaryCount = glossary.Where(s => s.Name.Contains(a.Description)).Count(),
                    };
                    applicationGloAbbrSearchModels.Add(abbr);
                });
            }
            if (type == "abbrevationGlossory")
            {
                var glossary = _context.ApplicationGlossary.AsNoTracking().ToList();
                if (data != null)
                {
                    glossary = glossary.Where(s => s.Name.Contains(data) || s.Definition.Contains(data)).ToList();
                }
                glossary.ForEach(a =>
                {
                    ApplicationGloAbbrSearchModel gloss = new ApplicationGloAbbrSearchModel
                    {
                        ApplicationGlossaryId = a.ApplicationGlossaryId,
                        Name = a.Name,
                        Definition = a.Definition,

                    };
                    applicationGloAbbrSearchModels.Add(gloss);
                });
                var abbrevation = _context.ApplicationAbbreviation.AsNoTracking().ToList();
                if (data != null)
                {
                    abbrevation = abbrevation.Where(s => s.Name.ToLower().Contains(data.ToLower()) || s.Description.ToLower().Contains(data.ToLower())).ToList();
                }
                abbrevation.ForEach(a =>
                {
                    ApplicationGloAbbrSearchModel abbr = new ApplicationGloAbbrSearchModel
                    {
                        ApplicationAbbreviationId = a.ApplicationAbbreviationId,
                        Name = a.Name,
                        Definition = glossary.Where(s => s.Name.Contains(a.Description)).OrderBy(s => s.ApplicationGlossaryId).Select(s => s.Definition).FirstOrDefault(),
                        Description = a.Description,
                        GlossaryCount = glossary.Where(s => s.Name.Contains(a.Description)).Count(),
                    };
                    applicationGloAbbrSearchModels.Add(abbr);
                });
            }
            return applicationGloAbbrSearchModels;
        }
    }
}