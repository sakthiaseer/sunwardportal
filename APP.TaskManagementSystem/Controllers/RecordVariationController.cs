﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RecordVariationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public RecordVariationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetRecordVariation")]
        public List<RecordVariationModel> Get()
        {
            List<RecordVariationModel> recordVariationModels = new List<RecordVariationModel>();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var recordVariationLine = _context.RecordVariationLine.AsNoTracking().ToList();
            var RecordVariation = _context.RecordVariation
                .Include(i => i.AddedByUser)
                .Include(i => i.ModifiedByUser)
                .Include(i => i.StatusCode)
                .Include(i => i.RegisterationCode)
                .Include(i => i.SubmissionStatus)
                .Include(s => s.Product).AsNoTracking().ToList();
            if (RecordVariation != null && RecordVariation.Count > 0)
            {
                RecordVariation.ForEach(s =>
                {
                    RecordVariationModel recordVariationModel = new RecordVariationModel();

                    recordVariationModel.RecordVariationId = s.RecordVariationId;
                    recordVariationModel.RelatedChangeControlNo = s.RelatedChangeControlNo;
                    recordVariationModel.RegisterationCodeId = s.RegisterationCodeId;
                    recordVariationModel.RegisterationCodeName = s.RegisterationCode?.CodeValue;
                    recordVariationModel.SubmissionDate = s.SubmissionDate;
                    recordVariationModel.RegistrationHolderId = s.RegistrationHolderId;
                    recordVariationModel.RegisterCountryId = s.Product?.RegisterCountry;
                    recordVariationModel.SubmittedPaidDate = s.SubmittedPaidDate;
                    recordVariationModel.EstimateApprovalDate = s.EstimateApprovalDate;
                    recordVariationModel.ProductId = s.ProductId;
                    recordVariationModel.VariationNo = s.VariationNo;
                    recordVariationModel.StatusCodeID = s.StatusCodeId;
                    recordVariationModel.AddedByUserID = s.AddedByUserId;
                    recordVariationModel.ModifiedByUserID = s.ModifiedByUserId;
                    recordVariationModel.DocumentLink = s.DocumentLink;
                    recordVariationModel.AddedDate = s.AddedDate;
                    recordVariationModel.ModifiedDate = s.ModifiedDate;
                    recordVariationModel.AddedByUser = s.AddedByUser?.UserName;
                    recordVariationModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    recordVariationModel.StatusCode = s.StatusCode?.CodeValue;
                    recordVariationModel.EstimateSubmissionDate = s.EstimateSubmissionDate;
                    recordVariationModel.SubmissionStatusId = s.SubmissionStatusId;
                    recordVariationModel.SubmissionStatus = s.SubmissionStatus?.CodeValue;
                    recordVariationModel.SessionId = s.SessionId;

                    if (s.Product != null)
                    {
                        recordVariationModel.ProductName = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList?.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList?.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)).Value : "";
                        recordVariationModel.RegisterCountryName = masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterCountryId).Select(m => m.Value).FirstOrDefault() : "";
                        recordVariationModel.ManufacturingSite = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList?.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList?.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)).Value : "";
                        recordVariationModel.PRHSpecificName = s.ProductId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.Product.PrhspecificProductId).Select(m => m.Value).FirstOrDefault() : "");
                    }
                    if (recordVariationLine != null && recordVariationLine.Count > 0)
                    {
                        recordVariationModel.RecordVariationLineItems = recordVariationLine.Select(h => new RecordVariationLineModel
                        {
                            RecordVariationLineId = h.RecordVariationLineId,
                            RecordVariationId = h.RecordVariationId,
                            VariationCodeId = h.VariationCodeId,
                            ApprovalDate = h.ApprovalDate,
                            ProjectApprovalDate = h.ProjectApprovalDate,
                            VariationCodeName = (masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == h.VariationCodeId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == h.VariationCodeId).Value : "") + " | " + (masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == h.VariationCodeId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == h.VariationCodeId).Description : ""),
                            CorrespondenceLink = h.CorrespondenceLink,
                            VariationForm = h.VariationForm,
                            RegistrationVariationId=h.RegistrationVariationId,
                        }).Where(w => w.RecordVariationId == s.RecordVariationId).OrderByDescending(o => o.RecordVariationLineId).ToList();
                    }
                    recordVariationModels.Add(recordVariationModel);
                });
            }
            return recordVariationModels.OrderByDescending(o => o.RecordVariationId).ToList();
        }
        [HttpGet]
        [Route("GetRegisterCountry")]
        public List<FinishProductGeneralInfoModel> GetRegisterCountry(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<FinishProductGeneralInfoModel> finishProductGeneralInfoModels = new List<FinishProductGeneralInfoModel>();
            var GetRegisterCountry = _context.FinishProductGeneralInfo.Where(a => a.ProductRegistrationHolderId == id).AsNoTracking().ToList();
            GetRegisterCountry.ForEach(s =>
            {
                FinishProductGeneralInfoModel finishProductGeneralInfoModel = new FinishProductGeneralInfoModel
                {
                    RegisterCountry = s.RegisterCountry,
                    ProductRegistrationHolderId = s.ProductRegistrationHolderId,
                    CountryName = s.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterCountry).FirstOrDefault().Value : "",
                };
                finishProductGeneralInfoModels.Add(finishProductGeneralInfoModel);
            });
            return finishProductGeneralInfoModels.Where(a => a.CountryName != "").ToList();
        }
        [HttpGet]
        [Route("GetFinishProduct")]
        public List<FinishProductGeneralInfoModel> GetFinishProduct(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<FinishProductGeneralInfoModel> finishProductGeneralInfoModels = new List<FinishProductGeneralInfoModel>();
            var GetRegisterCountry = _context.FinishProductGeneralInfo.Include(s => s.FinishProduct).Where(a => a.RegisterCountry == id).AsNoTracking().ToList();
            GetRegisterCountry.ForEach(s =>
            {
                FinishProductGeneralInfoModel finishProductGeneralInfoModel = new FinishProductGeneralInfoModel
                {
                    RegisterCountry = s.RegisterCountry,
                    ProductRegistrationHolderId = s.ProductRegistrationHolderId,
                    FinishProductID = s.FinishProduct.ProductId,
                    CountryName = s.RegisterCountry != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisterCountry).Select(m => m.Value).FirstOrDefault() : "",
                    ManufacturingSite = masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId).Value : "",
                    ProductName = (masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ProductId).Value : "") + " | " + (masterDetailList != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProduct.ManufacturingSiteId).Value : ""),
                };
                finishProductGeneralInfoModels.Add(finishProductGeneralInfoModel);
            });
            return finishProductGeneralInfoModels.OrderBy(o => o.ProductName).ToList();
        }
        [HttpGet]
        [Route("GetRecordVariationLine")]
        public List<RecordVariationLineModel> GetRecordVariationLine(int id)
        {
            List<RecordVariationLineModel> recordVariationLineModels = new List<RecordVariationLineModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RecordVariationLine = _context.RecordVariationLine
                .Include("StatusCode").Include(a => a.RegistrationVariation)
                .Include(d => d.RegistrationVariation.RegistrationCategoryNavigation)
                .Include(e => e.RegistrationVariation.RegistrationClassification)
                .Include(f => f.RegistrationVariation.RegistrationCountry)
                .Include(g => g.RegistrationVariation.RegistrationVariationCode)
                .Include(h => h.RegistrationVariation.RegistrationVariationType)
                .Include(a => a.RegistrationVariation.RegistrationVariationTypeNavigation)
                .Where(w => w.RecordVariationId == id).OrderByDescending(o => o.RecordVariationLineId).AsNoTracking().ToList();
            RecordVariationLine.ForEach(s =>
            {
                RecordVariationLineModel recordVariationLineModel = new RecordVariationLineModel
                {
                    RecordVariationLineId = s.RecordVariationLineId,
                    RecordVariationId = s.RecordVariationId,
                    VariationCodeId = s.VariationCodeId,
                    ApprovalDate = s.ApprovalDate,
                    ProjectApprovalDate = s.ProjectApprovalDate,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode.CodeValue,
                    VariationCodeName = s.RegistrationVariation?.RegistrationVariationCode?.Value + "|" + s.RegistrationVariation?.RegistrationVariationCode?.Description,
                    CorrespondenceLink = s.CorrespondenceLink,
                    VariationForm = s.VariationForm,
                    RegistrationVariationId = s.RegistrationVariationId,
                    VariationCodeOldName = (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCodeId).Value : "") + " | " + (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCodeId).Description : ""),

                };
                recordVariationLineModels.Add(recordVariationLineModel);
            });

            return recordVariationLineModels;
        }
        [HttpGet]
        [Route("GetRecordVariationLineItems")]
        public List<RecordVariationLineModel> GetRecordVariationLineItems()
        {
            List<RecordVariationLineModel> recordVariationLineModels = new List<RecordVariationLineModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RecordVariationLine = _context.RecordVariationLine
                .Include("StatusCode").Include(a => a.RegistrationVariation)
                .Include(d => d.RegistrationVariation.RegistrationCategoryNavigation)
                .Include(e => e.RegistrationVariation.RegistrationClassification)
                .Include(f => f.RegistrationVariation.RegistrationCountry)
                .Include(g => g.RegistrationVariation.RegistrationVariationCode)
                .Include(h => h.RegistrationVariation.RegistrationVariationType)
                .Include(a => a.RegistrationVariation.RegistrationVariationTypeNavigation).OrderByDescending(o => o.RecordVariationLineId).AsNoTracking().ToList();
            RecordVariationLine.ForEach(s =>
            {
                RecordVariationLineModel recordVariationLineModel = new RecordVariationLineModel
                {
                    RecordVariationLineId = s.RecordVariationLineId,
                    RecordVariationId = s.RecordVariationId,
                    VariationCodeId = s.VariationCodeId,
                    ApprovalDate = s.ApprovalDate,
                    ProjectApprovalDate = s.ProjectApprovalDate,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode.CodeValue,
                    VariationCodeName = s.RegistrationVariation?.RegistrationVariationCode?.Value + "|" + s.RegistrationVariation?.RegistrationVariationCode?.Description,
                    CorrespondenceLink = s.CorrespondenceLink,
                    VariationForm = s.VariationForm,
                    RegistrationVariationId = s.RegistrationVariationId,
                };
                recordVariationLineModels.Add(recordVariationLineModel);
            });

            return recordVariationLineModels;
        }
        [HttpGet]
        [Route("GetRecordVariationDetail")]
        public List<RecordVariationDetailModel> GetRecordVariationDetail()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            var FinishProductGeneralInfo = _context.FinishProductGeneralInfo.AsNoTracking().ToList();
            var statusCodeList = _context.CodeMaster.AsNoTracking().ToList();
            var RecordVariation = _context.RecordVariation
                .Include(i => i.AddedByUser)
                .Include(i => i.ModifiedByUser)
                .Include(i => i.StatusCode)
                .Include(i => i.RegisterationCode)
                .Include(i => i.SubmissionStatus)
                .Include(s => s.Product).Include(s => s.RecordVariationLine)
                .AsNoTracking().ToList();
            var recordVariationLine = _context.RecordVariationLine
               .Include(a => a.RegistrationVariation)
               .Include(d => d.RegistrationVariation.RegistrationCategoryNavigation)
               .Include(e => e.RegistrationVariation.RegistrationClassification)
               .Include(f => f.RegistrationVariation.RegistrationCountry)
               .Include(g => g.RegistrationVariation.RegistrationVariationCode)
               .Include(h => h.RegistrationVariation.RegistrationVariationType)
               .Include(a => a.RegistrationVariation.RegistrationVariationTypeNavigation)
               .OrderByDescending(o => o.RecordVariationLineId).AsNoTracking().ToList();
            var RecordVariationDetailList = new List<RecordVariationDetailModel>();
            if (RecordVariation.Count > 0)
            {
                RecordVariation.ForEach(s =>
                {
                    if (s.RecordVariationLine.Count > 0)
                    {
                        s.RecordVariationLine.ToList().ForEach(r =>
                        {
                            var recordVariationLineData = recordVariationLine.Where(w => w.RecordVariationLineId == r.RecordVariationLineId).FirstOrDefault();
                            var recordVariationDetailModeldetail = new RecordVariationDetailModel
                            {
                                RecordVariationId = s.RecordVariationId,
                                RelatedChangeControlNo = s.RelatedChangeControlNo,
                                RegisterationCodeId = s.RegisterationCodeId,
                                RegisterationCodeName = s.RegisterationCode?.CodeValue,
                                SubmissionDate = s.SubmissionDate,
                                RegistrationHolderId = s.RegistrationHolderId,
                                RegisterCountryId = s.Product?.RegisterCountry,
                                SubmittedPaidDate = s.SubmittedPaidDate,
                                EstimateApprovalDate = s.EstimateApprovalDate,
                                ProductId = s.ProductId,
                                VariationNo = s.VariationNo,
                                StatusCodeID = s.StatusCodeId,
                                AddedByUserID = s.AddedByUserId,
                                ModifiedByUserID = s.ModifiedByUserId,
                                DocumentLink = s.DocumentLink,
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,
                                AddedByUser = s.AddedByUser?.UserName,
                                ModifiedByUser = s.ModifiedByUser?.UserName,
                                StatusCode = s.StatusCode?.CodeValue,
                                EstimateSubmissionDate = s.EstimateSubmissionDate,
                                SubmissionStatusId = s.SubmissionStatusId,
                                SubmissionStatus = s.SubmissionStatus?.CodeValue,
                                SessionId = s.SessionId,
                                ProductName = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)).Value : ""),
                                ManufacturingSite = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)).Value : ""),
                                RegisterCountry = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterCountry).Select(m => m.Value).FirstOrDefault() : ""),
                                ProductionRegistrationHolder = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : ""),
                                RegisterProductOwner = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : ""),
                                ChangeControlNo = s.RelatedChangeControlNo,
                                VariationSubmissionDate = s.SubmissionDate,
                                VariationApplicationNo = s.VariationNo,
                                EntryStatus = s.StatusCode.CodeValue,
                                EvaluationApprovedDate = r.ApprovalDate,
                                VariationCode = recordVariationLineData != null ? (recordVariationLineData.RegistrationVariation?.RegistrationVariationCode?.Value + "|" + recordVariationLineData.RegistrationVariation?.RegistrationVariationCode?.Description) : "",
                                CorrespondenceLink = r.CorrespondenceLink,
                                VariationForm = r.VariationForm,
                                RegistrationVariationId = r.RegistrationVariationId,
                                VariationStatus = statusCodeList != null && statusCodeList.FirstOrDefault(a => a.CodeId == r.StatusCodeId) != null ? statusCodeList.FirstOrDefault(a => a.CodeId == r.StatusCodeId).CodeValue : "",
                                VariationCodeOld = (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId).Value : "") + " | " + (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId).Description : ""),

                            };
                            RecordVariationDetailList.Add(recordVariationDetailModeldetail);
                        });
                    }
                    else
                    {
                        var recordVariationDetailModeldetail = new RecordVariationDetailModel
                        {
                            RecordVariationId = s.RecordVariationId,
                            RelatedChangeControlNo = s.RelatedChangeControlNo,
                            RegisterationCodeId = s.RegisterationCodeId,
                            RegisterationCodeName = s.RegisterationCode?.CodeValue,
                            SubmissionDate = s.SubmissionDate,
                            RegistrationHolderId = s.RegistrationHolderId,
                            RegisterCountryId = s.Product?.RegisterCountry,
                            SubmittedPaidDate = s.SubmittedPaidDate,
                            EstimateApprovalDate = s.EstimateApprovalDate,
                            ProductId = s.ProductId,
                            VariationNo = s.VariationNo,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            DocumentLink = s.DocumentLink,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            AddedByUser = s.AddedByUser?.UserName,
                            ModifiedByUser = s.ModifiedByUser?.UserName,
                            StatusCode = s.StatusCode?.CodeValue,
                            EstimateSubmissionDate = s.EstimateSubmissionDate,
                            SubmissionStatusId = s.SubmissionStatusId,
                            SubmissionStatus = s.SubmissionStatus?.CodeValue,
                            SessionId = s.SessionId,
                            ProductName = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)).Value : ""),
                            ManufacturingSite = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)).Value : ""),
                            RegisterCountry = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterCountry).Select(m => m.Value).FirstOrDefault() : ""),
                            ProductionRegistrationHolder = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : ""),
                            RegisterProductOwner = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : ""),
                            ChangeControlNo = s.RelatedChangeControlNo,
                            VariationSubmissionDate = s.SubmissionDate,
                            VariationApplicationNo = s.VariationNo,
                            EntryStatus = s.StatusCode.CodeValue,


                        };
                        RecordVariationDetailList.Add(recordVariationDetailModeldetail);
                    }
                });

            }
            return RecordVariationDetailList;
        }
        [HttpPost]
        [Route("GetRecordVariationDetailSearch")]
        public List<RecordVariationDetailModel> GetRecordVariationDetailSearch(RecordVariationDetailModel SearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finsiproductList = _context.FinishProduct.AsNoTracking().ToList();
            //var FinishProductGeneralInfo = _context.FinishProductGeneralInfo.AsNoTracking().ToList();
            //var statusCodeList = _context.CodeMaster.AsNoTracking().ToList();
            var RecordVariations = _context.RecordVariation
                .Include(i => i.AddedByUser)
                .Include(i => i.ModifiedByUser)
                .Include(i => i.StatusCode)
                .Include(i => i.RegisterationCode)
                .Include(i => i.SubmissionStatus)
                .Include(s => s.Product).Include(s => s.RecordVariationLine).Where(w => w.RecordVariationId > 0);
            var recordVariationLine = _context.RecordVariationLine
               .Include(a => a.RegistrationVariation)
               .Include(s => s.StatusCode)              
               .Include(g => g.RegistrationVariation.RegistrationVariationCode)              
               .OrderByDescending(o => o.RecordVariationLineId).AsNoTracking().ToList();
            var RecordVariationDetailList = new List<RecordVariationDetailModel>();
            if (SearchModel.RelatedChangeControlNo != "")
            {
                RecordVariations = RecordVariations.Where(w => w.RelatedChangeControlNo.Contains(SearchModel.RelatedChangeControlNo));
            }
            if (SearchModel.RegisterCountryId != null)
            {
                RecordVariations = RecordVariations.Where(w => w.RegisterCountryId == SearchModel.RegisterCountryId);
            }
            if (SearchModel.ProductId != null)
            {
                RecordVariations = RecordVariations.Where(w => w.ProductId == SearchModel.ProductId);
            }
            if (SearchModel.SubmissionStatusId != null)
            {
                RecordVariations = RecordVariations.Where(w => w.SubmissionStatusId == SearchModel.SubmissionStatusId);
            }
            if (SearchModel.ProductOwnerId != null)
            {
                RecordVariations = RecordVariations.Where(w => w.Product.RegisterProductOwnerId == SearchModel.ProductOwnerId);
            }
            if (SearchModel.SubmissionDate != null)
            {
                RecordVariations = RecordVariations.Where(w => w.SubmissionDate.Value.Date == SearchModel.SubmissionDate.Value.Date);
            }
            var RecordVariation = RecordVariations.AsNoTracking().ToList();
            if (RecordVariation.Count > 0)
            {
                RecordVariation.ForEach(s =>
                {
                    if (s.RecordVariationLine.Count > 0)
                    {
                        s.RecordVariationLine.ToList().ForEach(r =>
                        {
                            var recordVariationLineData = recordVariationLine.Where(w => w.RecordVariationLineId == r.RecordVariationLineId).FirstOrDefault();
                            var recordVariationDetailModeldetail = new RecordVariationDetailModel
                            {
                                RecordVariationId = s.RecordVariationId,
                                RelatedChangeControlNo = s.RelatedChangeControlNo,
                                RegisterationCodeId = s.RegisterationCodeId,
                                RegisterationCodeName = s.RegisterationCode?.CodeValue,
                                SubmissionDate = s.SubmissionDate,
                                RegistrationHolderId = s.RegistrationHolderId,
                                RegisterCountryId = s.Product?.RegisterCountry,
                                SubmittedPaidDate = s.SubmittedPaidDate,
                                EstimateApprovalDate = s.EstimateApprovalDate,
                                ProductId = s.ProductId,
                                VariationNo = s.VariationNo,
                                StatusCodeID = s.StatusCodeId,
                                AddedByUserID = s.AddedByUserId,
                                ModifiedByUserID = s.ModifiedByUserId,
                                DocumentLink = s.DocumentLink,
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,
                                AddedByUser = s.AddedByUser?.UserName,
                                ModifiedByUser = s.ModifiedByUser?.UserName,
                                StatusCode = s.StatusCode?.CodeValue,
                                EstimateSubmissionDate = s.EstimateSubmissionDate,
                                SubmissionStatusId = s.SubmissionStatusId,
                                SubmissionStatus = s.SubmissionStatus?.CodeValue,
                                SessionId = s.SessionId,
                                ProductName = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)).Value : ""),
                                ManufacturingSite = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)).Value : ""),
                                RegisterCountry = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterCountry).Select(m => m.Value).FirstOrDefault() : ""),
                                ProductionRegistrationHolder = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : ""),
                                RegisterProductOwner = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : ""),
                                ChangeControlNo = s.RelatedChangeControlNo,
                                VariationSubmissionDate = s.SubmissionDate,
                                VariationApplicationNo = s.VariationNo,
                                EntryStatus = s.StatusCode.CodeValue,
                                EvaluationApprovedDate = r.ApprovalDate,
                                VariationCodeId = recordVariationLineData?.VariationCodeId,
                                VariationCode = recordVariationLineData != null ? (recordVariationLineData.RegistrationVariation?.RegistrationVariationCode?.Value + "|" + recordVariationLineData.RegistrationVariation?.RegistrationVariationCode?.Description) : "",
                                CorrespondenceLink = r.CorrespondenceLink,
                                VariationForm = r.VariationForm,
                                RegistrationVariationId = r.RegistrationVariationId,
                                VariationCodeOld = (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId).Value : "") + " | " + (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.VariationCodeId).Description : ""),
                                VariationStatus = recordVariationLineData != null ? recordVariationLineData?.StatusCode?.CodeValue : "",
                            };
                            RecordVariationDetailList.Add(recordVariationDetailModeldetail);
                        });
                    }
                    else
                    {
                        var recordVariationDetailModeldetail = new RecordVariationDetailModel
                        {
                            RecordVariationId = s.RecordVariationId,
                            RelatedChangeControlNo = s.RelatedChangeControlNo,
                            RegisterationCodeId = s.RegisterationCodeId,
                            RegisterationCodeName = s.RegisterationCode?.CodeValue,
                            SubmissionDate = s.SubmissionDate,
                            RegistrationHolderId = s.RegistrationHolderId,
                            RegisterCountryId = s.Product?.RegisterCountry,
                            SubmittedPaidDate = s.SubmittedPaidDate,
                            EstimateApprovalDate = s.EstimateApprovalDate,
                            ProductId = s.ProductId,
                            VariationNo = s.VariationNo,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            DocumentLink = s.DocumentLink,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            AddedByUser = s.AddedByUser?.UserName,
                            ModifiedByUser = s.ModifiedByUser?.UserName,
                            StatusCode = s.StatusCode?.CodeValue,
                            EstimateSubmissionDate = s.EstimateSubmissionDate,
                            SubmissionStatusId = s.SubmissionStatusId,
                            SubmissionStatus = s.SubmissionStatus?.CodeValue,
                            SessionId = s.SessionId,
                            ProductName = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ProductId)).Value : ""),
                            ManufacturingSite = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.Product.FinishProductId).ManufacturingSiteId)).Value : ""),
                            RegisterCountry = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterCountry).Select(m => m.Value).FirstOrDefault() : ""),
                            ProductionRegistrationHolder = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.ProductRegistrationHolderId).Select(m => m.Value).FirstOrDefault() : ""),
                            RegisterProductOwner = s.ProductId == null ? "" : (applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.Product.RegisterProductOwnerId).Select(m => m.Value).FirstOrDefault() : ""),
                            ChangeControlNo = s.RelatedChangeControlNo,
                            VariationSubmissionDate = s.SubmissionDate,
                            VariationApplicationNo = s.VariationNo,
                            EntryStatus = s.StatusCode.CodeValue,


                        };
                        RecordVariationDetailList.Add(recordVariationDetailModeldetail);
                    }
                });

            }
            if (SearchModel.VariationCodeId != null)
            {
                RecordVariationDetailList = RecordVariationDetailList.Where(w => w.RegistrationVariationId == SearchModel.VariationCodeId).ToList();
            }
            return RecordVariationDetailList.ToList();
        }
        [HttpGet]
        [Route("GetRegistrationVariationCountry")]
        public List<RegistrationVariationModel> GetRegistrationVariationCountry(int id)
        {
            List<RegistrationVariationModel> registrationVariationModels = new List<RegistrationVariationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var VariationList = _context.RegistrationVariationType.AsNoTracking().ToList();
            var RegistrationVariation = _context.RegistrationVariation
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatustCode)
                .Include(r => r.RegisterationCode)
                .Where(w => w.RegistrationCountryId == id).OrderByDescending(o => o.RegistrationVariationId).AsNoTracking().ToList();
            RegistrationVariation.ForEach(s =>
            {
                RegistrationVariationModel registrationVariationModel = new RegistrationVariationModel
                {
                    RegistrationVariationId = s.RegistrationVariationId,
                    RegistrationCountryId = s.RegistrationCountryId,
                    RegistrationCountryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCountryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationCountryId).Value : "",
                    RegistrationClassificationId = s.RegistrationClassificationId,
                    RegistrationClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationClassificationId).Value : "",
                    RegistrationVariationCodeId = s.RegistrationVariationCodeId,
                    RegistrationVariationCodeName = (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId).Value : "") + " | " + (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId).Description : ""),
                    VariationTitle = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.RegistrationVariationCodeId).Description : "",
                    RegistrationVariationTypeId = VariationList.Where(p => p.RegistrationVariationId == s.RegistrationVariationId).Select(p => p.VariationTypeId).ToList(),
                    RegistrationCategory = s.RegistrationCategory,
                    RegisterationCodeId = s.RegisterationCodeId,
                    RegisterationCodeName = s.RegisterationCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatustCode?.CodeValue,
                    StatusCodeID = s.StatustCodeId,
                };
                registrationVariationModels.Add(registrationVariationModel);
            });

            return registrationVariationModels;
        }

        [HttpGet]
        [Route("GetRecordVariationDocument")]
        public List<RecordVariationDocumentLinkModel> GetRecordVariationDocument(int id)
        {
            var recordDocumentlink = new List<RecordVariationDocumentLinkModel>();

            recordDocumentlink = _context.RecordVariationDocumentLink.Select(s => new RecordVariationDocumentLinkModel
            {
                RecordVariationDocumentLinkID = s.RecordVariationDocumentLinkId,
                RecordVariationID = s.RecordVariationId,
                RecordVariationLineID = s.RecordVariationLineId,
                IsHeader = s.IsHeader,
                DocumentLink = s.DocumentLink,

            }).Where(t => t.RecordVariationID == id).AsNoTracking().ToList();
            return recordDocumentlink;
        }
        [HttpGet]
        [Route("GetRecordVariationLineDocument")]
        public List<RecordVariationDocumentLinkModel> GetRecordVariationLineDocument(int id)
        {
            var recordDocumentlink = new List<RecordVariationDocumentLinkModel>();

            recordDocumentlink = _context.RecordVariationDocumentLink.Select(s => new RecordVariationDocumentLinkModel
            {
                RecordVariationDocumentLinkID = s.RecordVariationDocumentLinkId,
                RecordVariationID = s.RecordVariationId,
                RecordVariationLineID = s.RecordVariationLineId,
                IsHeader = s.IsHeader,
                DocumentLink = s.DocumentLink,

            }).Where(t => t.RecordVariationLineID == id).AsNoTracking().ToList();
            return recordDocumentlink;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RecordVariationModel> GetData(SearchModel searchModel)
        {
            var RecordVariation = new RecordVariation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).FirstOrDefault();
                        break;
                    case "Last":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).LastOrDefault();
                        break;
                    case "Next":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).LastOrDefault();
                        break;
                    case "Previous":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).FirstOrDefault();
                        break;
                    case "Last":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).LastOrDefault();
                        break;
                    case "Next":
                        RecordVariation = _context.RecordVariation.OrderBy(o => o.RecordVariationId).FirstOrDefault(s => s.RecordVariationId > searchModel.Id);
                        break;
                    case "Previous":
                        RecordVariation = _context.RecordVariation.OrderByDescending(o => o.RecordVariationId).FirstOrDefault(s => s.RecordVariationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<RecordVariationModel>(RecordVariation);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var finishProductItem = result.ProductId > 0 ? (_context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.ProductId).FinishProductId) : null;
                if (finishProductItem != null)
                {
                    result.RegisterCountryId = _context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.ProductId).RegisterCountry;

                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }
            }
            return result;
        }
        [HttpPost]
        [Route("InsertRecordVariation")]
        public RecordVariationModel Post(RecordVariationModel value)
        {
            var SessionId = Guid.NewGuid();
            var RecordVariation = new RecordVariation
            {
                RelatedChangeControlNo = value.RelatedChangeControlNo,
                SubmissionDate = value.SubmissionDate,
                RegistrationHolderId = value.RegistrationHolderId,
                RegisterationCodeId = value.RegisterationCodeId,
                RegisterCountryId = value.RegisterCountryId,
                DocumentLink = value.DocumentLink,
                SubmittedPaidDate = value.SubmittedPaidDate,
                EstimateApprovalDate = value.EstimateApprovalDate,
                ProductId = value.ProductId,
                VariationNo = value.VariationNo,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                EstimateSubmissionDate = value.EstimateSubmissionDate,
                SubmissionStatusId = value.SubmissionStatusId,
                SessionId = SessionId,
            };
            _context.RecordVariation.Add(RecordVariation);
            _context.SaveChanges();
            value.RecordVariationId = RecordVariation.RecordVariationId;
            value.SessionId = SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateRecordVariation")]
        public RecordVariationModel Put(RecordVariationModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var RecordVariation = _context.RecordVariation.SingleOrDefault(p => p.RecordVariationId == value.RecordVariationId);
            if (RecordVariation != null)
            {
                RecordVariation.RelatedChangeControlNo = value.RelatedChangeControlNo;
                RecordVariation.RegisterationCodeId = value.RegisterationCodeId;
                RecordVariation.SubmissionDate = value.SubmissionDate;
                RecordVariation.RegistrationHolderId = value.RegistrationHolderId;
                RecordVariation.RegisterCountryId = value.RegisterCountryId;
                RecordVariation.ProductId = value.ProductId;
                RecordVariation.DocumentLink = value.DocumentLink;
                RecordVariation.VariationNo = value.VariationNo;
                RecordVariation.SubmittedPaidDate = value.SubmittedPaidDate;
                RecordVariation.EstimateApprovalDate = value.EstimateApprovalDate;
                RecordVariation.ModifiedByUserId = value.ModifiedByUserID;
                RecordVariation.StatusCodeId = value.StatusCodeID.Value;
                RecordVariation.ModifiedDate = DateTime.Now;
                RecordVariation.EstimateSubmissionDate = value.EstimateSubmissionDate;
                RecordVariation.SubmissionStatusId = value.SubmissionStatusId.Value;
                RecordVariation.SessionId = value.SessionId;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertRecordVariationLine")]
        public RecordVariationLineModel InsertRecordVariationLine(RecordVariationLineModel value)
        {
            var RecordVariationLine = new RecordVariationLine
            {
                RecordVariationId = value.RecordVariationId,
                VariationCodeId = value.VariationCodeId,
                ApprovalDate = value.ApprovalDate,
                ProjectApprovalDate = value.ProjectApprovalDate,
                StatusCodeId = value.StatusCodeID.Value,
                CorrespondenceLink = value.CorrespondenceLink,
                VariationForm = value.VariationForm,
                RegistrationVariationId = value.RegistrationVariationId,
            };
            _context.RecordVariationLine.Add(RecordVariationLine);
            _context.SaveChanges();
            value.RecordVariationLineId = RecordVariationLine.RecordVariationLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateRecordVariationLine")]
        public RecordVariationLineModel Put(RecordVariationLineModel value)
        {
            var RecordVariationLine = _context.RecordVariationLine.SingleOrDefault(p => p.RecordVariationLineId == value.RecordVariationLineId);
            RecordVariationLine.RecordVariationId = value.RecordVariationId;
            RecordVariationLine.VariationCodeId = value.VariationCodeId;
            RecordVariationLine.ApprovalDate = value.ApprovalDate;
            RecordVariationLine.ProjectApprovalDate = value.ProjectApprovalDate;
            RecordVariationLine.StatusCodeId = value.StatusCodeID.Value;
            RecordVariationLine.StatusCodeId = value.StatusCodeID.Value;
            RecordVariationLine.CorrespondenceLink = value.CorrespondenceLink;
            RecordVariationLine.VariationForm = value.VariationForm;
            RecordVariationLine.RegistrationVariationId = value.RegistrationVariationId;
            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("InsertRecordVariationDocumentLink")]
        public RecordVariationDocumentLinkModel InsertRecordVariationDocumentLink(RecordVariationDocumentLinkModel value)
        {
            var RecordVariationDocument = new RecordVariationDocumentLink
            {
                RecordVariationId = value.RecordVariationID,
                RecordVariationLineId = value.RecordVariationLineID,
                IsHeader = value.IsHeader,
                DocumentLink = value.DocumentLink,
            };
            _context.RecordVariationDocumentLink.Add(RecordVariationDocument);
            _context.SaveChanges();
            value.RecordVariationDocumentLinkID = RecordVariationDocument.RecordVariationDocumentLinkId;
            return value;
        }
        [HttpDelete]
        [Route("DeleteRecordDocumentLink")]
        public ActionResult<string> DeleteRecordDocumentLink(int id)
        {
            try
            {
                var recordVariationDocument = _context.RecordVariationDocumentLink.Where(p => p.RecordVariationDocumentLinkId == id).FirstOrDefault();
                if (recordVariationDocument != null)
                {
                    _context.RecordVariationDocumentLink.Remove(recordVariationDocument);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteRecordVariation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var recordVariation = _context.RecordVariation.Where(p => p.RecordVariationId == id).FirstOrDefault();
                if (recordVariation != null)
                {
                    var RecordVariationLine = _context.RecordVariationLine.Where(p => p.RecordVariationId == id).AsNoTracking().ToList();
                    if (RecordVariationLine != null)
                    {
                        _context.RecordVariationLine.RemoveRange(RecordVariationLine);
                        _context.SaveChanges();
                    }
                    _context.RecordVariation.Remove(recordVariation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteRecordVariationLine")]
        public ActionResult<string> DeleteRecordVariationLine(int id)
        {
            try
            {
                var RecordVariationLine = _context.RecordVariationLine.SingleOrDefault(p => p.RecordVariationLineId == id);
                if (RecordVariationLine != null)
                {
                    _context.RecordVariationLine.Remove(RecordVariationLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}