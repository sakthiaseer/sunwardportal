﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.TaskManagementSystem.Helper;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Dynamic;
using Microsoft.OData.Edm;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesOrderEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SalesOrderEntryController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetSalesOrderEntryItems")]
        public List<SalesOrderEntryModel> Get()
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(a => a.AddedByUser)
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.OrderBy)
                .Include(c => c.BlanketOrderNo)
                .Include(l => l.Company).AsNoTracking()
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            if (SalesOrderEntrys != null && SalesOrderEntrys.Count > 0)
            {
                var masterdetailIds = SalesOrderEntrys?.Where(s => s.TenderAgencyId != null).Select(s => s.TenderAgencyId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(s => masterdetailIds.Contains(s.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }

                SalesOrderEntrys.ForEach(s =>
                {
                    SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                    salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    salesOrderEntryModel.CompanyID = s.CompanyId;
                    salesOrderEntryModel.ProfileID = s.ProfileId;
                    salesOrderEntryModel.OrderByID = s.OrderById;
                    salesOrderEntryModel.CustomerID = s.CustomerId;
                    salesOrderEntryModel.TenderAgencyID = s.TenderAgencyId;
                    salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                    salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                    if (s.TypeOfSalesOrderId == 1651)
                    {
                        salesOrderEntryModel.OrderNo = s.BlanketOrderNo?.OrderNo;
                    }
                    else
                    {
                        salesOrderEntryModel.OrderNo = s.OrderNo;
                    }
                    salesOrderEntryModel.QuotationNo = s.QuotationNo;
                    salesOrderEntryModel.OrderPeriodFrom = s.OrderPeriodFrom;
                    salesOrderEntryModel.OrderPeriodTo = s.OrderPeriodTo;
                    salesOrderEntryModel.BlanketItemId = s.BlanketItemId;
                    salesOrderEntryModel.IsAllowExtension = s.IsAllowExtension;
                    salesOrderEntryModel.IsExtensionInfoMultiple = s.IsExtensionInfoMultiple;
                    salesOrderEntryModel.IsMultipleQuatationNo = s.IsMultipleQuatationNo;
                    salesOrderEntryModel.IsEachDistribution = s.IsEachDistribution;
                    salesOrderEntryModel.ExtensionMonth = s.ExtensionMonth;
                    salesOrderEntryModel.ExtensionQuantity = s.ExtensionQuantity;
                    salesOrderEntryModel.SessionID = s.SessionId;
                    salesOrderEntryModel.OrderEntryType = s.OrderEntryType;
                    salesOrderEntryModel.ProfileReferenceNo = s.ProfileReferenceNo;
                    salesOrderEntryModel.StatusCodeID = s.StatusCodeId;
                    salesOrderEntryModel.AddedByUserID = s.AddedByUserId;
                    salesOrderEntryModel.ModifiedByUserID = s.ModifiedByUserId;
                    salesOrderEntryModel.AddedDate = s.AddedDate;
                    salesOrderEntryModel.BlanketOrderNoId = s.BlanketOrderNoId;
                    salesOrderEntryModel.ProductItemId = s.ProductItemId;
                    salesOrderEntryModel.ModifiedDate = s.ModifiedDate;
                    salesOrderEntryModel.IsTagAlongWithTenderAgency = s.IsTagAlongWithTenderAgency;
                    salesOrderEntryModel.IsWithOrderPeriod = s.IsWithOrderPeriod;
                    salesOrderEntryModel.TagAlongTenderNoID = s.TagAlongTenderNoId;
                    salesOrderEntryModel.TagAlongTenderNo = s.TagAlongTenderNo?.OrderNo;
                    salesOrderEntryModel.DateOfOrder = s.DateOfOrder;
                    salesOrderEntryModel.Ponumber = s.Ponumber;
                    salesOrderEntryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    if (s.CustomerId > 0)
                    {
                        salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;
                    }
                    if (s.OrderById == 1403)
                    {
                        salesOrderEntryModel.CustomerName = masterDetailList != null && s.TenderAgencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.TenderAgencyId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    salesOrderEntryModel.OrderByName = s.OrderBy?.CodeValue;
                    salesOrderEntryModel.TypeOfOrderName = s.TypeOfOrder?.CodeValue;
                    salesOrderEntryModel.CompanyName = s.Company?.PlantCode;
                    salesOrderEntryModel.TagAlongCustomerID = s.TagAlongCustomerId;
                    salesOrderEntryModel.TagAlongTenderID = s.TagAlongTenderId;

                    SalesOrderEntryModels.Add(salesOrderEntryModel);
                });
            }
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }

        [HttpGet]
        [Route("GetSalesOrderEntryItemsForDropDown")]
        public List<SalesOrderEntryModel> GetSalesOrderEntryItemsForDropDown()
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            var salesOrderIds = _context.PurchaseItemSalesEntryLine.Where(s => s.StatusCodeId != 1421).AsNoTracking().Select(s => s.SalesOrderEntryId).ToList();
            var purchaseItemLines = _context.PurchaseItemSalesEntryLine.Include(s => s.SobyCustomers).Where(s => s.StatusCodeId != 1421).AsNoTracking().ToList();
            if (purchaseItemLines != null && purchaseItemLines.Count > 0)
            {
                var masterdetailIds = purchaseItemLines?.Where(s => s.SobyCustomers.PerUomId != null).Select(s => s.SobyCustomers.PerUomId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterdetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            if (salesOrderIds != null && salesOrderIds.Count > 0)
            {
                var SalesOrderEntrys = _context.SalesOrderEntry.Include(s => s.PurchaseItemSalesEntryLine).Where(s => s.TypeOfSalesOrderId == 1403 && salesOrderIds.Contains(s.SalesOrderEntryId)).Distinct()
                    .AsNoTracking().ToList();
                if (SalesOrderEntrys != null && salesOrderIds.Count > 0)
                {
                    SalesOrderEntrys.ForEach(s =>
                    {
                        SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();
                        List<SalesEntryProduct> salesEntryProducts = new List<SalesEntryProduct>();
                        salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                        salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                        salesOrderEntryModel.OrderNo = s.OrderNo;
                        salesOrderEntryModel.StatusCodeID = s.StatusCodeId;
                        var purchaseItems = purchaseItemLines?.Where(p => p.SalesOrderEntryId == s.SalesOrderEntryId).ToList();
                        if (purchaseItems != null && purchaseItems.Count > 0)
                        {
                            purchaseItems.ForEach(p =>
                            {
                                SalesEntryProduct salesEntryProduct = new SalesEntryProduct();
                                salesEntryProduct.PurchaseItemSalesEntryLineID = p.PurchaseItemSalesEntryLineId;
                                salesEntryProduct.Description = p.SobyCustomers?.Description;
                                salesEntryProduct.PUOM = masterDetailList.Where(s => s.ApplicationMasterDetailId == p.SobyCustomers?.PerUomId).Select(s => s.Value).FirstOrDefault();
                                salesEntryProducts.Add(salesEntryProduct);
                            });

                        }
                        if (salesEntryProducts != null && salesEntryProducts.Count > 0)
                        {
                            salesOrderEntryModel.SalesEntryProducts = salesEntryProducts;
                        }
                        SalesOrderEntryModels.Add(salesOrderEntryModel);
                    });
                }
            }
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }
        [HttpGet]
        [Route("GetSalesOrderEntryItemsByType")]
        public List<SalesOrderEntryModel> GetSalesOrderEntryItemsByType(int id)
        {

            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(a => a.AddedByUser)
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.OrderBy)
                .Include(c => c.BlanketOrderNo)
                 .Include(c => c.StatusCode)
                .Include(l => l.Company).Where(s => s.TypeOfSalesOrderId == id || s.OrderById == id).AsNoTracking()
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                salesOrderEntryModel.CompanyID = s.CompanyId;
                salesOrderEntryModel.ProfileID = s.ProfileId;
                salesOrderEntryModel.OrderByID = s.OrderById;
                salesOrderEntryModel.CustomerID = s.CustomerId;
                salesOrderEntryModel.TenderAgencyID = s.TenderAgencyId;
                salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                if (s.TypeOfSalesOrderId == 1651)
                {
                    salesOrderEntryModel.OrderNo = s.BlanketOrderNo?.OrderNo;
                }
                else
                {
                    salesOrderEntryModel.OrderNo = s.OrderNo;
                }
                salesOrderEntryModel.QuotationNo = s.QuotationNo;
                salesOrderEntryModel.OrderPeriodFrom = s.OrderPeriodFrom;
                salesOrderEntryModel.OrderPeriodTo = s.OrderPeriodTo;
                salesOrderEntryModel.BlanketItemId = s.BlanketItemId;
                salesOrderEntryModel.IsAllowExtension = s.IsAllowExtension;
                salesOrderEntryModel.IsExtensionInfoMultiple = s.IsExtensionInfoMultiple;
                salesOrderEntryModel.IsMultipleQuatationNo = s.IsMultipleQuatationNo;
                salesOrderEntryModel.IsEachDistribution = s.IsEachDistribution;
                salesOrderEntryModel.ExtensionMonth = s.ExtensionMonth;
                salesOrderEntryModel.ExtensionQuantity = s.ExtensionQuantity;
                salesOrderEntryModel.SessionID = s.SessionId;
                salesOrderEntryModel.OrderEntryType = s.OrderEntryType;
                salesOrderEntryModel.ProfileReferenceNo = s.ProfileReferenceNo;
                salesOrderEntryModel.StatusCodeID = s.StatusCodeId;
                salesOrderEntryModel.AddedByUserID = s.AddedByUserId;
                salesOrderEntryModel.ModifiedByUserID = s.ModifiedByUserId;
                salesOrderEntryModel.AddedDate = s.AddedDate;
                salesOrderEntryModel.ModifiedDate = s.ModifiedDate;
                salesOrderEntryModel.IsTagAlongWithTenderAgency = s.IsTagAlongWithTenderAgency;
                salesOrderEntryModel.IsWithOrderPeriod = s.IsWithOrderPeriod;
                salesOrderEntryModel.TagAlongTenderNoID = s.TagAlongTenderNoId;
                salesOrderEntryModel.TagAlongTenderNo = s.TagAlongTenderNo?.OrderNo;
                salesOrderEntryModel.DateOfOrder = s.DateOfOrder;
                salesOrderEntryModel.Ponumber = s.Ponumber;
                salesOrderEntryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                salesOrderEntryModel.StatusCode = s.StatusCode?.CodeValue;
                salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;
                salesOrderEntryModel.OrderByName = s.OrderBy?.CodeValue;
                salesOrderEntryModel.TypeOfOrderName = s.TypeOfOrder?.CodeValue;
                salesOrderEntryModel.CompanyName = s.Company?.PlantCode;
                salesOrderEntryModel.BlanketOrderNoId = s.BlanketOrderNoId;
                salesOrderEntryModel.TagAlongCustomerID = s.TagAlongCustomerId;
                salesOrderEntryModel.TagAlongTenderID = s.TagAlongTenderId;
                salesOrderEntryModel.ProductItemId = s.ProductItemId;
                SalesOrderEntryModels.Add(salesOrderEntryModel);
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }

        [HttpGet]
        [Route("GetContractNosByCustomer")]
        public List<SalesOrderEntryModel> GetContractNosByCustomer(int id)
        {

            var customerSalesIds = _context.SalesOrderEntry.Where(s => s.CustomerId == id).AsNoTracking().Select(s => s.SalesOrderEntryId).ToList();
            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.StatusCode)
                .Where(c => customerSalesIds.Contains(c.SalesOrderEntryId))
                .AsNoTracking()
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                salesOrderEntryModel.CompanyID = s.CompanyId;
                salesOrderEntryModel.OrderByID = s.OrderById;
                salesOrderEntryModel.CustomerID = s.CustomerId;
                salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                salesOrderEntryModel.OrderNo = s.OrderNo;
                salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;

                SalesOrderEntryModels.Add(salesOrderEntryModel);
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }
        [HttpPost]
        [Route("GetCustomerTransferContractNos")]
        public List<SalesOrderEntryModel> GetCustomerTransferContractNos(GetTransferContractNoModel getTransferContractNoModel)
        {
            var purchaseIds = _context.ContractDistributionSalesEntryLine.Where(s => s.SocustomerId == getTransferContractNoModel.CustomerId).AsNoTracking().Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            var productSalescustomerId = _context.PurchaseItemSalesEntryLine.Where(s => s.SobyCustomersId == getTransferContractNoModel.ProductId && purchaseIds.Contains(s.PurchaseItemSalesEntryLineId)).AsNoTracking().Select(s => s.SalesOrderEntryId).ToList();
            var customerSalesIds = _context.SalesOrderEntry.Where(s => productSalescustomerId.Contains(s.SalesOrderEntryId)).AsNoTracking().Select(s => s.SalesOrderEntryId).ToList();
            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.StatusCode)
                .Where(c => customerSalesIds.Contains(c.SalesOrderEntryId))
                .AsNoTracking()
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                salesOrderEntryModel.CompanyID = s.CompanyId;
                salesOrderEntryModel.OrderByID = s.OrderById;
                salesOrderEntryModel.CustomerID = s.CustomerId;
                salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                salesOrderEntryModel.OrderNo = s.OrderNo;
                salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;

                SalesOrderEntryModels.Add(salesOrderEntryModel);
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).Where(s => s.SalesOrderEntryID != getTransferContractNoModel.SalesOrderEntryId).ToList();
        }
        [HttpGet]
        [Route("GetContractDistributionItemsBySalesId")]
        public List<ContractDistributionSalesEntryLineModel> GetContractDistributionItemsBySalesId(int id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var PurchaseInfoIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).AsNoTracking().Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            var ContractDistributionSalesEntryLines = _context.ContractDistributionSalesEntryLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(m => m.Socustomer)
                .Include(s => s.StatusCode).Where(s => PurchaseInfoIds.Contains(s.PurchaseItemSalesEntryLineId.Value)).AsNoTracking()
                .ToList();
            if (ContractDistributionSalesEntryLines != null && ContractDistributionSalesEntryLines.Count > 0)
            {
                var masterDetailIds = ContractDistributionSalesEntryLines.Where(c => c.Uomid != null).Select(c => c.Uomid).ToList();
                if (masterDetailIds != null && masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            List<ContractDistributionSalesEntryLineModel> ContractDistributionSalesEntryLineModels = new List<ContractDistributionSalesEntryLineModel>();
            ContractDistributionSalesEntryLines.ForEach(s =>
            {
                ContractDistributionSalesEntryLineModel ContractDistributionSalesEntryLineModel = new ContractDistributionSalesEntryLineModel
                {
                    ContractDistributionSalesEntryLineID = s.ContractDistributionSalesEntryLineId,
                    NoOfLots = s.NoOfLots,
                    TotalQty = s.TotalQty,
                    PONumber = s.Ponumber,
                    Uomid = s.Uomid,
                    SOCustomerID = s.SocustomerId,
                    PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId,
                    SessionID = s.SessionId,
                    CustomerName = s.Socustomer?.CompanyName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    UOM = masterDetailList != null && s.Uomid > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "",

                };
                ContractDistributionSalesEntryLineModels.Add(ContractDistributionSalesEntryLineModel);
            });
            return ContractDistributionSalesEntryLineModels.OrderByDescending(a => a.ContractDistributionSalesEntryLineID).ToList();
        }

        [HttpGet]
        [Route("GetProjectedDeliveryBySalesId")]
        public List<ProjectedDeliverySalesOrderLineModel> GetProjectedDeliveryBySalesId(int? id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var purchaseInfoIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).AsNoTracking().Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            var contractDistributionIds = _context.ContractDistributionSalesEntryLine.Where(s => purchaseInfoIds.Contains(s.PurchaseItemSalesEntryLineId.Value)).AsNoTracking().Select(s => s.ContractDistributionSalesEntryLineId).ToList();
            var projectedDeliverySalesOrderLine = _context.ProjectedDeliverySalesOrderLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => contractDistributionIds.Contains(s.ContractDistributionSalesEntryLineId.Value))
                            .AsNoTracking().ToList();
            List<ProjectedDeliverySalesOrderLineModel> ProjectedDeliverySalesOrderLineModel = new List<ProjectedDeliverySalesOrderLineModel>();
            if (projectedDeliverySalesOrderLine != null && projectedDeliverySalesOrderLine.Count > 0)
            {
                var masterDetailIds = projectedDeliverySalesOrderLine.Where(s => s.FrequencyId != null).Select(s => s.FrequencyId).ToList();
                if (masterDetailIds != null && masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            projectedDeliverySalesOrderLine.ForEach(s =>
            {
                ProjectedDeliverySalesOrderLineModel ProjectedDeliverySalesOrderLineModels = new ProjectedDeliverySalesOrderLineModel
                {
                    ProjectedDeliverySalesOrderLineID = s.ProjectedDeliverySalesOrderLineId,
                    FrequencyID = s.FrequencyId,
                    PerFrequencyQty = s.PerFrequencyQty,
                    IsQtyReference = s.IsQtyReference,
                    StartDate = s.StartDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Frequency = masterDetailList != null && s.FrequencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.FrequencyId).Select(a => a.Value).SingleOrDefault() : "",
                };
                ProjectedDeliverySalesOrderLineModel.Add(ProjectedDeliverySalesOrderLineModels);
            });
            return ProjectedDeliverySalesOrderLineModel.OrderByDescending(a => a.ProjectedDeliverySalesOrderLineID).ToList();
        }

        [HttpGet]
        [Route("GetSalesOrderEntryItemById")]
        public SalesOrderEntryModel GetSalesOrderEntryItemById(int id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var salesOrderEntry = _context.SalesOrderEntry
                .Include(a => a.AddedByUser)
                .Include(c => c.Profile)
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.OrderBy)
                .Include(c => c.BlanketOrderNo)
                 .Include(c => c.PurchaseItemSalesEntryLine)
                .Include(c => c.TagAlongCustomer)
                .Include(b => b.BlanketItem)
                .Include(l => l.Company).Where(s => s.SalesOrderEntryId == id).AsNoTracking()
                .FirstOrDefault();


            SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

            salesOrderEntryModel.SalesOrderEntryID = salesOrderEntry.SalesOrderEntryId;
            salesOrderEntryModel.CompanyID = salesOrderEntry.CompanyId;
            salesOrderEntryModel.ProfileID = salesOrderEntry.ProfileId;
            salesOrderEntryModel.OrderByID = salesOrderEntry.OrderById;
            salesOrderEntryModel.CustomerID = salesOrderEntry.CustomerId;
            salesOrderEntryModel.TenderAgencyID = salesOrderEntry.TenderAgencyId;
            salesOrderEntryModel.TypeOfOrderID = salesOrderEntry.TypeOfOrderId;
            salesOrderEntryModel.TypeOfSalesOrderId = salesOrderEntry.TypeOfSalesOrderId;
            salesOrderEntryModel.BlanketItemId = salesOrderEntry.BlanketItemId;
            salesOrderEntryModel.ItemDescription = salesOrderEntry.BlanketItem?.Description + "|" + salesOrderEntry.BlanketItem?.Description2;
            salesOrderEntryModel.ItemUom = salesOrderEntry.BlanketItem?.BaseUnitofMeasure;
            salesOrderEntryModel.TagAlongOrderNo = salesOrderEntry.TagAlongOrderNo;
            if (salesOrderEntry.TypeOfSalesOrderId == 1651)
            {
                salesOrderEntryModel.OrderNo = salesOrderEntry.BlanketOrderNo?.OrderNo;
            }
            else
            {
                salesOrderEntryModel.OrderNo = salesOrderEntry.OrderNo;
            }
            salesOrderEntryModel.OrderPeriodFrom = salesOrderEntry.OrderPeriodFrom;
            salesOrderEntryModel.OrderPeriodTo = salesOrderEntry.OrderPeriodTo;
            salesOrderEntryModel.QuotationNo = salesOrderEntry.QuotationNo;
            salesOrderEntryModel.IsAllowExtension = salesOrderEntry.IsAllowExtension;
            salesOrderEntryModel.IsExtensionInfoMultiple = salesOrderEntry.IsExtensionInfoMultiple;
            salesOrderEntryModel.IsMultipleQuatationNo = salesOrderEntry.IsMultipleQuatationNo;
            salesOrderEntryModel.IsEachDistribution = salesOrderEntry.IsEachDistribution;
            salesOrderEntryModel.ExtensionMonth = salesOrderEntry.ExtensionMonth;
            salesOrderEntryModel.ExtensionQuantity = salesOrderEntry.ExtensionQuantity;
            salesOrderEntryModel.SessionID = salesOrderEntry.SessionId;
            salesOrderEntryModel.OrderEntryType = salesOrderEntry.OrderEntryType;
            salesOrderEntryModel.ProfileReferenceNo = salesOrderEntry.ProfileReferenceNo;
            salesOrderEntryModel.StatusCodeID = salesOrderEntry.StatusCodeId;
            salesOrderEntryModel.AddedByUserID = salesOrderEntry.AddedByUserId;
            salesOrderEntryModel.ModifiedByUserID = salesOrderEntry.ModifiedByUserId;
            salesOrderEntryModel.AddedDate = salesOrderEntry.AddedDate;
            salesOrderEntryModel.ModifiedDate = salesOrderEntry.ModifiedDate;
            salesOrderEntryModel.AddedByUser = salesOrderEntry.AddedByUser != null ? salesOrderEntry.AddedByUser.UserName : "";
            salesOrderEntryModel.IsTagAlongWithTenderAgency = salesOrderEntry.IsTagAlongWithTenderAgency;
            salesOrderEntryModel.IsWithOrderPeriod = salesOrderEntry.IsWithOrderPeriod;
            salesOrderEntryModel.TagAlongTenderNoID = salesOrderEntry.TagAlongTenderNoId;
            salesOrderEntryModel.TagAlongTenderNo = salesOrderEntry.TagAlongTenderNo?.OrderNo;
            salesOrderEntryModel.BlanketOrderNoId = salesOrderEntry.BlanketOrderNoId;
            salesOrderEntryModel.ProductItemId = salesOrderEntry.ProductItemId;
            salesOrderEntryModel.DateOfOrder = salesOrderEntry.DateOfOrder;
            salesOrderEntryModel.Ponumber = salesOrderEntry.Ponumber;
            //salesOrderEntryModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
            //salesOrderEntryModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
            if (salesOrderEntry.OrderById == 1402)
            {
                salesOrderEntryModel.CustomerName = salesOrderEntry.Customer?.CompanyName;
            }
            if (salesOrderEntry.OrderById == 1403)
            {
                if (salesOrderEntry.TenderAgencyId > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == salesOrderEntry.TenderAgencyId).AsNoTracking().ToList();
                    salesOrderEntryModel.CustomerName = masterDetailList != null && salesOrderEntry.TenderAgencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == salesOrderEntry.TenderAgencyId).Select(a => a.Value).SingleOrDefault() : "";
                }
            }
            if (salesOrderEntry.TagAlongCustomerId != null && salesOrderEntry.TagAlongCustomerId > 0)
            {
                salesOrderEntryModel.TagAlongCustomerName = salesOrderEntry.TagAlongCustomer?.CompanyName;
            }
            if (salesOrderEntry.TagAlongTenderId != null && salesOrderEntry.TagAlongTenderId > 0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == salesOrderEntry.TagAlongTenderId).AsNoTracking().ToList();
                salesOrderEntryModel.TagAlongCustomerName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == salesOrderEntry.TagAlongTenderId).Select(a => a.Value).SingleOrDefault() : "";
            }
            salesOrderEntryModel.NovateProductId = salesOrderEntry.PurchaseItemSalesEntryLine?.Where(s => s.SalesOrderEntryId == salesOrderEntry.SalesOrderEntryId).Select(s => s.SobyCustomersId).FirstOrDefault();
            salesOrderEntryModel.OrderByName = salesOrderEntry.OrderBy?.CodeValue;
            salesOrderEntryModel.ProfileName = salesOrderEntry.Profile?.Name;
            salesOrderEntryModel.TypeOfOrderName = salesOrderEntry.TypeOfOrder?.CodeValue;
            salesOrderEntryModel.CompanyName = salesOrderEntry.Company?.PlantCode;
            salesOrderEntryModel.TagAlongCustomerID = salesOrderEntry.TagAlongCustomerId;
            salesOrderEntryModel.TagAlongTenderID = salesOrderEntry.TagAlongTenderId;

            return salesOrderEntryModel;
        }

        [HttpGet]
        [Route("GetProductsByContractNo")]
        public List<SocustomersItemCrossReferenceModel> GetProductsByContractNo(int id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();


            List<SocustomersItemCrossReferenceModel> socustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            var salesEntryContrctNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == id).Select(s => s.OrderNo).FirstOrDefault();
            var salesOrderEntryIds = _context.SalesOrderEntry.Where(s => s.OrderNo.ToLower().Trim() == salesEntryContrctNo.ToLower().Trim()).AsNoTracking().Select(s => s.SalesOrderEntryId).ToList();
            var productIds = _context.PurchaseItemSalesEntryLine.Where(s => salesOrderEntryIds.Contains(s.SalesOrderEntryId.Value)).AsNoTracking().Select(s => s.SobyCustomersId).ToList();
            var socustomerItemCrossReference = _context.SocustomersItemCrossReference.Where(s => productIds.Contains(s.SocustomersItemCrossReferenceId)).AsNoTracking().ToList();

            if (socustomerItemCrossReference != null && socustomerItemCrossReference.Count > 0)
            {
                var masterDetailIds = socustomerItemCrossReference.Where(s => s.PerUomId != null).Select(s => s.PerUomId).ToList();
                if (masterDetailIds != null && masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
                socustomerItemCrossReference.ForEach(s =>
                {
                    SocustomersItemCrossReferenceModel socustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                    socustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                    socustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                    socustomersItemCrossReferenceModel.ProductNo = s.CustomerReferenceNo + " | " + s.Description;
                    socustomersItemCrossReferenceModel.PerUomName = masterDetailList != null && s.PerUomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PerUomId).Select(a => a.Value).SingleOrDefault() : "";
                    socustomersItemCrossReferenceModels.Add(socustomersItemCrossReferenceModel);
                });
            }
            return socustomersItemCrossReferenceModels;
        }

        [HttpGet]
        [Route("GetContractNosById")]
        public List<SalesOrderEntryModel> GetContractNosById(int id)
        {


            List<SalesOrderEntryModel> salesOrderEntryModels = new List<SalesOrderEntryModel>();
            var salesEntryContrctNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == id).Select(s => s.OrderNo).AsNoTracking().FirstOrDefault();
            var salesOrderEntrys = _context.SalesOrderEntry.Where(s => s.OrderNo.ToLower().Trim() == salesEntryContrctNo.ToLower().Trim()).AsNoTracking().ToList();


            if (salesOrderEntrys != null && salesOrderEntrys.Count > 0)
            {
                salesOrderEntrys.ForEach(s =>
                {
                    SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();
                    salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    salesOrderEntryModel.OrderNo = s.OrderNo;

                    salesOrderEntryModels.Add(salesOrderEntryModel);
                });
            }
            return salesOrderEntryModels;
        }
        [HttpPost]
        [Route("GetSalesOrderEntryBySearch")]
        public List<SalesOrderEntryModel> GetSalesOrderEntryBySearch(SalesOrderSearchModel salesOrderSearchModel)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
            var salesList = new List<SalesOrderEntry>();
            var purchaseItemLines = _context.PurchaseItemSalesEntryLine.AsNoTracking().ToList();
            var salesEntryIds = new List<long?>();
            var salesEntryLineIds = new List<long?>();
            if (salesOrderSearchModel.DistributionCustomerID != null)
            {
                var distributionIds = _context.ContractDistributionSalesEntryLine.Where(s => s.SocustomerId == salesOrderSearchModel.DistributionCustomerID).AsNoTracking().Select(s => s.PurchaseItemSalesEntryLineId).ToList();
                purchaseItemLines = purchaseItemLines.Where(p => distributionIds.Contains(p.PurchaseItemSalesEntryLineId)).ToList();
                salesEntryIds = purchaseItemLines.Select(s => s.SalesOrderEntryId).ToList();
            }


            if (salesOrderSearchModel.ItemCustomerID != null)
            {
                salesEntryIds = purchaseItemLines.Where(p => p.SobyCustomersId == salesOrderSearchModel.ItemCustomerID).Select(s => s.SalesOrderEntryId).ToList();
            }
            if (salesOrderSearchModel.TenderProductNoID != null)
            {
                salesEntryIds = purchaseItemLines.Where(p => p.ItemId == salesOrderSearchModel.TenderProductNoID).Select(s => s.SalesOrderEntryId).ToList();
            }
            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.Profile)
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.OrderBy)
                .Include(s => s.BlanketOrderNo)
                .Include(l => l.Company).AsNoTracking()
                .ToList();
            if (SalesOrderEntrys != null && SalesOrderEntrys.Count > 0)
            {
                if (salesOrderSearchModel.OrderByID != null)
                {
                    salesList = SalesOrderEntrys.Where(s => s.OrderById == salesOrderSearchModel.OrderByID).ToList();
                }
                if (salesOrderSearchModel.TypeOfOrderID != null)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => s.TypeOfOrderId == salesOrderSearchModel.TypeOfOrderID).ToList();
                    }
                    else
                    {
                        salesList = SalesOrderEntrys.Where(s => s.TypeOfOrderId == salesOrderSearchModel.TypeOfOrderID).ToList();
                    }

                }
                if (salesOrderSearchModel.CustomerID != null)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => s.CustomerId == salesOrderSearchModel.CustomerID).ToList();
                    }
                    else
                    {
                        salesList = SalesOrderEntrys.Where(s => s.CustomerId == salesOrderSearchModel.CustomerID).ToList();
                    }
                }
                if (salesOrderSearchModel.TenderAgencyID != null)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => s.TenderAgencyId == salesOrderSearchModel.TenderAgencyID).ToList();
                    }
                    else
                    {
                        salesList = SalesOrderEntrys.Where(s => s.TenderAgencyId == salesOrderSearchModel.TenderAgencyID).ToList();

                    }
                }
                if (salesOrderSearchModel.CompanyID != null)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => s.CompanyId == salesOrderSearchModel.CompanyID).ToList();
                    }
                    else
                    {
                        salesList = SalesOrderEntrys.Where(s => s.CompanyId == salesOrderSearchModel.CompanyID).ToList();

                    }
                }
                if (salesEntryIds != null && salesEntryIds.Count > 0)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => salesEntryIds.Contains(s.SalesOrderEntryId)).ToList();
                    }
                    else
                    {
                        salesList = SalesOrderEntrys.Where(s => salesEntryIds.Contains(s.SalesOrderEntryId)).ToList();

                    }
                }
            }
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            if (salesList != null && salesList.Count > 0)
            {
                var tagAlongIds = salesList.Where(s => s.TagAlongTenderId != null).Select(s => s.TagAlongTenderId).ToList();
                var tenderAgencyIds = salesList.Where(s => s.TenderAgencyId != null).Select(s => s.TenderAgencyId).ToList();
                if (tagAlongIds.Count > 0)
                {
                    masterDetailIds.AddRange(tagAlongIds);
                }
                if (tenderAgencyIds.Count > 0)
                {
                    masterDetailIds.AddRange(tenderAgencyIds);
                }
                if (masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                salesList.ForEach(s =>
                {
                    SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                    salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    salesOrderEntryModel.CompanyID = s.CompanyId;
                    salesOrderEntryModel.ProfileID = s.ProfileId;
                    salesOrderEntryModel.OrderByID = s.OrderById;
                    salesOrderEntryModel.CustomerID = s.CustomerId;
                    salesOrderEntryModel.TenderAgencyID = s.TenderAgencyId;
                    salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                    salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                    if (s.TypeOfSalesOrderId == 1651)
                    {
                        salesOrderEntryModel.OrderNo = s.BlanketOrderNo?.OrderNo;
                    }
                    else
                    {
                        salesOrderEntryModel.OrderNo = s.OrderNo;
                    }
                    salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    salesOrderEntryModel.BlanketItemId = s.BlanketItemId;
                    salesOrderEntryModel.QuotationNo = s.QuotationNo;
                    salesOrderEntryModel.OrderPeriodFrom = s.OrderPeriodFrom;
                    salesOrderEntryModel.OrderPeriodTo = s.OrderPeriodTo;
                    salesOrderEntryModel.IsAllowExtension = s.IsAllowExtension;
                    salesOrderEntryModel.IsExtensionInfoMultiple = s.IsExtensionInfoMultiple;
                    salesOrderEntryModel.IsMultipleQuatationNo = s.IsMultipleQuatationNo;
                    salesOrderEntryModel.IsEachDistribution = s.IsEachDistribution;
                    salesOrderEntryModel.ExtensionMonth = s.ExtensionMonth;
                    salesOrderEntryModel.ExtensionQuantity = s.ExtensionQuantity;
                    salesOrderEntryModel.SessionID = s.SessionId;
                    salesOrderEntryModel.OrderEntryType = s.OrderEntryType;
                    salesOrderEntryModel.ProfileReferenceNo = s.ProfileReferenceNo;
                    salesOrderEntryModel.StatusCodeID = s.StatusCodeId;
                    salesOrderEntryModel.AddedByUserID = s.AddedByUserId;
                    salesOrderEntryModel.ModifiedByUserID = s.ModifiedByUserId;
                    salesOrderEntryModel.AddedDate = s.AddedDate;
                    salesOrderEntryModel.ModifiedDate = s.ModifiedDate;
                    salesOrderEntryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    salesOrderEntryModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    salesOrderEntryModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    salesOrderEntryModel.IsTagAlongWithTenderAgency = s.IsTagAlongWithTenderAgency;
                    salesOrderEntryModel.IsWithOrderPeriod = s.IsWithOrderPeriod;
                    salesOrderEntryModel.TagAlongTenderNoID = s.TagAlongTenderNoId;
                    salesOrderEntryModel.TagAlongTenderNo = s.TagAlongTenderNo?.OrderNo;
                    salesOrderEntryModel.DateOfOrder = s.DateOfOrder;
                    salesOrderEntryModel.Ponumber = s.Ponumber;
                    if (s.OrderById == 1402)
                    {
                        salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;
                    }
                    if (s.OrderById == 1403)
                    {
                        salesOrderEntryModel.CustomerName = masterDetailList != null && s.TenderAgencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.TenderAgencyId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    if (s.TagAlongCustomerId != null && s.TagAlongCustomerId > 0)
                    {
                        salesOrderEntryModel.TagAlongCustomerName = s.TagAlongCustomer?.CompanyName;
                    }
                    if (s.TagAlongTenderId != null && s.TagAlongTenderId > 0)
                    {
                        salesOrderEntryModel.TagAlongCustomerName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.TagAlongTenderId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    salesOrderEntryModel.OrderByName = s.OrderBy?.CodeValue;
                    salesOrderEntryModel.ProfileName = s.Profile?.Name;
                    salesOrderEntryModel.TypeOfOrderName = s.TypeOfOrder?.CodeValue;
                    salesOrderEntryModel.CompanyName = s.Company?.PlantCode;
                    salesOrderEntryModel.TagAlongCustomerID = s.TagAlongCustomerId;
                    salesOrderEntryModel.TagAlongTenderID = s.TagAlongTenderId;

                    SalesOrderEntryModels.Add(salesOrderEntryModel);
                });
            }
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }

        [HttpPost]
        [Route("GetSalesOrderEntryUpdateSearch")]
        public List<SalesOrderEntryModel> GetSalesOrderEntryUpdateSearch(SalesOrderSearchModel salesOrderSearchModel)
        {

            var salesList = new List<SalesOrderEntry>();
            var purchaseItemLines = _context.PurchaseItemSalesEntryLine.AsNoTracking().ToList();
            var salesEntryIds = new List<long?>();
            var salesEntryLineIds = new List<long?>();
            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.Profile)
                .Include(s => s.TypeOfOrder)
                .Include(c => c.Customer)
                .Include(c => c.OrderBy)
                .Include(l => l.Company).AsNoTracking()
                .ToList();
            salesList = SalesOrderEntrys;
            if (salesOrderSearchModel.CompanyID != null)
            {

                var selsalesEntryIds = SalesOrderEntrys.Where(s => s.CompanyId == salesOrderSearchModel.CompanyID).Select(s => s.SalesOrderEntryId).ToList();
                if (salesList != null && salesList.Count > 0)
                {
                    salesList = salesList.Where(s => selsalesEntryIds.Contains(s.SalesOrderEntryId)).ToList();
                }

            }
            if (salesOrderSearchModel.DistributionCustomerID != null)
            {
                var distributionIds = _context.ContractDistributionSalesEntryLine.Where(s => s.SocustomerId == salesOrderSearchModel.DistributionCustomerID).Select(s => s.PurchaseItemSalesEntryLineId).ToList();
                purchaseItemLines = purchaseItemLines.Where(p => distributionIds.Contains(p.PurchaseItemSalesEntryLineId)).ToList();
                var selsalesEntryIds = purchaseItemLines.Select(s => s.SalesOrderEntryId).ToList();
                if (selsalesEntryIds != null && selsalesEntryIds.Count > 0)
                {
                    salesList = salesList.Where(s => selsalesEntryIds.Contains(s.SalesOrderEntryId)).ToList();

                }
            }
            if (salesOrderSearchModel.PonumberId != null)
            {
                var distributionIds = _context.ContractDistributionSalesEntryLine.Where(s => s.ContractDistributionSalesEntryLineId == salesOrderSearchModel.PonumberId).Select(s => s.PurchaseItemSalesEntryLineId).ToList();
                purchaseItemLines = purchaseItemLines.Where(p => distributionIds.Contains(p.PurchaseItemSalesEntryLineId)).ToList();
                var selsalesEntryIds = purchaseItemLines.Select(s => s.SalesOrderEntryId).ToList();
                if (selsalesEntryIds != null && selsalesEntryIds.Count > 0)
                {

                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => selsalesEntryIds.Contains(s.SalesOrderEntryId)).ToList();
                    }


                }
            }
            if (salesOrderSearchModel.SalesOrderEntryId != null)
            {
                if (salesList != null && salesList.Count > 0)
                {
                    salesList = salesList.Where(s => s.SalesOrderEntryId == salesOrderSearchModel.SalesOrderEntryId).ToList();
                }

            }


            if (salesOrderSearchModel.ItemCustomerID != null)
            {
                var selsalesEntryIds = purchaseItemLines.Where(p => p.SobyCustomersId == salesOrderSearchModel.ItemCustomerID).Select(s => s.SalesOrderEntryId).ToList();
                if (selsalesEntryIds != null && selsalesEntryIds.Count > 0)
                {
                    if (salesList != null && salesList.Count > 0)
                    {
                        salesList = salesList.Where(s => selsalesEntryIds.Contains(s.SalesOrderEntryId)).ToList();
                    }

                }
            }



            if (salesOrderSearchModel.CustomerID != null)
            {

                var selsalesEntryIds = SalesOrderEntrys.Where(s => s.CustomerId == salesOrderSearchModel.CustomerID).Select(s => s.SalesOrderEntryId).ToList();
                if (salesList != null && salesList.Count > 0)
                {
                    salesList = salesList.Where(s => selsalesEntryIds.Contains(s.SalesOrderEntryId)).ToList();
                }

            }

            if (salesOrderSearchModel.CompanyID == null && salesOrderSearchModel.CustomerID == null && salesOrderSearchModel.DistributionCustomerID == null && salesOrderSearchModel.ItemCustomerID == null && salesOrderSearchModel.PonumberId == null && salesOrderSearchModel.SalesOrderEntryId == null)
            {
                salesList = SalesOrderEntrys;

            }


            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            if (salesList != null && salesList.Count > 0)
            {
                salesList.ForEach(s =>
                {
                    SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                    salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    salesOrderEntryModel.CompanyID = s.CompanyId;
                    salesOrderEntryModel.ProfileID = s.ProfileId;
                    salesOrderEntryModel.OrderByID = s.OrderById;
                    salesOrderEntryModel.CustomerID = s.CustomerId;
                    salesOrderEntryModel.TenderAgencyID = s.TenderAgencyId;
                    salesOrderEntryModel.TypeOfOrderID = s.TypeOfOrderId;
                    salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    salesOrderEntryModel.TagAlongOrderNo = s.TagAlongOrderNo;
                    salesOrderEntryModel.OrderNo = s.OrderNo;
                    salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    salesOrderEntryModel.BlanketItemId = s.BlanketItemId;
                    salesOrderEntryModel.QuotationNo = s.QuotationNo;
                    salesOrderEntryModel.OrderPeriodFrom = s.OrderPeriodFrom;
                    salesOrderEntryModel.OrderPeriodTo = s.OrderPeriodTo;
                    salesOrderEntryModel.IsAllowExtension = s.IsAllowExtension;
                    salesOrderEntryModel.IsExtensionInfoMultiple = s.IsExtensionInfoMultiple;
                    salesOrderEntryModel.IsMultipleQuatationNo = s.IsMultipleQuatationNo;
                    salesOrderEntryModel.IsEachDistribution = s.IsEachDistribution;
                    salesOrderEntryModel.ExtensionMonth = s.ExtensionMonth;
                    salesOrderEntryModel.ExtensionQuantity = s.ExtensionQuantity;
                    salesOrderEntryModel.SessionID = s.SessionId;
                    salesOrderEntryModel.OrderEntryType = s.OrderEntryType;
                    salesOrderEntryModel.ProfileReferenceNo = s.ProfileReferenceNo;
                    salesOrderEntryModel.StatusCodeID = s.StatusCodeId;
                    salesOrderEntryModel.AddedByUserID = s.AddedByUserId;
                    salesOrderEntryModel.ModifiedByUserID = s.ModifiedByUserId;
                    salesOrderEntryModel.AddedDate = s.AddedDate;
                    salesOrderEntryModel.ModifiedDate = s.ModifiedDate;
                    salesOrderEntryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    salesOrderEntryModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    salesOrderEntryModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    salesOrderEntryModel.IsTagAlongWithTenderAgency = s.IsTagAlongWithTenderAgency;
                    salesOrderEntryModel.IsWithOrderPeriod = s.IsWithOrderPeriod;
                    salesOrderEntryModel.TagAlongTenderNoID = s.TagAlongTenderNoId;
                    salesOrderEntryModel.TagAlongTenderNo = s.TagAlongTenderNo?.OrderNo;
                    salesOrderEntryModel.DateOfOrder = s.DateOfOrder;
                    salesOrderEntryModel.Ponumber = s.Ponumber;
                    salesOrderEntryModel.CustomerName = s.Customer?.CompanyName;
                    salesOrderEntryModel.ProfileName = s.Profile?.Name;
                    salesOrderEntryModel.CompanyName = s.Company?.PlantCode;
                    salesOrderEntryModel.TagAlongCustomerID = s.TagAlongCustomerId;
                    salesOrderEntryModel.TagAlongTenderID = s.TagAlongTenderId;

                    SalesOrderEntryModels.Add(salesOrderEntryModel);
                });
            }
            return SalesOrderEntryModels.Where(s => s.TypeOfSalesOrderId == salesOrderSearchModel.TypeOfSalesOrdeId).OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }
        [HttpGet]
        [Route("GetTagAlongOrderCustomer")]
        public List<TagAlongCustomerModel> GetTagAlongOrderCustomer()
        {
            var applicationmaster = _context.ApplicationMaster.Where(s => s.ApplicationMasterCodeId == 239).FirstOrDefault();
            var applicationMasterDetail = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterId == applicationmaster.ApplicationMasterId).AsNoTracking().ToList();
            var comapnyListing = _context.CompanyListing.AsNoTracking().ToList();
            List<TagAlongCustomerModel> tagAlongCustomerModels = new List<TagAlongCustomerModel>();
            TagAlongCustomerModel tagAlongCustomerModel = new TagAlongCustomerModel();
            if (applicationMasterDetail != null && applicationMasterDetail.Count > 0)
            {
                applicationMasterDetail.ForEach(a =>
                {
                    tagAlongCustomerModel = new TagAlongCustomerModel();
                    tagAlongCustomerModel.TenderAgencyID = a.ApplicationMasterDetailId;
                    tagAlongCustomerModel.IsTenderAgency = true;
                    tagAlongCustomerModel.CustomerName = a.Value;
                    tagAlongCustomerModels.Add(tagAlongCustomerModel);

                });
            }
            if (comapnyListing != null && comapnyListing.Count > 0)
            {
                comapnyListing.ForEach(c =>
                {
                    tagAlongCustomerModel = new TagAlongCustomerModel();
                    tagAlongCustomerModel.CustomerID = c.CompanyListingId;
                    tagAlongCustomerModel.IsCustomer = true;
                    tagAlongCustomerModel.CustomerName = c.CompanyName;
                    tagAlongCustomerModels.Add(tagAlongCustomerModel);
                });

            }


            return tagAlongCustomerModels.ToList();
        }
        [HttpPost()]
        [Route("GetSalesOrderEntryData")]
        public ActionResult<SalesOrderEntryModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var SalesOrderEntry = new SalesOrderEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Previous":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderBy(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).FirstOrDefault(s => s.SalesOrderEntryId > searchModel.Id);
                        break;
                    case "Previous":
                        SalesOrderEntry = _context.SalesOrderEntry.Include(t => t.TagAlongCustomer).Include(b => b.BlanketItem).OrderByDescending(o => o.SalesOrderEntryId).Where(o => o.TypeOfSalesOrderId == searchModel.MasterTypeID || o.OrderById == searchModel.MasterTypeID).FirstOrDefault(s => s.SalesOrderEntryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SalesOrderEntryModel>(SalesOrderEntry);
            if (result != null)
            {
                if (result.TagAlongCustomerID != null && result.TagAlongCustomerID > 0)
                {
                    result.TagAlongCustomerName = SalesOrderEntry.TagAlongCustomer?.CompanyName;
                }
                result.ItemDescription = SalesOrderEntry?.BlanketItem?.Description + "|" + SalesOrderEntry?.BlanketItem?.Description2;
                result.ItemUom = SalesOrderEntry?.BlanketItem?.BaseUnitofMeasure + "|" + SalesOrderEntry?.BlanketItem?.BaseUnitofMeasure;
            }

            return result;
        }

        [HttpPost]
        [Route("InsertSalesOrderEntry")]
        public SalesOrderEntryModel Post(SalesOrderEntryModel value)
        {

            var codeMasterlist = _context.CodeMaster.AsNoTracking().ToList();
            //if (value.IsAllowExtension == false)
            //{
            //    value.ExtensionMonth = null;
            //    value.ExtensionQuantity = null;
            //}
            var profileNo = "";
            if (value.ProfileID > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.OrderNo });
            }
            var SessionId = Guid.NewGuid();
            var SalesOrderEntry = new SalesOrderEntry
            {
                CompanyId = value.CompanyID,
                ProfileId = value.ProfileID,
                OrderById = value.OrderByID,
                CustomerId = value.CustomerID,
                TenderAgencyId = value.TenderAgencyID,
                TypeOfOrderId = value.TypeOfOrderID,
                TypeOfSalesOrderId = value.TypeOfSalesOrderId,
                TagAlongOrderNo = value.TagAlongOrderNo,
                BlanketItemId = value.BlanketItemId,
                OrderNo = value.OrderNo,
                QuotationNo = value.QuotationNo,
                OrderPeriodFrom = value.OrderPeriodFrom,
                OrderPeriodTo = value.OrderPeriodTo,
                IsAllowExtension = value.IsAllowExtension,
                IsExtensionInfoMultiple = value.IsExtensionInfoMultiple,
                IsMultipleQuatationNo = value.IsMultipleQuatationNo,
                IsEachDistribution = value.IsEachDistribution,
                ExtensionMonth = value.ExtensionMonth,
                ExtensionQuantity = value.ExtensionQuantity,
                SessionId = SessionId,
                OrderEntryType = value.OrderEntryType,
                ProfileReferenceNo = profileNo,
                StatusCodeId = 1420,
                AddedByUserId = value.AddedByUserID,
                TagAlongTenderId = value.TagAlongTenderID,
                TagAlongCustomerId = value.TagAlongCustomerID,
                AddedDate = DateTime.Now,
                IsWithOrderPeriod = value.IsWithOrderPeriod,
                IsTagAlongWithTenderAgency = value.IsTagAlongWithTenderAgency,
                TagAlongTenderNoId = value.TagAlongTenderNoID,
                BlanketOrderNoId = value.BlanketOrderNoId,
                ProductItemId = value.ProductItemId,
                DateOfOrder = value.DateOfOrder,
                Ponumber = value.Ponumber,

            };

            _context.SalesOrderEntry.Add(SalesOrderEntry);
            _context.SaveChanges();
            value.SessionID = SessionId;
            value.SalesOrderEntryID = SalesOrderEntry.SalesOrderEntryId;
            value.ProfileReferenceNo = SalesOrderEntry.ProfileReferenceNo;
            value.AddedDate = SalesOrderEntry.AddedDate;
            value.ProfileName = _context.DocumentProfileNoSeries.Where(s => s.ProfileId == value.ProfileID).Select(s => s.Name).FirstOrDefault();
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == SalesOrderEntry.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            value.OrderByName = codeMasterlist != null ? codeMasterlist.Where(c => c.CodeId == value.OrderByID).Select(c => c.CodeValue).FirstOrDefault() : "";
            value.TypeOfOrderName = codeMasterlist != null ? codeMasterlist.Where(c => c.CodeId == value.TypeOfOrderID).Select(c => c.CodeValue).FirstOrDefault() : "";
            value.StatusCode = codeMasterlist != null ? codeMasterlist.Where(s => s.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault() : "";
            if (value.OrderByID == 1402)
            {
                value.CustomerName = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerID).Select(c => c.CompanyName).FirstOrDefault();
            }
            if (value.TypeOfSalesOrderId == 1403 || value.OrderByID == 1403)
            {
                value.CustomerName = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerID).Select(c => c.CompanyName).FirstOrDefault();

            }
            if (value.CompanyID != null && value.CompanyID > 0)
            {
                value.CompanyName = _context.Plant.Where(p => p.PlantId == value.CompanyID).Select(p => p.PlantCode).FirstOrDefault();
            }

            return value;
        }
        [HttpPut]
        [Route("UpdateSalesOrderEntry")]
        public SalesOrderEntryModel Put(SalesOrderEntryModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionID == null)
            {
                value.SessionID = SessionId;
            }

            var codeMasterlist = _context.CodeMaster.AsNoTracking().ToList();
            var salesOrderEntry = _context.SalesOrderEntry.SingleOrDefault(p => p.SalesOrderEntryId == value.SalesOrderEntryID);
            if (value.OrderByID == 1403 && value.TypeOfOrderID == 1406)
            {
                value.TagAlongOrderNo = "";
                value.TagAlongCustomerID = null;
                if (value.OrderEntryType == "false")
                {
                    value.IsExtensionInfoMultiple = false;
                }
            }
            //if (value.IsAllowExtension == false)
            //{
            //    value.ExtensionMonth = null;
            //    value.ExtensionQuantity = null;
            //}
            if (value.OrderByID == 1402)
            {
                if (value.TypeOfOrderID == 1404)
                {
                    value.TagAlongOrderNo = "";
                    value.TagAlongCustomerID = null;
                    value.OrderPeriodFrom = null;
                    value.OrderPeriodTo = null;

                    value.IsAllowExtension = false;
                    value.IsEachDistribution = false;
                    value.IsMultipleQuatationNo = false;
                }

                else if (value.TypeOfOrderID == 1409)
                {
                    value.OrderPeriodFrom = null;
                    value.OrderPeriodTo = null;

                }
                else if (value.TypeOfOrderID == 1405)
                {
                    value.TagAlongOrderNo = "";
                    value.TagAlongCustomerID = null;
                    value.IsExtensionInfoMultiple = false;
                    value.OrderPeriodFrom = null;
                    value.OrderPeriodTo = null;

                    value.IsAllowExtension = false;
                    value.IsEachDistribution = false;

                    value.IsMultipleQuatationNo = false;
                }
                else if (value.TypeOfOrderID == 1406)
                {
                    value.TagAlongOrderNo = "";
                    value.TagAlongCustomerID = null;

                }
                else if (value.TypeOfOrderID == 1408)
                {
                    value.TagAlongOrderNo = "";
                    value.TagAlongCustomerID = null;
                    value.OrderPeriodFrom = null;
                    value.OrderPeriodTo = null;
                    value.IsEachDistribution = false;
                    if (value.OrderEntryType == "false")
                    {
                        value.IsExtensionInfoMultiple = false;
                    }
                }

            }
            salesOrderEntry.CompanyId = value.CompanyID;
            salesOrderEntry.ProfileId = value.ProfileID;
            salesOrderEntry.OrderById = value.OrderByID;
            salesOrderEntry.CustomerId = value.CustomerID;
            salesOrderEntry.TenderAgencyId = value.TenderAgencyID;
            salesOrderEntry.TypeOfOrderId = value.TypeOfOrderID;
            salesOrderEntry.BlanketItemId = value.BlanketItemId;
            salesOrderEntry.TypeOfSalesOrderId = value.TypeOfSalesOrderId;
            salesOrderEntry.TagAlongOrderNo = value.TagAlongOrderNo;
            salesOrderEntry.OrderNo = value.OrderNo;
            salesOrderEntry.QuotationNo = value.QuotationNo;
            salesOrderEntry.OrderPeriodFrom = value.OrderPeriodFrom;
            salesOrderEntry.OrderPeriodTo = value.OrderPeriodTo;
            salesOrderEntry.IsAllowExtension = value.IsAllowExtension;
            salesOrderEntry.IsExtensionInfoMultiple = value.IsExtensionInfoMultiple;
            salesOrderEntry.IsMultipleQuatationNo = value.IsMultipleQuatationNo;
            salesOrderEntry.IsEachDistribution = value.IsEachDistribution;
            salesOrderEntry.ExtensionMonth = value.ExtensionMonth;
            salesOrderEntry.ExtensionQuantity = value.ExtensionQuantity;
            salesOrderEntry.SessionId = value.SessionID;
            salesOrderEntry.OrderEntryType = value.OrderEntryType;
            salesOrderEntry.TagAlongCustomerId = value.TagAlongCustomerID;
            salesOrderEntry.TagAlongTenderId = value.TagAlongTenderID;
            salesOrderEntry.ProfileReferenceNo = value.ProfileReferenceNo;
            salesOrderEntry.StatusCodeId = value.StatusCodeID;
            salesOrderEntry.ModifiedByUserId = value.ModifiedByUserID;
            salesOrderEntry.ModifiedDate = DateTime.Now;
            salesOrderEntry.TagAlongTenderNoId = value.TagAlongTenderNoID;
            salesOrderEntry.IsTagAlongWithTenderAgency = value.IsTagAlongWithTenderAgency;
            salesOrderEntry.IsWithOrderPeriod = value.IsWithOrderPeriod;
            salesOrderEntry.BlanketOrderNoId = value.BlanketOrderNoId;
            salesOrderEntry.ProductItemId = value.ProductItemId;
            salesOrderEntry.DateOfOrder = value.DateOfOrder;
            salesOrderEntry.Ponumber = value.Ponumber;
            _context.SaveChanges();
            var purchaseItemInfo = _context.PurchaseItemSalesEntryLine.Where(p => p.SalesOrderEntryId == salesOrderEntry.SalesOrderEntryId).ToList();
            var distributionList = _context.ContractDistributionSalesEntryLine.ToList();
            var projectedDeliveryList = _context.ProjectedDeliverySalesOrderLine.ToList();
            if ((salesOrderEntry.OrderById == 1402) && (salesOrderEntry.TypeOfOrderId == 1404 || salesOrderEntry.TypeOfOrderId == 1405 || salesOrderEntry.TypeOfOrderId == 1406 || salesOrderEntry.TypeOfOrderId == 1408))
            {
                if (purchaseItemInfo != null && purchaseItemInfo.Count > 0)
                {
                    purchaseItemInfo.ForEach(p =>
                    {
                        var distribution = distributionList.Where(c => c.PurchaseItemSalesEntryLineId == p.PurchaseItemSalesEntryLineId).ToList();
                        if (distribution != null && distribution.Count > 0)
                        {
                            distribution.ForEach(d =>
                            {
                                var projectedDelivery = projectedDeliveryList.Where(p => p.ContractDistributionSalesEntryLineId == d.ContractDistributionSalesEntryLineId).ToList();
                                if (projectedDelivery != null && projectedDelivery.Count > 0)
                                {
                                    _context.ProjectedDeliverySalesOrderLine.RemoveRange(projectedDelivery);
                                    _context.SaveChanges();
                                }
                            });
                            _context.ContractDistributionSalesEntryLine.RemoveRange(distribution);
                            _context.SaveChanges();
                        }
                    });
                }
            }
            value.AddedDate = salesOrderEntry.AddedDate;
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == salesOrderEntry.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            value.ProfileName = _context.DocumentProfileNoSeries.Where(s => s.ProfileId == value.ProfileID).Select(s => s.Name).FirstOrDefault();
            value.OrderByName = codeMasterlist != null ? codeMasterlist.Where(c => c.CodeId == value.OrderByID).Select(c => c.CodeValue).FirstOrDefault() : "";
            value.TypeOfOrderName = codeMasterlist != null ? codeMasterlist.Where(c => c.CodeId == value.TypeOfOrderID).Select(c => c.CodeValue).FirstOrDefault() : "";
            value.StatusCode = codeMasterlist != null ? codeMasterlist.Where(s => s.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault() : "";
            if (value.OrderByID == 1402)
            {
                value.CustomerName = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerID).Select(c => c.CompanyName).FirstOrDefault();
            }
            if (value.TypeOfSalesOrderId == 1403 || value.OrderByID == 1403)
            {
                value.CustomerName = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerID).Select(c => c.CompanyName).FirstOrDefault();

            }

            //if (value.OrderByID == 1403)
            //{
            //    value.CustomerName = masterDetailList != null && value.TenderAgencyID > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.TenderAgencyID).Select(a => a.Value).SingleOrDefault() : "";
            //}
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesOrderEntry")]
        public void Delete(int id)
        {
            try
            {
                var SalesOrderEntry = _context.SalesOrderEntry.Where(p => p.SalesOrderEntryId == id).FirstOrDefault();
                if (SalesOrderEntry != null)
                {

                    _context.SalesOrderEntry.Remove(SalesOrderEntry);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Record Cannot be delete Reference to other records", ex);
            }
        }





        [HttpGet]
        [Route("GetSalesOrderEntryDocument")]
        public List<SalesOrderAttachmentModel> Get(Guid? SessionId)
        {
            List<SalesOrderAttachmentModel> SalesOrderEntryLineDocumentModels = new List<SalesOrderAttachmentModel>();
            var SalesOrderEntrylineDocuments = _context.SalesOrderAttachment.Select(s => new { s.SessionId, s.SalesOrderAttachmentId, s.FileName, s.ContentType, s.FileSize, s.UploadDate }).Where(f => f.SessionId == SessionId).AsNoTracking().ToList();
            if (SalesOrderEntrylineDocuments != null && SalesOrderEntrylineDocuments.Count > 0)
            {
                SalesOrderEntrylineDocuments.ForEach(s =>
                {
                    SalesOrderAttachmentModel SalesOrderEntryLineDocumentModel = new SalesOrderAttachmentModel
                    {
                        SalesOrderAttachmentId = s.SalesOrderAttachmentId,
                        FileName = s.FileName,
                        ContentType = s.ContentType,
                        FileSize = s.FileSize,
                        UploadDate = s.UploadDate,
                        SessionId = s.SessionId,

                    };
                    SalesOrderEntryLineDocumentModels.Add(SalesOrderEntryLineDocumentModel);
                });

            }


            return SalesOrderEntryLineDocumentModels;
        }

        [HttpDelete]
        [Route("DeleteDocuments")]
        public void DeleteDocuments(int id)
        {
            try
            {
                var query = string.Format("delete from SalesOrderAttachment Where SalesOrderAttachmentId =" + "{0}", id);

                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPost]
        [Route("UploadSalesOrderEntryDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {

            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var SalesOrderEntry = new SalesOrderAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    //Description = files.desc
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,

                };
                _context.SalesOrderAttachment.Add(SalesOrderEntry);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("DownLoadSalesOrderEntryLineDocument")]
        public IActionResult DownLoadSalesOrderEntryLineDocument(long id)
        {
            var document = _context.SalesOrderAttachment.SingleOrDefault(t => t.SalesOrderAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }


        [HttpGet]
        [Route("GetSalesOrderEntryLineDocumentById")]
        public SalesOrderAttachmentModel GetSalesOrderEntryLineDocumentById(long id)
        {
            SalesOrderAttachmentModel SalesOrderEntryLineDocumentModel = new SalesOrderAttachmentModel();
            var document = _context.SalesOrderAttachment.Select(s=>new { s.SalesOrderAttachmentId,s.SessionId,s.FileData,s.ContentType,s.FileName}).FirstOrDefault(t => t.SalesOrderAttachmentId == id);
            if (document != null)
            {
                SalesOrderEntryLineDocumentModel.SalesOrderAttachmentId = document.SalesOrderAttachmentId;
                SalesOrderEntryLineDocumentModel.SessionId = document.SessionId;
                SalesOrderEntryLineDocumentModel.ContentType = document.ContentType;
                SalesOrderEntryLineDocumentModel.FileName = document.FileName;
                SalesOrderEntryLineDocumentModel.FileData = document.FileData;
            }
            return SalesOrderEntryLineDocumentModel;
        }

        [HttpGet]
        [Route("GetSalesOrderReport")]
        public List<ExpandoObject> GetSalesOrderReport()
        {
            var tenderReport = new List<ExpandoObject>();
            List<SalesOrderReportModel> salesOrderReportModels = new List<SalesOrderReportModel>();
            SalesOrderReportModel salesOrderReportModel = new SalesOrderReportModel();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var salesEntries = _context.SalesOrderEntry.Where(f => f.OrderById == 1403).AsNoTracking().ToList();
            var salesEntryIds = salesEntries.Select(s => s.SalesOrderEntryId).ToList();
            var purchaseSalesLines = _context.PurchaseItemSalesEntryLine.Where(p => salesEntryIds.Contains(p.SalesOrderEntryId.Value)).AsNoTracking().ToList();
            var purchaseLineIds = purchaseSalesLines.Select(s => s.PurchaseItemSalesEntryLineId).ToList();

            var report = _context.ContractDistributionSalesEntryLine.
                Include(s => s.PurchaseItemSalesEntryLine).
                Include(s => s.PurchaseItemSalesEntryLine.SalesOrderEntry).
                Include(s => s.PurchaseItemSalesEntryLine.Item).Include(s => s.Socustomer).
                Include(s => s.ProjectedDeliverySalesOrderLine).
                Where(f => purchaseLineIds.Contains(f.PurchaseItemSalesEntryLineId.Value)).AsNoTracking().ToList();
            var contractDistIds = report.Select(s => s.ContractDistributionSalesEntryLineId).ToList();
            var projectedDeliverylist = _context.ProjectedDeliverySalesOrderLine.Where(f => contractDistIds.Contains(f.ContractDistributionSalesEntryLineId.Value)).AsNoTracking().ToList();
            if (projectedDeliverylist != null && projectedDeliverylist.Count > 0)
            {
                var masterdetailIds = projectedDeliverylist.Select(s => s.FrequencyId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterdetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
            }
            if (report != null && report.Count > 0)
            {
                report.ForEach(r =>
                {
                    salesOrderReportModel = new SalesOrderReportModel
                    {
                        ContractDistributionSalesEntryLineID = r.ContractDistributionSalesEntryLineId,

                        SalesOrderEntryID = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.SalesOrderEntryId,
                        PurchaseItemSalesEntryLineID = r.PurchaseItemSalesEntryLine?.PurchaseItemSalesEntryLineId,
                        TenderNo = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderNo,
                        OrderPeriodFrom = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderPeriodFrom,
                        OrderPeriodTo = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderPeriodTo,
                        PONumber = r.Ponumber,
                        NoOfLots = r.NoOfLots,
                        CustomerName = !string.IsNullOrEmpty(r.Socustomer?.No) ? r.Socustomer?.No + " | " + r.Socustomer?.CompanyName : r.Socustomer?.CompanyName,
                        TotalQty = r.TotalQty,
                        PerFrequencyQty = projectedDeliverylist?.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.PerFrequencyQty,
                        StartDate = projectedDeliverylist?.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.StartDate,
                        Frequency = masterDetailList != null && projectedDeliverylist != null ? projectedDeliverylist.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.FrequencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == projectedDeliverylist.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault().FrequencyId).Select(a => a.Value).SingleOrDefault() : "" : ""
                    };
                    if (salesOrderReportModel.OrderPeriodTo != null && salesOrderReportModel.OrderPeriodFrom != null)
                    {
                        salesOrderReportModel.TotalMonth = salesOrderReportModel.OrderPeriodTo.Value.Month - salesOrderReportModel.OrderPeriodFrom.Value.Month;
                    }
                    tenderReport.Add(GetTenderReport(salesOrderReportModel));
                    salesOrderReportModels.Add(salesOrderReportModel);
                });
            }

            return tenderReport;
        }

        [HttpPost]
        [Route("GetSalesOrderReportBySearch")]
        public List<SalesOrderReportModel> GetSalesOrderReportBySearch(SalesOrderReportSearchModel salesOrderReportSearchModel)
        {
            List<SalesOrderReportModel> salesOrderReportModels = new List<SalesOrderReportModel>();
            SalesOrderReportModel salesOrderReportModel = new SalesOrderReportModel();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var salesEntries = _context.SalesOrderEntry.Where(f => f.OrderById == 1403).AsNoTracking().ToList();
            var salesEntryIds = salesEntries.Select(s => s.SalesOrderEntryId).ToList();
            var purchaseSalesLines = _context.PurchaseItemSalesEntryLine.Where(p => salesEntryIds.Contains(p.SalesOrderEntryId.Value)).AsNoTracking().ToList();
            var purchaseLineIds = purchaseSalesLines.Select(s => s.PurchaseItemSalesEntryLineId).ToList();

            var report = _context.ContractDistributionSalesEntryLine.
                Include(s => s.PurchaseItemSalesEntryLine).
                Include(s => s.PurchaseItemSalesEntryLine.SalesOrderEntry).
                Include(s => s.PurchaseItemSalesEntryLine.Item).Include(s => s.Socustomer).
                Include(s => s.ProjectedDeliverySalesOrderLine).
                Where(f => purchaseLineIds.Contains(f.PurchaseItemSalesEntryLineId.Value)).AsNoTracking().ToList();
            var contractDistIds = report.Select(s => s.ContractDistributionSalesEntryLineId).ToList();
            var projectedDeliverylist = _context.ProjectedDeliverySalesOrderLine.Where(f => contractDistIds.Contains(f.ContractDistributionSalesEntryLineId.Value)).AsNoTracking().ToList();
            if (projectedDeliverylist != null && projectedDeliverylist.Count > 0)
            {
                var masterdetailIds = projectedDeliverylist.Select(s => s.FrequencyId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterdetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
            }
            if (report != null && report.Count > 0)
            {
                report.ForEach(r =>
                {
                    salesOrderReportModel = new SalesOrderReportModel
                    {
                        ContractDistributionSalesEntryLineID = r.ContractDistributionSalesEntryLineId,

                        SalesOrderEntryID = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.SalesOrderEntryId,
                        PurchaseItemSalesEntryLineID = r.PurchaseItemSalesEntryLine?.PurchaseItemSalesEntryLineId,
                        TenderNo = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderNo,
                        OrderPeriodFrom = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderPeriodFrom,
                        OrderPeriodTo = r.PurchaseItemSalesEntryLine?.SalesOrderEntry?.OrderPeriodTo,
                        PONumber = r.Ponumber,
                        NoOfLots = r.NoOfLots,
                        CustomerName = !string.IsNullOrEmpty(r.Socustomer?.No) ? r.Socustomer?.No + " | " + r.Socustomer?.CompanyName : r.Socustomer?.CompanyName,
                        TotalQty = r.TotalQty,
                        PerFrequencyQty = projectedDeliverylist?.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.PerFrequencyQty,
                        StartDate = projectedDeliverylist?.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.StartDate,
                        Frequency = masterDetailList != null && projectedDeliverylist != null ? projectedDeliverylist.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault()?.FrequencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == projectedDeliverylist.Where(s => s.ContractDistributionSalesEntryLineId == r.ContractDistributionSalesEntryLineId).FirstOrDefault().FrequencyId).Select(a => a.Value).SingleOrDefault() : "" : ""
                    };
                    if (salesOrderReportModel.OrderPeriodTo != null && salesOrderReportModel.OrderPeriodFrom != null)
                    {
                        salesOrderReportModel.TotalMonth = salesOrderReportModel.OrderPeriodTo.Value.Month - salesOrderReportModel.OrderPeriodFrom.Value.Month;
                    }

                    salesOrderReportModels.Add(salesOrderReportModel);
                });
            }
            if (salesOrderReportSearchModel.customerID != null)
            {

            }
            if (salesOrderReportSearchModel.ItemIds != null)
            {

            }
            if (salesOrderReportSearchModel.statusCodeID != null)
            {

            }
            return salesOrderReportModels;
        }
        [HttpGet]
        [Route("GetTenderReport")]
        public ExpandoObject GetTenderReport(SalesOrderReportModel model)
        {
            var months = GetMonthsBetweenDates(model.OrderPeriodFrom.Value, model.OrderPeriodTo.Value);
            var strYear = new List<string>();
            months.ForEach(f =>
            {
                if (!strYear.Contains(f.Split("-")[1]))
                    strYear.Add(f.Split("-")[1]);

            });
            var obje = new ExpandoObject();
            AddProperty(obje, "CustomerName", model.CustomerName);
            AddProperty(obje, "PONumber", model.PONumber);
            AddProperty(obje, "TenderNo", model.TenderNo);
            AddProperty(obje, "TotalQty", model.TotalQty);
            AddProperty(obje, "NoOfLots", model.NoOfLots);
            AddProperty(obje, "StartDate", model.StartDate);
            AddProperty(obje, "SalesOrderEntryID", model.SalesOrderEntryID);

            var distDetails = new List<TenderDistributionModel>();
            var packetQty = model.TotalQty.Value;

            if (!model.StartDate.HasValue || model.StartDate == DateTime.MinValue)
                model.StartDate = DateTime.Today;

            months.ForEach(f =>
            {
                var freQty = 0;
                DateTime date = new DateTime(int.Parse(f.Split("-")[2]), int.Parse(f.Split("-")[1]), 1);
                if (model.StartDate.Value <= date)
                {
                    packetQty -= model.PerFrequencyQty.Value;
                    if (packetQty > 0)
                    {
                        freQty = model.PerFrequencyQty.Value;
                    }
                }
                distDetails.Add(new TenderDistributionModel
                {
                    Month = f.Split("-")[0],
                    Year = int.Parse(f.Split("-")[2]),
                    IntMonth = int.Parse(f.Split("-")[1]),
                    Quantity = freQty,
                    StartDate = model.StartDate.Value,
                    TotalQty = model.TotalQty.Value
                });
                var monthQty = f.Split("-")[0] + "-" + f.Split("-")[2];
                AddProperty(obje, monthQty, freQty);

            });
            var groupedQty = distDetails.GroupBy(g => g.Year).ToList();
            var gridColumns = new List<GridColumns>();
            var yearsCol = new List<string>();
            groupedQty.ForEach(g =>
            {
                var year = g.Key;
                yearsCol.Add(year.ToString());
                var griColumn = new GridColumns();
                griColumn.headerText = year.ToString();
                AddProperty(obje, year.ToString(), g.ToList());
                griColumn.columns = new List<GridColumns>();
                g.ToList().ForEach(m =>
                {
                    var monthQty = m.Month + "-" + g.Key;
                    griColumn.columns.Add(new GridColumns
                    {
                        headerText = m.Month,
                        field = monthQty
                    });
                });
                gridColumns.Add(griColumn);
            });
            AddProperty(obje, "yearsCol", yearsCol);
            AddProperty(obje, "gridColumns", gridColumns);
            AddProperty(obje, "PacketQty", packetQty);

            model.PacketQty = packetQty;
            model.TenderDistribution = distDetails;



            return obje;
        }
        public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }
        private List<string> GetMonthsBetweenDates(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            var totalMonth = Math.Abs(monthsApart) + 1;
            return Enumerable.Range(0, totalMonth).Select(a => startDate.AddMonths(a))
            .TakeWhile(a => a <= endDate)
            .Select(a => String.Concat(a.ToString("MMMM") + "-" + a.Month + "-" + a.Year)).ToList();

        }
        [HttpGet]
        [Route("GetContractNos")]
        public List<SalesEntryContractModel> GetContractNos()
        {
            List<SalesEntryContractModel> salesEntryContractModels = new List<SalesEntryContractModel>();
            var salesOrderEntries = _context.SalesOrderEntry.Include(p => p.PurchaseItemSalesEntryLine).AsNoTracking().ToList();
            var productIds = salesOrderEntries.SelectMany(p => p.PurchaseItemSalesEntryLine).Select(p => p.ItemId);
            var products = _context.Navitems.Where(i => productIds.Contains(i.ItemId));
            salesOrderEntries.ForEach(s =>
            {
                SalesEntryContractModel salesEntryContractModel = new SalesEntryContractModel();
                salesEntryContractModel.ContractNo = s.OrderNo;
                salesEntryContractModel.SalesOrderID = s.SalesOrderEntryId;
                if (s.PurchaseItemSalesEntryLine != null)
                {
                    s.PurchaseItemSalesEntryLine.ToList().ForEach(p =>
                    {
                        if (p.ItemId != null)
                        {
                            var productItem = products.FirstOrDefault(i => i.ItemId == p.ItemId);
                            salesEntryContractModel.SalesEntryProducts.Add(new SalesEntryProduct
                            {
                                SalesOrderID = p.SalesOrderEntryId.Value,
                                PurchaseItemSalesEntryLineID = p.PurchaseItemSalesEntryLineId,
                                ItemID = p.ItemId ?? p.ItemId.Value,
                                ProductName = productItem.No + " | " + productItem.Description,
                                BUOM = productItem.BaseUnitofMeasure,
                                PUOM = productItem.PurchaseUom
                            });
                        }
                    });
                }

                salesEntryContractModels.Add(salesEntryContractModel);

            });
            return salesEntryContractModels;

        }
        [HttpPost]
        [Route("GetProductsContractNo")]
        public List<SocustomersItemCrossReferenceModel> GetProductsContractNo(SearchModel SearchModel)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            List<SocustomersItemCrossReferenceModel> socustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            var salesOrderEntry = _context.SalesOrderEntry.Where(s => s.OrderNo == SearchModel.SearchString).FirstOrDefault();
            if (salesOrderEntry != null)
            {
                var productIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == salesOrderEntry.SalesOrderEntryId).Select(s => s.SobyCustomersId).ToList();
                if (productIds != null && productIds.Count > 0)
                {
                    var socustomerCrossReference = _context.SocustomersItemCrossReference.Where(s => productIds.Contains(s.SocustomersItemCrossReferenceId)).AsNoTracking().ToList();
                    if (socustomerCrossReference != null && socustomerCrossReference.Count > 0)
                    {
                        var masterDetailIds = socustomerCrossReference.Select(s => s.PerUomId).ToList();
                        var customerPackingIds = socustomerCrossReference.Select(s => s.CustomerPackingUomId).ToList();
                        masterDetailList = _context.ApplicationMasterDetail.Where(s => masterDetailIds.Contains(s.ApplicationMasterDetailId) || customerPackingIds.Contains(s.ApplicationMasterDetailId)).AsNoTracking().ToList();
                        socustomerCrossReference.ForEach(c =>
                        {
                            SocustomersItemCrossReferenceModel socustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                            socustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = c.SocustomersItemCrossReferenceId;
                            socustomersItemCrossReferenceModel.CustomerReferenceNo = c.CustomerReferenceNo;
                            socustomersItemCrossReferenceModel.Description = c.Description;
                            socustomersItemCrossReferenceModel.PerUomName = c.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == c.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                            socustomersItemCrossReferenceModel.CustomerPackingUomName = c.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == c.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                            socustomersItemCrossReferenceModels.Add(socustomersItemCrossReferenceModel);
                        });
                    }
                }
            }
            return socustomersItemCrossReferenceModels;
        }
        [HttpPost]
        [Route("GetContractNoSearch")]
        public SalesEntryContractModel GetContractNoSearch(SearchModel SearchModel)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<SalesEntryContractModel> salesEntryContractModels = new List<SalesEntryContractModel>();
            var salesOrderEntrie = _context.SalesOrderEntry.Include(o => o.OrderBy).Include(p => p.PurchaseItemSalesEntryLine).Where(s => s.SalesOrderEntryId > 0);
            salesOrderEntrie = salesOrderEntrie.Where(s => s.OrderNo == SearchModel.SearchString);
            if (SearchModel.UserID > 0)
            {
                salesOrderEntrie = salesOrderEntrie.Where(w => w.CompanyId == SearchModel.UserID);
            }
            var salesOrderEntries = salesOrderEntrie.ToList();
            var productIds = salesOrderEntries.SelectMany(p => p.PurchaseItemSalesEntryLine).Select(p => p.ItemId);
            var customerProductIds = salesOrderEntries.SelectMany(p => p.PurchaseItemSalesEntryLine).Select(p => p.SobyCustomersId);
            var products = _context.Navitems.Where(i => productIds.Contains(i.ItemId));
            var customerProducts = _context.SocustomersItemCrossReference.Where(i => customerProductIds.Contains(i.SobyCustomersId));
            SalesEntryContractModel salesEntryContractModel = null;
            salesOrderEntries.ForEach(s =>
            {
                salesEntryContractModel = new SalesEntryContractModel();
                salesEntryContractModel.ContractNo = s.OrderNo;
                salesEntryContractModel.SalesOrderID = s.SalesOrderEntryId;
                salesEntryContractModel.OrderByName = s.OrderBy != null ? s.OrderBy.CodeValue : null;
                salesEntryContractModel.EffectiveFrom = s.OrderPeriodFrom;
                salesEntryContractModel.EffectiveTo = s.OrderPeriodTo;

                if (s.OrderById == 1402)
                {
                    salesEntryContractModel.CustomerName = s.Customer?.CompanyName;
                }
                if (s.OrderById == 1403)
                {
                    salesEntryContractModel.CustomerName = masterDetailList != null && s.TenderAgencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.TenderAgencyId).Select(a => a.Value).SingleOrDefault() : "";
                }
                if (s.PurchaseItemSalesEntryLine != null)
                {
                    s.PurchaseItemSalesEntryLine.ToList().ForEach(p =>
                    {
                        if (p.ItemId != null)
                        {
                            var productItem = products.FirstOrDefault(i => i.ItemId == p.ItemId);
                            SalesEntryProduct salesEntryProduct = new SalesEntryProduct();
                            salesEntryProduct.SalesOrderID = p.SalesOrderEntryId.Value;
                            salesEntryProduct.PurchaseItemSalesEntryLineID = p.PurchaseItemSalesEntryLineId;
                            salesEntryProduct.ItemID = p.ItemId ?? p.ItemId.Value;


                            if (productItem != null)
                            {
                                salesEntryProduct.ProductName = productItem?.No + " | " + productItem?.Description;
                                salesEntryProduct.BUOM = productItem?.BaseUnitofMeasure;
                                salesEntryProduct.PUOM = productItem?.PurchaseUom;
                                salesEntryProduct.Description = productItem?.Description;
                                salesEntryProduct.Description2 = productItem?.Description2;
                            }
                            salesEntryProduct.SellingPrice = p.SellingPrice;
                            salesEntryProduct.OrderCurrencyId = p.OrderCurrencyId;
                            salesEntryProduct.OrderCurrency = masterDetailList?.FirstOrDefault(m => m.ApplicationMasterDetailId == p.OrderCurrencyId) != null ? masterDetailList?.FirstOrDefault(m => m.ApplicationMasterDetailId == p.OrderCurrencyId).Value : "";
                            salesEntryContractModel.SalesEntryProducts.Add(salesEntryProduct);
                        }
                        if (p.SobyCustomersId != null)
                        {
                            var productItem = customerProducts.FirstOrDefault(i => i.SobyCustomersId == p.SobyCustomersId);
                            SalesEntryProduct salesEntryProduct = new SalesEntryProduct();
                            salesEntryProduct.SalesOrderID = p.SalesOrderEntryId.Value;
                            salesEntryProduct.PurchaseItemSalesEntryLineID = p.PurchaseItemSalesEntryLineId;
                            salesEntryProduct.ItemID = p.ItemId != null ? p.ItemId.Value : new long();
                            if (productItem != null)
                            {
                                salesEntryProduct.ProductName = productItem?.CustomerReferenceNo + " | " + productItem?.Description;
                            }

                            salesEntryProduct.OrderCurrency = masterDetailList?.FirstOrDefault(m => m.ApplicationMasterDetailId == p.OrderCurrencyId) != null ? masterDetailList?.FirstOrDefault(m => m.ApplicationMasterDetailId == p.OrderCurrencyId).Value : "";
                            salesEntryContractModel.SalesEntryProducts.Add(salesEntryProduct);




                        }

                    });
                }

            });
            return salesEntryContractModel;

        }

        [HttpGet]
        [Route("GetTenderAgencySalesOrders")]
        public List<SalesOrderEntryModel> GetTenderAgencySalesOrders()
        {

            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(p => p.PurchaseItemSalesEntryLine)
                .Include("PurchaseItemSalesEntryLine.Item")
                .AsNoTracking()
                .Where(o => o.TypeOfSalesOrderId == 1403)
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                if (s.PurchaseItemSalesEntryLine.Count > 0)
                {
                    foreach (var purchaseItemSalesEntryLine in s.PurchaseItemSalesEntryLine)
                    {
                        SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();
                        salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                        salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                        salesOrderEntryModel.OrderNo = s.OrderNo;
                        salesOrderEntryModel.ItemDescription = purchaseItemSalesEntryLine.Item?.Description + "|" + purchaseItemSalesEntryLine.Item?.Description2;
                        salesOrderEntryModel.ItemUom = purchaseItemSalesEntryLine.Item?.BaseUnitofMeasure;
                        salesOrderEntryModel.BlanketItemId = purchaseItemSalesEntryLine.Item?.ItemId;
                        SalesOrderEntryModels.Add(salesOrderEntryModel);
                    }
                }
                else
                {
                    SalesOrderEntryModel headersalesOrderEntryModel = new SalesOrderEntryModel();
                    headersalesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    headersalesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    headersalesOrderEntryModel.OrderNo = s.OrderNo;
                    SalesOrderEntryModels.Add(headersalesOrderEntryModel);
                }
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }

        [HttpPost]
        [Route("GetSalesOrdersByCustomer")]
        public List<SalesOrderEntryModel> GetSalesOrdersByCustomer(SearchModel searchModel)
        {

            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(p => p.PurchaseItemSalesEntryLine)
                .AsNoTracking()
                .Where(o => o.TypeOfSalesOrderId == 1403 && o.CustomerId == searchModel.Id)
                .ToList();
            List<SalesOrderEntryModel> salesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                if (s.PurchaseItemSalesEntryLine.Count > 0 && s.PurchaseItemSalesEntryLine.Any(i => i.PurchaseItemSalesEntryLineId == searchModel.UserID))
                {
                    SalesOrderEntryModel headersalesOrderEntryModel = new SalesOrderEntryModel();
                    headersalesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    headersalesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    headersalesOrderEntryModel.OrderNo = s.OrderNo;
                    salesOrderEntryModels.Add(headersalesOrderEntryModel);
                }
            });
            return salesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }

        [HttpGet]
        [Route("GetOrderNoDropDown")]
        public List<SalesOrderEntryModel> GetOrderNoDropDown()
        {

            var SalesOrderEntrys = _context.SalesOrderEntry
                .Include(p => p.PurchaseItemSalesEntryLine)
                 .Include("PurchaseItemSalesEntryLine.Item")
                  .Include("PurchaseItemSalesEntryLine.SobyCustomers")
                .AsNoTracking()
                .Where(o => (o.TypeOfSalesOrderId == 1403 || o.TypeOfSalesOrderId == 1402) || (o.OrderById == 1403 || o.OrderById == 1402))
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            SalesOrderEntrys.ForEach(s =>
            {
                if (s.PurchaseItemSalesEntryLine.Count > 0)
                {
                    foreach (var purchaseItemSalesEntryLine in s.PurchaseItemSalesEntryLine)
                    {
                        SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();
                        salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                        salesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                        salesOrderEntryModel.OrderNo = s.OrderNo;
                        if (purchaseItemSalesEntryLine.ItemId != null)
                        {
                            salesOrderEntryModel.BlanketItemId = purchaseItemSalesEntryLine.ItemId;
                            salesOrderEntryModel.ItemDescription = s.OrderNo + "|" + purchaseItemSalesEntryLine?.Item?.No + "|" + purchaseItemSalesEntryLine.Item?.Description + "|" + purchaseItemSalesEntryLine.Item.PackUom;
                            salesOrderEntryModel.ItemUom = purchaseItemSalesEntryLine.Item?.BaseUnitofMeasure;
                            salesOrderEntryModel.ItemNo = purchaseItemSalesEntryLine?.Item?.No;

                        }
                        if (purchaseItemSalesEntryLine.SobyCustomersId != null)
                        {
                            salesOrderEntryModel.CrossReferenceId = purchaseItemSalesEntryLine?.SobyCustomersId;
                            salesOrderEntryModel.ItemDescription = s.OrderNo;
                            salesOrderEntryModel.CrossReferenceNo = purchaseItemSalesEntryLine?.SobyCustomers?.CustomerReferenceNo;
                        }
                        SalesOrderEntryModels.Add(salesOrderEntryModel);
                    }
                }
                else
                {
                    SalesOrderEntryModel headersalesOrderEntryModel = new SalesOrderEntryModel();
                    headersalesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;
                    headersalesOrderEntryModel.TypeOfSalesOrderId = s.TypeOfSalesOrderId;
                    headersalesOrderEntryModel.OrderNo = s.OrderNo;
                    headersalesOrderEntryModel.ItemDescription = s.OrderNo;
                    SalesOrderEntryModels.Add(headersalesOrderEntryModel);
                }
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }


        [HttpPost]
        [Route("GetNavSalesOrderLines")]
        public async Task<List<NavSalesOrderLineModel>> GetNavSalesOrderLines(SearchModel searchModel)
        {
            List<NavSalesOrderLineModel> salesOrderLineModels = new List<NavSalesOrderLineModel>();
            var navSalesOrderLines = _context.NavSalesOrderLine.Include(d => d.NonDeliverSo).Where(n => n.CompanyId == searchModel.Id && n.ShipmentDate <= searchModel.Date).AsNoTracking().ToList();

            navSalesOrderLines.ForEach(n =>
            {
                var salesOrderLineModel = new NavSalesOrderLineModel
                {
                    NavSalesOrderLineId = n.NavSalesOrderLineId,
                    DocumentNo = n.DocumentNo,
                    VersionNo = n.VersionNo,
                    TotalOrderQuantity = n.TotalOrderQuantity,
                    DeliverQuantity = n.DeliverQuantity,
                    DocumentType = n.DocumentType,
                    Description = n.Description,
                    Description1 = n.Description1,
                    ItemNo = n.ItemNo,
                    OrderLineNo = n.OrderLineNo,
                    LastSyncDate = n.LastSyncDate,
                    LastSyncUserId = n.LastSyncUserId,
                    UnitofMeasureCode = n.UnitofMeasureCode,
                    OutstandingQuantity = n.OutstandingQuantity,
                    PromisedDeliveryDate = n.PromisedDeliveryDate,
                    SelltoCustomerNo = n.SelltoCustomerNo,
                    ShipmentDate = n.ShipmentDate,
                };
                if (n.NonDeliverSo.Count > 0)
                {
                    salesOrderLineModel.NonDeliverSOModels = new List<NonDeliverSOModel>();
                    n.NonDeliverSo.Where(l => l.IsLatest == true).ToList().ForEach(d =>
                    {
                        NonDeliverSOModel nonDeliverSOModel = new NonDeliverSOModel();
                        nonDeliverSOModel.NonDeliverSoid = d.NonDeliverSoid;
                        nonDeliverSOModel.NavSalesOrderLineId = d.NavSalesOrderLineId;
                        nonDeliverSOModel.IsChangeBalanceQty = d.IsChangeBalanceQty;
                        nonDeliverSOModel.NewBalanceQty = d.NewBalanceQty;
                        nonDeliverSOModel.ReasonId = d.ReasonId;
                        nonDeliverSOModel.ReasonDescription = d.ReasonDescription;
                        nonDeliverSOModel.NewShipmentDate = d.NewShipmentDate;
                        nonDeliverSOModel.UOM = n.UnitofMeasureCode;
                        nonDeliverSOModel.StatusCodeID = d.StatusCodeId;
                        nonDeliverSOModel.AddedByUserID = d.AddedByUserId;
                        nonDeliverSOModel.AddedDate = d.AddedDate;
                        nonDeliverSOModel.IsLatest = d.IsLatest;
                        salesOrderLineModel.NonDeliverSOModels.Add(nonDeliverSOModel);
                    });
                }
                salesOrderLineModels.Add(salesOrderLineModel);
            });
            return salesOrderLineModels;
        }

        [HttpPost]
        [Route("InsertNonDeliverSO")]
        public NonDeliverSOModel InsertNonDeliverSO(NonDeliverSOModel value)
        {
            NavSalesOrderLine navSalesOrderLine = _context.NavSalesOrderLine.Include(n => n.NonDeliverSo).FirstOrDefault(v => v.NavSalesOrderLineId == value.NavSalesOrderLineId);
            if (navSalesOrderLine.NonDeliverSo.Count > 0)
            {
                navSalesOrderLine.NonDeliverSo.ToList().ForEach(n =>
                {
                    n.IsLatest = false;
                });
                _context.SaveChanges();
            }
            NonDeliverSo nonDeliverSo = new NonDeliverSo
            {
                NavSalesOrderLineId = value.NavSalesOrderLineId,
                IsChangeBalanceQty = value.IsChangeBalanceQty,
                NewBalanceQty = value.NewBalanceQty,
                ReasonId = value.ReasonId,
                ReasonDescription = value.ReasonDescription,
                NewShipmentDate = value.NewShipmentDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsLatest = true,
            };
            _context.NonDeliverSo.Add(nonDeliverSo);
            _context.SaveChanges();
            value.NonDeliverSoid = nonDeliverSo.NonDeliverSoid;
            value.UOM = navSalesOrderLine.UnitofMeasureCode;
            return value;
        }



        //
        [HttpPost]
        [Route("UploadSalesOrderEntryAttachments")]
        public IActionResult UploadSalesOrderEntryAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new SalesOrderAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId

                };

                _context.SalesOrderAttachment.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetSalesOrderEntryAttachmentBySessionID")]
        public List<DocumentsModel> GetSalesOrderEntryAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.SalesOrderAttachment.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.SalesOrderAttachmentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                ScreenID = s.ScreenId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadSalesOrderEntryAttachment")]
        public IActionResult DownLoadSalesOrderEntryAttachment(long id)
        {
            var document = _context.SalesOrderAttachment.SingleOrDefault(t => t.SalesOrderAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteSalesOrderEntryAttachment")]
        public void DeleteSalesOrderEntryAttachment(int id)
        {
            var query = string.Format("delete from SalesOrderAttachment Where SalesOrderAttachmentId =" + "{0}", id);

            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }

    }
}