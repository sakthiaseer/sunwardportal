﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class StandardProcedureController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public StandardProcedureController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetStandardProcedures")]
        public List<StandardProcedureModel> Get(int? id)
        {
            List<StandardProcedureModel> standardProcedureModels = new List<StandardProcedureModel>();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
           
            var standardProcedure = _context.StandardProcedure.Include("AddedByUser").Include("ModifiedByUser").Where(l => l.StandardManufacturingProcessId == id).OrderByDescending(o => o.StandardProcedureId).AsNoTracking().ToList();
            if (standardProcedure != null && standardProcedure.Count > 0)
            {
                var masterDetailIds = standardProcedure.Where(s => s.ManufacturingSiteId != null).Select(s => s.ManufacturingSiteId).ToList();
                if(masterDetailIds.Count>0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
                standardProcedure.ForEach(s =>
                {
                    StandardProcedureModel standardProcedureModel = new StandardProcedureModel
                    {
                        StandardProcedureID = s.StandardProcedureId,
                        StandardManufacturingProcessId = s.StandardManufacturingProcessId,
                        NoID = s.NoId,
                        ICMasterOperationID = s.IcmasterOperationId,
                        ManufacturingSiteID = s.ManufacturingSiteId,
                        StatusCode = s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser.UserName,
                        ModifiedByUser = s.ModifiedByUser.UserName,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ManufacturingSite = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(a => a.Value).SingleOrDefault() : "",
                        OperationName = s.IcmasterOperation.MasterOperation,
                    };
                    standardProcedureModels.Add(standardProcedureModel);
                });
            }
            return standardProcedureModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<StandardProcedureModel> GetData(SearchModel searchModel)
        {
            var standardProcedure = new StandardProcedure();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).LastOrDefault();
                        break;
                    case "Previous":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        standardProcedure = _context.StandardProcedure.OrderBy(o => o.StandardProcedureId).FirstOrDefault(s => s.StandardProcedureId > searchModel.Id);
                        break;
                    case "Previous":
                        standardProcedure = _context.StandardProcedure.OrderByDescending(o => o.StandardProcedureId).FirstOrDefault(s => s.StandardProcedureId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<StandardProcedureModel>(standardProcedure);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertStandardProcedure")]
        public StandardProcedureModel Post(StandardProcedureModel value)
        {
           
            var standardProcedure = new StandardProcedure
            {


                NoId = value.NoID,
                StandardManufacturingProcessId = value.StandardManufacturingProcessId,
               IcmasterOperationId =  value.ICMasterOperationID ,
               ManufacturingSiteId = value.ManufacturingSiteID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.StandardProcedure.Add(standardProcedure);
            _context.SaveChanges();
            value.StandardProcedureID = standardProcedure.StandardProcedureId;
            if (value.ManufacturingSiteID > 0)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(s=>s.ApplicationMasterDetailId == value.ManufacturingSiteID).AsNoTracking().ToList();
                value.ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ManufacturingSiteID).Select(a => a.Value).SingleOrDefault() : "";
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateStandardProcedure")]
        public StandardProcedureModel Put(StandardProcedureModel value)
        {
           
            var standardProcedure = _context.StandardProcedure.SingleOrDefault(p => p.StandardProcedureId == value.StandardProcedureID);
            standardProcedure.IcmasterOperationId = value.ICMasterOperationID;
            standardProcedure.NoId = value.NoID;
            standardProcedure.ManufacturingSiteId = value.ManufacturingSiteID;
            standardProcedure.StandardManufacturingProcessId = value.StandardManufacturingProcessId;

            standardProcedure.ModifiedByUserId = value.ModifiedByUserID;
            standardProcedure.ModifiedDate = DateTime.Now;
            standardProcedure.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            if (value.ManufacturingSiteID > 0)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.ManufacturingSiteID).AsNoTracking().ToList();
                value.ManufacturingSite = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ManufacturingSiteID).Select(a => a.Value).SingleOrDefault() : "";
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteStandardProcedure")]
        public void Delete(int id)
        {
            var standardProcedure = _context.StandardProcedure.SingleOrDefault(p => p.StandardProcedureId == id);
            if (standardProcedure != null)
            {
                _context.StandardProcedure.Remove(standardProcedure);
                _context.SaveChanges();
            }
        }
    }
}