﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class EmployeeAdditionalInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public EmployeeAdditionalInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetEmployeeAdditionalInformations")]
        public List<EmployeeAdditionalInformationModel> Get()
        {
            var employeeadditionalinformation = _context.EmployeeAdditionalInformation.Include(a => a.Employee).Include(m => m.Document).AsNoTracking().ToList();
            List<EmployeeAdditionalInformationModel> employeeAdditionalInformationModel = new List<EmployeeAdditionalInformationModel>();
            employeeadditionalinformation.ForEach(s =>
            {
                EmployeeAdditionalInformationModel employeeAdditionalInformationModels = new EmployeeAdditionalInformationModel
                {
                    EmployeeAdditionalInfotmationId = s.EmployeeAdditionalInfotmationId,
                    EmployeeId = s.EmployeeId,
                    ReportingDate = s.ReportingDate,
                    SageNoSeries = s.SageNoSeries,
                    OfficeExtensionNo = s.OfficeExtensionNo,
                    SpeedDialNo = s.SpeedDialNo,
                    Photo = s.Photo,
                    CompanyEmail = s.Employee != null ? s.Employee.Email : "",
                    CompanySkype = s.Employee != null ? s.Employee.SkypeAddress : "",
                    PortalLogin = s.PortalLogin,
                    SageLogin = s.SageLogin,
                    EmployeeName = s.Employee.FirstName,
                    DocumentId = s.DocumentId,
                    DocumentName = s.Document != null ? s.Document.FileName : "",
                };
                employeeAdditionalInformationModel.Add(employeeAdditionalInformationModels);
            });
            return employeeAdditionalInformationModel.OrderByDescending(a => a.EmployeeAdditionalInfotmationId).ToList();
        }
        // GET: api/Project/2
        [HttpGet("GetEmployeeAdditionalInformation")]
        public ActionResult<EmployeeAdditionalInformationModel> Get(int? id)
        {
            EmployeeAdditionalInformationModel employeeAdditionalInformationModels = new EmployeeAdditionalInformationModel();
            if (id == null)
            {
                return NotFound();
            }
            var employeeadditionalinformation = _context.EmployeeAdditionalInformation.Include(a => a.Employee).Include(m => m.Document).OrderByDescending(a => a.EmployeeAdditionalInfotmationId).Where(o => o.EmployeeId == id.Value).FirstOrDefault();
            List<EmployeeAdditionalInformationModel> employeeAdditionalInformationModel = new List<EmployeeAdditionalInformationModel>();
            if (employeeadditionalinformation != null)
            {
                employeeAdditionalInformationModels = new EmployeeAdditionalInformationModel();
                employeeAdditionalInformationModels.EmployeeAdditionalInfotmationId = employeeadditionalinformation.EmployeeAdditionalInfotmationId;
                employeeAdditionalInformationModels.EmployeeId = employeeadditionalinformation.EmployeeId;
                employeeAdditionalInformationModels.ReportingDate = employeeadditionalinformation.ReportingDate;
                employeeAdditionalInformationModels.SageNoSeries = employeeadditionalinformation.SageNoSeries;
                employeeAdditionalInformationModels.OfficeExtensionNo = employeeadditionalinformation.Employee.Extension;
                employeeAdditionalInformationModels.SpeedDialNo = employeeadditionalinformation.Employee.SpeedDial;
                employeeAdditionalInformationModels.Photo = employeeadditionalinformation.Photo;
                employeeAdditionalInformationModels.CompanyEmail = employeeadditionalinformation.CompanyEmail;
                employeeAdditionalInformationModels.CompanySkype = employeeadditionalinformation.CompanySkype;
                employeeAdditionalInformationModels.PortalLogin = employeeadditionalinformation.PortalLogin;
                employeeAdditionalInformationModels.SageLogin = employeeadditionalinformation.SageLogin;
                employeeAdditionalInformationModels.EmployeeName = employeeadditionalinformation.Employee.FirstName;
                employeeAdditionalInformationModels.DocumentId = employeeadditionalinformation.DocumentId;
                employeeAdditionalInformationModels.DocumentName = employeeadditionalinformation.Document.FileName;
            }

            return employeeAdditionalInformationModels;

        }
        // POST: api/User
        [HttpPost]
        [Route("InsertEmployeeAdditionalInformation")]
        public EmployeeAdditionalInformationModel Post(EmployeeAdditionalInformationModel value)
        {
            var employeeadditionalinformations = _context.EmployeeAdditionalInformation.SingleOrDefault(p => p.EmployeeId == value.EmployeeId);
            if (employeeadditionalinformations == null)
            {
                var employeeadditionalinformation = new EmployeeAdditionalInformation
                {
                    EmployeeId = value.EmployeeId,
                    ReportingDate = value.ReportingDate,
                    SageNoSeries = value.SageNoSeries,
                    CompanyEmail = value.CompanyEmail,
                    CompanySkype = value.CompanySkype,
                };
                _context.EmployeeAdditionalInformation.Add(employeeadditionalinformation);
                _context.SaveChanges();
                value.EmployeeAdditionalInfotmationId = employeeadditionalinformation.EmployeeAdditionalInfotmationId;
            }
            else
            {
                var employeeadditionalinformation = _context.EmployeeAdditionalInformation.SingleOrDefault(p => p.EmployeeId == value.EmployeeId);
                employeeadditionalinformation.ReportingDate = value.ReportingDate;
                employeeadditionalinformation.SageNoSeries = value.SageNoSeries;
                _context.SaveChanges();
                value.EmployeeAdditionalInfotmationId = employeeadditionalinformation.EmployeeAdditionalInfotmationId;
            }
            var employee = _context.Employee.Where(e => e.EmployeeId == value.EmployeeId).FirstOrDefault();
            {
                employee.AcceptanceStatus = 682;
                employee.AcceptanceStatusDate = DateTime.Now;
                employee.SkypeAddress = value.CompanySkype;
                employee.Extension = value.OfficeExtensionNo;
                employee.SpeedDial = value.SpeedDialNo;
                employee.Email = value.CompanyEmail;
            }
            _context.SaveChanges();
            return value;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPublicInformation")]
        public EmployeeAdditionalInformationModel InsertPublicInformation(EmployeeAdditionalInformationModel value)
        {
            var documentId = _context.Documents.Select(s => new { s.DocumentId, s.SessionId }).Where(d => d.SessionId == value.SessionId).SingleOrDefault();
            if (documentId != null)
            {
                var employeeadditionalinformations = _context.EmployeeAdditionalInformation.SingleOrDefault(p => p.EmployeeId == value.EmployeeId);
                if (employeeadditionalinformations == null)
                {
                    var employeeadditionalinformation = new EmployeeAdditionalInformation
                    {
                        EmployeeId = value.EmployeeId,
                        DocumentId = documentId.DocumentId,

                    };
                    _context.EmployeeAdditionalInformation.Add(employeeadditionalinformation);
                    _context.SaveChanges();
                    value.EmployeeAdditionalInfotmationId = employeeadditionalinformation.EmployeeAdditionalInfotmationId;
                }
                else
                {
                    var employeeadditionalinformation = _context.EmployeeAdditionalInformation.SingleOrDefault(p => p.EmployeeId == value.EmployeeId);
                    employeeadditionalinformation.DocumentId = documentId.DocumentId;
                    _context.SaveChanges();
                    value.EmployeeAdditionalInfotmationId = employeeadditionalinformation.EmployeeAdditionalInfotmationId;
                }
            }
            var employee = _context.Employee.Where(e => e.EmployeeId == value.EmployeeId).FirstOrDefault();
            {
                employee.AcceptanceStatus = 682;
                employee.AcceptanceStatusDate = DateTime.Now;
                employee.SkypeAddress = value.CompanySkype;
                employee.Extension = value.OfficeExtensionNo;
                employee.SpeedDial = value.SpeedDialNo;
                employee.Email = value.CompanyEmail;
            }
            _context.SaveChanges();
            return value;
        }
    }
}