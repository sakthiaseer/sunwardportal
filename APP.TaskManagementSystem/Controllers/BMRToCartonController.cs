﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BMRToCartonController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public BMRToCartonController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        #region BMRToCarton

        // GET: api/Project
        [HttpGet]
        [Route("GetBMRToCartons")]
        public List<BMRToCartonModel> Get()
        {
            List<BMRToCartonModel> BMRToCartonModels = new List<BMRToCartonModel>();
            var BMRToCartons = _context.BmrtoCarton.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.BmrtoCartonId).AsNoTracking().ToList();

            BMRToCartons.ForEach(s =>
            {
                var BMRToCartonModel = new BMRToCartonModel
                {
                    BmrtoCartonId = s.BmrtoCartonId,
                    Displaybox = s.Displaybox,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                BMRToCartonModels.Add(BMRToCartonModel);
            });
            return BMRToCartonModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BMRToCartonModel> GetData(SearchModel searchModel)
        {
            var BmrtoCarton = new BmrtoCarton();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).FirstOrDefault();
                        break;
                    case "Last":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).LastOrDefault();
                        break;
                    case "Next":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).LastOrDefault();
                        break;
                    case "Previous":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).FirstOrDefault();
                        break;
                    case "Last":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).LastOrDefault();
                        break;
                    case "Next":
                        BmrtoCarton = _context.BmrtoCarton.OrderBy(o => o.BmrtoCartonId).FirstOrDefault(s => s.BmrtoCartonId > searchModel.Id);
                        break;
                    case "Previous":
                        BmrtoCarton = _context.BmrtoCarton.OrderByDescending(o => o.BmrtoCartonId).FirstOrDefault(s => s.BmrtoCartonId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BMRToCartonModel>(BmrtoCarton);


            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBMRToCarton")]
        public async Task<BMRToCartonModel> Post(BMRToCartonModel value)
        {
            try
            {
                var bmrCarton = _context.BmrtoCarton.FirstOrDefault(s => s.Displaybox == value.Displaybox);
                if (bmrCarton == null)
                {
                    var BmrtoCarton = new BmrtoCarton
                    {
                        Displaybox = value.Displaybox,
                        AddedByUserId = value.LoginUserId.Value,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 1,
                    };
                    _context.BmrtoCarton.Add(BmrtoCarton);
                    _context.SaveChanges();
                    
                }
                else
                {
                    value.BmrtoCartonId = bmrCarton.BmrtoCartonId;
                }
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBMRToCarton")]
        public BMRToCartonModel Put(BMRToCartonModel value)
        {
            var BmrtoCarton = _context.BmrtoCarton.SingleOrDefault(p => p.BmrtoCartonId == value.BmrtoCartonId);

            BmrtoCarton.Displaybox = value.Displaybox;
            BmrtoCarton.ModifiedByUserId = value.ModifiedByUserID.Value;
            BmrtoCarton.ModifiedDate = DateTime.Now;
            BmrtoCarton.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBMRToCarton")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var BmrtoCarton = _context.BmrtoCarton.Include(i => i.BmrtoCartonLine).SingleOrDefault(p => p.BmrtoCartonId == id);
                if (BmrtoCarton != null)
                {
                    _context.BmrtoCarton.Remove(BmrtoCarton);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region BMRToCarton Line
        // GET: api/Project
        [HttpGet]
        [Route("GetBMRToCartonLines")]
        public List<BMRToCartonLineModel> GetBMRToCartonLinesById(int id)
        {
            List<BMRToCartonLineModel> BMRToCartonLineModels = new List<BMRToCartonLineModel>();
            var BMRToCartonLines = _context.BmrtoCartonLine.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(p => p.BmrtoCartonId == id).OrderByDescending(o => o.BmrtoCartonLineId).AsNoTracking().ToList();

            BMRToCartonLines.ForEach(s =>
            {
                var BMRToCartonLineModel = new BMRToCartonLineModel
                {
                    BmrtoCartonLineId = s.BmrtoCartonLineId,
                    BmrtoCartonId = s.BmrtoCartonId,
                    Bmrno = s.Bmrno,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                BMRToCartonLineModels.Add(BMRToCartonLineModel);
            });
            return BMRToCartonLineModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertBMRToCartonLine")]
        public async Task<BMRToCartonLineModel> Post(BMRToCartonLineModel value)
        {
            try
            {
                var BMRToCartonLine = new BmrtoCartonLine
                {
                    BmrtoCartonId = value.BmrtoCartonId,
                    Bmrno = value.Bmrno,
                    AddedByUserId = value.LoginUserId.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                _context.BmrtoCartonLine.Add(BMRToCartonLine);
                _context.SaveChanges();
                value.BmrtoCartonLineId = BMRToCartonLine.BmrtoCartonLineId;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBMRToCartonLine")]
        public BMRToCartonLineModel Put(BMRToCartonLineModel value)
        {
            var BMRToCartonLine = _context.BmrtoCartonLine.SingleOrDefault(p => p.BmrtoCartonLineId == value.BmrtoCartonLineId);
            BMRToCartonLine.BmrtoCartonId = value.BmrtoCartonId;
            BMRToCartonLine.Bmrno = value.Bmrno;
            BMRToCartonLine.ModifiedByUserId = value.ModifiedByUserID.Value;
            BMRToCartonLine.ModifiedDate = DateTime.Now;
            BMRToCartonLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBMRToCartonLine")]
        public ActionResult<string> DeleteBMRToCartonLine(int id)
        {
            try
            {
                var BMRToCartonLine = _context.BmrtoCartonLine.SingleOrDefault(p => p.BmrtoCartonLineId == id);
                if (BMRToCartonLine != null)
                {
                    _context.BmrtoCartonLine.Remove(BMRToCartonLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
