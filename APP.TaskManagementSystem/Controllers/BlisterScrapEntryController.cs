﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterScrapEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public BlisterScrapEntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetBlisterScrapEntrys")]
        public List<BlisterScrapEntryModel> Get()
        {
            var blisterScrapEntry = _context.BlisterScrapEntry.Include("AddedByUser").Include("ModifiedByUser").Select(s => new BlisterScrapEntryModel
            {
                ScrapEntryId = s.ScrapEntryId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                SourceItemNo = s.SourceItemNo,
                ItemDescription = s.ItemDescription,
                BatchNo = s.BatchNo,
                BlisterBom = s.BlisterBom,
                PlasticBag = s.PlasticBag,
                BlisterScrapType = s.BlisterScrapType,
                Weight = s.Weight,
                SessionId = s.SessionId,
                StatusCodeId = s.StatusCodeId,
                BlisterScrapTypeID =s.BlisterScrapTypeId,
                PlasticBagID =s.PlasticBagId,
                ModifiedByUserID = s.AddedByUserId,
                AddedByUserID = s.ModifiedByUserId,
                StatusCodeID = s.StatusCodeId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ScrapEntryId).AsNoTracking().ToList();
            //var result = _mapper.Map<List<BlisterScrapEntryModel>>(blisterScrapEntry);
            return blisterScrapEntry;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get BlisterScrapEntry")]
        [HttpGet("GetBlisterScrapEntrys/{id:int}")]
        public ActionResult<BlisterScrapEntryModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var blisterScrapEntry = _context.BlisterScrapEntry.SingleOrDefault(p => p.ScrapEntryId == id.Value);
            var result = _mapper.Map<BlisterScrapEntryModel>(blisterScrapEntry);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterScrapEntryModel> GetData(SearchModel searchModel)
        {
            var blisterScrapEntry = new BlisterScrapEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderBy(o => o.ScrapEntryId).FirstOrDefault(s => s.ScrapEntryId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterScrapEntry = _context.BlisterScrapEntry.OrderByDescending(o => o.ScrapEntryId).FirstOrDefault(s => s.ScrapEntryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlisterScrapEntryModel>(blisterScrapEntry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBlisterScrapEntry")]
        public BlisterScrapEntryModel Post(BlisterScrapEntryModel value)
        {
            var blisterScrapEntry = new BlisterScrapEntry
            {
                // ScrapEntryId=value.ScrapEntryId,
                //ScrapEntryId = value.ScrapEntryId,
                ProdOrderNo = value.ProdOrderNo,
                ProdLineNo = value.ProdLineNo,
                SourceItemNo = value.SourceItemNo,
                ItemDescription = value.ItemDescription,
                BatchNo = value.BatchNo,
                BlisterBom = value.BlisterBom,
                PlasticBag = value.PlasticBag,
                BlisterScrapType = value.BlisterScrapType,
                PlasticBagId = value.PlasticBagID,
                BlisterScrapTypeId = value.BlisterScrapTypeID,
                Weight = value.Weight,
                SessionId = value.SessionId,
                StatusCodeId = value.StatusCodeId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                // StatusCodeId = value.StatusCodeID.Value

            };
            _context.BlisterScrapEntry.Add(blisterScrapEntry);
            _context.SaveChanges();
            value.ScrapEntryId = blisterScrapEntry.ScrapEntryId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBlisterScrapEntry")]
        public BlisterScrapEntryModel Put(BlisterScrapEntryModel value)
        {
            var blisterScrapEntry = _context.BlisterScrapEntry.SingleOrDefault(p => p.ScrapEntryId == value.ScrapEntryId);
            blisterScrapEntry.ModifiedByUserId = value.ModifiedByUserID;
            blisterScrapEntry.ModifiedDate = DateTime.Now;
            blisterScrapEntry.ProdOrderNo = value.ProdOrderNo;
            blisterScrapEntry.ProdLineNo = value.ProdLineNo;
            blisterScrapEntry.SourceItemNo = value.SourceItemNo;
            blisterScrapEntry.ItemDescription = value.ItemDescription;
            blisterScrapEntry.BatchNo = value.BatchNo;
            blisterScrapEntry.BlisterBom = value.BlisterBom;
            blisterScrapEntry.PlasticBag = value.PlasticBag;
            blisterScrapEntry.BlisterScrapType = value.BlisterScrapType;
            blisterScrapEntry.PlasticBagId = value.PlasticBagID;
            blisterScrapEntry.BlisterScrapTypeId = value.BlisterScrapTypeID;
            blisterScrapEntry.Weight = value.Weight;
            blisterScrapEntry.SessionId = value.SessionId;
            //blisterScrapEntry.AddedByUserId = value.AddedByUserID;
            // blisterScrapEntry.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            blisterScrapEntry.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBlisterScrapEntry")]
        public void Delete(int id)
        {
            var blisterScrapEntry = _context.BlisterScrapEntry.SingleOrDefault(p => p.ScrapEntryId == id);
            if (blisterScrapEntry != null)
            {
                _context.BlisterScrapEntry.Remove(blisterScrapEntry);
                _context.SaveChanges();
            }
        }
    }
}