﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OperationSequenceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public OperationSequenceController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetOperationSequences")]
        public List<OperationSequenceModel> Get(int? id)
        {
            List<OperationSequenceModel> operationSequenceModels = new List<OperationSequenceModel>();
            var operationSequence = _context.OperationSequence.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.StatusCode).Where(l => l.StandardManufacturingProcessId == id).OrderByDescending(o => o.OperationSequenceId).AsNoTracking().ToList();
            if(operationSequence!=null && operationSequence.Count>0)
            {
                operationSequence.ForEach(s =>
                {
                    OperationSequenceModel operationSequenceModel = new OperationSequenceModel
                    {
                        OperationSequenceId = s.OperationSequenceId,
                        StandardManufacturingProcessId = s.StandardManufacturingProcessId,
                        EndRepeatProcessNo = s.EndRepeatProcessNo,
                        ProcessType = s.ProcessType,
                        Qcapproval = s.Qcapproval,
                        StartAfterProcessNo = s.StartAfterProcessNo,
                        StartRepeatProcessNo = s.StartRepeatProcessNo,
                        TimeGap = s.TimeGap,
                        TimeGapOfNextProcess = s.TimeGapOfNextProcess,
                        TimeGapOperand = s.TimeGapOperand,                       
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate

                    };
                    operationSequenceModels.Add(operationSequenceModel);
                });

            }
           
            return operationSequenceModels;
        }

     
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<OperationSequenceModel> GetData(SearchModel searchModel)
        {
            var operationSequence = new OperationSequence();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).FirstOrDefault();
                        break;
                    case "Last":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).LastOrDefault();
                        break;
                    case "Next":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).LastOrDefault();
                        break;
                    case "Previous":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).FirstOrDefault();
                        break;
                    case "Last":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).LastOrDefault();
                        break;
                    case "Next":
                        operationSequence = _context.OperationSequence.OrderBy(o => o.OperationSequenceId).FirstOrDefault(s => s.OperationSequenceId > searchModel.Id);
                        break;
                    case "Previous":
                        operationSequence = _context.OperationSequence.OrderByDescending(o => o.OperationSequenceId).FirstOrDefault(s => s.OperationSequenceId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<OperationSequenceModel>(operationSequence);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertOperationSequence")]
        public OperationSequenceModel Post(OperationSequenceModel value)
        {
            var operationSequence = new OperationSequence
            {
                EndRepeatProcessNo = value.EndRepeatProcessNo,
                StandardManufacturingProcessId=value.StandardManufacturingProcessId,
                ProcessType = value.ProcessType,
                Qcapproval = value.Qcapproval,
                StartAfterProcessNo = value.StartAfterProcessNo,
                StartRepeatProcessNo = value.StartRepeatProcessNo,
                TimeGap = value.TimeGap,
                TimeGapOfNextProcess = value.TimeGapOfNextProcess,
                TimeGapOperand = value.TimeGapOperand,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.OperationSequence.Add(operationSequence);
            _context.SaveChanges();
            value.OperationSequenceId = operationSequence.OperationSequenceId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateOperationSequence")]
        public OperationSequenceModel Put(OperationSequenceModel value)
        {
            var operationSequence = _context.OperationSequence.SingleOrDefault(p => p.OperationSequenceId == value.OperationSequenceId);
            operationSequence.EndRepeatProcessNo = value.EndRepeatProcessNo;
            operationSequence.StandardManufacturingProcessId = value.StandardManufacturingProcessId;
            operationSequence.ProcessType = value.ProcessType;
            operationSequence.Qcapproval = value.Qcapproval;
            operationSequence.StartAfterProcessNo = value.StartAfterProcessNo;
            operationSequence.StartRepeatProcessNo = value.StartRepeatProcessNo;
            operationSequence.TimeGap = value.TimeGap;
            operationSequence.TimeGapOfNextProcess = value.TimeGapOfNextProcess;
            operationSequence.TimeGapOperand = value.TimeGapOperand;
            operationSequence.ModifiedByUserId = value.ModifiedByUserID;
            operationSequence.ModifiedDate = DateTime.Now;
            operationSequence.StatusCodeId = value.StatusCodeID.Value;
            
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteOperationSequence")]
        public void Delete(int id)
        {
            var operationSequence = _context.OperationSequence.SingleOrDefault(p => p.OperationSequenceId == id);
            if (operationSequence != null)
            {
                _context.OperationSequence.Remove(operationSequence);
                _context.SaveChanges();
            }
        }
    }
}