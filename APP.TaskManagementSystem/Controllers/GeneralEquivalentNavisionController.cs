﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class GeneralEquivalentNavisionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public GeneralEquivalentNavisionController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetGeneralEquivalentNavisions")]
        public List<GeneralEquivalentNavisionModel> Get()
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var generalEquivalentNavision = _context.GeneralEquivalaentNavision.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.Navision).AsNoTracking().ToList();
            List<GeneralEquivalentNavisionModel> GeneralEquivalentNavisionModel = new List<GeneralEquivalentNavisionModel>();
            generalEquivalentNavision.ForEach(s =>
            {
                GeneralEquivalentNavisionModel GeneralEquivalentNavisionModels = new GeneralEquivalentNavisionModel
                {
                    GeneralEquivalentNavisionID = s.GeneralEquivalaentNavisionId,
                    DatabaseID = s.DatabaseId,
                    NavisionID = s.NavisionId,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                    Navision = s.Navision != null ? (s.Navision.No + '|' + s.Navision.Description + '|' + s.Navision.Description2) : "",
                    ProfileLinkReferenceNo = s.ProfileRefNo,
                    LinkProfileReferenceNo = s.LinkProfileRefNo,
                    MasterProfileReferenceNo = s.MasterProfileRefNo
                };
                GeneralEquivalentNavisionModel.Add(GeneralEquivalentNavisionModels);
            });
            return GeneralEquivalentNavisionModel.OrderByDescending(o => o.GeneralEquivalentNavisionID).ToList();
        }

        [HttpPost]
        [Route("GetGeneralEquivalentNavisionsByRefNo")]
        public List<GeneralEquivalentNavisionModel> GetGeneralEquivalentNavisionsByRefNo(RefSearchModel refSearchModel)
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var generalEquivalentNavision = _context.GeneralEquivalaentNavision.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.Navision).AsNoTracking().ToList();
            List<GeneralEquivalentNavisionModel> GeneralEquivalentNavisionModel = new List<GeneralEquivalentNavisionModel>();
            generalEquivalentNavision.ForEach(s =>
            {
                GeneralEquivalentNavisionModel GeneralEquivalentNavisionModels = new GeneralEquivalentNavisionModel
                {
                    GeneralEquivalentNavisionID = s.GeneralEquivalaentNavisionId,
                    DatabaseID = s.DatabaseId,
                    NavisionID = s.NavisionId,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                    Navision = s.Navision != null ? (s.Navision.No + '|' + s.Navision.Description + '|' + s.Navision.Description2 + '|' + s.Navision.InternalRef) : "",
                    NavisionBuom = s.Navision != null ? s.Navision.BaseUnitofMeasure : "",
                    ProfileLinkReferenceNo = s.ProfileRefNo,
                    LinkProfileReferenceNo = s.LinkProfileRefNo,
                    MasterProfileReferenceNo = s.MasterProfileRefNo
                };
                GeneralEquivalentNavisionModel.Add(GeneralEquivalentNavisionModels);
            });
            if (refSearchModel.IsHeader)
            {
                return GeneralEquivalentNavisionModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.GeneralEquivalentNavisionID).ToList();
            }
            return GeneralEquivalentNavisionModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.GeneralEquivalentNavisionID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<GeneralEquivalentNavisionModel> GetData(SearchModel searchModel)
        {
            var generalEquivalentNavision = new GeneralEquivalaentNavision();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).FirstOrDefault();
                        break;
                    case "Last":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).LastOrDefault();
                        break;
                    case "Next":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).LastOrDefault();
                        break;
                    case "Previous":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).FirstOrDefault();
                        break;
                    case "Last":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).LastOrDefault();
                        break;
                    case "Next":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderBy(o => o.GeneralEquivalaentNavisionId).FirstOrDefault(s => s.GeneralEquivalaentNavisionId > searchModel.Id);
                        break;
                    case "Previous":
                        generalEquivalentNavision = _context.GeneralEquivalaentNavision.OrderByDescending(o => o.GeneralEquivalaentNavisionId).FirstOrDefault(s => s.GeneralEquivalaentNavisionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<GeneralEquivalentNavisionModel>(generalEquivalentNavision);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertGeneralEquivalentNavision")]
        public GeneralEquivalentNavisionModel Post(GeneralEquivalentNavisionModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 , Title= "GeneralEquivalentNavision" });
            var generalEquivalentNavision = new GeneralEquivalaentNavision
            {

                DatabaseId = value.DatabaseID,
                NavisionId = value.NavisionID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                ProfileRefNo = profileNo,
                LinkProfileRefNo = value.LinkProfileReferenceNo,
                MasterProfileRefNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.GeneralEquivalaentNavision.Add(generalEquivalentNavision);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = generalEquivalentNavision.MasterProfileRefNo;
            value.ProfileLinkReferenceNo = generalEquivalentNavision.ProfileRefNo;
            value.GeneralEquivalentNavisionID = generalEquivalentNavision.GeneralEquivalaentNavisionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateGeneralEquivalentNavision")]
        public GeneralEquivalentNavisionModel Put(GeneralEquivalentNavisionModel value)
        {
            var generalEquivalentNavision = _context.GeneralEquivalaentNavision.SingleOrDefault(p => p.GeneralEquivalaentNavisionId == value.GeneralEquivalentNavisionID);
            generalEquivalentNavision.DatabaseId = value.DatabaseID;
            generalEquivalentNavision.NavisionId = value.NavisionID;
            generalEquivalentNavision.StatusCodeId = value.StatusCodeID;
            generalEquivalentNavision.ModifiedByUserId = value.ModifiedByUserID;
            generalEquivalentNavision.ModifiedDate = DateTime.Now;
            generalEquivalentNavision.MasterProfileRefNo = value.MasterProfileReferenceNo;
            generalEquivalentNavision.LinkProfileRefNo = value.LinkProfileReferenceNo;
            generalEquivalentNavision.ProfileRefNo = value.ProfileLinkReferenceNo;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteGeneralEquivalentNavision")]
        public void Delete(int id)
        {
            var generalEquivalentNavision = _context.GeneralEquivalaentNavision.SingleOrDefault(p => p.GeneralEquivalaentNavisionId == id);
            if (generalEquivalentNavision != null)
            {
                _context.GeneralEquivalaentNavision.Remove(generalEquivalentNavision);
                _context.SaveChanges();
            }
        }
    }
}