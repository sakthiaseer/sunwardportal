﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TeamMemberController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TeamMemberController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTeamMemberModel")]
        public List<TeamMemberModel> Get()
        {
            var teamMember = _context.TeamMember.Select(s=>new TeamMemberModel
            {
               TeamMemberID = s.TeamMemberId,
                MemberID= s.MemberId,
                TeamID = s.TeamId
            } ).OrderByDescending(o => o.TeamMemberID).ToList();

            //var result = _mapper.Map<List<TeamMemberModel>>(teamMember);
            return teamMember;
        }

        // GET: api/Project/2
        [HttpGet("GetTeamMember/{id:int}")]
        public ActionResult<TeamMemberModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var teamMember = _context.TeamMember.SingleOrDefault(p => p.TeamMemberId == id.Value);
            var result = _mapper.Map<TeamMemberModel>(teamMember);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TeamMemberModel> GetData(SearchModel searchModel)
        {
            var teamMember = new TeamMember();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).FirstOrDefault();
                        break;
                    case "Last":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).LastOrDefault();
                        break;
                    case "Next":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).LastOrDefault();
                        break;
                    case "Previous":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).FirstOrDefault();
                        break;
                    case "Last":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).LastOrDefault();
                        break;
                    case "Next":
                        teamMember = _context.TeamMember.OrderBy(o => o.TeamMemberId).FirstOrDefault(s => s.TeamMemberId > searchModel.Id);
                        break;
                    case "Previous":
                        teamMember = _context.TeamMember.OrderByDescending(o => o.TeamMemberId).FirstOrDefault(s => s.TeamMemberId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TeamMemberModel>(teamMember);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTeamMember")]
        public TeamMemberModel Post(TeamMemberModel value)
        {
            var teamMember = new TeamMember
            {
                TeamMemberId=value.TeamMemberID,
                MemberId=value.MemberID,
                TeamId=value.TeamID
                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID,
                //Name = value.Name,
                //Description = value.Description
            };
            _context.TeamMember.Add(teamMember);
            _context.SaveChanges();
            value.TeamMemberID = teamMember.TeamMemberId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        [Route("UpdateTeamMember")]
        public TeamMemberModel Put(TeamMemberModel value)
        {
            var teamMember = _context.TeamMember.SingleOrDefault(p => p.TeamMemberId == value.TeamMemberID);
            teamMember.MemberId = value.MemberID;
            teamMember.TeamId = value.TeamID;
            //teamMember.ModifiedByUserId = value.ModifiedByUserID;
            //teamMember.ModifiedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            //project.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Route("DeleteTeamMember")]
        public void Delete(int id)
        {
            var teamMember = _context.TeamMember.SingleOrDefault(p => p.TeamMemberId == id);
            if (teamMember != null)
            {
                _context.TeamMember.Remove(teamMember);
                _context.SaveChanges();
            }
        }
    }
}