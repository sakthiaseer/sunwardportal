﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ManufacturingProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ManufacturingProcessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetManufacturingProcess")]
        public List<ManufacturingProcessModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ManufacturingProcessModel> manufacturingProcessModels = new List<ManufacturingProcessModel>();
            var ManufacturingProcess = _context.ManufacturingProcess
              .Include("AddedByUser")
              .Include("ModifiedByUser")
              .Include("StatusCode")
              .Include("RegisterationCode")
              .Include("ManufacturingFlowChart")
              .Include(s => s.FinishProductGeneralInfo).OrderByDescending(o => o.ManufacturingProcessId).AsNoTracking().ToList();

            var finsiproductList = _context.FinishProduct.ToList();
            if (ManufacturingProcess != null && ManufacturingProcess.Count > 0)
            {
                ManufacturingProcess.ForEach(s =>
                {
                    ManufacturingProcessModel manufacturingProcessModel = new ManufacturingProcessModel
                    {
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        FinishProductId = s.FinishProductId,
                        FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                        ProductName = s.FinishProductGeneralInfo != null && applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                        IsMasterDocument = s.IsMasterDocument,
                        RegisterationCodeId = s.RegisterationCodeId,
                        RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                        IsInformationvsmaster = s.IsInformationvsmaster,
                        ManufacturingFlowChartId = s.ManufacturingFlowChartId,
                        ManufacturingFlowChartName = s.ManufacturingFlowChart != null ? s.ManufacturingFlowChart.FileName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        SessionId = s.SessionId,
                    };
                    manufacturingProcessModels.Add(manufacturingProcessModel);
                });
            }

            return manufacturingProcessModels;
        }
        [HttpGet]
        [Route("GetManufacturingProcessLine")]
        public List<ManufacturingProcessLineModel> GetManufacturingProcessLine(int? id)
        {
            List<ManufacturingProcessLineModel> manufacturingProcessLineModels = new List<ManufacturingProcessLineModel>();

            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var manufacturingProcessLine = _context.ManufacturingProcessLine
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("ManufacturingProcessUpload")
                .Include("StatusCode")
                .Where(a => a.ManufacturingProcessId == id)
                .OrderByDescending(o => o.ManufacturingProcessLineId).AsNoTracking().ToList();
            if (manufacturingProcessLine != null && manufacturingProcessLine.Count > 0)
            {
                manufacturingProcessLine.ForEach(s =>
                {
                    ManufacturingProcessLineModel manufacturingProcessLineModel = new ManufacturingProcessLineModel
                    {
                        ManufacturingProcessLineId = s.ManufacturingProcessLineId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        ProcessStageId = s.ProcessStageId,
                        Remarks = s.Remarks,
                        ProcessStageName = s.ProcessStageId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ProcessStageId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingProcessUploadId = s.ManufacturingProcessUploadId,
                        ManufacturingProcessUploadName = s.ManufacturingProcessUpload != null ? s.ManufacturingProcessUpload.FileName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    };
                    manufacturingProcessLineModels.Add(manufacturingProcessLineModel);
                });


            }

            return manufacturingProcessLineModels;
        }
        [HttpGet]
        [Route("GetManufacturingProcessByFinishProduct")]
        public List<ManufacturingProcessModel> GetManufacturingProcessByFinishProduct(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var finsiproductList = _context.FinishProduct.ToList();
            List<ManufacturingProcessModel> manufacturingProcessModels = new List<ManufacturingProcessModel>();
            var manufacturingProcess = _context.ManufacturingProcess
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("RegisterationCode")
                .Include("ManufacturingFlowChart")
                .Include(s => s.FinishProductGeneralInfo)
                .Where(p => p.FinishProductGeneralInfoId == id && p.IsMasterDocument == true)
                .OrderByDescending(o => o.ManufacturingProcessId).AsNoTracking().ToList();
            if (manufacturingProcess != null && manufacturingProcess.Count > 0)
            {
                manufacturingProcess.ForEach(s =>
                {
                    ManufacturingProcessModel manufacturingProcessModel = new ManufacturingProcessModel
                    {
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        FinishProductId = s.FinishProductId,
                        FinishProductGeneralInfoId = s.FinishProductGeneralInfoId,
                        ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == (finsiproductList.FirstOrDefault(f => f.FinishProductId == s.FinishProductGeneralInfo.FinishProductId).ProductId)).Value : "",
                        IsMasterDocument = s.IsMasterDocument,
                        //IsInformationvsmaster = s.IsInformationvsmaster,
                        ManufacturingFlowChartId = s.ManufacturingFlowChartId,
                        ManufacturingFlowChartName = s.ManufacturingFlowChart != null ? s.ManufacturingFlowChart.FileName : "",
                        RegisterationCodeId = s.RegisterationCodeId,
                        RegisterationCodeName = s.RegisterationCode != null ? s.RegisterationCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        SessionId = s.SessionId,
                    };
                    manufacturingProcessModels.Add(manufacturingProcessModel);
                });


            }

            return manufacturingProcessModels;
        }
        [HttpGet]
        [Route("IsMasterExist")]
        public bool IsMasterExist(int? id)
        {
            var list = _context.ManufacturingProcess.Where(s => s.FinishProductId == id && s.IsMasterDocument == true).FirstOrDefault();
            if (list != null)
            {
                throw new AppException("Product Master Document Already Exist!!!", null);
            }
            else
            {
                return false;
            }
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ManufacturingProcessModel> GetData(SearchModel searchModel)
        {
            var ManufacturingProcess = new ManufacturingProcess();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).LastOrDefault();
                        break;
                    case "Next":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).LastOrDefault();
                        break;
                    case "Previous":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).LastOrDefault();
                        break;
                    case "Next":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderBy(o => o.ManufacturingProcessId).FirstOrDefault(s => s.ManufacturingProcessId > searchModel.Id);
                        break;
                    case "Previous":
                        ManufacturingProcess = _context.ManufacturingProcess.OrderByDescending(o => o.ManufacturingProcessId).FirstOrDefault(s => s.ManufacturingProcessId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ManufacturingProcessModel>(ManufacturingProcess);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
                var finishProductItem = result.FinishProductGeneralInfoId > 0 ? (_context.FinishProductGeneralInfo.FirstOrDefault(f => f.FinishProductGeneralInfoId == result.FinishProductGeneralInfoId).FinishProductId) : null;
                if (finishProductItem != null)
                {
                    var productId = _context.FinishProduct.FirstOrDefault(f => f.FinishProductId == finishProductItem).ProductId;
                    result.ProductName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == productId).Value : "";
                }
            }
            return result;
        }
        [HttpPost]
        [Route("InsertManufacturingProcess")]
        public ManufacturingProcessModel Post(ManufacturingProcessModel value)
        {
            var SessionId = Guid.NewGuid();
            var docu = value.SessionID != null ? (_context.Documents.Select(a => new { a.SessionId, a.DocumentId }).Where(d => d.SessionId == value.SessionID).FirstOrDefault()) : null;
            var ManufacturingProcess = new ManufacturingProcess
            {
                FinishProductId = value.FinishProductId,
                FinishProductGeneralInfoId = value.FinishProductGeneralInfoId,
                RegisterationCodeId = value.RegisterationCodeId.Value,
                IsMasterDocument = value.IsMasterDocument,
                IsInformationvsmaster = value.IsInformationvsmaster,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                ManufacturingFlowChartId = docu != null && docu.DocumentId > 0 ? docu.DocumentId : default(long?),
                SessionId = SessionId,
            };
            _context.ManufacturingProcess.Add(ManufacturingProcess);
            _context.SaveChanges();
            value.ManufacturingProcessId = value.ManufacturingProcessId;
            value.SessionId = SessionId;
            if (value.IsMasterDocument == false && value.IsInformationvsmaster == true && value.FinishProductGeneralInfoId > 0)
            {
                var ManufacturingProcessId = _context.ManufacturingProcess.FirstOrDefault(s => s.FinishProductGeneralInfoId == value.FinishProductGeneralInfoId && s.IsInformationvsmaster == false && s.IsMasterDocument == true).ManufacturingProcessId;
                if (ManufacturingProcessId > 0)
                {
                    var ManufacturingProcessLineItems = _context.ManufacturingProcessLine.Where(s => s.ManufacturingProcessId == ManufacturingProcessId).ToList();

                    ManufacturingProcessLineItems.ForEach(c =>
                    {
                        var ManufacturingProcessLine = new ManufacturingProcessLine
                        {
                            ManufacturingProcessId = value.ManufacturingProcessId,
                            ProcessStageId = c.ProcessStageId,
                            Remarks = c.Remarks,
                            ManufacturingProcessUploadId = docu.DocumentId > 0 ? docu.DocumentId : default(long?),
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                        };
                        _context.ManufacturingProcessLine.Add(ManufacturingProcessLine);
                        _context.SaveChanges();
                    });
                }
            }
            return value;
        }
        [HttpPost]
        [Route("InsertManufacturingProcessLine")]
        public ManufacturingProcessLineModel post(ManufacturingProcessLineModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var docu = value.SessionID != null ? (_context.Documents.Select(a => new { a.SessionId, a.DocumentId }).Where(d => d.SessionId == value.SessionID).FirstOrDefault()) : null;
            var ManufacturingProcessLine = new ManufacturingProcessLine
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                ProcessStageId = value.ProcessStageId,
                Remarks = value.Remarks,
                ManufacturingProcessUploadId = docu != null && docu.DocumentId > 0 ? docu.DocumentId : default(long?),
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,

            };
            value.ProcessStageName = value.ProcessStageId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.ProcessStageId).Select(m => m.Value).FirstOrDefault() : "";
            _context.ManufacturingProcessLine.Add(ManufacturingProcessLine);
            ManufacturingProcessLine.ManufacturingProcessLineId = ManufacturingProcessLine.ManufacturingProcessLineId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateManufacturingProcess")]
        public ManufacturingProcessModel Put(ManufacturingProcessModel value)
        {
            var docu = value.SessionID != null ? (_context.Documents.Select(a => new { a.SessionId, a.DocumentId }).Where(d => d.SessionId == value.SessionID).FirstOrDefault()) : null;
            var ManufacturingProcess = _context.ManufacturingProcess.SingleOrDefault(p => p.ManufacturingProcessId == value.ManufacturingProcessId);
            ManufacturingProcess.FinishProductId = value.FinishProductId;
            ManufacturingProcess.FinishProductGeneralInfoId = value.FinishProductGeneralInfoId;
            ManufacturingProcess.RegisterationCodeId = value.RegisterationCodeId;
            ManufacturingProcess.IsInformationvsmaster = value.IsInformationvsmaster;
            ManufacturingProcess.IsMasterDocument = value.IsMasterDocument;
            ManufacturingProcess.ModifiedByUserId = value.ModifiedByUserID;
            ManufacturingProcess.StatusCodeId = value.StatusCodeID.Value;
            ManufacturingProcess.ModifiedDate = DateTime.Now;
            ManufacturingProcess.ManufacturingFlowChartId = value.ManufacturingFlowChartId = docu != null && docu.DocumentId > 0 ? docu.DocumentId : value.ManufacturingFlowChartId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateManufacturingProcessLine")]
        public ManufacturingProcessLineModel Put(ManufacturingProcessLineModel value)
        {
            var docu = value.SessionID != null ? (_context.Documents.Select(a => new { a.SessionId, a.DocumentId }).Where(d => d.SessionId == value.SessionID).FirstOrDefault()) : null;

            var ManufacturingProcessLine = _context.ManufacturingProcessLine.SingleOrDefault(p => p.ManufacturingProcessLineId == value.ManufacturingProcessLineId);
            ManufacturingProcessLine.ManufacturingProcessId = value.ManufacturingProcessId;
            ManufacturingProcessLine.ProcessStageId = value.ProcessStageId;
            ManufacturingProcessLine.Remarks = value.Remarks;
            ManufacturingProcessLine.ManufacturingProcessUploadId = value.ManufacturingProcessUploadId = docu != null && docu.DocumentId > 0 ? docu.DocumentId : value.ManufacturingProcessUploadId;
            ManufacturingProcessLine.ModifiedByUserId = value.ModifiedByUserID;
            ManufacturingProcessLine.StatusCodeId = value.StatusCodeID.Value;
            ManufacturingProcessLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteManufacturingProcess")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var ManufacturingProcess = _context.ManufacturingProcess.SingleOrDefault(p => p.ManufacturingProcessId == id);
                if (ManufacturingProcess != null)
                {
                    var ManufacturingProcessLine = _context.ManufacturingProcessLine.Where(s => s.ManufacturingProcessId == id).ToList();
                    if (ManufacturingProcessLine != null)
                    {
                        _context.ManufacturingProcessLine.RemoveRange(ManufacturingProcessLine);
                        _context.SaveChanges();
                    }
                    _context.ManufacturingProcess.Remove(ManufacturingProcess);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteManufacturingProcessLine")]
        public ActionResult<string> DeleteManufacturingProcessLine(int id)
        {
            try
            {
                var ManufacturingProcessLine = _context.ManufacturingProcessLine.Where(p => p.ManufacturingProcessLineId == id).FirstOrDefault();
                if (ManufacturingProcessLine != null)
                {
                    _context.ManufacturingProcessLine.Remove(ManufacturingProcessLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}