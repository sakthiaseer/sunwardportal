﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserGroupController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public UserGroupController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetUserGroups")]
        public List<UserGroupModel> Get()
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var UserGrouplist = _context.UserGroup.Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Include(s => s.UserGroupUser).OrderByDescending(o => o.UserGroupId).Where(u => u.IsTms == true).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel

            if (UserGrouplist != null && UserGrouplist.Count > 0)
            {
                UserGrouplist.ForEach(s =>
                {
                    UserGroupModel userGroupModel = new UserGroupModel
                    {
                        UserGroupID = s.UserGroupId,
                        Name = s.Name,
                        Description = s.Description,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsTMS = s.IsTms.Value,
                        UserIDs = s.UserGroupUser != null ? s.UserGroupUser.Select(c => c.UserId).ToList() : new List<long?>(),


                    };
                    userGroupModels.Add(userGroupModel);
                });
            }



            return userGroupModels;
        }

        [HttpGet]
        [Route("GetUserGroupsBySession")]
        public List<UserGroupNameModel> GetUserGroupsBySession(Guid sessionId)
        {
            List<UserGroupNameModel> userGroupModels = new List<UserGroupNameModel>();
            var UserGrouplist = _context.UserGroupUser.Include(a => a.User).Include(a => a.UserGroup).OrderByDescending(o => o.UserGroupId).Where(u => u.User.SessionId == sessionId && u.UserGroupId != null).AsNoTracking().ToList();

            if (UserGrouplist != null && UserGrouplist.Count > 0)
            {
                UserGrouplist.ForEach(s =>
                {
                    UserGroupNameModel userGroupModel = new UserGroupNameModel
                    {
                        UserGroupID = s.UserGroupId.Value,
                        Name = s.UserGroup?.Name,
                        Description = s.UserGroup?.Description,
                    };
                    userGroupModels.Add(userGroupModel);
                });
            }



            return userGroupModels;
        }
        [HttpGet]
        [Route("GetUserGroupsInfo")]
        public List<GetUserGroupModel> GetUserGroupsInfo()
        {
            List<GetUserGroupModel> userGroupModels = new List<GetUserGroupModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var UserGrouplist = _context.UserGroup.Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Include(s => s.UserGroupUser).OrderByDescending(o => o.UserGroupId).Where(u => u.IsTms == true).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel

            if (UserGrouplist != null && UserGrouplist.Count > 0)
            {
                UserGrouplist.ForEach(s =>
                {
                    GetUserGroupModel userGroupModel = new GetUserGroupModel
                    {
                        UserGroupID = s.UserGroupId,
                        Name = s.Name,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        IsTMS = s.IsTms.Value,
                        UserIDs = s.UserGroupUser != null ? s.UserGroupUser.Select(c => c.UserId).ToList() : new List<long?>(),


                    };
                    userGroupModels.Add(userGroupModel);
                });
            }



            return userGroupModels;
        }

        [HttpPost]
        [Route("GetUserGroupsUserSessionIds")]
        public List<Guid?> GetUserGroupsUserSessionIds(List<long?> userGroupIds)
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var Userlist = _context.UserGroupUser.Where(u => userGroupIds.Contains(u.UserGroupId)).AsNoTracking().Select(u => u.UserId).Distinct().ToList();//.Select(s => new BompackingMasterModel
            List<Guid?> sessionIds = new List<Guid?>();
            sessionIds = _context.ApplicationUser.Where(t => Userlist.Contains(t.UserId)).Select(s => s.SessionId).ToList();


            return sessionIds;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetMobileUserGroups")]
        public List<CodeMasterModel> GetMobileUserGroups()
        {
            List<CodeMasterModel> userGroupModels = new List<CodeMasterModel>();

            var UserGrouplist = _context.UserGroup.OrderByDescending(o => o.UserGroupId).Where(u => u.IsTms == true).AsNoTracking().ToList();
            if (UserGrouplist != null && UserGrouplist.Count > 0)
            {
                UserGrouplist.ForEach(s =>
                {
                    CodeMasterModel userGroupModel = new CodeMasterModel
                    {
                        Id = s.UserGroupId,
                        CodeValue = s.Name,
                    };
                    userGroupModels.Add(userGroupModel);
                });
            }
            return userGroupModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get UserGroup")]
        [HttpGet("GetUserGroups/{id:int}")]
        public ActionResult<UserGroupModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var UserGroup = _context.UserGroup.SingleOrDefault(p => p.UserGroupId == id.Value);
            var result = _mapper.Map<UserGroupModel>(UserGroup);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<UserGroupModel> GetData(SearchModel searchModel)
        {
            var UserGroup = new UserGroup();
            var userGroupModel = new UserGroupModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).FirstOrDefault();
                        break;
                    case "Last":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).LastOrDefault();
                        break;
                    case "Next":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).LastOrDefault();
                        break;
                    case "Previous":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).FirstOrDefault();
                        break;
                    case "Last":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).LastOrDefault();
                        break;
                    case "Next":
                        UserGroup = _context.UserGroup.OrderBy(o => o.UserGroupId).FirstOrDefault(s => s.UserGroupId > searchModel.Id);
                        break;
                    case "Previous":
                        UserGroup = _context.UserGroup.OrderByDescending(o => o.UserGroupId).FirstOrDefault(s => s.UserGroupId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<UserGroupModel>(UserGroup);
            userGroupModel.UserIDs = _context.UserGroupUser.Where(t => t.UserGroupId == searchModel.Id).Select(t => t.UserId).ToList();
            if (result != null)
            {
                if (result.UserGroupID > 0)
                {

                    result.UserIDs = userGroupModel.UserIDs;
                }
                if (result.IsTMS)
                {
                    return result;
                }
            }

            return null;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertUserGroup")]
        public UserGroupModel Post(UserGroupModel value)
        {
            var UserGroup = new UserGroup
            {
                // UserGroupId=value.UserGroupID,
                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                IsTms = true,


            };

            value.UserIDs.ForEach(c =>
            {
                var userGroupUser = new UserGroupUser
                {
                    UserId = c,
                };
                UserGroup.UserGroupUser.Add(userGroupUser);
            });

            _context.UserGroup.Add(UserGroup);
            _context.SaveChanges();
            value.UserGroupID = UserGroup.UserGroupId;

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateUserGroup")]
        public UserGroupModel Put(UserGroupModel value)
        {
            var userGroup = _context.UserGroup.SingleOrDefault(p => p.UserGroupId == value.UserGroupID);
            userGroup.ModifiedByUserId = value.ModifiedByUserID;
            userGroup.ModifiedDate = DateTime.Now;
            userGroup.Name = value.Name;
            userGroup.Description = value.Description;
            userGroup.IsTms = value.IsTMS;
            //UserGroup.AddedByUserId = value.AddedByUserID;
            // UserGroup.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            userGroup.StatusCodeId = value.StatusCodeID.Value;


            var userGroupUsers = _context.UserGroupUser.Where(l => l.UserGroupId == value.UserGroupID).ToList();
            if (userGroupUsers.Count > 0)
            {
                _context.UserGroupUser.RemoveRange(userGroupUsers);
                _context.SaveChanges();
            }
            value.UserIDs.ForEach(c =>
            {
                var userGroupUser = new UserGroupUser
                {
                    UserId = c,
                    UserGroupId = value.UserGroupID,
                };
                userGroup.UserGroupUser.Add(userGroupUser);
            });

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteUserGroup")]
        public void Delete(int id)
        {
            var UserGroup = _context.UserGroup.SingleOrDefault(p => p.UserGroupId == id);
            if (UserGroup != null)
            {
                var userGroupuser = _context.UserGroupUser.Where(d => d.UserGroupId == UserGroup.UserGroupId).ToList();
                if (userGroupuser.Count > 0)
                {
                    _context.UserGroupUser.RemoveRange(userGroupuser);
                    _context.SaveChanges();
                }
                _context.UserGroup.Remove(UserGroup);
                _context.SaveChanges();
            }
        }
    }
}