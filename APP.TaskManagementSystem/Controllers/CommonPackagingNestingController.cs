﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingNestingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingNestingController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingNesting")]
        public List<CommonPackagingNestingModel> Get()
        {
            var commonPackagingNesting = _context.CommonPackagingNesting
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingNestingModel> commonPackagingNestingModel = new List<CommonPackagingNestingModel>();
            if (commonPackagingNesting.Count > 0)
            {
                List<long?> masterIds = commonPackagingNesting.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingNesting.ForEach(s =>
                {
                    CommonPackagingNestingModel commonPackagingNestingModels = new CommonPackagingNestingModel
                    {
                        NestingSpecificationId = s.NestingSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        RowMeasurementLength = s.RowMeasurementLength,
                        RowMeasurementWidth = s.RowMeasurementWidth,
                        ColumnMeasurementLength = s.ColumnMeasurementLength,
                        ColumnMeasurementWidth = s.ColumnMeasurementWidth,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.NestingSpecificationId == s.NestingSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingNestingModel.Add(commonPackagingNestingModels);
                });
            }
            return commonPackagingNestingModel.OrderByDescending(a => a.NestingSpecificationId).ToList();
        }
        [HttpPost]
        [Route("GetCommonPackagingNestingByRefNo")]
        public List<CommonPackagingNestingModel> GetCommonPackagingNestingByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingNesting = _context.CommonPackagingNesting
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.BottleCapUseForItems)
                 .Include(s => s.StatusCode).Where(w => w.NestingSpecificationId > 0);
            List<CommonPackagingNestingModel> commonPackagingNestingModel = new List<CommonPackagingNestingModel>();
            if (refSearchModel.IsHeader)
            {
                commonPackagingNesting = commonPackagingNesting.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingNesting = commonPackagingNesting.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingNestings = commonPackagingNesting.AsNoTracking().ToList();
            if (commonPackagingNestings.Count > 0)
            {
                List<long?> masterIds = commonPackagingNestings.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingNestings.ForEach(s =>
                {
                    CommonPackagingNestingModel commonPackagingNestingModels = new CommonPackagingNestingModel
                    {
                        NestingSpecificationId = s.NestingSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        RowMeasurementLength = s.RowMeasurementLength,
                        RowMeasurementWidth = s.RowMeasurementWidth,
                        ColumnMeasurementLength = s.ColumnMeasurementLength,
                        ColumnMeasurementWidth = s.ColumnMeasurementWidth,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.NestingSpecificationId == s.NestingSpecificationId).Select(b => b.UseForItemId).ToList(),
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingNestingModel.Add(commonPackagingNestingModels);
                });
            }
            return commonPackagingNestingModel.OrderByDescending(o => o.NestingSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingNestingModel> GetData(SearchModel searchModel)
        {
            var commonPackagingNesting = new CommonPackagingNesting();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderBy(o => o.NestingSpecificationId).FirstOrDefault(s => s.NestingSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingNesting = _context.CommonPackagingNesting.OrderByDescending(o => o.NestingSpecificationId).FirstOrDefault(s => s.NestingSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingNestingModel>(commonPackagingNesting);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingNesting")]
        public CommonPackagingNestingModel Post(CommonPackagingNestingModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingNesting" });

            var commonPackagingNesting = new CommonPackagingNesting
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                RowMeasurementLength = value.RowMeasurementLength,
                RowMeasurementWidth = value.RowMeasurementWidth,
                ColumnMeasurementLength = value.ColumnMeasurementLength,
                ColumnMeasurementWidth = value.ColumnMeasurementWidth,
                VersionControl = value.VersionControl,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingNesting.Add(commonPackagingNesting);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingNesting.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingNesting.ProfileLinkReferenceNo;
            value.NestingSpecificationId = commonPackagingNesting.NestingSpecificationId;
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.NestingSpecificationId == value.NestingSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        NestingSpecificationId = value.NestingSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingNestingModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    if (value.RowMeasurementLength != null && value.RowMeasurementLength > 0)
                    {
                        itemName = value.RowMeasurementLength.ToString();
                    }
                    if (value.RowMeasurementWidth != null && value.RowMeasurementWidth > 0)
                    {
                        itemName = itemName + "X" + value.RowMeasurementWidth + "mm" + "," + " ";
                    }
                    if (value.ColumnMeasurementLength != null && value.ColumnMeasurementLength > 0)
                    {
                        itemName = itemName + value.ColumnMeasurementLength;
                    }
                    if (value.ColumnMeasurementWidth != null && value.ColumnMeasurementWidth > 0)
                    {
                        itemName = itemName + "X" + value.ColumnMeasurementWidth;
                    }
                    itemName = itemName + "mm Nesting";
                    //itemName = value.RowMeasurementLength + "X" + value.RowMeasurementWidth + "mm" + "," + " " + value.ColumnMeasurementLength + "X" + value.ColumnMeasurementWidth + "mm Nesting";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingNesting")]
        public CommonPackagingNestingModel Put(CommonPackagingNestingModel value)
        {
            var commonPackagingNesting = _context.CommonPackagingNesting.SingleOrDefault(p => p.NestingSpecificationId == value.NestingSpecificationId);
            commonPackagingNesting.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingNesting.Set = value.Set;
            commonPackagingNesting.RowMeasurementLength = value.RowMeasurementLength;
            commonPackagingNesting.RowMeasurementWidth = value.RowMeasurementWidth;
            commonPackagingNesting.ColumnMeasurementLength = value.ColumnMeasurementLength;
            commonPackagingNesting.ColumnMeasurementWidth = value.ColumnMeasurementWidth;
            commonPackagingNesting.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingNesting.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingNesting.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingNesting.VersionControl = value.VersionControl;
            commonPackagingNesting.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingNesting.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.NestingSpecificationId == value.NestingSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.NestingSpecificationId == value.NestingSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        NestingSpecificationId = value.NestingSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingNesting")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingNesting = _context.CommonPackagingNesting.Where(p => p.NestingSpecificationId == id).FirstOrDefault();
                if (commonPackagingNesting != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.NestingSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingNesting.Remove(commonPackagingNesting);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}