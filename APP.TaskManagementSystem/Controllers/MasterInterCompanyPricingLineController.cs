﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterInterCompanyPricingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public MasterInterCompanyPricingLineController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }
        [HttpGet]
        [Route("GetNavisionCompany")]
        public List<CompanyListingModel> GetNavisionCompany(string type)
        {
            var companylist = _context.CompanyListing.ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavisionCompanyModel> navisionCompanys = new List<NavisionCompanyModel>();
            List<NavisionCompany> navisionCompany = new List<NavisionCompany>();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();
            navisionCompany = _context.NavisionCompany.Include(c => c.NavCustomer).AsNoTracking().ToList();
            if (navisionCompany != null && navisionCompany.Count > 0)
            {
                navisionCompany.ForEach(s =>
                {
                    NavisionCompanyModel navisionCompanyModel = new NavisionCompanyModel
                    {
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        CompanyId = companylist.Where(w => w.ProfileReferenceNo == s.LinkProfileReferenceNo).Select(c => c.CompanyListingId).FirstOrDefault(),
                        CompanyName = companylist.Where(w => w.ProfileReferenceNo == s.LinkProfileReferenceNo).Select(c => c.CompanyName).FirstOrDefault(),
                        NavisionCompanyId = s.NavisionCompanyId,
                        NavCustomerName = s.NavCustomer != null ? s.NavCustomer.Name : null,
                        DatabaseName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DatabaseId).Select(a => a.Value).SingleOrDefault() : "",
                        CompanyTypeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CompanyTypeId).Select(a => a.Value).SingleOrDefault() : "",
                    };
                    navisionCompanys.Add(navisionCompanyModel);
                });
            }
            var navisionCompanyss = navisionCompanys.Where(w => w.CompanyTypeName == "Customer" && w.DatabaseName == type).GroupBy(s => new { s.CompanyTypeName, s.LinkProfileReferenceNo, s.CompanyId, s.CompanyName }).ToList();
            if (navisionCompanyss.Count > 0)
            {
                navisionCompanyss.ForEach(h =>
                {
                    CompanyListingModel CompanyListingModel = new CompanyListingModel
                    {
                        CompanyName = h.Key.CompanyName,
                        CompanyListingID = h.Key.CompanyId.Value,
                    };
                    CompanyListingModels.Add(CompanyListingModel);
                });
            }
            return CompanyListingModels;
        }
        [HttpGet]
        [Route("GetMasterInterCompanyPricingLines")]
        public List<MasterInterCompanyPricingLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var masterInterCompanyPricingLines = _context.MasterInterCompanyPricingLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.SellingTo)
                .AsNoTracking()
                .Where(m => m.MasterInterCompanyPricingId == id)
                .ToList();
            List<MasterInterCompanyPricingLineModel> masterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
            masterInterCompanyPricingLines.ForEach(s =>
            {
                MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel
                {
                    MasterInterCompanyPricingLineId = s.MasterInterCompanyPricingLineId,
                    MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                    CurrencyId = s.CurrencyId,
                    Currency = masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId).Value : "",
                    BillingPercentage = s.BillingPercentage,
                    SellingToId = s.SellingToId,
                    SellingCompany = s.SellingTo?.CompanyName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                };
                masterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);
            });
            return masterInterCompanyPricingLineModels.OrderByDescending(a => a.MasterInterCompanyPricingLineId).ToList();
        }

        [HttpPost()]
        [Route("GetMasterInterCompanyPricingLineData")]
        public ActionResult<MasterInterCompanyPricingLineModel> GetMasterInterCompanyPricingLineData(SearchModel searchModel)
        {
            var masterInterCompanyPricingLine = new MasterInterCompanyPricingLine();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).LastOrDefault();
                        break;
                    case "Next":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).LastOrDefault();
                        break;
                    case "Previous":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).LastOrDefault();
                        break;
                    case "Next":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderBy(o => o.MasterInterCompanyPricingLineId).FirstOrDefault(s => s.MasterInterCompanyPricingLineId > searchModel.Id);
                        break;
                    case "Previous":
                        masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Include(s => s.SellingTo).OrderByDescending(o => o.MasterInterCompanyPricingLineId).FirstOrDefault(s => s.MasterInterCompanyPricingLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MasterInterCompanyPricingLineModel>(masterInterCompanyPricingLine);
            if (result != null)
            {
                result.SellingCompany = masterInterCompanyPricingLine.SellingTo != null ? masterInterCompanyPricingLine.SellingTo.CompanyName : null;
            }

            return result;
        }

        [HttpPost]
        [Route("InsertMasterInterCompanyPricingLine")]
        public MasterInterCompanyPricingLineModel Post(MasterInterCompanyPricingLineModel value)
        {
            var masterInterCompanyPricingLine = new MasterInterCompanyPricingLine
            {
                MasterInterCompanyPricingId = value.MasterInterCompanyPricingId,
                SellingToId = value.SellingToId,
                CurrencyId = value.CurrencyId,
                BillingPercentage = value.BillingPercentage,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.MasterInterCompanyPricingLine.Add(masterInterCompanyPricingLine);
            _context.SaveChanges();
            value.MasterInterCompanyPricingLineId = masterInterCompanyPricingLine.MasterInterCompanyPricingLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateMasterInterCompanyPricingLine")]
        public async Task<MasterInterCompanyPricingLineModel> Put(MasterInterCompanyPricingLineModel value)
        {
            var masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.SingleOrDefault(p => p.MasterInterCompanyPricingLineId == value.MasterInterCompanyPricingLineId);
            if (masterInterCompanyPricingLine != null)
            {
                if (value.SaveVersionData)
                {
                    var masterIntercompanyPricingModel = GetMasterInterCompanyPricingByID((int)value.MasterInterCompanyPricingId);
                    var versionInfo = new TableDataVersionInfoModel<MasterBlanketOrderModel>
                    {
                        JsonData = JsonSerializer.Serialize(masterIntercompanyPricingModel),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = masterIntercompanyPricingModel.VersionSessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "MasterInterCompanyPricing",
                        PrimaryKey = masterIntercompanyPricingModel.MasterInterCompanyPricingId,
                        ReferenceInfo = value.ReferenceInfo,
                    };
                    await _repository.Insert(versionInfo);
                }
                masterInterCompanyPricingLine.SellingToId = value.SellingToId;
                masterInterCompanyPricingLine.CurrencyId = value.CurrencyId;
                masterInterCompanyPricingLine.BillingPercentage = value.BillingPercentage;
                masterInterCompanyPricingLine.StatusCodeId = value.StatusCodeID.Value;
                masterInterCompanyPricingLine.ModifiedByUserId = value.ModifiedByUserID;
                masterInterCompanyPricingLine.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }


            return value;
        }

        private MasterInterCompanyPricingModel GetMasterInterCompanyPricingByID(long? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var s = _context.MasterInterCompanyPricing
                .Include(a => a.AddedByUser)
                .Include(m => m.MasterInterCompanyPricingLine)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.FromCompany)
                .Include(c => c.ToCompany)
                .FirstOrDefault(m => m.MasterInterCompanyPricingId == id);
            MasterInterCompanyPricingModel masterInterCompanyPricingModel = new MasterInterCompanyPricingModel
            {
                MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                ValidityFrom = s.ValidityFrom,
                ValidityTo = s.ValiditiyTo,
                IsNeedVersionFunction = s.IsNeedVersionFunction,
                FromCompanyId = s.FromCompanyId,
                FromCompany = s.FromCompany != null ? s.FromCompany.PlantCode : "",
                ToCompanyId = s.ToCompanyId,
                ToCompany = s.ToCompany != null ? s.ToCompany.PlantCode : "",
                InterCompanyMinMargin = s.InterCompanyMinMargin,
                InterCompanyProfitSharing = s.InterCompanyProfitSharing,
                MinBillingToCustomer = s.MinBillingToCustomer,
                CurrencyId = s.CurrencyId,
                CurrencyName = masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId) != null ? masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId).Value : string.Empty,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                VersionSessionId = s.VersionSessionId,
            };

            if (s.MasterInterCompanyPricingLine != null && s.MasterInterCompanyPricingLine.Count > 0)
            {
                masterInterCompanyPricingModel.MasterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
                s.MasterInterCompanyPricingLine.ToList().ForEach(l =>
                {
                    MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel
                    {
                        SellingToId = l.SellingToId,
                        CurrencyId = l.CurrencyId,
                        BillingPercentage = l.BillingPercentage,
                        StatusCodeID = l.StatusCodeId,
                        AddedByUserID = l.AddedByUserId,
                        AddedDate = l.AddedDate
                    };
                    masterInterCompanyPricingModel.MasterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);
                });

            }

            return masterInterCompanyPricingModel;
        }

        [HttpDelete]
        [Route("DeleteMasterInterCompanyPricingLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var masterInterCompanyPricingLine = _context.MasterInterCompanyPricingLine.Where(p => p.MasterInterCompanyPricingLineId == id).FirstOrDefault();

                if (masterInterCompanyPricingLine != null)
                {
                    _context.MasterInterCompanyPricingLine.Remove(masterInterCompanyPricingLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}