﻿using System;
using System.Collections.Generic;
using APP.EntityModel;
using APP.EntityModel.Param;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using System.Data;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationHandlerController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly MailService _mailService;
        public NotificationHandlerController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host, MailService mailService)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
            _mailService = mailService;
        }
        [HttpGet]
        [Route("GetNotificationHandler")]
        public void Get()
        {
            var ApplicationWikiLine = _context.ApplicationWikiLine.Include(a => a.Repeat).Include(b => b.Custom).Include(c => c.ApplicationWikiWeekly).ToList();
            var Notification = _context.NotificationHandler.Where(w => w.NextNotifyDate.Value.Date ==DateTime.Now.Date && w.NotifyStatusId == 1).ToList();
            if (Notification != null)
            {
                Notification.ForEach(h =>
                {
                    NotificationHandler NotificationHandler = new NotificationHandler();
                    var ApplicationWikiLineData = ApplicationWikiLine.Where(s => s.SessionId == h.SessionId).FirstOrDefault();
                    if (ApplicationWikiLineData.CustomId == 2133)
                    {
                        var date = new DateTime(h.NextNotifyDate.Value.Year, h.NextNotifyDate.Value.Month, ApplicationWikiLineData.Monthly.Value);
                        var addOneMonth = h.NextNotifyDate.Value.AddMonths(1);
                        var nextDate = (new DateTime(addOneMonth.Year, addOneMonth.Month, DateTime.DaysInMonth(addOneMonth.Year, addOneMonth.Month))).Day;
                        if (nextDate >= ApplicationWikiLineData.Monthly)
                        {
                            date = date.AddMonths(1);
                        }
                        else
                        {
                            date = date.AddMonths(2);
                        }
                        NotificationHandler.NextNotifyDate = date;
                        NotificationHandler.AddedDays = 0;

                    }
                    else if (ApplicationWikiLineData.CustomId == 2134)
                    {
                        if (ApplicationWikiLineData.DaysOfWeek == true)
                        {
                            var nextDate = h.NextNotifyDate.Value.Date.AddDays(7);
                            var nextYear = h.NextNotifyDate.Value.Date.AddYears(1);
                            var day = h.NextNotifyDate.Value.Date.DayOfWeek;
                            DateTime firstDay = new DateTime(nextYear.Year, nextYear.Month, 1);
                            var t = firstDay.AddDays((7 - firstDay.DayOfWeek.GetHashCode() + day.GetHashCode()) % 7);
                            NotificationHandler.NextNotifyDate = (nextDate.Date.Month == ApplicationWikiLineData.Yearly) ? nextDate : t;
                            NotificationHandler.AddedDays = (nextDate.AddDays(7).Date.Month == ApplicationWikiLineData.Yearly) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                        }
                        else
                        {
                            NotificationHandler.NextNotifyDate = h.NextNotifyDate.Value.AddYears(1);
                            NotificationHandler.AddedDays = 0;
                        }

                    }
                    else
                    {
                        if (ApplicationWikiLineData.RepeatId == 1885)
                        {
                            NotificationHandler.NextNotifyDate = new DateTime(h.NextNotifyDate.Value.Year, h.NextNotifyDate.Value.Month, 1).AddMonths(+1);
                            NotificationHandler.AddedDays = 0;
                        }
                        else if (ApplicationWikiLineData.RepeatId == 1886)
                        {
                            NotificationHandler.NextNotifyDate = new DateTime(h.NextNotifyDate.Value.Year, h.NextNotifyDate.Value.Month, 1).AddYears(1);
                            NotificationHandler.AddedDays = 0;
                        }
                        else
                        {
                            NotificationHandler.NextNotifyDate = h.NextNotifyDate.Value.Date.AddDays((int)h.AddedDays);
                            NotificationHandler.AddedDays = h.AddedDays;
                        }
                    }
                    NotificationHandler.SessionId = h.SessionId;
                    NotificationHandler.NotifyStatusId = h.NotifyStatusId;
                    NotificationHandler.Title = h.Title;
                    NotificationHandler.Message = h.Message;
                    NotificationHandler.ScreenId = h.ScreenId;
                    NotificationHandler.NotifyTo = h.NotifyTo;
                    if (ApplicationWikiLineData.NotifyEndDate == null || ApplicationWikiLineData.NotifyEndDate.Value.Date >= NotificationHandler.NextNotifyDate.Value.Date)
                    {
                        _context.NotificationHandler.Add(NotificationHandler);

                    }
                });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetNotificationHandlers")]
        public List<NotificationHandlerModel> GetNotificationHandlers(int id)
        {
            List<NotificationHandlerModel> notificationHandlerModels = new List<NotificationHandlerModel>();
            var notificationHandlers = _context.NotificationHandler.Where(w => w.NotifyTo.Value == id && w.NotifyStatusId == 1).ToList();
            if (notificationHandlers != null)
            {
                notificationHandlers.ForEach(h =>
                {
                    NotificationHandlerModel notificationHandlerModel = new NotificationHandlerModel();
                    notificationHandlerModel.NextNotifyDate = h.NextNotifyDate;
                    notificationHandlerModel.SessionId = h.SessionId;
                    notificationHandlerModel.NotifyStatusId = h.NotifyStatusId;
                    notificationHandlerModel.AddedDays = h.AddedDays;
                    notificationHandlerModel.Title = h.Title;
                    notificationHandlerModel.Message = h.Message;
                    notificationHandlerModel.AddedDays = h.AddedDays;
                    notificationHandlerModel.ScreenId = h.ScreenId;
                    notificationHandlerModel.NotifyTo = h.NotifyTo;
                    notificationHandlerModels.Add(notificationHandlerModel);
                });
            }

            return notificationHandlerModels;
        }
        [HttpGet]
        [Route("GetNotificationList")]
        public SpNotificationList GetNotificationList(int id)
        {
            List<SpNotificationList> spNotificationLists = new List<SpNotificationList>();
            SpNotificationList spNotificationList = new SpNotificationList();
            try
            {
                NotifyDocumentController itemClassificationController = new NotifyDocumentController(_context, _mapper,_mailService, _hostingEnvironment);
                DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                dashboardNotificationList.Title = "Received Notify Document";
                dashboardNotificationList.Count = itemClassificationController.GetNotifyDocumentByUser(id,"").Where(w=>w.CssClass=="false").Count();
                spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                if (dashboardNotificationList.Count > 0)
                {
                    if (spNotificationList.TotalCount > 0)
                    {
                        spNotificationList.TotalCount = spNotificationList.TotalCount + dashboardNotificationList.Count;
                    }
                    else
                    {
                        spNotificationList.TotalCount = dashboardNotificationList.Count;
                    }
                }
                DashboardNotificationList ccUserNotify = new DashboardNotificationList();
                ccUserNotify.Title = "CC User Notify Document";
                ccUserNotify.Count = itemClassificationController.GetNotifyDocumentByCCUser(id).Where(w => w.CssClass == "false").Count();
                spNotificationList.DashboardNotificationLists.Add(ccUserNotify);
                if (ccUserNotify.Count > 0)
                {
                    if (spNotificationList.TotalCount > 0)
                    {
                        spNotificationList.TotalCount = spNotificationList.TotalCount + ccUserNotify.Count;
                    }
                    else
                    {
                        spNotificationList.TotalCount = ccUserNotify.Count;
                    }
                }
                CompanyCalendarLineController companyCalendarLine = new CompanyCalendarLineController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);

                DashboardNotificationList calendarDocumentList = new DashboardNotificationList();
                calendarDocumentList.Title = "Calendar Document";
                calendarDocumentList.Count = companyCalendarLine.GetCalandarNotesUsers(id).Where(w=>w.IsRead==false).Count();
                spNotificationList.DashboardNotificationLists.Add(calendarDocumentList);
                if (calendarDocumentList.Count > 0)
                {
                    if (spNotificationList.TotalCount > 0)
                    {
                        spNotificationList.TotalCount = spNotificationList.TotalCount + calendarDocumentList.Count;
                    }
                    else
                    {
                        spNotificationList.TotalCount = calendarDocumentList.Count;
                    }
                }
                DashboardNotificationList unReadSubjectList = new DashboardNotificationList();
                unReadSubjectList.Title = "UnRead Subject List";
                unReadSubjectList.Count = GetUnReadSubjectListCount(id);
                spNotificationList.DashboardNotificationLists.Add(unReadSubjectList);
                if (unReadSubjectList.Count > 0)
                {
                    if (spNotificationList.TotalCount > 0)
                    {
                        spNotificationList.TotalCount = spNotificationList.TotalCount + unReadSubjectList.Count;
                    }
                    else
                    {
                        spNotificationList.TotalCount = unReadSubjectList.Count;
                    }
                }
                // Settings.  
                SqlParameter roleParam = new SqlParameter("@userId", id);

                // Processing.  
               /* string sqlQuery = "EXEC [dbo].[DashboardNotification] @userId";
               
                var spNotification = _context.Set<SpNotificationList>().FromSqlRaw(sqlQuery, roleParam).AsNoTracking().ToListAsync();
                spNotification.Result.ToList().ForEach(s =>
                {
                    
                    if(s.RequestPerson!=null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Request For Persion";
                        dashboardNotificationList.Count = s.RequestPerson;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.RequestPerson > 0)
                        {
                            //spNotificationList.TotalCount = s.RequestPerson;
                        }
                    }
                    if (s.RequestAC != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Request AC List";
                        dashboardNotificationList.Count = s.RequestAC;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.RequestAC > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                //spNotificationList.TotalCount = spNotificationList.TotalCount + s.RequestAC;
                            }
                            else
                            {
                                //spNotificationList.TotalCount =  s.RequestAC;
                            }
                        }
                    }                
                    if (s.ItemCrossReferenceReport != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Cross Reference Report";
                        dashboardNotificationList.Count = s.ItemCrossReferenceReport;
                       // spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.ItemCrossReferenceReport > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                               // spNotificationList.TotalCount = spNotificationList.TotalCount + s.ItemCrossReferenceReport;
                            }
                            else
                            {
                                //spNotificationList.TotalCount = s.ItemCrossReferenceReport;
                            }
                        }
                    }
                    if (s.WorkUnread != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Un-Read Work Order";
                        dashboardNotificationList.Count = s.WorkUnread;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.WorkUnread > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                //spNotificationList.TotalCount = spNotificationList.TotalCount + s.WorkUnread;
                            }
                            else
                            {
                                //spNotificationList.TotalCount = s.WorkUnread;
                            }
                        }
                    }
                    if (s.AssignedWorkOrderUnread != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Assigned Work Order Message";
                        dashboardNotificationList.Count = s.AssignedWorkOrderUnread;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.AssignedWorkOrderUnread > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                //spNotificationList.TotalCount = spNotificationList.TotalCount + s.AssignedWorkOrderUnread;
                            }
                            else
                            {
                                //spNotificationList.TotalCount = s.AssignedWorkOrderUnread;
                            }
                        }
                    }

                    if (s.SendNotifyDoc != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Send Notify Document";
                        dashboardNotificationList.Count = s.SendNotifyDoc;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.SendNotifyDoc > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                //spNotificationList.TotalCount = spNotificationList.TotalCount + s.SendNotifyDoc;
                            }
                            else
                            {
                                //spNotificationList.TotalCount = s.SendNotifyDoc;
                            }
                        }
                    }

                    if (s.ReceiveNotifyDoc != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Received Notify Document";
                        dashboardNotificationList.Count = s.ReceiveNotifyDoc;
                        spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.ReceiveNotifyDoc > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                spNotificationList.TotalCount = spNotificationList.TotalCount + s.ReceiveNotifyDoc;
                            }
                            else
                            {
                                spNotificationList.TotalCount = s.ReceiveNotifyDoc;
                            }
                        }
                    }
                    if (s.CCUserNotify != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "CC User Notify Document";
                        dashboardNotificationList.Count = s.CCUserNotify;
                        spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.CCUserNotify > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                spNotificationList.TotalCount = spNotificationList.TotalCount + s.CCUserNotify;
                            }
                            else
                            {
                                spNotificationList.TotalCount = s.CCUserNotify;
                            }
                        }
                    }
                    if (s.CalendarPrint != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Calendar Print Document";
                        dashboardNotificationList.Count = s.CalendarPrint;
                       // spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.CalendarPrint > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                               // spNotificationList.TotalCount = spNotificationList.TotalCount + s.CalendarPrint;
                            }
                            else
                            {
                               // spNotificationList.TotalCount = s.CalendarPrint;
                            }
                        }
                    }
                    




                    if (s.SharedDocument != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Shared Document";
                        dashboardNotificationList.Count = s.SharedDocument;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.SharedDocument > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                               // spNotificationList.TotalCount = spNotificationList.TotalCount + s.SharedDocument;
                            }
                            else
                            {
                               // spNotificationList.TotalCount = s.SharedDocument;
                            }
                        }
                    }
                    if (s.CalendarDocument != null)
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Calendar Document";
                        dashboardNotificationList.Count = s.CalendarDocument;
                        spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.CalendarDocument > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                                spNotificationList.TotalCount = spNotificationList.TotalCount + s.CalendarDocument;
                            }
                            else
                            {
                                spNotificationList.TotalCount = s.CalendarDocument;
                            }
                        }
                    }
                    if (s.Template != null )
                    {
                        DashboardNotificationList dashboardNotificationList = new DashboardNotificationList();
                        dashboardNotificationList.Title = "Template PIC";
                        dashboardNotificationList.Count = s.Template;
                        //spNotificationList.DashboardNotificationLists.Add(dashboardNotificationList);
                        if (s.Template > 0)
                        {
                            if (spNotificationList.TotalCount > 0)
                            {
                               // spNotificationList.TotalCount = spNotificationList.TotalCount + s.Template;
                            }
                            else
                            {
                               // spNotificationList.TotalCount = s.Template;
                            }
                        }
                    }                  

                });*/
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return spNotificationList;
            }

        [HttpGet]
        [Route("GetUnReadSubjectListCount")]
        public int GetUnReadSubjectListCount(int id)
        {
           
            var taskIds = new List<long>();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetUnReadTaskComment";

            var result = _context.Set<ViewGetUnReadTaskComment>().FromSqlRaw(sqlQuery).AsQueryable();
            var taskCommentUser = _context.TaskCommentUser.Select(s => new
            {
                s.TaskCommentId,
                s.TaskCommentUserId,
                s.TaskMasterId,
                s.UserId,
                s.IsRead,
                s.IsPin,
                s.IsClosed,
                s.IsAssignedTo,
                s.PinnedBy,
                s.DueDate,

            }).Where(d => d.IsClosed == false || d.IsClosed == null).AsNoTracking().ToList();
            var query = taskCommentUser?.Where(t => t.UserId == id).ToList();
            var alltaskcommentUser = taskCommentUser?.Where(t => t.UserId != id).ToList();
            var taskCommentIds = new List<long>();
            var parentCommentIds = new List<long?>();
            List<long> filtertaskcommentIds = new List<long>();
            var assignedCommentIds = new List<long>();
            var assignedUserCommentIds = new List<long>();
            var commentIds = taskCommentUser?.Select(s => s.TaskCommentId).Distinct().ToList();
            var dashItems = new List<TaskCommentModel>();


            taskCommentIds.AddRange(query.Where(t => (t.UserId == id && t.IsClosed != true) || (t.IsPin == true && t.PinnedBy == id)).Select(t => t.TaskCommentId).ToList());

            var taskComment = result.
                   Select(s => new
                   {
                       s.TaskCommentId,
                       s.TaskMasterId,
                       s.CommentedBy,
                       s.CommentedByName,
                       s.ParentCommentId,
                       s.Comment,
                       s.IsNoDueDate,
                       s.MainTaskId,
                       s.TaskSubject,
                       s.CommentedDate,
                       s.TaskDueDate,
                       s.TaskName,
                       s.SubjectDueDate,
                       s.LastCommentedDate
                   }).Where(t => taskCommentIds.Distinct().Contains(t.TaskCommentId) && (t.CommentedBy != id));

            var taskCommentId = taskComment.Select(s => s.TaskCommentId).ToList();
            var CommentedByIds = taskComment.Select(s => s.CommentedBy).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => CommentedByIds.Contains(w.UserId)).AsNoTracking().ToList();

            var ParentCommentIds = taskComment.Select(s => s.ParentCommentId.GetValueOrDefault(1)).ToList();
            var ParentComment = _context.TaskComment.Select(s => new
            {
                s.TaskCommentId,
                s.TaskSubject,
            }).Where(w => ParentCommentIds.Contains(w.TaskCommentId)).ToList();
            List<long> taskId = new List<long>();
            taskId = _context.TaskAssigned.Where(t => (t.TaskOwnerId ==id && t.IsRead == false)).Select(s => s.TaskId.Value).Distinct().ToList();
            taskIds.AddRange(taskId);
            taskId = new List<long>();
            taskId = _context.TaskMembers.Where(t => t.AssignedCc == id && t.IsRead == false).Select(s => s.TaskId.Value).ToList();
            taskIds.AddRange(taskId);
            taskIds = new List<long>();
            taskId = _context.TaskMaster.Where(t => (t.OnBehalf == id || t.OwnerId == id || (taskIds.Contains(t.TaskId))) && (t.IsEmail == true)).Select(s => s.TaskId).ToList();
            taskIds.AddRange(taskId);

            var taskMasterId = taskComment.Select(s => s.TaskMasterId.GetValueOrDefault(-1)).ToList();
            var taskmaster = _context.TaskMaster.Select(s => new
            {
                s.TaskId,
                s.Title,
                s.DueDate,
                s.IsNoDueDate,
            }).Where(w => taskMasterId.Contains(w.TaskId)).ToList();
            //List<long> taskIds = taskComment.Select(t => t.TaskMasterId.Value).Distinct().ToList();
            bool isAssigned = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.UserId == id);
            bool isOnBehalf = false;
            bool isAssignedCC = false;
            if (!isAssigned)
            {
                isOnBehalf = _context.TaskAssigned.Any(t => taskIds.Contains(t.TaskId.Value) && t.OnBehalfId == id);
            }
            if (!isOnBehalf)
            {
                isAssignedCC = _context.TaskMembers.Any(t => taskIds.Contains(t.TaskId.Value) && t.AssignedCc == id);
            }
            // List<TaskComment> taskCommentItems = taskComments.ToList();
            var selectTaskCommentIds = taskComment.Where(s => s.ParentCommentId != null).Select(s => s.TaskCommentId).Distinct().ToList();
            var selcommentIds = taskComment.GroupBy(d => new { d.ParentCommentId, d.TaskCommentId }).Select(s => new TaskCommentModel
            {
                TaskCommentID = s.Key.TaskCommentId,
                ParentCommentId = s.Key.ParentCommentId,

            });
            if (taskCommentIds.Any())
            {

                var mainComments = taskComment?.Where(d => d.ParentCommentId == null).ToList();
                filtertaskcommentIds = mainComments.Select(s => s.TaskCommentId).Distinct().ToList();
                var subCommentItems = taskComment?.Where(d => d.ParentCommentId != null).ToList();
                subCommentItems.ForEach(d =>
                {
                    if (!mainComments.Exists(m => m.ParentCommentId == d.ParentCommentId))
                    {
                        var subcomment = subCommentItems?.Where(s => s.ParentCommentId == d.ParentCommentId).Select(s => s.TaskCommentId).ToList();
                        if (subcomment != null && subcomment.Count > 0)
                        {
                            if (subcomment.Count > 1)
                            {
                                var selsubcomment = subcomment.LastOrDefault();

                                var existingmain = mainComments.Where(m => m.TaskCommentId == d.ParentCommentId).FirstOrDefault();
                                if (existingmain == null)
                                {
                                    //mainComments.ToList().Add(selsubcomment);
                                    filtertaskcommentIds.Add(selsubcomment);
                                }

                            }

                        }
                        else
                        {
                            var existingmain = mainComments.Where(m => m.TaskCommentId == d.ParentCommentId).FirstOrDefault();
                            if (existingmain == null)
                            {
                                //mainComments.ToList().AddRange(subcomment);
                                filtertaskcommentIds.AddRange(subcomment);
                            }


                        }
                    }

                });
            }
            var filterIds = filtertaskcommentIds.Distinct().ToList();
            var selecttaskComment = taskComment.
                   Select(s => new
                   {
                       s.TaskCommentId,
                       s.TaskMasterId,
                       s.CommentedBy,
                       s.CommentedByName,
                       s.ParentCommentId,
                       s.Comment,
                       s.IsNoDueDate,
                       s.MainTaskId,
                       s.TaskSubject,
                       s.CommentedDate,
                       AddedDate = s.CommentedDate,
                       s.TaskDueDate,
                       s.TaskName,
                       s.SubjectDueDate,
                       s.LastCommentedDate,
                   }).Where(t => filterIds.Contains(t.TaskCommentId)).ToList();
                  
            if (selecttaskComment != null)
            {
                selecttaskComment.ToList().ForEach(s =>
                {
                    var parentTaskSubject = ParentComment.FirstOrDefault(f => f.TaskCommentId == s.ParentCommentId)?.TaskSubject;
                    TaskCommentModel taskCommentModel = new TaskCommentModel();
                    if (!dashItems.Any(d => d.TaskCommentID == s.TaskCommentId))
                    {
                        taskCommentModel.TaskSubject = parentTaskSubject != null ? parentTaskSubject : s.TaskSubject;
                        taskCommentModel.TaskMasterID = s.TaskMasterId;
                        taskCommentModel.TaskCommentID = s.TaskCommentId;
                        // taskCommentModel.Comment = s.Comment;
                        taskCommentModel.MainTaskId = s.MainTaskId;
                        taskCommentModel.TaskName = s.TaskName;
                        taskCommentModel.TaskDueDate = s.TaskDueDate;
                        taskCommentModel.IsNoDueDate = taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId.Value)?.IsNoDueDate;
                        // taskCommentModel.SubjectDueDate = s.ParentCommentId != null ?taskCommentUser !=null? taskCommentUser.Where(t => t.TaskCommentId == s.ParentCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault(): null : taskCommentUser!=null? taskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault(): null;
                        //taskCommentModel.SubjectDueDate = taskCommentUser!=null? taskCommentUser.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.UserId == id).Select(t => t.DueDate).FirstOrDefault() :null;
                        taskCommentModel.CommentedByName = s.CommentedByName;
                        taskCommentModel.CommentedBy = s.CommentedBy;
                        taskCommentModel.ParentCommentId = s.ParentCommentId;
                        taskCommentModel.LastCommentedDate = s.CommentedDate;
                        //taskCommentModel.LastCommentedDate = taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault() != null ? taskComments.Where(t => t.TaskMasterId == s.TaskMasterId).LastOrDefault().CommentedDate : null;
                        taskCommentModel.IsPin = taskCommentUser?.Where(t => t.TaskCommentId == s.TaskCommentId && t.TaskMasterId == s.TaskMasterId && t.IsPin == true && t.PinnedBy == id).Select(t => t.IsPin).FirstOrDefault();
                        taskCommentModel.AssignedType = isAssigned ? "Assigned" : isOnBehalf ? "OnBehalf" : isAssignedCC ? "AssignedCC" : "Created";
                        dashItems.Add(taskCommentModel);
                    }

                });
            }
            return dashItems.Count;

        }
    }
}
