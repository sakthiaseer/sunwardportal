﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApptransferOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public ApptransferOrderController(CRT_TMSContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApptransferOrders")]
        public List<APPTransferOrderModel> Get()
        {
            List<APPTransferOrderModel> aPPTransferOrderModels = new List<APPTransferOrderModel>();
            List<APPTransferOrderModel> consumptionResults = new List<APPTransferOrderModel>();
            var ApptransferOrder = _context.AppconsumptionEntry.Include(s => s.AddedByUser).Include("ModifiedByUser").Where(s => s.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).AsNoTracking().ToList();

            ApptransferOrder.ForEach(s =>
            {
                APPTransferOrderModel aPPTransferOrderModel = new APPTransferOrderModel()
                {
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    TransferFrom = s.TransferFrom,
                    TransferFromID = s.TransferFromId.Value,
                    TransferTo = s.TransferTo,
                    TransferToID = s.TransferToId.Value,
                    ProdOrderNo = s.ProdOrderNo,
                    ReplanRefNo = s.ReplanRefNo,
                    SubLotNo = s.SubLotNo == "0" ? "1" : s.SubLotNo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Description = s.Description,
                    IsNewEntry = s.IsNewEntry

                };
                aPPTransferOrderModels.Add(aPPTransferOrderModel);
            });
            var consumptionIds = aPPTransferOrderModels.Select(s => s.ConsumptionEntryID).ToList();
            var APPTransferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).OrderByDescending(o => o.ConsumptionLineId)
                .Where(t => consumptionIds.Contains(t.ConsumptionEntryId.Value)).AsNoTracking().AsQueryable();
         
                aPPTransferOrderModels.ForEach(f =>
                {

                    //var consumLines = APPTransferOrderLines.Where(c => c.ConsumptionEntryId == f.ConsumptionEntryID).Select(s =>
                    //     new
                    //     {
                    //         ProdLineNo = s.ProdLineNo.GetValueOrDefault(10000),
                    //         lineNo = s.ProdLineNo.Value/10000,
                    //         s.AddedUser.UserName,
                    //         s.AddedDate,
                    //         s.ConsumptionEntryId
                    //     }).ToList();


                    int sublotNumber = 0;
                    if (!int.TryParse(f.SubLotNo, out sublotNumber))
                    {
                        sublotNumber = 1;
                    }
                    for (int i = 0; i < sublotNumber; i++)
                    {
                        var sublotNo = (i + 1);
                        sublotNumber = i + 1;
                        //var consumline = consumLines.FirstOrDefault(l => l.lineNo == sublotNo);
                        var consump = new APPTransferOrderModel
                        {
                            ConsumptionEntryID = f.ConsumptionEntryID,
                            TransferFrom = f.TransferFrom,
                            TransferFromID = f.TransferFromID,
                            TransferTo = f.TransferTo,
                            TransferToID = f.TransferToID,
                            ProdOrderNo = f.ProdOrderNo,
                            ReplanRefNo = f.ReplanRefNo,
                            StatusCodeID = f.StatusCodeID,
                            AddedByUserID = f.AddedByUserID,
                            ModifiedByUserID = f.ModifiedByUserID,
                            AddedByUser = f.AddedByUser,
                            ModifiedByUser = f.ModifiedByUser,
                            AddedDate = f.AddedDate,
                            ModifiedDate = f.ModifiedDate,
                            Description = f.Description,
                            IsNewEntry = f.IsNewEntry,
                            SubLotNo = sublotNumber.ToString(),
                            LineNo = sublotNo,
                        };
                        //if (APPTransferOrderLines.Where(a=>a.ConsumptionEntryId == f.ConsumptionEntryID  && a.PostedtoNav == true).Any())
                        //{
                            consumptionResults.Add(consump);
                        //}
                    }
                });
            
            return consumptionResults;
        }

        /// <summary>
        /// Returns first part of number.
        /// </summary>
        /// <param name="number">Initial number</param>
        /// <param name="N">Amount of digits required</param>
        /// <returns>First part of number</returns>
        public static int? takeNDigits(int number, int N)
        {
            // this is for handling negative numbers, we are only insterested in postitve number
            number = Math.Abs(number);

            // special case for 0 as Log of 0 would be infinity
            if (number == 0)
                return number;

            // getting number of digits on this input number
            int numberOfDigits = (int)Math.Floor(Math.Log10(number) + 1);

            // check if input number has more digits than the required get first N digits
            if (numberOfDigits >= N)
                return (int)Math.Truncate((number / Math.Pow(10, numberOfDigits - N)));
            else
                return number;
        }
        //Changes Done by Aravinth Start

        [HttpPost]
        [Route("GetConsumptionEntryFilter")]
        public List<APPTransferOrderModel> GetFilter(APPTransferOrderSearchModel value)
        {
            var apptransferOrder = _context.AppconsumptionEntry.Include(c=>c.AppconsumptionLines).Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(n => n.IsNewEntry == true);
            if (value.CompanyID > 0)
            {
                var Company = value.CompanyID == 1 ? "NAV_JB" : value.CompanyID == 2 ? "NAV_SG" : "";
                apptransferOrder = apptransferOrder.Where(w => w.Company == Company);
            }
            if (value.ProdOrderNo.Any())
            {
                apptransferOrder = apptransferOrder.Where(p => p.ProdOrderNo != null ? (p.ProdOrderNo.ToLower() == (value.ProdOrderNo.ToLower())) : false);
            }
            if (value.Description.Any())
            {
                apptransferOrder = apptransferOrder.Where(p => p.Description != null ? (p.Description.ToLower() == (value.Description.ToLower())) : false);
            }
            if (value.ReplanRefNo.Any())
            {
                apptransferOrder = apptransferOrder.Where(p => p.ReplanRefNo != null ? (p.ReplanRefNo.ToLower() == (value.ReplanRefNo.ToLower())) : false);
            }
            if (value.FromDate != null)
            {
                apptransferOrder = apptransferOrder.Where(p => p.AddedDate.Value.Date >= value.FromDate.Value.Date);
            }
            if (value.ToDate != null)
            {
                apptransferOrder = apptransferOrder.Where(p => p.AddedDate.Value.Date <= value.ToDate.Value.Date);
            }
            var apptransferOrders = apptransferOrder.OrderByDescending(o => o.ConsumptionEntryId).AsNoTracking().ToList();
            List<APPTransferOrderModel> filteredItems = new List<APPTransferOrderModel>();
            apptransferOrders.ForEach(s=>
            {
                APPTransferOrderModel aPPTransferOrderModel = new APPTransferOrderModel()
                {
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    TransferFrom = s.TransferFrom,
                    TransferTo = s.TransferTo,
                    ProdOrderNo = s.ProdOrderNo,
                    ReplanRefNo = s.ReplanRefNo,
                    SubLotNo = s.SubLotNo == "0" ? "1" : s.SubLotNo,
                    Description = s.Description,
                    IsNewEntry = s.IsNewEntry,
                    AddedDate = s.AddedDate,
                    Company = s.Company
                };
                filteredItems.Add(aPPTransferOrderModel);
            });
            return filteredItems;
        }

        //Changes Done by Aravinth End


        [HttpGet]
        [Route("GetApptransferOrder")]
        public List<APPTransferOrderModel> GetICT(int id)
        {
            var apptransferOrder = _context.AppconsumptionEntry.Include("AddedByUser").Include("ModifiedByUser").Select(s => new APPTransferOrderModel
            {
                TransferFrom = s.TransferFrom,
                TransferFromID = s.TransferFromId.Value,
                TransferTo = s.TransferTo,
                ConsumptionEntryID = s.TransferToId.Value,
                ProdOrderNo = s.ProdOrderNo,
                ReplanRefNo = s.ReplanRefNo,
                SubLotNo = s.SubLotNo == "0" ? "1" : s.SubLotNo,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                Description = s.Description,

            }).OrderByDescending(o => o.ConsumptionEntryID).Where(t => t.ConsumptionEntryID == id && t.IsNewEntry == true).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ApptransferOrderModel>>(ApptransferOrder);
            return apptransferOrder;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPTransferOrderModel> GetData(SearchModel searchModel)
        {
            var apptransferOrder = new AppconsumptionEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).LastOrDefault();
                        break;
                    case "Previous":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderBy(o => o.ConsumptionEntryId).FirstOrDefault(s => s.ConsumptionEntryId > searchModel.Id);
                        break;
                    case "Previous":
                        apptransferOrder = _context.AppconsumptionEntry.Where(c => c.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).FirstOrDefault(s => s.ConsumptionEntryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPTransferOrderModel>(apptransferOrder);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppConsumption")]
        public async Task<APPTransferOrderModel> Post(APPTransferOrderModel value)
        {
            var transferOrder = _context.AppconsumptionEntry.SingleOrDefault(p => p.ProdOrderNo == value.ProdOrderNo);
            if (transferOrder == null)
            {
                var sublotNo = value.SubLotNo;
                var description = "";
                try
                {
                    var context = new DataService(_config, value.CompanyName);
                    var nquery = context.Context.ProductionOrder.Where(r => r.No == value.ProdOrderNo);
                    DataServiceQuery<NAV.ProductionOrder> query = (DataServiceQuery<NAV.ProductionOrder>)nquery;

                    TaskFactory<IEnumerable<NAV.ProductionOrder>> taskFactory = new TaskFactory<IEnumerable<NAV.ProductionOrder>>();
                    IEnumerable<NAV.ProductionOrder> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                    var productionOrder = result.FirstOrDefault();
                    if (productionOrder != null)
                    {
                        sublotNo = productionOrder.Sub_Lot.ToString();
                        description = productionOrder.Description;
                    }
                }
                catch { }
                transferOrder = new AppconsumptionEntry
                {
                    TransferFrom = value.TransferFrom,
                    TransferFromId = value.TransferFromID,
                    TransferTo = value.TransferTo,
                    TransferToId = value.TransferToID,
                    ProdOrderNo = value.ProdOrderNo,
                    ReplanRefNo = value.ReplanRefNo,
                    SubLotNo = sublotNo,
                    Description = description,
                    StatusCodeId = 1,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    Company = value.CompanyName,
                    IsNewEntry = true
                };
                _context.AppconsumptionEntry.Add(transferOrder);
            }
            else
            {
                transferOrder.TransferFrom = value.TransferFrom;
                transferOrder.TransferFromId = value.TransferFromID;
                transferOrder.TransferTo = value.TransferTo;
                transferOrder.TransferToId = value.TransferToID;
                transferOrder.ReplanRefNo = value.ReplanRefNo;
                transferOrder.ModifiedByUserId = value.AddedByUserID;
                transferOrder.ModifiedDate = DateTime.Now;
                transferOrder.StatusCodeId = 1;
                transferOrder.IsNewEntry = true;
            }
            _context.SaveChanges();

            value.ConsumptionEntryID = transferOrder.ConsumptionEntryId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApptransferOrder")]
        public APPTransferOrderModel Put(APPTransferOrderModel value)
        {
            var transferOrder = _context.AppconsumptionEntry.SingleOrDefault(p => p.ConsumptionEntryId == value.ConsumptionEntryID);
            transferOrder.TransferFrom = value.TransferFrom;
            transferOrder.TransferFromId = value.TransferFromID;
            transferOrder.TransferTo = value.TransferTo;
            transferOrder.TransferToId = value.TransferToID;
            transferOrder.ProdOrderNo = value.ProdOrderNo;
            transferOrder.ReplanRefNo = value.ReplanRefNo;
            //transferOrder.SubLotNo = value.SubLotNo;
            //ApptransferOrder.ApptransferOrderId = value.ApptransferOrderID;
            transferOrder.ModifiedByUserId = value.ModifiedByUserID;
            transferOrder.ModifiedDate = DateTime.Now;

            transferOrder.ModifiedByUserId = value.ModifiedByUserID;
            transferOrder.ModifiedDate = value.ModifiedDate;
            transferOrder.StatusCodeId = value.StatusCodeID;
            transferOrder.AddedByUserId = value.AddedByUserID;
            transferOrder.AddedDate = value.AddedDate;
            _context.SaveChanges();



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApptransferOrder")]
        public void Delete(int id)
        {
            var transferOrder = _context.AppconsumptionEntry.SingleOrDefault(p => p.ConsumptionEntryId == id);
            var errorMessage = "";
            try
            {
                if (transferOrder != null)
                {
                    _context.AppconsumptionEntry.Remove(transferOrder);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        [HttpPost]
        [Route("GetByCompany")]
        public List<AppDashboardModel> GetByCompany(SearchModel searchModel)
        {
            var appconsumptionEntries = _context.AppconsumptionEntry
             .Where(t => t.AddedDate.Value.Date == searchModel.Date.Value.Date).AsNoTracking().ToList();

            var dashItems = new List<AppDashboardModel>();

            var item = new AppDashboardModel
            {
                DashboardType = "Malaysia",
                Name = "Company",
                TotalCount = appconsumptionEntries.Where(c => c.Company == "NAV_JB").Count().ToString(),
            };
            dashItems.Add(item);

            item = new AppDashboardModel
            {
                DashboardType = "Singapore",
                Name = "Company",
                TotalCount = appconsumptionEntries.Where(c => c.Company == "NAV_SG").Count().ToString(),
            };
            dashItems.Add(item);

            return dashItems;
        }

        [HttpPost]
        [Route("GetConsumptionEntryByCompany")]
        public List<APPTransferOrderModel> GetConsumptionEntryByCompany(SearchModel searchModel)
        {
            List<APPTransferOrderModel> aPPTransferOrderModels = new List<APPTransferOrderModel>();
            var appconsumptionEntries = _context.AppconsumptionEntry.Where(t => t.AddedDate.Value.Date >= searchModel.Date.Value.Date && t.IsNewEntry == true).ToList();
            appconsumptionEntries.ForEach(s =>
            {
                var aPPTransferOrderModel = new APPTransferOrderModel
                {
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    ProdOrderNo = s.ProdOrderNo,
                    ReplanRefNo = s.ReplanRefNo,
                    SubLotNo = s.SubLotNo == "0" ? "1" : s.SubLotNo,
                    Description = s.Description,
                };
                aPPTransferOrderModels.Add(aPPTransferOrderModel);
            });

            return aPPTransferOrderModels;
        }

        // GET: api/Project
        [HttpPost]
        [Route("GetConsumptionInfo")]
        public List<APPTransferOrderModel> GetConsumptionInfo(SearchModel searchModel)
        {
            List<APPTransferOrderModel> aPPTransferOrderModels = new List<APPTransferOrderModel>();
            var ApptransferOrder = _context.AppconsumptionEntry.Include(o => o.AppconsumptionLines).Where(t => t.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).AsNoTracking().ToList();

            if (!string.IsNullOrEmpty(searchModel.SearchString))
            {
                ApptransferOrder = ApptransferOrder.Where(c => c.ProdOrderNo.Contains(searchModel.SearchString)).ToList();
            }
            ApptransferOrder.ForEach(s =>
            {
                APPTransferOrderModel aPPTransferOrderModel = new APPTransferOrderModel()
                {
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    ProdOrderNo = s.ProdOrderNo,
                    LineCount = s.AppconsumptionLines.Count
                };
                aPPTransferOrderModels.Add(aPPTransferOrderModel);
            });

            return aPPTransferOrderModels;
        }
        [HttpPost]
        [Route("GetConsumptionUnScannedItems")]
        public List<APPTransferOrderModel> GetUnScannedItems(SearchModel searchModel)
        {
            List<APPTransferOrderModel> aPPTransferOrderModels = new List<APPTransferOrderModel>();
            List<APPTransferOrderModel> consumptionResults = new List<APPTransferOrderModel>();
            var ApptransferOrder = _context.AppconsumptionEntry.Include("ModifiedByUser").Where(s => s.IsNewEntry == true).OrderByDescending(o => o.ConsumptionEntryId).AsNoTracking().ToList();

            ApptransferOrder.ForEach(s =>
            {
                APPTransferOrderModel aPPTransferOrderModel = new APPTransferOrderModel()
                {
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    TransferFrom = s.TransferFrom,
                    TransferFromID = s.TransferFromId.Value,
                    TransferTo = s.TransferTo,
                    TransferToID = s.TransferToId.Value,
                    ProdOrderNo = s.ProdOrderNo,
                    ReplanRefNo = s.ReplanRefNo,
                    SubLotNo = s.SubLotNo == "0" ? "1" : s.SubLotNo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Description = s.Description,
                    IsNewEntry = s.IsNewEntry

                };
                aPPTransferOrderModels.Add(aPPTransferOrderModel);
            });
            var consumptionIds = aPPTransferOrderModels.Select(s => s.ConsumptionEntryID).ToList();
            var APPTransferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).OrderByDescending(o => o.ConsumptionLineId)
                .Where(t => consumptionIds.Contains(t.ConsumptionEntryId.Value)).AsNoTracking().AsQueryable();
            aPPTransferOrderModels.ForEach(f =>
            {

                var consumLines = APPTransferOrderLines.Where(c => c.ConsumptionEntryId == f.ConsumptionEntryID).Select(s =>
                     new
                     {
                         ProdLineNo = s.ProdLineNo.GetValueOrDefault(10000),
                         lineNo = takeNDigits(s.ProdLineNo.Value, 1),
                         s.AddedUser.UserName,
                         s.AddedDate,
                         s.ConsumptionEntryId
                     }).ToList();


                int sublotNumber = 0;
                if (!int.TryParse(f.SubLotNo, out sublotNumber))
                {
                    sublotNumber = 1;
                }
                for (int i = 0; i < sublotNumber; i++)
                {
                    var sublotNo = (i + 1);
                    var consumline = consumLines.FirstOrDefault(l => l.lineNo == sublotNo);
                    if (consumline == null)
                    {
                        var consump = new APPTransferOrderModel
                        {
                            ConsumptionEntryID = f.ConsumptionEntryID,
                            TransferFrom = f.TransferFrom,
                            TransferFromID = f.TransferFromID,
                            TransferTo = f.TransferTo,
                            TransferToID = f.TransferToID,
                            ProdOrderNo = f.ProdOrderNo,
                            ReplanRefNo = f.ReplanRefNo,
                            StatusCodeID = f.StatusCodeID,
                            AddedByUserID = f.AddedByUserID,
                            ModifiedByUserID = f.ModifiedByUserID,
                            AddedByUser = consumline?.UserName,
                            ModifiedByUser = f.ModifiedByUser,
                            AddedDate = consumline?.AddedDate,
                            ModifiedDate = f.ModifiedDate,
                            Description = f.Description,
                            IsNewEntry = f.IsNewEntry,
                            SubLotNo = sublotNumber.ToString(),
                            LineNo = sublotNo,
                        };
                        consumptionResults.Add(consump);
                    }
                }
            });
            return consumptionResults;
        }

    }
}