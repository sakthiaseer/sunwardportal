﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesTargetByStaffController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public SalesTargetByStaffController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSalesTargetByStaff")]
        public List<SalesTargetByStaffModel> Get()
        {
            List<SalesTargetByStaffModel> salesTargetByStaffModels = new List<SalesTargetByStaffModel>();
            var salesTargetByStaff = _context.SalesTargetByStaff.ToList();
            if (salesTargetByStaff != null)
            {
                var employeeIds = salesTargetByStaff.Select(s => s.LeadById.GetValueOrDefault(0)).ToList();
                var masterIds = salesTargetByStaff.Select(s => s.SalesGroupId.GetValueOrDefault(0)).ToList();
                masterIds.AddRange(salesTargetByStaff.Select(s => s.CompanyId.GetValueOrDefault(0)).ToList());
                var userIds = salesTargetByStaff.Select(s => s.AddedByUserId).ToList();
                var codeMasterIds = salesTargetByStaff.Select(s => s.StatusCodeId).ToList();

                userIds.AddRange(salesTargetByStaff.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var employee = _context.Employee.Select(s => new
                {
                    s.EmployeeId,
                    s.FirstName,
                    s.LastName
                }).Where(w => employeeIds.Contains(w.EmployeeId)).AsNoTracking().ToList();
                var masters = _context.ApplicationMasterDetail.Select(s => new
                {
                    s.ApplicationMasterDetailId,
                    s.Value,
                }).Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeMasterIds.Contains(w.CodeId)).AsNoTracking().ToList();
                salesTargetByStaff.ForEach(s =>
                {
                    SalesTargetByStaffModel salesTargetByStaffModel = new SalesTargetByStaffModel();
                    salesTargetByStaffModel.SalesTargetByStaffId = s.SalesTargetByStaffId;
                    salesTargetByStaffModel.CompanyId = s.CompanyId;
                    salesTargetByStaffModel.SalesGroupId = s.SalesGroupId;
                    salesTargetByStaffModel.LeadById = s.LeadById;
                    salesTargetByStaffModel.AddedByUserID = s.AddedByUserId;
                    salesTargetByStaffModel.AddedDate = s.AddedDate;
                    salesTargetByStaffModel.ModifiedDate = s.ModifiedDate;
                    salesTargetByStaffModel.ModifiedByUserID = s.ModifiedByUserId;
                    salesTargetByStaffModel.StatusCodeID = s.StatusCodeId;
                    salesTargetByStaffModel.SessionId = s.SessionId;
                    salesTargetByStaffModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.AddedByUserId)?.UserName : "";
                    salesTargetByStaffModel.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.ModifiedByUserId)?.UserName : "";
                    salesTargetByStaffModel.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : "";
                    salesTargetByStaffModel.CompanyName = masters != null ? masters.FirstOrDefault(u => u.ApplicationMasterDetailId == s.CompanyId)?.Value : "";
                    salesTargetByStaffModel.SalesGroup = masters != null ? masters.FirstOrDefault(u => u.ApplicationMasterDetailId == s.SalesGroupId)?.Value : "";
                    salesTargetByStaffModel.LeadBy = employee != null ? employee.FirstOrDefault(u => u.EmployeeId == s.LeadById)?.FirstName : "";
                    salesTargetByStaffModels.Add(salesTargetByStaffModel);
                });
            }
            return salesTargetByStaffModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SalesTargetByStaffModel> GetData(SearchModel searchModel)
        {
            var salesTargetByStaff = new SalesTargetByStaff();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).FirstOrDefault();
                        break;
                    case "Last":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).LastOrDefault();
                        break;
                    case "Next":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).LastOrDefault();
                        break;
                    case "Previous":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).FirstOrDefault();
                        break;
                    case "Last":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).LastOrDefault();
                        break;
                    case "Next":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderBy(o => o.SalesTargetByStaffId).FirstOrDefault(s => s.SalesTargetByStaffId > searchModel.Id);
                        break;
                    case "Previous":
                        salesTargetByStaff = _context.SalesTargetByStaff.OrderByDescending(o => o.SalesTargetByStaffId).FirstOrDefault(s => s.SalesTargetByStaffId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SalesTargetByStaffModel>(salesTargetByStaff);
            return result;
        }
        [HttpGet]
        [Route("GetSalesTargetByStaffLine")]
        public List<SalesTargetByStaffLineModel> GetSalesTargetByStaffLine(long? id)
        {
            List<SalesTargetByStaffLineModel> salesTargetByStaffModels = new List<SalesTargetByStaffLineModel>();
            var salesTargetByStaff = _context.SalesTargetByStaffLine.Where(w => w.SalesTargetByStaffId == id).ToList();
            if (salesTargetByStaff != null)
            {
                var salesTargetByStaffLineIds = salesTargetByStaff.Select(s => s.SalesTargetByStaffLineId).ToList();
                var employeeIds = salesTargetByStaff.Select(s => s.SalesPersonId.GetValueOrDefault(0)).ToList();
                var userIds = salesTargetByStaff.Select(s => s.AddedByUserId).ToList();
                var codeMasterIds = salesTargetByStaff.Select(s => s.StatusCodeId).ToList();
                userIds.AddRange(salesTargetByStaff.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
                var salesTargetByStaffSalesCover = _context.SalesTargetByStaffSalesCover.Select(s => new
                {
                    s.SalesTargetByStaffLineId,
                    s.SalesCoverCodeId,
                }).Where(w => salesTargetByStaffLineIds.Contains(w.SalesTargetByStaffLineId.Value)).AsNoTracking().ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var employee = _context.Employee.Select(s => new
                {
                    s.EmployeeId,
                    s.FirstName,
                    s.LastName
                }).Where(w => employeeIds.Contains(w.EmployeeId)).AsNoTracking().ToList();

                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeMasterIds.Contains(w.CodeId)).AsNoTracking().ToList();
                salesTargetByStaff.ForEach(s =>
                {
                    SalesTargetByStaffLineModel salesTargetByStaffModel = new SalesTargetByStaffLineModel();
                    salesTargetByStaffModel.SalesTargetByStaffLineId = s.SalesTargetByStaffLineId;
                    salesTargetByStaffModel.SalesTargetByStaffId = s.SalesTargetByStaffId;
                    salesTargetByStaffModel.SalesPersonId = s.SalesPersonId;
                    salesTargetByStaffModel.Target = s.Target;
                    salesTargetByStaffModel.EffectiveFrom = s.EffectiveFrom;
                    salesTargetByStaffModel.SalesPersonName = s.SalesPersonName;
                    salesTargetByStaffModel.AddedByUserID = s.AddedByUserId;
                    salesTargetByStaffModel.AddedDate = s.AddedDate;
                    salesTargetByStaffModel.ModifiedDate = s.ModifiedDate;
                    salesTargetByStaffModel.ModifiedByUserID = s.ModifiedByUserId;
                    salesTargetByStaffModel.StatusCodeID = s.StatusCodeId;
                    salesTargetByStaffModel.SessionId = s.SessionId;
                    salesTargetByStaffModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.AddedByUserId)?.UserName : "";
                    salesTargetByStaffModel.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.ModifiedByUserId)?.UserName : "";
                    salesTargetByStaffModel.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : "";
                    salesTargetByStaffModel.SalesCoverCodeIds = salesTargetByStaffSalesCover != null ? salesTargetByStaffSalesCover.Where(w => w.SalesTargetByStaffLineId == s.SalesTargetByStaffLineId).Select(s => s.SalesCoverCodeId).ToList() : new List<long?>();
                    salesTargetByStaffModels.Add(salesTargetByStaffModel);
                });
            }
            return salesTargetByStaffModels;
        }
        [HttpPost]
        [Route("InsertSalesTargetByStaff")]
        public SalesTargetByStaffModel Post(SalesTargetByStaffModel value)
        {
            var SessionId = Guid.NewGuid();
            var salesTargetByStaff = new SalesTargetByStaff
            {
                CompanyId = value.CompanyId,
                SalesGroupId = value.SalesGroupId,
                LeadById = value.LeadById,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            _context.SalesTargetByStaff.Add(salesTargetByStaff);
            _context.SaveChanges();
            value.SalesTargetByStaffId = salesTargetByStaff.SalesTargetByStaffId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSalesTargetByStaff")]
        public SalesTargetByStaffModel Put(SalesTargetByStaffModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var salesTargetByStaff = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == value.SalesTargetByStaffId);
            salesTargetByStaff.CompanyId = value.CompanyId;
            salesTargetByStaff.SalesGroupId = value.SalesGroupId;
            salesTargetByStaff.LeadById = value.LeadById;
            salesTargetByStaff.StatusCodeId = value.StatusCodeID.Value;
            salesTargetByStaff.ModifiedByUserId = value.ModifiedByUserID;
            salesTargetByStaff.ModifiedDate = DateTime.Now;
            salesTargetByStaff.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesTargetByStaff")]
        public void Delete(int id)
        {
            var salesTargetByStaff = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == id);
            if (salesTargetByStaff != null)
            {
                var salesTargetByStaffLine = _context.SalesTargetByStaffLine.Include(a => a.SalesTargetByStaffSalesCover).Where(p => p.SalesTargetByStaffId == id).ToList();
                if (salesTargetByStaffLine != null)
                {
                    _context.SalesTargetByStaffLine.RemoveRange(salesTargetByStaffLine);
                    _context.SaveChanges();
                }
                _context.SalesTargetByStaff.Remove(salesTargetByStaff);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("InsertSalesTargetByStaffLine")]
        public SalesTargetByStaffLineModel InsertSalesTargetByStaffLine(SalesTargetByStaffLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var salesTargetByStaffLine = new SalesTargetByStaffLine
            {
                SalesTargetByStaffId = value.SalesTargetByStaffId,
                SalesPersonId = value.SalesPersonId,
                SalesPersonName = value.SalesPersonName,
                Target = value.Target,
                EffectiveFrom = value.EffectiveFrom,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            if (value.SalesCoverCodeIds.Count > 0)
            {
                value.SalesCoverCodeIds.ForEach(a =>
                {
                    var salesTargetByStaffSalesCover = new SalesTargetByStaffSalesCover
                    {
                        SalesCoverCodeId = a
                    };
                    salesTargetByStaffLine.SalesTargetByStaffSalesCover.Add(salesTargetByStaffSalesCover);
                });
            }
            _context.SalesTargetByStaffLine.Add(salesTargetByStaffLine);
            _context.SaveChanges();
            value.SalesTargetByStaffLineId = salesTargetByStaffLine.SalesTargetByStaffLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesTargetByStaffLine")]
        public SalesTargetByStaffLineModel UpdateSalesTargetByStaffLine(SalesTargetByStaffLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var salesTargetByStaff = _context.SalesTargetByStaffLine.SingleOrDefault(p => p.SalesTargetByStaffLineId == value.SalesTargetByStaffLineId);
            salesTargetByStaff.SalesTargetByStaffId = value.SalesTargetByStaffId;
            salesTargetByStaff.SalesPersonName = value.SalesPersonName;
            salesTargetByStaff.SalesPersonId = value.SalesPersonId;
            salesTargetByStaff.Target = value.Target;
            salesTargetByStaff.EffectiveFrom = value.EffectiveFrom;
            salesTargetByStaff.StatusCodeId = value.StatusCodeID.Value;
            salesTargetByStaff.ModifiedByUserId = value.ModifiedByUserID;
            salesTargetByStaff.ModifiedDate = DateTime.Now;
            salesTargetByStaff.SessionId = value.SessionId;
            var salesTargetByStaffSalesCover = _context.SalesTargetByStaffSalesCover.Where(w => w.SalesTargetByStaffLineId == value.SalesTargetByStaffLineId).ToList();
            if (salesTargetByStaffSalesCover != null)
            {
                _context.SalesTargetByStaffSalesCover.RemoveRange(salesTargetByStaffSalesCover);
                _context.SaveChanges();
            }
            if (value.SalesCoverCodeIds.Count > 0)
            {
                value.SalesCoverCodeIds.ForEach(a =>
                {
                    var salesTargetByStaffSalesCover = new SalesTargetByStaffSalesCover
                    {
                        SalesCoverCodeId = a,
                        SalesTargetByStaffLineId = salesTargetByStaff.SalesTargetByStaffLineId
                    };
                    _context.SalesTargetByStaffSalesCover.Add(salesTargetByStaffSalesCover);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesTargetByStaffLine")]
        public void DeleteSalesTargetByStaffLine(int id)
        {
            var salesTargetByStaffLine = _context.SalesTargetByStaffLine.Include(s => s.SalesTargetByStaffSalesCover).SingleOrDefault(p => p.SalesTargetByStaffLineId == id);
            if (salesTargetByStaffLine != null)
            {
                _context.SalesTargetByStaffLine.Remove(salesTargetByStaffLine);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetSalesTargetByStaffVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<SalesTargetByStaffModel>>> GetSalesTargetByStaffVersion(string sessionID)
        {
            return await _repository.GetList<SalesTargetByStaffModel>(sessionID);
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<SalesTargetByStaffModel> CreateVersion(SalesTargetByStaffModel value)
        {
            value.SessionId = value.SessionId.Value;
            var salesTargetByStaff = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == value.SalesTargetByStaffId);
            if (salesTargetByStaff != null)
            {
                var verObject = _mapper.Map<SalesTargetByStaffModel>(salesTargetByStaff);
                verObject.SalesTargetByStaffLineModel = GetSalesTargetByStaffLine(value.SalesTargetByStaffId);
                var versionInfo = new TableDataVersionInfoModel<SalesTargetByStaffModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ApplicationWiki",
                    PrimaryKey = value.SalesTargetByStaffId,
                    ReferenceInfo = value.ReferenceInfo
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(SalesTargetByStaffModel value)
        {
            value.SessionId = value.SessionId.Value;
            var salesTargetByStaff = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == value.SalesTargetByStaffId);

            if (salesTargetByStaff != null)
            {
                var verObject = _mapper.Map<SalesTargetByStaffModel>(salesTargetByStaff);
                verObject.SalesTargetByStaffLineModel = GetSalesTargetByStaffLine(value.SalesTargetByStaffId);
                var versionInfo = new TableDataVersionInfoModel<SalesTargetByStaffModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ApplicationWiki",
                    PrimaryKey = value.SalesTargetByStaffId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<SalesTargetByStaffModel> ApplyVersion(SalesTargetByStaffModel value)
        {
            value.SessionId = value.SessionId.Value;
            var salesTargetByStaffModel = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == value.SalesTargetByStaffId);

            if (salesTargetByStaffModel != null)
            {
                salesTargetByStaffModel.SalesTargetByStaffId = value.SalesTargetByStaffId;
                salesTargetByStaffModel.CompanyId = value.CompanyId;
                salesTargetByStaffModel.SalesGroupId = value.SalesGroupId;
                salesTargetByStaffModel.LeadById = value.LeadById;
                salesTargetByStaffModel.AddedByUserId = value.AddedByUserID.Value;
                salesTargetByStaffModel.AddedDate = value.AddedDate.Value;
                salesTargetByStaffModel.ModifiedDate = value.ModifiedDate;
                salesTargetByStaffModel.ModifiedByUserId = value.ModifiedByUserID;
                salesTargetByStaffModel.StatusCodeId = value.StatusCodeID.Value;
                salesTargetByStaffModel.SessionId = value.SessionId;
                _context.SaveChanges();
                var salesTargetByStaffLine = _context.SalesTargetByStaffLine.Where(prc => prc.SalesTargetByStaffId == value.SalesTargetByStaffId).ToList();
                if (salesTargetByStaffLine != null)
                {
                    var salesTargetByStaffLineIds = salesTargetByStaffLine.Select(s=>s.SalesTargetByStaffLineId).ToList();
                    if(salesTargetByStaffLineIds.Count>0)
                    {
                        string joined = string.Join(",", salesTargetByStaffLineIds);
                        var Query = string.Format("Delete from SalesTargetByStaffSalesCover Where SalesTargetByStaffLineId in  (" + joined + ")");
                        _context.Database.ExecuteSqlRaw(Query);
                    }
                    _context.SalesTargetByStaffLine.RemoveRange(salesTargetByStaffLine);
                    _context.SaveChanges();
                }
                if (value.SalesTargetByStaffLineModel != null)
                {
                    value.SalesTargetByStaffLineModel.ForEach(f =>
                    {
                        var salesTargetByStaffLine = new SalesTargetByStaffLine
                        {
                            SalesTargetByStaffId = f.SalesTargetByStaffId,
                            SalesPersonId = f.SalesPersonId,
                            SalesPersonName = f.SalesPersonName,
                            Target = f.Target,
                            EffectiveFrom = f.EffectiveFrom,
                            AddedByUserId = f.AddedByUserID.Value,
                            StatusCodeId = f.StatusCodeID.Value,
                            AddedDate = DateTime.Now,
                            SessionId = f.SessionId,
                        };
                        if (f.SalesCoverCodeIds.Count > 0)
                        {
                            f.SalesCoverCodeIds.ForEach(a =>
                            {
                                var salesTargetByStaffSalesCover = new SalesTargetByStaffSalesCover
                                {
                                    SalesCoverCodeId = a
                                };
                                salesTargetByStaffLine.SalesTargetByStaffSalesCover.Add(salesTargetByStaffSalesCover);
                            });
                        }
                        _context.SalesTargetByStaffLine.Add(salesTargetByStaffLine);
                        _context.SaveChanges();
                    });
                }
            }
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<SalesTargetByStaffModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<SalesTargetByStaffModel>(tableData.JsonData);

                    var salesTargetByStaffModel = _context.SalesTargetByStaff.SingleOrDefault(p => p.SalesTargetByStaffId == verObject.SalesTargetByStaffId);
                    if (verObject != null && salesTargetByStaffModel != null)
                    {
                        salesTargetByStaffModel.SalesTargetByStaffId = verObject.SalesTargetByStaffId;
                        salesTargetByStaffModel.CompanyId = verObject.CompanyId;
                        salesTargetByStaffModel.SalesGroupId = verObject.SalesGroupId;
                        salesTargetByStaffModel.LeadById = verObject.LeadById;
                        salesTargetByStaffModel.AddedByUserId = verObject.AddedByUserID.Value;
                        salesTargetByStaffModel.AddedDate = verObject.AddedDate.Value;
                        salesTargetByStaffModel.ModifiedDate = verObject.ModifiedDate;
                        salesTargetByStaffModel.ModifiedByUserId = verObject.ModifiedByUserID;
                        salesTargetByStaffModel.StatusCodeId = verObject.StatusCodeID.Value;
                        salesTargetByStaffModel.SessionId = verObject.SessionId;
                        _context.SalesTargetByStaffLine.RemoveRange(salesTargetByStaffModel.SalesTargetByStaffLine);
                        _context.SaveChanges();
                        if (verObject.SalesTargetByStaffLineModel != null)
                        {
                            verObject.SalesTargetByStaffLineModel.ForEach(f =>
                            {
                                var salesTargetByStaffLine = new SalesTargetByStaffLine
                                {
                                    SalesTargetByStaffId = f.SalesTargetByStaffId,
                                    SalesPersonId = f.SalesPersonId,
                                    SalesPersonName = f.SalesPersonName,
                                    Target = f.Target,
                                    EffectiveFrom = f.EffectiveFrom,
                                    AddedByUserId = f.AddedByUserID.Value,
                                    StatusCodeId = f.StatusCodeID.Value,
                                    AddedDate = DateTime.Now,
                                    SessionId = f.SessionId,
                                };
                                if (f.SalesCoverCodeIds.Count > 0)
                                {
                                    f.SalesCoverCodeIds.ForEach(a =>
                                    {
                                        var salesTargetByStaffSalesCover = new SalesTargetByStaffSalesCover
                                        {
                                            SalesCoverCodeId = a
                                        };
                                        salesTargetByStaffLine.SalesTargetByStaffSalesCover.Add(salesTargetByStaffSalesCover);
                                    });
                                }
                                _context.SalesTargetByStaffLine.Add(salesTargetByStaffLine);
                                _context.SaveChanges();
                            });
                        }
                    }
                    return verObject;
                }
                return new SalesTargetByStaffModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }

        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
    }
}
