﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionForecastController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionForecastController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionForecasts")]
        public List<ProductionForecastModel> Get()
        {
            List<ProductionForecastModel> productionForecastModels = new List<ProductionForecastModel>();
            var productionForecast = _context.ProductionForecast.Include("AddedByUser").Include("MethodCode").Include("ModifiedByUser").Include("Item").OrderByDescending(o => o.ProductionForecastId).AsNoTracking().ToList();
            if(productionForecast!=null && productionForecast.Count>0)
            {
                productionForecast.ForEach(s =>
                {
                    ProductionForecastModel productionForecastModel = new ProductionForecastModel
                    {
                        ProductionForecastID = s.ProductionForecastId,
                        MethodCodeID = s.MethodCodeId,
                        ItemId = s.ItemId,
                        ItemNo = s.Item != null ? s.Item.No : "",
                        MethodCodeName = s.MethodCode.MethodName,
                        Description1 = s.Item!=null? s.Item.Description : "",
                        Description2 = s.Item != null ? s.Item.Description2 : "",
                        PackQuantity = s.PackQuantity,

                        BatchSize = s.BatchSize,
                        //Quantity = s.ProductionForecastLines.Where(al => al.ProductionForecastId == s.ProductionForecastId).Select(al => al.Quantity).SingleOrDefault(),
                        //StatusCode=s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,


                    };
                    productionForecastModels.Add(productionForecastModel);
                });
            }
           
            return productionForecastModels;
        }

        [HttpGet]
        [Route("GetProductionForecastsById")]
        public List<ProductionForecastModel> GetProductionForecasts(int id)
        {
            List<ProductionForecastModel> productionForecastModels = new List<ProductionForecastModel>();
            var productionForecast = _context.ProductionForecast.Include("AddedByUser").Include("MethodCode").Include("ModifiedByUser").Include("Item").Where(o => o.MethodCodeId == id).AsNoTracking().ToList();
            if(productionForecast!=null && productionForecast.Count>0)
            {
                productionForecast.ForEach(s =>
                {
                    ProductionForecastModel productionForecastModel = new ProductionForecastModel
                    {
                        ProductionForecastID = s.ProductionForecastId,
                        MethodCodeID = s.MethodCodeId,
                        ItemId = s.ItemId,
                        ItemNo = s.Item != null ? s.Item.No : "",
                        MethodCodeName = s.MethodCode != null ? s.MethodCode.MethodName :"",
                        Description1 = s.Item!=null? s.Item.Description : "",
                        Description2 = s.Item != null ? s.Item.Description2 : "",
                        PackQuantity = s.PackQuantity,
                        ProductionMonth = s.ProductionMonth,
                        BatchSize = s.BatchSize,
                        //Quantity = s.ProductionForecastLines.Where(al => al.ProductionForecastId == s.ProductionForecastId).Select(al => al.Quantity).SingleOrDefault(),
                        //StatusCode=s.StatusCode.CodeValue,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,

                    };
                    productionForecastModels.Add(productionForecastModel);
                });

            }          
            return productionForecastModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionForecastModel> GetData(SearchModel searchModel)
        {
            var productionForecast = new ProductionForecast();
            List<ProductionForecast> ProductionForecastlist = new List<ProductionForecast>();
            ProductionForecastModel ProductionForecastModel = new ProductionForecastModel();
            //ProductionForecastLinesModel entryLinesModel = new ProductionForecastLinesModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).FirstOrDefault();
                        break;
                    case "Last":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).LastOrDefault();
                        break;
                    case "Next":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).LastOrDefault();
                        break;
                    case "Previous":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).FirstOrDefault();
                        break;
                    case "Last":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).LastOrDefault();
                        break;
                    case "Next":
                        productionForecast = _context.ProductionForecast.OrderBy(o => o.ProductionForecastId).FirstOrDefault(s => s.ProductionForecastId > searchModel.Id);
                        break;
                    case "Previous":
                        productionForecast = _context.ProductionForecast.OrderByDescending(o => o.ProductionForecastId).FirstOrDefault(s => s.ProductionForecastId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionForecastModel>(productionForecast);

            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionForecast")]
        public ProductionForecastModel Post(ProductionForecastModel value)
        {

            var productionForecast = new ProductionForecast
            {

                MethodCodeId = value.MethodCodeID,              
                ProductionMonth = value.ProductionMonth,
                PackQuantity = value.PackQuantity,
                BatchSize = value.BatchSize,
                ItemId = value.ItemId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.ProductionForecast.Add(productionForecast);          
            _context.SaveChanges();
            value.MethodCodeName = _context.NavMethodCode.Where(m => m.MethodCodeId == productionForecast.MethodCodeId).Select(t => t.MethodName).FirstOrDefault();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionForecast")]
        public ProductionForecastModel Put(ProductionForecastModel value)
        {

            var productionForecast = _context.ProductionForecast.SingleOrDefault(p => p.ProductionForecastId == value.ProductionForecastID);
            if (productionForecast != null)
            {
                productionForecast.ModifiedDate = DateTime.Now;
                productionForecast.MethodCodeId = value.MethodCodeID;
                productionForecast.PackQuantity = value.PackQuantity;
                productionForecast.ProductionMonth = value.ProductionMonth;
                productionForecast.BatchSize = value.BatchSize;
                productionForecast.ItemId = value.ItemId;
                //ProductionForecast.Quantity = value.Quantity;
                productionForecast.ModifiedByUserId = value.ModifiedByUserID;
                productionForecast.StatusCodeId = value.StatusCodeID.Value;
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionForecast")]
        public void Delete(int id)
        {
            var productionForecast = _context.ProductionForecast.SingleOrDefault(p => p.ProductionForecastId == id);
            if (productionForecast != null)
            {

                _context.ProductionForecast.Remove(productionForecast);
                _context.SaveChanges();
            }
        }

    }
}