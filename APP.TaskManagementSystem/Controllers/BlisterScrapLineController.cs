﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterScrapLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public BlisterScrapLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/BlisterScrapLine
        [HttpGet]
        [Route("GetBlisterScrapLine")]
        public List<BlisterScrapLineModel> Get(int id)
        {
            var blisterScrapLine = _context.BlisterScrapLine.Include("AddedByUser").Include("ModifiedByUser").Select(s => new BlisterScrapLineModel
            {
                BlisterScrapLineId = s.BlisterScrapLineId,
                BlisterScrapId = s.BlisterScrapId,
                Type = s.Type,
                TypeText = s.Type,
                ProdOrderNo = s.ProdOrderNo,
                ProdOrderNoText = s.ProdOrderNo,
                Description = s.Description,
                CalculationType = s.CalculationType,
                CalculationTypeText = "",
                AddonQty =s.AddonQty,
                QtyPer=s.QtyPer,
                Uom=s.Uom,
                UomText = s.Uom,
                ScrapPercentage =s.ScrapPercentage,
                QcInheritance = s.Qcinheritance,
                Inheritance=s.Inheritance,

            }).OrderByDescending(o => o.BlisterScrapLineId).Where(i=>i.BlisterScrapId == id).AsNoTracking().ToList();
            //var result = _mapper.Map<List<BlisterScrapLineModel>>(blisterScrapLine);
            return blisterScrapLine;
        }

        // GET: api/BlisterScrapLine/2
        //[HttpGet("{id}", Name = "GetProject")]
        [HttpGet("GetBlisterScrapLine/{id:int}")]
        public ActionResult<BlisterScrapLineModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var blisterScrapLine = _context.BlisterScrapLine.SingleOrDefault(p => p.BlisterScrapLineId == id.Value);
            var result = _mapper.Map<BlisterScrapLineModel>(blisterScrapLine);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterScrapLineModel> GetData(SearchModel searchModel)
        {
            var blisterScrapLine = new BlisterScrapLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapLine = _context.BlisterScrapLine.OrderBy(o => o.BlisterScrapLineId).FirstOrDefault(s => s.BlisterScrapLineId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterScrapLine = _context.BlisterScrapLine.OrderByDescending(o => o.BlisterScrapLineId).FirstOrDefault(s => s.BlisterScrapLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlisterScrapLineModel>(blisterScrapLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBlisterScrapLine")]
        public BlisterScrapLineModel Post(BlisterScrapLineModel value)
        {
            var blisterScrapLine = new BlisterScrapLine
            {
                BlisterScrapId = value.BlisterScrapId,
                Type = value.Type,
                ProdOrderNo = value.ProdOrderNo,
                Description = value.Description,
                CalculationType = value.CalculationType,
                AddonQty = value.AddonQty,
                QtyPer = value.QtyPer,
                Uom = value.Uom,
                ScrapPercentage = value.ScrapPercentage,
                Qcinheritance = value.QcInheritance,
                Inheritance = value.Inheritance,
            };
            _context.BlisterScrapLine.Add(blisterScrapLine);
            _context.SaveChanges();
            value.BlisterScrapLineId = blisterScrapLine.BlisterScrapLineId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateBlisterScrapLine")]
        public BlisterScrapLineModel Put(BlisterScrapLineModel value)
        {
            var blisterScrapLine = _context.BlisterScrapLine.SingleOrDefault(p => p.BlisterScrapLineId == value.BlisterScrapLineId);
            blisterScrapLine.BlisterScrapId = value.BlisterScrapId;
            blisterScrapLine.Type = value.Type;
            blisterScrapLine.ProdOrderNo = value.ProdOrderNo;
            blisterScrapLine.Description = value.Description;
            blisterScrapLine.CalculationType = value.CalculationType;
            blisterScrapLine.AddonQty = value.AddonQty;
            blisterScrapLine.QtyPer = value.QtyPer;
            blisterScrapLine.Uom = value.Uom;
            blisterScrapLine.ScrapPercentage = value.ScrapPercentage;
            blisterScrapLine.Qcinheritance= value.QcInheritance;
            blisterScrapLine.Inheritance = value.Inheritance;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteBlisterScrapLine")]
        public void Delete(int id)
        {
            var blisterScrapLine = _context.BlisterScrapLine.SingleOrDefault(p => p.BlisterScrapLineId == id);
            if (blisterScrapLine != null)
            {
                _context.BlisterScrapLine.Remove(blisterScrapLine);
                _context.SaveChanges();
            }
        }

    }
}