﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskAttachmentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskAttachmentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/TaskAttachment
        [HttpGet]
        [Route("GetTaskAttachment")]
        public List<TaskAttachmentModel> Get(long id)
        {
            var taskAttachment = _context.TaskAttachment.Include("Document").Select(s => new TaskAttachmentModel
            {
                TaskAttachmentID = s.TaskAttachmentId,
                TaskMasterID = s.TaskMasterId,
                DocumentID = s.DocumentId,
                FileName = s.Document.FileName,
                ContentType = s.Document.ContentType,
                FileSize = s.Document!=null && s.Document.FileSize!=null? (long)Math.Round(Convert.ToDouble(s.Document.FileSize / 1024)):0,
            }).OrderByDescending(o => o.TaskAttachmentID).Where(t => t.TaskMasterID == id).AsNoTracking().ToList();

            return taskAttachment;
        }
        [HttpGet]
        [Route("GetTaskAttachmentByDocument")]
        public List<TaskAttachmentModel> GetTaskAttachmentByDocument(long id)
        {
            var taskAttachment = _context.TaskAttachment.Include(a=>a.TaskMaster).Select(s => new TaskAttachmentModel
            {
                TaskAttachmentID = s.TaskAttachmentId,
                TaskMasterID = s.TaskMasterId,
                DocumentID = s.DocumentId,
                PreviousDocumentID=s.PreviousDocumentId,
                IsLatest=s.IsLatest.Value,
                IsLocked=s.IsLocked.Value,
                VersionNo=s.VersionNo,
                IsMajorChange=s.IsMajorChange,
                LockedByUserID=s.LockedByUserId,
                LockedDate=s.LockedDate,
                UploadedByUserID=s.UploadedByUserId,
                UploadedDate=s.UploadedDate,
                CheckInDescription=s.CheckInDescription,
                IsReleaseVersion=s.IsReleaseVersion,
                Title= s.TaskMaster!=null?s.TaskMaster.Title:"",
                Description = s.TaskMaster != null ? s.TaskMaster.Description : "",
                MainTaskId = s.TaskMaster != null ? s.TaskMaster.MainTaskId : null,
                ParentTaskID = s.TaskMaster != null ? s.TaskMaster.ParentTaskId :null,
                DueDate = s.TaskMaster != null ? s.TaskMaster.DueDate :null,
                NewDueDate = s.TaskMaster != null ? s.TaskMaster.NewDueDate :null,
                ProjectID=s.TaskMaster != null ? s.TaskMaster.ProjectId : null,
            }).OrderByDescending(o => o.TaskAttachmentID).Where(t => t.DocumentID == id).AsNoTracking().ToList();

            return taskAttachment;
        }
        // GET: api/TaskAttachment/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<TaskAttachmentModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskAttachment = _context.TaskAttachment.SingleOrDefault(p => p.TaskAttachmentId == id.Value);
            var result = _mapper.Map<TaskAttachmentModel>(taskAttachment);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskAttachmentModel> GetData(SearchModel searchModel)
        {
            var taskAttachment = new TaskAttachment();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).LastOrDefault();
                        break;
                    case "Next":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).LastOrDefault();
                        break;
                    case "Previous":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).FirstOrDefault();
                        break;
                    case "Last":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).LastOrDefault();
                        break;
                    case "Next":
                        taskAttachment = _context.TaskAttachment.OrderBy(o => o.TaskAttachmentId).FirstOrDefault(s => s.TaskAttachmentId > searchModel.Id);
                        break;
                    case "Previous":
                        taskAttachment = _context.TaskAttachment.OrderByDescending(o => o.TaskAttachmentId).FirstOrDefault(s => s.TaskAttachmentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskAttachmentModel>(taskAttachment);
            return result;
        }

        // POST: api/TaskAttachment
        [HttpPost]
        [Route("InsertTaskAttachment")]
        public TaskAttachmentModel Post(TaskAttachmentModel value)
        {
            var taskAttachment = new TaskAttachment
            {
                TaskAttachmentId = value.TaskAttachmentID,
                TaskMasterId = value.TaskMasterID,
                DocumentId = value.DocumentID

                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID,
                //Name = value.Name,
                //Description = value.Description
            };
            _context.TaskAttachment.Add(taskAttachment);
            _context.SaveChanges();
            value.TaskAttachmentID = taskAttachment.TaskAttachmentId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskAttachment")]
        public TaskAttachmentModel Put(TaskAttachmentModel value)
        {
            var taskAttachment = _context.TaskAttachment.SingleOrDefault(p => p.TaskAttachmentId == value.TaskAttachmentID);
            taskAttachment.TaskMasterId = value.TaskMasterID;
            taskAttachment.DocumentId = value.DocumentID;
            //taskAttachment.ModifiedByUserId = value.ModifiedByUserID;
            //taskAttachment project.Name = value.Name;
            //taskAttachment.Description = value.Description;
            //taskAttachment.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskAttachment")]
        public void Delete(int id)
        {
            var taskAttachment = _context.TaskAttachment.SingleOrDefault(p => p.TaskAttachmentId == id);
            if (taskAttachment != null)
            {
                _context.TaskAttachment.Remove(taskAttachment);
                _context.SaveChanges();
            }
        }


        [HttpGet]
        [Route("GetAttachmentLinks")]
        public List<DocumentsModel> GetAttachmentLinks(int id)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var taskmaster = _context.TaskMaster.Select(s => new { s.MainTaskId, s.TaskId, s.ParentTaskId, s.Title, s.SessionId }).AsNoTracking().ToList();
            //var taskAttachments = _context.TaskAttachment.Include("Document").Where(d => d.TaskMasterId.Value == id && d.IsLatest.Value == true);
            var documents = _context.Documents.
               Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.FileSize,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   s.DocumentParentId,
                   s.TableName,
                   s.ExpiryDate,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   IsLocked = s.IsLocked,
                   LockedByUserId = s.LockedByUserId,
                   LockedDate = s.LockedDate,
                   s.AddedDate,
                   s.DepartmentId,
                   s.WikiId,
                   s.Extension,
                   s.CategoryId,
                   s.DocumentType,
                   s.DisplayName,
                   s.LinkId,
                   s.IsSpecialFile,
                   s.IsTemp,
                   s.ReferenceNumber,
                   s.Description,
                   s.StatusCodeId,
                   s.ScreenId,
                   s.ProfileNo,
                   s.IsLatest,
                   s.IsCompressed,
                   s.IsVideoFile,
                   s.CloseDocumentId,
                   s.FileIndex,

               }).AsNoTracking().ToList();
            var taskAttachments = _context.TaskAttachment
                                    .Where(d => d.TaskMasterId.Value == id && d.IsLatest.Value == true)
                                    .Select(s => new
                                    {
                                        s.DocumentId,
                                        s.TaskComment,
                                        s.CheckInDescription,
                                        s.TaskMaster.Title,
                                        s.TaskMasterId,
                                        s.TaskMaster.ParentTaskId,
                                        s.UploadedDate,
                                        s.TaskAttachmentId,
                                        s.VersionNo,
                                        s.IsLocked,
                                        s.IsMajorChange,
                                        s.LockedByUserId,
                                        s.LockedDate,
                                        s.IsMeetingNotes,
                                        s.IsDiscussionNotes,
                                        s.IsSubTask,
                                        s.PreviousDocumentId,
                                        s.UploadedByUser.UserName,
                                        s.UploadedByUserId,
                                        s.ActualVersionNo,
                                        s.IsNoChange,
                                        s.DraftingVersionNo,
                                        s.IsReleaseVersion,

                                    }).AsNoTracking().ToList();
            //List<AttachmentModel> attachmentModels = taskAttachments.Select(t => new AttachmentModel
            //{
            //    TaskAttachmentID = t.TaskAttachmentId,
            //    TaskMasterID = t.TaskMasterId,
            //    DocumentID = t.DocumentId,
            //    DocumentName = t.Document.FileName,
            //    Description = t.Document.Description,
            //    IsVideoFile = t.Document.IsVideoFile,
            //    SessionId = t.Document.SessionId

            //}).ToList();
            var taskSessionId = taskmaster?.FirstOrDefault(t => t.TaskId == id)?.SessionId;
            var taskAttachmentdocumentid = taskAttachments.Where(c => c.DocumentId != null).Select(s => s.DocumentId).ToList();
            var tasktitle = taskmaster?.FirstOrDefault(t => t.TaskId == id)?.Title;
            if (taskSessionId != null)
            {
                var documentids = documents.Where(d => taskAttachmentdocumentid.Contains(d.DocumentId)).Select(d => d.DocumentId).Distinct().ToList();
                var linkProfileDocument = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == taskSessionId).Select(l => new { l.DocumentId, l.AddedDate, l.AddedByUserId, l.FileProfileTypeId, l.TransactionSessionId }).Distinct().ToList();
                var linkProfileDocumentIds = linkProfileDocument.Select(l => l.DocumentId).Distinct().ToList();

                if (linkProfileDocumentIds.Count > 0 || documentids.Count > 0)
                {
                    var documentRightsNew = _context.DocumentRights.Where(t => linkProfileDocumentIds.Contains(t.DocumentId) || documentids.Contains(t.DocumentId.Value)).AsNoTracking().ToList();
                    var linkProfiledocuments = documents.Where(t => linkProfileDocumentIds.Contains(t.DocumentId) || documentids.Contains(t.DocumentId)).ToList();
                    if (linkProfiledocuments != null && linkProfiledocuments.Count > 0)
                    {
                        linkProfiledocuments.ForEach(l =>
                        {
                            var seldocument = documents.FirstOrDefault(d => d.DocumentId == l.DocumentId);
                            var fileName = seldocument?.FileName.Split('.');
                            DocumentsModel documentsModel = new DocumentsModel
                            {
                                DocumentType = l.DocumentType,

                                CategoryID = l.CategoryId,
                                ContentType = l.ContentType,
                                DepartmentID = l.DepartmentId,
                                Description = l.Description,
                                DisplayName = l.DisplayName,
                                DocumentID = l.DocumentId,
                                Extension = l.Extension,
                                FileName = l.FileIndex > 0 ? fileName[0] + "_V0" + l.FileIndex + "." + fileName[1] : l.FileName,
                                FileSize = (long)Math.Round(Convert.ToDouble(l.FileSize / 1024)),
                                ReferenceNumber = l.ReferenceNumber,
                                SessionID = l.SessionId,
                                IsVideoFile = l.IsVideoFile,
                                FilterProfileTypeId = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.FileProfileTypeId : l.FilterProfileTypeId,
                                ProfileNo = l.ProfileNo,
                                IsLatest = l.IsLatest,
                                LockedDate = l.LockedDate,
                                UploadDate = l.UploadDate,
                                CheckInDescription = l.Description,
                                ExpiryDate = l.ExpiryDate,
                                IsLocked = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == id)?.Select(s => s.IsLocked)?.FirstOrDefault(),
                                LockedByUserId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == id)?.Select(s => s.LockedByUserId)?.FirstOrDefault(),
                                Type = "Document",
                                AddedByUserID = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.AddedByUserId : l.AddedByUserId,
                                LinkedTask = tasktitle,
                                LinkID = id,
                                PreviousDocumentId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == id).Select(s => s.PreviousDocumentId)?.FirstOrDefault(),
                                //FileProfileTypeName = fileProfileType?.FirstOrDefault(f => f.FileProfileTypeId == (linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.FileProfileTypeId))?.Name,
                                UploadedByUserId = linkProfileDocument != null ? linkProfileDocument.FirstOrDefault(w => w.DocumentId == l.DocumentId)?.AddedByUserId : l.AddedByUserId,
                                CloseDocumentId = l.CloseDocumentId,
                                TaskAttachmentId = taskAttachments?.Where(t => t.DocumentId == l.DocumentId && t.TaskMasterId == id).Select(s => s.TaskAttachmentId)?.FirstOrDefault(),
                               
                                //LockedByUser = applicationUsers.FirstOrDefault(f => f.UserId == l.LockedByUserId)?.UserName,
                                //UploadedByUser = applicationUsers.FirstOrDefault(f => f.UserId == l.AddedByUserId)?.UserName,
                            };

                            documentsModels.Add(documentsModel);
                        });
                    }

                }
            }
            documentsModels = documentsModels.OrderByDescending(d => d.UploadDate).Distinct().ToList();
            return documentsModels;

        }
    }
}