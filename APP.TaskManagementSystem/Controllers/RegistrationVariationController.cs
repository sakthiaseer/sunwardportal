﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RegistrationVariationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public RegistrationVariationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetRegistrationVariation")]
        public List<RegistrationVariationModel> Get()
        {
            List<RegistrationVariationModel> registrationVariationModels = new List<RegistrationVariationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var VariationList = _context.RegistrationVariationType.AsNoTracking().ToList();
            var RegistrationVariation = _context.RegistrationVariation
                .Include(a => a.AddedByUser)
                .Include(b => b.ModifiedByUser)
                .Include(c => c.StatustCode)
                .Include(d => d.RegistrationCategoryNavigation)
                .Include(e => e.RegistrationClassification)
                .Include(f => f.RegistrationCountry)
                .Include(g => g.RegistrationVariationCode)
                .Include(h => h.RegistrationVariationType)
                .Include(a => a.RegistrationVariationTypeNavigation)
                .OrderByDescending(o => o.RegistrationVariationId).AsNoTracking().ToList();
            RegistrationVariation.ForEach(s =>
            {
                RegistrationVariationModel registrationVariationModel = new RegistrationVariationModel();
                registrationVariationModel.RegistrationVariationId = s.RegistrationVariationId;
                registrationVariationModel.RegistrationCountryId = s.RegistrationCountryId;
                registrationVariationModel.RegisterationCodeId = s.RegisterationCodeId;
                registrationVariationModel.RegisterationCodeName = s.RegisterationCode?.CodeValue;
                registrationVariationModel.RegistrationCountryName = s.RegistrationCountry?.Value;
                registrationVariationModel.RegistrationClassificationId = s.RegistrationClassificationId;
                registrationVariationModel.RegistrationClassificationName = s.RegistrationClassification?.Value;
                registrationVariationModel.RegistrationVariationCodeId = s.RegistrationVariationCodeId;
                registrationVariationModel.RegistrationVariationCodeName = s.RegistrationVariationCode?.Value + "|" + s.RegistrationVariationCode?.Description;
                registrationVariationModel.VariationTitle = s.VariationTitle;
                registrationVariationModel.RegistrationVariationTypeId = s.RegistrationVariationTypeNavigation != null ? s.RegistrationVariationTypeNavigation.Where(p => p.RegistrationVariationId == s.RegistrationVariationId).Select(p => p.VariationTypeId).ToList() : new List<long?>();
                registrationVariationModel.RegistrationCategory = s.RegistrationCategory;
                registrationVariationModel.RegistrationCategoryName = s.RegistrationCategoryNavigation?.Value;
                registrationVariationModel.VariationCodeandCountry = s.RegistrationVariationCode?.Value + "|" + s.RegistrationCountry?.Description;
                registrationVariationModel.ModifiedByUserID = s.ModifiedByUserId;
                registrationVariationModel.AddedDate = s.AddedDate;
                registrationVariationModel.ModifiedDate = s.ModifiedDate;
                registrationVariationModel.AddedByUser = s.AddedByUser.UserName;
                registrationVariationModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                registrationVariationModel.StatusCode = s.StatustCode.CodeValue;
                registrationVariationModel.StatusCodeID = s.StatustCodeId;
                registrationVariationModel.RegistrationVariationTypeName = s.RegistrationVariationTypeNavigation != null && applicationmasterdetail != null ? string.Join(",", s.RegistrationVariationTypeNavigation.Where(b => b.RegistrationVariationId == s.RegistrationVariationId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.VariationTypeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                registrationVariationModel.RegisterationName = registrationVariationModel.RegistrationCountryName + "|" + registrationVariationModel.RegistrationClassificationName + "|" + registrationVariationModel.RegistrationVariationTypeName + "|" + registrationVariationModel.RegistrationCategoryName + "|" + registrationVariationModel.RegistrationVariationCodeName;
                registrationVariationModels.Add(registrationVariationModel);
            });

            return registrationVariationModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RegistrationVariationModel> GetData(SearchModel searchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var VariationList = _context.RegistrationVariationType.AsNoTracking().ToList();

            var RegistrationVariation = new RegistrationVariation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).FirstOrDefault();
                        break;
                    case "Last":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).LastOrDefault();
                        break;
                    case "Next":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).LastOrDefault();
                        break;
                    case "Previous":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).FirstOrDefault();
                        break;
                    case "Last":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).LastOrDefault();
                        break;
                    case "Next":
                        RegistrationVariation = _context.RegistrationVariation.OrderBy(o => o.RegistrationVariationId).FirstOrDefault(s => s.RegistrationVariationId > searchModel.Id);
                        break;
                    case "Previous":
                        RegistrationVariation = _context.RegistrationVariation.OrderByDescending(o => o.RegistrationVariationId).FirstOrDefault(s => s.RegistrationVariationId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<RegistrationVariationModel>(RegistrationVariation);
            if (result != null)
            {
                result.StatusCodeID = result.StatustCodeId;
                result.RegistrationCountryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationCountryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationCountryId).Value : "";
                result.RegistrationClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationClassificationId).Value : "";
                result.RegistrationVariationCodeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationVariationCodeId).Value : "";
                result.RegistrationVariationTypeId = VariationList.Where(p => p.RegistrationVariationId == result.RegistrationVariationId).Select(p => p.VariationTypeId).ToList();
                result.RegistrationCategoryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationCategory) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationCategory).Value : "";
                result.RegistrationVariationCodeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.RegistrationVariationCodeId).Value : "";

            }
            return result;
        }
        [HttpPost]
        [Route("InsertRegistrationVariation")]
        public RegistrationVariationModel Post(RegistrationVariationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RegistrationVariation = new RegistrationVariation
            {

                RegistrationCountryId = value.RegistrationCountryId,
                RegisterationCodeId = value.RegisterationCodeId,
                RegistrationClassificationId = value.RegistrationClassificationId,
                RegistrationVariationCodeId = value.RegistrationVariationCodeId,
                RegistrationCategory = value.RegistrationCategory,
                VariationTitle = value.VariationTitle,
                AddedDate = DateTime.Now,
                StatustCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.RegistrationVariation.Add(RegistrationVariation);
            if (value.RegistrationVariationTypeId != null)
            {
                value.RegistrationVariationTypeId.ForEach(c =>
                {
                    var RegistrationVariationType = new RegistrationVariationType
                    {
                        VariationTypeId = c,
                    };
                    RegistrationVariation.RegistrationVariationTypeNavigation.Add(RegistrationVariationType);
                });
            }
            _context.SaveChanges();
            value.RegistrationVariationId = RegistrationVariation.RegistrationVariationId;
            value.RegistrationCountryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCountryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCountryId).Value : "";
            value.RegistrationClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationClassificationId).Value : "";
            value.RegistrationVariationCodeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId).Value : "";
            value.RegistrationCategoryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCategory) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCategory).Value : "";
            value.RegistrationVariationCodeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId).Value : "";

            return value;
        }
        [HttpPut]
        [Route("UpdateRegistrationVariation")]
        public RegistrationVariationModel Put(RegistrationVariationModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RegistrationVariation = _context.RegistrationVariation.SingleOrDefault(p => p.RegistrationVariationId == value.RegistrationVariationId);
            RegistrationVariation.RegistrationCountryId = value.RegistrationCountryId;
            RegistrationVariation.RegisterationCodeId = value.RegisterationCodeId;
            RegistrationVariation.RegistrationClassificationId = value.RegistrationClassificationId;
            RegistrationVariation.RegistrationVariationCodeId = value.RegistrationVariationCodeId;
            RegistrationVariation.RegistrationCategory = value.RegistrationCategory;
            RegistrationVariation.VariationTitle = value.VariationTitle;
            RegistrationVariation.ModifiedByUserId = value.ModifiedByUserID;
            RegistrationVariation.ModifiedDate = DateTime.Now;
            RegistrationVariation.StatustCodeId = value.StatusCodeID.Value;
            var RegistrationVariationTypeRemove = _context.RegistrationVariationType.Where(l => l.RegistrationVariationId == value.RegistrationVariationId).AsNoTracking().ToList();
            if (RegistrationVariationTypeRemove.Count > 0)
            {
                _context.RegistrationVariationType.RemoveRange(RegistrationVariationTypeRemove);
            }
            if (value.RegistrationVariationTypeId != null)
            {
                value.RegistrationVariationTypeId.ForEach(c =>
                {
                    var RegistrationVariationType = new RegistrationVariationType
                    {
                        VariationTypeId = c,
                    };
                    RegistrationVariation.RegistrationVariationTypeNavigation.Add(RegistrationVariationType);
                });
            }
            _context.SaveChanges();
            value.RegistrationVariationId = RegistrationVariation.RegistrationVariationId;
            value.RegistrationCountryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCountryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCountryId).Value : "";
            value.RegistrationClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationClassificationId).Value : "";
            value.RegistrationVariationCodeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationVariationCodeId).Value : "";
            value.RegistrationCategoryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCategory) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == value.RegistrationCategory).Value : "";

            return value;
        }
        [HttpDelete]
        [Route("DeleteRegistrationVariation")]
        public void Delete(int id)
        {
            var RegistrationVariation = _context.RegistrationVariation.SingleOrDefault(p => p.RegistrationVariationId == id);
            if (RegistrationVariation != null)
            {
                var VariationList = _context.RegistrationVariationType.Where(s => s.RegistrationVariationId == id).AsNoTracking().ToList();
                if (VariationList != null)
                {
                    _context.RegistrationVariationType.RemoveRange(VariationList);
                    _context.SaveChanges();
                }
                _context.RegistrationVariation.Remove(RegistrationVariation);
                _context.SaveChanges();
            }
        }
    }
}