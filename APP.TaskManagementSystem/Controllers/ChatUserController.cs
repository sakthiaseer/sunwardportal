﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ChatUserController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ChatUserController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        //WorkDateId
        //WorkDateID
        [HttpGet]
        [Route("GetChatUsers")]
        public List<ChatUserModel> Get()
        {
            var chatUsers = _context.ChatUser.Include(a => a.User).ToList();
            List<ChatUserModel> chatUserModel = new List<ChatUserModel>();
            chatUsers.ForEach(s =>
            {
                ChatUserModel chatUserModels = new ChatUserModel
                {
                    ChatUserId = s.ChatUserId,
                    UserName = s.User != null ? s.User.UserName : "",
                    SessionId = s.SessionId,
                    ConnectionId = s.ConnectionId,
                    ImageURL = "",
                };
                chatUserModel.Add(chatUserModels);
            });
            return chatUserModel.OrderByDescending(a => a.ChatUserId).ToList();
        }


        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get ApplicationUser")]
        [HttpGet("GetChatUser/{id:int}")]
        public ActionResult<ChatUserModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var chatUser = _context.ChatUser.Include("User").Where(t => t.ChatUserId == id);
            var result = _mapper.Map<ChatUserModel>(chatUser);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertChatUser")]
        public ChatUserModel Post(ChatUserModel value)
        {
            var chatUser = new ChatUser
            {
                UserId = value.UserId,
                SessionId = value.SessionId,
                ConnectionId = value.ConnectionId
            };
            _context.ChatUser.Add(chatUser);
            _context.SaveChanges();
            value.ChatUserId = chatUser.ChatUserId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateChatUser")]
        public ChatUserModel Put(ChatUserModel value)
        {
            var chatUser = _context.ChatUser.SingleOrDefault(p => p.ChatUserId == value.ChatUserId);
            chatUser.ConnectionId = value.ConnectionId;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteChatUser")]
        public void Delete(int id)
        {
            var chatUser = _context.ChatUser.SingleOrDefault(p => p.ChatUserId == id);
            if (chatUser != null)
            {
                _context.ChatUser.Remove(chatUser);
                _context.SaveChanges();
            }
        }
    }
}