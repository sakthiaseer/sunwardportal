﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SOWithoutBlanketController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SOWithoutBlanketController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        [HttpGet]
        [Route("GetSOWithoutBlanketOrders")]
        public List<SOWithoutBlanketOrderModel> Get(int id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
           
            var sowithOutBlanketOrders = _context.SowithOutBlanketOrder
                .Include(p => p.SalesOrder)
                .Include(p => p.ShipToCode)
                .Include(p => p.ShipToCode.SobyCustomersMasterAddress)
                .Include(p => p.Soproduct)
                .Include(a => a.AddedByUser)
                .Include(c => c.StatusCode)
                .Include(a => a.ModifiedByUser)
                .Where(p => p.SalesOrderId == id)
                .AsNoTracking()
                .ToList();
            List<SOWithoutBlanketOrderModel> sOWithoutBlanketOrderModels = new List<SOWithoutBlanketOrderModel>();
            if (sowithOutBlanketOrders != null && sowithOutBlanketOrders.Count > 0)
            {
                var ordercurrencyIds = sowithOutBlanketOrders.Where(s => s.OrderCurrencyId != null).Select(s => s.OrderCurrencyId).ToList();
                var priceUnitIds = sowithOutBlanketOrders.Where(s => s.PriceUnitId != null).Select(s => s.PriceUnitId).ToList();
                if(ordercurrencyIds.Count>0)
                {
                    masterDetailIds.AddRange(ordercurrencyIds);
                }
                if(priceUnitIds.Count>0)
                {
                    masterDetailIds.AddRange(priceUnitIds);
                }
                if(masterDetailIds.Count>0)
                {
                     masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
                sowithOutBlanketOrders.ForEach(s =>
                {
                    SOWithoutBlanketOrderModel sOWithoutBlanketOrderModel = new SOWithoutBlanketOrderModel();
                    sOWithoutBlanketOrderModel.SowithoutBlanketOrderId = s.SowithoutBlanketOrderId;
                    sOWithoutBlanketOrderModel.SalesOrderId = s.SalesOrderId;
                    sOWithoutBlanketOrderModel.ShipToCodeId = s.ShipToCodeId;
                    sOWithoutBlanketOrderModel.ShipToCode = s.ShipToCode.SobyCustomersMasterAddress?.PostalCode;
                    sOWithoutBlanketOrderModel.SoproductId = s.SoproductId;
                    sOWithoutBlanketOrderModel.SoProduct = s.Soproduct?.CustomerReferenceNo;
                    sOWithoutBlanketOrderModel.IsLotInformation = s.IsLotInformation;
                    sOWithoutBlanketOrderModel.OrderQty = s.OrderQty;
                    sOWithoutBlanketOrderModel.OrderCurrencyId = s.OrderCurrencyId;
                    sOWithoutBlanketOrderModel.SellingPrice = s.SellingPrice;
                    sOWithoutBlanketOrderModel.PriceUnitId = s.PriceUnitId;
                    sOWithoutBlanketOrderModel.IsFoc = s.IsFoc;
                    sOWithoutBlanketOrderModel.RequestShipmentDate = s.RequestShipmentDate;
                    sOWithoutBlanketOrderModel.StatusCodeID = s.StatusCodeId;
                    sOWithoutBlanketOrderModel.StatusCode = s.StatusCode?.CodeValue;
                    sOWithoutBlanketOrderModel.AddedByUserID = s.AddedByUserId;
                    sOWithoutBlanketOrderModel.AddedByUser = s.AddedByUser?.UserName;
                    sOWithoutBlanketOrderModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    sOWithoutBlanketOrderModel.AddedDate = s.AddedDate;
                    sOWithoutBlanketOrderModel.ModifiedByUserID = s.ModifiedByUserId;
                    sOWithoutBlanketOrderModel.ModifiedDate = s.ModifiedDate;
                    sOWithoutBlanketOrderModel.OrderCurrency = masterDetailList != null && s.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                    sOWithoutBlanketOrderModel.PriceUnit = masterDetailList != null && s.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
                    sOWithoutBlanketOrderModels.Add(sOWithoutBlanketOrderModel);
                });
            }
            return sOWithoutBlanketOrderModels.OrderByDescending(a => a.SowithoutBlanketOrderId).ToList();
        }

        [HttpPost()]
        [Route("GetSOWithoutBlanketOrderData")]
        public ActionResult<SOWithoutBlanketOrderModel> GetSalesOrderProductData(SearchModel searchModel)
        {
            var sowithOutBlanketOrder = new SowithOutBlanketOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).LastOrDefault();
                        break;
                    case "Next":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderBy(o => o.SowithoutBlanketOrderId).FirstOrDefault(s => s.SowithoutBlanketOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        sowithOutBlanketOrder = _context.SowithOutBlanketOrder.OrderByDescending(o => o.SowithoutBlanketOrderId).FirstOrDefault(s => s.SowithoutBlanketOrderId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SOWithoutBlanketOrderModel>(sowithOutBlanketOrder);
            return result;
        }

        [HttpPost]
        [Route("InsertSowithoutBlanketOrder")]
        public SOWithoutBlanketOrderModel Post(SOWithoutBlanketOrderModel value)
        {
           
            var sOWithoutBlanketOrder = new SowithOutBlanketOrder();
            sOWithoutBlanketOrder.SalesOrderId = value.SalesOrderId;
            sOWithoutBlanketOrder.ShipToCodeId = value.ShipToCodeId;
            sOWithoutBlanketOrder.SoproductId = value.SoproductId;
            sOWithoutBlanketOrder.IsLotInformation = value.IsLotInformation;
            sOWithoutBlanketOrder.OrderQty = value.OrderQty;
            sOWithoutBlanketOrder.OrderCurrencyId = value.OrderCurrencyId;
            sOWithoutBlanketOrder.SellingPrice = value.SellingPrice;
            sOWithoutBlanketOrder.PriceUnitId = value.PriceUnitId;
            sOWithoutBlanketOrder.IsFoc = value.IsFoc;
            sOWithoutBlanketOrder.RequestShipmentDate = value.RequestShipmentDate;
            sOWithoutBlanketOrder.StatusCodeId = value.StatusCodeID.Value;
            sOWithoutBlanketOrder.AddedByUserId = value.AddedByUserID;
            sOWithoutBlanketOrder.AddedDate = DateTime.Now;
            _context.SowithOutBlanketOrder.Add(sOWithoutBlanketOrder);
            _context.SaveChanges();
            value.SowithoutBlanketOrderId = sOWithoutBlanketOrder.SowithoutBlanketOrderId;
            if (value.OrderCurrencyId > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s=>s.ApplicationMasterDetailId == value.OrderCurrencyId).AsNoTracking().ToList();
                value.OrderCurrency = masterDetailList != null && value.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
            }
            if(value.PriceUnitId >0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s=>s.ApplicationMasterDetailId == value.PriceUnitId).AsNoTracking().ToList();
                value.PriceUnit = masterDetailList != null && value.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
            }
            value.AddedDate = sOWithoutBlanketOrder.AddedDate;
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == sOWithoutBlanketOrder.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            if (value.ShipToCodeId > 0)
            {
                value.ShipToCode = _context.SobyCustomersAddress.Include(t=>t.SobyCustomersMasterAddress).Where(s => s.SobyCustomersAddressId == value.ShipToCodeId).Select(s => s.SobyCustomersMasterAddress.PostalCode).FirstOrDefault();
            }
            if (value.SoproductId > 0)
            {
                value.SoProduct = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoproductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSOWithoutBlanketOrder")]
        public SOWithoutBlanketOrderModel Put(SOWithoutBlanketOrderModel value)
        {
           
            var sOWithoutBlanketOrder = _context.SowithOutBlanketOrder.SingleOrDefault(p => p.SowithoutBlanketOrderId == value.SowithoutBlanketOrderId);
            sOWithoutBlanketOrder.SalesOrderId = value.SalesOrderId;
            sOWithoutBlanketOrder.ShipToCodeId = value.ShipToCodeId;
            sOWithoutBlanketOrder.SoproductId = value.SoproductId;
            sOWithoutBlanketOrder.IsLotInformation = value.IsLotInformation;
            sOWithoutBlanketOrder.OrderQty = value.OrderQty;
            sOWithoutBlanketOrder.OrderCurrencyId = value.OrderCurrencyId;
            sOWithoutBlanketOrder.SellingPrice = value.SellingPrice;
            sOWithoutBlanketOrder.PriceUnitId = value.PriceUnitId;
            sOWithoutBlanketOrder.IsFoc = value.IsFoc;
            sOWithoutBlanketOrder.RequestShipmentDate = value.RequestShipmentDate;
            sOWithoutBlanketOrder.StatusCodeId = value.StatusCodeID.Value;
            sOWithoutBlanketOrder.ModifiedByUserId = value.ModifiedByUserID;
            sOWithoutBlanketOrder.ModifiedDate = value.ModifiedDate.Value;
            _context.SaveChanges();
            if (value.ShipToCodeId > 0)
            {
                value.ShipToCode = _context.SobyCustomersAddress.Include(t=>t.SobyCustomersMasterAddress).Where(s => s.SobyCustomersAddressId == value.ShipToCodeId).Select(s => s.SobyCustomersMasterAddress.PostalCode).FirstOrDefault();
            }
            if (value.SoproductId > 0)
            {
                value.SoProduct = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoproductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            if (value.OrderCurrencyId > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.OrderCurrencyId).AsNoTracking().ToList();
                value.OrderCurrency = masterDetailList != null && value.OrderCurrencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
            }
            if (value.PriceUnitId > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.PriceUnitId).AsNoTracking().ToList();
                value.PriceUnit = masterDetailList != null && value.PriceUnitId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.PriceUnitId).Select(a => a.Value).SingleOrDefault() : "";
            }
            value.ModifiedDate = sOWithoutBlanketOrder.ModifiedDate;
            value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == sOWithoutBlanketOrder.ModifiedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSoWithOutBlanketOrder")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var sowithOutBlanketOrder = _context.SowithOutBlanketOrder.Where(p => p.SowithoutBlanketOrderId == id).FirstOrDefault();
                if (sowithOutBlanketOrder != null)
                {
                    var solotInformations = _context.SolotInformation.Where(s => s.SowithoutBlanketOrderId == sowithOutBlanketOrder.SowithoutBlanketOrderId);
                    if (solotInformations != null)
                    {
                        _context.SolotInformation.RemoveRange(solotInformations);
                        _context.SaveChanges();
                    }
                    _context.SowithOutBlanketOrder.Remove(sowithOutBlanketOrder);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

