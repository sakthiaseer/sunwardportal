﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PKGApprovalInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;


        public PKGApprovalInformationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetPKGApprovalInformation")]
        public List<PKGApprovalInformationModel> Get()
        {
            var PkgapprovalInformation = _context.PkgapprovalInformation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Include(a => a.Item).Include(a => a.Plant).AsNoTracking().ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            if (PkgapprovalInformation != null && PkgapprovalInformation.Count > 0)
            {
                var masterdetailIds = PkgapprovalInformation?.Where(s => s.ProductGroupCodeId != null).Select(s => s.ProductGroupCodeId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(s => masterdetailIds.Contains(s.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            List<PKGApprovalInformationModel> PKGApprovalInformationModel = new List<PKGApprovalInformationModel>();
            PkgapprovalInformation.ForEach(s =>
            {
                PKGApprovalInformationModel PKGApprovalInformationModels = new PKGApprovalInformationModel
                {
                    PkgapprovalInformationId = s.PkgapprovalInformationId,
                    PlantId = s.PlantId,
                    ProductGroupCodeId = s.ProductGroupCodeId,
                    ProductGroupCode = masterDetailList != null && s.ProductGroupCodeId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.ProductGroupCodeId).Select(a => a.Value).SingleOrDefault() : "",
                    ItemId = s.ItemId,
                    NavisionItemCode = s.Item?.No,
                    CompanyName = s.Plant?.PlantCode,
                    SessionId = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                PKGApprovalInformationModel.Add(PKGApprovalInformationModels);
            });
            return PKGApprovalInformationModel.OrderByDescending(a => a.PkgapprovalInformationId).ToList();
        }

        [HttpGet]
        [Route("GetPKGRegisteredPackingInformationByID")]
        public List<PKGRegisteredPackingInformationModel> GetPKGRegisteredPackingInformationByID(long? id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var PkgregisteredPackingInformation = _context.PkgregisteredPackingInformation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Where(s => s.PkgapprovalInformationId == id).AsNoTracking().ToList();
            if (PkgregisteredPackingInformation != null && PkgregisteredPackingInformation.Count > 0)
            {
                var masterdetailIds = PkgregisteredPackingInformation?.Where(s => s.PackagingItemId != null).Select(s => s.PackagingItemId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(s => masterdetailIds.Contains(s.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            List<PKGRegisteredPackingInformationModel> PKGRegisteredPackingInformationModels = new List<PKGRegisteredPackingInformationModel>();
            PkgregisteredPackingInformation.ForEach(s =>
            {
                PKGRegisteredPackingInformationModel PKGRegisteredPackingInformationModel = new PKGRegisteredPackingInformationModel
                {

                    PkgapprovalInformationId = s.PkgapprovalInformationId,
                    PkgregisteredPackingInformationId = s.PkgregisteredPackingInformationId,
                    PackagingItemId = s.PackagingItemId,
                    PackagingItem = masterDetailList != null && s.PackagingItemId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,
                };
                PKGRegisteredPackingInformationModels.Add(PKGRegisteredPackingInformationModel);
            });
            return PKGRegisteredPackingInformationModels.OrderByDescending(a => a.PkgregisteredPackingInformationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PKGApprovalInformationModel> GetData(SearchModel searchModel)
        {
            var PKGApprovalInformation = new PkgapprovalInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).LastOrDefault();
                        break;
                    case "Next":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).LastOrDefault();
                        break;
                    case "Next":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderBy(o => o.PkgapprovalInformationId).FirstOrDefault(s => s.PkgapprovalInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        PKGApprovalInformation = _context.PkgapprovalInformation.OrderByDescending(o => o.PkgapprovalInformationId).FirstOrDefault(s => s.PkgapprovalInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PKGApprovalInformationModel>(PKGApprovalInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPKGApprovalInformation")]
        public PKGApprovalInformationModel Post(PKGApprovalInformationModel value)
        {
            var SessionId = Guid.NewGuid();
            var PkgapprovalInformation = new PkgapprovalInformation
            {
                ProductGroupCodeId = value.ProductGroupCodeId,
                ItemId = value.ItemId,
                PlantId = value.PlantId,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.PkgapprovalInformation.Add(PkgapprovalInformation);
            _context.SaveChanges();
            value.PkgapprovalInformationId = PkgapprovalInformation.PkgapprovalInformationId;
            value.SessionId = PkgapprovalInformation.SessionId;

            return value;
        }
        [HttpPut]
        [Route("UpdatePKGApprovalInformation")]
        public PKGApprovalInformationModel Put(PKGApprovalInformationModel value)
        {
            var SessionId = Guid.NewGuid();

            var PkgapprovalInformation = _context.PkgapprovalInformation.SingleOrDefault(p => p.PkgapprovalInformationId == value.PkgapprovalInformationId);
            PkgapprovalInformation.PlantId = value.PlantId;
            PkgapprovalInformation.ItemId = value.ItemId;
            PkgapprovalInformation.ProductGroupCodeId = value.ProductGroupCodeId;
            if (value.SessionId != null)
            {
                PkgapprovalInformation.SessionId = value.SessionId;
            }
            else
            {
                PkgapprovalInformation.SessionId = SessionId;
            }
            PkgapprovalInformation.ModifiedByUserId = value.ModifiedByUserID;
            PkgapprovalInformation.ModifiedDate = DateTime.Now;
            PkgapprovalInformation.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePKGApprovalInformation")]
        public void Delete(int id)
        {
            try
            {
                var PKGApprovalInformationModel = _context.PkgapprovalInformation.SingleOrDefault(p => p.PkgapprovalInformationId == id);
                if (PKGApprovalInformationModel != null)
                {
                    _context.PkgapprovalInformation.Remove(PKGApprovalInformationModel);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("This Pkg Approval Information Cannot be Delete Reference to others", ex);
            }
        }


        [HttpPost]
        [Route("InsertPKGRegisteredPackingInformation")]
        public PKGRegisteredPackingInformationModel InsertPKGRegisteredPackingInformation(PKGRegisteredPackingInformationModel value)
        {
            var SessionId = Guid.NewGuid();
            var PkgregisteredPackingInformation = new PkgregisteredPackingInformation
            {
                PackagingItemId = value.PackagingItemId,
                PkgapprovalInformationId = value.PkgapprovalInformationId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                SessionId = SessionId,
            };
            _context.PkgregisteredPackingInformation.Add(PkgregisteredPackingInformation);
            _context.SaveChanges();
            value.PkgregisteredPackingInformationId = PkgregisteredPackingInformation.PkgregisteredPackingInformationId;
            value.SessionId = PkgregisteredPackingInformation.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePKGRegisteredPackingInformation")]
        public PKGRegisteredPackingInformationModel UpdatePKGRegisteredPackingInformation(PKGRegisteredPackingInformationModel value)
        {
            var SessionId = Guid.NewGuid();


            var PkgregisteredPackingInformation = _context.PkgregisteredPackingInformation.SingleOrDefault(p => p.PkgregisteredPackingInformationId == value.PkgregisteredPackingInformationId);

            PkgregisteredPackingInformation.PkgapprovalInformationId = value.PkgapprovalInformationId;
            PkgregisteredPackingInformation.PackagingItemId = value.PackagingItemId;
            PkgregisteredPackingInformation.ModifiedByUserId = value.ModifiedByUserID;
            PkgregisteredPackingInformation.ModifiedDate = DateTime.Now;
            PkgregisteredPackingInformation.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePKGRegisteredPackingInformation")]
        public void DeletePKGRegisteredPackingInformation(int id)
        {
            try
            {
                var PkgregisteredPackingInformation = _context.PkgregisteredPackingInformation.SingleOrDefault(p => p.PkgregisteredPackingInformationId == id);
                if (PkgregisteredPackingInformation != null)
                {
                    _context.PkgregisteredPackingInformation.Remove(PkgregisteredPackingInformation);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("This Pkg Registered Packing Information Cannot be Delete Reference to others", ex);
            }
        }
        [HttpGet]
        [Route("GetPKGInformationByPackSizeByID")]
        public List<PKGInformationByPackSizeModel> GetPKGInformationByPackSizeByID(long? id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var PkginformationByPackSize = _context.PkginformationByPackSize.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Where(s => s.PkgregisteredPackingInformationId == id).AsNoTracking().ToList();
            if (PkginformationByPackSize != null && PkginformationByPackSize.Count > 0)
            {
                var masterdetailIds = PkginformationByPackSize?.Where(s => s.PackSizeId != null).Select(s => s.PackSizeId).ToList();
                if (masterdetailIds != null && masterdetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(s => masterdetailIds.Contains(s.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            List<PKGInformationByPackSizeModel> PKGInformationByPackSizeModels = new List<PKGInformationByPackSizeModel>();
            PkginformationByPackSize.ForEach(s =>
            {
                PKGInformationByPackSizeModel PKGInformationByPackSizeModel = new PKGInformationByPackSizeModel
                {

                    PkginformationByPackSizeId = s.PkginformationByPackSizeId,
                    PkgregisteredPackingInformationId = s.PkgregisteredPackingInformationId,
                    ProfileRefNo = s.ProfileRefNo,
                    PackSizeId = s.PackSizeId,
                    PackSize = masterDetailList != null && s.PackSizeId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PackSizeId).Select(a => a.Value).SingleOrDefault() : "",
                    DateOfApproval = s.DateOfApproval,
                    DateOfDownload = s.DateOfDownload,
                    VersionNo = s.VersionNo,
                    Link = s.Link,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                PKGInformationByPackSizeModels.Add(PKGInformationByPackSizeModel);
            });
            return PKGInformationByPackSizeModels.OrderByDescending(a => a.PkgregisteredPackingInformationId).ToList();
        }

        [HttpPost]
        [Route("InsertPKGInformationByPackSize")]
        public PKGInformationByPackSizeModel InsertPKGInformationByPackSize(PKGInformationByPackSizeModel value)
        {
            var profileNo = "";
            if (value.ProfileID > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Information By Pack Size" });
            }
            var PkginformationByPackSize = new PkginformationByPackSize
            {
                PkgregisteredPackingInformationId = value.PkgregisteredPackingInformationId,
                ProfileRefNo = profileNo,
                PackSizeId = value.PackSizeId,
                DateOfApproval = value.DateOfApproval,
                DateOfDownload = value.DateOfDownload,
                VersionNo = value.VersionNo,
                Link = value.Link,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.PkginformationByPackSize.Add(PkginformationByPackSize);
            _context.SaveChanges();
            value.PkginformationByPackSizeId = PkginformationByPackSize.PkginformationByPackSizeId;
            value.ProfileRefNo = PkginformationByPackSize.ProfileRefNo;
            return value;
        }
        [HttpPut]
        [Route("UpdatePKGInformationByPackSize")]
        public PKGInformationByPackSizeModel UpdatePKGInformationByPackSize(PKGInformationByPackSizeModel value)
        {
            var SessionId = Guid.NewGuid();


            var PkginformationByPackSize = _context.PkginformationByPackSize.SingleOrDefault(p => p.PkginformationByPackSizeId == value.PkginformationByPackSizeId);

            PkginformationByPackSize.PkgregisteredPackingInformationId = value.PkgregisteredPackingInformationId;
            PkginformationByPackSize.PackSizeId = value.PackSizeId;
            PkginformationByPackSize.DateOfApproval = value.DateOfApproval;
            PkginformationByPackSize.DateOfDownload = value.DateOfDownload;
            PkginformationByPackSize.VersionNo = value.VersionNo;
            PkginformationByPackSize.Link = value.Link;
            PkginformationByPackSize.ModifiedByUserId = value.ModifiedByUserID;
            PkginformationByPackSize.ModifiedDate = DateTime.Now;

            PkginformationByPackSize.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePKGInformationByPackSize")]
        public void DeletePKGInformationByPackSize(int id)
        {
            var PkginformationByPackSize = _context.PkginformationByPackSize.SingleOrDefault(p => p.PkginformationByPackSizeId == id);
            if (PkginformationByPackSize != null)
            {
                _context.PkginformationByPackSize.Remove(PkginformationByPackSize);
                _context.SaveChanges();
            }
        }
    }
}
