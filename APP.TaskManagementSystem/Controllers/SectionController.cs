﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SectionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SectionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSections")]
        public List<SectionModel> Get()
        {
            List<SectionModel> sectionModels = new List<SectionModel>();
            var section = _context.Section
              .Include("AddedByUser")
              .Include("ModifiedByUser")
              .Include("StatusCode")
              .Include("Department")
              .Include("Department.Division")
              .Include("Department.Division.Company").OrderByDescending(o => o.SectionId).AsNoTracking().ToList();
            //var deparmentlist = _context.Department.ToList();
            //var divisionlist = _context.Division.ToList();
            //var companylist = _context.Plant.ToList();
            section.ForEach(s =>
            {
                SectionModel sectionModel = new SectionModel
                {
                    SectionID = s.SectionId,
                    DepartmentID = s.DepartmentId,
                    DepartmentName = s.Department?.Name,
                    //DepartmentName = s.Department!=null?s.Department.Name: "",
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    IsWiki = s.IsWiki,
                    IsTestPaper = s.IsTestPaper,
                    HeadCount = s.HeadCount,
                    CompanyName = s.Department?.Division?.Company?.Description,
                    DivisionName = s.Department?.Division?.Name,
                    //CompanyName =s.Department!=null && s.Department.Division!=null && s.Department.Division.Company!=null ? s.Department.Division.Company.Description: "",
                    //DivisionName = s.Department != null && s.Department.Division != null? s.Department.Division.Name : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ProfileCode = s.ProfileCode,
                    ModifiedDate = s.ModifiedDate
                };
                sectionModels.Add(sectionModel);

            });
           
            if (sectionModels != null && sectionModels.Count > 0)
            {
                sectionModels.ForEach(d =>
                {
                    d.CompanyDivisionName = d.CompanyName + " | " + d.DivisionName + " | " + d.DepartmentName +" | " + d.Name;

                });
            }
            //var result = _mapper.Map<List<SectionModel>>(Section);
            return sectionModels;
        }

        [HttpGet]
        [Route("GetSectionByDepartment")]
        public List<SectionModel> GetSectionByDepartment(int id)
        {            
                var section = _context.Section
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Department")
                .Select(s => new SectionModel
                {
                    SectionID = s.SectionId,
                    DepartmentID = s.DepartmentId,
                    DepartmentName = s.Department.Name,
                    Name = s.Name,
                    IsWiki = s.IsWiki,
                    IsTestPaper = s.IsTestPaper,
                    HeadCount = s.HeadCount,
                    StatusCodeID = s.StatusCodeId,
                    Code = s.Code,
                    StatusCode = s.StatusCode.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedDate = s.AddedDate,
                    ProfileCode = s.ProfileCode,
                    ModifiedDate = s.ModifiedDate


            }).Where(t => t.DepartmentID == id).AsNoTracking().ToList();           
            return section;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Section")]
        [HttpGet("GetSections/{id:int}")]
        public ActionResult<SectionModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var section = _context.Section.SingleOrDefault(p => p.SectionId == id.Value);
            var result = _mapper.Map<SectionModel>(section);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SectionModel> GetData(SearchModel searchModel)
        {
            var section = new Section();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        section = _context.Section.OrderByDescending(o => o.SectionId).FirstOrDefault();
                        break;
                    case "Last":
                        section = _context.Section.OrderByDescending(o => o.SectionId).LastOrDefault();
                        break;
                    case "Next":
                        section = _context.Section.OrderByDescending(o => o.SectionId).LastOrDefault();
                        break;
                    case "Previous":
                        section = _context.Section.OrderByDescending(o => o.SectionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        section = _context.Section.OrderByDescending(o => o.SectionId).FirstOrDefault();
                        break;
                    case "Last":
                        section = _context.Section.OrderByDescending(o => o.SectionId).LastOrDefault();
                        break;
                    case "Next":
                        section = _context.Section.OrderBy(o => o.SectionId).FirstOrDefault(s => s.SectionId > searchModel.Id);
                        break;
                    case "Previous":
                        section = _context.Section.OrderByDescending(o => o.SectionId).FirstOrDefault(s => s.SectionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SectionModel>(section);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSection")]
        public SectionModel Post(SectionModel value)
        {
            var section = new Section
            {
                DepartmentId = value.DepartmentID,
                Name = value.Name,
                Code  = value.Code,
                Description = value.Description,
                IsWiki = value.IsWiki,
                IsTestPaper = value.IsTestPaper,
                HeadCount = value.HeadCount,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ProfileCode = value.ProfileCode,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.Section.Add(section);
            _context.SaveChanges();
            value.SectionID = section.SectionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSection")]
        public SectionModel Put(SectionModel value)
        {
            var section = _context.Section.SingleOrDefault(p => p.SectionId == value.SectionID);

            section.DepartmentId = value.DepartmentID;
            section.Name = value.Name;
            section.Code = value.Code;
            section.Description = value.Description;
            section.IsWiki = value.IsWiki;
            section.IsTestPaper = value.IsTestPaper;
            section.HeadCount = value.HeadCount;
            section.ModifiedByUserId = value.ModifiedByUserID;
            section.ModifiedDate = DateTime.Now;
            section.StatusCodeId = value.StatusCodeID.Value;
            section.ProfileCode = value.ProfileCode;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSection")]
        public void Delete(int id)
        {
            try
            { 
            var section = _context.Section.SingleOrDefault(p => p.SectionId == id);
            if (section != null)
            {
                _context.Section.Remove(section);
                _context.SaveChanges();
            }
            }
            catch (Exception ex)
            {
                throw new AppException("Section Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}