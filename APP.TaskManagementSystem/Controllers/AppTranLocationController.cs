﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppTranLocationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public AppTranLocationController(CRT_TMSContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        #region AppTranLocation

        // GET: api/Project
        [HttpGet]
        [Route("GetAppTranLocations")]
        public List<AppTranLocationModel> Get()
        {
            List<AppTranLocationModel> appTranLocationModels = new List<AppTranLocationModel>();
            var appTranLfromLtos = _context.AppTranLfromLto.Include(s => s.AddedByUser).Include("ModifiedByUser").OrderByDescending(o => o.Id).AsNoTracking().ToList();

            appTranLfromLtos.ForEach(s =>
            {
                AppTranLocationModel appTranLocationModel = new AppTranLocationModel()
                {
                    Id = s.Id,
                    LocationFrom = s.LocationFrom,
                    LocationTo = s.LocationTo,
                    RequisitionNo = s.RequisitionNo,
                    TransferNo = s.TransferNo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                appTranLocationModels.Add(appTranLocationModel);
            });

            return appTranLocationModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppTranLocationModel> GetData(SearchModel searchModel)
        {
            var appTranLfromLto = new AppTranLfromLto();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Previous":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranLfromLto = _context.AppTranLfromLto.OrderBy(o => o.Id).FirstOrDefault(s => s.Id > searchModel.Id);
                        break;
                    case "Previous":
                        appTranLfromLto = _context.AppTranLfromLto.OrderByDescending(o => o.Id).FirstOrDefault(s => s.Id < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppTranLocationModel>(appTranLfromLto);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppTranLocation")]
        public async Task<AppTranLocationModel> Post(AppTranLocationModel value)
        {
            AppTranLfromLto tranLfromLto = _context.AppTranLfromLto.FirstOrDefault(p => p.LocationFrom == value.LocationFrom && p.LocationTo == value.LocationTo && p.RequisitionNo == value.RequisitionNo);
            if (tranLfromLto == null)
            {
                tranLfromLto = new AppTranLfromLto
                {
                    LocationFrom = value.LocationFrom,
                    LocationTo = value.LocationTo,
                    RequisitionNo = value.RequisitionNo,
                    TransferNo = value.TransferNo,
                    StatusCodeId = 1,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.AppTranLfromLto.Add(tranLfromLto);
            }
            else
            {
                tranLfromLto.LocationFrom = value.LocationFrom;
                tranLfromLto.LocationTo = value.LocationTo;
                tranLfromLto.RequisitionNo = value.RequisitionNo;
                tranLfromLto.TransferNo = value.TransferNo;
                tranLfromLto.ModifiedByUserId = value.AddedByUserID;
                tranLfromLto.ModifiedDate = DateTime.Now;
                tranLfromLto.StatusCodeId = 1;
            }
            _context.SaveChanges();

            value.Id = tranLfromLto.Id;
            return value;
        }

        [HttpPost]
        [Route("PostLocationToNav")]
        public async Task<AppTranLocationModel> PostLocationToNav(AppTranLocationModel value)
        {
            try
            {
                var locationItem = await _context.AppTranLfromLto.Include(c => c.AppTranLfromLtoLine).FirstOrDefaultAsync(t => t.Id == value.Id);
                if (locationItem != null)
                {
                    var context = new DataService(_config, value.CompanyName);
                    var guid = Guid.NewGuid();
                    int onlyThisAmount = 8;
                    string ticks = DateTime.Now.Ticks.ToString();
                    ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                    int entryNumber = int.Parse(ticks);
                    locationItem.AppTranLfromLtoLine.ToList().ForEach(cons =>
                    {

                        // SW Reference
                        var consumption = new CRTIMS_TransferEntry()
                        {
                            Entry_No = entryNumber,
                            Created_by = value.AddedByUser,
                            Description = cons.ItemNo,
                            Entry_Type = "Create",
                            Item_No = cons.ItemNo,
                            Created_on = DateTime.Now,
                            Lot_No = cons.LotNo,
                            // QC_Ref_No = cons.QcrefNo,
                            Order_Line_No = 0,
                            Posting_Date = DateTime.Now,
                            Posted_by = value.AddedByUser,
                            Session_ID = guid,
                            Transfer_from_Code = locationItem.LocationFrom,
                            Transfer_to_Code = locationItem.LocationTo,
                            Quantity = cons.Qty,
                            Session_User_ID = value.AddedByUser,
                        };
                        context.Context.AddToCRTIMS_TransferEntry(consumption);

                    });
                    TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                    var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                    var post = new TransferEntryService.CRTIMS_PostTransferEntry_PortClient();

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/CRTIMS_PostTransferEntry"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                  System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    var transferNo = await post.FnRunFromAppsAsync(entryNumber);
                    locationItem.TransferNo = transferNo.return_value;
                    value.TransferNo = transferNo.return_value;
                    _context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppTranLocation")]
        public AppTranLocationModel Put(AppTranLocationModel value)
        {
            var tranLfromLto = _context.AppTranLfromLto.SingleOrDefault(p => p.Id == value.Id);
            tranLfromLto.LocationFrom = value.LocationFrom;
            tranLfromLto.LocationTo = value.LocationTo;
            tranLfromLto.RequisitionNo = value.RequisitionNo;
            tranLfromLto.TransferNo = value.TransferNo;
            tranLfromLto.ModifiedByUserId = value.ModifiedByUserID;
            tranLfromLto.ModifiedDate = DateTime.Now;
            tranLfromLto.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppTranLocation")]
        public void Delete(int id)
        {
            var appTranLfromLto = _context.AppTranLfromLto.SingleOrDefault(p => p.Id == id);
            var errorMessage = "";
            try
            {
                if (appTranLfromLto != null)
                {
                    _context.AppTranLfromLto.Remove(appTranLfromLto);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        #endregion

        #region AppTranLocationLine

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPTranLocationLines")]
        public List<AppTranLocationLineModel> GetAPPTranLocationLines()
        {
            var locationLines = _context.AppTranLfromLtoLine.Include(a => a.AddedByUser).Select(s => new AppTranLocationLineModel
            {
                Id = s.Id,
                AppTranLfromLtoId = s.AppTranLfromLtoId,
                ItemNo = s.ItemNo,
                LotNo = s.LotNo,
                TotalLableQty = s.TotalLableQty,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                AddedDate = s.AddedDate,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty
            }).OrderByDescending(o => o.Id).AsNoTracking().ToList();
            return locationLines;
        }



        [HttpGet]
        [Route("GetAPPTranLocationLinesById")]
        public List<AppTranLocationLineModel> GetAPPTranLocationLinesById(int id)
        {
            List<AppTranLocationLineModel> locationlineModels = new List<AppTranLocationLineModel>();
            var locationlines = _context.AppTranLfromLtoLine.Include(a => a.AddedByUser).AsNoTracking().Where(t => t.AppTranLfromLtoId == id).ToList();
            locationlines.ForEach(s =>
            {
                var locationLineModel = new AppTranLocationLineModel
                {
                    Id = s.Id,
                    AppTranLfromLtoId = s.AppTranLfromLtoId,
                    ItemNo = s.ItemNo,
                    LotNo = s.LotNo,
                    Qty = s.Qty,
                    TotalLableQty = s.TotalLableQty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty

                };
                locationlineModels.Add(locationLineModel);
            });


            return locationlineModels.OrderByDescending(o => o.Id).ToList();
        }


        [HttpPost()]
        [Route("GetDataLine")]
        public ActionResult<AppTranLocationLineModel> GetDataLine(SearchModel searchModel)
        {
            var tranLfromLtoLine = new AppTranLfromLtoLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Previous":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderBy(o => o.Id).FirstOrDefault(s => s.Id > searchModel.Id);
                        break;
                    case "Previous":
                        tranLfromLtoLine = _context.AppTranLfromLtoLine.OrderByDescending(o => o.Id).FirstOrDefault(s => s.Id < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppTranLocationLineModel>(tranLfromLtoLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppTranLocationLine")]
        public async Task<AppTranLocationLineModel> Post(AppTranLocationLineModel value)
        {
            var isExist = _context.AppTranLfromLtoLine.Any(t => t.AppTranLfromLtoId == value.AppTranLfromLtoId && t.ItemNo == value.ItemNo);

            if (!isExist)
            {
                var appTranLfromLtoLine = new AppTranLfromLtoLine
                {
                    AppTranLfromLtoId = value.AppTranLfromLtoId,
                    ItemNo = value.ItemNo,
                    LotNo = value.LotNo,
                    Qty = value.Qty,
                    TotalLableQty = value.TotalLableQty,
                    AddedByUserId = value.AddedByUserID,
                    StatusCodeId = 1,
                    AddedDate = DateTime.Now,

                };
                _context.AppTranLfromLtoLine.Add(appTranLfromLtoLine);
                _context.SaveChanges();
                value.Id = appTranLfromLtoLine.Id;
                return value;
            }
            else
            {
                value.IsError = true;
                value.Errormessage = "Transfer Location for this item no. already scanned.";
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTranLocationLine")]
        public AppTranLocationLineModel Put(AppTranLocationLineModel value)
        {
            var appTranLfromLtoLine = _context.AppTranLfromLtoLine.SingleOrDefault(p => p.Id == value.Id);
            appTranLfromLtoLine.AppTranLfromLtoId = value.AppTranLfromLtoId;
            appTranLfromLtoLine.ItemNo = value.ItemNo;
            appTranLfromLtoLine.Qty = value.Qty;
            appTranLfromLtoLine.TotalLableQty = value.TotalLableQty;
            appTranLfromLtoLine.LotNo = value.LotNo;
            appTranLfromLtoLine.ModifiedDate = DateTime.Now;
            appTranLfromLtoLine.ModifiedByUserId = value.ModifiedByUserID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTranLocationLine")]
        public void DeleteTranLocationLine(int id)
        {
            var appTranLfromLtoLine = _context.AppTranLfromLtoLine.SingleOrDefault(p => p.Id == id);
            var errorMessage = "";
            try
            {
                if (appTranLfromLtoLine != null)
                {
                    _context.AppTranLfromLtoLine.Remove(appTranLfromLtoLine);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
        #endregion
    }
}
