﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProjectedDeliveryBlanketOthersController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProjectedDeliveryBlanketOthersController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProjectedDeliverys")]
        public List<ProjectedDeliveryBlanketOthersModel> Get(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.PurchaseItemSalesEntryLineId == id)
                            .AsNoTracking().ToList();
            List<ProjectedDeliveryBlanketOthersModel> ProjectedDeliveryBlanketOthersModel = new List<ProjectedDeliveryBlanketOthersModel>();
            ProjectedDeliveryBlanketOthers.ForEach(s =>
            {
                ProjectedDeliveryBlanketOthersModel ProjectedDeliveryBlanketOthersModels = new ProjectedDeliveryBlanketOthersModel
                {
                    PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId,
                    ProjectedDeliverySalesOrderLineID = s.ProjectedDeliverySalesOrderLineId,
                    FrequencyID = s.FrequencyId,
                    PerFrequencyQty = s.PerFrequencyQty,
                    IsQtyReference = s.IsQtyReference,
                    StartDate = s.StartDate,
                    UomId = s.UomId,
                    Uom = masterDetailList != null && s.UomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.UomId).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Frequency = masterDetailList != null && s.FrequencyId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.FrequencyId).Select(a => a.Value).SingleOrDefault() : "",
                };
                ProjectedDeliveryBlanketOthersModel.Add(ProjectedDeliveryBlanketOthersModels);
            });
            return ProjectedDeliveryBlanketOthersModel.OrderByDescending(a => a.ProjectedDeliverySalesOrderLineID).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProjectedDeliveryBlanketOthersModel> GetData(SearchModel searchModel)
        {
            var ProjectedDeliveryBlanketOthers = new ProjectedDeliveryBlanketOthers();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Previous":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderBy(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault(s => s.ProjectedDeliverySalesOrderLineId > searchModel.Id);
                        break;
                    case "Previous":
                        ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.OrderByDescending(o => o.ProjectedDeliverySalesOrderLineId).FirstOrDefault(s => s.ProjectedDeliverySalesOrderLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProjectedDeliveryBlanketOthersModel>(ProjectedDeliveryBlanketOthers);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProjectedDelivery")]
        public ProjectedDeliveryBlanketOthersModel Post(ProjectedDeliveryBlanketOthersModel value)
        {

            var ProjectedDeliveryBlanketOthers = new ProjectedDeliveryBlanketOthers
            {

                PurchaseItemSalesEntryLineId = value.PurchaseItemSalesEntryLineID,
                FrequencyId = value.FrequencyID,
                PerFrequencyQty = value.PerFrequencyQty,
                StartDate = value.StartDate,
                UomId = value.UomId,
                IsQtyReference = value.IsQtyReference,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,


            };
            _context.ProjectedDeliveryBlanketOthers.Add(ProjectedDeliveryBlanketOthers);
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.Frequency = masterDetailList != null && value.FrequencyID > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.FrequencyID).Select(a => a.Value).SingleOrDefault() : "";
            value.Uom = masterDetailList != null && value.UomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.UomId).Select(a => a.Value).SingleOrDefault() : "";
            value.ProjectedDeliverySalesOrderLineID = ProjectedDeliveryBlanketOthers.ProjectedDeliverySalesOrderLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProjectedDelivery")]
        public ProjectedDeliveryBlanketOthersModel Put(ProjectedDeliveryBlanketOthersModel value)
        {
            var ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.SingleOrDefault(p => p.ProjectedDeliverySalesOrderLineId == value.ProjectedDeliverySalesOrderLineID);


            ProjectedDeliveryBlanketOthers.PurchaseItemSalesEntryLineId = value.PurchaseItemSalesEntryLineID;
            ProjectedDeliveryBlanketOthers.FrequencyId = value.FrequencyID;
            ProjectedDeliveryBlanketOthers.PerFrequencyQty = value.PerFrequencyQty;
            ProjectedDeliveryBlanketOthers.StartDate = value.StartDate;
            ProjectedDeliveryBlanketOthers.IsQtyReference = value.IsQtyReference;
            ProjectedDeliveryBlanketOthers.UomId = value.UomId;
            ProjectedDeliveryBlanketOthers.StatusCodeId = value.StatusCodeID.Value;
            ProjectedDeliveryBlanketOthers.ModifiedByUserId = value.ModifiedByUserID;
            ProjectedDeliveryBlanketOthers.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.Frequency = masterDetailList != null && value.FrequencyID > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.FrequencyID).Select(a => a.Value).SingleOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProjectedDelivery")]
        public void Delete(int id)
        {
            var ProjectedDeliveryBlanketOthers = _context.ProjectedDeliveryBlanketOthers.SingleOrDefault(p => p.ProjectedDeliverySalesOrderLineId == id);
            if (ProjectedDeliveryBlanketOthers != null)
            {
                _context.ProjectedDeliveryBlanketOthers.Remove(ProjectedDeliveryBlanketOthers);
                _context.SaveChanges();
            }
        }
    }
}