﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DeviceGroupListController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DeviceGroupListController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetDeviceGroupLists")]
        public List<DeviceGroupListModel> Get()
        {
            var DeviceGroupList = _context.DeviceGroupList.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<DeviceGroupListModel> deviceGroupListModel = new List<DeviceGroupListModel>();
            DeviceGroupList.ForEach(s =>
            {
                DeviceGroupListModel deviceGroupListModels = new DeviceGroupListModel
                {
                    DeviceGroupListID = s.DeviceGroupListId,
                    DeviceGroupName = s.DeviceGroupName,
                    DeviceGroupNo = s.DeviceGroupNo,
                    Appreviation = s.Appreviation,
                    UOM = s.Uom,
                    IsRequire = s.IsRequire.Value,
                    CalibrationTypeID = s.CalibrationTypeId,
                    CalibrationName = s.CalibrationType?.Name,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                deviceGroupListModel.Add(deviceGroupListModels);
            });
            return deviceGroupListModel.OrderByDescending(a => a.DeviceGroupListID).ToList();
        }
        [HttpGet]
        [Route("GetActiveDeviceGroupList")]
        public List<DeviceGroupListModel> GetActiveDeviceGroupList()
        {
            var DeviceGroupList = _context.DeviceGroupList.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Where(a => a.StatusCodeId == 1).AsNoTracking().ToList();
            List<DeviceGroupListModel> deviceGroupListModel = new List<DeviceGroupListModel>();
            DeviceGroupList.ForEach(s =>
            {
                DeviceGroupListModel deviceGroupListModels = new DeviceGroupListModel
                {
                    DeviceGroupListID = s.DeviceGroupListId,
                    DeviceGroupName = s.DeviceGroupName,
                    DeviceGroupNo = s.DeviceGroupNo,
                    Appreviation = s.Appreviation,
                    UOM = s.Uom,
                    IsRequire = s.IsRequire.Value,
                    CalibrationTypeID = s.CalibrationTypeId.Value,
                    CalibrationName = s.CalibrationType.Name,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                deviceGroupListModel.Add(deviceGroupListModels);
            });
            return deviceGroupListModel.OrderByDescending(a => a.DeviceGroupListID).ToList();
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<DeviceGroupListModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var deviceGroupList = _context.DeviceGroupList.SingleOrDefault(p => p.DeviceGroupListId == id.Value);
            var result = _mapper.Map<DeviceGroupListModel>(deviceGroupList);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DeviceGroupListModel> GetData(SearchModel searchModel)
        {
            var deviceGroupList = new DeviceGroupList();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).LastOrDefault();
                        break;
                    case "Next":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).LastOrDefault();
                        break;
                    case "Previous":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).FirstOrDefault();
                        break;
                    case "Last":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).LastOrDefault();
                        break;
                    case "Next":
                        deviceGroupList = _context.DeviceGroupList.OrderBy(o => o.DeviceGroupListId).FirstOrDefault(s => s.DeviceGroupListId > searchModel.Id);
                        break;
                    case "Previous":
                        deviceGroupList = _context.DeviceGroupList.OrderByDescending(o => o.DeviceGroupListId).FirstOrDefault(s => s.DeviceGroupListId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DeviceGroupListModel>(deviceGroupList);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDeviceGroupList")]
        public DeviceGroupListModel Post(DeviceGroupListModel value)
        {
            var deviceGroupList = new DeviceGroupList
            {

                DeviceGroupName = value.DeviceGroupName,
                DeviceGroupNo = value.DeviceGroupNo,
                Appreviation = value.Appreviation,
                Uom = value.UOM,
                IsRequire = value.IsRequire,
                CalibrationTypeId = value.CalibrationTypeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.DeviceGroupList.Add(deviceGroupList);
            _context.SaveChanges();
            value.DeviceGroupListID = deviceGroupList.DeviceGroupListId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDeviceGroupList")]
        public DeviceGroupListModel Put(DeviceGroupListModel value)
        {
            var deviceGroupList = _context.DeviceGroupList.SingleOrDefault(p => p.DeviceGroupListId == value.DeviceGroupListID);
            deviceGroupList.DeviceGroupNo = value.DeviceGroupNo;
            deviceGroupList.DeviceGroupName = value.DeviceGroupName;
            deviceGroupList.Appreviation = value.Appreviation;
            deviceGroupList.Uom = value.UOM;
            deviceGroupList.IsRequire = value.IsRequire;
            deviceGroupList.ModifiedByUserId = value.ModifiedByUserID;
            deviceGroupList.ModifiedDate = DateTime.Now;
            deviceGroupList.StatusCodeId = value.StatusCodeID.Value;
            deviceGroupList.CalibrationTypeId = value.CalibrationTypeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDeviceGroupList")]
        public void Delete(int id)
        {
            var deviceGroupList = _context.DeviceGroupList.SingleOrDefault(p => p.DeviceGroupListId == id);
            if (deviceGroupList != null)
            {
                _context.DeviceGroupList.Remove(deviceGroupList);
                _context.SaveChanges();
            }
        }
    }
}