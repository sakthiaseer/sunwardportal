﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionCapsuleLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionCapsuleLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("ProductionCapsuleLinesByID")]
        public List<ProductionCapsuleLineModel> Get(int id)
        {
            List<ProductionCapsuleLineModel> productionCapsuleLineModels = new List<ProductionCapsuleLineModel>();
            var productionCapsuleLine = _context.ProductionCapsuleLine.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").Where(t => t.ProductionCapsuleId == id).OrderByDescending(o => o.ProductionCapsuleLineId).AsNoTracking().ToList();
            if(productionCapsuleLine!=null && productionCapsuleLine.Count>0)
            {
                List<long?> masterIds = productionCapsuleLine.Where(w => w.DatabseRequireId != null).Select(a => a.DatabseRequireId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                List<long?> navisionIds = productionCapsuleLine.Where(w => w.NavisionId != null).Select(a => a.NavisionId).Distinct().ToList();
                var items = _context.Navitems.Where(w => navisionIds.Contains(w.ItemId)).AsNoTracking().ToList();
                productionCapsuleLine.ForEach(s =>
                {
                    ProductionCapsuleLineModel productionCapsuleLineModel = new ProductionCapsuleLineModel
                    {
                        ProductionCapsuleLineID = s.ProductionCapsuleLineId,
                        ProductionCapsuleID = s.ProductionCapsuleId,
                        DatabseRequireID = s.DatabseRequireId,
                        NavisionID = s.NavisionId,
                        DatabseRequire = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DatabseRequireId).Select(a => a.Value).SingleOrDefault() : "",
                        //Navision = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.NavisionId).Select(a => a.Value).SingleOrDefault() : "",
                        Navision = items!=null? items.Where(t => t.ItemId == s.NavisionId).Select(t => t.No).FirstOrDefault() : "",
                        BUOM = s.Buom,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionCapsuleLineModels.Add(productionCapsuleLineModel);
                });
            }
           
            return productionCapsuleLineModels;
        }     



        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionCapsuleLineModel> GetData(SearchModel searchModel)
        {
            var productionCapsuleLine = new ProductionCapsuleLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).LastOrDefault();
                        break;
                    case "Previous":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderBy(o => o.ProductionCapsuleLineId).FirstOrDefault(s => s.ProductionCapsuleLineId > searchModel.Id);
                        break;
                    case "Previous":
                        productionCapsuleLine = _context.ProductionCapsuleLine.OrderByDescending(o => o.ProductionCapsuleLineId).FirstOrDefault(s => s.ProductionCapsuleLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionCapsuleLineModel>(productionCapsuleLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionCapsuleLine")]
        public ProductionCapsuleLineModel Post(ProductionCapsuleLineModel value)
        {
            var productionCapsuleLine = new ProductionCapsuleLine
            {
                ProductionCapsuleId = value.ProductionCapsuleID,
                DatabseRequireId = value.DatabseRequireID,
                NavisionId = value.NavisionID,
                Buom = value.BUOM,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.ProductionCapsuleLine.Add(productionCapsuleLine);
            _context.SaveChanges();
            value.ProductionCapsuleLineID = productionCapsuleLine.ProductionCapsuleLineId;
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionCapsuleLine")]
        public ProductionCapsuleLineModel Put(ProductionCapsuleLineModel value)
        {
            var productionCapsuleLine = _context.ProductionCapsuleLine.SingleOrDefault(p => p.ProductionCapsuleLineId == value.ProductionCapsuleLineID);
            productionCapsuleLine.ProductionCapsuleId = value.ProductionCapsuleID;
            productionCapsuleLine.DatabseRequireId = value.DatabseRequireID;
            productionCapsuleLine.NavisionId = value.NavisionID;
            productionCapsuleLine.Buom = value.BUOM;
            productionCapsuleLine.ModifiedByUserId = value.ModifiedByUserID;
            productionCapsuleLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionCapsuleLine")]
        public void Delete(int id)
        {
            var productionCapsuleLine = _context.ProductionCapsuleLine.SingleOrDefault(p => p.ProductionCapsuleLineId == id);
            if (productionCapsuleLine != null)
            {
                _context.ProductionCapsuleLine.Remove(productionCapsuleLine);
                _context.SaveChanges();
            }
        }
    }
}