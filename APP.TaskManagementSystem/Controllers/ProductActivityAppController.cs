﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using APP.Common;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.AspNetCore.SignalR;
using APP.BussinessObject;
using DocumentFormat.OpenXml.Office2010.Excel;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;
using DocumentFormat.OpenXml;
using APP.DataAccess.Models.Views;
using NAV;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductActivityAppController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public ProductActivityAppController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [HttpGet]
        [Route("GetNavprodOrderLineAll")]
        public List<NavprodOrderLineModel> GetNavprodOrderLineAll(long? id)
        {
            var productActivityApps = _context.NavprodOrderLine.Where(w => w.OrderLineNo > 0 && w.CompanyId == id).ToList();
            List<NavprodOrderLineModel> productActivityAppModels = new List<NavprodOrderLineModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                productActivityApps.ForEach(s =>
                {
                    NavprodOrderLineModel productActivityApp = new NavprodOrderLineModel();
                    productActivityApp.OrderLineNo = s.OrderLineNo;
                    productActivityApp.OutputQty = s.OutputQty;
                    productActivityApp.CompletionDate = s.CompletionDate;
                    productActivityApp.StartDate = s.StartDate;
                    productActivityApp.Status = s.Status;
                    productActivityApp.Name = s.ProdOrderNo + "||" + s.BatchNo + "||" + s.Description;
                    productActivityApp.Name = s.Description + (string.IsNullOrEmpty(s.Description1) ? "" : (" | " + s.Description1));
                    productActivityApp.ProdOrderNo = s.ProdOrderNo;
                    productActivityApp.ItemNo = s.ItemNo;
                    productActivityApp.Description = s.Description;
                    productActivityApp.Description1 = s.Description1;
                    productActivityApp.BatchNo = s.BatchNo;
                    productActivityApp.RePlanRefNo = s.RePlanRefNo;
                    productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                    productActivityApp.BatchNo = s.BatchNo;
                    productActivityAppModels.Add(productActivityApp);
                });
            }
            return productActivityAppModels;
        }
        [HttpPost]
        [Route("GetProductionActivityAppFilter")]
        public List<ProductActivityAppModel> GetProductionActivityAppFilter(SearchModel searchModel)
        {
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            var activityMasterMultiplelist = _context.ActivityMasterMultiple.ToList();
            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("ProductionActivity")).ToList();
            var productActivityApps = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcess)
                .Include(a => a.ProdActivityCategory)
                .Include(a => a.ProdActivityAction)
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp.Company)
                .Include(a => a.StatusCode)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.QaCheckUser)
                 .Include(a => a.NavprodOrderLine)
                 .Include(a => a.ActivityMasterMultiple).Where(a=>a.SessionId == searchModel.SessionID)
           
                .ToList();


            var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
            var activityCaseList = _context.ProductActivityCase.ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            var productActivitycaseresponslist = _context.ProductActivityCaseRespons.ToList();
            var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
            var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
            var emplist = _context.Employee.ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 340, 341, 342 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }

            if (productActivityApps != null)
            {
                ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();
                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityAppLineDoc.Where(w => w.Type == "Production Activity").ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
                var appUsersList = _context.ApplicationUser.ToList();

                productActivityApps.ToList().ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    var currentActivityResponseIds = ResponseDutyIds.Where(r => r.ProductActivityCaseResponsId == s.ProductionActivityAppLineId).Select(r => r.ProductActivityCaseResponsDutyId).ToList();
                    List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var responseId = productActivitycaseresponslist.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseId)).Select(r => r.ProductActivityCaseResponsId).ToList();
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => responseId.Contains(r.ProductActivityCaseResponsId.Value)).Select(t => t.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {

                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId.Value).ToList();
                            //var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            //var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            //var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (empIds != null && empIds.Count > 0)
                            {

                                responsibilityUsers.AddRange(empIds);

                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            // ProductActivityPermissions

                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d => {
                                    ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();


                                    ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    ProductActivityPermissionData.IsMail = d.IsMail;
                                    ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    ProductActivityPermissionData.UserID = d.UserId;
                                    ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    ProductActivityPermissions.Add(ProductActivityPermissionData);



                                });
                            }
                        }
                    }
                    else
                    {
                        responsibilityUsers.Add(searchModel.UserID);
                    }
                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.ActivityMasterIds = s.ActivityMasterMultiple.Where(t => t.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(t => t.AcitivityMasterId).ToList();
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Activity" && w.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Count();
                    // productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId, s);
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                    productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.ProdOrderNo = s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;

                    productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                    productActivityApp.Description = s.NavprodOrderLine?.Description;
                    productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                    productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                    productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                    productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.NavprodOrderLine?.BatchNo + "||" + s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    if (s.IsOthersOptions == true)
                    {
                        productActivityApp.ProdOrderNoDesc = "Other";
                    }
                    else
                    {
                        productActivityApp.ProdOrderNoDesc = s.NavprodOrderLine?.ProdOrderNo + "|" + s.NavprodOrderLine?.Description + (string.IsNullOrEmpty(s.NavprodOrderLine?.Description1) ? "" : (" | " + s.NavprodOrderLine?.Description1));

                    }
                    productActivityApp.Type = "Production Activity";
                    //var qaUserList = productActivityApp.ProductionActivityAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == userid).FirstOrDefault();
                    //if (qaUserList != null)
                    //{
                    //    productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                    //    productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                    //    productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    //}
                    if (productActivityApp.ProdActivityResultId != null)
                    {
                        productActivityApp.ActivityResult = productActivityApp.ProdActivityResultId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == productActivityApp.ProdActivityResultId).FirstOrDefault()?.Value : "";

                    }
                    if (productActivityApp.ActivityMasterIds != null && productActivityApp.ActivityMasterIds.Count > 0)
                    {
                        productActivityApp.ActivityMaster = productActivityApp.ActivityMasterIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.ActivityMasterIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }

                    productActivityApp.ActivityStatus = s.ActivityStatusId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.ActivityStatusId).FirstOrDefault()?.Value : "";
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();

                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();

                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            productActivityApp.FilePath = counts.FilePath;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == searchModel.UserID)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if (ProductActivityPermissions != null && ProductActivityPermissions.Count > 0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {


                            if (d.ProductActivityPermissionId > 0)
                            {
                                if (searchModel.UserID == d.UserID)
                                {
                                    productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                    productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    productActivityApp.IsActionPermission = true;
                                }



                            }
                            else
                            {
                                productActivityApp.IsActionPermission = false;
                            }
                        });

                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }

                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(searchModel.UserID))
                        {
                            productActivityAppModels.Add(productActivityApp);
                            // productActivityApps.RowCount = productActivityAppModels.Count;
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                        // productActivityApps.RowCount = productActivityAppModels.Count;
                    }

                });
            }
            productActivityAppModels = productActivityAppModels.OrderByDescending(s => s.AddedDate).ToList();
            return productActivityAppModels;
        }

            [HttpGet]
        [Route("GetProductActivityApp")]
        public List<ProductActivityAppModel> GetProductActivityApp(long? id)
        {
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            var activityMasterMultiplelist = _context.ActivityMasterMultiple.ToList();
            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("ProductionActivity")).ToList();
            var productActivityApps = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)     
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp.Company)              
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)               
                 .Include(a => a.NavprodOrderLine)               
                .ToList();

           
            var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
            var activityCaseList = _context.ProductActivityCase.ToList();
            var activityResultList = _context.ActivityResultMultiple.ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
            var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
            var emplist = _context.Employee.ToList();
            var productActivitycaseresponslist = _context.ProductActivityCaseRespons.ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 340, 341, 342, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
      
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
           
            if (productActivityApps != null)
            {
               
                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityAppLineDoc.Where(w => w.Type == "Production Activity").ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();               
                var appUsersList = _context.ApplicationUser.ToList();
              
                productActivityApps.ToList().ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    //var currentActivityResponseIds = ResponseDutyIds.Where(r => r.ProductActivityCaseResponsId == s.ProductionActivityAppLineId).Select(r => r.ProductActivityCaseResponsDutyId).ToList();
                    List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    var activityResultIds = activityResultList.Where(a=>a.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(a=>a.AcitivityResultId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var responseId = productActivitycaseresponslist.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseId)).Select(r => r.ProductActivityCaseResponsId).ToList();
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => responseId.Contains(r.ProductActivityCaseResponsId.Value)).Select(t=>t.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {

                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId.Value).ToList();
                            //var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            //var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            //var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (empIds != null && empIds.Count > 0)
                            {
                               
                                    responsibilityUsers.AddRange(empIds);
                                
                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            // ProductActivityPermissions
                           
                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d => { 
                                ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();
                                   

                                        ProductActivityPermissionData.IsChecker = d.IsChecker;
                                        ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                        ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                        ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                        ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                        ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                        ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                        ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                        ProductActivityPermissionData.IsMail = d.IsMail;
                                        ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                        ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                        ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                        ProductActivityPermissionData.UserID = d.UserId;
                                        ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                        ProductActivityPermissions.Add(ProductActivityPermissionData);



                                });
                            }
                        }
                       
                    }
                    else
                    {
                        responsibilityUsers.Add(id.Value);
                    }
                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.ActivityMasterIds = activityMasterMultiplelist.Where(t => t.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(t => t.AcitivityMasterId).ToList();
                    productActivityApp.ActivityResultIds = activityResultList.Where(t => t.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(t=>t.AcitivityResultId).ToList();
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Activity" && w.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Count();
                   // productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId, s);
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                    productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    //productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    //productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                    //productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    //productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }                    //productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.ProdOrderNo = s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;

                    productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                    productActivityApp.Description = s.NavprodOrderLine?.Description;
                    productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                    productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                    productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                    productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                    productActivityApp.ProfileId = s.ProfileId;
                    productActivityApp.ProfileNo = s.ProfileNo;
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.NavprodOrderLine?.BatchNo +"||"+ s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument; 
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    if (s.IsOthersOptions == true)
                    {
                        productActivityApp.ProdOrderNoDesc = "Other";
                    }
                    else
                    {
                        productActivityApp.ProdOrderNoDesc = s.NavprodOrderLine?.ProdOrderNo + "|" + s.NavprodOrderLine?.Description + (string.IsNullOrEmpty(s.NavprodOrderLine?.Description1) ? "" : (" | " + s.NavprodOrderLine?.Description1));

                    }
                    productActivityApp.Type = "Production Activity";
                    var qaUserList = productActivityApp.ProductionActivityAppLineQaCheckerModels?.ToList()?.Where(w => w.QaCheckUserId == id).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (productActivityApp.ProdActivityResultId != null)
                    {
                        productActivityApp.ActivityResult = productActivityApp.ProdActivityResultId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == productActivityApp.ProdActivityResultId).FirstOrDefault()?.Value : "";

                    }
                    if (productActivityApp.ActivityResultIds !=null && productActivityApp.ActivityResultIds.Count>0)
                    {
                        productActivityApp.ActivityResult = productActivityApp.ActivityResultIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.ActivityResultIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";
                    }
                    if (productActivityApp.ActivityMasterIds != null && productActivityApp.ActivityMasterIds.Count > 0)
                    {
                        productActivityApp.ActivityMaster = productActivityApp.ActivityMasterIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.ActivityMasterIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }

                    productActivityApp.ActivityStatus = s.ActivityStatusId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.ActivityStatusId).FirstOrDefault()?.Value : "";
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();

                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();

                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            productActivityApp.FilePath = counts.FilePath;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == id)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if(ProductActivityPermissions != null && ProductActivityPermissions.Count>0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {

                       
                        if (d.ProductActivityPermissionId > 0)
                        {
                            if (id == d.UserID)
                            {
                                productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    productActivityApp.IsActionPermission = true;
                                }
                           


                        }
                        else
                        {
                            productActivityApp.IsActionPermission = false;
                        }
                        });
                       
                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }
                    
                    
                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(id.Value))
                        {
                            productActivityAppModels.Add(productActivityApp);
                            // productActivityApps.RowCount = productActivityAppModels.Count;
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                        // productActivityApps.RowCount = productActivityAppModels.Count;
                    }

                });
            }
            productActivityAppModels = productActivityAppModels.OrderByDescending(s => s.AddedDate).ToList();
            return productActivityAppModels;
        }

        [HttpPost]
        [Route("GetProductActivityAppSearch")]
        public List<ProductActivityAppModel> GetProductActivityAppSearch(SearchModel searchModel)
        {
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            var activityMasterMultiplelist = _context.ActivityMasterMultiple.ToList();
            var activityResultList = _context.ActivityResultMultiple.ToList();
            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("ProductionActivity")).ToList();
            var productActivityApps = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
              
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp.Company)
              
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
               
                 .Include(a => a.NavprodOrderLine)
                
                .ToList();

            var productActivitycaseresponslist = _context.ProductActivityCaseRespons.ToList();
            List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
            var activityCaseList = _context.ProductActivityCase.ToList();
            var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
            var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            var emplist = _context.Employee.ToList();
            var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 340, 341, 342, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            if (searchModel.ClassificationTypeId != null && searchModel.ClassificationTypeId > 0)
            {
                productActivityApps = productActivityApps.Where(w => w.ProductionActivityAppLineId == searchModel.ClassificationTypeId).ToList();
            }
            if (searchModel.ProductActivityAppSearchModel != null)
            {
                if (searchModel.ProductActivityAppSearchModel.CompanyId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductionActivityApp.CompanyId == searchModel.ProductActivityAppSearchModel.CompanyId).ToList();
                }
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.ProdOrderNo))
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductionActivityApp.ProdOrderNo == searchModel.ProductActivityAppSearchModel.ProdOrderNo).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.NavprodOrderLineId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.NavprodOrderLineId == searchModel.ProductActivityAppSearchModel.NavprodOrderLineId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ManufacturingProcessId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ManufacturingProcessChildId == searchModel.ProductActivityAppSearchModel.ManufacturingProcessId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityCategoryChildId == searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityActionId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityActionChildD == searchModel.ProductActivityAppSearchModel.ProdActivityActionId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.LocationId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.LocationId == searchModel.ProductActivityAppSearchModel.LocationId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductActivityCaseLineId == searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityResultId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityResultId == searchModel.ProductActivityAppSearchModel.ProdActivityResultId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ActivityStatusId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ActivityStatusId == searchModel.ProductActivityAppSearchModel.ActivityStatusId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.BatchNo != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.NavprodOrderLine?.BatchNo == searchModel.ProductActivityAppSearchModel.BatchNo).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.StatusCodeID != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.StatusCodeId == searchModel.ProductActivityAppSearchModel.StatusCodeID).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.StartDate != null && searchModel.ProductActivityAppSearchModel.EndDate == null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate == null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value && w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value).ToList();
                }

            }

            if (productActivityApps != null)
            {
                ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();
                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityAppLineDoc.Where(w => w.Type == "Production Activity").ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
                var appUsersList = _context.ApplicationUser.ToList();

                productActivityApps.ToList().ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    var currentActivityResponseIds = ResponseDutyIds.Where(r => r.ProductActivityCaseResponsId == s.ProductionActivityAppLineId).Select(r => r.ProductActivityCaseResponsDutyId).ToList();

                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var responseId = productActivitycaseresponslist.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseId)).Select(r => r.ProductActivityCaseResponsId).ToList();
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => responseId.Contains(r.ProductActivityCaseResponsId.Value)).Select(t => t.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {

                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId.Value).ToList();
                            //var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            //var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            //var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (empIds != null && empIds.Count > 0)
                            {

                                responsibilityUsers.AddRange(empIds);

                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            // ProductActivityPermissions

                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d => {
                                    ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();


                                    ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    ProductActivityPermissionData.IsMail = d.IsMail;
                                    ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    ProductActivityPermissionData.UserID = d.UserId;
                                    ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    ProductActivityPermissions.Add(ProductActivityPermissionData);



                                });
                            }
                        }
                    }
                    else
                    {
                        responsibilityUsers.Add(searchModel.UserID);
                    }
                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.ActivityResultIds = activityResultList.Where(t => t.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(t => t.AcitivityResultId).ToList();

                    productActivityApp.ActivityMasterIds = activityMasterMultiplelist?.Where(t => t.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(t => t.AcitivityMasterId).ToList();
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Activity" && w.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Count();
                    // productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId, s);
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                    productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.ProdOrderNo = s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;

                    productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                    productActivityApp.Description = s.NavprodOrderLine?.Description;
                    productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                    productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                    productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                    productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.NavprodOrderLine?.BatchNo + "||" + s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.ProfileId = s.ProfileId;
                    productActivityApp.ProfileNo = s.ProfileNo;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    if (s.IsOthersOptions == true)
                    {
                        productActivityApp.ProdOrderNoDesc = "Other";
                    }
                    else
                    {
                        productActivityApp.ProdOrderNoDesc = s.NavprodOrderLine?.ProdOrderNo + "|" + s.NavprodOrderLine?.Description + (string.IsNullOrEmpty(s.NavprodOrderLine?.Description1) ? "" : (" | " + s.NavprodOrderLine?.Description1));

                    }
                    productActivityApp.Type = "Production Activity";
                    var qaUserList = productActivityApp.ProductionActivityAppLineQaCheckerModels?.ToList()?.Where(w => w.QaCheckUserId == searchModel.UserID).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (productActivityApp.ProdActivityResultId != null)
                    {
                        productActivityApp.ActivityResult = productActivityApp.ProdActivityResultId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == productActivityApp.ProdActivityResultId).FirstOrDefault()?.Value : "";

                    }
                    if (productActivityApp.ActivityResultIds != null && productActivityApp.ActivityResultIds.Count > 0)
                    {
                        productActivityApp.ActivityResult = productActivityApp.ActivityResultIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.ActivityResultIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";
                    }
                    if (productActivityApp.ActivityMasterIds != null && productActivityApp.ActivityMasterIds.Count > 0)
                    {
                        productActivityApp.ActivityMaster = productActivityApp.ActivityMasterIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.ActivityMasterIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }

                    productActivityApp.ActivityStatus = s.ActivityStatusId != null ? applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.ActivityStatusId).FirstOrDefault()?.Value : "";
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();

                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();

                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            productActivityApp.FilePath = counts.FilePath;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == searchModel.UserID)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if (ProductActivityPermissions != null && ProductActivityPermissions.Count > 0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {


                            if (d.ProductActivityPermissionId > 0)
                            {
                                if (searchModel.UserID == d.UserID)
                                {
                                    productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                    productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                }
                                else
                                {
                                    productActivityApp.IsActionPermission = false;
                                }


                            }
                            else
                            {
                                productActivityApp.IsActionPermission = false;
                            }
                        });
                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }


                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(searchModel.UserID))
                        {
                            productActivityAppModels.Add(productActivityApp);
                            // productActivityApps.RowCount = productActivityAppModels.Count;
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                        // productActivityApps.RowCount = productActivityAppModels.Count;
                    }

                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetNavprodOrderLine")]
        public List<NavprodOrderLineModel> GetNavprodOrderLine(long? id, string type)
        {
            var productActivityApps = _context.NavprodOrderLine.Where(w => w.RePlanRefNo == type && w.CompanyId == id).ToList();
            List<NavprodOrderLineModel> productActivityAppModels = new List<NavprodOrderLineModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var itemNos = productActivityApps.Where(w => w.CompanyId == id).Select(s => s.ItemNo).ToList();
                var navItems = _context.Navitems.Where(w => itemNos.Contains(w.No)).ToList();
                productActivityApps.ForEach(s =>
                  {
                      var navItem = navItems.FirstOrDefault(a => a.No == s.ItemNo);
                      NavprodOrderLineModel productActivityApp = new NavprodOrderLineModel();
                      productActivityApp.OrderLineNo = s.OrderLineNo;
                      productActivityApp.OutputQty = s.OutputQty;
                      productActivityApp.CompletionDate = s.CompletionDate;
                      productActivityApp.StartDate = s.StartDate;
                      productActivityApp.Status = s.Status;
                      productActivityApp.Name = s.Description + (string.IsNullOrEmpty(s.Description1) ? "" : (" | " + s.Description1));
                      productActivityApp.ProdOrderNo = s.ProdOrderNo;
                      productActivityApp.ItemNo = s.ItemNo;
                      productActivityApp.Description = s.Description;
                      productActivityApp.Description1 = s.Description1;
                      productActivityApp.BatchNo = s.BatchNo;
                      productActivityApp.RePlanRefNo = s.RePlanRefNo;
                      productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                      productActivityApp.Uom = navItem?.BaseUnitofMeasure;
                      productActivityApp.InternalRef = navItem?.InternalRef;
                      productActivityApp.PackQty = navItem?.PackQty;
                      productActivityApp.OutputQty = navItem?.PackQty;
                      productActivityAppModels.Add(productActivityApp);
                  });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetProductionActivityAppLineQaChecker")]
        public List<ProductionActivityAppLineQaCheckerModel> GetProductionActivityAppLineQaChecker(long? id)
        {
            List<ProductionActivityAppLineQaCheckerModel> productionActivityAppLineQaCheckerModels = new List<ProductionActivityAppLineQaCheckerModel>();
            var productionActivityAppLineQaChecker = _context.ProductionActivityAppLineQaChecker.Include(a => a.QaCheckUser).Where(w => w.ProductionActivityAppLineId == id).ToList();
            productionActivityAppLineQaChecker.ForEach(s =>
            {
                ProductionActivityAppLineQaCheckerModel productionActivityAppLineQaCheckerModel = new ProductionActivityAppLineQaCheckerModel();
                productionActivityAppLineQaCheckerModel.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                productionActivityAppLineQaCheckerModel.ProductionActivityAppLineQaCheckerId = s.ProductionActivityAppLineQaCheckerId;
                productionActivityAppLineQaCheckerModel.QaCheck = s.QaCheck;
                productionActivityAppLineQaCheckerModel.QaCheckDate = s.QaCheckDate;
                productionActivityAppLineQaCheckerModel.QaCheckUserId = s.QaCheckUserId;
                productionActivityAppLineQaCheckerModel.QaCheckUser = s.QaCheckUser?.UserName;
                productionActivityAppLineQaCheckerModels.Add(productionActivityAppLineQaCheckerModel);
            });
            return productionActivityAppLineQaCheckerModels;
        }

        [HttpPost]
        [Route("GetProductActivityEmailActivitySubjects")]
        public List<view_ActivityEmailSubjects> GetProductActivityEmailActivitySubjects(SearchModel searchModel)            
           
        {
            var activitymaster = _context.ActivityEmailTopics.Where(a => a.ActivityMasterId == searchModel.Id && a.ActivityType == searchModel.SearchString)?.FirstOrDefault();
            if (activitymaster != null)
            {
                if (activitymaster.EmailTopicSessionId != null)
                {
                    searchModel.SessionID = activitymaster.EmailTopicSessionId;
                }
            }
            List<view_ActivityEmailSubjects> activityEmailSubjects = new List<view_ActivityEmailSubjects>();
            string search = string.Empty;
            string sqlQuery = string.Empty;
            if (searchModel.SessionID != null)
            {
                sqlQuery = "select ET.ID as TopicId,EC.ID, EC.Name as TopicName,EC.AddedDate,EC.AddedByUserID, E.FirstName, a.UserName as AddedByUser, ET.DueDate as DueDate, E.LastName from EmailTopics ET INNER JOIN EmailConversations EC ON EC.TopicID = ET.ID  inner join ApplicationUser a on a.UserID = EC.AddedByUserID INNER JOIN EmailConversationParticipant ECP ON ECP.ConversationId = EC.ID AND ECP.UserId = " + searchModel.UserID + "CROSS APPLY(SELECT DISTINCT ReplyId = CASE WHEN ECC.ReplyId >0 THEN ECC.ReplyId ELSE ECC.ID END FROM EmailConversations ECC WHERE ECC.TopicID=ET.ID AND (ECC.OnBehalf = " + searchModel.UserID + "OR EXISTS(SELECT * FROM EmailConversationAssignTo TP WHERE ECC.ID = TP.ConversationId AND (TP.UserId = " + searchModel.UserID + " or TP.AddedByUserID = " + searchModel.UserID + ")) OR EXISTS(SELECT * FROM EmailConversationAssignCC TP WHERE ECC.ID = TP.ConversationId AND (TP.UserId = " + searchModel.UserID + " or TP.AddedByUserID = " + searchModel.UserID + ")) OR EXISTS(SELECT * FROM EmailConversationParticipant TP WHERE ECC.ID = TP.ConversationId AND (TP.UserId = " + searchModel.UserID + " or TP.AddedByUserID = " + searchModel.UserID + "))))K LEFT JOIN Employee E ON EC.LastUpdateUserID = E.UserId where ET.SessionId = '" + searchModel.SessionID + "' AND  ET.OnDraft = 0 AND EC.ID=K.ReplyId";
                var records = _context.Set<view_ActivityEmailSubjects>().FromSqlRaw(sqlQuery).AsQueryable().Distinct().ToList();
                if (records.Count > 0)
                {
                    records.ForEach(r =>
                    {
                        view_ActivityEmailSubjects view_ActivityEmailSubjects = new view_ActivityEmailSubjects();
                        view_ActivityEmailSubjects.TopicId = r.TopicId;
                        view_ActivityEmailSubjects.AddedByUserID = r.AddedByUserID;
                        view_ActivityEmailSubjects.AddedDate = r.AddedDate;
                        view_ActivityEmailSubjects.FirstName = r.FirstName;
                        view_ActivityEmailSubjects.LastName = r.LastName;
                        view_ActivityEmailSubjects.DueDate = r.DueDate;
                        view_ActivityEmailSubjects.TopicName = r.TopicName;
                        view_ActivityEmailSubjects.AddedByUser = r.AddedByUser;
                        activityEmailSubjects.Add(view_ActivityEmailSubjects);
                    });



                }
            }
            return activityEmailSubjects;
        }
        [HttpGet]
        [Route("GetProductActivityAppLine")]
        public List<ProductActivityAppModel> GetProductActivityAppLine(long? id, string type, long? userId, long? locationId)
        {

            var productActivityAppQuery = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProductionActivityApp.Company)
                .Include(a => a.StatusCode)
                 .Include(a => a.NavprodOrderLine)
                .Include(a => a.ProductActivityCaseLine).Where(w => w.ProductionActivityApp.CompanyId == id && w.NavprodOrderLineId != null);

            if (locationId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.LocationId == locationId);
            }
            if (!string.IsNullOrEmpty(type))
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ProductionActivityApp.ProdOrderNo == type);
            }
            var productActivityApps = productActivityAppQuery.ToList();
            var ActivtiResultList = _context.ActivityResultMultiple.ToList();
            var ActivityMasterList = _context.ActivityMasterMultiple.ToList();
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityAppLineDoc.Where(w => w.Type == "Production Activity").ToList();
                var addedIds = productActivityApps.ToList().Select(s => s.AddedByUserId).ToList();
                addedIds.Add(userId);
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", addedIds.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.UserID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.DepartmentName,
                    s.PlantID,
                    s.DepartmentID
                }).ToList();
                var loginUser = employee.FirstOrDefault(w => w.UserID == userId)?.DepartmentID;
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();

                productActivityApps.ForEach(s =>
                {
                    var checkSameDept = false;

                    if (s.AddedByUserId == userId)
                    {
                        checkSameDept = true;
                    }
                    else
                    {
                        var employeeDep = employee.FirstOrDefault(w => w.UserID == s.AddedByUserId)?.DepartmentID;
                        if (loginUser != null && employeeDep != null && loginUser == employeeDep)
                        {
                            checkSameDept = true;
                        }
                    }
                    if (checkSameDept == true)
                    {
                        List<long> responsibilityUsers = new List<long>();
                        if (s.ProductActivityCaseId > 0)
                        {
                            var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                            var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                            var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                            var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                            var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                            var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                            var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                            if (appUserss != null && appUserss.Count > 0)
                            {
                                var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                                if (usersIdsBy != null && usersIdsBy.Count > 0)
                                {
                                    responsibilityUsers.AddRange(usersIdsBy);
                                }
                            }
                        }

                        ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                        productActivityApp.Type = "Production Activity";
                        productActivityApp.ResponsibilityUsers = responsibilityUsers;
                        productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                        productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Activity" && w.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Count();
                        productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                        productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                        productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                        productActivityApp.LineComment = s.Comment;
                        productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                        productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                        productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                        productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                        productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                        productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                        productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                        productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                        productActivityApp.ActionDropdown = s.ActionDropdown;
                        productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                        productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                        productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                        productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                        productActivityApp.ProdOrderNo = s.ProductionActivityApp?.ProdOrderNo;
                        productActivityApp.StatusCodeID = s.StatusCodeId;
                        productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                        productActivityApp.ModifiedDate = s.ModifiedDate;
                        productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                        productActivityApp.LineSessionId = s.SessionId;
                        productActivityApp.AddedByUserID = s.AddedByUserId;
                        productActivityApp.AddedDate = s.AddedDate;
                        productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                        productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                        productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                        productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                        productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                        productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                        productActivityApp.LocationToSaveId = s.ProductActivityCaseLine?.LocationToSaveId;
                        productActivityApp.QaCheck = s.QaCheck;
                        productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                        productActivityApp.Description = s.NavprodOrderLine?.Description;
                        productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                        productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                        productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                        productActivityApp.ProdOrderNoDesc = s.NavprodOrderLine?.ProdOrderNo + "|" + s.NavprodOrderLine?.Description + (string.IsNullOrEmpty(s.NavprodOrderLine?.Description1) ? "" : (" | " + s.NavprodOrderLine?.Description1));
                        productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                        productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                        productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                        productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                        productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                        productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                        productActivityApp.TopicId = s.TopicId;
                        productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                        productActivityApp.ActivityMasterIds = new List<long?>();
                        productActivityApp.ActivityResultIds = new List<long?>();
                        productActivityApp.ActivityMasterIds = ActivityMasterList.Where(a => a.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(a => a.AcitivityMasterId).ToList();
                        productActivityApp.ActivityResultIds = ActivtiResultList.Where(a => a.ProductionActivityAppLineId == s.ProductionActivityAppLineId).Select(a => a.AcitivityResultId).ToList();
                        productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId);
                        productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                        if (documents != null && s.SessionId != null)
                        {
                            var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                            if (counts != null)
                            {
                                productActivityApp.DocumentId = counts.DocumentId;
                                productActivityApp.DocumentID = counts.DocumentId;
                                productActivityApp.DocumentParentId = counts.DocumentParentId;
                                productActivityApp.FileName = counts.FileName;
                                productActivityApp.FilePath = counts.FilePath;
                                productActivityApp.ContentType = counts.ContentType;
                                productActivityApp.IsLocked = counts.IsLocked;
                                productActivityApp.LockedByUserId = counts.LockedByUserId;
                                productActivityApp.ModifiedDate = counts.UploadDate;
                                productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                                productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                                productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                                productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                                long? roleId = 0;
                                roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == userId)?.RoleId;
                                if (roleId != null)
                                {
                                    var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                    if (documentPermission != null)
                                    {
                                        productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                        productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                        productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                        productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                        productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                        productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                        productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                        productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                        productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                        productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                        productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                        productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                        productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                        productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                        productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                        productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                        productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                        productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                        productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                        productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                        productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                        productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                    }
                                    else
                                    {
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                        productActivityApp.DocumentPermissionData.IsRead = false;
                                        productActivityApp.DocumentPermissionData.IsDelete = false;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                    }
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = true;
                                    productActivityApp.DocumentPermissionData.IsDelete = true;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                                }
                            }
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetProductActivityAppLineByForum")]
        public List<ProductActivityAppModel> GetProductActivityAppLineByForum(long? id)
        {
            var productActivityAppQuery = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProductionActivityApp.Company)
                .Include(a => a.StatusCode)
                 .Include(a => a.NavprodOrderLine)
                .Include(a => a.ProductActivityCaseLine).Where(w => w.ProductionActivityAppLineId == id);
            var productActivityApps = productActivityAppQuery.ToList();
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {

                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.FilePath, s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                productActivityApps.ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    if (s.ProductActivityCaseId > 0)
                    {
                        var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                        var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                        var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                        var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                        var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                        var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                        var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                        var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                        if (appUserss != null && appUserss.Count > 0)
                        {
                            var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                            if (usersIdsBy != null && usersIdsBy.Count > 0)
                            {
                                responsibilityUsers.AddRange(usersIdsBy);
                            }
                        }
                    }

                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    productActivityApp.Type = "Production Activity";
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                    productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.ProdOrderNo = s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.LocationToSaveId = s.ProductActivityCaseLine?.LocationToSaveId;
                    productActivityApp.QaCheck = s.QaCheck;
                    productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                    productActivityApp.Description = s.NavprodOrderLine?.Description;
                    productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                    productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                    productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                    productActivityApp.ProdOrderNoDesc = s.NavprodOrderLine?.ProdOrderNo + "|" + s.NavprodOrderLine?.Description + (string.IsNullOrEmpty(s.NavprodOrderLine?.Description1) ? "" : (" | " + s.NavprodOrderLine?.Description1));
                    productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId);
                    productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.ModifiedDate = counts.UploadDate;
                            productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                        }
                        productActivityAppModels.Add(productActivityApp);
                    }
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetNavprodOrdersNo")]
        public List<NavprodOrderLineModel> GetNavprodOrdersNo(long? id)
        {
            List<NavprodOrderLineModel> productActivityAppModels = new List<NavprodOrderLineModel>();
            var productionsimulationlist = _context.ProductionSimulation.Where(p=>p.CompanyId == id).ToList();
            var productActivityApps = _context.NavprodOrderLine.Where(w => w.ProdOrderNo != null && w.ProdOrderNo != "" && w.CompanyId == id && w.RePlanRefNo != null).Select(s => new { s.RePlanRefNo, s.BatchNo, s.CompanyId }).Distinct().AsNoTracking().ToList();
            // var productActivityApps = _context.NavprodOrderLine.Where(w => w.ProdOrderNo != null && w.ProdOrderNo != "" && w.CompanyId == id && w.RePlanRefNo != null && w.BatchNo != null && w.BatchNo != "").Select(s => new { s.RePlanRefNo, s.BatchNo, s.CompanyId }).Distinct().AsNoTracking().ToList();

            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                long i = 1;
                var navItesmList = _context.Navitems.Select(s => new { s.No, s.BatchNos, s.PackSize }).ToList();
                var navprodOrderLine = _context.NavprodOrderLine.Select(s => new { s.RePlanRefNo, s.CompanyId, s.ItemNo, s.Description }).ToList();
                productActivityApps.ForEach(s =>
                {
                    // var topicId = _context.ProductionActivityApp.Select(x => new { x.ProdOrderNo, x.CompanyId, x.TopicId }).FirstOrDefault(f => f.ProdOrderNo == s.RePlanRefNo && f.CompanyId == id)?.TopicId;
                    var navItems = navprodOrderLine.Where(f => f.RePlanRefNo == s.RePlanRefNo && f.CompanyId == id).Select(x => x.ItemNo).ToList();
                    var description = "";
                    var batchNo = "";
                    batchNo = productionsimulationlist.Where(p => p.ProdOrderNo == s.RePlanRefNo).Select(x => x.BatchNo).FirstOrDefault();
                    description = navprodOrderLine.FirstOrDefault(f => f.RePlanRefNo == s.RePlanRefNo && f.CompanyId == id)?.Description;
                    var batchNos = navItesmList.Where(w => navItems.Contains(w.No) && w.BatchNos != null).Select(b => b.BatchNos).Distinct().ToList();
                    var packSize = navItesmList.Where(w => navItems.Contains(w.No) && w.PackSize != null).Select(b => b.PackSize).Distinct().ToList();
                    NavprodOrderLineModel productActivityApp = new NavprodOrderLineModel();
                    productActivityApp.NavprodOrderLineId = i;
                    productActivityApp.ProdOrderNo = s.RePlanRefNo;
                    productActivityApp.Description = description;
                    productActivityApp.BatchNo = s.BatchNo;
                    productActivityApp.Name = s.RePlanRefNo + " || " + batchNo + (description == "" ? "" : ("||" + description));
                    // productActivityApp.TopicId = topicId;
                    productActivityApp.CompanyId = s.CompanyId;
                    productActivityApp.BatchNos = navItems != null ? string.Join("||", batchNos) : "";
                    productActivityApp.BatchSize = navItems != null ? string.Join("||", packSize) : "";
                    i++;
                    productActivityAppModels.Add(productActivityApp);
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetNavprodOrdersNoList")]
        public List<NavprodOrderLineModel> GetNavprodOrdersNoList(string type, long? id)
        {

            var productActivityApps = _context.NavprodOrderLine.Where(w => w.RePlanRefNo == type && w.CompanyId == id).AsNoTracking().ToList();
            List<NavprodOrderLineModel> productActivityAppModels = new List<NavprodOrderLineModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var itemNos = productActivityApps.Where(w => w.CompanyId == id).Select(s => s.ItemNo).ToList();
                var navItems = _context.Navitems.Where(w => itemNos.Contains(w.No)).ToList();
                productActivityApps.ForEach(s =>
                {
                    var navItem = navItems.FirstOrDefault(a => a.No == s.ItemNo);
                    NavprodOrderLineModel productActivityApp = new NavprodOrderLineModel();
                    productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                    productActivityApp.ProdOrderNo = s.ProdOrderNo;
                    productActivityApp.Name = s.ProdOrderNo + "|" + s.Description + (string.IsNullOrEmpty(s.Description1) ? "" : (" | " + s.Description1));
                    productActivityApp.Status = s.Status;
                    productActivityApp.RePlanRefNo = s.RePlanRefNo;
                    productActivityApp.OrderLineNo = s.OrderLineNo;
                    productActivityApp.ItemNo = s.ItemNo;
                    productActivityApp.Description = s.Description;
                    productActivityApp.Description1 = s.Description1;
                    productActivityApp.BatchNo = s.BatchNo;
                    productActivityApp.StartDate = s.StartDate;
                    productActivityApp.CompletionDate = s.CompletionDate;
                    productActivityApp.OutputQty = s.OutputQty;
                    productActivityApp.RemainingQuantity = s.RemainingQuantity;
                    productActivityApp.UnitofMeasureCode = s.UnitofMeasureCode;
                    productActivityApp.LastSyncDate = s.LastSyncDate;
                    productActivityApp.LastSyncUserId = s.LastSyncUserId;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.Uom = navItem?.BaseUnitofMeasure;
                    productActivityApp.InternalRef = navItem?.InternalRef;
                    productActivityApp.PackQty = navItem?.PackQty;
                    productActivityApp.OutputQty = navItem?.PackQty;
                    productActivityAppModels.Add(productActivityApp);
                });
            }
            return productActivityAppModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductActivityAppModel> GetData(SearchModel searchModel)
        {
            var processTransfer = new ProductionActivityApp();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderBy(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId > searchModel.Id);
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductActivityAppModel>(processTransfer);
            if (processTransfer != null)
            {
                result.AddedByUser = processTransfer.AddedByUser?.UserName;
                //result.IsTemplateUploadFlag = processTransfer.IsTemplateUpload == true ? "Yes" : "No";
            }

            return result;
        }

        [HttpPost]
        [Route("DeleteSupportDocument")]
        public void DeleteSupportDocument(SearchModel searchModel)
        {
            var id = searchModel.Id;
            try
            {
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.DocumentParentId, s.FileName, s.FilterProfileTypeId, s.IsVideoFile }).FirstOrDefault(f => f.DocumentId == id);
              
                if (documents != null && documents.DocumentParentId == null)
                {
                    var productionActivityAppLineDoc = _context.ProductionActivityAppLineDoc.Where(p => p.DocumentId == id).FirstOrDefault();
                    if (productionActivityAppLineDoc != null)
                    {
                        _context.ProductionActivityAppLineDoc.Remove(productionActivityAppLineDoc);
                        _context.SaveChanges();

                    }
                    var isVideoFile = documents.IsVideoFile.GetValueOrDefault(false);
                    if (isVideoFile)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + documents.FileName;
                        System.IO.File.Delete(serverPath);
                    }
                    var query = string.Format("Delete from Documents  Where  DocumentId='{0}' ", documents.DocumentId);
                    _context.Database.ExecuteSqlRaw(query);
                }
              


            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }

            [HttpPost]
        [Route("InsertTopicId")]
        public ProductActivityAppModel InsertTopicId(ProductActivityAppModel value)
        {
            var productActivityApp = _context.ProductionActivityApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ProdOrderNo.ToLower() == value.ProdOrderNo.ToLower());
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();
                var productActivityApps = new ProductionActivityApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    ProdOrderNo = value.ProdOrderNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    TopicId = value.TopicId,
                };
                _context.ProductionActivityApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityAppId = productActivityApps.ProductionActivityAppId;
            }
            else
            {
                productActivityApp.TopicId = value.TopicId;
                value.ProductionActivityAppId = productActivityApp.ProductionActivityAppId;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertProductActivityApp")]
        public ProductActivityAppModel InsertProductActivityApp(ProductActivityAppModel value)
        {
            var profileNo = "";
            value.ProfileId = _context.ProductActivityCase.FirstOrDefault(a => a.ManufacturingProcessChildId == value.ManufacturingProcessChildId)?.ProfileId;
            if (value.ProfileId != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Production Activity" });
            }
            value.LineSessionId ??= Guid.NewGuid();
            var productActivityApp = _context.ProductionActivityApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ProdOrderNo.ToLower() == value.ProdOrderNo.ToLower() && s.LocationId == value.LocationId);
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();
                var productActivityApps = new ProductionActivityApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    ProdOrderNo = value.ProdOrderNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    LocationId = value.LocationId == -1 ? null : value.LocationId,
                   
                };
                _context.ProductionActivityApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityAppId = productActivityApps.ProductionActivityAppId;
            }
            else
            {
                value.SessionId ??= Guid.NewGuid();
                value.ProductionActivityAppId = productActivityApp.ProductionActivityAppId;
                productActivityApp.CompanyId = value.CompanyId;
                productActivityApp.Comment = value.Comment;
                productActivityApp.ProdOrderNo = value.ProdOrderNo;
                productActivityApp.StatusCodeId = value.StatusCodeID.Value;
                productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
                productActivityApp.ModifiedDate = DateTime.Now;
                productActivityApp.SessionId = value.SessionId;
                productActivityApp.LocationId = value.LocationId == -1 ? null : value.LocationId;
                _context.SaveChanges();
            }
            var productActivityAppLine = new ProductionActivityAppLine
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                ProductionActivityAppId = value.ProductionActivityAppId,
                ProdActivityActionId = value.ProdActivityActionId,
                ActionDropdown = value.ActionDropdown,
                ProdActivityCategoryId = value.ProdActivityCategoryId,
                IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.LineSessionId,
                ProductActivityCaseLineId = value.ProductActivityCaseLineId,
                NavprodOrderLineId = value.NavprodOrderLineId,
                Comment = value.LineComment,
                QaCheck = false,
                IsOthersOptions = value.IsOthersOptions,
                ProdActivityResultId = value.ProdActivityResultId,
                ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                ProdActivityActionChildD = value.ProdActivityActionChildD,
                ProdActivityCategoryChildId = value.ProdActivityCategoryChildId,
                LocationId = value.LocationId == -1 ? null : value.LocationId,
                ProductActivityCaseId = value.ProductActivityCaseId,
                CommentImage = value.CommentImage,
                CommentImageType = value.CommentImageType,
                ActivityStatusId= value.ActivityStatusId,
                ProfileId = value.ProfileId,
                ProfileNo = profileNo,
                


            };
            _context.ProductionActivityAppLine.Add(productActivityAppLine);
            _context.SaveChanges();
            value.ProductionActivityAppLineId = productActivityAppLine.ProductionActivityAppLineId;
            if (value.ActivityMasterIds.Count > 0)
            {
                value.ActivityMasterIds.ForEach(r =>
                {
                    var activityMasterMultiple = new ActivityMasterMultiple();
                    activityMasterMultiple.AcitivityMasterId = r;
                    activityMasterMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                    _context.Add(activityMasterMultiple);

                });
                _context.SaveChanges();
            }
            if (value.ActivityResultIds.Count > 0)
            {
                value.ActivityResultIds.ForEach(r =>
                {
                    var existing = _context.ActivityResultMultiple.Where(t => t.ProductionActivityAppLineId == value.ProductionActivityAppLineId && t.AcitivityResultId == r).FirstOrDefault();
                    if (existing == null)
                    {
                        var activityResultMultiple = new ActivityResultMultiple();
                        activityResultMultiple.AcitivityResultId = r.Value;
                        activityResultMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                        _context.Add(activityResultMultiple);
                    }

                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityApp")]
        public ProductActivityAppModel UpdateProductActivityApp(ProductActivityAppModel value)
        {
            value.LineSessionId ??= Guid.NewGuid();
            var productActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(s => s.ProductionActivityAppLineId == value.ProductionActivityAppLineId);
            productActivityApp.ManufacturingProcessId = value.ManufacturingProcessId;
            productActivityApp.ProdActivityActionId = value.ProdActivityActionId;
            productActivityApp.ActionDropdown = value.ActionDropdown;
            productActivityApp.ProdActivityCategoryId = value.ProdActivityCategoryId;
            productActivityApp.IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false;
            productActivityApp.StatusCodeId = value.StatusCodeID.Value;
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.SessionId = value.LineSessionId;
            productActivityApp.Comment = value.LineComment;
            productActivityApp.IsOthersOptions = value.IsOthersOptions;
            productActivityApp.ProductActivityCaseLineId = value.ProductActivityCaseLineId;
            productActivityApp.ProdActivityResultId = value.ProdActivityResultId;
            productActivityApp.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
            productActivityApp.ProdActivityActionChildD = value.ProdActivityActionChildD;
            productActivityApp.ProdActivityCategoryChildId = value.ProdActivityCategoryChildId;
            productActivityApp.LocationId = value.LocationId;
            productActivityApp.ProductActivityCaseId = value.ProductActivityCaseId;
            productActivityApp.CommentImage = value.CommentImage;
            productActivityApp.CommentImageType = value.CommentImageType;
            productActivityApp.ActivityStatusId = value.ActivityStatusId;
            if (value.ActivityMasterIds!=null && value.ActivityMasterIds.Count > 0)
            {
                value.ActivityMasterIds.ForEach(r =>
                {
                    var existing = _context.ActivityMasterMultiple.Where(t => t.ProductionActivityAppLineId == value.ProductionActivityAppLineId && t.AcitivityMasterId == r).FirstOrDefault();
                    if (existing == null)
                    {
                        var activityMasterMultiple = new ActivityMasterMultiple();
                        activityMasterMultiple.AcitivityMasterId = r;
                        activityMasterMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                        _context.Add(activityMasterMultiple);
                    }

                });
            }
            if (value.ActivityMasterIds!=null && value.ActivityResultIds.Count > 0)
            {
                value.ActivityResultIds.ForEach(r =>
                {
                    var existing = _context.ActivityResultMultiple.Where(t => t.ProductionActivityAppLineId == value.ProductionActivityAppLineId && t.AcitivityResultId == r).FirstOrDefault();
                    if (existing == null)
                    {
                        var activityResultMultiple = new ActivityResultMultiple();
                        activityResultMultiple.AcitivityResultId = r.Value;
                        activityResultMultiple.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
                        _context.Add(activityResultMultiple);
                    }

                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppQaCheck")]
        public ProductActivityAppModel UpdateProductActivityAppQaCheck(ProductActivityAppModel value)
        {
            var productActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(s => s.ProductionActivityAppLineId == value.ProductionActivityAppLineId);

            productActivityApp.QaCheckUserId = value.QaCheckUserId;
            productActivityApp.QaCheckDate = DateTime.Now;
            productActivityApp.QaCheck = value.QaCheck;
            _context.SaveChanges();
            var productionActivityAppLine = new ProductionActivityAppLineQaChecker
            {
                QaCheck = value.QaCheck,
                QaCheckDate = DateTime.Now,
                QaCheckUserId = value.QaCheckUserId,
                ProductionActivityAppLineId = productActivityApp.ProductionActivityAppLineId
            };
            _context.ProductionActivityAppLineQaChecker.Add(productionActivityAppLine);
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppTopic")]
        public ProductActivityAppModel UpdateProductActivityAppTopic(ProductActivityAppModel value)
        {
            var productActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(s => s.ProductionActivityAppLineId == value.ProductionActivityAppLineId);
            productActivityApp.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppChecker")]
        public ProductActivityAppModel UpdateProductActivityAppChecker(ProductActivityAppModel value)
        {
           

            var productActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(s => s.ProductionActivityAppLineId == value.ProductionActivityAppLineId);
            productActivityApp.IsCheckNoIssue = value.IsCheckNoIssue;
            productActivityApp.IsCheckReferSupportDocument = value.IsCheckReferSupportDocument;
            productActivityApp.CheckedById = value.AddedByUserID;
            productActivityApp.CheckedDate = DateTime.Now;
            productActivityApp.CheckedRemark = value.CheckedRemark;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityRoutineStatus")]
        public ProductionActivityRoutineAppLineModel UpdateProductActivityRoutineStatus(ProductionActivityRoutineAppLineModel value)
        {
          
            var productActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(s => s.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId);
            productActivityApp.RoutineStatusId =value.StatusCodeID;
            productActivityApp.ModifiedByUserId = value.AddedByUserID;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppLineStatus")]
        public ProductActivityAppModel UpdateProductActivityAppLineStatus(ProductActivityAppModel value)
        {
           
            var productActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(s => s.ProductionActivityAppLineId == value.ProductionActivityAppLineId);
            productActivityApp.ActivityStatusId = value.ActivityStatusId;
            productActivityApp.ModifiedByUserId = value.AddedByUserID;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {

            var SessionId = new Guid(files["sessionId"].ToString());
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
                var filePath = files["filePath"].ToString();
                if (filePath == "filePath")
                {
                    UploadFileAsByPath(files, SessionId);
                }
                else
                {
                    var userId = new long?();
                    var user = files["userID"].ToString();
                    if (user != "" && user != "undefined" && user != null)
                    {
                        userId = Convert.ToInt32(files["userID"].ToString());
                    }
                    var documentNo = "";
                    var numberSeriesId = files["numberSeriesId"].ToString();
                    if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
                    {
                        documentNo = files["numberSeriesId"].ToString();
                    }


                    var videoFiles = files["isVideoFile"].ToString();

                    var videoFile = false;
                    if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                    {
                        videoFile = Convert.ToBoolean(videoFiles);
                    }
                    var fileProfileTypeId = new long?();
                    var locationToSaveId = files["locationToSaveId"].ToString();
                    var isTemplate = files["isTemplate"].ToString();
                    if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
                    {
                        fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                        if (isTemplate == "No")
                        {
                            var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                            if (profileId != null && profileId.ProfileId > 0)
                            {
                                documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                            }
                        }
                    }
                    var newFileName = files["newFileName"].ToString();
                    var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
                    files.Files.ToList().ForEach(f =>
                    {
                        var file = f;
                        var ext = "";
                        var newFile = "";
                        if (f.FileName == "blob")
                        {
                            newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                        }
                        else
                        {
                            ext = f.FileName;
                            int i = ext.LastIndexOf('.');
                            string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                            newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhs);
                        }
                        if (videoFile == true)
                        {
                            ext = f.FileName;
                            int i = ext.LastIndexOf('.');
                            string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                            var fileName1 = SessionId + "." + rhs;
                            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                            using (var targetStream = System.IO.File.Create(serverPath))
                            {
                                file.CopyTo(targetStream);
                                targetStream.Flush();
                            }
                            if (docs == null)
                            {
                                var documents = new Documents
                                {
                                    FileName = newFile,
                                    ContentType = file.ContentType,
                                    FileData = null,
                                    FileSize = file.Length,
                                    UploadDate = DateTime.Now,
                                    AddedByUserId = userId,
                                    AddedDate = DateTime.Now,
                                    SessionId = SessionId,
                                    IsTemp = true,
                                    IsCompressed = true,
                                    IsVideoFile = true,
                                    IsLatest = true,
                                    FilterProfileTypeId = fileProfileTypeId,
                                    ProfileNo = documentNo,
                                    SourceFrom = "FileProfile",
                                };
                                _context.Documents.Add(documents);
                            }
                            else
                            {
                                docs.FileName = newFile;
                                docs.ContentType = file.ContentType;
                                docs.FileData = null;
                                docs.FileSize = file.Length;
                                docs.UploadDate = DateTime.Now;
                                docs.IsLatest = true;
                                docs.IsVideoFile = true;
                                docs.FilterProfileTypeId = fileProfileTypeId;
                                docs.ProfileNo = documentNo;
                            }
                        }
                        else
                        {
                            var fs = file.OpenReadStream();
                            var br = new BinaryReader(fs);
                            Byte[] document = br.ReadBytes((Int32)fs.Length);
                            var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                            if (docs == null)
                            {
                                var documents = new Documents
                                {
                                    FileName = newFile,
                                    ContentType = file.ContentType,
                                    FileData = compressedData,
                                    FileSize = fs.Length,
                                    UploadDate = DateTime.Now,
                                    SessionId = SessionId,
                                    IsTemp = true,
                                    IsCompressed = true,
                                    AddedByUserId = userId,
                                    AddedDate = DateTime.Now,
                                    IsLatest = true,
                                    FilterProfileTypeId = fileProfileTypeId,
                                    ProfileNo = documentNo,
                                    SourceFrom = "FileProfile",
                                };
                                _context.Documents.Add(documents);
                            }
                            else
                            {
                                docs.FileName = newFile;
                                docs.ContentType = file.ContentType;
                                docs.FileData = compressedData;
                                docs.FileSize = fs.Length;
                                docs.UploadDate = DateTime.Now;
                                docs.IsLatest = true;
                                docs.IsVideoFile = false;
                                docs.FilterProfileTypeId = fileProfileTypeId;
                                docs.ProfileNo = documentNo;
                                docs.SourceFrom = "FileProfile";
                            }
                        }
                        var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                        if (documentNoSeries != null)
                        {
                            var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                            _context.Database.ExecuteSqlRaw(query);
                        }
                    });
                    _context.SaveChanges();
                
            }
            return Content(SessionId.ToString());

        }
        private void UploadFileAsByPath(IFormCollection files, Guid SessionId)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
          
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');              
                string[] split = f.FileName.Split('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
              //  var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + Guid.NewGuid() + '.' + split.Last();
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                    serverPath = serverPaths + @"\" + Guid.NewGuid() + ".pdf";
                }
                else
                {

                }
               
                var filePath = getNextFileName(serverPath);
                // newFile = filePath.Replace(serverPaths + @"\", "");
               
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = newFile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",

                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                  
                }
                else if(docs !=null)
                {
                   
                        var docquery = string.Format("Update Documents Set IsLatest ='{1}' Where DocumentId= {0}  ", docs.DocumentId, 0);
                        _context.Database.ExecuteSqlRaw(docquery);
                    
                    
                   
                    var documents = new Documents
                    {
                        FileName = newFile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                }
               
                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
                var imageAttachemnts = files["imageAttachments"].ToString();
                if(imageAttachemnts!=null)
                {
                    var documents = new Documents
                    {
                        FileName = imageAttachemnts,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                }
            });
            _context.SaveChanges();
        }

        private void UploadSupportDocFileAsByPath(IFormCollection files, Guid SessionId, long productionActivityAppLineId, string type)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
           
            var description = files["description"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                string[] split = f.FileName.Split('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + SessionId + '.' + split.Last();
                var filePath = getNextFileName(serverPath);
                newFile = filePath.Replace(serverPaths + @"\", "");
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = f.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",
                        Description = description,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    InsertUploadDoument(documents.DocumentId, productionActivityAppLineId, type);
                }

                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
            });
            _context.SaveChanges();
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpPost]
        [Route("GetProductionActivityAppExcel")]
        public IActionResult GetProductionActivityAppExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Company");
            headers.Add("Date");
            headers.Add("Location");
            headers.Add("Manufacturing Process");
            headers.Add("Category");
            headers.Add("Action");
            headers.Add("Comment");
            headers.Add("Result");
            headers.Add("Activity Master");
            headers.Add("Activity Status");
            headers.Add("Read By");
            headers.Add("Modify By");
            var productActivityAppss = _context.ProductionActivityAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcess)
                .Include(a => a.ProdActivityCategory)
                .Include(a => a.ProdActivityAction)
                .Include(a => a.ProductionActivityApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityApp.Company)
                .Include(a => a.StatusCode)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.QaCheckUser)
                .Include(a => a.Location)                
                .Include(a => a.ActivityStatus)
                .AsQueryable();
            if (searchModel.ClassificationTypeId != null && searchModel.ClassificationTypeId > 0)
            {
                productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityAppLineId == searchModel.ClassificationTypeId);
            }
            if (searchModel.ProductActivityAppSearchModel != null)
            {
                //if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.BatchNo))
                //{
                //    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityApp.ba == searchModel.ProductActivityAppSearchModel.BatchNo);
                //}
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.ProdOrderNo))
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityApp.ProdOrderNo == searchModel.ProductActivityAppSearchModel.ProdOrderNo);
                }
                if (searchModel.ProductActivityAppSearchModel.CompanyId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityApp.CompanyId == searchModel.ProductActivityAppSearchModel.CompanyId);
                }
                if (searchModel.ProductActivityAppSearchModel.ManufacturingProcessId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ManufacturingProcessChildId == searchModel.ProductActivityAppSearchModel.ManufacturingProcessId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityCategoryChildId == searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityActionId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityActionChildD == searchModel.ProductActivityAppSearchModel.ProdActivityActionId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductActivityCaseLineId == searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityResultId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityResultId == searchModel.ProductActivityAppSearchModel.ProdActivityResultId);
                }
                if (searchModel.ProductActivityAppSearchModel.StatusCodeID != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.StatusCodeId == searchModel.ProductActivityAppSearchModel.StatusCodeID);
                }
                if (searchModel.ProductActivityAppSearchModel.StartDate != null && searchModel.ProductActivityAppSearchModel.EndDate == null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate == null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value && w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.LocationId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.LocationId == searchModel.ProductActivityAppSearchModel.LocationId);
                }
            }
            var productActivityApps = productActivityAppss.ToList();
            List<ProductActivityAppModel> productActivityAppModels = new List<ProductActivityAppModel>();
            if (productActivityApps != null)
            {
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.FilePath, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();

                productActivityApps.ToList().ForEach(s =>
                {
                    /*List<long> responsibilityUsers = new List<long>();
                    if (s.ProductActivityCaseId > 0)
                    {
                        var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                        var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                        var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                        var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                        var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                        var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                        var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                        var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                        if (appUserss != null && appUserss.Count > 0)
                        {
                            var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                            if (usersIdsBy != null && usersIdsBy.Count > 0)
                            {
                                responsibilityUsers.AddRange(usersIdsBy);
                            }
                        }
                    }*/
                    ProductActivityAppModel productActivityApp = new ProductActivityAppModel();
                    /*productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                    */
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                   
                  
                 
                    productActivityApp.ActivityStatusId = s.ActivityStatusId;
                    productActivityApp.ActivityStatus = s.ActivityStatus?.Value;
                    productActivityApp.ProductionActivityAppId = s.ProductionActivityApp.ProductionActivityAppId;
                    productActivityApp.Comment = s.ProductionActivityApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.QaCheckUserId = s.QaCheckUserId;
                    productActivityApp.QaCheckDate = s.QaCheckDate;
                    productActivityApp.QaCheckUser = s.QaCheckUser?.UserName;
                    productActivityApp.Type = "Production Routine";
                    productActivityApp.LocationId = s.ProductionActivityApp?.LocationId;
                    //productActivityApp.TicketNo = s.ProductionActivityApp?.ProdOrderNo;
                    productActivityApp.LocationName = s.Location?.Description  + "||" + s.ProductionActivityApp?.ProdOrderNo; 
                    /* productActivityApp.DocumentPermissionData = new DocumentPermissionModel();*/
                    productActivityApp.ProductionActivityAppLineQaCheckerModels = GetProductionActivityAppLineQaChecker(s.ProductionActivityAppLineId);
                    var qaUserList = productActivityApp?.ProductionActivityAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == searchModel.UserID).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.FilePath = counts.FilePath;
                            /*productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();*/
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                        }
                    }
                    productActivityAppModels.Add(productActivityApp);
                });
            }
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            string FromLocation = folderName + @"\" + newFolderName + @"\" + searchModel.UserID + ".xlsx";
            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                int j = 1;

                List<string> headersBy = new List<string>();
                headersBy.AddRange(headers);
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                var sheetName = "Production Activity";
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = sheetName };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);

                Row headerRow = new Row();
                foreach (string column in headersBy)
                {
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                if (productActivityAppModels != null && productActivityAppModels.Count > 0)
                {
                    productActivityAppModels.ForEach(s =>
                    {
                        Row row = new Row();
                        Cell companyNameCell = new Cell();
                        companyNameCell.DataType = CellValues.String;
                        companyNameCell.CellValue = new CellValue(s.CompanyName?.ToString());
                        row.AppendChild(companyNameCell);

                        Cell profileTypeNamecell = new Cell();
                        profileTypeNamecell.DataType = CellValues.String;
                        profileTypeNamecell.CellValue = new CellValue(s.AddedDate?.ToString("dd-MMM-yyyy hh:mm tt"));
                        row.AppendChild(profileTypeNamecell);

                        Cell locationNameCell = new Cell();
                        locationNameCell.DataType = CellValues.String;
                        locationNameCell.CellValue = new CellValue(s.LocationName?.ToString());
                        row.AppendChild(locationNameCell);

                        Cell manufacturingProcessChildCell = new Cell();
                        manufacturingProcessChildCell.DataType = CellValues.String;
                        manufacturingProcessChildCell.CellValue = new CellValue(s.ManufacturingProcessChild?.ToString());
                        row.AppendChild(manufacturingProcessChildCell);

                        Cell ProdActivityCategoryChildCell = new Cell();
                        ProdActivityCategoryChildCell.DataType = CellValues.String;
                        ProdActivityCategoryChildCell.CellValue = new CellValue(s.ProdActivityCategoryChild?.ToString());
                        row.AppendChild(ProdActivityCategoryChildCell);


                        Cell ProdActivityActionChildCell = new Cell();
                        ProdActivityActionChildCell.DataType = CellValues.String;
                        ProdActivityActionChildCell.CellValue = new CellValue(s.ProdActivityActionChild?.ToString());
                        row.AppendChild(ProdActivityActionChildCell);

                        Cell LineCommentCell = new Cell();
                        LineCommentCell.DataType = CellValues.String;
                        LineCommentCell.CellValue = new CellValue(s.LineComment);
                        row.AppendChild(LineCommentCell);

                        Cell prodActivityResultCell = new Cell();
                        prodActivityResultCell.DataType = CellValues.String;
                        prodActivityResultCell.CellValue = new CellValue(s.ProdActivityResult);
                        row.AppendChild(prodActivityResultCell);

                       

                        Cell routineStatusCell = new Cell();
                        routineStatusCell.DataType = CellValues.String;
                        routineStatusCell.CellValue = new CellValue(s.ActivityStatus);
                        row.AppendChild(routineStatusCell);

                        if (s.ProductionActivityAppLineQaCheckerModels != null && s.ProductionActivityAppLineQaCheckerModels.Count > 0)
                        {
                            var readBy = s.ProductionActivityAppLineQaCheckerModels.Where(w => w.QaCheckUser != null).Select(a => a.QaCheckUser).ToList();
                            Cell ReadBycell = new Cell();
                            ReadBycell.DataType = CellValues.String;
                            ReadBycell.CellValue = new CellValue(string.Join(",", readBy));
                            row.AppendChild(ReadBycell);
                        }
                        else
                        {
                            Cell ReadBycell = new Cell();
                            ReadBycell.DataType = CellValues.String;
                            ReadBycell.CellValue = new CellValue(string.Empty);
                            row.AppendChild(ReadBycell);
                        }
                        Cell modifiedByUsercell = new Cell();
                        modifiedByUsercell.DataType = CellValues.String;
                        modifiedByUsercell.CellValue = new CellValue(s.ModifiedByUser?.ToString());
                        row.AppendChild(modifiedByUsercell);

                        if (s.LineSessionId != null)
                        {
                            var Url = searchModel.BaseUrl + "?id=" + s.LineSessionId + "&&type=latest";
                            Cell cellHeader = new Cell();
                            cellHeader.DataType = CellValues.InlineString;
                            CellFormula cellFormula1 = new CellFormula() { Space = SpaceProcessingModeValues.Preserve };
                            cellFormula1.Text = @"HYPERLINK(""" + Url + @""", ""Click Here"")";
                            cellHeader.CellFormula = cellFormula1;
                            Run run1 = new Run();
                            run1.Append(new Text("Click Here"));
                            RunProperties run1Properties = new RunProperties();
                            run1Properties.Append(new Bold());
                            run1.RunProperties = run1Properties;
                            InlineString inlineString = new InlineString();
                            inlineString.Append(run1);
                            cellHeader.Append(inlineString);
                            row.AppendChild(cellHeader);
                        }

                        sheetData.AppendChild(row);
                    });
                }
                j++;
                sheetId++;
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        [HttpPost]
        [Route("UploadSupportDocuments")]
        public async Task<IActionResult> UploadSupportDocuments(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            var productionActivityAppLineId = Convert.ToInt32(files["productionActivityAppLineId"].ToString());
            var type = files["type"].ToString();
            if (userExits > 0)
            {
                var filePath = files["filePath"].ToString();
                if (filePath == "filePath")
                {

                    UploadSupportDocFileAsByPath(files, SessionId,productionActivityAppLineId,type);
                   
                }

                else
                {

                    var userId = new long?();
                    var user = files["userID"].ToString();
                    var description = files["description"].ToString();
                    var videoFile = false;
                    if (user != "" && user != "undefined" && user != null)
                    {
                        userId = Convert.ToInt32(files["userID"].ToString());
                    }
                  

                    var videoFiles = files["isVideoFile"].ToString();
                   
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                    {
                        videoFile = Convert.ToBoolean(videoFiles);
                    }
                    foreach (var f in files.Files)
                    {
                        DateTime? expiryDate = new DateTime?();
                        var userIds = files["userID"].ToString().Split(',').ToList();
                        SessionId = Guid.NewGuid();
                        userId = Convert.ToInt32(userIds.FirstOrDefault().ToString());

                        description = files["description"].ToString();
                        var expiryDateString = files["expiryDate"].ToString();
                        if (expiryDateString != null && expiryDateString != "" && expiryDateString != "undefined" && expiryDateString != "null")
                        {
                            expiryDate = Convert.ToDateTime(expiryDateString);
                        }
                        else
                        {
                            expiryDate = null;
                        }

                        //var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString().Split(',')[0].ToString());
                        var tableName = files["type"].ToString().Split(',')[0].ToString();

                        // var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                        string profileNo = "";




                        var fileName = SessionId + "." + f.FileName.Split(".").Last();
                        var file = f;

                        byte[] dataArray = new byte[0];
                        //var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName;
                        serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;

                        if (!System.IO.Directory.Exists(serverPaths))
                        {
                            System.IO.Directory.CreateDirectory(serverPaths);
                        }
                        var ext = "";
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        var isVideoFiles = false;
                        var contentType = f.ContentType.Split("/");
                        if (contentType[0] == "video")
                        {
                            isVideoFiles = true;
                        }
                        string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                        var serverPath = serverPaths + @"\" + fileName;
                        filePath = getNextFileName(serverPath);
                        var newFile = filePath.Replace(serverPaths + @"\", "");
                        using (var targetStream = System.IO.File.Create(filePath))
                        {

                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }


                    }
                    files.Files.ToList().ForEach(f =>
                    {
                        var file = f;
                        var ext = "";
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        var isVideoFiles = false;
                        var contentType = f.ContentType.Split("/");
                        if (contentType[0] == "video")
                        {
                            isVideoFiles = true;
                        }
                        if (videoFile == true)
                        {
                            string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                            var serverPath = serverPaths + @"\" + f.FileName;
                            var filePath = getNextFileName(serverPath);
                            var newFile = filePath.Replace(serverPaths + @"\", "");
                            using (var targetStream = System.IO.File.Create(filePath))
                            {
                                file.CopyTo(targetStream);
                                targetStream.Flush();
                            }
                            var documents = new Documents
                            {
                                FileName = newFile,
                                ContentType = file.ContentType,
                                FileData = null,
                                FileSize = file.Length,
                                UploadDate = DateTime.Now,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                IsVideoFile = isVideoFiles,
                                IsLatest = true,
                                FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                                SourceFrom = "FileProfile",

                            };
                            _context.Documents.Add(documents);
                            _context.SaveChanges();
                            InsertUploadDoument(documents.DocumentId, productionActivityAppLineId, type);
                        }
                        else
                        {
                            var fs = file.OpenReadStream();
                            var br = new BinaryReader(fs);
                            Byte[] document = br.ReadBytes((Int32)fs.Length);
                            var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);

                            var documents = new Documents
                            {
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FileData = compressedData,
                                FileSize = fs.Length,
                                UploadDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                IsLatest = true,
                                Description = description,
                                FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                                SourceFrom = "FileProfile",

                            };
                            _context.Documents.Add(documents);
                            _context.SaveChanges();
                            InsertUploadDoument(documents.DocumentId, productionActivityAppLineId, type);
                        }

                    });
                }
            }
            return Content(SessionId.ToString());

        }
        private void InsertUploadDoument(long documentId, long productionActivityAppLineId, string type)
        {
            if (type == "Production Activity")
            {
                var productionActivityAppLineDoc = new ProductionActivityAppLineDoc
                {
                    DocumentId = documentId,
                    ProductionActivityAppLineId = productionActivityAppLineId,
                    Type = type,
                };
                _context.ProductionActivityAppLineDoc.Add(productionActivityAppLineDoc);
            }
            if (type == "Ipir")
            {
                var productionActivityAppLineDoc = new ProductionActivityAppLineDoc
                {
                    DocumentId = documentId,
                    IpirReportId = productionActivityAppLineId,
                    Type = type,
                };
                _context.ProductionActivityAppLineDoc.Add(productionActivityAppLineDoc);
            }
            _context.SaveChanges();
        }

        [HttpGet]
        [Route("GetProductActivityComment")]
        public ProductActivityAppModel GetProductActivityComment(long? id)
        {
            ProductActivityAppModel productionActivityAppModel = new ProductActivityAppModel();
            var productionActivityAppLine = _context.ProductionActivityAppLine.Where(w => w.ProductionActivityAppLineId == id).FirstOrDefault();
            if (productionActivityAppLine != null)
            {
                productionActivityAppModel.Comment = productionActivityAppLine.Comment;
                productionActivityAppModel.CommentImage = productionActivityAppLine.CommentImage;
                productionActivityAppModel.CommentImageType = productionActivityAppLine.CommentImageType;
                productionActivityAppModel.ProductionActivityAppLineId = productionActivityAppLine.ProductionActivityAppLineId;
                productionActivityAppModel.ProductionActivityAppId = productionActivityAppLine.ProductionActivityAppId.Value;
            }
            return productionActivityAppModel;
        }

        [HttpGet]
        [Route("GetProductActivityChecker")]
        public ProductActivityAppModel GetProductActivityChecker(long? id)
        {
            ProductActivityAppModel productionActivityAppModel = new ProductActivityAppModel();
            var productionActivityAppLine = _context.ProductionActivityAppLine.Where(w => w.ProductionActivityAppLineId == id).FirstOrDefault();
            if (productionActivityAppLine != null)
            {
                var userName = "";
                if (productionActivityAppLine.CheckedById != null)
                {
                    var applicationUser = _context.ApplicationUser.Where(s => s.UserId == productionActivityAppLine.CheckedById)?.FirstOrDefault();
                    if(applicationUser!=null)
                    {
                        userName = applicationUser.UserName;
                    }
                }
                productionActivityAppModel.IsCheckNoIssue = productionActivityAppLine.IsCheckNoIssue;
                productionActivityAppModel.IsCheckReferSupportDocument = productionActivityAppLine.IsCheckReferSupportDocument;
                productionActivityAppModel.CheckedById = productionActivityAppLine.CheckedById;
                productionActivityAppModel.CheckedRemark = productionActivityAppLine.CheckedRemark;
                productionActivityAppModel.CheckedDate = productionActivityAppLine.CheckedDate;
                productionActivityAppModel.CheckedByUser = userName;
                productionActivityAppModel.ProductionActivityAppId = productionActivityAppLine.ProductionActivityAppId.Value;
            }
            return productionActivityAppModel;
        }
        [HttpGet]
        [Route("GetSupportDocuments")]
        public List<DocumentsModel> GetSupportDocuments(long? id, string type)
        {
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var documentsId = _context.ProductionActivityAppLineDoc.Where(w => w.DocumentId != null && w.Type.ToLower() == type.ToLower());
            if (type == "Production Activity")
            {
                documentsId = documentsId.Where(w => w.ProductionActivityAppLineId == id);
            }
            else
            {
                documentsId = documentsId.Where(w => w.IpirReportId == id);
            }
            var documentsIds = documentsId.Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
            var documetsparents = _context.Documents.Where(s => documentsIds.Contains(s.DocumentId) && s.IsLatest == true).Select(s => new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.AddedDate,
                s.IsCompressed,
                s.FileIndex,
                s.ProfileNo,
                s.Description,
                s.FilePath,
                
            }).AsNoTracking().ToList();
            if (documetsparents != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).AsNoTracking().ToList();
                documetsparents.ForEach(documetsparent =>
                {
                    var fileName = documetsparent.FileName.Split('.');
                    DocumentsModel documentsModelss = new DocumentsModel
                    {
                        DocumentID = documetsparent.DocumentId,
                        FileName = documetsparent.FileIndex > 0 ? fileName[0] + "_V0" + documetsparent.FileIndex + "." + fileName[1] : documetsparent.FileName,
                        ContentType = documetsparent.ContentType,
                        FileSize = (long)Math.Round(Convert.ToDouble(documetsparent.FileSize / 1024)),
                        UploadDate = documetsparent.UploadDate,
                        SessionID = documetsparent.SessionId,
                        FilterProfileTypeId = documetsparent.FilterProfileTypeId,
                        DocumentParentId = documetsparent.DocumentParentId,
                        TableName = documetsparent.TableName,
                        Type = "Document",
                        AddedDate = documetsparent.AddedDate,
                        AddedByUser = appUsers.FirstOrDefault(f => f.UserId == documetsparent.AddedByUserId)?.UserName,
                        IsCompressed = documetsparent.IsCompressed,
                        FileIndex = documetsparent.FileIndex,
                        ProfileNo = documetsparent.ProfileNo,
                        Description = documetsparent.Description,
                        FilePath = documetsparent.FilePath,
                    };
                    documentsModel.Add(documentsModelss);
                });
            }
            return documentsModel;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityApp")]
        public ActionResult<string> DeleteProductionActivityApp(int id)
        {
            try
            {
                var ProductionActivityApp = _context.ProductionActivityAppLine.SingleOrDefault(p => p.ProductionActivityAppLineId == id);
                if (ProductionActivityApp != null)
                {
                    var query = string.Format("Update Documents Set Islatest={1}  Where SessionId='{0}'", ProductionActivityApp.SessionId, 0);
                    _context.Database.ExecuteSqlRaw(query);
                    _context.ProductionActivityAppLine.Remove(ProductionActivityApp);
                    _context.SaveChanges();
                    var ProductionActivityAppCount = _context.ProductionActivityAppLine.Where(p => p.ProductionActivityAppLineId == id).Count();
                    if (ProductionActivityAppCount == 0)
                    {
                        var ProductionActivityApps = _context.ProductionActivityApp.SingleOrDefault(p => p.ProductionActivityAppId == ProductionActivityApp.ProductionActivityAppId);
                        if (ProductionActivityApps != null)
                        {
                            _context.ProductionActivityApp.Remove(ProductionActivityApps);
                            _context.SaveChanges();
                        }
                    }
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        [Route("GetProductionActivityNonCompliance")]
        public ProductionActivityNonComplianceModel GetProductionActivityNonCompliance(string type, long? id, string actionType)
        {

            var productActivityApps = _context.ProductionActivityNonCompliance.Include(a => a.ProductionActivityNonComplianceUser).Where(w => w.Type == type && w.ActionType == actionType);
            if (type == "Production Activity")
            {
                productActivityApps = productActivityApps.Where(w => w.ProductionActivityAppLineId == id);
            }
            if (type == "Ipir")
            {
                productActivityApps = productActivityApps.Where(w => w.IpirReportId == id);
            }
            if (type == "Production Routine")
            {
                productActivityApps = productActivityApps.Where(w => w.ProductionActivityRoutineAppLineId == id);
            }
            if (type == "Production Planning")
            {
                productActivityApps = productActivityApps.Where(w => w.ProductionActivityPlanningAppLineId == id);
            }
            var productActivityApp = productActivityApps.AsNoTracking().FirstOrDefault();
            ProductionActivityNonComplianceModel productActivityAppModels = new ProductionActivityNonComplianceModel();
            if (productActivityApp != null)
            {
                productActivityAppModels.ProductionActivityRoutineAppLineId = productActivityApp.ProductionActivityRoutineAppLineId;
                productActivityAppModels.Type = productActivityApp.Type;
                productActivityAppModels.ProductionActivityNonComplianceId = productActivityApp.ProductionActivityNonComplianceId;
                productActivityAppModels.IpirReportId = productActivityApp.IpirReportId;
                productActivityAppModels.ProductionActivityRoutineAppLineId = productActivityApp.ProductionActivityRoutineAppLineId;
                productActivityAppModels.ModifiedByUserID = productActivityApp.ModifiedByUserId;
                productActivityAppModels.AddedByUserID = productActivityApp.AddedByUserId;
                productActivityAppModels.AddedDate = productActivityApp.AddedDate;
                productActivityAppModels.ModifiedDate = productActivityApp.ModifiedDate;
                productActivityAppModels.ActionType = productActivityApp.ActionType;
                productActivityAppModels.Notes = productActivityApp.Notes;
                productActivityAppModels.ProductionActivityPlanningAppLineId = productActivityApp.ProductionActivityPlanningAppLineId;
                productActivityAppModels.UserIDs = productActivityApp.ProductionActivityNonComplianceUser != null ? productActivityApp.ProductionActivityNonComplianceUser.Select(s => s.UserId).ToList() : new List<long?>();
                List<ProductionActivityNonComplianceUserModel> ProductionActivityNonComplianceUserModels = new List<ProductionActivityNonComplianceUserModel>();
                var userNames = _context.ApplicationUser.Where(w => productActivityAppModels.UserIDs.Contains(w.UserId)).ToList();
                if (productActivityApp.ProductionActivityNonComplianceUser != null)
                {
                    productActivityApp.ProductionActivityNonComplianceUser.ToList().ForEach(a =>
                    {
                        ProductionActivityNonComplianceUserModel productionActivityNonComplianceUserModel = new ProductionActivityNonComplianceUserModel();
                        productionActivityNonComplianceUserModel.ProductionActivityNonComplianceId = a.ProductionActivityNonComplianceId;
                        productionActivityNonComplianceUserModel.ProductionActivityNonComplianceUserId = a.ProductionActivityNonComplianceUserId;
                        productionActivityNonComplianceUserModel.UserId = a.UserId;
                        productionActivityNonComplianceUserModel.Notes = productActivityApp.Notes;
                        productionActivityNonComplianceUserModel.UserName = userNames != null ? userNames.FirstOrDefault(f => f.UserId == a.UserId)?.UserName : string.Empty;
                        ProductionActivityNonComplianceUserModels.Add(productionActivityNonComplianceUserModel);

                    });
                }
                productActivityAppModels.productionActivityNonComplianceUserModels = ProductionActivityNonComplianceUserModels;
            }
            return productActivityAppModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityNonCompliance")]
        public ProductionActivityNonComplianceModel InsertProductionActivityNonCompliance(ProductionActivityNonComplianceModel value)
        {
            var productActivityApps = new ProductionActivityNonCompliance
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                Type = value.Type,
                ProductionActivityAppLineId = value.ProductionActivityAppLineId,
                IpirReportId = value.IpirReportId,
                ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId,
                ProductionActivityPlanningAppLineId = value.ProductionActivityPlanningAppLineId,
                ActionType = value.ActionType,
                Notes = value.Notes,
            };
            if (value.UserIDs != null && value.UserIDs.Count > 0)
            {
                value.UserIDs.ForEach(s =>
                {
                    var productionActivityNonComplianceUser = new ProductionActivityNonComplianceUser
                    {
                        UserId = s,
                    };
                    productActivityApps.ProductionActivityNonComplianceUser.Add(productionActivityNonComplianceUser);
                });
            }
            _context.ProductionActivityNonCompliance.Add(productActivityApps);
            _context.SaveChanges();
            value.ProductionActivityNonComplianceId = productActivityApps.ProductionActivityNonComplianceId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityNonCompliance")]
        public ProductionActivityNonComplianceModel UpdateProductionActivityNonCompliance(ProductionActivityNonComplianceModel value)
        {
            var productActivityApp = _context.ProductionActivityNonCompliance.SingleOrDefault(s => s.ProductionActivityNonComplianceId == value.ProductionActivityNonComplianceId);
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.Notes = value.Notes;
            var ProductionActivityNonComplianceUserReomve = _context.ProductionActivityNonComplianceUser.Where(w => w.ProductionActivityNonComplianceId == productActivityApp.ProductionActivityNonComplianceId).ToList();
            if (ProductionActivityNonComplianceUserReomve != null)
            {
                _context.ProductionActivityNonComplianceUser.RemoveRange(ProductionActivityNonComplianceUserReomve);
                _context.SaveChanges();
            }
            if (value.UserIDs != null && value.UserIDs.Count > 0)
            {
                value.UserIDs.ForEach(s =>
                {
                    var productionActivityNonComplianceUser = new ProductionActivityNonComplianceUser
                    {
                        ProductionActivityNonComplianceId = productActivityApp.ProductionActivityNonComplianceId,
                        UserId = s,
                    };
                    _context.ProductionActivityNonComplianceUser.Add(productionActivityNonComplianceUser);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityNonCompliance")]
        public void DeleteProductionActivityNonCompliance(int id)
        {
            var wikiresponsible = _context.ProductionActivityNonComplianceUser.SingleOrDefault(p => p.ProductionActivityNonComplianceUserId == id);
            if (wikiresponsible != null)
            {
                _context.ProductionActivityNonComplianceUser.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetProductionActivityCheckedDetails")]
        public List<ProductionActivityCheckedDetailsModel> GetProductionActivityCheckedDetails(long? id)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 340, 341, 342 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var productActivityApps = _context.ProductionActivityCheckedDetails.Include(p=>p.AddedByUser).Include(p=>p.CheckedBy).Where(w => w.ProductionActivityAppLineId == id).ToList();
            List<ProductionActivityCheckedDetailsModel> productionActivityCheckedDetailsModels = new List<ProductionActivityCheckedDetailsModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                productActivityApps.ForEach(s =>
                {
                    ProductionActivityCheckedDetailsModel productActivityApp = new ProductionActivityCheckedDetailsModel();
                    productActivityApp.ActivityInfoId = s.ActivityInfoId;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.CommentImage = s.CommentImage;
                    productActivityApp.CommentImageType = s.CommentImageType;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedComment = s.CheckedComment;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.ProductionActivityCheckedDetailsId = s.ProductionActivityCheckedDetailsId;
                    productActivityApp.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.ModifiedDate= s.ModifiedDate;
                    productActivityApp.ActivityInfoName = s.ActivityInfo?.CodeValue;
                    productActivityApp.CheckedByUserName = s.CheckedBy?.UserName;
                    productActivityApp.ActivityResultId = s.ActivityResultId;
                    productActivityApp.ActivityStatusId = s.ActivityStatusId;
                    productActivityApp.ActivityStatusName = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.ActivityStatusId).FirstOrDefault()?.Value;
                    productActivityApp.ActivityResultName = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.ActivityResultId).FirstOrDefault()?.Value;
                    productionActivityCheckedDetailsModels.Add(productActivityApp);
                });
            }
            return productionActivityCheckedDetailsModels;
        }
        [HttpPost]
        [Route("InsertProductionActivityCheckedDetails")]
        public ProductionActivityCheckedDetailsModel InsertProductionActivityCheckedDetails(ProductionActivityCheckedDetailsModel value)
        {
            var productActivityApps = new ProductionActivityCheckedDetails
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,              
                ActivityInfoId = value.ActivityInfoId,
                ProductionActivityAppId = value.ProductionActivityAppId,
                IsCheckNoIssue      = value.IsCheckNoIssue, 
                CheckedById     = value.CheckedById,    
                CheckedComment   = value.CheckedComment,    
                CheckedDate     = DateTime.Now,    
                CommentImage            = value.CommentImage,   
                CommentImageType     =  value.CommentImageType,
                IsCheckReferSupportDocument = value.IsCheckReferSupportDocument,
                ProductionActivityAppLineId = value.ProductionActivityAppLineId,
                ActivityStatusId = value.ActivityStatusId,
                ActivityResultId = value.ActivityResultId,
                
            };
           
            _context.ProductionActivityCheckedDetails.Add(productActivityApps);
            _context.SaveChanges();
            value.ProductionActivityCheckedDetailsId = productActivityApps.ProductionActivityCheckedDetailsId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityCheckedDetails")]
        public ProductionActivityCheckedDetailsModel UpdateProductionActivityCheckedDetails(ProductionActivityCheckedDetailsModel value)
        {
            var productActivityApp = _context.ProductionActivityCheckedDetails.SingleOrDefault(s => s.ProductionActivityCheckedDetailsId == value.ProductionActivityCheckedDetailsId);
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.ActivityInfoId = value.ActivityInfoId;           
            productActivityApp.CheckedComment = value.CheckedComment;
            productActivityApp.CommentImage = value.CommentImage;
            productActivityApp.CommentImageType = value.CommentImageType;
            productActivityApp.IsCheckNoIssue = value.IsCheckNoIssue;
            productActivityApp.IsCheckReferSupportDocument = value.IsCheckReferSupportDocument;           
            productActivityApp.ActivityResultId = value.ActivityResultId;
            productActivityApp.ActivityStatusId = value.ActivityStatusId;

            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityCheckedDetails")]
        public void DeleteProductionActivityCheckedDetails(int id)
        {
            var wikiresponsible = _context.ProductionActivityCheckedDetails.SingleOrDefault(p => p.ProductionActivityCheckedDetailsId == id);
            if (wikiresponsible != null)
            {
                _context.ProductionActivityCheckedDetails.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }
    }
}
