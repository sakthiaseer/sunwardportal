﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingBottleInsertController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingBottleInsertController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingBottleInsert")]
        public List<CommonPackagingBottleInsertModel> Get()
        {
            var commonPackagingBottleInsert = _context.CommonPackagingBottleInsert
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingBottleInsertModel> commonPackagingBottleInsertModel = new List<CommonPackagingBottleInsertModel>();
            if (commonPackagingBottleInsert.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottleInsert.Where(w => w.PackagingMaterialColorId != null).Select(a => a.PackagingMaterialColorId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottleInsert.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleInsert.Where(w => w.PackagingTypeId != null).Select(a => a.PackagingTypeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleInsert.Where(w => w.PackagingMaterialShapeId != null).Select(a => a.PackagingMaterialShapeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottleInsert.ForEach(s =>
                {
                    CommonPackagingBottleInsertModel commonPackagingBottleInsertModels = new CommonPackagingBottleInsertModel
                    {
                        BottleInsertSpecificationId = s.BottleInsertSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        Measurement = s.Measurement,
                        Code = s.Code,
                        PackagingMaterialColorId = s.PackagingMaterialColorId,
                        PackagingMaterialShapeId = s.PackagingMaterialShapeId,
                        PackagingTypeId = s.PackagingTypeId,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleInsertSpecificationId == s.BottleInsertSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackagingMaterialColorName = s.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingTypeName = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingMaterialShapeName = s.PackagingMaterialShapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialShapeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingBottleInsertModel.Add(commonPackagingBottleInsertModels);
                });
            }
            return commonPackagingBottleInsertModel.OrderByDescending(a => a.BottleInsertSpecificationId).ToList();
        }

        [HttpPost]
        [Route("GetCommonPackagingBottleInsertByRefNo")]
        public List<CommonPackagingBottleInsertModel> GetCommonPackagingBottleInsertByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingBottleInsert = _context.CommonPackagingBottleInsert
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).Where(w => w.BottleInsertSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingBottleInsert = commonPackagingBottleInsert.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingBottleInsert = commonPackagingBottleInsert.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingBottleInserts = commonPackagingBottleInsert.AsNoTracking().ToList();
            List<CommonPackagingBottleInsertModel> commonPackagingBottleInsertModel = new List<CommonPackagingBottleInsertModel>();
            if (commonPackagingBottleInserts.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottleInserts.Where(w => w.PackagingMaterialColorId != null).Select(a => a.PackagingMaterialColorId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottleInserts.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleInserts.Where(w => w.PackagingTypeId != null).Select(a => a.PackagingTypeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleInserts.Where(w => w.PackagingMaterialShapeId != null).Select(a => a.PackagingMaterialShapeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottleInserts.ForEach(s =>
                {
                    CommonPackagingBottleInsertModel commonPackagingBottleInsertModels = new CommonPackagingBottleInsertModel
                    {
                        BottleInsertSpecificationId = s.BottleInsertSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        Measurement = s.Measurement,
                        Code = s.Code,
                        PackagingMaterialColorId = s.PackagingMaterialColorId,
                        PackagingMaterialShapeId = s.PackagingMaterialShapeId,
                        PackagingTypeId = s.PackagingTypeId,
                        PackagingMaterialColorName = s.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingTypeName = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingMaterialShapeName = s.PackagingMaterialShapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialShapeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleInsertSpecificationId == s.BottleInsertSpecificationId).Select(b => b.UseForItemId).ToList(),
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    commonPackagingBottleInsertModel.Add(commonPackagingBottleInsertModels);
                });
            }
            return commonPackagingBottleInsertModel.OrderByDescending(o => o.BottleInsertSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingBottleInsertModel> GetData(SearchModel searchModel)
        {
            var commonPackagingBottleInsert = new CommonPackagingBottleInsert();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderBy(o => o.BottleInsertSpecificationId).FirstOrDefault(s => s.BottleInsertSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.OrderByDescending(o => o.BottleInsertSpecificationId).FirstOrDefault(s => s.BottleInsertSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingBottleInsertModel>(commonPackagingBottleInsert);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingBottleInsert")]
        public CommonPackagingBottleInsertModel Post(CommonPackagingBottleInsertModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Code });
            var commonPackagingBottleInsert = new CommonPackagingBottleInsert
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                Measurement = value.Measurement,
                Code = value.Code,
                PackagingMaterialShapeId = value.PackagingMaterialShapeId,
                PackagingMaterialColorId = value.PackagingMaterialColorId,
                PackagingTypeId = value.PackagingTypeId,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingBottleInsert.Add(commonPackagingBottleInsert);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingBottleInsert.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingBottleInsert.ProfileLinkReferenceNo;
            value.BottleInsertSpecificationId = commonPackagingBottleInsert.BottleInsertSpecificationId;
            value.PackagingItemName = UpdateCommonPackage(value);
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.BottleInsertSpecificationId == value.BottleInsertSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        BottleInsertSpecificationId = value.BottleInsertSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingBottleInsertModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    var masterDetailList = _context.ApplicationMasterDetail.ToList();
                    var PackagingMaterialColorName = value.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "";
                    var PackagingTypeName = value.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "";
                    var PackagingMaterialShapeName = value.PackagingMaterialShapeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialShapeId).Select(m => m.Value).FirstOrDefault() : "";
                    if (!string.IsNullOrEmpty(value.Code))
                    {
                        itemName = value.Code + " ";
                    }
                    if (value.Measurement != null && value.Measurement > 0)
                    {
                        itemName = itemName + value.Measurement + "mm" + " ";
                    }
                    if (!string.IsNullOrEmpty(PackagingMaterialColorName))
                    {
                        itemName = itemName + PackagingMaterialColorName + " ";
                    }
                    if (!string.IsNullOrEmpty(PackagingTypeName))
                    {
                        itemName = itemName + PackagingTypeName + " ";
                    }
                    if (!string.IsNullOrEmpty(PackagingMaterialShapeName))
                    {
                        itemName = itemName + PackagingMaterialShapeName + " ";
                    }
                    itemName = itemName + "Insert";
                    //itemName = value.Code + " " + value.Measurement + "mm" + " " + PackagingMaterialColorName + " " + PackagingTypeName + " " + PackagingMaterialShapeName + " " + "Insert";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingBottleInsert")]
        public CommonPackagingBottleInsertModel Put(CommonPackagingBottleInsertModel value)
        {
            var commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.SingleOrDefault(p => p.BottleInsertSpecificationId == value.BottleInsertSpecificationId);
            commonPackagingBottleInsert.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingBottleInsert.Set = value.Set;
            commonPackagingBottleInsert.Measurement = value.Measurement;
            commonPackagingBottleInsert.Code = value.Code;
            commonPackagingBottleInsert.PackagingMaterialShapeId = value.PackagingMaterialShapeId;
            commonPackagingBottleInsert.PackagingMaterialColorId = value.PackagingMaterialColorId;
            commonPackagingBottleInsert.PackagingTypeId = value.PackagingTypeId;
            commonPackagingBottleInsert.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingBottleInsert.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingBottleInsert.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingBottleInsert.VersionControl = value.VersionControl;
            commonPackagingBottleInsert.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingBottleInsert.ModifiedDate = DateTime.Now;
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    var exist = _context.BottleCapUseForItems.Where(b => b.BottleInsertSpecificationId == value.BottleInsertSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    if (exist == null)
                    {
                        var bottlecapUseForItems = new BottleCapUseForItems()
                        {
                            BottleInsertSpecificationId = value.BottleInsertSpecificationId,
                            UseForItemId = u,
                        };
                        _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                        _context.SaveChanges();
                    }
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingBottleInsert")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingBottleInsert = _context.CommonPackagingBottleInsert.Where(p => p.BottleInsertSpecificationId == id).FirstOrDefault();
                if (commonPackagingBottleInsert != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.BottleInsertSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingBottleInsert.Remove(commonPackagingBottleInsert);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}