﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SubSectionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SubSectionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSubSections")]
        public List<SubSectionModel> Get()
        {
            List<SubSectionModel> subSectionModels = new List<SubSectionModel>();
            var subSection = _context.SubSection
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(s => s.StatusCode)
               .Include(s => s.Section)
               .Include(s => s.Section.Department)
               .Include(s => s.Section.Department.Division)
               .Include(s => s.Section.Department.Division.Company)              
               .OrderByDescending(o => o.SubSectionId).AsNoTracking().ToList();
            subSection.ForEach(s =>
            {
                SubSectionModel subSectionModel = new SubSectionModel
                {
                    SubSectionID = s.SubSectionId,
                    SectionID = s.SectionId,
                    SectionName = s.Section?.Name,
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    HeadCount = s.HeadCount,
                    CompanyName = s.Section?.Department?.Division?.Company?.Description,
                    DivisionName = s.Section?.Department?.Division?.Name,
                    DepartmentName = s.Section?.Department?.Name,                  
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ProfileCode = s.ProfileCode,
                    ModifiedDate = s.ModifiedDate
                };
                subSectionModels.Add(subSectionModel);
            });
           
            if (subSectionModels != null && subSectionModels.Count > 0)
            {
                subSectionModels.ForEach(d =>
                {
                    d.CompanyDivisionName = d.CompanyName + " | " + d.DivisionName + " | " + d.DepartmentName + " | " + d.SectionName + " | " + d.Name;

                });
            }           
            return subSectionModels;
        }
        [HttpGet]
        [Route("GetSubSectionsBySection")]
        public List<SubSectionModel> GetSubSectionsBySection(int id)
        {
            List<SubSectionModel> subSectionModels = new List<SubSectionModel>();
            var subSection = _context.SubSection
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("Section").Where(s => s.SectionId == id).AsNoTracking().ToList();
            subSection.ForEach(s =>
            {
                SubSectionModel subSectionModel = new SubSectionModel
                {
                    SubSectionID = s.SubSectionId,
                    SectionID = s.SectionId,
                    SectionName = s.Section?.Name,
                    Name = s.Name,
                    HeadCount = s.HeadCount,
                    Code = s.Code,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    AddedDate = s.AddedDate,
                    ProfileCode = s.ProfileCode,
                    ModifiedDate = s.ModifiedDate
                };
                subSectionModels.Add(subSectionModel);
                });
           
            //var result = _mapper.Map<List<SubSectionModel>>(SubSection);
            return subSectionModels;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get SubSection")]
        [HttpGet("GetSubSections/{id:int}")]
        public ActionResult<SubSectionModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subSection = _context.SubSection.SingleOrDefault(p => p.SubSectionId == id.Value);
            var result = _mapper.Map<SubSectionModel>(subSection);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SubSectionModel> GetData(SearchModel searchModel)
        {
            var subSection = new SubSection();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).FirstOrDefault();
                        break;
                    case "Last":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).LastOrDefault();
                        break;
                    case "Next":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).LastOrDefault();
                        break;
                    case "Previous":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).FirstOrDefault();
                        break;
                    case "Last":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).LastOrDefault();
                        break;
                    case "Next":
                        subSection = _context.SubSection.OrderBy(o => o.SubSectionId).FirstOrDefault(s => s.SubSectionId > searchModel.Id);
                        break;
                    case "Previous":
                        subSection = _context.SubSection.OrderByDescending(o => o.SubSectionId).FirstOrDefault(s => s.SubSectionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SubSectionModel>(subSection);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSubSection")]
        public SubSectionModel Post(SubSectionModel value)
        {
            var subSection = new SubSection
            {
                SectionId = value.SectionID,
                Name = value.Name,
                Code = value.Code,
                Description = value.Description,
                HeadCount = value.HeadCount,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProfileCode = value.ProfileCode,

            };
            _context.SubSection.Add(subSection);
            _context.SaveChanges();
            value.SubSectionID = subSection.SubSectionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSubSection")]
        public SubSectionModel Put(SubSectionModel value)
        {
            var subSection = _context.SubSection.SingleOrDefault(p => p.SubSectionId == value.SubSectionID);

            subSection.SectionId = value.SectionID;
            subSection.Name = value.Name;
            subSection.Code = value.Code;
            subSection.Description = value.Description;
            subSection.HeadCount = value.HeadCount;
            subSection.ModifiedByUserId = value.ModifiedByUserID;
            subSection.ModifiedDate = DateTime.Now;
            subSection.StatusCodeId = value.StatusCodeID.Value;
            subSection.ProfileCode = value.ProfileCode;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSubSection")]
        public void Delete(int id)
        {
            try
            {
                var subSection = _context.SubSection.SingleOrDefault(p => p.SubSectionId == id);
                if (subSection != null)
                {
                    _context.SubSection.Remove(subSection);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Sub Section Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}