﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class LocalClinicController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public LocalClinicController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetLocalClinic")]
        public List<LocalClinicModel> Get()
        {
            List<LocalClinicModel> localClinicModels = new List<LocalClinicModel>();
            var localClinic = _context.LocalClinic.Include(c => c.CompanyStatus).Include(d => d.ToSwstatus).AsNoTracking().ToList();
            if (localClinic.Count > 0)
            {
                List<long?> masterIds = localClinic.Where(w => w.CountryId != null).Select(a => a.CountryId).Distinct().ToList();
                masterIds.AddRange(localClinic.Where(w => w.StateId != null).Select(a => a.StateId).Distinct().ToList());
                masterIds.AddRange(localClinic.Where(w => w.CityId != null).Select(a => a.CityId).Distinct().ToList());
                masterIds.AddRange(localClinic.Where(w => w.SpecialityId != null).Select(a => a.SpecialityId).Distinct().ToList());
                masterIds.AddRange(localClinic.Where(w => w.SalesmanCodeId != null).Select(a => a.SalesmanCodeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                localClinic.ForEach(s =>
                {
                    LocalClinicModel localClinicModel = new LocalClinicModel();
                    localClinicModel.LocalClinicId = s.LocalClinicId;
                    localClinicModel.CountryId = s.CountryId;
                    localClinicModel.Company = s.Company;
                    localClinicModel.Address1 = s.Address1;
                    localClinicModel.Address2 = s.Address2;
                    localClinicModel.StateId = s.StateId;
                    localClinicModel.CityId = s.CityId;
                    localClinicModel.PostalCode = s.PostalCode;
                    localClinicModel.SpecialityId = s.SpecialityId;
                    localClinicModel.CompanyStatusId = s.CompanyStatusId;
                    localClinicModel.ToSwstatusId = s.ToSwstatusId;
                    localClinicModel.CountryName = s.CountryId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CountryId).Select(m => m.Value).FirstOrDefault() : "");
                    localClinicModel.StateName = s.StateId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.StateId).Select(m => m.Value).FirstOrDefault() : "");
                    localClinicModel.CityName = s.CityId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CityId).Select(m => m.Value).FirstOrDefault() : "");
                    localClinicModel.SpecialityName = s.SpecialityId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SpecialityId).Select(m => m.Value).FirstOrDefault() : "");
                    localClinicModel.CompanyStatusName = s.CompanyStatus?.CodeValue;
                    localClinicModel.ToSwstatusName = s.ToSwstatus?.CodeValue;
                    localClinicModel.AddedDate = s.AddedDate;
                    localClinicModel.ModifiedDate = s.ModifiedDate;
                    //localClinicModel.AddedByUser = s.AddedByUser?.UserName;
                    //localClinicModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    localClinicModel.ClinicName = s.Company + "|" + s.PostalCode;
                    localClinicModel.SalesmanCodeName = s.SalesmanCodeId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SalesmanCodeId).Select(m => m.Value).FirstOrDefault() : "");
                    localClinicModel.SalesmanCodeId = s.SalesmanCodeId;
                    localClinicModels.Add(localClinicModel);
                });
            }
            return localClinicModels.OrderByDescending(o => o.LocalClinicId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<LocalClinicModel> GetData(SearchModel searchModel)
        {
            var localClinic = new LocalClinic();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).FirstOrDefault();
                        break;
                    case "Last":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).LastOrDefault();
                        break;
                    case "Next":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).LastOrDefault();
                        break;
                    case "Previous":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).FirstOrDefault();
                        break;
                    case "Last":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).LastOrDefault();
                        break;
                    case "Next":
                        localClinic = _context.LocalClinic.OrderBy(o => o.LocalClinicId).FirstOrDefault(s => s.LocalClinicId > searchModel.Id);
                        break;
                    case "Previous":
                        localClinic = _context.LocalClinic.OrderByDescending(o => o.LocalClinicId).FirstOrDefault(s => s.LocalClinicId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<LocalClinicModel>(localClinic);
            return result;
        }
        [HttpPost]
        [Route("InsertLocalClinic")]
        public LocalClinicModel Post(LocalClinicModel value)
        {
            var localClinic = new LocalClinic
            {
                CountryId = value.CountryId,
                Company = value.Company,
                Address1 = value.Address1,
                Address2 = value.Address2,
                StateId = value.StateId,
                CityId = value.CityId,
                PostalCode = value.PostalCode,
                SpecialityId = value.SpecialityId,
                CompanyStatusId = value.CompanyStatusId,
                ToSwstatusId = value.ToSwstatusId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SalesmanCodeId = value.SalesmanCodeId,
            };
            _context.LocalClinic.Add(localClinic);
            _context.SaveChanges();
            value.LocalClinicId = localClinic.LocalClinicId;
            return value;
        }
        [HttpPut]
        [Route("UpdateLocalClinic")]
        public LocalClinicModel Put(LocalClinicModel value)
        {
            var localClinic = _context.LocalClinic.SingleOrDefault(p => p.LocalClinicId == value.LocalClinicId);
            localClinic.CountryId = value.CountryId;
            localClinic.Company = value.Company;
            localClinic.Address1 = value.Address1;
            localClinic.Address2 = value.Address2;
            localClinic.StateId = value.StateId;
            localClinic.CityId = value.CityId;
            localClinic.PostalCode = value.PostalCode;
            localClinic.SpecialityId = value.SpecialityId;
            localClinic.CompanyStatusId = value.CompanyStatusId;
            localClinic.ToSwstatusId = value.ToSwstatusId;
            localClinic.ModifiedByUserId = value.ModifiedByUserID;
            localClinic.ModifiedDate = DateTime.Now;
            localClinic.SalesmanCodeId = value.SalesmanCodeId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteLocalClinic")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var localClinic = _context.LocalClinic.SingleOrDefault(p => p.LocalClinicId == id);
                if (localClinic != null)
                {
                    _context.LocalClinic.Remove(localClinic);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
