﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SampleRequestForDoctorController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public SampleRequestForDoctorController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        // GET: api/Project
        #region SampleRequestForDoctorHeader
        [HttpGet]
        [Route("GetSampleRequestForDoctorHeader")]
        public List<SampleRequestForDoctorHeaderModel> Get()
        {
            List<SampleRequestForDoctorHeaderModel> sampleRequestForDoctorHeaderModels = new List<SampleRequestForDoctorHeaderModel>();
            SampleRequestForDoctorHeaderModel sampleRequestForDoctorHeaderModel = new SampleRequestForDoctorHeaderModel();
            var sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader
                .Include(i => i.AddedByUser)
                .Include(i => i.ModifiedByUser)
                .Include(i => i.InventoryType)
                .Include(i => i.StatusCode)
                .Include(s => s.ClassComCustomer)
                .Include(a => a.TypeOfStock)
               .Include(b => b.RequestBy)
                .OrderByDescending(o => o.SampleRequestForDoctorHeaderId).AsNoTracking().ToList();
            if (sampleRequestForDoctorHeader != null && sampleRequestForDoctorHeader.Count > 0)
            {
                sampleRequestForDoctorHeader.ForEach(s =>
                {
                    sampleRequestForDoctorHeaderModel = new SampleRequestForDoctorHeaderModel();
                    sampleRequestForDoctorHeaderModel.SampleRequestForDoctorHeaderId = s.SampleRequestForDoctorHeaderId;
                    sampleRequestForDoctorHeaderModel.TypeOfStockId = s.TypeOfStockId;
                    sampleRequestForDoctorHeaderModel.TypeOfStockName = s.TypeOfStock?.Value;
                    sampleRequestForDoctorHeaderModel.ProfileId = s.ProfileId;
                    sampleRequestForDoctorHeaderModel.ProfileName = s.Profile?.Name;
                    sampleRequestForDoctorHeaderModel.SampleChitNo = s.SampleChitNo;
                    sampleRequestForDoctorHeaderModel.DateBy = s.DateBy;
                    sampleRequestForDoctorHeaderModel.RequestById = s.RequestById;
                    sampleRequestForDoctorHeaderModel.RequestByName = s.RequestBy?.UserName;
                    sampleRequestForDoctorHeaderModel.InventoryTypeId = s.InventoryTypeId;
                    sampleRequestForDoctorHeaderModel.InventoryTypeName = s.InventoryType?.Name;
                    sampleRequestForDoctorHeaderModel.ClassComCustomerId = s.ClassComCustomerId;
                    sampleRequestForDoctorHeaderModel.ClassComCustomerName = s.ClassComCustomer?.CompanyName;
                    sampleRequestForDoctorHeaderModel.AddedByUser = s.AddedByUser?.UserName;
                    sampleRequestForDoctorHeaderModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    sampleRequestForDoctorHeaderModel.StatusCode = s.StatusCode?.CodeValue;
                    sampleRequestForDoctorHeaderModel.AddedByUserID = s.AddedByUserId;
                    sampleRequestForDoctorHeaderModel.ModifiedByUserID = s.ModifiedByUserId;
                    sampleRequestForDoctorHeaderModel.StatusCodeID = s.StatusCodeId;
                    sampleRequestForDoctorHeaderModel.AddedDate = s.AddedDate;
                    sampleRequestForDoctorHeaderModel.Signature = s.Signature;
                    sampleRequestForDoctorHeaderModel.Remarks = s.Remarks;
                    sampleRequestForDoctorHeaderModel.IsConfirm = s.IsConfirm;
                    sampleRequestForDoctorHeaderModel.SessionId = s.SessionId;
                    sampleRequestForDoctorHeaderModels.Add(sampleRequestForDoctorHeaderModel);
                });

            }
            return sampleRequestForDoctorHeaderModels;
        }
        [HttpPost()]
        [Route("GetSampleRequestForDoctorHeaderData")]
        public ActionResult<SampleRequestForDoctorHeaderModel> GetSampleRequestForDoctorHeaderData(SearchModel searchModel)
        {
            var sampleRequestForDoctorHeader = new SampleRequestForDoctorHeader();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).FirstOrDefault();
                        break;
                    case "Last":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).LastOrDefault();
                        break;
                    case "Next":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).LastOrDefault();
                        break;
                    case "Previous":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).FirstOrDefault();
                        break;
                    case "Last":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).LastOrDefault();
                        break;
                    case "Next":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderBy(o => o.SampleRequestForDoctorHeaderId).FirstOrDefault(s => s.SampleRequestForDoctorHeaderId > searchModel.Id);
                        break;
                    case "Previous":
                        sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(b => b.RequestBy).OrderByDescending(o => o.SampleRequestForDoctorHeaderId).FirstOrDefault(s => s.SampleRequestForDoctorHeaderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SampleRequestForDoctorHeaderModel>(sampleRequestForDoctorHeader);
            if (result != null)
            {
                result.RequestByName = sampleRequestForDoctorHeader?.RequestBy?.UserName;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertSampleRequestForDoctorHeader")]
        public SampleRequestForDoctorHeaderModel Post(SampleRequestForDoctorHeaderModel value)
        {
            value.ProfileId = _context.ApplicationForm.FirstOrDefault(a => a.ScreenId == value.ScreenId).ProfileId;
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = value.StatusCodeID, Title = "SampleRequestForDoctorHeader" });
            var SessionId = Guid.NewGuid();
            var sampleRequestForDoctorHeader = new SampleRequestForDoctorHeader
            {
                TypeOfStockId = value.TypeOfStockId,
                ProfileId = value.ProfileId,
                DateBy = value.DateBy,
                RequestById = value.RequestById,
                InventoryTypeId = value.InventoryTypeId,
                ClassComCustomerId = value.ClassComCustomerId,
                AddedByUserId = value.AddedByUserID,
                Purpose = value.Purpose,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SampleChitNo = profileNo,
                Signature = value.Signature,
                Remarks = value.Remarks,
                IsConfirm = value.IsConfirm,
                SessionId = SessionId,
            };
            _context.SampleRequestForDoctorHeader.Add(sampleRequestForDoctorHeader);
            _context.SaveChanges();
            value.SampleChitNo = sampleRequestForDoctorHeader.SampleChitNo;
            value.SampleRequestForDoctorHeaderId = sampleRequestForDoctorHeader.SampleRequestForDoctorHeaderId;
            value.SessionId = SessionId;
            return value;
        }
        private void UpdateStockInfoByConfirm(SampleRequestForDoctorHeaderModel value)
        {
            var sampleRequestForDoctorLine = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == value.SampleRequestForDoctorHeaderId && w.ItemId != null && w.IsOutOfStock != false).ToList();
            if (sampleRequestForDoctorLine != null && sampleRequestForDoctorLine.Count > 0)
            {
                sampleRequestForDoctorLine.ForEach(s =>
                {
                    var sampleRequestForDoctorLines = _context.SampleRequestForDoctor.SingleOrDefault(p => p.SampleRequestForDoctorId == s.SampleRequestForDoctorId);
                    var itemBatchInfo = _context.ItemBatchInfo.Where(w => w.ItemId == s.ItemId && w.BatchNo == s.BatchNo && w.QuantityOnHand > 0).FirstOrDefault();
                    if (itemBatchInfo != null)
                    {
                        var existingBatchInfo = _context.ItemBatchInfo.Where(s => s.ItemId == itemBatchInfo.ItemId && s.BatchNo == itemBatchInfo.BatchNo).FirstOrDefault();
                        if (existingBatchInfo != null)
                        {

                            existingBatchInfo.StockOutBatchConfirm -= sampleRequestForDoctorLines.OutStockQty;
                            existingBatchInfo.QuantityOnHand -= sampleRequestForDoctorLines.OutStockQty;
                            var issueQty = existingBatchInfo.IssueQuantity > 0 ? existingBatchInfo.IssueQuantity : 0;
                            existingBatchInfo.IssueQuantity = issueQty + sampleRequestForDoctorLines.OutStockQty;
                        }
                        var existingStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == itemBatchInfo.ItemId).FirstOrDefault();
                        if (existingStockInfo != null)
                        {
                            existingStockInfo.StockOutConfirm -= sampleRequestForDoctorLines.OutStockQty;
                            existingStockInfo.QuantityOnHand -= sampleRequestForDoctorLines.OutStockQty;
                            var issueQty = existingStockInfo.IssueQuantity > 0 ? existingStockInfo.IssueQuantity : 0;
                            existingStockInfo.IssueQuantity = issueQty + sampleRequestForDoctorLines.OutStockQty;
                        }
                        sampleRequestForDoctorLines.InStockQty = sampleRequestForDoctorLines.OutStockQty;
                        sampleRequestForDoctorLines.BalanceStockQty = sampleRequestForDoctorLines.OutStockQty;
                        sampleRequestForDoctorLines.TotalQuantity = sampleRequestForDoctorLines.OutStockQty;
                        sampleRequestForDoctorLines.IsOutOfStock = false;
                    }
                    else
                    {
                        sampleRequestForDoctorLines.IsOutOfStock = true;
                    }
                    _context.SaveChanges();
                });
            }
        }
        [HttpPut]
        [Route("UpdateSampleRequestForDoctorHeader")]
        public SampleRequestForDoctorHeaderModel UpdateSampleRequestForDoctorHeader(SampleRequestForDoctorHeaderModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.SingleOrDefault(p => p.SampleRequestForDoctorHeaderId == value.SampleRequestForDoctorHeaderId);
            sampleRequestForDoctorHeader.TypeOfStockId = value.TypeOfStockId;
            sampleRequestForDoctorHeader.ProfileId = value.ProfileId;
            // sampleRequestForDoctorHeader.DateBy = value.DateBy;
            //sampleRequestForDoctorHeader.RequestById = value.RequestById;
            sampleRequestForDoctorHeader.InventoryTypeId = value.InventoryTypeId;
            sampleRequestForDoctorHeader.ClassComCustomerId = value.ClassComCustomerId;
            sampleRequestForDoctorHeader.ModifiedByUserId = value.ModifiedByUserID;
            sampleRequestForDoctorHeader.Purpose = value.Purpose;
            sampleRequestForDoctorHeader.ModifiedDate = DateTime.Now;
            sampleRequestForDoctorHeader.StatusCodeId = value.StatusCodeID.Value;
            sampleRequestForDoctorHeader.SampleChitNo = value.SampleChitNo;
            sampleRequestForDoctorHeader.Signature = value.Signature;
            sampleRequestForDoctorHeader.Remarks = value.Remarks;
            sampleRequestForDoctorHeader.IsConfirm = value.IsConfirm;
            sampleRequestForDoctorHeader.SessionId = value.SessionId;
            _context.SaveChanges();
            if (sampleRequestForDoctorHeader.IsConfirm == true)
            {
                UpdateStockInfoByConfirm(value);
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteSampleRequestForDoctorHeader")]
        public void Delete(int id)
        {
            var sampleRequestForDoctorHeader = _context.SampleRequestForDoctorHeader.Include(a => a.SampleRequestForDoctor).SingleOrDefault(p => p.SampleRequestForDoctorHeaderId == id);
            if (sampleRequestForDoctorHeader != null)
            {
                _context.SampleRequestForDoctorHeader.Remove(sampleRequestForDoctorHeader);
                _context.SaveChanges();
            }
        }
        #endregion
        #region SampleRequestForDoctorLine
        [HttpGet]
        [Route("GetSampleRequestForDoctorLine")]
        public List<SampleRequestForDoctorModel> GetSampleRequestForDoctorLine(long? id)
        {

            List<SampleRequestForDoctorModel> SampleRequestForDoctorModels = new List<SampleRequestForDoctorModel>();
            SampleRequestForDoctorModel SampleRequestForDoctorModel = new SampleRequestForDoctorModel();
            var SampleRequestForDoctor = _context.SampleRequestForDoctor
                .Include(i => i.AddedByUser)
                .Include(i => i.ModifiedByUser)
                .Include(i => i.Item)
                .Include(i => i.StatusCode)
                .Include(s => s.Company)
                .Where(w => w.SampleRequestForDoctorHeaderId == id)
                .OrderByDescending(o => o.SampleRequestForDoctorId).AsNoTracking().ToList();
            if (SampleRequestForDoctor != null && SampleRequestForDoctor.Count > 0)
            {
                SampleRequestForDoctor.ForEach(s =>
                {
                    SampleRequestForDoctorModel = new SampleRequestForDoctorModel();
                    SampleRequestForDoctorModel.SampleRequestForDoctorID = s.SampleRequestForDoctorId;
                    SampleRequestForDoctorModel.SampleRequestForDoctorHeaderId = s.SampleRequestForDoctorHeaderId;
                    SampleRequestForDoctorModel.ItemId = s.ItemId;
                    SampleRequestForDoctorModel.CompanyId = s.CompanyId;
                    SampleRequestForDoctorModel.BatchNo = s.BatchNo;
                    SampleRequestForDoctorModel.ItemNo = s.Item?.No;
                    SampleRequestForDoctorModel.MfgDate = s.MfgDate;
                    SampleRequestForDoctorModel.ExpDate = s.ExpDate;
                    SampleRequestForDoctorModel.BalanceStockQty = s.BalanceStockQty;
                    SampleRequestForDoctorModel.InStockQty = s.InStockQty;
                    SampleRequestForDoctorModel.OutStockQty = s.OutStockQty;
                    SampleRequestForDoctorModel.AddedByUser = s.AddedByUser?.UserName;
                    SampleRequestForDoctorModel.CompanyName = s.Company?.Description;
                    SampleRequestForDoctorModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    SampleRequestForDoctorModel.StatusCode = s.StatusCode?.CodeValue;
                    SampleRequestForDoctorModel.AddedByUserID = s.AddedByUserId;
                    SampleRequestForDoctorModel.ModifiedByUserID = s.ModifiedByUserId;
                    SampleRequestForDoctorModel.StatusCodeID = s.StatusCodeId;
                    SampleRequestForDoctorModel.AddedDate = s.AddedDate;
                    SampleRequestForDoctorModel.TotalQuantity = s.TotalQuantity;
                    SampleRequestForDoctorModel.IsOutOfStock = s.IsOutOfStock;
                    SampleRequestForDoctorModel.ConfirmStockStatus = s.IsOutOfStock == true ? "Out Of Stock" : s.IsOutOfStock == false ? "Confirmed" : "Not Confirmed";
                    SampleRequestForDoctorModels.Add(SampleRequestForDoctorModel);
                });

            }
            return SampleRequestForDoctorModels;
        }

        [HttpGet]
        [Route("GetSampleRequestForDoctorByItemId")]
        public List<SampleRequestForDoctorModel> GetSampleRequestForDoctorByItemId(int id)
        {
            List<SampleRequestForDoctorModel> SampleRequestForDoctorModels = new List<SampleRequestForDoctorModel>();
            SampleRequestForDoctorModel SampleRequestForDoctorModel = new SampleRequestForDoctorModel();
            var SampleRequestForDoctor = _context.SampleRequestForDoctor.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == id).OrderByDescending(o => o.SampleRequestForDoctorId).AsNoTracking().ToList();
            if (SampleRequestForDoctor != null && SampleRequestForDoctor.Count > 0)
            {
                SampleRequestForDoctor.ForEach(s =>
                {
                    SampleRequestForDoctorModel = new SampleRequestForDoctorModel();
                    SampleRequestForDoctorModel.SampleRequestForDoctorHeaderId = s.SampleRequestForDoctorHeaderId;
                    SampleRequestForDoctorModel.SampleRequestForDoctorID = s.SampleRequestForDoctorId;
                    SampleRequestForDoctorModel.ItemId = s.ItemId;
                    SampleRequestForDoctorModel.CompanyId = s.CompanyId;
                    SampleRequestForDoctorModel.BatchNo = s.BatchNo;
                    SampleRequestForDoctorModel.ItemNo = s.Item?.No;
                    SampleRequestForDoctorModel.MfgDate = s.MfgDate;
                    SampleRequestForDoctorModel.ExpDate = s.ExpDate;
                    SampleRequestForDoctorModel.InStockQty = s.InStockQty;
                    SampleRequestForDoctorModel.OutStockQty = s.OutStockQty;
                    SampleRequestForDoctorModel.BalanceStockQty = s.BalanceStockQty;
                    SampleRequestForDoctorModel.AddedByUser = s.AddedByUser?.UserName;
                    SampleRequestForDoctorModel.CompanyName = s.Company?.Description;
                    SampleRequestForDoctorModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    SampleRequestForDoctorModel.StatusCode = s.StatusCode?.CodeValue;
                    SampleRequestForDoctorModel.AddedByUserID = s.AddedByUserId;
                    SampleRequestForDoctorModel.ModifiedByUserID = s.ModifiedByUserId;
                    SampleRequestForDoctorModel.StatusCodeID = s.StatusCodeId;
                    SampleRequestForDoctorModel.AddedDate = s.AddedDate;
                    SampleRequestForDoctorModel.TotalQuantity = s.TotalQuantity;
                    SampleRequestForDoctorModels.Add(SampleRequestForDoctorModel);
                });

            }
            return SampleRequestForDoctorModels;
        }
        [HttpPost()]
        [Route("GetSampleRequestForDoctorData")]
        public ActionResult<SampleRequestForDoctorModel> GetData(SearchModel searchModel)
        {
            var SampleRequestForDoctor = new SampleRequestForDoctor();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).FirstOrDefault();
                        break;
                    case "Last":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).LastOrDefault();
                        break;
                    case "Next":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).LastOrDefault();
                        break;
                    case "Previous":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).FirstOrDefault();
                        break;
                    case "Last":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).LastOrDefault();
                        break;
                    case "Next":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderBy(o => o.SampleRequestForDoctorId).FirstOrDefault(s => s.SampleRequestForDoctorId > searchModel.Id);
                        break;
                    case "Previous":
                        SampleRequestForDoctor = _context.SampleRequestForDoctor.Where(w => w.SampleRequestForDoctorHeaderId == searchModel.MasterTypeID).OrderByDescending(o => o.SampleRequestForDoctorId).FirstOrDefault(s => s.SampleRequestForDoctorId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SampleRequestForDoctorModel>(SampleRequestForDoctor);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSampleRequestForDoctor")]
        public SampleRequestForDoctorModel InsertSampleRequestForDoctor(SampleRequestForDoctorModel value)
        {

            var existingsample = _context.SampleRequestForDoctor
                .Where(s => s.BatchNo.ToLower().Trim() == value.BatchNo.ToLower().Trim() && s.SampleRequestForDoctorHeaderId == value.SampleRequestForDoctorHeaderId && s.ItemId == value.ItemId).FirstOrDefault();

            if (existingsample == null)
            {
                var SampleRequestForDoctor = new SampleRequestForDoctor
                {
                    SampleRequestForDoctorHeaderId = value.SampleRequestForDoctorHeaderId,
                    ItemId = value.ItemId.Value,
                    BatchNo = value.BatchNo,
                    MfgDate = value.MfgDate,
                    ExpDate = value.ExpDate,
                    AddedByUserId = value.AddedByUserID,
                    CompanyId = value.CompanyId,
                    //InStockQty = value.InStockQty,
                    OutStockQty = value.OutStockQty,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    //BalanceStockQty = value.InStockQty - value.OutStockQty,
                    //TotalQuantity = value.OutStockQty,
                };
                _context.SampleRequestForDoctor.Add(SampleRequestForDoctor);

                _context.SaveChanges();
                value.TotalQuantity = SampleRequestForDoctor.TotalQuantity;
                value.SampleRequestForDoctorID = SampleRequestForDoctor.SampleRequestForDoctorId;
            }
            else
            {
                existingsample.TotalQuantity += value.OutStockQty;
                existingsample.OutStockQty = value.OutStockQty;
                existingsample.InStockQty = value.InStockQty;
                existingsample.BalanceStockQty = value.InStockQty - value.OutStockQty;
                value.TotalQuantity = existingsample.TotalQuantity;
                _context.SaveChanges();
            }

            var existingBatchInfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
            if (existingBatchInfo != null)
            {
                var StockOutBatchConfirm = existingBatchInfo.StockOutBatchConfirm > 0 ? existingBatchInfo.StockOutBatchConfirm : 0;
                existingBatchInfo.StockOutBatchConfirm = StockOutBatchConfirm + value.OutStockQty;
            }
            var existingStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();
            if (existingStockInfo != null)
            {
                var StockOutConfirm = existingStockInfo.StockOutConfirm > 0 ? existingStockInfo.StockOutConfirm : 0;
                existingStockInfo.StockOutConfirm = StockOutConfirm + value.OutStockQty;
            }
            _context.SaveChanges();
            if (value.CompanyId > 0)
            {
                value.CompanyName = _context.Plant.Where(c => c.PlantId == value.CompanyId).Select(c => c.Description).FirstOrDefault();
            }
            if (value.ItemId > 0)
            {
                value.ItemNo = _context.Navitems.Where(s => s.ItemId == value.ItemId).Select(s => s.No).FirstOrDefault();
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSampleRequestForDoctor")]
        public SampleRequestForDoctorModel Put(SampleRequestForDoctorModel value)
        {

            var SampleRequestForDoctor = _context.SampleRequestForDoctor.SingleOrDefault(p => p.SampleRequestForDoctorId == value.SampleRequestForDoctorID);
            var OutStockQty = SampleRequestForDoctor.OutStockQty == null ? 0 : SampleRequestForDoctor.OutStockQty;
            SampleRequestForDoctor.SampleRequestForDoctorHeaderId = value.SampleRequestForDoctorHeaderId;
            SampleRequestForDoctor.ModifiedByUserId = value.ModifiedByUserID;
            SampleRequestForDoctor.ModifiedDate = DateTime.Now;
            SampleRequestForDoctor.StatusCodeId = value.StatusCodeID.Value;
            SampleRequestForDoctor.OutStockQty = value.OutStockQty;
            var existingBatchInfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
            if (existingBatchInfo != null)
            {
                var diff = value.OutStockQty - OutStockQty;
                if (diff > 0)
                {
                    var StockOutBatchConfirm = existingBatchInfo.StockOutBatchConfirm > 0 ? existingBatchInfo.StockOutBatchConfirm : 0;
                    existingBatchInfo.StockOutBatchConfirm = StockOutBatchConfirm + diff;
                }
                if (diff < 0)
                {
                    var StockOutBatchConfirm = existingBatchInfo.StockOutBatchConfirm > 0 ? existingBatchInfo.StockOutBatchConfirm : 0;
                    existingBatchInfo.StockOutBatchConfirm = StockOutBatchConfirm - diff;
                }
            }
            var existingStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();
            if (existingStockInfo != null)
            {
                var diff = value.OutStockQty - OutStockQty;
                if (diff > 0)
                {
                    var StockOutBatchConfirm = existingStockInfo.StockOutConfirm > 0 ? existingStockInfo.StockOutConfirm : 0;
                    existingStockInfo.StockOutConfirm = StockOutBatchConfirm + diff;
                }
                if (diff < 0)
                {
                    var StockOutBatchConfirm = existingStockInfo.StockOutConfirm > 0 ? existingStockInfo.StockOutConfirm : 0;
                    existingStockInfo.StockOutConfirm = StockOutBatchConfirm - diff;
                }
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSampleRequestForDoctor")]
        public void DeleteSampleRequestForDoctor(int id)
        {
            var SampleRequestForDoctor = _context.SampleRequestForDoctor.SingleOrDefault(p => p.SampleRequestForDoctorId == id);
            if (SampleRequestForDoctor != null)
            {
                var existingBatchInfo = _context.ItemBatchInfo.Where(s => s.ItemId == SampleRequestForDoctor.ItemId && s.BatchNo == SampleRequestForDoctor.BatchNo).FirstOrDefault();
                if (existingBatchInfo != null)
                {
                    var StockOutBatchConfirm = existingBatchInfo.StockOutBatchConfirm > 0 ? existingBatchInfo.StockOutBatchConfirm : 0;
                    existingBatchInfo.StockOutBatchConfirm = StockOutBatchConfirm - SampleRequestForDoctor.OutStockQty;
                }
                var existingStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == existingBatchInfo.ItemId).FirstOrDefault();
                if (existingStockInfo != null)
                {
                    var StockOutConfirm = existingStockInfo.StockOutConfirm > 0 ? existingStockInfo.StockOutConfirm : 0;
                    existingStockInfo.StockOutConfirm = StockOutConfirm - SampleRequestForDoctor.OutStockQty;
                }
                _context.SaveChanges();
                _context.SampleRequestForDoctor.Remove(SampleRequestForDoctor);
                _context.SaveChanges();
            }
        }
        #endregion
    }
}