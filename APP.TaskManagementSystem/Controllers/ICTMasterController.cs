﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ICTMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ICTMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetICTMasters")]
        public List<ICTMasterModel> Get()
        {
            var ICTMaster = _context.Ictmaster.Include("AddedByUser").Include("ModifiedByUser").AsNoTracking().Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyID = s.CompanyId,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId.Value,
                ParentICTID = s.ParentIctid,
                //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                VersionNo = s.VersionNo,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                LocationDescription = s.LocationDescription,

            }).OrderByDescending(o => o.ICTMasterID).ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            return ICTMaster;
        }

        [HttpGet]
        [Route("GetICTMasterlocation")]
        public List<ICTMasterModel> GetICTMasterlocation()
        {
            var ictMasterlist = _context.Ictmaster.AsNoTracking().ToList();
            var ICTMaster = _context.Ictmaster.Include("AddedByUser").Include("ModifiedByUser").Include("Company").AsNoTracking().Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyName = s.Company != null ? s.Company.PlantCode : "",
                CompanyID = s.CompanyId,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId,
                ParentICTID = s.ParentIctid,
                //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                VersionNo = s.VersionNo,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                LocationDescription = s.LocationDescription,

            }).Where(t => t.MasterType == 572).ToList().OrderByDescending(o => o.ICTMasterID).ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(i =>
                {
                    i.ZoneID = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.ParentIctid).FirstOrDefault();
                    i.SiteID = ictMasterlist.Where(t => t.IctmasterId == i.ZoneID).Select(t => t.IctmasterId).FirstOrDefault();
                    i.Site = ictMasterlist.Where(t => t.IctmasterId == i.SiteID).Select(t => t.Name).FirstOrDefault();
                    i.Zone = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.Name).FirstOrDefault();
                    i.LocationDropDown = i.CompanyName + "|" + i.Site + "|" + i.Zone + "|" + i.Name;

                });
            }
            return ICTMaster;
        }
        [HttpPost]
        [Route("GetICTMasterByCompany")]
        public List<ICTMasterModel> GetICT(SearchModel searchModel)
        {
            List<ICTMasterModel> iCTMasterModels = new List<ICTMasterModel>();
            var ICTMaster = _context.Ictmaster.Include(a => a.AddedByUser).Include(a => a.ModifiedByUser).Include(a => a.IctlayoutPlanTypes)

                .AsNoTracking().OrderByDescending(o => o.IctmasterId).Where(t => t.MasterType == searchModel.MasterTypeID && t.StatusCodeId == 1 && t.ParentIctid == searchModel.ParentID && t.CompanyId == searchModel.Id).AsNoTracking().ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(s =>
                {
                    ICTMasterModel iCTMasterModel = new ICTMasterModel();
                    iCTMasterModel.ICTMasterID = s.IctmasterId;
                    iCTMasterModel.Name = s.Name;
                    iCTMasterModel.ParentICTID = s.ParentIctid;
                    iCTMasterModels.Add(iCTMasterModel);
                });
            }

            return iCTMasterModels;
        }
        [HttpGet]
        [Route("GetICTMaster")]
        public List<ICTMasterModel> GetICT(int id)
        {
            List<ICTMasterModel> iCTMasterModels = new List<ICTMasterModel>();
            var ICTMaster = _context.Ictmaster.Include(a => a.AddedByUser).Include(a => a.ModifiedByUser).Include(a => a.IctlayoutPlanTypes)
                .Include(a => a.Company)
                .Include(a => a.ParentIct)
                .Include(a => a.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct.ParentIct.ParentIct)
                .AsNoTracking().OrderByDescending(o => o.IctmasterId).Where(t => t.MasterType == id).AsNoTracking().ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(s =>
                {


                    ICTMasterModel iCTMasterModel = new ICTMasterModel();
                    iCTMasterModel.ICTMasterID = s.IctmasterId;
                    iCTMasterModel.Name = s.Name;
                    iCTMasterModel.CompanyID = s.CompanyId.Value;
                    iCTMasterModel.CompanyName = s.Company?.Description;
                    iCTMasterModel.Description = s.Description;
                    iCTMasterModel.MasterType = s.MasterType.Value;
                    iCTMasterModel.LayoutPlanID = s.LayoutPlanId;
                    iCTMasterModel.ParentICTID = s.ParentIctid;
                    //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                    iCTMasterModel.VersionNo = s.VersionNo;
                    iCTMasterModel.EffectiveDate = s.EffectiveDate.Value;
                    iCTMasterModel.SessionId = s.SessionId;
                    iCTMasterModel.SiteID = s.SiteId;
                    iCTMasterModel.ZoneID = s.ZoneId;
                    iCTMasterModel.LocationID = s.LocationId;
                    iCTMasterModel.AreaID = s.AreaId;
                    //StatusCode=s.StatusCode.CodeValue,
                    iCTMasterModel.StatusCodeID = s.StatusCodeId;
                    iCTMasterModel.AddedByUserID = s.AddedByUserId;
                    if (s.MasterType == 570)
                    {
                        iCTMasterModel.SiteName = s.Company?.PlantCode + " | " + s.Name;
                    }
                    if (s.MasterType == 571)
                    {
                        iCTMasterModel.ZoneName = s.Company?.PlantCode + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 572)
                    {
                        iCTMasterModel.LocationName = s.Company?.PlantCode + " | " + s.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 573)
                    {
                        iCTMasterModel.AreaName = s.Company?.PlantCode + " | " + s.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 574)
                    {
                        iCTMasterModel.Site = s.ParentIct?.ParentIct?.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Zone = s.ParentIct?.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Location = s.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Area = s.ParentIct?.Name;
                        iCTMasterModel.SpecificAreaName = s.Company?.PlantCode + "|" + s.ParentIct?.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.Name + "|" + s.ParentIct?.Name + "|" + s.Name;
                    }
                    iCTMasterModel.ModifiedByUserID = s.ModifiedByUserId;
                    iCTMasterModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    iCTMasterModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    iCTMasterModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    iCTMasterModel.AddedDate = s.AddedDate;
                    iCTMasterModel.ModifiedDate = s.ModifiedDate;
                    iCTMasterModels.Add(iCTMasterModel);
                });
            }

            return iCTMasterModels;
        }

        [HttpGet]
        [Route("GetICTMasterSpecificArea")]
        public List<ICTMasterModel> GetICTMasterSpecificArea(int id)
        {
            List<ICTMasterModel> iCTMasterModels = new List<ICTMasterModel>();
            var ICTMaster = _context.Ictmaster.Include(a => a.AddedByUser).Include(a => a.ModifiedByUser).Include(a => a.IctlayoutPlanTypes)
                .Include(a => a.Company)
                .Include(a => a.ParentIct)
                .Include(a => a.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct.ParentIct)
                .Include(a => a.ParentIct.ParentIct.ParentIct.ParentIct.ParentIct)
                .AsNoTracking().OrderByDescending(o => o.IctmasterId).Where(t => t.CompanyId == id && t.MasterType == 574 && t.StatusCodeId == 1).AsNoTracking().ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(s =>
                {


                    ICTMasterModel iCTMasterModel = new ICTMasterModel();
                    iCTMasterModel.ICTMasterID = s.IctmasterId;
                    iCTMasterModel.Name = s.Name;
                    iCTMasterModel.CompanyID = s.CompanyId.Value;
                    iCTMasterModel.CompanyName = s.Company?.Description;
                    iCTMasterModel.Description = s.Description;
                    iCTMasterModel.MasterType = s.MasterType.Value;
                    iCTMasterModel.LayoutPlanID = s.LayoutPlanId;
                    iCTMasterModel.ParentICTID = s.ParentIctid;
                    //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                    iCTMasterModel.VersionNo = s.VersionNo;
                    iCTMasterModel.EffectiveDate = s.EffectiveDate.Value;
                    iCTMasterModel.SessionId = s.SessionId;
                    iCTMasterModel.SiteID = s.SiteId;
                    iCTMasterModel.ZoneID = s.ZoneId;
                    iCTMasterModel.LocationID = s.LocationId;
                    iCTMasterModel.AreaID = s.AreaId;
                    //StatusCode=s.StatusCode.CodeValue,
                    iCTMasterModel.StatusCodeID = s.StatusCodeId;
                    iCTMasterModel.AddedByUserID = s.AddedByUserId;
                    if (s.MasterType == 570)
                    {
                        iCTMasterModel.SiteName = s.Company?.PlantCode + " | " + s.Name;
                    }
                    if (s.MasterType == 571)
                    {
                        iCTMasterModel.ZoneName = s.Company?.PlantCode + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 572)
                    {
                        iCTMasterModel.LocationName = s.Company?.PlantCode + " | " + s.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 573)
                    {
                        iCTMasterModel.AreaName = s.Company?.PlantCode + " | " + s.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.Name + " | " + s.Name;
                    }
                    if (s.MasterType == 574)
                    {
                        iCTMasterModel.Site = s.ParentIct?.ParentIct?.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Zone = s.ParentIct?.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Location = s.ParentIct?.ParentIct?.Name;
                        iCTMasterModel.Area = s.ParentIct?.Name;
                        iCTMasterModel.SpecificAreaName = s.Company?.PlantCode + "|" + s.ParentIct?.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.ParentIct?.Name + " | " + s.ParentIct?.ParentIct?.Name + "|" + s.ParentIct?.Name + "|" + s.Name;
                    }
                    iCTMasterModel.ModifiedByUserID = s.ModifiedByUserId;
                    iCTMasterModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    iCTMasterModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    iCTMasterModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    iCTMasterModel.AddedDate = s.AddedDate;
                    iCTMasterModel.ModifiedDate = s.ModifiedDate;
                    iCTMasterModels.Add(iCTMasterModel);
                });
            }

            return iCTMasterModels;
        }
        [HttpGet]
        [Route("GetICTMasterByLocation")]
        public List<ICTMasterModel> GetICTMasterByLocation(int id)
        {
            List<ICTMasterModel> iCTMasterModels = new List<ICTMasterModel>();
            var ICTMaster = _context.Ictmaster.Include(s => s.ParentIct).Include(p => p.ParentIct.ParentIct).AsNoTracking().OrderByDescending(o => o.IctmasterId).Where(t => t.CompanyId == id && t.MasterType == 574).AsNoTracking().ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(s =>
                {
                    ICTMasterModel iCTMasterModel = new ICTMasterModel();
                    iCTMasterModel.ICTMasterID = s.IctmasterId;
                    iCTMasterModel.Area = s.ParentIct != null ? s.ParentIct.Name : null;
                    iCTMasterModel.LocationName = s.ParentIct != null && s.ParentIct.ParentIct != null ? s.ParentIct.ParentIct.Name : null;
                    iCTMasterModel.SpecificAreaName = s.Name;
                    iCTMasterModel.LocationDropDown = iCTMasterModel.LocationName + "|" + iCTMasterModel.Area + "|" + s.Name;
                    iCTMasterModels.Add(iCTMasterModel);
                });
            }

            return iCTMasterModels;
        }
        [HttpGet]
        [Route("GetICTMasterActive")]
        public List<ICTMasterModel> GetICTActive(int id)
        {
            var ICTMaster = _context.Ictmaster.Include(a => a.Company).Include("AddedByUser").Include("ModifiedByUser").Include("IctlayoutPlanTypes").AsNoTracking().Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyID = s.CompanyId.Value,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId.Value,
                ParentICTID = s.ParentIctid,

                //LayoutPlanTypeID = s.IctlayoutPlanTypes.Where(t => t.IctmasterId == s.IctmasterId).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault(),
                VersionNo = s.VersionNo,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                LocationDescription = s.LocationDescription,

            }).OrderByDescending(o => o.ICTMasterID).Where(t => t.MasterType == id && t.StatusCodeID == 1).ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            return ICTMaster;
        }

        [HttpGet]
        [Route("GetICTMasterLocationDropDown")]
        public List<ICTMasterModel> GetICTMasterLocationDropDown(int id)
        {
            var ictMasterlist = _context.Ictmaster.AsNoTracking().ToList();
            var ICTMaster = _context.Ictmaster.Include("AddedByUser").Include("ModifiedByUser").Include("Company").AsNoTracking().Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyName = s.Company != null ? s.Company.PlantCode : "",
                CompanyID = s.CompanyId,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId,
                ParentICTID = s.ParentIctid,
                VersionNo = s.VersionNo,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).Where(t => t.MasterType == 572 && t.CompanyID == id).ToList().OrderByDescending(o => o.ICTMasterID).ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(i =>
                {
                    i.ZoneID = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.ParentIctid).FirstOrDefault();
                    i.SiteID = ictMasterlist.Where(t => t.IctmasterId == i.ZoneID).Select(t => t.IctmasterId).FirstOrDefault();
                    i.Site = ictMasterlist.Where(t => t.IctmasterId == i.SiteID).Select(t => t.Name).FirstOrDefault();
                    i.Zone = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.Name).FirstOrDefault();
                    i.LocationDropDown = i.Site + " | " + i.Zone + " | " + i.Name + " | " + i.Description;

                });
            }
            return ICTMaster;
        }

        [HttpGet]
        [Route("GetICTMasterLocationDropDownByParentIctid")]
        public List<ICTMasterModel> GetICTMasterLocationDropDownByParentIctid(int id)
        {
            //Comment by SA
            //List<long?> parentIctids = new List<long?>() { 231, 3511, 195 };
            var ictMasterlist = _context.Ictmaster.AsNoTracking().ToList();
            var ICTMaster = _context.Ictmaster.Include(c=>c.Company).AsNoTracking().Select(s => new ICTMasterModel
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyName = s.Company != null ? s.Company.PlantCode : "",
                CompanyID = s.CompanyId,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                LayoutPlanID = s.LayoutPlanId,
                ParentICTID = s.ParentIctid,
                VersionNo = s.VersionNo,
                SiteID = s.SiteId,
                ZoneID = s.ZoneId,
                LocationID = s.LocationId,
                AreaID = s.AreaId,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).Where(t => t.MasterType == 572 && t.CompanyID == id ).ToList().OrderByDescending(o => o.ICTMasterID).ToList();
            if (ICTMaster != null && ICTMaster.Count > 0)
            {
                ICTMaster.ForEach(i =>
                {
                    i.ZoneID = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.ParentIctid).FirstOrDefault();
                    i.SiteID = ictMasterlist.Where(t => t.IctmasterId == i.ZoneID).Select(t => t.IctmasterId).FirstOrDefault();
                    i.Site = ictMasterlist.Where(t => t.IctmasterId == i.SiteID).Select(t => t.Name).FirstOrDefault();
                    i.Zone = ictMasterlist.Where(t => t.IctmasterId == i.ParentICTID).Select(t => t.Name).FirstOrDefault();
                    i.LocationDropDown = i.Site + "|" + i.Zone + "|" + i.Name + "|" + i.Description;
                    i.LocationDescription = i.Name + "|" + i.Description + "|" + i.Site + "|" + i.Zone;

                });
            }
            return ICTMaster;
        }
        [HttpGet]
        [Route("GetPublicFolderDocuments")]
        public List<DocumentsModel> GetPublicFoldersDocument(int id)
        {
            var folderIds = _context.DocumentFolder.Where(t => t.Folder.FolderTypeId == 524).AsNoTracking().Select(f => f.FolderId.Value).ToList().Distinct();
            List<DocumentsModel> documentLocation = new List<DocumentsModel>();

            foreach (var folderId in folderIds)
            {

                docPath = string.Empty;
                documentLocation = FindDocumentLocation(folderId);
            }



            return documentLocation;

        }


        string docPath = string.Empty;
        List<DocumentsModel> documentLocation = new List<DocumentsModel>();
        private List<DocumentsModel> FindDocumentLocation(long folderId)
        {
            var documentDetails = new List<object>();
            List<FoldersModel> foldersModels = new List<FoldersModel>();
            //List<Folders> folders = new List<Folders>();
            var foldersList = _context.Folders.Include(d => d.DocumentFolder).Where(f => f.FolderId == folderId).AsNoTracking().ToList();
            if (foldersList != null && foldersList.Count > 0)
            {
                foldersList.ForEach(s =>
                {
                    FoldersModel foldersModel = new FoldersModel();
                    foldersModel.FolderID =
                    foldersModel.FolderID = s.FolderId;
                    foldersModel.ParentFolderID = s.ParentFolderId;
                    foldersModel.Name = s.Name;
                    foldersModel.DocumentDetails = s.DocumentFolder?.Where(sa => sa.IsLatest == true).Select(sa =>
                    new DocumentDetailsModel
                    { DocumentID = sa.DocumentId.Value, FileName = sa.Document?.FileName, ContentType = sa.Document?.ContentType }).ToList();
                    foldersModels.Add(foldersModel);
                });
            }


            DocumentsModel document = new DocumentsModel();
            foreach (var f in foldersModels)
            {
                if (f.ParentFolderID.HasValue)
                {
                    docPath = docPath + f.Name + " > ";
                    FindDocumentLocation(f.ParentFolderID.Value);
                }
                if (f.DocumentDetails.Any())
                {
                    foreach (var d in f.DocumentDetails)
                    {
                        document = new DocumentsModel();
                        string tempDocPath = docPath;
                        // docPath = f.folderName + " > "  + d.fileName;
                        document.DocumentID = d.DocumentID.Value;
                        document.ContentType = d.ContentType;
                        document.DocumentPath = docPath + f.Name + " > " + d.FileName;
                        docPath = tempDocPath;
                        documentLocation.Add(document);
                    }


                }


            }
            return documentLocation;
        }

        [HttpGet]
        [Route("GetICTMobileMasters")]
        public List<ICTMobileMaster> GetICTMasters(int id)
        {
            var ICTMaster = _context.Ictmaster.Where(c => c.StatusCodeId == 1).AsNoTracking().Select(s => new ICTMobileMaster
            {
                ICTMasterID = s.IctmasterId,
                Name = s.Name,
                CompanyID = s.CompanyId.Value,
                Description = s.Description,
                MasterType = s.MasterType.Value,
                ParentICTID = s.ParentIctid

            }).OrderByDescending(o => o.ICTMasterID).Where(t => t.CompanyID == id).ToList();
            return ICTMaster;
        }

        [HttpGet]
        [Route("GetICTLayoutPlanType")]
        public List<ICTLayoutPlanTypesModel> GetICTLayoutPlan(int id)
        {
            var ICTMasterlayoutplan = _context.IctlayoutPlanTypes.AsNoTracking().Select(s => new ICTLayoutPlanTypesModel
            {
                ICTLayoutTypeID = s.IctlayoutTypeId,
                ICTMasterID = s.IctmasterId.Value,
                Name = s.Name,
                Description = s.Description,

                DocumentID = s.DocumentId.Value,
                LayoutTypeID = s.LayoutTypeId,
                LayoutTypeName = s.LayoutType != null ? s.LayoutType.Name : "",
                VersionNo = s.VersionNo,
                EffectiveDate = s.EffectiveDate.Value,
                SessionId = s.SessionId,

            }).OrderByDescending(o => o.ICTLayoutTypeID).Where(t => t.ICTMasterID == id).ToList();
            //var result = _mapper.Map<List<ICTMasterModel>>(ICTMaster);
            return ICTMasterlayoutplan;
        }
        //// GET: api/Project/2
        ////[HttpGet("{id}", Name = "Get ICTMaster")]
        //[HttpGet("GetICTMasters/{id:int}")]
        //public ActionResult<ICTMasterModel> Get(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    var ICTMaster = _context.Ictmaster.SingleOrDefault(p => p.IctmasterId == id.Value);
        //    var result = _mapper.Map<ICTMasterModel>(ICTMaster);
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }
        //    return result;
        //}
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ICTMasterModel> GetData(SearchModel searchModel)
        {
            var ICTMaster = new Ictmaster();
            var IctMastermodel = new ICTMasterModel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Previous":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault();
                        break;
                    case "Last":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).LastOrDefault();
                        break;
                    case "Next":
                        ICTMaster = _context.Ictmaster.OrderBy(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault(s => s.IctmasterId > searchModel.Id);
                        break;
                    case "Previous":
                        ICTMaster = _context.Ictmaster.OrderByDescending(o => o.IctmasterId).Where(i => i.MasterType == searchModel.MasterTypeID).FirstOrDefault(s => s.IctmasterId < searchModel.Id);

                        break;
                }
            }
            var result = _mapper.Map<ICTMasterModel>(ICTMaster);
            //IctMastermodel.LayoutPlanTypeID = _context.IctlayoutPlanTypes.Where(t => t.IctmasterId == searchModel.Id).Select(t => t.LayoutPlanTypeId.Value).FirstOrDefault();
            //if (IctMastermodel.LayoutPlanTypeID > 0 && result !=null)
            //{
            //    result.LayoutPlanTypeID = IctMastermodel.LayoutPlanTypeID;
            //}
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertICTMaster")]
        public ICTMasterModel Post(ICTMasterModel value)
        {
            if (!value.IsAllLoction)
            {
                var SessionId = Guid.NewGuid();
                var iCTMaster = new Ictmaster()
                {
                    //ICTMasterId = value.ICTMasterID,
                    Name = value.Name,
                    Description = value.Description,
                    CompanyId = value.CompanyID,
                    MasterType = value.MasterType,
                    LayoutPlanId = value.LayoutPlanID <= 0 ? null : value.LayoutPlanID,
                    VersionNo = value.VersionNo,
                    ParentIctid = value.ParentICTID,
                    EffectiveDate = value.EffectiveDate,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    SessionId = SessionId,
                    StatusCodeId = value.StatusCodeID.Value,
                    SiteId = value.SiteID,
                    ZoneId = value.ZoneID,
                    LocationId = value.LocationID,
                    AreaId = value.AreaID,
                    LocationDescription = value.LocationDescription,
                    //StatusCode=value.StatusCode.Select()
                    //StatusCode = value.Status

                };


                _context.Ictmaster.Add(iCTMaster);
                _context.SaveChanges();
                value.SessionId = SessionId;
                value.ICTMasterID = iCTMaster.IctmasterId;
                //var layouttypes = value.LayoutPlanTypes;
                //if (layouttypes.Count != 0)
                //{
                //    layouttypes.ForEach(layout =>
                //    {

                //        var layoutType = new IctlayoutPlanTypes()
                //        {
                //            Name = layout.Name,
                //            Description = layout.Description,
                //            DocumentId = layout.DocumentID <= 0 ? null : layout.DocumentID,
                //            LayoutTypeId = layout.LayoutTypeID,
                //            VersionNo = layout.VersionNo,
                //            EffectiveDate = layout.EffectiveDate,
                //            IctmasterId = value.ICTMasterID,


                //        };
                //        _context.IctlayoutPlanTypes.Add(layoutType);
                //    }

                //    );
                //}

            }
            else
            {
                var locationIds = _context.Ictmaster.Where(l => l.MasterType == value.SelectMasterType).Select(l => l.IctmasterId).ToList();
                locationIds.ForEach(locations =>
                {

                    var iCTMaster = new Ictmaster()
                    {
                        //ICTMasterId = value.ICTMasterID,
                        Name = value.Name,
                        Description = value.Description,
                        CompanyId = value.CompanyID,
                        MasterType = value.MasterType,
                        LayoutPlanId = value.LayoutPlanID <= 0 ? null : value.LayoutPlanID,
                        VersionNo = value.VersionNo,
                        ParentIctid = locations,
                        EffectiveDate = value.EffectiveDate,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        SiteId = value.SiteID,
                        ZoneId = value.ZoneID,
                        LocationId = value.LocationID,
                        AreaId = value.AreaID,

                        //StatusCode=value.StatusCode.Select()
                        //StatusCode = value.Status

                    };


                    _context.Ictmaster.Add(iCTMaster);
                    //var layouttypes = value.LayoutPlanTypes;
                    //if (layouttypes.Count != 0)
                    //{
                    //    layouttypes.ForEach(layout =>
                    //    {

                    //        var layoutType = new IctlayoutPlanTypes()
                    //        {
                    //            Name = layout.Name,
                    //            Description = layout.Description,
                    //            DocumentId = layout.DocumentID <= 0 ? null : layout.DocumentID,
                    //            LayoutTypeId = layout.LayoutTypeID,
                    //            VersionNo = layout.VersionNo,
                    //            EffectiveDate = layout.EffectiveDate,
                    //            IctmasterId = value.ICTMasterID,


                    //        };
                    //        _context.IctlayoutPlanTypes.Add(layoutType);
                    //    }

                    //    );
                    //}
                }

                    );

            }
            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateICTMaster")]
        public ICTMasterModel Put(ICTMasterModel value)
        {

            var ICTMaster = _context.Ictmaster.SingleOrDefault(p => p.IctmasterId == value.ICTMasterID);
            if (ICTMaster != null)
            {
                //ICTMaster.ICTMasterId = value.ICTMasterID;
                ICTMaster.ModifiedByUserId = value.ModifiedByUserID;
                ICTMaster.ModifiedDate = DateTime.Now;
                ICTMaster.Name = value.Name;
                ICTMaster.LayoutPlanId = value.LayoutPlanID <= 0 ? null : value.LayoutPlanID;
                ICTMaster.EffectiveDate = value.EffectiveDate;
                ICTMaster.Description = value.Description;
                ICTMaster.ParentIctid = value.ParentICTID;
                if (value.SessionId == null && ICTMaster.SessionId == null)
                {
                    var SessionId = Guid.NewGuid();
                    ICTMaster.SessionId = SessionId;
                    value.SessionId = SessionId;
                }
                else
                {
                    ICTMaster.SessionId = value.SessionId;
                }


                ICTMaster.VersionNo = value.VersionNo;
                ICTMaster.MasterType = value.MasterType;
                ICTMaster.CompanyId = value.CompanyID;
                ICTMaster.StatusCodeId = value.StatusCodeID.Value;
                ICTMaster.SiteId = value.SiteID;
                ICTMaster.AreaId = value.AreaID;
                ICTMaster.LocationId = value.LocationID;
                ICTMaster.ZoneId = value.ZoneID;
                ICTMaster.LocationDescription = value.LocationDescription;
                //var layouttypes = value.LayoutPlanTypes;
                //if (layouttypes.Count > 0)
                //{
                //    layouttypes.ForEach(layout =>
                //    {
                //        if (layout.ICTLayoutTypeID > 0)
                //        {
                //            var layoutType = _context.IctlayoutPlanTypes.SingleOrDefault(p => p.IctlayoutTypeId == layout.ICTLayoutTypeID);
                //            {
                //                if (layoutType != null)
                //                {
                //                    layoutType.Name = layout.Name;
                //                    layoutType.Description = layout.Description;
                //                    layoutType.DocumentId = layout.DocumentID <= 0 ? null : layout.DocumentID;
                //                    layoutType.LayoutTypeId = layout.LayoutTypeID;
                //                    layoutType.VersionNo = layout.VersionNo;
                //                    layoutType.EffectiveDate = layout.EffectiveDate;
                //                    layoutType.IctmasterId = layout.ICTMasterID;
                //                    layoutType.IctlayoutTypeId = layout.ICTLayoutTypeID;
                //                    //  _context.IctlayoutPlanTypes.Add(layoutType);
                //                }

                //            };
                //        }
                //        else
                //        {
                //            var layoutType = new IctlayoutPlanTypes()
                //            {
                //                Name = layout.Name,
                //                Description = layout.Description,
                //                DocumentId = layout.DocumentID <= 0 ? null : layout.DocumentID,
                //                LayoutTypeId = layout.LayoutTypeID,
                //                VersionNo = layout.VersionNo,
                //                EffectiveDate = layout.EffectiveDate,
                //                IctmasterId = value.ICTMasterID,


                //            };
                //            _context.IctlayoutPlanTypes.Add(layoutType);

                //        }


                //    }

                //    );
                //}
                _context.SaveChanges();

            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteICTMaster")]
        public void Delete(int id)
        {
            try
            {
                var ICTMaster = _context.Ictmaster.SingleOrDefault(p => p.IctmasterId == id);
                if (ICTMaster != null)
                {
                    _context.Ictmaster.Remove(ICTMaster);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("InsertLayoutTypes")]
        public ICTLayoutPlanTypesModel InsertLayoutTypes(ICTLayoutPlanTypesModel value)
        {

            var SessionId = Guid.NewGuid();
            var layoutType = new IctlayoutPlanTypes()
            {
                Name = value.Name,
                Description = value.Description,
                LayoutTypeId = value.LayoutTypeID,
                VersionNo = value.VersionNo,
                EffectiveDate = value.EffectiveDate,
                IctmasterId = value.ICTMasterID,
                SessionId = SessionId,


            };
            _context.IctlayoutPlanTypes.Add(layoutType);
            _context.SaveChanges();
            value.SessionId = SessionId;
            return value;
        }


        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateLayoutTypes")]
        public ICTLayoutPlanTypesModel UpdateLayoutTypes(ICTLayoutPlanTypesModel value)
        {
            var layoutType = _context.IctlayoutPlanTypes.SingleOrDefault(p => p.IctlayoutTypeId == value.ICTLayoutTypeID);
            {
                if (layoutType != null)
                {
                    layoutType.Name = value.Name;
                    layoutType.Description = value.Description;

                    layoutType.VersionNo = value.VersionNo;
                    layoutType.EffectiveDate = value.EffectiveDate;
                    layoutType.IctmasterId = value.ICTMasterID;
                    layoutType.LayoutTypeId = value.LayoutTypeID;
                    //  _context.IctlayoutPlanTypes.Add(layoutType);
                }

            };
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteLayoutTypes")]
        public void DeleteLayoutTypes(int id)
        {
            try
            {
                var ictLayoutypes = _context.IctlayoutPlanTypes.Where(d => d.IctlayoutTypeId == id).ToList();
                if (ictLayoutypes.Count > 0)
                {
                    _context.IctlayoutPlanTypes.RemoveRange(ictLayoutypes);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        [Route("GetICTMasterDocument")]
        public List<ICTMasterDocumentModel> GetICTMasterDocument(Guid? SessionId)
        {
            List<ICTMasterDocumentModel> iCTMasterDocumentModels = new List<ICTMasterDocumentModel>();


            var ictDocuments = _context.IctmasterDocument.Select(s => new { s.SessionId, s.IctmasterDocumentId, s.FileName, s.ContentType, s.FileSize, s.UploadDate }).Where(f => f.SessionId == SessionId).AsNoTracking().ToList();
            if (ictDocuments != null && ictDocuments.Count > 0)
            {
                ictDocuments.ForEach(s =>
                {
                    ICTMasterDocumentModel iCTMasterDocumentModel = new ICTMasterDocumentModel
                    {
                        ICTMasterDocumentID = s.IctmasterDocumentId,
                        FileName = s.FileName,
                        ContentType = s.ContentType,
                        FileSize = s.FileSize,
                        UploadDate = s.UploadDate,
                        SessionId = s.SessionId,

                    };
                    iCTMasterDocumentModels.Add(iCTMasterDocumentModel);
                });

            }


            return iCTMasterDocumentModels;
        }

        [HttpDelete]
        [Route("DeleteICTMasterDocuments")]
        public void DeleteICTMasterDocuments(int id)
        {
            try
            {
                var query = string.Format("delete from IctmasterDocument Where IctmasterDocumentId='{0}'", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPost]
        [Route("UploadICTMasterDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {

            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var ictDocument = new IctmasterDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    //Description = files.desc
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,

                };
                _context.IctmasterDocument.Add(ictDocument);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("DownLoadICTMasterDocument")]
        public IActionResult DownLoadICTMasterDocument(long id)
        {
            var document = _context.IctmasterDocument.SingleOrDefault(t => t.IctmasterDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }

    }
}