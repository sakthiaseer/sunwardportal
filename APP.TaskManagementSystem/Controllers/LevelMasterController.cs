﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class LevelMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public LevelMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetLevelMasters")]
        public List<LevelMasterModel> Get()
        {
            var levelMaster = _context.LevelMaster
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Company")
                .Select(s => new LevelMasterModel
            {
                LevelID = s.LevelId,
                CompanyID = s.CompanyId,
                CompanyNamee = s.Company!=null? s.Company.Description : "",
                Name = s.Name,
                Priority = s.Priority,

                StatusCodeID = s.StatusCodeId,
                
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.LevelID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<LevelMasterModel>>(LevelMaster);
            return levelMaster;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get LevelMaster")]
        [HttpGet("GetLevelMasters/{id:int}")]
        public ActionResult<LevelMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var levelMaster = _context.LevelMaster.SingleOrDefault(p => p.LevelId == id.Value);
            var result = _mapper.Map<LevelMasterModel>(levelMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<LevelMasterModel> GetData(SearchModel searchModel)
        {
            var levelMaster = new LevelMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).FirstOrDefault();
                        break;
                    case "Last":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).LastOrDefault();
                        break;
                    case "Next":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).LastOrDefault();
                        break;
                    case "Previous":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).FirstOrDefault();
                        break;
                    case "Last":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).LastOrDefault();
                        break;
                    case "Next":
                        levelMaster = _context.LevelMaster.OrderBy(o => o.LevelId).FirstOrDefault(s => s.LevelId > searchModel.Id);
                        break;
                    case "Previous":
                        levelMaster = _context.LevelMaster.OrderByDescending(o => o.LevelId).FirstOrDefault(s => s.LevelId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<LevelMasterModel>(levelMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertLevelMaster")]
        public LevelMasterModel Post(LevelMasterModel value)
        {
            var levelMaster = new LevelMaster
            {
                CompanyId = value.CompanyID,
                Name = value.Name,
                Priority = value.Priority,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.LevelMaster.Add(levelMaster);
            _context.SaveChanges();
            value.LevelID = levelMaster.LevelId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateLevelMaster")]
        public LevelMasterModel Put(LevelMasterModel value)
        {
            var levelMaster = _context.LevelMaster.SingleOrDefault(p => p.LevelId == value.LevelID);

            levelMaster.CompanyId = value.CompanyID;
            levelMaster.Name = value.Name;
            levelMaster.Priority = value.Priority;
            levelMaster.ModifiedByUserId = value.ModifiedByUserID;
            levelMaster.ModifiedDate = DateTime.Now;
            levelMaster.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteLevelMaster")]
        public void Delete(int id)
        {
            var levelMaster = _context.LevelMaster.SingleOrDefault(p => p.LevelId == id);
            if (levelMaster != null)
            {
                _context.LevelMaster.Remove(levelMaster);
                _context.SaveChanges();
            }
        }
    }
}