﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyHolidaysController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CompanyHolidaysController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCompanyHolidays")]
        public List<CompanyHolidaysModel> Get()
        {
            var companyHolidayDetail = _context.CompanyHolidays.
               Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(p => p.State)
               .AsNoTracking().ToList();
            List<CompanyHolidaysModel> companyHolidaysModel = new List<CompanyHolidaysModel>();
            companyHolidayDetail.ForEach(s =>
            {
                CompanyHolidaysModel companyHolidaysModels = new CompanyHolidaysModel
                {
                    CompanyHolidayID = s.CompanyHolidayId,
                    CalendarYear = s.CalendarYear,
                    PlantID = s.PlantId,
                    StateID = s.StateId,
                    NoOfHolidays = s.NoOfHolidays,
                    Reference = s.Reference,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    State = s.State != null ? s.State.Name : "",
                    CountryID = s.State.CountryId,
                };
                companyHolidaysModel.Add(companyHolidaysModels);
            });
            return companyHolidaysModel.OrderByDescending(a => a.CompanyHolidayID).ToList();
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get CompanyHolidayss")]
        [HttpGet("GetCompanyHolidays/{id:int}")]
        public ActionResult<CompanyHolidaysModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyHolidayDetail = _context.CompanyHolidays.SingleOrDefault(p => p.CompanyHolidayId == id.Value);
            var result = _mapper.Map<CompanyHolidaysModel>(companyHolidayDetail);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyHolidaysModel> GetData(SearchModel searchModel)
        {
            var companyHolidayDetail = new CompanyHolidays();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).FirstOrDefault();
                        break;
                    case "Last":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).LastOrDefault();
                        break;
                    case "Next":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).LastOrDefault();
                        break;
                    case "Previous":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).FirstOrDefault();
                        break;
                    case "Last":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).LastOrDefault();
                        break;
                    case "Next":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderBy(o => o.CompanyHolidayId).FirstOrDefault(s => s.CompanyHolidayId > searchModel.Id);
                        break;
                    case "Previous":
                        companyHolidayDetail = _context.CompanyHolidays.Include("State").OrderByDescending(o => o.CompanyHolidayId).FirstOrDefault(s => s.CompanyHolidayId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyHolidaysModel>(companyHolidayDetail);
            if (companyHolidayDetail != null)
                result.CountryID = companyHolidayDetail.State?.CountryId;

            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyHolidayss")]
        public CompanyHolidaysModel Post(CompanyHolidaysModel value)
        {
            var companyHolidays = new CompanyHolidays
            {

                CalendarYear = value.CalendarYear,
                PlantId = value.PlantID,
                StateId = value.StateID,
                NoOfHolidays = value.NoOfHolidays,
                Reference = value.Reference,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                CompanyHolidayDetail = new List<CompanyHolidayDetail>(),

            };
            value.CompanyHolidayDetail.ToList().ForEach(d =>
            {
                var detail = new CompanyHolidayDetail
                {
                    AllowEarlyReleaseId = d.AllowEarlyReleaseID,
                    HolidayDate = d.HolidayDate,
                    HolidayId = d.HolidayID,
                    HolidayTypeId = d.HolidayTypeID,
                    IsCompanyChoose = d.IsCompanyChoose,
                    IsCompulsory = d.IsCompulsory,
                    EarlyRelease = d.EarlyRelease,
                    //CompanyHolidayId =value.CompanyHolidayID
                };
                companyHolidays.CompanyHolidayDetail.Add(detail);

            });
            _context.CompanyHolidays.Add(companyHolidays);
            _context.SaveChanges();
            value.CompanyHolidayID = companyHolidays.CompanyHolidayId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyHolidays")]
        public CompanyHolidaysModel Put(CompanyHolidaysModel value)
        {
            var companyHolidayDetail = _context.CompanyHolidays.SingleOrDefault(p => p.CompanyHolidayId == value.CompanyHolidayID);
            companyHolidayDetail.CalendarYear = value.CalendarYear;
            companyHolidayDetail.PlantId = value.PlantID;
            companyHolidayDetail.StateId = value.StateID;
            companyHolidayDetail.NoOfHolidays = value.NoOfHolidays;
            companyHolidayDetail.Reference = value.Reference;
            companyHolidayDetail.ModifiedByUserId = value.ModifiedByUserID;
            companyHolidayDetail.ModifiedDate = DateTime.Now;
            companyHolidayDetail.StatusCodeId = value.StatusCodeID.Value;

            var details = _context.CompanyHolidayDetail.Where(l => l.CompanyHolidayId == value.CompanyHolidayID).ToList();
            if (details.Count > 0)
            {
                _context.CompanyHolidayDetail.RemoveRange(details);
                _context.SaveChanges();
            }

            value.CompanyHolidayDetail.ToList().ForEach(d =>
            {
                var detail = new CompanyHolidayDetail
                {
                    AllowEarlyReleaseId = d.AllowEarlyReleaseID,
                    HolidayDate = d.HolidayDate,
                    HolidayId = d.HolidayID,
                    HolidayTypeId = d.HolidayTypeID,
                    IsCompanyChoose = d.IsCompanyChoose,
                    IsCompulsory = d.IsCompulsory,
                    EarlyRelease = d.EarlyRelease,
                    CompanyHolidayId = value.CompanyHolidayID
                };
                companyHolidayDetail.CompanyHolidayDetail.Add(detail);

            });
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyHolidays")]
        public void Delete(int id)
        {
            var companyHolidayDetail = _context.CompanyHolidays.Include("CompanyHolidayDetail").SingleOrDefault(p => p.CompanyHolidayId == id);
            if (companyHolidayDetail != null)
            {
                _context.CompanyHolidays.Remove(companyHolidayDetail);
                _context.SaveChanges();
            }
        }
    }
}