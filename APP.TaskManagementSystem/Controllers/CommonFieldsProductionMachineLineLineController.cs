﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonFieldsProductionMachineLineLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonFieldsProductionMachineLineLineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonFieldsProductionMachineLineLine")]
        public List<CommonFieldsProductionMachineLineLineModel> Get(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachineLineLine = _context.CommonFieldsProductionMachineLineLine
                                                        .Include(t => t.StatusOfRequalification)
                                                        .Include(a => a.AddedByUser)
                                                        .Include(m => m.ModifiedByUser)
                                                        .Include(s => s.StatusCode)
                                                        .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineLineLineModel> commonFieldsProductionMachineLineLineModel = new List<CommonFieldsProductionMachineLineLineModel>();
            commonFieldsProductionMachineLineLine.ForEach(s =>
            {
                CommonFieldsProductionMachineLineLineModel commonFieldsProductionMachineLineLineModels = new CommonFieldsProductionMachineLineLineModel();
                commonFieldsProductionMachineLineLineModels.CommonFieldsProductionMachineLineLineId = s.CommonFieldsProductionMachineLineLineId;
                commonFieldsProductionMachineLineLineModels.DateToStartRequalification = s.DateToStartRequalification;
                commonFieldsProductionMachineLineLineModels.DateCompleteRequalification = s.DateCompleteRequalification;
                commonFieldsProductionMachineLineLineModels.StatusOfRequalificationId = s.StatusOfRequalificationId;
                commonFieldsProductionMachineLineLineModels.StatusOfRequalification = s.StatusOfRequalification != null ? s.StatusOfRequalification.CodeValue : null;
                commonFieldsProductionMachineLineLineModels.SessionId = s.SessionId;
                commonFieldsProductionMachineLineLineModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                commonFieldsProductionMachineLineLineModels.StatusCodeID = s.StatusCodeId;
                commonFieldsProductionMachineLineLineModels.AddedByUserID = s.AddedByUserId;
                commonFieldsProductionMachineLineLineModels.ModifiedByUserID = s.ModifiedByUserId;
                commonFieldsProductionMachineLineLineModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                commonFieldsProductionMachineLineLineModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                commonFieldsProductionMachineLineLineModels.AddedDate = s.AddedDate;
                commonFieldsProductionMachineLineLineModels.ModifiedDate = s.ModifiedDate;
                commonFieldsProductionMachineLineLineModel.Add(commonFieldsProductionMachineLineLineModels);
            });
            return commonFieldsProductionMachineLineLineModel.Where(w => w.CommonFieldsProductionMachineLineId == id).OrderByDescending(a => a.CommonFieldsProductionMachineLineLineId).ToList();
        }
        [HttpPost]
        [Route("GetCommonFieldsProductionMachineLineLineRefByNo")]
        public List<CommonFieldsProductionMachineLineLineModel> GetCommonFieldsProductionMachineLineLineRefByNo(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var commonFieldsProductionMachineLineLine = _context.CommonFieldsProductionMachineLineLine
                                                        .Include(t => t.StatusOfRequalification)
                                                        .Include(a => a.AddedByUser)
                                                        .Include(m => m.ModifiedByUser)
                                                        .Include(s => s.StatusCode)
                                                        .AsNoTracking().ToList();
            List<CommonFieldsProductionMachineLineLineModel> commonFieldsProductionMachineLineLineModel = new List<CommonFieldsProductionMachineLineLineModel>();
            commonFieldsProductionMachineLineLine.ForEach(s =>
            {
                CommonFieldsProductionMachineLineLineModel commonFieldsProductionMachineLineLineModels = new CommonFieldsProductionMachineLineLineModel();
                commonFieldsProductionMachineLineLineModels.CommonFieldsProductionMachineLineLineId = s.CommonFieldsProductionMachineLineLineId;
                commonFieldsProductionMachineLineLineModels.DateToStartRequalification = s.DateToStartRequalification;
                commonFieldsProductionMachineLineLineModels.DateCompleteRequalification = s.DateCompleteRequalification;
                commonFieldsProductionMachineLineLineModels.StatusOfRequalificationId = s.StatusOfRequalificationId;
                commonFieldsProductionMachineLineLineModels.StatusOfRequalification = s.StatusOfRequalification != null ? s.StatusOfRequalification.CodeValue : null;
                commonFieldsProductionMachineLineLineModels.SessionId = s.SessionId;
                commonFieldsProductionMachineLineLineModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                commonFieldsProductionMachineLineLineModels.StatusCodeID = s.StatusCodeId;
                commonFieldsProductionMachineLineLineModels.AddedByUserID = s.AddedByUserId;
                commonFieldsProductionMachineLineLineModels.ModifiedByUserID = s.ModifiedByUserId;
                commonFieldsProductionMachineLineLineModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                commonFieldsProductionMachineLineLineModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                commonFieldsProductionMachineLineLineModels.AddedDate = s.AddedDate;
                commonFieldsProductionMachineLineLineModels.ModifiedDate = s.ModifiedDate;
                commonFieldsProductionMachineLineLineModels.ProfileLinkReferenceNo = s.ProfileLinkReferenceNo;
                commonFieldsProductionMachineLineLineModels.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                commonFieldsProductionMachineLineLineModels.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                commonFieldsProductionMachineLineLineModel.Add(commonFieldsProductionMachineLineLineModels);
            });
            if (refSearchModel.IsHeader)
            {
                return commonFieldsProductionMachineLineLineModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.CommonFieldsProductionMachineLineLineId).ToList();
            }
            return commonFieldsProductionMachineLineLineModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.CommonFieldsProductionMachineLineLineId).ToList();
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCommonFieldsProductionMachineLineLine")]
        public CommonFieldsProductionMachineLineLineModel Post(CommonFieldsProductionMachineLineLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 });
            var commonFieldsProductionMachineLine = new CommonFieldsProductionMachineLineLine
            {
                DateToStartRequalification = value.DateToStartRequalification,
                DateCompleteRequalification = value.DateCompleteRequalification,
                StatusOfRequalificationId = value.StatusOfRequalificationId,
                SessionId = SessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,

            };
            _context.CommonFieldsProductionMachineLineLine.Add(commonFieldsProductionMachineLine);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.CommonFieldsProductionMachineLineLineId = commonFieldsProductionMachineLine.CommonFieldsProductionMachineLineLineId;
            value.MasterProfileReferenceNo = commonFieldsProductionMachineLine.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonFieldsProductionMachineLine.ProfileLinkReferenceNo;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCommonFieldsProductionMachineLineLine")]
        public CommonFieldsProductionMachineLineLineModel Put(CommonFieldsProductionMachineLineLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLineLine.SingleOrDefault(p => p.CommonFieldsProductionMachineLineLineId == value.CommonFieldsProductionMachineLineLineId);
            commonFieldsProductionMachineLine.DateToStartRequalification = value.DateToStartRequalification;
            commonFieldsProductionMachineLine.DateCompleteRequalification = value.DateCompleteRequalification;
            commonFieldsProductionMachineLine.StatusOfRequalificationId = value.StatusOfRequalificationId;
            commonFieldsProductionMachineLine.SessionId = value.SessionId;
            commonFieldsProductionMachineLine.ModifiedByUserId = value.ModifiedByUserID;
            commonFieldsProductionMachineLine.ModifiedDate = DateTime.Now;
            commonFieldsProductionMachineLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCommonFieldsProductionMachineLineLine")]
        public void Delete(int id)
        {
            var commonFieldsProductionMachineLine = _context.CommonFieldsProductionMachineLineLine.SingleOrDefault(p => p.CommonFieldsProductionMachineLineLineId == id);
            if (commonFieldsProductionMachineLine != null)
            {
                var machineDocumentbandlist = _context.CommonFieldsProductionMachineDocument.Where(p => p.SessionId == commonFieldsProductionMachineLine.SessionId).Select(s => s.MachineDocumentId).ToList();
                if (machineDocumentbandlist != null)
                {
                    machineDocumentbandlist.ForEach(h =>
                    {
                        var machineDocumentbanditem = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == h);
                        _context.CommonFieldsProductionMachineDocument.Remove(machineDocumentbanditem);
                        _context.SaveChanges();
                    });

                }
                _context.CommonFieldsProductionMachineLineLine.Remove(commonFieldsProductionMachineLine);
                _context.SaveChanges();
            }
        }
    }
}