﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemBatchInfoController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ItemBatchInfoController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetItemBatchInfo")]
        public List<ItemBatchInfoModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(s => s.Company).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }
        [HttpGet]
        [Route("GetItemBatchInfoByBatchInfoId")]
        public List<ItemBatchInfoModel> GetItemBatchInfoByBatchInfoId(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(w => w.QuantityOnHand > 0 && w.ItemBatchId == id).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }

        [HttpGet]
        [Route("GetItemBatchInfoById")]
        public List<ItemBatchInfoModel> GetItemBatchInfoById(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(w => w.ItemId == id).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }
        [HttpGet]
        [Route("GetItemStockInfoByInventoryType")]
        public List<ItemStockInfoModel> GetItemStockInfoByInventoryType(int id)
        {

            List<ItemStockInfoModel> itemStockInfoModels = new List<ItemStockInfoModel>();
            ItemStockInfoModel itemStockInfoModel = new ItemStockInfoModel();
            var openingBalanceIds = _context.InventoryTypeOpeningStockBalance.Where(s => s.InventoryTypeId == id).AsNoTracking().Select(s => s.OpeningBalanceId).ToList();
            if (openingBalanceIds.Count > 0)
            {

                var itemIds = _context.InventoryTypeOpeningStockBalanceLine.Where(s => openingBalanceIds.Contains(s.OpeningStockId.Value)).AsNoTracking().Select(s => s.ItemId).ToList();
                if (itemIds.Count > 0)
                {
                    var itemStockInfo = _context.ItemStockInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Where(w => itemIds.Contains(w.ItemId)).OrderByDescending(o => o.ItemStockId).AsNoTracking().ToList();
                    if (itemStockInfo != null && itemStockInfo.Count > 0)
                    {
                        itemStockInfo.ForEach(s =>
                        {
                            ItemStockInfoModel itemStockInfoModel = new ItemStockInfoModel();
                            itemStockInfoModel.ItemStockId = s.ItemStockId;
                            itemStockInfoModel.ItemId = s.ItemId;
                            itemStockInfoModel.ItemNo = s.Item?.No;
                            itemStockInfoModel.Description = s.Item?.Description;
                            itemStockInfoModel.QuantityOnHand = s.QuantityOnHand;
                            itemStockInfoModel.IssueQuantity = s.IssueQuantity;
                            itemStockInfoModel.AddedByUser = s.AddedByUser?.UserName;
                            itemStockInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                            itemStockInfoModel.StatusCode = s.StatusCode?.CodeValue;
                            itemStockInfoModel.AddedByUserID = s.AddedByUserId;
                            itemStockInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                            itemStockInfoModel.StatusCodeID = s.StatusCodeId;
                            itemStockInfoModel.AddedDate = s.AddedDate;
                            itemStockInfoModels.Add(itemStockInfoModel);
                        });

                    }
                }
            }
            return itemStockInfoModels;
        }
        [HttpGet]
        [Route("GetItemBatchInfoByItemId")]
        public List<ItemBatchInfoModel> GetItemBatchInfoByItemId(int id)
        {

            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == id).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }

        [HttpPost]
        [Route("GetBalanceBatchInfoByItemId")]
        public List<ItemBatchInfoModel> GetBalanceBatchInfoByItemId(DisposalItemModel disposalItemModel)
        {

            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var sampleRequest = _context.SampleRequestFormLine.Where(s => s.ItemId == disposalItemModel.ItemId && s.InventoryItemId == disposalItemModel.InventoryTypeId).AsNoTracking().Select(s => s.BatchNo).ToList();
            var ItemBatchNos = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == disposalItemModel.ItemId).OrderByDescending(o => o.ItemBatchId).AsNoTracking().Select(s => s.BatchNo).ToList();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == disposalItemModel.ItemId).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            var issueRequestSampleLine = _context.IssueRequestSampleLine.Where(a => ItemBatchNos.Contains(a.BatchNo)).ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - (issueRequestSampleLine.Where(i => i.BatchNo == s.BatchNo).Sum(i => i.IssueQuantity));
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            if (itemBatchInfoModels.Count > 0)
            {
                itemBatchInfoModels = itemBatchInfoModels.Where(s => s.BalanceQuantity > 0 || s.BalanceQuantity == null).ToList();
            }
            return itemBatchInfoModels;
        }
        [HttpGet]
        [Route("GetItemBatchInfoForSampleLine")]
        public List<ItemBatchInfoModel> GetItemBatchInfoForSampleLine(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == id && s.NavQuantity > 0).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.BatchDetail = s.BatchNo + '|' + s.LotNo;
                    itemBatchInfoModel.LocationCode = s.LocationCode;
                    itemBatchInfoModel.LotNo = s.LotNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }
        [HttpGet]
        [Route("GetItemBatchInfoByOutStockQty")]
        public List<ItemBatchInfoModel> GetItemBatchInfoByOutStockQty(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            List<ItemBatchInfoModel> itemBatchInfoModels = new List<ItemBatchInfoModel>();
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var ItemBatchInfo = _context.ItemBatchInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).Include(c => c.Company).Where(s => s.ItemId == id && s.QuantityOnHand > 0).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    itemBatchInfoModel = new ItemBatchInfoModel();
                    itemBatchInfoModel.ItemBatchId = s.ItemBatchId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.CompanyId = s.CompanyId;
                    itemBatchInfoModel.BatchNo = s.BatchNo;
                    itemBatchInfoModel.BatchDetail = s.BatchNo + '|' + s.LotNo;
                    itemBatchInfoModel.LocationCode = s.LocationCode;
                    itemBatchInfoModel.LotNo = s.LotNo;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.ManufacturingDate = s.ManufacturingDate;
                    itemBatchInfoModel.ExpiryDate = s.ExpiryDate;
                    //itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.NavQuantity = s.NavQuantity;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.BalanceQuantity = s.QuantityOnHand - s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.CompanyName = s.Company?.Description;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand - (s.StockOutBatchConfirm != null || s.StockOutBatchConfirm > 0 ? s.StockOutBatchConfirm : 0);
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels.Where(w => w.QuantityOnHand > 0).ToList();
        }
        [HttpPost]
        [Route("GetItemByBatchNo")]
        public ItemBatchInfoModel GetItemByBatchNo(SearchModel SearchModel)
        {
            ItemBatchInfoModel itemBatchInfoModel = new ItemBatchInfoModel();
            var existingBatchNo = _context.ItemBatchInfo.Where(s => s.BatchNo.ToLower().Trim() == SearchModel.SearchString.ToLower().Trim()).FirstOrDefault();
            if (existingBatchNo != null)
            {
                itemBatchInfoModel.ItemBatchId = existingBatchNo.ItemBatchId;
                itemBatchInfoModel.ItemId = existingBatchNo.ItemId;
                itemBatchInfoModel.CompanyId = existingBatchNo.CompanyId;
                itemBatchInfoModel.BalanceQuantity = existingBatchNo.BalanceQuantity;
                itemBatchInfoModel.QuantityOnHand = existingBatchNo.QuantityOnHand;
                itemBatchInfoModel.NavQuantity = existingBatchNo.NavQuantity;
                itemBatchInfoModel.IssueQuantity = existingBatchNo.IssueQuantity;
                itemBatchInfoModel.ManufacturingDate = existingBatchNo.ManufacturingDate;
                itemBatchInfoModel.ExpiryDate = existingBatchNo.ExpiryDate;
            }
            return itemBatchInfoModel;
        }
        [HttpPost()]
        [Route("GetItemBatchInfoData")]
        public ActionResult<ItemBatchInfoModel> GetData(SearchModel searchModel)
        {
            var ItemBatchInfo = new ItemBatchInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).FirstOrDefault();
                        break;
                    case "Last":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).LastOrDefault();
                        break;
                    case "Next":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).LastOrDefault();
                        break;
                    case "Previous":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).FirstOrDefault();
                        break;
                    case "Last":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).LastOrDefault();
                        break;
                    case "Next":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderBy(o => o.ItemBatchId).FirstOrDefault(s => s.ItemBatchId > searchModel.Id);
                        break;
                    case "Previous":
                        ItemBatchInfo = _context.ItemBatchInfo.OrderByDescending(o => o.ItemBatchId).FirstOrDefault(s => s.ItemBatchId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ItemBatchInfoModel>(ItemBatchInfo);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertItemBatchInfo")]
        public ItemBatchInfoModel Post(ItemBatchInfoModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var existingbatch = _context.ItemBatchInfo.Where(s => s.BatchNo.ToLower().Trim() == value.BatchNo.ToLower().Trim()).FirstOrDefault();
            try
            {
                if (existingbatch == null)
                {
                    var ItemBatchInfo = new ItemBatchInfo
                    {
                        ItemId = value.ItemId.Value,
                        BatchNo = value.BatchNo,
                        ManufacturingDate = value.ManufacturingDate,
                        ExpiryDate = value.ExpiryDate,
                        AddedByUserId = value.AddedByUserID,
                        CompanyId = value.CompanyId,
                        QuantityOnHand = value.QuantityOnHand,
                        IssueQuantity = value.IssueQuantity,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        BalanceQuantity = value.QuantityOnHand - value.IssueQuantity,
                    };
                    _context.ItemBatchInfo.Add(ItemBatchInfo);
                    value.ItemBatchId = ItemBatchInfo.ItemBatchId;
                    _context.SaveChanges();
                }
                else
                {
                    var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.BatchNo.ToLower().Trim() == value.BatchNo.ToLower().Trim()).FirstOrDefault();
                    if (itemBatchinfo != null)
                    {
                        itemBatchinfo.QuantityOnHand = itemBatchinfo.QuantityOnHand + value.QuantityOnHand;
                    }

                    var existingStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();
                    if (existingStockInfo != null)
                    {
                        existingStockInfo.QuantityOnHand = existingStockInfo.QuantityOnHand + value.QuantityOnHand;
                        existingStockInfo.BalanceQuantity = existingStockInfo.BalanceQuantity + value.QuantityOnHand;
                    }
                    _context.SaveChanges();
                }
                if (value.CompanyId > 0)
                {
                    value.CompanyName = _context.Plant.Where(c => c.PlantId == value.CompanyId).Select(c => c.Description).FirstOrDefault();
                }
                if (value.ItemId > 0)
                {
                    value.ItemNo = _context.Navitems.Where(s => s.ItemId == value.ItemId).Select(s => s.No).FirstOrDefault();
                }

                var existing = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();
                if (existing == null)
                {
                    var itemStockInfo = new ItemStockInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QuantityOnHand,
                        IssueQuantity = value.IssueQuantity,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        BalanceQuantity = value.QuantityOnHand - value.IssueQuantity,

                    };
                    _context.ItemStockInfo.Add(itemStockInfo);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Batch No Already Exist To The Current Selected Item", ex);
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemBatchInfo")]
        public ItemBatchInfoModel Put(ItemBatchInfoModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var itemBatchInfo = _context.ItemBatchInfo.SingleOrDefault(p => p.ItemBatchId == value.ItemBatchId);
            if (itemBatchInfo != null)
            {


                itemBatchInfo.ModifiedByUserId = value.ModifiedByUserID;
                itemBatchInfo.ModifiedDate = DateTime.Now;
                itemBatchInfo.StatusCodeId = value.StatusCodeID.Value;
                itemBatchInfo.QuantityOnHand = value.QuantityOnHand;
                itemBatchInfo.IssueQuantity = value.IssueQuantity;
                itemBatchInfo.BalanceQuantity = value.QuantityOnHand - value.IssueQuantity;

            }
            _context.SaveChanges();
            decimal? quantityOnHand = 0;
            decimal? issueQuantity = 0;
            if (value.ItemId > 0)
            {
                var items = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId).ToList();
                if (items != null && items.Count > 0)
                {
                    items.ForEach(i =>
                    {
                        quantityOnHand = quantityOnHand + i.QuantityOnHand;
                        issueQuantity = issueQuantity + i.IssueQuantity;
                    });
                }
                var itemStockInfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();
                if (itemStockInfo != null)
                {
                    itemStockInfo.QuantityOnHand = quantityOnHand;
                    itemBatchInfo.IssueQuantity = issueQuantity;
                    itemBatchInfo.BalanceQuantity = quantityOnHand - issueQuantity;
                }
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemBatchInfo")]
        public void Delete(int id)
        {
            var ItemBatchInfo = _context.ItemBatchInfo.SingleOrDefault(p => p.ItemBatchId == id);
            if (ItemBatchInfo != null)
            {
                _context.ItemBatchInfo.Remove(ItemBatchInfo);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetItemStockInfo")]
        public List<ItemStockInfoModel> GetItemStockInfo()
        {
            List<ItemStockInfoModel> itemBatchInfoModels = new List<ItemStockInfoModel>();
            var ItemBatchInfo = _context.ItemStockInfo.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.StatusCode).OrderByDescending(o => o.ItemStockId).AsNoTracking().ToList();
            if (ItemBatchInfo != null && ItemBatchInfo.Count > 0)
            {
                ItemBatchInfo.ForEach(s =>
                {
                    ItemStockInfoModel itemBatchInfoModel = new ItemStockInfoModel();
                    itemBatchInfoModel.ItemStockId = s.ItemStockId;
                    itemBatchInfoModel.ItemId = s.ItemId;
                    itemBatchInfoModel.ItemNo = s.Item?.No;
                    itemBatchInfoModel.Description = s.Item?.Description;
                    itemBatchInfoModel.QuantityOnHand = s.QuantityOnHand;
                    itemBatchInfoModel.IssueQuantity = s.IssueQuantity;
                    itemBatchInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    itemBatchInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    itemBatchInfoModel.StatusCode = s.StatusCode?.CodeValue;
                    itemBatchInfoModel.AddedByUserID = s.AddedByUserId;
                    itemBatchInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                    itemBatchInfoModel.StatusCodeID = s.StatusCodeId;
                    itemBatchInfoModel.AddedDate = s.AddedDate;
                    itemBatchInfoModel.BalanceQuantity = s.BalanceQuantity;
                    itemBatchInfoModels.Add(itemBatchInfoModel);
                });

            }
            return itemBatchInfoModels;
        }

        [HttpGet]
        [Route("GetItemStockInfoByItemId")]
        public ItemStockInfoModel GetItemStockInfoByItemId(long? id)
        {
            ItemStockInfoModel itemBatchInfoModel = new ItemStockInfoModel();
            List<ItemStockInfoModel> itemBatchInfoModels = new List<ItemStockInfoModel>();
            var itemBatchInfo = _context.ItemStockInfo.Where(s => s.ItemId == id).OrderByDescending(o => o.ItemStockId).AsNoTracking().FirstOrDefault();
            if (itemBatchInfo != null)
            {

                itemBatchInfoModel.ItemStockId = itemBatchInfo.ItemStockId;
                itemBatchInfoModel.ItemId = itemBatchInfo.ItemId;
                itemBatchInfoModel.QuantityOnHand = itemBatchInfo.QuantityOnHand;
                itemBatchInfoModel.IssueQuantity = itemBatchInfo.IssueQuantity;
                itemBatchInfoModel.BalanceQuantity = itemBatchInfo.BalanceQuantity;


            }
            return itemBatchInfoModel;
        }

        [HttpGet]
        [Route("GetItemStockReport")]
        public ItemStockReportAllModel GetItemStockReport()
        {
            ItemStockReportAllModel ItemStockReportAllModel = new ItemStockReportAllModel();
            List<ItemStockReportModel> ItemStockReportModels = new List<ItemStockReportModel>();
            List<ItemExportStockReportModel> ItemExportStockReportModels = new List<ItemExportStockReportModel>();
            var itemstockinfo = _context.ItemStockInfo.Include(a => a.Item).Where(w => w.QuantityOnHand > 0).ToList();
            var itemBatchInfos = _context.ItemBatchInfo.Include(a => a.Item).Where(w => w.QuantityOnHand > 0).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            itemstockinfo.ForEach(s =>
            {
                ItemExportStockReportModel ItemExportStockReportModel = new ItemExportStockReportModel();
                ItemStockReportModel itemStockReportModel = new ItemStockReportModel();
                itemStockReportModel.BalanceQuantity = s.BalanceQuantity;
                itemStockReportModel.QuantityOnHand = s.QuantityOnHand;
                itemStockReportModel.ItemId = s.ItemId;
                itemStockReportModel.ItemNo = s.Item?.No;
                itemStockReportModel.Description = s.Item?.Description;
                itemStockReportModel.ItemCategoryCode = s.Item?.ItemCategoryCode;
                itemStockReportModel.Uom = s.Item?.BaseUnitofMeasure;
                itemStockReportModel.IssueQuantity = s.IssueQuantity;

                //export
                ItemExportStockReportModel.ItemNo = s.Item?.No;
                ItemExportStockReportModel.Description = s.Item?.Description;
                ItemExportStockReportModel.ItemCategoryCode = s.Item?.ItemCategoryCode;
                ItemExportStockReportModel.QuantityOnHand = s.QuantityOnHand != null ? s.QuantityOnHand.ToString() : "";
                ItemExportStockReportModel.IssueQuantity = s.IssueQuantity != null ? s.IssueQuantity.ToString() : "";
                ItemExportStockReportModel.Uom = s.Item?.BaseUnitofMeasure;
                //
                ItemExportStockReportModels.Add(ItemExportStockReportModel);
                var itemBatchInfo = itemBatchInfos.Where(w => w.ItemId == s.ItemId).ToList();
                if (itemBatchInfo != null)
                {
                    ItemExportStockReportModel ItemExportStockReportModelbatchheader = new ItemExportStockReportModel();
                    ItemExportStockReportModelbatchheader.ItemNo = "Batch No";
                    ItemExportStockReportModelbatchheader.Description = "Mfg Date";
                    ItemExportStockReportModelbatchheader.ItemCategoryCode = "Exp Date";
                    ItemExportStockReportModelbatchheader.QuantityOnHand = "";
                    ItemExportStockReportModelbatchheader.IssueQuantity = "";
                    ItemExportStockReportModelbatchheader.Uom = "";
                    ItemExportStockReportModels.Add(ItemExportStockReportModelbatchheader);
                    List<ItemBatchInfoModel> ItemBatchInfoModels = new List<ItemBatchInfoModel>();
                    itemBatchInfo.ForEach(a =>
                    {
                        ItemBatchInfoModel ItemBatchInfoModel = new ItemBatchInfoModel();
                        ItemBatchInfoModel.ItemId = a.ItemId;
                        ItemBatchInfoModel.BatchNo = a.BatchNo;
                        ItemBatchInfoModel.ManufacturingDate = a.ManufacturingDate;
                        ItemBatchInfoModel.ExpiryDate = a.ExpiryDate;
                        ItemBatchInfoModel.QuantityOnHand = a.QuantityOnHand;
                        ItemBatchInfoModel.IssueQuantity = a.IssueQuantity;
                        ItemBatchInfoModel.BalanceQuantity = a.BalanceQuantity;
                        ItemBatchInfoModel.Uom = a.Item?.BaseUnitofMeasure;
                        ItemBatchInfoModels.Add(ItemBatchInfoModel);
                        //export
                        ItemExportStockReportModel ItemExportStockReportModelbatch = new ItemExportStockReportModel();
                        ItemExportStockReportModelbatch.ItemNo = a.BatchNo;
                        ItemExportStockReportModelbatch.Description = a.ManufacturingDate.ToString();
                        ItemExportStockReportModelbatch.ItemCategoryCode = a.ExpiryDate.ToString();
                        ItemExportStockReportModelbatch.QuantityOnHand = a.QuantityOnHand != null ? a.QuantityOnHand.ToString() : "";
                        ItemExportStockReportModelbatch.IssueQuantity = a.IssueQuantity != null ? a.IssueQuantity.ToString() : "";
                        ItemExportStockReportModelbatch.Uom = a.Item?.BaseUnitofMeasure;
                        ItemExportStockReportModels.Add(ItemExportStockReportModelbatch);
                    });
                    itemStockReportModel.ItemBatchInfo = ItemBatchInfoModels;
                }
                ItemStockReportModels.Add(itemStockReportModel);
            });
            ItemStockReportAllModel.ItemStockReportModel = ItemStockReportModels;
            ItemStockReportAllModel.ItemExportStockReportModel = ItemExportStockReportModels;
            return ItemStockReportAllModel;
        }
        [HttpPost]
        [Route("GetItemStockReportByClosingDate")]
        public ItemStockReportAllModel GetItemStockReportByClosingDate(SearchModel searchModel)
        {
            ItemStockReportAllModel ItemStockReportAllModel = new ItemStockReportAllModel();
            List<ItemStockReportModel> ItemStockReportModels = new List<ItemStockReportModel>();
            List<ItemExportStockReportModel> ItemExportStockReportModels = new List<ItemExportStockReportModel>();
            var itemstockinfo = _context.ItemStockInfo.Include(a => a.Item).Where(w => w.QuantityOnHand > 0).ToList();
            var itemBatchInfos = _context.ItemBatchInfo.Include(a => a.Item).Where(w => w.QuantityOnHand > 0).OrderByDescending(o => o.ItemBatchId).AsNoTracking().ToList();
            var SampleRequestFormLine = _context.SampleRequestFormLine.Include(a => a.IssueRequestSampleLine).ToList();
            var IssueRequestSampleLine = _context.IssueRequestSampleLine.Include(a => a.SampleRequestLine).ToList();
            if (itemstockinfo != null)
            {
                itemstockinfo.ForEach(s =>
                {
                    var IssueQuantitys = IssueRequestSampleLine.Where(w => w.SampleRequestLine.ItemId == s.ItemId && w.IssueQuantity != null && w.AddedDate.Value.Date <= searchModel.Date.Value.Date).Sum(i => i.IssueQuantity);
                    ItemExportStockReportModel ItemExportStockReportModel = new ItemExportStockReportModel();
                    ItemStockReportModel itemStockReportModel = new ItemStockReportModel();
                    itemStockReportModel.BalanceQuantity = s.BalanceQuantity;
                    itemStockReportModel.QuantityOnHand = s.QuantityOnHand- IssueQuantitys;
                    itemStockReportModel.ItemId = s.ItemId;
                    itemStockReportModel.ItemNo = s.Item?.No;
                    itemStockReportModel.Description = s.Item?.Description;
                    itemStockReportModel.ItemCategoryCode = s.Item?.ItemCategoryCode;
                    itemStockReportModel.Uom = s.Item?.BaseUnitofMeasure;
                    itemStockReportModel.IssueQuantity = s.IssueQuantity;

                    //export
                    ItemExportStockReportModel.ItemNo = s.Item?.No;
                    ItemExportStockReportModel.Description = s.Item?.Description;
                    ItemExportStockReportModel.ItemCategoryCode = s.Item?.ItemCategoryCode;
                    ItemExportStockReportModel.QuantityOnHand = s.QuantityOnHand != null ? (s.QuantityOnHand- IssueQuantitys).ToString() : "";
                    ItemExportStockReportModel.IssueQuantity = s.IssueQuantity != null ? s.IssueQuantity.ToString() : "";
                    ItemExportStockReportModel.Uom = s.Item?.BaseUnitofMeasure;
                    //
                    ItemExportStockReportModels.Add(ItemExportStockReportModel);
                    var itemBatchInfo = itemBatchInfos.Where(w => w.ItemId == s.ItemId).ToList();
                    if (itemBatchInfo != null)
                    {
                        ItemExportStockReportModel ItemExportStockReportModelbatchheader = new ItemExportStockReportModel();
                        ItemExportStockReportModelbatchheader.ItemNo = "Batch No";
                        ItemExportStockReportModelbatchheader.Description = "Mfg Date";
                        ItemExportStockReportModelbatchheader.ItemCategoryCode = "Exp Date";
                        ItemExportStockReportModelbatchheader.QuantityOnHand = "";
                        ItemExportStockReportModelbatchheader.IssueQuantity = "";
                        ItemExportStockReportModelbatchheader.Uom = "";
                        ItemExportStockReportModels.Add(ItemExportStockReportModelbatchheader);
                        List<ItemBatchInfoModel> ItemBatchInfoModels = new List<ItemBatchInfoModel>();
                        itemBatchInfo.ForEach(a =>
                        {
                            var IssueQuantity = IssueRequestSampleLine.Where(w => w.BatchNo == a.BatchNo && w.IssueQuantity != null && w.AddedDate.Value.Date <= searchModel.Date.Value.Date).Sum(i => i.IssueQuantity);
                            ItemBatchInfoModel ItemBatchInfoModel = new ItemBatchInfoModel();
                            ItemBatchInfoModel.ItemId = a.ItemId;
                            ItemBatchInfoModel.BatchNo = a.BatchNo;
                            ItemBatchInfoModel.ManufacturingDate = a.ManufacturingDate;
                            ItemBatchInfoModel.ExpiryDate = a.ExpiryDate;
                            ItemBatchInfoModel.QuantityOnHand = a.QuantityOnHand - IssueQuantity;
                            ItemBatchInfoModel.IssueQuantity = a.IssueQuantity;
                            ItemBatchInfoModel.BalanceQuantity = a.BalanceQuantity;
                            ItemBatchInfoModel.Uom = a.Item?.BaseUnitofMeasure;
                            ItemBatchInfoModels.Add(ItemBatchInfoModel);
                            //export
                            ItemExportStockReportModel ItemExportStockReportModelbatch = new ItemExportStockReportModel();
                            ItemExportStockReportModelbatch.ItemNo = a.BatchNo;
                            ItemExportStockReportModelbatch.Description = a.ManufacturingDate.ToString();
                            ItemExportStockReportModelbatch.ItemCategoryCode = a.ExpiryDate.ToString();
                            ItemExportStockReportModelbatch.QuantityOnHand = a.QuantityOnHand != null ? (a.QuantityOnHand - IssueQuantity).ToString() : "";
                            ItemExportStockReportModelbatch.IssueQuantity = a.IssueQuantity != null ? a.IssueQuantity.ToString() : "";
                            ItemExportStockReportModelbatch.Uom = a.Item?.BaseUnitofMeasure;
                            ItemExportStockReportModels.Add(ItemExportStockReportModelbatch);
                        });
                        itemStockReportModel.ItemBatchInfo = ItemBatchInfoModels;
                    }
                    ItemStockReportModels.Add(itemStockReportModel);
                });
            }
            ItemStockReportAllModel.ItemStockReportModel = ItemStockReportModels;
            ItemStockReportAllModel.ItemExportStockReportModel = ItemExportStockReportModels;
            return ItemStockReportAllModel;
        }
    }
}