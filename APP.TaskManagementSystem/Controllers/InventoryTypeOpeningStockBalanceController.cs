﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class InventoryTypeOpeningStockBalanceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IHubContext<ChatHub> _hub;
        public InventoryTypeOpeningStockBalanceController(CRT_TMSContext context, IMapper mapper, IConfiguration configuration, IHubContext<ChatHub> hub)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
            _hub = hub;
        }

        #region InventoryTypeOpeningStockBalance


        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetInventoryTypeOpeningBalance")]
        public List<InventoryTypeOpeningStockBalanceModel> Get(int id)
        {

            var inventoryTypeOpeningStockBalance = _context.InventoryTypeOpeningStockBalance
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .AsNoTracking().OrderByDescending(o => o.OpeningBalanceId).ToList();
            List<InventoryTypeOpeningStockBalanceModel> inventoryTypeOpeningStockBalanceModels = new List<InventoryTypeOpeningStockBalanceModel>();
            inventoryTypeOpeningStockBalance.ForEach(s =>
            {
                InventoryTypeOpeningStockBalanceModel inventoryTypeOpeningStockBalanceModel = new InventoryTypeOpeningStockBalanceModel
                {
                    OpeningBalanceId = s.OpeningBalanceId,
                    InventoryTypeId = s.InventoryTypeId,
                    LocationId = s.LocationId,
                    AreaId = s.AreaId,
                    SpecificAreaId = s.SpecificAreaId,
                    Location = s.Location?.Name,
                    Area = s.Area?.Name,
                    SpecificArea = s.SpecificArea?.Name,
                    ItemCategory = s.ItemCategory,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                };
                inventoryTypeOpeningStockBalanceModels.Add(inventoryTypeOpeningStockBalanceModel);
            });

            return inventoryTypeOpeningStockBalanceModels.ToList();
        }


        [HttpPost()]
        [Route("GetInventoryTypeOpeningStockBalanceData")]
        public ActionResult<InventoryTypeOpeningStockBalanceModel> GetData(SearchModel searchModel)
        {
            var openingBalance = new InventoryTypeOpeningStockBalance();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).FirstOrDefault();
                        break;
                    case "Last":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).LastOrDefault();
                        break;
                    case "Next":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).LastOrDefault();
                        break;
                    case "Previous":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).FirstOrDefault();
                        break;
                    case "Last":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).LastOrDefault();
                        break;
                    case "Next":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderBy(o => o.OpeningBalanceId).FirstOrDefault(s => s.OpeningBalanceId > searchModel.Id);
                        break;
                    case "Previous":
                        openingBalance = _context.InventoryTypeOpeningStockBalance.OrderByDescending(o => o.OpeningBalanceId).FirstOrDefault(s => s.OpeningBalanceId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<InventoryTypeOpeningStockBalanceModel>(openingBalance);
            return result;
        }
        // POST: api/InventoryTypeOpeningStockBalance
        [HttpPost]
        [Route("InsertInventoryTypeOpeningBalance")]
        public InventoryTypeOpeningStockBalanceModel Post(InventoryTypeOpeningStockBalanceModel value)
        {
            var sessionId = Guid.NewGuid();


            var openingBalance = new InventoryTypeOpeningStockBalance
            {

                InventoryTypeId = value.InventoryTypeId,
                LocationId = value.LocationId,
                AreaId = value.AreaId,
                SpecificAreaId = value.SpecificAreaId,
                ItemCategory = value.ItemCategory,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = sessionId,


            };
            _context.InventoryTypeOpeningStockBalance.Add(openingBalance);
            _context.SaveChanges();
            value.SessionId = sessionId;
            value.OpeningBalanceId = openingBalance.OpeningBalanceId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateInventoryTypeOpeningBalance")]
        public InventoryTypeOpeningStockBalanceModel Put(InventoryTypeOpeningStockBalanceModel value)
        {
            var inventorytypeOpeningBalance = _context.InventoryTypeOpeningStockBalance.SingleOrDefault(p => p.OpeningBalanceId == value.OpeningBalanceId);


            inventorytypeOpeningBalance.InventoryTypeId = value.InventoryTypeId;
            inventorytypeOpeningBalance.LocationId = value.LocationId;
            inventorytypeOpeningBalance.AreaId = value.AreaId;
            inventorytypeOpeningBalance.SpecificAreaId = value.SpecificAreaId;
            inventorytypeOpeningBalance.ItemCategory = value.ItemCategory;
            inventorytypeOpeningBalance.ModifiedByUserId = value.ModifiedByUserID;
            inventorytypeOpeningBalance.ModifiedDate = DateTime.Now;
            inventorytypeOpeningBalance.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInventoryTypeOpeningBalance")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var openingBalance = _context.InventoryTypeOpeningStockBalance.SingleOrDefault(p => p.OpeningBalanceId == id);
                if (openingBalance != null)
                {
                    _context.InventoryTypeOpeningStockBalance.Remove(openingBalance);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Inventory Opening Stock Cannot be delete! Reference to others");
            }
        }

        #endregion

        #region InventoryTypeOpeningStockBalanceLine

        // GET: api/WorkOrders
        [HttpGet]
        [Route("GetInventoryTypeOpeningBalanceLines")]
        public List<InventoryTypeOpeningStockBalanceLineModel> GetInventoryTypeOpeningBalanceLines(int id)
        {
            var workOrderLines = _context.InventoryTypeOpeningStockBalanceLine
                                .Include(p => p.StatusCode)
                                .Include(p => p.AddedByUser)
                                .Include(p => p.ModifiedByUser)
                                .Include(a => a.Item)
                                .AsNoTracking().Where(o => o.OpeningStockId == id).ToList();
            List<InventoryTypeOpeningStockBalanceLineModel> inventoryTypeOpeningStockBalanceLineModels = new List<InventoryTypeOpeningStockBalanceLineModel>();
            workOrderLines.ForEach(s =>
            {
                InventoryTypeOpeningStockBalanceLineModel inventoryTypeOpeningStockBalanceLineModel = new InventoryTypeOpeningStockBalanceLineModel
                {
                    OpeningStockId = s.OpeningStockId,
                    OpeningStockLineId = s.OpeningStockLineId,
                    BatchNo = s.BatchNo,
                    ItemId = s.ItemId,
                    ItemNo = s.Item?.No,
                    QtyOnHand = s.QtyOnHand,
                    SessionId = s.SessionId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                };
                inventoryTypeOpeningStockBalanceLineModels.Add(inventoryTypeOpeningStockBalanceLineModel);
            });

            return inventoryTypeOpeningStockBalanceLineModels;
        }



        // POST: api/InventoryTypeOpeningStockBalance
        [HttpPost]
        [Route("InsertInventoryTypeOpeningStockBalanceLine")]
        public InventoryTypeOpeningStockBalanceLineModel InsertInventoryTypeOpeningStockBalanceLine(InventoryTypeOpeningStockBalanceLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var inventoryTypeOpeningStock = _context.InventoryTypeOpeningStockBalanceLine
                .Where(w => w.BatchNo == value.BatchNo && w.ItemId == value.ItemId && w.OpeningStockId == value.OpeningStockId).FirstOrDefault();
            if (inventoryTypeOpeningStock == null)
            {
                var inventoryTypeOpeningStockBalanceLine = new InventoryTypeOpeningStockBalanceLine
                {
                    OpeningStockId = value.OpeningStockId,
                    ItemId = value.ItemId,
                    BatchNo = value.BatchNo,
                    QtyOnHand = value.QtyOnHand,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    SessionId = sessionId,
                };
                _context.InventoryTypeOpeningStockBalanceLine.Add(inventoryTypeOpeningStockBalanceLine);
                _context.SaveChanges();
                value.SessionId = sessionId;
                value.OpeningStockLineId = inventoryTypeOpeningStockBalanceLine.OpeningStockLineId;
            }
            else
            {
                inventoryTypeOpeningStock.QtyOnHand += value.QtyOnHand;
                _context.SaveChanges();
            }
            if (value.ItemId != null && value.BatchNo != null)
            {
                var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
                if (itemBatchinfo != null)
                {
                    itemBatchinfo.QuantityOnHand = itemBatchinfo.QuantityOnHand + value.QtyOnHand;
                    _context.SaveChanges();
                }
                else if (itemBatchinfo == null)
                {
                    var itembatchinfo = new ItemBatchInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QtyOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        BatchNo = value.BatchNo,
                    };
                    _context.ItemBatchInfo.Add(itembatchinfo);
                    _context.SaveChanges();
                }
                var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();

                if (itemStockinfo == null)
                {
                    var itemstockinfo = new ItemStockInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QtyOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                    };
                    _context.ItemStockInfo.Add(itemstockinfo);

                }
                else if (itemStockinfo != null)
                {
                    itemStockinfo.QuantityOnHand = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId).Sum(t => t.QuantityOnHand);
                }

                _context.SaveChanges();
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateInventoryTypeOpeningStockBalanceLine")]
        public InventoryTypeOpeningStockBalanceLineModel UpdateInventoryTypeOpeningStockBalanceLine(InventoryTypeOpeningStockBalanceLineModel value)
        {
            var inventoryTypeOpeningStockBalanceLine = _context.InventoryTypeOpeningStockBalanceLine.SingleOrDefault(p => p.OpeningStockLineId == value.OpeningStockLineId);
            var differQty = value.QtyOnHand - inventoryTypeOpeningStockBalanceLine.QtyOnHand;
            inventoryTypeOpeningStockBalanceLine.OpeningStockId = value.OpeningStockId;
            inventoryTypeOpeningStockBalanceLine.ItemId = value.ItemId;
            inventoryTypeOpeningStockBalanceLine.BatchNo = value.BatchNo;
            inventoryTypeOpeningStockBalanceLine.QtyOnHand = value.QtyOnHand;
            inventoryTypeOpeningStockBalanceLine.ModifiedByUserId = value.ModifiedByUserID;
            inventoryTypeOpeningStockBalanceLine.ModifiedDate = DateTime.Now;
            inventoryTypeOpeningStockBalanceLine.StatusCodeId = value.StatusCodeID.Value;
            if (value.ItemId != null && value.BatchNo != null)
            {
                var itemBatchinfo = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId && s.BatchNo == value.BatchNo).FirstOrDefault();
                if (itemBatchinfo != null)
                {
                    itemBatchinfo.QuantityOnHand += differQty;
                    _context.SaveChanges();
                }
                else if (itemBatchinfo == null)
                {
                    var itembatchinfo = new ItemBatchInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QtyOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,
                        BatchNo = value.BatchNo,
                    };
                    _context.ItemBatchInfo.Add(itembatchinfo);
                    _context.SaveChanges();
                }
                var itemStockinfo = _context.ItemStockInfo.Where(s => s.ItemId == value.ItemId).FirstOrDefault();

                if (itemStockinfo == null)
                {
                    var itemstockinfo = new ItemStockInfo
                    {
                        ItemId = value.ItemId.Value,
                        QuantityOnHand = value.QtyOnHand,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID.Value,

                    };
                    _context.ItemStockInfo.Add(itemstockinfo);

                }
                else if (itemStockinfo != null)
                {
                    itemStockinfo.QuantityOnHand = _context.ItemBatchInfo.Where(s => s.ItemId == value.ItemId).Sum(t => t.QuantityOnHand);
                }

                _context.SaveChanges();
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInventoryTypeOpeningStockBalanceLine")]
        public ActionResult<string> DeleteInventoryTypeOpeningStockBalanceLine(int id)
        {
            try
            {
                var inventoryTypeOpeningStockBalanceLine = _context.InventoryTypeOpeningStockBalanceLine.SingleOrDefault(p => p.OpeningStockLineId == id);
                if (inventoryTypeOpeningStockBalanceLine != null)
                {
                    _context.InventoryTypeOpeningStockBalanceLine.Remove(inventoryTypeOpeningStockBalanceLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Inventory Opening Stock line Cannot be delete! Reference to others");
            }
        }

        #endregion
    }
}
