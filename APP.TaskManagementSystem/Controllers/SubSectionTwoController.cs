﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SubSectionTwoController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SubSectionTwoController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetSubSectionsTwo")]
        public List<SubSectionTwoModel> Get()
        {
            List<SubSectionTwoModel> subSectionTwoModels = new List<SubSectionTwoModel>();
            var subSectionTwo = _context.SubSectionTwo
                .Include(a=>a.AddedByUser)
                .Include(a => a.ModifiedByUser)
               .Include(a => a.StatusCode)
              .Include(a=>a.SubSecton)
               .Include(a=>a.SubSecton.Section)
              .Include(a => a.SubSecton.Section.Department)
               .Include(a=>a.SubSecton.Section.Department.Division)
               .Include(a=>a.SubSecton.Section.Department.Division.Company).OrderByDescending(o => o.SubSectionTid).AsNoTracking().ToList();
            if (subSectionTwo != null && subSectionTwo.Count > 0)
            {
                subSectionTwo.ForEach(s =>
                {
                    SubSectionTwoModel subSectionTwoModel = new SubSectionTwoModel();
                    subSectionTwoModel.SubSectionID = s.SubSectonId;
                    subSectionTwoModel.SubSectionTID = s.SubSectionTid;
                    subSectionTwoModel.Name = s.Name;
                    subSectionTwoModel.HeadCount = s.HeadCount;
                    subSectionTwoModel.SectionName = s.SubSecton?.Section?.Name;
                    subSectionTwoModel.CompanyName = s.SubSecton?.Section?.Department?.Division?.Company?.Description;
                    subSectionTwoModel.CompanyID = s.SubSecton?.Section?.Department?.Division?.Company?.CompanyId;
                    subSectionTwoModel.DivisionName = s.SubSecton?.Section?.Department?.Division?.Name;
                    subSectionTwoModel.DepartmentName = s.SubSecton?.Section?.Department?.Name;
                    subSectionTwoModel.SubSectionName = s.SubSecton != null ? s.SubSecton.Name : string.Empty;
                    subSectionTwoModel.Code = s.Code;
                    subSectionTwoModel.Description = s.Description;
                    subSectionTwoModel.StatusCodeID = s.StatusCodeId;
                    subSectionTwoModel.AddedByUserID = s.AddedByUserId;
                    subSectionTwoModel.ModifiedByUserID = s.ModifiedByUserId;
                    subSectionTwoModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    subSectionTwoModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    subSectionTwoModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    subSectionTwoModel.AddedDate = s.AddedDate;
                    subSectionTwoModel.ModifiedDate = s.ModifiedDate;
                    subSectionTwoModel.SectionID = s.SubSecton != null ? s.SubSecton.Section != null ? s.SubSecton.Section.SectionId : new long() : new long();
                    subSectionTwoModel.PlantID = s.SubSecton?.Section?.Department?.Division?.Company?.PlantId;
                    subSectionTwoModel.DivisionID = s.SubSecton?.Section?.Department?.Division?.DivisionId;
                    subSectionTwoModel.DepartmentID = s.SubSecton?.Section?.Department?.DepartmentId;
                    subSectionTwoModels.Add(subSectionTwoModel);
                });
            }

            if (subSectionTwoModels != null && subSectionTwoModels.Count > 0)
            {
                subSectionTwoModels.ForEach(d =>
                {
                    d.DropDownNames = d.CompanyName + " | " + d.DivisionName + " | " + d.DepartmentName + " | " + d.SectionName + " | " + d.SubSectionName + "|" + d.Name;

                });
            }
            //var result = _mapper.Map<List<SubSectionModel>>(SubSectionTwo);
            return subSectionTwoModels;
        }
        [HttpGet]
        [Route("GetSubSectionsTwoBySubSection")]
        public List<SubSectionTwoModel> GetSubSectionsTwoBySubSection(int id)
        {
            var subSectionTwo = _context.SubSectionTwo
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Select(s => new SubSectionTwoModel
                {
                    SubSectionID = s.SubSectonId,
                    SubSectionTID = s.SubSectionTid,
                    Name = s.Name,
                    HeadCount = s.HeadCount,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate

                }).Where(s => s.SubSectionID == id).AsNoTracking().ToList();
            //var result = _mapper.Map<List<SubSectionModel>>(SubSectionTwo);
            return subSectionTwo;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get SubSectionTwo")]
        [HttpGet("GetSubSections/{id:int}")]
        public ActionResult<SubSectionTwoModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subSectionTwo = _context.SubSectionTwo.SingleOrDefault(p => p.SubSectionTid == id.Value);
            var result = _mapper.Map<SubSectionTwoModel>(subSectionTwo);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SubSectionTwoModel> GetData(SearchModel searchModel)
        {
            var subSectionTwo = new SubSectionTwo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).FirstOrDefault();
                        break;
                    case "Last":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).LastOrDefault();
                        break;
                    case "Next":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).LastOrDefault();
                        break;
                    case "Previous":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).FirstOrDefault();
                        break;
                    case "Last":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).LastOrDefault();
                        break;
                    case "Next":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderBy(o => o.SubSectionTid).FirstOrDefault(s => s.SubSectionTid > searchModel.Id);
                        break;
                    case "Previous":
                        subSectionTwo = _context.SubSectionTwo.Include("SubSecton").OrderByDescending(o => o.SubSectionTid).FirstOrDefault(s => s.SubSectionTid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SubSectionTwoModel>(subSectionTwo);
            if (result != null && result.SubSectionTID > 0)
            {
                if (subSectionTwo != null)
                {
                    result.SubSectionID = subSectionTwo.SubSectonId;
                }
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSubSectionTwo")]
        public SubSectionTwoModel Post(SubSectionTwoModel value)
        {
            var subSectionTwo = new SubSectionTwo
            {
                SubSectonId = value.SubSectionID,
                Name = value.Name,
                Code = value.Code,
                Description = value.Description,
                HeadCount = value.HeadCount,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.SubSectionTwo.Add(subSectionTwo);
            _context.SaveChanges();
            value.SubSectionID = subSectionTwo.SubSectionTid;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSubSectionTwo")]
        public SubSectionTwoModel Put(SubSectionTwoModel value)
        {
            var subSectionTwo = _context.SubSectionTwo.SingleOrDefault(p => p.SubSectionTid == value.SubSectionTID);
            subSectionTwo.SubSectionTid = value.SubSectionTID;
            subSectionTwo.SubSectonId = value.SubSectionID;
            subSectionTwo.Name = value.Name;
            subSectionTwo.Code = value.Code;
            subSectionTwo.HeadCount = value.HeadCount;
            subSectionTwo.Description = value.Description;
            subSectionTwo.ModifiedByUserId = value.ModifiedByUserID;
            subSectionTwo.ModifiedDate = DateTime.Now;
            subSectionTwo.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSubSectionTwo")]
        public void Delete(int id)
        {
            try
            {

                var subSectionTwo = _context.SubSectionTwo.SingleOrDefault(p => p.SubSectionTid == id);
                if (subSectionTwo != null)
                {
                    _context.SubSectionTwo.Remove(subSectionTwo);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Sub Section 2 Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}