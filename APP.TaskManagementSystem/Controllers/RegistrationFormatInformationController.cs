﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RegistrationFormatInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public RegistrationFormatInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetRegistrationFormatInformation")]
        public List<RegistrationFormatInformationModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RegistrationFormatMultiple = _context.RegistrationFormatMultiple.AsNoTracking().ToList();
            var RegistrationVariation = _context.RegistrationVariation.AsNoTracking().ToList();
            List<RegistrationFormatInformationModel> registrationFormatInformationModels = new List<RegistrationFormatInformationModel>();
            var RegistrationFormatInformation = _context.RegistrationFormatInformation
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .OrderByDescending(o => o.RegistrationFormatInformationId).AsNoTracking().ToList();
            RegistrationFormatInformation.ForEach(s =>
            {
                RegistrationFormatInformationModel registrationFormatInformationModel = new RegistrationFormatInformationModel();

                registrationFormatInformationModel.RegistrationFormatInformationId = s.RegistrationFormatInformationId;
                registrationFormatInformationModel.FormatId = s.FormatId;
                registrationFormatInformationModel.ProductCategoryId = s.ProductCategoryId;
                registrationFormatInformationModel.TypeId = s.TypeId;
                registrationFormatInformationModel.VariationDescription = s.VariationDescription;
                registrationFormatInformationModel.VariationCategoryNoId = s.VariationCategoryNoId;
                registrationFormatInformationModel.VariationOnId = s.VariationOnId;
                registrationFormatInformationModel.VariationCategoryId = s.VariationCategoryId;
                registrationFormatInformationModel.IsCombinationOfOtherFormat = s.IsCombinationOfOtherFormat;
                registrationFormatInformationModel.IsCombinationOfOtherFlag = s.IsCombinationOfOtherFormat == true ? "1" : "0";
                registrationFormatInformationModel.FormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId).Value : "";
                registrationFormatInformationModel.ProductCategoryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProductCategoryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ProductCategoryId).Value : "";
                registrationFormatInformationModel.TypeName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TypeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.TypeId).Value : "";
                registrationFormatInformationModel.VariationOnName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationOnId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationOnId).Value : "";
                registrationFormatInformationModel.VariationCategoryName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCategoryId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCategoryId).Value : "";
                registrationFormatInformationModel.VariationCategoryNoName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCategoryNoId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.VariationCategoryNoId).Value : "";
                registrationFormatInformationModel.RegistrationFormatListIds = RegistrationFormatMultiple.Where(p => p.RegistrationFormatInformationId == s.RegistrationFormatInformationId).Select(p => p.FormatId.Value).ToList();
                registrationFormatInformationModel.VariationCodeId = s.VariationCodeId;
                registrationFormatInformationModel.VariationCodeName = RegistrationVariation.Where(r => r.RegistrationVariationId == s.VariationCodeId).Select(r => (applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.RegistrationVariationCodeId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == r.RegistrationVariationCodeId).Value : "")).FirstOrDefault();
                registrationFormatInformationModel.AddedByUserID = s.AddedByUserId;
                registrationFormatInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                registrationFormatInformationModel.AddedDate = s.AddedDate;
                registrationFormatInformationModel.ModifiedDate = s.ModifiedDate;
                registrationFormatInformationModel.AddedByUser = s.AddedByUser.UserName;
                registrationFormatInformationModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                registrationFormatInformationModel.StatusCode = s.StatusCode?.CodeValue;
                registrationFormatInformationModel.StatusCodeID = s.StatusCodeId;
                registrationFormatInformationModels.Add(registrationFormatInformationModel);
            });

            return registrationFormatInformationModels;
        }
        [HttpGet]
        [Route("GetRegistrationFormatInformationItems")]
        public List<RegistrationFormatInformationModel> RegistrationFormatInformationItems()
        {
            List<RegistrationFormatInformationModel> registrationFormatInformationModels = new List<RegistrationFormatInformationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var RegistrationFormatInformation = _context.RegistrationFormatInformation
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .OrderByDescending(o => o.RegistrationFormatInformationId).AsNoTracking().ToList();
            RegistrationFormatInformation.ForEach(s =>
            {
                RegistrationFormatInformationModel registrationFormatInformationModel = new RegistrationFormatInformationModel
                {
                    RegistrationFormatInformationId = s.RegistrationFormatInformationId,
                    FormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId).Value : "",
                };
                registrationFormatInformationModels.Add(registrationFormatInformationModel);
            });

            return registrationFormatInformationModels;
        }
        [HttpGet]
        [Route("GetRegistrationFormatInformationLineUsers")]
        public List<RegistrationFormatInformationLineUsersModel> GetRegistrationFormatInformationLineUsers(int id)
        {

            var itemClassificationAccess = _context.RegistrationFormatInformationLineUsers.Include(a => a.User)
                .Include(d => d.User.Employe)
                .Include(b => b.Role)
                .Include(c => c.UserGroup)
                .Include(c => c.UserGroup.UserGroupUser).Where(f => f.RegistrationFormatInformationLineId == id)
                .AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<long> userIds = itemClassificationAccess.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee.Include("Department").Include("Designation").Where(e => e.UserId != null && userIds.Distinct().ToList().Contains(e.UserId.Value)).AsNoTracking().ToList();
            List<RegistrationFormatInformationLineUsersModel> itemClassificationAccessModels = new List<RegistrationFormatInformationLineUsersModel>();
            itemClassificationAccess.ForEach(u =>
            {
                var employee = employees.FirstOrDefault(e => e.UserId == u.UserId);
                var userUserRole = itemClassificationAccess.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u.UserId);
                RegistrationFormatInformationLineUsersModel itemClassificationAccessModel = new RegistrationFormatInformationLineUsersModel();
                itemClassificationAccessModel.RegistrationFormatInformationLineUsersId = u.RegistrationFormatInformationLineUsersId;
                itemClassificationAccessModel.RegistrationFormatInformationLineId = u.RegistrationFormatInformationLineId;
                itemClassificationAccessModel.UserId = u.UserId;
                itemClassificationAccessModel.RoleId = u.RoleId;
                itemClassificationAccessModel.RoleName = u.Role?.DocumentRoleName;
                itemClassificationAccessModel.UserName = u.User?.UserName;
                itemClassificationAccessModel.NickName = employee.NickName;
                itemClassificationAccessModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                itemClassificationAccessModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                itemClassificationAccessModels.Add(itemClassificationAccessModel);
            });

            itemClassificationAccessModels = itemClassificationAccessModels.OrderByDescending(o => o.RegistrationFormatInformationLineUsersId).Where(s => s.RegistrationFormatInformationLineId == id).ToList();
            return itemClassificationAccessModels;

        }

        [HttpGet]
        [Route("GetRegistrationFormatInformationLine")]
        public List<RegistrationFormatInformationLineModel> GetRegistrationFormatInformationLine(int id, long? userId)
        {
            List<RegistrationFormatInformationLineModel> registrationFormatInformationLineModels = new List<RegistrationFormatInformationLineModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            var RegistrationFormatInformationLine = _context.RegistrationFormatInformationLine
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include(a => a.RegistrationFormatInformationLineUsers)
                .Where(f => f.RegistrationFormatInformationId == id).OrderByDescending(o => o.RegistrationFormatInformationLineId).AsNoTracking().ToList();
            var documentPermission = _context.DocumentPermission.AsNoTracking().ToList();
            RegistrationFormatInformationLine.ForEach(s =>
            {
                bool? isEdit = false;
                bool? IsDelete = false;
                if (s.AddedByUserId == userId)
                {
                    isEdit = true;
                    IsDelete = true;
                }
                else
                {
                    if (s.RegistrationFormatInformationLineUsers != null)
                    {
                        var roleId = s.RegistrationFormatInformationLineUsers.Where(w => w.UserId == userId).FirstOrDefault()?.RoleId;
                        if (roleId != null)
                        {
                            var documentPermissionData = documentPermission.Where(w => w.DocumentRoleId == roleId).FirstOrDefault();
                            isEdit = documentPermissionData?.IsEdit;
                            IsDelete = documentPermissionData?.IsDelete;
                        }
                    }
                }
                RegistrationFormatInformationLineModel registrationFormatInformationLineModel = new RegistrationFormatInformationLineModel
                {
                    RegistrationFormatInformationLineId = s.RegistrationFormatInformationLineId,
                    RegistrationFormatInformationId = s.RegistrationFormatInformationId,
                    PartNameId = s.PartNameId,
                    SectionNameId = s.SectionNameId,
                    PartName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PartNameId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PartNameId).Value : "",
                    SectionName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SectionNameId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.SectionNameId).Value : "",
                    Numbering = s.Numbering,
                    Description = s.Description,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    StatusCodeID = s.StatusCodeId,
                    FieldTypeId = s.FieldTypeId,
                    FieldType = s.FieldType?.CodeValue,
                    isEdit = isEdit,
                    isDelete = IsDelete,

                };
                registrationFormatInformationLineModels.Add(registrationFormatInformationLineModel);
            });

            return registrationFormatInformationLineModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RegistrationFormatInformationModel> GetData(SearchModel searchModel)
        {
            var RegistrationFormatInformation = new RegistrationFormatInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).LastOrDefault();
                        break;
                    case "Next":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).LastOrDefault();
                        break;
                    case "Next":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderBy(o => o.RegistrationFormatInformationId).FirstOrDefault(s => s.RegistrationFormatInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        RegistrationFormatInformation = _context.RegistrationFormatInformation.OrderByDescending(o => o.RegistrationFormatInformationId).FirstOrDefault(s => s.RegistrationFormatInformationId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<RegistrationFormatInformationModel>(RegistrationFormatInformation);
            if (result != null)
            {
                var RegistrationFormatMultiple = _context.RegistrationFormatMultiple.AsNoTracking().ToList();
                result.IsCombinationOfOtherFlag = result.IsCombinationOfOtherFormat == true ? "1" : "0";
                result.RegistrationFormatListIds = RegistrationFormatMultiple.Where(p => p.RegistrationFormatInformationId == result.RegistrationFormatInformationId).Select(p => p.FormatId.Value).ToList();

            }
            return result;
        }
        [HttpPost]
        [Route("InsertRegistrationFormatInformation")]
        public RegistrationFormatInformationModel Post(RegistrationFormatInformationModel value)
        {
            var RegistrationFormatInformation = new RegistrationFormatInformation
            {
                FormatId = value.FormatId,
                ProductCategoryId = value.ProductCategoryId,
                TypeId = value.TypeId,
                VariationDescription = value.VariationDescription,
                VariationOnId = value.VariationOnId,
                VariationCategoryId = value.VariationCategoryId,
                VariationCategoryNoId = value.VariationCategoryNoId,
                IsCombinationOfOtherFormat = value.IsCombinationOfOtherFlag == "1" ? true : false,
                VariationCodeId = value.VariationCodeId,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.RegistrationFormatInformation.Add(RegistrationFormatInformation);
            if (value.RegistrationFormatListIds != null)
            {
                value.RegistrationFormatListIds.ForEach(c =>
                {
                    var RegistrationFormatListId = new RegistrationFormatMultiple
                    {
                        FormatId = c,
                    };
                    RegistrationFormatInformation.RegistrationFormatMultiple.Add(RegistrationFormatListId);
                });
            }
            _context.SaveChanges();
            value.RegistrationFormatInformationId = RegistrationFormatInformation.RegistrationFormatInformationId;
            return value;
        }
        [HttpPost]
        [Route("InsertRegistrationFormatInformationLine")]
        public RegistrationFormatInformationLineModel InsertRegistrationFormatInformationLine(RegistrationFormatInformationLineModel value)
        {
            var RegistrationFormatInformationLine = new RegistrationFormatInformationLine
            {
                RegistrationFormatInformationId = value.RegistrationFormatInformationId,
                PartNameId = value.PartNameId,
                SectionNameId = value.SectionNameId,
                Description = value.Description,
                Numbering = value.Numbering,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                FieldTypeId = value.FieldTypeId,
            };
            _context.RegistrationFormatInformationLine.Add(RegistrationFormatInformationLine);
            _context.SaveChanges();
            value.RegistrationFormatInformationLineId = RegistrationFormatInformationLine.RegistrationFormatInformationLineId;
            if (value.FieldTypeId > 0)
            {
                value.FieldType = _context.CodeMaster.Where(t => t.CodeId == value.FieldTypeId)?.FirstOrDefault()?.CodeValue;
            }
            CreateItemClassificationAccess(value);
            return value;
        }
        private void CreateItemClassificationAccess(RegistrationFormatInformationLineModel value)
        {
            var registrationFormatInformationLineUsers = new RegistrationFormatInformationLineUsers();
            var userList = value.RegistrationFormatInformationLineUsersModels.UserIDs.Distinct().ToList();
            var userGroupUserList = _context.UserGroupUser.Where(u => value.RegistrationFormatInformationLineUsersModels.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.RegistrationFormatInformationLineUsersModels.UserGroupIDs.Distinct().ToList();

            if ((userList != null) && (userList.Count > 0))
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    userList.ForEach(u =>
                    {
                        var userExisting = _context.RegistrationFormatInformationLineUsers.Where(d => d.UserId == u && d.RegistrationFormatInformationLineId == value.RegistrationFormatInformationLineId).FirstOrDefault();
                        if (userExisting == null)
                        {
                            registrationFormatInformationLineUsers = new RegistrationFormatInformationLineUsers
                            {
                                UserId = u,
                                RegistrationFormatInformationLineId = value.RegistrationFormatInformationLineId,
                                RoleId = value.RegistrationFormatInformationLineUsersModels.RoleId,
                            };
                            _context.RegistrationFormatInformationLineUsers.Add(registrationFormatInformationLineUsers);
                            _context.SaveChanges();
                        }
                    });

                }
            }
            else if ((userGroupList != null) && (userGroupList.Count > 0))
            {
                userGroupList.ForEach(ug =>
                {
                    List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                    currentGroupUsers.ForEach(c =>
                    {
                        var userExist = _context.RegistrationFormatInformationLineUsers.Where(du => du.RegistrationFormatInformationLineId == value.RegistrationFormatInformationLineId && du.UserId == c.UserId).ToList();
                        if (userExist.Count == 0)
                        {
                            registrationFormatInformationLineUsers = new RegistrationFormatInformationLineUsers
                            {
                                UserId = c.UserId,
                                RoleId = value.RegistrationFormatInformationLineUsersModels.RoleId,
                                UserGroupId = ug,
                                RegistrationFormatInformationLineId = value.RegistrationFormatInformationLineId,
                            };
                            _context.RegistrationFormatInformationLineUsers.Add(registrationFormatInformationLineUsers);
                            _context.SaveChanges();
                        }
                    });
                });

            }

        }
        [HttpPut]
        [Route("UpdateRegistrationFormatInformation")]
        public RegistrationFormatInformationModel Put(RegistrationFormatInformationModel value)
        {
            var RegistrationFormatInformation = _context.RegistrationFormatInformation.SingleOrDefault(p => p.RegistrationFormatInformationId == value.RegistrationFormatInformationId);
            RegistrationFormatInformation.FormatId = value.FormatId;
            RegistrationFormatInformation.ProductCategoryId = value.ProductCategoryId;
            RegistrationFormatInformation.TypeId = value.TypeId;
            RegistrationFormatInformation.VariationDescription = value.VariationDescription;
            RegistrationFormatInformation.VariationOnId = value.VariationOnId;
            RegistrationFormatInformation.VariationCategoryId = value.VariationCategoryId;
            RegistrationFormatInformation.VariationCategoryNoId = value.VariationCategoryNoId;
            RegistrationFormatInformation.IsCombinationOfOtherFormat = value.IsCombinationOfOtherFlag == "1" ? true : false;
            RegistrationFormatInformation.VariationCodeId = value.VariationCodeId;
            RegistrationFormatInformation.ModifiedByUserId = value.ModifiedByUserID;
            RegistrationFormatInformation.ModifiedDate = DateTime.Now;
            RegistrationFormatInformation.StatusCodeId = value.StatusCodeID.Value;
            var RegistrationFormatMultipleRemove = _context.RegistrationFormatMultiple.Where(l => l.RegistrationFormatInformationId == value.RegistrationFormatInformationId).ToList();
            if (RegistrationFormatMultipleRemove.Count > 0)
            {
                _context.RegistrationFormatMultiple.RemoveRange(RegistrationFormatMultipleRemove);
            }
            if (value.RegistrationFormatListIds != null)
            {
                value.RegistrationFormatListIds.ForEach(c =>
                {
                    var RegistrationFormatListId = new RegistrationFormatMultiple
                    {
                        FormatId = c,
                    };
                    RegistrationFormatInformation.RegistrationFormatMultiple.Add(RegistrationFormatListId);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateApplicationFormAccess")]
        public RegistrationFormatInformationLineUsersModel UpdateItemClassificationAccess(RegistrationFormatInformationLineUsersModel value)
        {
            var itemAccess = _context.RegistrationFormatInformationLineUsers.Where(i => i.RegistrationFormatInformationLineUsersId == value.RegistrationFormatInformationLineUsersId).FirstOrDefault();
            if (itemAccess != null)
            {
                itemAccess.RoleId = value.RoleId;
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateRegistrationFormatInformationLine")]
        public RegistrationFormatInformationLineModel UpdateRegistrationFormatInformationLine(RegistrationFormatInformationLineModel value)
        {
            var RegistrationFormatInformationLine = _context.RegistrationFormatInformationLine.SingleOrDefault(p => p.RegistrationFormatInformationLineId == value.RegistrationFormatInformationLineId);
            RegistrationFormatInformationLine.RegistrationFormatInformationId = value.RegistrationFormatInformationId;
            RegistrationFormatInformationLine.PartNameId = value.PartNameId;
            RegistrationFormatInformationLine.SectionNameId = value.SectionNameId;
            RegistrationFormatInformationLine.Description = value.Description;
            RegistrationFormatInformationLine.Numbering = value.Numbering;
            RegistrationFormatInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            RegistrationFormatInformationLine.ModifiedDate = DateTime.Now;
            RegistrationFormatInformationLine.StatusCodeId = value.StatusCodeID.Value;
            RegistrationFormatInformationLine.FieldTypeId = value.FieldTypeId;
            _context.SaveChanges();
            if (value.FieldTypeId > 0)
            {
                value.FieldType = _context.CodeMaster.Where(t => t.CodeId == value.FieldTypeId)?.FirstOrDefault()?.CodeValue;
            }
            CreateItemClassificationAccess(value);
            return value;
        }
        [HttpPut]
        [Route("DeleteApplicationFormAccess")]
        public RegistrationFormatInformationLineUsersModel DeleteItemClassificationAccess(RegistrationFormatInformationLineUsersModel value)
        {

            var selectItemClassificationAccessIDs = value.ItemClassificationAccessIDs;
            if (selectItemClassificationAccessIDs.Count > 0)
            {
                selectItemClassificationAccessIDs.ForEach(sel =>
                {
                    var itemAccess = _context.RegistrationFormatInformationLineUsers.Where(p => p.RegistrationFormatInformationLineUsersId == sel).FirstOrDefault();
                    if (itemAccess != null)
                    {
                        _context.RegistrationFormatInformationLineUsers.Remove(itemAccess);
                        _context.SaveChanges();
                    }
                });

                _context.SaveChanges();
            }


            return value;
        }
        [HttpDelete]
        [Route("DeleteRegistrationFormatInformationLine")]
        public void DeleteRegistrationFormatInformationLine(int id)
        {
            var RegistrationFormatInformationLine = _context.RegistrationFormatInformationLine.SingleOrDefault(p => p.RegistrationFormatInformationLineId == id);
            if (RegistrationFormatInformationLine != null)
            {
                var RegistrationFormatInformationLineUsers = _context.RegistrationFormatInformationLineUsers.Where(s => s.RegistrationFormatInformationLineId == id).AsNoTracking().ToList();
                if (RegistrationFormatInformationLineUsers != null)
                {
                    _context.RegistrationFormatInformationLineUsers.RemoveRange(RegistrationFormatInformationLineUsers);
                    _context.SaveChanges();
                }
                _context.RegistrationFormatInformationLine.Remove(RegistrationFormatInformationLine);
                _context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("DeleteRegistrationFormatInformation")]
        public void Delete(int id)
        {
            var RegistrationFormatInformation = _context.RegistrationFormatInformation.SingleOrDefault(p => p.RegistrationFormatInformationId == id);
            if (RegistrationFormatInformation != null)
            {
                var RegistrationFormatMultiple = _context.RegistrationFormatMultiple.Where(s => s.RegistrationFormatInformationId == id).AsNoTracking().ToList();
                if (RegistrationFormatMultiple != null)
                {
                    _context.RegistrationFormatMultiple.RemoveRange(RegistrationFormatMultiple);
                    _context.SaveChanges();
                }
                var RegistrationFormatInformationLine = _context.RegistrationFormatInformationLine.Where(s => s.RegistrationFormatInformationId == id).AsNoTracking().ToList();
                if (RegistrationFormatInformationLine != null)
                {
                    RegistrationFormatInformationLine.ForEach(f =>
                    {
                        var RegistrationFormatInformationLineUsers = _context.RegistrationFormatInformationLineUsers.Where(p => p.RegistrationFormatInformationLineId == f.RegistrationFormatInformationLineId).ToList();
                        _context.RegistrationFormatInformationLineUsers.RemoveRange(RegistrationFormatInformationLineUsers);
                        _context.SaveChanges();
                    });
                    _context.RegistrationFormatInformationLine.RemoveRange(RegistrationFormatInformationLine);
                    _context.SaveChanges();
                }
                _context.RegistrationFormatInformation.Remove(RegistrationFormatInformation);
                _context.SaveChanges();
            }
        }
    }
}