﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class GenericItemNameListingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public GenericItemNameListingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetGenericItemNameListing")]
        public List<GenericItemNameListingModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var GenericItemNameListing = _context.GenericItemNameListing.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<GenericItemNameListingModel> GenericItemNameListingModel = new List<GenericItemNameListingModel>();
            GenericItemNameListing.ForEach(s =>
            {
                GenericItemNameListingModel GenericItemNameListingModels = new GenericItemNameListingModel
                {
                    GenericItemNameListingId = s.GenericItemNameListingId,
                    GenericName = s.GenericName,
                    AlsoKownAs = s.AlsoKownAs,
                    ItemClassificationId = s.ItemClassificationId,
                    ItemClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ItemClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ItemClassificationId).Value : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                };
                GenericItemNameListingModel.Add(GenericItemNameListingModels);
            });
            return GenericItemNameListingModel.OrderByDescending(o => o.GenericItemNameListingId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<GenericItemNameListingModel> GetData(SearchModel searchModel)
        {
            var GenericItemNameListing = new GenericItemNameListing();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).FirstOrDefault();
                        break;
                    case "Last":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).LastOrDefault();
                        break;
                    case "Next":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).LastOrDefault();
                        break;
                    case "Previous":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).FirstOrDefault();
                        break;
                    case "Last":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).LastOrDefault();
                        break;
                    case "Next":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderBy(o => o.GenericItemNameListingId).FirstOrDefault(s => s.GenericItemNameListingId > searchModel.Id);
                        break;
                    case "Previous":
                        GenericItemNameListing = _context.GenericItemNameListing.OrderByDescending(o => o.GenericItemNameListingId).FirstOrDefault(s => s.GenericItemNameListingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<GenericItemNameListingModel>(GenericItemNameListing);
            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
                result.ItemClassificationName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.ItemClassificationId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == result.ItemClassificationId).Value : "";
            }
            return result;
        }
        [HttpPost]
        [Route("InsertGenericItemNameListing")]
        public GenericItemNameListingModel Post(GenericItemNameListingModel value)
        {
            var GenericItemNameListing = new GenericItemNameListing
            {
                AlsoKownAs = value.AlsoKownAs,
                GenericName = value.GenericName,
                ItemClassificationId = value.ItemClassificationId.Value,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.GenericItemNameListing.Add(GenericItemNameListing);
            _context.SaveChanges();
            value.GenericItemNameListingId = GenericItemNameListing.GenericItemNameListingId;
            return value;
        }
        [HttpPut]
        [Route("UpdateGenericItemNameListing")]
        public GenericItemNameListingModel Put(GenericItemNameListingModel value)
        {

            var GenericItemNameListing = _context.GenericItemNameListing.SingleOrDefault(p => p.GenericItemNameListingId == value.GenericItemNameListingId);
            GenericItemNameListing.AlsoKownAs = value.AlsoKownAs;
            GenericItemNameListing.GenericName = value.GenericName;
            GenericItemNameListing.ItemClassificationId = value.ItemClassificationId;
            GenericItemNameListing.ModifiedByUserId = value.ModifiedByUserID;
            GenericItemNameListing.StatusCodeId = value.StatusCodeID.Value;
            GenericItemNameListing.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteGenericItemNameListing")]
        public ActionResult<string> Delete(int id)
        {
            try
            {

                var GenericItemNameListing = _context.GenericItemNameListing.SingleOrDefault(p => p.GenericItemNameListingId == id);
                if (GenericItemNameListing != null)
                {
                    var ProductionMaterial = _context.ProductionMaterial.SingleOrDefault(p => p.GenericItemNameListingId == id);
                    if (ProductionMaterial != null)
                    {
                        var ProductionFuntionOfMaterialItems = _context.ProductionFuntionOfMaterialItems.Where(s => s.ProdutionMaterialId == ProductionMaterial.ProductionMaterialId).ToList();
                        if (ProductionFuntionOfMaterialItems != null)
                        {
                            _context.ProductionFuntionOfMaterialItems.RemoveRange(ProductionFuntionOfMaterialItems);
                            _context.SaveChanges();
                        }
                        var ProductionMaterialIngredientItems = _context.ProductionMaterialIngredientItems.Where(s => s.ProductionMaterialId == ProductionMaterial.ProductionMaterialId).ToList();
                        if (ProductionMaterialIngredientItems != null)
                        {
                            _context.ProductionMaterialIngredientItems.RemoveRange(ProductionMaterialIngredientItems);
                            _context.SaveChanges();
                        }
                        var ProductionMaterialLine = _context.ProductionMaterialLine.Where(s => s.ProductionMaterialId == ProductionMaterial.ProductionMaterialId).ToList();
                        if (ProductionMaterialLine != null)
                        {
                            _context.ProductionMaterialLine.RemoveRange(ProductionMaterialLine);
                            _context.SaveChanges();
                        }
                        _context.ProductionMaterial.Remove(ProductionMaterial);
                        _context.SaveChanges();
                    }
                    _context.GenericItemNameListing.Remove(GenericItemNameListing);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}