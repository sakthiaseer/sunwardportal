﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackingInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CommonPackingInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetCommonPackingInformation")]
        public List<CommonPackingInformationModel> CommonPackingInformation(int? id)
        {
            var commonPackingInfoOptionallist = _context.CommonPackingInfoOptional.AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var commonPackingInformation = _context.CommonPackingInformation.AsNoTracking().ToList();
            List<CommonPackingInformationModel> commonPackingInformationModel = new List<CommonPackingInformationModel>();
            commonPackingInformation.ForEach(s =>
            {
                CommonPackingInformationModel commonPackingInformationModels = new CommonPackingInformationModel
                {
                    PackingInformationId = s.PackingInformationId,
                    SetInformationId = s.SetInformationId,
                    PackagingMethodId = s.PackagingMethodId,
                    PackagingMethodName = s.PackagingMethodId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMethodId).Select(m => m.Value).FirstOrDefault() : "",
                    PackagingItemSetInfoId = commonPackingInfoOptionallist.Where(l => l.PackingInformationId == s.PackingInformationId).Select(l => l.SetInformationId.Value).ToList(),
                };
                commonPackingInformationModel.Add(commonPackingInformationModels);
            });
            return commonPackingInformationModel.Where(w => w.SetInformationId == id).OrderByDescending(a => a.PackingInformationId).ToList();
        }
        [HttpGet]
        [Route("GetCommonPackingInformationLine")]
        public List<CommonPackingInformationLineModel> CommonPackingInformationLine(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var commonPackingInformationLine = _context.CommonPackingInformationLine.
                Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(p => p.PackagingItemName)
                .Include(m => m.PackagingMethod).ToList();
            List<CommonPackingInformationLineModel> commonPackingInformationLineModel = new List<CommonPackingInformationLineModel>();
            commonPackingInformationLine.ForEach(s =>
            {
                CommonPackingInformationLineModel commonPackingInformationLineModels = new CommonPackingInformationLineModel
                {
                    PackingInformationLineId = s.PackingInformationLineId,
                    PackingInformationId = s.PackingInformationId,
                    PackagingItemCategoryId = s.PackagingItemCategoryId,
                    PackagingItemNameId = s.PackagingItemNameId,
                    NoOfUnits = s.NoOfUnits,
                    UomId = s.UomId,
                    Link = s.Link,
                    NameOfPackingMethod = s.NameOfPackingMethod,
                    PackagingMethodId = s.PackagingMethodId,
                    UnitsPackingContent = s.UnitsPackingContent,
                    PackagingItemName = s.PackagingItemName != null ? s.PackagingItemName.PackagingItemName : "",
                    PackagingMethodName = s.PackagingMethod != null ? s.PackagingMethod.Name : "",
                    UomName = s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : "",
                    PackagingItemCategoryName = s.PackagingItemCategoryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemCategoryId).Select(m => m.Value).FirstOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                };
                commonPackingInformationLineModel.Add(commonPackingInformationLineModels);
            });
            return commonPackingInformationLineModel.Where(w => w.PackingInformationId == id).OrderByDescending(a => a.PackingInformationLineId).ToList();
        }
        [HttpPost]
        [Route("InsertCommonPackingInformation")]
        public CommonPackingInformationModel Post(CommonPackingInformationModel value)
        {
            var commonPackingInfo = new CommonPackingInformation
            {
                SetInformationId = value.SetInformationId,
                PackagingMethodId = value.PackagingMethodId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            if (value.PackagingItemSetInfoId != null)
            {
                value.PackagingItemSetInfoId.ForEach(c =>
                {
                    var setInfoId = new CommonPackingInfoOptional
                    {
                        SetInformationId = c,
                    };
                    commonPackingInfo.CommonPackingInfoOptional.Add(setInfoId);
                });
            }
            _context.CommonPackingInformation.Add(commonPackingInfo);
            _context.SaveChanges();
            value.PackingInformationId = commonPackingInfo.PackingInformationId;
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            value.PackagingMethodName = value.PackagingMethodId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMethodId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonPackingInformation")]
        public CommonPackingInformationModel Put(CommonPackingInformationModel value)
        {
            var commonPackingInfo = _context.CommonPackingInformation.SingleOrDefault(p => p.PackingInformationId == value.PackingInformationId);
            commonPackingInfo.SetInformationId = value.SetInformationId;
            commonPackingInfo.PackagingMethodId = value.PackagingMethodId;
            commonPackingInfo.StatusCodeId = value.StatusCodeID.Value;
            commonPackingInfo.ModifiedByUserId = value.ModifiedByUserID;
            commonPackingInfo.ModifiedDate = DateTime.Now;
            var setInfoIds = _context.CommonPackingInfoOptional.Where(l => l.PackingInformationId == value.PackingInformationId).ToList();
            if (setInfoIds.Count > 0)
            {
                _context.CommonPackingInfoOptional.RemoveRange(setInfoIds);
            }
            if (value.PackagingItemSetInfoId != null)
            {
                value.PackagingItemSetInfoId.ForEach(c =>
                {
                    var setInfoId = new CommonPackingInfoOptional
                    {
                        SetInformationId = c,
                    };
                    commonPackingInfo.CommonPackingInfoOptional.Add(setInfoId);
                });
            }
            _context.SaveChanges();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            value.PackagingMethodName = value.PackagingMethodId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMethodId).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }
        [HttpPost]
        [Route("InsertCommonPackingInformationLine")]
        public CommonPackingInformationLineModel InsertCommonPackingInformationLine(CommonPackingInformationLineModel value)
        {
            var commonPackingInfoLine = new CommonPackingInformationLine
            {
                PackingInformationId = value.PackingInformationId,
                PackagingItemCategoryId = value.PackagingItemCategoryId,
                PackagingItemNameId = value.PackagingItemNameId,
                UomId = value.UomId,
                NoOfUnits = value.NoOfUnits,
                Link = value.Link,
                PackagingMethodId = value.PackagingMethodId,
                NameOfPackingMethod = value.NameOfPackingMethod,
                UnitsPackingContent = value.UnitsPackingContent,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackingInformationLine.Add(commonPackingInfoLine);
            _context.SaveChanges();
            value.PackingInformationLineId = commonPackingInfoLine.PackingInformationLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonPackingInformationLine")]
        public CommonPackingInformationLineModel UpdateCommonPackingInformationLine(CommonPackingInformationLineModel value)
        {
            var commonPackingInfoLine = _context.CommonPackingInformationLine.SingleOrDefault(p => p.PackingInformationLineId == value.PackingInformationLineId);
            commonPackingInfoLine.PackingInformationId = value.PackingInformationId;
            commonPackingInfoLine.PackagingItemCategoryId = value.PackagingItemCategoryId;
            commonPackingInfoLine.PackagingItemNameId = value.PackagingItemNameId;
            commonPackingInfoLine.UomId = value.UomId;
            commonPackingInfoLine.NoOfUnits = value.NoOfUnits;
            commonPackingInfoLine.Link = value.Link;
            commonPackingInfoLine.PackagingMethodId = value.PackagingMethodId;
            commonPackingInfoLine.NameOfPackingMethod = value.NameOfPackingMethod;
            commonPackingInfoLine.UnitsPackingContent = value.UnitsPackingContent;
            commonPackingInfoLine.StatusCodeId = value.StatusCodeID.Value;
            commonPackingInfoLine.ModifiedByUserId = value.ModifiedByUserID;
            commonPackingInfoLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackingInformationLine")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackingInformationLine = _context.CommonPackingInformationLine.Where(p => p.PackingInformationLineId == id).FirstOrDefault();
                if (commonPackingInformationLine != null)
                {
                    _context.CommonPackingInformationLine.Remove(commonPackingInformationLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}