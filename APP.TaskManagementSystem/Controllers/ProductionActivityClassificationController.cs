﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActivityClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public ProductionActivityClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetProductionActivityClassification")]
        public List<ProductionActivityClassificationModel> Get(int id)
        {
            var productionActivityClassification = _context.ProductionActivityClassification.
               Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(s => s.StatusCode)
               .Include(p=>p.Profile)
               .AsNoTracking().ToList();
            List<ProductionActivityClassificationModel> productionActivityClassificationModels = new List<ProductionActivityClassificationModel>();
            productionActivityClassification.ForEach(s =>
            {
                ProductionActivityClassificationModel productionActivityClassificationModel = new ProductionActivityClassificationModel
                {
                    ProductionActivityClassificationId = s.ProductionActivityClassificationId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    StatusCodeID = s.StatusCodeId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    SessionId=s.SessionId,
                    ReplanNo=s.ReplanNo,
                    ItemDescription=s.ItemDescription,
                    BatchNo=s.BatchNo,
                    BatchSize=s.BatchSize,
                    Uom=s.Uom,
                    Pv=s.Pv
                };
                productionActivityClassificationModels.Add(productionActivityClassificationModel);
            });
            return productionActivityClassificationModels.Where(w => w.ItemClassificationMasterId == id).OrderByDescending(a => a.ProductionActivityClassificationId).ToList();
        }
        [HttpPost]
        [Route("InsertProductionActivityClassification")]
        public ProductionActivityClassificationModel Post(ProductionActivityClassificationModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var sessionId = value.SessionId ?? Guid.NewGuid();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var productionActivityClassification = new ProductionActivityClassification
            {
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
                SessionId= sessionId,
                ReplanNo = value.ReplanNo,
                ItemDescription = value.ItemDescription,
                BatchNo = value.BatchNo,
                BatchSize = value.BatchSize,
                Uom = value.Uom,
                Pv = value.Pv
            };
            _context.ProductionActivityClassification.Add(productionActivityClassification);
            _context.SaveChanges();
            value.ProductionActivityClassificationId = productionActivityClassification.ProductionActivityClassificationId;
            value.ProfileReferenceNo = productionActivityClassification.ProfileReferenceNo;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityClassification")]
        public ProductionActivityClassificationModel Put(ProductionActivityClassificationModel value)
        {
            var productionActivityClassification = _context.ProductionActivityClassification.SingleOrDefault(p => p.ProductionActivityClassificationId == value.ProductionActivityClassificationId);
            productionActivityClassification.ReplanNo = value.ReplanNo;
            productionActivityClassification.ItemDescription = value.ItemDescription;
            productionActivityClassification.BatchNo = value.BatchNo;
            productionActivityClassification.BatchSize = value.BatchSize;
            productionActivityClassification.Uom = value.Uom;
            productionActivityClassification.Pv = value.Pv;
            productionActivityClassification.StatusCodeId = value.StatusCodeID.Value;
            productionActivityClassification.ModifiedByUserId = value.ModifiedByUserID;
            productionActivityClassification.ModifiedDate = DateTime.Now;
            productionActivityClassification.ProfileReferenceNo = value.ProfileReferenceNo;
            productionActivityClassification.ProfileId = value.ProfileId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityClassification")]
        public ActionResult<string> ProductionActivityClassification(int id)
        {
            try
            {
                var productionActivityClassification = _context.ProductionActivityClassification.Where(p => p.ProductionActivityClassificationId == id).FirstOrDefault();
                if (productionActivityClassification != null)
                {
                    _context.ProductionActivityClassification.Remove(productionActivityClassification);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
