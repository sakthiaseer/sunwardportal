﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingSetInfoController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CommonPackagingSetInfoController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetCommonPackagingSetInfo")]
        public List<CommonPackagingSetInfoModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var documentProfileNoSeries = _context.DocumentProfileNoSeries.ToList();
            var commonPackagingSetInfo = _context.CommonPackagingSetInfo
            .Include(a => a.AddedByUser)
            .Include(m => m.ModifiedByUser)
             .Include(p => p.PackagingMaterial)
            .Include(s => s.StatusCode)
            .AsNoTracking().ToList();
            List<CommonPackagingSetInfoModel> commonPackagingSetInfoModel = new List<CommonPackagingSetInfoModel>();
            commonPackagingSetInfo.ForEach(s =>
            {
                CommonPackagingSetInfoModel commonPackagingSetInfoModels = new CommonPackagingSetInfoModel
                {
                    SetInformationId = s.SetInformationId,
                    PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                    Set = s.Set,
                    PackagingMaterialId = s.PackagingMaterialId,
                    PackagingMaterialName = s.PackagingMaterial != null ? s.PackagingMaterial.PackagingItemName : "",
                    PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    PackagingMaterialNo = s.PackagingMaterial.ProfileId != 0 && documentProfileNoSeries.Count > 0 ? documentProfileNoSeries.Where(m => m.ProfileId == s.PackagingMaterial.ProfileId).Select(m => m.Name).FirstOrDefault() : "",
                    UomName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterial.UomId).Select(m => m.Value).FirstOrDefault() : "",

                };
                commonPackagingSetInfoModel.Add(commonPackagingSetInfoModels);
            });
            return commonPackagingSetInfoModel.OrderByDescending(a => a.SetInformationId).ToList();
        }
        [HttpGet]
        [Route("GetCommonPackagingSetInfoByRefNo")]
        public List<CommonPackagingSetInfoModel> GetCommonPackagingSetInfoByRefNo(RefSearchModel refSearchModel)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var documentProfileNoSeries = _context.DocumentProfileNoSeries.ToList();
            var commonPackagingSetInfo = _context.CommonPackagingSetInfo
            .Include(a => a.AddedByUser)
            .Include(m => m.ModifiedByUser)
             .Include(p => p.PackagingMaterial)
            .Include(s => s.StatusCode)
            .AsNoTracking().ToList();
            List<CommonPackagingSetInfoModel> commonPackagingSetInfoModel = new List<CommonPackagingSetInfoModel>();
            commonPackagingSetInfo.ForEach(s =>
            {
                CommonPackagingSetInfoModel commonPackagingSetInfoModels = new CommonPackagingSetInfoModel
                {
                    SetInformationId = s.SetInformationId,
                    PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                    Set = s.Set,
                    PackagingMaterialId = s.PackagingMaterialId,
                    PackagingMaterialName = s.PackagingMaterial != null ? s.PackagingMaterial.PackagingItemName : "",
                    PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    PackagingMaterialNo = s.PackagingMaterial.ProfileId != 0 && documentProfileNoSeries.Count > 0 ? documentProfileNoSeries.Where(m => m.ProfileId == s.PackagingMaterial.ProfileId).Select(m => m.Name).FirstOrDefault() : "",
                    UomName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterial.UomId).Select(m => m.Value).FirstOrDefault() : "",
                };
                commonPackagingSetInfoModel.Add(commonPackagingSetInfoModels);
            });
            if (refSearchModel.IsHeader)
            {
                return commonPackagingSetInfoModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.SetInformationId).ToList();
            }
            return commonPackagingSetInfoModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.SetInformationId).ToList();
        }
        [HttpGet]
        [Route("GetCommonPackagingItemCategory")]
        public List<CommonPackagingItemHeaderModel> GetCommonPackagingItemCategory(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var commonPackagingItemHeaderList = _context.CommonPackagingItemHeader
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode)
                .AsNoTracking().ToList();
            List<CommonPackagingItemHeaderModel> commonPackagingItemHeaderModel = new List<CommonPackagingItemHeaderModel>();
            commonPackagingItemHeaderList.ForEach(s =>
            {
                CommonPackagingItemHeaderModel commonPackagingItemHeaderModels = new CommonPackagingItemHeaderModel
                {
                    CommonPackagingItemId = s.CommonPackagingItemId,
                    PackagingItemName = s.PackagingItemName,
                    PackagingItemCategoryId = s.PackagingItemCategoryId,
                    UomId = s.UomId,
                    UomName = s.UomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.UomId).Select(m => m.Value).FirstOrDefault() : "",
                };
                commonPackagingItemHeaderModel.Add(commonPackagingItemHeaderModels);
            });
            return commonPackagingItemHeaderModel.Where(w => w.PackagingItemCategoryId == id).OrderByDescending(a => a.CommonPackagingItemId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingSetInfoModel> GetData(SearchModel searchModel)
        {
            var commonPackagingSetInfo = new CommonPackagingSetInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderBy(o => o.SetInformationId).FirstOrDefault(s => s.SetInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingSetInfo = _context.CommonPackagingSetInfo.OrderByDescending(o => o.SetInformationId).FirstOrDefault(s => s.SetInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingSetInfoModel>(commonPackagingSetInfo);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingSetInfo")]
        public CommonPackagingSetInfoModel Post(CommonPackagingSetInfoModel value)
        {
            var commonPackagingSetInfo = new CommonPackagingSetInfo
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                PackagingMaterialId = value.PackagingMaterialId,
                ProfileLinkReferenceNo = value.ProfileLinkReferenceNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? "" : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingSetInfo.Add(commonPackagingSetInfo);
            _context.SaveChanges();
            value.SetInformationId = commonPackagingSetInfo.SetInformationId;
            value.MasterProfileReferenceNo = commonPackagingSetInfo.MasterProfileReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingSetInfo")]
        public CommonPackagingSetInfoModel Put(CommonPackagingSetInfoModel value)
        {
            var commonPackagingSetInfo = _context.CommonPackagingSetInfo.SingleOrDefault(p => p.SetInformationId == value.SetInformationId);
            commonPackagingSetInfo.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingSetInfo.Set = value.Set;
            commonPackagingSetInfo.PackagingMaterialId = value.PackagingMaterialId;
            commonPackagingSetInfo.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingSetInfo.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingSetInfo.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingSetInfo.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingSetInfo.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingSetInfo")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingSetInfo = _context.CommonPackagingSetInfo.Where(p => p.SetInformationId == id).FirstOrDefault();
                if (commonPackagingSetInfo != null)
                {
                    _context.CommonPackagingSetInfo.Remove(commonPackagingSetInfo);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}