﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class EmployeeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public EmployeeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetEmployees")]
        public List<EmployeeModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employee = _context.Employee
               .Include(a => a.User).
                Include(d => d.Level).
                Include(f => f.Designation).
                Include(g => g.Language).
                Include(h => h.City).
                Include(i => i.Region)
                .Include(s => s.EmployeeReportTo)
                .Include(s => s.User.ApplicationUserRole)
                .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Include(n => n.ApplicationUser)
                .Include(o => o.AddedByUser)
                .Include(p => p.ModifiedByUser)
                .Include(q => q.StatusCode).AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.UserID = s.UserId;
                employeeModel.SageID = s.SageId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.LevelID = s.LevelId;
                employeeModel.LanguageID = s.LanguageId;
                employeeModel.UserCode = s.User != null ? s.User.UserCode : "";
                employeeModel.CityID = s.CityId;
                employeeModel.RegionID = s.RegionId;
                employeeModel.ReportID = s.ReportId;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.Email = s.Email;
                employeeModel.LoginID = s.User != null ? s.User.LoginId : "";
                employeeModel.LoginPassword = EncryptDecryptPassword.Decrypt(s.User.LoginPassword);
                employeeModel.UserGroupID = s.User != null ? s.User.UserGroupUser.Where(a => a.UserId == s.UserId).Select(t => t.UserGroupId).FirstOrDefault() : 0;
                employeeModel.RoleID = s.User?.ApplicationUserRole?.Where(a => a.UserId == s.UserId).Select(t => t.RoleId).FirstOrDefault();
                employeeModel.TypeOfEmployeement = s.TypeOfEmployeement;
                employeeModel.ReportToIDs = s.EmployeeReportTo?.Where(r => r.EmployeeId == s.EmployeeId).Select(r => r.ReportToId).ToList();
                employeeModel.Signature = s.Signature;
                employeeModel.ImageUrl = s.ImageUrl;
                employeeModel.DateOfEmployeement = s.DateOfEmployeement;
                employeeModel.LastWorkingDate = s.LastWorkingDate;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.Mobile = s.Mobile;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModel.IsActive = s.IsActive;
                employeeModel.SectionID = s.SectionId;
                employeeModel.SectionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Name;
                employeeModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                employeeModel.DivisionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.SubSectionName = s.Designation?.SubSectionT?.SubSecton?.Name;
                employeeModel.SubSectionTwoName = s.Designation?.SubSectionT?.Name;
                //SectionName = s.Section != null ? s.Section.Name : "",
                employeeModel.DivisionID = s.DivisionId;
                //DivisionName = s.Division != null ? s.Division.Name : "",
                employeeModel.DesignationID = s.DesignationId;
                employeeModel.DepartmentID = s.DepartmentId;
                employeeModel.SubSectionID = s.SubSectionId;
                //SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                employeeModel.SubSectionTID = s.SubSectionTid;
                //SubSectionTwoName = s.SubSectionT != null ? s.SubSectionT.Name : "",
                //Drop Down Names
                employeeModel.UserName = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.PlantName = s.Plant != null ? s.Plant.PlantCode : "";
                employeeModel.LevelName = s.Level != null ? s.Level.Name : "";
                //DepartmentName = s.DepartmentNavigation != null ? s.DepartmentNavigation.Name : "",
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.LanguageName = s.Language != null ? s.Language.Name : "";
                employeeModel.CityName = s.City != null ? s.City.Name : "";
                employeeModel.RegionName = s.Region != null ? s.Region.Name : "";
                //ReportName = s.Report != null ? s.Report.UserName : "",
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                employeeModel.AddedByUserID = s.AddedByUserId;
                employeeModel.ModifiedByUserID = s.ModifiedByUserId;
                employeeModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                employeeModel.AddedDate = s.AddedDate;
                employeeModel.ModifiedDate = s.ModifiedDate;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.AcceptanceStatusDate = s.AcceptanceStatusDate;
                employeeModel.HeadCount = s.HeadCount;

                if (s.User.StatusCodeId == 2)
                {
                    employeeModel.isEnableLogin = false;
                }
                else if (s.User.StatusCodeId == 1)
                {
                    employeeModel.isEnableLogin = true;
                }
                employeeModel.Status = s.AcceptanceStatus != null && s.AcceptanceStatus != 0 && applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "";

                employeeModels.Add(employeeModel);
            });
            employeeModels.ForEach(e =>
            {
                e.DropDownName = e.DepartmentName + " | " + e.DesignationName + " | " + e.HeadCount;
            });
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByDepartent")]
        public List<EmployeeModel> GetEmployeesByDepartent(long? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employee = _context.Employee
               .Include(a => a.User).
                Include(d => d.Level).
                Include(f => f.Designation).
                Include(g => g.Language).
                Include(h => h.City).
                Include(i => i.Region)
                .Include(s => s.EmployeeReportTo)
                .Include(s => s.User.ApplicationUserRole)
                .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Include(n => n.ApplicationUser)
                .Include(o => o.AddedByUser)
                .Include(p => p.ModifiedByUser)
                .Include(q => q.StatusCode).Where(w => w.PlantId == id && w.Designation.SubSectionT.SubSecton.Section.Department.Name.ToLower() == "sales").AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.UserID = s.UserId;
                employeeModel.SageID = s.SageId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.LevelID = s.LevelId;
                employeeModel.LanguageID = s.LanguageId;
                employeeModel.UserCode = s.User != null ? s.User.UserCode : "";
                employeeModel.CityID = s.CityId;
                employeeModel.RegionID = s.RegionId;
                employeeModel.ReportID = s.ReportId;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.Email = s.Email;
                employeeModel.LoginID = s.User != null ? s.User.LoginId : "";
                employeeModel.Signature = s.Signature;
                employeeModel.ImageUrl = s.ImageUrl;
                employeeModel.DateOfEmployeement = s.DateOfEmployeement;
                employeeModel.LastWorkingDate = s.LastWorkingDate;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.Mobile = s.Mobile;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModel.IsActive = s.IsActive;
                employeeModel.SectionID = s.SectionId;
                employeeModel.SectionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Name;
                employeeModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                employeeModel.DivisionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.SubSectionName = s.Designation?.SubSectionT?.SubSecton?.Name;
                employeeModel.SubSectionTwoName = s.Designation?.SubSectionT?.Name;


                employeeModel.DepartmentID = s.DepartmentId;
                employeeModel.SubSectionID = s.SubSectionId;
                //SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                employeeModel.SubSectionTID = s.SubSectionTid;
                //SubSectionTwoName = s.SubSectionT != null ? s.SubSectionT.Name : "",
                //Drop Down Names
                employeeModel.UserName = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.PlantName = s.Plant != null ? s.Plant.PlantCode : "";
                employeeModel.LevelName = s.Level != null ? s.Level.Name : "";
                //DepartmentName = s.DepartmentNavigation != null ? s.DepartmentNavigation.Name : "",
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.LanguageName = s.Language != null ? s.Language.Name : "";
                employeeModel.CityName = s.City != null ? s.City.Name : "";
                employeeModel.RegionName = s.Region != null ? s.Region.Name : "";
                //ReportName = s.Report != null ? s.Report.UserName : "",
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                employeeModel.AddedByUserID = s.AddedByUserId;
                employeeModel.ModifiedByUserID = s.ModifiedByUserId;
                employeeModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                employeeModel.AddedDate = s.AddedDate;
                employeeModel.ModifiedDate = s.ModifiedDate;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.AcceptanceStatusDate = s.AcceptanceStatusDate;
                employeeModel.HeadCount = s.HeadCount;
                employeeModels.Add(employeeModel);
            });
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesCommonDropDown")]
        public List<EmployeeModel> GetEmployeesCommonDropDown()
        {
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            var employee = _context.Employee
             .AsNoTracking().ToList();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.DesignationID = s.DesignationId;
                employeeModel.SectionID = s.SectionId;
                employeeModel.SubSectionID = s.SubSectionId;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModels.Add(employeeModel);
            });

            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByStatus")]
        public List<EmployeeModel> GetEmployeesByStatus(string Name)
        {
            DateTime threeMonthsb4Date = DateTime.Now.AddMonths(-3);
            var applicationMasterIds = _context.ApplicationMaster.Where(f => f.ApplicationMasterCodeId == 226).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var employee = _context.Employee.
                Include(f => f.Designation)
                .Include(f => f.Plant)
                .Include(e => e.EmployeeResignation)
                .Include("EmployeeReportTo.ReportTo")
                .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
              .Include(n => n.ApplicationUser).Include(o => o.AddedByUser).Include(p => p.ModifiedByUser).Include(q => q.StatusCode).AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.UserID = s.UserId;
                employeeModel.SageID = s.SageId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.LevelID = s.LevelId;
                employeeModel.LanguageID = s.LanguageId;

                employeeModel.CityID = s.CityId;
                employeeModel.RegionID = s.RegionId;
                employeeModel.ReportID = s.ReportId;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.Email = s.Email;
                employeeModel.TypeOfEmployeement = s.TypeOfEmployeement;
                employeeModel.Signature = s.Signature;
                employeeModel.ImageUrl = s.ImageUrl;
                employeeModel.DateOfEmployeement = s.DateOfEmployeement;
                employeeModel.LastWorkingDate = s.LastWorkingDate;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.Mobile = s.Mobile;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModel.IsActive = s.IsActive;
                employeeModel.SectionID = s.SectionId;

                employeeModel.DivisionID = s.DivisionId;
                //DivisionName = s.Division != null ? s.Division.Name : "",
                employeeModel.DesignationID = s.DesignationId;
                employeeModel.DepartmentID = s.DepartmentId;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.SubSectionID = s.SubSectionId;
                //SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                employeeModel.SubSectionTID = s.SubSectionTid;
                //SubSectionTwoName = s.SubSectionT != null ? s.SubSectionT.Name : "",
                //Drop Down Names
                employeeModel.UserName = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.PlantName = s.Plant != null ? s.Plant.PlantCode : "";
                employeeModel.LevelName = s.Level != null ? s.Level.Name : "";
                //DepartmentName = s.DepartmentNavigation != null ? s.DepartmentNavigation.Name : "",
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                employeeModel.AddedByUserID = s.AddedByUserId;
                employeeModel.ModifiedByUserID = s.ModifiedByUserId;
                employeeModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                employeeModel.AddedDate = s.AddedDate;
                employeeModel.ModifiedDate = s.ModifiedDate;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.AcceptanceStatusDate = s.AcceptanceStatusDate;
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.Status = s.AcceptanceStatus != null && s.AcceptanceStatus != 0 && applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "";
                employeeModel.ReportName = s.EmployeeReportTo != null ? string.Join(",", s.EmployeeReportTo.Where(w => w.EmployeeId == s.EmployeeId).Select(a => a.ReportTo?.UserName).ToList()) : "";
                employeeModel.ResignAgreedLastDate = s.EmployeeResignation?.Where(e => e.EmployeeId == s.EmployeeId).FirstOrDefault()?.AgreedLastDate;
                employeeModels.Add(employeeModel);
            });
            employeeModels.ForEach(e =>
            {
                e.DropDownName = e.DepartmentName + " | " + e.DesignationName;
            });
            return employeeModels.Where(w => w.Status?.ToLower() == Name.ToLower() && w.ResignAgreedLastDate >= threeMonthsb4Date && w.ResignAgreedLastDate <= DateTime.Now).OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByWeek")]
        public List<EmployeeModel> GetEmployeesByWeek()
        {
            DateTime threeMonthsb4Date = DateTime.Now.AddMonths(-3);
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employee = _context.Employee.
                Include(u => u.User).
                Include(f => f.Designation)
                .Include(s => s.EmployeeReportTo)
                .Include(p => p.Plant)
                .Include(p => p.AddedByUser)
                .Include(p => p.ModifiedByUser)
                .Include("EmployeeReportTo.ReportTo")
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                //.Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                //.Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Include(q => q.StatusCode)
                .Where(w => w.ExpectedJoiningDate.Value.Date >= threeMonthsb4Date.Date && w.ExpectedJoiningDate.Value.Date <= DateTime.Now.Date && w.User.StatusCodeId == 1)
                .AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                var userIds = s.EmployeeReportTo.Where(w => w.EmployeeId == s.EmployeeId).Select(a => a.ReportTo?.UserId);
                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.DateOfEmployeement = s.DateOfEmployeement;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.PlantName = s.Plant != null ? s.Plant.PlantCode : "";
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.ReportName = s.EmployeeReportTo != null ? string.Join(",", s.EmployeeReportTo.Where(w => w.EmployeeId == s.EmployeeId).Select(a => a.ReportTo?.UserName).ToList()) : "";
                employeeModel.ModifiedByUser = s.ModifiedByUser == null ? s.AddedByUser.UserName : s.ModifiedByUser.UserName;
                employeeModel.ModifiedDate = s.ModifiedDate == null ? s.AddedDate : s.ModifiedDate;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.Email = s.Email;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModels.Add(employeeModel);
            });
            employeeModels.ForEach(e =>
            {
                e.DropDownName = e.DepartmentName + " | " + e.DesignationName;
            });
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByDropdown")]
        public List<EmployeeModel> GetEmployeesByDropdown()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employee = _context.Employee.Include(a => a.User).Include(p => p.Plant)
                .Include(f => f.Designation)
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
             .AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.UserID = s.UserId;
                employeeModel.FirstName = s.FirstName + " " + s.LastName + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "");
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                //SectionName = s.Section != null ? s.Section.Name : "",
                employeeModel.DivisionID = s.DivisionId;
                //DivisionName = s.Division != null ? s.Division.Name : "",
                employeeModel.DesignationID = s.DesignationId;
                employeeModel.DepartmentID = s.DepartmentId;
                employeeModel.SubSectionID = s.SubSectionId;
                //SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                employeeModel.SubSectionTID = s.SubSectionTid;
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.AcceptanceStatusDate = s.AcceptanceStatusDate;
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.Status = s.AcceptanceStatus != null && s.AcceptanceStatus != 0 && applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "";
                employeeModel.DivisionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                employeeModel.Name = employeeModel.CompanyName + '|' + employeeModel.DivisionName + '|' + employeeModel.DepartmentName + '|' + employeeModel.DesignationName + '|' + s.HeadCount;
                employeeModels.Add(employeeModel);
            });
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesSalesByDropdown")]
        public List<EmployeeModel> GetEmployeesSalesByDropdown(long? id)
        {
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            if (id != null)
            {
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where DepartmentName like '%Sales%' and PlantID=" + id;
                //sqlQuery = "Select  * from view_GetEmployee where PlantID=" + id;
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.Gender,
                    s.DepartmentName,
                    s.PlantID,
                }).ToList();

                employee.ForEach(s =>
                {
                    EmployeeModel employeeModel = new EmployeeModel();
                    employeeModel.EmployeeID = s.EmployeeID;
                    employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                    employeeModel.FirstName = s.FirstName;
                    employeeModel.NickName = s.NickName;
                    employeeModel.LastName = s.LastName;
                    employeeModel.Gender = s.Gender;
                    employeeModel.DepartmentName = s.DepartmentName;
                    employeeModels.Add(employeeModel);
                });
            }
            return employeeModels.ToList();
        }
        [HttpGet]
        [Route("GetEmployeesUserByDropdown")]
        public List<EmployeeModel> GetEmployeesUserByDropdown(long? id, string type)
        {
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            if (id != null && type != null)
            {
                string sqlQuery = string.Empty;
                if (type.ToLower() == "country")
                {
                    sqlQuery = "Select  * from view_GetEmployee where Status='Active' and PlantID=" + id;
                    //sqlQuery = "Select  * from view_GetEmployee where  PlantID=" + id;
                }
                if (type.ToLower() == "usergroup")
                {
                    var userGroupUser = _context.UserGroupUser.Where(w => w.UserGroupId == id).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                    sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", userGroupUser.Distinct().ToList()) + ")";
                    //sqlQuery = "Select  * from view_GetEmployee where  UserID in(" + string.Join(",", userGroupUser.Distinct().ToList()) + ")";

                }
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.Status,
                    s.EmployeeID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.Gender,
                    s.DepartmentName,
                    s.PlantID,
                    s.UserID
                }).ToList();

                employee.ForEach(s =>
                {
                    EmployeeModel employeeModel = new EmployeeModel();
                    employeeModel.EmployeeID = s.EmployeeID;
                    employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                    employeeModel.UserID = s.UserID;
                    employeeModel.FirstName = s.FirstName;
                    employeeModel.NickName = s.NickName;
                    employeeModel.LastName = s.LastName;
                    employeeModel.Gender = s.Gender;
                    employeeModel.DepartmentName = s.DepartmentName;
                    employeeModels.Add(employeeModel);
                });
            }
            return employeeModels.ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByDropdownAll")]
        public List<EmployeeModel> GetEmployeesByDropdownAll()
        {
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            var department = _context.Department.Select(s => new { s.DepartmentId, s.Name, s.Description }).AsNoTracking().ToList();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee";
            var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            var employee = result.Select(s => new
            {
                s.EmployeeID,
                s.UserID,
                s.SageID,
                s.PlantID,
                s.LevelID,
                s.LanguageID,
                s.CityID,
                s.RegionID,
                s.ReportID,
                s.FirstName,
                s.NickName,
                s.LastName,
                s.Gender,
                s.JobTitle,
                s.Email,
                s.LoginID,
                s.LoginPassword,
                s.TypeOfEmployeement,
                s.UserCode,
                s.Signature,
                s.ImageUrl,
                s.DateOfEmployeement,
                s.LastWorkingDate,
                s.Extension,
                s.SpeedDial,
                s.Mobile,
                s.SkypeAddress,
                s.IsActive,
                s.SectionID,
                s.SectionName,
                s.CompanyName,
                s.DivisionName,
                s.DepartmentName,
                s.SubSectionName,
                s.SubSectionTwoName,
                s.DivisionID,
                s.DesignationID,
                s.DepartmentID,
                s.SubSectionID,
                s.SubSectionTID,
                s.ExpectedJoiningDate,
                s.DesignationName,
                s.StatusCodeID,
                s.StatusCode,
                s.AddedByUserID,
                s.ModifiedByUserID,
                s.AddedByUser,
                s.ModifiedByUser,
                s.AddedDate,
                s.ModifiedDate,
                s.AcceptanceStatus,
                s.AcceptanceStatusDate,
                s.Status,
                s.HeadCount
            }).ToList();

            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                employeeModel.EmployeeID = s.EmployeeID;
                employeeModel.UserID = s.UserID;
                employeeModel.SageID = s.SageID;
                employeeModel.PlantID = s.PlantID;
                employeeModel.LevelID = s.LevelID;
                employeeModel.LanguageID = s.LanguageID;
                employeeModel.CityID = s.CityID;
                employeeModel.RegionID = s.RegionID;
                employeeModel.ReportID = s.ReportID;
                employeeModel.DepartmentName = department != null && s.DepartmentID != null ? department.Where(w => w.DepartmentId == s.DepartmentID).FirstOrDefault()?.Name : "";
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName + "|" + employeeModel.DepartmentName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.Email = s.Email;
                employeeModel.LoginID = s.LoginID;
                employeeModel.UserCode = s.UserCode;
                employeeModel.TypeOfEmployeement = s.TypeOfEmployeement;
                employeeModel.Signature = s.Signature;
                employeeModel.ImageUrl = s.ImageUrl;
                employeeModel.DateOfEmployeement = s.DateOfEmployeement;
                employeeModel.LastWorkingDate = s.LastWorkingDate;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.Mobile = s.Mobile;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModel.IsActive = s.IsActive;
                employeeModel.SectionID = s.SectionID;
                employeeModel.SectionName = s.SectionName;
                employeeModel.CompanyName = s.CompanyName;
                employeeModel.DivisionName = s.DivisionName;
                employeeModel.SubSectionName = s.SubSectionName;
                employeeModel.SubSectionTwoName = s.SubSectionName;
                employeeModel.DivisionID = s.DivisionID;
                employeeModel.DesignationID = s.DesignationID;
                employeeModel.DepartmentID = s.DepartmentID;
                employeeModel.SubSectionID = s.SubSectionID;
                employeeModel.SubSectionTID = s.SubSectionTID;
                employeeModel.UserName = s.AddedByUser;
                employeeModel.DesignationName = s.DesignationName;
                employeeModel.StatusCodeID = s.StatusCodeID;
                employeeModel.StatusCode = s.StatusCode;
                employeeModel.AddedByUserID = s.AddedByUserID;
                employeeModel.ModifiedByUserID = s.ModifiedByUserID;
                employeeModel.AddedByUser = s.AddedByUser;
                employeeModel.ModifiedByUser = s.ModifiedByUser;
                employeeModel.AddedDate = s.AddedDate;
                employeeModel.ModifiedDate = s.ModifiedDate;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.ExpectedJoiningDate = s.ExpectedJoiningDate;
                employeeModel.AcceptanceStatusDate = s.AcceptanceStatusDate;
                employeeModel.HeadCount = s.HeadCount;

                if (s.StatusCodeID == 2)
                {
                    employeeModel.isEnableLogin = false;
                }
                else if (s.StatusCodeID == 1)
                {
                    employeeModel.isEnableLogin = true;
                }
                employeeModel.Status = s.Status;

                employeeModels.Add(employeeModel);
            });
            return employeeModels.ToList();
        }
        [HttpGet]
        [Route("GetEmployeeInformation")]
        public List<EmployeeInformationModel> GetEmployeeInformation(int id)
        {
            var employeeInformation = _context.EmployeeInformation.Include(a => a.Employee).Include(b => b.Company).Include(c => c.Division).Include(d => d.Department)
                .Include(e => e.Designation).Include(f => f.Section).Include(g => g.SubSection).AsNoTracking().ToList();
            List<EmployeeInformationModel> employeeInformationModel = new List<EmployeeInformationModel>();
            employeeInformation.ForEach(s =>
            {
                EmployeeInformationModel employeeInformationModels = new EmployeeInformationModel
                {
                    EmployeeInformationID = s.EmployeeInformationId,
                    EmployeeID = s.EmployeeId,
                    CompanyID = s.CompanyId,
                    DivisionID = s.DivisionId,
                    DepartmentID = s.DepartmentId,
                    DesignationID = s.DesignationId,
                    SectionID = s.SectionId,
                    SubSectionID = s.SubSectionId,
                    EmployeeName = s.Employee != null ? s.Employee.NickName : "",
                    CompanyName = s.Company != null ? s.Company.PlantCode : "",
                    DivisionName = s.Division != null ? s.Division.Name : "",
                    DepartmentName = s.Department != null ? s.Department.Name : "",
                    Designation = s.Designation != null ? s.Designation.Name : "",
                    SectionName = s.Section != null ? s.Section.Name : "",
                    SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                    IsOversees = s.IsOversees,
                };
                employeeInformationModel.Add(employeeInformationModels);
            });
            return employeeInformationModel.Where(o => o.EmployeeID == id).OrderByDescending(a => a.EmployeeInformationID).ToList();
        }

        [HttpGet]
        [Route("GetEmployeesLeaveInformation")]
        public List<EmployeeLeaveInformationModel> GetEmployeesLeaveInformation()
        {
            var employeesOnLeave = GetEmployeesOnLeave();
            List<string> sageIds = employeesOnLeave.Select(s => s.EmpID).ToList();
            List<EmployeeLeaveInformationModel> employeeLeaveInformationModels = new List<EmployeeLeaveInformationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employees = _context.Employee
                .Include(s => s.Designation.SubSectionT)
                .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Include(n => n.ApplicationUser)
                .Include(s => s.AddedByUser)
                .Include(s => s.ModifiedByUser)
                .Include(u => u.User)
                .Include(s => s.StatusCode).Where(s => s.SageId != null && sageIds.Contains(s.SageId) && s.User.StatusCodeId == 1).AsNoTracking().ToList();
            if (employees != null && employees.Count > 0)
            {
                employees.ForEach(s =>
                {
                    var sageData = employeesOnLeave.FirstOrDefault(l => l.EmpID == s.SageId && l.PlantID == s.PlantId);
                    if (sageData != null)
                    {
                        EmployeeLeaveInformationModel employeeLeaveInformationModel = new EmployeeLeaveInformationModel();
                        employeeLeaveInformationModel.EmployeeId = s.EmployeeId;
                        employeeLeaveInformationModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                        employeeLeaveInformationModel.EmployeeName = s.FirstName + " | " + s.NickName + " | " + s.LastName;
                        employeeLeaveInformationModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                        employeeLeaveInformationModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                        employeeLeaveInformationModel.SectionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Name;
                        employeeLeaveInformationModel.Designation = s.Designation?.Name;
                        employeeLeaveInformationModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                        employeeLeaveInformationModel.StatusCode = s.StatusCode?.CodeValue;
                        employeeLeaveInformationModel.NonPresence = sageData.Code;
                        employeeLeaveInformationModel.Remarks = sageData.Remarks;
                        employeeLeaveInformationModel.Date = sageData.SubmitDate;
                        employeeLeaveInformationModels.Add(employeeLeaveInformationModel);
                    }
                });
            }
            if (employeeLeaveInformationModels != null && employeeLeaveInformationModels.Count > 0)
            {
                employeeLeaveInformationModels.ForEach(e =>
                {
                    e.DepartmentDesignation = e.DepartmentName + " | " + e.Designation + " | " + e.SectionName;
                });
            }
            return employeeLeaveInformationModels;
        }

        [HttpGet]
        [Route("GetEmployeesByDepartment")]
        public List<EmployeeModel> GetEmployeesByDepartment()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employee = _context.Employee.Include(a => a.User).Include(p => p.Plant)
                .Include(f => f.Designation)
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company)
                .Where(s => s.DepartmentId == 14)
             .AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.UserID = s.UserId;
                employeeModel.FirstName = s.FirstName;
                employeeModel.Name = s.FirstName + " " + s.LastName + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "");
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.DivisionID = s.DivisionId;
                employeeModel.DesignationID = s.DesignationId;
                employeeModel.DepartmentID = s.DepartmentId;
                employeeModel.SubSectionID = s.SubSectionId;
                employeeModel.SubSectionTID = s.SubSectionTid;
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.AcceptanceStatus = s.AcceptanceStatus;
                employeeModel.Status = s.AcceptanceStatus != null && s.AcceptanceStatus != 0 && applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "";
                employeeModel.DivisionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.DesignationName = s.Designation != null ? s.Designation.Name : "";
                employeeModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                //employeeModel.Name = employeeModel.CompanyName + '|' + employeeModel.DivisionName + '|' + employeeModel.DepartmentName + '|' + employeeModel.DesignationName;
                employeeModels.Add(employeeModel);
            });
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetEmployeesList")]
        public List<EmployeeModel> GetEmployeesList(string Companyname, string Companynames, int employeeid)
        {
            var applicationUserDetails = _context.ApplicationUser.AsNoTracking().ToList();
            List<Employee> employee = new List<Employee>();
            if (employeeid > 0)
            {
                employee = _context.Employee.Include(p => p.Plant).Include(d => d.DepartmentNavigation).Include(u => u.ApplicationUser).Include(l => l.Language).Where(w => w.EmployeeId == employeeid).ToList();
            }
            else
            {
                if (Companyname == "" || Companyname == null)
                {
                    employee = _context.Employee.Include(a => a.DepartmentNavigation).Include(u => u.ApplicationUser).Include(l => l.Language).ToList();
                }
                else
                {
                    if (Companynames != null && Companynames != "")
                    {
                        employee = _context.Employee.Include(p => p.Plant).Include(u => u.ApplicationUser).Include(a => a.DepartmentNavigation).Include(l => l.Language).Where(u => new[] { Companyname, Companynames }.Contains(u.Plant.PlantCode)).ToList();
                    }
                    else
                    {
                        employee = _context.Employee.Include(p => p.Plant).Include(u => u.ApplicationUser).Include(a => a.DepartmentNavigation).Include(l => l.Language).Where(s => s.Plant.PlantCode == Companyname).ToList();
                    }
                }
            }
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.StatusCodeID = applicationUserDetails != null ? applicationUserDetails.Where(a => a.UserId == s.UserId).Select(a => a.StatusCodeId).FirstOrDefault() : 0;
                employeeModel.FirstName = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.LanguageName = s.Language != null ? s.Language.Name : null;
                employeeModel.Mobile = s.Mobile;
                employeeModel.CompanyName = s.Plant != null ? s.Plant.PlantCode : "";
                employeeModel.PlantID = s.PlantId;
                employeeModel.Nricno = s.Nricno;
                employeeModel.LastName = (s.DepartmentNavigation != null ? (s.DepartmentNavigation.Name + "|") : "") + s.NickName + "|" + s.FirstName + (s.LastName != null ? (" | " + s.LastName) : "");
                employeeModels.Add(employeeModel);
            });
            if (employeeid > 0)
            {
                return employeeModels.Where(w => w.StatusCodeID == 1 && w.EmployeeID == employeeid).OrderBy(a => a.FirstName).ToList();
            }
            else
            {
                return employeeModels.Where(w => w.StatusCodeID == 1).OrderBy(a => a.FirstName).ToList();
            }
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetgetEmployeeItemsByPlant")]
        public List<EmployeeModel> GetgetEmployeeItemsByPlant(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var employee = _context.Employee.Include(a => a.User).Include(b => b.Section).
                 Include(c => c.Plant).
                 Include(d => d.Level).
                 Include(e => e.DepartmentNavigation).
                 Include(f => f.Designation).
                 Include(g => g.Language).
                 Include(h => h.City).
                 Include(i => i.Region).Include(j => j.Report).
                 Include(k => k.Division).Include(l => l.SubSection).Include(m => m.SubSectionT).Include(n => n.ApplicationUser).Include(o => o.AddedByUser).Include(p => p.ModifiedByUser).Include(q => q.StatusCode).AsNoTracking().ToList();
            List<EmployeeModel> employeeModel = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModels = new EmployeeModel
                {
                    EmployeeID = s.EmployeeId,
                    UserID = s.UserId,
                    SageID = s.SageId,
                    PlantID = s.PlantId,
                    LevelID = s.LevelId,
                    LanguageID = s.LanguageId,
                    UserCode = s.User != null ? s.User.UserCode : "",
                    CityID = s.CityId,
                    RegionID = s.RegionId,
                    ReportID = s.ReportId,
                    FirstName = s.FirstName,
                    NickName = s.NickName,
                    LastName = s.LastName,
                    Gender = s.Gender,
                    JobTitle = s.JobTitle,
                    Email = s.Email,

                    DesignationID = s.DesignationId,
                    DepartmentID = s.DepartmentId,
                    UserName = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    PlantName = s.Plant != null ? s.Plant.PlantCode : "",
                    LevelName = s.Level != null ? s.Level.Name : "",
                    DepartmentName = s.DepartmentNavigation != null ? s.DepartmentNavigation.Name : "",
                    DesignationName = s.Designation != null ? s.Designation.Name : "",
                    LanguageName = s.Language != null ? s.Language.Name : "",
                    HeadCount = s.HeadCount,
                    Status = s.AcceptanceStatus != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "",
                };
                employeeModel.Add(employeeModels);
            });
            return employeeModel.Where(s => s.PlantID == id).OrderByDescending(a => a.EmployeeID).ToList();
        }

        [HttpGet]
        [Route("GetEmployeePersonItemsByPlant")]
        public List<EmployeeModel> GetEmployeePersonItemsByPlant(int id)
        {

            var employee = _context.Employee
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company).Include(q => q.StatusCode).AsNoTracking().ToList();
            List<EmployeeModel> employeeModel = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModels = new EmployeeModel
                {
                    EmployeeID = s.EmployeeId,
                    UserID = s.UserId,
                    SageID = s.SageId,
                    PlantID = s.PlantId,
                    LevelID = s.LevelId,
                    LanguageID = s.LanguageId,
                    CityID = s.CityId,
                    RegionID = s.RegionId,
                    ReportID = s.ReportId,
                    FirstName = s.FirstName,
                    NickName = s.NickName,
                    LastName = s.LastName,
                    Gender = s.Gender,
                    JobTitle = s.JobTitle,
                    StatusCodeID = s.StatusCodeId,
                    Email = s.Email,
                    DesignationID = s.DesignationId,
                    DepartmentID = s.DepartmentId,
                    PlantName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.PlantCode,
                    DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name,
                    DesignationName = s.Designation != null ? s.Designation.Name : "",
                    HeadCount = s.HeadCount,

                };
                employeeModel.Add(employeeModels);
            });
            employeeModel = employeeModel?.Where(s => s.PlantID == id && s.StatusCodeID == 1).ToList();
            if (employeeModel != null && employeeModel.Count > 0)
            {
                employeeModel.ForEach(e =>
                {
                    e.Name = e.PlantName + " | " + e.DepartmentName + " | " + e.DesignationName + " | " + e.FirstName + " | " + e.LastName + " | " + e.NickName;

                });
            }
            return employeeModel.OrderByDescending(a => a.EmployeeID).ToList();
        }



        [HttpGet]
        [Route("GetEmployeePersonSampleRequestForm")]
        public List<EmployeeModel> GetEmployeePersonSampleRequestForm()
        {
            var employee = _context.Employee
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company).Include(q => q.StatusCode).AsNoTracking().ToList();
            List<EmployeeModel> employeeModel = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModels = new EmployeeModel
                {
                    EmployeeID = s.EmployeeId,
                    UserID = s.UserId,
                    SageID = s.SageId,
                    PlantID = s.PlantId,
                    LevelID = s.LevelId,
                    LanguageID = s.LanguageId,
                    CityID = s.CityId,
                    RegionID = s.RegionId,
                    ReportID = s.ReportId,
                    FirstName = s.FirstName,
                    NickName = s.NickName,
                    LastName = s.LastName,
                    Gender = s.Gender,
                    JobTitle = s.JobTitle,
                    StatusCodeID = s.StatusCodeId,
                    Email = s.Email,
                    DesignationID = s.DesignationId,
                    DepartmentID = s.DepartmentId,
                    PlantName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.PlantCode,
                    DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name,
                    DesignationName = s.Designation != null ? s.Designation.Name : "",
                    HeadCount = s.HeadCount,

                };
                employeeModel.Add(employeeModels);
            });
            employeeModel = employeeModel?.Where(s => s.StatusCodeID == 1).ToList();
            if (employeeModel != null && employeeModel.Count > 0)
            {
                employeeModel.ForEach(e =>
                {
                    e.Name = e.PlantName + " | " + e.DepartmentName + " | " + e.DesignationName + " | " + e.FirstName + " | " + e.LastName + " | " + e.NickName;

                });
            }
            return employeeModel.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet("GetEmployeeResignation")]
        public EmployeeResignationModel Get(int? id)
        {
            EmployeeResignationModel employeeResignationModels = new EmployeeResignationModel();
            var employeeResignation = _context.EmployeeResignation.Include(a => a.Designation).Include(b => b.Document).
                 Include(c => c.Employee).Where(s => s.EmployeeId == id).OrderByDescending(a => a.EmployeeId).FirstOrDefault();
            List<EmployeeResignationModel> employeeResignationModel = new List<EmployeeResignationModel>();
            if (employeeResignation != null)
            {
                employeeResignationModels = new EmployeeResignationModel();


                employeeResignationModels.EmployeeID = employeeResignation.EmployeeId;
                employeeResignationModels.EmployeeResignationID = employeeResignation.EmployeeResignationId;
                employeeResignationModels.AgreedLastDate = employeeResignation.AgreedLastDate;
                employeeResignationModels.IntendedLastDate = employeeResignation.IntendedLastDate;
                employeeResignationModels.ApproveStatus = employeeResignation.ApproveStatus;
                employeeResignationModels.ApprovedBy = employeeResignation.ApprovedBy;
                employeeResignationModels.DesignationHCNo = employeeResignation.DesignationHcno;
                employeeResignationModels.ResignationType = employeeResignation.ResignationType;
                employeeResignationModels.ResigneeName = employeeResignation.Employee?.NickName;
                employeeResignationModels.DesignationID = employeeResignation.DesignationId;
                employeeResignationModels.DocumentID = employeeResignation.DocumentId;
                employeeResignationModels.DocumentName = employeeResignation.Document != null ? employeeResignation.Document.FileName : "";
            }



            return employeeResignationModels;

        }
        [HttpPost("GetExistingnHCEmployeeInformation")]
        public Boolean GetExistingnHCEmployeeInformation(EmployeeInformationModel value)
        {
            bool result = false;
            var employeeinformation = _context.EmployeeInformation.Where(e => e.CompanyId == value.CompanyID && e.DivisionId == value.DivisionID && e.DepartmentId == value.DepartmentID
                                        && e.SectionId == value.SectionID && e.SubSectionId == value.SubSectionID && e.SubSectionTid == value.SubSectionTID && e.DesignationId == value.DesignationID && e.HeadCount == value.DesignationHeadCount).FirstOrDefault();
            if (employeeinformation != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;


        }
        [HttpPost("GetExistingnHeadCountEmployee")]
        public Boolean GetExistingnHeadCountEmployee(EmployeeInformationModel value)
        {
            bool result = false;
            var employeeinformation = _context.Employee.Where(e => e.PlantId == value.CompanyID && e.DivisionId == value.DivisionID && e.DepartmentId == value.DepartmentID
                                        && e.SectionId == value.SectionID && e.SubSectionId == value.SubSectionID && e.SubSectionTid == value.SubSectionTID && e.DesignationId == value.DesignationID && e.HeadCount == value.DesignationHeadCount).FirstOrDefault();
            if (employeeinformation != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;


        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<EmployeeModel> GetData(SearchModel searchModel)
        {
            var employee = new Employee();
            List<EmployeeInformationModel> empinforList = new List<EmployeeInformationModel>();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).FirstOrDefault();
                        break;
                    case "Last":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).LastOrDefault();
                        break;
                    case "Next":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).LastOrDefault();
                        break;
                    case "Previous":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).FirstOrDefault();
                        break;
                    case "Last":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).LastOrDefault();
                        break;
                    case "Next":
                        employee = _context.Employee.OrderBy(o => o.EmployeeId).FirstOrDefault(s => s.EmployeeId > searchModel.Id);
                        break;
                    case "Previous":
                        employee = _context.Employee.OrderByDescending(o => o.EmployeeId).FirstOrDefault(s => s.EmployeeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<EmployeeModel>(employee);
            if (result != null && result.EmployeeID > 0)
            {
                result.ReportToIDs = _context.EmployeeReportTo.Where(r => r.EmployeeId == result.EmployeeID).AsNoTracking().Select(r => r.ReportToId).ToList();
                result.LoginID = _context.ApplicationUser.Where(a => a.UserId == result.UserID).Select(t => t.LoginId).FirstOrDefault();
                result.LoginPassword = EncryptDecryptPassword.Decrypt(_context.ApplicationUser.Where(a => a.UserId == result.UserID).Select(t => t.LoginPassword).FirstOrDefault());
                result.UserCode = _context.ApplicationUser.Where(a => a.UserId == result.UserID).Select(t => t.UserCode).FirstOrDefault();
                result.UserGroupID = _context.UserGroupUser.Where(a => a.UserId == result.UserID).Select(t => t.UserGroupId).FirstOrDefault();
                result.RoleID = _context.ApplicationUserRole.Where(a => a.UserId == result.UserID).Select(t => t.RoleId).FirstOrDefault();
                result.UserName = _context.ApplicationUser.Where(a => a.UserId == result.UserID).Select(t => t.UserName).FirstOrDefault();
                // empinforList = _context.EmployeeInformation.Where(e => e.EmployeeId == result.EmployeeID).ToList();

                var empinforLists = _context.EmployeeInformation.Include(a => a.Employee).
                    Include(b => b.Department).Include(c => c.Designation).Include(k => k.Division).
                    Include(l => l.Section).Include(ss => ss.SubSection).Include(n => n.Company).
                    Include(aa => aa.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();

                List<EmployeeInformationModel> employeeInformationModel = new List<EmployeeInformationModel>();
                empinforLists.ForEach(s =>
                {
                    EmployeeInformationModel employeeInformationModels = new EmployeeInformationModel
                    {
                        EmployeeInformationID = s.EmployeeInformationId,
                        EmployeeID = s.EmployeeId,
                        CompanyID = s.CompanyId,
                        DivisionID = s.DivisionId,
                        DepartmentID = s.DepartmentId,
                        DesignationID = s.DesignationId,
                        SectionID = s.SectionId,
                        SubSectionID = s.SubSectionId,
                        EmployeeName = s.Employee != null ? s.Employee.NickName : "",
                        CompanyName = s.Company != null ? s.Company.PlantCode : "",
                        DivisionName = s.Division != null ? s.Division.Name : "",
                        DepartmentName = s.Department != null ? s.Department.Name : "",
                        Designation = s.Designation != null ? s.Designation.Name : "",
                        SectionName = s.Section != null ? s.Section.Name : "",
                        SubSectionName = s.SubSection != null ? s.SubSection.Name : "",
                        IsOversees = s.IsOversees,
                    };
                    employeeInformationModel.Add(employeeInformationModels);
                });
                employeeInformationModel.Where(w => w.EmployeeID == result.EmployeeID).ToList();
                if (employeeInformationModel.Count != 0 && employeeInformationModel != null)
                {
                    result.EmployeeActingList = employeeInformationModel.Where(e => e.IsOversees == false).ToList();
                    result.EmployeeOverseesList = employeeInformationModel.Where(e => e.IsOversees == true).ToList();
                }
                //result.EmployeeOverseesList = employeeOverseelist;
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertEmployee")]
        public EmployeeModel Post(EmployeeModel value)
        {

            if (value.LoginID != "")
            {

                //if (value.PlantID != null)
                //{
                //    value.LoginID = value.PlantID == 1 ? "SWJB" + value.SageID : value.PlantID == 2 ? "SWSIN" + value.SageID : "SW" + value.SageID;
                //}
                var existinguser = _context.ApplicationUser.Where(a => a.LoginId == value.LoginID).FirstOrDefault();

                var password = "";
                if (value.SageID != null)
                {
                    password = EncryptDecryptPassword.Encrypt(value.LoginPassword);
                }
                if (value.isEnableLogin == false)
                {
                    value.StatusCodeID = 2;
                }
                else
                {
                    value.StatusCodeID = 1;
                }
                if (existinguser == null)
                {
                    var applicationUser = new ApplicationUser
                    {
                        //WorkDateId = value.WorkDateID,
                        //CountryOptions = value.CountryOptions,
                        UserCode = value.SageID,
                        UserName = value.FirstName + value.LastName,
                        EmployeeNo = value.EmployeeNo,
                        UserEmail = value.Email,
                        AuthenticationType = 1,
                        LoginId = value.LoginID,
                        LoginPassword = password,
                        StatusCodeId = value.StatusCodeID.Value,
                        SessionId = Guid.NewGuid(),
                        LastAccessDate = value.LastAccessDate,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,


                    };
                    _context.ApplicationUser.Add(applicationUser);
                    _context.SaveChanges();
                    value.UserID = applicationUser.UserId;
                    if (value.UserGroupID != null && value.UserGroupID > 0)
                    {
                        var userGroupuser = new UserGroupUser
                        {
                            UserId = value.UserID,
                            UserGroupId = value.UserGroupID,
                        };
                        _context.UserGroupUser.Add(userGroupuser);
                    }
                    if (value.RoleID != null && value.RoleID > 0)
                    {
                        var userRole = new ApplicationUserRole
                        {
                            UserId = value.UserID.Value,
                            RoleId = value.RoleID.Value,

                        };
                        _context.ApplicationUserRole.Add(userRole);
                    }
                    //if(value.AcceptanceStatus == 0)
                    // {
                    //     value.AcceptanceStatus = 681;
                    // }
                    var employee = new Employee
                    {
                        UserId = value.UserID,
                        SageId = value.SageID,
                        PlantId = value.PlantID,
                        LevelId = value.LevelID,

                        LanguageId = value.LanguageID,
                        CityId = value.CityID,
                        RegionId = value.RegionID,
                        ReportId = value.ReportID,
                        FirstName = value.FirstName,
                        NickName = value.NickName,
                        LastName = value.LastName,
                        Gender = value.Gender,
                        JobTitle = value.JobTitle,
                        Email = value.Email,
                        TypeOfEmployeement = value.TypeOfEmployeement,
                        Signature = value.Signature,
                        ImageUrl = value.ImageUrl,
                        DateOfEmployeement = value.DateOfEmployeement,
                        LastWorkingDate = value.LastWorkingDate,
                        Extension = value.Extension,
                        SpeedDial = value.SpeedDial,
                        Mobile = value.Mobile,
                        SkypeAddress = value.SkypeAddress,
                        IsActive = value.IsActive,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                        // StatusCodeId = value.StatusCodeID.Value,
                        AcceptanceStatus = value.AcceptanceStatus,
                        ExpectedJoiningDate = value.ExpectedJoiningDate,
                        AcceptanceStatusDate = value.AcceptanceStatusDate,
                        DivisionId = value.DivisionID,
                        SectionId = value.SectionID,
                        SubSectionId = value.SubSectionID,
                        SubSectionTid = value.SubSectionTID,
                        DesignationId = value.DesignationID,
                        DepartmentId = value.DepartmentID,
                        HeadCount = value.HeadCount,

                    };
                    _context.Employee.Add(employee);
                    _context.SaveChanges();
                    value.EmployeeID = employee.EmployeeId;
                    if (value.ReportToIDs != null && value.ReportToIDs.Count > 0)
                    {
                        value.ReportToIDs.ForEach(r =>
                        {
                            var existing = _context.EmployeeReportTo.Where(em => em.EmployeeId == value.EmployeeID && em.ReportToId == r).FirstOrDefault();
                            if (existing == null)
                            {
                                var employeeReportTo = new EmployeeReportTo
                                {
                                    ReportToId = r,
                                    EmployeeId = value.EmployeeID,

                                };
                                _context.EmployeeReportTo.Add(employeeReportTo);
                                _context.SaveChanges();
                            }
                        });
                    }
                }
                else
                {
                    throw new AppException("Already existing loginID! Please change the employee loginID", null);
                }
            }
            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("InsertEmployeeInformation")]
        public EmployeeInformationModel PostEmployeeInformation(EmployeeInformationModel value)
        {

            var employeeInformation = new EmployeeInformation
            {
                EmployeeId = value.EmployeeID,
                CompanyId = value.CompanyID,
                DivisionId = value.DivisionID,
                DepartmentId = value.DepartmentID,
                SectionId = value.SectionID,
                SubSectionTid = value.SubSectionTID,
                SubSectionId = value.SubSectionID,
                DesignationId = value.DesignationID,
                DesignationHeadCountId = value.DesignationHeadCountID,
                IsOversees = value.IsOversees,
                HeadCount = value.DesignationHeadCount,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
            };
            _context.EmployeeInformation.Add(employeeInformation);
            _context.SaveChanges();
            value.EmployeeInformationID = employeeInformation.EmployeeInformationId;
            return value;
        }
        [HttpPut]
        [Route("UpdateEmployeeInformation")]
        public EmployeeInformationModel PutEmployeeInformation(EmployeeInformationModel value)
        {

            var employeeInformation = _context.EmployeeInformation.Where(ei => ei.EmployeeInformationId == value.EmployeeInformationID).FirstOrDefault();
            {
                employeeInformation.EmployeeId = value.EmployeeID;
                employeeInformation.CompanyId = value.CompanyID;
                employeeInformation.DivisionId = value.DivisionID;
                employeeInformation.DepartmentId = value.DepartmentID;
                employeeInformation.SectionId = value.SectionID;
                employeeInformation.SubSectionId = value.SubSectionID;
                employeeInformation.SubSectionTid = value.SubSectionTID;
                employeeInformation.DesignationId = value.DesignationID;
                employeeInformation.DesignationHeadCountId = value.DesignationHeadCountID;
                employeeInformation.IsOversees = value.IsOversees;
                employeeInformation.ModifiedByUserId = value.ModifiedByUserID;
                employeeInformation.ModifiedDate = DateTime.Now;

            };

            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertEmployeeResignation")]
        public EmployeeResignationModel PostEmployeeResignation(EmployeeResignationModel value)
        {
            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new Documents
                {
                    DocumentId = s.DocumentId,
                    SessionId = s.SessionId
                }).Where(d => d.SessionId == value.SessionID).FirstOrDefault();
                if (docu != null)
                {
                    value.DocumentID = docu.DocumentId;
                }
            }
            var employeeResignation = new EmployeeResignation
            {
                EmployeeId = value.EmployeeID,
                ResignationType = value.ResignationType,
                IntendedLastDate = value.IntendedLastDate,
                AgreedLastDate = value.AgreedLastDate,
                DesignationId = value.DesignationID,
                DesignationHcno = value.DesignationHCNo,
                ApprovedBy = value.ApprovedBy,
                ApproveStatus = value.ApproveStatus,
                ResigneeName = value.ResigneeName,
                DocumentId = value.DocumentID,



            };
            _context.EmployeeResignation.Add(employeeResignation);
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateEmployeeResignation")]
        public EmployeeResignationModel PutEmployeeResignation(EmployeeResignationModel value)
        {

            if (value.SessionID != null)
            {
                var docu = _context.Documents.Select(s => new Documents
                {
                    DocumentId = s.DocumentId,
                    SessionId = s.SessionId
                }).Where(d => d.SessionId == value.SessionID).FirstOrDefault();
                if (docu != null)
                {
                    value.DocumentID = docu.DocumentId;
                }
            }
            var employeeResignation = _context.EmployeeResignation.Where(e => e.EmployeeResignationId == value.EmployeeResignationID).FirstOrDefault();
            employeeResignation.EmployeeId = value.EmployeeID;
            if (value.ApproveStatus == 701 && value.AgreedLastDate == null)
            {
                throw new Exception("Please Select Agreed Last Date");
            }
            employeeResignation.AgreedLastDate = value.AgreedLastDate;
            employeeResignation.IntendedLastDate = value.IntendedLastDate;
            employeeResignation.ResignationType = value.ResignationType;
            employeeResignation.DesignationId = value.DesignationID;
            employeeResignation.DesignationHcno = value.DesignationHCNo;
            employeeResignation.ApprovedBy = value.ApprovedBy;
            employeeResignation.ApproveStatus = value.ApproveStatus;
            employeeResignation.DocumentId = value.DocumentID;

            _context.SaveChanges();


            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateEmployee")]
        public EmployeeModel Put(EmployeeModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var employee = _context.Employee.SingleOrDefault(p => p.EmployeeId == value.EmployeeID);

            employee.UserId = value.UserID;
            employee.SageId = value.SageID;
            employee.PlantId = value.PlantID;
            employee.LevelId = value.LevelID;

            employee.LanguageId = value.LanguageID;
            employee.CityId = value.CityID;
            employee.RegionId = value.RegionID;
            employee.ReportId = value.ReportID;
            employee.FirstName = value.FirstName;
            employee.NickName = value.NickName;
            employee.LastName = value.LastName;
            employee.Gender = value.Gender;
            employee.JobTitle = value.JobTitle;
            employee.Email = value.Email;
            employee.TypeOfEmployeement = value.TypeOfEmployeement;
            employee.Signature = value.Signature;
            employee.ImageUrl = value.ImageUrl;
            employee.DateOfEmployeement = value.DateOfEmployeement;
            employee.LastWorkingDate = value.LastWorkingDate;
            employee.Extension = value.Extension;
            employee.SpeedDial = value.SpeedDial;
            employee.Mobile = value.Mobile;
            employee.SkypeAddress = value.SkypeAddress;
            employee.IsActive = value.IsActive;

            employee.ModifiedByUserId = value.ModifiedByUserID;
            employee.ModifiedDate = DateTime.Now;
            //employee.StatusCodeId = value.StatusCodeID.Value;
            employee.AcceptanceStatus = value.AcceptanceStatus;
            employee.ExpectedJoiningDate = value.ExpectedJoiningDate;
            employee.AcceptanceStatusDate = value.AcceptanceStatusDate;
            employee.DivisionId = value.DivisionID;
            employee.SubSectionId = value.SubSectionID;
            employee.SubSectionTid = value.SubSectionTID;
            employee.HeadCount = value.HeadCount;
            employee.DepartmentId = value.DepartmentID;
            employee.DesignationId = value.DesignationID;

            var applicationuser = _context.ApplicationUser.Where(a => a.UserId == value.UserID).FirstOrDefault();
            var password = "";
            if (value.LoginPassword != null)
            {
                password = EncryptDecryptPassword.Encrypt(value.LoginPassword);
            }
            if (value.isEnableLogin == false)
            {
                applicationuser.StatusCodeId = 2;
            }
            else
            {
                applicationuser.StatusCodeId = 1;
            }
            var status = value.AcceptanceStatus != null && value.AcceptanceStatus != 0 && applicationmasterdetail != null && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.AcceptanceStatus).Select(m => m.Value).FirstOrDefault() : "";
            if (status.ToLower() == "resign")
            {
                applicationuser.StatusCodeId = 2;
            }
            if (status.ToLower() == "active")
            {
                applicationuser.StatusCodeId = 1;
            }
            applicationuser.UserName = value.FirstName + value.LastName;
            if (value.UserCode != null)
            {
                applicationuser.UserCode = value.UserCode;
            }
            applicationuser.EmployeeNo = value.EmployeeNo;
            applicationuser.UserEmail = value.Email;
            applicationuser.AuthenticationType = 1;
            applicationuser.LoginId = value.LoginID;
            applicationuser.LoginPassword = password;

            applicationuser.ModifiedByUserId = value.ModifiedByUserID;
            applicationuser.ModifiedDate = DateTime.Now;
            applicationuser.LastAccessDate = value.LastAccessDate;
            var userGroupuser = _context.UserGroupUser.Where(ug => ug.UserId == value.UserID).FirstOrDefault();
            if (userGroupuser != null)
            {
                userGroupuser.UserId = value.UserID;
                userGroupuser.UserGroupId = value.UserGroupID;
            };

            var roleupdate = _context.ApplicationUserRole.Where(r => r.UserId == value.UserID).FirstOrDefault();
            if (roleupdate != null && value.RoleID != null)
            {
                roleupdate.RoleId = value.RoleID.Value;
            }
            else
            {
                if (value.RoleID != null && value.RoleID > 0)
                {
                    var userRole = new ApplicationUserRole
                    {
                        UserId = value.UserID.Value,
                        RoleId = value.RoleID.Value,

                    };
                    _context.ApplicationUserRole.Add(userRole);
                }
            }
            if (value.ReportToIDs != null && value.ReportToIDs.Count > 0)
            {
                var existing = _context.EmployeeReportTo.Where(em => em.EmployeeId == value.EmployeeID).ToList();
                if (existing != null && existing.Count > 0)
                {
                    _context.EmployeeReportTo.RemoveRange(existing);
                    _context.SaveChanges();
                }
                value.ReportToIDs.ForEach(r =>
                {

                    var employeeReportTo = new EmployeeReportTo
                    {
                        ReportToId = r,
                        EmployeeId = value.EmployeeID,

                    };
                    _context.EmployeeReportTo.Add(employeeReportTo);
                    _context.SaveChanges();

                });
            }

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteEmployee")]
        public void Delete(int id)
        {
            try
            {
                var employee = _context.Employee.SingleOrDefault(p => p.EmployeeId == id);

                if (employee != null)
                {
                    _context.Employee.Remove(employee);
                    _context.SaveChanges();
                    var user = _context.ApplicationUser.Where(e => e.UserId == employee.UserId).FirstOrDefault();
                    if (user != null)
                    {
                        var userrole = _context.ApplicationUserRole.Where(r => r.UserId == user.UserId).ToList();
                        if (userrole != null)
                        {
                            _context.ApplicationUserRole.RemoveRange(userrole);
                            _context.SaveChanges();
                        }
                        _context.ApplicationUser.Remove(user);
                        _context.SaveChanges();
                    }
                }


            }
            catch (Exception ex)
            {

                throw new AppException("This user cannot be delete", ex);
            }


        }
        [HttpDelete]
        [Route("DeleteEmployeeInformation")]
        public void DeleteEmployeeInformation(int id)
        {
            try
            {
                var employee = _context.EmployeeInformation.SingleOrDefault(p => p.EmployeeInformationId == id);
                if (employee != null)
                {
                    _context.EmployeeInformation.Remove(employee);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                throw new AppException("This user cannot be delete", ex);
            }


        }

        // GET: api/Project
        [HttpPost]
        [Route("GetCompanyDirectory")]
        public List<EmployeeModel> GetCompanyDirectory(EmployeeDirectoryParam employeeDirectoryParam)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var applicationUsers = _context.ApplicationUser.Where(s => s.StatusCodeId == 1).Select(s => s.UserId).ToList();
            var employee = _context.Employee.Include(a => a.User).
                Include(d => d.Level).
                Include(f => f.Designation).
                Include(g => g.Language).
                Include(h => h.City).
                Include(i => i.Region)
                .Include(s => s.EmployeeReportTo)
                .Include(s => s.User.ApplicationUserRole)
                 .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department.Division.Company).Include(n => n.ApplicationUser).Include(o => o.AddedByUser).Include(p => p.ModifiedByUser).Include(q => q.StatusCode).Where(s => s.UserId != null && applicationUsers.Contains(s.UserId.Value) && s.StatusCodeId != 2 && s.StatusCodeId != 3).AsNoTracking().ToList();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            employee.ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();

                employeeModel.EmployeeID = s.EmployeeId;
                employeeModel.UserID = s.UserId;
                employeeModel.SageID = s.SageId;
                employeeModel.PlantID = s.PlantId;
                employeeModel.LevelID = s.LevelId;
                employeeModel.LanguageID = s.LanguageId;
                employeeModel.UserCode = s.User != null ? s.User.UserCode : "";
                employeeModel.CityID = s.CityId;
                employeeModel.RegionID = s.RegionId;
                employeeModel.ReportID = s.ReportId;
                employeeModel.CompanyName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Company?.Description;
                employeeModel.Name = s.FirstName + "|" + s.LastName + "|" + s.NickName;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.SectionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Name;
                employeeModel.DivisionName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Division?.Name;
                employeeModel.DepartmentID = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.DepartmentId;
                employeeModel.DepartmentName = s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                employeeModel.SubSectionName = s.Designation?.SubSectionT?.SubSecton?.Name;
                employeeModel.SubSectionTwoName = s.Designation?.SubSectionT?.Name;
                employeeModel.DesignationID = s.Designation?.DesignationId;
                employeeModel.DesignationName = s.Designation != null ? s.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name + "|" + s.Designation.Name : "";
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.Email = s.Email;
                employeeModel.SkypeAddress = s.SkypeAddress;
                employeeModel.Extension = s.Extension;
                employeeModel.SpeedDial = s.SpeedDial;
                employeeModel.IsActive = s.IsActive;
                employeeModel.StatusCodeID = s.StatusCodeId;
                employeeModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                employeeModel.AddedByUserID = s.AddedByUserId;
                employeeModel.ModifiedByUserID = s.ModifiedByUserId;
                employeeModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                employeeModel.AddedDate = s.AddedDate;
                employeeModel.ModifiedDate = s.ModifiedDate;
                employeeModels.Add(employeeModel);
            });
            if (employeeDirectoryParam.CompanyId != null)
            {
                employeeModels = employeeModels.Where(c => c.PlantID == employeeDirectoryParam.CompanyId).ToList();
            }
            if (employeeDirectoryParam.DepartmentId != null)
            {
                employeeModels = employeeModels.Where(c => c.DepartmentID == employeeDirectoryParam.DepartmentId).ToList();
            }
            if (employeeDirectoryParam.DesignationId != null)
            {
                employeeModels = employeeModels.Where(c => c.DesignationID != null && c.DesignationID == employeeDirectoryParam.DesignationId).ToList();
            }
            return employeeModels.OrderByDescending(a => a.EmployeeID).ToList();
        }
        [HttpGet]
        [Route("GetEmployeesByCompany")]
        public List<EmployeeModel> GetEmployeesByCompany(long? id)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee where plantid=" + id + " and (Status!='Resign' or Status is null) ";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var employee = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            employee.ToList().ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                employeeModel.EmployeeID = s.EmployeeID;
                employeeModel.UserID = s.UserID;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.SectionName = s.SectionName;
                employeeModel.CompanyName = s.CompanyName;
                employeeModel.DivisionName = s.DivisionName;
                employeeModel.DepartmentName = s.DepartmentName;
                employeeModel.DesignationName = s.DesignationName;
                employeeModel.Name = employeeModel.DivisionName + "|" + employeeModel.DepartmentName + "|" + employeeModel.DesignationName + "|" + s.FirstName + " " + s.LastName + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "");
                employeeModel.DropDownName = s.FirstName + " | " + s.NickName + " | " + s.DesignationName;
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.DepartmentID = s.DepartmentID;
                employeeModel.DesignationID = s.DesignationID;
                employeeModel.Status = s.Status;
                employeeModels.Add(employeeModel);
            });
            return employeeModels;
        }

        [HttpPost]
        [Route("GetSelectedEmployeesByCompany")]
        public List<EmployeeModel> GetSelectedEmployeesByCompany(SearchModel data)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee where plantid=" + data.Id + " and (Status!='Resign' or Status is null) ";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var employee = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            employee.ToList().ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                employeeModel.EmployeeID = s.EmployeeID;
                employeeModel.UserID = s.UserID;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.SectionName = s.SectionName;
                employeeModel.CompanyName = s.CompanyName;
                employeeModel.DivisionName = s.DivisionName;
                employeeModel.DepartmentName = s.DepartmentName;
                employeeModel.DesignationName = s.DesignationName;
                employeeModel.Name = employeeModel.DivisionName + "|" + employeeModel.DepartmentName + "|" + employeeModel.DesignationName + "|" + s.FirstName + " " + s.LastName + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "");
                employeeModel.DropDownName = s.FirstName + " | " + s.NickName + " | " + s.DesignationName;
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.DepartmentID = s.DepartmentID;
                employeeModel.DesignationID = s.DesignationID;
                employeeModel.Status = s.Status;
                employeeModels.Add(employeeModel);
            });
            return employeeModels = employeeModels.Where(w=>data.Ids.Contains(w.UserID)).ToList();

        }
            [HttpGet]
        [Route("GetEmployeesByCompanyForRoutine")]
        public List<EmployeeModel> GetEmployeesByCompanyForRoutine(long? id)
        {
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee where plantid=" + id + " and (Status!='Resign' or Status is null) ";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var employee = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            List<EmployeeModel> employeeModels = new List<EmployeeModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            employee.ToList().ForEach(s =>
            {
                EmployeeModel employeeModel = new EmployeeModel();
                employeeModel.EmployeeID = s.EmployeeID;
                employeeModel.UserID = s.UserID;
                employeeModel.FirstName = s.FirstName;
                employeeModel.NickName = s.NickName;
                employeeModel.LastName = s.LastName;
                employeeModel.Gender = s.Gender;
                employeeModel.JobTitle = s.JobTitle;
                employeeModel.SectionName = s.SectionName;
                employeeModel.CompanyName = s.CompanyName;
                employeeModel.DivisionName = s.DivisionName;
                employeeModel.DepartmentName = s.DepartmentName;
                employeeModel.DesignationName = s.DesignationName;
                employeeModel.Name = employeeModel.DivisionName + "|" + employeeModel.DepartmentName + "|" + employeeModel.DesignationName + "|" + s.FirstName + " " + s.LastName + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "");
                employeeModel.DropDownName = s.CompanyName + " | " + s.FirstName + " | " + s.NickName;
                employeeModel.HeadCount = s.HeadCount;
                employeeModel.DepartmentID = s.DepartmentID;
                employeeModel.DesignationID = s.DesignationID;
                employeeModel.Status = s.Status;
                employeeModels.Add(employeeModel);
            });
            return employeeModels;
        }

        private List<View_SageAttendance> GetEmployeesOnLeave()
        {
            List<View_SageAttendance> sageAttendances = new List<View_SageAttendance>();
            try
            {
                string sqlQuery = string.Empty;
                string sgQuery = string.Empty;
                var date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                sqlQuery = "Select * from GetSageAttendance where CAST(StartDate AS Date) = '" + date + "' or (CAST(EndDate AS Date) >= '" + date + "' and CAST(EndDate AS Date) <= '" + date + "')";
                sgQuery = "Select * from GetSGSageAttndance where CAST(StartDate AS Date) = '" + date + "' or (CAST(EndDate AS Date) >= '" + date + "' and CAST(EndDate AS Date) <= '" + date + "')";
                var sageItems = _context.Set<View_SageAttendance>().FromSqlRaw(sqlQuery).AsQueryable();
                sageItems.ToList().ForEach(s =>
                {
                    View_SageAttendance view_SageAttendance = new View_SageAttendance();
                    view_SageAttendance.Code = s.Code;
                    view_SageAttendance.Duration = s.Duration;
                    view_SageAttendance.EmpID = s.EmpID;
                    view_SageAttendance.EndDate = s.EndDate;
                    view_SageAttendance.StartDate = s.StartDate;
                    view_SageAttendance.SubmitDate = s.SubmitDate;
                    view_SageAttendance.Remarks = s.Remarks;
                    view_SageAttendance.PlantID = 1;
                    sageAttendances.Add(view_SageAttendance);
                });
                var sgsageItems = _context.Set<View_SageAttendance>().FromSqlRaw(sgQuery).AsQueryable();
                sgsageItems.ToList().ForEach(s =>
                {
                    View_SageAttendance view_SageAttendance = new View_SageAttendance();
                    view_SageAttendance.Code = s.Code;
                    view_SageAttendance.Duration = s.Duration;
                    view_SageAttendance.EmpID = s.EmpID;
                    view_SageAttendance.EndDate = s.EndDate;
                    view_SageAttendance.StartDate = s.StartDate;
                    view_SageAttendance.SubmitDate = s.SubmitDate;
                    view_SageAttendance.Remarks = s.Remarks;
                    view_SageAttendance.PlantID = 2;
                    sageAttendances.Add(view_SageAttendance);
                });
                //sageItems = sageItems.Concat(sgsageItems);
                //sageAttendances = sageAttendances.Where(s => s.StartDate.Value.Date <= DateTime.Now.Date && s.EndDate >= DateTime.Now.Date).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // Info.  
            return sageAttendances.ToList(); ;
        }

        [HttpGet]
        [Route("GetDepartmentByUserId")]
        public List<CodeMasterModel> GetDepartmentByUserId(int id)
        {
            List<CodeMasterModel> codeMasterModels = new List<CodeMasterModel>();
            var employees = _context.Employee
                .Include(s => s.Designation.SubSectionT.SubSecton)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section)
                .Include(s => s.Designation.SubSectionT.SubSecton.Section.Department).Where(u => u.UserId == id).AsNoTracking().ToList();
            employees.ForEach(e =>
            {
                if (e.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name != null)
                {
                    CodeMasterModel codeMasterModel = new CodeMasterModel
                    {
                        CodeID = (int)e.UserId,
                        CodeDescription = e.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name,
                    };
                    codeMasterModels.Add(codeMasterModel);
                }
            });
            return codeMasterModels;
        }
        [HttpPut]
        [Route("UpdateEmployeeSign")]
        public EmployeeSignModel UpdateEmployeeSign(EmployeeSignModel value)
        {
            var employeeSign = _context.EmployeeSign.SingleOrDefault(a => a.EmployeeId == value.EmployeeId);
            if (employeeSign != null)
            {
                byte[] bytes = Convert.FromBase64String(value.EmployeeSignature);
                employeeSign.EmployeeSignature = DocumentZipUnZip.Zip(bytes);
                value.Status = "update";
                _context.SaveChanges();
            }
            else
            {
                byte[] bytes = Convert.FromBase64String(value.EmployeeSignature);
                var employeeSigns = new EmployeeSign
                {
                    EmployeeId = value.EmployeeId,
                    EmployeeSignature = DocumentZipUnZip.Zip(bytes),
                };
                _context.EmployeeSign.Add(employeeSigns);
                _context.SaveChanges();
                value.Status = "insert";
            }
            return value;
        }
        [HttpGet]
        [Route("GetEmployeeSignature")]
        public IActionResult GetEmployeeSignature(long id)
        {
            var document = _context.EmployeeSign.SingleOrDefault(t => t.EmployeeId == id);
            if (document != null)
            {
                var file = DocumentZipUnZip.Unzip(document.EmployeeSignature);
                if (file != null)
                {
                    Stream stream = new MemoryStream(file);

                    if (stream == null)
                        return NotFound();
                    return Ok(stream);
                }
            }
            return NotFound();
        }
    }
}