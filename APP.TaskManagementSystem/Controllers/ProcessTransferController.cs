﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProcessTransferController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public ProcessTransferController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        #region Process Transfer

        // GET: api/Project
        [HttpGet]
        [Route("GetProcessTransfers")]
        public List<ProcessTransferModel> Get()
        {
            List<ProcessTransferModel> processTransferModels = new List<ProcessTransferModel>();
            var processTransfers = _context.ProcessTransfer.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.ProcessTransferId).AsNoTracking().ToList();

            processTransfers.ForEach(s =>
            {
                var processTransferModel = new ProcessTransferModel
                {
                    ProcessTransferId = s.ProcessTransferId,
                    LocationFrom = s.LocationFrom,
                    LocationTo = s.LocationTo,
                    TransferAction = s.TransferAction,
                    TrolleyPalletNo = s.TrolleyPalletNo,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                processTransferModels.Add(processTransferModel);
            });
            return processTransferModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProcessTransferModel> GetData(SearchModel searchModel)
        {
            var processTransfer = new ProcessTransfer();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).LastOrDefault();
                        break;
                    case "Previous":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProcessTransfer.OrderBy(o => o.ProcessTransferId).FirstOrDefault(s => s.ProcessTransferId > searchModel.Id);
                        break;
                    case "Previous":
                        processTransfer = _context.ProcessTransfer.OrderByDescending(o => o.ProcessTransferId).FirstOrDefault(s => s.ProcessTransferId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProcessTransferModel>(processTransfer);


            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProcessTransfer")]
        public async Task<ProcessTransferModel> Post(ProcessTransferModel value)
        {
            try
            {
                var processTransfer = new ProcessTransfer
                {
                    LocationFrom = value.LocationFrom,
                    LocationTo = value.LocationTo,
                    TransferAction = value.TransferAction,
                    TrolleyPalletNo = value.TrolleyPalletNo,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                _context.ProcessTransfer.Add(processTransfer);
                _context.SaveChanges();
                value.ProcessTransferId = processTransfer.ProcessTransferId;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProcessTransfer")]
        public ProcessTransferModel Put(ProcessTransferModel value)
        {
            var processTransfer = _context.ProcessTransfer.SingleOrDefault(p => p.ProcessTransferId == value.ProcessTransferId);

            processTransfer.LocationFrom = value.LocationFrom;
            processTransfer.LocationTo = value.LocationTo;
            processTransfer.TransferAction = value.TransferAction;
            processTransfer.TrolleyPalletNo = value.TrolleyPalletNo;
            processTransfer.ModifiedByUserId = value.ModifiedByUserID.Value;
            processTransfer.ModifiedDate = DateTime.Now;
            processTransfer.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProcessTransfer")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var processTransfer = _context.ProcessTransfer.Include(i => i.ProcessTransferLine).SingleOrDefault(p => p.ProcessTransferId == id);
                if (processTransfer != null)
                {
                    _context.ProcessTransfer.Remove(processTransfer);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Process Transfer Line
        // GET: api/Project
        [HttpGet]
        [Route("GetProcessTransferLines")]
        public List<ProcessTransferLineModel> GetProcessTransferLinesById(int id)
        {
            List<ProcessTransferLineModel> processTransferLineModels = new List<ProcessTransferLineModel>();
            var processTransferLines = _context.ProcessTransferLine.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Where(p => p.ProcessTransferId == id).OrderByDescending(o => o.ProcessTransferId).AsNoTracking().ToList();

            processTransferLines.ForEach(s =>
            {
                var processTransferLineModel = new ProcessTransferLineModel
                {
                    ProcessTransferLineId = s.ProcessTransferLineId,
                    ProcessTransferId = s.ProcessTransferId,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DrumInfo = s.DrumInfo,
                    ProcessInfo = s.ProcessInfo,
                    Weight = s.Weight,
                };
                processTransferLineModels.Add(processTransferLineModel);
            });
            return processTransferLineModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertProcessTransferLine")]
        public async Task<ProcessTransferLineModel> Post(ProcessTransferLineModel value)
        {
            try
            {
                var processTransferLine = new ProcessTransferLine
                {
                    ProcessTransferId = value.ProcessTransferId,
                    DrumInfo = value.DrumInfo,
                    ProcessInfo = value.ProcessInfo,
                    Weight = value.Weight,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                _context.ProcessTransferLine.Add(processTransferLine);
                _context.SaveChanges();
                value.ProcessTransferLineId = processTransferLine.ProcessTransferLineId;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProcessTransferLine")]
        public ProcessTransferLineModel Put(ProcessTransferLineModel value)
        {
            var processTransferLine = _context.ProcessTransferLine.SingleOrDefault(p => p.ProcessTransferLineId == value.ProcessTransferLineId);
            processTransferLine.ProcessTransferId = value.ProcessTransferId;
            processTransferLine.DrumInfo = value.DrumInfo;
            processTransferLine.ProcessInfo = value.ProcessInfo;
            processTransferLine.Weight = value.Weight;
            processTransferLine.ModifiedByUserId = value.ModifiedByUserID.Value;
            processTransferLine.ModifiedDate = DateTime.Now;
            processTransferLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProcessTransferLine")]
        public ActionResult<string> DeleteProcessTransferLine(int id)
        {
            try
            {
                var processTransferLine = _context.ProcessTransferLine.SingleOrDefault(p => p.ProcessTransferLineId == id);
                if (processTransferLine != null)
                {
                    _context.ProcessTransferLine.Remove(processTransferLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

    }
}
