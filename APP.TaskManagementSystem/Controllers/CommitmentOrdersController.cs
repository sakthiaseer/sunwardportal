﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommitmentOrdersController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public CommitmentOrdersController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommitmentOrders")]
        public List<CommitmentOrdersModel> Get()
        {

            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            var commitmentOrders = _context.CommitmentOrders
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.CommitmentOrdersLine)
                .OrderByDescending(a => a.CommitmentOrdersId).AsNoTracking()
                .ToList();
            var customerIds = commitmentOrders?.Select(q => q.CustomerId).ToList();
            var CommitmentOrdersIds = commitmentOrders.Select(q => q.CommitmentOrdersId).ToList();
            var producIds = _context.CommitmentOrdersLine.Where(q => CommitmentOrdersIds.Contains(q.CommitmentOrdersId.Value)).AsNoTracking().Select(q => q.ProductId).ToList();
            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            if (producIds != null && producIds.Count > 0)
            {
                genericCodes = _context.GenericCodes.Where(g => producIds.Contains(g.ProductNameId)).AsNoTracking().ToList();
            }
            List<CommitmentOrdersModel> CommitmentOrdersModels = new List<CommitmentOrdersModel>();
            if (commitmentOrders != null && commitmentOrders.Count > 0)
            {
                commitmentOrders.ForEach(s =>
                {
                    CommitmentOrdersModel CommitmentOrdersModel = new CommitmentOrdersModel();
                    CommitmentOrdersModel = new CommitmentOrdersModel
                    {
                        CommitmentOrdersId = s.CommitmentOrdersId,
                        CompanyId = s.CompanyId,
                        Company = s.Company != null ? s.Company.PlantCode : "",
                        SwreferenceNo = s.SwreferenceNo,
                        Date = s.Date,
                        CustomerId = s.CustomerId,
                        CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                        CustomerRefNo = s.CustomerRefNo,                      
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        SessionId = s.SessionId,

                    };
                    CommitmentOrdersModels.Add(CommitmentOrdersModel);







                });
            }
            return CommitmentOrdersModels;
        }

       
        [HttpPost()]
        [Route("GetCommitmentOrdersData")]
        public ActionResult<CommitmentOrdersModel> GetCommitmentOrdersData(SearchModel searchModel)
        {
            var CommitmentOrders = new CommitmentOrders();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).FirstOrDefault();
                        break;
                    case "Last":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).LastOrDefault();
                        break;
                    case "Next":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).LastOrDefault();
                        break;
                    case "Previous":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).FirstOrDefault();
                        break;
                    case "Last":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).LastOrDefault();
                        break;
                    case "Next":
                        CommitmentOrders = _context.CommitmentOrders.OrderBy(o => o.CommitmentOrdersId).FirstOrDefault(s => s.CommitmentOrdersId > searchModel.Id);
                        break;
                    case "Previous":
                        CommitmentOrders = _context.CommitmentOrders.OrderByDescending(o => o.CommitmentOrdersId).FirstOrDefault(s => s.CommitmentOrdersId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommitmentOrdersModel>(CommitmentOrders);
            return result;
        }

        [HttpPost]
        [Route("InsertCommitmentOrders")]
        public CommitmentOrdersModel Post(CommitmentOrdersModel value)
        {
            var SessionId = Guid.NewGuid();
            var CommitmentOrders = new CommitmentOrders
            {
                CompanyId = value.CompanyId,
                SwreferenceNo = value.SwreferenceNo,
                Date = value.Date,
                CustomerId = value.CustomerId,
                CustomerRefNo = value.CustomerRefNo,               
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };
            _context.CommitmentOrders.Add(CommitmentOrders);
            _context.SaveChanges();
            value.SessionId = SessionId;
            if (value.CompanyId > 0)
            {
                value.Company = _context.Plant.Where(p => p.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();

            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.FirstOrDefault(s => s.UserId == value.AddedByUserID)?.UserName;
            }
            value.CommitmentOrdersId = CommitmentOrders.CommitmentOrdersId;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommitmentOrders")]
        public CommitmentOrdersModel Put(CommitmentOrdersModel value)
        {
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var CommitmentOrders = _context.CommitmentOrders.SingleOrDefault(p => p.CommitmentOrdersId == value.CommitmentOrdersId);
            CommitmentOrders.CompanyId = value.CompanyId;
            CommitmentOrders.SwreferenceNo = value.SwreferenceNo;
            CommitmentOrders.Date = value.Date;
            CommitmentOrders.CustomerId = value.CustomerId;
            CommitmentOrders.CustomerRefNo = value.CustomerRefNo;
            CommitmentOrders.SessionId = value.SessionId;
            CommitmentOrders.StatusCodeId = value.StatusCodeID.Value;
            CommitmentOrders.ModifiedByUserId = value.ModifiedByUserID;
            CommitmentOrders.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (value.CompanyId > 0)
            {
                value.Company = _context.Plant.Where(p => p.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();

            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.FirstOrDefault(s => s.UserId == value.AddedByUserID)?.UserName;
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommitmentOrders")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var CommitmentOrders = _context.CommitmentOrders.Where(p => p.CommitmentOrdersId == id).FirstOrDefault();
                if (CommitmentOrders != null)
                {
                   _context.CommitmentOrders.Remove(CommitmentOrders);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("GetCommitmentOrdersLine")]
        public List<CommitmentOrdersLineModel> GetCommitmentOrdersLine(int? id)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var CommitmentOrdersLines = _context.CommitmentOrdersLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.Product).Where(w => w.CommitmentOrdersId == id).OrderByDescending(a => a.CommitmentOrdersLineId).AsNoTracking().ToList();
            var productGroupings = _context.GenericCodes.AsNoTracking().ToList();
            List<CommitmentOrdersLineModel> CommitmentOrdersLineModels = new List<CommitmentOrdersLineModel>();
            if (CommitmentOrdersLines != null)
            {
                CommitmentOrdersLines.ForEach(s =>
                {
                    CommitmentOrdersLineModel CommitmentOrdersLineModel = new CommitmentOrdersLineModel
                    {
                        CommitmentOrdersLineId = s.CommitmentOrdersLineId,
                        CommitmentOrdersId = s.CommitmentOrdersId,
                        ProductId = s.ProductId,

                        ProductName = s.Product?.Code,
                        CommitmentDate = s.CommitmentDate,
                        Source = s.Source,
                        Quantity = s.Quantity,
                        Uomid = s.Uomid,
                        PackingId = s.PackingId,
                       
                        
                        ShippingTermsId = s.ShippingTermsId,
                        IsTenderExceed = s.IsTenderExceed,
                        IsPlanned = s.IsPlanned,
                        SessionId = s.SessionId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Remarks = s.Remarks,
                    };
                    CommitmentOrdersLineModels.Add(CommitmentOrdersLineModel);
                });
            }
            return CommitmentOrdersLineModels;
        }
     

        // POST: api/User
        [HttpPost]
        [Route("InsertCommitmentOrdersLine")]
        public CommitmentOrdersLineModel InsertCommitmentOrdersLine(CommitmentOrdersLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var CommitmentOrdersLine = new CommitmentOrdersLine
            {
                CommitmentOrdersId = value.CommitmentOrdersId,
                ProductId = value.ProductId,

                Source = value.Source,
                Quantity = value.Quantity,
                Uomid = value.Uomid,
                PackingId = value.PackingId,
                CommitmentDate = value.CommitmentDate,
                ShippingTermsId = value.ShippingTermsId,
                IsTenderExceed = value.IsTenderExceed,
                SessionId = SessionId,
                IsPlanned = value.IsPlanned,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Remarks = value.Remarks,

            };
            _context.CommitmentOrdersLine.Add(CommitmentOrdersLine);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.CommitmentOrdersLineId = CommitmentOrdersLine.CommitmentOrdersLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCommitmentOrdersLine")]
        public CommitmentOrdersLineModel UpdateCommitmentOrdersLine(CommitmentOrdersLineModel value)
        {
            var CommitmentOrdersLine = _context.CommitmentOrdersLine.SingleOrDefault(p => p.CommitmentOrdersLineId == value.CommitmentOrdersLineId);
            CommitmentOrdersLine.ProductId = value.ProductId;
            CommitmentOrdersLine.Source = value.Source;
            CommitmentOrdersLine.Quantity = value.Quantity;
            CommitmentOrdersLine.Uomid = value.Uomid;
            CommitmentOrdersLine.PackingId = value.PackingId;         
            CommitmentOrdersLine.ShippingTermsId = value.ShippingTermsId;
            CommitmentOrdersLine.IsTenderExceed = value.IsTenderExceed;
            CommitmentOrdersLine.SessionId = value.SessionId;
            CommitmentOrdersLine.IsPlanned = value.IsPlanned;
            CommitmentOrdersLine.ModifiedByUserId = value.ModifiedByUserID;
            CommitmentOrdersLine.ModifiedDate = DateTime.Now;
            CommitmentOrdersLine.StatusCodeId = value.StatusCodeID.Value;
            CommitmentOrdersLine.Remarks = value.Remarks;
            CommitmentOrdersLine.CommitmentDate = value.CommitmentDate;
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateCommitmentOrdersLineStatus")]
        public CommitmentOrdersLineModel UpdateCommitmentOrdersLineStatus(CommitmentOrdersLineModel value)
        {
            var CommitmentOrdersLine = _context.CommitmentOrdersLine.SingleOrDefault(p => p.CommitmentOrdersLineId == value.CommitmentOrdersLineId);
          
            CommitmentOrdersLine.IsPlanned = value.IsPlanned;
         
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCommitmentOrdersLine")]
        public void DeleteCommitmentOrdersLine(int id)
        {
            var CommitmentOrdersLine = _context.CommitmentOrdersLine.SingleOrDefault(p => p.CommitmentOrdersLineId == id);
            if (CommitmentOrdersLine != null)
            {                
                _context.CommitmentOrdersLine.Remove(CommitmentOrdersLine);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("GetCommitmentOrdersReportBySearch")]
        public List<CommitmentOrdersReportModel> GetCommitmentOrdersReportBySearch(QuotationHistorySearchModel searchModel)
        {
            List<CommitmentOrdersReportModel> CommitmentOrdersReportModels = new List<CommitmentOrdersReportModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            var quotationHistories = _context.CommitmentOrders
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.CommitmentOrdersLine)
                 .Include("CommitmentOrdersLine.Product")
                .OrderByDescending(a => a.CommitmentOrdersId).AsNoTracking()
                .ToList();
            if (searchModel.CompanyId != null && searchModel.CompanyId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CompanyId == searchModel.CompanyId).ToList();
            }
            if (searchModel.CustomerId != null && searchModel.CustomerId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CustomerId == searchModel.CustomerId).ToList();
            }

            if (searchModel.SwreferenceNo != null && searchModel.SwreferenceNo != "")
            {
                quotationHistories = quotationHistories.Where(s => s.SwreferenceNo.ToLower().Contains(searchModel.SwreferenceNo.ToLower())).ToList();
            }
            if (searchModel.FromMonth != null && searchModel.ToMonth != null)
            {
                quotationHistories = quotationHistories.Where(s => s.Date >= searchModel.FromMonth && s.Date <= searchModel.ToMonth.Value.AddMonths(1).AddDays(-1)).ToList();
            }


            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();

            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            List<CommitmentOrdersModel> CommitmentOrdersModels = new List<CommitmentOrdersModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    CommitmentOrdersReportModel CommitmentOrdersReportModel = new CommitmentOrdersReportModel();
                    if (!s.CommitmentOrdersLine.Any())
                    {
                        CommitmentOrdersReportModel = new CommitmentOrdersReportModel
                        {
                            CommitmentOrdersId = s.CommitmentOrdersId,
                            Company = s.Company != null ? s.Company.PlantCode : "",
                            SwreferenceNo = s.SwreferenceNo,
                            CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                            DateOfOffer = s.Date,
                           
                            CustomerRefNo = s.CustomerRefNo,


                        };
                        CommitmentOrdersReportModels.Add(CommitmentOrdersReportModel);
                    }
                    else if (s.CommitmentOrdersLine.ToList().Count > 0)
                    {
                        s.CommitmentOrdersLine.ToList().ForEach(l =>
                        {
                            CommitmentOrdersReportModel = new CommitmentOrdersReportModel
                            {
                                CommitmentOrdersId = s.CommitmentOrdersId,
                                Company = s.Company != null ? s.Company.PlantCode : "",
                                SwreferenceNo = s.SwreferenceNo,
                                CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                                CommitmentOrdersLineId = l.CommitmentOrdersLineId,
                                ProductName = l.Product?.Code,
                                Qty = l.Quantity,
                                UOM = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == (l.Product?.Uom))?.Value,
                                ProductId = l.ProductId,
                               
                              
                                DateOfOffer = s.Date,
                                IsPlanned = l.IsPlanned,
                                
                                CustomerRefNo = s.CustomerRefNo,

                            };
                            CommitmentOrdersReportModels.Add(CommitmentOrdersReportModel);
                        });
                    }


                });
            }
            if (searchModel.ProductId != null && searchModel.ProductId > 0)
            {
                if (CommitmentOrdersReportModels != null && CommitmentOrdersReportModels.Count > 0)
                {
                    //var CommitmentOrdersIds = _context.CommitmentOrdersLine.Where(q => q.ProductId == searchModel.ProductId).Select(s => s.QutationHistoryId).ToList();
                    CommitmentOrdersReportModels = CommitmentOrdersReportModels.Where(s => s.ProductId == searchModel.ProductId).ToList();
                }
            }
            return CommitmentOrdersReportModels;
        }

        [HttpGet]
        [Route("GetCommitmentOrdersReports")]
        public List<CommitmentOrdersReportModel> GetCommitmentOrdersReports()
        {

            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            //var genericCodes = _context.GenericCodes.AsNoTracking().ToList();
            //var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            var quotationHistories = _context.CommitmentOrders
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.CommitmentOrdersLine)
                .Include("CommitmentOrdersLine.Product")
                .OrderByDescending(a => a.CommitmentOrdersId).AsNoTracking()
                .ToList();
            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();
            var CommitmentOrdersIds = quotationHistories.Select(q => q.CommitmentOrdersId).ToList();
            var producIds = _context.CommitmentOrdersLine.Where(q => CommitmentOrdersIds.Contains(q.CommitmentOrdersId.Value)).AsNoTracking().Select(q => q.ProductId).ToList();
            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            if (producIds != null && producIds.Count > 0)
            {
                genericCodes = _context.GenericCodes.Where(g => producIds.Contains(g.ProductNameId)).AsNoTracking().ToList();
            }
            List<CommitmentOrdersReportModel> CommitmentOrdersReportModels = new List<CommitmentOrdersReportModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    CommitmentOrdersReportModel CommitmentOrdersReportModel = new CommitmentOrdersReportModel();
                    if (!s.CommitmentOrdersLine.Any())
                    {
                        CommitmentOrdersReportModel = new CommitmentOrdersReportModel
                        {
                            CommitmentOrdersId = s.CommitmentOrdersId,
                            Company = s.Company != null ? s.Company.PlantCode : "",
                            SwreferenceNo = s.SwreferenceNo,
                            CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                            DateOfOffer = s.Date,                          
                            CustomerRefNo = s.CustomerRefNo,
                            

                        };
                        CommitmentOrdersReportModels.Add(CommitmentOrdersReportModel);
                    }
                    else if (s.CommitmentOrdersLine.Any())
                    {
                        s.CommitmentOrdersLine.ToList().ForEach(l =>
                        {
                            CommitmentOrdersReportModel = new CommitmentOrdersReportModel
                            {
                                CommitmentOrdersId = s.CommitmentOrdersId,
                                Company = s.Company != null ? s.Company.PlantCode : "",
                                SwreferenceNo = s.SwreferenceNo,
                                CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                                CommitmentOrdersLineId = l.CommitmentOrdersLineId,
                                ProductName = l.Product?.Code,
                                Qty = l.Quantity,
                                UOM = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == l.Product?.Uom)?.Value,
                                CommitmentDate = l.CommitmentDate,
                              
                             
                                DateOfOffer = s.Date,
                                IsPlanned = l.IsPlanned,
                               
                                CustomerRefNo = s.CustomerRefNo,


                            };
                            CommitmentOrdersReportModels.Add(CommitmentOrdersReportModel);
                        });
                    }


                });
            }
            return CommitmentOrdersReportModels;
        }

    }
}
