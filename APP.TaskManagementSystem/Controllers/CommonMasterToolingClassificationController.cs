﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonMasterToolingClassificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonMasterToolingClassificationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonMasterToolingClassification")]
        public List<CommonMasterToolingClassificationModel> Get(int id)
        {
            var commonMasterToolingClassification = _context.CommonMasterToolingClassification
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.ToolInfo)
                .Include(p => p.Profile)
                .Include(l => l.Location)
                .AsNoTracking().ToList();
            List<CommonMasterToolingClassificationModel> commonMasterToolingClassificationModel = new List<CommonMasterToolingClassificationModel>();
            commonMasterToolingClassification.ForEach(s =>
            {
                CommonMasterToolingClassificationModel commonMasterToolingClassificationModels = new CommonMasterToolingClassificationModel
                {
                    CommonMasterToolingClassificationId = s.CommonMasterToolingClassificationId,
                    LocationId = s.LocationId,
                    ToolName = s.ToolName,
                    ToolInfoId = s.ToolInfoId,
                    LocationName = s.Location != null ? s.Location.Name : "",
                    ToolInfoName = s.ToolInfo != null ? s.ToolInfo.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                };
                commonMasterToolingClassificationModel.Add(commonMasterToolingClassificationModels);
            });
            return commonMasterToolingClassificationModel.Where(w => w.ItemClassificationMasterId == id).OrderByDescending(a => a.CommonMasterToolingClassificationId).ToList();
        }
        [HttpPost]
        [Route("InsertCommonMasterToolingClassification")]
        public CommonMasterToolingClassificationModel Post(CommonMasterToolingClassificationModel value)
        {

            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var commonMasterToolingClassification = new CommonMasterToolingClassification
            {
                ToolInfoId = value.ToolInfoId,
                LocationId = value.LocationId,
                ToolName = value.ToolName,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonMasterToolingClassification.Add(commonMasterToolingClassification);
            _context.SaveChanges();
            value.CommonMasterToolingClassificationId = commonMasterToolingClassification.CommonMasterToolingClassificationId;
            value.ProfileReferenceNo = commonMasterToolingClassification.ProfileReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateCommonMasterToolingClassification")]
        public CommonMasterToolingClassificationModel Put(CommonMasterToolingClassificationModel value)
        {
            var commonMasterToolingClassification = _context.CommonMasterToolingClassification.SingleOrDefault(p => p.CommonMasterToolingClassificationId == value.CommonMasterToolingClassificationId);
            commonMasterToolingClassification.ToolInfoId = value.ToolInfoId;
            commonMasterToolingClassification.LocationId = value.LocationId;
            commonMasterToolingClassification.StatusCodeId = value.StatusCodeID.Value;
            commonMasterToolingClassification.ModifiedByUserId = value.ModifiedByUserID;
            commonMasterToolingClassification.ModifiedDate = DateTime.Now;
            commonMasterToolingClassification.ProfileReferenceNo = value.ProfileReferenceNo;
            commonMasterToolingClassification.ToolName = value.ToolName;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonMasterToolingClassification")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonMasterToolingClassification = _context.CommonMasterToolingClassification.Where(p => p.CommonMasterToolingClassificationId == id).FirstOrDefault();
                if (commonMasterToolingClassification != null)
                {
                    _context.CommonMasterToolingClassification.Remove(commonMasterToolingClassification);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}