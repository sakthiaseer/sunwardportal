﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ClassificationProductionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ClassificationProductionController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetClassificationProductionsByRefNo")]
        public List<ClassificationProductionModel> GetClassificationProductionByRefNo(int? id)
        {
            var classificationProduction = _context.ClassificationProduction.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(p => p.Profile).AsNoTracking().ToList();
            List<ClassificationProductionModel> classificationProductionModel = new List<ClassificationProductionModel>();
            classificationProduction.ForEach(s =>
            {
                ClassificationProductionModel classificationProductionModels = new ClassificationProductionModel
                {
                    ClassificationProductionId = s.ClassificationProductionId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileId = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                };
                classificationProductionModel.Add(classificationProductionModels);
            });
            return classificationProductionModel.Where(l => l.ItemClassificationMasterId == id).OrderByDescending(a => a.ClassificationProductionId).ToList();
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertClassificationProduction")]
        public ClassificationProductionModel Post(ClassificationProductionModel value)
        {
            var profileNo = "";
            var itemclassificationName = "";
            if(value.ItemClassificationMasterId>0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            }
            var classificationProduction = new ClassificationProduction
            {
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
            };
            _context.ClassificationProduction.Add(classificationProduction);
            _context.SaveChanges();
            value.ClassificationProductionId = classificationProduction.ClassificationProductionId;
            value.ProfileReferenceNo = classificationProduction.ProfileReferenceNo;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateClassificationProduction")]
        public ClassificationProductionModel Put(ClassificationProductionModel value)
        {
            var classificationProduction = _context.ClassificationProduction.SingleOrDefault(p => p.ClassificationProductionId == value.ClassificationProductionId);
            classificationProduction.ProfileReferenceNo = value.ProfileReferenceNo;
            classificationProduction.ModifiedByUserId = value.ModifiedByUserID;
            classificationProduction.ModifiedDate = DateTime.Now;
            classificationProduction.ProfileId = value.ProfileId;
            classificationProduction.StatusCodeId = value.StatusCodeID.Value;
            classificationProduction.ItemClassificationMasterId = value.ItemClassificationMasterId;

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteClassificationProduction")]
        public void Delete(int id)
        {
            try
            {
                var CompanyListing = _context.ClassificationProduction.SingleOrDefault(p => p.ClassificationProductionId == id);
                if (CompanyListing != null)
                {
                    _context.ClassificationProduction.Remove(CompanyListing);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                var errorMessage = "This record cannot be deleted! Reference to others!";
                throw new AppException(errorMessage, ex);
            }
        }
    }
}