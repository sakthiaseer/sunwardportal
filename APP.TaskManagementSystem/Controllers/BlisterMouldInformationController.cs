﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterMouldInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public BlisterMouldInformationController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetBlisterMouldInformation")]
        public List<BlisterMouldInformationModel> Get()
        {
            List<BlisterMouldInformationModel> blisterMouldInformationModels = new List<BlisterMouldInformationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var commonFiledsProductionMachineList = _context.BlisterMachineCommonFieldMultiple.ToList();
            var blisterMouldInformation = _context.BlisterMouldInformation
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Profile")
                .Include("Vendor")
                .Include("UpperKnurlingCoverage")
                .Include("BlisterMouldInformationDocument.Document").OrderByDescending(o => o.BlisterMouldInformationId).AsNoTracking().ToList();
            //.Select(s => new BlisterMouldInformationModel
            if (blisterMouldInformation != null && blisterMouldInformation.Count > 0)
            {
                blisterMouldInformation.ForEach(s =>
                {
                    BlisterMouldInformationModel blisterMouldInformationModel = new BlisterMouldInformationModel
                    {
                        BlisterMouldInformationId = s.BlisterMouldInformationId,
                        BlistedMouldPartsId = s.BlistedMouldPartsId,
                        ProfileId = s.ProfileId,
                        EngraveId = s.EngraveId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        BlistedMouldPartsName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId).Value : "",
                        NameOfMould = s.NameOfMould,
                        VendorId = s.VendorId,
                        VendorName = s.Vendor != null ? s.Vendor.CompanyName : "",
                        PurchaseDate = s.PurchaseDate,
                        FmdiameterOfMould = s.FmdiameterOfMould,
                        FmdepthOfMould = s.FmdepthOfMould,
                        SmdiameterOfMould = s.SmdiameterOfMould,
                        SmdepthOfMould = s.SmdepthOfMould,
                        NoOfTrack = s.NoOfTrack,
                        CblisterLength = s.CblisterLength,
                        CblisterWidth = s.CblisterWidth,
                        CcuNo = s.CcuNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        
                        CommonFiledsProductionMachineId = commonFiledsProductionMachineList != null ? commonFiledsProductionMachineList.Where(l => l.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(l => l.CommonFiledsProductionMachineId.Value).ToList() : new List<long>(),
                        DocumentAttachmentModel = new BlisterMouldInformationAttachmentModel
                        {
                            BlisterMouldInformationDocumentId = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.BlisterMouldInformationDocumentId).ToList() : new List<long>(),
                            FileName = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.FileName).ToList() : new List<string>(),
                            DocumentIdList = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.DocumentId).ToList() : new List<long>(),
                            ContentTypes = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.ContentType).ToList() : new List<string>(),

                        },
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        FmUpperCupDiameter = s.FmUpperCupDiameter,
                        FmUpperCupDepth = s.FmUpperCupDepth,
                        FmLowerCupShape = s.FmLowerCupShape,
                        FeederId = s.FeederId,
                        BlisterKnurlingPatternId = s.BlisterKnurlingPatternId,
                        UpperKnurlingCoverageId = s.UpperKnurlingCoverageId,
                        LowerCupDiameter = s.LowerCupDiameter,
                        LowerCupDepth = s.LowerCupDepth,
                        Widthtrack = s.Widthtrack,
                        FormatId = s.FormatId,
                        CuNoFormatId = s.CuNoFormatId,
                        Perforation = s.Perforation,
                        SessionId=s.SessionId,
                        PerforationFlag = s.Perforation == true ? "Yes" : "No",
                        UpperKnurlingCoverageName = s.UpperKnurlingCoverage != null ? s.UpperKnurlingCoverage.CodeValue : null,
                        FeederName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId).Value : "",
                        BlisterKnurlingPatternName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId).Value : "",
                        FormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId).Value : "",
                        CuNoFormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId).Value : "",

                    };

                    blisterMouldInformationModels.Add(blisterMouldInformationModel);
                });
            }



            return blisterMouldInformationModels;
        }
        [HttpPost]
        [Route("GetBlisterMouldInformationByRefNo")]
        public List<BlisterMouldInformationModel> GetBlisterMouldInformationByRefNo(RefSearchModel refSearchModel)
        {
            List<BlisterMouldInformationModel> blisterMouldInformationModels = new List<BlisterMouldInformationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var commonFiledsProductionMachineList = _context.BlisterMachineCommonFieldMultiple.ToList();
            var BlisterMouldInformationlist = _context.BlisterMouldInformation
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Profile")
                .Include("Vendor")
                .Include("UpperKnurlingCoverage")
                .Include("BlisterMouldInformationDocument.Document").OrderByDescending(o => o.BlisterMouldInformationId).AsNoTracking().ToList();

            if (BlisterMouldInformationlist != null && BlisterMouldInformationlist.Count > 0)
            {
                BlisterMouldInformationlist.ForEach(s =>
                {
                    BlisterMouldInformationModel blisterMouldInformationModel = new BlisterMouldInformationModel
                    {

                        BlisterMouldInformationId = s.BlisterMouldInformationId,
                        BlistedMouldPartsId = s.BlistedMouldPartsId,
                        ProfileId = s.ProfileId,
                        EngraveId = s.EngraveId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        BlistedMouldPartsName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId).Value : "",
                        NameOfMould = s.NameOfMould,
                        VendorId = s.VendorId,
                        VendorName = s.Vendor != null ? s.Vendor.CompanyName : "",
                        PurchaseDate = s.PurchaseDate,
                        FmdiameterOfMould = s.FmdiameterOfMould,
                        FmdepthOfMould = s.FmdepthOfMould,
                        SmdiameterOfMould = s.SmdiameterOfMould,
                        SmdepthOfMould = s.SmdepthOfMould,
                        NoOfTrack = s.NoOfTrack,
                        CblisterLength = s.CblisterLength,
                        CblisterWidth = s.CblisterWidth,
                        CcuNo = s.CcuNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        CommonFiledsProductionMachineId = commonFiledsProductionMachineList != null ? commonFiledsProductionMachineList.Where(l => l.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(l => l.CommonFiledsProductionMachineId.Value).ToList() : new List<long>(),
                        DocumentAttachmentModel = new BlisterMouldInformationAttachmentModel
                        {
                            BlisterMouldInformationDocumentId = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.BlisterMouldInformationDocumentId).ToList() : new List<long>(),
                            FileName = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.FileName).ToList() : new List<string>(),
                            DocumentIdList = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.DocumentId).ToList() : new List<long>(),
                            ContentTypes = s.BlisterMouldInformationDocument != null ? s.BlisterMouldInformationDocument.Where(c => c.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(c => c.Document.ContentType).ToList() : new List<string>(),

                        },
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        FmUpperCupDiameter = s.FmUpperCupDiameter,
                        FmUpperCupDepth = s.FmUpperCupDepth,
                        FmLowerCupShape = s.FmLowerCupShape,
                        FeederId = s.FeederId,
                        BlisterKnurlingPatternId = s.BlisterKnurlingPatternId,
                        UpperKnurlingCoverageId = s.UpperKnurlingCoverageId,
                        LowerCupDiameter = s.LowerCupDiameter,
                        LowerCupDepth = s.LowerCupDepth,
                        Widthtrack = s.Widthtrack,
                        FormatId = s.FormatId,
                        CuNoFormatId = s.CuNoFormatId,
                        Perforation = s.Perforation,
                        SessionId=s.SessionId,
                        PerforationFlag = s.Perforation == true ? "Yes" : "No",
                        UpperKnurlingCoverageName = s.UpperKnurlingCoverage!=null?s.UpperKnurlingCoverage.CodeValue:null,
                        FeederName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId).Value : "",
                        BlisterKnurlingPatternName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId).Value : "",
                        FormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId).Value : "",
                        CuNoFormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId).Value : "",

                    };

                    blisterMouldInformationModels.Add(blisterMouldInformationModel);
                });
            }


            //     MasterProfileReferenceNo = s.MasterProfileReferenceNo
            // }).OrderByDescending(o => o.BlisterMouldInformationId).ToList();
            if (refSearchModel.IsHeader)
            {
                return blisterMouldInformationModels.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BlisterMouldInformationId).ToList();
            }
            return blisterMouldInformationModels.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BlisterMouldInformationId).ToList();
        }
        [HttpGet]
        [Route("GetBlisterMouldInformationByPartName")]
        public List<BlisterMouldInformationModel> GetBlisterMouldInformationByPartName(string type)
        {
            List<BlisterMouldInformationModel> blisterMouldInformationModels = new List<BlisterMouldInformationModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var commonFiledsProductionMachineList = _context.BlisterMachineCommonFieldMultiple.ToList();
            var blisterMouldInformationlist = _context.BlisterMouldInformation
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Profile")
                .Include("Vendor")
                .Include("UpperKnurlingCoverage")
                .OrderByDescending(o => o.BlisterMouldInformationId).AsNoTracking().ToList();//.Where(l => l.BlistedMouldPartsName.ToLower() == type.ToLower()).OrderByDescending(o => o.BlisterMouldInformationId).ToList();
            if (blisterMouldInformationlist != null && blisterMouldInformationlist.Count > 0)
            {
                blisterMouldInformationlist.ForEach(s =>
                {
                    BlisterMouldInformationModel blisterMouldInformationModel = new BlisterMouldInformationModel
                    {

                        BlisterMouldInformationId = s.BlisterMouldInformationId,
                        BlistedMouldPartsId = s.BlistedMouldPartsId,
                        ProfileId = s.ProfileId,
                        EngraveId = s.EngraveId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        BlistedMouldPartsName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlistedMouldPartsId).Value : "",
                        NameOfMould = s.NameOfMould,
                        VendorId = s.VendorId,
                        VendorName = s.Vendor != null ? s.Vendor.CompanyName : null,
                        PurchaseDate = s.PurchaseDate,
                        FmdiameterOfMould = s.FmdiameterOfMould,
                        FmdepthOfMould = s.FmdepthOfMould,
                        SmdiameterOfMould = s.SmdiameterOfMould,
                        SmdepthOfMould = s.SmdepthOfMould,
                        NoOfTrack = s.NoOfTrack,
                        CblisterLength = s.CblisterLength,
                        CblisterWidth = s.CblisterWidth,
                        CcuNo = s.CcuNo,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        CommonFiledsProductionMachineId = commonFiledsProductionMachineList != null ? commonFiledsProductionMachineList.Where(l => l.BlisterMouldInformationId == s.BlisterMouldInformationId).Select(l => l.CommonFiledsProductionMachineId.Value).ToList() : new List<long>(),
                        FmUpperCupDiameter = s.FmUpperCupDiameter,
                        FmUpperCupDepth = s.FmUpperCupDepth,
                        FmLowerCupShape = s.FmLowerCupShape,
                        FeederId = s.FeederId,
                        BlisterKnurlingPatternId = s.BlisterKnurlingPatternId,
                        UpperKnurlingCoverageId = s.UpperKnurlingCoverageId,
                        LowerCupDiameter = s.LowerCupDiameter,
                        LowerCupDepth = s.LowerCupDepth,
                        Widthtrack = s.Widthtrack,
                        FormatId = s.FormatId,
                        CuNoFormatId = s.CuNoFormatId,
                        Perforation = s.Perforation,
                        SessionId=s.SessionId,
                        PerforationFlag = s.Perforation == true ? "Yes" : "No",
                        UpperKnurlingCoverageName = s.UpperKnurlingCoverage != null ? s.UpperKnurlingCoverage.CodeValue : null,
                        FeederName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FeederId).Value : "",
                        BlisterKnurlingPatternName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BlisterKnurlingPatternId).Value : "",
                        FormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FormatId).Value : "",
                        CuNoFormatName = applicationmasterdetail != null && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.CuNoFormatId).Value : "",
                    };

                    blisterMouldInformationModels.Add(blisterMouldInformationModel);
                });
            }
            return blisterMouldInformationModels.Where(l => l.BlistedMouldPartsName.ToLower() == type.ToLower()).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterMouldInformationModel> GetData(SearchModel searchModel)
        {
            var blisterMouldInformation = new BlisterMouldInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).LastOrDefault();
                        break;
                    case "Next":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).LastOrDefault();
                        break;
                    case "Next":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderBy(o => o.BlisterMouldInformationId).FirstOrDefault(s => s.BlisterMouldInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterMouldInformation = _context.BlisterMouldInformation.OrderByDescending(o => o.BlisterMouldInformationId).FirstOrDefault(s => s.BlisterMouldInformationId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<BlisterMouldInformationModel>(blisterMouldInformation);
            if (result != null)
            {
                var commonFiledsProductionMachineList = _context.BlisterMachineCommonFieldMultiple.ToList();
                result.CommonFiledsProductionMachineId = commonFiledsProductionMachineList.Where(l => l.BlisterMouldInformationId == result.BlisterMouldInformationId).Select(l => l.CommonFiledsProductionMachineId.Value).ToList();

            }
            return result;
        }
        [HttpPost]
        [Route("InsertBlisterMouldInformation")]
        public BlisterMouldInformationModel Post(BlisterMouldInformationModel value)
        {
            var profileNo = "";
            if (value.ProfileId != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.NameOfMould });
            }
            var SessionId = Guid.NewGuid();
            var blisterMouldInformation = new BlisterMouldInformation
            {
                BlistedMouldPartsId = value.BlistedMouldPartsId,
                ProfileId = value.ProfileId,
                EngraveId = value.EngraveId,
                NameOfMould = value.NameOfMould,
                VendorId = value.VendorId,
                PurchaseDate = value.PurchaseDate,
                FmdiameterOfMould = value.FmdiameterOfMould,
                FmdepthOfMould = value.FmdepthOfMould,
                SmdiameterOfMould = value.SmdiameterOfMould,
                SmdepthOfMould = value.SmdepthOfMould,
                NoOfTrack = value.NoOfTrack,
                CblisterLength = value.CblisterLength,
                CblisterWidth = value.CblisterWidth,
                CcuNo = value.CcuNo,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                FmUpperCupDiameter = value.FmUpperCupDiameter,
                FmUpperCupDepth = value.FmUpperCupDepth,
                FmLowerCupShape = value.FmLowerCupShape,
                FeederId = value.FeederId,
                BlisterKnurlingPatternId = value.BlisterKnurlingPatternId,
                UpperKnurlingCoverageId = value.UpperKnurlingCoverageId,
                LowerCupDiameter = value.LowerCupDiameter,
                LowerCupDepth = value.LowerCupDepth,
                Widthtrack = value.Widthtrack,
                FormatId = value.FormatId,
                CuNoFormatId = value.CuNoFormatId,
                Perforation = value.PerforationFlag == "Yes" ? true : false,
                SessionId=SessionId,
                
            };
            _context.BlisterMouldInformation.Add(blisterMouldInformation);
            if (value.CommonFiledsProductionMachineId != null)
            {
                value.CommonFiledsProductionMachineId.ForEach(c =>
                {
                    var blisterMachineCommonFieldMultipleItems = new BlisterMachineCommonFieldMultiple
                    {
                        CommonFiledsProductionMachineId = c,
                    };
                    blisterMouldInformation.BlisterMachineCommonFieldMultiple.Add(blisterMachineCommonFieldMultipleItems);
                });
            }
            var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).ToList();
            if (documents != null)
            {
                documents.ForEach(c =>
                {
                    var doumentsItems = new BlisterMouldInformationDocument
                    {
                        DocumentId = c.DocumentId,
                    };
                    blisterMouldInformation.BlisterMouldInformationDocument.Add(doumentsItems);
                });
            }
            _context.SaveChanges();
            value.MasterProfileReferenceNo = blisterMouldInformation.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = blisterMouldInformation.ProfileLinkReferenceNo;
            value.BlisterMouldInformationId = blisterMouldInformation.BlisterMouldInformationId;
            value.SessionId = SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateBlisterMouldInformation")]
        public BlisterMouldInformationModel Put(BlisterMouldInformationModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var blisterMouldInformation = _context.BlisterMouldInformation.SingleOrDefault(p => p.BlisterMouldInformationId == value.BlisterMouldInformationId);
            blisterMouldInformation.BlistedMouldPartsId = value.BlistedMouldPartsId;
            blisterMouldInformation.ProfileId = value.ProfileId;
            blisterMouldInformation.EngraveId = value.EngraveId;
            blisterMouldInformation.NameOfMould = value.NameOfMould;
            blisterMouldInformation.VendorId = value.VendorId;
            blisterMouldInformation.PurchaseDate = value.PurchaseDate;
            blisterMouldInformation.FmdiameterOfMould = value.FmdiameterOfMould;
            blisterMouldInformation.FmdepthOfMould = value.FmdepthOfMould;
            blisterMouldInformation.SmdiameterOfMould = value.SmdiameterOfMould;
            blisterMouldInformation.SmdepthOfMould = value.SmdepthOfMould;
            blisterMouldInformation.NoOfTrack = value.NoOfTrack;
            blisterMouldInformation.CblisterLength = value.CblisterLength;
            blisterMouldInformation.CblisterWidth = value.CblisterWidth;
            blisterMouldInformation.CcuNo = value.CcuNo;
            blisterMouldInformation.ModifiedByUserId = value.ModifiedByUserID;
            blisterMouldInformation.ModifiedDate = DateTime.Now;
            blisterMouldInformation.StatusCodeId = value.StatusCodeID.Value;
            blisterMouldInformation.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            blisterMouldInformation.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            blisterMouldInformation.FmUpperCupDiameter = value.FmUpperCupDiameter;
            blisterMouldInformation.FmUpperCupDepth = value.FmUpperCupDepth;
            blisterMouldInformation.FmLowerCupShape = value.FmLowerCupShape;
            blisterMouldInformation.FeederId = value.FeederId;
            blisterMouldInformation.BlisterKnurlingPatternId = value.BlisterKnurlingPatternId;
            blisterMouldInformation.UpperKnurlingCoverageId = value.UpperKnurlingCoverageId;
            blisterMouldInformation.LowerCupDiameter = value.LowerCupDiameter;
            blisterMouldInformation.LowerCupDepth = value.LowerCupDepth;
            blisterMouldInformation.Widthtrack = value.Widthtrack;
            blisterMouldInformation.FormatId = value.FormatId;
            blisterMouldInformation.CuNoFormatId = value.CuNoFormatId;
            blisterMouldInformation.SessionId = value.SessionId;
            blisterMouldInformation.Perforation = value.PerforationFlag == "Yes" ? true : false;
            var blisterMachineCommonFieldMultipleRemove = _context.BlisterMachineCommonFieldMultiple.Where(l => l.BlisterMouldInformationId == value.BlisterMouldInformationId).ToList();
            if (blisterMachineCommonFieldMultipleRemove.Count > 0)
            {
                _context.BlisterMachineCommonFieldMultiple.RemoveRange(blisterMachineCommonFieldMultipleRemove);
            }
            if (value.CommonFiledsProductionMachineId != null)
            {
                value.CommonFiledsProductionMachineId.ForEach(c =>
                {
                    var blisterMachineCommonFieldMultipleItems = new BlisterMachineCommonFieldMultiple
                    {
                        CommonFiledsProductionMachineId = c,
                    };
                    blisterMouldInformation.BlisterMachineCommonFieldMultiple.Add(blisterMachineCommonFieldMultipleItems);
                });
            }
            var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(d => d.SessionId == value.SessionID).ToList();
            if (documents != null)
            {
                documents.ForEach(c =>
                {
                    var doumentsItems = new BlisterMouldInformationDocument
                    {
                        DocumentId = c.DocumentId,
                    };
                    blisterMouldInformation.BlisterMouldInformationDocument.Add(doumentsItems);
                });
            }
            _context.SaveChanges();
            value.BlisterMouldInformationId = blisterMouldInformation.BlisterMouldInformationId;

            return value;
        }
        [HttpDelete]
        [Route("RemoveDocumentBlisterMouldInformation")]
        public void RemoveDocumentBlisterMouldInformation(int id)
        {
            var blisterMouldInformation = _context.BlisterMouldInformationDocument.SingleOrDefault(p => p.BlisterMouldInformationDocumentId == id);
            if (blisterMouldInformation != null)
            {
                _context.BlisterMouldInformationDocument.Remove(blisterMouldInformation);
                _context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("DeleteBlisterMouldInformation")]
        public void Delete(int id)
        {
            var blisterMouldInformation = _context.BlisterMouldInformation.SingleOrDefault(p => p.BlisterMouldInformationId == id);
            if (blisterMouldInformation != null)
            {
                var blisterMachineCommonFieldMultipleRemove = _context.BlisterMachineCommonFieldMultiple.Where(s => s.BlisterMouldInformationId == id).ToList();
                if (blisterMachineCommonFieldMultipleRemove != null)
                {
                    _context.BlisterMachineCommonFieldMultiple.RemoveRange(blisterMachineCommonFieldMultipleRemove);
                    _context.SaveChanges();
                }
                var blisterMouldInformationDocument = _context.BlisterMachineCommonFieldMultiple.Where(s => s.BlisterMouldInformationId == id).ToList();
                if (blisterMouldInformationDocument != null)
                {
                    _context.BlisterMachineCommonFieldMultiple.RemoveRange(blisterMouldInformationDocument);
                    _context.SaveChanges();
                }
                _context.BlisterMouldInformation.Remove(blisterMouldInformation);
                _context.SaveChanges();
            }
        }
    }
}