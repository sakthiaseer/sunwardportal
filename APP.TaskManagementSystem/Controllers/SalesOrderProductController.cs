﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesOrderProductController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SalesOrderProductController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        [HttpGet]
        [Route("GetSalesOrderProducts")]
        public List<SalesOrderProductModel> Get(int id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
           
            var salesOrderProducts = _context.SalesOrderProduct
                .Include(p => p.SalesOrder)
                .Include(a => a.AddedByUser)
                .Include(c => c.CrossReferenceProduct)
                .Include(c => c.StatusCode)
                .Include(c => c.PurchaseItem)
                .Include(a => a.ModifiedByUser)
                .Include(s=>s.SalesOrderEntry)
                .Where(p => p.SalesOrderId == id)
                .AsNoTracking()
                .ToList();
            List<SalesOrderProductModel> salesOrderProductModels = new List<SalesOrderProductModel>();
            if(salesOrderProducts!=null && salesOrderProducts.Count>0)
            {
                var ordercurrencyids = salesOrderProducts.Where(s => s.PurchaseItem?.OrderCurrencyId != null).Select(s => s.PurchaseItem?.OrderCurrencyId).ToList();
                var uomids = salesOrderProducts.Where(s => s.PurchaseItem?.Uomid != null).Select(s => s.PurchaseItem?.Uomid).ToList();
                if(ordercurrencyids.Count>0)
                {
                    masterDetailIds.AddRange(ordercurrencyids);
                }
                if(uomids.Count>0)
                {
                    masterDetailIds.AddRange(uomids);
                }
            }
            if(masterDetailIds.Count>0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
            }
            salesOrderProducts.ForEach(s =>
            {
                var OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PurchaseItem?.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                SalesOrderProductModel salesOrderProductModel = new SalesOrderProductModel();
                salesOrderProductModel.UomName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PurchaseItem?.Uomid).Select(a => a.Value).SingleOrDefault() : "";
                salesOrderProductModel.SalesOrderProductId = s.SalesOrderProductId;
                salesOrderProductModel.SalesOrderId = s.SalesOrderId;
                salesOrderProductModel.CrossReferenceProductId = s.CrossReferenceProductId;
                salesOrderProductModel.BalanceQty = s.BalanceQty;
                salesOrderProductModel.SpecialRemarks = s.SpecialRemarks;
                salesOrderProductModel.QtyOrder = s.QtyOrder;
                salesOrderProductModel.PurchaseItemId = s.PurchaseItemId;
                salesOrderProductModel.ProductNo = s.CrossReferenceProduct?.CustomerReferenceNo;
                if(OrderCurrency!="" || s.PurchaseItem?.SellingPrice!=null || s.PurchaseItem.PricePerQty != null)
                {
                    salesOrderProductModel.Price = OrderCurrency + "/" + s.PurchaseItem?.SellingPrice + "/" + s.PurchaseItem?.PricePerQty;
                }
                else
                {
                    salesOrderProductModel.Price = null;
                }
                
                salesOrderProductModel.RequestShipmentDate = s.RequestShipmentDate;
                salesOrderProductModel.QtyIntend = s.QtyIntend;
                salesOrderProductModel.Kivqty = s.Kivqty;
                salesOrderProductModel.StatusCodeID = s.StatusCodeId;
                salesOrderProductModel.StatusCode = s.StatusCode?.CodeValue;
                salesOrderProductModel.AddedByUserID = s.AddedByUserId;
                salesOrderProductModel.AddedByUser = s.AddedByUser?.UserName;
                salesOrderProductModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                salesOrderProductModel.AddedDate = s.AddedDate;
                salesOrderProductModel.ModifiedByUserID = s.ModifiedByUserId;
                salesOrderProductModel.ModifiedDate = s.ModifiedDate;
                salesOrderProductModel.Description = s.CrossReferenceProduct?.Description;
                salesOrderProductModel.SalesOrderEntryId = s.SalesOrderEntryId;
                salesOrderProductModel.ContractNo = s.SalesOrderEntry?.OrderNo;
                salesOrderProductModel.TenderPeriodEnds = s.SalesOrderEntry?.OrderPeriodTo;
                salesOrderProductModels.Add(salesOrderProductModel);
            });
            return salesOrderProductModels.OrderByDescending(a => a.SalesOrderProductId).ToList();
        }

        [HttpPost()]
        [Route("GetSalesOrderProductData")]
        public ActionResult<SalesOrderProductModel> GetSalesOrderProductData(SearchModel searchModel)
        {
            var salesOrderProduct = new SalesOrderProduct();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).FirstOrDefault();
                        break;
                    case "Last":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).LastOrDefault();
                        break;
                    case "Next":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).LastOrDefault();
                        break;
                    case "Previous":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).FirstOrDefault();
                        break;
                    case "Last":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).LastOrDefault();
                        break;
                    case "Next":
                        salesOrderProduct = _context.SalesOrderProduct.OrderBy(o => o.SalesOrderProductId).FirstOrDefault(s => s.SalesOrderProductId > searchModel.Id);
                        break;
                    case "Previous":
                        salesOrderProduct = _context.SalesOrderProduct.OrderByDescending(o => o.SalesOrderProductId).FirstOrDefault(s => s.SalesOrderProductId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SalesOrderProductModel>(salesOrderProduct);
            return result;
        }

        [HttpPost]
        [Route("InsertSalesOrderProduct")]
        public SalesOrderProductModel Post(SalesOrderProductModel value)
        {
            var salesOrderProduct = new SalesOrderProduct();
            salesOrderProduct.SalesOrderId = value.SalesOrderId;
            salesOrderProduct.CrossReferenceProductId = value.CrossReferenceProductId;
            salesOrderProduct.BalanceQty = value.BalanceQty;
            salesOrderProduct.SpecialRemarks = value.SpecialRemarks;
            salesOrderProduct.QtyOrder = value.QtyOrder;
            salesOrderProduct.Uom = value.Uom;
            salesOrderProduct.RequestShipmentDate = value.RequestShipmentDate;
            salesOrderProduct.QtyIntend = value.QtyIntend;
            salesOrderProduct.Kivqty = value.Kivqty;
            salesOrderProduct.StatusCodeId = value.StatusCodeID.Value;
            salesOrderProduct.AddedByUserId = value.AddedByUserID;
            salesOrderProduct.AddedDate = DateTime.Now;
            _context.SalesOrderProduct.Add(salesOrderProduct);
            _context.SaveChanges();
            value.SalesOrderProductId = salesOrderProduct.SalesOrderProductId;
            if(value.CrossReferenceProductId>0)
            {
                value.ProductNo = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.CrossReferenceProductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesOrderProduct")]
        public SalesOrderProductModel Put(SalesOrderProductModel value)
        {
            var salesOrderProduct = _context.SalesOrderProduct.SingleOrDefault(p => p.SalesOrderProductId == value.SalesOrderProductId);
            salesOrderProduct.SalesOrderId = value.SalesOrderId;
            salesOrderProduct.CrossReferenceProductId = value.CrossReferenceProductId;
            salesOrderProduct.BalanceQty = value.BalanceQty;
            salesOrderProduct.SpecialRemarks = value.SpecialRemarks;
            salesOrderProduct.QtyOrder = value.QtyOrder;
            salesOrderProduct.Uom = value.Uom;
            salesOrderProduct.RequestShipmentDate = value.RequestShipmentDate;
            salesOrderProduct.QtyIntend = value.QtyIntend;
            salesOrderProduct.Kivqty = value.Kivqty;
            salesOrderProduct.StatusCodeId = value.StatusCodeID.Value;
            salesOrderProduct.ModifiedByUserId = value.ModifiedByUserID;
            salesOrderProduct.ModifiedDate = value.ModifiedDate.Value;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesOrderProduct")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var salesOrderProduct = _context.SalesOrderProduct.Where(p => p.SalesOrderProductId == id).FirstOrDefault();
                if (salesOrderProduct != null)
                {
                    _context.SalesOrderProduct.Remove(salesOrderProduct);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
