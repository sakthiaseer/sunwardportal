﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NAVItemLinksController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NAVItemLinksController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNAVItemLinkss")]
        public List<NAVItemLinksModel> Get()
        {
            List<NAVItemLinksModel> nAVItemLinksModels = new List<NAVItemLinksModel>();
            var navitemLinks = _context.NavitemLinks
              .Include("AddedByUser")
              .Include("ModifiedByUser")
              .Include("StatusCode")
             .Include(n => n.MyItem).Include(s=>s.SgItem).OrderByDescending(o => o.ItemLinkId).AsNoTracking().ToList();
            if (navitemLinks != null && navitemLinks.Count > 0)
            {
                navitemLinks.ForEach(s =>
                {
                    NAVItemLinksModel nAVItemLinksModel = new NAVItemLinksModel
                    {
                        ItemLinkId = s.ItemLinkId,
                        MyItemId = s.MyItemId,
                        SgItemId = s.SgItemId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        MalaysiaItemNo = s.MyItem != null ? s.MyItem.No : string.Empty,
                        SingaporeItemNo = s.SgItem != null ? s.SgItem.No : string.Empty,
                    };
                    nAVItemLinksModels.Add(nAVItemLinksModel);
                });

            }

            return nAVItemLinksModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NAVItemLinksModel> GetData(SearchModel searchModel)
        {
            var navitemLinks = new NavitemLinks();
            List<Navitems> navItemsList = _context.Navitems.AsNoTracking().ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).FirstOrDefault();
                        break;
                    case "Last":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).LastOrDefault();
                        break;
                    case "Next":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).LastOrDefault();
                        break;
                    case "Previous":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).FirstOrDefault();
                        break;
                    case "Last":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).LastOrDefault();
                        break;
                    case "Next":
                        navitemLinks = _context.NavitemLinks.OrderBy(o => o.ItemLinkId).FirstOrDefault(s => s.ItemLinkId > searchModel.Id);
                        break;
                    case "Previous":
                        navitemLinks = _context.NavitemLinks.OrderByDescending(o => o.ItemLinkId).FirstOrDefault(s => s.ItemLinkId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NAVItemLinksModel>(navitemLinks);
            if (result != null && result.ItemLinkId > 0)
            {
                result.MalaysiaItemNo = navItemsList.Where(a => a.ItemId == result.MyItemId).Select(t => t.No).FirstOrDefault();
                result.SingaporeItemNo = navItemsList.Where(a => a.ItemId == result.SgItemId).Select(t => t.No).FirstOrDefault();
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertNAVItemLinks")]
        public NAVItemLinksModel Post(NAVItemLinksModel value)
        {
            var navitemLinks = new NavitemLinks
            {

                MyItemId = value.MyItemId,
                SgItemId = value.SgItemId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.NavitemLinks.Add(navitemLinks);
            _context.SaveChanges();
            value.ItemLinkId = navitemLinks.ItemLinkId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNAVItemLinks")]
        public NAVItemLinksModel Put(NAVItemLinksModel value)
        {
            var navitemLinks = _context.NavitemLinks.SingleOrDefault(p => p.ItemLinkId == value.ItemLinkId);

            navitemLinks.SgItemId = value.SgItemId;
            navitemLinks.MyItemId = value.MyItemId;
            navitemLinks.ModifiedByUserId = value.ModifiedByUserID;
            navitemLinks.ModifiedDate = DateTime.Now;
            navitemLinks.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNAVItemLinks")]
        public void Delete(int id)
        {
            var navitemLinks = _context.NavitemLinks.SingleOrDefault(p => p.ItemLinkId == id);
            if (navitemLinks != null)
            {
                _context.NavitemLinks.Remove(navitemLinks);
                _context.SaveChanges();
            }
        }
    }
}