﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TenderPeriodPricingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public TenderPeriodPricingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [Route("GetTenderPeriodPricing")]
        public List<TenderPeriodPricingModel> Get()
        {
            var tenderPeriodPricing = _context.TenderPeriodPricing.Include(a => a.AddedByUser).Include(c => c.Contract).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.Customer).AsNoTracking().ToList();

            List<TenderPeriodPricingModel> tenderPeriodPricingModel = new List<TenderPeriodPricingModel>();
            tenderPeriodPricing.ForEach(s =>
            {
                TenderPeriodPricingModel tenderPeriodPricingModels = new TenderPeriodPricingModel
                {
                    TenderPeriodPricingId = s.TenderPeriodPricingId,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.CompanyName,
                    ContractNo = s.Contract?.OrderNo,
                    ContractNoName = s.Contract?.OrderNo,
                    ContractId = s.ContractId,
                    SessionId = s.SessionId,
                    EffectiveMonth = s.EffectiveMonth,
                    EffectiveFrom = s.Contract?.OrderPeriodFrom,
                    EffectiveTo = s.Contract?.OrderPeriodTo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                tenderPeriodPricingModel.Add(tenderPeriodPricingModels);
            });
            return tenderPeriodPricingModel.OrderByDescending(a => a.TenderPeriodPricingId).ToList();
        }
        [HttpGet]
        [Route("GetTenderPeriodPricingByContractId")]
        public List<TenderPeriodPricingModel> GetTenderPeriodPricingByContractId(int id)
        {
            var tenderPeriodPricing = _context.TenderPeriodPricing.Include(a => a.AddedByUser).Include(c => c.Contract).Include(c => c.Contract.BlanketOrderNo).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.Customer).Where(s=>s.ContractId == id).AsNoTracking().ToList();

            List<TenderPeriodPricingModel> tenderPeriodPricingModel = new List<TenderPeriodPricingModel>();
            tenderPeriodPricing.ForEach(s =>
            {
                TenderPeriodPricingModel tenderPeriodPricingModels = new TenderPeriodPricingModel
                {
                    TenderPeriodPricingId = s.TenderPeriodPricingId,
                    CustomerId = s.CustomerId,
                    CustomerName = s.Customer?.CompanyName,
                    ContractNo = s.Contract?.BlanketOrderNo?.OrderNo,
                    ContractNoName = s.Contract?.OrderNo,
                    ContractId = s.ContractId,
                    EffectiveMonth = s.EffectiveMonth,
                    EffectiveFrom = s.Contract?.OrderPeriodFrom,
                    EffectiveTo = s.Contract?.OrderPeriodTo,
                    StatusCodeID = s.StatusCodeId,
                    SessionId  = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    StatusCode = s.StatusCode?.CodeValue,
                };
                tenderPeriodPricingModel.Add(tenderPeriodPricingModels);
            });
            return tenderPeriodPricingModel.OrderByDescending(a => a.TenderPeriodPricingId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TenderPeriodPricingModel> GetData(SearchModel searchModel)
        {
            var tenderPeriodPricing = new TenderPeriodPricing();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).LastOrDefault();
                        break;
                    case "Next":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).LastOrDefault();
                        break;
                    case "Previous":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).LastOrDefault();
                        break;
                    case "Next":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderBy(o => o.TenderPeriodPricingId).FirstOrDefault(s => s.TenderPeriodPricingId > searchModel.Id);
                        break;
                    case "Previous":
                        tenderPeriodPricing = _context.TenderPeriodPricing.Include(c => c.Contract).OrderByDescending(o => o.TenderPeriodPricingId).FirstOrDefault(s => s.TenderPeriodPricingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TenderPeriodPricingModel>(tenderPeriodPricing);
            if (result != null)
            {
                result.ContractNo = tenderPeriodPricing.Contract?.OrderNo;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertTenderPeriodPricing")]
        public TenderPeriodPricingModel Post(TenderPeriodPricingModel value)
        {
            if(value.ContractId>0)
            {
                value.CustomerId = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.ContractId).Select(s => s.CustomerId).FirstOrDefault();
            }
            var sessionId = Guid.NewGuid();
            var tenderPeriodPricing = new TenderPeriodPricing
            {
                CustomerId = value.CustomerId,
                ContractId = value.ContractId,
                EffectiveMonth = value.EffectiveMonth,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.TenderPeriodPricing.Add(tenderPeriodPricing);
            _context.SaveChanges();
            if(value.ContractId>0)
            {
                var selcontractNo = _context.SalesOrderEntry.Include(s => s.BlanketOrderNo).Where(s => s.SalesOrderEntryId == value.ContractId).FirstOrDefault();
                if(selcontractNo!=null)
                {
                    value.ContractNo = selcontractNo.BlanketOrderNo?.OrderNo;
                }
            }
            if(value.CustomerId>0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if(value.StatusCodeID>0)
            {
                value.StatusCode = _context.CodeMaster.Where(c => c.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault();
            }
            value.TenderPeriodPricingId = tenderPeriodPricing.TenderPeriodPricingId;
            value.SessionId = tenderPeriodPricing.SessionId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTenderPeriodPricing")]
        public TenderPeriodPricingModel Put(TenderPeriodPricingModel value)
        {
            if (value.ContractId > 0)
            {
                value.CustomerId = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.ContractId).Select(s => s.CustomerId).FirstOrDefault();
            }
            var tenderPeriodPricing = _context.TenderPeriodPricing.SingleOrDefault(p => p.TenderPeriodPricingId == value.TenderPeriodPricingId);
            tenderPeriodPricing.CustomerId = value.CustomerId;
            tenderPeriodPricing.ContractId = value.ContractId;
            tenderPeriodPricing.EffectiveMonth = value.EffectiveMonth;
            tenderPeriodPricing.ModifiedDate = DateTime.Now;
            tenderPeriodPricing.ModifiedByUserId = value.ModifiedByUserID;
            tenderPeriodPricing.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            if (value.ContractId > 0)
            {
                var selcontractNo = _context.SalesOrderEntry.Include(s => s.BlanketOrderNo).Where(s => s.SalesOrderEntryId == value.ContractId).FirstOrDefault();
                if (selcontractNo != null)
                {
                    value.ContractNo = selcontractNo.BlanketOrderNo?.OrderNo;
                }
            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.StatusCodeID > 0)
            {
                value.StatusCode = _context.CodeMaster.Where(c => c.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault();
            }
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTenderPeriodPricing")]
        public void Delete(int id)
        {
            var tenderPeriodPricing = _context.TenderPeriodPricing.SingleOrDefault(p => p.TenderPeriodPricingId == id);
            if (tenderPeriodPricing != null)
            {
                var tenderPeriodPricingLine = _context.TenderPeriodPricingLine.Where(p => p.TenderPeriodPricingId == id).ToList();
                if (tenderPeriodPricingLine != null)
                {
                    _context.TenderPeriodPricingLine.RemoveRange(tenderPeriodPricingLine);
                    _context.SaveChanges();
                }
                _context.TenderPeriodPricing.Remove(tenderPeriodPricing);
                _context.SaveChanges();
            }
        }
    }
}