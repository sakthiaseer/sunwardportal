﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class HRExternalPersonalController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public HRExternalPersonalController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetHRExternalPersonal")]
        public List<HRExternalPersonalModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var hrexternalPersonal = _context.HrexternalPersonal.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.CompanyListing).AsNoTracking().ToList();
            List<HRExternalPersonalModel> hrexternalPersonalModels = new List<HRExternalPersonalModel>();
            hrexternalPersonal.ForEach(s =>
            {
                HRExternalPersonalModel hrexternalPersonalModel = new HRExternalPersonalModel();
                hrexternalPersonalModel.HrexternalPersonalId = s.HrexternalPersonalId;
                hrexternalPersonalModel.CompanyListingId = s.CompanyListingId;
                hrexternalPersonalModel.FirstName = s.FirstName;
                hrexternalPersonalModel.LastName = s.LastName;
                hrexternalPersonalModel.NickName = s.NickName;
                hrexternalPersonalModel.StartDate = s.StartDate;
                hrexternalPersonalModel.EndDate = s.EndDate;
                hrexternalPersonalModel.Gender = s.Gender;
                hrexternalPersonalModel.Icinformation = s.Icinformation;
                hrexternalPersonalModel.HandphoneNo = s.HandphoneNo;
                hrexternalPersonalModel.PurposeForExternalPersonalId = s.PurposeForExternalPersonalId;
                hrexternalPersonalModel.CompanyListingName = s.CompanyListing != null ? s.CompanyListing.CompanyName : null;
                hrexternalPersonalModel.PurposeForExternalPersonal = s.PurposeForExternalPersonalId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.PurposeForExternalPersonalId).Select(m => m.Value).FirstOrDefault() : "";
                hrexternalPersonalModel.AddedByUserID = s.AddedByUserId;
                hrexternalPersonalModel.ModifiedByUserID = s.ModifiedByUserId;
                hrexternalPersonalModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                hrexternalPersonalModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                hrexternalPersonalModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                hrexternalPersonalModel.StatusCodeID = s.StatusCodeId;
                hrexternalPersonalModel.AddedDate = s.AddedDate;
                hrexternalPersonalModel.ModifiedDate = s.ModifiedDate;
                hrexternalPersonalModels.Add(hrexternalPersonalModel);
            });
            return hrexternalPersonalModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<HRExternalPersonalModel> GetData(SearchModel searchModel)
        {
            var hrexternalPersonal = new HrexternalPersonal();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).FirstOrDefault();
                        break;
                    case "Last":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).LastOrDefault();
                        break;
                    case "Next":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).LastOrDefault();
                        break;
                    case "Previous":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).FirstOrDefault();
                        break;
                    case "Last":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).LastOrDefault();
                        break;
                    case "Next":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderBy(o => o.HrexternalPersonalId).FirstOrDefault(s => s.HrexternalPersonalId > searchModel.Id);
                        break;
                    case "Previous":
                        hrexternalPersonal = _context.HrexternalPersonal.OrderByDescending(o => o.HrexternalPersonalId).FirstOrDefault(s => s.HrexternalPersonalId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<HRExternalPersonalModel>(hrexternalPersonal);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertHRExternalPersonal")]
        public HRExternalPersonalModel Post(HRExternalPersonalModel value)
        {
            var hrexternalPersonal = new HrexternalPersonal
            {
                CompanyListingId = value.CompanyListingId,
                PurposeForExternalPersonalId = value.PurposeForExternalPersonalId,
                FirstName = value.FirstName,
                LastName = value.LastName,
                NickName = value.NickName,
                Gender = value.Gender,
                StartDate = value.StartDate,
                EndDate = value.EndDate,
                Icinformation = value.Icinformation,
                HandphoneNo = value.HandphoneNo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.HrexternalPersonal.Add(hrexternalPersonal);
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateHRExternalPersonal")]
        public HRExternalPersonalModel Put(HRExternalPersonalModel value)
        {
            var hrexternalPersonal = _context.HrexternalPersonal.SingleOrDefault(s => s.HrexternalPersonalId == value.HrexternalPersonalId);
            hrexternalPersonal.CompanyListingId = value.CompanyListingId;
            hrexternalPersonal.PurposeForExternalPersonalId = value.PurposeForExternalPersonalId;
            hrexternalPersonal.FirstName = value.FirstName;
            hrexternalPersonal.LastName = value.LastName;
            hrexternalPersonal.NickName = value.NickName;
            hrexternalPersonal.Gender = value.Gender;
            hrexternalPersonal.Icinformation = value.Icinformation;
            hrexternalPersonal.HandphoneNo = value.HandphoneNo;
            hrexternalPersonal.StatusCodeId = value.StatusCodeID.Value;
            hrexternalPersonal.ModifiedByUserId = value.ModifiedByUserID;
            hrexternalPersonal.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteHRExternalPersonal")]
        public void Delete(int id)
        {
            try
            {
                var hrexternalPersonal = _context.HrexternalPersonal.SingleOrDefault(p => p.HrexternalPersonalId == id);
                if (hrexternalPersonal != null)
                {
                    _context.HrexternalPersonal.Remove(hrexternalPersonal);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                var errorMessage = "This record cannot be deleted!";
                throw new AppException(errorMessage, ex);
            }
        }
    }

}