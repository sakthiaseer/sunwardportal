﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ContractDistributionSalesEntryLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ContractDistributionSalesEntryLineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetContractDistributionItems")]
        public List<ContractDistributionSalesEntryLineModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ContractDistributionSalesEntryLines = _context.ContractDistributionSalesEntryLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(m => m.Socustomer)
                .Include(m => m.PurchaseItemSalesEntryLine)
                .Include(m => m.PurchaseItemSalesEntryLine.SobyCustomers)
                .Include(s => s.StatusCode).Where(s => s.PurchaseItemSalesEntryLineId == id).AsNoTracking()
                .ToList();
            List<ContractDistributionSalesEntryLineModel> ContractDistributionSalesEntryLineModels = new List<ContractDistributionSalesEntryLineModel>();
            ContractDistributionSalesEntryLines.ForEach(s =>
            {
                ContractDistributionSalesEntryLineModel ContractDistributionSalesEntryLineModel = new ContractDistributionSalesEntryLineModel
                {
                    ContractDistributionSalesEntryLineID = s.ContractDistributionSalesEntryLineId,
                    NoOfLots = s.NoOfLots,
                    TotalQty = s.TotalQty,
                    PONumber = s.Ponumber,
                    Uomid = s.Uomid,
                    SOCustomerID = s.SocustomerId,
                    PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId,
                    SessionID = s.SessionId,
                    CustomerName = s.Socustomer?.CompanyName,
                    FrequencyID = s.FrequencyId,
                    PerFrequencyQty = s.PerFrequencyQty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    UOM = masterDetailList != null && s.PurchaseItemSalesEntryLine?.SobyCustomers?.PerUomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.PurchaseItemSalesEntryLine?.SobyCustomers?.PerUomId).Select(a => a.Value).SingleOrDefault() : "",

                };
                ContractDistributionSalesEntryLineModels.Add(ContractDistributionSalesEntryLineModel);
            });
            return ContractDistributionSalesEntryLineModels.OrderByDescending(a => a.ContractDistributionSalesEntryLineID).ToList();
        }

        [HttpGet]
        [Route("GetContractDistributionItemsAll")]
        public List<ContractDistributionSalesEntryLineModel> GetContractDistributionItemsAll()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ContractDistributionSalesEntryLines = _context.ContractDistributionSalesEntryLine
                .Include(s => s.StatusCode).AsNoTracking()
                .ToList();
            List<ContractDistributionSalesEntryLineModel> ContractDistributionSalesEntryLineModels = new List<ContractDistributionSalesEntryLineModel>();
            ContractDistributionSalesEntryLines.ForEach(s =>
            {
                ContractDistributionSalesEntryLineModel ContractDistributionSalesEntryLineModel = new ContractDistributionSalesEntryLineModel
                {
                    ContractDistributionSalesEntryLineID = s.ContractDistributionSalesEntryLineId,
                    PONumber = s.Ponumber,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",


                };
                ContractDistributionSalesEntryLineModels.Add(ContractDistributionSalesEntryLineModel);
            });
            return ContractDistributionSalesEntryLineModels.OrderByDescending(a => a.ContractDistributionSalesEntryLineID).ToList();
        }

        [HttpPost()]
        [Route("GetContractDistributionData")]
        public ActionResult<ContractDistributionSalesEntryLineModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var ContractDistributionSalesEntryLine = new ContractDistributionSalesEntryLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).LastOrDefault();
                        break;
                    case "Previous":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderBy(o => o.ContractDistributionSalesEntryLineId).FirstOrDefault(s => s.ContractDistributionSalesEntryLineId > searchModel.Id);
                        break;
                    case "Previous":
                        ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.OrderByDescending(o => o.ContractDistributionSalesEntryLineId).FirstOrDefault(s => s.ContractDistributionSalesEntryLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ContractDistributionSalesEntryLineModel>(ContractDistributionSalesEntryLine);
            return result;
        }

        [HttpPost]
        [Route("InsertContractDistribution")]
        public ContractDistributionSalesEntryLineModel Post(ContractDistributionSalesEntryLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var companyListing = _context.CompanyListing.ToList();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ContractDistributionSalesEntryLine = new ContractDistributionSalesEntryLine
            {
                TotalQty = value.TotalQty,
                NoOfLots = value.NoOfLots,
                Uomid = value.Uomid,
                Ponumber = value.PONumber,
                SocustomerId = value.SOCustomerID,
                PurchaseItemSalesEntryLineId = value.PurchaseItemSalesEntryLineID,
                SessionId = SessionId,
                FrequencyId = value.FrequencyID,
                PerFrequencyQty = value.PerFrequencyQty,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.ContractDistributionSalesEntryLine.Add(ContractDistributionSalesEntryLine);
            _context.SaveChanges();
            value.SessionID = ContractDistributionSalesEntryLine.SessionId;
            var company = companyListing.Where(c => c.CompanyListingId == value.SOCustomerID).FirstOrDefault();
            if (company != null)
            {
                value.CustomerName = company.CompanyName;
            }
            value.AddedByUser = _context.ApplicationUser?.Where(s => s.UserId == value.AddedByUserID).Select(s => s.UserName).FirstOrDefault();
            value.ContractDistributionSalesEntryLineID = ContractDistributionSalesEntryLine.ContractDistributionSalesEntryLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateContractDistribution")]
        public ContractDistributionSalesEntryLineModel Put(ContractDistributionSalesEntryLineModel value)
        {
            var companyListing = _context.CompanyListing.ToList();
            var contractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.SingleOrDefault(p => p.ContractDistributionSalesEntryLineId == value.ContractDistributionSalesEntryLineID);
            contractDistributionSalesEntryLine.SocustomerId = value.SOCustomerID;
            contractDistributionSalesEntryLine.PurchaseItemSalesEntryLineId = value.PurchaseItemSalesEntryLineID;
            contractDistributionSalesEntryLine.TotalQty = value.TotalQty;
            contractDistributionSalesEntryLine.Uomid = value.Uomid;
            contractDistributionSalesEntryLine.NoOfLots = value.NoOfLots;
            contractDistributionSalesEntryLine.Ponumber = value.PONumber;
            contractDistributionSalesEntryLine.PerFrequencyQty = value.PerFrequencyQty;
            contractDistributionSalesEntryLine.FrequencyId = value.FrequencyID;
            contractDistributionSalesEntryLine.SessionId = value.SessionID;
            contractDistributionSalesEntryLine.ModifiedByUserId = value.ModifiedByUserID;
            contractDistributionSalesEntryLine.ModifiedDate = DateTime.Now;
            contractDistributionSalesEntryLine.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            var company = companyListing.Where(c => c.CompanyListingId == value.SOCustomerID).FirstOrDefault();
            if (company != null)
            {
                value.CustomerName = company.CompanyName;
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteContractDistribution")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var ContractDistributionSalesEntryLine = _context.ContractDistributionSalesEntryLine.Where(p => p.ContractDistributionSalesEntryLineId == id).FirstOrDefault();
                if (ContractDistributionSalesEntryLine != null)
                {

                    _context.ContractDistributionSalesEntryLine.Remove(ContractDistributionSalesEntryLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }





        [HttpGet]
        [Route("GetContractDistributionDocument")]
        public List<ContractDistributionAttachmentModel> Get(Guid? SessionId)
        {
            List<ContractDistributionAttachmentModel> ContractDistributionSalesEntryLineLineDocumentModels = new List<ContractDistributionAttachmentModel>();
            var ContractDistributionSalesEntryLinelineDocuments = _context.ContractDistributionAttachment.Select(s => new { s.ContractDistributionAttachmentId, s.FileName, s.ContentType, s.FileSize, s.UploadDate, s.SessionId }).Where(f => f.SessionId == SessionId).AsNoTracking().ToList();
            if (ContractDistributionSalesEntryLinelineDocuments != null && ContractDistributionSalesEntryLinelineDocuments.Count > 0)
            {
                ContractDistributionSalesEntryLinelineDocuments.ForEach(s =>
                {
                    ContractDistributionAttachmentModel ContractDistributionSalesEntryLineLineDocumentModel = new ContractDistributionAttachmentModel
                    {
                        ContractDistributionAttachmentId = s.ContractDistributionAttachmentId,
                        FileName = s.FileName,
                        ContentType = s.ContentType,
                        FileSize = s.FileSize,
                        UploadDate = s.UploadDate,
                        SessionId = s.SessionId,
                    };
                    ContractDistributionSalesEntryLineLineDocumentModels.Add(ContractDistributionSalesEntryLineLineDocumentModel);
                });

            }


            return ContractDistributionSalesEntryLineLineDocumentModels;
        }

        [HttpDelete]
        [Route("DeleteContractDistributionDocuments")]
        public void DeleteDocuments(int id)
        {
            try
            {

                var query = string.Format("delete from ContractDistributionAttachment Where ContractDistributionAttachmentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
                //var ContractDistributionSalesEntryLinelineDocument = _context.ContractDistributionAttachment.FirstOrDefault(f => f.ContractDistributionAttachmentId == id);
                //if (ContractDistributionSalesEntryLinelineDocument != null)
                //{
                //    _context.ContractDistributionAttachment.Remove(ContractDistributionSalesEntryLinelineDocument);
                //    _context.SaveChanges();

                //}
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }

        [HttpPost()]
        [Route("GetDistributionTenderQty")]
        public ActionResult<ContractDistributionSalesEntryLineModel> GetDistributionTenderQty(GetTenderQtyModel getTenderQtyModel)
        {
            ContractDistributionSalesEntryLineModel contractDistributionSalesEntryLineModel = new ContractDistributionSalesEntryLineModel();
            var tenderInfoqty = _context.ContractDistributionSalesEntryLine.Include(c => c.PurchaseItemSalesEntryLine).Where(s => s.PurchaseItemSalesEntryLine.SalesOrderEntryId == getTenderQtyModel.ContractId && s.SocustomerId == getTenderQtyModel.CustomerId && s.PurchaseItemSalesEntryLine.SobyCustomersId == getTenderQtyModel.ProductId).Max(s => s.TotalQty);

            if (tenderInfoqty != null)
            {
                contractDistributionSalesEntryLineModel.TotalQty = tenderInfoqty;
                contractDistributionSalesEntryLineModel.SellingPrice = _context.ContractDistributionSalesEntryLine.Include(c => c.PurchaseItemSalesEntryLine).Where(s => s.PurchaseItemSalesEntryLine.SalesOrderEntryId == getTenderQtyModel.ContractId && s.SocustomerId == getTenderQtyModel.CustomerId && s.PurchaseItemSalesEntryLine.SobyCustomersId == getTenderQtyModel.ProductId && s.TotalQty == tenderInfoqty).FirstOrDefault().PurchaseItemSalesEntryLine?.SellingPrice;


            }
            return contractDistributionSalesEntryLineModel;
        }
        [HttpPost]
        [Route("UploadContractDistributionDocuments")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {

            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var ContractDistributionSalesEntryLine = new ContractDistributionAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    //Description = files.desc
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,

                };
                _context.ContractDistributionAttachment.Add(ContractDistributionSalesEntryLine);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("DownLoadContractDistributionDocument")]
        public IActionResult DownLoadContractDistributionSalesEntryLineLineDocument(long id)
        {
            var document = _context.ContractDistributionAttachment.SingleOrDefault(t => t.ContractDistributionAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }


        [HttpGet]
        [Route("GetContractDistributionDocumentById")]
        public ContractDistributionAttachmentModel GetContractDistributionSalesEntryLineLineDocumentById(long id)
        {
            ContractDistributionAttachmentModel ContractDistributionSalesEntryLineLineDocumentModel = new ContractDistributionAttachmentModel();
            var document = _context.ContractDistributionAttachment.FirstOrDefault(t => t.ContractDistributionAttachmentId == id);
            if (document != null)
            {


                ContractDistributionSalesEntryLineLineDocumentModel.ContractDistributionAttachmentId = document.ContractDistributionAttachmentId;
                ContractDistributionSalesEntryLineLineDocumentModel.SessionId = document.SessionId;
                ContractDistributionSalesEntryLineLineDocumentModel.ContentType = document.ContentType;
                ContractDistributionSalesEntryLineLineDocumentModel.FileName = document.FileName;
                ContractDistributionSalesEntryLineLineDocumentModel.FileData = document.FileData;


            }
            return ContractDistributionSalesEntryLineLineDocumentModel;
        }




        [HttpPost]
        [Route("UploadContractDistributionSalesEntryAttachments")]
        public IActionResult UploadContractDistributionSalesEntryAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new ContractDistributionAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId
                };

                _context.ContractDistributionAttachment.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetContractDistributionSalesEntryAttachmentBySessionID")]
        public List<DocumentsModel> GetContractDistributionSalesEntryAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.ContractDistributionAttachment.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.ContractDistributionAttachmentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadContractDistributionSalesEntryAttachment")]
        public IActionResult DownLoadContractDistributionSalesEntryAttachment(long id)
        {
            var document = _context.ContractDistributionAttachment.SingleOrDefault(t => t.ContractDistributionAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteContractDistributionSalesEntryAttachment")]
        public void DeleteContractDistributionSalesEntryAttachment(int id)
        {
            var query = _context.ContractDistributionAttachment.SingleOrDefault(p => p.ContractDistributionAttachmentId == id);
            if (query != null)
            {
                _context.ContractDistributionAttachment.Remove(query);
                _context.SaveChanges();
            }
        }



    }
}