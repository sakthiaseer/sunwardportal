﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NAV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActivityRoutineAppController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public ProductionActivityRoutineAppController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [HttpGet]
        [Route("GetProductionActivityRoutineAppLine")]
        public List<ProductionActivityRoutineAppModel> GetProductionActivityRoutineAppLine(long? id, long? userId, long? appId, long? locationId, string type)
        {
            var productActivityAppQuery = _context.ProductionActivityRoutineAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityRoutineApp)
                .Include(a => a.ProductionActivityRoutineApp.Company)
                .Include(a => a.Location)
                .Include(a => a.StatusCode)
                 .Include(a => a.NavprodOrderLine)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.VisaMaster)
                .Include(a => a.RoutineStatus)
                .Include(a => a.RoutineInfoMultiple)
                 .Where(w => w.ProductionActivityRoutineApp.CompanyId == id);
            if (appId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ManufacturingProcessChildId == appId && w.ProductionActivityRoutineApp.ManufacturingProcessChildId == appId);
            }
            if (locationId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.LocationId == locationId);
            }
            if (!string.IsNullOrEmpty(type))
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ProductionActivityRoutineApp.ProdOrderNo == type);
            }
            //.Where(w => w.ProductionActivityRoutineApp.CompanyId == id && w.ProductionActivityRoutineApp.ManufacturingProcessChildId == appId && w.ManufacturingProcessChildId == appId && w.ProductionActivityRoutineApp.ProdOrderNo == type && w.LocationId == (locationId == -1 ? null : locationId));
            var productActivityApps = productActivityAppQuery.ToList();
            List<ProductionActivityRoutineAppModel> productActivityAppModels = new List<ProductionActivityRoutineAppModel>();
            var productLineIds = productActivityApps.Select(s => s.ProductionActivityRoutineAppLineId).ToList();
            var routineInfoMultipleLists = new List<RoutineInfoMultiple>();
            List<long?> applicationMasterCodeIds = new List<long?> { 331, 334, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine").ToList();
                var addedIds = productActivityApps.ToList().Select(s => s.AddedByUserId).ToList();
                addedIds.Add(userId);
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", addedIds.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.UserID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.DepartmentName,
                    s.PlantID,
                    s.DepartmentID
                }).ToList();
                var loginUser = employee.FirstOrDefault(w => w.UserID == userId)?.DepartmentID;
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.FilePath, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();


                productActivityApps.ForEach(s =>
                {
                    var checkSameDept = false;

                    if (s.AddedByUserId == userId)
                    {
                        checkSameDept = true;
                    }
                    else
                    {
                        var employeeDep = employee.FirstOrDefault(w => w.UserID == s.AddedByUserId)?.DepartmentID;
                        if (loginUser != null && employeeDep != null && loginUser == employeeDep)
                        {
                            checkSameDept = true;
                        }
                    }
                    if (checkSameDept == true)
                    {
                        List<long> responsibilityUsers = new List<long>();
                        if (s.ProductActivityCaseId > 0)
                        {
                            var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                            var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                            var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                            var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                            var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                            var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                            var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                            if (appUserss != null && appUserss.Count > 0)
                            {
                                var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                                if (usersIdsBy != null && usersIdsBy.Count > 0)
                                {
                                    responsibilityUsers.AddRange(usersIdsBy);
                                }
                            }
                        }

                        ProductionActivityRoutineAppModel productActivityApp = new ProductionActivityRoutineAppModel();
                        productActivityApp.ResponsibilityUsers = responsibilityUsers;
                        productActivityApp.Type = "Production Routine";
                        productActivityApp.VisaMasterId = s.VisaMasterId;
                        //productActivityApp.VisaMaster = s.VisaMaster?.Value;
                        productActivityApp.RoutineInfoIds = s.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Select(r => r.RoutineInfoId.Value).ToList();
                        productActivityApp.RoutineStatusId = s.RoutineStatusId;
                        productActivityApp.RoutineStatus = s.RoutineStatus?.Value;
                        productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                        productActivityApp.TicketNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                        productActivityApp.LocationName = s.Location?.Name + "||" + s.ProductionActivityRoutineApp?.BatchNo + "||" + s.ProductionActivityRoutineApp?.ProdOrderNo;
                        productActivityApp.ProdOrderNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                        productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                        productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                        productActivityApp.ProductionActivityRoutineAppId = s.ProductionActivityRoutineApp.ProductionActivityRoutineAppId;
                        productActivityApp.Comment = s.ProductionActivityRoutineApp?.Comment;
                        productActivityApp.LineComment = s.Comment;
                        productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                        productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                        productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                        productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                        productActivityApp.CompanyId = s.ProductionActivityRoutineApp?.CompanyId;
                        productActivityApp.CompanyName = s.ProductionActivityRoutineApp?.Company?.PlantCode;
                        productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                        productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                        productActivityApp.ActionDropdown = s.ActionDropdown;
                        productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                        productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                        productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                        productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                        productActivityApp.StatusCodeID = s.StatusCodeId;
                        productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                        productActivityApp.ModifiedDate = s.ModifiedDate;
                        productActivityApp.SessionId = s.ProductionActivityRoutineApp?.SessionId;
                        productActivityApp.LineSessionId = s.SessionId;
                        productActivityApp.AddedByUserID = s.AddedByUserId;
                        productActivityApp.AddedDate = s.AddedDate;
                        productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                        productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                        productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                        productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                        productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                        productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                        productActivityApp.LocationToSaveId = s.ProductActivityCaseLine?.LocationToSaveId;
                        productActivityApp.QaCheck = s.QaCheck;
                        productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                        productActivityApp.Description = s.NavprodOrderLine?.Description;
                        productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                        productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                        productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                        productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                        productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                        productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                        productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                        productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                        productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                        productActivityApp.TopicId = s.TopicId;
                        productActivityApp.Type = "Production Routine";
                        productActivityApp.LocationId = s.ProductionActivityRoutineApp?.LocationId;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.BatchNo = s.ProductionActivityRoutineApp?.BatchNo;
                        productActivityApp.ProfileNo = s.ProfileNo;
                        productActivityApp.ProfileId = s.ProfileId;
                        productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                        productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                        productActivityApp.CheckedRemark = s.CheckedRemark;
                        productActivityApp.CheckedById = s.CheckedById;
                        productActivityApp.CheckedDate = s.CheckedDate;
                        productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                        if (productActivityApp.RoutineInfoIds.Count > 0)
                        {
                            productActivityApp.VisaMaster = productActivityApp.RoutineInfoIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.RoutineInfoIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                        }
                        productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels = GetProductionActivityRoutineAppLineQaChecker(s.ProductionActivityRoutineAppLineId);
                        productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                        if (documents != null && s.SessionId != null)
                        {
                            var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                            if (counts != null)
                            {
                                productActivityApp.DocumentId = counts.DocumentId;
                                productActivityApp.DocumentID = counts.DocumentId;
                                productActivityApp.DocumentParentId = counts.DocumentParentId;
                                productActivityApp.FileName = counts.FileName;
                                productActivityApp.FilePath = counts.FilePath;
                                productActivityApp.ContentType = counts.ContentType;
                                productActivityApp.IsLocked = counts.IsLocked;
                                productActivityApp.LockedByUserId = counts.LockedByUserId;
                                productActivityApp.ModifiedDate = counts.UploadDate;
                                productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                                productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                                productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                                productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                                long? roleId = 0;
                                roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == userId)?.RoleId;
                                if (roleId != null)
                                {
                                    var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                    if (documentPermission != null)
                                    {
                                        productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                        productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                        productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                        productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                        productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                        productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                        productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                        productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                        productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                        productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                        productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                        productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                        productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                        productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                        productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                        productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                        productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                        productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                        productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                        productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                        productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                        productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                    }
                                    else
                                    {
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                        productActivityApp.DocumentPermissionData.IsRead = false;
                                        productActivityApp.DocumentPermissionData.IsDelete = false;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                    }
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = true;
                                    productActivityApp.DocumentPermissionData.IsDelete = true;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                                }
                            }
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                });
            }
            return productActivityAppModels.OrderByDescending(s => s.ProductionActivityRoutineAppLineId).ToList();
        }
        [HttpGet]
        [Route("GetProductionActivityRoutineAppLineQaChecker")]
        public List<ProductionActivityRoutineAppLineQaCheckerModel> GetProductionActivityRoutineAppLineQaChecker(long? id)
        {
            List<ProductionActivityRoutineAppLineQaCheckerModel> productionActivityAppLineQaCheckerModels = new List<ProductionActivityRoutineAppLineQaCheckerModel>();
            var productionActivityAppLineQaChecker = _context.ProductionActivityRoutineAppLineQaChecker.Include(a => a.QaCheckUser).Where(w => w.ProductionActivityRoutineAppLineId == id).ToList();
            productionActivityAppLineQaChecker.ForEach(s =>
            {
                ProductionActivityRoutineAppLineQaCheckerModel productionActivityAppLineQaCheckerModel = new ProductionActivityRoutineAppLineQaCheckerModel();
                productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineQaCheckerId = s.ProductionActivityRoutineAppLineQaCheckerId;
                productionActivityAppLineQaCheckerModel.QaCheck = s.QaCheck;
                productionActivityAppLineQaCheckerModel.QaCheckDate = s.QaCheckDate;
                productionActivityAppLineQaCheckerModel.QaCheckUserId = s.QaCheckUserId;
                productionActivityAppLineQaCheckerModel.QaCheckUser = s.QaCheckUser?.UserName;
                productionActivityAppLineQaCheckerModels.Add(productionActivityAppLineQaCheckerModel);
            });
            return productionActivityAppLineQaCheckerModels;
        }
        [HttpPost]
        [Route("GetProductionActivityAppRoutineFilter")]
        public List<ProductionActivityRoutineAppModel> GetProductionActivityAppRoutineFilter(SearchModel searchModel)
        {
            List<ProductionActivityRoutineAppModel> productActivityAppModels = new List<ProductionActivityRoutineAppModel>();
            var routineinfolist = _context.RoutineInfoMultiple.ToList();
            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("RoutineActivity")).ToList();
            var productActivityApps = _context.ProductionActivityRoutineAppLine
                 .Include(a => a.ModifiedByUser)
                .Include(a => a.ProductionActivityRoutineApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityRoutineApp.Company)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.QaCheckUser)
                .Include(a => a.Location)
                .Include(a => a.VisaMaster)
                .Include(a => a.RoutineStatus)
                .Include(a => a.RoutineInfoMultiple).Where(a => a.SessionId == searchModel.SessionID)
                .ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 331, 334, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }

            //var productActivityApps = await productActivityAppss.GetPaged(searchModel.PageCount, searchModel.PageSize, searchModel.SortCol, searchModel.SortBy, searchModel.FilterFields, searchModel.SearchString);

            if (productActivityApps != null)
            {
                ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();
                var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
                var activityCaseList = _context.ProductActivityCase.Select(e => new { e.ManufacturingProcessChildId, e.ProductActivityCaseId, e.CategoryActionId, e.ManufacturingProcessId }).ToList();
                var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
                var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
                var emplist = _context.Employee.Select(e => new { e.UserId, e.EmployeeId }).ToList();
                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.FilePath, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                //var appUser = appUsersList.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine").ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();


                productActivityApps.ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseResponsId)).Select(r => r.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {
                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId).ToList();
                            var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            // var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (appUser != null && appUser.Count > 0)
                            {
                                var usersIdsBy = appUser.Select(s => s.UserId).ToList();
                                if (usersIdsBy != null && usersIdsBy.Count > 0)
                                {
                                    responsibilityUsers.AddRange(usersIdsBy);
                                }
                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d =>
                                {
                                    ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();


                                    ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    ProductActivityPermissionData.IsMail = d.IsMail;
                                    ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    ProductActivityPermissionData.UserID = d.UserId;
                                    ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    ProductActivityPermissions.Add(ProductActivityPermissionData);
                                });


                            }
                        }
                    }
                    ProductionActivityRoutineAppModel productActivityApp = new ProductionActivityRoutineAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                    productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityRoutineAppId = s.ProductionActivityRoutineAppId.Value;
                    productActivityApp.Comment = s.ProductionActivityRoutineApp?.Comment;
                    productActivityApp.RoutineInfoIds = s.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Select(r => r.RoutineInfoId.Value).ToList();
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityRoutineApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityRoutineApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    //productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    //productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityRoutineApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    //productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    // productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.QaCheckUserId = s.QaCheckUserId;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityRoutineAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }
                    productActivityApp.QaCheckDate = s.QaCheckDate;
                    productActivityApp.QaCheckUser = s.QaCheckUser?.UserName;
                    productActivityApp.Type = "Production Routine";
                    productActivityApp.VisaMasterId = s.VisaMasterId;
                    productActivityApp.VisaMaster = s.VisaMaster?.Value;
                    productActivityApp.RoutineStatusId = s.RoutineStatusId;
                    productActivityApp.RoutineStatus = s.RoutineStatus?.Value;
                    productActivityApp.LocationId = s.ProductionActivityRoutineApp?.LocationId;
                    productActivityApp.TicketNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                    productActivityApp.BatchNo = s.ProductionActivityRoutineApp?.BatchNo;
                    productActivityApp.ProfileNo = s.ProfileNo;
                    productActivityApp.ProfileId = s.ProfileId;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    if (productActivityApp.RoutineInfoIds != null && productActivityApp.RoutineInfoIds.Count > 0)
                    {
                        productActivityApp.VisaMaster = productActivityApp.RoutineInfoIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.RoutineInfoIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.ProductionActivityRoutineApp?.BatchNo;
                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();
                    productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels = GetProductionActivityRoutineAppLineQaChecker(s.ProductionActivityRoutineAppLineId, s);
                    var qaUserList = productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == searchModel.UserID).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == searchModel.UserID)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if (ProductActivityPermissions != null && ProductActivityPermissions.Count > 0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {


                            if (d.ProductActivityPermissionId > 0)
                            {
                                if (searchModel.UserID == d.UserID)
                                {
                                    productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                    productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    productActivityApp.IsActionPermission = true;
                                }



                            }
                            else
                            {
                                productActivityApp.IsActionPermission = false;
                            }
                        });

                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }
                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(searchModel.UserID))
                        {
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                    }
                });
            }

            // productActivityAppModels = productActivityAppModels.Where(w => w.SessionId == searchModel.SessionID).ToList();
            return productActivityAppModels;
        }

        [HttpGet]
        [Route("GetProductionActivityAppRoutine")]
        public List<ProductionActivityRoutineAppModel> GetProductionActivityAppRoutine(long? id)
        {

            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("RoutineActivity")).ToList();
            var productActivityApps = _context.ProductionActivityRoutineAppLine
                 .Include(a => a.ModifiedByUser)
                .Include(a => a.ProductionActivityRoutineApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityRoutineApp.Company)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.Location)
                .Include(a => a.VisaMaster)
                .Include(a => a.RoutineStatus)
                .Include(a => a.RoutineInfoMultiple)
                .ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 331, 334, 324 };
            var productActivitycaseresponslist = _context.ProductActivityCaseRespons.ToList();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }

            //var productActivityApps = await productActivityAppss.GetPaged(searchModel.PageCount, searchModel.PageSize, searchModel.SortCol, searchModel.SortBy, searchModel.FilterFields, searchModel.SearchString);
            List<ProductionActivityRoutineAppModel> productActivityAppModels = new List<ProductionActivityRoutineAppModel>();
            if (productActivityApps != null)
            {
                List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
                var activityCaseList = _context.ProductActivityCase.Select(e => new { e.ManufacturingProcessChildId, e.ProductActivityCaseId, e.CategoryActionId, e.ManufacturingProcessId }).ToList();
                var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
                var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();

                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.FilePath, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                //var appUser = appUsersList.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine").ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();


                productActivityApps.ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();
                    List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var responseId = productActivitycaseresponslist.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseId)).Select(r => r.ProductActivityCaseResponsId).ToList();
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => responseId.Contains(r.ProductActivityCaseResponsId.Value)).Select(t => t.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {

                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId.Value).ToList();
                            //var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            //var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            //var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (empIds != null && empIds.Count > 0)
                            {

                                responsibilityUsers.AddRange(empIds);

                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            // ProductActivityPermissions

                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d =>
                                {
                                    ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();


                                    ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    ProductActivityPermissionData.IsMail = d.IsMail;
                                    ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    ProductActivityPermissionData.UserID = d.UserId;
                                    ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    ProductActivityPermissions.Add(ProductActivityPermissionData);
                                });


                            }
                        }
                    }
                    ProductionActivityRoutineAppModel productActivityApp = new ProductionActivityRoutineAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                    productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityRoutineAppId = s.ProductionActivityRoutineAppId.Value;
                    productActivityApp.Comment = s.ProductionActivityRoutineApp?.Comment;
                    productActivityApp.RoutineInfoIds = s.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Select(r => r.RoutineInfoId.Value).ToList();
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityRoutineApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityRoutineApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    //productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    //productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityRoutineApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    //productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    // productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.QaCheckUserId = s.QaCheckUserId;
                    productActivityApp.ProfileNo = s.ProfileNo;
                    productActivityApp.ProfileId = s.ProfileId;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityRoutineAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }

                    productActivityApp.QaCheckDate = s.QaCheckDate;
                    productActivityApp.QaCheckUser = s.QaCheckUser?.UserName;
                    productActivityApp.Type = "Production Routine";
                    productActivityApp.VisaMasterId = s.VisaMasterId;
                    productActivityApp.VisaMaster = s.VisaMaster?.Value;
                    productActivityApp.RoutineStatusId = s.RoutineStatusId;
                    productActivityApp.RoutineStatus = s.RoutineStatus?.Value;
                    productActivityApp.LocationId = s.ProductionActivityRoutineApp?.LocationId;
                    productActivityApp.TicketNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                    productActivityApp.BatchNo = s.ProductionActivityRoutineApp?.BatchNo;
                    if (productActivityApp.RoutineInfoIds != null && productActivityApp.RoutineInfoIds.Count > 0)
                    {
                        productActivityApp.VisaMaster = productActivityApp.RoutineInfoIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.RoutineInfoIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.ProductionActivityRoutineApp?.BatchNo;
                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();
                    productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels = GetProductionActivityRoutineAppLineQaChecker(s.ProductionActivityRoutineAppLineId, s);
                    var qaUserList = productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == id.Value).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == id.Value)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if (ProductActivityPermissions != null && ProductActivityPermissions.Count > 0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {


                            if (d.ProductActivityPermissionId > 0)
                            {
                                if (id == d.UserID)
                                {
                                    productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                    productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    productActivityApp.IsActionPermission = true;
                                }



                            }
                            else
                            {
                                productActivityApp.IsActionPermission = false;
                            }
                        });

                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }
                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(id.Value))
                        {
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                    }
                });
            }
            productActivityAppModels = productActivityAppModels.OrderByDescending(s => s.AddedDate).ToList();
            return productActivityAppModels;
        }

        [HttpPost]
        [Route("GetProductionActivityAppRoutineSearch")]
        public List<ProductionActivityRoutineAppModel> GetProductionActivityAppRoutineSearch(SearchModel searchModel)
        {
            var routineinfolist = _context.RoutineInfoMultiple.ToList();
            var activityEmailTopicList = _context.ActivityEmailTopics.Where(a => a.ActivityType.ToLower().Contains("RoutineActivity")).ToList();
            var productActivityPermissionList = _context.ProductActivityPermission.ToList();
            var productActivitycaseresponslist = _context.ProductActivityCaseRespons.ToList();
            var productActivityApps = _context.ProductionActivityRoutineAppLine

                .Include(a => a.ModifiedByUser)

                .Include(a => a.ProductionActivityRoutineApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityRoutineApp.Company)
                .Include(a => a.StatusCode)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.QaCheckUser)
                .Include(a => a.Location)
                .Include(a => a.VisaMaster)
                .Include(a => a.RoutineStatus)
                .Include(a => a.RoutineInfoMultiple)
                .ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 331, 334, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            if (searchModel.ClassificationTypeId != null && searchModel.ClassificationTypeId > 0)
            {
                productActivityApps = productActivityApps.Where(w => w.ProductionActivityRoutineAppLineId == searchModel.ClassificationTypeId).ToList();
            }
            if (searchModel.ProductActivityAppSearchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.BatchNo))
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductionActivityRoutineApp.BatchNo == searchModel.ProductActivityAppSearchModel.BatchNo).ToList();
                }
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.ProdOrderNo))
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductionActivityRoutineApp.ProdOrderNo == searchModel.ProductActivityAppSearchModel.ProdOrderNo).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.CompanyId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductionActivityRoutineApp.CompanyId == searchModel.ProductActivityAppSearchModel.CompanyId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.RoutineStatusId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.RoutineStatusId == searchModel.ProductActivityAppSearchModel.RoutineStatusId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ManufacturingProcessId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ManufacturingProcessChildId == searchModel.ProductActivityAppSearchModel.ManufacturingProcessId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityCategoryChildId == searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityActionId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityActionChildD == searchModel.ProductActivityAppSearchModel.ProdActivityActionId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProductActivityCaseLineId == searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityResultId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.ProdActivityResultId == searchModel.ProductActivityAppSearchModel.ProdActivityResultId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.StatusCodeID != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.StatusCodeId == searchModel.ProductActivityAppSearchModel.StatusCodeID).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.LocationId != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.LocationId == searchModel.ProductActivityAppSearchModel.LocationId).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.StartDate != null && searchModel.ProductActivityAppSearchModel.EndDate == null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate == null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate != null)
                {
                    productActivityApps = productActivityApps.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value && w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value).ToList();
                }
                if (searchModel.ProductActivityAppSearchModel.RoutineInfoIds.Count > 0)
                {
                    var proActivitylineIds = routineinfolist.Where(s => searchModel.ProductActivityAppSearchModel.RoutineInfoIds.Contains(s.RoutineInfoId)).Select(s => s.ProductionActivityRoutineAppLineId.Value).Distinct().ToList();
                    productActivityApps = productActivityApps.Where(w => proActivitylineIds.Contains(w.ProductionActivityRoutineAppLineId)).ToList();
                }
            }

            //var productActivityApps = await productActivityAppss.GetPaged(searchModel.PageCount, searchModel.PageSize, searchModel.SortCol, searchModel.SortBy, searchModel.FilterFields, searchModel.SearchString);
            List<ProductionActivityRoutineAppModel> productActivityAppModels = new List<ProductionActivityRoutineAppModel>();
            if (productActivityApps != null)
            {
                ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();
                List<ProductActivityPermissionModel> ProductActivityPermissions = new List<ProductActivityPermissionModel>();
                var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
                var activityCaseList = _context.ProductActivityCase.ToList();
                var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
                var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
                var emplist = _context.Employee.ToList();
                var sessionIds = productActivityApps.Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.FilePath, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
                var appUsersList = _context.ApplicationUser.ToList();
                var appUser = appUsersList.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();
                var productionActivityRoutineAppLineDoc = _context.ProductionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine").ToList();



                productActivityApps.ForEach(s =>
                {
                    List<long> responsibilityUsers = new List<long>();

                    var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == s.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == s.ProdActivityCategoryChildId).Select(s => s.ProductActivityCaseId).ToList();
                    var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == s.ProdActivityActionChildD).Select(s => s.ProductActivityCaseId).ToList();
                    if (actionmultipleIds != null && actionmultipleIds.Count > 0)
                    {
                        var responseId = productActivitycaseresponslist.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseId)).Select(r => r.ProductActivityCaseResponsId).ToList();
                        var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => responseId.Contains(r.ProductActivityCaseResponsId.Value)).Select(t => t.ProductActivityCaseResponsDutyId).ToList();
                        if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                        {

                            var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId.Value).ToList();
                            //var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                            //var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            //var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                            if (empIds != null && empIds.Count > 0)
                            {

                                responsibilityUsers.AddRange(empIds);

                            }
                            var selectPermissiondata = productActivityPermissionList.Where(p => productActivityCaseResponsDutyIds.Contains(p.ProductActivityCaseResponsDutyId.Value)).ToList();
                            // ProductActivityPermissions

                            if (selectPermissiondata != null && selectPermissiondata.Count > 0)
                            {
                                selectPermissiondata.ForEach(d =>
                                {
                                    ProductActivityPermissionModel ProductActivityPermissionData = new ProductActivityPermissionModel();


                                    ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    ProductActivityPermissionData.IsMail = d.IsMail;
                                    ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    ProductActivityPermissionData.UserID = d.UserId;
                                    ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    ProductActivityPermissions.Add(ProductActivityPermissionData);
                                });


                            }
                        }
                    }
                    ProductionActivityRoutineAppModel productActivityApp = new ProductionActivityRoutineAppModel();
                    productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                    productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.ProductionActivityRoutineAppId = s.ProductionActivityRoutineApp.ProductionActivityRoutineAppId;
                    productActivityApp.Comment = s.ProductionActivityRoutineApp?.Comment;
                    productActivityApp.RoutineInfoIds = s.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Select(r => r.RoutineInfoId.Value).ToList();
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityRoutineApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityRoutineApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityRoutineApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.QaCheckUserId = s.QaCheckUserId;
                    productActivityApp.ProfileNo = s.ProfileNo;
                    productActivityApp.ProfileId = s.ProfileId;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedRemark = s.CheckedRemark;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedByUser = appUser?.Where(a => a.UserId == s.CheckedById)?.FirstOrDefault()?.UserName;
                    var emailcreated = activityEmailTopicList.Where(a => a.ActivityMasterId == s.ProductionActivityRoutineAppLineId && a.EmailTopicSessionId != null)?.FirstOrDefault();
                    if (emailcreated != null)
                    {
                        productActivityApp.IsEmailCreated = true;
                    }
                    productActivityApp.QaCheckDate = s.QaCheckDate;
                    productActivityApp.QaCheckUser = s.QaCheckUser?.UserName;
                    productActivityApp.Type = "Production Routine";
                    productActivityApp.VisaMasterId = s.VisaMasterId;
                    productActivityApp.VisaMaster = s.VisaMaster?.Value;
                    productActivityApp.RoutineStatusId = s.RoutineStatusId;
                    productActivityApp.RoutineStatus = s.RoutineStatus?.Value;
                    productActivityApp.LocationId = s.ProductionActivityRoutineApp?.LocationId;
                    productActivityApp.TicketNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                    productActivityApp.BatchNo = s.ProductionActivityRoutineApp?.BatchNo;
                    if (productActivityApp.RoutineInfoIds != null && productActivityApp.RoutineInfoIds.Count > 0)
                    {
                        productActivityApp.VisaMaster = productActivityApp.RoutineInfoIds != null ? string.Join(",", applicationmasterdetail.Where(w => productActivityApp.RoutineInfoIds.Contains(w.ApplicationMasterDetailId)).Select(s => s.Value).ToList()) : "";

                    }
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.ProductionActivityRoutineApp?.BatchNo;
                    productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                    productActivityApp.ProductActivityPermissionData = new ProductActivityPermissionModel();
                    productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels = GetProductionActivityRoutineAppLineQaChecker(s.ProductionActivityRoutineAppLineId, s);
                    var qaUserList = productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == searchModel.UserID).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                            long? roleId = 0;
                            roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == searchModel.UserID)?.RoleId;
                            if (roleId != null)
                            {
                                var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                if (documentPermission != null)
                                {
                                    productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                    productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                    productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                    productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                    productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                    productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                    productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                    productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                    productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                    productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                    productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                    productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                    productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                    productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                    productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                    productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                    productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                    productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                    productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                    productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                    productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                    productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = false;
                                    productActivityApp.DocumentPermissionData.IsDelete = false;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                }
                            }
                            else
                            {
                                productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                productActivityApp.DocumentPermissionData.IsRead = true;
                                productActivityApp.DocumentPermissionData.IsDelete = true;
                                productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                            }
                        }
                    }
                    if (ProductActivityPermissions != null && ProductActivityPermissions.Count > 0)
                    {
                        ProductActivityPermissions.ForEach(d =>
                        {


                            if (d.ProductActivityPermissionId > 0)
                            {
                                if (searchModel.UserID == d.UserID)
                                {
                                    productActivityApp.ProductActivityPermissionData.IsChecker = d.IsChecker;
                                    productActivityApp.ProductActivityPermissionData.IsUpdateStatus = d.IsUpdateStatus;
                                    productActivityApp.ProductActivityPermissionData.IsCheckOut = d.IsCheckOut;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityPermissionId = d.ProductActivityPermissionId;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseId = d.ProductActivityCaseId;
                                    productActivityApp.ProductActivityPermissionData.IsViewFile = d.IsViewFile;
                                    productActivityApp.ProductActivityPermissionData.IsViewHistory = d.IsViewHistory;
                                    productActivityApp.ProductActivityPermissionData.IsCopyLink = d.IsCopyLink;
                                    productActivityApp.ProductActivityPermissionData.IsMail = d.IsMail;
                                    productActivityApp.ProductActivityPermissionData.IsActivityInfo = d.IsActivityInfo;
                                    productActivityApp.ProductActivityPermissionData.IsNonCompliance = d.IsNonCompliance;
                                    productActivityApp.ProductActivityPermissionData.IsSupportDocuments = d.IsSupportDocuments;
                                    productActivityApp.ProductActivityPermissionData.UserID = d.UserID;
                                    productActivityApp.ProductActivityPermissionData.ProductActivityCaseResponsDutyId = d.ProductActivityCaseResponsDutyId;
                                    productActivityApp.IsActionPermission = true;
                                }



                            }
                            else
                            {
                                productActivityApp.IsActionPermission = false;
                            }
                        });

                    }
                    else
                    {
                        productActivityApp.IsActionPermission = true;
                    }
                    if (productActivityApp.ResponsibilityUsers != null && productActivityApp.ResponsibilityUsers.Count > 0)
                    {
                        if (productActivityApp.ResponsibilityUsers.Contains(searchModel.UserID))
                        {
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                    else
                    {
                        productActivityAppModels.Add(productActivityApp);
                    }
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetNavprodOrderLineBatchNo")]
        public List<ProductionActivityRoutineAppModel> GetNavprodOrderLineBatchNo(long? id)
        {
            List<ProductionActivityRoutineAppModel> productionActivityRoutineAppModels = new List<ProductionActivityRoutineAppModel>();
            var productionActivityRoutineAppModelList = _context.NavprodOrderLine.GroupBy(g => new { g.CompanyId, g.BatchNo, g.RePlanRefNo }).Select(s => new { s.Key.CompanyId, s.Key.BatchNo, s.Key.RePlanRefNo });
            productionActivityRoutineAppModelList.ToList().ForEach(s =>
            {
                if (!String.IsNullOrEmpty(s.BatchNo))
                {
                    ProductionActivityRoutineAppModel productionActivityRoutineAppModel = new ProductionActivityRoutineAppModel();
                    productionActivityRoutineAppModel.RePlanRefNo = s.RePlanRefNo;
                    productionActivityRoutineAppModel.BatchNo = s.BatchNo;
                    productionActivityRoutineAppModel.CompanyId = s.CompanyId;
                    productionActivityRoutineAppModels.Add(productionActivityRoutineAppModel);
                }
            });
            productionActivityRoutineAppModels = productionActivityRoutineAppModels.Where(s => s.CompanyId == id).ToList();
            return productionActivityRoutineAppModels;
        }
        [Route("GetData")]
        public ActionResult<ProductActivityAppModel> GetData(SearchModel searchModel)
        {
            var processTransfer = new ProductionActivityApp();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderBy(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId > searchModel.Id);
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductActivityAppModel>(processTransfer);
            if (processTransfer != null)
            {
                result.AddedByUser = processTransfer.AddedByUser?.UserName;
                //result.IsTemplateUploadFlag = processTransfer.IsTemplateUpload == true ? "Yes" : "No";
            }

            return result;
        }
        [HttpPost]
        [Route("InsertTopicId")]
        public ProductActivityAppModel InsertTopicId(ProductActivityAppModel value)
        {
            var productActivityApp = _context.ProductionActivityApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ProdOrderNo.ToLower() == value.ProdOrderNo.ToLower());
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();
                var productActivityApps = new ProductionActivityApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    ProdOrderNo = value.ProdOrderNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    TopicId = value.TopicId,
                };
                _context.ProductionActivityApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityAppId = productActivityApps.ProductionActivityAppId;
            }
            else
            {
                productActivityApp.TopicId = value.TopicId;
                value.ProductionActivityAppId = productActivityApp.ProductionActivityAppId;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertProductionActivityRoutineApp")]
        public ProductionActivityRoutineAppModel InsertProductActivityApp(ProductionActivityRoutineAppModel value)
        {
            value.LineSessionId ??= Guid.NewGuid();
            var profileNo = "";
            value.ProfileId = _context.ProductActivityCase.FirstOrDefault(a => a.ManufacturingProcessChildId == value.ManufacturingProcessChildId)?.ProfileId;
            if (value.ProfileId != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "Company Routine" });
            }
            var productActivityApp = _context.ProductionActivityRoutineApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ManufacturingProcessChildId == value.ManufacturingProcessChildId && s.LocationId == value.LocationId && s.ProdOrderNo == value.ProdOrderNo);
            var batchNo = _context.NavprodOrderLine.FirstOrDefault(f => f.RePlanRefNo == value.ProdOrderNo)?.BatchNo;
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();

                var productActivityApps = new ProductionActivityRoutineApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                    LocationId = value.LocationId == -1 ? null : value.LocationId,
                    ProdOrderNo = value.ProdOrderNo,
                    ProductActivityCaseId = value.ProductActivityCaseId,
                    IsOthersOptions = value.IsOthersOptions,
                    BatchNo = batchNo,
                    
                };
                _context.ProductionActivityRoutineApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityRoutineAppId = productActivityApps.ProductionActivityRoutineAppId;

            }
            else
            {
                value.SessionId ??= Guid.NewGuid();
                value.ProductionActivityRoutineAppId = productActivityApp.ProductionActivityRoutineAppId;
                productActivityApp.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
                productActivityApp.CompanyId = value.CompanyId;
                productActivityApp.Comment = value.Comment;
                productActivityApp.StatusCodeId = value.StatusCodeID.Value;
                productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
                productActivityApp.ModifiedDate = DateTime.Now;
                productActivityApp.SessionId = value.SessionId;
                productActivityApp.ProdOrderNo = value.ProdOrderNo;
                productActivityApp.LocationId = value.LocationId == -1 ? null : value.LocationId;
                productActivityApp.ProductActivityCaseId = value.ProductActivityCaseId;
                productActivityApp.IsOthersOptions = value.IsOthersOptions;
                productActivityApp.BatchNo ??= batchNo;
                _context.SaveChanges();
            }
            var productActivityAppLine = new ProductionActivityRoutineAppLine
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                ProductionActivityRoutineAppId = value.ProductionActivityRoutineAppId,
                ProdActivityActionId = value.ProdActivityActionId,
                ActionDropdown = value.ActionDropdown,
                ProdActivityCategoryId = value.ProdActivityCategoryId,
                IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.LineSessionId,
                ProductActivityCaseLineId = value.ProductActivityCaseLineId,
                NavprodOrderLineId = value.NavprodOrderLineId,
                Comment = value.LineComment,
                QaCheck = false,
                IsOthersOptions = value.IsOthersOptions,
                ProdActivityResultId = value.ProdActivityResultId,
                ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                ProdActivityActionChildD = value.ProdActivityActionChildD,
                ProdActivityCategoryChildId = value.ProdActivityCategoryChildId,
                LocationId = value.LocationId == -1 ? null : value.LocationId,
                ProductActivityCaseId = value.ProductActivityCaseId,
                VisaMasterId = value.VisaMasterId,
                RoutineStatusId = value.RoutineStatusId,
                CommentImage = value.CommentImage,
                CommentImageType = value.CommentImageType,
                ProfileId = value.ProfileId,
                ProfileNo = profileNo,

            };
            _context.ProductionActivityRoutineAppLine.Add(productActivityAppLine);
            _context.SaveChanges();
            value.ProductionActivityRoutineAppLineId = productActivityAppLine.ProductionActivityRoutineAppLineId;
            if (value.RoutineInfoIds.Count > 0)
            {
                value.RoutineInfoIds.ForEach(r =>
                {
                    var routineMultiple = new RoutineInfoMultiple();
                    routineMultiple.RoutineInfoId = r;
                    routineMultiple.ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId;
                    _context.Add(routineMultiple);

                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityRoutineAppLine")]
        public ProductionActivityRoutineAppModel UpdateProductionActivityRoutineAppLine(ProductionActivityRoutineAppModel value)
        {
            value.LineSessionId ??= Guid.NewGuid();
            var productActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(s => s.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId);
            productActivityApp.ManufacturingProcessId = value.ManufacturingProcessId;
            productActivityApp.ProdActivityActionId = value.ProdActivityActionId;
            productActivityApp.ActionDropdown = value.ActionDropdown;
            productActivityApp.ProdActivityCategoryId = value.ProdActivityCategoryId;
            productActivityApp.IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false;
            productActivityApp.StatusCodeId = value.StatusCodeID.Value;
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.SessionId = value.LineSessionId;
            productActivityApp.Comment = value.LineComment;
            productActivityApp.IsOthersOptions = value.IsOthersOptions;
            productActivityApp.ProductActivityCaseLineId = value.ProductActivityCaseLineId;
            productActivityApp.ProdActivityResultId = value.ProdActivityResultId;
            productActivityApp.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
            productActivityApp.ProdActivityActionChildD = value.ProdActivityActionChildD;
            productActivityApp.ProdActivityCategoryChildId = value.ProdActivityCategoryChildId;
            productActivityApp.LocationId = value.LocationId;
            productActivityApp.ProductActivityCaseId = value.ProductActivityCaseId;
            productActivityApp.VisaMasterId = value.VisaMasterId;
            productActivityApp.RoutineStatusId = value.RoutineStatusId;
            productActivityApp.CommentImage = value.CommentImage;
            productActivityApp.CommentImageType = value.CommentImageType;
           
            if (value.RoutineInfoIds.Count > 0)
            {
                value.RoutineInfoIds.ForEach(r =>
                {
                    var routineMultiple = new RoutineInfoMultiple();
                    routineMultiple.RoutineInfoId = r;
                    routineMultiple.ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId;
                    _context.Add(routineMultiple);

                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppRoutineQaCheck")]
        public ProductionActivityRoutineAppModel UpdateProductActivityAppRoutineQaCheck(ProductionActivityRoutineAppModel value)
        {
            var productActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(s => s.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId);

            productActivityApp.QaCheckUserId = value.QaCheckUserId;
            productActivityApp.QaCheckDate = DateTime.Now;
            productActivityApp.QaCheck = value.QaCheck;
            _context.SaveChanges();
            var productionActivityAppLine = new ProductionActivityRoutineAppLineQaChecker
            {
                QaCheck = value.QaCheck,
                QaCheckDate = DateTime.Now,
                QaCheckUserId = value.QaCheckUserId,
                ProductionActivityRoutineAppLineId = productActivityApp.ProductionActivityRoutineAppLineId,
            };
            _context.ProductionActivityRoutineAppLineQaChecker.Add(productionActivityAppLine);
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppRoutineTopic")]
        public ProductionActivityRoutineAppModel UpdateProductActivityAppRoutineTopic(ProductionActivityRoutineAppModel value)
        {
            var productActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(s => s.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId);
            productActivityApp.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("DeleteRoutineSupportDocument")]
        public void DeleteDocument(SearchModel searchModel)
        {
            var id = searchModel.Id;
            try
            {
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.DocumentParentId, s.FileName, s.FilterProfileTypeId, s.IsVideoFile }).FirstOrDefault(f => f.DocumentId == id);

                if (documents != null && documents.DocumentParentId == null)
                {
                    var productionActivityRoutineAppLineDoc = _context.ProductionActivityRoutineAppLineDoc.Where(p => p.DocumentId == id).FirstOrDefault();
                    if (productionActivityRoutineAppLineDoc != null)
                    {
                        _context.ProductionActivityRoutineAppLineDoc.Remove(productionActivityRoutineAppLineDoc);
                        _context.SaveChanges();

                    }
                    var isVideoFile = documents.IsVideoFile.GetValueOrDefault(false);
                    if (isVideoFile)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + documents.FileName;
                        System.IO.File.Delete(serverPath);
                    }
                    var query = string.Format("Delete from Documents  Where  DocumentId='{0}' ", documents.DocumentId);
                    _context.Database.ExecuteSqlRaw(query);
                }



            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {
            var SessionId = new Guid(files["sessionId"].ToString());
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
            var filePath = files["filePath"].ToString();
            if (filePath == "filePath")
            {
                UploadFileAsByPath(files, SessionId);
            }
            else
            {
                var userId = new long?();
                var user = files["userID"].ToString();
                if (user != "" && user != "undefined" && user != null)
                {
                    userId = Convert.ToInt32(files["userID"].ToString());
                }
                var documentNo = "";
                var numberSeriesId = files["numberSeriesId"].ToString();
                if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
                {
                    documentNo = files["numberSeriesId"].ToString();
                }

                var videoFiles = files["isVideoFile"].ToString();

                var videoFile = false;
                if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                {
                    videoFile = Convert.ToBoolean(videoFiles);
                }
                var fileProfileTypeId = new long?();
                var locationToSaveId = files["locationToSaveId"].ToString();
                var isTemplate = files["isTemplate"].ToString();
                if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
                {
                    fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                    if (isTemplate == "No")
                    {
                        var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                        if (profileId != null && profileId.ProfileId > 0)
                        {
                            documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                        }
                    }
                }
                var newFileName = files["newFileName"].ToString();
                var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    var ext = "";
                    var newFile = "";
                    if (f.FileName == "blob")
                    {
                        newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                    }
                    else
                    {
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                        newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhs);
                    }
                    if (videoFile == true)
                    {
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                        var fileName1 = SessionId + "." + rhs;
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                        using (var targetStream = System.IO.File.Create(serverPath))
                        {
                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }
                        if (docs == null)
                        {
                            var documents = new Documents
                            {
                                FileName = newFile,
                                ContentType = file.ContentType,
                                FileData = null,
                                FileSize = file.Length,
                                UploadDate = DateTime.Now,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                IsVideoFile = true,
                                IsLatest = true,
                                FilterProfileTypeId = fileProfileTypeId,
                                ProfileNo = documentNo,
                                SourceFrom = "FileProfile",
                            };
                            _context.Documents.Add(documents);
                        }
                        else
                        {
                            docs.FileName = newFile;
                            docs.ContentType = file.ContentType;
                            docs.FileData = null;
                            docs.FileSize = file.Length;
                            docs.UploadDate = DateTime.Now;
                            docs.IsLatest = true;
                            docs.IsVideoFile = true;
                            docs.FilterProfileTypeId = fileProfileTypeId;
                            docs.ProfileNo = documentNo;
                        }
                    }
                    else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        if (docs == null)
                        {
                            var documents = new Documents
                            {
                                FileName = newFile,
                                ContentType = file.ContentType,
                                FileData = compressedData,
                                FileSize = fs.Length,
                                UploadDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                IsLatest = true,
                                FilterProfileTypeId = fileProfileTypeId,
                                ProfileNo = documentNo,
                                SourceFrom = "FileProfile",
                            };
                            _context.Documents.Add(documents);
                        }
                        else
                        {
                            docs.FileName = newFile;
                            docs.ContentType = file.ContentType;
                            docs.FileData = compressedData;
                            docs.FileSize = fs.Length;
                            docs.UploadDate = DateTime.Now;
                            docs.IsLatest = true;
                            docs.IsVideoFile = false;
                            docs.FilterProfileTypeId = fileProfileTypeId;
                            docs.ProfileNo = documentNo;
                        }
                    }
                    var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                    if (documentNoSeries != null)
                    {
                        var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                        _context.Database.ExecuteSqlRaw(query);
                    }
                });
                _context.SaveChanges();

            }
            return Content(SessionId.ToString());


        }

        private void UploadSupportDocFileAsByPath(IFormCollection files, Guid SessionId, long productionActivityRoutineAppLineId, string type)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
          
            var description = files["description"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                string[] split = f.FileName.Split('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + SessionId + '.' + split.Last();
                var filePath = getNextFileName(serverPath);
                newFile = filePath.Replace(serverPaths + @"\", "");
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = f.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",
                        Description = description,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    InsertUploadDoument(documents.DocumentId, productionActivityRoutineAppLineId, type);
                }

                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
            });
            _context.SaveChanges();
        }
        private void UploadFileAsByPath(IFormCollection files, Guid SessionId)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                string[] split = f.FileName.Split('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + Guid.NewGuid() + '.' + split.Last();
               
                // newFile = filePath.Replace(serverPaths + @"\", "");
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                    serverPath = serverPaths + @"\" + Guid.NewGuid() + ".pdf";
                }
                else
                {

                }
                var filePath = getNextFileName(serverPath);
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = newFile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                        SourceFrom = "FileProfile",

                    };
                    _context.Documents.Add(documents);
                }
                else if (docs != null)
                {

                    var docquery = string.Format("Update Documents Set IsLatest ='{1}' Where DocumentId= {0}  ", docs.DocumentId, 0);
                    _context.Database.ExecuteSqlRaw(docquery);



                    var documents = new Documents
                    {
                        FileName = f.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = videoFile,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        SourceFrom = "FileProfile",
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                }
                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
            });
            _context.SaveChanges();
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpPost]
        [Route("UploadSupportDocuments")]
        public IActionResult UploadSupportDocuments(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
            var productionActivityRoutineAppLineId = Convert.ToInt32(files["productionActivityRoutineAppLineId"].ToString());

            var videoFiles = files["isVideoFile"].ToString();
            var type = files["type"].ToString();
            var filePath = files["filePath"].ToString();
            if (filePath == "filePath")
            {
                UploadSupportDocFileAsByPath(files, SessionId,productionActivityRoutineAppLineId,type);
            }
            else
            {
                var userId = new long?();
                var user = files["userID"].ToString();
                var description = files["description"].ToString();
                var videoFile = false;
                if (user != "" && user != "undefined" && user != null)
                {
                    userId = Convert.ToInt32(files["userID"].ToString());
                }
               
                var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
                if (!System.IO.Directory.Exists(serverPaths))
                {
                    System.IO.Directory.CreateDirectory(serverPaths);
                }
                if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                {
                    videoFile = Convert.ToBoolean(videoFiles);
                }
                foreach (var f in files.Files)
                {
                    DateTime? expiryDate = new DateTime?();
                    var userIds = files["userID"].ToString().Split(',').ToList();
                    SessionId = Guid.NewGuid();
                    userId = Convert.ToInt32(userIds.FirstOrDefault().ToString());

                    description = files["description"].ToString();
                    var expiryDateString = files["expiryDate"].ToString();
                    if (expiryDateString != null && expiryDateString != "" && expiryDateString != "undefined" && expiryDateString != "null")
                    {
                        expiryDate = Convert.ToDateTime(expiryDateString);
                    }
                    else
                    {
                        expiryDate = null;
                    }

                    //var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString().Split(',')[0].ToString());
                    var tableName = files["type"].ToString().Split(',')[0].ToString();

                    // var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                    string profileNo = "";




                    var fileName = SessionId + "." + f.FileName.Split(".").Last();
                    var file = f;

                    byte[] dataArray = new byte[0];
                    //var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName;
                    serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                    var serverPath = serverPaths + @"\" + fileName;
                    filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {

                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }


                }
                files.Files.ToList().ForEach(f =>
                    {
                        var file = f;
                        var ext = "";
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        var isVideoFiles = false;
                        var contentType = f.ContentType.Split("/");
                        if (contentType[0] == "video")
                        {
                            isVideoFiles = true;
                        }
                        if (videoFile == true)
                        {
                            string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                            var serverPath = serverPaths + @"\" + f.FileName;
                            var filePath = getNextFileName(serverPath);
                            var newFile = filePath.Replace(serverPaths + @"\", "");
                            using (var targetStream = System.IO.File.Create(filePath))
                            {
                                file.CopyTo(targetStream);
                                targetStream.Flush();
                            }
                            var documents = new Documents
                            {
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FileData = null,
                                FileSize = file.Length,
                                UploadDate = DateTime.Now,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                IsVideoFile = isVideoFiles,
                                IsLatest = true,
                                Description = description,
                                FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                                SourceFrom = "FileProfile",

                            };
                            _context.Documents.Add(documents);
                            _context.SaveChanges();
                            InsertUploadDoument(documents.DocumentId, productionActivityRoutineAppLineId, type);
                        }
                        else
                        {
                            var fs = file.OpenReadStream();
                            var br = new BinaryReader(fs);
                            Byte[] document = br.ReadBytes((Int32)fs.Length);
                            var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);

                            var documents = new Documents
                            {
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FileData = compressedData,
                                FileSize = fs.Length,
                                UploadDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                IsLatest = true,
                                Description = description,
                                FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                                SourceFrom = "FileProfile",


                            };
                            _context.Documents.Add(documents);
                            _context.SaveChanges();
                            InsertUploadDoument(documents.DocumentId, productionActivityRoutineAppLineId, type);
                        }


                    });

            }
            return Content(SessionId.ToString());

        }
        private void InsertUploadDoument(long documentId, long productionActivityRoutineAppLineId, string type)
        {
            var productionActivityAppLineDoc = new ProductionActivityRoutineAppLineDoc
            {
                DocumentId = documentId,
                ProductionActivityRoutineAppLineId = productionActivityRoutineAppLineId,
                Type = type,
            };
            _context.ProductionActivityRoutineAppLineDoc.Add(productionActivityAppLineDoc);
            _context.SaveChanges();
        }
        [HttpGet]
        [Route("GetSupportDocuments")]
        public List<DocumentsModel> GetSupportDocuments(long? id, string type)
        {
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var documentsId = _context.ProductionActivityRoutineAppLineDoc.Where(w => w.DocumentId != null && w.Type.ToLower() == type.ToLower());
            documentsId = documentsId.Where(w => w.ProductionActivityRoutineAppLineId == id);
            var documentsIds = documentsId.Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
            var documetsparents = _context.Documents.Where(s => documentsIds.Contains(s.DocumentId) && s.IsLatest == true).Select(s => new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.AddedDate,
                s.IsCompressed,
                s.FileIndex,
                s.ProfileNo,
                s.Description,
                s.FilePath,
            }).AsNoTracking().ToList();
            if (documetsparents != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).AsNoTracking().ToList();
                documetsparents.ForEach(documetsparent =>
                {
                    var fileName = documetsparent.FileName.Split('.');
                    DocumentsModel documentsModelss = new DocumentsModel
                    {
                        DocumentID = documetsparent.DocumentId,
                        FileName = documetsparent.FileIndex > 0 ? fileName[0] + "_V0" + documetsparent.FileIndex + "." + fileName[1] : documetsparent.FileName,
                        ContentType = documetsparent.ContentType,
                        FileSize = (long)Math.Round(Convert.ToDouble(documetsparent.FileSize / 1024)),
                        UploadDate = documetsparent.UploadDate,
                        SessionID = documetsparent.SessionId,
                        FilterProfileTypeId = documetsparent.FilterProfileTypeId,
                        DocumentParentId = documetsparent.DocumentParentId,
                        TableName = documetsparent.TableName,
                        Type = "Document",
                        AddedDate = documetsparent.AddedDate,
                        AddedByUser = appUsers.FirstOrDefault(f => f.UserId == documetsparent.AddedByUserId)?.UserName,
                        IsCompressed = documetsparent.IsCompressed,
                        FileIndex = documetsparent.FileIndex,
                        ProfileNo = documetsparent.ProfileNo,
                        Description = documetsparent.Description,
                        FilePath = documetsparent.FilePath,
                    };
                    documentsModel.Add(documentsModelss);
                });
            }
            return documentsModel;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityRoutineApp")]
        public ActionResult<string> DeleteProductionActivityRoutineApp(int id)
        {
            try
            {
                var ProductionActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(p => p.ProductionActivityRoutineAppLineId == id);
                if (ProductionActivityApp != null)
                {
                    var query = string.Format("Update Documents Set Islatest={1}  Where SessionId='{0}'", ProductionActivityApp.SessionId, 0);
                    _context.Database.ExecuteSqlRaw(query);
                    _context.ProductionActivityRoutineAppLine.Remove(ProductionActivityApp);
                    _context.SaveChanges();
                    var ProductionActivityAppCount = _context.ProductionActivityRoutineAppLine.Where(p => p.ProductionActivityRoutineAppLineId == id).Count();
                    if (ProductionActivityAppCount == 0)
                    {
                        var ProductionActivityApps = _context.ProductionActivityRoutineApp.SingleOrDefault(p => p.ProductionActivityRoutineAppId == ProductionActivityApp.ProductionActivityRoutineAppId);
                        if (ProductionActivityApps != null)
                        {
                            _context.ProductionActivityRoutineApp.Remove(ProductionActivityApps);
                            _context.SaveChanges();
                        }
                    }
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        public List<ProductionActivityRoutineAppLineQaCheckerModel> GetProductionActivityRoutineAppLineQaChecker(long? id, ProductionActivityRoutineAppLine s)
        {
            List<ProductionActivityRoutineAppLineQaCheckerModel> productionActivityAppLineQaCheckerModels = new List<ProductionActivityRoutineAppLineQaCheckerModel>();
            var productionActivityAppLineQaChecker = _context.ProductionActivityRoutineAppLineQaChecker.Include(a => a.QaCheckUser).Where(w => w.ProductionActivityRoutineAppLineId == id).ToList();
            if (productionActivityAppLineQaChecker != null && productionActivityAppLineQaChecker.Count > 0)
            {
                productionActivityAppLineQaChecker.ForEach(s =>
                {
                    ProductionActivityRoutineAppLineQaCheckerModel productionActivityAppLineQaCheckerModel = new ProductionActivityRoutineAppLineQaCheckerModel();
                    productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineQaCheckerId = s.ProductionActivityRoutineAppLineQaCheckerId;
                    productionActivityAppLineQaCheckerModel.QaCheck = s.QaCheck;
                    productionActivityAppLineQaCheckerModel.QaCheckDate = s.QaCheckDate;
                    productionActivityAppLineQaCheckerModel.QaCheckUserId = s.QaCheckUserId;
                    productionActivityAppLineQaCheckerModel.QaCheckUser = s.QaCheckUser?.UserName;
                    productionActivityAppLineQaCheckerModels.Add(productionActivityAppLineQaCheckerModel);
                });
            }
            else
            {
                if (s.QaCheck == true)
                {
                    ProductionActivityRoutineAppLineQaCheckerModel productionActivityAppLineQaCheckerModel = new ProductionActivityRoutineAppLineQaCheckerModel();
                    productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productionActivityAppLineQaCheckerModel.ProductionActivityRoutineAppLineQaCheckerId = 0;
                    productionActivityAppLineQaCheckerModel.QaCheck = s.QaCheck;
                    productionActivityAppLineQaCheckerModel.QaCheckDate = s.QaCheckDate;
                    productionActivityAppLineQaCheckerModel.QaCheckUserId = s.QaCheckUserId;
                    productionActivityAppLineQaCheckerModel.QaCheckUser = s.QaCheckUser?.UserName;
                    productionActivityAppLineQaCheckerModels.Add(productionActivityAppLineQaCheckerModel);
                }
            }
            return productionActivityAppLineQaCheckerModels;
        }
        [HttpPost]
        [Route("GetProductionActivityAppRoutineExcel")]
        public IActionResult GetProductionActivityAppRoutineExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Company");
            headers.Add("Date");
            headers.Add("Location");
            headers.Add("Production Routine");
            headers.Add("Category");
            headers.Add("Action");
            headers.Add("Comment");
            headers.Add("Result");
            headers.Add("Routine Master");
            headers.Add("Routine Status");
            headers.Add("Read By");
            headers.Add("Modify By");
            var productActivityAppss = _context.ProductionActivityRoutineAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcess)
                .Include(a => a.ProdActivityCategory)
                .Include(a => a.ProdActivityAction)
                .Include(a => a.ProductionActivityRoutineApp)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityRoutineApp.Company)
                .Include(a => a.StatusCode)
                .Include(a => a.ProductActivityCaseLine)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.QaCheckUser)
                .Include(a => a.Location)
                .Include(a => a.VisaMaster)
                .Include(a => a.RoutineStatus)
                .AsQueryable();
            if (searchModel.ClassificationTypeId != null && searchModel.ClassificationTypeId > 0)
            {
                productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityRoutineAppLineId == searchModel.ClassificationTypeId);
            }
            if (searchModel.ProductActivityAppSearchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.BatchNo))
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityRoutineApp.BatchNo == searchModel.ProductActivityAppSearchModel.BatchNo);
                }
                if (!string.IsNullOrEmpty(searchModel.ProductActivityAppSearchModel.ProdOrderNo))
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityRoutineApp.ProdOrderNo == searchModel.ProductActivityAppSearchModel.ProdOrderNo);
                }
                if (searchModel.ProductActivityAppSearchModel.CompanyId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductionActivityRoutineApp.CompanyId == searchModel.ProductActivityAppSearchModel.CompanyId);
                }
                if (searchModel.ProductActivityAppSearchModel.ManufacturingProcessId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ManufacturingProcessChildId == searchModel.ProductActivityAppSearchModel.ManufacturingProcessId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityCategoryChildId == searchModel.ProductActivityAppSearchModel.ProdActivityCategoryId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityActionId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityActionChildD == searchModel.ProductActivityAppSearchModel.ProdActivityActionId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProductActivityCaseLineId == searchModel.ProductActivityAppSearchModel.ProductActivityCaseLineId);
                }
                if (searchModel.ProductActivityAppSearchModel.ProdActivityResultId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.ProdActivityResultId == searchModel.ProductActivityAppSearchModel.ProdActivityResultId);
                }
                if (searchModel.ProductActivityAppSearchModel.StatusCodeID != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.StatusCodeId == searchModel.ProductActivityAppSearchModel.StatusCodeID);
                }
                if (searchModel.ProductActivityAppSearchModel.StartDate != null && searchModel.ProductActivityAppSearchModel.EndDate == null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate == null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.EndDate != null && searchModel.ProductActivityAppSearchModel.StartDate != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.AddedDate.Value.Date >= searchModel.ProductActivityAppSearchModel.StartDate.Value && w.AddedDate.Value.Date <= searchModel.ProductActivityAppSearchModel.EndDate.Value);
                }
                if (searchModel.ProductActivityAppSearchModel.LocationId != null)
                {
                    productActivityAppss = productActivityAppss.Where(w => w.LocationId == searchModel.ProductActivityAppSearchModel.LocationId);
                }
            }
            var productActivityApps = productActivityAppss.ToList();
            List<ProductionActivityRoutineAppModel> productActivityAppModels = new List<ProductionActivityRoutineAppModel>();
            if (productActivityApps != null)
            {
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.FilePath, s.LockedByUserId, s.FileName, s.IsLocked, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
               
                productActivityApps.ToList().ForEach(s =>
                {
                    /*List<long> responsibilityUsers = new List<long>();
                    if (s.ProductActivityCaseId > 0)
                    {
                        var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                        var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                        var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                        var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                        var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                        var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                        var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                        var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                        if (appUserss != null && appUserss.Count > 0)
                        {
                            var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                            if (usersIdsBy != null && usersIdsBy.Count > 0)
                            {
                                responsibilityUsers.AddRange(usersIdsBy);
                            }
                        }
                    }*/
                    ProductionActivityRoutineAppModel productActivityApp = new ProductionActivityRoutineAppModel();
                    /*productActivityApp.ResponsibilityUsers = responsibilityUsers;
                    productActivityApp.SupportDocCount = productionActivityRoutineAppLineDoc.Where(w => w.Type == "Production Routine" && w.ProductionActivityRoutineAppLineId == s.ProductionActivityRoutineAppLineId).Count();
                    */
                    productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                    productActivityApp.VisaMasterId = s.VisaMasterId;
                    productActivityApp.BatchNo = s.ProductionActivityRoutineApp?.BatchNo;
                    productActivityApp.VisaMaster = s.VisaMaster?.Value;
                    productActivityApp.RoutineStatusId = s.RoutineStatusId;
                    productActivityApp.RoutineStatus = s.RoutineStatus?.Value;
                    productActivityApp.ProductionActivityRoutineAppId = s.ProductionActivityRoutineApp.ProductionActivityRoutineAppId;
                    productActivityApp.Comment = s.ProductionActivityRoutineApp?.Comment;
                    productActivityApp.LineComment = s.Comment;
                    productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                    productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                    productActivityApp.CompanyId = s.ProductionActivityRoutineApp?.CompanyId;
                    productActivityApp.CompanyName = s.ProductionActivityRoutineApp?.Company?.PlantCode;
                    productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                    productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                    productActivityApp.ActionDropdown = s.ActionDropdown;
                    productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                    productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                    productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                    productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                    productActivityApp.StatusCodeID = s.StatusCodeId;
                    productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.SessionId = s.ProductionActivityRoutineApp?.SessionId;
                    productActivityApp.LineSessionId = s.SessionId;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                    productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                    productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                    productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                    productActivityApp.QaCheck = false;
                    productActivityApp.IsOthersOptions = s.IsOthersOptions;
                    productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                    productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                    productActivityApp.TopicId = s.TopicId;
                    productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                    productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                    productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                    productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                    productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                    productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                    productActivityApp.QaCheckUserId = s.QaCheckUserId;
                    productActivityApp.QaCheckDate = s.QaCheckDate;
                    productActivityApp.QaCheckUser = s.QaCheckUser?.UserName;
                    productActivityApp.Type = "Production Routine";
                    productActivityApp.LocationId = s.ProductionActivityRoutineApp?.LocationId;
                    productActivityApp.TicketNo = s.ProductionActivityRoutineApp?.ProdOrderNo;
                    productActivityApp.LocationName = s.Location?.Description + "||" + s.ProductionActivityRoutineApp?.BatchNo + "||" + s.ProductionActivityRoutineApp?.ProdOrderNo; ;
                    /* productActivityApp.DocumentPermissionData = new DocumentPermissionModel();*/
                    productActivityApp.ProductionActivityRoutineAppLineQaCheckerModels = GetProductionActivityRoutineAppLineQaChecker(s.ProductionActivityRoutineAppLineId, s);
                    var qaUserList = productActivityApp?.ProductionActivityRoutineAppLineQaCheckerModels.ToList().Where(w => w.QaCheckUserId == searchModel.UserID).FirstOrDefault();
                    if (qaUserList != null)
                    {
                        productActivityApp.QaCheckUser = qaUserList.QaCheckUser;
                        productActivityApp.QaCheckUserId = qaUserList.QaCheckUserId;
                        productActivityApp.QaCheckDate = qaUserList.QaCheckDate;
                    }
                    if (documents != null && s.SessionId != null)
                    {
                        var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                        if (counts != null)
                        {
                            productActivityApp.DocumentId = counts.DocumentId;
                            productActivityApp.DocumentID = counts.DocumentId;
                            productActivityApp.DocumentParentId = counts.DocumentParentId;
                            productActivityApp.FileName = counts.FileName;
                            productActivityApp.ContentType = counts.ContentType;
                            productActivityApp.FilePath = counts.FilePath;
                            productActivityApp.IsLocked = counts.IsLocked;
                            productActivityApp.LockedByUserId = counts.LockedByUserId;
                            productActivityApp.FilePath = counts.FilePath;
                            /*productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                            productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();*/
                            productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                        }
                    }
                    productActivityAppModels.Add(productActivityApp);
                });
            }
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            string FromLocation = folderName + @"\" + newFolderName + @"\" + searchModel.UserID + ".xlsx";
            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                int j = 1;

                List<string> headersBy = new List<string>();
                headersBy.AddRange(headers);
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                var sheetName = "Production Routine";
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = sheetName };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);

                Row headerRow = new Row();
                foreach (string column in headersBy)
                {
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                if (productActivityAppModels != null && productActivityAppModels.Count > 0)
                {
                    productActivityAppModels.ForEach(s =>
                    {
                        Row row = new Row();
                        Cell companyNameCell = new Cell();
                        companyNameCell.DataType = CellValues.String;
                        companyNameCell.CellValue = new CellValue(s.CompanyName?.ToString());
                        row.AppendChild(companyNameCell);

                        Cell profileTypeNamecell = new Cell();
                        profileTypeNamecell.DataType = CellValues.String;
                        profileTypeNamecell.CellValue = new CellValue(s.AddedDate?.ToString("dd-MMM-yyyy hh:mm tt"));
                        row.AppendChild(profileTypeNamecell);

                        Cell locationNameCell = new Cell();
                        locationNameCell.DataType = CellValues.String;
                        locationNameCell.CellValue = new CellValue(s.LocationName?.ToString());
                        row.AppendChild(locationNameCell);

                        Cell manufacturingProcessChildCell = new Cell();
                        manufacturingProcessChildCell.DataType = CellValues.String;
                        manufacturingProcessChildCell.CellValue = new CellValue(s.ManufacturingProcessChild?.ToString());
                        row.AppendChild(manufacturingProcessChildCell);

                        Cell ProdActivityCategoryChildCell = new Cell();
                        ProdActivityCategoryChildCell.DataType = CellValues.String;
                        ProdActivityCategoryChildCell.CellValue = new CellValue(s.ProdActivityCategoryChild?.ToString());
                        row.AppendChild(ProdActivityCategoryChildCell);


                        Cell ProdActivityActionChildCell = new Cell();
                        ProdActivityActionChildCell.DataType = CellValues.String;
                        ProdActivityActionChildCell.CellValue = new CellValue(s.ProdActivityActionChild?.ToString());
                        row.AppendChild(ProdActivityActionChildCell);

                        Cell LineCommentCell = new Cell();
                        LineCommentCell.DataType = CellValues.String;
                        LineCommentCell.CellValue = new CellValue(s.LineComment);
                        row.AppendChild(LineCommentCell);

                        Cell prodActivityResultCell = new Cell();
                        prodActivityResultCell.DataType = CellValues.String;
                        prodActivityResultCell.CellValue = new CellValue(s.ProdActivityResult);
                        row.AppendChild(prodActivityResultCell);

                        Cell visaMasterCell = new Cell();
                        visaMasterCell.DataType = CellValues.String;
                        visaMasterCell.CellValue = new CellValue(s.VisaMaster);
                        row.AppendChild(visaMasterCell);

                        Cell routineStatusCell = new Cell();
                        routineStatusCell.DataType = CellValues.String;
                        routineStatusCell.CellValue = new CellValue(s.RoutineStatus);
                        row.AppendChild(routineStatusCell);

                        if (s.ProductionActivityRoutineAppLineQaCheckerModels != null && s.ProductionActivityRoutineAppLineQaCheckerModels.Count > 0)
                        {
                            var readBy = s.ProductionActivityRoutineAppLineQaCheckerModels.Where(w => w.QaCheckUser != null).Select(a => a.QaCheckUser).ToList();
                            Cell ReadBycell = new Cell();
                            ReadBycell.DataType = CellValues.String;
                            ReadBycell.CellValue = new CellValue(string.Join(",", readBy));
                            row.AppendChild(ReadBycell);
                        }
                        else
                        {
                            Cell ReadBycell = new Cell();
                            ReadBycell.DataType = CellValues.String;
                            ReadBycell.CellValue = new CellValue(string.Empty);
                            row.AppendChild(ReadBycell);
                        }
                        Cell modifiedByUsercell = new Cell();
                        modifiedByUsercell.DataType = CellValues.String;
                        modifiedByUsercell.CellValue = new CellValue(s.ModifiedByUser?.ToString());
                        row.AppendChild(modifiedByUsercell);

                        if (s.LineSessionId !=null)
                        {
                            var Url = searchModel.BaseUrl + "?id=" + s.LineSessionId + "&&type=latest";
                            Cell cellHeader = new Cell();
                            cellHeader.DataType = CellValues.InlineString;
                            CellFormula cellFormula1 = new CellFormula() { Space = SpaceProcessingModeValues.Preserve };
                            cellFormula1.Text = @"HYPERLINK(""" + Url + @""", ""Click Here"")";
                            cellHeader.CellFormula = cellFormula1;
                            Run run1 = new Run();
                            run1.Append(new Text("Click Here"));
                            RunProperties run1Properties = new RunProperties();
                            run1Properties.Append(new Bold());
                            run1.RunProperties = run1Properties;
                            InlineString inlineString = new InlineString();
                            inlineString.Append(run1);
                            cellHeader.Append(inlineString);
                            row.AppendChild(cellHeader);
                        }

                        sheetData.AppendChild(row);
                    });
                }
                j++;
                sheetId++;
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        [HttpGet]
        [Route("GetRoutineInfoList")]
        public List<RoutineInfoModel> GetRoutineInfoList()
        {
            List<RoutineInfoModel> routineInfoModels = new List<RoutineInfoModel>();
            var routineInfoList = _context.RoutineInfo.ToList();
            routineInfoList.ForEach(s =>
            {
                RoutineInfoModel routineInfoModel = new RoutineInfoModel();
                routineInfoModel.RoutineInfoId = s.RoutineInfoId;
                routineInfoModel.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                routineInfoModel.Description = s.Description;
                routineInfoModel.AddedByUserID = s.AddedByUserId;
                routineInfoModel.AddedDate = s.AddedDate;
                routineInfoModel.ModifiedByUserID = s.ModifiedByUserId;
                routineInfoModel.StatusCodeID = s.StatusCodeId;
                routineInfoModels.Add(routineInfoModel);
            });
            return routineInfoModels;
        }
        [HttpGet]
        [Route("GetRoutineInfo")]
        public List<RoutineInfoModel> GetRoutineInfo(long? id)
        {
            var routineInfoMultipleLists = new List<RoutineInfoMultiple>();
            List<long?> applicationMasterCodeIds = new List<long?> { 331, 334, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var routineInfoMultiple = _context.RoutineInfoMultiple.Where(s => s.ProductionActivityRoutineAppLineId == id).ToList();
            List<RoutineInfoModel> routineInfoModels = new List<RoutineInfoModel>();

            if (routineInfoMultiple.Count > 0)
            {
                routineInfoMultiple.ForEach(s =>
                {
                    RoutineInfoModel routineInfoModel = new RoutineInfoModel();
                    routineInfoModel.RoutineInfoId = s.RoutineInfoMultipleId;
                    routineInfoModel.Description = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.RoutineInfoId).FirstOrDefault()?.Value;
                    routineInfoModel.AddedByUser = s.AddedByUser?.UserName;
                    routineInfoModel.AddedDate = s.AddedDate;
                    routineInfoModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    routineInfoModels.Add(routineInfoModel);

                });


            }

            return routineInfoModels;
        }
        [HttpPost]
        [Route("InsertRoutineInfo")]
        public RoutineInfoModel InsertRoutineInfo(RoutineInfoModel value)
        {
            var routineinfomultiple = _context.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId).ToList();
            if (routineinfomultiple != null && routineinfomultiple.Count > 0)
            {
                _context.RoutineInfoMultiple.RemoveRange(routineinfomultiple);
                _context.SaveChanges();
            }
            if (value.RoutineInfoIds.Count > 0)
            {
                value.RoutineInfoIds.ForEach(r =>
                {
                    var routineMultiple = new RoutineInfoMultiple();
                    routineMultiple.RoutineInfoId = r;
                    routineMultiple.ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId;
                    routineMultiple.AddedByUserId = value.AddedByUserID;
                    routineMultiple.AddedDate = DateTime.Now;

                    _context.RoutineInfoMultiple.Add(routineMultiple);

                });
                _context.SaveChanges();

            }


            return value;
        }
        [HttpPut]
        [Route("UpdateRoutineChecker")]
        public ProductionActivityRoutineAppModel UpdateRoutineChecker(ProductionActivityRoutineAppModel value)
        {


            var productActivityApp = _context.ProductionActivityRoutineAppLine.SingleOrDefault(s => s.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId);
            productActivityApp.IsCheckNoIssue = value.IsCheckNoIssue;
            productActivityApp.IsCheckReferSupportDocument = value.IsCheckReferSupportDocument;
            productActivityApp.CheckedById = value.AddedByUserID;
            productActivityApp.CheckedDate = DateTime.Now;
            productActivityApp.CheckedRemark = value.CheckedRemark;
            _context.SaveChanges();
            return value;
        }
        [HttpGet]
        [Route("GetProductActivityRoutineChecker")]
        public ProductionActivityRoutineAppModel GetProductActivityChecker(long? id)
        {
            ProductionActivityRoutineAppModel productionActivityAppModel = new ProductionActivityRoutineAppModel();
            var productionActivityAppLine = _context.ProductionActivityRoutineAppLine.Where(w => w.ProductionActivityRoutineAppLineId == id).FirstOrDefault();
            if (productionActivityAppLine != null)
            {
                var userName = "";
                if (productionActivityAppLine.CheckedById != null)
                {
                    var applicationUser = _context.ApplicationUser.Where(s => s.UserId == productionActivityAppLine.CheckedById)?.FirstOrDefault();
                    if (applicationUser != null)
                    {
                        userName = applicationUser.UserName;
                    }
                }
                productionActivityAppModel.IsCheckNoIssue = productionActivityAppLine.IsCheckNoIssue;
                productionActivityAppModel.IsCheckReferSupportDocument = productionActivityAppLine.IsCheckReferSupportDocument;
                productionActivityAppModel.CheckedById = productionActivityAppLine.CheckedById;
                productionActivityAppModel.CheckedRemark = productionActivityAppLine.CheckedRemark;
                productionActivityAppModel.CheckedDate = productionActivityAppLine.CheckedDate;
                productionActivityAppModel.CheckedByUser = userName;
                productionActivityAppModel.ProductionActivityRoutineAppLineId = productionActivityAppLine.ProductionActivityRoutineAppLineId;
            }
            return productionActivityAppModel;
        }
        [HttpPut]
        [Route("UpdateRoutineInfo")]
        public RoutineInfoModel UpdateRoutineInfo(RoutineInfoModel value)
        {
            var routineinfomultiple = _context.RoutineInfoMultiple.Where(r => r.ProductionActivityRoutineAppLineId == value.ProductionActivityRoutineAppLineId).ToList();
            if (routineinfomultiple != null && routineinfomultiple.Count > 0)
            {
                _context.RoutineInfoMultiple.RemoveRange(routineinfomultiple);
                _context.SaveChanges();
            }
            if (value.RoutineInfoIds.Count > 0)
            {
                value.RoutineInfoIds.ForEach(r =>
                {
                    var routineMultiple = new RoutineInfoMultiple();
                    routineMultiple.RoutineInfoId = r;
                    routineMultiple.ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId;
                    _context.RoutineInfoMultiple.Add(routineMultiple);

                });
                _context.SaveChanges();

            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteRoutineInfo")]
        public void DeleteRoutineInfo(int id)
        {
            var routineInfo = _context.RoutineInfo.SingleOrDefault(p => p.RoutineInfoId == id);
            if (routineInfo != null)
            {
                _context.RoutineInfo.Remove(routineInfo);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetProductActivityRoutineComment")]
        public ProductionActivityRoutineAppModel GetProductActivityRoutineComment(long? id)
        {
            ProductionActivityRoutineAppModel productionActivityRoutineAppModel = new ProductionActivityRoutineAppModel();
            var productionActivityRoutineAppLine = _context.ProductionActivityRoutineAppLine.Where(w => w.ProductionActivityRoutineAppLineId == id).FirstOrDefault();
            if (productionActivityRoutineAppLine != null)
            {
                productionActivityRoutineAppModel.LineComment = productionActivityRoutineAppLine.Comment;
                productionActivityRoutineAppModel.CommentImage = productionActivityRoutineAppLine.CommentImage;
                productionActivityRoutineAppModel.CommentImageType = productionActivityRoutineAppLine.CommentImageType;
                productionActivityRoutineAppModel.ProductionActivityRoutineAppLineId = productionActivityRoutineAppLine.ProductionActivityRoutineAppLineId;
                productionActivityRoutineAppModel.ProductionActivityRoutineAppId = productionActivityRoutineAppLine.ProductionActivityRoutineAppId.Value;
            }
            return productionActivityRoutineAppModel;
        }


        [HttpGet]
        [Route("GetProductionRoutineCheckedDetails")]
        public List<ProductionActivityRoutineCheckedDetailsModel> GetProductionRoutineCheckedDetails(long? id)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 334, 324 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var productActivityApps = _context.ProductionActivityRoutineCheckedDetails.Include(p => p.AddedByUser).Include(p => p.ActivityInfo).Include(p => p.CheckedBy).Where(w => w.ProductionActivityRoutineAppLineId == id).ToList();
            List<ProductionActivityRoutineCheckedDetailsModel> ProductionActivityRoutineCheckedDetailsModels = new List<ProductionActivityRoutineCheckedDetailsModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                productActivityApps.ForEach(s =>
                {
                    ProductionActivityRoutineCheckedDetailsModel productActivityApp = new ProductionActivityRoutineCheckedDetailsModel();
                    productActivityApp.ActivityInfoId = s.ActivityInfoId;
                    productActivityApp.IsCheckNoIssue = s.IsCheckNoIssue;
                    productActivityApp.AddedByUserID = s.AddedByUserId;
                    productActivityApp.CommentImage = s.CommentImage;
                    productActivityApp.CommentImageType = s.CommentImageType;
                    productActivityApp.IsCheckReferSupportDocument = s.IsCheckReferSupportDocument;
                    productActivityApp.CheckedComment = s.CheckedComment;
                    productActivityApp.CheckedDate = s.CheckedDate;
                    productActivityApp.CheckedById = s.CheckedById;
                    productActivityApp.ProductionActivityRoutineCheckedDetailsId = s.ProductionActivityRoutineCheckedDetailsId;
                    productActivityApp.ProductionActivityRoutineAppLineId = s.ProductionActivityRoutineAppLineId;
                    productActivityApp.AddedDate = s.AddedDate;
                    productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                    productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                    productActivityApp.ModifiedDate = s.ModifiedDate;
                    productActivityApp.ActivityInfoName = s.ActivityInfo?.CodeValue;
                    productActivityApp.CheckedByUserName = s.CheckedBy?.UserName;
                    productActivityApp.RoutineResultId = s.RoutineResultId;
                    productActivityApp.RoutineStatusId = s.RoutineStatusId;
                    productActivityApp.RoutineResultName = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.RoutineResultId).FirstOrDefault()?.Value;
                    productActivityApp.RoutineStatusName = applicationmasterdetail.Where(w => w.ApplicationMasterDetailId == s.RoutineStatusId).FirstOrDefault()?.Value;
                    ProductionActivityRoutineCheckedDetailsModels.Add(productActivityApp);
                });
            }
            return ProductionActivityRoutineCheckedDetailsModels;
        }
        [HttpPost]
        [Route("InsertProductionRoutineCheckedDetails")]
        public ProductionActivityRoutineCheckedDetailsModel InsertProductionRoutineCheckedDetails(ProductionActivityRoutineCheckedDetailsModel value)
        {
            var productActivityApps = new ProductionActivityRoutineCheckedDetails
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ActivityInfoId = value.ActivityInfoId,
                ProductionActivityRoutineAppLineId = value.ProductionActivityRoutineAppLineId,
                IsCheckNoIssue = value.IsCheckNoIssue,
                CheckedById = value.CheckedById,
                CheckedComment = value.CheckedComment,
                CheckedDate = DateTime.Now,
                CommentImage = value.CommentImage,
                CommentImageType = value.CommentImageType,
                IsCheckReferSupportDocument = value.IsCheckReferSupportDocument,
                ProductionActivityRoutineAppId = value.ProductionActivityRoutineAppId,
                RoutineResultId = value.RoutineResultId,
                RoutineStatusId = value.RoutineStatusId,

            };

            _context.ProductionActivityRoutineCheckedDetails.Add(productActivityApps);
            _context.SaveChanges();
            value.ProductionActivityRoutineCheckedDetailsId = productActivityApps.ProductionActivityRoutineCheckedDetailsId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionRoutineCheckedDetails")]
        public ProductionActivityRoutineCheckedDetailsModel UpdateProductionRoutineCheckedDetails(ProductionActivityRoutineCheckedDetailsModel value)
        {
            var productActivityApp = _context.ProductionActivityRoutineCheckedDetails.SingleOrDefault(s => s.ProductionActivityRoutineCheckedDetailsId == value.ProductionActivityRoutineCheckedDetailsId);
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.ActivityInfoId = value.ActivityInfoId;          
            productActivityApp.CheckedComment = value.CheckedComment;
            productActivityApp.CommentImage = value.CommentImage;
            productActivityApp.CommentImageType = value.CommentImageType;
            productActivityApp.IsCheckNoIssue = value.IsCheckNoIssue;
            productActivityApp.IsCheckReferSupportDocument = value.IsCheckReferSupportDocument;          
            productActivityApp.RoutineStatusId = value.RoutineStatusId;
            productActivityApp.RoutineResultId = value.RoutineResultId;
           

            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionRoutineCheckedDetails")]
        public void DeleteProductionRoutineCheckedDetails(int id)
        {
            var wikiresponsible = _context.ProductionActivityRoutineCheckedDetails.SingleOrDefault(p => p.ProductionActivityRoutineCheckedDetailsId == id);
            if (wikiresponsible != null)
            {
                _context.ProductionActivityRoutineCheckedDetails.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }
    }
}
