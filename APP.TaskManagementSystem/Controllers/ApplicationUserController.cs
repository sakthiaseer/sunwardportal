﻿using APP.DataAccess.Models;
using APP.Common;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using APP.TempleManagement.Helper;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationUserController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly MailService _mailService;
        public MailServiceModel _mailModel;

        public ApplicationUserController(CRT_TMSContext context, IMapper mapper, MailService mailService)
        {
            _context = context;
            _mapper = mapper;
            _mailService = mailService;
        }


        // GET: api/Project
        //WorkDateId
        //WorkDateID
        [HttpGet]
        [Route("GetApplicationUsers")]
        public List<ApplicationUserModel> Get()
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            //var applicationUsers = _context.Employee.Include(u => u.User).Include("Designation").AsNoTracking().ToList();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee where (Status!='Resign' or Status is null) and UserID is Not null";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var applicationUsers = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            applicationUsers.ToList().ForEach(s =>
            {
                var SessionId = appUser.SingleOrDefault(a => a.UserId == s.UserID)?.SessionId;
                ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                applicationUserModel.UserID = s.UserID.Value;
                applicationUserModel.EmployeeID = s.EmployeeID;
                applicationUserModel.UserName = s.FirstName;
                applicationUserModel.DepartmentID = s.DepartmentID;
                applicationUserModel.StatusCodeID = s.StatusCodeID;
                applicationUserModel.AddedByUserID = s.AddedByUserID;
                applicationUserModel.ModifiedByUserID = s.ModifiedByUserID;
                applicationUserModel.AddedDate = s.AddedDate;
                applicationUserModel.ModifiedDate = s.ModifiedDate;
                applicationUserModel.NickName = s.NickName;
                applicationUserModel.LoginID = s.LoginID;
                applicationUserModel.FirstName = s.FirstName;
                applicationUserModel.PlantId = s.PlantID;
                applicationUserModel.SessionId = SessionId;
                applicationUserModel.Designation = s.DesignationName != null ? s.DesignationName : "No Designation";
                applicationUserModel.Name = s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                applicationUserModels.Add(applicationUserModel);
            });

            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);
            return applicationUserModels;
        }
        [HttpGet]
        [Route("GetApplicationUsersByUser")]
        public List<ApplicationUserByModel> GetApplicationUsersByUser()
        {
            List<ApplicationUserByModel> applicationUserModels = new List<ApplicationUserByModel>();
            //var applicationUsers = _context.Employee.Include(u => u.User).Include("Designation").AsNoTracking().ToList();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var applicationUsers = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            applicationUsers.ToList().ForEach(s =>
            {
                var SessionId = appUser.SingleOrDefault(a => a.UserId == s.UserID)?.SessionId;
                ApplicationUserByModel applicationUserModel = new ApplicationUserByModel();
                applicationUserModel.UserID = s.UserID.Value;
                applicationUserModel.EmployeeID = s.EmployeeID;
                applicationUserModel.UserName = s.FirstName;
                applicationUserModel.DepartmentID = s.DepartmentID;
                applicationUserModel.StatusCodeID = s.StatusCodeID;
                applicationUserModel.AddedByUserID = s.AddedByUserID;
                applicationUserModel.ModifiedByUserID = s.ModifiedByUserID;
                applicationUserModel.AddedDate = s.AddedDate;
                applicationUserModel.ModifiedDate = s.ModifiedDate;
                applicationUserModel.NickName = s.NickName;
                applicationUserModel.LoginID = s.LoginID;
                applicationUserModel.FirstName = s.FirstName;
                applicationUserModel.PlantId = s.PlantID;
                applicationUserModel.Status = s.Status;
                applicationUserModel.SessionId = SessionId;
                applicationUserModel.Designation = s.DesignationName != null ? s.DesignationName : "No Designation";
                applicationUserModel.Name = s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                applicationUserModels.Add(applicationUserModel);
            });

            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);
            return applicationUserModels;
        }
        [HttpGet]
        [Route("GetApplicationUsersByUserGroup")]
        public List<ApplicationUserModel> GetApplicationUsersByUserGroup(long id)
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            var userIds = _context.UserGroupUser.Where(w => w.UserId != null && w.UserGroupId == id).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
            string sqlQuery = string.Empty;
            if (userIds != null && userIds.Count > 0)
            {
                sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", userIds) + ")";
                var applicationUsers = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                applicationUsers.ToList().ForEach(s =>
                {
                    var SessionId = _context.ApplicationUser.SingleOrDefault(a => a.UserId == s.UserID)?.SessionId;
                    ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                    applicationUserModel.UserID = s.UserID.Value;
                    applicationUserModel.Id = s.UserID.Value;
                    applicationUserModel.EmployeeID = s.EmployeeID;
                    applicationUserModel.UserName = s.FirstName;
                    applicationUserModel.DepartmentID = s.DepartmentID;
                    applicationUserModel.StatusCodeID = s.StatusCodeID;
                    applicationUserModel.AddedByUserID = s.AddedByUserID;
                    applicationUserModel.ModifiedByUserID = s.ModifiedByUserID;
                    applicationUserModel.AddedDate = s.AddedDate;
                    applicationUserModel.ModifiedDate = s.ModifiedDate;
                    applicationUserModel.NickName = s.NickName;
                    applicationUserModel.LoginID = s.LoginID;
                    applicationUserModel.FirstName = s.FirstName;
                    applicationUserModel.PlantId = s.PlantID;
                    applicationUserModel.SessionId = SessionId;
                    applicationUserModel.Designation = s.DesignationName != null ? s.DesignationName : "No Designation";
                    applicationUserModel.Name = s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                    applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                    applicationUserModels.Add(applicationUserModel);
                });

            }
            return applicationUserModels;
        }
        [HttpGet]
        [Route("GetApplicationUsersBySession")]
        public ApplicationUserModel GetApplicationUsersBySession(Guid? sessionId)
        {
            ApplicationUserModel applicationUserModel = new ApplicationUserModel();
            var applicationUsers = _context.ApplicationUser.Where(w => w.SessionId == sessionId).AsNoTracking().ToList();
            applicationUsers.ForEach(s =>
            {

                applicationUserModel.UserID = s.UserId;
                applicationUserModel.UserName = s.UserName;
                applicationUserModel.DepartmentID = s.DepartmentId;
                applicationUserModel.SessionId = s.SessionId;
            });
            return applicationUserModel;
        }
        [HttpGet]
        [Route("GetApplicationUsersForTask")]
        public List<ApplicationUserModel> GetApplicationUsersForTask()
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            var applicationUsers = _context.Employee.Include(u => u.User).Include("Designation").AsNoTracking().Where(s => s.User.StatusCodeId == 1).ToList();
            applicationUsers.ForEach(s =>
            {

                ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                applicationUserModel.UserID = s.UserId.Value;
                applicationUserModel.EmployeeID = s.EmployeeId;
                applicationUserModel.UserName = s.FirstName;
                applicationUserModel.DepartmentID = s.DepartmentId;
                applicationUserModel.StatusCodeID = s.User?.StatusCodeId;
                applicationUserModel.AddedByUserID = s.AddedByUserId;
                applicationUserModel.ModifiedByUserID = s.ModifiedByUserId;
                applicationUserModel.AddedDate = s.AddedDate;
                applicationUserModel.ModifiedDate = s.ModifiedDate;
                applicationUserModel.NickName = s.NickName;
                applicationUserModel.FirstName = s.FirstName;
                applicationUserModel.Designation = s.Designation != null ? s.Designation.Name : "No Designation";
                applicationUserModel.Name = s.LastName + " | " + s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                applicationUserModels.Add(applicationUserModel);
            });

            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);
            return applicationUserModels;
        }
        [HttpGet("ApplicationUsers")]
        [Route("GetUsers")]
        public List<ApplicationUserModel> GetUsers(UserSelectModel value)
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            var applicationUser = _context.Employee.Include("Designation").Include(s => s.User).Where(s => s.User.StatusCodeId == 1)
                .OrderByDescending(o => o.UserId).AsQueryable().ToList();

            var listofID = new List<long>();

            if (value.AssignedCc != null && value.AssignedCc.Count > 0)
            {
                listofID.AddRange(value.AssignedCc);
            }

            if (value.AssignedTo != null && value.AssignedTo.Count > 0)
            {
                listofID.AddRange(value.AssignedTo);
            }

            if (value.OnBehalfID != null && value.OnBehalfID > 0)
            {
                listofID.Add(value.OnBehalfID.Value);
            }
            if (value.OwnerID != null && value.OwnerID > 0)
            {
                listofID.Add(value.OwnerID.Value);
            }
            //var applicationmodel = applicationUser.Select(s => new ApplicationUserModel
            //{
            if (applicationUser != null && applicationUser.Count > 0)
            {
                applicationUser.ForEach(s =>
                {
                    ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                    applicationUserModel.UserID = s.UserId.Value;
                    applicationUserModel.UserName = s.FirstName;
                    applicationUserModel.DepartmentID = s.DepartmentId;
                    applicationUserModel.StatusCodeID = s.StatusCodeId;
                    applicationUserModel.AddedByUserID = s.AddedByUserId;
                    applicationUserModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationUserModel.AddedDate = s.AddedDate;
                    applicationUserModel.ModifiedDate = s.ModifiedDate;
                    applicationUserModel.NickName = s.NickName;
                    applicationUserModel.FirstName = s.FirstName;
                    applicationUserModel.Designation = s.Designation?.Name;
                    applicationUserModel.Name = s.NickName + " | " + s.Designation?.Name;
                    applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                    if (applicationUserModel.EmplyeeDropDown == null)
                    {
                        applicationUserModel.EmplyeeDropDown = s.FirstName;
                    }
                    applicationUserModels.Add(applicationUserModel);
                });
            }
            //}).Where(u => listofID.Contains(u.UserID)).AsNoTracking().ToList();
            return applicationUserModels;
        }
        [HttpGet("ApplicationUser")]
        [Route("GetUsersById")]
        public List<ApplicationUserModel> GetuserId(int id)
        {

            var userGroup = _context.UserGroupUser.Where(t => t.UserGroupId == id).AsNoTracking().ToList();
            var taskIds = userGroup.Select(s => s.UserId).ToList();

            var applicationUser = _context.Employee.Include("Designation")
                .Where(t => taskIds.Contains(t.UserId))
                .Select(s => new ApplicationUserModel
                {
                    UserID = s.UserId.Value,
                    UserName = s.FirstName,
                    DepartmentID = s.DepartmentId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    NickName = s.NickName,
                    FirstName = s.FirstName,
                    Designation = s.Designation.Name,
                    Name = s.NickName + " | " + s.Designation.Name

                }).OrderByDescending(o => o.UserID).AsNoTracking().ToList();

            return applicationUser;
        }
        [HttpGet("ApplicationUser")]
        [Route("GetUserPassword")]
        public ApplicationUserModel Getuser(int id)
        {



            var applicationUser = _context.ApplicationUser.Select(s => new ApplicationUserModel
            {
                UserID = s.UserId,
                UserName = s.UserName,
                LoginID = s.LoginId,
                LoginPassword = s.LoginPassword,
                DepartmentID = s.DepartmentId,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,


            }).OrderByDescending(o => o.UserID).Where(u => u.UserID == id).FirstOrDefault();
            applicationUser.LoginPassword = EncryptDecryptPassword.Decrypt(applicationUser.LoginPassword);
            return applicationUser;
        }
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get ApplicationUser")]
        [HttpGet("GetApplicationUsers/{id:int}")]
        public ActionResult<ApplicationUserModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationUser = _context.ApplicationUser.SingleOrDefault(p => p.UserId == id.Value);
            var result = _mapper.Map<ApplicationUserModel>(applicationUser);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationUserModel> GetData(SearchModel searchModel)
        {
            var applicationUser = new ApplicationUser();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).LastOrDefault();
                        break;
                    case "Next":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).LastOrDefault();
                        break;
                    case "Next":
                        applicationUser = _context.ApplicationUser.OrderBy(o => o.UserId).FirstOrDefault(s => s.UserId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationUser = _context.ApplicationUser.OrderByDescending(o => o.UserId).FirstOrDefault(s => s.UserId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationUserModel>(applicationUser);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationUser")]
        public ApplicationUserModel Post(ApplicationUserModel value)
        {
            var applicationUser = new ApplicationUser
            {
                //WorkDateId = value.WorkDateID,
                //CountryOptions = value.CountryOptions,
                UserCode = value.UserCode,
                UserName = value.UserName,
                EmployeeNo = value.EmployeeNo,
                UserEmail = value.UserEmail,
                AuthenticationType = value.AuthenticationType,
                LoginId = value.LoginID,
                LoginPassword = value.LoginPassword,
                DepartmentId = value.DepartmentID,
                // StatusCodeID = value.StatusCodeID,
                LastAccessDate = value.LastAccessDate,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = Guid.NewGuid(),

            };
            _context.ApplicationUser.Add(applicationUser);
            _context.SaveChanges();
            value.UserID = applicationUser.UserId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationUser")]
        public ApplicationUserModel Put(ApplicationUserModel value)
        {
            var applicationUser = _context.ApplicationUser.SingleOrDefault(p => p.UserId == value.UserID);
            //applicationUser.WorkDateId = value.WorkDateID;
            applicationUser.UserCode = value.UserCode;
            applicationUser.UserName = value.UserName;
            applicationUser.EmployeeNo = value.EmployeeNo;
            applicationUser.UserEmail = value.UserEmail;
            applicationUser.AuthenticationType = value.AuthenticationType;
            applicationUser.LoginId = value.LoginID;
            applicationUser.LoginPassword = value.LoginPassword;
            applicationUser.DepartmentId = value.DepartmentID;
            applicationUser.LastAccessDate = value.LastAccessDate;
            applicationUser.ModifiedByUserId = value.ModifiedByUserID;
            applicationUser.ModifiedDate = DateTime.Now;
            //applicationUser.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();

            return value;
        }
        [HttpPut]
        [Route("UpdatePassword")]
        public ApplicationUserModel ChangePassword(ApplicationUserModel value)
        {
            var password = EncryptDecryptPassword.Encrypt(value.LoginPassword);
            var applicationUser = _context.ApplicationUser.SingleOrDefault(p => p.UserId == value.UserID);
            applicationUser.LoginPassword = password;
            applicationUser.ModifiedByUserId = value.ModifiedByUserID;
            applicationUser.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationUser")]
        public void Delete(int id)
        {
            var applicationUser = _context.ApplicationUser.SingleOrDefault(p => p.UserId == id);
            if (applicationUser != null)
            {
                _context.ApplicationUser.Remove(applicationUser);
                _context.SaveChanges();
            }
        }


        [HttpGet]
        [Route("GetUserPermission")]
        public List<ApplicationPermissionModel> GetUserPermission(int id, int itemId)
        {
            List<ApplicationPermissionModel> userPermissions = new List<ApplicationPermissionModel>();
            try
            {
                string sqlQuery = string.Empty;
                // Settings.  
                SqlParameter userParam = new SqlParameter("@UserID", id);
                if (itemId == 1)
                {
                    sqlQuery = "Select* from view_UserPermission where IsMobile=1 and UserID = @UserID ORDER BY PermissionOrder";
                }
                else
                {
                    sqlQuery = "Select* from view_UserPermission where UserID = @UserID ORDER BY PermissionOrder";
                }

                var spApplicationPermission = _context.Set<SpUserPermission>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();

                if (itemId == 1)
                {
                    spApplicationPermission = spApplicationPermission.Where(s => s.ParentID > 0).ToList();
                }

                spApplicationPermission.ForEach(s =>
                {
                    ApplicationPermissionModel applicationPermissionModel = new ApplicationPermissionModel
                    {
                        PermissionID = s.PermissionID,
                        PermissionName = s.PermissionName,
                        ParentID = s.ParentID,
                        ActionName = s.ActionName,
                        ControllerName = s.ControllerName,
                        Icon = s.Icon,
                        IsDisplay = s.IsDisplay,
                        MenuId = s.MenuId,
                        PermissionCode = s.PermissionCode,
                        PermissionGroup = s.PermissionGroup,
                        PermissionLevel = s.PermissionLevel,
                        PermissionOrder = s.PermissionOrder,
                        PermissionURL = s.PermissionURL
                    };
                    userPermissions.Add(applicationPermissionModel);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // Info.  
            return userPermissions;
        }


        [Route("GetWebUserPermissionById")]
        public PortalMenuPermission GetWebUserPermissionById(int id)
        {
            string sqlQuery = string.Empty;
            // Settings.  
            SqlParameter userParam = new SqlParameter("@UserID", id);
            sqlQuery = "Select  * from view_UserPermission where UserID = @UserID and IsNewPortal =1 and IsCmsApp is null and  IsProductionApp is null and (IsMobile is null or IsMobile=0) ORDER BY PermissionOrder";
            var applicationUser = _context.Set<SpUserPermission>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();
            var menuList = new List<PortalMenuModel>();
            var permissionList = new List<PortalPermissionModel>();
            var applicationPermissions = applicationUser.Where(p => !p.IsDisplay).ToList();
            applicationPermissions.ForEach(a =>
            {
                permissionList.Add(new PortalPermissionModel
                {
                    ParentID = a.ParentID,
                    PermissionID = a.PermissionID,
                    PermissionCode = a.PermissionCode,
                    ScreenID = a.ScreenID
                });
            });
            applicationUser.Where(p => p.ParentID == null && p.IsDisplay).ToList().ForEach(m =>
                 {
                     var childs = applicationUser.Where(c => c.ParentID == m.PermissionID && c.IsDisplay).ToList();
                     var menu = new PortalMenuModel
                     {
                         Header = m.PermissionName,
                         Title = null,
                         Group = null,
                         Component = null,
                         Name = null,
                         MenuOrder = null,
                         Icon = null,
                         Items = null,
                         ScreenID = null,
                     };

                     if (childs.Count > 0)
                     {
                         menuList.Add(menu);
                         childs.ForEach(ch =>
                         {
                             var pchilds = applicationUser.Where(c => c.ParentID == ch.PermissionID && ch.IsDisplay).ToList();
                             var childmenu = new PortalMenuModel
                             {
                                 Title = ch.PermissionName,
                                 Group = ch.PermissionGroup,
                                 Component = ch.Component,
                                 Name = ch.Name,
                                 MenuOrder = ch.PermissionOrder,
                                 Icon = ch.Icon,
                                 Items = null,
                                 ScreenID = ch.ScreenID,
                             };

                             if (pchilds.Count > 0)
                             {
                                 childmenu.Items = new List<PortalMenuModel>();
                                 pchilds.ForEach(p =>
                                 {
                                     var ppchilds = applicationUser.Where(c => c.ParentID == p.PermissionID && p.IsDisplay).ToList();
                                     var pchildmenu = new PortalMenuModel
                                     {
                                         Title = p.PermissionName,
                                         Group = p.PermissionGroup,
                                         Component = p.Component,
                                         Name = p.Name,
                                         MenuOrder = p.PermissionOrder,
                                         Icon = p.Icon,
                                         Items = null,
                                         Header = (ppchilds.Count > 0) ? "Yes" : "No",
                                         ScreenID = p.ScreenID,
                                     };
                                     childmenu.Items.Add(pchildmenu);
                                     if (ppchilds.Count > 0 && ppchilds.All(d => d.IsDisplay))
                                     {
                                         pchildmenu.Items = new List<PortalMenuModel>();
                                         ppchilds.ForEach(pp =>
                                         {
                                             var ppchildmenu = new PortalMenuModel
                                             {
                                                 Title = pp.PermissionName,
                                                 Group = pp.PermissionGroup,
                                                 Component = pp.Component,
                                                 Name = pp.Name,
                                                 MenuOrder = pp.PermissionOrder,
                                                 Icon = pp.Icon,
                                                 Items = null,
                                                 ScreenID = pp.ScreenID,
                                             };
                                             pchildmenu.Items.Add(ppchildmenu);
                                         });
                                     }
                                 });
                             }
                             menuList.Add(childmenu);
                         });
                     }

                 });
            return new PortalMenuPermission { PortalMenuModels = menuList, PortalPermissionModels = permissionList };
        }

        [Route("GetUserPermissionById")]
        public List<PortalMenuModel> GetUserPermissionById(int id)
        {
            string sqlQuery = string.Empty;
            // Settings.  
            SqlParameter userParam = new SqlParameter("@UserID", id);
            sqlQuery = "Select  * from view_UserPermission where UserID = @UserID and IsNewPortal =1  and isDisplay = 1 and (IsMobile is null or IsMobile=0) ORDER BY PermissionOrder";
            var applicationUser = _context.Set<SpUserPermission>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();
            var menuList = new List<PortalMenuModel>();

            applicationUser.Where(p => p.ParentID == null).ToList().ForEach(m =>
            {
                var childs = applicationUser.Where(c => c.ParentID == m.PermissionID).ToList();
                var menu = new PortalMenuModel
                {
                    Header = m.PermissionName,
                    Title = null,
                    Group = null,
                    Component = null,
                    Name = null,
                    MenuOrder = null,
                    Icon = null,
                    Items = null,
                };

                if (childs.Count > 0)
                {
                    menuList.Add(menu);
                    childs.ForEach(ch =>
                    {
                        var pchilds = applicationUser.Where(c => c.ParentID == ch.PermissionID).ToList();
                        var childmenu = new PortalMenuModel
                        {
                            Title = ch.PermissionName,
                            Group = ch.PermissionGroup,
                            Component = ch.Component,
                            Name = ch.Name,
                            MenuOrder = ch.PermissionOrder,
                            Icon = ch.Icon,
                            Items = null,
                        };

                        if (pchilds.Count > 0)
                        {
                            childmenu.Items = new List<PortalMenuModel>();
                            pchilds.ForEach(p =>
                            {
                                var ppchilds = applicationUser.Where(c => c.ParentID == p.PermissionID).ToList();
                                var pchildmenu = new PortalMenuModel
                                {
                                    Title = p.PermissionName,
                                    Group = p.PermissionGroup,
                                    Component = p.Component,
                                    Name = p.Name,
                                    MenuOrder = p.PermissionOrder,
                                    Icon = p.Icon,
                                    Items = null,
                                    Header = (ppchilds.Count > 0) ? "Yes" : "No",
                                };
                                childmenu.Items.Add(pchildmenu);
                                if (ppchilds.Count > 0)
                                {
                                    pchildmenu.Items = new List<PortalMenuModel>();
                                    ppchilds.ForEach(pp =>
                                    {
                                        var ppchildmenu = new PortalMenuModel
                                        {
                                            Title = pp.PermissionName,
                                            Group = pp.PermissionGroup,
                                            Component = pp.Component,
                                            Name = pp.Name,
                                            MenuOrder = pp.PermissionOrder,
                                            Icon = pp.Icon,
                                            Items = null,
                                        };
                                        pchildmenu.Items.Add(ppchildmenu);
                                    });
                                }
                            });
                        }
                        menuList.Add(childmenu);
                    });
                }

            });
            //var menufooter = new PortalMenuModel
            //{
            //    Header = ".............",
            //    Title = null,
            //    Group = null,
            //    Component = null,
            //    Name = null,
            //    MenuOrder = null,
            //    Icon = null,
            //    Items = null,

            //};
            //menuList.Add(menufooter);
            return menuList;
        }


        [HttpGet]
        [Route("GetCompanies")]
        public List<CompanyModel> GetCompanies()
        {
            var companies = _context.Company.Select(s => new CompanyModel
            {
                CompanyId = s.CompanyId,
                Code = s.CompanyCode

            }).OrderByDescending(o => o.CompanyId).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);
            return companies;
        }

        [HttpGet]
        [Route("GetDepartments")]
        public List<DepartmentModel> GetDepartments()
        {
            var departments = _context.Department.Select(s => new DepartmentModel
            {
                DepartmentId = s.DepartmentId,
                Name = s.Name,
                CompanyId = s.CompanyId

            }).OrderByDescending(o => o.DepartmentId).AsNoTracking().ToList();
            return departments;
        }

        [HttpPost]
        [Route("PostError")]
        public bool PostError(ErrorDetails errorDetails)
        {
            _mailModel = new MailServiceModel
            {
                Body = errorDetails.Message,
                Subject = "Sunward Error Details",
            };
            return _mailService.EmailService(_mailModel);
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCodeMasterByType")]
        public List<CodeMasterModel> GetCodeMasterByType(string type)
        {
            var codeMasterDetails = _context.CodeMaster.Select(s => new CodeMasterModel
            {
                CodeID = s.CodeId,
                CodeType = s.CodeType,
                CodeValue = s.CodeValue,
                CodeDescription = s.CodeDescription,

            }).Where(o => o.CodeType == type).AsNoTracking().ToList();
            //var result = _mapper.Map<List<FoldersModel>>(Folders);
            return codeMasterDetails;

        }

        [HttpGet]
        [Route("GetPlantsCodeMaster")]
        public List<CodeMasterModel> GetPlantsCodeMaster()
        {
            return _context.Plant.Select(p => new CodeMasterModel { Id = p.PlantId, CodeValue = p.PlantCode, CodeDescription = p.Description }).ToList();
        }

        [HttpGet]
        [Route("GetDivisionsCodeMaster")]
        public List<CodeMasterModel> GetDivisionsCodeMaster(int id)
        {
            return _context.Division.Where(s => s.CompanyId == id).Select(p => new CodeMasterModel { Id = p.DivisionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
        }


        [HttpGet]
        [Route("GetDepartmentCodeMaster")]
        public List<CodeMasterModel> GetDepartmentCodeMaster(int id)
        {
            return _context.Department.Where(s => s.DivisionId == id).Select(p => new CodeMasterModel { Id = p.DepartmentId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
        }

        [HttpGet]
        [Route("GetSectionCodeMaster")]
        public List<CodeMasterModel> GetSectionCodeMaster(int id)
        {
            return _context.Section.Where(s => s.DepartmentId == id).Select(p => new CodeMasterModel { Id = p.SectionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
        }


        [HttpGet]
        [Route("GetSubSectionCodeMaster")]
        public List<CodeMasterModel> GetSubSectionCodeMaster(int id)
        {
            return _context.SubSection.Where(s => s.SectionId == id).Select(p => new CodeMasterModel { Id = p.SubSectionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
        }


        [HttpGet]
        [Route("UpdateSessionApplicationUsers")]
        public void UpdateSessionApplicationUsers()
        {
            var appuser = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => w.SessionId == null).ToList();
            if (appuser != null && appuser.Count > 0)
            {
                appuser.ForEach(s =>
                {
                    var sessionId = Guid.NewGuid();
                    var query = string.Format("Update ApplicationUser Set SessionId = '{1}' Where UserId in" + '(' + "{0}" + ')', s.UserId, sessionId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                });
            }
        }
        [Route("GetWebUserPermissionByProductionId")]
        public PortalMenuPermission GetWebUserPermissionByProductionId(int id)
        {
            string sqlQuery = string.Empty;
            // Settings.  
            SqlParameter userParam = new SqlParameter("@UserID", id);
            sqlQuery = "Select  * from view_UserPermission where UserID = @UserID and IsNewPortal =1 and IsProductionApp=1 and (IsMobile is null or IsMobile=0) ORDER BY PermissionOrder";
            var applicationUser = _context.Set<SpUserPermission>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();
            var menuList = new List<PortalMenuModel>();
            var permissionList = new List<PortalPermissionModel>();
            var applicationPermissions = applicationUser.Where(p => !p.IsDisplay).ToList();
            applicationPermissions.ForEach(a =>
            {
                permissionList.Add(new PortalPermissionModel
                {
                    ParentID = a.ParentID,
                    PermissionID = a.PermissionID,
                    PermissionCode = a.PermissionCode,
                    ScreenID = a.ScreenID
                });
            });
            applicationUser.Where(p => p.ParentID == null && p.IsDisplay).ToList().ForEach(m =>
            {
                var childs = applicationUser.Where(c => c.ParentID == m.PermissionID && c.IsDisplay).ToList();
                var menu = new PortalMenuModel
                {
                    Header = m.PermissionName,
                    Title = null,
                    Group = null,
                    Component = null,
                    Name = null,
                    MenuOrder = null,
                    Icon = null,
                    Items = null,
                    ScreenID = null,
                };

                if (childs.Count > 0)
                {
                    menuList.Add(menu);
                    childs.ForEach(ch =>
                    {
                        var pchilds = applicationUser.Where(c => c.ParentID == ch.PermissionID && ch.IsDisplay).ToList();
                        var childmenu = new PortalMenuModel
                        {
                            Title = ch.PermissionName,
                            Group = ch.PermissionGroup,
                            Component = ch.Component,
                            Name = ch.Name,
                            MenuOrder = ch.PermissionOrder,
                            Icon = ch.Icon,
                            Items = null,
                            ScreenID = ch.ScreenID,
                        };

                        if (pchilds.Count > 0)
                        {
                            childmenu.Items = new List<PortalMenuModel>();
                            pchilds.ForEach(p =>
                            {
                                var ppchilds = applicationUser.Where(c => c.ParentID == p.PermissionID && p.IsDisplay).ToList();
                                var pchildmenu = new PortalMenuModel
                                {
                                    Title = p.PermissionName,
                                    Group = p.PermissionGroup,
                                    Component = p.Component,
                                    Name = p.Name,
                                    MenuOrder = p.PermissionOrder,
                                    Icon = p.Icon,
                                    Items = null,
                                    Header = (ppchilds.Count > 0) ? "Yes" : "No",
                                    ScreenID = p.ScreenID,
                                };
                                childmenu.Items.Add(pchildmenu);
                                if (ppchilds.Count > 0 && ppchilds.All(d => d.IsDisplay))
                                {
                                    pchildmenu.Items = new List<PortalMenuModel>();
                                    ppchilds.ForEach(pp =>
                                    {
                                        var ppchildmenu = new PortalMenuModel
                                        {
                                            Title = pp.PermissionName,
                                            Group = pp.PermissionGroup,
                                            Component = pp.Component,
                                            Name = pp.Name,
                                            MenuOrder = pp.PermissionOrder,
                                            Icon = pp.Icon,
                                            Items = null,
                                            ScreenID = pp.ScreenID,
                                        };
                                        pchildmenu.Items.Add(ppchildmenu);
                                    });
                                }
                            });
                        }
                        menuList.Add(childmenu);
                    });
                }

            });
            return new PortalMenuPermission { PortalMenuModels = menuList, PortalPermissionModels = permissionList };
        }
    }
}