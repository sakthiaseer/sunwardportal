﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ACItemsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ACItemsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetACItems")]
        public List<ACItemsModel> Get()
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };

            var acitems = _context.Acitems.Include("Company").Include("ModifiedByUser").Include("AddedByUser").Include("Customer").Select(s => new ACItemsModel
            {
                DistACID = s.DistAcid,
                ItemNo = s.ItemNo,
                DistName = s.DistName,
                ItemGroup = s.ItemGroup,
                Steriod = s.Steriod,
                ShelfLife = s.ShelfLife,
                Quota = s.Quota,
                Status = s.Status,
                ItemDesc = s.ItemDesc,
                PackSize = s.PackSize,
                ACQty = 0,
                ACMonth = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                CompanyId = s.CompanyId,
                CustomerId = s.CustomerId,
                CompanyName = s.Company.PlantCode,
            }).Where(f => f.StatusCodeID == 1).AsNoTracking().ToList();

            var acItemIDs = acitems.Select(c => c.DistACID).ToList();
            var navItemCList = _context.NavItemCitemList.Where(c => acItemIDs.Contains(c.NavItemCustomerItemId.Value)).ToList();
            var navItems = _context.Navitems.Include("GenericCode").Where(i => navItemCList.Select(c => c.NavItemId).Distinct().Contains(i.ItemId) && i.StatusCodeId == 1).AsNoTracking().ToList();

            acitems.ForEach(a =>
            {
                var navCItem = navItemCList.FirstOrDefault(c => c.NavItemCustomerItemId == a.DistACID);
                if (navCItem != null)
                {

                    var navItem = navItems.FirstOrDefault(n => n.ItemId == navCItem.NavItemId);
                    var groupName = navItem != null ? navItem.CategoryId.GetValueOrDefault(0) : 0;
                    a.PackSize = navItem != null ? navItem.PackSize.ToString() : string.Empty;
                    a.Packuom = navItem != null ? navItem.PackUom : string.Empty;
                    a.GenericCode = navItem != null ? navItem.GenericCode?.Code : string.Empty;
                    a.SWItemNo = navItem != null ? navItem.No : "Not Mapped";
                    a.SWItemId = navItem != null ? navItem.ItemId : 0;
                    a.ItemIds = navItem != null ? navItemCList.Where(n => n.NavItemCustomerItemId == navCItem.NavItemCustomerItemId).Select(n => n.NavItemId).ToList() : null;
                    a.CategoryCode = navItem != null ? navItem.ItemCategoryCode : "";
                    a.ItemGroup = groupName > 0 ? categoryList[int.Parse(groupName.ToString()) - 1].ToString() : "";
                    a.ShelfLife = navItem != null ? navItem.ExpirationCalculation : "";
                    a.InternalRefNo = navItem != null ? navItem.InternalRef : "";
                    a.SWItemDesc = navItem != null ? navItem.Description : "";
                    a.SWItemDesc2 = navItem != null ? navItem.Description2 : "";
                }
                else
                {
                    a.SWItemNo = "Not Mapped";
                }
            });


            //var result = _mapper.Map<List<AcitemsModel>>(Acitems);
            return acitems.OrderByDescending(o => o.DistACID).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetACItembyCountry")]
        public List<ACItemsModel> GetACItembyCountry(long Id)
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };

            var acitems = _context.Acitems.Include("Company").Include("ModifiedByUser").Include("AddedByUser").Include("Customer").Where(f => f.CompanyId == Id).Select(s => new ACItemsModel
            {
                DistACID = s.DistAcid,
                ItemNo = s.ItemNo,
                DistName = s.DistName,
                ItemGroup = s.ItemGroup,
                Steriod = s.Steriod,
                ShelfLife = s.ShelfLife,
                Quota = s.Quota,
                Status = s.Status,
                ItemDesc = s.ItemDesc,
                PackSize = s.PackSize,
                ACQty = 0,
                ACMonth = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                CompanyId = s.CompanyId,
                CustomerId = s.CustomerId,
                CompanyName = s.Company.PlantCode,
            }).Where(f => f.StatusCodeID == 1).AsNoTracking().ToList();

            var acItemIDs = acitems.Select(c => c.DistACID).ToList();
            var navItemCList = _context.NavItemCitemList.Where(c => acItemIDs.Contains(c.NavItemCustomerItemId.Value)).ToList();
            var navItems = _context.Navitems.Include("GenericCode").Where(i => navItemCList.Select(c => c.NavItemId).Distinct().Contains(i.ItemId) && i.CompanyId == Id).ToList();

            acitems.ForEach(a =>
            {
                var navCItem = navItemCList.FirstOrDefault(c => c.NavItemCustomerItemId == a.DistACID);
                if (navCItem != null)
                {

                    var navItem = navItems.FirstOrDefault(n => n.ItemId == navCItem.NavItemId);
                    var groupName = navItem != null ? navItem.CategoryId.GetValueOrDefault(0) : 0;
                    a.PackSize = navItem != null ? navItem.PackSize.ToString() : string.Empty;
                    a.Packuom = navItem != null ? navItem.PackUom : string.Empty;
                    a.GenericCode = navItem != null ? navItem.GenericCode?.Code : string.Empty;
                    a.SWItemNo = navItem != null ? navItem.No : "Not Mapped";
                    //a.SWItemId = navItem != null ? navItem.ItemId : 0;
                    a.ItemIds = navItem != null ? navItemCList.Where(n => n.NavItemCustomerItemId == navCItem.NavItemCustomerItemId).Select(n => n.NavItemId).ToList() : null;
                    a.CategoryCode = navItem != null ? navItem.ItemCategoryCode : "";
                    a.ItemGroup = groupName > 0 ? categoryList[int.Parse(groupName.ToString()) - 1].ToString() : "";
                    a.ShelfLife = navItem != null ? navItem.ExpirationCalculation : "";
                    a.InternalRefNo = navItem != null ? navItem.InternalRef : "";
                    a.SWItemDesc = navItem != null ? navItem.Description : "";
                    a.SWItemDesc2 = navItem != null ? navItem.Description2 : "";
                }
                else
                {
                    a.SWItemNo = "Not Mapped";
                }
            });


            //var result = _mapper.Map<List<AcitemsModel>>(Acitems);
            return acitems.OrderByDescending(o => o.DistACID).ToList();
        }

        [HttpGet]
        [Route("GetNoACItems")]
        public List<ACItemsModel> GetNoACItems()
        {
            var acitems = _context.Acitems.Include("ModifiedByUser").Include("AddedByUser").Include("Customer").Select(s => new ACItemsModel
            {
                DistACID = s.DistAcid,
                ItemNo = s.ItemNo,
                DistName = s.DistName,
                ItemGroup = s.ItemGroup,
                Steriod = s.Steriod,
                ShelfLife = s.ShelfLife,
                Quota = s.Quota,
                Status = s.Status,
                ItemDesc = s.ItemDesc,
                PackSize = s.PackSize,
                ACQty = 0,
                ACMonth = DateTime.Now,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate
            }).Where(f => f.StatusCodeID == 2).AsNoTracking().ToList();
            return acitems.OrderByDescending(o => o.DistACID).ToList();
        }

        [HttpPost]
        [Route("GetItemNo")]
        public List<ACItemsModel> GetItemNo(ACItemsModel value)
        {
            var acitems = _context.Acitems.Include("ModifiedByUser").Include("AddedByUser").Select(s => new ACItemsModel
            {
                DistACID = s.DistAcid,
                ItemNo = s.ItemNo,
                DistName = s.Customer.Name,
                CustomerId = s.CustomerId,
                CompanyId = s.CompanyId,
                CompanyName = s.Company.Description,

            }).Where(f => f.CustomerId == value.CustomerId && f.CompanyId == value.CompanyId).AsNoTracking().ToList();
            return acitems;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ACItemsModel> GetData(SearchModel searchModel)
        {
            var acitems = new Acitems();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).FirstOrDefault();
                        break;
                    case "Last":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).LastOrDefault();
                        break;
                    case "Next":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).LastOrDefault();
                        break;
                    case "Previous":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).FirstOrDefault();
                        break;
                    case "Last":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).LastOrDefault();
                        break;
                    case "Next":
                        acitems = _context.Acitems.OrderBy(o => o.DistAcid).FirstOrDefault(s => s.DistAcid > searchModel.Id);
                        break;
                    case "Previous":
                        acitems = _context.Acitems.OrderByDescending(o => o.DistAcid).FirstOrDefault(s => s.DistAcid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ACItemsModel>(acitems);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertACItems")]
        public ACItemsModel Post(ACItemsModel value)
        {
            var acitems = new Acitems
            {
                DistName = value.DistName,
                ItemNo = value.ItemNo,
                ItemGroup = value.ItemGroup,
                Steriod = value.Steriod,
                ShelfLife = value.ShelfLife,
                Quota = value.Quota,
                Status = value.Status,
                ItemDesc = value.ItemDesc,
                PackSize = value.PackSize,
                Acqty = value.ACQty,
                Acmonth = value.ACMonth,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = 1,
                CompanyId = value.CompanyId,
                CustomerId = value.CustomerId,
            };
            _context.Acitems.Add(acitems);
            _context.SaveChanges();
            value.DistACID = acitems.DistAcid;
            value.ItemIds.ForEach(sw =>
            {
                if (sw.GetValueOrDefault(0) > 0)
                {
                    var navItemCitemList = new NavItemCitemList
                    {
                        NavItemId = sw,
                        NavItemCustomerItemId = value.DistACID,
                    };
                    value.SWItemNo = _context.Navitems.FirstOrDefault(n => n.ItemId == sw).No;
                    _context.NavItemCitemList.Add(navItemCitemList);
                    _context.SaveChanges();
                }

            });


            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateACItems")]
        public ACItemsModel Put(ACItemsModel value)
        {
            if (value.DistACID <= 0)
            {
                return Post(value);
            }
            else
            {

                var acitems = _context.Acitems.SingleOrDefault(p => p.DistAcid == value.DistACID);
                var deleteNavCItemList = _context.NavItemCitemList.Where(c => c.NavItemCustomerItemId == value.DistACID).ToList();
                _context.NavItemCitemList.RemoveRange(deleteNavCItemList);
                _context.SaveChanges();
                value.SWItemNo = string.Empty;
                if (value.ItemIds != null)
                {
                    value.ItemIds.ForEach(sw =>
                    {
                        if (sw.GetValueOrDefault(0) > 0)
                        {
                            var navItemCitemList = new NavItemCitemList
                            {
                                NavItemId = sw,
                                NavItemCustomerItemId = value.DistACID,
                            };
                            value.SWItemNo = _context.Navitems.FirstOrDefault(n => n.ItemId == sw).No;
                            _context.NavItemCitemList.Add(navItemCitemList);
                            _context.SaveChanges();
                        }

                    });
                }

                var existing = _context.Acitems.Where(a => a.CustomerId == value.CustomerId && a.CompanyId == value.CompanyId && a.ItemNo == value.ItemNo).FirstOrDefault();
                if (existing == null)
                {
                    acitems.DistName = acitems.DistName;
                    acitems.ItemGroup = value.ItemGroup;
                    acitems.Steriod = value.Steriod;
                    acitems.ShelfLife = value.ShelfLife;
                    acitems.Quota = value.Quota;
                    acitems.Status = value.Status;
                    acitems.PackSize = value.PackSize;
                    acitems.ItemDesc = value.ItemDesc;
                    acitems.Acqty = value.ACQty;
                    acitems.Acmonth = value.ACMonth;
                    acitems.ModifiedByUserId = value.ModifiedByUserID;
                    acitems.ModifiedDate = DateTime.Now;
                    acitems.ItemNo = value.ItemNo;
                    acitems.StatusCodeId = 1;
                    acitems.CompanyId = value.CompanyId;
                    acitems.CustomerId = value.CustomerId;
                    _context.SaveChanges();
                }
                else
                {

                }

                return value;
            }
        }

        [HttpPost]
        [Route("SaveDistStockBalance")]
        public DistStockBalanceModel SaveDistStockBalance(DistStockBalanceModel value)
        {
            int stockBalWeek = 0;
            DistStockBalance distItem = null;
            if (value.StockBalMonth != null)
            {
                stockBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
                var distStockBalances = _context.DistStockBalance.Where(d => d.StockBalMonth.Value.Month == value.StockBalMonth.Value.Month && d.StockBalMonth.Value.Year == value.StockBalMonth.Value.Year).ToList();
                if (distStockBalances.Any(s => s.StockBalWeek != null) && stockBalWeek > 0)
                {
                    distItem = distStockBalances.FirstOrDefault(b => b.StockBalWeek == stockBalWeek);
                }
                else
                {
                    distItem = distStockBalances.FirstOrDefault();
                }
            }
            if (distItem != null)
            {
                distItem.StockBalMonth = value.StockBalMonth;
                distItem.StockBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
                distItem.Quantity = value.Quantity;
                distItem.Poquantity = value.PoQty;
                distItem.ModifiedByUserId = value.AddedByUserID;
                distItem.ModifiedDate = DateTime.Now;
                distItem.StatusCodeId = 1;
            }
            else
            {
                distItem = new DistStockBalance
                {
                    DistItemId = value.DistItemId,
                    StockBalMonth = value.StockBalMonth,
                    Quantity = value.Quantity,
                    Poquantity =value.PoQty,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                distItem.StockBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
                _context.DistStockBalance.Add(distItem);
            }

            _context.SaveChanges();
            var acItem = _context.Acitems.FirstOrDefault(a => a.DistAcid == distItem.DistItemId);
            value.DistName = acItem.DistName;
            value.ItemName = acItem.ItemNo;
            value.DistStockBalanceId = distItem.DistStockBalanceId;
            value.StockBalWeek = stockBalWeek;
            return value;
        }

        [HttpPut]
        [Route("UpdateDistStockBalance")]
        public StockBalanceModel UpdateDistStockBalanceQuantity(StockBalanceModel value)
        {
            int stkBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
            var distItem = _context.DistStockBalance.FirstOrDefault(d => d.DistItemId == value.ACId && d.StockBalMonth.Value.Month == value.StockBalMonth.Value.Month && d.StockBalMonth.Value.Year == value.StockBalMonth.Value.Year && d.StockBalWeek == stkBalWeek);
            if (distItem != null)
            {
                if (distItem.StockBalMonth != null)
                {
                    distItem.StockBalWeek = GetWeekNumberOfMonth(distItem.StockBalMonth.Value);
                }
                distItem.Quantity = value.Quantity;
                distItem.Poquantity = value.PoQty;
                distItem.StockBalMonth = value.StockBalMonth;
                distItem.ModifiedByUserId = value.AddedByUserID;
                distItem.ModifiedDate = DateTime.Now;
                distItem.StatusCodeId = 1;

                value.StockBalWeek = distItem.StockBalWeek;
            }
            else
            {
                DistStockBalance distStockBalance = new DistStockBalance
                {
                    DistItemId = value.ACId,
                    Quantity = value.Quantity,
                    StockBalMonth = value.StockBalMonth,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                };
                if (distStockBalance.StockBalMonth != null)
                {
                    distStockBalance.StockBalWeek = stkBalWeek;
                }
                _context.DistStockBalance.Add(distStockBalance);
                value.StockBalWeek = distStockBalance.StockBalWeek;
            }

            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("AddDistMaster")]
        public ACItemsModel AddDistMaster(ACItemsModel value)
        {
            var distItem = _context.Acitems.FirstOrDefault(d => d.DistAcid == value.DistACID);
            if (distItem != null)
            {
                distItem.StatusCodeId = 1;
            }
            _context.SaveChanges();
            return value;
        }


        [HttpDelete]
        [Route("DeleteStockBalanceItem")]
        public void DeleteStockBalanceItem(int id)
        {
            var acstkBal = _context.DistStockBalance.FirstOrDefault(f => f.DistStockBalanceId == id);
            _context.DistStockBalance.Remove(acstkBal);
            _context.SaveChanges();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteACItems")]
        public void Delete(int id)
        {
            var acstkBal = _context.DistStockBalance.Where(f => f.DistItemId == id).ToList();
            var mapper = _context.NavItemCitemList.Where(f => f.NavItemCustomerItemId == id).ToList();
            if (acstkBal.Count > 0) _context.DistStockBalance.RemoveRange(acstkBal);
            if (mapper.Count > 0) _context.NavItemCitemList.RemoveRange(mapper);
            _context.SaveChanges();

            var acitems = _context.Acitems.SingleOrDefault(p => p.DistAcid == id);
            if (acitems != null)
            {
                _context.Acitems.Remove(acitems);
                _context.SaveChanges();
            }
        }


        [HttpGet]
        [Route("GetDDACItems")]
        public List<ACItemDDModel> GetDDACItems()
        {
            var acitems = _context.Acitems.Select(s => new ACItemDDModel
            {
                DistACID = s.DistAcid,
                Description = (String.IsNullOrEmpty(s.ItemNo) ? "No ItemNo" : s.ItemNo) + " | " + s.ItemDesc + " | " + (String.IsNullOrEmpty(s.PackSize) ? "No Packsize" : s.PackSize)
            }).OrderByDescending(o => o.DistACID).AsNoTracking().ToList();
            return acitems;
        }

        [HttpGet]
        [Route("GetDistStockBalanceItemsById")]
        public List<DistStockBalanceModel> GetDistStockBalanceItemsById(int id)
        {
            var distStockBalances = _context.DistStockBalance.Include("DistItem").Select(s => new DistStockBalanceModel
            {
                DistStockBalanceId = s.DistStockBalanceId,
                DistItemId = s.DistItemId,
                DistName = s.DistItem.DistName,
                ItemName = s.DistItem.ItemNo,
                Quantity = s.Quantity,
                PoQty =s.Poquantity,
                StockBalMonth = s.StockBalMonth,
                StockBalWeek = s.StockBalWeek
            }).Where(d => d.DistItemId == id).AsNoTracking().ToList();
            return distStockBalances.OrderByDescending(o => o.DistStockBalanceId).ToList();
        }

        private int GetWeekNumberOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDay = (int)firstDayOfMonth.DayOfWeek;
            if (firstDay == 0)
            {
                firstDay = 7;
            }
            double d = (firstDay + date.Day - 1) / 7.0;
            return d > 5 ? (int)Math.Floor(d) : (int)Math.Ceiling(d);
        }

    }
}