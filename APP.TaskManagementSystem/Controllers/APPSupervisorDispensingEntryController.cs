﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class APPSupervisorDispensingEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public APPSupervisorDispensingEntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPSupervisorDispensingEntrys")]
        public List<APPSupervisorDispensingEntryModel> Get()
        {
            var appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.Include("ModifiedByUser").GroupBy(w => new { w.WorkOrderNo, w.ItemName, w.DrumNo }).Select(s => new APPSupervisorDispensingEntryModel
            {

                // SupervisorDispensingId = s.Key.
                WorkOrderNo = s.Key.WorkOrderNo,
                ItemName = s.Key.ItemName,
                DrumNo = s.Key.DrumNo,

            }).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPSupervisorDispensingEntryModel>>(APPSupervisorDispensingEntry);
            return appSupervisorDispensingEntry;
        }

        //Changes Done by Aravinth Start

        [HttpPost]
        [Route("GetAPPSupervisorDispensingEntryFilter")]
        public List<APPSupervisorDispensingEntryModel> GetFilter(APPSupervisorDispensingEntrySearchModel value)
        {
            var appsupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new APPSupervisorDispensingEntryModel
            {
                SupervisorDispensingId = s.SupervisorDispensingId,
                WorkOrderNo = s.WorkOrderNo,
                ItemName = s.ItemName,
                DrumNo = s.DrumNo,

            }).AsNoTracking().OrderByDescending(o => o.SupervisorDispensingId).ToList();

            List<APPSupervisorDispensingEntryModel> filteredItems = new List<APPSupervisorDispensingEntryModel>();
            if (value.WorkOrderNo.Any())
            {
                var itemWorkOrderNoFilters = appsupervisorDispensingEntry.Where(p => p.WorkOrderNo != null ? (p.WorkOrderNo.ToLower() == (value.WorkOrderNo.ToLower())) : false).ToList();
                itemWorkOrderNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.SupervisorDispensingId == i.SupervisorDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ItemName.Any())
            {
                var itemItemNameFilters = appsupervisorDispensingEntry.Where(p => p.ItemName != null ? (p.ItemName.ToLower() == (value.ItemName.ToLower())) : false).ToList();
                itemItemNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.SupervisorDispensingId == i.SupervisorDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.DrumNo.Any())
            {
                var itemDrumNoFilters = appsupervisorDispensingEntry.Where(p => p.DrumNo != null ? (p.DrumNo.ToLower() == (value.DrumNo.ToLower())) : false).ToList();
                itemDrumNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.SupervisorDispensingId == i.SupervisorDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            
            return filteredItems;
        }

        //Changes Done by Aravinth End

        [HttpGet]
        [Route("GetDispensingEntryByID")]
        public List<APPSupervisorDispensingEntryModel> GetDispensingByWorkOrder(string Id)
        {
            var appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.Select(s => new APPSupervisorDispensingEntryModel
            {
                SupervisorDispensingId = s.SupervisorDispensingId,
                PreLineClearance = s.PreLineClearance.Value,
                WeighingMachine = s.WeighingMachine,
                WorkOrderNo = s.WorkOrderNo,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemName = s.ItemName,
                LotNo = s.LotNo,
                JobNo = s.JobNo,
                SubLotNo = s.SubLotNo,
                UOM = s.Uom,
                QCRefNo = s.QcrefNo,
                DrumNo = s.DrumNo,
                DrumWeight = s.DrumWeight,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate


            }).OrderByDescending(o => o.SupervisorDispensingId).Where(t => t.WorkOrderNo == Id).AsNoTracking().ToList();

            return appSupervisorDispensingEntry;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPSupervisorDispensingEntryModel> GetData(SearchModel searchModel)
        {
            var appSupervisorDispensingEntry = new AppsupervisorDispensingEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).FirstOrDefault();
                        break;
                    case "Last":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).LastOrDefault();
                        break;
                    case "Next":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).LastOrDefault();
                        break;
                    case "Previous":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).FirstOrDefault();
                        break;
                    case "Last":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).LastOrDefault();
                        break;
                    case "Next":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderBy(o => o.SupervisorDispensingId).FirstOrDefault(s => s.SupervisorDispensingId > searchModel.Id);
                        break;
                    case "Previous":
                        appSupervisorDispensingEntry = _context.AppsupervisorDispensingEntry.OrderByDescending(o => o.SupervisorDispensingId).FirstOrDefault(s => s.SupervisorDispensingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPSupervisorDispensingEntryModel>(appSupervisorDispensingEntry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSupervisorDispensing")]
        public APPSupervisorDispensingEntryModel Post(APPSupervisorDispensingEntryModel value)
        {


            var appDispensingEntry = new AppsupervisorDispensingEntry
            {

                PreLineClearance = value.PreLineClearance,
                WeighingMachine = value.WeighingMachine,
                WorkOrderNo = value.WorkOrderNo,
                ProdOrderNo = value.ProdOrderNo,
                ProdLineNo = value.ProdLineNo,
                ItemName = value.ItemName,
                LotNo = value.LotNo,
                SubLotNo = value.SubLotNo,
                Uom = value.UOM,
                JobNo = value.JobNo,
                QcrefNo = value.QCRefNo,
                DrumNo = value.DrumNo,
                DrumWeight = value.DrumWeight,
                StatusCodeId = 1,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,


            };
            _context.AppsupervisorDispensingEntry.Add(appDispensingEntry);

            _context.SaveChanges();

            value.SupervisorDispensingId = appDispensingEntry.SupervisorDispensingId;
            return value;
        }
        [HttpPost]
        [Route("PostDispensing")]
        public APPSupervisorDispensingEntryModel PostDispensing(APPSupervisorDispensingEntryModel value)
        {
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAPPSupervisorDispensingEntry")]
        public APPSupervisorDispensingEntryModel Put(APPSupervisorDispensingEntryModel value)
        {
            var appDispensingEntry = _context.AppsupervisorDispensingEntry.SingleOrDefault(p => p.SupervisorDispensingId == value.SupervisorDispensingId);
            appDispensingEntry.PreLineClearance = value.PreLineClearance;
            appDispensingEntry.WeighingMachine = value.WeighingMachine;
            appDispensingEntry.WorkOrderNo = value.WorkOrderNo;
            appDispensingEntry.ProdOrderNo = value.ProdOrderNo;
            appDispensingEntry.ProdLineNo = value.ProdLineNo;
            appDispensingEntry.ItemName = value.ItemName;

            appDispensingEntry.QcrefNo = value.QCRefNo;
            appDispensingEntry.LotNo = value.LotNo;
            appDispensingEntry.DrumNo = value.DrumNo;
            appDispensingEntry.DrumWeight = value.DrumWeight;
            //APPSupervisorDispensingEntry.APPSupervisorDispensingEntryId = value.APPSupervisorDispensingEntryID;
            appDispensingEntry.ModifiedByUserId = value.ModifiedByUserID;
            appDispensingEntry.ModifiedDate = DateTime.Now;

            appDispensingEntry.ModifiedByUserId = value.ModifiedByUserID;
            appDispensingEntry.ModifiedDate = value.ModifiedDate;
            appDispensingEntry.StatusCodeId = value.StatusCodeID;
            appDispensingEntry.AddedByUserId = value.AddedByUserID;
            appDispensingEntry.AddedDate = value.AddedDate;
            _context.SaveChanges();



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDispensingEntry")]
        public void Delete(int id)
        {
            var appDispensingEntry = _context.AppsupervisorDispensingEntry.SingleOrDefault(p => p.SupervisorDispensingId == id);
            var errorMessage = "";
            try
            {
                if (appDispensingEntry != null)
                {
                    _context.AppsupervisorDispensingEntry.Remove(appDispensingEntry);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}