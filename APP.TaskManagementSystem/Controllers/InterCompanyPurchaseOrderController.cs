using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class InterCompanyPurchaseOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;


        public InterCompanyPurchaseOrderController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetInterCompanyPurchaseOrder")]
        public List<InterCompanyPurchaseOrderModel> Get()
        {
            var purchaseOrder = _context.InterCompanyPurchaseOrder.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(c => c.BuyerCompany).
                Include(s => s.StatusCode).Include(u => u.Login).AsNoTracking().ToList();
            List<InterCompanyPurchaseOrderModel> purchaseOrderModel = new List<InterCompanyPurchaseOrderModel>();
            purchaseOrder.ForEach(s =>
            {
                InterCompanyPurchaseOrderModel purchaseOrderModels = new InterCompanyPurchaseOrderModel
                {
                    InterCompanyPurchaseOrderId = s.InterCompanyPurchaseOrderId,
                    Date = s.Date,
                    LoginId = s.LoginId,
                    LoginName = s.Login != null ? s.Login.UserName : "",
                    BuyerCompanyId = s.BuyerCompanyId,
                    CompanyName = s.BuyerCompany != null ? s.BuyerCompany.Description : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                purchaseOrderModel.Add(purchaseOrderModels);
            });
            return purchaseOrderModel.OrderByDescending(a => a.InterCompanyPurchaseOrderId).ToList();
        }
        [HttpPost()]
        [Route("GetDataInterCompanyPurchaseOrder")]
        public ActionResult<InterCompanyPurchaseOrderModel> GetData(SearchModel searchModel)
        {
            var purchaseOrder = new InterCompanyPurchaseOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderBy(o => o.InterCompanyPurchaseOrderId).FirstOrDefault(s => s.InterCompanyPurchaseOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        purchaseOrder = _context.InterCompanyPurchaseOrder.OrderByDescending(o => o.InterCompanyPurchaseOrderId).FirstOrDefault(s => s.InterCompanyPurchaseOrderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<InterCompanyPurchaseOrderModel>(purchaseOrder);
            if (result != null)
            {
                var app = _context.ApplicationUser.Where(w => w.UserId == result.LoginId).Select(s => s.UserName).FirstOrDefault();

                result.LoginName = app;


            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertInterCompanyPurchaseOrder")]
        public InterCompanyPurchaseOrderModel Post(InterCompanyPurchaseOrderModel value)
        {
            //  var SessionId = Guid.NewGuid();
            var purchaseOrder = new InterCompanyPurchaseOrder
            {

                Date = value.Date,
                LoginId = value.LoginId,
                BuyerCompanyId = value.BuyerCompanyId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.InterCompanyPurchaseOrder.Add(purchaseOrder);
            _context.SaveChanges();
            value.InterCompanyPurchaseOrderId = purchaseOrder.InterCompanyPurchaseOrderId;
            //value.SessionId=purchaseOrder.SessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateInterCompanyPurchaseOrder")]
        public InterCompanyPurchaseOrderModel Put(InterCompanyPurchaseOrderModel value)
        {
            var SessionId = Guid.NewGuid();

            var purchaseOrder = _context.InterCompanyPurchaseOrder.SingleOrDefault(p => p.InterCompanyPurchaseOrderId == value.InterCompanyPurchaseOrderId);
            purchaseOrder.Date = value.Date;
            purchaseOrder.LoginId = value.LoginId;
            purchaseOrder.BuyerCompanyId = value.BuyerCompanyId;
            purchaseOrder.ModifiedByUserId = value.ModifiedByUserID;
            purchaseOrder.ModifiedDate = DateTime.Now;
            purchaseOrder.StatusCodeId = value.StatusCodeID.Value;

            //  if(value.SessionId!=null)
            // {
            //      purchaseOrder.SessionId=value.SessionId;


            // }
            // else
            // {
            //     purchaseOrder.SessionId=SessionId;

            // }

            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInterCompanyPurchaseOrder")]
        public void Delete(int id)
        {
            var purchaseOrder = _context.InterCompanyPurchaseOrder.SingleOrDefault(p => p.InterCompanyPurchaseOrderId == id);
            if (purchaseOrder != null)
            {
                var purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.Where(t => t.InterCompanyPurchaseOrderId == purchaseOrder.InterCompanyPurchaseOrderId).AsNoTracking().ToList();
                if (purchaseOrderLine != null)
                {
                    _context.InterCompanyPurchaseOrderLine.RemoveRange(purchaseOrderLine);
                    _context.SaveChanges();
                }

                //    var purchaseOrder1 = _context.PurchaseOrder.Where(p => p.PurchaseOrderId == id).Select(s=>s.SessionId).SingleOrDefault();

                //     var purchaseOrderDocument = _context.PurchaseOrderDocument.Where(p => p.SessionId == purchaseOrder1).AsNoTracking().ToList();
                //     if (purchaseOrderDocument != null)
                //     {
                //         _context.PurchaseOrderDocument.RemoveRange(purchaseOrderDocument);
                //         _context.SaveChanges();
                //     }
                _context.InterCompanyPurchaseOrder.Remove(purchaseOrder);
                _context.SaveChanges();
            }
        }



        // [HttpPost]
        // [Route("UploadPurchaseOrderDocuments")]
        // public IActionResult UploadPurchaseOrderDocuments(IFormCollection files, Guid SessionId)
        // {
        //     files.Files.ToList().ForEach(f =>
        //     {
        //         var file = f;
        //         var fs = file.OpenReadStream();
        //         var br = new BinaryReader(fs);
        //         Byte[] document = br.ReadBytes((Int32)fs.Length);

        //         var documents = new PurchaseOrderDocument
        //         {
        //             FileName = file.FileName,
        //             ContentType = file.ContentType,
        //             FileData = document,
        //             FileSize = fs.Length,
        //             UploadDate = DateTime.Now,
        //             SessionId = SessionId,                  
        //         };
        //         _context.PurchaseOrderDocument.Add(documents);
        //     });
        //     _context.SaveChanges();
        //     return Content(SessionId.ToString());

        // }
        // [HttpGet]
        // [Route("GetPurchaseOrderDocument")]
        // public List<PurchaseOrderDocumentModel> GetPurchaseOrderDocument(int? id)
        // {
        //     var purchaseOrder = _context.PurchaseOrder.Where(w=>w.PurchaseOrderId==id).Select(s=>s.SessionId).FirstOrDefault();
        //     var query = _context.PurchaseOrderDocument.Select(s => new PurchaseOrderDocumentModel
        //     {
        //         SessionId = s.SessionId,
        //         PurchaseOrderDocumentId = s.PurchaseOrderDocumentId,
        //         FileName = s.FileName,
        //         ContentType = s.ContentType,
        //         FileSize = s.FileSize,
        //         IsImage = s.ContentType.ToLower().Contains("image")?true:false,
        //     }).Where(l => l.SessionId ==purchaseOrder ).OrderByDescending(o => o.PurchaseOrderDocumentId).AsNoTracking().ToList();
        //     return query;
        // }
        // [HttpGet]
        // [Route("DownLoadPurchaseOrderDocument")]
        // public IActionResult DownLoadPurchaseOrderDocument(long id)
        // {
        //     var document = _context.PurchaseOrderDocument.SingleOrDefault(t => t.PurchaseOrderDocumentId == id);
        //     if (document != null)
        //     {
        //         Stream stream = new MemoryStream(document.FileData);

        //         if (stream == null)
        //             return NotFound();

        //         return Ok(stream);
        //     }
        //     return NotFound();
        // }
        // [HttpDelete]
        // [Route("DeletePurchaseOrderDocument")]
        // public void DeletePurchaseOrderDocument(int id)
        // {
        //     var purchaseOrderDocument = _context.PurchaseOrderDocument.SingleOrDefault(p => p.PurchaseOrderDocumentId == id);
        //     if (purchaseOrderDocument != null)
        //     {
        //         _context.PurchaseOrderDocument.Remove(purchaseOrderDocument);
        //         _context.SaveChanges();
        //     }
        // }



        [HttpGet]
        [Route("GetInterCompanyPurchaseOrderLine")]
        public List<InterCompanyPurchaseOrderLineModel> Get(int id)
        {

            var purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.Include(c => c.AddedByUser).Include(b => b.ModifiedByUser).Include(a => a.StatusCode).Include(m => m.NavItem).Where(c => c.InterCompanyPurchaseOrderId == id).OrderByDescending(a => a.InterCompanyPurchaseOrderLineId).AsNoTracking().ToList();
            List<InterCompanyPurchaseOrderLineModel> purchaseOrderLineModel = new List<InterCompanyPurchaseOrderLineModel>();
            purchaseOrderLine.ForEach(s =>
            {
                InterCompanyPurchaseOrderLineModel purchaseOrderLineModels = new InterCompanyPurchaseOrderLineModel
                {
                    InterCompanyPurchaseOrderLineId = s.InterCompanyPurchaseOrderLineId,
                    InterCompanyPurchaseOrderId = s.InterCompanyPurchaseOrderId,
                    NavItemId = s.NavItemId,
                    NavNo = s.NavItem != null ? s.NavItem.No : "",
                    Description = s.NavItem != null ? s.NavItem.Description : "",
                    Description2 = s.NavItem != null ? s.NavItem.Description2 : "",
                    InternalRef = s.NavItem != null ? s.NavItem.InternalRef : "",
                    PackUom = s.NavItem != null ? s.NavItem.PackUom : "",
                    Quantity = s.Quantity,
                    ShipmentDate = s.ShipmentDate,
                    PromisedDeliveryDate = s.PromisedDeliveryDate,
                    Location = s.Location,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                purchaseOrderLineModel.Add(purchaseOrderLineModels);
            });
            return purchaseOrderLineModel;
        }
        [HttpPost()]
        [Route("GetDataInterCompanyPurchaseOrderLine")]
        public ActionResult<InterCompanyPurchaseOrderLineModel> GetDataline(SearchModel searchModel)
        {
            var purchaseOrderLine = new InterCompanyPurchaseOrderLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Previous":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).FirstOrDefault();
                        break;
                    case "Last":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).LastOrDefault();
                        break;
                    case "Next":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderBy(o => o.InterCompanyPurchaseOrderLineId).FirstOrDefault(s => s.InterCompanyPurchaseOrderLineId > searchModel.Id);
                        break;
                    case "Previous":
                        purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.OrderByDescending(o => o.InterCompanyPurchaseOrderLineId).FirstOrDefault(s => s.InterCompanyPurchaseOrderLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<InterCompanyPurchaseOrderLineModel>(purchaseOrderLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertInterCompanyPurchaseOrderLine")]
        public InterCompanyPurchaseOrderLineModel Post(InterCompanyPurchaseOrderLineModel value)
        {
            var purchaseOrderLine = new InterCompanyPurchaseOrderLine
            {
                InterCompanyPurchaseOrderId = value.InterCompanyPurchaseOrderId,
                NavItemId = value.NavItemId,
                Quantity = value.Quantity,
                ShipmentDate = value.ShipmentDate,
                PromisedDeliveryDate = value.PromisedDeliveryDate,
                Location = value.Location,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,

            };
            _context.InterCompanyPurchaseOrderLine.Add(purchaseOrderLine);
            _context.SaveChanges();
            value.InterCompanyPurchaseOrderLineId = purchaseOrderLine.InterCompanyPurchaseOrderLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateInterCompanyPurchaseOrderLine")]
        public InterCompanyPurchaseOrderLineModel Put(InterCompanyPurchaseOrderLineModel value)
        {
            var purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.SingleOrDefault(p => p.InterCompanyPurchaseOrderLineId == value.InterCompanyPurchaseOrderLineId);

            purchaseOrderLine.NavItemId = value.NavItemId;
            purchaseOrderLine.Quantity = value.Quantity;
            purchaseOrderLine.ShipmentDate = value.ShipmentDate;
            purchaseOrderLine.PromisedDeliveryDate = value.PromisedDeliveryDate;
            purchaseOrderLine.Location = value.Location;
            purchaseOrderLine.ModifiedByUserId = value.ModifiedByUserID;
            purchaseOrderLine.ModifiedDate = DateTime.Now;
            purchaseOrderLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInterCompanyPurchaseOrderLine")]
        public void DeleteLine(int id)
        {
            var purchaseOrderLine = _context.InterCompanyPurchaseOrderLine.SingleOrDefault(p => p.InterCompanyPurchaseOrderLineId == id);
            if (purchaseOrderLine != null)
            {
                _context.InterCompanyPurchaseOrderLine.Remove(purchaseOrderLine);
                _context.SaveChanges();
            }
        }
    }


}