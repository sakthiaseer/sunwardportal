﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Admin")]
    public class UploadController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly CRT_TMSContext _context;
        public UploadController(IWebHostEnvironment host, CRT_TMSContext context)
        {
            _context = context;
            _hostingEnvironment = host;
        }
        [HttpGet("downloadfromurl")]
        public IActionResult DownloadFromUrl(string url)
        {
            var result = DownloadExtention.GetUrlContent(url);
            var request = HttpWebRequest.Create(url) as HttpWebRequest;
            string contentType = "";
            string filename = "";
            Uri uri = new Uri(url);
            if (uri.IsFile)
            {
                filename = System.IO.Path.GetFileName(uri.LocalPath);
            }
            if (request != null)
            {
                var response = request.GetResponse() as HttpWebResponse;
                if (response != null)
                    contentType = response.ContentType;
            }
            if (result != null)
            {
                return File(result.Result, contentType, filename);
            }
            return Ok("file is not exist");
        }
        [HttpGet]
        [Route("DownLoadDocumentAppend")]
        public IActionResult DownLoadDocumentAppend(long id)
        {
            var doc = _context.Documents.Select(s => new { s.DocumentId, s.FilePath, s.FileName }).FirstOrDefault(f => f.DocumentId == id);
            if (doc != null)
            {
                string serverPath = _hostingEnvironment.ContentRootPath + @"\" + doc.FilePath;
                FileStream stream = System.IO.File.OpenRead(serverPath);
                var br = new BinaryReader(stream);
                Byte[] documents = br.ReadBytes((Int32)stream.Length);
                Stream streams = new MemoryStream(documents);
                if (streams == null)
                    return NotFound();
                return Ok(streams);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        [Route("GetDownLoadFiles")]
        public IActionResult GetDownLoadFiles(long id)
        {
            var doc = _context.Documents.Select(s => new { s.DocumentId, s.FilePath, s.FileName }).FirstOrDefault(f => f.DocumentId == id);
            if (doc != null)
            {
                string serverPath = _hostingEnvironment.ContentRootPath + @"\" + doc.FilePath;
                var contentType = MimeKit.MimeTypes.GetMimeType(serverPath);
                var filename = Path.GetFileName(serverPath);
                return File(System.IO.File.ReadAllBytes(serverPath), contentType, doc.FileName);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        [Route("GetDownLoad")]
        public IActionResult GetDownLoad(string id)
        {
            string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Upload\" + id;
            var contentType = MimeKit.MimeTypes.GetMimeType(serverPath);
            var filename = Path.GetFileName(serverPath);
            return File(System.IO.File.ReadAllBytes(serverPath), contentType, id);
        }
        [HttpGet]
        [Route("GetDocuments")]
        public List<DocumentNameModel> GetDocuments()
        {
            List<DocumentNameModel> documentNameModes = new List<DocumentNameModel>();

            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload\Upload";
            var files = Directory.GetFiles(folderName);
            if (files.Length > 0)
            {
                foreach (var file in files)
                {
                    DocumentNameModel documentNameModel = new DocumentNameModel();
                    documentNameModel.ContentType = MimeKit.MimeTypes.GetMimeType(file);
                    documentNameModel.FileName = Path.GetFileName(file);
                    documentNameModel.Extension = Path.GetExtension(file).Replace(".", "");
                    documentNameModes.Add(documentNameModel);
                }
            }
            return documentNameModes;
        }
        [HttpGet]
        [Route("DownLoadDocument")]
        public IActionResult DownLoadDocument(string id)
        {
            string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Upload\" + id;
            FileStream stream = System.IO.File.OpenRead(serverPath);
            var br = new BinaryReader(stream);
            Byte[] documents = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documents);
            if (streams == null)
                return NotFound();
            return Ok(streams);
        }
        [HttpPost]
        [Route("ViewDocument")]
        public string ViewDocument(string fileName)
        {
            string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Upload\" + fileName;
            FileStream stream = System.IO.File.OpenRead(serverPath);
            var br = new BinaryReader(stream);
            Byte[] documents = br.ReadBytes((Int32)stream.Length);
            var contentType = MimeKit.MimeTypes.GetMimeType(serverPath);
            var length = Path.GetFileName(serverPath).Length;
            var plainTextBytes = "The file is either older version or program not able to read. please download  & view in your local computer.";
            if (contentType == "application/msword" || contentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
                return ParseDOCX(documents, fileName);
            }
            if (contentType.Contains("image"))
            {
                string base64String = Convert.ToBase64String(System.IO.File.ReadAllBytes(serverPath));
                var filebaseString = "data:" + contentType + ";base64," + base64String;
                return filebaseString;
            }
            else if (contentType == "text/html" || contentType == "text/plain")
            {
                var filebaseString = System.Text.Encoding.UTF8.GetString(documents);
                return filebaseString;
            }
            return plainTextBytes;
        }
        public static string ParseDOCX(byte[] byteArray, string fileName)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    using (WordprocessingDocument wDoc =
                                                WordprocessingDocument.Open(memoryStream, true))
                    {
                        int imageCounter = 0;
                        var pageTitle = fileName;
                        var part = wDoc.CoreFilePropertiesPart;
                        if (part != null)
                            pageTitle = (string)part.GetXDocument()
                                                    .Descendants(DC.title)
                                                    .FirstOrDefault() ?? fileName;
                        MainDocumentPart documentPart = wDoc.MainDocumentPart;
                        var headerParts = wDoc.MainDocumentPart.HeaderParts.ToList().FirstOrDefault()?.Header.OuterXml;
                        //var footerParts = wDoc.MainDocumentPart.FooterParts.ToList().FirstOrDefault()?.Footer.InnerText;

                        WmlToHtmlConverterSettings settings = new WmlToHtmlConverterSettings()
                        {
                            // AdditionalCss = "body { margin: 1cm auto; max-width: 20cm; padding: 0; }",
                            PageTitle = pageTitle,
                            CssClassPrefix = "pt-",
                            FabricateCssClasses = true,
                            RestrictToSupportedLanguages = false,
                            RestrictToSupportedNumberingFormats = false,
                            ImageHandler = imageInfo =>
                            {
                                ++imageCounter;
                                string extension = imageInfo.ContentType.Split('/')[1].ToLower();
                                ImageFormat imageFormat = null;
                                if (extension == "png") imageFormat = ImageFormat.Png;
                                else if (extension == "gif") imageFormat = ImageFormat.Gif;
                                else if (extension == "bmp") imageFormat = ImageFormat.Bmp;
                                else if (extension == "jpeg") imageFormat = ImageFormat.Jpeg;
                                else if (extension == "tiff")
                                {
                                    extension = "gif";
                                    imageFormat = ImageFormat.Gif;
                                }
                                else if (extension == "x-wmf")
                                {
                                    extension = "wmf";
                                    imageFormat = ImageFormat.Wmf;
                                }

                                if (imageFormat == null) return null;

                                string base64 = null;
                                try
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        imageInfo.Bitmap.Save(ms, imageFormat);
                                        var ba = ms.ToArray();
                                        base64 = System.Convert.ToBase64String(ba);
                                    }
                                }
                                catch (System.Runtime.InteropServices.ExternalException)
                                { return null; }

                                ImageFormat format = imageInfo.Bitmap.RawFormat;
                                ImageCodecInfo codec = ImageCodecInfo.GetImageDecoders()
                                                            .First(c => c.FormatID == format.Guid);
                                string mimeType = codec.MimeType;

                                string imageSource =
                                        string.Format("data:{0};base64,{1}", mimeType, base64);

                                XElement img = new XElement(Xhtml.img,
                                        new XAttribute(NoNamespace.src, imageSource),
                                        imageInfo.ImgStyleAttribute,
                                        imageInfo.AltText != null ?
                                            new XAttribute(NoNamespace.alt, imageInfo.AltText) : null);
                                return img;
                            }
                        };
                        XElement htmlElement = WmlToHtmlConverter.ConvertToHtml(wDoc, settings);
                        var html = new XDocument(new XDocumentType("html", null, null, null), htmlElement);
                        var htmlString = html.ToString(SaveOptions.DisableFormatting);
                        return headerParts + htmlString;
                    }
                }
            }
            catch (Exception ex)
            {
                return "The file is either older version or program not able to read. please download  & view in your local computer.";
            }
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {
            var sessionId = Guid.NewGuid();
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "Upload";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var filePath = getNextFileName(serverPath + @"\" + f.FileName);
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
            });
            return Content(sessionId.ToString());
        }

        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpGet]
        [Route("GetUpdateExtension")]
        public void GetUpdateExtension(long? top)
        {
            if (top > 0)
            {
                string sqlQuery = string.Empty;
                sqlQuery = "SELECT TOP " + top + " TT.* FROM(select *,right(FileName, charindex('.', reverse(FileName) + '.') - 1) as Filenames,right(FilePath, charindex('.', reverse(FilePath) + '.') - 1) as FilePaths from Documents where  filepath is not null) as TT where Filenames!= FilePaths";
                var documents = _context.Set<Documents>().FromSqlRaw(sqlQuery).AsQueryable();
                if (documents != null && documents.ToList().Count > 0)
                {
                    documents.ToList().ForEach(s =>
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\" + s.FilePath;
                        if (System.IO.File.Exists(serverPath))
                        {
                            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + s.SessionId;
                            if (!System.IO.Directory.Exists(serverPaths))
                            {
                                System.IO.Directory.CreateDirectory(serverPaths);
                            }
                            var ext = s.FileName.Split(".").Last();
                            var newFilePath = serverPaths + @"\" + s.SessionId + "." + ext;
                            var filename = getNextFileName(newFilePath);
                            System.IO.File.WriteAllBytes(filename, System.IO.File.ReadAllBytes(serverPath));
                            var paths = newFilePath.Replace(_hostingEnvironment.ContentRootPath + @"\", "");
                            var query = string.Format("Update Documents Set FilePath = '{1}' Where DocumentId={0}", s.DocumentId, paths);
                            _context.Database.ExecuteSqlRaw(query);
                        }
                    });
                }
            }
        }
        [HttpGet]
        [Route("GetUpdateExtensionByDoc")]
        public void GetUpdateExtensionByDoc(string id)
        {
            if (id != null)
            {
                int mos = 0;
                var intList = id.Split(',').Select(m => { int.TryParse(m, out mos); return mos; }).Where(m => m != 0).ToList();
                if (intList != null && intList.Count > 0)
                {
                    var sqlQuery = "SELECT  TT.* FROM(select *,right(FileName, charindex('.', reverse(FileName) + '.') - 1) as Filenames,right(FilePath, charindex('.', reverse(FilePath) + '.') - 1) as FilePaths from Documents where  filepath is not null AND DocumentId in(" + string.Join(",", intList) + ")) as TT where Filenames!= FilePaths";
                    var documents = _context.Set<Documents>().FromSqlRaw(sqlQuery).AsQueryable();
                    if (documents != null && documents.ToList().Count > 0)
                    {
                        documents.ToList().ForEach(s =>
                        {
                            var serverPath = _hostingEnvironment.ContentRootPath + @"\" + s.FilePath;
                            if (System.IO.File.Exists(serverPath))
                            {
                                var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + s.SessionId;
                                if (!System.IO.Directory.Exists(serverPaths))
                                {
                                    System.IO.Directory.CreateDirectory(serverPaths);
                                }
                                var ext = s.FileName.Split(".").Last();
                                var newFilePath = serverPaths + @"\" + s.SessionId + "." + ext;
                                var filename = getNextFileName(newFilePath);
                                System.IO.File.WriteAllBytes(filename, System.IO.File.ReadAllBytes(serverPath));
                                var paths = newFilePath.Replace(_hostingEnvironment.ContentRootPath + @"\", "");
                                var query = string.Format("Update Documents Set FilePath = '{1}' Where DocumentId={0}", s.DocumentId, paths);
                                _context.Database.ExecuteSqlRaw(query);
                            }
                        });
                    }
                }
            }
        }
    }
}
