﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionMaterialController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionMaterialController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetProductionMaterial")]
        public List<ProductionMaterialModel> Get()
        {
            List<ProductionMaterialModel> productionMaterialModels = new List<ProductionMaterialModel>();
            var typeofIngredientlist = _context.ProductionMaterialIngredientItems.AsNoTracking().ToList();
            var functionOfMateriallist = _context.ProductionFuntionOfMaterialItems.AsNoTracking().ToList();
            var productionMaterial = _context.ProductionMaterial
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("ProcessUseSpec").OrderByDescending(o => o.ProductionMaterialId).AsNoTracking().ToList();
            if(productionMaterial !=null && productionMaterial.Count>0)
            {
                List<long?> masterIds = productionMaterial.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList();
                masterIds.AddRange(productionMaterial.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionMaterial.ForEach(s =>
                {
                    ProductionMaterialModel productionMaterialModel = new ProductionMaterialModel
                    {
                        ProductionMaterialId = s.ProductionMaterialId,
                        RawMaterialName = s.RawMaterialName,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedByDate,
                        ModifiedDate = s.ModifiedByDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        TypeofIngredientId = typeofIngredientlist.Where(w => w.ProductionMaterialId == s.ProductionMaterialId).Select(w => w.IngredientId.Value).ToList(),
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        SpecNos = "Spec " + s.SpecNo,
                        FunctionOfMaterialId = functionOfMateriallist.Where(w => w.ProdutionMaterialId == s.ProductionMaterialId).Select(w => w.FunctionOfMaterialId.Value).ToList(),
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec!=null? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",

                    };
                    productionMaterialModels.Add(productionMaterialModel);
                });

            }
            
            return productionMaterialModels;
        }
        [HttpPost]
        [Route("GetProductionMaterialByRefNo")]
        public List<ProductionMaterialModel> GetProductionMaterialByRefNo(RefSearchModel refSearchModel)
        {
            var typeofIngredientlist = _context.ProductionMaterialIngredientItems.AsNoTracking().ToList();
            var functionOfMateriallist = _context.ProductionFuntionOfMaterialItems.AsNoTracking().ToList();
            List<ProductionMaterialModel> productionMaterialModels = new List<ProductionMaterialModel>();
            List<ProductionMaterial> productionMaterial = new List<ProductionMaterial>();
            if (refSearchModel.IsHeader)
            {
                productionMaterial = _context.ProductionMaterial
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("ProcessUseSpec").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionMaterialId).AsNoTracking().ToList();
            }
            else
            {
                productionMaterial = _context.ProductionMaterial
               .Include("AddedByUser")
               .Include("ModifiedByUser")
               .Include("StatusCode")
               .Include("ProcessUseSpec").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionMaterialId).AsNoTracking().ToList();

            }
            if(productionMaterial!=null && productionMaterial.Count>0)
            {
                List<long?> masterIds = productionMaterial.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList();
                masterIds.AddRange(productionMaterial.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionMaterial.ForEach(s =>
                {
                    ProductionMaterialModel productionMaterialModel = new ProductionMaterialModel
                    {
                        ProductionMaterialId = s.ProductionMaterialId,
                        RawMaterialName = s.RawMaterialName,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedByDate,
                        ModifiedDate = s.ModifiedByDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        TypeofIngredientId = typeofIngredientlist.Where(w => w.ProductionMaterialId == s.ProductionMaterialId).Select(w => w.IngredientId.Value).ToList(),
                        FunctionOfMaterialId = functionOfMateriallist.Where(w => w.ProdutionMaterialId == s.ProductionMaterialId).Select(w => w.FunctionOfMaterialId.Value).ToList(),
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        SpecNoID = s.ProductionMaterialId,
                        SpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                        SpecNos = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()) + " " + "|" + " " + s.MaterialName,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec!=null?"Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",


                    };
                    productionMaterialModels.Add(productionMaterialModel);
                });
            }
               
            return productionMaterialModels;
        }
        [HttpGet]
        [Route("GetProductionMaterialGenericList")]
        public List<ProductionMaterialModel> GetProductionMaterialGenericList(int id)
        {
            var typeofIngredientlist = _context.ProductionMaterialIngredientItems.AsNoTracking().ToList();
            var functionOfMateriallist = _context.ProductionFuntionOfMaterialItems.AsNoTracking().ToList();
            List<ProductionMaterialModel> productionMaterialModels = new List<ProductionMaterialModel>();
            var productionMaterial = _context.ProductionMaterial
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode").Where(w => w.GenericItemNameListingId == id).OrderByDescending(o => o.ProductionMaterialId).AsNoTracking().ToList();
            if(productionMaterial!=null && productionMaterial.Count>0)
            {
                productionMaterial.ForEach(s =>
                {
                    ProductionMaterialModel productionMaterialModel = new ProductionMaterialModel
                    {
                        ProductionMaterialId = s.ProductionMaterialId,
                        RawMaterialName = s.RawMaterialName,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedByDate,
                        ModifiedDate = s.ModifiedByDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        GenericItemNameListingId = s.GenericItemNameListingId,
                        TypeofIngredientId = typeofIngredientlist.Where(w => w.ProductionMaterialId == s.ProductionMaterialId).Select(w => w.IngredientId.Value).ToList(),
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        FunctionOfMaterialId = functionOfMateriallist.Where(w => w.ProdutionMaterialId == s.ProductionMaterialId).Select(w => w.FunctionOfMaterialId.Value).ToList(),
                    };
                    productionMaterialModels.Add(productionMaterialModel);
                });
            }           
            return productionMaterialModels;
        }
        [HttpGet]
        [Route("GetProductionMaterialLine")]
        public List<ProductionMaterialLineModel> ProductionMaterialLine(int id)
        {
            var MaterialClassificationList = _context.ProductionMaterialLineClassification.AsNoTracking().ToList();
            List<ProductionMaterialLineModel> productionMaterialLineModels = new List<ProductionMaterialLineModel>();
            var productionMaterialLine = _context.ProductionMaterialLine
                .Include("SaltOrBase")
                .Include("DensityNavigation")
                .Include("MicronisedOrMesh").Where(w => w.ProductionMaterialId == id).OrderByDescending(o => o.ProductionMaterialLineId).AsNoTracking().ToList();
           if(productionMaterialLine!=null && productionMaterialLine.Count>0)
            {
                List<long?> masterIds = productionMaterialLine.Where(w => w.HydrationId != null).Select(a => a.HydrationId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionMaterialLine.ForEach(s =>
                {
                    ProductionMaterialLineModel productionMaterialLineModel = new ProductionMaterialLineModel
                    {
                        ProductionMaterialLineId = s.ProductionMaterialLineId,
                        ProductionMaterialId = s.ProductionMaterialId,
                        SaltOrBaseId = s.SaltOrBaseId,
                        SaltOrBaseName = s.SaltOrBase!=null? s.SaltOrBase.CodeValue : null,
                        XofGeneralItemName = s.XofGeneralItemName,
                        IsWaterMalecule = s.IsWaterMalecule,
                        IsWaterMaleculeFlag = s.IsWaterMalecule == true ? "1" : "0",
                        HydrationId = s.HydrationId,
                        HydrationName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.HydrationId).Select(a => a.Value).SingleOrDefault() : "",
                        MicronisedOrMeshId = s.MicronisedOrMeshId,
                        MicronisedOrMeshName = s.MicronisedOrMesh!=null? s.MicronisedOrMesh.CodeValue : null,
                        ParticleSize = s.ParticleSize,
                        MeshNo = s.MeshNo,
                        IsDensity = s.IsDensity,
                        IsDensityFlag = s.IsDensity == true ? "1" : "0",
                        Density = s.Density,
                        DensityName = s.DensityNavigation!=null? s.DensityNavigation.CodeValue : null,
                        DyeContentFrom = s.DyeContentFrom,
                        DyeContentTo = s.DyeContentTo,
                        DatabaseRequireId = s.DatabaseRequireId,
                        SuggestNameBySystem = s.SuggestNameBySystem,
                        NavisionSinid = s.NavisionSinid,
                        ItemCategorySinid = s.ItemCategorySinid,
                        NavisionMalId = s.NavisionMalId,
                        ItemCategorMalId = s.ItemCategorMalId,
                        IsChangeNavisonToProposeFlag = s.IsChangeNavisonToPropose == true ? "1" : "0",
                        IsChangeNavisonToPropose = s.IsChangeNavisonToPropose,
                        MaterialClassificationSIGId = MaterialClassificationList.Where(f => f.ProductionMaterialLineId == s.ProductionMaterialLineId && f.NavisionType == "SIG").Select(f => f.DrugClassificationId.Value).ToList(),
                        MaterialClassificationMALId = MaterialClassificationList.Where(f => f.ProductionMaterialLineId == s.ProductionMaterialLineId && f.NavisionType == "MAL").Select(f => f.DrugClassificationId.Value).ToList(),
                       
                    };
                    productionMaterialLineModels.Add(productionMaterialLineModel);
                });
            }            
          
            return productionMaterialLineModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionMaterialModel> GetData(SearchModel searchModel)
        {
            var ProductionMaterial = new ProductionMaterial();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).FirstOrDefault();
                        break;
                    case "Last":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).LastOrDefault();
                        break;
                    case "Next":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).LastOrDefault();
                        break;
                    case "Previous":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).FirstOrDefault();
                        break;
                    case "Last":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).LastOrDefault();
                        break;
                    case "Next":
                        ProductionMaterial = _context.ProductionMaterial.OrderBy(o => o.ProductionMaterialId).FirstOrDefault(s => s.ProductionMaterialId > searchModel.Id);
                        break;
                    case "Previous":
                        ProductionMaterial = _context.ProductionMaterial.OrderByDescending(o => o.ProductionMaterialId).FirstOrDefault(s => s.ProductionMaterialId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<ProductionMaterialModel>(ProductionMaterial);
            if (result != null)
            {
                result.TypeofIngredientId = _context.ProductionMaterialIngredientItems.Where(p => p.ProductionMaterialId == result.ProductionMaterialId).Select(p => p.IngredientId.Value).ToList();
                result.FunctionOfMaterialId = _context.ProductionFuntionOfMaterialItems.Where(p => p.ProdutionMaterialId == result.ProductionMaterialId).Select(p => p.FunctionOfMaterialId.Value).ToList();
            }
            return result;
        }
        [HttpPost]
        [Route("InsertProductionMaterial")]
        public ProductionMaterialModel Post(ProductionMaterialModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.RawMaterialName });
            var productionMaterialCount = _context.ProductionMaterial.Where(p => p.LinkProfileReferenceNo == value.LinkProfileReferenceNo).Count();
            var ProductionMaterial = new ProductionMaterial
            {
                RawMaterialName = value.RawMaterialName,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByDate = DateTime.Now,
                GenericItemNameListingId = value.GenericItemNameListingId,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                IsSameAsMaster=value.IsSameAsMaster,
                MaterialName=value.MaterialName,
                SpecNo = productionMaterialCount + 1,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                PurposeId = value.PurposeId,
                StudyLink = value.StudyLink,
                ProcessUseSpecId = value.ProcessUseSpecId,
                RndpurposeId = value.RndpurposeId,
            };
            if (value.FunctionOfMaterialId != null)
            {
                value.FunctionOfMaterialId.ForEach(c =>
                {
                    var FunctionOfMaterialId = new ProductionFuntionOfMaterialItems
                    {
                        FunctionOfMaterialId = c,
                    };
                    ProductionMaterial.ProductionFuntionOfMaterialItems.Add(FunctionOfMaterialId);
                });
            }
            if (value.TypeofIngredientId != null)
            {
                value.TypeofIngredientId.ForEach(c =>
                {
                    var ProductionMaterialIngredient = new ProductionMaterialIngredientItems
                    {
                        IngredientId = c,
                    };
                    ProductionMaterial.ProductionMaterialIngredientItems.Add(ProductionMaterialIngredient);
                });
            }
            _context.ProductionMaterial.Add(ProductionMaterial);
            _context.SaveChanges();
            value.ProductionMaterialId = ProductionMaterial.ProductionMaterialId;
            value.MasterProfileReferenceNo = ProductionMaterial.MasterProfileReferenceNo;
            value.SpecificationNo = "Spec" + " " + ((ProductionMaterial.SpecNo > 10) ? ProductionMaterial.SpecNo.ToString() : "0" + ProductionMaterial.SpecNo.ToString());

            return value;
        }
        [HttpPost]
        [Route("InsertProductionMaterialLine")]
        public ProductionMaterialLineModel InsertProductionMaterialLine(ProductionMaterialLineModel value)
        {
            var ProductionMaterialLine = new ProductionMaterialLine
            {
                ProductionMaterialId = value.ProductionMaterialId,
                SaltOrBaseId = value.SaltOrBaseId,
                XofGeneralItemName = value.XofGeneralItemName,
                IsWaterMalecule = value.IsWaterMaleculeFlag == "1" ? true : false,
                HydrationId = value.HydrationId,
                MicronisedOrMeshId = value.MicronisedOrMeshId,
                ParticleSize = value.ParticleSize,
                MeshNo = value.MeshNo,
                IsDensity = value.IsDensityFlag == "1" ? true : false,
                Density = value.Density,
                DyeContentFrom = value.DyeContentFrom,
                DyeContentTo = value.DyeContentTo,
                DatabaseRequireId = value.DatabaseRequireId,
                SuggestNameBySystem = value.SuggestNameBySystem,
                NavisionSinid = value.NavisionSinid,
                ItemCategorySinid = value.ItemCategorySinid,
                NavisionMalId = value.NavisionMalId,
                ItemCategorMalId = value.ItemCategorMalId,
                IsChangeNavisonToPropose = value.IsChangeNavisonToProposeFlag == "1" ? true : false,
            };
            if (value.MaterialClassificationSIGId != null)
            {
                value.MaterialClassificationSIGId.ForEach(c =>
                {
                    var MaterialClassificationSIG = new ProductionMaterialLineClassification
                    {
                        DrugClassificationId = c,
                        NavisionType = "SIG",
                    };
                    ProductionMaterialLine.ProductionMaterialLineClassification.Add(MaterialClassificationSIG);
                });
            }
            if (value.MaterialClassificationMALId != null)
            {
                value.MaterialClassificationMALId.ForEach(c =>
                {
                    var MaterialClassificationMAL = new ProductionMaterialLineClassification
                    {
                        DrugClassificationId = c,
                        NavisionType = "MAL",
                    };
                    ProductionMaterialLine.ProductionMaterialLineClassification.Add(MaterialClassificationMAL);
                });
            }
            _context.ProductionMaterialLine.Add(ProductionMaterialLine);
            _context.SaveChanges();
            value.ProductionMaterialLineId = ProductionMaterialLine.ProductionMaterialLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionMaterial")]
        public ProductionMaterialModel Put(ProductionMaterialModel value)
        {
            var ProductionMaterial = _context.ProductionMaterial.SingleOrDefault(p => p.ProductionMaterialId == value.ProductionMaterialId);
            ProductionMaterial.RawMaterialName = value.RawMaterialName;
            ProductionMaterial.ModifiedByUserId = value.ModifiedByUserID;
            ProductionMaterial.ModifiedByDate = DateTime.Now;
            ProductionMaterial.StatusCodeId = value.StatusCodeID.Value;
            ProductionMaterial.GenericItemNameListingId = value.GenericItemNameListingId;
            ProductionMaterial.IsSameAsMaster = value.IsSameAsMaster;
            ProductionMaterial.MaterialName = value.MaterialName;
            ProductionMaterial.PurposeId = value.PurposeId;
            ProductionMaterial.StudyLink = value.StudyLink;
            ProductionMaterial.ProcessUseSpecId = value.ProcessUseSpecId;
            ProductionMaterial.RndpurposeId = value.RndpurposeId;
            var ProductionFuntionOfMaterial = _context.ProductionFuntionOfMaterialItems.Where(l => l.ProdutionMaterialId == value.ProductionMaterialId).ToList();
            if (ProductionFuntionOfMaterial.Count > 0)
            {
                _context.ProductionFuntionOfMaterialItems.RemoveRange(ProductionFuntionOfMaterial);
            }
            if (value.FunctionOfMaterialId != null)
            {
                value.FunctionOfMaterialId.ForEach(c =>
                {
                    var FunctionOfMaterialId = new ProductionFuntionOfMaterialItems
                    {
                        FunctionOfMaterialId = c,
                    };
                    ProductionMaterial.ProductionFuntionOfMaterialItems.Add(FunctionOfMaterialId);
                });
            }
            var ProductionMaterialIngredients = _context.ProductionMaterialIngredientItems.Where(l => l.ProductionMaterialId == value.ProductionMaterialId).ToList();
            if (ProductionMaterialIngredients.Count > 0)
            {
                _context.ProductionMaterialIngredientItems.RemoveRange(ProductionMaterialIngredients);
            }
            if (value.TypeofIngredientId != null)
            {
                value.TypeofIngredientId.ForEach(c =>
                {
                    var ProductionMaterialIngredient = new ProductionMaterialIngredientItems
                    {
                        IngredientId = c,
                    };
                    ProductionMaterial.ProductionMaterialIngredientItems.Add(ProductionMaterialIngredient);
                });
            }
            _context.SaveChanges();
            value.SpecificationNo = "Spec" + " " + ((ProductionMaterial.SpecNo > 10) ? ProductionMaterial.SpecNo.ToString() : "0" + ProductionMaterial.SpecNo.ToString());

            return value;
        }
        [HttpPut]
        [Route("UpdateProductionMaterialLine")]
        public ProductionMaterialLineModel UpdateProductionMaterialLine(ProductionMaterialLineModel value)
        {
            var ProductionMaterialLine = _context.ProductionMaterialLine.SingleOrDefault(p => p.ProductionMaterialLineId == value.ProductionMaterialLineId);
            ProductionMaterialLine.ProductionMaterialId = value.ProductionMaterialId;
            ProductionMaterialLine.SaltOrBaseId = value.SaltOrBaseId;
            ProductionMaterialLine.XofGeneralItemName = value.XofGeneralItemName;
            ProductionMaterialLine.IsWaterMalecule = value.IsWaterMalecule;
            ProductionMaterialLine.HydrationId = value.HydrationId;
            ProductionMaterialLine.MicronisedOrMeshId = value.MicronisedOrMeshId;
            ProductionMaterialLine.ParticleSize = value.ParticleSize;
            ProductionMaterialLine.MeshNo = value.MeshNo;
            ProductionMaterialLine.IsDensity = value.IsDensity;
            ProductionMaterialLine.Density = value.Density;
            ProductionMaterialLine.DyeContentFrom = value.DyeContentFrom;
            ProductionMaterialLine.DyeContentTo = value.DyeContentTo;
            ProductionMaterialLine.DatabaseRequireId = value.DatabaseRequireId;
            ProductionMaterialLine.SuggestNameBySystem = value.SuggestNameBySystem;
            ProductionMaterialLine.NavisionSinid = value.NavisionSinid;
            ProductionMaterialLine.ItemCategorySinid = value.ItemCategorySinid;
            ProductionMaterialLine.NavisionMalId = value.NavisionMalId;
            ProductionMaterialLine.ItemCategorMalId = value.ItemCategorMalId;
            ProductionMaterialLine.IsChangeNavisonToPropose = value.IsChangeNavisonToProposeFlag == "1" ? true : false;
            var MaterialClassificationItems = _context.ProductionMaterialLineClassification.Where(l => l.ProductionMaterialLineId == value.ProductionMaterialLineId).ToList();
            if (MaterialClassificationItems.Count > 0)
            {
                _context.ProductionMaterialLineClassification.RemoveRange(MaterialClassificationItems);
            }
            if (value.MaterialClassificationSIGId != null)
            {
                value.MaterialClassificationSIGId.ForEach(c =>
                {
                    var MaterialClassificationSIG = new ProductionMaterialLineClassification
                    {
                        DrugClassificationId = c,
                        NavisionType = "SIG",
                    };
                    ProductionMaterialLine.ProductionMaterialLineClassification.Add(MaterialClassificationSIG);
                });
            }
            if (value.MaterialClassificationMALId != null)
            {
                value.MaterialClassificationMALId.ForEach(c =>
                {
                    var MaterialClassificationMAL = new ProductionMaterialLineClassification
                    {
                        DrugClassificationId = c,
                        NavisionType = "MAL",
                    };
                    ProductionMaterialLine.ProductionMaterialLineClassification.Add(MaterialClassificationMAL);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteProductionMaterial")]
        public void Delete(int id)
        {
            try
            {
                var ProductionMaterial = _context.ProductionMaterial.SingleOrDefault(p => p.ProductionMaterialId == id);
                if (ProductionMaterial != null)
                {
                    var ProductionFuntionOfMaterialItems = _context.ProductionFuntionOfMaterialItems.Where(s => s.ProdutionMaterialId == id).AsNoTracking().ToList();
                    if (ProductionFuntionOfMaterialItems != null)
                    {
                        _context.ProductionFuntionOfMaterialItems.RemoveRange(ProductionFuntionOfMaterialItems);
                        _context.SaveChanges();
                    }
                    var ProductionMaterialIngredientItems = _context.ProductionMaterialIngredientItems.Where(s => s.ProductionMaterialId == id).AsNoTracking().ToList();
                    if (ProductionMaterialIngredientItems != null)
                    {
                        _context.ProductionMaterialIngredientItems.RemoveRange(ProductionMaterialIngredientItems);
                        _context.SaveChanges();
                    }
                    var ProductionMaterialLine = _context.ProductionMaterialLine.Where(s => s.ProductionMaterialId == id).AsNoTracking().ToList();
                    if (ProductionMaterialLine != null)
                    {
                        _context.ProductionMaterialLine.RemoveRange(ProductionMaterialLine);
                        _context.SaveChanges();
                    }
                    _context.ProductionMaterial.Remove(ProductionMaterial);
                    _context.SaveChanges();
                   /* var ProductionMaterialList = _context.ProductionMaterial.Where(p => p.LinkProfileReferenceNo == ProductionMaterial.LinkProfileReferenceNo).OrderBy(a=>a.ProductionMaterialId).AsNoTracking().ToList();
                    int inc = 1;
                    if(ProductionMaterialList!=null)
                    {
                        ProductionMaterialList.ForEach(h =>
                        {
                            var ProductionMaterialListItems = _context.ProductionMaterial.SingleOrDefault(p => p.ProductionMaterialId == h.ProductionMaterialId);
                            ProductionMaterialListItems.SpecNo = inc;
                            _context.SaveChanges();
                            inc++;
                        });
                    }*/
                }
                //return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new AppException("You’re not allowed to delete this record", ex);
            }
        }
        [HttpDelete]
        [Route("DeleteProductionMaterialLine")]
        public ActionResult<string> DeleteProductionMaterialLine(int id)
        {
            try
            {
                var ProductionMaterialLine = _context.ProductionMaterialLine.Where(p => p.ProductionMaterialLineId == id).FirstOrDefault();
                if (ProductionMaterialLine != null)
                {
                    _context.ProductionMaterialLine.Remove(ProductionMaterialLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}