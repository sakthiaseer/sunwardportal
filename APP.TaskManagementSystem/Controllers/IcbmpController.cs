﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IcbmpController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public IcbmpController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetIcbmp")]
        public List<IcbmpModel> Get()
        {
            var icBmpManufacturingSiteList = _context.IcBmpManufacturingSite.AsNoTracking().ToList();
            List<IcbmpModel> icbmpModels = new List<IcbmpModel>();
            var icbmp = _context.Icbmp
                 .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode").OrderByDescending(o => o.IcBmpId).AsNoTracking().ToList();
            if(icbmp!=null && icbmp.Count>0)
            {
                icbmp.ForEach(s =>
                {
                    IcbmpModel icbmpModel = new IcbmpModel
                    {
                        IcBmpId = s.IcBmpId,
                        BmpNo = s.BmpNo,
                        WiLink = s.WiLink,
                        ManufacturingSiteIds = icBmpManufacturingSiteList.Where(l => l.IcBmpId == s.IcBmpId).Select(l => l.ManufacturingSiteId.Value).ToList(),
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",

                    };
                    icbmpModels.Add(icbmpModel);

                });
            }
           
            return icbmpModels;
        }
        [HttpPost]
        [Route("InsertIcbmp")]
        public IcbmpModel Post(IcbmpModel value)
        {
            var icbmp = new Icbmp
            {
                BmpNo = value.BmpNo,
                WiLink = value.WiLink,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            if (value.ManufacturingSiteIds != null)
            {
                value.ManufacturingSiteIds.ForEach(c =>
                {
                    var setInfoId = new IcBmpManufacturingSite
                    {
                        ManufacturingSiteId = c,
                    };
                    icbmp.IcBmpManufacturingSite.Add(setInfoId);
                });
            }
            _context.Icbmp.Add(icbmp);
            _context.SaveChanges();
            value.IcBmpId = icbmp.IcBmpId;
             return value;
        }
        [HttpPut]
        [Route("UpdateIcbmp")]
        public IcbmpModel Put(IcbmpModel value)
        {
            var icbmp = _context.Icbmp.SingleOrDefault(p => p.IcBmpId == value.IcBmpId);
            icbmp.BmpNo = value.BmpNo;
            icbmp.WiLink = value.WiLink;
            icbmp.StatusCodeId = value.StatusCodeID.Value;
            icbmp.ModifiedByUserId = value.ModifiedByUserID;
            icbmp.ModifiedDate = DateTime.Now;
            var setInfoIds = _context.IcBmpManufacturingSite.Where(l => l.IcBmpId == value.IcBmpId).ToList();
            if (setInfoIds.Count > 0)
            {
                _context.IcBmpManufacturingSite.RemoveRange(setInfoIds);
            }
            if (value.ManufacturingSiteIds != null)
            {
                value.ManufacturingSiteIds.ForEach(c =>
                {
                    var setInfoId = new IcBmpManufacturingSite
                    {
                        ManufacturingSiteId = c,
                    };
                    icbmp.IcBmpManufacturingSite.Add(setInfoId);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteIcbmp")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var icbmp = _context.Icbmp.Where(p => p.IcBmpId == id).FirstOrDefault();
                if (icbmp != null)
                {
                    var manufacturingSiteIds = _context.IcBmpManufacturingSite.Where(s => s.IcBmpId == icbmp.IcBmpId).ToList();
                    if (manufacturingSiteIds != null)
                    {
                        _context.IcBmpManufacturingSite.RemoveRange(manufacturingSiteIds);
                        _context.SaveChanges();
                    }
                    _context.Icbmp.Remove(icbmp);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}