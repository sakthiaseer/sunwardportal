﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavCustomerController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavCustomerController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavCustomers")]
        public List<NavCustomerModel> Get()
        {
            var navCustomer = _context.Navcustomer.Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavCustomerModel
            {
                CustomerId = s.CustomerId,                
                Code = s.Code,
                Name = s.Name,
                ResponsibilityCenter = s.ResponsibilityCenter,
                LocationCode = s.LocationCode,
                Address = s.Address,
                Address2 = s.Address2,
                City = s.City,
                PostCode = s.PostCode,
                County = s.County,
                CountryRegionCode = s.CountryRegionCode,
                PhoneNo = s.PhoneNo,
                Contact = s.Contact,
                SalespersonCode = s.SalespersonCode,
                CustomerPostingGroup = s.CustomerPostingGroup,
                GenBusPostingGroup = s.GenBusPostingGroup,
                VatbusPostingGroup = s.VatbusPostingGroup,
                PaymentTermsCode = s.PaymentTermsCode,
                CurrencyCode = s.CurrencyCode,
                LanguageCode = s.LanguageCode,
                ShippingAdvice = s.ShippingAdvice,
                ShippingAgentCode = s.ShippingAgentCode,
                BalanceLcy = s.BalanceLcy,
                BalanceDueLcy = s.BalanceDueLcy,
                SalesLcy = s.SalesLcy,
                Company = s.Company,
                LastSyncDate = s.LastSyncDate,
                LastSyncBy = s.LastSyncBy,                
                StatusCodeId = s.StatusCodeId,
                CustomerName=s.Code+"|"+s.Name,
            }).OrderByDescending(o => o.CustomerId).AsNoTracking().ToList();
            return navCustomer;
        }
        [HttpGet]
        [Route("GetNavCustomersDropDown")]
        public List<NavCustomerModel> GetNavCustomersDropDown()
        {
            var navCustomer = _context.Navcustomer.Select(s => new NavCustomerModel
            {
                CustomerId = s.CustomerId,
                Code = s.Code,
                Name = s.Name,
            }).OrderByDescending(o => o.CustomerId).AsNoTracking().ToList();
            return navCustomer;
        }
        [HttpGet]
        [Route("GetNavCustomersByCountryID")]
        public List<NavCustomerModel> GetNavCustomersByCountryID(int? id)
        {
            var navCustomer = _context.Navcustomer.Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavCustomerModel
            {
                CustomerId = s.CustomerId,
                Code = s.Code,
                Name = s.Name,
                ResponsibilityCenter = s.ResponsibilityCenter,
                LocationCode = s.LocationCode,
                Address = s.Address,
                Address2 = s.Address2,
                City = s.City,
                PostCode = s.PostCode,
                County = s.County,
                CountryRegionCode = s.CountryRegionCode,
                PhoneNo = s.PhoneNo,
                Contact = s.Contact,
                SalespersonCode = s.SalespersonCode,
                CustomerPostingGroup = s.CustomerPostingGroup,
                GenBusPostingGroup = s.GenBusPostingGroup,
                VatbusPostingGroup = s.VatbusPostingGroup,
                PaymentTermsCode = s.PaymentTermsCode,
                CurrencyCode = s.CurrencyCode,
                LanguageCode = s.LanguageCode,
                ShippingAdvice = s.ShippingAdvice,
                ShippingAgentCode = s.ShippingAgentCode,
                BalanceLcy = s.BalanceLcy,
                BalanceDueLcy = s.BalanceDueLcy,
                SalesLcy = s.SalesLcy,
                Company = s.Company,
                LastSyncDate = s.LastSyncDate,
                LastSyncBy = s.LastSyncBy,
                StatusCodeId = s.StatusCodeId,
                CustomerName = s.Code + "|" + s.Name,
                CompanyId=s.CompanyId,
            }).Where(w=>w.CompanyId==id).OrderByDescending(o => o.CustomerId).AsNoTracking().ToList();
            return navCustomer;
        }
        [HttpPost]
        [Route("GetNavCustomersByCountry")]
        public List<NavCustomerModel> GetNavCustomersByCountry(StockBalanceSearch SearchModel)
        {
            var navCustomer = _context.Navcustomer.Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavCustomerModel
            {
                CustomerId = s.CustomerId,
                Code = s.Code,
                Name = s.Name,
                ResponsibilityCenter = s.ResponsibilityCenter,
                LocationCode = s.LocationCode,
                Address = s.Address,
                Address2 = s.Address2,
                City = s.City,
                PostCode = s.PostCode,
                County = s.County,
                CountryRegionCode = s.CountryRegionCode,
                PhoneNo = s.PhoneNo,
                Contact = s.Contact,
                SalespersonCode = s.SalespersonCode,
                CustomerPostingGroup = s.CustomerPostingGroup,
                GenBusPostingGroup = s.GenBusPostingGroup,
                VatbusPostingGroup = s.VatbusPostingGroup,
                PaymentTermsCode = s.PaymentTermsCode,
                CurrencyCode = s.CurrencyCode,
                LanguageCode = s.LanguageCode,
                ShippingAdvice = s.ShippingAdvice,
                ShippingAgentCode = s.ShippingAgentCode,
                BalanceLcy = s.BalanceLcy,
                BalanceDueLcy = s.BalanceDueLcy,
                SalesLcy = s.SalesLcy,
                Company = s.Company,
                LastSyncDate = s.LastSyncDate,
                LastSyncBy = s.LastSyncBy,
                StatusCodeId = s.StatusCodeId,
                CustomerName = s.Code + "|" + s.Name,
                CompanyId = s.CompanyId,
            }).Where(w => w.CompanyId == SearchModel.CompanyId).OrderByDescending(o => o.CustomerId).AsNoTracking().ToList();
            return navCustomer;
        }
        [HttpGet]
        [Route("GetCustomers")]
        public List<NavCustomerModel> GetCustomer(int? id =1)
        {
            var navCustomer = _context.Navcustomer.Select(s => new NavCustomerModel
            {
                CustomerId = s.CustomerId,
                Code = s.Code,
                Name = s.Name,               
                StatusCodeId = s.StatusCodeId,
                Company =s.Company,
                CompanyId =s.CompanyId
            }).OrderByDescending(o => o.CustomerId).Where(f=>f.CompanyId == id).AsNoTracking().ToList();
            return navCustomer;
        }
    }
}