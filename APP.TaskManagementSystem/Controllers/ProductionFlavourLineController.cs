﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionFlavourLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionFlavourLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionFlavourLinesByID")]
        public List<ProductionFlavourLineModel> GetProductionFlavourLinesByID(long id)
        {
            List<ProductionFlavourLineModel> productionFlavourLineModels = new List<ProductionFlavourLineModel>();
            var productionFlavourLine = _context.ProductionFlavourLine.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").Where(l => l.ProductionFlavourId == id).OrderByDescending(o => o.ProductionFlavourLineId).AsNoTracking().ToList();
            if(productionFlavourLine !=null && productionFlavourLine.Count>0)
            {
                List<long?> masterIds = productionFlavourLine.Where(w => w.DataBaseRequireId != null).Select(a => a.DataBaseRequireId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                List<long?> navisionIds = productionFlavourLine.Where(w => w.NavisionId != null).Select(a => a.NavisionId).Distinct().ToList();
                var items = _context.Navitems.Where(w => navisionIds.Contains(w.ItemId)).AsNoTracking().ToList();
                productionFlavourLine.ForEach(s =>
                {
                    ProductionFlavourLineModel productionFlavourLineModel = new ProductionFlavourLineModel
                    {
                        ProductionFlavourLineID = s.ProductionFlavourLineId,
                        ProductionFlavourID = s.ProductionFlavourId,
                        DatabseRequireID = s.DataBaseRequireId,
                        NavisionID = s.NavisionId,
                        BUOM = s.Buom,
                        AddedByUserID = s.AddedByUserId,                       
                        DatabseRequire = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.DataBaseRequireId).Select(a => a.Value).SingleOrDefault() : "",
                        Navision = items.Where(t => t.ItemId == s.NavisionId).Select(t => t.No).FirstOrDefault(),
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionFlavourLineModels.Add(productionFlavourLineModel);
                });
            }           
            return productionFlavourLineModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionFlavourLineModel> GetData(SearchModel searchModel)
        {
            var productionFlavourLine = new ProductionFlavourLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).LastOrDefault();
                        break;
                    case "Previous":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderBy(o => o.ProductionFlavourLineId).FirstOrDefault(s => s.ProductionFlavourLineId > searchModel.Id);
                        break;
                    case "Previous":
                        productionFlavourLine = _context.ProductionFlavourLine.OrderByDescending(o => o.ProductionFlavourLineId).FirstOrDefault(s => s.ProductionFlavourLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionFlavourLineModel>(productionFlavourLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionFlavourLine")]
        public ProductionFlavourLineModel Post(ProductionFlavourLineModel value)
        {
            var productionFlavourLine = new ProductionFlavourLine
            {
                ProductionFlavourId = value.ProductionFlavourID,
                DataBaseRequireId = value.DatabseRequireID,
                NavisionId = value.NavisionID,
                Buom = value.BUOM,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.ProductionFlavourLine.Add(productionFlavourLine);
            _context.SaveChanges();
            value.ProductionFlavourLineID = productionFlavourLine.ProductionFlavourLineId;
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionFlavourLine")]
        public ProductionFlavourLineModel Put(ProductionFlavourLineModel value)
        {
            var productionFlavourLine = _context.ProductionFlavourLine.SingleOrDefault(p => p.ProductionFlavourLineId == value.ProductionFlavourLineID);
            productionFlavourLine.ProductionFlavourId = value.ProductionFlavourID;
            productionFlavourLine.DataBaseRequireId = value.DatabseRequireID;
            productionFlavourLine.NavisionId = value.NavisionID;
            productionFlavourLine.Buom = value.BUOM;
            productionFlavourLine.ModifiedByUserId = value.ModifiedByUserID;
            productionFlavourLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.DatabseRequire = value.DatabseRequireID != 0 ? _context.ApplicationMasterDetail.Where(m => m.ApplicationMasterDetailId == value.DatabseRequireID).Select(m => m.Value).FirstOrDefault() : "";
            value.Navision = _context.Navitems.Where(t => t.ItemId == value.NavisionID).Select(t => t.No).FirstOrDefault();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionFlavourLine")]
        public void Delete(int id)
        {
            var productionFlavourLine = _context.ProductionFlavourLine.SingleOrDefault(p => p.ProductionFlavourLineId == id);
            if (productionFlavourLine != null)
            {
                _context.ProductionFlavourLine.Remove(productionFlavourLine);
                _context.SaveChanges();
            }
        }
    }
}