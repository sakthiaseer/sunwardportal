﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PurchaseItemSalesEntryLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PurchaseItemSalesEntryLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPurchaseItemSalesEntryLines")]
        public List<PurchaseItemSalesEntryLineModel> Get(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.SalesOrderEntryId == id).Include(i => i.SobyCustomers).Include(i => i.Item)
                            .AsNoTracking().ToList();
            List<PurchaseItemSalesEntryLineModel> PurchaseItemSalesEntryLineModels = new List<PurchaseItemSalesEntryLineModel>();
            PurchaseItemSalesEntryLine.ForEach(s =>
            {
                PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();

                purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId;
                purchaseItemSalesEntryLineModel.QuotationNo = s.QuotationNo;
                purchaseItemSalesEntryLineModel.SalesOrderEntryID = s.SalesOrderEntryId;
                purchaseItemSalesEntryLineModel.SoByCustomersId = s.SobyCustomersId;
                purchaseItemSalesEntryLineModel.PricePerQty = masterDetailList != null && s.SobyCustomers?.PurchasePerUomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.SobyCustomers?.PurchasePerUomId).Select(a => a.Value).SingleOrDefault() : "";
                purchaseItemSalesEntryLineModel.OrderQty = s.OrderQty;
                purchaseItemSalesEntryLineModel.OrderCurrencyID = s.OrderCurrencyId;
                purchaseItemSalesEntryLineModel.SellingPrice = s.SellingPrice;
                purchaseItemSalesEntryLineModel.FOC = s.Foc;
                purchaseItemSalesEntryLineModel.SchdAgreementNo = s.SchdAgreementNo;
                purchaseItemSalesEntryLineModel.TenderAgencyNo = s.TenderAgencyNo;
                purchaseItemSalesEntryLineModel.ShipToAddressID = s.ShipToAddressId;
                purchaseItemSalesEntryLineModel.UOMID = s.Uomid;
                purchaseItemSalesEntryLineModel.ItemId = s.ItemId;
                purchaseItemSalesEntryLineModel.UOMfirstID = s.UomfirstId;
                purchaseItemSalesEntryLineModel.UOMsecondID = s.UomsecondId;
                purchaseItemSalesEntryLineModel.IsAllowExtension = s.IsAllowExtension;
                purchaseItemSalesEntryLineModel.ExtensionMonth = s.ExtensionMonth;
                purchaseItemSalesEntryLineModel.ExtensionQuantity = s.ExtensionQuantity;
                purchaseItemSalesEntryLineModel.IsLotDeliveries = s.IsLotDeliveries;
                purchaseItemSalesEntryLineModel.DeliveryDateOfOrder = s.DeliveryDateOfOrder;
                purchaseItemSalesEntryLineModel.StatusCodeID = s.StatusCodeId;
                purchaseItemSalesEntryLineModel.AddedByUserID = s.AddedByUserId;
                purchaseItemSalesEntryLineModel.ModifiedByUserID = s.ModifiedByUserId;
                purchaseItemSalesEntryLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                purchaseItemSalesEntryLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                purchaseItemSalesEntryLineModel.StatusCode = s.StatusCode?.CodeValue;
                purchaseItemSalesEntryLineModel.AddedDate = s.AddedDate;
                purchaseItemSalesEntryLineModel.ModifiedDate = s.ModifiedDate;
                purchaseItemSalesEntryLineModel.OptionalQty = s.OptionalQty;
                //purchaseItemSalesEntryLineModel.PricePerQty = s.PricePerQty;

                if (s.SobyCustomersId != null && s.SobyCustomersId > 0)
                {
                    purchaseItemSalesEntryLineModel.ProductNO = s.SobyCustomers?.CustomerReferenceNo;
                    purchaseItemSalesEntryLineModel.Description = s.SobyCustomers?.Description;
                    var PerUomId = s.SobyCustomers?.PerUomId;
                    if (PerUomId > 0)
                    {
                        purchaseItemSalesEntryLineModel.UOM = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == PerUomId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    var productGroupId = _context.SobyCustomerSunwardEquivalent.Where(s => s.SocustomersItemCrossReferenceId == purchaseItemSalesEntryLineModel.SoByCustomersId).Select(s => s.NavItemId).FirstOrDefault();
                    if (productGroupId != null)
                    {
                        var productGroupCode = _context.GenericCodes.Where(s => s.GenericCodeId == productGroupId).FirstOrDefault();
                        if (productGroupCode != null)
                        {
                            purchaseItemSalesEntryLineModel.ProductGroupCode = productGroupCode.Code;
                            purchaseItemSalesEntryLineModel.ProductGroupUom = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == productGroupCode.Uom).Select(a => a.Value).SingleOrDefault() : "";
                        }
                    }

                }
                if (s.ItemId != null && s.ItemId > 0)
                {
                    purchaseItemSalesEntryLineModel.ProductNO = s.Item?.No;
                }
                purchaseItemSalesEntryLineModel.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";

                PurchaseItemSalesEntryLineModels.Add(purchaseItemSalesEntryLineModel);
            });
            return PurchaseItemSalesEntryLineModels.OrderByDescending(a => a.PurchaseItemSalesEntryLineID).ToList();
        }

        [HttpGet]
        [Route("GetPurchaseItemSalesEntryLineById")]
        public ActionResult<PurchaseItemSalesEntryLineModel> GetPurchaseItemSalesEntryLineById(int? id)
        {
            PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.PurchaseItemSalesEntryLineId == id).Include(i => i.SobyCustomers).Include(i => i.Item)
                            .AsNoTracking().FirstOrDefault();
            List<PurchaseItemSalesEntryLineModel> PurchaseItemSalesEntryLineModels = new List<PurchaseItemSalesEntryLineModel>();

            purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();

            purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = PurchaseItemSalesEntryLine.PurchaseItemSalesEntryLineId;
            purchaseItemSalesEntryLineModel.QuotationNo = PurchaseItemSalesEntryLine.QuotationNo;
            purchaseItemSalesEntryLineModel.SalesOrderEntryID = PurchaseItemSalesEntryLine.SalesOrderEntryId;
            purchaseItemSalesEntryLineModel.SoByCustomersId = PurchaseItemSalesEntryLine.SobyCustomersId;
            purchaseItemSalesEntryLineModel.PricePerQty = masterDetailList != null && PurchaseItemSalesEntryLine.SobyCustomers?.PurchasePerUomId > 0 ? masterDetailList.Where(a => a.ApplicationMasterDetailId == PurchaseItemSalesEntryLine.SobyCustomers?.PurchasePerUomId).Select(a => a.Value).SingleOrDefault() : "";
            purchaseItemSalesEntryLineModel.OrderQty = PurchaseItemSalesEntryLine.OrderQty;
            purchaseItemSalesEntryLineModel.OrderCurrencyID = PurchaseItemSalesEntryLine.OrderCurrencyId;
            purchaseItemSalesEntryLineModel.SellingPrice = PurchaseItemSalesEntryLine.SellingPrice;
            purchaseItemSalesEntryLineModel.FOC = PurchaseItemSalesEntryLine.Foc;
            purchaseItemSalesEntryLineModel.SchdAgreementNo = PurchaseItemSalesEntryLine.SchdAgreementNo;
            purchaseItemSalesEntryLineModel.TenderAgencyNo = PurchaseItemSalesEntryLine.TenderAgencyNo;
            purchaseItemSalesEntryLineModel.ShipToAddressID = PurchaseItemSalesEntryLine.ShipToAddressId;
            purchaseItemSalesEntryLineModel.UOMID = PurchaseItemSalesEntryLine.Uomid;
            purchaseItemSalesEntryLineModel.ItemId = PurchaseItemSalesEntryLine.ItemId;
            purchaseItemSalesEntryLineModel.UOMfirstID = PurchaseItemSalesEntryLine.UomfirstId;
            purchaseItemSalesEntryLineModel.UOMsecondID = PurchaseItemSalesEntryLine.UomsecondId;
            purchaseItemSalesEntryLineModel.IsAllowExtension = PurchaseItemSalesEntryLine.IsAllowExtension;
            purchaseItemSalesEntryLineModel.ExtensionMonth = PurchaseItemSalesEntryLine.ExtensionMonth;
            purchaseItemSalesEntryLineModel.ExtensionQuantity = PurchaseItemSalesEntryLine.ExtensionQuantity;
            purchaseItemSalesEntryLineModel.IsLotDeliveries = PurchaseItemSalesEntryLine.IsLotDeliveries;
            purchaseItemSalesEntryLineModel.DeliveryDateOfOrder = PurchaseItemSalesEntryLine.DeliveryDateOfOrder;
            purchaseItemSalesEntryLineModel.StatusCodeID = PurchaseItemSalesEntryLine.StatusCodeId;
            purchaseItemSalesEntryLineModel.AddedByUserID = PurchaseItemSalesEntryLine.AddedByUserId;
            purchaseItemSalesEntryLineModel.ModifiedByUserID = PurchaseItemSalesEntryLine.ModifiedByUserId;
            purchaseItemSalesEntryLineModel.AddedByUser = PurchaseItemSalesEntryLine.AddedByUser != null ? PurchaseItemSalesEntryLine.AddedByUser.UserName : "";
            purchaseItemSalesEntryLineModel.ModifiedByUser = PurchaseItemSalesEntryLine.ModifiedByUser != null ? PurchaseItemSalesEntryLine.ModifiedByUser.UserName : "";
            purchaseItemSalesEntryLineModel.StatusCode = PurchaseItemSalesEntryLine.StatusCode?.CodeValue;
            purchaseItemSalesEntryLineModel.AddedDate = PurchaseItemSalesEntryLine.AddedDate;
            purchaseItemSalesEntryLineModel.ModifiedDate = PurchaseItemSalesEntryLine.ModifiedDate;
            purchaseItemSalesEntryLineModel.OptionalQty = PurchaseItemSalesEntryLine.OptionalQty;
            //purchaseItemSalesEntryLineModel.PricePerQty = s.PricePerQty;

            if (PurchaseItemSalesEntryLine.SobyCustomersId != null && PurchaseItemSalesEntryLine.SobyCustomersId > 0)
            {
                purchaseItemSalesEntryLineModel.ProductNO = PurchaseItemSalesEntryLine.SobyCustomers?.CustomerReferenceNo;
                purchaseItemSalesEntryLineModel.Description = PurchaseItemSalesEntryLine.SobyCustomers?.Description;
                var PerUomId = PurchaseItemSalesEntryLine.SobyCustomers?.PerUomId;
                if (PerUomId > 0)
                {
                    purchaseItemSalesEntryLineModel.UOM = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == PerUomId).Select(a => a.Value).SingleOrDefault() : "";
                }
                var productGroupId = _context.SobyCustomerSunwardEquivalent.Where(s => s.SocustomersItemCrossReferenceId == purchaseItemSalesEntryLineModel.SoByCustomersId).Select(s => s.NavItemId).FirstOrDefault();
                if(productGroupId!=null)
                {
                    var productGroupCode = _context.GenericCodes.Where(s => s.GenericCodeId == productGroupId).FirstOrDefault();
                    if (productGroupCode != null)
                    {
                        purchaseItemSalesEntryLineModel.ProductGroupCode = productGroupCode.Code;
                        purchaseItemSalesEntryLineModel.ProductGroupUom = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == productGroupCode.Uom).Select(a => a.Value).SingleOrDefault() : "";
                    }
                }
            }
            if (PurchaseItemSalesEntryLine.ItemId != null && PurchaseItemSalesEntryLine.ItemId > 0)
            {
                purchaseItemSalesEntryLineModel.ProductNO = PurchaseItemSalesEntryLine.Item?.No;
            }
            purchaseItemSalesEntryLineModel.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == PurchaseItemSalesEntryLine.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";

            PurchaseItemSalesEntryLineModels.Add(purchaseItemSalesEntryLineModel);

            return purchaseItemSalesEntryLineModel;
        }
        [HttpGet]
        [Route("GetPurchaseItemSalesEntryLinesItems")]
        public List<PurchaseItemSalesEntryLineModel> GetPurchaseItemSalesEntryLinesItems()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(i => i.SobyCustomers)
                            .AsNoTracking().ToList();
            List<PurchaseItemSalesEntryLineModel> PurchaseItemSalesEntryLineModels = new List<PurchaseItemSalesEntryLineModel>();
            PurchaseItemSalesEntryLine.ForEach(s =>
            {
                PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();

                purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId;
                purchaseItemSalesEntryLineModel.QuotationNo = s.QuotationNo;
                purchaseItemSalesEntryLineModel.SalesOrderEntryID = s.SalesOrderEntryId;
                purchaseItemSalesEntryLineModel.SoByCustomersId = s.SobyCustomersId;

                purchaseItemSalesEntryLineModel.OrderQty = s.OrderQty;
                purchaseItemSalesEntryLineModel.OrderCurrencyID = s.OrderCurrencyId;
                purchaseItemSalesEntryLineModel.SellingPrice = s.SellingPrice;
                purchaseItemSalesEntryLineModel.FOC = s.Foc;
                purchaseItemSalesEntryLineModel.SchdAgreementNo = s.SchdAgreementNo;
                purchaseItemSalesEntryLineModel.TenderAgencyNo = s.TenderAgencyNo;
                purchaseItemSalesEntryLineModel.ShipToAddressID = s.ShipToAddressId;
                purchaseItemSalesEntryLineModel.UOMID = s.Uomid;
                purchaseItemSalesEntryLineModel.ItemId = s.ItemId;
                purchaseItemSalesEntryLineModel.UOMfirstID = s.UomfirstId;
                purchaseItemSalesEntryLineModel.UOMsecondID = s.UomsecondId;
                purchaseItemSalesEntryLineModel.IsAllowExtension = s.IsAllowExtension;
                purchaseItemSalesEntryLineModel.ExtensionMonth = s.ExtensionMonth;
                purchaseItemSalesEntryLineModel.ExtensionQuantity = s.ExtensionQuantity;
                purchaseItemSalesEntryLineModel.IsLotDeliveries = s.IsLotDeliveries;
                purchaseItemSalesEntryLineModel.DeliveryDateOfOrder = s.DeliveryDateOfOrder;
                purchaseItemSalesEntryLineModel.StatusCodeID = s.StatusCodeId;
                purchaseItemSalesEntryLineModel.AddedByUserID = s.AddedByUserId;
                purchaseItemSalesEntryLineModel.ModifiedByUserID = s.ModifiedByUserId;
                purchaseItemSalesEntryLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                purchaseItemSalesEntryLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                purchaseItemSalesEntryLineModel.StatusCode = s.StatusCode?.CodeValue;
                purchaseItemSalesEntryLineModel.AddedDate = s.AddedDate;
                purchaseItemSalesEntryLineModel.ModifiedDate = s.ModifiedDate;
                purchaseItemSalesEntryLineModel.OptionalQty = s.OptionalQty;
                //purchaseItemSalesEntryLineModel.PricePerQty = s.PricePerQty;
                if (s.SobyCustomersId != null && s.SobyCustomersId > 0)
                {
                    purchaseItemSalesEntryLineModel.ProductNO = s.SobyCustomers?.CustomerReferenceNo;
                }
                if (s.ItemId != null && s.ItemId > 0)
                {
                    purchaseItemSalesEntryLineModel.ProductNO = s.Item?.No;
                }
                purchaseItemSalesEntryLineModel.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.OrderCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                purchaseItemSalesEntryLineModel.UOM = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Uomid).Select(a => a.Value).SingleOrDefault() : "";
                var productGroupId = _context.SobyCustomerSunwardEquivalent.Where(s => s.SocustomersItemCrossReferenceId == purchaseItemSalesEntryLineModel.SoByCustomersId).Select(s => s.NavItemId).FirstOrDefault();
                if (productGroupId != null)
                {
                    var productGroupCode = _context.GenericCodes.Where(s => s.GenericCodeId == productGroupId).FirstOrDefault();
                    if (productGroupCode != null)
                    {
                        purchaseItemSalesEntryLineModel.ProductGroupCode = productGroupCode.Code;
                        purchaseItemSalesEntryLineModel.ProductGroupUom = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == productGroupCode.Uom).Select(a => a.Value).SingleOrDefault() : "";
                    }
                }
                PurchaseItemSalesEntryLineModels.Add(purchaseItemSalesEntryLineModel);
            });
            return PurchaseItemSalesEntryLineModels.OrderByDescending(a => a.PurchaseItemSalesEntryLineID).ToList();
        }
        [HttpGet]
        [Route("GetGenericCodeForReference")]
        public List<GenericCodesModel> GetGenericCodeForReference(int id)
        {
            List<GenericCodesModel> genericCodeModels = new List<GenericCodesModel>();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<long?> sunwardEquvalentNavItemIds = _context.SobyCustomerSunwardEquivalent.Where(s => s.SocustomersItemCrossReferenceId == id && s.IsThisDefault == true).Select(s => s.NavItemId).ToList();
            if (sunwardEquvalentNavItemIds != null && sunwardEquvalentNavItemIds.Count > 0)
            {
                var gerericcodeList = _context.GenericCodes.Where(g => sunwardEquvalentNavItemIds.Contains(g.GenericCodeId)).ToList();
                if (gerericcodeList != null && gerericcodeList.Count > 0)
                {
                    gerericcodeList.ForEach(s =>
                    {
                        GenericCodesModel genericCodeModel = new GenericCodesModel();
                        genericCodeModel.Code = s.Code;
                        genericCodeModel.Description = s.Description;
                        genericCodeModel.Uom = s.Uom;
                        genericCodeModel.UomName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Uom).Select(a => a.Value).SingleOrDefault() : "";
                        genericCodeModels.Add(genericCodeModel);
                    });

                }



            }
            return genericCodeModels;
        }

        [HttpPost()]
        [Route("GetNavisionReference")]
        public List<NavItemModel> GetNavisionReference(GetNavisionReferenceModel getNavisionReferenceModel)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
           
            List<ProductGroupingNavModel> productGroupingNavModels = new List<ProductGroupingNavModel>();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var supplytoId = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == getNavisionReferenceModel.SalesOrderEntryId).FirstOrDefault().CompanyId;
            var navitemIds = _context.SobyCustomerSunwardEquivalent.Where(s => s.SocustomersItemCrossReferenceId == getNavisionReferenceModel.Id && s.OrderPlacetoCompanyId ==supplytoId ).Select(s => s.NavItemId).FirstOrDefault();
            var genericCodeSupplyToMultiples = _context.GenericCodeSupplyToMultiple.Where(g => g.GenericCodeId == navitemIds).ToList();
            if (genericCodeSupplyToMultiples != null && genericCodeSupplyToMultiples.Count > 0)
            {
                genericCodeSupplyToMultiples.ForEach(s =>
                {
                    GenericCodeSupplyToMultipleModel genericCodeSupplyToMultipleModel = new GenericCodeSupplyToMultipleModel();

                    genericCodeSupplyToMultipleModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                    genericCodeSupplyToMultipleModel.GenericCodeId = s.GenericCodeId;
                    genericCodeSupplyToMultipleModel.Description = s.GenericCodeSupplyDescription;
                    if (s.GenericCodeId != null)
                    {
                        var Navitems = _context.ProductGroupingNav.Include(p => p.ProductGroupingManufacture).Include(n => n.Item).Where(w => w.ProductGroupingManufacture.ProductGroupingId == s.GenericCodeId && w.GenericCodeSupplyToMultipleId == s.GenericCodeSupplyToMultipleId).ToList();
                        if (Navitems != null)
                        {

                            Navitems.ForEach(h =>
                            {
                                if (h.Item != null)
                                {
                                    NavItemModel NavItemModel = new NavItemModel();
                                    NavItemModel.ItemId = h.Item.ItemId;
                                    NavItemModel.No = h.Item.No;
                                    NavItemModel.Description = h.Item.Description;
                                    NavItemModel.Description2 = h.Item.Description2;
                                    NavItemModel.InternalRef = h.Item.InternalRef;
                                    NavItemModel.ItemCategoryCode = h.Item.ItemCategoryCode;
                                    NavItemModel.BaseUnitofMeasure = h.Item.BaseUnitofMeasure;
                                    NavItemModel.CompanyId = h.Item.CompanyId;
                                    NavItemModel.VendorNo = h.Item.VendorNo;
                                    NavItemModel.ReplenishmentMethod = h.Item.VendorNo;
                                    navItemModels.Add(NavItemModel);
                                }
                            });
                        }
                    }

                    //var navItems = _context.Navitems.Where(s => navitemIds.Contains(s.ItemId)).ToList();

                    //if(navItems!=null && navItems.Count>0)
                    //{
                    //    navItems.ForEach(n =>
                    //    {


                    //    NavItemModel navItemModel = new NavItemModel();
                    //    navItemModel.ItemId = n.ItemId;
                    //    navItemModel.No = n.No;
                    //    navItemModel.Description = n.Description;
                    //    navItemModel.Description2 = n.Description2;
                    //    navItemModel.BaseUnitofMeasure = n.BaseUnitofMeasure;
                    //    navItemModel.ItemCategoryCode = n.ItemCategoryCode;
                    //    navItemModel.InternalRef = n.InternalRef;
                    //    navItemModel.ReplenishmentMethod = n.ReplenishmentMethodId!=null&&masterDetailList!=null? masterDetailList.Where(a => a.ApplicationMasterDetailId == n.ReplenishmentMethodId).Select(a => a.Value).SingleOrDefault() : "";
                    //    navItemModels.Add(navItemModel);
                    //    });
                    //}
                });

            }
            return navItemModels;

        }
        [HttpGet]
        [Route("GetGenericCodeForContractDistributionReference")]
        public List<GenericCodesModel> GetGenericCodeForContractDistributionReference(int id)
        {
            List<GenericCodesModel> genericCodeModels = new List<GenericCodesModel>();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<long?> sunwardEquvalentNavItemIds = _context.SobyCustomerSunwardEquivalent.Where(s => s.SobyCustomersId == id).Select(s => s.NavItemId).ToList();
            if (sunwardEquvalentNavItemIds != null && sunwardEquvalentNavItemIds.Count > 0)
            {
                var gerericcodeList = _context.GenericCodes.Where(g => sunwardEquvalentNavItemIds.Contains(g.GenericCodeId)).ToList();
                if (gerericcodeList != null && gerericcodeList.Count > 0)
                {
                    gerericcodeList.ForEach(s =>
                    {
                        GenericCodesModel genericCodeModel = new GenericCodesModel();
                        genericCodeModel.Code = s.Code;
                        genericCodeModel.Description = s.Description;
                        genericCodeModel.Uom = s.Uom;
                        genericCodeModel.UomName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Uom).Select(a => a.Value).SingleOrDefault() : "";
                        genericCodeModels.Add(genericCodeModel);
                    });

                }



            }
            return genericCodeModels;
        }
        [HttpPost]
        [Route("GetPurchaseItemsByProductNo")]
        public List<PurchaseItemReferenceModel> GetPurchaseItemsByProductNo(PurchaseItemInfoGetReferenceModel refmodel)
        {
            List<PurchaseItemSalesEntryLine> purchaseItemSalesEntryLinelist = new List<PurchaseItemSalesEntryLine>();
            if (refmodel.SoCustomerId != null)
            {
                purchaseItemSalesEntryLinelist = _context.PurchaseItemSalesEntryLine.Include(s => s.SalesOrderEntry).Include(s => s.SalesOrderEntry.OrderBy).Include(s => s.SalesOrderEntry.Customer).Include(s => s.SobyCustomers).Include(s => s.StatusCode).Where(s => s.SobyCustomersId == refmodel.SoCustomerId && s.StatusCodeId != 1421).ToList();
            }
            if (refmodel.ItemId != null)
            {
                purchaseItemSalesEntryLinelist = _context.PurchaseItemSalesEntryLine.Include(s => s.SalesOrderEntry).Include(s => s.SalesOrderEntry.OrderBy).Include(s => s.SalesOrderEntry.Customer).Include(s => s.SobyCustomers).Include(i => i.Item).Include(s => s.StatusCode).Where(s => s.ItemId == refmodel.ItemId && s.StatusCodeId != 1421).ToList();

            }
            List<PurchaseItemReferenceModel> purchaseItemReferenceModels = new List<PurchaseItemReferenceModel>();
            PurchaseItemReferenceModel purchaseItemReferenceModel = new PurchaseItemReferenceModel();
            if (purchaseItemSalesEntryLinelist != null && purchaseItemSalesEntryLinelist.Count > 0)
            {
                purchaseItemSalesEntryLinelist.ForEach(p =>
                {
                    purchaseItemReferenceModel = new PurchaseItemReferenceModel();
                    purchaseItemReferenceModel.OrderBy = p.SalesOrderEntry?.OrderBy?.CodeValue;
                    purchaseItemReferenceModel.CustomerName = p.SalesOrderEntry?.Customer?.CompanyName;
                    if (p.ItemId != null)
                    {
                        purchaseItemReferenceModel.ProductNo = p.Item?.No;
                        purchaseItemReferenceModel.Description = p.Item?.Description;
                    }
                    if (p.SobyCustomersId != null)
                    {
                        purchaseItemReferenceModel.ProductNo = p.SobyCustomers?.CustomerReferenceNo;
                        purchaseItemReferenceModel.Description = p.SobyCustomers?.Description;
                    }
                    purchaseItemReferenceModel.Status = p.StatusCode?.CodeValue;
                    purchaseItemReferenceModels.Add(purchaseItemReferenceModel);
                });
            }
            return purchaseItemReferenceModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PurchaseItemSalesEntryLineModel> GetData(SearchModel searchModel)
        {
            var PurchaseItemSalesEntryLine = new PurchaseItemSalesEntryLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).LastOrDefault();
                        break;
                    case "Previous":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).LastOrDefault();
                        break;
                    case "Next":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderBy(o => o.PurchaseItemSalesEntryLineId).FirstOrDefault(s => s.PurchaseItemSalesEntryLineId > searchModel.Id);
                        break;
                    case "Previous":
                        PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.OrderByDescending(o => o.PurchaseItemSalesEntryLineId).FirstOrDefault(s => s.PurchaseItemSalesEntryLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PurchaseItemSalesEntryLineModel>(PurchaseItemSalesEntryLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPurchaseItemSalesEntryLine")]
        public PurchaseItemSalesEntryLineModel Post(PurchaseItemSalesEntryLineModel value)
        {
            var salesEntry = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).FirstOrDefault();
            if (salesEntry != null)
            {
                if (salesEntry.IsMultipleQuatationNo == false)
                {

                }

                if (salesEntry.OrderById == 1402)
                {
                    value.ItemId = null;
                    if (salesEntry.TypeOfOrderId == 1404 || salesEntry.TypeOfOrderId == 1405 || salesEntry.TypeOfOrderId == 1406 || salesEntry.TypeOfOrderId == 1408)
                    {
                        value.IsLotDeliveries = false;
                    }
                    else if (salesEntry.TypeOfOrderId == 1407)
                    {
                        if (value.IsLotDeliveries == true)
                        {
                            value.ShipToAddressID = null;
                            value.DeliveryDateOfOrder = null;
                        }

                    }
                    else if (salesEntry.TypeOfOrderId == 1409)
                    {
                        value.SchdAgreementNo = "";
                        value.DeliveryDateOfOrder = null;
                        value.ShipToAddressID = null;
                    }
                    else if (salesEntry.TypeOfOrderId == 1406)
                    {
                        value.SchdAgreementNo = "";
                        value.TenderAgencyNo = "";
                        value.DeliveryDateOfOrder = null;
                    }
                    else if (salesEntry.TypeOfOrderId == 1408)
                    {
                        value.SchdAgreementNo = "";
                        value.TenderAgencyNo = "";

                    }
                }
            }
            var PurchaseItemSalesEntryLine = new PurchaseItemSalesEntryLine
            {
                QuotationNo = value.QuotationNo,
                SalesOrderEntryId = value.SalesOrderEntryID,
                SobyCustomersId = value.SoByCustomersId,
                OrderQty = value.OrderQty,
                OrderCurrencyId = value.OrderCurrencyID,
                SellingPrice = value.SellingPrice,
                Foc = value.FOC,
                IsFoc = value.IsFOC,
                Uomid = value.UOMID,
                UomfirstId = value.UOMfirstID,
                UomsecondId = value.UOMsecondID,
                ItemId = value.ItemId,
                ShipToAddressId = value.ShipToAddressID,
                SchdAgreementNo = value.SchdAgreementNo,
                TenderAgencyNo = value.TenderAgencyNo,
                IsAllowExtension = value.IsAllowExtension,
                ExtensionMonth = value.ExtensionMonth,
                ExtensionQuantity = value.ExtensionQuantity,
                IsLotDeliveries = value.IsLotDeliveries,
                DeliveryDateOfOrder = value.DeliveryDateOfOrder,

                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,
                OptionalQty = value.OptionalQty,
                //PricePerQty = value.PricePerQty,

            };
            _context.PurchaseItemSalesEntryLine.Add(PurchaseItemSalesEntryLine);
            _context.SaveChanges();
            var purchaselinesStatus = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).Select(s => s.StatusCodeId).ToList();
            var closedStatus = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID && s.StatusCodeId == 1421).Select(s => s.StatusCodeId).ToList();
            if (purchaselinesStatus != null && purchaselinesStatus.Count > 0)
            {
                if (closedStatus != null && closedStatus.Count > 0)
                {
                    if (purchaselinesStatus.Count == closedStatus.Count)
                    {
                        var salesentry = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).FirstOrDefault();
                        if (salesentry != null)
                        {
                            salesentry.StatusCodeId = 1421;
                        }
                    }
                    else
                    {
                        var salesentry = _context.SalesOrderEntry.Where(s => purchaselinesStatus.Contains(1422)).FirstOrDefault();
                        if (salesentry != null)
                        {
                            salesentry.StatusCodeId = 1422;
                        }
                    }
                }
                else
                {
                    var salesentry = _context.SalesOrderEntry.Where(s => purchaselinesStatus.Contains(1422)).FirstOrDefault();
                    if (salesentry != null)
                    {
                        salesentry.StatusCodeId = 1422;
                    }
                }
                _context.SaveChanges();
            }
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var codeMasterlist = _context.CodeMaster.AsNoTracking().ToList();
            value.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyID).Select(a => a.Value).SingleOrDefault() : "";
            if (value.SoByCustomersId > 0)
            {
                var Peruomid = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.PerUomId).FirstOrDefault();
                if (Peruomid != null && Peruomid > 0)
                {
                    value.UOM = masterDetailList.Where(a => a.ApplicationMasterDetailId == Peruomid).Select(a => a.Value).SingleOrDefault();
                }
                value.ProductNO = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
                value.Description = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.Description).FirstOrDefault();

            }
            value.PurchaseItemSalesEntryLineID = PurchaseItemSalesEntryLine.PurchaseItemSalesEntryLineId;

            value.StatusCode = codeMasterlist != null ? codeMasterlist.Where(s => s.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault() : "";
            value.AddedDate = PurchaseItemSalesEntryLine.AddedDate;
            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == PurchaseItemSalesEntryLine.AddedByUserId).Select(s => s.UserName).FirstOrDefault();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePurchaseItemSalesEntryLine")]
        public PurchaseItemSalesEntryLineModel Put(PurchaseItemSalesEntryLineModel value)
        {
            var salesEntry = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).FirstOrDefault();
            if (salesEntry != null)
            {
                if (salesEntry.OrderById == 1403 && salesEntry.TypeOfOrderId == 1406)
                {
                    value.SchdAgreementNo = "";
                    value.TenderAgencyNo = "";
                    value.DeliveryDateOfOrder = null;
                    value.ShipToAddressID = null;

                }
                if (salesEntry.OrderById == 1402 && salesEntry.TypeOfSalesOrderId == 1402)
                {
                    value.ItemId = null;
                    if (salesEntry.TypeOfOrderId == 1404)
                    {
                        value.IsLotDeliveries = false;
                    }
                    else if (salesEntry.TypeOfOrderId == 1407)
                    {
                        if (value.IsLotDeliveries == true)
                        {
                            value.DeliveryDateOfOrder = null;
                            value.ShipToAddressID = null;
                        }

                    }
                    else if (salesEntry.TypeOfOrderId == 1409)
                    {
                        value.SchdAgreementNo = "";
                        value.DeliveryDateOfOrder = null;
                        value.ShipToAddressID = null;
                    }
                    else if (salesEntry.TypeOfOrderId == 1405)
                    {
                        value.IsLotDeliveries = false;
                    }
                    else if (salesEntry.TypeOfOrderId == 1406)
                    {
                        value.SchdAgreementNo = "";
                        value.TenderAgencyNo = "";
                        value.DeliveryDateOfOrder = null;
                    }
                    else if (salesEntry.TypeOfOrderId == 1408)
                    {
                        value.SchdAgreementNo = "";
                        value.TenderAgencyNo = "";


                    }

                    else
                    {
                        if (salesEntry.TypeOfOrderId == 1405 || salesEntry.TypeOfOrderId == 1406 || salesEntry.TypeOfOrderId == 1408)
                        {
                            var distribution = _context.ContractDistributionSalesEntryLine.Where(c => c.PurchaseItemSalesEntryLineId == value.PurchaseItemSalesEntryLineID).ToList();
                            if (distribution != null && distribution.Count > 0)
                            {
                                distribution.ForEach(d =>
                                {
                                    var projectedDelivery = _context.ProjectedDeliverySalesOrderLine.Where(p => p.ContractDistributionSalesEntryLineId == d.ContractDistributionSalesEntryLineId).ToList();
                                    if (projectedDelivery != null && projectedDelivery.Count > 0)
                                    {
                                        _context.ProjectedDeliverySalesOrderLine.RemoveRange(projectedDelivery);
                                        _context.SaveChanges();
                                    }
                                });
                                _context.ContractDistributionSalesEntryLine.RemoveRange(distribution);
                                _context.SaveChanges();
                            }
                        }
                    }
                }
            }
            var purchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.SingleOrDefault(p => p.PurchaseItemSalesEntryLineId == value.PurchaseItemSalesEntryLineID);
            purchaseItemSalesEntryLine.QuotationNo = value.QuotationNo;
            purchaseItemSalesEntryLine.SalesOrderEntryId = value.SalesOrderEntryID;
            purchaseItemSalesEntryLine.SobyCustomersId = value.SoByCustomersId;
            purchaseItemSalesEntryLine.OrderQty = value.OrderQty;
            purchaseItemSalesEntryLine.OrderCurrencyId = value.OrderCurrencyID;
            purchaseItemSalesEntryLine.SellingPrice = value.SellingPrice;
            purchaseItemSalesEntryLine.Foc = value.FOC;
            purchaseItemSalesEntryLine.IsFoc = value.IsFOC;
            purchaseItemSalesEntryLine.TenderAgencyNo = value.TenderAgencyNo;
            purchaseItemSalesEntryLine.SchdAgreementNo = value.SchdAgreementNo;
            purchaseItemSalesEntryLine.ShipToAddressId = value.ShipToAddressID;
            purchaseItemSalesEntryLine.Uomid = value.UOMID;
            purchaseItemSalesEntryLine.UomfirstId = value.UOMfirstID;
            purchaseItemSalesEntryLine.UomsecondId = value.UOMsecondID;
            purchaseItemSalesEntryLine.ItemId = value.ItemId;
            purchaseItemSalesEntryLine.IsAllowExtension = value.IsAllowExtension;
            purchaseItemSalesEntryLine.ExtensionMonth = value.ExtensionMonth;
            purchaseItemSalesEntryLine.ExtensionQuantity = value.ExtensionQuantity;
            purchaseItemSalesEntryLine.IsLotDeliveries = value.IsLotDeliveries;
            purchaseItemSalesEntryLine.DeliveryDateOfOrder = value.DeliveryDateOfOrder;
            purchaseItemSalesEntryLine.OptionalQty = value.OptionalQty;
            // purchaseItemSalesEntryLine.PricePerQty = value.PricePerQty;
            purchaseItemSalesEntryLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            var purchaselinesStatus = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).Select(s => s.StatusCodeId).ToList();
            var closedStatus = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID && s.StatusCodeId == 1421).Select(s => s.StatusCodeId).ToList();
            if (purchaselinesStatus != null && purchaselinesStatus.Count > 0)
            {
                if (closedStatus != null && closedStatus.Count > 0)
                {
                    if (purchaselinesStatus.Count == closedStatus.Count)
                    {
                        var salesentry = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryID).FirstOrDefault();
                        if (salesentry != null)
                        {
                            salesentry.StatusCodeId = 1421;
                        }
                    }
                    else
                    {
                        var salesentry = _context.SalesOrderEntry.Where(s => purchaselinesStatus.Contains(1422)).FirstOrDefault();
                        if (salesentry != null)
                        {
                            salesentry.StatusCodeId = 1422;
                        }
                    }
                }
                else
                {
                    var salesentry = _context.SalesOrderEntry.Where(s => purchaselinesStatus.Contains(1422)).FirstOrDefault();
                    if (salesentry != null)
                    {
                        salesentry.StatusCodeId = 1422;
                    }
                }
                _context.SaveChanges();
            }
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var codeMasterlist = _context.CodeMaster.AsNoTracking().ToList();
            value.OrderCurrency = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.OrderCurrencyID).Select(a => a.Value).SingleOrDefault() : "";
            if (value.SoByCustomersId > 0)
            {
                var Peruomid = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.PerUomId).FirstOrDefault();
                if (Peruomid != null && Peruomid > 0)
                {
                    value.UOM = masterDetailList.Where(a => a.ApplicationMasterDetailId == Peruomid).Select(a => a.Value).SingleOrDefault();
                }
                value.ProductNO = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
                value.Description = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.SoByCustomersId).Select(s => s.Description).FirstOrDefault();

            }

            value.StatusCode = codeMasterlist != null ? codeMasterlist.Where(s => s.CodeId == value.StatusCodeID).Select(s => s.CodeValue).FirstOrDefault() : "";
            value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == value.ModifiedByUserID).Select(s => s.UserName).FirstOrDefault();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePurchaseItemSalesEntryLine")]
        public void Delete(int id)
        {
            try
            {
                var PurchaseItemSalesEntryLine = _context.PurchaseItemSalesEntryLine.SingleOrDefault(p => p.PurchaseItemSalesEntryLineId == id);
                if (PurchaseItemSalesEntryLine != null)
                {
                    _context.PurchaseItemSalesEntryLine.Remove(PurchaseItemSalesEntryLine);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Record Cannot be delete Reference to other records", ex);
            }
        }
    }
}