﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesSurveyByFieldForceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SalesSurveyByFieldForceController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetSalesSurveyByFieldForce")]
        public List<SalesSurveyByFieldForceModel> Get()
        {
            List<SalesSurveyByFieldForceModel> salesSurveyByFieldForceModels = new List<SalesSurveyByFieldForceModel>();
            var salesSurveyByFieldForce = _context.SalesSurveyByFieldForce.Include(a => a.Clinic).Include(b => b.SalesPerson).Include(c => c.StatusCode).Include(d => d.AddedByUser).Include(e => e.ModifiedByUser).AsNoTracking().ToList();
            salesSurveyByFieldForce.ForEach(s =>
            {
                SalesSurveyByFieldForceModel salesSurveyByFieldForceModel = new SalesSurveyByFieldForceModel();
                salesSurveyByFieldForceModel.SalesSurveyByFieldForceId = s.SalesSurveyByFieldForceId;
                salesSurveyByFieldForceModel.SalesPersonId = s.SalesPersonId;
                salesSurveyByFieldForceModel.ClinicId = s.ClinicId;
                salesSurveyByFieldForceModel.StatusCodeID = s.StatusCodeId;
                salesSurveyByFieldForceModel.SalesPersonName = s.SalesPerson?.UserName;
                salesSurveyByFieldForceModel.AddedDate = s.AddedDate;
                salesSurveyByFieldForceModel.AddedByUser = s.AddedByUser?.UserName;
                salesSurveyByFieldForceModel.ModifiedDate = s.ModifiedDate;
                salesSurveyByFieldForceModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                salesSurveyByFieldForceModel.StatusCode = s.StatusCode?.CodeValue;
                salesSurveyByFieldForceModel.ClinicName = s.Clinic?.Company + "|" + s.Clinic?.PostalCode;
                salesSurveyByFieldForceModel.Latitude = s.Latitude;
                salesSurveyByFieldForceModel.Longitude = s.Longitude;
                salesSurveyByFieldForceModel.AddressDetails = s.AddressDetails;
                salesSurveyByFieldForceModel.AddressName = s.AddressName;
                salesSurveyByFieldForceModels.Add(salesSurveyByFieldForceModel);
            });
            return salesSurveyByFieldForceModels;
        }
        [HttpGet]
        [Route("GetSalesSurveyByFieldForceBySalesPerson")]
        public List<SalesSurveyByFieldForceModel> GetSalesSurveyByFieldForceBySalesPerson(long? id)
        {
            List<SalesSurveyByFieldForceModel> salesSurveyByFieldForceModels = new List<SalesSurveyByFieldForceModel>();
            var salesSurveyByFieldForce = _context.SalesSurveyByFieldForce.Include(a => a.Clinic).Include(b => b.SalesPerson).Include(c => c.StatusCode).Include(d => d.AddedByUser).Include(e => e.ModifiedByUser).Where(w => w.SalesPersonId == id).AsNoTracking().ToList();
            salesSurveyByFieldForce.ForEach(s =>
            {
                SalesSurveyByFieldForceModel salesSurveyByFieldForceModel = new SalesSurveyByFieldForceModel();
                salesSurveyByFieldForceModel.SalesSurveyByFieldForceId = s.SalesSurveyByFieldForceId;
                salesSurveyByFieldForceModel.SalesPersonId = s.SalesPersonId;
                salesSurveyByFieldForceModel.ClinicId = s.ClinicId;
                salesSurveyByFieldForceModel.StatusCodeID = s.StatusCodeId;
                salesSurveyByFieldForceModel.SalesPersonName = s.SalesPerson?.UserName;
                salesSurveyByFieldForceModel.AddedDate = s.AddedDate;
                salesSurveyByFieldForceModel.AddedByUser = s.AddedByUser?.UserName;
                salesSurveyByFieldForceModel.ModifiedDate = s.ModifiedDate;
                salesSurveyByFieldForceModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                salesSurveyByFieldForceModel.StatusCode = s.StatusCode?.CodeValue;
                salesSurveyByFieldForceModel.ClinicName = s.Clinic?.Company + "|" + s.Clinic?.PostalCode;
                salesSurveyByFieldForceModel.Latitude = s.Latitude;
                salesSurveyByFieldForceModel.Longitude = s.Longitude;
                salesSurveyByFieldForceModel.AddressDetails = s.AddressDetails;
                salesSurveyByFieldForceModel.AddressName = s.AddressName;
                salesSurveyByFieldForceModels.Add(salesSurveyByFieldForceModel);
            });
            return salesSurveyByFieldForceModels;
        }
        [HttpGet]
        [Route("GetSalesSurveyByFieldForceLine")]
        public List<SalesSurveyByFieldForceLineModel> GetProductGroupSurveyLine(long? id)
        {
            List<SalesSurveyByFieldForceLineModel> salesSurveyByFieldForceLineModels = new List<SalesSurveyByFieldForceLineModel>();
            var salesSurveyByFieldForceLine = _context.SalesSurveyByFieldForceLine.Include(a => a.ProductGroupSurvey).Include(c => c.StatusCode).Include(d => d.AddedByUser).Include(e => e.ModifiedByUser).Where(w => w.SalesSurveyByFieldForceId == id).AsNoTracking().ToList();
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            salesSurveyByFieldForceLine.ForEach(s =>
            {
                SalesSurveyByFieldForceLineModel salesSurveyByFieldForceLineModel = new SalesSurveyByFieldForceLineModel();
                salesSurveyByFieldForceLineModel.SalesSurveyByFieldForceLineId = s.SalesSurveyByFieldForceLineId;
                salesSurveyByFieldForceLineModel.SalesSurveyByFieldForceId = s.SalesSurveyByFieldForceId;
                salesSurveyByFieldForceLineModel.ProductGroupSurveyId = s.ProductGroupSurveyId;
                salesSurveyByFieldForceLineModel.SpecificCombinationDrug = s.SpecificCombinationDrug;
                salesSurveyByFieldForceLineModel.SpecificCombinationDrugFlag = s.SpecificCombinationDrug == true ? "Yes" : "No";
                salesSurveyByFieldForceLineModel.Symptoms = s.Symptoms;
                salesSurveyByFieldForceLineModel.IsUseSwproduct = s.IsUseSwproduct;
                salesSurveyByFieldForceLineModel.DoctorBuyFrom = s.DoctorBuyFrom;
                salesSurveyByFieldForceLineModel.PossibleIndicatePrice = s.PossibleIndicatePrice;
                salesSurveyByFieldForceLineModel.AddedDate = s.AddedDate;
                salesSurveyByFieldForceLineModel.AddedByUser = s.AddedByUser?.UserName;
                salesSurveyByFieldForceLineModel.ModifiedDate = s.ModifiedDate;
                salesSurveyByFieldForceLineModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                salesSurveyByFieldForceLineModel.StatusCode = s.StatusCode?.CodeValue;
                salesSurveyByFieldForceLineModel.StatusCodeID = s.StatusCodeId;
                var PharmacologicalCategory = s.ProductGroupSurvey != null && s.ProductGroupSurvey.PharmacologicalCategoryId == null ? "" : (masterDetailList != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ProductGroupSurvey.PharmacologicalCategoryId).Select(m => m.Value).FirstOrDefault() : "");
                salesSurveyByFieldForceLineModel.ProductGroupSurveyName = s.ProductGroupSurvey?.Company?.PlantCode + "|" + s.ProductGroupSurvey?.SurveyDate.Value.ToString("MMM - yyy") + "|" + PharmacologicalCategory;
                salesSurveyByFieldForceLineModels.Add(salesSurveyByFieldForceLineModel);
            });
            return salesSurveyByFieldForceLineModels.OrderByDescending(o => o.SalesSurveyByFieldForceLineId).ToList();
        }
        [HttpPost]
        [Route("InsertSalesSurveyByFieldForce")]
        public SalesSurveyByFieldForceModel Post(SalesSurveyByFieldForceModel value)
        {
            var salesSurveyByFieldForce = new SalesSurveyByFieldForce
            {
                ClinicId = value.ClinicId,
                SalesPersonId = value.SalesPersonId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                Latitude = value.Latitude,
                Longitude = value.Longitude,
                AddressName = value.AddressName,
                AddressDetails = value.AddressDetails,
            };
            _context.SalesSurveyByFieldForce.Add(salesSurveyByFieldForce);
            _context.SaveChanges();
            value.SalesSurveyByFieldForceId = salesSurveyByFieldForce.SalesSurveyByFieldForceId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesSurveyByFieldForce")]
        public SalesSurveyByFieldForceModel Put(SalesSurveyByFieldForceModel value)
        {
            var salesSurveyByFieldForce = _context.SalesSurveyByFieldForce.SingleOrDefault(p => p.SalesSurveyByFieldForceId == value.SalesSurveyByFieldForceId);
            salesSurveyByFieldForce.ClinicId = value.ClinicId;
            salesSurveyByFieldForce.SalesPersonId = value.SalesPersonId;
            salesSurveyByFieldForce.ModifiedByUserId = value.ModifiedByUserID;
            salesSurveyByFieldForce.StatusCodeId = value.StatusCodeID.Value;
            salesSurveyByFieldForce.ModifiedDate = DateTime.Now;
            salesSurveyByFieldForce.Latitude = value.Latitude;
            salesSurveyByFieldForce.Longitude = value.Longitude;
            salesSurveyByFieldForce.AddressName = value.AddressName;
            salesSurveyByFieldForce.AddressDetails = value.AddressDetails;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertSalesSurveyByFieldForceLine")]
        public SalesSurveyByFieldForceLineModel InsertSalesSurveyByFieldForceLine(SalesSurveyByFieldForceLineModel value)
        {
            var salesSurveyByFieldForceLine = new SalesSurveyByFieldForceLine
            {
                SalesSurveyByFieldForceId = value.SalesSurveyByFieldForceId,
                ProductGroupSurveyId = value.ProductGroupSurveyId,
                SpecificCombinationDrug = value.SpecificCombinationDrugFlag == "Yes" ? true : false,
                Symptoms = value.Symptoms,
                IsUseSwproduct = value.IsUseSwproduct,
                DoctorBuyFrom = value.DoctorBuyFrom,
                PossibleIndicatePrice = value.PossibleIndicatePrice,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.SalesSurveyByFieldForceLine.Add(salesSurveyByFieldForceLine);
            _context.SaveChanges();
            value.SalesSurveyByFieldForceLineId = salesSurveyByFieldForceLine.SalesSurveyByFieldForceLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesSurveyByFieldForceLine")]
        public SalesSurveyByFieldForceLineModel UpdateSalesSurveyByFieldForceLine(SalesSurveyByFieldForceLineModel value)
        {
            var salesSurveyByFieldForceLine = _context.SalesSurveyByFieldForceLine.SingleOrDefault(p => p.SalesSurveyByFieldForceLineId == value.SalesSurveyByFieldForceLineId);
            salesSurveyByFieldForceLine.SalesSurveyByFieldForceId = value.SalesSurveyByFieldForceId;
            salesSurveyByFieldForceLine.ProductGroupSurveyId = value.ProductGroupSurveyId;
            salesSurveyByFieldForceLine.SpecificCombinationDrug = value.SpecificCombinationDrugFlag == "Yes" ? true : false;
            salesSurveyByFieldForceLine.Symptoms = value.Symptoms;
            salesSurveyByFieldForceLine.IsUseSwproduct = value.IsUseSwproduct;
            salesSurveyByFieldForceLine.DoctorBuyFrom = value.DoctorBuyFrom;
            salesSurveyByFieldForceLine.PossibleIndicatePrice = value.PossibleIndicatePrice;
            salesSurveyByFieldForceLine.ModifiedByUserId = value.ModifiedByUserID;
            salesSurveyByFieldForceLine.StatusCodeId = value.StatusCodeID.Value;
            salesSurveyByFieldForceLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesSurveyByFieldForce")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var salesSurveyByFieldForce = _context.SalesSurveyByFieldForce.Where(p => p.SalesSurveyByFieldForceId == id).FirstOrDefault();
                if (salesSurveyByFieldForce != null)
                {
                    var salesSurveyByFieldForceLine = _context.SalesSurveyByFieldForceLine.Where(p => p.SalesSurveyByFieldForceId == id).AsNoTracking().ToList();
                    if (salesSurveyByFieldForceLine != null)
                    {
                        _context.SalesSurveyByFieldForceLine.RemoveRange(salesSurveyByFieldForceLine);
                        _context.SaveChanges();
                    }
                    _context.SalesSurveyByFieldForce.Remove(salesSurveyByFieldForce);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSalesSurveyByFieldForceLine")]
        public ActionResult<string> DeleteSalesSurveyByFieldForceLine(int id)
        {
            try
            {
                var salesSurveyByFieldForceLine = _context.SalesSurveyByFieldForceLine.SingleOrDefault(p => p.SalesSurveyByFieldForceLineId == id);
                if (salesSurveyByFieldForceLine != null)
                {
                    _context.SalesSurveyByFieldForceLine.Remove(salesSurveyByFieldForceLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

}
