﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PacketInformationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PacketInformationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetPacketInformationLines")]
        public List<PacketInformationLineModel> Get(int id)
        {
            var packetInformationLines = _context.PacketInformationLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Where(s => s.TenderInformationId == id)
                .AsNoTracking()
                .ToList();
            List<PacketInformationLineModel> packetInformationLineModels = new List<PacketInformationLineModel>();
            packetInformationLines.ForEach(s =>
            {
                PacketInformationLineModel packetInformationLineModel = new PacketInformationLineModel
                {
                    PacketInformationId = s.PacketInformationId,
                    PacketInformationDate = s.PacketInformationDate,
                    QtyForPacket = s.QtyForPacket,
                    TenderInformationId = s.TenderInformationId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                packetInformationLineModels.Add(packetInformationLineModel);
            });
            return packetInformationLineModels.OrderByDescending(a => a.PacketInformationId).ToList();
        }


        [HttpPost]
        [Route("InsertPacketInformationLine")]
        public PacketInformationLineModel Post(PacketInformationLineModel value)
        {
            var packetInformationLine = new PacketInformationLine
            {
                PacketInformationDate = value.PacketInformationDate,
                QtyForPacket = value.QtyForPacket,
                TenderInformationId = value.TenderInformationId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.PacketInformationLine.Add(packetInformationLine);
            _context.SaveChanges();
            value.PacketInformationId = packetInformationLine.PacketInformationId;
            return value;
        }
        [HttpPut]
        [Route("UpdatePacketInformationLine")]
        public PacketInformationLineModel Put(PacketInformationLineModel value)
        {
            var packetInformationLine = _context.PacketInformationLine.SingleOrDefault(p => p.PacketInformationId == value.PacketInformationId);
            packetInformationLine.PacketInformationDate = value.PacketInformationDate;
            packetInformationLine.QtyForPacket = value.QtyForPacket;
            packetInformationLine.TenderInformationId = value.TenderInformationId;
            packetInformationLine.StatusCodeId = value.StatusCodeID.Value;
            packetInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            packetInformationLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeletePacketInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var packetInformationLine = _context.PacketInformationLine.Where(p => p.PacketInformationId == id).FirstOrDefault();
                if (packetInformationLine != null)
                {
                    _context.PacketInformationLine.Remove(packetInformationLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
