﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AbbreviationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AbbreviationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetAbbreviations")]
        public List<ApplicationAbbreviationModel> Get()
        {
            var abbreviation = _context.ApplicationAbbreviation
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Deparment")
                .Select(s => new ApplicationAbbreviationModel
                {
                    ApplicationAbbreviationId=s.ApplicationAbbreviationId,
                    DeparmentId=s.DeparmentId,
                    DepartmentName=s.Deparment.Name,
                    Name = s.Name,
                    Description = s.Description,
                    Definition=s.Definition,

                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser=s.AddedByUser.UserName,
                    ModifiedByUser=s.ModifiedByUser.UserName,
                    StatusCode=s.StatusCode.CodeValue

                }).OrderByDescending(o => o.ApplicationAbbreviationId).AsNoTracking().ToList();
            return abbreviation;
        }
      
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Designation")]
        [HttpGet("GetAbbreviations/{id:int}")]
        public ActionResult<ApplicationAbbreviationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var abbreviations = _context.ApplicationAbbreviation.SingleOrDefault(p => p.ApplicationAbbreviationId == id.Value);
            var result = _mapper.Map<ApplicationAbbreviationModel>(abbreviations);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationAbbreviationModel> GetData(SearchModel searchModel)
        {
            var abbreviations = new ApplicationAbbreviation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).FirstOrDefault();
                        break;
                    case "Last":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).LastOrDefault();
                        break;
                    case "Next":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).LastOrDefault();
                        break;
                    case "Previous":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).FirstOrDefault();
                        break;
                    case "Last":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).LastOrDefault();
                        break;
                    case "Next":
                        abbreviations = _context.ApplicationAbbreviation.OrderBy(o => o.ApplicationAbbreviationId).FirstOrDefault(s => s.ApplicationAbbreviationId > searchModel.Id);
                        break;
                    case "Previous":
                        abbreviations = _context.ApplicationAbbreviation.OrderByDescending(o => o.ApplicationAbbreviationId).FirstOrDefault(s => s.ApplicationAbbreviationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationAbbreviationModel>(abbreviations);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAbbreviations")]
        public ApplicationAbbreviationModel Post(ApplicationAbbreviationModel value)
        {
            var abbreviations = new ApplicationAbbreviation
            {
                DeparmentId = value.DeparmentId,
                Definition = value.Definition,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID.Value,
                Name = value.Name,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.ApplicationAbbreviation.Add(abbreviations);
            _context.SaveChanges();
            value.ApplicationAbbreviationId = abbreviations.ApplicationAbbreviationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAbbreviations")]
        public ApplicationAbbreviationModel Put(ApplicationAbbreviationModel value)
        {
            var abbreviations = _context.ApplicationAbbreviation.SingleOrDefault(p => p.ApplicationAbbreviationId == value.ApplicationAbbreviationId);

            abbreviations.DeparmentId = value.DeparmentId;
            abbreviations.Definition = value.Definition;
            abbreviations.Description = value.Description;
            abbreviations.Name = value.Name;
            abbreviations.ModifiedByUserId = value.ModifiedByUserID;
            abbreviations.ModifiedDate = DateTime.Now;
            abbreviations.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAbbreviations")]
        public void Delete(int id)
        {
            var abbreviations = _context.ApplicationAbbreviation.SingleOrDefault(p => p.ApplicationAbbreviationId == id);
            if (abbreviations != null)
            {
                _context.ApplicationAbbreviation.Remove(abbreviations);
                _context.SaveChanges();
            }
        }
    }
}