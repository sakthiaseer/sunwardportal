﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AcceptableCalibrationInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AcceptableCalibrationInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAcceptableCalibrationInformation")]
        public List<AcceptableCalibrationInformationModel> Get()
        {
            //var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new AcceptableCalibrationInformationModel
            var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.Select(s => new AcceptableCalibrationInformationModel
            {
                AcceptableCalibrationInformationID = s.AcceptableCalibrationInformationId,
                Parameter = s.Parameter,
                StandardReference = s.StandardReference,
                Max = s.Max,
                Min = s.Min,
                UnitOfMeasure = s.UnitOfMeasure,
                Remarks = s.Remarks,
                CalibrationServiceInformationID = s.CalibrationServiceInformationId,

                //ModifiedByUserID = s.AddedByUserId,
                //AddedByUserID = s.ModifiedByUserId,
                //StatusCodeID = s.StatusCodeId,
                //AddedByUser = s.AddedByUser.UserName,
                //ModifiedByUser = s.ModifiedByUser.UserName,
                //AddedDate = s.AddedDate,
                //ModifiedDate = s.ModifiedDate,
                //StatusCode = s.StatusCode.CodeValue,


            }).OrderByDescending(o => o.AcceptableCalibrationInformationID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<AcceptableCalibrationInformationModel>>(AcceptableCalibrationInfo);
            return acceptableCalibrationInformation;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetAcceptableCalibrationInformation/{id:int}")]
        public ActionResult<AcceptableCalibrationInformationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.SingleOrDefault(p => p.AcceptableCalibrationInformationId == id.Value);
            var result = _mapper.Map<AcceptableCalibrationInformationModel>(acceptableCalibrationInformation);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AcceptableCalibrationInformationModel> GetData(SearchModel searchModel)
        {
            var acceptableCalibrationInformation = new AcceptableCalibrationInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).LastOrDefault();
                        break;
                    case "Next":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).LastOrDefault();
                        break;
                    case "Next":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderBy(o => o.AcceptableCalibrationInformationId).FirstOrDefault(s => s.AcceptableCalibrationInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.OrderByDescending(o => o.AcceptableCalibrationInformationId).FirstOrDefault(s => s.AcceptableCalibrationInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AcceptableCalibrationInformationModel>(acceptableCalibrationInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAcceptableCalibrationInformation")]
        public AcceptableCalibrationInformationModel Post(AcceptableCalibrationInformationModel value)
        {
            var acceptableCalibrationInformation = new AcceptableCalibrationInfo
            {
                Parameter = value.Parameter,
                StandardReference = value.StandardReference,
                Max = value.Max,
                Min = value.Min,
                UnitOfMeasure = value.UnitOfMeasure,
                Remarks = value.Remarks,
                CalibrationServiceInformationId = value.CalibrationServiceInformationID,

                //ContactActivities = new List<ContactActivities>(),

            };
            

            _context.AcceptableCalibrationInfo.Add(acceptableCalibrationInformation);
            _context.SaveChanges();
            value.AcceptableCalibrationInformationID = acceptableCalibrationInformation.AcceptableCalibrationInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAcceptableCalibrationInformation")]
        public AcceptableCalibrationInformationModel Put(AcceptableCalibrationInformationModel value)
        {
            var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.SingleOrDefault(p => p.AcceptableCalibrationInformationId == value.AcceptableCalibrationInformationID);

            acceptableCalibrationInformation.Parameter = value.Parameter;
            acceptableCalibrationInformation.StandardReference = value.StandardReference;
            acceptableCalibrationInformation.Max = value.Max;
            acceptableCalibrationInformation.Min = value.Min;
            acceptableCalibrationInformation.UnitOfMeasure = value.UnitOfMeasure;
            acceptableCalibrationInformation.Remarks = value.Remarks;
            acceptableCalibrationInformation.CalibrationServiceInformationId = value.CalibrationServiceInformationID;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAcceptableCalibrationInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var acceptableCalibrationInformation = _context.AcceptableCalibrationInfo.SingleOrDefault(p => p.AcceptableCalibrationInformationId == id);
                if (acceptableCalibrationInformation != null)
                {
                    _context.AcceptableCalibrationInfo.Remove(acceptableCalibrationInformation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}