﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DraftAppwikiController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public DraftAppwikiController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository, GenerateDocumentNoSeries generate, IConfiguration configuration, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
            _generateDocumentNoSeries = generate;
            _configuration = configuration;
            _hostingEnvironment = host;
        }

        [HttpGet]
        [Route("GetApplicationWikiVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<DraftApplicationWikiModel>>> GetApplicationWikiVersion(string sessionID)
        {
            return await _repository.GetList<DraftApplicationWikiModel>(sessionID);
        }
        [HttpGet]
        [Route("GetApplicationWikiLineVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<DraftApplicationWikiLineModel>>> GetApplicationWikiLineVersion(string sessionID)
        {
            return await _repository.GetList<DraftApplicationWikiLineModel>(sessionID);
        }
        [HttpPost]
        [Route("GetExcel")]
        public IActionResult GetExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Wiki Owner");
            headers.Add("Owner Dept");
            headers.Add("Wiki Type");
            headers.Add("Category");
            headers.Add("Topic");
            headers.Add("Profile ReferenceNo");
            headers.Add("Title");
            headers.Add("Version No");
            headers.Add("Status");
            headers.Add("Content");
            headers.Add("Translation");
            headers.Add("Effective Date");
            headers.Add("New Review Date");
            headers.Add("Preparation time/month");
            headers.Add("Modified By");
            headers.Add("Modified Date");
            string newFolderName = "WikiExcel";
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var SessionId = Guid.NewGuid();
            string FromLocation = folderName + @"\" + newFolderName + @"\" + SessionId + ".xlsx";

            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = "Draft Wiki" };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);
                Row headerRow = new Row();
                foreach (string column in headers)
                {
                    /*Cell cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.CellValue = new CellValue(column);
                    headerRow.AppendChild(cell);*/
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                var applicationWikiExcel = GetApplicationDraftWikiExcel(searchModel);
                applicationWikiExcel.ForEach(d =>
                {
                    Row row = new Row();
                    row.AppendChild(AddCellColumn(d.WikiOwner));
                    row.AppendChild(AddCellColumn(d.WikiOwnerName));
                    row.AppendChild(AddCellColumn(d.WikiType));
                    row.AppendChild(AddCellColumn(d.WikiCategory));
                    row.AppendChild(AddCellColumn(d.WikiTopic));
                    row.AppendChild(AddCellColumn(d.ProfileReferenceNo));
                    row.AppendChild(AddCellColumn(d.Title));
                    row.AppendChild(AddCellColumn(d.VersionNo));
                    row.AppendChild(AddCellColumn(d.StatusCode));
                    row.AppendChild(AddCellColumn(d.ContentInfo));
                    row.AppendChild(AddCellColumn(d.TranslationRequired));
                    row.AppendChild(AddCellColumn(d.EffectiveDate != null ? d.EffectiveDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.NewReviewDate != null ? d.NewReviewDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.EstimationPreparationTimeMonth != null && d.EstimationPreparationTimeMonth > 0 ? d.EstimationPreparationTimeMonth.ToString() : ""));
                    row.AppendChild(AddCellColumn(d.ModifiedByUser));
                    row.AppendChild(AddCellColumn(d.ModifiedDate != null ? d.ModifiedDate?.ToString("dd-MMM-yyyy hh:mm tt") : ""));
                    sheetData.AppendChild(row);
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        private Cell AddCellColumn(string value)
        {
            Cell fileNamecell = new Cell();
            fileNamecell.DataType = CellValues.String;
            fileNamecell.CellValue = new CellValue(value);
            return fileNamecell;
        }
        [HttpPost]
        [Route("GetApplicationDraftWikiExcel")]
        public List<DraftApplicationWikiModel> GetApplicationDraftWikiExcel(SearchModel searchModel)
        {
            List<DraftApplicationWikiModel> applicationWikiModels = new List<DraftApplicationWikiModel>();
            string sqlQuery = string.Empty;
            string search = "statusCodeID=840 ";
            if (searchModel.ApplicationWikiSearch != null)
            {
                if (searchModel.ApplicationWikiSearch.WikiTypeId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiTypeID =" + searchModel.ApplicationWikiSearch.WikiTypeId;
                }
                if (searchModel.ApplicationWikiSearch.WikiOwnerId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiOwnerID =" + searchModel.ApplicationWikiSearch.WikiOwnerId;
                }
                if (!String.IsNullOrEmpty(searchModel.ApplicationWikiSearch.Title))
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "Title like '%" + searchModel.ApplicationWikiSearch.Title + "%'";
                }
                var wikiIds = new List<long>();
                if (searchModel.ApplicationWikiSearch.WikiCategoryId != null)
                {
                    wikiIds.Add(-1);
                    wikiIds.AddRange(_context.AppWikiCategoryMultiple.Where(a => a.WikiCategoryId == searchModel.ApplicationWikiSearch.WikiCategoryId).AsNoTracking().Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).ToList());
                }
                if (searchModel.ApplicationWikiSearch.WikiTopicId != null)
                {
                    wikiIds.Add(-1);
                    wikiIds.AddRange(_context.AppWikiTopicMultiple.Where(a => a.WikiTopicId == searchModel.ApplicationWikiSearch.WikiTopicId).AsNoTracking().Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).ToList());
                }
                if (wikiIds != null && wikiIds.Count > 0)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "ApplicationWikiId in (" + string.Join(",", wikiIds.Distinct().ToList()) + ")";
                }
                if (searchModel.ApplicationWikiSearch.WikiOwnerTypeId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiOwnerTypeId =" + searchModel.ApplicationWikiSearch.WikiOwnerTypeId;
                    if (searchModel.ApplicationWikiSearch.PlantId != null)
                    {
                        search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "PlantId =" + searchModel.ApplicationWikiSearch.PlantId;
                    }
                    if (searchModel.ApplicationWikiSearch.EmployeeId != null)
                    {
                        search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "EmployeeId =" + searchModel.ApplicationWikiSearch.EmployeeId;
                    }
                }
                if (!String.IsNullOrEmpty(search))
                {
                    search = "WHERE " + search;
                }
            }
            sqlQuery = "Select  * from View_GetDraftApplicationWiki" + " " + search;
            var applicationWikis = _context.Set<View_GetDraftApplicationWiki>().FromSqlRaw(sqlQuery).AsQueryable().ToList();
            var applicationWiki = applicationWikis.ToList();
            var applicationWikiIds = applicationWiki.Select(s => s.ApplicationWikiId).ToList();
            var ApplicationWikiLineLists = _context.DraftApplicationWikiLine.Select(a => new { a.ApplicationWikiLineId, a.ApplicationWikiId }).Where(a => applicationWikiIds.Contains(a.ApplicationWikiId.Value)).ToList();
            var ApplicationWikiLineIds = ApplicationWikiLineLists.Select(s => s.ApplicationWikiLineId).ToList();
            var ApplicationWikiLineDutys = _context.DraftApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId }).Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).ToList();
            var ApplicationWikiLineDutyIds = ApplicationWikiLineDutys.Select(s => s.ApplicationWikiLineDutyId).ToList();
            var WikiResponsible = _context.DraftWikiResponsible.Include(e => e.Employee).Select(s => new { s.ApplicationWikiLineDutyId, s.WikiResponsibilityId, s.EmployeeId, UserId = s.Employee.UserId.Value }).Where(w => ApplicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId != null).ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 101, 102 };
            var appWikiTaskLink = _context.DraftAppWikiTaskLink.Select(n => new DraftAppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => applicationWikiIds.Contains(w.ApplicationWikiId.Value)).ToList();
            var applicationMasterChild = _context.ApplicationMasterChild.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterParentId)).AsNoTracking().ToList();
            applicationWikis.ToList().ForEach(s =>
            {
                DraftApplicationWikiModel applicationWikiModel = new DraftApplicationWikiModel();
                applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                applicationWikiModel.WikiDate = s.WikiDate;
                applicationWikiModel.WikiEntryById = s.WikiEntryById;
                applicationWikiModel.EntryBy = s.WikiEntryBy;
                applicationWikiModel.WikiTypeId = s.WikiTypeId;
                applicationWikiModel.WikiType = s.WikiType;
                applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                applicationWikiModel.WikiOwner = s.WikiOwner;
                applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                applicationWikiModel.WikiTopicId = s.WikiTopicId;
                applicationWikiModel.Title = s.Title;
                applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                applicationWikiModel.TranslationRequired = s.TranslationRequired;
                applicationWikiModel.Objective = s.Objective;
                applicationWikiModel.Scope = s.Scope;
                applicationWikiModel.PreRequisition = s.PreRequisition;
                applicationWikiModel.Content = s.Content;
                applicationWikiModel.VersionNo = s.VersionNo;
                applicationWikiModel.StatusCodeID = s.StatusCodeId;
                applicationWikiModel.StatusCode = s.StatusCode;
                applicationWikiModel.AddedByUser = s.AddedByUser;
                applicationWikiModel.AddedByUserID = s.AddedByUserId;
                applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                applicationWikiModel.ModifiedByUser = s.ModifiedByUser;
                applicationWikiModel.IsMalay = s.IsMalay;
                applicationWikiModel.IsChinese = s.IsChinese;
                applicationWikiModel.AddedDate = s.AddedDate;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                applicationWikiModel.DepartmentId = s.DepartmentId;
                applicationWikiModel.ProfileNo = s.ProfileNo;
                applicationWikiModel.DepartmentName = s.DepartmentName;
                applicationWikiModel.Description = s.Description;
                applicationWikiModel.Remarks = s.Remarks;
                applicationWikiModel.EffectiveDate = s.EffectiveDate;
                applicationWikiModel.NewReviewDate = s.NewReviewDate;
                applicationWikiModel.SessionId = s.SessionId;
                applicationWikiModel.IsEdit = false;
                applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                applicationWikiModel.ProfileId = s.ProfileId;
                applicationWikiModel.ProfileName = s.ProfileName;
                applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                applicationWikiModel.IsContentEntry = s.IsContentEntry;
                applicationWikiModel.TaskLink = s.TaskLink;
                applicationWikiModel.WikiCategoryIds = !String.IsNullOrEmpty(s.WikiCategoryIds) ? (s.WikiCategoryIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiTopicIds = !String.IsNullOrEmpty(s.WikiTopicIds) ? (s.WikiTopicIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiCategory = applicationWikiModel.WikiCategoryIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiCategoryIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.WikiTopic = applicationWikiModel.WikiTopicIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiTopicIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.ReleaseApplicationWikiId = s.ReleaseApplicationWikiId;
                applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                applicationWikiModel.PlantId = s.PlantId;
                applicationWikiModel.EmployeeId = s.EmployeeId;
                applicationWikiModel.ContentInfo = s.ContentInfo;
                applicationWikiModel.IsEdit = true;
                applicationWikiModel.IsEdits = true;
                applicationWikiModel.WikiOwnerName = s.WikiOwnerTypeId == 2401 ? (s.PlantName) : (s.FirstName);
                if (s.AddedByUserId == searchModel.UserID)
                {
                    applicationWikiModel.IsEdits = true;
                }
                else
                {
                    var ApplicationWikiLineList = ApplicationWikiLineLists.Where(a => a.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.ApplicationWikiLineId).ToList();
                    if (ApplicationWikiLineList != null)
                    {
                        var ApplicationWikiLineDutyList = ApplicationWikiLineDutys != null ? ApplicationWikiLineDutys.Where(w => ApplicationWikiLineList.Contains(w.ApplicationWikiLineId.Value)).Select(d => d.ApplicationWikiLineDutyId).ToList() : null;
                        var WikiResponsibleList = ApplicationWikiLineDutyList != null ? WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId == searchModel.UserID).ToList() : null;
                        applicationWikiModel.IsEdits = WikiResponsibleList != null && WikiResponsibleList.Count > 0 ? true : false;
                    }
                }
                applicationWikiModel.AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.ApplicationWikiId == s.ApplicationWikiId).ToList();
                applicationWikiModels.Add(applicationWikiModel);
            });
            return applicationWikiModels;
        }

        private List<DraftApplicationWikiModel> GetApplicationWikiList(int? userId, int? superUser)
        {
            var ApplicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.ToList();
            var WikiResponsible = _context.DraftWikiResponsible.Include(w => w.Employee).ToList();
            List<DraftApplicationWikiModel> applicationWikiModels = new List<DraftApplicationWikiModel>();
            var applicationWikis = _context.DraftApplicationWiki
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(t => t.TranslationRequired)
                                .Include(w => w.WikiEntryBy)
                                .Include(o => o.WikiOwner)
                                .Include(d => d.Department)
                                .Include(z => z.DraftApplicationWikiLine)
                                .Include(p => p.Profile)
                                .Include(c => c.DraftAppWikiCategoryMultiple)
                                .Include("DraftAppWikiCategoryMultiple.WikiCategory")
                                .Include(r => r.DraftAppWikiTopicMultiple)
                                .Include("DraftAppWikiTopicMultiple.WikiTopic")
                                .Include("DraftApplicationWikiLine.DraftApplicationWikiLineDuty")
                                 .Include("DraftApplicationWikiLine.DraftApplicationWikiLineDuty.DraftWikiResponsible")
                                 .Where(w => w.StatusCodeId == 840)
                                .OrderByDescending(o => o.ApplicationWikiId).AsNoTracking().ToList();
            if (applicationWikis != null)
            {
                List<long?> masterIds = applicationWikis.Where(w => w.WikiTypeId != null).Select(a => a.WikiTypeId).Distinct().ToList();
                masterIds.AddRange(applicationWikis.Where(w => w.WikiCategoryId != null).Select(a => a.WikiCategoryId).Distinct().ToList());
                masterIds.AddRange(applicationWikis.Where(w => w.WikiTopicId != null).Select(a => a.WikiTopicId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                applicationWikis.ForEach(s =>
                {
                    DraftApplicationWikiModel applicationWikiModel = new DraftApplicationWikiModel();
                    applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                    applicationWikiModel.WikiDate = s.WikiDate;
                    applicationWikiModel.WikiEntryById = s.WikiEntryById;
                    applicationWikiModel.EntryBy = s.WikiEntryBy != null ? s.WikiEntryBy.UserName : string.Empty;
                    applicationWikiModel.WikiTypeId = s.WikiTypeId;
                    applicationWikiModel.WikiType = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTypeId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTypeId).Value : string.Empty;
                    applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                    applicationWikiModel.WikiOwner = s.WikiOwner != null ? s.WikiOwner.Name : string.Empty;
                    applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                    applicationWikiModel.WikiCategory = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId).Value : string.Empty;
                    applicationWikiModel.WikiTopicId = s.WikiTopicId;
                    applicationWikiModel.WikiTopic = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId).Value : string.Empty;
                    applicationWikiModel.Title = s.Title;
                    applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                    applicationWikiModel.TranslationRequired = s.TranslationRequired != null ? s.TranslationRequired.CodeValue : string.Empty;
                    applicationWikiModel.Objective = s.Objective;
                    applicationWikiModel.Scope = s.Scope;
                    applicationWikiModel.PreRequisition = s.PreRequisition;
                    applicationWikiModel.Content = s.Content;
                    applicationWikiModel.VersionNo = s.VersionNo;
                    applicationWikiModel.StatusCodeID = s.StatusCodeId;
                    applicationWikiModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty;
                    applicationWikiModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty;
                    applicationWikiModel.AddedByUserID = s.AddedByUserId;
                    applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationWikiModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : string.Empty;
                    applicationWikiModel.IsMalay = s.IsMalay;
                    applicationWikiModel.IsChinese = s.IsChinese;
                    applicationWikiModel.ModifiedDate = s.ModifiedDate;
                    applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                    applicationWikiModel.DepartmentId = s.DepartmentId;
                    applicationWikiModel.ProfileNo = s.ProfileNo;
                    applicationWikiModel.DepartmentName = s.Department?.Name;
                    applicationWikiModel.Description = s.Description;
                    applicationWikiModel.Remarks = s.Remarks;
                    applicationWikiModel.EffectiveDate = s.EffectiveDate;
                    applicationWikiModel.NewReviewDate = s.NewReviewDate;
                    applicationWikiModel.SessionId = s.SessionId;
                    applicationWikiModel.IsEdit = false;
                    applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                    applicationWikiModel.ProfileId = s.ProfileId;
                    applicationWikiModel.ProfileName = s.Profile != null ? s.Profile.Name : "";
                    applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                    applicationWikiModel.IsContentEntry = s.IsContentEntry;
                    applicationWikiModel.TaskLink = s.TaskLink;
                    applicationWikiModel.WikiCategoryIds = s.DraftAppWikiCategoryMultiple != null ? s.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList() : new List<long>();
                    applicationWikiModel.WikiTopicIds = s.DraftAppWikiTopicMultiple != null ? s.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList() : new List<long>();
                    applicationWikiModel.WikiCategory = s.DraftAppWikiCategoryMultiple != null ? string.Join(",", s.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiCategory.Value).ToList()) : "";
                    applicationWikiModel.WikiTopic = s.DraftAppWikiTopicMultiple != null ? string.Join(",", s.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiTopic.Value).ToList()) : "";
                    applicationWikiModel.ReleaseApplicationWikiId = s.ReleaseApplicationWikiId;
                    applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                    applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                    applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                    applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                    applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                    applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                    applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                    applicationWikiModel.PlantId = s.PlantId;
                    applicationWikiModel.EmployeeId = s.EmployeeId;
                    applicationWikiModel.ContentInfo = s.ContentInfo;
                    if (userId != superUser)
                    {
                        if (s.AddedByUserId == userId)
                        {
                            applicationWikiModel.IsEdit = true;
                        }
                        else
                        {
                            if (s.DraftApplicationWikiLine != null)
                            {
                                var ApplicationWikiLineList = s.DraftApplicationWikiLine.Where(a => a.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.ApplicationWikiLineId).ToList();
                                var ApplicationWikiLineDutyList = ApplicationWikiLineDuty != null ? ApplicationWikiLineDuty.Where(w => ApplicationWikiLineList.Contains(w.ApplicationWikiLineId.Value)).Select(d => d.ApplicationWikiLineDutyId).ToList() : null;
                                var WikiResponsibleList = ApplicationWikiLineDutyList != null ? WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value) && w.Employee?.UserId == userId).ToList() : null;
                                applicationWikiModel.IsEdit = WikiResponsibleList != null && WikiResponsibleList.Count > 0 ? true : false;
                            }
                        }
                    }
                    else
                    {
                        applicationWikiModel.IsEdit = true;
                    }
                    applicationWikiModels.Add(applicationWikiModel);
                });
            }
            return applicationWikiModels;
        }
        [HttpGet]
        [Route("GetApplicationWiki")]
        public List<DraftApplicationWikiModel> Get(int? userId, int? superUser)
        {
            List<DraftApplicationWikiModel> applicationWikiModels = new List<DraftApplicationWikiModel>();
            var getApplicationWikiLists = GetApplicationWikiList(userId, superUser);
            return getApplicationWikiLists;
        }
        [HttpGet]
        [Route("GetApplicationWikiLine")]
        public List<DraftApplicationWikiLineModel> ApplicationWikiLine(long id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.DraftApplicationWikiLine
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("FunctionLinkNavigation")
                                .Include("PageLinkNavigation")
                                .Include(a => a.DraftApplicationWikiWeekly)
                                .Include(b => b.DraftApplicationWikiLineNotify)
                                .Where(w => w.ApplicationWikiId == id).OrderByDescending(o => o.ApplicationWikiId).AsNoTracking().ToList();
            List<DraftApplicationWikiLineModel> applicationWikiLineModels = new List<DraftApplicationWikiLineModel>();
            applicationWikis.ForEach(s =>
            {
                DraftApplicationWikiLineModel applicationWikiLineModel = new DraftApplicationWikiLineModel
                {
                    ApplicationWikiLineId = s.ApplicationWikiLineId,
                    ApplicationWikiId = s.ApplicationWikiId,
                    DutyId = s.DutyId,
                    DutyName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId).Value : string.Empty,
                    Responsibility = s.Responsibility,
                    PageLink = s.PageLink,
                    PageLinkName = s.PageLinkNavigation?.PermissionName,
                    FunctionLink = s.FunctionLink,
                    FunctionLinkName = s.FunctionLinkNavigation?.PermissionName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    NotificationAdvice = s.NotificationAdvice,
                    NotificationAdviceFlag = s.NotificationAdvice == true ? "Yes" : "No",
                    NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                    CustomId = s.CustomId,
                    RepeatId = s.RepeatId,
                    DueDate = s.DueDate,
                    Monthly = s.Monthly,
                    Yearly = s.Yearly,
                    EventDescription = s.EventDescription,
                    DaysOfWeek = s.DaysOfWeek,
                    SessionId = s.SessionId,
                    NotificationStatusId = s.NotificationStatusId,
                    ScreenId = s.ScreenId,
                    Title = s.Title,
                    Message = s.Message,
                    NotifyEndDate = s.NotifyEndDate,
                    IsAllowDocAccess=s.IsAllowDocAccess,
                    ApplicationWikiLineDutys = ApplicationWikiLineDuty(s.ApplicationWikiLineId),
                    ApplicationWikiRecurrences = ApplicationWikiRecurrence(s.ApplicationWikiLineId),
                    NotifyIds = s.DraftApplicationWikiLineNotify != null ? s.DraftApplicationWikiLineNotify.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId).Select(c => c.NotifyUserId).ToList() : new List<long?>(),
                    WeeklyIds = s.DraftApplicationWikiWeekly != null ? s.DraftApplicationWikiWeekly.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    DaysOfWeekIds = s.DraftApplicationWikiWeekly != null ? s.DraftApplicationWikiWeekly.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),

                };
                applicationWikiLineModels.Add(applicationWikiLineModel);
            });

            return applicationWikiLineModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiRecurrence")]
        public List<DraftApplicationWikiRecurrenceModel> ApplicationWikiRecurrence(long id)
        {
            var ApplicationWikiRecurrence = _context.DraftApplicationWikiRecurrence
                 .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Type")
                .Include("OccurenceOption")
               .Select(s => new DraftApplicationWikiRecurrenceModel
               {
                   ApplicationWikiRecurrenceId = s.ApplicationWikiRecurrenceId,
                   ApplicationWikiLineId = s.ApplicationWikiLineId,
                   TypeId = s.TypeId,
                   RepeatNos = s.RepeatNos,
                   OccurenceOptionId = s.OccurenceOptionId,
                   NoOfOccurences = s.NoOfOccurences,
                   Sunday = s.Sunday,
                   Monday = s.Monday,
                   Tuesday = s.Tuesday,
                   Wednesday = s.Wednesday,
                   Thursday = s.Thursday,
                   Friday = s.Friday,
                   Saturyday = s.Saturyday,
                   StartDate = s.StartDate,
                   EndDate = s.EndDate,
                   StatusCodeID = s.StatusCodeId,
                   AddedByUserID = s.AddedByUserId,
                   ModifiedByUserID = s.ModifiedByUserId,
                   AddedDate = s.AddedDate,
                   ModifiedDate = s.ModifiedDate,
                   AddedByUser = s.AddedByUser.UserName,
                   ModifiedByUser = s.ModifiedByUser.UserName,
                   StatusCode = s.StatusCode.CodeValue,
                   TypeName = s.Type.CodeValue,
                   OccurenceOptionName = s.OccurenceOption.CodeValue,
               }).Where(w => w.ApplicationWikiLineId == id).OrderByDescending(o => o.ApplicationWikiRecurrenceId).AsNoTracking().ToList();
            return ApplicationWikiRecurrence;
        }
        [HttpGet]
        [Route("GetApplicationWikiLineDuty")]
        public List<DraftApplicationWikiLineDutyModel> ApplicationWikiLineDuty(long id)
        {
            List<DraftApplicationWikiLineDutyModel> applicationWikiLineDutyModels = new List<DraftApplicationWikiLineDutyModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.DraftApplicationWikiLineDuty
                                .Include("Departmant")
                                .Include("Designation")
                                .Include("Company")
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("Employee")
                                .Include(e => e.DraftWikiResponsible)
                                .Where(w => w.ApplicationWikiLineId == id).OrderByDescending(o => o.ApplicationWikiLineDutyId).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                DraftApplicationWikiLineDutyModel applicationWikiLineDutyModel = new DraftApplicationWikiLineDutyModel
                {
                    ApplicationWikiLineId = s.ApplicationWikiLineId,
                    ApplicationWikiLineDutyId = s.ApplicationWikiLineDutyId,
                    DepartmentId = s.DepartmantId,
                    EmployeeId = s.EmployeeId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Departmant?.Name,
                    DesignationNumber = s.DesignationNumber,
                    CompanyId = s.CompanyId,
                    PlantCompanyName = s.Company?.Description,
                    DesignationId = s.DesignationId,
                    DesignationName = s.Designation?.Name,
                    DutyNo = s.DutyNo,
                    DutyNoName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    Type = s.Type,
                    //EmployeeIDs = s.DraftWikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.EmployeeId).ToList(),
                    //UserGroupIDs = s.DraftWikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.UserGroupId).Distinct().ToList(),
                };
                applicationWikiLineDutyModels.Add(applicationWikiLineDutyModel);
            });

            return applicationWikiLineDutyModels;
        }
        // GET: api/Project/2
        [HttpGet]
        [Route("GetApplicationWikiByID")]
        public List<DraftApplicationWikiModel> Get(int? id)
        {
            List<DraftApplicationWikiModel> applicationWikiModels = new List<DraftApplicationWikiModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.DraftApplicationWiki
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(t => t.TranslationRequired)
                                .Include(w => w.WikiEntryBy)
                                .Include(o => o.WikiOwner)
                                .Include(d => d.Department)
                                .Where(s => s.ApplicationWikiId == id.Value).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                DraftApplicationWikiModel applicationWikiModel = new DraftApplicationWikiModel
                {
                    ApplicationWikiId = s.ApplicationWikiId,
                    WikiDate = s.WikiDate,
                    WikiEntryById = s.WikiEntryById,
                    EntryBy = s.WikiEntryBy != null ? s.WikiEntryBy.UserName : string.Empty,
                    WikiTypeId = s.WikiTypeId,
                    WikiType = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTypeId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTypeId).Value : string.Empty,
                    WikiOwnerId = s.WikiOwnerId,
                    WikiOwner = s.WikiOwner != null ? s.WikiOwner.Name : string.Empty,
                    WikiCategoryId = s.WikiCategoryId,
                    WikiCategory = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId).Value : string.Empty,
                    WikiTopicId = s.WikiTopicId,
                    WikiTopic = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId).Value : string.Empty,
                    Title = s.Title,
                    TranslationRequiredId = s.TranslationRequiredId,
                    TranslationRequired = s.TranslationRequired != null ? s.TranslationRequired.CodeValue : string.Empty,
                    Objective = s.Objective,
                    Scope = s.Scope,
                    PreRequisition = s.PreRequisition,
                    Content = s.Content,
                    VersionNo = s.VersionNo,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : string.Empty,
                    ModifiedDate = s.ModifiedDate,
                    IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value)),
                    DepartmentId = s.DepartmentId,
                    ProfileNo = s.ProfileNo,
                    DepartmentName = s.Department.Name,
                    Description = s.Description,
                    Remarks = s.Remarks,
                    NewReviewDate = s.NewReviewDate,
                    EffectiveDate = s.EffectiveDate,
                    SessionId = s.SessionId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth,
                    WikiOwnerTypeId = s.WikiOwnerTypeId,
                    PlantId = s.PlantId,
                    EmployeeId = s.EmployeeId,
                    ContentInfo=s.ContentInfo,
                };
                applicationWikiModels.Add(applicationWikiModel);
            });

            return applicationWikiModels;
        }
        [HttpPut]
        [Route("DraftUpdateApplicationWiki")]
        public async Task<DraftApplicationWikiModel> DraftUpdateApplicationWiki(DraftApplicationWikiModel value)
        {
            var applicationWiki = _context.DraftApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            //value.SessionId ??= Guid.NewGuid();
            value.ProfileReferenceNo ??= _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID });
            if (applicationWiki != null)
            {
                applicationWiki.WikiDate = value.WikiDate;
                applicationWiki.WikiEntryById = value.WikiEntryById;
                applicationWiki.WikiTypeId = value.WikiTypeId;
                applicationWiki.WikiOwnerId = value.WikiOwnerId;
                applicationWiki.WikiCategoryId = value.WikiCategoryId;
                applicationWiki.WikiTopicId = value.WikiTopicId;
                applicationWiki.Title = value.Title;
                applicationWiki.TranslationRequiredId = value.TranslationRequiredId;
                applicationWiki.Objective = value.Objective;
                applicationWiki.Scope = value.Scope;
                applicationWiki.PreRequisition = value.PreRequisition;
                applicationWiki.Content = value.Content;
                applicationWiki.ProfileReferenceNo = value.ProfileReferenceNo;
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.EstimationPreparationTimeMonth = value.EstimationPreparationTimeMonth;
                applicationWiki.WikiOwnerTypeId = value.WikiOwnerTypeId;
                applicationWiki.PlantId = value.PlantId;
                applicationWiki.EmployeeId = value.EmployeeId;
                applicationWiki.ContentInfo = value.ContentInfo;
                if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
                {
                    applicationWiki.IsMalay = value.IsMalay;
                    applicationWiki.IsChinese = value.IsChinese;
                }
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.StatusCodeId = value.StatusCodeID.Value;
                applicationWiki.ModifiedByUserId = value.ModifiedByUserID;
                applicationWiki.ModifiedDate = DateTime.Now;
                applicationWiki.DepartmentId = value.DepartmentId;
                applicationWiki.Description = value.Description;
                applicationWiki.Remarks = value.Remarks;
                applicationWiki.EffectiveDate = value.EffectiveDate;
                applicationWiki.NewReviewDate = value.NewReviewDate;
                applicationWiki.ProfileId = value.ProfileId;
                applicationWiki.TaskLink = value.TaskLink;
                applicationWiki.IsContentEntry = value.IsContentEntryFlag == "Yes" ? true : false;
                applicationWiki.ProfilePlantId = value.ProfilePlantId;
                applicationWiki.ProfileDepartmentId = value.ProfileDepartmentId;
                applicationWiki.ProfileDivisionId = value.ProfileDivisionId;
                applicationWiki.ProfileSectionId = value.ProfileSectionId;
                applicationWiki.ProfileSubSectionId = value.ProfileSubSectionId;
                _context.SaveChanges();
                if (value.StatusCodeID == 841)
                {
                    var appWikiDraftDoc = _context.AppWikiDraftDoc.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                    if (appWikiDraftDoc.Count > 0)
                    {
                        _context.AppWikiDraftDoc.RemoveRange(appWikiDraftDoc);
                        _context.SaveChanges();
                    }
                    DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
                    var documentsBySession = itemClassificationController.GetDocumentsBySessionID(value.SessionId, (int)value.AddedByUserID, "Draft");
                    if (documentsBySession != null && documentsBySession.Count > 0)
                    {
                        documentsBySession.ForEach(d =>
                        {
                            var docus = new AppWikiDraftDoc
                            {
                                DocumentId = d.DocumentID,
                                ApplicationWikiId = value.ApplicationWikiId,
                            };
                            _context.AppWikiDraftDoc.Add(docus);
                            _context.SaveChanges();
                            var query = string.Format("Update Documents Set IsWikiDraft = null Where DocumentId in" + '(' + "{0}" + ')', d.DocumentID);
                            _context.Database.ExecuteSqlRaw(query);
                        });
                    }
                }
                var AppWikiCategoryMultipleRemove = _context.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiCategoryMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                    _context.SaveChanges();
                }
                var AppWikiTopicMultipleRemove = _context.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiTopicMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                    _context.SaveChanges();
                }
                var draftAppWikiTaskLinkRemove = _context.DraftAppWikiTaskLink.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (draftAppWikiTaskLinkRemove.Count > 0)
                {
                    _context.DraftAppWikiTaskLink.RemoveRange(draftAppWikiTaskLinkRemove);
                    _context.SaveChanges();
                }
                if (value.WikiCategoryIds.Count > 0)
                {
                    value.WikiCategoryIds.ForEach(h =>
                    {
                        DraftAppWikiCategoryMultiple AppWikiCategoryMultiple = new DraftAppWikiCategoryMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiCategoryId = h,
                        };
                        _context.DraftAppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                    });
                    _context.SaveChanges();
                }
                if (value.AppWikiTaskLinkModel!=null && value.AppWikiTaskLinkModel.Count > 0)
                {
                    value.AppWikiTaskLinkModel.ForEach(h =>
                    {
                        DraftAppWikiTaskLink draftAppWikiTaskLink = new DraftAppWikiTaskLink
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            TaskLink = h.TaskLink,
                            Subject=h.Subject,
                        };
                        _context.DraftAppWikiTaskLink.Add(draftAppWikiTaskLink);
                    });
                    _context.SaveChanges();
                }
                if (value.WikiTopicIds.Count > 0)
                {
                    value.WikiTopicIds.ForEach(h =>
                    {
                        DraftAppWikiTopicMultiple appWikiTopicMultiple = new DraftAppWikiTopicMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiTopicId = h,
                        };
                        _context.DraftAppWikiTopicMultiple.Add(appWikiTopicMultiple);
                    });
                    _context.SaveChanges();
                }
               
            }
            return value;

        }
        [HttpPut]
        [Route("DraftUpdateApplicationWikiUpdate")]
        public async Task<DraftApplicationWikiModel> DraftUpdateApplicationWikiUpdate(DraftApplicationWikiModel value)
        {
            var applicationWiki = _context.DraftApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            //value.SessionId ??= Guid.NewGuid();
            value.ProfileReferenceNo ??= _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID });
            if (applicationWiki != null)
            {
                applicationWiki.WikiDate = value.WikiDate;
                applicationWiki.WikiEntryById = value.WikiEntryById;
                applicationWiki.WikiTypeId = value.WikiTypeId;
                applicationWiki.WikiOwnerId = value.WikiOwnerId;
                applicationWiki.WikiCategoryId = value.WikiCategoryId;
                applicationWiki.WikiTopicId = value.WikiTopicId;
                applicationWiki.Title = value.Title;
                applicationWiki.TranslationRequiredId = value.TranslationRequiredId;
                applicationWiki.Objective = value.Objective;
                applicationWiki.Scope = value.Scope;
                applicationWiki.PreRequisition = value.PreRequisition;
                applicationWiki.Content = value.Content;
                applicationWiki.ProfileReferenceNo = value.ProfileReferenceNo;
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.EstimationPreparationTimeMonth = value.EstimationPreparationTimeMonth;
                applicationWiki.WikiOwnerTypeId = value.WikiOwnerTypeId;
                applicationWiki.PlantId = value.PlantId;
                applicationWiki.EmployeeId = value.EmployeeId;
                applicationWiki.ContentInfo = value.ContentInfo;
                if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
                {
                    applicationWiki.IsMalay = value.IsMalay;
                    applicationWiki.IsChinese = value.IsChinese;
                }
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.StatusCodeId = 2;
                applicationWiki.ModifiedByUserId = value.ModifiedByUserID;
                applicationWiki.ModifiedDate = DateTime.Now;
                applicationWiki.DepartmentId = value.DepartmentId;
                applicationWiki.Description = value.Description;
                applicationWiki.Remarks = value.Remarks;
                applicationWiki.EffectiveDate = value.EffectiveDate;
                applicationWiki.NewReviewDate = value.NewReviewDate;
                applicationWiki.ProfileId = value.ProfileId;
                applicationWiki.TaskLink = value.TaskLink;
                applicationWiki.IsContentEntry = value.IsContentEntryFlag == "Yes" ? true : false;
                applicationWiki.ProfilePlantId = value.ProfilePlantId;
                applicationWiki.ProfileDepartmentId = value.ProfileDepartmentId;
                applicationWiki.ProfileDivisionId = value.ProfileDivisionId;
                applicationWiki.ProfileSectionId = value.ProfileSectionId;
                applicationWiki.ProfileSubSectionId = value.ProfileSubSectionId;
                _context.SaveChanges();
                if (value.StatusCodeID == 841)
                {
                    var appWikiDraftDoc = _context.AppWikiDraftDoc.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                    if (appWikiDraftDoc.Count > 0)
                    {
                        _context.AppWikiDraftDoc.RemoveRange(appWikiDraftDoc);
                        _context.SaveChanges();
                    }
                    DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
                    var documentsBySession = itemClassificationController.GetDocumentsBySessionID(value.SessionId, (int)value.AddedByUserID, "Draft");
                    if (documentsBySession != null && documentsBySession.Count > 0)
                    {
                        documentsBySession.ForEach(d =>
                        {
                            var docus = new AppWikiDraftDoc
                            {
                                DocumentId = d.DocumentID,
                                ApplicationWikiId = value.ApplicationWikiId,
                            };
                            _context.AppWikiDraftDoc.Add(docus);
                            _context.SaveChanges();
                            var query = string.Format("Update Documents Set IsWikiDraft = null Where DocumentId in" + '(' + "{0}" + ')', d.DocumentID);
                            _context.Database.ExecuteSqlRaw(query);
                        });
                    }
                }
                var AppWikiCategoryMultipleRemove = _context.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiCategoryMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                    _context.SaveChanges();
                }
                var AppWikiTopicMultipleRemove = _context.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiTopicMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                    _context.SaveChanges();
                }
                var draftAppWikiTaskLinkRemove = _context.DraftAppWikiTaskLink.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (draftAppWikiTaskLinkRemove.Count > 0)
                {
                    _context.DraftAppWikiTaskLink.RemoveRange(draftAppWikiTaskLinkRemove);
                    _context.SaveChanges();
                }
                if (value.WikiCategoryIds.Count > 0)
                {
                    value.WikiCategoryIds.ForEach(h =>
                    {
                        DraftAppWikiCategoryMultiple AppWikiCategoryMultiple = new DraftAppWikiCategoryMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiCategoryId = h,
                        };
                        _context.DraftAppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                    });
                    _context.SaveChanges();
                }
                if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
                {
                    value.AppWikiTaskLinkModel.ForEach(h =>
                    {
                        DraftAppWikiTaskLink draftAppWikiTaskLink = new DraftAppWikiTaskLink
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            TaskLink = h.TaskLink,
                            Subject = h.Subject,
                        };
                        _context.DraftAppWikiTaskLink.Add(draftAppWikiTaskLink);
                    });
                    _context.SaveChanges();
                }
                if (value.WikiTopicIds.Count > 0)
                {
                    value.WikiTopicIds.ForEach(h =>
                    {
                        DraftAppWikiTopicMultiple appWikiTopicMultiple = new DraftAppWikiTopicMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiTopicId = h,
                        };
                        _context.DraftAppWikiTopicMultiple.Add(appWikiTopicMultiple);
                    });
                    _context.SaveChanges();
                }

            }
            return value;

        }
        [HttpPost]
        [Route("InsertReleaseApplicationWiki")]
        public async Task<DraftApplicationWikiModel> InsertReleaseApplicationWiki(DraftApplicationWikiModel value)
        {
            DraftUpdateApplicationWikiUpdate(value);
            var applicationWikis = _context.DraftApplicationWiki.Include(a=>a.DraftAppWikiTaskLink).Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            
            if (applicationWikis != null)
            {
                var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == applicationWikis.ReleaseApplicationWikiId);
                if (applicationWiki != null)
                {
                    var AppWikiCategoryMultipleRemove = _context.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (AppWikiCategoryMultipleRemove.Count > 0)
                    {
                        _context.AppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                        _context.SaveChanges();
                    }
                    var AppWikiTopicMultipleRemove = _context.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (AppWikiTopicMultipleRemove.Count > 0)
                    {
                        _context.AppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                        _context.SaveChanges();
                    }
                    var appWikiTaskLinkRemove = _context.AppWikiTaskLink.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                    if (appWikiTaskLinkRemove.Count > 0)
                    {
                        _context.AppWikiTaskLink.RemoveRange(appWikiTaskLinkRemove);
                        _context.SaveChanges();
                    }
                    var ApplicationWikiLine = _context.ApplicationWikiLine.Where(p => p.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (ApplicationWikiLine != null)
                    {
                        ApplicationWikiLine.ForEach(c =>
                        {
                            var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                            if (ApplicationWikiRecurrence != null)
                            {
                                _context.ApplicationWikiRecurrence.RemoveRange(ApplicationWikiRecurrence);
                                _context.SaveChanges();
                            }
                            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                            if (ApplicationWikiLineDuty != null)
                            {
                                ApplicationWikiLineDuty.ForEach(d =>
                                {
                                    var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == d.ApplicationWikiLineDutyId).ToList();
                                    if (wikiresponsible != null)
                                    {
                                        _context.WikiResponsible.RemoveRange(wikiresponsible);
                                        _context.SaveChanges();
                                    }
                                });
                                _context.ApplicationWikiLineDuty.RemoveRange(ApplicationWikiLineDuty);
                                _context.SaveChanges();
                            }
                            var ApplicationWikiWeekly = _context.ApplicationWikiWeekly.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                            if (ApplicationWikiWeekly != null)
                            {
                                _context.ApplicationWikiWeekly.RemoveRange(ApplicationWikiWeekly);
                                _context.SaveChanges();
                            }
                            var notificationHandler = _context.NotificationHandler.Where(w => w.SessionId == c.SessionId).ToList();
                            if (notificationHandler != null)
                            {
                                _context.NotificationHandler.RemoveRange(notificationHandler);
                                _context.SaveChanges();
                            }
                            var ApplicationWikiLineNotify = _context.ApplicationWikiLineNotify.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                            if (ApplicationWikiLineNotify != null)
                            {
                                _context.ApplicationWikiLineNotify.RemoveRange(ApplicationWikiLineNotify);
                                _context.SaveChanges();
                            }
                        });
                        _context.ApplicationWikiLine.RemoveRange(ApplicationWikiLine);
                        _context.SaveChanges();
                        var appWikiReleaseDoc = _context.AppWikiReleaseDoc.Where(p => p.ApplicationWikiId==applicationWiki.ApplicationWikiId).ToList();
                        if (appWikiReleaseDoc != null)
                        {
                            _context.AppWikiReleaseDoc.RemoveRange(appWikiReleaseDoc);
                            _context.SaveChanges();
                        }
                    }
                    applicationWiki.WikiDate = applicationWikis.WikiDate;
                    applicationWiki.WikiEntryById = applicationWikis.WikiEntryById;
                    applicationWiki.WikiTypeId = applicationWikis.WikiTypeId;
                    applicationWiki.WikiOwnerId = applicationWikis.WikiOwnerId;
                    applicationWiki.WikiCategoryId = applicationWikis.WikiCategoryId;
                    applicationWiki.WikiTopicId = applicationWikis.WikiTopicId;
                    applicationWiki.Title = applicationWikis.Title;
                    applicationWiki.TranslationRequiredId = applicationWikis.TranslationRequiredId;
                    applicationWiki.Objective = applicationWikis.Objective;
                    applicationWiki.Scope = applicationWikis.Scope;
                    applicationWiki.PreRequisition = applicationWikis.PreRequisition;
                    applicationWiki.Content = applicationWikis.Content;
                    applicationWiki.StatusCodeId = 841;
                    applicationWiki.ModifiedByUserId = value.AddedByUserID;
                    applicationWiki.ModifiedDate = DateTime.Now;
                    applicationWiki.DepartmentId = applicationWikis.DepartmentId;
                    applicationWiki.Description = applicationWikis.Description;
                    applicationWiki.Remarks = applicationWikis.Remarks;
                    applicationWiki.EffectiveDate = applicationWikis.EffectiveDate;
                    applicationWiki.NewReviewDate = applicationWikis.NewReviewDate;
                    applicationWiki.VersionNo = value.VersionNo;
                    applicationWiki.SessionId = applicationWiki.SessionId;
                    applicationWiki.ProfileReferenceNo = applicationWikis.ProfileReferenceNo;
                    applicationWiki.ProfileId = applicationWikis.ProfileId;
                    applicationWiki.TaskLink = applicationWikis.TaskLink;
                    applicationWiki.IsContentEntry = applicationWikis.IsContentEntry;
                    applicationWiki.IsMalay = applicationWikis.IsMalay;
                    applicationWiki.IsChinese = applicationWikis.IsChinese;
                    applicationWiki.ProfilePlantId = applicationWikis.ProfilePlantId;
                    applicationWiki.ProfileDepartmentId = applicationWikis.ProfileDepartmentId;
                    applicationWiki.ProfileDivisionId = applicationWikis.ProfileDivisionId;
                    applicationWiki.ProfileSectionId = applicationWikis.ProfileSectionId;
                    applicationWiki.ProfileSubSectionId = applicationWikis.ProfileSubSectionId;
                    applicationWiki.EstimationPreparationTimeMonth = applicationWikis.EstimationPreparationTimeMonth;
                    applicationWiki.WikiOwnerTypeId = applicationWikis.WikiOwnerTypeId;
                    applicationWiki.PlantId = applicationWikis.PlantId;
                    applicationWiki.EmployeeId = applicationWikis.EmployeeId;
                    applicationWiki.ContentInfo = applicationWikis.ContentInfo;
                    if (applicationWikis.DraftAppWikiCategoryMultiple != null)
                    {
                        applicationWikis.DraftAppWikiCategoryMultiple.ToList().ForEach(h =>
                        {
                            AppWikiCategoryMultiple AppWikiCategoryMultiple = new AppWikiCategoryMultiple
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                WikiCategoryId = h.WikiCategoryId,
                            };
                            _context.AppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                        });
                    }
                    if (applicationWikis.DraftAppWikiTopicMultiple != null)
                    {
                        applicationWikis.DraftAppWikiTopicMultiple.ToList().ForEach(h =>
                        {
                            AppWikiTopicMultiple appWikiTopicMultiple = new AppWikiTopicMultiple
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                WikiTopicId = h.WikiTopicId,
                            };
                            _context.AppWikiTopicMultiple.Add(appWikiTopicMultiple);
                        });
                    }
                    if (applicationWikis.DraftAppWikiTaskLink != null && applicationWikis.DraftAppWikiTaskLink.Count > 0)
                    {
                        applicationWikis.DraftAppWikiTaskLink.ToList().ForEach(h =>
                        {
                            AppWikiTaskLink appWikiTaskLink = new AppWikiTaskLink
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                TaskLink = h.TaskLink,
                                Subject=h.Subject
                            };
                            _context.AppWikiTaskLink.Add(appWikiTaskLink);
                        });
                    }
                }
                var ApplicationWikiLines = _context.DraftApplicationWikiLine.Include(a => a.DraftApplicationWikiRecurrence).
                    Include(b => b.DraftApplicationWikiWeekly).
                    Include(c => c.DraftApplicationWikiLineDuty).
                    Include(d => d.DraftApplicationWikiLineNotify).
                    Where(w => w.ApplicationWikiId == applicationWikis.ApplicationWikiId).ToList();
                if (ApplicationWikiLines != null)
                {
                    ApplicationWikiLines.ForEach(ec =>
                    {
                        var ApplicationWikiLine = new ApplicationWikiLine
                        {
                            IsAllowDocAccess=ec.IsAllowDocAccess,
                            DutyId = ec.DutyId,
                            Responsibility = ec.Responsibility,
                            FunctionLink = ec.FunctionLink,
                            PageLink = ec.PageLink,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = ec.StatusCodeId.Value,
                            NotificationAdviceTypeId = ec.NotificationAdviceTypeId,
                            NotificationAdvice = ec.NotificationAdvice,
                            RepeatId = ec.RepeatId,
                            CustomId = ec.CustomId,
                            DueDate = ec.DueDate,
                            Monthly = ec.Monthly,
                            Yearly = ec.Yearly,
                            EventDescription = ec.EventDescription,
                            DaysOfWeek = ec.DaysOfWeek,
                            SessionId = ec.SessionId,
                            NotificationStatusId = ec.NotificationStatusId,
                            Title = ec.Title,
                            Message = ec.Message,
                            ScreenId = ec.ScreenId,
                        };
                        applicationWiki.ApplicationWikiLine.Add(ApplicationWikiLine);
                        if (ec.DraftApplicationWikiRecurrence != null)
                        {
                            ec.DraftApplicationWikiRecurrence.ToList().ForEach(r =>
                            {
                                var ApplicationWikiRecurrence = new ApplicationWikiRecurrence
                                {
                                    TypeId = r.TypeId,
                                    RepeatNos = r.RepeatNos,
                                    OccurenceOptionId = r.OccurenceOptionId,
                                    NoOfOccurences = r.NoOfOccurences,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = r.StatusCodeId.Value,
                                    Sunday = r.Sunday,
                                    Monday = r.Monday,
                                    Tuesday = r.Monday,
                                    Wednesday = r.Wednesday,
                                    Thursday = r.Thursday,
                                    Friday = r.Friday,
                                    Saturyday = r.Saturyday,
                                    StartDate = r.StartDate,
                                    EndDate = r.EndDate,
                                };
                                ApplicationWikiLine.ApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                            });
                        }
                        if (ec.DraftApplicationWikiWeekly != null)
                        {
                            ec.DraftApplicationWikiWeekly.ToList().ForEach(u =>
                            {
                                var applicationWikiWeekly = new ApplicationWikiWeekly()
                                {
                                    WeeklyId = u.WeeklyId,
                                    CustomType = u.CustomType,
                                };
                                ApplicationWikiLine.ApplicationWikiWeekly.Add(applicationWikiWeekly);

                            });
                        }
                        if (ec.DraftApplicationWikiLineNotify != null)
                        {
                            ec.DraftApplicationWikiLineNotify.ToList().ForEach(u =>
                            {
                                var applicationWikiLineNotify = new ApplicationWikiLineNotify()
                                {
                                    NotifyUserId = u.NotifyUserId,
                                };
                                ApplicationWikiLine.ApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                            });
                        }
                        if (ec.DraftApplicationWikiLineDuty != null)
                        {
                            ec.DraftApplicationWikiLineDuty.ToList().ForEach(duty =>
                            {
                                var ApplicationWikiLineDuty = new ApplicationWikiLineDuty
                                {
                                    ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                                    DepartmantId = duty.DepartmantId,
                                    DesignationId = duty.DesignationId,
                                    EmployeeId = duty.EmployeeId,
                                    CompanyId = duty.CompanyId,
                                    DesignationNumber = duty.DesignationNumber,
                                    DutyNo = duty.DutyNo,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = duty.StatusCodeId,
                                    Type = duty.Type,
                                };
                                ApplicationWikiLine.ApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
                                var WikiResponsible = _context.DraftWikiResponsible.Where(w => w.ApplicationWikiLineDutyId == duty.ApplicationWikiLineDutyId).ToList();
                                if (WikiResponsible != null && duty.DraftWikiResponsible.Count > 0)
                                {
                                    WikiResponsible.ForEach(s =>
                                    {
                                        var wikiResponsible = new WikiResponsible
                                        {
                                            EmployeeId = s.EmployeeId,
                                            UserId = s.UserId,
                                            UserGroupId = s.UserGroupId,
                                        };
                                        ApplicationWikiLineDuty.WikiResponsible.Add(wikiResponsible);
                                    });
                                }
                            });
                        }
                    });
                }
                _context.SaveChanges();
                
            }
            return value;
        }
        [HttpPost]
        [Route("InsertDraftApplicationWiki")]
        public ApplicationWikiModel InsertDraftApplicationWiki(ApplicationWikiModel value, long? ReleaseApplicationWikiId)
        {
            var applicationWiki = new DraftApplicationWiki
            {
                WikiDate = value.WikiDate,
                WikiEntryById = value.WikiEntryById,
                WikiTypeId = value.WikiTypeId,
                WikiOwnerId = value.WikiOwnerId,
                WikiCategoryId = value.WikiCategoryId,
                WikiTopicId = value.WikiTopicId,
                Title = value.Title,
                TranslationRequiredId = value.TranslationRequiredId,
                Objective = value.Objective,
                Scope = value.ScopeDesc,
                PreRequisition = value.PreRequisition,
                Content = value.Content,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                DepartmentId = value.DepartmentId,
                Description = value.Description,
                Remarks = value.Remarks,
                EffectiveDate = value.EffectiveDate,
                NewReviewDate = value.NewReviewDate,
                VersionNo = value.VersionNo,
                SessionId = value.SessionId,
                ProfileReferenceNo = value.ProfileReferenceNo,
                ProfileId = value.ProfileId,
                TaskLink = value.TaskLink,
                IsContentEntry = value.IsContentEntryFlag == "Yes" ? true : false,
                ProfilePlantId = value.ProfilePlantId,
                ProfileDepartmentId = value.ProfileDepartmentId,
                ProfileDivisionId = value.ProfileDivisionId,
                ProfileSectionId = value.ProfileSectionId,
                ProfileSubSectionId = value.ProfileSubSectionId,
                ReleaseApplicationWikiId = ReleaseApplicationWikiId,
                EstimationPreparationTimeMonth = value.EstimationPreparationTimeMonth,
                WikiOwnerTypeId = value.WikiOwnerTypeId,
                PlantId = value.PlantId,
                EmployeeId = value.EmployeeId,
                ContentInfo=value.ContentInfo,
            };
            if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
            {
                applicationWiki.IsMalay = value.IsMalay;
                applicationWiki.IsChinese = value.IsChinese;
            }
            if (value.WikiCategoryIds.Count > 0)
            {
                value.WikiCategoryIds.ForEach(h =>
                {
                    DraftAppWikiCategoryMultiple AppWikiCategoryMultiple = new DraftAppWikiCategoryMultiple
                    {
                        WikiCategoryId = h,
                    };
                    applicationWiki.DraftAppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                });
            }
            if (value.WikiTopicIds.Count > 0)
            {
                value.WikiTopicIds.ForEach(h =>
                {
                    DraftAppWikiTopicMultiple appWikiTopicMultiple = new DraftAppWikiTopicMultiple
                    {
                        WikiTopicId = h,
                    };
                    applicationWiki.DraftAppWikiTopicMultiple.Add(appWikiTopicMultiple);
                });
            }
            if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
            {
                value.AppWikiTaskLinkModel.ForEach(h =>
                {
                    DraftAppWikiTaskLink appWikiTaskLink = new DraftAppWikiTaskLink
                    {
                        ApplicationWikiId = applicationWiki.ApplicationWikiId,
                        TaskLink = h.TaskLink,
                        Subject=h.Subject,
                    };
                    _context.DraftAppWikiTaskLink.Add(appWikiTaskLink);
                });
                _context.SaveChanges();
            }
            _context.DraftApplicationWiki.Add(applicationWiki);
            _context.SaveChanges();
            value.ApplicationWikiId = applicationWiki.ApplicationWikiId;
            value.ProfileReferenceNo = value.ProfileReferenceNo;
            return value;
        }
        [HttpGet]
        [Route("GetWikiResponsibleByID")]
        public List<DraftWikiResponsibilityModel> GetWikiResponsibleByID(int? id)
        {
            List<DraftWikiResponsibilityModel> wikiResponsibilityModels = new List<DraftWikiResponsibilityModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var usergroup = _context.UserGroupUser.Include(u => u.UserGroup).ToList();
            var wikiResponsible = _context.DraftWikiResponsible

                                .Include(o => o.Employee)
                                .Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.UserGroup)
                                .Where(s => s.ApplicationWikiLineDutyId == id.Value).AsNoTracking().ToList();
            wikiResponsible.ForEach(s =>
            {
                DraftWikiResponsibilityModel wikiResponsibilityModel = new DraftWikiResponsibilityModel
                {
                    WikiResponsibilityID = s.WikiResponsibilityId,
                    ApplicationWikiLineDutyID = s.ApplicationWikiLineDutyId,
                    EmployeeID = s.EmployeeId,
                    UserGroupID = s.UserGroupId,
                    UserID = s.UserId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Employee?.DepartmentNavigation?.Name,
                    DesignationName = s.Employee?.Designation?.Name,
                    HeadCount = s.Employee?.HeadCount,
                    UserGroupName = usergroup != null && s.Employee != null ? (usergroup.Where(w => w.UserId == s.Employee.UserId && w.UserGroupId == s.UserGroupId).Select(g => g.UserGroup?.Name).FirstOrDefault()) : "",
                };
                wikiResponsibilityModels.Add(wikiResponsibilityModel);
            });

            return wikiResponsibilityModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DraftApplicationWikiModel> GetData(SearchModel searchModel)
        {
            var applicationWiki = new DraftApplicationWiki();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Next":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Next":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderBy(o => o.ApplicationWikiId).FirstOrDefault(s => s.ApplicationWikiId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationWiki = _context.DraftApplicationWiki.Include(a => a.DraftAppWikiCategoryMultiple).Include(b => b.DraftAppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault(s => s.ApplicationWikiId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DraftApplicationWikiModel>(applicationWiki);
            if (applicationWiki != null)
            {
                result.WikiCategoryIds = applicationWiki.DraftAppWikiCategoryMultiple != null ? applicationWiki.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList() : new List<long>();
                result.WikiTopicIds = applicationWiki.DraftAppWikiTopicMultiple != null ? applicationWiki.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList() : new List<long>();
                var appWikiTaskLink = _context.DraftAppWikiTaskLink.Select(n => new DraftAppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => w.ApplicationWikiId == result.ApplicationWikiId).ToList();
                result.AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.ApplicationWikiId == result.ApplicationWikiId).ToList();
                
                result.IsEdits = true;
                var ApplicationWikiLineLists = _context.DraftApplicationWikiLine.Select(a => new { a.ApplicationWikiLineId, a.ApplicationWikiId, a.IsAllowDocAccess }).Where(a => a.ApplicationWikiId == result.ApplicationWikiId).ToList();
                var ApplicationWikiLineIds = ApplicationWikiLineLists.Select(s => s.ApplicationWikiLineId).ToList();
                var ApplicationWikiLineDutys = _context.DraftApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId }).Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).ToList();
                var ApplicationWikiLineDutyIds = ApplicationWikiLineDutys.Select(s => s.ApplicationWikiLineDutyId).ToList();
                var WikiResponsible = _context.DraftWikiResponsible.Include(e => e.Employee).Select(s => new { s.ApplicationWikiLineDutyId, s.WikiResponsibilityId, s.EmployeeId, UserId = s.Employee.UserId.Value }).Where(w => ApplicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId != null).ToList();

                if (result.AddedByUserID == searchModel.UserID)
                {
                    result.IsEdits = true;
                }
                else
                {
                    if (ApplicationWikiLineLists != null && ApplicationWikiLineLists.Count > 0)
                    {
                        var ApplicationWikiLineDoc = ApplicationWikiLineLists.Where(a => a.ApplicationWikiId == result.ApplicationWikiId && a.IsAllowDocAccess == true).Select(a => a.IsAllowDocAccess).Distinct().Count();
                        if (ApplicationWikiLineDoc > 0)
                        {
                            result.IsEdits = true;
                        }
                        else
                        {
                            var ApplicationWikiLineDutyList = ApplicationWikiLineDutys != null ? ApplicationWikiLineDutys.Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).Select(d => d.ApplicationWikiLineDutyId).ToList() : null;
                            var WikiResponsibleCount = WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value)).Count();
                            if (WikiResponsibleCount > 0)
                            {
                                var WikiResponsibleList = ApplicationWikiLineDutyList != null ? WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId == searchModel.UserID).ToList() : null;
                                result.IsEdits = WikiResponsibleList != null && WikiResponsibleList.Count > 0 ? true : false;
                            }
                        }
                    }
                }
            }
            return result;
        }

        [HttpPost]
        [Route("InsertApplicationWikiLine")]
        public DraftApplicationWikiLineModel InsertApplicationWikiLine(DraftApplicationWikiLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var ApplicationWikiLine = new DraftApplicationWikiLine
            {
                ApplicationWikiId = value.ApplicationWikiId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = value.ScreenId,
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess=value.IsAllowDocAccess,
            };
            _context.DraftApplicationWikiLine.Add(ApplicationWikiLine);
            _context.SaveChanges();
            value.ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId;
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new DraftApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.DraftApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new DraftApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.DraftApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.NotifyIds != null && value.NotifyIds.Count > 0)
            {
                value.NotifyIds.ForEach(u =>
                {
                    var applicationWikiLineNotify = new DraftApplicationWikiLineNotify()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        NotifyUserId = u,
                    };
                    _context.DraftApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                    _context.SaveChanges();
                });
            }
            value.SessionId = SessionId;
            value.NotificationAdvice = ApplicationWikiLine.NotificationAdvice;
            NotificationHandeler(value);
            return value;
        }
        private void NotificationHandeler(DraftApplicationWikiLineModel value)
        {
            var notificationHandlerItems = _context.NotificationHandler.Where(w => w.SessionId == value.SessionId).ToList();
            List<NotificationHandler> NotificationHandlers = new List<NotificationHandler>();
            NotificationHandler NotificationHandler = new NotificationHandler();
            if (notificationHandlerItems != null)
            {
                notificationHandlerItems.ForEach(h =>
                {
                    var notificationHandler = _context.NotificationHandler.SingleOrDefault(w => w.NotificationHandlerId == h.NotificationHandlerId);
                    if (notificationHandler != null)
                    {
                        notificationHandler.NotifyStatusId = 2;
                    }
                    _context.SaveChanges();
                });
            }
            if (value.NotificationAdvice == true && value.DueDate != null)
            {
                var DueDate = value.DueDate;
                value.DueDate = value.DueDate == null ? DateTime.Now : value.DueDate;
                if (value.RepeatId != null && value.RepeatId != 1881)
                {
                    if (value.RepeatId == 1882)
                    {
                        NotificationHandler.NextNotifyDate = DueDate == null ? DateTime.Now.AddDays(1) : DueDate;
                        NotificationHandler.AddedDays = 1;
                    }
                    else if (value.RepeatId == 1883)
                    {
                        DateTime Monday = value.DueDate.Value.Date;
                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                        NotificationHandler.NextNotifyDate = Monday.AddDays(daysUntilMonday);
                        NotificationHandler.AddedDays = 7;
                    }
                    else if (value.RepeatId == 1884)
                    {
                        DateTime Monday = value.DueDate.Value.Date;
                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                        DateTime NextMonday = Monday.AddDays(daysUntilMonday);
                        NotificationHandler.NextNotifyDate = NextMonday.AddDays(7);
                        NotificationHandler.AddedDays = 14;
                    }
                    else if (value.RepeatId == 1885)
                    {
                        NotificationHandler.NextNotifyDate = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, 1).AddMonths(+1);
                        var totalDays = (NotificationHandler.NextNotifyDate.Value.AddMonths(1) - NotificationHandler.NextNotifyDate.Value).TotalDays;
                        NotificationHandler.AddedDays = 0;
                    }
                    else if (value.RepeatId == 1886)
                    {
                        NotificationHandler.NextNotifyDate = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, 1).AddYears(+1);
                        NotificationHandler.AddedDays = 0;
                    }
                }
                if (value.CustomId != null)
                {
                    if (value.CustomId == 2131)
                    {
                        NotificationHandler.NextNotifyDate = DueDate == null ? DateTime.Now.AddDays(1) : DueDate;
                        NotificationHandler.AddedDays = 1;
                    }
                    if (value.CustomId == 2132)
                    {
                        if (value.WeeklyIds.Count > 0)
                        {

                            value.WeeklyIds.ForEach(h =>
                            {
                                if (h == 1445)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Monday = value.DueDate.Value.Date;
                                    int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                                    DateTime NextMonday = Monday.AddDays(daysUntilMonday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextMonday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1446)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Tuesday = value.DueDate.Value.Date;
                                    int daysUntilTuesday = ((int)DayOfWeek.Tuesday - (int)Tuesday.DayOfWeek + 7) % 7;
                                    DateTime NextTuesday = Tuesday.AddDays(daysUntilTuesday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextTuesday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1447)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Wednesday = value.DueDate.Value.Date;
                                    int daysUntilWednesday = ((int)DayOfWeek.Wednesday - (int)Wednesday.DayOfWeek + 7) % 7;
                                    DateTime NextWednesday = Wednesday.AddDays(daysUntilWednesday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextWednesday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1448)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Thursday = value.DueDate.Value.Date;
                                    int daysUntilThursday = ((int)DayOfWeek.Thursday - (int)Thursday.DayOfWeek + 7) % 7;
                                    DateTime NextThursday = Thursday.AddDays(daysUntilThursday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextThursday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1449)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Friday = value.DueDate.Value.Date;
                                    int daysUntilFriday = ((int)DayOfWeek.Friday - (int)Friday.DayOfWeek + 7) % 7;
                                    DateTime NextFriday = Friday.AddDays(daysUntilFriday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextFriday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1450)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Saturday = value.DueDate.Value.Date;
                                    int daysUntilSaturday = ((int)DayOfWeek.Saturday - (int)Saturday.DayOfWeek + 7) % 7;
                                    DateTime NextSaturday = Saturday.AddDays(daysUntilSaturday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextSaturday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1451)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Sunday = value.DueDate.Value.Date;
                                    int daysUntilSunday = ((int)DayOfWeek.Sunday - (int)Sunday.DayOfWeek + 7) % 7;
                                    DateTime NextSunday = Sunday.AddDays(daysUntilSunday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextSunday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                            });
                        }
                    }
                    if (value.CustomId == 2133)
                    {
                        var date = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, value.Monthly.Value);
                        if (DueDate == null)
                        {
                            date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, value.Monthly.Value);

                            if (DateTime.Now.Day >= value.Monthly)
                            {
                                var currentday = DateTime.Now.AddMonths(1);
                                date = new DateTime(currentday.Year, currentday.Month, value.Monthly.Value);
                            }
                        }
                        NotificationHandler.NextNotifyDate = date;
                        NotificationHandler.AddedDays = 0;
                    }
                    if (value.CustomId == 2134)
                    {
                        var dates = (new DateTime(DateTime.Now.Year, value.Yearly.Value, DateTime.Now.Day)).AddYears(1);
                        var dt = new DateTime(dates.Date.Year, dates.Date.Month, 1);
                        if (value.DaysOfWeek == true)
                        {
                            if (value.DaysOfWeekIds.Count > 0)
                            {
                                value.DaysOfWeekIds.ForEach(h =>
                                {
                                    if (h == 1445)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Monday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1446)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Tuesday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1447)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Wednesday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1448)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Thursday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1449)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Friday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1450)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Saturday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1451)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Sunday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                });
                            }
                        }
                        else
                        {
                            NotificationHandler.NextNotifyDate = dates;
                            NotificationHandler.AddedDays = 0;
                        }
                    }
                }
                NotificationHandlers.Add(NotificationHandler);
                if (NotificationHandlers.Count > 0)
                {
                    NotificationHandlers.ForEach(h =>
                    {
                        if (h.NextNotifyDate != null)
                        {
                            if (h.NextNotifyDate.Value.Date <= value.NotifyEndDate || value.NotifyEndDate == null)
                            {
                                // if (value.NotifyIds.Count > 0)
                                //{
                                //value.NotifyIds.ForEach(u =>
                                // {
                                NotificationHandler NotificationHandlerinsert = new NotificationHandler();
                                NotificationHandlerinsert.SessionId = value.SessionId;
                                NotificationHandlerinsert.NotifyStatusId = value.NotificationStatusId;
                                NotificationHandlerinsert.Title = value.Title;
                                NotificationHandlerinsert.Message = value.Message;
                                NotificationHandlerinsert.NextNotifyDate = h.NextNotifyDate;
                                NotificationHandlerinsert.AddedDays = h.AddedDays;
                                NotificationHandlerinsert.ScreenId = value.ScreenId;
                                // NotificationHandlerinsert.NotifyTo = u;
                                _context.NotificationHandler.Add(NotificationHandlerinsert);
                                //});
                                //}
                            }

                        }
                    });
                }
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("InsertApplicationWikiRecurrence")]
        public DraftApplicationWikiRecurrenceModel InsertApplicationWikiRecurrence(DraftApplicationWikiRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.DraftApplicationWikiRecurrence.Where(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.DraftApplicationWikiRecurrence.SingleOrDefault(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId);
                ApplicationWikiRecurrences.ApplicationWikiLineId = value.ApplicationWikiLineId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new DraftApplicationWikiRecurrence
                {
                    ApplicationWikiLineId = value.ApplicationWikiLineId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.DraftApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.ApplicationWikiRecurrenceId = ApplicationWikiRecurrence.ApplicationWikiRecurrenceId;
                return value;
            }
        }
        [HttpPost]
        [Route("InsertApplicationWikiLineDuty")]
        public DraftApplicationWikiLineDutyModel InsertApplicationWikiLineDuty(DraftApplicationWikiLineDutyModel value)
        {

            var ApplicationWikiLineDuty = new DraftApplicationWikiLineDuty
            {
                ApplicationWikiLineId = value.ApplicationWikiLineId,
                DepartmantId = value.DepartmentId,
                DesignationId = value.DesignationId,
                EmployeeId = value.EmployeeId,
                CompanyId = value.CompanyId,
                DesignationNumber = value.DesignationNumber,
                DutyNo = value.DutyNo,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Type = value.Type,
            };
            _context.DraftApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
            _context.SaveChanges();
            value.ApplicationWikiLineDutyId = ApplicationWikiLineDuty.ApplicationWikiLineDutyId;
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {
                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.DraftWikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new DraftWikiResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i=>i.UserGroupId).FirstOrDefault() : null,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                        };
                        _context.DraftWikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                    if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                    {
                        var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                        if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                        {
                            userGroupEmpIds.ForEach(emp =>
                            {
                                var existing = _context.WikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                                if (existing == null)
                                {
                                    var wikiResponsible = new DraftWikiResponsible
                                    {
                                        EmployeeId = emp,
                                        UserGroupId = s,
                                        ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                                    };
                                    _context.DraftWikiResponsible.Add(wikiResponsible);
                                    _context.SaveChanges();
                                }
                            });
                        }
                    }
                });
            }

            return value;
        }
        [HttpPut]
        [Route("UpdateApplicationWikiLine")]
        public DraftApplicationWikiLineModel UpdateApplicationWikiLine(DraftApplicationWikiLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            value.DueDate ??= DateTime.Now;
            var ApplicationWikiLine = _context.DraftApplicationWikiLine.SingleOrDefault(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId);
            ApplicationWikiLine.ApplicationWikiId = value.ApplicationWikiId;
            ApplicationWikiLine.DutyId = value.DutyId;
            ApplicationWikiLine.Responsibility = value.Responsibility;
            ApplicationWikiLine.FunctionLink = value.FunctionLink;
            ApplicationWikiLine.PageLink = value.PageLink;
            ApplicationWikiLine.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLine.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLine.ModifiedDate = DateTime.Now;
            ApplicationWikiLine.NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false;
            ApplicationWikiLine.NotificationAdviceTypeId = value.NotificationAdviceTypeId;
            ApplicationWikiLine.RepeatId = value.RepeatId;
            ApplicationWikiLine.CustomId = value.CustomId;
            ApplicationWikiLine.DueDate = value.DueDate;
            ApplicationWikiLine.Monthly = value.Monthly;
            ApplicationWikiLine.Yearly = value.Yearly;
            ApplicationWikiLine.EventDescription = value.EventDescription;
            ApplicationWikiLine.DaysOfWeek = value.DaysOfWeek;
            ApplicationWikiLine.SessionId = value.SessionId;
            ApplicationWikiLine.NotificationStatusId = value.NotificationStatusId;
            ApplicationWikiLine.Title = value.Title;
            ApplicationWikiLine.Message = value.Message;
            ApplicationWikiLine.ScreenId = value.ScreenId;
            ApplicationWikiLine.NotifyEndDate = value.NotifyEndDate;
            ApplicationWikiLine.IsAllowDocAccess = value.IsAllowDocAccess;
            _context.SaveChanges();
            var ApplicationWikiWeeklyRemove = _context.DraftApplicationWikiWeekly.Where(w => w.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (ApplicationWikiWeeklyRemove != null)
            {
                _context.DraftApplicationWikiWeekly.RemoveRange(ApplicationWikiWeeklyRemove);
                _context.SaveChanges();
            }
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new DraftApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Weekly"
                    };
                    _context.DraftApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new DraftApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Yearly"
                    };
                    _context.DraftApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            var notifyUserRemove = _context.DraftApplicationWikiLineNotify.Where(w => w.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (notifyUserRemove != null)
            {
                _context.DraftApplicationWikiLineNotify.RemoveRange(notifyUserRemove);
                _context.SaveChanges();
            }
            if (value.NotifyIds != null && value.NotifyIds.Count > 0)
            {
                value.NotifyIds.ForEach(u =>
                {
                    var applicationWikiLineNotify = new DraftApplicationWikiLineNotify()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        NotifyUserId = u,
                    };
                    _context.DraftApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                    _context.SaveChanges();
                });
            }
            value.NotificationAdvice = ApplicationWikiLine.NotificationAdvice;
            NotificationHandeler(value);
            return value;

        }
        [HttpPut]
        [Route("UpdateApplicationWikiLineDuty")]
        public DraftApplicationWikiLineDutyModel UpdateApplicationWikiLineDuty(DraftApplicationWikiLineDutyModel value)
        {
            var ApplicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.SingleOrDefault(p => p.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId);
            ApplicationWikiLineDuty.ApplicationWikiLineId = value.ApplicationWikiLineId;
            ApplicationWikiLineDuty.DepartmantId = value.DepartmentId;
            ApplicationWikiLineDuty.DesignationId = value.DesignationId;
            ApplicationWikiLineDuty.CompanyId = value.CompanyId;
            ApplicationWikiLineDuty.EmployeeId = value.EmployeeId;
            ApplicationWikiLineDuty.DesignationNumber = value.DesignationNumber;
            ApplicationWikiLineDuty.DutyNo = value.DutyNo;
            ApplicationWikiLineDuty.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLineDuty.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLineDuty.ModifiedDate = DateTime.Now;
            ApplicationWikiLineDuty.Type = value.Type;
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.DraftWikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new DraftWikiResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i => i.UserGroupId).FirstOrDefault() : null,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                        };
                        _context.DraftWikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.DraftWikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new DraftWikiResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                                        };
                                        _context.DraftWikiResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            _context.SaveChanges();

            return value;

        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationWiki")]
        public void Delete(int id)
        {
            var applicationWiki = _context.DraftApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == id);
            if (applicationWiki != null)
            {
                var AppWikiCategoryMultipleRemove = _context.DraftAppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (AppWikiCategoryMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                    _context.SaveChanges();
                }
                var draftAppWikiTaskLinkRemove = _context.DraftAppWikiTaskLink.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (draftAppWikiTaskLinkRemove.Count > 0)
                {
                    _context.DraftAppWikiTaskLink.RemoveRange(draftAppWikiTaskLinkRemove);
                    _context.SaveChanges();
                }
                var AppWikiTopicMultipleRemove = _context.DraftAppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (AppWikiTopicMultipleRemove.Count > 0)
                {
                    _context.DraftAppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                    _context.SaveChanges();
                }
                var ApplicationWikiLine = _context.DraftApplicationWikiLine.Where(p => p.ApplicationWikiId == id).ToList();
                if (ApplicationWikiLine != null)
                {
                    ApplicationWikiLine.ForEach(c =>
                    {
                        var ApplicationWikiRecurrence = _context.DraftApplicationWikiRecurrence.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiRecurrence != null)
                        {
                            _context.DraftApplicationWikiRecurrence.RemoveRange(ApplicationWikiRecurrence);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiLineDuty != null)
                        {
                            ApplicationWikiLineDuty.ForEach(d =>
                            {
                                var wikiresponsible = _context.DraftWikiResponsible.Where(p => p.ApplicationWikiLineDutyId == d.ApplicationWikiLineDutyId).ToList();
                                if (wikiresponsible != null)
                                {
                                    _context.DraftWikiResponsible.RemoveRange(wikiresponsible);
                                    _context.SaveChanges();
                                }
                            });
                            _context.DraftApplicationWikiLineDuty.RemoveRange(ApplicationWikiLineDuty);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiWeekly = _context.DraftApplicationWikiWeekly.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiWeekly != null)
                        {
                            _context.DraftApplicationWikiWeekly.RemoveRange(ApplicationWikiWeekly);
                            _context.SaveChanges();
                        }
                        var notificationHandler = _context.NotificationHandler.Where(w => w.SessionId == c.SessionId).ToList();
                        if (notificationHandler != null)
                        {
                            _context.NotificationHandler.RemoveRange(notificationHandler);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiLineNotify = _context.DraftApplicationWikiLineNotify.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiLineNotify != null)
                        {
                            _context.DraftApplicationWikiLineNotify.RemoveRange(ApplicationWikiLineNotify);
                            _context.SaveChanges();
                        }
                    });
                    _context.DraftApplicationWikiLine.RemoveRange(ApplicationWikiLine);
                    _context.SaveChanges();
                }
            }
            _context.DraftApplicationWiki.Remove(applicationWiki);
            _context.SaveChanges();
        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLine")]
        public void DeleteApplicationWikiLine(int id)
        {
            var ApplicationWikiLine = _context.DraftApplicationWikiLine.SingleOrDefault(p => p.ApplicationWikiLineId == id);
            var ApplicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.Where(p => p.ApplicationWikiLineId == id).ToList();
            var ApplicationWikiRecurrence = _context.DraftApplicationWikiRecurrence.SingleOrDefault(p => p.ApplicationWikiLineId == id);
            if (ApplicationWikiRecurrence != null)
            {
                _context.DraftApplicationWikiRecurrence.Remove(ApplicationWikiRecurrence);
                _context.SaveChanges();
            }
            var notificationHandler = _context.NotificationHandler.Where(w => w.SessionId == ApplicationWikiLine.SessionId).ToList();
            if (notificationHandler != null)
            {
                _context.NotificationHandler.RemoveRange(notificationHandler);
                _context.SaveChanges();
            }
            var ApplicationWikiWeekly = _context.DraftApplicationWikiWeekly.Where(p => p.ApplicationWikiLineId == id).ToList();
            if (ApplicationWikiWeekly != null)
            {
                _context.DraftApplicationWikiWeekly.RemoveRange(ApplicationWikiWeekly);
                _context.SaveChanges();
            }
            if (ApplicationWikiLineDuty != null)
            {
                ApplicationWikiLineDuty.ForEach(h =>
                {
                    var wikiresponsible = _context.DraftWikiResponsible.Where(p => p.ApplicationWikiLineDutyId == h.ApplicationWikiLineDutyId).ToList();
                    if (wikiresponsible != null)
                    {
                        _context.DraftWikiResponsible.RemoveRange(wikiresponsible);
                        _context.SaveChanges();
                    }
                });
                _context.DraftApplicationWikiLineDuty.RemoveRange(ApplicationWikiLineDuty);
                _context.SaveChanges();
            }
            _context.DraftApplicationWikiLine.Remove(ApplicationWikiLine);
            _context.SaveChanges();
        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLineDuty")]
        public void DeleteApplicationWikiLineDuty(int id)
        {
            var ApplicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.SingleOrDefault(p => p.ApplicationWikiLineDutyId == id);
            var wikiresponsible = _context.DraftWikiResponsible.Where(p => p.ApplicationWikiLineDutyId == id).ToList();
            if (wikiresponsible != null)
            {
                _context.DraftWikiResponsible.RemoveRange(wikiresponsible);
                _context.SaveChanges();
            }
            _context.DraftApplicationWikiLineDuty.Remove(ApplicationWikiLineDuty);
            _context.SaveChanges();
        }

        [HttpPost]
        [Route("InsertWikiResponsible")]
        public DraftWikiResponsibilityModel InsertWikiResponsible(DraftWikiResponsibilityModel value)
        {
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    var existing = _context.DraftWikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyID).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new DraftWikiResponsible
                        {
                            EmployeeId = s,
                            UserGroupId = value.UserGroupID,
                            UserId = value.UserID,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyID,
                        };
                        _context.DraftWikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.DraftWikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyID).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new DraftWikiResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            UserId = value.UserID,
                                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyID,
                                        };
                                        _context.DraftWikiResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteWikiResponsible")]
        public void DeleteWikiResponsible(int id)
        {
            var wikiresponsible = _context.DraftWikiResponsible.SingleOrDefault(p => p.WikiResponsibilityId == id);
            if (wikiresponsible != null)
            {
                _context.DraftWikiResponsible.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }
        [HttpPut]
        [Route("DeleteWikiResponsibles")]
        public DraftWikiResponsibilityModel DeleteWikiResponsibles(DraftWikiResponsibilityModel value)
        {

            var selectDocumentRoleIds = value.WikiResponsibilityIDs;
            if (value.WikiResponsibilityIDs.Count > 0)
            {
                value.WikiResponsibilityIDs.ForEach(sel =>
                {
                    var wikiresponsible = _context.DraftWikiResponsible.Where(p => p.WikiResponsibilityId == sel).FirstOrDefault();
                    if (wikiresponsible != null)
                    {


                        _context.DraftWikiResponsible.Remove(wikiresponsible);
                        _context.SaveChanges();

                    }
                });

                _context.SaveChanges();
            }


            return value;
        }
    }
}