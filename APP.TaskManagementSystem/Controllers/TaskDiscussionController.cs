﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskDiscussionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskDiscussionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskDiscussion")]
        public ActionResult<TaskDiscussionModel> Get(int id, int userId)
        {

            var taskDiscussion = _context.TaskDiscussion.SingleOrDefault(t => t.TaskId == id && t.UserId == userId);
            var result = _mapper.Map<TaskDiscussionModel>(taskDiscussion);
            return result = null;


        }

        // GET: api/Project/2
        [HttpGet("GetdiscussionNotes/{id:int}")]
        //public ActionResult<TaskNotesModel> Get(int? id)
        //{
        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //var DiscussionNotes = _context.DiscussionNotes.SingleOrDefault(p => p.TaskNotesId == id.Value);
        //    //var result = _mapper.Map<TaskNotesModel>(DiscussionNotes);
        //    //if (result == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    return result;
        //}


        // POST: api/User
        [HttpPost]
        [Route("InsertTaskDiscussion")]
        public TaskDiscussion Post(TaskDiscussion value)
        {
            if (value.TaskId != 0)
            {
                var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskId);
                if (taskMaster != null)
                {
                    taskMaster.DiscussionDate = value.DiscussionDate;
                    //_context.SaveChanges();              



                }
            }
            //DateTime remDate;
            //if (value.RemainderDate <= value.DueDate)
            //{
            //    remDate = value.RemainderDate;
            //}
            var taskDiscussion = _context.TaskDiscussion.Where(p => p.TaskId == value.TaskId && p.UserId == value.UserId).FirstOrDefault();
            if (taskDiscussion == null)
            {
                taskDiscussion = new TaskDiscussion
                {

                    TaskId = value.TaskId,
                    DiscussionNotes = value.DiscussionNotes,
                    DiscussionDate = value.DiscussionDate,
                    UserId = value.UserId,
                    StatusCodeId = value.StatusCodeId,


                };
                //    _context.DiscussionNotes.Add(DiscussionNotes);
                //    _context.SaveChanges();
                //    value.TaskNotesID = DiscussionNotes.TaskNotesId;
            }
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskDiscussion")]
        public TaskDiscussion Put(TaskDiscussion value)
        {
            if (value.TaskId != 0)
            {
                var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskId);
                if (taskMaster != null)
                {
                    taskMaster.DiscussionDate = value.DiscussionDate;
                    //_context.SaveChanges();              



                }
            }
            var taskDiscussion = _context.TaskDiscussion.SingleOrDefault(p => p.DiscussionNotesId == value.DiscussionNotesId);
            //  folders.TaskMemberId = value.TaskMemberID;
            taskDiscussion.TaskId = value.TaskId;
            taskDiscussion.UserId = value.UserId;
            taskDiscussion.DiscussionNotes = value.DiscussionNotes;
            taskDiscussion.DiscussionDate = value.DiscussionDate;
            taskDiscussion.StatusCodeId = value.StatusCodeId;


            _context.SaveChanges();
            return value;
        }
    }
}