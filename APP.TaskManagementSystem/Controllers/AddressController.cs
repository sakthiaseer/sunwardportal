﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AddressController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AddressController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetAddress")]
        public List<AddressModel> Get()
        {
            var address = _context.Address.Include("AddedByUser").Include("ModifiedByUser").Select(s => new AddressModel
            {
                AddressID = s.AddressId,
                AddressType = s.AddressType.Value,
                Address1 = s.Address1,
                Address2 = s.Address2,
                PostCode = s.PostCode.Value,
                City = s.City,
                Country = s.Country,
                CountryCode = s.CountryCode,
                OfficePhone = s.OfficePhone.Value,
                Email = s.Email,
                Website = s.Website,

               

            }).OrderByDescending(o => o.AddressID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<AddressModel>>(Address);
            return address;
        }
        [HttpGet("GetAddressByID")]
        public ActionResult<AddressModel> GetAddress(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var address = _context.Address.SingleOrDefault(p => p.AddressId == id.Value);
            var result = _mapper.Map<AddressModel>(address);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet]
        [Route("GetActiveAddress")]
        public List<AddressModel> GetActiveAddress()
        {
            var address = _context.Address.Select(s => new AddressModel
            {
                AddressID = s.AddressId,
                AddressType = s.AddressType.Value,
                PostCode = s.PostCode.Value,
                City = s.City,
                Country = s.Country,
                CountryCode = s.CountryCode,
                OfficePhone = s.OfficePhone.Value,
                Email = s.Email,
                Website = s.Website,

            }).OrderByDescending(o => o.AddressID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<AddressModel>>(Address);
            return address;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<AddressModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var address = _context.Address.SingleOrDefault(p => p.AddressId == id.Value);
            var result = _mapper.Map<AddressModel>(address);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AddressModel> GetData(SearchModel searchModel)
        {
            var address = new Address();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        address = _context.Address.OrderByDescending(o => o.AddressId).FirstOrDefault();
                        break;
                    case "Last":
                        address = _context.Address.OrderByDescending(o => o.AddressId).LastOrDefault();
                        break;
                    case "Next":
                        address = _context.Address.OrderByDescending(o => o.AddressId).LastOrDefault();
                        break;
                    case "Previous":
                        address = _context.Address.OrderByDescending(o => o.AddressId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        address = _context.Address.OrderByDescending(o => o.AddressId).FirstOrDefault();
                        break;
                    case "Last":
                        address = _context.Address.OrderByDescending(o => o.AddressId).LastOrDefault();
                        break;
                    case "Next":
                        address = _context.Address.OrderBy(o => o.AddressId).FirstOrDefault(s => s.AddressId > searchModel.Id);
                        break;
                    case "Previous":
                        address = _context.Address.OrderByDescending(o => o.AddressId).FirstOrDefault(s => s.AddressId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AddressModel>(address);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAddress")]
        public AddressModel Post(AddressModel value)
        {
            var address = new Address
            {

               AddressType = value.AddressType,
               PostCode = value.PostCode,
               Address1 = value.Address1,
               Address2 = value.Address2,
               City = value.City,
               Country = value.Country,
               CountryCode = value.CountryCode,
               OfficePhone = value.OfficePhone,
               Email = value.Email,
               Website = value.Website,


            };
            _context.Address.Add(address);
            _context.SaveChanges();
            value.AddressID = address.AddressId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAddress")]
        public AddressModel Put(AddressModel value)
        {
            var address = _context.Address.SingleOrDefault(p => p.AddressId == value.AddressID);
            address.AddressType = value.AddressType;
            address.PostCode = value.PostCode;
            address.Address1 = value.Address1;
            address.Address2 = value.Address2;
            address.City = value.City;
            address.Country = value.Country;
            address.CountryCode = value.CountryCode;
            address.OfficePhone =value.OfficePhone;
            address.Email = value.Email;
            address.Website = value.Website;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAddress")]
        public void Delete(int id)
        {
            var address = _context.Address.SingleOrDefault(p => p.AddressId == id);
            if (address != null)
            {
                _context.Address.Remove(address);
                _context.SaveChanges();
            }
        }
    }
}