﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.TaskManagementSystem.Hubs;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BroadcastController : ControllerBase
    {
        private readonly IHubContext<ChatHub> _hub;
     

        public BroadcastController(IHubContext<ChatHub> hub)
        {
            _hub = hub;
            
        }

        [HttpGet]
        public async Task Get(string id)
        {
           
        }
        private async Task PerformBackgroundJob(string jobId)
        {
            for (int i = 0; i <= 100; i += 5)
            {
                await _hub.Clients.Group(jobId).SendAsync("progress", i);

                await Task.Delay(200);
            }
        }
    }
}
