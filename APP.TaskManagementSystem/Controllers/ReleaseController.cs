﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ReleaseController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ReleaseController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetRelease")]
        public List<ReleaseVersionModel> Get()
        {
            var releaseList = new List<ReleaseVersionModel>();
            var Release = _context.ReleaseVersion.Include("ReleaseDetails").OrderByDescending(o => o.VersionId).AsNoTracking().ToList();

            Release.ForEach(r =>
            {
                var rel = new ReleaseVersionModel
                {
                    AddedDate = r.AddedDate,
                    Description = r.Description,
                    ReleaseDate = r.ReleaseDate,
                    ReleaseType = r.ReleaseType,
                    VersionId = r.VersionId,
                    VersionNo = r.VersionNo
                };
                var details = new List<ReleaseDetailsModel>();
                r.ReleaseDetails.ToList().ForEach(d =>
                {
                    var detail = new ReleaseDetailsModel
                    {
                        Description =d.Description,
                        Name =d.Name,
                        VersionId =d.VersionId,
                        ReleaseDetailId=d.ReleaseDetailId,
                    };
                    details.Add(detail);
                });

                rel.ReleaseDetails = details;
                releaseList.Add(rel);

            });


            return releaseList;
        }
    }
}