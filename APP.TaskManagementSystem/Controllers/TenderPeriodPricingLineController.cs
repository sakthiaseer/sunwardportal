﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TenderPeriodPricingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public TenderPeriodPricingLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [Route("GetTenderPeriodPricingLine")]
        public List<TenderPeriodPricingLineModel> Get(int? id)
        {
            var tenderPeriodPricingLine = _context.TenderPeriodPricingLine.Include(p => p.Product).Include(ss => ss.Product.SobyCustomers).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
           
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
            List<TenderPeriodPricingLineModel> tenderPeriodPricingLineModel = new List<TenderPeriodPricingLineModel>();
            if(tenderPeriodPricingLine!=null && tenderPeriodPricingLine.Count>0)
            {
                var perUOMIds = tenderPeriodPricingLine.Where(s => s.Product?.PerUomId != null).Select(s => s.Product?.PerUomId).ToList();
                var currencyIds = tenderPeriodPricingLine.Where(s => s.Currency != null).Select(s => s.Currency).ToList();
                if(perUOMIds.Count>0)
                {
                    masterDetailIds.AddRange(perUOMIds);
                }
                if(currencyIds.Count>0)
                {
                    masterDetailIds.AddRange(currencyIds);
                }
                if(masterDetailIds.Count>0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
            }
            tenderPeriodPricingLine.ForEach(s =>
            {
                TenderPeriodPricingLineModel tenderPeriodPricingLineModels = new TenderPeriodPricingLineModel
                {
                    TenderPeriodPricingId = s.TenderPeriodPricingId,
                    TenderPeriodPricingLineId = s.TenderPeriodPricingLineId,
                    ProductId = s.ProductId,
                    ProductName = s.Product?.CustomerReferenceNo,
                    Uom = s.Product?.PerUomId!=null? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).SingleOrDefault() : "",
                  
                    Currency = s.Currency,
                    CurrencyName = s.Currency != null && masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Currency).Select(a => a.Value).SingleOrDefault() : "",
                    Price = s.Price,
                    SessionId = s.SessionId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                tenderPeriodPricingLineModel.Add(tenderPeriodPricingLineModels);
            });
            return tenderPeriodPricingLineModel.Where(w => w.TenderPeriodPricingId == id).OrderByDescending(a => a.TenderPeriodPricingLineId).ToList();
        }

        [HttpPost]
        [Route("InsertTenderPeriodPricingLine")]
        public TenderPeriodPricingLineModel Post(TenderPeriodPricingLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var tenderPeriodPricingLine = new TenderPeriodPricingLine
            {
                TenderPeriodPricingId = value.TenderPeriodPricingId,
                ProductId = value.ProductId,
                Currency = value.Currency,
                Price = value.Price,
                //Uomid = value.Uomid,    
                SessionId = sessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.TenderPeriodPricingLine.Add(tenderPeriodPricingLine);
            _context.SaveChanges();
            value.TenderPeriodPricingLineId = tenderPeriodPricingLine.TenderPeriodPricingLineId;
            value.SessionId = tenderPeriodPricingLine.SessionId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTenderPeriodPricingLine")]
        public TenderPeriodPricingLineModel Put(TenderPeriodPricingLineModel value)
        {
            var tenderPeriodPricingLine = _context.TenderPeriodPricingLine.SingleOrDefault(p => p.TenderPeriodPricingLineId == value.TenderPeriodPricingLineId);
            tenderPeriodPricingLine.TenderPeriodPricingId = value.TenderPeriodPricingId;
            tenderPeriodPricingLine.ProductId = value.ProductId;
            tenderPeriodPricingLine.Currency = value.Currency;
            tenderPeriodPricingLine.Price = value.Price;
            //tenderPeriodPricingLine.Uomid = value.Uomid;
            tenderPeriodPricingLine.ModifiedDate = DateTime.Now;
            tenderPeriodPricingLine.ModifiedByUserId = value.ModifiedByUserID;
            tenderPeriodPricingLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTenderPeriodPricingLine")]
        public void Delete(int id)
        {
            var tenderPeriodPricingLine = _context.TenderPeriodPricingLine.SingleOrDefault(p => p.TenderPeriodPricingLineId == id);
            if (tenderPeriodPricingLine != null)
            {
                _context.TenderPeriodPricingLine.Remove(tenderPeriodPricingLine);
                _context.SaveChanges();
            }
        }
    }
}