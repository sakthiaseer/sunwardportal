﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Roles = "Admin")]
    public class ProductionOutputController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public ProductionOutputController(CRT_TMSContext context, IMapper mapper, IConfiguration config, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductionOutputs")]
        public List<ProductionOutputModel> Get()
        {
            List<ProductionOutputModel> productionOutputModels = new List<ProductionOutputModel>();
            var productionOutputs = _context.ProductionOutput.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.ProductionOutputId).AsNoTracking().ToList();

            productionOutputs.ForEach(s =>
            {
                var productionOutputModel = new ProductionOutputModel
                {
                    ProductionOutputId = s.ProductionOutputId,
                    LocationName = s.LocationName,
                    ProductionEntryId = s.ProductionEntryId,
                    SubLotNo = s.SubLotNo,
                    DrumNo = s.DrumNo,
                    Description = s.Description,
                    BatchNo = s.BatchNo,
                    OutputQty = s.OutputQty,
                    NetWeight = s.NetWeight,
                    Buom = s.Buom,
                    ItemId = s.ItemId,
                    ItemNo = s.ItemNo,
                    IsLotComplete = s.IsLotComplete,
                    IsProdutionOrderComplete = s.IsProdutionOrderComplete,
                    ProductionOrderNo = s.ProductionOrderNo,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                productionOutputModels.Add(productionOutputModel);
            });
            return productionOutputModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionOutputModel> GetData(SearchModel searchModel)
        {
            var productionOutput = new ProductionOutput();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).FirstOrDefault();
                        break;
                    case "Last":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).LastOrDefault();
                        break;
                    case "Next":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).LastOrDefault();
                        break;
                    case "Previous":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).FirstOrDefault();
                        break;
                    case "Last":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).LastOrDefault();
                        break;
                    case "Next":
                        productionOutput = _context.ProductionOutput.OrderBy(o => o.ProductionOutputId).FirstOrDefault(s => s.ProductionOutputId > searchModel.Id);
                        break;
                    case "Previous":
                        productionOutput = _context.ProductionOutput.OrderByDescending(o => o.ProductionOutputId).FirstOrDefault(s => s.ProductionOutputId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionOutputModel>(productionOutput);
            return result;
        }

        [HttpPost]
        [Route("GetProductionOutputItem")]
        public async Task<ProductionOutputModel> GetProductionOutputItem(ProductionOutputModel productionOutputModel)
        {
            var context = new DataService(_config, productionOutputModel.CompanyName);
            var nquery = context.Context.ItemLedgerEntries.Where(r => r.Item_No == productionOutputModel.ItemNo && r.Batch_No == productionOutputModel.BatchNo);
            DataServiceQuery<NAV.ItemLedgerEntries> query = (DataServiceQuery<NAV.ItemLedgerEntries>)nquery;
            List<APPTransferOrderLinesModel> aPPTransferOrderLinesModels = new List<APPTransferOrderLinesModel>();
            TaskFactory<IEnumerable<NAV.ItemLedgerEntries>> taskFactory = new TaskFactory<IEnumerable<NAV.ItemLedgerEntries>>();
            IEnumerable<NAV.ItemLedgerEntries> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

            var itemLedgerEntry = result.LastOrDefault();

            var s = _context.ProductionOutput.FirstOrDefault(c => c.ProductionOrderNo == productionOutputModel.ProductionOrderNo && c.DrumNo == productionOutputModel.DrumNo && c.SubLotNo == productionOutputModel.SubLotNo);

            if (s != null)
            {
                var productionOutputModelItem = new ProductionOutputModel
                {
                    ProductionOutputId = s.ProductionOutputId,
                    LocationName = s.LocationName,
                    ProductionEntryId = s.ProductionEntryId,
                    SubLotNo = s.SubLotNo,
                    DrumNo = s.DrumNo,
                    Description = s.Description,
                    BatchNo = s.BatchNo,
                    QCRefNo = itemLedgerEntry?.QC_Ref_No,
                    LotNo = itemLedgerEntry?.Lot_No,
                    OutputQty = s.OutputQty,
                    NetWeight = s.NetWeight,
                    Buom = s.Buom,
                    ItemId = s.ItemId,
                    ItemNo = s.ItemNo,
                    IsLotComplete = s.IsLotComplete,
                    IsProdutionOrderComplete = s.IsProdutionOrderComplete,
                    ProductionOrderNo = s.ProductionOrderNo,
                };

                return productionOutputModelItem;
            }
            return new ProductionOutputModel();
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertProductionOutput")]
        public async Task<ProductionOutputModel> Post(ProductionOutputModel value)
        {
            try
            {
                if (_context.ProductionOutput.Any(a => a.SubLotNo == value.SubLotNo && a.DrumNo == value.DrumNo && a.ProductionOrderNo == value.ProductionOrderNo))
                {
                    value.IsError = true;
                    value.Errormessage = "Production Output already scanned for this drum - " + value.DrumNo + " & sublot - " + value.SubLotNo + " no.";
                    return value;
                }

                var context = new DataService(_config, value.CompanyName);
                var Company = value.CompanyName;


                var ndrumpquery = context.Context.DrumWeight.Where(i => i.Item_No == value.ItemNo);
                DataServiceQuery<NAV.DrumWeight> drumquery = (DataServiceQuery<NAV.DrumWeight>)ndrumpquery;
                TaskFactory<IEnumerable<NAV.DrumWeight>> taskFactory1 = new TaskFactory<IEnumerable<NAV.DrumWeight>>();
                var drumWeights = await taskFactory1.FromAsync(drumquery.BeginExecute(null, null), iar => drumquery.EndExecute(iar));
                var drum = drumWeights.FirstOrDefault();
                if (drum != null)
                {
                    value.OutputQty = value.OutputQty - drum.Drum_Weight.GetValueOrDefault(0);
                }

                var navItem = _context.Navitems.FirstOrDefault(n => n.No == value.ItemNo);
                var productionOutput = new ProductionOutput
                {
                    LocationName = value.LocationName,
                    ProductionEntryId = value.ProductionEntryId,
                    SubLotNo = value.SubLotNo,
                    ProductionLineNo = value.SubLotNo,
                    DrumNo = value.DrumNo,
                    Description = value.Description,
                    BatchNo = value.BatchNo,
                    OutputQty = value.OutputQty,
                    NetWeight = value.NetWeight,
                    Buom = value.Buom,
                    ItemId = navItem?.ItemId,
                    ItemNo = navItem?.No,
                    IsLotComplete = value.IsLotComplete,
                    IsProdutionOrderComplete = value.IsProdutionOrderComplete,
                    ProductionOrderNo = value.ProductionOrderNo,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = 1,
                    IsPostedToNav = false
                };

                _context.ProductionOutput.Add(productionOutput);
                _context.SaveChanges();
                value.ProductionOutputId = productionOutput.ProductionOutputId;

                //if (value.IsLotComplete.GetValueOrDefault(false))
                //{

                //    value.OutputQty = prodOutputLines.Sum(s => s.OutputQty.GetValueOrDefault(0));
                var navStatus = await PostNAVOutputQty(value);
                if (!navStatus)
                {
                    var deleteExist = _context.ProductionOutput.FirstOrDefault(f => f.ProductionOutputId == value.ProductionOutputId);
                    if (deleteExist != null)
                    {
                        _context.ProductionOutput.Remove(deleteExist);
                        _context.SaveChanges();
                    }
                    value.IsError = true;
                    value.Errormessage = exceptionMessage;
                    return value;
                }
                else
                {
                    //var context = new DataService(_config, value.CompanyName);
                    //var Company = value.CompanyName;


                    //var nquery = context.Context.ProdOrderOutputLine.Where(i => i.Prod_Order_No == value.ProductionOrderNo && i.Item_No == value.ItemNo);
                    //DataServiceQuery<NAV.ProdOrderOutputLine> query = (DataServiceQuery<NAV.ProdOrderOutputLine>)nquery;
                    //TaskFactory<IEnumerable<NAV.ProdOrderOutputLine>> taskFactory1 = new TaskFactory<IEnumerable<NAV.ProdOrderOutputLine>>();
                    //var prodLine = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                    var prodOutputLines = _context.ProductionOutput.Where(f => f.ProductionOrderNo == value.ProductionOrderNo && f.ProductionLineNo == value.SubLotNo
                                           && f.IsPostedToNav == false
                                           && f.StatusCodeId == 1).ToList();

                    //var prodLines = prodLine.ToList();

                    prodOutputLines.ForEach(f =>
                    {
                        f.IsPostedToNav = true;
                        //f.SubLotNo = prodLines.FirstOrDefault().
                    });
                    _context.SaveChanges();
                }
                //}
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        [HttpPost]
        [Route("GetProductionOrderInformation")]
        public async Task<ProductionOutputModel> GetProductionOrderInformation(ProductionOutputModel value)
        {
            try
            {
                var context = new DataService(_config, value.CompanyName);
                var Company = value.CompanyName;


                var nquery = context.Context.CPS_ProdOrderLine.Where(i => i.Prod_Order_No == value.ProductionOrderNo && i.Item_No == value.ItemNo);
                DataServiceQuery<NAV.CPS_ProdOrderLine> query = (DataServiceQuery<NAV.CPS_ProdOrderLine>)nquery;
                TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>> taskFactory1 = new TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>>();
                var prodLine = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                var prodList = prodLine.FirstOrDefault();
                value.Buom = prodList.Unit_of_Measure_Code;
                value.Description = prodList.Description;
                value.BatchNo = prodList.Batch_No;
                value.ItemNo = prodList.Item_No;
                return value;
            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }

        }

        string exceptionMessage = "Update NAV Output Qty Failed.";
        private async Task<bool> PostNAVOutputQty(ProductionOutputModel model)
        {
            try
            {
                var context = new DataService(_config, model.CompanyName);
                var Company = model.CompanyName;


                //var ndrumpquery = context.Context.DrumWeight.Where(i => i.Item_No == model.ItemNo);
                //DataServiceQuery<NAV.DrumWeight> drumquery = (DataServiceQuery<NAV.DrumWeight>)ndrumpquery;
                //TaskFactory<IEnumerable<NAV.DrumWeight>> taskFactory1 = new TaskFactory<IEnumerable<NAV.DrumWeight>>();
                //var drumWeights = await taskFactory1.FromAsync(drumquery.BeginExecute(null, null), iar => drumquery.EndExecute(iar));
                //var drum = drumWeights.FirstOrDefault();



                var userAcc = _context.ApplicationUser.FirstOrDefault(f => f.UserId == model.AddedByUserID);

                var prodEntry = new NAV.ProductionEntry()
                {
                    User_ID = userAcc != null ? userAcc.UserName : model.AddedByUserID.ToString(),
                    // Base_Unit_of_Measure = prodList?.Unit_of_Measure_Code,
                    // Batch_No = model.BatchNo,
                    /// Description = model.Description,
                    // End_Date = DateTime.Now,
                    Entry_No = (int)model.ProductionOutputId,
                    Session_Type = "Mobile",
                    Session_User_ID = userAcc != null ? userAcc.LoginId : model.AddedByUserID.ToString(),
                    // Item_No = model.ItemNo,
                    // Lot_No = model.SubLotNo,
                    // Location_Code = model.LocationName,
                    Production_Status = "Released",
                    //Production_Order_No = model.ProductionOrderNo,
                    //Prod_Order_Line_No = prodList?.Line_No,
                    //Quantity = model.OutputQty,
                };
                context.Context.AddToProductionEntry(prodEntry);
                var entryNo = (int)model.ProductionOutputId;
                try
                {
                    TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                    var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));


                    var nquery1 = context.Context.ProductionEntry.Where(i => i.Entry_No == entryNo);
                    DataServiceQuery<NAV.ProductionEntry> query1 = (DataServiceQuery<NAV.ProductionEntry>)nquery1;
                    TaskFactory<IEnumerable<NAV.ProductionEntry>> taskFactory2 = new TaskFactory<IEnumerable<NAV.ProductionEntry>>();
                    var prodEntryUpdate = await taskFactory2.FromAsync(query1.BeginExecute(null, null), iar => query1.EndExecute(iar));

                    var objeToUpdate = prodEntryUpdate.FirstOrDefault();
                    objeToUpdate.Production_Order_No = model.ProductionOrderNo;
                    objeToUpdate.Item_No = model.ItemNo;
                    objeToUpdate.Quantity = model.OutputQty;
                    objeToUpdate.Quantity_Base = model.OutputQty;
                    objeToUpdate.Prod_Order_Line_No = int.Parse(model.SubLotNo);

                    context.Context.UpdateObject(objeToUpdate);


                    TaskFactory<DataServiceResponse> updateFactory = new TaskFactory<DataServiceResponse>();
                    var operationResponses = await updateFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                }


                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        if (!ex.InnerException.Message.Contains("Another user has already changed the record."))
                        {
                            var error = ex.InnerException != null ? ex.InnerException.Message : "";
                            exceptionMessage = ex.Message + " " + error;
                            return false;
                        }
                    }
                }


                var post = new WebIntegration.WebIntegration_PortClient();

                post.Endpoint.Address =
           new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/WebIntegration"),
           new DnsEndpointIdentity(""));

                post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                post.ClientCredentials.Windows.AllowedImpersonationLevel =
                System.Security.Principal.TokenImpersonationLevel.Impersonation;
                entryNo = (int)model.ProductionOutputId;
                var result = await post.FnInitNewRecordsAsync(entryNo);

                if (model.IsProdutionOrderComplete.GetValueOrDefault(false))
                {
                    post = new WebIntegration.WebIntegration_PortClient();

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/WebIntegration"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                    System.Security.Principal.TokenImpersonationLevel.Impersonation;
                    var lineNo = int.Parse(model.SubLotNo);
                    await post.FnUpdateCompleteOrderbyLineAsync(model.ProductionOrderNo, lineNo);
                }


                var postoutput = new PostOutput.PostOutput_PortClient();

                postoutput.Endpoint.Address =
           new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/PostOutput"),
           new DnsEndpointIdentity(""));

                postoutput.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                postoutput.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                postoutput.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                postoutput.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                postoutput.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                postoutput.ClientCredentials.Windows.AllowedImpersonationLevel =
                System.Security.Principal.TokenImpersonationLevel.Impersonation;
                entryNo = (int)model.ProductionOutputId;
                await postoutput.FnPostFromWebAsync(entryNo);


                return true;
            }
            catch (Exception ex)
            {
                var error = ex.InnerException != null ? ex.InnerException.Message : "";
                exceptionMessage = ex.Message + " " + error;
                return false;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionOutput")]
        public ProductionOutputModel Put(ProductionOutputModel value)
        {
            var productionOutput = _context.ProductionOutput.SingleOrDefault(p => p.ProductionOutputId == value.ProductionOutputId);

            productionOutput.ProductionEntryId = value.ProductionEntryId;
            productionOutput.LocationName = value.LocationName;
            productionOutput.ProductionOrderNo = value.ProductionOrderNo;
            productionOutput.SubLotNo = value.SubLotNo;
            productionOutput.DrumNo = value.DrumNo;
            productionOutput.ItemNo = value.ItemNo;
            productionOutput.ItemId = value.ItemId;
            productionOutput.Description = value.Description;
            productionOutput.BatchNo = value.BatchNo;
            productionOutput.OutputQty = value.OutputQty;
            productionOutput.NetWeight = value.NetWeight;
            productionOutput.ModifiedByUserId = value.ModifiedByUserID.Value;
            productionOutput.ModifiedDate = DateTime.Now;
            productionOutput.StatusCodeId = value.StatusCodeID.Value;
            productionOutput.IsLotComplete = value.IsLotComplete;
            productionOutput.IsProdutionOrderComplete = value.IsProdutionOrderComplete;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionOutput")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var productionOutput = _context.ProductionOutput.SingleOrDefault(p => p.ProductionOutputId == id);
                if (productionOutput != null)
                {
                    _context.ProductionOutput.Remove(productionOutput);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GetProductionOutputReports")]
        public List<ProductionOutputReportModel> GetProductionOutputReports(SearchModel searchModel)
        {
            List<ProductionOutputReportModel> productionOutputModels = new List<ProductionOutputReportModel>();
            var companyNavItemIds = _context.Navitems.Where(n => n.Company == searchModel.CompanyName).AsNoTracking().Select(i => i.ItemId).ToList();

            var productionEntry = _context.ProductionEntry.Where(c => c.Company == searchModel.CompanyName && c.ActualStartDate.Value.Date == searchModel.Date.Value.Date).OrderByDescending(o => o.ProductionEntryId).AsNoTracking().ToList();
            var productionOutputs = _context.ProductionOutput.Include(c => c.Item).Where(i => i.ItemId > 0 && i.Item.Company== searchModel.CompanyName  && companyNavItemIds.Contains(i.ItemId.Value) && i.AddedDate.Value.Date == searchModel.Date.Value.Date).OrderByDescending(o => o.ProductionOutputId).AsNoTracking().ToList();
            var apptransferOrders = _context.AppconsumptionEntry.Include(a => a.AppconsumptionLines).Where(s => s.IsNewEntry == true && s.Company == searchModel.CompanyName && s.AddedDate.Value.Date == searchModel.Date.Value.Date).OrderByDescending(o => o.ConsumptionEntryId).AsNoTracking().ToList();
            productionOutputs.ForEach(s =>
            {
                var replan = s.ProductionOrderNo.Split('-');
                var replanRefNo = replan.Any() ? replan[0] + "-" + replan[1] : "";
                var productionOutputModel = new ProductionOutputReportModel
                {
                    ProductionId = s.ProductionOutputId,
                    LocationName = s.LocationName,
                    ProductionEntryId = s.ProductionEntryId,
                    SubLotNo = s.SubLotNo,
                    DrumNo = s.DrumNo,
                    //Description = s.Description,
                    BatchNo = s.BatchNo,
                    OutputQty = s.OutputQty,
                    NetWeight = s.NetWeight,
                    Buom = s.Buom,
                    ItemId = s.ItemId,
                    ItemNo = s.ItemNo,
                    ItemName = s.Item?.Description,
                    IsLotComplete = s.IsLotComplete,
                    IsProdutionOrderComplete = s.IsProdutionOrderComplete,
                    ProductionOrderNo = s.ProductionOrderNo,
                    ReplanRefNo = replanRefNo,
                    Action = "Output",
                    PlanDate = s.AddedDate,
                    PlanTime = s.AddedDate,
                };
                productionOutputModels.Add(productionOutputModel);
            });
            productionEntry.ForEach(s =>
            {
                var replan = s.ProductionOrderNo.Split('-');
                var replanRefNo = replan.Any() ? replan[0] + "-" + replan[1] : "";
                var productionOutputModel = new ProductionOutputReportModel
                {
                    ProductionId = s.ProductionEntryId,
                    LocationName = s.LocationName,
                    ProductionEntryId = s.ProductionEntryId,
                    //Description = s.Deascription,
                    BatchNo = s.BatchNo,
                    ItemName = s.ItemName,
                    ProductionOrderNo = s.ProductionOrderNo,
                    Action = "Start",
                    ReplanRefNo = replanRefNo,
                    PlanDate = s.ActualStartDate,
                    PlanTime = s.ActualStartDate,
                };
                productionOutputModels.Add(productionOutputModel);
            });

            apptransferOrders.ForEach(s =>
            {
                if (s.AppconsumptionLines.Count > 0)
                {
                    s.AppconsumptionLines.ToList().ForEach(a =>
                    {
                        var replan = s.ProdOrderNo.Split('-');
                        var replanRefNo = "";
                        if (replan.Length > 2)
                        {
                            replanRefNo = replan.Any() ? replan[0] + "-" + replan[1] : "";
                        }
                        ProductionOutputReportModel productionOutputModel = new ProductionOutputReportModel();
                        productionOutputModel.ProductionId = a.ConsumptionLineId;
                        productionOutputModel.SubLotNo = a.ProdLineNo != null ? a.ProdLineNo / 10000 > 0 ? (a.ProdLineNo / 10000).ToString() : string.Empty : string.Empty;
                        productionOutputModel.Description = a.Description;
                        productionOutputModel.BatchNo = a.QcrefNo;
                        productionOutputModel.OutputQty = a.Quantity;
                        productionOutputModel.Buom = a.Uom;
                        productionOutputModel.ItemNo = a.ItemNo;
                        productionOutputModel.ItemName = s.Description;
                        productionOutputModel.Action = "Consumption";
                        productionOutputModel.ProductionOrderNo = s.ProdOrderNo;
                        productionOutputModel.ReplanRefNo = replanRefNo;
                        productionOutputModel.PlanDate = a.AddedDate;
                        productionOutputModel.PlanTime = a.AddedDate;
                        if (productionOutputModels.Any(p => p.Action == "Consumption" && p.ItemName == s.Description))
                        {
                            productionOutputModel.ItemName = string.Empty;
                        }
                        productionOutputModels.Add(productionOutputModel);
                    });
                }
            });

            var outputGrouping = productionOutputModels.ToList();
            List<string> locationNames = outputGrouping.Where(s => s.LocationName != string.Empty).Select(l => l.LocationName).ToList();
            var locationMasters = _context.Ictmaster.Where(c => c.MasterType == 572 && locationNames.Contains(c.Description)).ToList();
            List<ProductionOutputReportModel> outputGroupingModels = new List<ProductionOutputReportModel>();
            if(!string.IsNullOrEmpty(searchModel.Search))
            {
                List<string> itemNames = new List<string>();
                itemNames.AddRange(productionOutputModels.Where(w => w.ItemName!=null && searchModel.Search.Contains(w.ItemName)).Select(s => s.ItemName).ToList());
                itemNames.AddRange(productionOutputModels.Where(w => w.ProductionOrderNo != null && searchModel.Search.Contains(w.ProductionOrderNo)).Select(s => s.ItemName).ToList());
                itemNames.AddRange(productionOutputModels.Where(w => w.BatchNo != null && searchModel.Search.Contains(w.BatchNo)).Select(s => s.ItemName).ToList());
                itemNames.AddRange(productionOutputModels.Where(w => w.ItemNo != null &&  searchModel.Search.Contains(w.ItemNo)).Select(s => s.ItemName).ToList());
                var itemNamesAll = itemNames.Distinct().ToList();
                outputGrouping = outputGrouping.Where(w=> itemNamesAll.Contains(w.ItemName)).ToList();
            }
            outputGrouping.ForEach(s =>
                    {
                        var location = locationMasters.FirstOrDefault(d => d.Description == s.LocationName);
                        var pOutputModel = new ProductionOutputReportModel();

                        pOutputModel.ProductionId = s.ProductionId;
                        pOutputModel.LocationName = location != null ? location.LocationDescription : string.Empty;
                        pOutputModel.ProductionEntryId = s.ProductionEntryId;
                        if (s.Action != "Consumption")
                        {
                            pOutputModel.SubLotNo = s.SubLotNo != null && s.SubLotNo != string.Empty ? (Convert.ToInt32(s.SubLotNo) / 10000).ToString() : string.Empty;
                        }
                        else
                        {
                            pOutputModel.SubLotNo = s.SubLotNo;
                        }
                        pOutputModel.DrumNo = s.DrumNo;
                        pOutputModel.Description = s.Description;
                        pOutputModel.BatchNo = s.BatchNo;
                        pOutputModel.OutputQty = s.OutputQty;
                        pOutputModel.Buom = s.Buom;
                        pOutputModel.ItemName = s.ItemName;
                        pOutputModel.ProductionOrderNo = s.ProductionOrderNo;
                        pOutputModel.ReplanRefNo = s.ReplanRefNo;
                        pOutputModel.Action = s.Action;
                        pOutputModel.PlanDate = s.PlanDate;
                        pOutputModel.PlanTime = s.PlanTime;
                        pOutputModel.Date = s.PlanDate.Value.ToString("dd-MMM-yyyy");
                        pOutputModel.Time = s.PlanTime.Value.ToString("HH:mm:ss");
                        outputGroupingModels.Add(pOutputModel);
                    });

            return outputGroupingModels;
        }

        [HttpPost]
        [Route("GetProductionOutputFilter")]
        public List<ProductionOutputModel> GetProductionOutputFilter(ProductionSearchModel value)
        {
            var productionDatefilter = new List<ProductionOutputModel>();
            var productionDateIds = new List<long?>();
            List<ProductionOutputModel> productionOutputModels = new List<ProductionOutputModel>();
            var productionOutputs = _context.ProductionOutput.OrderByDescending(o => o.ProductionEntryId).AsQueryable();
            if (productionOutputs != null && productionOutputs.Count() > 0)
            {

                List<ProductionOutputModel> filteredItems = new List<ProductionOutputModel>();
                if (value.ItemName.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (value.ItemName.ToLower())) : false);
                }
                if (value.ProductionOrderNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ProductionOrderNo != null ? (p.ProductionOrderNo.ToLower() == (value.ProductionOrderNo.ToLower())) : false);
                }
                if (value.BatchNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.BatchNo != null ? (p.BatchNo.ToLower() == (value.BatchNo.ToLower())) : false);
                }
                if (value.Description.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.Description != null ? (p.Description.ToLower() == (value.Description.ToLower())) : false);
                }
                if ((value.StartDate != DateTime.Now.Date) && (value.EndDate != DateTime.Now.Date))
                {
                    productionOutputs = productionOutputs.Where(p => (p.AddedDate.Value.Date >= value.StartDate.Value.Date) && (p.AddedDate.Value.Date <= value.EndDate.Value.Date));
                }


                var userIds = productionOutputs.Select(s => s.AddedByUserId).ToList();
                var codeMasterIds = productionOutputs.Select(s => s.StatusCodeId).ToList();
                userIds.AddRange(productionOutputs.Select(s => s.ModifiedByUserId.GetValueOrDefault(-1)).ToList());

                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(u => userIds.Contains(u.UserId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(c => codeMasterIds.Contains(c.CodeId)).AsNoTracking().ToList();
                List<string> locationNames = productionOutputs.Where(s => s.LocationName != string.Empty).Select(l => l.LocationName).ToList();
                var locationMasters = _context.Ictmaster.Where(c => c.MasterType == 572 && locationNames.Contains(c.Description)).ToList();
                foreach (var s in productionOutputs)
                {
                    var location = locationMasters.FirstOrDefault(d => d.Description == s.LocationName);
                    var productionOutputModel = new ProductionOutputModel
                    {
                        ProductionOutputId = s.ProductionOutputId,
                        LocationName = location != null ? location.LocationDescription : string.Empty,
                        ProductionEntryId = s.ProductionEntryId,
                        SubLotNo = s.SubLotNo != null && s.SubLotNo != string.Empty ? (Convert.ToInt32(s.SubLotNo) / 10000).ToString() : string.Empty,
                        DrumNo = s.DrumNo,
                        Description = s.Description,
                        BatchNo = s.BatchNo,
                        OutputQty = s.OutputQty == null ? 0 : s.OutputQty,
                        NetWeight = s.NetWeight == null ? 0 : s.NetWeight,
                        Buom = s.Buom,
                        ItemId = s.ItemId,
                        ItemNo = s.ItemNo,
                        IsLotComplete = s.IsLotComplete,
                        IsProdutionOrderComplete = s.IsProdutionOrderComplete,
                        ProductionOrderNo = s.ProductionOrderNo,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.AddedByUserId)?.UserName : "",
                        StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : "",
                        ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,

                    };
                    productionOutputModels.Add(productionOutputModel);
                }
            }



            return productionOutputModels;
        }

        [HttpPost]
        [Route("GetProductionOutputExcel")]
        public IActionResult GetProductionOutputExcel(SearchModel searchModel)
        {
            var productionEntry = _context.ProductionEntry.Select(s => new { s.ProductionOrderNo, s.ActualStartDate, s.StartDate }).ToList();
            var productionOrderNo = productionEntry.Select(s => s.ProductionOrderNo).Distinct().ToList();
            var productionOutputs = _context.ProductionOutput.Include(n => n.Item).Where(w => productionOrderNo.Contains(w.ProductionOrderNo)).OrderByDescending(o => o.ProductionEntryId).AsQueryable();
            if (searchModel.ProductionSearchModel != null)
            {
                if (searchModel.ProductionSearchModel.ItemName.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (searchModel.ProductionSearchModel.ItemName.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.ProductionOrderNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ProductionOrderNo != null ? (p.ProductionOrderNo.ToLower() == (searchModel.ProductionSearchModel.ProductionOrderNo.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.BatchNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.BatchNo != null ? (p.BatchNo.ToLower() == (searchModel.ProductionSearchModel.BatchNo.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.Description.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.Description != null ? (p.Description.ToLower() == (searchModel.ProductionSearchModel.Description.ToLower())) : false);
                }
                if ((searchModel.StartDate != null) && (searchModel.EndDate != null))
                {
                    productionOutputs = productionOutputs.Where(p => (p.AddedDate.Value.Date >= searchModel.StartDate.Value.Date) && (p.AddedDate.Value.Date <= searchModel.EndDate.Value.Date));
                }
                if (searchModel.PlantId != null)
                {
                    var plants = _context.Plant.FirstOrDefault(w => w.PlantId == searchModel.PlantId && w.NavCompanyName != null)?.NavCompanyName;
                    if (plants != null)
                    {
                        productionOutputs = productionOutputs.Where(p => p.Item.Company == plants);

                    }
                }
            }
            var userIds = productionOutputs.Select(s => s.AddedByUserId).ToList();
            var codeMasterIds = productionOutputs.Select(s => s.StatusCodeId).ToList();
            userIds.AddRange(productionOutputs.Select(s => s.ModifiedByUserId.GetValueOrDefault(-1)).ToList());

            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(u => userIds.Contains(u.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(c => codeMasterIds.Contains(c.CodeId)).AsNoTracking().ToList();
            List<string> locationNames = productionOutputs.Where(s => s.LocationName != string.Empty).Select(l => l.LocationName).ToList();
            var locationMasters = _context.Ictmaster.Where(c => c.MasterType == 572 && locationNames.Contains(c.Description)).ToList();
            var results = productionOutputs.ToList();
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Output Date");
            headers.Add("Time");
            headers.Add("Location Name");
            headers.Add("Production Order No");
            headers.Add("Item No");
            headers.Add("Description");
            headers.Add("Batch no");
            headers.Add("Sub Lot No");
            headers.Add("Drum No");
            headers.Add("Gross Weight");
            headers.Add("Net Weight");
            headers.Add("User Name");
            string newFolderName = "WikiExcel";
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var SessionId = Guid.NewGuid();
            string FromLocation = folderName + @"\" + newFolderName + @"\" + SessionId + ".xlsx";

            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = "Production Output" };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);
                Row headerRow = new Row();
                foreach (string column in headers)
                {
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                results.ForEach(d =>
                {
                    var location = locationMasters.FirstOrDefault(a => a.Description == d.LocationName);
                    var productionEntrys = productionEntry.FirstOrDefault(f => f.ProductionOrderNo.ToLower() == d.ProductionOrderNo.ToLower());
                    Row row = new Row();
                    row.AppendChild(AddCellColumn(d.AddedDate != null ? d.AddedDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.AddedDate != null ? d.AddedDate?.ToString("hh:mm tt") : ""));
                    row.AppendChild(AddCellColumn(location != null ? location.LocationDescription : ""));
                    row.AppendChild(AddCellColumn(d.ProductionOrderNo));
                    row.AppendChild(AddCellColumn(d.ItemNo));
                    row.AppendChild(AddCellColumn(d.Description));
                    row.AppendChild(AddCellColumn(d.BatchNo));
                    row.AppendChild(AddCellColumn(d.SubLotNo != null && d.SubLotNo != string.Empty ? (Convert.ToInt32(d.SubLotNo) / 10000).ToString() : string.Empty));
                    row.AppendChild(AddCellColumn(d.DrumNo));
                    row.AppendChild(AddCellColumn(d.NetWeight != null ? d.NetWeight.ToString() : "0"));
                    row.AppendChild(AddCellColumn(d.OutputQty != null ? d.OutputQty.ToString() :"0"));
                    row.AppendChild(AddCellColumn(appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == d.AddedByUserId)?.UserName : ""));
                    sheetData.AppendChild(row);
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        [HttpPost]
        [Route("GetProductionOutputReportExcel")]
        public IActionResult GetProductionOutputReportExcel(SearchModel searchModel)
        {
            var productionDatefilter = new List<ProductionOutputModel>();
            var productionDateIds = new List<long?>();
            List<ProductionOutputModel> productionOutputModels = new List<ProductionOutputModel>();
            var productionEntry = _context.ProductionEntry.Select(s => new { s.ProductionOrderNo, s.ActualStartDate, s.StartDate }).ToList();
            var productionOrderNo = productionEntry.Select(s => s.ProductionOrderNo).Distinct().ToList();
            var productionOutputs = _context.ProductionOutput.Include(n => n.Item).Where(w => productionOrderNo.Contains(w.ProductionOrderNo)).OrderByDescending(o => o.ProductionEntryId).AsQueryable();


            List<ProductionOutputModel> filteredItems = new List<ProductionOutputModel>();
            if (searchModel.ProductionSearchModel != null)
            {
                if (searchModel.ProductionSearchModel.ItemName.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (searchModel.ProductionSearchModel.ItemName.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.ProductionOrderNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.ProductionOrderNo != null ? (p.ProductionOrderNo.ToLower() == (searchModel.ProductionSearchModel.ProductionOrderNo.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.BatchNo.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.BatchNo != null ? (p.BatchNo.ToLower() == (searchModel.ProductionSearchModel.BatchNo.ToLower())) : false);
                }
                if (searchModel.ProductionSearchModel.Description.Any())
                {
                    productionOutputs = productionOutputs.Where(p => p.Description != null ? (p.Description.ToLower() == (searchModel.ProductionSearchModel.Description.ToLower())) : false);
                }
                if ((searchModel.ProductionSearchModel.StartDate != null) && (searchModel.ProductionSearchModel.EndDate != null))
                {
                    productionOutputs = productionOutputs.Where(p => (p.AddedDate.Value.Date >= searchModel.ProductionSearchModel.StartDate.Value.Date) && (p.AddedDate.Value.Date <= searchModel.ProductionSearchModel.EndDate.Value.Date));
                }
                if (searchModel.ProductionSearchModel.PlantId != null)
                {
                    var plants = _context.Plant.FirstOrDefault(w => w.PlantId == searchModel.ProductionSearchModel.PlantId && w.NavCompanyName != null)?.NavCompanyName;
                    if (plants != null)
                    {
                        productionOutputs = productionOutputs.Where(p => p.Item.Company == plants);

                    }
                }
            }
            var userIds = productionOutputs.Select(s => s.AddedByUserId).ToList();
            var codeMasterIds = productionOutputs.Select(s => s.StatusCodeId).ToList();
            userIds.AddRange(productionOutputs.Select(s => s.ModifiedByUserId.GetValueOrDefault(-1)).ToList());

            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(u => userIds.Contains(u.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(c => codeMasterIds.Contains(c.CodeId)).AsNoTracking().ToList();
            List<string> locationNames = productionOutputs.Where(s => s.LocationName != string.Empty).Select(l => l.LocationName).ToList();
            var locationMasters = _context.Ictmaster.Where(c => c.MasterType == 572 && locationNames.Contains(c.Description)).ToList();
            var results = productionOutputs.ToList();

            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Output Date");
            headers.Add("Location Name");
            headers.Add("Production Order No");
            headers.Add("Start Date");
            headers.Add("Actual Start Date");
            
            headers.Add("Item No");
            headers.Add("Description");
            headers.Add("Batch no");
            headers.Add("Sub Lot No");
            headers.Add("Drum No");
            headers.Add("Gross Weight");
            headers.Add("Net Weight");
            headers.Add("User Name");
            string newFolderName = "WikiExcel";
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var SessionId = Guid.NewGuid();
            string FromLocation = folderName + @"\" + newFolderName + @"\" + SessionId + ".xlsx";

            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = "Production Output" };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);
                Row headerRow = new Row();
                foreach (string column in headers)
                {
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                results.ForEach(d =>
                {
                    var location = locationMasters.FirstOrDefault(a => a.Description == d.LocationName);
                    var productionEntrys = productionEntry.FirstOrDefault(f => f.ProductionOrderNo.ToLower() == d.ProductionOrderNo.ToLower());
                    Row row = new Row();
                    row.AppendChild(AddCellColumn(d.AddedDate != null ? d.AddedDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(location != null ? location.LocationDescription : ""));
                    row.AppendChild(AddCellColumn(d.ProductionOrderNo));
                    row.AppendChild(AddCellColumn(productionEntrys != null ? productionEntrys.StartDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(productionEntrys != null ? productionEntrys.ActualStartDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.ItemNo));
                    row.AppendChild(AddCellColumn(d.Description));
                    row.AppendChild(AddCellColumn(d.BatchNo));
                    row.AppendChild(AddCellColumn(d.SubLotNo != null && d.SubLotNo != string.Empty ? (Convert.ToInt32(d.SubLotNo) / 10000).ToString() : string.Empty));
                    row.AppendChild(AddCellColumn(d.DrumNo));
                    row.AppendChild(AddCellColumn(d.NetWeight != null ? d.NetWeight.ToString() : "0"));
                    row.AppendChild(AddCellColumn(d.OutputQty != null ? d.OutputQty.ToString() : "0"));
                    row.AppendChild(AddCellColumn(appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == d.AddedByUserId)?.UserName : ""));
                    sheetData.AppendChild(row);
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        private Cell AddCellColumn(string value)
        {
            Cell fileNamecell = new Cell();
            fileNamecell.DataType = CellValues.String;
            fileNamecell.CellValue = new CellValue(value);
            return fileNamecell;
        }
    }
}
