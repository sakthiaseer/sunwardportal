﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class LanguageMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public LanguageMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetLanguageMasters")]
        public List<LanguageMasterModel> Get()
        {
            var languageMaster = _context.LanguageMaster.Select(s => new LanguageMasterModel
            {
                LanguageID = s.LanguageId,
                Code = s.Code,
                Name = s.Name,
                Description = s.Description

            }).OrderByDescending(o => o.LanguageID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<LanguageMasterModel>>(LanguageMaster);
            return languageMaster;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get LanguageMaster")]
        [HttpGet("GetLanguageMasters/{id:int}")]
        public ActionResult<LanguageMasterModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var languageMaster = _context.LanguageMaster.SingleOrDefault(p => p.LanguageId == id.Value);
            var result = _mapper.Map<LanguageMasterModel>(languageMaster);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<LanguageMasterModel> GetData(SearchModel searchModel)
        {
            var languageMaster = new LanguageMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).FirstOrDefault();
                        break;
                    case "Last":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).LastOrDefault();
                        break;
                    case "Next":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).LastOrDefault();
                        break;
                    case "Previous":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).FirstOrDefault();
                        break;
                    case "Last":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).LastOrDefault();
                        break;
                    case "Next":
                        languageMaster = _context.LanguageMaster.OrderBy(o => o.LanguageId).FirstOrDefault(s => s.LanguageId > searchModel.Id);
                        break;
                    case "Previous":
                        languageMaster = _context.LanguageMaster.OrderByDescending(o => o.LanguageId).FirstOrDefault(s => s.LanguageId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<LanguageMasterModel>(languageMaster);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertLanguageMaster")]
        public LanguageMasterModel Post(LanguageMasterModel value)
        {
            var languageMaster = new LanguageMaster
            {

                Code = value.Code,
                Name = value.Name,
                Description = value.Description

            };
            _context.LanguageMaster.Add(languageMaster);
            _context.SaveChanges();
            value.LanguageID = languageMaster.LanguageId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateLanguageMaster")]
        public LanguageMasterModel Put(LanguageMasterModel value)
        {
            var languageMaster = _context.LanguageMaster.SingleOrDefault(p => p.LanguageId == value.LanguageID);

            languageMaster.Code = value.Code;
            languageMaster.Name = value.Name;

            languageMaster.Description = value.Description;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteLanguageMaster")]
        public void Delete(int id)
        {
            var languageMaster = _context.LanguageMaster.SingleOrDefault(p => p.LanguageId == id);
            if (languageMaster != null)
            {
                _context.LanguageMaster.Remove(languageMaster);
                _context.SaveChanges();
            }
        }
    }
}