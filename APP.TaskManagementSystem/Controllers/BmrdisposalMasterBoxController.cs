﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BmrdisposalMasterBoxController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public BmrdisposalMasterBoxController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpPost]
        [Route("GetBmrdisposalMasterBoxByRefByNo")]
        public List<BmrdisposalMasterBoxModel> Get(RefSearchModel refSearchModel)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<BmrdisposalMasterBoxModel> BmrdisposalMasterBoxModels = new List<BmrdisposalMasterBoxModel>();
            var BmrdisposalMasterBox = _context.BmrdisposalMasterBox.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(o => o.Location).Include(o => o.Location.ParentIct).Include(o => o.Location.ParentIct.ParentIct).Include(p => p.Company).AsNoTracking().ToList();
            if (BmrdisposalMasterBox != null && BmrdisposalMasterBox.Count > 0)
            {
                BmrdisposalMasterBox.ForEach(s =>
                {
                    BmrdisposalMasterBoxModel BmrdisposalMasterBoxModel = new BmrdisposalMasterBoxModel
                    {
                        BmrdisposalMasterBoxId = s.BmrdisposalMasterBoxId,
                        CompanyId = s.CompanyId,
                        CompanyDatabaseName = s.Company != null ? s.Company.Description : null,
                        TypeOfDisposalBoxId = s.TypeOfDisposalBoxId,
                        BoxNo = s.BoxNo,
                        DisposalDate = s.DisposalDate,
                        EstimateNumberOfBmr = s.EstimateNumberOfBmr,
                        LocationId = s.LocationId,
                        LocationName = s.Location != null ? s.Location.Name : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        TypeOfDisposalBoxName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TypeOfDisposalBoxId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        Area = s.Location != null && s.Location.ParentIct != null ? s.Location.ParentIct.Name : null,
                        SpecificAreaName = s.Location != null && s.Location.ParentIct != null && s.Location.ParentIct.ParentIct != null ? s.Location.ParentIct.ParentIct.Name : null,
                    };
                    BmrdisposalMasterBoxModels.Add(BmrdisposalMasterBoxModel);
                });
            }

            if (refSearchModel.IsHeader)
            {
                return BmrdisposalMasterBoxModels.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BmrdisposalMasterBoxId).ToList();
            }
            return BmrdisposalMasterBoxModels.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.BmrdisposalMasterBoxId).ToList();
        }

        [HttpGet]
        [Route("GetBmrdisposalMasterBoxs")]
        public List<BmrdisposalMasterBoxModel> GetBmrdisposalMasterBoxs(string RefNo)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<BmrdisposalMasterBoxModel> BmrdisposalMasterBoxModels = new List<BmrdisposalMasterBoxModel>();
            var BmrdisposalMasterBox = _context.BmrdisposalMasterBox.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(o => o.Location).Include(o => o.Location.ParentIct).Include(o => o.Location.ParentIct.ParentIct).Include(p => p.Company).AsNoTracking().ToList();
            var profileAutoNumber = _context.ProfileAutoNumber.Include(c => c.Company).Include(d => d.Department).Include(s => s.Section).Include(b => b.SubSection).Where(w => w.ScreenId == RefNo).ToList();
            if (BmrdisposalMasterBox != null && BmrdisposalMasterBox.Count > 0)
            {
                BmrdisposalMasterBox.ForEach(s =>
                {
                    BmrdisposalMasterBoxModel BmrdisposalMasterBoxModel = new BmrdisposalMasterBoxModel();
                    BmrdisposalMasterBoxModel.BmrdisposalMasterBoxId = s.BmrdisposalMasterBoxId;
                    BmrdisposalMasterBoxModel.CompanyId = s.CompanyId;
                    BmrdisposalMasterBoxModel.CompanyDatabaseName = s.Company != null ? s.Company.Description : null;
                    BmrdisposalMasterBoxModel.TypeOfDisposalBoxId = s.TypeOfDisposalBoxId;
                    BmrdisposalMasterBoxModel.BoxNo = s.BoxNo;
                    BmrdisposalMasterBoxModel.DisposalDate = s.DisposalDate;
                    BmrdisposalMasterBoxModel.EstimateNumberOfBmr = s.EstimateNumberOfBmr;
                    BmrdisposalMasterBoxModel.LocationId = s.LocationId;
                    BmrdisposalMasterBoxModel.LocationName = s.Location != null ? s.Location.Name : null;
                    BmrdisposalMasterBoxModel.StatusCodeID = s.StatusCodeId;
                    BmrdisposalMasterBoxModel.AddedByUserID = s.AddedByUserId;
                    BmrdisposalMasterBoxModel.ModifiedByUserID = s.ModifiedByUserId;
                    BmrdisposalMasterBoxModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                    BmrdisposalMasterBoxModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    BmrdisposalMasterBoxModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    BmrdisposalMasterBoxModel.AddedDate = s.AddedDate;
                    BmrdisposalMasterBoxModel.ModifiedDate = s.ModifiedDate;
                    BmrdisposalMasterBoxModel.TypeOfDisposalBoxName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TypeOfDisposalBoxId).Select(a => a.Value).SingleOrDefault() : "";
                    BmrdisposalMasterBoxModel.ProfileID = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TypeOfDisposalBoxId).Select(a => a.ProfileId).SingleOrDefault() : null;
                    BmrdisposalMasterBoxModel.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                    BmrdisposalMasterBoxModel.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                    BmrdisposalMasterBoxModel.ProfileLinkReferenceNo = s.ProfileLinkReferenceNo;
                    BmrdisposalMasterBoxModel.Area = s.Location != null && s.Location.ParentIct != null ? s.Location.ParentIct.Name : null;
                    BmrdisposalMasterBoxModel.SpecificAreaName = s.Location != null && s.Location.ParentIct != null && s.Location.ParentIct.ParentIct != null ? s.Location.ParentIct.ParentIct.Name : null;
                    var profileAutoNumbers = profileAutoNumber.Where(w => w.ScreenAutoNumberId == s.BmrdisposalMasterBoxId && w.ScreenId == RefNo).FirstOrDefault();
                    if (profileAutoNumbers != null)
                    {
                        DocumentNoSeriesModel DocumentNoSeriesModel = new DocumentNoSeriesModel();
                        DocumentNoSeriesModel.ScreenID = profileAutoNumbers.ScreenId;
                        DocumentNoSeriesModel.ProfileID = profileAutoNumbers.ProfileId;
                        DocumentNoSeriesModel.PlantID = profileAutoNumbers.CompanyId;
                        DocumentNoSeriesModel.DepartmentId = profileAutoNumbers.DepartmentId;
                        DocumentNoSeriesModel.SectionId = profileAutoNumbers.SectionId;
                        DocumentNoSeriesModel.SubSectionId = profileAutoNumbers.SubSectionId;
                        DocumentNoSeriesModel.DepartmentName = profileAutoNumbers.Department != null ? profileAutoNumbers.Department.Name : null;
                        DocumentNoSeriesModel.CompanyCode = profileAutoNumbers.Company != null ? profileAutoNumbers.Company.PlantCode : null;
                        DocumentNoSeriesModel.SectionName = profileAutoNumbers.Section != null ? profileAutoNumbers.Section.Name : null;
                        DocumentNoSeriesModel.SubSectionName = profileAutoNumbers.SubSection != null ? profileAutoNumbers.SubSection.Name : null;
                        BmrdisposalMasterBoxModel.ProfileNoModel = DocumentNoSeriesModel;
                    }
                    BmrdisposalMasterBoxModels.Add(BmrdisposalMasterBoxModel);
                });
            }


            return BmrdisposalMasterBoxModels.OrderByDescending(o => o.BmrdisposalMasterBoxId).ToList();
        }

        [HttpGet]
        [Route("GetBmrdisposalMasterByBoxNo")]
        public BmrdisposalMasterBoxModel GetBmrdisposalMasterByBoxNo(string No)
        {

            BmrdisposalMasterBoxModel bmrdisposalMasterBoxModel = new BmrdisposalMasterBoxModel();
            var bmrdisposalMasterBox = _context.BmrdisposalMasterBox.Where(s => s.BoxNo.Trim() == No.Trim()).FirstOrDefault();
            if (bmrdisposalMasterBox != null)
            {
                bmrdisposalMasterBoxModel = new BmrdisposalMasterBoxModel();
                bmrdisposalMasterBoxModel.BoxNo = bmrdisposalMasterBox.BoxNo;
                bmrdisposalMasterBoxModel.BmrdisposalMasterBoxId = bmrdisposalMasterBox.BmrdisposalMasterBoxId;
                bmrdisposalMasterBoxModel.CompanyId = bmrdisposalMasterBox.CompanyId;
                bmrdisposalMasterBoxModel.TypeOfDisposalBoxId = bmrdisposalMasterBox.TypeOfDisposalBoxId;
                bmrdisposalMasterBoxModel.DisposalDate = bmrdisposalMasterBox.DisposalDate;
                bmrdisposalMasterBoxModel.EstimateNumberOfBmr = bmrdisposalMasterBox.EstimateNumberOfBmr;
                bmrdisposalMasterBoxModel.LocationId = bmrdisposalMasterBox.LocationId;
                bmrdisposalMasterBoxModel.StatusCodeID = bmrdisposalMasterBox.StatusCodeId;
                bmrdisposalMasterBoxModel.AddedByUserID = bmrdisposalMasterBox.AddedByUserId;
                bmrdisposalMasterBoxModel.ModifiedByUserID = bmrdisposalMasterBox.ModifiedByUserId;
                bmrdisposalMasterBoxModel.AddedDate = bmrdisposalMasterBox.AddedDate;
                bmrdisposalMasterBoxModel.ModifiedDate = bmrdisposalMasterBox.ModifiedDate;
                bmrdisposalMasterBoxModel.LinkProfileReferenceNo = bmrdisposalMasterBox.LinkProfileReferenceNo;
                bmrdisposalMasterBoxModel.MasterProfileReferenceNo = bmrdisposalMasterBox.MasterProfileReferenceNo;
                bmrdisposalMasterBoxModel.ProfileLinkReferenceNo = bmrdisposalMasterBox.ProfileLinkReferenceNo;
            }
            return bmrdisposalMasterBoxModel;

        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BmrdisposalMasterBoxModel> GetData(SearchModel searchModel)
        {
            var bmrdisposalMasterBox = new BmrdisposalMasterBox();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).FirstOrDefault();
                        break;
                    case "Last":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).LastOrDefault();
                        break;
                    case "Next":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).LastOrDefault();
                        break;
                    case "Previous":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).FirstOrDefault();
                        break;
                    case "Last":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).LastOrDefault();
                        break;
                    case "Next":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderBy(o => o.BmrdisposalMasterBoxId).FirstOrDefault(s => s.BmrdisposalMasterBoxId > searchModel.Id);
                        break;
                    case "Previous":
                        bmrdisposalMasterBox = _context.BmrdisposalMasterBox.OrderByDescending(o => o.BmrdisposalMasterBoxId).FirstOrDefault(s => s.BmrdisposalMasterBoxId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BmrdisposalMasterBoxModel>(bmrdisposalMasterBox);
            if (result != null)
            {
                var profileAutoNumbers = _context.ProfileAutoNumber.Where(w => w.ScreenAutoNumberId == result.BmrdisposalMasterBoxId && w.ScreenId == searchModel.ScreenID).FirstOrDefault();
                if (profileAutoNumbers != null)
                {
                    DocumentNoSeriesModel DocumentNoSeriesModel = new DocumentNoSeriesModel();
                    DocumentNoSeriesModel.ScreenID = profileAutoNumbers.ScreenId;
                    DocumentNoSeriesModel.ProfileID = profileAutoNumbers.ProfileId;
                    DocumentNoSeriesModel.PlantID = profileAutoNumbers.CompanyId;
                    DocumentNoSeriesModel.DepartmentId = profileAutoNumbers.DepartmentId;
                    DocumentNoSeriesModel.SectionId = profileAutoNumbers.SectionId;
                    DocumentNoSeriesModel.SubSectionId = profileAutoNumbers.SubSectionId;
                    DocumentNoSeriesModel.DepartmentName = profileAutoNumbers.Department != null ? profileAutoNumbers.Department.Name : null;
                    DocumentNoSeriesModel.CompanyCode = profileAutoNumbers.Company != null ? profileAutoNumbers.Company.PlantCode : null;
                    DocumentNoSeriesModel.SectionName = profileAutoNumbers.Section != null ? profileAutoNumbers.Section.Name : null;
                    DocumentNoSeriesModel.SubSectionName = profileAutoNumbers.SubSection != null ? profileAutoNumbers.SubSection.Name : null;
                    result.ProfileNoModel = DocumentNoSeriesModel;
                }
            }
            return result;
        }
        [HttpPost]
        [Route("InsertBmrdisposalMasterBox")]
        public BmrdisposalMasterBoxModel Post(BmrdisposalMasterBoxModel value)
        {
            var profileNo = "";
            if (value.ProfileID != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.BoxNo });
            }
            var TypeOfDisposalBox = _context.ApplicationMasterDetail.Where(w => w.ApplicationMasterDetailId == value.TypeOfDisposalBoxId.Value).Select(p => p.ProfileId).SingleOrDefault();
            var TypeOfDisposalBoxProfile = "";
            var BmrdisposalMasterBox = new BmrdisposalMasterBox
            {
                TypeOfDisposalBoxId = value.TypeOfDisposalBoxId,
                CompanyId = value.CompanyId,
                BoxNo = TypeOfDisposalBoxProfile,
                DisposalDate = value.DisposalDate,
                EstimateNumberOfBmr = value.EstimateNumberOfBmr,
                LocationId = value.LocationId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProfileLinkReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo
            };
            _context.BmrdisposalMasterBox.Add(BmrdisposalMasterBox);
            _context.SaveChanges();
            value.BmrdisposalMasterBoxId = BmrdisposalMasterBox.BmrdisposalMasterBoxId;
            value.BoxNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
            {
                ProfileID = _context.ApplicationMasterDetail.Where(w => w.ApplicationMasterDetailId == value.TypeOfDisposalBoxId.Value).Select(p => p.ProfileId).SingleOrDefault(),
                AddedByUserID = value.AddedByUserID,
                StatusCodeID = 710,
                DepartmentName = value.ProfileNoModel?.DepartmentName,
                CompanyCode = value.ProfileNoModel?.CompanyCode,
                SectionName = value.ProfileNoModel?.SectionName,
                SubSectionName = value.ProfileNoModel?.SubSectionName,
                DepartmentId = value.ProfileNoModel?.DepartmentId,
                PlantID = value.ProfileNoModel?.PlantID,
                SectionId = value.ProfileNoModel?.SectionId,
                SubSectionId = value.ProfileNoModel?.SubSectionId,
                ScreenID = value.ScreenID,
                ScreenAutoNumberId = value.BmrdisposalMasterBoxId,
            });
            value.MasterProfileReferenceNo = BmrdisposalMasterBox.MasterProfileReferenceNo;
            var BmrdisposalMasterBoxs = _context.BmrdisposalMasterBox.SingleOrDefault(p => p.BmrdisposalMasterBoxId == value.BmrdisposalMasterBoxId);
            BmrdisposalMasterBoxs.BoxNo = value.BoxNo;
            _context.SaveChanges();
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBmrdisposalMasterBox")]
        public BmrdisposalMasterBoxModel Put(BmrdisposalMasterBoxModel value)
        {
            var BmrdisposalMasterBox = _context.BmrdisposalMasterBox.SingleOrDefault(p => p.BmrdisposalMasterBoxId == value.BmrdisposalMasterBoxId);
            var TypeOfDisposalBoxId = BmrdisposalMasterBox.TypeOfDisposalBoxId;
            BmrdisposalMasterBox.TypeOfDisposalBoxId = value.TypeOfDisposalBoxId;
            BmrdisposalMasterBox.CompanyId = value.CompanyId;
            BmrdisposalMasterBox.BoxNo = value.BoxNo;
            BmrdisposalMasterBox.DisposalDate = value.DisposalDate;
            BmrdisposalMasterBox.EstimateNumberOfBmr = value.EstimateNumberOfBmr;
            BmrdisposalMasterBox.LocationId = value.LocationId;
            BmrdisposalMasterBox.ModifiedByUserId = value.ModifiedByUserID;
            BmrdisposalMasterBox.ModifiedDate = DateTime.Now;
            BmrdisposalMasterBox.StatusCodeId = value.StatusCodeID.Value;
            var DocumentNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
            {
                ProfileID = _context.ApplicationMasterDetail.Where(w => w.ApplicationMasterDetailId == value.TypeOfDisposalBoxId.Value).Select(p => p.ProfileId).SingleOrDefault(),
                AddedByUserID = value.AddedByUserID,
                StatusCodeID = 710,
                DepartmentName = value.ProfileNoModel.DepartmentName,
                CompanyCode = value.ProfileNoModel.CompanyCode,
                SectionName = value.ProfileNoModel.SectionName,
                SubSectionName = value.ProfileNoModel.SubSectionName,
                DepartmentId = value.ProfileNoModel.DepartmentId,
                PlantID = value.ProfileNoModel.PlantID,
                SectionId = value.ProfileNoModel.SectionId,
                SubSectionId = value.ProfileNoModel.SubSectionId,
                ScreenID = value.ScreenID,
                ScreenAutoNumberId = value.BmrdisposalMasterBoxId,
            });
            if (DocumentNo != "")
            {
                BmrdisposalMasterBox.BoxNo = DocumentNo;
                value.BoxNo = BmrdisposalMasterBox.BoxNo;
            }
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBmrdisposalMasterBox")]
        public void Delete(int id)
        {
            var BmrdisposalMasterBox = _context.BmrdisposalMasterBox.SingleOrDefault(p => p.BmrdisposalMasterBoxId == id);
            if (BmrdisposalMasterBox != null)
            {
                _context.BmrdisposalMasterBox.Remove(BmrdisposalMasterBox);
                _context.SaveChanges();
            }
        }
    }
}