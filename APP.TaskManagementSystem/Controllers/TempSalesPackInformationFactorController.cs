﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TempSalesPackInformationFactorController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public TempSalesPackInformationFactorController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetTempSalesPackInformationFactor")]
        public List<TempSalesPackInformationFactorModel> Get(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<TempSalesPackInformationFactorModel> tempSalesPackInformationFactorModels = new List<TempSalesPackInformationFactorModel>();
            var tempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.Include(a=>a.AddedByUser).Include(m=>m.ModifiedByUser).Include(s=>s.StatusCode).Include(s=>s.Item).Where(s=>s.TempSalesPackInformationId == id).OrderByDescending(o => o.TempSalesPackInformationFactorId).AsNoTracking().ToList();
            if(tempSalesPackInformationFactor!=null && tempSalesPackInformationFactor.Count>0)
            {
                tempSalesPackInformationFactor.ForEach(s =>
                {
                    TempSalesPackInformationFactorModel tempSalesPackInformationFactorModel = new TempSalesPackInformationFactorModel();
                    tempSalesPackInformationFactorModel.TempSalesPackInformationFactorID = s.TempSalesPackInformationFactorId;
                    tempSalesPackInformationFactorModel.TempSalesPackInformationID = s.TempSalesPackInformationId;
                    tempSalesPackInformationFactorModel.SalesFactor = s.SalesFactor;
                    tempSalesPackInformationFactorModel.ProfileID = s.ProfileId;
                    tempSalesPackInformationFactorModel.ProfileNo = s.ProfileNo;
                    tempSalesPackInformationFactorModel.FPName = s.Fpname;
                    tempSalesPackInformationFactorModel.ItemID = s.ItemId;
                    tempSalesPackInformationFactorModel.ItemName = s.Item?.No;
                    tempSalesPackInformationFactorModel.ItemDescription = s.Item?.Description;
                    tempSalesPackInformationFactorModel.QTYPackPerCarton = s.QtypackPerCarton;
                    //tempSalesPackInformationFactorModel.QTYPackPerCartonName = s.QtypackPerCarton != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.QtypackPerCarton).Select(m => m.Value).FirstOrDefault() : "";
                    tempSalesPackInformationFactorModels.Add(tempSalesPackInformationFactorModel);
                });
                
            }
            
            return tempSalesPackInformationFactorModels;
        }
       
       
       
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TempSalesPackInformationFactorModel> GetData(SearchModel searchModel)
        {
            var TempSalesPackInformationFactor = new TempSalesPackInformationFactor();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).FirstOrDefault();
                        break;
                    case "Last":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).LastOrDefault();
                        break;
                    case "Next":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).LastOrDefault();
                        break;
                    case "Previous":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).FirstOrDefault();
                        break;
                    case "Last":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).LastOrDefault();
                        break;
                    case "Next":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderBy(o => o.TempSalesPackInformationFactorId).FirstOrDefault(s => s.TempSalesPackInformationFactorId > searchModel.Id);
                        break;
                    case "Previous":
                        TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.OrderByDescending(o => o.TempSalesPackInformationFactorId).FirstOrDefault(s => s.TempSalesPackInformationFactorId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TempSalesPackInformationFactorModel>(TempSalesPackInformationFactor);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTempSalesPackInformationFactor")]
        public TempSalesPackInformationFactorModel Post(TempSalesPackInformationFactorModel value)
        {
            var fpNo = "";
            if (value.ProfileID > 0)
            {
                fpNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 });
            }
            if (value.RegistrationFactor == 1 || value.SalesFactor == 1)
            {
                value.FPName = "";
            }
            else
            {
                value.FPName = "[" + value.PrhspecificProductName + "] " + " [" + value.RegistrationFactor + " or " + value.SalesFactor + "] " + " X " + " [" + value.SmallestPackQty + "] " + " [" + value.SmallestQtyUnit + "] " + "/" + "Registration Till Smallest Production Pack" + " - " + value.RegistrationPerPack + " ]";
            }
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var TempSalesPackInformationFactor = new TempSalesPackInformationFactor
            {
                TempSalesPackInformationId = value.TempSalesPackInformationID,
                SalesFactor = value.SalesFactor,
                ProfileId = value.ProfileID,
                ProfileNo = fpNo,
                Fpname = value.FPName,
                ItemId = value.ItemID,
                
                QtypackPerCarton = value.QTYPackPerCarton,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate =DateTime.Now,


            };
            _context.TempSalesPackInformationFactor.Add(TempSalesPackInformationFactor);
            _context.SaveChanges();
            value.QTYPackPerCartonName = value.QTYPackPerCarton != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.QTYPackPerCarton).Select(m => m.Value).FirstOrDefault() : "";
            value.ProfileNo = TempSalesPackInformationFactor.ProfileNo;
            value.TempSalesPackInformationFactorID = TempSalesPackInformationFactor.TempSalesPackInformationFactorId;
            value.FPName = TempSalesPackInformationFactor.Fpname;
            return value;
        }

      
        [HttpPut]
        [Route("UpdateTempSalesPackInformationFactor")]
        public TempSalesPackInformationFactorModel Put(TempSalesPackInformationFactorModel value)
        {
            if (value.RegistrationFactor == 1 || value.SalesFactor == 1)
            {
                value.FPName = "";
            }
            else
            {
                value.FPName = "[" + value.PrhspecificProductName + "] " + " [" + value.RegistrationFactor + "Or" + value.SalesFactor + "] " + " X " + " [" + value.SmallestPackQty + "] " + " [" + value.SmallestQtyUnit + "] " + "/" + "Registration Till Smallest Production Pack" + " - " + value.RegistrationPerPack + " ]";
            }
            var tempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.SingleOrDefault(p => p.TempSalesPackInformationFactorId == value.TempSalesPackInformationFactorID);
            tempSalesPackInformationFactor.TempSalesPackInformationId = value.TempSalesPackInformationID;
            tempSalesPackInformationFactor.SalesFactor = value.SalesFactor;
            tempSalesPackInformationFactor.ProfileId = value.ProfileID;           
            tempSalesPackInformationFactor.ItemId = value.ItemID;
            tempSalesPackInformationFactor.Fpname = value.FPName;
            tempSalesPackInformationFactor.QtypackPerCarton = value.QTYPackPerCarton;
            tempSalesPackInformationFactor.StatusCodeId = value.StatusCodeID.Value;
            tempSalesPackInformationFactor.ModifiedByUserId = value.ModifiedByUserID;
            tempSalesPackInformationFactor.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.FPName = tempSalesPackInformationFactor.Fpname;
            return value;
        }
      
        [HttpDelete]
        [Route("DeleteTempSalesPackInformationFactor")]
        public void Delete(int id)
        {
            var TempSalesPackInformationFactor = _context.TempSalesPackInformationFactor.SingleOrDefault(p => p.TempSalesPackInformationFactorId == id);
            if (TempSalesPackInformationFactor != null)
            {
                _context.TempSalesPackInformationFactor.Remove(TempSalesPackInformationFactor);
                _context.SaveChanges();
            }
        }
    }
}