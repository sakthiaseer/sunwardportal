﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserPermissionTransferController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public UserPermissionTransferController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        private void AddTransferPermissionLog(BulkTransferTaskModel value, string tableName, string type = "")
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                value.TransferIds.ForEach(s =>
                {
                    var transferPermissionLog = new TransferPermissionLog
                    {
                        TableName = tableName,
                        PrimaryTableId = s,
                        PreviousUserId = value.TransferFromUserId,
                        CurrentUserId = value.TransferToUserId,
                        AddedByUserId = value.TransferByUserId,
                        Type = type,
                        AddedDate = DateTime.Now,
                    };
                    _context.TransferPermissionLog.Add(transferPermissionLog);
                });
                _context.SaveChanges();
            }
        }
        private List<TransferPermissionLogModel> TransferPermissionLog(string tableName, string type)
        {
            List<TransferPermissionLogModel> transferPermissionLogModels = new List<TransferPermissionLogModel>();
            var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).AsNoTracking().ToList();
            var transferPermissionLog = _context.TransferPermissionLog.Where(w => w.TableName == tableName && w.Type == type).ToList();
            transferPermissionLog.ForEach(s =>
            {
                TransferPermissionLogModel transferPermissionLogModel = new TransferPermissionLogModel();
                transferPermissionLogModel.TransferPermissionLogId = s.TransferPermissionLogId;
                transferPermissionLogModel.TableName = s.TableName;
                transferPermissionLogModel.PrimaryTableId = s.PrimaryTableId;
                transferPermissionLogModel.PreviousUserId = s.PreviousUserId;
                transferPermissionLogModel.CurrentUserId = s.CurrentUserId;
                transferPermissionLogModel.Type = s.Type;
                transferPermissionLogModel.AddedByUserId = s.AddedByUserId;
                transferPermissionLogModel.AddedByUser = s.AddedByUserId != 0 && appUser.Count > 0 ? appUser.Where(m => m.UserId == s.AddedByUserId).Select(m => m.UserName).FirstOrDefault() : "";
                transferPermissionLogModel.CurrentUser = s.CurrentUserId != 0 && appUser.Count > 0 ? appUser.Where(m => m.UserId == s.CurrentUserId).Select(m => m.UserName).FirstOrDefault() : "";
                transferPermissionLogModel.PreviousUser = s.PreviousUserId != 0 && appUser.Count > 0 ? appUser.Where(m => m.UserId == s.PreviousUserId).Select(m => m.UserName).FirstOrDefault() : "";
                transferPermissionLogModels.Add(transferPermissionLogModel);
            });
            return transferPermissionLogModels;
        }
        #region DocumentUserRole
        [HttpPost]
        [Route("GetDocumentUserRoleByUserId")]
        public List<DocumentUserRoleModel> GetDocumentUserRoleByUserId(SearchModel searchModel)
        {
            List<DocumentUserRoleModel> documentUserRoleModels = new List<DocumentUserRoleModel>();

            var documentUserRole = _context.DocumentUserRole.Where(w => w.UserId == searchModel.UserID).ToList();

            if (documentUserRole != null && documentUserRole.Count > 0)
            {
                var folderIds = documentUserRole.Select(s => s.FolderId.GetValueOrDefault(0)).Distinct().ToList();
                var fileProfileTypeIds = documentUserRole.Select(s => s.FileProfileTypeId.GetValueOrDefault(0)).Distinct().ToList();
                var documentIds = documentUserRole.Select(s => s.DocumentId.GetValueOrDefault(0)).Distinct().ToList();
                var roleIds = documentUserRole.Select(s => s.RoleId.GetValueOrDefault(0)).Distinct().ToList();
                var userGroupIds = documentUserRole.Select(s => s.UserGroupId.GetValueOrDefault(0)).Distinct().ToList();
                var userGroup = _context.UserGroup.Select(s => new { s.UserGroupId, s.Name, s.Description }).Where(w => userGroupIds.Contains(w.UserGroupId)).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.FileName }).Where(w => documentIds.Contains(w.DocumentId)).ToList();
                var fileProfileType = _context.FileProfileType.Select(s => new { s.FileProfileTypeId, s.Name }).Where(w => fileProfileTypeIds.Contains(w.FileProfileTypeId)).ToList();
                var folders = _context.Folders.Select(s => new { s.FolderId, s.Name }).Where(w => folderIds.Contains(w.FolderId)).ToList();
                var documentRole = _context.DocumentRole.Select(s => new { s.DocumentRoleId, s.DocumentRoleName, s.DocumentRoleDescription }).Where(w => roleIds.Contains(w.DocumentRoleId)).ToList();
                var transferPermissionLog = TransferPermissionLog("DocumentUserRole", "");
                documentUserRole.ForEach(s =>
                {
                    DocumentUserRoleModel documentUserRoleModel = new DocumentUserRoleModel
                    {
                        DocumentUserRoleID = s.DocumentUserRoleId,
                        UserID = s.UserId,
                        RoleID = s.RoleId,
                        UserGroupID = s.UserGroupId,
                        DocumentID = s.DocumentId,
                        FolderID = s.FolderId,
                        FileProfileTypeId = s.FileProfileTypeId,
                        FolderName = s.FolderId != null && folders != null ? folders.FirstOrDefault(f => f.FolderId == s.FolderId)?.Name : null,
                        FileName = s.DocumentId != null && documents != null ? documents.FirstOrDefault(f => f.DocumentId == s.DocumentId)?.FileName : null,
                        FileProfileTypeName = s.FileProfileTypeId != null && fileProfileType != null ? fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name : null,
                        UserGroupName = s.UserGroupId != null && userGroup != null ? userGroup.FirstOrDefault(f => f.UserGroupId == s.UserGroupId)?.Name : null,
                        RoleName = s.RoleId != null && documentRole != null ? documentRole.FirstOrDefault(f => f.DocumentRoleId == s.RoleId)?.DocumentRoleName : null,
                        TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.DocumentUserRoleId).OrderByDescending(o => o.TransferPermissionLogId).ToList(),

                    };
                    documentUserRoleModels.Add(documentUserRoleModel);
                });
            }
            return documentUserRoleModels;
        }
        [HttpPut]
        [Route("UpdatedocumentUserRole")]
        public BulkTransferTaskModel UpdatedocumentUserRole(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update DocumentUserRole Set UserId = {0} Where DocumentUserRoleId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "DocumentUserRole");
            }
            return value;
        }
        #endregion
        #region UserGroupUser
        [HttpPost]
        [Route("GetUserGroupsByUserId")]
        public List<UserGroupModel> GetUserGroupsByUserId(SearchModel searchModel)
        {
            List<UserGroupModel> userGroupModels = new List<UserGroupModel>();

            var UserGrouplist = _context.UserGroupUser.Include(s => s.UserGroup).Include(s => s.UserGroup.AddedByUser).Include(s => s.UserGroup.StatusCode).Where(w => w.UserId == searchModel.UserID).ToList();

            if (UserGrouplist != null && UserGrouplist.Count > 0)
            {
                var transferPermissionLog = TransferPermissionLog("UserGroupUser", "");
                UserGrouplist.ForEach(s =>
                {
                    UserGroupModel userGroupModel = new UserGroupModel
                    {
                        UserGroupUserId = s.UserGroupUserId,
                        UserGroupId = s.UserGroupId,
                        UserId = s.UserId,
                        Name = s.UserGroup?.Name,
                        Description = s.UserGroup?.Description,
                        ModifiedByUserID = s.UserGroup?.AddedByUserId,
                        AddedByUserID = s.UserGroup?.ModifiedByUserId,
                        StatusCodeID = s.UserGroup?.StatusCodeId,
                        AddedDate = s.UserGroup?.AddedDate,
                        ModifiedDate = s.UserGroup?.ModifiedDate,
                        AddedByUser = s.UserGroup?.AddedByUser?.UserName,
                        StatusCode = s.UserGroup?.StatusCode?.CodeValue,
                        TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.UserGroupUserId).OrderByDescending(o=>o.TransferPermissionLogId).ToList(),
                    };
                    userGroupModels.Add(userGroupModel);
                });
            }
            return userGroupModels;
        }
        [HttpPut]
        [Route("UpdateUserGroupsByUser")]
        public BulkTransferTaskModel UpdateUserGroupsByUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                AddTransferPermissionLog(value, "UserGroupUser");
                var query = string.Format("Update UserGroupUser Set UserId = {0} Where UserGroupUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', value.TransferIds));
                _context.Database.ExecuteSqlRaw(query);
            }
            return value;
        }
        #endregion
        #region WikiResponsible
        [HttpPost]
        [Route("GetWikiResponsibleByUserId")]
        public List<WikiResponsibleModel> GetWikiResponsibleByUserId(SearchModel searchModel)
        {
            List<WikiResponsibleModel> applicationWikiModels = new List<WikiResponsibleModel>();
            var employeeData = _context.Employee.Select(s => new { s.EmployeeId, s.UserId }).FirstOrDefault(w => w.UserId == searchModel.UserID);
            if (employeeData != null)
            {
                var wikiResponsible = _context.WikiResponsible.Where(w => w.EmployeeId == employeeData.EmployeeId || w.UserId == searchModel.UserID).ToList();
                if (wikiResponsible != null && wikiResponsible.Count > 0)
                {
                    var applicationWikiLineDutyIds = wikiResponsible.Select(s => s.ApplicationWikiLineDutyId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikiLineDuty = _context.ApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId, s.DutyNo, s.CompanyId }).Where(w => applicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId)).ToList();
                    List<long?> dutyNoIds = applicationWikiLineDuty.Where(w => w.DutyNo != null).Select(s => s.DutyNo).Distinct().ToList();
                    var applicationWikiLineIds = applicationWikiLineDuty.Select(s => s.ApplicationWikiLineId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikiLines = _context.ApplicationWikiLine.Select(s => new { s.ApplicationWikiLineId, s.DutyId, s.ApplicationWikiId }).Where(w => applicationWikiLineIds.Contains(w.ApplicationWikiLineId)).ToList();
                    dutyNoIds.AddRange(applicationWikiLines.Where(w => w.DutyId != null).Select(a => a.DutyId).Distinct().ToList());
                    var applicationWikiIds = applicationWikiLines.Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikis = _context.ApplicationWiki.Select(s => new { s.Title, s.ApplicationWikiId, s.StatusCodeId, s.AddedByUserId, s.AddedDate }).Where(w => applicationWikiIds.Contains(w.ApplicationWikiId)).ToList();
                    var applicationMasterDetails = _context.ApplicationMasterDetail.Where(w => dutyNoIds.Distinct().ToList().Contains(w.ApplicationMasterDetailId)).ToList();
                    var statusCodeIds = applicationWikis.Select(s => s.StatusCodeId).Distinct().ToList();
                    var userIds = applicationWikis.Select(s => s.AddedByUserId.GetValueOrDefault(0)).Distinct().ToList();
                    var appUsers = _context.ApplicationUser.Select(c => new { c.UserId, c.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                    var transferPermissionLog = TransferPermissionLog("WikiResponsible", "");
                    var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                    wikiResponsible.ForEach(s =>
                    {
                        WikiResponsibleModel applicationWikiModel = new WikiResponsibleModel();
                        applicationWikiModel.WikiResponsibilityId = s.WikiResponsibilityId;
                        var applicationWikiLineDutyData = applicationWikiLineDuty.FirstOrDefault(f => f.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId);
                        if (applicationWikiLineDutyData != null)
                        {
                            applicationWikiModel.DutyNoName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == applicationWikiLineDutyData?.DutyNo)?.Value;
                            var applicationWikiLineData = applicationWikiLines.FirstOrDefault(a => a.ApplicationWikiLineId == applicationWikiLineDutyData.ApplicationWikiLineId);
                            applicationWikiModel.DutyName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == applicationWikiLineData?.DutyId)?.Value;
                            var applicationWiki = applicationWikis.FirstOrDefault(g => g.ApplicationWikiId == applicationWikiLineData?.ApplicationWikiId);
                            applicationWikiModel.Title = applicationWiki?.Title;
                            applicationWikiModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(g => g.UserId == applicationWiki?.AddedByUserId)?.UserName : null;
                            applicationWikiModel.AddedDate = applicationWiki?.AddedDate;
                            applicationWikiModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(g => g.CodeId == applicationWiki?.StatusCodeId)?.CodeValue : null;
                        }
                        applicationWikiModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.WikiResponsibilityId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                        applicationWikiModels.Add(applicationWikiModel);
                    });
                }
            }
            return applicationWikiModels;
        }
        [HttpPut]
        [Route("UpdateAppWikiResponsibleByUser")]
        public BulkTransferTaskModel UpdateAppWikiResponsibleByUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                var employeeData = _context.Employee.Select(s => new { s.EmployeeId, s.UserId }).Where(w => w.UserId == value.TransferToUserId).FirstOrDefault();
                long? employeeId = employeeData?.EmployeeId;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update WikiResponsible Set EmployeeId={0} , UserId = {1} Where WikiResponsibilityId in" + '(' + "{2}" + ')', employeeId, value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "WikiResponsible");
            }
            return value;
        }
        #endregion
        #region DraftWikiResponsible
        [HttpPost]
        [Route("GetDraftWikiResponsibleByUserId")]
        public List<WikiResponsibleModel> GetDraftWikiResponsibleByUserId(SearchModel searchModel)
        {
            List<WikiResponsibleModel> applicationWikiModels = new List<WikiResponsibleModel>();
            var employeeData = _context.Employee.Select(s => new { s.EmployeeId, s.UserId }).FirstOrDefault(w => w.UserId == searchModel.UserID);
            if (employeeData != null)
            {
                var wikiResponsible = _context.DraftWikiResponsible.Where(w => w.EmployeeId == employeeData.EmployeeId || w.UserId == searchModel.UserID).ToList();
                if (wikiResponsible != null && wikiResponsible.Count > 0)
                {
                    var applicationWikiLineDutyIds = wikiResponsible.Select(s => s.ApplicationWikiLineDutyId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikiLineDuty = _context.DraftApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId, s.DutyNo, s.CompanyId }).Where(w => applicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId)).ToList();
                    List<long?> dutyNoIds = applicationWikiLineDuty.Where(w => w.DutyNo != null).Select(s => s.DutyNo).Distinct().ToList();
                    var applicationWikiLineIds = applicationWikiLineDuty.Select(s => s.ApplicationWikiLineId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikiLines = _context.DraftApplicationWikiLine.Select(s => new { s.ApplicationWikiLineId, s.DutyId, s.ApplicationWikiId }).Where(w => applicationWikiLineIds.Contains(w.ApplicationWikiLineId)).ToList();
                    dutyNoIds.AddRange(applicationWikiLines.Where(w => w.DutyId != null).Select(a => a.DutyId).Distinct().ToList());
                    var applicationWikiIds = applicationWikiLines.Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).Distinct().ToList();
                    var applicationWikis = _context.DraftApplicationWiki.Select(s => new { s.Title, s.ApplicationWikiId, s.StatusCodeId, s.AddedByUserId, s.AddedDate }).Where(w => applicationWikiIds.Contains(w.ApplicationWikiId)).ToList();
                    var applicationMasterDetails = _context.ApplicationMasterDetail.Where(w => dutyNoIds.Distinct().ToList().Contains(w.ApplicationMasterDetailId)).ToList();
                    var statusCodeIds = applicationWikis.Select(s => s.StatusCodeId).Distinct().ToList();
                    var userIds = applicationWikis.Select(s => s.AddedByUserId.GetValueOrDefault(0)).Distinct().ToList();
                    var appUsers = _context.ApplicationUser.Select(c => new { c.UserId, c.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                    var transferPermissionLog = TransferPermissionLog("DraftWikiResponsible", "");
                    var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                    wikiResponsible.ForEach(s =>
                    {
                        WikiResponsibleModel applicationWikiModel = new WikiResponsibleModel();
                        applicationWikiModel.WikiResponsibilityId = s.WikiResponsibilityId;
                        var applicationWikiLineDutyData = applicationWikiLineDuty.FirstOrDefault(f => f.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId);
                        if (applicationWikiLineDutyData != null)
                        {
                            applicationWikiModel.DutyNoName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == applicationWikiLineDutyData?.DutyNo)?.Value;
                            var applicationWikiLineData = applicationWikiLines.FirstOrDefault(a => a.ApplicationWikiLineId == applicationWikiLineDutyData.ApplicationWikiLineId);
                            applicationWikiModel.DutyName = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == applicationWikiLineData?.DutyId)?.Value;
                            var applicationWiki = applicationWikis.FirstOrDefault(g => g.ApplicationWikiId == applicationWikiLineData?.ApplicationWikiId);
                            applicationWikiModel.Title = applicationWiki?.Title;
                            applicationWikiModel.AddedByUser = appUsers != null ? (appUsers.FirstOrDefault(g => g.UserId == applicationWiki?.AddedByUserId)?.UserName) : null;
                            applicationWikiModel.AddedDate = applicationWiki?.AddedDate;
                            applicationWikiModel.StatusCode = codemaster != null ? (codemaster.FirstOrDefault(g => g.CodeId == applicationWiki?.StatusCodeId)?.CodeValue) : null;
                        }
                        applicationWikiModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.WikiResponsibilityId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                        applicationWikiModels.Add(applicationWikiModel);
                    });
                }
            }
            return applicationWikiModels;
        }
        [HttpPut]
        [Route("UpdateDraftWikiResponsibleByUser")]
        public BulkTransferTaskModel UpdateDraftWikiResponsibleByUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                var employeeData = _context.Employee.Select(s => new { s.EmployeeId, s.UserId }).Where(w => w.UserId == value.TransferToUserId).FirstOrDefault();
                long? employeeId = employeeData?.EmployeeId;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update DraftWikiResponsible Set EmployeeId={0} , UserId = {1} Where WikiResponsibilityId in" + '(' + "{2}" + ')', employeeId, value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "DraftWikiResponsible");
            }
            return value;
        }
        #endregion
        #region ItemClassificationAccess
        [HttpPost]
        [Route("GetApplicationFormByUserId")]
        public List<ItemClassificationAccessModel> GetApplicationFormByUserId(SearchModel searchModel)
        {
            List<ItemClassificationAccessModel> itemClassificationAccessModels = new List<ItemClassificationAccessModel>();
            var itemClassificationAccess = _context.ItemClassificationAccess.Where(w => w.UserId == searchModel.UserID).ToList();
            if (itemClassificationAccess != null && itemClassificationAccess.Count > 0)
            {
                var userGroupIds = itemClassificationAccess.Select(s => s.UserGroupId.GetValueOrDefault(0)).Distinct().ToList();
                var userGroup = _context.UserGroup.Select(s => new { s.UserGroupId, s.Name, s.Description }).Where(w => userGroupIds.Contains(w.UserGroupId)).ToList();
                var roleIds = itemClassificationAccess.Select(s => s.RoleId.GetValueOrDefault(0)).Distinct().ToList();
                var documentRole = _context.DocumentRole.Select(s => new { s.DocumentRoleId, s.DocumentRoleName, s.DocumentRoleDescription }).Where(w => roleIds.Contains(w.DocumentRoleId)).ToList();
                var formIds = itemClassificationAccess.Select(s => s.FormId.GetValueOrDefault(0)).Distinct().ToList();
                var appForms = _context.ApplicationForm.Select(s => new { s.FormId, s.Name, s.ClassificationTypeId, s.ProfileId }).Where(w => formIds.Contains(w.FormId)).ToList();
                var classificationTypeIds = appForms.Select(s => s.ClassificationTypeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => classificationTypeIds.Contains(w.CodeId)).ToList();
                var profileIds = appForms.Select(s => s.ProfileId.GetValueOrDefault(0)).Distinct().ToList();
                var profiles = _context.DocumentProfileNoSeries.Select(s => new { s.ProfileId, s.Name }).Where(w => profileIds.Contains(w.ProfileId)).ToList();
                var transferPermissionLog = TransferPermissionLog("ItemClassificationAccess", "");
                itemClassificationAccess.ForEach(s =>
                {
                    ItemClassificationAccessModel itemClassificationAccessModel = new ItemClassificationAccessModel();
                    itemClassificationAccessModel.ItemClassificationAccessID = s.ItemClassificationAccessId;
                    itemClassificationAccessModel.UserID = s.UserId;
                    itemClassificationAccessModel.UserGroupID = s.UserGroupId;
                    itemClassificationAccessModel.RoleID = s.RoleId;
                    itemClassificationAccessModel.FormId = s.FormId;
                    itemClassificationAccessModel.UserGroupName = s.UserGroupId != null && userGroup != null ? userGroup.FirstOrDefault(f => f.UserGroupId == s.UserGroupId)?.Name : null;
                    itemClassificationAccessModel.RoleName = s.RoleId != null && documentRole != null ? documentRole.FirstOrDefault(f => f.DocumentRoleId == s.RoleId)?.DocumentRoleName : null;
                    var appForm = s.FormId != null && appForms != null ? appForms.FirstOrDefault(f => f.FormId == s.FormId) : null;
                    itemClassificationAccessModel.FormName = appForm?.Name;
                    itemClassificationAccessModel.ClassificationTypeName = codemaster != null ? (codemaster.FirstOrDefault(g => g.CodeId == appForm?.ClassificationTypeId)?.CodeValue) : null;
                    itemClassificationAccessModel.ProfileName = profiles != null ? (profiles.FirstOrDefault(g => g.ProfileId == appForm?.ProfileId)?.Name) : null;
                    itemClassificationAccessModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.ItemClassificationAccessId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    itemClassificationAccessModels.Add(itemClassificationAccessModel);
                });
            };
            return itemClassificationAccessModels;
        }
        [HttpPut]
        [Route("UpdateApplicationFormByUser")]
        public BulkTransferTaskModel UpdateApplicationFormByUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update ItemClassificationAccess Set UserId = {0} Where ItemClassificationAccessId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "ItemClassificationAccess");
            }
            return value;
        }
        #endregion
        #region ItemClassificationPermission
        [HttpPost]
        [Route("GetItemClassificationPermissionByUserId")]
        public List<ItemClassificationPermissionModel> GetItemClassificationPermissionByUserId(SearchModel searchModel)
        {
            List<ItemClassificationPermissionModel> itemClassificationPermissionModels = new List<ItemClassificationPermissionModel>();
            var itemClassificationPermission = _context.ItemClassificationPermission.Include(a => a.ApplicationCode)
                .Include(b => b.ApplicationCode.ApplicationFormCode)
                .Include(c => c.ApplicationCode.Parent)
                .Include(d => d.ApplicationCode.Parent.ApplicationFormCode)
                .Where(w => w.UserId == searchModel.UserID).ToList();
            if (itemClassificationPermission != null && itemClassificationPermission.Count > 0)
            {
                var itemClassificationIds = itemClassificationPermission.Select(s => s.ItemClassificationId.GetValueOrDefault(0)).Distinct().ToList();
                var itemClassificationMaster = _context.ItemClassificationMaster.Select(s => new { s.ItemClassificationId, s.Name, s.Description, s.MainClassificationId }).Where(w => itemClassificationIds.Contains(w.ItemClassificationId)).ToList();
                var transferPermissionLog = TransferPermissionLog("ItemClassificationPermission", "");
                itemClassificationPermission.ForEach(s =>
                {
                    ItemClassificationPermissionModel itemClassificationPermissionModel = new ItemClassificationPermissionModel();
                    itemClassificationPermissionModel.ItemClassificationPermissionId = s.ItemClassificationPermissionId;
                    itemClassificationPermissionModel.ApplicationCodeId = s.ApplicationCodeId;
                    itemClassificationPermissionModel.ItemClassificationId = s.ItemClassificationId;
                    itemClassificationPermissionModel.UserId = s.UserId;
                    itemClassificationPermissionModel.ApplicationFormCodeId = s.ApplicationCode?.ApplicationFormCode?.CodeId;
                    itemClassificationPermissionModel.ApplicationFormCodeName = s.ApplicationCode?.ApplicationFormCode?.CodeValue;
                    itemClassificationPermissionModel.ParentId = s.ApplicationCode?.ParentId;
                    itemClassificationPermissionModel.ParentName = s.ApplicationCode?.Parent?.ApplicationFormCode?.CodeValue;
                    itemClassificationPermissionModel.ItemClassificationName = itemClassificationMaster != null ? (itemClassificationMaster.FirstOrDefault(g => g.ItemClassificationId == s.ItemClassificationId)?.Name) : null;
                    itemClassificationPermissionModel.Description = itemClassificationMaster != null ? (itemClassificationMaster.FirstOrDefault(g => g.ItemClassificationId == s.ItemClassificationId)?.Description) : null;
                    itemClassificationPermissionModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.ItemClassificationPermissionId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    itemClassificationPermissionModels.Add(itemClassificationPermissionModel);
                });
            }
            return itemClassificationPermissionModels;
        }
        [HttpPut]
        [Route("UpdateItemClassificationPermissionByUser")]
        public BulkTransferTaskModel UpdateItemClassificationPermissionByUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update ItemClassificationPermission Set UserId = {0} Where ItemClassificationPermissionId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "ItemClassificationPermission");
            }
            return value;
        }
        #endregion
        #region DocumentRights
        [HttpPost]
        [Route("GetDocumentRightsByUserId")]
        public List<DocumentRightsModel> GetDocumentRightsByUserId(SearchModel searchModel)
        {
            List<DocumentRightsModel> documentRightsModels = new List<DocumentRightsModel>();
            var documentRights = _context.DocumentRights.Where(w => w.UserId == searchModel.UserID).ToList();
            if (documentRights != null && documentRights.Count > 0)
            {
                var documentIds = documentRights.Select(s => s.DocumentId.GetValueOrDefault(0)).Distinct().ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.FileName }).Where(w => documentIds.Contains(w.DocumentId)).ToList();
                var transferPermissionLog = TransferPermissionLog("DocumentRights", "");
                documentRights.ForEach(s =>
                {
                    DocumentRightsModel documentRightsModel = new DocumentRightsModel();
                    documentRightsModel.DocumentAccessID = s.DocumentAccessId;
                    documentRightsModel.UserID = s.UserId;
                    documentRightsModel.DocumentID = s.DocumentId;
                    documentRightsModel.IsRead = s.IsRead;
                    documentRightsModel.IsReadWrite = s.IsReadWrite;
                    documentRightsModel.DocumentName = s.DocumentId != null && documents != null ? documents.FirstOrDefault(f => f.DocumentId == s.DocumentId)?.FileName : null;
                    documentRightsModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.DocumentAccessId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    documentRightsModels.Add(documentRightsModel);
                });
            }
            return documentRightsModels;
        }
        [HttpPut]
        [Route("UpdateDocumentRights")]
        public BulkTransferTaskModel UpdateDocumentRights(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update DocumentRights Set UserId = {0} Where DocumentAccessId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "DocumentRights");
            }
            return value;
        }
        #endregion
        #region DocumentShareUser
        [HttpPost]
        [Route("GetDocumentShareUserByUserId")]
        public List<DocumentShareUserModel> GetDocumentShareUserByUserId(SearchModel searchModel)
        {
            List<DocumentShareUserModel> documentShareUserModels = new List<DocumentShareUserModel>();
            var documentShareUser = _context.DocumentShareUser.Include(d => d.DocumentShare).Where(w => w.UserId == searchModel.UserID).ToList();
            if (documentShareUser != null && documentShareUser.Count > 0)
            {
                var documentIds = documentShareUser.Select(s => s.DocumentShare?.DocumentId.GetValueOrDefault(0)).Distinct().ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.FileName, s.FilterProfileTypeId }).Where(w => documentIds.Contains(w.DocumentId)).ToList();
                var fileProfileTypeIds = documents.Select(s => s.FilterProfileTypeId.GetValueOrDefault(0)).Distinct().ToList();
                var fileProfileType = _context.FileProfileType.Select(s => new { s.FileProfileTypeId, s.Name }).Where(w => fileProfileTypeIds.Contains(w.FileProfileTypeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("DocumentShareUser", "");
                documentShareUser.ForEach(s =>
                {
                    DocumentShareUserModel documentShareUserModel = new DocumentShareUserModel();
                    documentShareUserModel.DocumentShareUserId = s.DocumentShareUserId;
                    documentShareUserModel.DocumentShareId = s.DocumentShareId;
                    documentShareUserModel.UserId = s.UserId;
                    documentShareUserModel.DocumentId = s.DocumentShare?.DocumentId;
                    documentShareUserModel.IsShareAnyOne = s.DocumentShare?.IsShareAnyOne;
                    var document = documents.FirstOrDefault(f => f.DocumentId == s.DocumentShare?.DocumentId);
                    documentShareUserModel.DocumentName = document?.FileName;
                    var fileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == document?.FilterProfileTypeId);
                    documentShareUserModel.FileProfileTypeName = fileProfileTypeName?.Name;
                    documentShareUserModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.DocumentShareUserId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    documentShareUserModels.Add(documentShareUserModel);
                });
            }
            return documentShareUserModels;
        }
        [HttpPut]
        [Route("UpdateDocumentShareUser")]
        public BulkTransferTaskModel UpdateDocumentShareUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update DocumentShareUser Set UserId = {0} Where DocumentShareUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "DocumentShareUser");
            }
            return value;
        }
        #endregion
        #region FileProfileTypeSetAccess
        [HttpPost]
        [Route("GetFileProfileTypeSetAccessByUserId")]
        public List<FileProfileTypeSetAccessModel> GetFileProfileTypeSetAccessByUserId(SearchModel searchModel)
        {
            List<FileProfileTypeSetAccessModel> fileProfileTypeSetAccessModels = new List<FileProfileTypeSetAccessModel>();
            var fileProfileTypeSetAccess = _context.FileProfileTypeSetAccess.Where(w => w.UserId == searchModel.UserID).ToList();
            if (fileProfileTypeSetAccess != null && fileProfileTypeSetAccess.Count > 0)
            {
                var documentIds = fileProfileTypeSetAccess.Select(s => s.DocumentId.GetValueOrDefault(0)).Distinct().ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.FileName, s.FilterProfileTypeId }).Where(w => documentIds.Contains(w.DocumentId)).ToList();
                var fileProfileTypeIds = documents.Select(s => s.FilterProfileTypeId.GetValueOrDefault(0)).Distinct().ToList();
                var fileProfileType = _context.FileProfileType.Select(s => new { s.FileProfileTypeId, s.Name }).Where(w => fileProfileTypeIds.Contains(w.FileProfileTypeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("FileProfileTypeSetAccess", "");
                fileProfileTypeSetAccess.ForEach(s =>
                {
                    FileProfileTypeSetAccessModel fileProfileTypeSetAccessModel = new FileProfileTypeSetAccessModel();
                    fileProfileTypeSetAccessModel.FileProfileTypeSetAccessId = s.FileProfileTypeSetAccessId;
                    fileProfileTypeSetAccessModel.UserId = s.UserId;
                    fileProfileTypeSetAccessModel.DocumentId = s.DocumentId;
                    var document = documents.FirstOrDefault(f => f.DocumentId == s.DocumentId);
                    fileProfileTypeSetAccessModel.DocumentName = document?.FileName;
                    var fileProfileTypeName = fileProfileType != null ? fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == document?.FilterProfileTypeId) : null;
                    fileProfileTypeSetAccessModel.FileProfileTypeName = fileProfileTypeName?.Name;
                    fileProfileTypeSetAccessModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.FileProfileTypeSetAccessId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    fileProfileTypeSetAccessModels.Add(fileProfileTypeSetAccessModel);
                });
            }
            return fileProfileTypeSetAccessModels;
        }

        [HttpPut]
        [Route("UpdateFileProfileTypeSetAccess")]
        public BulkTransferTaskModel UpdateFileProfileTypeSetAccess(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update FileProfileTypeSetAccess Set UserId = {0} Where FileProfileTypeSetAccessId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "FileProfileTypeSetAccess");
            }
            return value;
        }
        #endregion
        #region FolderDiscussionUser
        [HttpPost]
        [Route("GetFolderDiscussionUserByUserId")]
        public List<FolderDiscussionUserModel> GetFolderDiscussionUserByUserId(SearchModel searchModel)
        {
            List<FolderDiscussionUserModel> folderDiscussionUserModels = new List<FolderDiscussionUserModel>();

            var folderDiscussionUser = _context.FolderDiscussionUser.Where(w => w.UserId == searchModel.UserID).ToList();
            if (folderDiscussionUser != null && folderDiscussionUser.Count > 0)
            {
                var foldersId = folderDiscussionUser.Select(s => s.FolderId).Distinct().ToList();
                var folders = _context.Folders.Select(s => new { s.Name, s.FolderId, s.Description }).Where(w => foldersId.Contains(w.FolderId)).ToList();
                var transferPermissionLog = TransferPermissionLog("FolderDiscussionUser", "");
                folderDiscussionUser.ForEach(s =>
                {
                    FolderDiscussionUserModel folderDiscussionUserModel = new FolderDiscussionUserModel();
                    folderDiscussionUserModel.FolderDiscussionNotesId = s.FolderDiscussionNotesId;
                    folderDiscussionUserModel.FolderDiscussionNotesUserId = s.FolderDiscussionNotesUserId;
                    folderDiscussionUserModel.UserId = s.UserId;
                    folderDiscussionUserModel.FolderId = s.FolderId;
                    folderDiscussionUserModel.FolderName = folders != null ? folders.FirstOrDefault(f => f.FolderId == s.FolderId)?.Name : null;
                    folderDiscussionUserModel.Description = folders != null ? folders.FirstOrDefault(f => f.FolderId == s.FolderId)?.Description : null;
                    folderDiscussionUserModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.FolderDiscussionNotesId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    folderDiscussionUserModels.Add(folderDiscussionUserModel);
                });
            }
            return folderDiscussionUserModels;
        }
        [HttpPut]
        [Route("UpdateFolderDiscussionUser")]
        public BulkTransferTaskModel UpdateFolderDiscussionUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update FolderDiscussionUser Set UserId = {0} Where FolderDiscussionNotesUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "FolderDiscussionUser");
            }
            return value;
        }
        #endregion
        #region TaskAppointment
        [HttpPost]
        [Route("GetTaskAppointmentUserByUserId")]
        public List<TaskAppointmentModel> GetTaskAppointmentUserByUserId(SearchModel searchModel)
        {
            List<TaskAppointmentModel> taskAppointmentModels = new List<TaskAppointmentModel>();

            var taskAppointment = _context.TaskAppointment.Where(w => w.AppointmentBy == searchModel.UserID).ToList();
            if (taskAppointment != null && taskAppointment.Count > 0)
            {
                var taskIds = taskAppointment.Select(s => s.TaskMasterId.GetValueOrDefault(0)).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var statusCodeIds = taskAppointment.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskAppointment", "");
                taskAppointment.ForEach(s =>
                {
                    TaskAppointmentModel taskAppointmentModel = new TaskAppointmentModel();
                    taskAppointmentModel.TaskAppointmentID = s.TaskAppointmentId;
                    taskAppointmentModel.AppointmentBy = s.AppointmentBy.Value;
                    taskAppointmentModel.DiscussionDate = s.DiscussionDate.Value;
                    taskAppointmentModel.DiscussionNotes = s.DiscussionNotes;
                    taskAppointmentModel.TaskTitle = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId)?.Title : null;
                    taskAppointmentModel.Status = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskAppointmentModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskAppointmentId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskAppointmentModels.Add(taskAppointmentModel);
                });
            }
            return taskAppointmentModels;
        }
        [HttpPut]
        [Route("UpdateTaskAppointmentUser")]
        public BulkTransferTaskModel UpdateTaskAppointmentUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskAppointment Set AppointmentBy = {0} Where TaskAppointmentId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskAppointment");
            }
            return value;
        }
        #endregion
        #region TaskCommentUser
        [HttpPost]
        [Route("GetTaskCommentUserByUserId")]
        public List<TaskCommentUserModel> GetTaskCommentUserByUserId(SearchModel searchModel)
        {
            List<TaskCommentUserModel> taskCommentUserModels = new List<TaskCommentUserModel>();
            var taskCommentUser = _context.TaskCommentUser.Where(w => w.UserId == searchModel.UserID).ToList();
            if (taskCommentUser != null && taskCommentUser.Count > 0)
            {
                var taskIds = taskCommentUser.Select(s => s.TaskMasterId).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var statusCodeIds = taskCommentUser.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var taskCommentIds = taskCommentUser.Select(s => s.TaskCommentId).Distinct().ToList();
                var taskComment = _context.TaskComment.Select(s => new { s.TaskCommentId, s.TaskSubject, s.CommentedDate }).Where(w => taskCommentIds.Contains(w.TaskCommentId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskCommentUser", "");
                taskCommentUser.ForEach(s =>
                {
                    TaskCommentUserModel taskCommentUserModel = new TaskCommentUserModel();
                    taskCommentUserModel.TaskCommentUserId = s.TaskCommentUserId;
                    taskCommentUserModel.TaskCommentId = s.TaskCommentId;
                    taskCommentUserModel.TaskMasterId = s.TaskMasterId;
                    taskCommentUserModel.UserId = s.UserId;
                    taskCommentUserModel.StatusCodeID = s.StatusCodeId;
                    taskCommentUserModel.IsAssignedTo = s.IsAssignedTo;
                    taskCommentUserModel.IsRead = s.IsRead;
                    taskCommentUserModel.DueDate = s.DueDate;
                    taskCommentUserModel.IsClosed = s.IsClosed;
                    taskCommentUserModel.Title = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskMasterId)?.Title : null;
                    taskCommentUserModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskCommentUserModel.TaskSubject = taskComment != null ? taskComment.FirstOrDefault(f => f.TaskCommentId == s.TaskCommentId)?.TaskSubject : null;
                    taskCommentUserModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskCommentUserId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskCommentUserModels.Add(taskCommentUserModel);
                });
            }
            return taskCommentUserModels;
        }
        [HttpPut]
        [Route("UpdateTaskCommentUser")]
        public BulkTransferTaskModel UpdateTaskCommentUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskCommentUser Set UserId = {0} Where TaskCommentUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskCommentUser");
            }
            return value;
        }
        #endregion
        #region TaskDiscussion
        [HttpPost]
        [Route("GetTaskDiscussionByUserId")]
        public List<TaskDiscussionModel> GetTaskDiscussionByUserId(SearchModel searchModel)
        {
            List<TaskDiscussionModel> taskDiscussionModels = new List<TaskDiscussionModel>();
            var taskDiscussion = _context.TaskDiscussion.Where(w => w.UserId == searchModel.UserID).ToList();
            if (taskDiscussion != null && taskDiscussion.Count > 0)
            {
                var taskIds = taskDiscussion.Select(s => s.TaskId.GetValueOrDefault(0)).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var statusCodeIds = taskDiscussion.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskDiscussion", "");
                taskDiscussion.ForEach(s =>
                {
                    TaskDiscussionModel taskDiscussionModel = new TaskDiscussionModel();
                    taskDiscussionModel.DiscussionNotesID = s.DiscussionNotesId;
                    taskDiscussionModel.TaskID = s.TaskId;
                    taskDiscussionModel.UserID = s.UserId;
                    taskDiscussionModel.StatusCodeID = s.StatusCodeId;
                    taskDiscussionModel.DiscussionDate = s.DiscussionDate;
                    taskDiscussionModel.DiscussionNotes = s.DiscussionNotes;
                    taskDiscussionModel.Title = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskId)?.Title : null;
                    taskDiscussionModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskDiscussionModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.DiscussionNotesId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskDiscussionModels.Add(taskDiscussionModel);
                });
            }
            return taskDiscussionModels;
        }
        [HttpPut]
        [Route("UpdateTaskDiscussion")]
        public BulkTransferTaskModel UpdateTaskDiscussion(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskDiscussion Set UserId = {0} Where DiscussionNotesId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskDiscussion");
            }
            return value;
        }
        #endregion
        #region TaskInviteUser
        [HttpPost]
        [Route("GetTaskInviteUserByUserId")]
        public List<TaskInviteUserModel> GetTaskInviteUserByUserId(SearchModel searchModel)
        {
            List<TaskInviteUserModel> taskInviteUserModels = new List<TaskInviteUserModel>();
            var taskInviteUser = _context.TaskInviteUser.Where(w => w.InviteUserId == searchModel.UserID).ToList();
            if (taskInviteUser != null && taskInviteUser.Count > 0)
            {
                var taskIds = taskInviteUser.Select(s => s.TaskId.GetValueOrDefault(0)).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var statusCodeIds = taskInviteUser.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskInviteUser", "");
                taskInviteUser.ForEach(s =>
                {
                    TaskInviteUserModel taskInviteUserModel = new TaskInviteUserModel();
                    taskInviteUserModel.TaskInviteUserId = s.TaskInviteUserId;
                    taskInviteUserModel.InviteUserId = s.InviteUserId;
                    taskInviteUserModel.TaskId = s.TaskId;
                    taskInviteUserModel.StatusCodeId = s.StatusCodeId;
                    taskInviteUserModel.InvitedDate = s.InvitedDate;
                    taskInviteUserModel.DueDate = s.DueDate;
                    taskInviteUserModel.Title = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskId)?.Title : null;
                    taskInviteUserModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskInviteUserModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskInviteUserId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskInviteUserModels.Add(taskInviteUserModel);
                });
            }
            return taskInviteUserModels;
        }
        [HttpPut]
        [Route("UpdateTaskInviteUser")]
        public BulkTransferTaskModel UpdateTaskInviteUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskInviteUser Set UserId = {0} Where TaskInviteUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskInviteUser");
            }
            return value;
        }
        #endregion
        #region TaskMembers
        [HttpPost]
        [Route("GetTaskMembersByUserId")]
        public List<TaskMembersModel> GetTaskMembersByUserId(SearchModel searchModel)
        {
            List<TaskMembersModel> taskMembersModels = new List<TaskMembersModel>();
            var taskMembers = _context.TaskMembers.Where(w => w.AssignedCc == searchModel.UserID).ToList();
            if (taskMembers != null && taskMembers.Count > 0)
            {
                var taskIds = taskMembers.Select(s => s.TaskId.GetValueOrDefault(0)).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var statusCodeIds = taskMembers.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskMembers", "");
                taskMembers.ForEach(s =>
                {
                    TaskMembersModel taskMembersModel = new TaskMembersModel();
                    taskMembersModel.TaskMemberID = s.TaskMemberId;
                    taskMembersModel.AssignedCC = s.AssignedCc;
                    taskMembersModel.TaskID = s.TaskId;
                    taskMembersModel.StatusCodeID = s.StatusCodeId;
                    taskMembersModel.DueDate = s.DueDate;
                    taskMembersModel.NewDueDate = s.NewDueDate;
                    taskMembersModel.Title = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskId)?.Title : null;
                    taskMembersModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskMembersModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskMemberId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskMembersModels.Add(taskMembersModel);
                });
            }
            return taskMembersModels;
        }
        [HttpPut]
        [Route("UpdateTaskMembers")]
        public BulkTransferTaskModel UpdateTaskMembers(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskMembers Set AssignedCc = {0} Where TaskMemberId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskMembers");
            }
            return value;
        }
        #endregion
        #region TaskNotes
        [HttpPost]
        [Route("GetTaskNotesByUserId")]
        public List<TaskNotesModel> GetTaskNotesByUserId(SearchModel searchModel)
        {
            List<TaskNotesModel> taskNotesModels = new List<TaskNotesModel>();
            var taskNotes = _context.TaskNotes.Where(w => w.TaskUserId == searchModel.UserID).ToList();
            if (taskNotes != null && taskNotes.Count > 0)
            {
                var taskIds = taskNotes.Select(s => s.TaskId.GetValueOrDefault(0)).Distinct().ToList();
                var taskmaster = _context.TaskMaster.Select(s => new { s.TaskId, s.Title }).Where(w => taskIds.Contains(w.TaskId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskNotes", "");
                taskNotes.ForEach(s =>
                {
                    TaskNotesModel taskNotesModel = new TaskNotesModel();
                    taskNotesModel.TaskNotesID = s.TaskNotesId;
                    taskNotesModel.TaskUserID = s.TaskUserId;
                    taskNotesModel.TaskID = s.TaskId;
                    taskNotesModel.Notes = s.Notes;
                    taskNotesModel.RemainderDate = s.RemainderDate;
                    taskNotesModel.Title = taskmaster != null ? taskmaster.FirstOrDefault(f => f.TaskId == s.TaskId)?.Title : null;
                    taskNotesModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskNotesId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    taskNotesModels.Add(taskNotesModel);
                });
            }
            return taskNotesModels;
        }
        [HttpPut]
        [Route("UpdateTaskNotes")]
        public BulkTransferTaskModel UpdateTaskNotes(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TaskNotes Set TaskUserId = {0} Where TaskNotesId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TaskNotes");
            }
            return value;
        }
        #endregion
        #region TeamMember
        [HttpPost]
        [Route("GetTeamMemberByUserId")]
        public List<TeamMemberModel> GetTeamMemberByUserId(SearchModel searchModel)
        {
            List<TeamMemberModel> teamMemberModels = new List<TeamMemberModel>();
            var teamMember = _context.TeamMember.Where(w => w.MemberId == searchModel.UserID).ToList();
            if (teamMember != null && teamMember.Count > 0)
            {
                var teamIds = teamMember.Select(s => s.TeamId.GetValueOrDefault(0)).Distinct().ToList();
                var teamMaster = _context.TeamMaster.Select(s => new { s.TeamMasterId, s.Name, s.Description, s.StatusCodeId }).Where(w => teamIds.Contains(w.TeamMasterId)).ToList();
                var statusCodeIds = teamMaster.Select(s => s.StatusCodeId).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TeamMember", "");
                teamMember.ForEach(s =>
                {
                    TeamMemberModel teamMemberModel = new TeamMemberModel();
                    teamMemberModel.TeamMemberID = s.TeamMemberId;
                    teamMemberModel.MemberID = s.MemberId;
                    teamMemberModel.TeamID = s.TeamId;
                    teamMemberModel.StatusCodeID = teamMaster != null ? teamMaster.FirstOrDefault(f => f.TeamMasterId == s.TeamId)?.StatusCodeId : null;
                    teamMemberModel.TeamName = teamMaster != null ? teamMaster.FirstOrDefault(f => f.TeamMasterId == s.TeamId)?.Name : null;
                    teamMemberModel.Description = teamMaster != null ? teamMaster.FirstOrDefault(f => f.TeamMasterId == s.TeamId)?.Description : null;
                    teamMemberModel.StatusCode = codemaster != null && teamMemberModel.StatusCodeID != null ? codemaster.FirstOrDefault(f => f.CodeId == teamMemberModel.StatusCodeID)?.CodeValue : null;
                    teamMemberModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TeamMemberId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    teamMemberModels.Add(teamMemberModel);
                });
            }
            return teamMemberModels;
        }
        [HttpPut]
        [Route("UpdateTeamMember")]
        public BulkTransferTaskModel UpdateTeamMember(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update TeamMember Set MemberId = {0} Where TeamMemberId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "TeamMember");
            }
            return value;
        }
        #endregion
        #region NoticeUser
        [HttpPost]
        [Route("GetNoticeUserByUserId")]
        public List<NoticeUserModel> GetNoticeUserByUserId(SearchModel searchModel)
        {
            List<NoticeUserModel> noticeUserModels = new List<NoticeUserModel>();
            var noticeUser = _context.NoticeUser.Where(w => w.UserId == searchModel.UserID).ToList();
            if (noticeUser != null && noticeUser.Count > 0)
            {
                var noticeIds = noticeUser.Select(s => s.NoticeId.GetValueOrDefault(0)).Distinct().ToList();
                var notice = _context.Notice.Select(s => new { s.NoticeId, s.Name, s.Description, s.StatusCodeId }).Where(w => noticeIds.Contains(w.NoticeId)).ToList();
                var statusCodeIds = notice.Select(s => s.StatusCodeId).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("NoticeUser", "");
                noticeUser.ForEach(s =>
                {
                    NoticeUserModel noticeUserModel = new NoticeUserModel();
                    noticeUserModel.NoticeUserId = s.NoticeUserId;
                    noticeUserModel.NoticeId = s.NoticeId;
                    noticeUserModel.UserId = s.UserId;
                    noticeUserModel.StatusCodeID = notice != null ? notice.FirstOrDefault(f => f.NoticeId == s.NoticeId)?.StatusCodeId : null;
                    noticeUserModel.Name = notice != null ? notice.FirstOrDefault(f => f.NoticeId == s.NoticeId)?.Name : null;
                    noticeUserModel.Description = notice != null ? notice.FirstOrDefault(f => f.NoticeId == s.NoticeId)?.Description : null;
                    noticeUserModel.StatusCode = codemaster != null && noticeUserModel.StatusCodeID != null ? codemaster.FirstOrDefault(f => f.CodeId == noticeUserModel.StatusCodeID)?.CodeValue : null;
                    noticeUserModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.NoticeUserId).OrderByDescending(o => o.TransferPermissionLogId).ToList();
                    noticeUserModels.Add(noticeUserModel);
                });
            }
            return noticeUserModels;
        }
        [HttpPut]
        [Route("UpdateNoticeUser")]
        public BulkTransferTaskModel UpdateNoticeUser(BulkTransferTaskModel value)
        {
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                int pageSize = 100;
                int page = 0;
                while (true)
                {
                    var inpReports = value.TransferIds.Skip(page * pageSize).Take(pageSize).ToList();
                    if (inpReports.Count > 0)
                    {
                        var query = string.Format("Update NoticeUser Set UserId = {0} Where NoticeUserId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', inpReports));
                        _context.Database.ExecuteSqlRaw(query);
                    }
                    if (inpReports.Count <= 0)
                        break;
                    page++;
                }
                AddTransferPermissionLog(value, "NoticeUser");
            }
            return value;
        }
        #endregion
        #region TaskMaster
        [HttpPost]
        [Route("GetTaskMasterByUserId")]
        public List<TaskMasterModel> GetTaskMasterByUserId(SearchModel searchModel)
        {
            List<TaskMasterModel> taskMasterModels = new List<TaskMasterModel>();
            var taskMaster = _context.TaskMaster.Where(w => w.OwnerId == searchModel.UserID || w.OnBehalf == searchModel.UserID).ToList();
            if (taskMaster != null && taskMaster.Count > 0)
            {
                var statusCodeIds = taskMaster.Select(s => s.StatusCodeId.GetValueOrDefault(0)).Distinct().ToList();
                var codemaster = _context.CodeMaster.Select(c => new { c.CodeId, c.CodeValue }).Where(w => statusCodeIds.Contains(w.CodeId)).ToList();
                var transferPermissionLog = TransferPermissionLog("TaskMaster", "");
                taskMaster.ForEach(s =>
                {
                    TaskMasterModel taskMasterModel = new TaskMasterModel();
                    taskMasterModel.TaskID = s.TaskId;
                    taskMasterModel.Title = s.Title;
                    taskMasterModel.Description = s.Description;
                    taskMasterModel.DueDate = s.DueDate;
                    taskMasterModel.AddedDate = s.AddedDate;
                    taskMasterModel.StatusCode = codemaster != null ? codemaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : null;
                    taskMasterModel.TransferPermissionLogModels = transferPermissionLog.Where(w => w.PrimaryTableId == s.TaskId).OrderByDescending(o => o.TransferPermissionLogId).ToList();

                    taskMasterModels.Add(taskMasterModel);
                });
            }
            return taskMasterModels;
        }
        [HttpPut]
        [Route("UpdateTaskMaster")]
        public BulkTransferTaskModel UpdateTaskMaster(BulkTransferTaskModel value)
        {
            var values = value;
            if (value.TransferIds != null && value.TransferIds.Count > 0)
            {
                var taskmasterOwnerIds = _context.TaskMaster.Select(s => new { s.TaskId, s.OwnerId }).Where(s => values.TransferIds.Contains(s.TaskId) && s.OwnerId == value.TransferFromUserId).Select(s => s.TaskId).ToList();
                if (taskmasterOwnerIds.Count > 0)
                {
                    var query = string.Format("Update TaskMaster Set OwnerId = {0} Where TaskId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', taskmasterOwnerIds));
                    value.TransferIds = taskmasterOwnerIds;
                    _context.Database.ExecuteSqlRaw(query);
                    AddTransferPermissionLog(value, "TaskMaster", "OwnerId");
                }
                var taskmasterOnBehalfIds = _context.TaskMaster.Select(s => new { s.TaskId, s.OnBehalf }).Where(s => values.TransferIds.Contains(s.TaskId) && s.OnBehalf == value.TransferFromUserId).Select(s => s.TaskId).ToList();
                if (taskmasterOnBehalfIds.Count > 0)
                {
                    var query = string.Format("Update TaskMaster Set OnBehalf = {0} Where TaskId in" + '(' + "{1}" + ')', value.TransferToUserId, string.Join(',', taskmasterOnBehalfIds));
                    value.TransferIds = taskmasterOnBehalfIds;
                    _context.Database.ExecuteSqlRaw(query);
                    AddTransferPermissionLog(value, "TaskMaster", "OnBehalf");
                }
            }
            return values;
        }
        #endregion
    }
}
