﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PlantMaintenanceEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public PlantMaintenanceEntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/PlantMaintenanceEntry
        [HttpGet]
        [Route("GetPlantMaintenanceEntrys")]
        public List<PlantMaintenanceEntryModel> Get()
        {
            List<PlantMaintenanceEntryModel> plantMaintenanceEntryModels = new List<PlantMaintenanceEntryModel>();
            var plantMaintenanceEntrys = _context.PlantMaintenanceEntry.Include("AddedByUser").Include("ModifiedByUser")
                .Include(s=>s.Company).Include(s=>s.SpecificArea).Include(s=>s.SpecificArea.ParentIct).Include(s => s.SpecificArea.ParentIct.ParentIct).Include(s => s.SpecificArea.ParentIct.ParentIct.ParentIct).Include(s => s.SpecificArea.ParentIct.ParentIct.ParentIct.ParentIct)
                .OrderByDescending(o => o.PlantEntryId).AsNoTracking().ToList();
            if(plantMaintenanceEntrys!=null && plantMaintenanceEntrys.Count>0)
            {
                plantMaintenanceEntrys.ForEach(s =>
                {
                    PlantMaintenanceEntryModel plantMaintenanceEntryModel = new PlantMaintenanceEntryModel
                    {
                        PlantEntryId = s.PlantEntryId,
                        CompanyId = s.CompanyId,
                        //CompanyName = _context.Plant.Where(p=>p.CompanyId == s.CompanyId ).Select(a=>a.Description).ToString(),
                        CompanyName = s.Company!=null? s.Company.Description : "",
                        SiteId = s.SiteId,
                        //SiteName = s
                        SiteName =  s.SpecificArea?.ParentIct?.ParentIct?.ParentIct?.ParentIct?.Name,
                        ZoneId = s.ZoneId,
                        ZoneName = s.SpecificArea?.ParentIct?.ParentIct?.ParentIct?.Name,
                        LocationId = s.LocationId,
                        LocationName = s.SpecificArea?.ParentIct?.ParentIct?.Name,
                        AreaId = s.AreaId,
                        AreaName = s.SpecificArea?.ParentIct?.Name,
                        SpecificAreaId = s.SpecificAreaId,
                        SpecificAreaName =  s.SpecificArea?.Name,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantMaintenanceEntryModels.Add(plantMaintenanceEntryModel);

                });
            }
            return plantMaintenanceEntryModels;
        }
        [HttpGet]
        [Route("GetActivePlantMaintenanceEntrys")]
        public List<PlantMaintenanceEntryModel> GetActivePlantMaintenanceEntrys()
        {
            List<PlantMaintenanceEntryModel> plantMaintenanceEntryModels = new List<PlantMaintenanceEntryModel>();
            var plantMaintenanceEntrys = _context.PlantMaintenanceEntry.Include("AddedByUser").Include("ModifiedByUser").Where(a => a.StatusCodeId == 1).OrderByDescending(o => o.PlantEntryId).AsNoTracking().ToList();
           if(plantMaintenanceEntrys!=null && plantMaintenanceEntrys.Count>0)
            {
                plantMaintenanceEntrys.ForEach(s =>
                {
                    PlantMaintenanceEntryModel plantMaintenanceEntryModel = new PlantMaintenanceEntryModel
                    {
                        PlantEntryId = s.PlantEntryId,
                        CompanyId = s.CompanyId,
                        SiteId = s.SiteId,
                        ZoneId = s.ZoneId,
                        LocationId = s.LocationId,
                        AreaId = s.AreaId,
                        SpecificAreaId = s.SpecificAreaId,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate
                    };
                    plantMaintenanceEntryModels.Add(plantMaintenanceEntryModel);
                });
            }
                   
            return plantMaintenanceEntryModels;
        }

        //Changes Done by Aravinth Start

        [HttpPost]
        [Route("GetPlantMaintenanceEntryFilter")]
        public List<PlantMaintenanceEntryModel> GetFilter(PlantMaintenanceEntrySearchModel value)
        {
            var plantMaintenanceEntry = _context.PlantMaintenanceEntry.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.PlantEntryId).AsNoTracking().ToList();
            List<PlantMaintenanceEntryModel> plantMaintenanceEntryModels = new List<PlantMaintenanceEntryModel>();
            if (plantMaintenanceEntry!=null && plantMaintenanceEntry.Count>0)
            {
                plantMaintenanceEntry.ForEach(s =>
                {
                    PlantMaintenanceEntryModel plantMaintenanceEntryModel = new PlantMaintenanceEntryModel
                    {
                        PlantEntryId = s.PlantEntryId,
                        CompanyName =s.Company!=null? s.Company.Description : "",
                        SiteName = s.SpecificArea.ParentIct.ParentIct.ParentIct.ParentIct!=null? s.SpecificArea.ParentIct.ParentIct.ParentIct.ParentIct.Name : "",
                        ZoneName = s.SpecificArea.ParentIct.ParentIct.ParentIct!=null?s.SpecificArea.ParentIct.ParentIct.ParentIct.Name : "",
                        LocationName = s.SpecificArea.ParentIct.ParentIct!=null? s.SpecificArea.ParentIct.ParentIct.Name : "",
                        AreaName = s.SpecificArea.ParentIct!=null? s.SpecificArea.ParentIct.Name : "",
                        SpecificAreaName = s.SpecificArea!=null? s.SpecificArea.Name : "",

                    };
                    plantMaintenanceEntryModels.Add(plantMaintenanceEntryModel);
                });

            }
           

            List<PlantMaintenanceEntryModel> filteredItems = new List<PlantMaintenanceEntryModel>();
            if (value.CompanyName.Any())
            {
                var itemCompanyNameFilters = plantMaintenanceEntryModels.Where(p => p.CompanyName != null ? (p.CompanyName.ToLower() == (value.CompanyName.ToLower())) : false).ToList();
                itemCompanyNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.SiteName.Any())
            {
                var itemSiteNameFilters = plantMaintenanceEntryModels.Where(p => p.SiteName != null ? (p.SiteName.ToLower() == (value.SiteName.ToLower())) : false).ToList();
                itemSiteNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ZoneName.Any())
            {
                var itemZoneNameFilters = plantMaintenanceEntryModels.Where(p => p.ZoneName != null ? (p.ZoneName.ToLower() == (value.ZoneName.ToLower())) : false).ToList();
                itemZoneNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.LocationName.Any())
            {
                var itemLocationNameFilters = plantMaintenanceEntryModels.Where(p => p.LocationName != null ? (p.LocationName.ToLower() == (value.LocationName.ToLower())) : false).ToList();
                itemLocationNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.AreaName.Any())
            {
                var itemAreaNameFilters = plantMaintenanceEntryModels.Where(p => p.AreaName != null ? (p.AreaName.ToLower() == (value.AreaName.ToLower())) : false).ToList();
                itemAreaNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.SpecificAreaName.Any())
            {
                var itemSpecificAreaNameFilters = plantMaintenanceEntryModels.Where(p => p.SpecificAreaName != null ? (p.SpecificAreaName.ToLower() == (value.SpecificAreaName.ToLower())) : false).ToList();
                itemSpecificAreaNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.PlantEntryId == i.PlantEntryId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }

            return filteredItems;
        }

        //Changes Done by Aravinth End

        // GET: api/PlantMaintenanceEntry/2
        //[HttpGet("{id}", Name = "GetPlantMaintenanceEntry")]
        [HttpGet("GetPlantMaintenanceEntrys/{id:int}")]
        public ActionResult<PlantMaintenanceEntryModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var plantMaintenanceEntry = _context.PlantMaintenanceEntry.SingleOrDefault(p => p.PlantEntryId == id.Value);
            var result = _mapper.Map<PlantMaintenanceEntryModel>(plantMaintenanceEntry);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PlantMaintenanceEntryModel> GetData(SearchModel searchModel)
        {
            var plantMaintenanceEntry = new PlantMaintenanceEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).LastOrDefault();
                        break;
                    case "Previous":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).FirstOrDefault();
                        break;
                    case "Last":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).LastOrDefault();
                        break;
                    case "Next":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderBy(o => o.PlantEntryId).FirstOrDefault(s => s.PlantEntryId > searchModel.Id);
                        break;
                    case "Previous":
                        plantMaintenanceEntry = _context.PlantMaintenanceEntry.OrderByDescending(o => o.PlantEntryId).FirstOrDefault(s => s.PlantEntryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PlantMaintenanceEntryModel>(plantMaintenanceEntry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPlantMaintenanceEntrys")]
        public PlantMaintenanceEntryModel Post(PlantMaintenanceEntryModel value)
        {
            var plantMaintenanceEntry = _context.PlantMaintenanceEntry.SingleOrDefault(p => p.CompanyId == value.CompanyId && p.SiteId == value.SiteId && p.ZoneId == value.ZoneId && p.LocationId == value.LocationId && p.AreaId == value.AreaId && p.SpecificAreaId == value.SpecificAreaId);
            if (plantMaintenanceEntry == null)
            {
                var newplantMaintenanceEntry = new PlantMaintenanceEntry
                {
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    CompanyId = value.CompanyId,
                    SiteId = value.SiteId,
                    ZoneId = value.ZoneId,
                    LocationId = value.LocationId,
                    AreaId = value.AreaId,
                    SpecificAreaId = value.SpecificAreaId,
                    StatusCodeId = 1

                };
                _context.PlantMaintenanceEntry.Add(newplantMaintenanceEntry);
                _context.SaveChanges();
                value.PlantEntryId = newplantMaintenanceEntry.PlantEntryId;
                return value;
            }
            return _mapper.Map<PlantMaintenanceEntryModel>(plantMaintenanceEntry);
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdatePlantMaintenanceEntrys")]
        public PlantMaintenanceEntryModel Put(PlantMaintenanceEntryModel value)
        {
            var plantMaintenanceEntry = _context.PlantMaintenanceEntry.SingleOrDefault(p => p.PlantEntryId == value.PlantEntryId);
            plantMaintenanceEntry.ModifiedByUserId = value.ModifiedByUserID;
            plantMaintenanceEntry.ModifiedDate = DateTime.Now;
            plantMaintenanceEntry.CompanyId = value.CompanyId;
            plantMaintenanceEntry.SiteId = value.SiteId;
            plantMaintenanceEntry.ZoneId = value.ZoneId;
            plantMaintenanceEntry.StatusCodeId = value.StatusCodeID.Value;
            plantMaintenanceEntry.LocationId = value.LocationId;
            plantMaintenanceEntry.AreaId = value.AreaId;
            plantMaintenanceEntry.SpecificAreaId = value.SpecificAreaId;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeletePlantMaintenanceEntrys")]
        public void Delete(int id)
        {
            var plantMaintenanceEntry = _context.PlantMaintenanceEntry.SingleOrDefault(p => p.PlantEntryId == id);
            if (plantMaintenanceEntry != null)
            {
                _context.PlantMaintenanceEntry.Remove(plantMaintenanceEntry);
                _context.SaveChanges();
            }
        }
    }
}