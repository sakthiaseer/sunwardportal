﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CalibrationVendorInformationController : Controller
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CalibrationVendorInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCalibrationVendorInformation")]
        public List<CalibrationVendorInformationModel> Get()
        {
            var calibrationVendorInformation = _context.CalibrationVendorInfo.ToList();
            List<CalibrationVendorInformationModel> calibrationVendorInformationModel = new List<CalibrationVendorInformationModel>();
            calibrationVendorInformation.ForEach(s =>
            {
                CalibrationVendorInformationModel calibrationVendorInformationModels = new CalibrationVendorInformationModel
                {
                    CalibrationVendorInformationID = s.CalibrationVendorInformationId,
                    VendorName = s.VendorName,
                    MethodOfCalibration = s.MethodOfCalibration,
                    CalibrationLeadTime = s.CalibrationLeadTime,
                    MethodOfQuotation = s.MethodOfQuotation,
                    Rating = s.Rating,
                    CalibrationServiceInformationID = s.CalibrationServiceInformationId,
                    Status = s.Status,
                };
                calibrationVendorInformationModel.Add(calibrationVendorInformationModels);
            });
            return calibrationVendorInformationModel.OrderByDescending(a => a.CalibrationVendorInformationID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetCalibrationVendorInformation/{id:int}")]
        public ActionResult<CalibrationVendorInformationModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var calibrationVendorInformation = _context.CalibrationVendorInfo.SingleOrDefault(p => p.CalibrationVendorInformationId == id.Value);
            var result = _mapper.Map<CalibrationVendorInformationModel>(calibrationVendorInformation);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CalibrationVendorInformationModel> GetData(SearchModel searchModel)
        {
            var calibrationVendorInformation = new CalibrationVendorInfo();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderBy(o => o.CalibrationVendorInformationId).FirstOrDefault(s => s.CalibrationVendorInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        calibrationVendorInformation = _context.CalibrationVendorInfo.OrderByDescending(o => o.CalibrationVendorInformationId).FirstOrDefault(s => s.CalibrationVendorInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CalibrationVendorInformationModel>(calibrationVendorInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCalibrationVendorInformation")]
        public CalibrationVendorInformationModel Post(CalibrationVendorInformationModel value)
        {
            var calibrationVendorInformation = new CalibrationVendorInfo
            {
                VendorName = value.VendorName,
                MethodOfCalibration = value.MethodOfCalibration,
                CalibrationLeadTime = value.CalibrationLeadTime,
                MethodOfQuotation = value.MethodOfQuotation,
                Rating = value.Rating,
                CalibrationServiceInformationId = value.CalibrationServiceInformationID,
                Status = value.Status,

            };

            _context.CalibrationVendorInfo.Add(calibrationVendorInformation);
            _context.SaveChanges();
            value.CalibrationVendorInformationID = calibrationVendorInformation.CalibrationVendorInformationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCalibrationVendorInformation")]
        public CalibrationVendorInformationModel Put(CalibrationVendorInformationModel value)
        {
            var calibrationVendorInformation = _context.CalibrationVendorInfo.SingleOrDefault(p => p.CalibrationVendorInformationId == value.CalibrationVendorInformationID);

            calibrationVendorInformation.VendorName = value.VendorName;
            calibrationVendorInformation.MethodOfCalibration = value.MethodOfCalibration;
            calibrationVendorInformation.CalibrationLeadTime = value.CalibrationLeadTime;
            calibrationVendorInformation.MethodOfQuotation = value.MethodOfQuotation;
            calibrationVendorInformation.Rating = value.Rating;
            calibrationVendorInformation.CalibrationServiceInformationId = value.CalibrationServiceInformationID;
            calibrationVendorInformation.Status = value.Status;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCalibrationVendorInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var calibrationVendorInformation = _context.CalibrationVendorInfo.SingleOrDefault(p => p.CalibrationVendorInformationId == id);
                if (calibrationVendorInformation != null)
                {
                    _context.CalibrationVendorInfo.Remove(calibrationVendorInformation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}