﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingCartonController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingCartonController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingCarton")]
        public List<CommonPackagingCartonModel> Get()
        {
            var commonPackagingCarton = _context.CommonPackagingCarton
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingCartonModel> commonPackagingCartonModel = new List<CommonPackagingCartonModel>();
            if (commonPackagingCarton.Count > 0)
            {
                List<long?> masterIds = commonPackagingCarton.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                masterIds.AddRange(commonPackagingCarton.Where(w => w.TypeWallId != null).Select(a => a.TypeWallId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingCarton.ForEach(s =>
                {
                    CommonPackagingCartonModel commonPackagingCartonModels = new CommonPackagingCartonModel
                    {
                        CartonSpecificationId = s.CartonSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        OuterMeasurementLength = s.OuterMeasurementLength,
                        OuterMeasurementWidth = s.OuterMeasurementWidth,
                        OuterMeasurementHeight = s.OuterMeasurementHeight,
                        InnerMeasurementLength = s.InnerMeasurementLength,
                        InnerMeasurementWidth = s.InnerMeasurementWidth,
                        InnerMeasurementHeight = s.InnerMeasurementHeight,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        TypeWallId = s.TypeWallId,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.CartonSpecificationId == s.CartonSpecificationId).Select(b => b.UseForItemId).ToList(),
                        TypeWallName = s.TypeWallId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeWallId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    commonPackagingCartonModel.Add(commonPackagingCartonModels);
                });
            }
            return commonPackagingCartonModel.OrderByDescending(a => a.CartonSpecificationId).ToList();
        }
        [HttpPost]
        [Route("GetCommonPackagingCartonByRefNo")]
        public List<CommonPackagingCartonModel> GetCommonPackagingCartonByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingCarton = _context.CommonPackagingCarton
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).Where(w => w.CartonSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingCarton = commonPackagingCarton.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingCarton = commonPackagingCarton.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingCartons = commonPackagingCarton.AsNoTracking().ToList();
            List<CommonPackagingCartonModel> commonPackagingCartonModel = new List<CommonPackagingCartonModel>();
            if (commonPackagingCartons.Count > 0)
            {
                List<long?> masterIds = commonPackagingCartons.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                masterIds.AddRange(commonPackagingCartons.Where(w => w.TypeWallId != null).Select(a => a.TypeWallId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingCartons.ForEach(s =>
                {
                    CommonPackagingCartonModel commonPackagingCartonModels = new CommonPackagingCartonModel
                    {
                        CartonSpecificationId = s.CartonSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        OuterMeasurementLength = s.OuterMeasurementLength,
                        OuterMeasurementWidth = s.OuterMeasurementWidth,
                        OuterMeasurementHeight = s.OuterMeasurementHeight,
                        InnerMeasurementLength = s.InnerMeasurementLength,
                        InnerMeasurementWidth = s.InnerMeasurementWidth,
                        InnerMeasurementHeight = s.InnerMeasurementHeight,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        TypeWallId = s.TypeWallId,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.CartonSpecificationId == s.CartonSpecificationId).Select(b => b.UseForItemId).ToList(),
                        TypeWallName = s.TypeWallId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeWallId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingCartonModel.Add(commonPackagingCartonModels);
                });
            }
            return commonPackagingCartonModel.OrderByDescending(o => o.CartonSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingCartonModel> GetData(SearchModel searchModel)
        {
            var commonPackagingCarton = new CommonPackagingCarton();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderBy(o => o.CartonSpecificationId).FirstOrDefault(s => s.CartonSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingCarton = _context.CommonPackagingCarton.OrderByDescending(o => o.CartonSpecificationId).FirstOrDefault(s => s.CartonSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingCartonModel>(commonPackagingCarton);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingCarton")]
        public CommonPackagingCartonModel Post(CommonPackagingCartonModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingCarton" });
            var commonPackagingCarton = new CommonPackagingCarton
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                OuterMeasurementLength = value.OuterMeasurementLength,
                OuterMeasurementWidth = value.OuterMeasurementWidth,
                OuterMeasurementHeight = value.OuterMeasurementHeight,
                InnerMeasurementLength = value.InnerMeasurementLength,
                InnerMeasurementWidth = value.InnerMeasurementWidth,
                InnerMeasurementHeight = value.InnerMeasurementHeight,
                TypeWallId = value.TypeWallId,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingCarton.Add(commonPackagingCarton);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingCarton.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingCarton.ProfileLinkReferenceNo;
            value.CartonSpecificationId = commonPackagingCarton.CartonSpecificationId;
            value.PackagingItemName = UpdateCommonPackage(value);
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.CartonSpecificationId == value.CartonSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        CartonSpecificationId = value.CartonSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingCartonModel value)
        {
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{

            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    //var codemasterlist = _context.CodeMaster.ToList();
                    var applicationmasterlist = _context.ApplicationMasterDetail.ToList();
                    var typeofwall = applicationmasterlist.Where(s => s.ApplicationMasterDetailId == value.TypeWallId).Select(s => s.Value).SingleOrDefault();
                    if (value.OuterMeasurementLength != null || value.OuterMeasurementHeight != null || value.OuterMeasurementWidth != null)
                    {
                        itemName = "(O)";
                    }
                    if (value.OuterMeasurementLength != null)
                    {
                        itemName = itemName + " " + value.OuterMeasurementLength + "mm" + " ";
                    }

                    if (value.OuterMeasurementWidth != null)
                    {
                        itemName = itemName + "X" + " " + value.OuterMeasurementWidth + "mm" + " ";
                    }
                    if (value.OuterMeasurementHeight != null)
                    {
                        itemName = itemName + "X" + " " + value.OuterMeasurementHeight + "mm" + " ";
                    }
                    if (value.InnerMeasurementHeight != null || value.InnerMeasurementLength != null || value.InnerMeasurementWidth != null)
                    {
                        itemName = itemName + "(I)" + " ";
                    }
                    if (value.InnerMeasurementLength != null)
                    {
                        itemName = itemName + value.InnerMeasurementLength + "mm" + " ";

                    }
                    if (value.InnerMeasurementWidth != null)
                    {
                        itemName = itemName + "X" + " " + value.InnerMeasurementWidth + "mm" + " ";
                    }
                    if (value.InnerMeasurementHeight != null)
                    {
                        itemName = itemName + "X" + " " + value.InnerMeasurementHeight + "mm" + " ";
                    }

                    itemName = itemName + typeofwall + " " + "Wall Carton";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingCarton")]
        public CommonPackagingCartonModel Put(CommonPackagingCartonModel value)
        {
            var commonPackagingCarton = _context.CommonPackagingCarton.SingleOrDefault(p => p.CartonSpecificationId == value.CartonSpecificationId);
            commonPackagingCarton.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingCarton.Set = value.Set;
            commonPackagingCarton.OuterMeasurementLength = value.OuterMeasurementLength;
            commonPackagingCarton.OuterMeasurementWidth = value.OuterMeasurementWidth;
            commonPackagingCarton.OuterMeasurementHeight = value.OuterMeasurementHeight;
            commonPackagingCarton.InnerMeasurementLength = value.InnerMeasurementLength;
            commonPackagingCarton.InnerMeasurementWidth = value.InnerMeasurementWidth;
            commonPackagingCarton.InnerMeasurementHeight = value.InnerMeasurementHeight;
            commonPackagingCarton.TypeWallId = value.TypeWallId;
            commonPackagingCarton.VersionControl = value.VersionControl;
            commonPackagingCarton.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingCarton.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingCarton.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingCarton.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingCarton.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.CartonSpecificationId == value.CartonSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.CartonSpecificationId == value.CartonSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        CartonSpecificationId = value.CartonSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingCarton")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingCarton = _context.CommonPackagingCarton.Where(p => p.CartonSpecificationId == id).FirstOrDefault();
                if (commonPackagingCarton != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.CartonSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingCarton.Remove(commonPackagingCarton);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}