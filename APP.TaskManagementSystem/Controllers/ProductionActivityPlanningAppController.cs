﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionActivityPlanningAppController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public ProductionActivityPlanningAppController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [HttpGet]
        [Route("GetProductionActivityPlanningAppLine")]
        public List<ProductionActivityPlanningAppModel> GetProductionActivityPlanningAppLine(long? id, long? userId, long? appId, long? locationId, string type)
        {
            var productActivityAppQuery = _context.ProductionActivityPlanningAppLine
                .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(a => a.ManufacturingProcessChild)
                .Include(a => a.ProdActivityActionChildDNavigation)
                .Include(a => a.ProdActivityCategoryChild)
                .Include(a => a.ProdActivityResult)
                .Include(a => a.ProductionActivityPlanningApp)
                .Include(a => a.ProductionActivityPlanningApp.Company)
                .Include(a => a.Location)
                .Include(a => a.StatusCode)
                 .Include(a => a.NavprodOrderLine)
                .Include(a => a.ProductActivityCaseLine)
                 .Where(w => w.ProductionActivityPlanningApp.CompanyId == id);
            if (appId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ManufacturingProcessChildId == appId && w.ProductionActivityPlanningApp.ManufacturingProcessChildId == appId);
            }
            if (locationId > 0)
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.LocationId == locationId);
            }
            if (!string.IsNullOrEmpty(type))
            {
                productActivityAppQuery = productActivityAppQuery.Where(w => w.ProductionActivityPlanningApp.ProdOrderNo == type);
            }
            //.Where(w => w.ProductionActivityPlanningApp.CompanyId == id && w.ProductionActivityPlanningApp.ManufacturingProcessChildId == appId && w.ManufacturingProcessChildId == appId && w.ProductionActivityPlanningApp.ProdOrderNo == type && w.LocationId == (locationId == -1 ? null : locationId));
            var productActivityApps = productActivityAppQuery.ToList();
            List<ProductionActivityPlanningAppModel> productActivityAppModels = new List<ProductionActivityPlanningAppModel>();
            if (productActivityApps != null && productActivityApps.Count > 0)
            {
                var productionActivityPlanningAppLineDoc = _context.ProductionActivityPlanningAppLineDoc.Where(w => w.Type == "Production Planning").ToList();
                var addedIds = productActivityApps.ToList().Select(s => s.AddedByUserId).ToList();
                addedIds.Add(userId);
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", addedIds.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.UserID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.DepartmentName,
                    s.PlantID,
                    s.DepartmentID
                }).ToList();
                var loginUser = employee.FirstOrDefault(w => w.UserID == userId)?.DepartmentID;
                var sessionIds = productActivityApps.ToList().Where(w => w.SessionId != null).Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.DocumentId, s.ContentType, s.SessionId, s.AddedByUserId, s.UploadDate, s.LockedByUserId, s.FileName, s.IsLocked, s.FilePath, s.IsLatest, s.DocumentParentId, s.FilterProfileTypeId }).Where(w => sessionIds.Contains(w.SessionId) && w.IsLatest == true).ToList();
                var docIds = documents.Select(s => s.DocumentId).ToList();
                var userIds = documents.Select(s => s.LockedByUserId.GetValueOrDefault(0)).ToList();
                var appUser = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).ToList();
                var notifyDoc = _context.NotifyDocument.Select(s => new { s.DocumentId, s.NotifyDocumentId }).Where(w => docIds.Contains(w.DocumentId.Value)).ToList();

                var templateTestCaseCheckListIds = productActivityApps.Where(w => w.ProductActivityCaseId != null).Select(s => s.ProductActivityCaseId).ToList();
                var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => templateTestCaseCheckListIds.Contains(w.ProductActivityCaseId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();


                productActivityApps.ForEach(s =>
                {
                    var checkSameDept = false;

                    if (s.AddedByUserId == userId)
                    {
                        checkSameDept = true;
                    }
                    else
                    {
                        var employeeDep = employee.FirstOrDefault(w => w.UserID == s.AddedByUserId)?.DepartmentID;
                        if (loginUser != null && employeeDep != null && loginUser == employeeDep)
                        {
                            checkSameDept = true;
                        }
                    }
                    if (checkSameDept == true)
                    {
                        List<long> responsibilityUsers = new List<long>();
                        if (s.ProductActivityCaseId > 0)
                        {
                            var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == s.ProductActivityCaseId && s.ProductActivityCaseId != null).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
                            var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
                            var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
                            var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
                            var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
                            var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                            var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                            var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();

                            if (appUserss != null && appUserss.Count > 0)
                            {
                                var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                                if (usersIdsBy != null && usersIdsBy.Count > 0)
                                {
                                    responsibilityUsers.AddRange(usersIdsBy);
                                }
                            }
                        }

                        ProductionActivityPlanningAppModel productActivityApp = new ProductionActivityPlanningAppModel();
                        productActivityApp.ResponsibilityUsers = responsibilityUsers;
                        productActivityApp.Type = "Production Planning";
                        productActivityApp.ProductActivityCaseId = s.ProductActivityCaseId;
                        productActivityApp.TicketNo = s.ProductionActivityPlanningApp?.ProdOrderNo;
                        productActivityApp.LocationName = s.Location?.Name + "||" + s.ProductionActivityPlanningApp?.ProdOrderNo;
                        productActivityApp.ProdOrderNo = s.ProductionActivityPlanningApp?.ProdOrderNo;
                        productActivityApp.SupportDocCount = productionActivityPlanningAppLineDoc.Where(w => w.Type == "Production Planning" && w.ProductionActivityPlanningAppLineId == s.ProductionActivityPlanningAppLineId).Count();
                        productActivityApp.ProductionActivityPlanningAppLineId = s.ProductionActivityPlanningAppLineId;
                        productActivityApp.ProductionActivityPlanningAppId = s.ProductionActivityPlanningApp.ProductionActivityPlanningAppId;
                        productActivityApp.Comment = s.ProductionActivityPlanningApp?.Comment;
                        productActivityApp.LineComment = s.Comment;
                        productActivityApp.ManufacturingProcessId = s.ManufacturingProcessId;
                        productActivityApp.ProdActivityResultId = s.ProdActivityResultId;
                        productActivityApp.ProdActivityResult = s.ProdActivityResult?.Value;
                        productActivityApp.ManufacturingProcess = s.ManufacturingProcess?.Value;
                        productActivityApp.CompanyId = s.ProductionActivityPlanningApp?.CompanyId;
                        productActivityApp.CompanyName = s.ProductionActivityPlanningApp?.Company?.PlantCode;
                        productActivityApp.ProdActivityActionId = s.ProdActivityActionId;
                        productActivityApp.ProdActivityAction = s.ProdActivityAction?.Value;
                        productActivityApp.ActionDropdown = s.ActionDropdown;
                        productActivityApp.ProdActivityCategoryId = s.ProdActivityCategoryId;
                        productActivityApp.ProdActivityCategory = s.ProdActivityCategory?.Value;
                        productActivityApp.IsTemplateUpload = s.IsTemplateUpload;
                        productActivityApp.IsTemplateUploadFlag = s.IsTemplateUpload == true ? "Yes" : "No";
                        productActivityApp.StatusCodeID = s.StatusCodeId;
                        productActivityApp.ModifiedByUserID = s.ModifiedByUserId;
                        productActivityApp.ModifiedDate = s.ModifiedDate;
                        productActivityApp.SessionId = s.ProductionActivityPlanningApp?.SessionId;
                        productActivityApp.LineSessionId = s.SessionId;
                        productActivityApp.AddedByUserID = s.AddedByUserId;
                        productActivityApp.AddedDate = s.AddedDate;
                        productActivityApp.AddedByUser = s.AddedByUser?.UserName;
                        productActivityApp.ModifiedByUser = s.ModifiedByUser?.UserName;
                        productActivityApp.StatusCode = s.StatusCode?.CodeValue;
                        productActivityApp.ProductActivityCaseLineId = s.ProductActivityCaseLineId;
                        productActivityApp.NameOfTemplate = s.ProductActivityCaseLine?.NameOfTemplate;
                        productActivityApp.Link = s.ProductActivityCaseLine?.Link;
                        productActivityApp.LocationToSaveId = s.ProductActivityCaseLine?.LocationToSaveId;
                        productActivityApp.QaCheck = s.QaCheck;
                        productActivityApp.ItemNo = s.NavprodOrderLine?.ItemNo;
                        productActivityApp.Description = s.NavprodOrderLine?.Description;
                        productActivityApp.Description1 = s.NavprodOrderLine?.Description1;
                        productActivityApp.BatchNo = s.NavprodOrderLine?.BatchNo;
                        productActivityApp.RePlanRefNo = s.NavprodOrderLine?.RePlanRefNo;
                        productActivityApp.NavprodOrderLineId = s.NavprodOrderLineId;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.ManufacturingProcessChildId = s.ManufacturingProcessChildId;
                        productActivityApp.ProdActivityActionChildD = s.ProdActivityActionChildD;
                        productActivityApp.ProdActivityCategoryChildId = s.ProdActivityCategoryChildId;
                        productActivityApp.ManufacturingProcessChild = s.ManufacturingProcessChild?.Value;
                        productActivityApp.ProdActivityActionChild = s.ProdActivityActionChildDNavigation?.Value;
                        productActivityApp.ProdActivityCategoryChild = s.ProdActivityCategoryChild?.Value;
                        productActivityApp.TopicId = s.TopicId;
                        productActivityApp.Type = "Production Planning";
                        productActivityApp.LocationId = s.ProductionActivityPlanningApp?.LocationId;
                        productActivityApp.IsOthersOptions = s.IsOthersOptions;
                        productActivityApp.ProductionActivityPlanningAppLineQaCheckerModels = GetProductionActivityPlanningAppLineQaChecker(s.ProductionActivityPlanningAppLineId);
                        productActivityApp.DocumentPermissionData = new DocumentPermissionModel();
                        if (documents != null && s.SessionId != null)
                        {
                            var counts = documents.FirstOrDefault(w => w.SessionId == s.SessionId);
                            if (counts != null)
                            {
                                productActivityApp.DocumentId = counts.DocumentId;
                                productActivityApp.DocumentID = counts.DocumentId;
                                productActivityApp.DocumentParentId = counts.DocumentParentId;
                                productActivityApp.FileName = counts.FileName;
                                productActivityApp.FilePath = counts.FilePath;
                                productActivityApp.ContentType = counts.ContentType;
                                productActivityApp.IsLocked = counts.IsLocked;
                                productActivityApp.LockedByUserId = counts.LockedByUserId;
                                productActivityApp.ModifiedDate = counts.UploadDate;
                                productActivityApp.ModifiedByUser = counts.AddedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.AddedByUserId)?.UserName : "";
                                productActivityApp.LockedByUser = counts.LockedByUserId != null ? appUser.FirstOrDefault(f => f.UserId == counts.LockedByUserId)?.UserName : "";
                                productActivityApp.NotifyCount = notifyDoc.Where(w => w.DocumentId == counts.DocumentId).Count();
                                productActivityApp.LocationToSaveId = counts.FilterProfileTypeId;
                                long? roleId = 0;
                                roleId = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId == counts.FilterProfileTypeId && f.UserId == userId)?.RoleId;
                                if (roleId != null)
                                {
                                    var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                                    if (documentPermission != null)
                                    {
                                        productActivityApp.DocumentPermissionData.IsRead = documentPermission.IsRead;
                                        productActivityApp.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                                        productActivityApp.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                                        productActivityApp.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                                        productActivityApp.DocumentPermissionData.IsRename = documentPermission.IsRename;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                                        productActivityApp.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                                        productActivityApp.DocumentPermissionData.IsMove = documentPermission.IsMove;
                                        productActivityApp.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                                        productActivityApp.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                                        productActivityApp.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                                        productActivityApp.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                                        productActivityApp.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                                        productActivityApp.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                                        productActivityApp.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                                        productActivityApp.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                                        productActivityApp.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                                        productActivityApp.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                                        productActivityApp.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                                        productActivityApp.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                                        productActivityApp.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                                        productActivityApp.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;
                                        productActivityApp.DocumentPermissionData.DocumentPermissionID = documentPermission.DocumentPermissionId;
                                    }
                                    else
                                    {
                                        productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                        productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                        productActivityApp.DocumentPermissionData.IsRead = false;
                                        productActivityApp.DocumentPermissionData.IsDelete = false;
                                        productActivityApp.DocumentPermissionData.IsUpdateDocument = false;
                                    }
                                }
                                else
                                {
                                    productActivityApp.DocumentPermissionData.IsCreateDocument = false;
                                    productActivityApp.DocumentPermissionData.IsDocumentAccess = false;
                                    productActivityApp.DocumentPermissionData.IsRead = true;
                                    productActivityApp.DocumentPermissionData.IsDelete = true;
                                    productActivityApp.DocumentPermissionData.IsUpdateDocument = true;
                                }
                            }
                            productActivityAppModels.Add(productActivityApp);
                        }
                    }
                });
            }
            return productActivityAppModels;
        }
        [HttpGet]
        [Route("GetProductionActivityPlanningAppLineQaChecker")]
        public List<ProductionActivityPlanningAppLineQaCheckerModel> GetProductionActivityPlanningAppLineQaChecker(long? id)
        {
            List<ProductionActivityPlanningAppLineQaCheckerModel> productionActivityAppLineQaCheckerModels = new List<ProductionActivityPlanningAppLineQaCheckerModel>();
            var productionActivityAppLineQaChecker = _context.ProductionActivityPlanningAppLineQaChecker.Include(a => a.QaCheckUser).Where(w => w.ProductionActivityPlanningAppLineId == id).ToList();
            productionActivityAppLineQaChecker.ForEach(s =>
            {
                ProductionActivityPlanningAppLineQaCheckerModel productionActivityAppLineQaCheckerModel = new ProductionActivityPlanningAppLineQaCheckerModel();
                productionActivityAppLineQaCheckerModel.ProductionActivityPlanningAppLineId = s.ProductionActivityPlanningAppLineId;
                productionActivityAppLineQaCheckerModel.ProductionActivityPlanningAppLineQaCheckerId = s.ProductionActivityPlanningAppLineQaCheckerId;
                productionActivityAppLineQaCheckerModel.QaCheck = s.QaCheck;
                productionActivityAppLineQaCheckerModel.QaCheckDate = s.QaCheckDate;
                productionActivityAppLineQaCheckerModel.QaCheckUserId = s.QaCheckUserId;
                productionActivityAppLineQaCheckerModel.QaCheckUser = s.QaCheckUser?.UserName;
                productionActivityAppLineQaCheckerModels.Add(productionActivityAppLineQaCheckerModel);
            });
            return productionActivityAppLineQaCheckerModels;
        }
        [Route("GetData")]
        public ActionResult<ProductActivityAppModel> GetData(SearchModel searchModel)
        {
            var processTransfer = new ProductionActivityApp();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault();
                        break;
                    case "Last":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).LastOrDefault();
                        break;
                    case "Next":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderBy(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId > searchModel.Id);
                        break;
                    case "Previous":
                        processTransfer = _context.ProductionActivityApp.Include(a => a.AddedByUser).OrderByDescending(o => o.ProductionActivityAppId).FirstOrDefault(s => s.ProductionActivityAppId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductActivityAppModel>(processTransfer);
            if (processTransfer != null)
            {
                result.AddedByUser = processTransfer.AddedByUser?.UserName;
                //result.IsTemplateUploadFlag = processTransfer.IsTemplateUpload == true ? "Yes" : "No";
            }

            return result;
        }
        [HttpPost]
        [Route("InsertTopicId")]
        public ProductActivityAppModel InsertTopicId(ProductActivityAppModel value)
        {
            var productActivityApp = _context.ProductionActivityApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ProdOrderNo.ToLower() == value.ProdOrderNo.ToLower());
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();
                var productActivityApps = new ProductionActivityApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    ProdOrderNo = value.ProdOrderNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    TopicId = value.TopicId,
                };
                _context.ProductionActivityApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityAppId = productActivityApps.ProductionActivityAppId;
            }
            else
            {
                productActivityApp.TopicId = value.TopicId;
                value.ProductionActivityAppId = productActivityApp.ProductionActivityAppId;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertProductionActivityPlanningApp")]
        public ProductionActivityPlanningAppModel InsertProductActivityApp(ProductionActivityPlanningAppModel value)
        {
            value.LineSessionId ??= Guid.NewGuid();
            var productActivityApp = _context.ProductionActivityPlanningApp.SingleOrDefault(s => s.CompanyId == value.CompanyId && s.ManufacturingProcessChildId == value.ManufacturingProcessChildId && s.LocationId == value.LocationId && s.ProdOrderNo == value.ProdOrderNo);
            if (productActivityApp == null)
            {
                value.SessionId ??= Guid.NewGuid();
                var productActivityApps = new ProductionActivityPlanningApp
                {
                    CompanyId = value.CompanyId,
                    Comment = value.Comment,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = value.SessionId,
                    ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                    LocationId = value.LocationId == -1 ? null : value.LocationId,
                    ProdOrderNo = value.ProdOrderNo,
                    ProductActivityCaseId = value.ProductActivityCaseId,
                    IsOthersOptions = value.IsOthersOptions,
                };
                _context.ProductionActivityPlanningApp.Add(productActivityApps);
                _context.SaveChanges();
                value.ProductionActivityPlanningAppId = productActivityApps.ProductionActivityPlanningAppId;
            }
            else
            {
                value.SessionId ??= Guid.NewGuid();
                value.ProductionActivityPlanningAppId = productActivityApp.ProductionActivityPlanningAppId;
                productActivityApp.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
                productActivityApp.CompanyId = value.CompanyId;
                productActivityApp.Comment = value.Comment;
                productActivityApp.StatusCodeId = value.StatusCodeID.Value;
                productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
                productActivityApp.ModifiedDate = DateTime.Now;
                productActivityApp.SessionId = value.SessionId;
                productActivityApp.ProdOrderNo = value.ProdOrderNo;
                productActivityApp.LocationId = value.LocationId == -1 ? null : value.LocationId;
                productActivityApp.ProductActivityCaseId = value.ProductActivityCaseId;
                productActivityApp.IsOthersOptions = value.IsOthersOptions;
                _context.SaveChanges();
            }
            var productActivityAppLine = new ProductionActivityPlanningAppLine
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                ProductionActivityPlanningAppId = value.ProductionActivityPlanningAppId,
                ProdActivityActionId = value.ProdActivityActionId,
                ActionDropdown = value.ActionDropdown,
                ProdActivityCategoryId = value.ProdActivityCategoryId,
                IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = value.LineSessionId,
                ProductActivityCaseLineId = value.ProductActivityCaseLineId,
                NavprodOrderLineId = value.NavprodOrderLineId,
                Comment = value.LineComment,
                QaCheck = false,
                IsOthersOptions = value.IsOthersOptions,
                ProdActivityResultId = value.ProdActivityResultId,
                ManufacturingProcessChildId = value.ManufacturingProcessChildId,
                ProdActivityActionChildD = value.ProdActivityActionChildD,
                ProdActivityCategoryChildId = value.ProdActivityCategoryChildId,
                LocationId = value.LocationId == -1 ? null : value.LocationId,
                ProductActivityCaseId = value.ProductActivityCaseId,
            };
            _context.ProductionActivityPlanningAppLine.Add(productActivityAppLine);
            _context.SaveChanges();
            value.ProductionActivityPlanningAppLineId = productActivityAppLine.ProductionActivityPlanningAppLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateProductionActivityPlanningAppLine")]
        public ProductionActivityPlanningAppModel UpdateProductionActivityPlanningAppLine(ProductionActivityPlanningAppModel value)
        {
            value.LineSessionId ??= Guid.NewGuid();
            var productActivityApp = _context.ProductionActivityPlanningAppLine.SingleOrDefault(s => s.ProductionActivityPlanningAppLineId == value.ProductionActivityPlanningAppLineId);
            productActivityApp.ManufacturingProcessId = value.ManufacturingProcessId;
            productActivityApp.ProdActivityActionId = value.ProdActivityActionId;
            productActivityApp.ActionDropdown = value.ActionDropdown;
            productActivityApp.ProdActivityCategoryId = value.ProdActivityCategoryId;
            productActivityApp.IsTemplateUpload = value.IsTemplateUploadFlag == "Yes" ? true : false;
            productActivityApp.StatusCodeId = value.StatusCodeID.Value;
            productActivityApp.ModifiedByUserId = value.ModifiedByUserID;
            productActivityApp.ModifiedDate = DateTime.Now;
            productActivityApp.SessionId = value.LineSessionId;
            productActivityApp.Comment = value.LineComment;
            productActivityApp.IsOthersOptions = value.IsOthersOptions;
            productActivityApp.ProductActivityCaseLineId = value.ProductActivityCaseLineId;
            productActivityApp.ProdActivityResultId = value.ProdActivityResultId;
            productActivityApp.ManufacturingProcessChildId = value.ManufacturingProcessChildId;
            productActivityApp.ProdActivityActionChildD = value.ProdActivityActionChildD;
            productActivityApp.ProdActivityCategoryChildId = value.ProdActivityCategoryId;
            productActivityApp.LocationId = value.LocationId;
            productActivityApp.ProductActivityCaseId = value.ProductActivityCaseId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppPlanningQaCheck")]
        public ProductionActivityPlanningAppModel UpdateProductActivityAppPlanningQaCheck(ProductionActivityPlanningAppModel value)
        {
            var productActivityApp = _context.ProductionActivityPlanningAppLine.SingleOrDefault(s => s.ProductionActivityPlanningAppLineId == value.ProductionActivityPlanningAppLineId);

            productActivityApp.QaCheckUserId = value.QaCheckUserId;
            productActivityApp.QaCheckDate = DateTime.Now;
            productActivityApp.QaCheck = value.QaCheck;
            _context.SaveChanges();
            var productionActivityAppLine = new ProductionActivityPlanningAppLineQaChecker
            {
                QaCheck = value.QaCheck,
                QaCheckDate = DateTime.Now,
                QaCheckUserId = value.QaCheckUserId,
                ProductionActivityPlanningAppLineId = productActivityApp.ProductionActivityPlanningAppLineId,
            };
            _context.ProductionActivityPlanningAppLineQaChecker.Add(productionActivityAppLine);
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateProductActivityAppPlanningTopic")]
        public ProductionActivityPlanningAppModel UpdateProductActivityAppPlanningTopic(ProductionActivityPlanningAppModel value)
        {
            var productActivityApp = _context.ProductionActivityPlanningAppLine.SingleOrDefault(s => s.ProductionActivityPlanningAppLineId == value.ProductionActivityPlanningAppLineId);
            productActivityApp.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {
            var SessionId = new Guid(files["sessionId"].ToString());
            var filePath = files["filePath"].ToString();
            if (filePath == "filePath")
            {
                UploadFileAsByPath(files, SessionId);
            }
            else
            {
                var userId = new long?();
                var user = files["userID"].ToString();
                if (user != "" && user != "undefined" && user != null)
                {
                    userId = Convert.ToInt32(files["userID"].ToString());
                }
                var documentNo = "";
                var numberSeriesId = files["numberSeriesId"].ToString();
                if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
                {
                    documentNo = files["numberSeriesId"].ToString();
                }

                var videoFiles = files["isVideoFile"].ToString();

                var videoFile = false;
                if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
                {
                    videoFile = Convert.ToBoolean(videoFiles);
                }
                var fileProfileTypeId = new long?();
                var locationToSaveId = files["locationToSaveId"].ToString();
                var isTemplate = files["isTemplate"].ToString();
                if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
                {
                    fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                    if (isTemplate == "No")
                    {
                        var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                        if (profileId != null && profileId.ProfileId > 0)
                        {
                            documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                        }
                    }
                }
                var newFileName = files["newFileName"].ToString();
                var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    var ext = "";
                    var newFile = "";
                    if (f.FileName == "blob")
                    {
                        newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                    }
                    else
                    {
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                        newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhs);
                    }
                    if (videoFile == true)
                    {
                        ext = f.FileName;
                        int i = ext.LastIndexOf('.');
                        string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                        var fileName1 = SessionId + "." + rhs;
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                        using (var targetStream = System.IO.File.Create(serverPath))
                        {
                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }
                        if (docs == null)
                        {
                            var documents = new Documents
                            {
                                FileName = newFile,
                                ContentType = file.ContentType,
                                FileData = null,
                                FileSize = file.Length,
                                UploadDate = DateTime.Now,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                IsVideoFile = true,
                                IsLatest = true,
                                FilterProfileTypeId = fileProfileTypeId,
                                ProfileNo = documentNo,
                            };
                            _context.Documents.Add(documents);
                        }
                        else
                        {
                            docs.FileName = newFile;
                            docs.ContentType = file.ContentType;
                            docs.FileData = null;
                            docs.FileSize = file.Length;
                            docs.UploadDate = DateTime.Now;
                            docs.IsLatest = true;
                            docs.IsVideoFile = true;
                            docs.FilterProfileTypeId = fileProfileTypeId;
                            docs.ProfileNo = documentNo;
                        }
                    }
                    else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        if (docs == null)
                        {
                            var documents = new Documents
                            {
                                FileName = newFile,
                                ContentType = file.ContentType,
                                FileData = compressedData,
                                FileSize = fs.Length,
                                UploadDate = DateTime.Now,
                                SessionId = SessionId,
                                IsTemp = true,
                                IsCompressed = true,
                                AddedByUserId = userId,
                                AddedDate = DateTime.Now,
                                IsLatest = true,
                                FilterProfileTypeId = fileProfileTypeId,
                                ProfileNo = documentNo,
                            };
                            _context.Documents.Add(documents);
                        }
                        else
                        {
                            docs.FileName = newFile;
                            docs.ContentType = file.ContentType;
                            docs.FileData = compressedData;
                            docs.FileSize = fs.Length;
                            docs.UploadDate = DateTime.Now;
                            docs.IsLatest = true;
                            docs.IsVideoFile = false;
                            docs.FilterProfileTypeId = fileProfileTypeId;
                            docs.ProfileNo = documentNo;
                        }
                    }
                    var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                    if (documentNoSeries != null)
                    {
                        var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                        _context.Database.ExecuteSqlRaw(query);
                    }
                });
                _context.SaveChanges();
            }
            return Content(SessionId.ToString());


        }
        private void UploadFileAsByPath(IFormCollection files, Guid SessionId)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var documentNo = "";
            var numberSeriesId = files["numberSeriesId"].ToString();
            if (numberSeriesId != "" && numberSeriesId != "undefined" && numberSeriesId != null)
            {
                documentNo = files["numberSeriesId"].ToString();
            }

            var videoFiles = files["isVideoFile"].ToString();

            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            var fileProfileTypeId = new long?();
            var locationToSaveId = files["locationToSaveId"].ToString();
            var isTemplate = files["isTemplate"].ToString();
            if (locationToSaveId != "" && locationToSaveId != "undefined" && locationToSaveId != null)
            {
                fileProfileTypeId = Convert.ToInt32(files["locationToSaveId"].ToString());
                if (isTemplate == "No")
                {
                    var profileId = _context.FileProfileType.Where(w => w.FileProfileTypeId == fileProfileTypeId).SingleOrDefault();
                    if (profileId != null && profileId.ProfileId > 0)
                    {
                        documentNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profileId.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profileId.Name });
                    }
                }
            }
            var newFileName = files["newFileName"].ToString();
            var docs = _context.Documents.SingleOrDefault(s => s.SessionId == SessionId);
            var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + SessionId;
            if (!System.IO.Directory.Exists(serverPaths))
            {
                System.IO.Directory.CreateDirectory(serverPaths);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";
                var newFile = "";
                if (f.FileName == "blob")
                {
                    newFile = string.IsNullOrEmpty(newFileName) ? "MergedImage.pdf" : (newFileName + ".pdf");
                }
                else
                {
                    ext = f.FileName;
                    int j = ext.LastIndexOf('.');
                    string lhss = j < 0 ? ext : ext.Substring(0, j), rhss = j < 0 ? "" : ext.Substring(j + 1);
                    newFile = string.IsNullOrEmpty(newFileName) ? file.FileName : (newFileName + "." + rhss);
                }
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                var fileName1 = SessionId + "." + rhs;
                var serverPath = serverPaths + @"\" + newFile;
                var filePath = getNextFileName(serverPath);
                newFile = filePath.Replace(serverPaths + @"\", "");
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                if (docs == null)
                {
                    var documents = new Documents
                    {
                        FileName = newFile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = true,
                        IsLatest = true,
                        FilterProfileTypeId = fileProfileTypeId,
                        ProfileNo = documentNo,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                }
                var documentNoSeries = _context.DocumentNoSeries.FirstOrDefault(w => w.DocumentNo == documentNo && w.IsUpload == false);
                if (documentNoSeries != null)
                {
                    var query = string.Format("Update DocumentNoSeries Set IsUpload = null,SessionId='{1}' Where NumberSeriesId= {0}  ", documentNoSeries.NumberSeriesId, SessionId);
                    _context.Database.ExecuteSqlRaw(query);
                }
            });
            _context.SaveChanges();
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpPost]
        [Route("UploadSupportDocuments")]
        public IActionResult UploadSupportDocuments(IFormCollection files)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var productionActivityPlanningAppLineId = Convert.ToInt32(files["productionActivityPlanningAppLineId"].ToString());
            var SessionId = Guid.NewGuid();
            var videoFiles = files["isVideoFile"].ToString();
            var type = files["type"].ToString();
            var videoFile = false;
            if (videoFiles != "" && videoFiles != "undefined" && videoFiles != null)
            {
                videoFile = Convert.ToBoolean(videoFiles);
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var ext = "";

                if (videoFile == true)
                {
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                    var fileName1 = SessionId + "." + rhs;
                    var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                    using (var targetStream = System.IO.File.Create(serverPath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = true,
                        IsLatest = true,
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    InsertUploadDoument(documents.DocumentId, productionActivityPlanningAppLineId, type);
                }
                else
                {
                    var fs = file.OpenReadStream();
                    var br = new BinaryReader(fs);
                    Byte[] document = br.ReadBytes((Int32)fs.Length);
                    var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = compressedData,
                        FileSize = fs.Length,
                        UploadDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        IsLatest = true,
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    InsertUploadDoument(documents.DocumentId, productionActivityPlanningAppLineId, type);
                }

            });
            return Content(SessionId.ToString());

        }
        private void InsertUploadDoument(long documentId, long productionActivityPlanningAppLineId, string type)
        {
            var productionActivityAppLineDoc = new ProductionActivityPlanningAppLineDoc
            {
                DocumentId = documentId,
                ProductionActivityPlanningAppLineId = productionActivityPlanningAppLineId,
                Type = type,
            };
            _context.ProductionActivityPlanningAppLineDoc.Add(productionActivityAppLineDoc);
            _context.SaveChanges();
        }
        [HttpGet]
        [Route("GetSupportDocuments")]
        public List<DocumentsModel> GetSupportDocuments(long? id, string type)
        {
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var documentsId = _context.ProductionActivityPlanningAppLineDoc.Where(w => w.DocumentId != null && w.Type.ToLower() == type.ToLower());
            documentsId = documentsId.Where(w => w.ProductionActivityPlanningAppLineId == id);
            var documentsIds = documentsId.Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
            var documetsparents = _context.Documents.Where(s => documentsIds.Contains(s.DocumentId) && s.IsLatest == true).Select(s => new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.AddedDate,
                s.IsCompressed,
                s.FileIndex,
                s.ProfileNo,
                s.Description
            }).AsNoTracking().ToList();
            if (documetsparents != null)
            {
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).AsNoTracking().ToList();
                documetsparents.ForEach(documetsparent =>
                {
                    var fileName = documetsparent.FileName.Split('.');
                    DocumentsModel documentsModelss = new DocumentsModel
                    {
                        DocumentID = documetsparent.DocumentId,
                        FileName = documetsparent.FileIndex > 0 ? fileName[0] + "_V0" + documetsparent.FileIndex + "." + fileName[1] : documetsparent.FileName,
                        ContentType = documetsparent.ContentType,
                        FileSize = (long)Math.Round(Convert.ToDouble(documetsparent.FileSize / 1024)),
                        UploadDate = documetsparent.UploadDate,
                        SessionID = documetsparent.SessionId,
                        FilterProfileTypeId = documetsparent.FilterProfileTypeId,
                        DocumentParentId = documetsparent.DocumentParentId,
                        TableName = documetsparent.TableName,
                        Type = "Document",
                        AddedDate = documetsparent.AddedDate,
                        AddedByUser = appUsers.FirstOrDefault(f => f.UserId == documetsparent.AddedByUserId)?.UserName,
                        IsCompressed = documetsparent.IsCompressed,
                        FileIndex = documetsparent.FileIndex,
                        ProfileNo = documetsparent.ProfileNo,
                        Description = documetsparent.Description,
                    };
                    documentsModel.Add(documentsModelss);
                });
            }
            return documentsModel;
        }
        [HttpDelete]
        [Route("DeleteProductionActivityPlanningApp")]
        public ActionResult<string> DeleteProductionActivityPlanningApp(int id)
        {
            try
            {
                var ProductionActivityApp = _context.ProductionActivityPlanningAppLine.SingleOrDefault(p => p.ProductionActivityPlanningAppLineId == id);
                if (ProductionActivityApp != null)
                {
                    var query = string.Format("Update Documents Set Islatest={1}  Where SessionId='{0}'", ProductionActivityApp.SessionId, 0);
                    _context.Database.ExecuteSqlRaw(query);
                    _context.ProductionActivityPlanningAppLine.Remove(ProductionActivityApp);
                    _context.SaveChanges();
                    var ProductionActivityAppCount = _context.ProductionActivityPlanningAppLine.Where(p => p.ProductionActivityPlanningAppLineId == id).Count();
                    if (ProductionActivityAppCount == 0)
                    {
                        var ProductionActivityApps = _context.ProductionActivityPlanningApp.SingleOrDefault(p => p.ProductionActivityPlanningAppId == ProductionActivityApp.ProductionActivityPlanningAppId);
                        if (ProductionActivityApps != null)
                        {
                            _context.ProductionActivityPlanningApp.Remove(ProductionActivityApps);
                            _context.SaveChanges();
                        }
                    }
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
