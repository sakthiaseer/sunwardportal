﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskMembersController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskMembersController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskMembers")]
        public List<TaskMembersModel> Get()
        {
            var taskMembers = _context.TaskMembers.Select(s=>new TaskMembersModel
            {
                TaskMemberID = s.TaskMemberId,
                TaskID = s.TaskId,
                //OnBehalfID = s.OnBehalfId,
                //AssignedTo = s.AssignedTo,
                //AssignedFrom = s.AssignedFrom,
                AssignedCC = s.AssignedCc


            }).OrderByDescending(o => o.TaskMemberID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<TaskMembersModel>>(taskMembers);
            return taskMembers;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMembers/{id:int}")]
        public ActionResult<TaskMembersModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskMembers = _context.TaskMembers.SingleOrDefault(p => p.TaskMemberId == id.Value);
            var result = _mapper.Map<TaskMembersModel>(taskMembers);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskMembersModel> GetData(SearchModel searchModel)
        {
            var taskMembers = new TaskMembers();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).LastOrDefault();
                        break;
                    case "Next":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).LastOrDefault();
                        break;
                    case "Previous":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).FirstOrDefault();
                        break;
                    case "Last":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).LastOrDefault();
                        break;
                    case "Next":
                        taskMembers = _context.TaskMembers.OrderBy(o => o.TaskMemberId).FirstOrDefault(s => s.TaskMemberId > searchModel.Id);
                        break;
                    case "Previous":
                        taskMembers = _context.TaskMembers.OrderByDescending(o => o.TaskMemberId).FirstOrDefault(s => s.TaskMemberId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskMembersModel>(taskMembers);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTeam")]
        public TaskMembersModel Post(TaskMembersModel value)
        {
            var taskMembers = new TaskMembers
            {
                TaskMemberId=value.TaskMemberID,
                TaskId=value.TaskID,
                //OnBehalfId=value.OnBehalfID,
                //AssignedTo=value.AssignedTo,
                //AssignedFrom=value.AssignedFrom,
                AssignedCc=value.AssignedCC

                //FoldersId = value.FoldersId,
                // MemberId = value.MemberID,
                // TeamId = value.TeamID
                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID,
                //Name = value.Name,
                //Description = value.Description
            };
            _context.TaskMembers.Add(taskMembers);
            _context.SaveChanges();
            value.TaskMemberID = taskMembers.TaskMemberId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskMembers")]
        public TaskMembersModel Put(TaskMembersModel value)
        {
            var folders = _context.TaskMembers.SingleOrDefault(p => p.TaskMemberId ==value.TaskMemberID);
          //  folders.TaskMemberId = value.TaskMemberID;
            folders.TaskId = value.TaskID;
            //folders.OnBehalfId = value.OnBehalfID;
            //folders.AssignedTo = value.AssignedTo;
            //folders.AssignedFrom = value.AssignedFrom;
            folders.AssignedCc = value.AssignedCC;

            //teamMember.MemberId = value.MemberID;
            //teamMember.TeamId = value.TeamID;
            //teamMember.ModifiedByUserId = value.ModifiedByUserID;
            //teamMember.ModifiedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            //project.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskMembers")]
        public void Delete(int id)
        {
            var taskMembers = _context.TaskMembers.SingleOrDefault(p => p.TaskMemberId == id);
            if (taskMembers != null)
            {
                _context.TaskMembers.Remove(taskMembers);
                _context.SaveChanges();
            }
        }
    }
}