﻿using APP.DataAccess.Models;
using APP.Common;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CalibrationTypeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CalibrationTypeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetCalibrationTypes")]
        public List<CalibrationTypeModel> Get()
        {
            var calibrationType = _context.CalibrationType.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<CalibrationTypeModel> calibrationTypeModel = new List<CalibrationTypeModel>();
            calibrationType.ForEach(s =>
            {
                CalibrationTypeModel calibrationTypeModels = new CalibrationTypeModel
                {
                    CalibrationTypeID = s.CalibrationTypeId,
                    Name = s.Name,
                    Description = s.Description,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                calibrationTypeModel.Add(calibrationTypeModels);
            });
            return calibrationTypeModel.OrderByDescending(a => a.CalibrationTypeID).ToList();
        }
        [HttpGet]
        [Route("GetActiveCalibrationType")]
        public List<CalibrationTypeModel> GetActiveVendorList()
        {
            var calibrationType = _context.CalibrationType.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser)
                                    .Where(a => a.StatusCodeId == 1).AsNoTracking().ToList();
            List<CalibrationTypeModel> calibrationTypeModel = new List<CalibrationTypeModel>();
            calibrationType.ForEach(s =>
            {
                CalibrationTypeModel calibrationTypeModels = new CalibrationTypeModel
                {
                    CalibrationTypeID = s.CalibrationTypeId,
                    Description = s.Description,
                    Name = s.Name,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = DateTime.Now,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                };
                calibrationTypeModel.Add(calibrationTypeModels);
            });
            return calibrationTypeModel.OrderByDescending(a => a.CalibrationTypeID).ToList();
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<CalibrationTypeModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var calibrationType = _context.CalibrationType.SingleOrDefault(p => p.CalibrationTypeId == id.Value);
            var result = _mapper.Map<CalibrationTypeModel>(calibrationType);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CalibrationTypeModel> GetData(SearchModel searchModel)
        {
            var calibrationType = new CalibrationType();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).LastOrDefault();
                        break;
                    case "Previous":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).LastOrDefault();
                        break;
                    case "Next":
                        calibrationType = _context.CalibrationType.OrderBy(o => o.CalibrationTypeId).FirstOrDefault(s => s.CalibrationTypeId > searchModel.Id);
                        break;
                    case "Previous":
                        calibrationType = _context.CalibrationType.OrderByDescending(o => o.CalibrationTypeId).FirstOrDefault(s => s.CalibrationTypeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CalibrationTypeModel>(calibrationType);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCalibrationType")]
        public CalibrationTypeModel Post(CalibrationTypeModel value)
        {
            var calibrationType = new CalibrationType
            {

                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,


            };
            _context.CalibrationType.Add(calibrationType);
            _context.SaveChanges();
            value.CalibrationTypeID = calibrationType.CalibrationTypeId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCalibrationType")]
        public CalibrationTypeModel Put(CalibrationTypeModel value)
        {
            var calibrationType = _context.CalibrationType.SingleOrDefault(p => p.CalibrationTypeId == value.CalibrationTypeID);
            calibrationType.Name = value.Name;
            calibrationType.Description = value.Description;
            calibrationType.ModifiedByUserId = value.ModifiedByUserID;
            calibrationType.ModifiedDate = DateTime.Now;
            calibrationType.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCalibrationType")]
        public void Delete(int id)
        {


            try
            {
                var calibrationType = _context.CalibrationType.SingleOrDefault(p => p.CalibrationTypeId == id);
                if (calibrationType != null)
                {
                    _context.CalibrationType.Remove(calibrationType);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("This calibration are used!", ex);
            }
        }
    }
}