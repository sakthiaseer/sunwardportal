﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.Param;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyListingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly ApprovalService _approvalService;

        public CompanyListingController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate, ApprovalService approvalService)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
            _approvalService = approvalService;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetCompanyListings")]
        public List<CompanyListingModel> Get()
        {
            var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            if (companyListing != null)
            {
                companyListing.ForEach(s =>
                 {
                     CompanyListingModel companyListingModels = new CompanyListingModel
                     {
                         CompanyListingID = s.CompanyListingId,
                         CompanyListingName = s.CompanyName,
                         ProfileReferenceNo = s.ProfileReferenceNo,
                         AddedByUserID = s.AddedByUserId,
                         StatusCodeID = s.StatusCodeId,
                         AddedDate = s.AddedDate,
                         ModifiedDate = s.ModifiedDate,
                         ProfileID = s.ProfileId,
                         CustomerCodeId = s.CustomerCodeId,
                         SessionId = s.SessionId,
                         ItemClassificationMasterId = s.ItemClassificationMasterId,
                         No = s.No,
                         IsNonTransaction = s.IsNonTransaction,
                         LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,

                     };
                     companyListingModel.Add(companyListingModels);
                 });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingsForRegistration")]
        public List<CompanyListingModel> GetCompanyListingsForRegistration()
        {
            var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            if (companyListing != null)
            {
                companyListing.ForEach(s =>
                {
                    CompanyListingModel companyListingModels = new CompanyListingModel
                    {
                        CompanyListingID = s.CompanyListingId,
                        CompanyListingName = s.CompanyName,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileID = s.ProfileId,
                        CustomerCodeId = s.CustomerCodeId,
                        SessionId = s.SessionId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        No = s.No,
                        IsNonTransaction = s.IsNonTransaction,
                        LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,

                    };
                    companyListingModel.Add(companyListingModels);
                });
            }
            return companyListingModel.OrderBy(a => a.CompanyListingName).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingsForNonTransactions")]
        public List<CompanyListingModel> GetCompanyListingsForNonTransactions()
        {
            var companyListing = _context.CompanyListing.Where(s => s.StatusCodeId == 1 && (s.IsNonTransaction == null || s.IsNonTransaction == false)).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            if (companyListing != null)
            {
                companyListing.ForEach(s =>
                {
                    CompanyListingModel companyListingModels = new CompanyListingModel
                    {
                        CompanyListingID = s.CompanyListingId,
                        CompanyListingName = s.CompanyName,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileID = s.ProfileId,
                        CustomerCodeId = s.CustomerCodeId,
                        SessionId = s.SessionId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        No = s.No,
                        IsNonTransaction = s.IsNonTransaction,
                        LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    };
                    companyListingModel.Add(companyListingModels);
                });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCustomerCompanyId")]
        public List<long?> GetCustomerCompanyId(int Id)
        {
            var compantIds = new List<long?>();

            var refNp = _context.CompanyListing
                .FirstOrDefault(s => s.CompanyListingId == Id).ProfileReferenceNo;
            var sobyCutItems = _context.SobyCustomers.Include("SocustomersItemCrossReference.SobyCustomerSunwardEquivalent").Where(f => f.LinkProfileReferenceNo == refNp).AsNoTracking().ToList();
            sobyCutItems.ForEach(f =>
            {
                f.SocustomersItemCrossReference.ToList().ForEach(c =>
                {
                    c.SobyCustomerSunwardEquivalent.ToList().ForEach(e =>
                    {
                        compantIds.Add(e.OrderPlacetoCompanyId);
                    });
                });
            });
            return compantIds.Distinct().ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingsAll")]
        public List<CompanyListingModel> GetCompanyListingsAll()
        {
            var companyListing = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).
               Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            if (companyListing != null)
            {
                var companyListingIds = companyListing.Select(s => s.CompanyListingId).ToList();
                List<long?> masterIds = _context.CompanyListingCustomerCode.Where(w => companyListingIds.Contains(w.CompanyListingId.Value)).Select(s => s.CustomerCodeId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                companyListing.ForEach(s =>
                {
                    CompanyListingModel companyListingModels = new CompanyListingModel
                    {
                        CompanyListingID = s.CompanyListingId,
                        CompanyListingName = s.CompanyName,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        ProfileID = s.ProfileId,
                        CustomerCodeId = s.CustomerCodeId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        IsNonTransaction = s.IsNonTransaction,
                        LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                        CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                    };
                    if (companyListingModels != null)
                    {
                        companyListingModels.BlanketName = companyListingModels.CompanyListingName + " | " + companyListingModels.CustomerCode;
                    }
                    companyListingModel.Add(companyListingModels);
                });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).Where(a => a.CustomerCode.Trim() == "also known as").ToList();
        }

        [HttpGet]
        [Route("GetDistributorCustomerNameBySalesEntryId")]
        public List<CompanyListingModel> GetDistributorCustomerNameBySalesEntryId(int id)
        {
            List<long?> companylistingIds = new List<long?>();
            var purchaseItemids = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            if (purchaseItemids != null && purchaseItemids.Count > 0)
            {
                var selcompanylisting = _context.ContractDistributionSalesEntryLine.Where(c => purchaseItemids.Contains(c.PurchaseItemSalesEntryLineId.Value)).Select(c => c.SocustomerId).Distinct().ToList();
                if (selcompanylisting != null && selcompanylisting.Count > 0)
                {
                    companylistingIds.AddRange(selcompanylisting);
                }
            }
            var companyListing = _context.CompanyListing
               .Where(s => s.StatusCodeId == 1 && companylistingIds.Contains(s.CompanyListingId)).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                };

                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingForBlanketEntry")]

        public List<CompanyListingModel> GetCompanyListingForBlanketEntry()
        {
            var CompanyListingItems = _context.CompanyListing.Include(c => c.CompanyListingCompanyType).Select(s=>new { s.CompanyListingId,s.ProfileReferenceNo,s.CompanyName,s.CompanyListingCompanyType }).ToList();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();
            List<SocustomersIssueModel> SocustomersIssues = new List<SocustomersIssueModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 190, 267 };
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var companyCodeList = _context.CompanyListingCustomerCode.ToList();
            var SOCustomersIssue = _context.SocustomersIssue.Include(s => s.Socustomers).Include(a => a.SobyCustomersManner).Include(b => b.SobyCustomersTenderAgency).ToList();
            if (SOCustomersIssue.Count > 0)
            {
                SOCustomersIssue.ForEach(s =>
                {
                    SocustomersIssueModel SocustomersIssue = new SocustomersIssueModel();
                    SocustomersIssue.LinkProfileReferenceNo = s.Socustomers != null ? s.Socustomers.LinkProfileReferenceNo : "";
                    SocustomersIssue.CompanyListingID = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault() : 0;
                    SocustomersIssue.CompanyListingName = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault() : "";
                    SocustomersIssue.SocustomersIssueId = s.SocustomersIssueId;
                    SocustomersIssue.SocustomersId = s.SocustomersId;
                    SocustomersIssue.BuyingThroughId = s.BuyingThroughId;
                    SocustomersIssue.BuyingThrough = s.BuyingThrough?.CompanyName;
                    SocustomersIssue.SobyCustomersTenderAgencyList = (s.SobyCustomersTenderAgency != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TenderAgencyId && m.Value.ToLower() == "alps gpo").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssue.SobyCustomersMannerList = (s.SobyCustomersManner != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.MannersId && m.Value.ToLower() == "by tender agency").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    var companyListingCompanyType = CompanyListingItems.Where(c => c.CompanyListingId == SocustomersIssue.CompanyListingID).FirstOrDefault();
                    SocustomersIssue.CompanyListingTypeList = (companyListingCompanyType != null && companyListingCompanyType.CompanyListingCompanyType != null && masterDetailList != null ? (string.Join(",", companyListingCompanyType.CompanyListingCompanyType.Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CompanyTypeId && m.Value.ToLower() == "customer").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssues.Add(SocustomersIssue);
                });
                SocustomersIssues.ForEach(s =>
                {
                    s.CustomerCode = masterDetailList != null && companyCodeList != null ? string.Join(",", companyCodeList.Where(b => b.CompanyListingId == s.CompanyListingID).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                });
            };
            var socustomersIssuess = SocustomersIssues.Where(w => (w.SobyCustomersMannerList.ToLower() == "by tender agency" || w.SobyCustomersMannerList.ToLower() == "Third Party") && w.CompanyListingTypeList.ToLower() == "customer").ToList();
            var companyListingItems = socustomersIssuess.GroupBy(s => new { s.CompanyListingID, s.CompanyListingName, s.CustomerCode, s.BuyingThroughId, s.BuyingThrough }).Select(s => new { s.Key.CompanyListingID, s.Key.CompanyListingName, s.Key.CustomerCode, s.Key.BuyingThrough, s.Key.BuyingThroughId }).ToList();
            companyListingItems.ForEach(h =>
            {
                CompanyListingModel CompanyListingModel = new CompanyListingModel();
                CompanyListingModel.CompanyListingID = h.CompanyListingID;
                CompanyListingModel.CompanyListingName = h.CompanyListingName;
                CompanyListingModel.CustomerCode = h.CustomerCode;
                CompanyListingModel.BlanketName = h.CompanyListingName + " | " + h.CustomerCode;
                CompanyListingModel.BuyingThrough = h.BuyingThrough;
                CompanyListingModel.BuyingThroughId = h.BuyingThroughId;
                CompanyListingModels.Add(CompanyListingModel);
            });
            return CompanyListingModels;
        }

        [HttpGet]
        [Route("GetCompanyListingForAlpsTenderInformation")]

        public List<CompanyListingModel> GetCompanyListingForAlpsTenderInformation()
        {
            var CompanyListingItems = _context.CompanyListing.Include(c => c.CompanyListingCompanyType).Select(s=>new { s.ProfileReferenceNo,s.CompanyListingId,s.CompanyName,s.CompanyListingCompanyType }).ToList();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();
            List<SocustomersIssueModel> SocustomersIssues = new List<SocustomersIssueModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 190, 267 };
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var companyCodeList = _context.CompanyListingCustomerCode.ToList();
            var SOCustomersIssue = _context.SocustomersIssue.Include(s => s.Socustomers).Include(a => a.SobyCustomersManner).Include(b => b.SobyCustomersTenderAgency).ToList();
            if (SOCustomersIssue.Count > 0)
            {
                SOCustomersIssue.ForEach(s =>
                {
                    SocustomersIssueModel SocustomersIssue = new SocustomersIssueModel();
                    SocustomersIssue.LinkProfileReferenceNo = s.Socustomers != null ? s.Socustomers.LinkProfileReferenceNo : "";
                    SocustomersIssue.CompanyListingID = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault() : 0;
                    SocustomersIssue.CompanyListingName = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault() : "";
                    SocustomersIssue.SocustomersIssueId = s.SocustomersIssueId;
                    SocustomersIssue.SocustomersId = s.SocustomersId;
                    SocustomersIssue.BuyingThroughId = s.BuyingThroughId;
                    SocustomersIssue.BuyingThrough = s.BuyingThrough?.CompanyName;
                    SocustomersIssue.SobyCustomersTenderAgencyList = (s.SobyCustomersTenderAgency != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TenderAgencyId && m.Value.ToLower() == "alps gpo").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssue.SobyCustomersMannerList = (s.SobyCustomersManner != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.MannersId && m.Value.ToLower() == "by tender agency").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    var companyListingCompanyType = CompanyListingItems.Where(c => c.CompanyListingId == SocustomersIssue.CompanyListingID).FirstOrDefault();
                    SocustomersIssue.CompanyListingTypeList = (companyListingCompanyType != null && companyListingCompanyType.CompanyListingCompanyType != null && masterDetailList != null ? (string.Join(",", companyListingCompanyType.CompanyListingCompanyType.Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CompanyTypeId && m.Value.ToLower() == "customer").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssues.Add(SocustomersIssue);
                });
                SocustomersIssues.ForEach(s =>
                {
                    s.CustomerCode = masterDetailList != null && companyCodeList != null ? string.Join(",", companyCodeList.Where(b => b.CompanyListingId == s.CompanyListingID).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                });
            };
            var socustomersIssuess = SocustomersIssues.Where(w => (w.SobyCustomersMannerList.ToLower() == "by tender agency")).ToList();
            var companyListingItems = socustomersIssuess.GroupBy(s => new { s.CompanyListingID, s.CompanyListingName, s.CustomerCode, s.BuyingThroughId, s.BuyingThrough }).Select(s => new { s.Key.CompanyListingID, s.Key.CompanyListingName, s.Key.CustomerCode, s.Key.BuyingThrough, s.Key.BuyingThroughId }).ToList();
            companyListingItems.ForEach(h =>
            {
                CompanyListingModel CompanyListingModel = new CompanyListingModel();
                CompanyListingModel.CompanyListingID = h.CompanyListingID;
                CompanyListingModel.CompanyListingName = h.CompanyListingName;
                CompanyListingModel.CustomerCode = h.CustomerCode;
                CompanyListingModel.BlanketName = h.CompanyListingName + " | " + h.CustomerCode;
                CompanyListingModel.BuyingThrough = h.BuyingThrough;
                CompanyListingModel.BuyingThroughId = h.BuyingThroughId;
                CompanyListingModels.Add(CompanyListingModel);
            });
            return CompanyListingModels;
        }


        [HttpGet]
        [Route("GetCompanyListingByID")]
        public List<CompanyListingModel> GetCompanyListingByID(int id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var companyListing = _context.CompanyListing.
                Include(s => s.StatusCode).Include(l=>l.CompanyListingCompanyType).Include(c => c.CompanyListingCustomerCode).Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 190, 267 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName,
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate != null ? s.ModifiedDate : s.AddedDate,
                    ProfileID = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                    SessionId = s.SessionId,
                    CustomerCodeId = s.CustomerCodeId,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    CustomerCodeIds = s.CompanyListingCustomerCode != null ? s.CompanyListingCustomerCode.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CustomerCodeId).ToList() : new List<long?>(),
                    CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                    CompanyTypeIds = s.CompanyListingCompanyType != null ? s.CompanyListingCompanyType.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CompanyTypeId).ToList() : new List<long?>(),
                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.Where(i => i.CompanyListingID == id).OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpPost]
        [Route("GetCompanyListingByIDs")]
        public List<CompanyListingModel> GetCompanyListingByID(List<long> ids)
        {
            var companyListing = _context.CompanyListing
                .Where(s => s.StatusCodeId == 1 && ids.Contains(s.CompanyListingId)).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModels = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModel = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                };
                companyListingModels.Add(companyListingModel);
            });
            return companyListingModels;
        }

        [HttpGet]
        [Route("GetCompanyListingsByRefNo")]
        public List<CompanyListingModel> GetCompanyListingsByRefNo(int? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var companyListing = _context.CompanyListing.
                
                Include(c => c.CompanyListingCustomerCode).
                Include(l => l.CompanyListingCompanyType).
                Include(s => s.StatusCode).
                Include(p => p.Profile).Where(l => l.ItemClassificationMasterId == id).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            if (companyListing != null)
            {
                var companyListingIds = companyListing.Select(s => s.CompanyListingId).ToList();
                List<long?> masterIds = _context.CompanyListingCustomerCode.Where(w => companyListingIds.Contains(w.CompanyListingId.Value)).Select(s => s.CustomerCodeId).Distinct().ToList();
                masterIds.AddRange(_context.CompanyListingCompanyType.Where(w => companyListingIds.Contains(w.CompanyListingId.Value)).Select(s => s.CompanyTypeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                companyListing.ForEach(s =>
                {
                    CompanyListingModel companyListingModels = new CompanyListingModel
                    {
                        CompanyListingID = s.CompanyListingId,
                        CompanyListingName = s.CompanyName,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        AddedByUserID = s.AddedByUserId,
                        AddedByUser =appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName,
                        ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileID = s.ProfileId,
                        SessionId = s.SessionId,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        No = s.No,
                        CustomerCodeId = s.CustomerCodeId,
                        IsNonTransaction = s.IsNonTransaction,
                        LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                        CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                        CompanyTypeIds = s.CompanyListingCompanyType != null ? s.CompanyListingCompanyType.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CompanyTypeId).ToList() : new List<long?>(),
                        CustomerCodeIds = s.CompanyListingCustomerCode != null ? s.CompanyListingCustomerCode.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CustomerCodeId).ToList() : new List<long?>(),
                    };
                    companyListingModel.Add(companyListingModels);
                });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingCompanyType")]
        public List<CompanyListingCompanyTypeModel> GetCompanyListingCompanyType(string type)
        {
            var companyListing = _context.CompanyListingCompanyType.Include(a => a.CompanyListing).AsNoTracking().ToList();
            List<CompanyListingCompanyTypeModel> companyListingCompanyTypeModel = new List<CompanyListingCompanyTypeModel>();
            if (companyListing != null && companyListing.Count > 0)
            {
                List<long?> masterIds = companyListing.Where(w => w.CompanyTypeId != null).Select(a => a.CompanyTypeId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                companyListing.ForEach(s =>
                {
                    CompanyListingCompanyTypeModel companyListingCompanyTypeModels = new CompanyListingCompanyTypeModel
                    {
                        CompanyListingCompanyTypeID = s.CompanyListingCompanyTypeId,
                        CompanyListingID = s.CompanyListingId,
                        CompanyTypeID = s.CompanyTypeId,
                        CompanyName = s.CompanyListing?.CompanyName,
                        CompanyTypeName = s.CompanyTypeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.CompanyTypeId).Select(m => m.Value).FirstOrDefault() : "",
                    };
                    companyListingCompanyTypeModel.Add(companyListingCompanyTypeModels);
                });
            }
            return companyListingCompanyTypeModel.Where(l => l.CompanyTypeName != null && l.CompanyTypeName.ToLower() == type.ToLower()).OrderByDescending(a => a.CompanyListingCompanyTypeID).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingCompanyTypeList")]
        public List<CompanyListingCompanyTypeModel> GetCompanyListingCompanyTypeList()
        {
            var companyListing = _context.CompanyListingCompanyType.Include(a => a.CompanyListing).AsNoTracking().ToList();
            List<CompanyListingCompanyTypeModel> companyListingCompanyTypeModel = new List<CompanyListingCompanyTypeModel>();
            if (companyListing != null)
            {
                List<long?> masterIds = companyListing.Where(w => w.CompanyTypeId != null).Select(a => a.CompanyTypeId).Distinct().ToList();
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                companyListing.ForEach(s =>
                {
                    CompanyListingCompanyTypeModel companyListingCompanyTypeModels = new CompanyListingCompanyTypeModel
                    {
                        CompanyListingCompanyTypeID = s.CompanyListingCompanyTypeId,
                        CompanyListingID = s.CompanyListingId,
                        CompanyTypeID = s.CompanyTypeId,
                        CompanyName = s.CompanyListing.CompanyName,
                        CompanyTypeName = s.CompanyTypeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.CompanyTypeId).Select(m => m.Value).FirstOrDefault() : "",
                    };
                    companyListingCompanyTypeModel.Add(companyListingCompanyTypeModels);
                });
            }
            return companyListingCompanyTypeModel.Where(l => l.CompanyTypeName.ToLower() == "customer" || l.CompanyTypeName.ToLower() == "inter company").OrderByDescending(a => a.CompanyListingCompanyTypeID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingForProductGroup")]
        public List<CompanyListingModel> GetCompanyListingForProductGroup()
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 190, 267 };
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            if (applicationMasterIds.Count > 0)
            {
                applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            }
            var customercompanyTypeIds = applicationmasterdetail.Where(s => s.Value.ToLower().Trim() == "customer").Select(s => s.ApplicationMasterDetailId);
            var companylistingIds = _context.CompanyListingCompanyType.Where(c => customercompanyTypeIds.Contains(c.CompanyTypeId.Value)).Select(s => s.CompanyListingId).ToList();
            var companyListing = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).
                Include(s => s.StatusCode).Include(p => p.Profile).Where(s => companylistingIds.Contains(s.CompanyListingId)).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    StatusCodeID = s.StatusCodeId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    No = s.No,
                    CustomerCodeId = s.CustomerCodeId,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                    CustomerCodeIds = s.CompanyListingCustomerCode != null ? s.CompanyListingCustomerCode.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CustomerCodeId).ToList() : new List<long?>(),

                };
                companyListingModel.Add(companyListingModels);
            });
            if (companyListingModel != null && companyListingModel.Count > 0)
            {
                companyListingModel.ForEach(c =>
                {
                    c.BlanketName = c.CompanyListingName + " | " + c.CustomerCode;

                });
            }
            return companyListingModel.ToList();


        }
        [HttpGet]
        [Route("GetCompanyListingsForDistributionInformation")]
        public List<CompanyListingModel> GetCompanyListingsForDistributionInformation()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companyListing = _context.CompanyListing.Include(c => c.CompanyListingCustomerCode).Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                string CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    ProfileID = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                    DistributionSalesCustomerName = s.CompanyName + "|" + CustomerCode,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,

                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingsForBlanketOrder")]
        public List<CompanyListingModel> GetCompanyListingsForBlanketOrder()
        {
            var salesOrderCustomerIds = _context.SalesOrderEntry.Where(s => s.OrderById == 1403 && (s.TypeOfOrderId == 1406 || s.TypeOfOrderId == 1407 || s.TypeOfOrderId == 1409)).Select(s => s.CustomerId).ToList();
            var companyListing = _context.CompanyListing.Where(s => salesOrderCustomerIds.Contains(s.CompanyListingId) && s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    ProfileID = s.ProfileId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                    DistributionSalesCustomerName = s.No + "|" + s.CompanyName,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,

                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyListingModel> GetData(SearchModel searchModel)
        {
            var companyListing = new CompanyListing();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                    case "Last":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Next":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Previous":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                    case "Last":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Next":
                        companyListing = _context.CompanyListing.OrderBy(o => o.CompanyListingId).FirstOrDefault(s => s.CompanyListingId > searchModel.Id);
                        break;
                    case "Previous":
                        companyListing = _context.CompanyListing.OrderByDescending(o => o.CompanyListingId).FirstOrDefault(s => s.CompanyListingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyListingModel>(companyListing);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyListing")]
        public CompanyListingModel Post(CompanyListingModel value)
        {
            var itemclassificationName = "";
            if (value.ItemClassificationMasterId > 0)
            {
                itemclassificationName = _context.ItemClassificationMaster.FirstOrDefault(i => i.ItemClassificationId == value.ItemClassificationMasterId).Name;
            }
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = itemclassificationName });
            var companyListing = new CompanyListing
            {
                CompanyName = value.CompanyListingName,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileID,
                No = value.No,
                CustomerCodeId = value.CustomerCodeId,
                IsNonTransaction = value.IsNonTransaction,
                LinkNonTransactionCompanyId = value.LinkNonTransactionCompanyId,
            };
            companyListing.SessionId = Guid.NewGuid();
            _context.CompanyListing.Add(companyListing);
            _context.SaveChanges();
            value.CompanyListingID = companyListing.CompanyListingId;
            value.ProfileReferenceNo = companyListing.ProfileReferenceNo;
            if (value.CompanyTypeIds != null && value.CompanyTypeIds.Count > 0)
            {
                value.CompanyTypeIds.ForEach(f =>
                {
                    //var existing = _context.CompanyListingCompanyType.Where(c => c.CompanyListingId == value.CompanyListingID && c.CompanyTypeId == f).FirstOrDefault();
                    //if (existing == null)
                    //{
                    var CompanyListingCompanyType = new CompanyListingCompanyType
                    {
                        CompanyListingId = value.CompanyListingID,
                        CompanyTypeId = f,

                    };
                    _context.CompanyListingCompanyType.Add(CompanyListingCompanyType);
                    // }

                });
                _context.SaveChanges();
            }
            if (value.CustomerCodeIds != null && value.CustomerCodeIds.Count > 0)
            {
                value.CustomerCodeIds.ForEach(f =>
                {
                    var companyListingCustomerCode = new CompanyListingCustomerCode
                    {
                        CompanyListingId = value.CompanyListingID,
                        CustomerCodeId = f,
                    };
                    _context.CompanyListingCustomerCode.Add(companyListingCustomerCode);
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPost]
        [Route("InsertCompanyListingByCustomer")]
        public CompanyListingModel InsertCompanyListingByCustomer(CompanyListingModel value)
        {
            var companyListingData = _context.CompanyListing.Where(w => w.ItemClassificationMasterId != null && w.ProfileId != null).FirstOrDefault();
            if (companyListingData != null)
            {
                value.ItemClassificationMasterId = companyListingData.ItemClassificationMasterId;
                value.ProfileID = companyListingData.ProfileId;
                var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = value.StatusCodeID, Title = value.CompanyName });
                var companyListing = new CompanyListing
                {
                    CompanyName = value.CompanyListingName,
                    ItemClassificationMasterId = value.ItemClassificationMasterId,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    AddedByUserId = value.AddedByUserID.Value,
                    ProfileReferenceNo = profileNo,
                    ProfileId = value.ProfileID,
                    No = value.No,
                    CustomerCodeId = value.CustomerCodeId,
                    IsNonTransaction = value.IsNonTransaction,
                    LinkNonTransactionCompanyId = value.LinkNonTransactionCompanyId,
                };
                companyListing.SessionId = Guid.NewGuid();
                _context.CompanyListing.Add(companyListing);
                _context.SaveChanges();
                value.CompanyListingID = companyListing.CompanyListingId;
                value.ProfileReferenceNo = companyListing.ProfileReferenceNo;
                value.ProfileName = _context.DocumentProfileNoSeries.Where(w => w.ProfileId == companyListing.ProfileId).FirstOrDefault()?.Name;
                if (value.CompanyTypeIds != null && value.CompanyTypeIds.Count > 0)
                {
                    value.CompanyTypeIds.ForEach(f =>
                    {
                        var CompanyListingCompanyType = new CompanyListingCompanyType
                        {
                            CompanyListingId = value.CompanyListingID,
                            CompanyTypeId = f,

                        };
                        _context.CompanyListingCompanyType.Add(CompanyListingCompanyType);
                    });
                    _context.SaveChanges();
                }
                if (value.CustomerCodeIds != null && value.CustomerCodeIds.Count > 0)
                {
                    value.CustomerCodeIds.ForEach(f =>
                    {
                        var companyListingCustomerCode = new CompanyListingCustomerCode
                        {
                            CompanyListingId = value.CompanyListingID,
                            CustomerCodeId = f,
                        };
                        _context.CompanyListingCustomerCode.Add(companyListingCustomerCode);
                    });
                    _context.SaveChanges();
                }
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateCompanyListingByCustomer")]
        public CompanyListingModel UpdateCompanyListingByCustomer(CompanyListingModel value)
        {
            var companyListing = _context.CompanyListing.SingleOrDefault(p => p.CompanyListingId == value.CompanyListingID);
            companyListing.CompanyName = value.CompanyListingName;
            companyListing.ModifiedByUserId = value.ModifiedByUserID;
            companyListing.ModifiedDate = DateTime.Now;
            companyListing.StatusCodeId = value.StatusCodeID.Value;
            companyListing.IsNonTransaction = value.IsNonTransaction;
            companyListing.LinkNonTransactionCompanyId = value.LinkNonTransactionCompanyId;
            var companylistingcompanyType = _context.CompanyListingCompanyType.Where(t => t.CompanyListingId == value.CompanyListingID).ToList();
            if (companylistingcompanyType != null)
            {
                _context.CompanyListingCompanyType.RemoveRange(companylistingcompanyType);
                _context.SaveChanges();
            }
            if (value.CompanyTypeIds != null && value.CompanyTypeIds.Count > 0)
            {
                value.CompanyTypeIds.ForEach(f =>
                {
                    var CompanyListingCompanyType = new CompanyListingCompanyType
                    {
                        CompanyListingId = value.CompanyListingID,
                        CompanyTypeId = f,

                    };
                    _context.CompanyListingCompanyType.Add(CompanyListingCompanyType);
                });
                _context.SaveChanges();
            }
            var removeCustomerCode = _context.CompanyListingCustomerCode.Where(t => t.CompanyListingId == value.CompanyListingID).ToList();
            if (removeCustomerCode != null)
            {
                _context.CompanyListingCustomerCode.RemoveRange(removeCustomerCode);
                _context.SaveChanges();
            }
            if (value.CustomerCodeIds != null && value.CustomerCodeIds.Count > 0)
            {
                value.CustomerCodeIds.ForEach(f =>
                {
                    var companyListingCustomerCode = new CompanyListingCustomerCode
                    {
                        CompanyListingId = value.CompanyListingID,
                        CustomerCodeId = f,
                    };
                    _context.CompanyListingCustomerCode.Add(companyListingCustomerCode);
                });
                _context.SaveChanges();
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyListing")]
        public CompanyListingModel Put(CompanyListingModel value)
        {
            var companyListing = _context.CompanyListing.SingleOrDefault(p => p.CompanyListingId == value.CompanyListingID);
            companyListing.SessionId ??= Guid.NewGuid();
            companyListing.ProfileReferenceNo = value.ProfileReferenceNo;
            companyListing.CompanyName = value.CompanyListingName;
            companyListing.ModifiedByUserId = value.ModifiedByUserID;
            companyListing.ModifiedDate = DateTime.Now;
            companyListing.No = value.No;
            companyListing.ProfileId = value.ProfileID;
            companyListing.StatusCodeId = value.StatusCodeID.Value;
            companyListing.ItemClassificationMasterId = value.ItemClassificationMasterId;
            companyListing.CustomerCodeId = value.CustomerCodeId;
            companyListing.IsNonTransaction = value.IsNonTransaction;
            companyListing.LinkNonTransactionCompanyId = value.LinkNonTransactionCompanyId;
            var companylistingcompanyType = _context.CompanyListingCompanyType.Where(t => t.CompanyListingId == value.CompanyListingID).ToList();
            if (companylistingcompanyType != null)
            {
                _context.CompanyListingCompanyType.RemoveRange(companylistingcompanyType);
                _context.SaveChanges();
            }
            if (value.CompanyTypeIds != null && value.CompanyTypeIds.Count > 0)
            {
                value.CompanyTypeIds.ForEach(f =>
                {
                    var CompanyListingCompanyType = new CompanyListingCompanyType
                    {
                        CompanyListingId = value.CompanyListingID,
                        CompanyTypeId = f,

                    };
                    _context.CompanyListingCompanyType.Add(CompanyListingCompanyType);
                });
                _context.SaveChanges();
            }
            var removeCustomerCode = _context.CompanyListingCustomerCode.Where(t => t.CompanyListingId == value.CompanyListingID).ToList();
            if (removeCustomerCode != null)
            {
                _context.CompanyListingCustomerCode.RemoveRange(removeCustomerCode);
                _context.SaveChanges();
            }
            if (value.CustomerCodeIds != null && value.CustomerCodeIds.Count > 0)
            {
                value.CustomerCodeIds.ForEach(f =>
                {
                    var companyListingCustomerCode = new CompanyListingCustomerCode
                    {
                        CompanyListingId = value.CompanyListingID,
                        CustomerCodeId = f,
                    };
                    _context.CompanyListingCustomerCode.Add(companyListingCustomerCode);
                });
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyListing")]
        public void Delete(int id)
        {
            try
            {
                var CompanyListing = _context.CompanyListing.SingleOrDefault(p => p.CompanyListingId == id);
                if (CompanyListing != null)
                {
                    var companylistingcompanyType = _context.CompanyListingCompanyType.Where(t => t.CompanyListingId == CompanyListing.CompanyListingId).ToList();
                    if (companylistingcompanyType != null)
                    {
                        _context.CompanyListingCompanyType.RemoveRange(companylistingcompanyType);
                        _context.SaveChanges();
                    }
                    var removeCustomerCode = _context.CompanyListingCustomerCode.Where(t => t.CompanyListingId == CompanyListing.CompanyListingId).ToList();
                    if (removeCustomerCode != null)
                    {
                        _context.CompanyListingCustomerCode.RemoveRange(removeCustomerCode);
                        _context.SaveChanges();
                    }
                    _context.CompanyListing.Remove(CompanyListing);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                var errorMessage = "This record cannot be deleted! Reference to others!";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpPost]
        [Route("GetCompanyReports")]
        public List<ReportModel> GetCompanyReports(ClassificationReportParam reportParam)
        {
            List<ReportModel> reportModels = new List<ReportModel>();
            ItemClassificationController itemClassificationController = new ItemClassificationController(_context, _mapper, _generateDocumentNoSeries);
            List<ItemClassificationFormsModel> itemClassificationFormsModels = itemClassificationController.GetItemForm((int?)reportParam.ItemClassificationMasterID, (int)reportParam.UserId);

            ReportModel reportModel = new ReportModel();

            List<Header> headers = new List<Header>
            {
                new Header{Text="Profile Reference No",Value="profileReferenceNo",Align="left" },
                new Header{Text="Company Name",Value="companyListingName",Align="left" },
                new Header{Text="Status",Value="statusCode",Align="left" },
                new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                new Header{Text="Modified Date",Value="modifiedDate",Align="left" },

            };
            reportModel.Headers = headers;
            reportModel.HeaderName = "Company Header";
            var commonPackageModel = GetCompanyListingByID((int)reportParam.ItemId);
            commonPackageModel.ForEach(c =>
            {
                reportModel.DynamicModels.Add(c as dynamic);
            });
            reportModels.Add(reportModel);
            itemClassificationFormsModels.ForEach(i =>
            {
                reportModel = GenerateCPReportModel(i.ModuleName, reportParam, i.FormName);
                reportModels.Add(reportModel);
                if (i.ApplicationFormSubModelList != null)
                {
                    reportModel = new ReportModel();
                    i.ApplicationFormSubModelList.ForEach(s =>
                    {
                        reportModel = GenerateCPReportModel(s.ModuleName, reportParam, s.FormName);
                        reportModels.Add(reportModel);
                    });
                }
            });
            return reportModels;
        }
        private ReportModel GenerateCPReportModel(string moduleName, ClassificationReportParam reportParam, string FormName)
        {
            ReportModel reportModel = new ReportModel();
            SobyCustomersController sobyCustomers = new SobyCustomersController(_context, _mapper, _generateDocumentNoSeries, _approvalService);
            NavisionCompanyController navisionCompany = new NavisionCompanyController(_context, _mapper, _generateDocumentNoSeries);
            if (moduleName == "SObyCustomers")
            {
                reportModel.HeaderName = FormName;
                List<Header> recipeHeaders = new List<Header>
                {
                    new Header{Text="Payment mode",Value="paymentModeName",Align="left" },
                    new Header{Text="Activation Of Order",Value="activationOfOrderName",Align="left" },
                    new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                    new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                };
                reportModel.Headers = recipeHeaders;
                var sobyCustomersList = sobyCustomers.GetSobyCustomersByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });
                foreach (var item in sobyCustomersList)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            if (moduleName == "NavisionNamingCompany")
            {
                reportModel.HeaderName = FormName;
                List<Header> recipeHeaders = new List<Header>
                {
                    new Header{Text="Database",Value="databaseName",Align="left" },
                    new Header{Text="Company Type",Value="companyTypeName",Align="left" },
                    new Header{Text="Customer",Value="navCustomerName",Align="left" },
                    new Header{Text="Vendor",Value="navVendorName",Align="left" },
                    new Header{Text="Modified By",Value="modifiedByUser",Align="left" },
                    new Header{Text="Modified Date",Value="modifiedDate",Align="left" },
                };
                reportModel.Headers = recipeHeaders;
                var navisionCompanyList = navisionCompany.GetNavisionCompanyByRefNo(new RefSearchModel { IsHeader = true, ProfileReferenceNo = reportParam.ProfileReferenceNo });
                foreach (var item in navisionCompanyList)
                {
                    reportModel.DynamicModels.Add(item as dynamic);
                }
            }
            return reportModel;
        }
        [HttpGet]
        [Route("GetCompanyListingForBlanketTenderAgency")]
        public List<CompanyListingModel> GetCompanyListingForBlanketTenderAgency()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companylistingtype = _context.CompanyListingCompanyType.ToList();
            var businessCategoryId = applicationmasterdetail.Where(a => a.Value.ToLower().Trim() == "buyer - tender agency").Select(a => a.ApplicationMasterDetailId).FirstOrDefault();
            var comanyListingTypeId = applicationmasterdetail.Where(a => a.Value.ToLower().Trim() == "customer").Select(a => a.ApplicationMasterDetailId).FirstOrDefault();
            var customerCompanyListingId = _context.CompanyListingCompanyType.Where(s => s.CompanyListingCompanyTypeId == comanyListingTypeId).Select(s => s.CompanyListingId).Distinct().ToList();
            var companyListingIds = _context.CompanyListingLine.Where(s => s.BusinessCategoryId == businessCategoryId).Select(s => s.CompanyListingId).Distinct().ToList();

            var companyListing = _context.CompanyListing.Where(s => (companyListingIds.Contains(s.CompanyListingId) || customerCompanyListingId.Contains(s.CompanyListingId) && s.StatusCodeId == 1)).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileID = s.ProfileId,
                    CustomerCodeId = s.CustomerCodeId,
                    CustomerCode = s.CustomerCodeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.CustomerCodeId).Select(m => m.Value).FirstOrDefault() : "",
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    No = s.No,
                    CompanyTypeIds = companylistingtype != null ? companylistingtype.Where(c => c.CompanyListingId == s.CompanyListingId).Select(c => c.CompanyTypeId).ToList() : new List<long?>(),
                    BlanketName = s.CompanyName + " | " + (s.CustomerCodeId != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == s.CustomerCodeId).Select(m => m.Value).FirstOrDefault() : ""),
                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingForBuyerDistributor")]
        public List<CompanyListingModel> GetCompanyListingForBuyerDistributor()
        {
            var businessCategoryId = _context.ApplicationMasterDetail.Where(a => a.Value.ToLower().Trim() == "buyer -  distributor" || a.Value.ToLower().Trim() == "sw distributor").Select(a => a.ApplicationMasterDetailId).FirstOrDefault();
            var companyListingIds = _context.CompanyListingLine.Where(s => s.BusinessCategoryId == businessCategoryId).Select(s => s.CompanyListingId).Distinct().ToList();

            var companyListing = _context.CompanyListing
              .Where(s => (companyListingIds.Contains(s.CompanyListingId)) && s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileID = s.ProfileId,
                    CustomerCodeId = s.CustomerCodeId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId
                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingForProductGroupingManufacture")]
        public List<CompanyListingModel> GetCompanyListingForProductGroupingManufacture()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companylistingtype = _context.CompanyListingCompanyType.ToList();
            var businessCategoryIds = applicationmasterdetail.Where(a => a.Value.ToLower().Trim() == "supplier - principal" || a.Value.ToLower().Trim() == "sunward - group").Select(a => a.ApplicationMasterDetailId).ToList();
            var companyListingIds = _context.CompanyListingLine.Where(s => businessCategoryIds.Contains(s.BusinessCategoryId.Value)).Select(s => s.CompanyListingId).Distinct().ToList();

            var companyListing = _context.CompanyListing.
               Include(c => c.CompanyListingCustomerCode).Where(s => (companyListingIds.Contains(s.CompanyListingId)) && s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileID = s.ProfileId,
                    CustomerCodeId = s.CustomerCodeId,
                    CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                };
                companyListingModel.Add(companyListingModels);
            });
            if (companyListingModel != null && companyListingModel.Count > 0)
            {
                companyListingModel.ForEach(c =>
                {
                    c.BlanketName = c.CompanyListingName + " | " + c.CustomerCode;

                });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingForSampleRequestForm")]
        public List<CompanyListingModel> GetCompanyListingForSampleRequestForm()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companylistingtype = _context.CompanyListingCompanyType.ToList();
            var businessCategoryIds = applicationmasterdetail.Where(a => a.Value.ToLower().Trim() == "sunward - group").Select(a => a.ApplicationMasterDetailId).ToList();
            var companyListingIds = _context.CompanyListingLine.Where(s => businessCategoryIds.Contains(s.BusinessCategoryId.Value)).Select(s => s.CompanyListingId).Distinct().ToList();

            var companyListing = _context.CompanyListing.
               Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(c => c.CompanyListingCustomerCode).Where(s => (companyListingIds.Contains(s.CompanyListingId)) && s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileID = s.ProfileId,
                    CustomerCodeId = s.CustomerCodeId,
                    CustomerCode = s.CompanyListingCustomerCode != null && applicationmasterdetail != null ? string.Join(",", s.CompanyListingCustomerCode.Where(b => b.CompanyListingId == s.CompanyListingId).Select(b => applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == b.CustomerCodeId).Select(m => m.Value).FirstOrDefault()).ToList()) : "",
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                };
                companyListingModel.Add(companyListingModels);
            });
            if (companyListingModel != null && companyListingModel.Count > 0)
            {
                companyListingModel.ForEach(c =>
                {
                    c.BlanketName = c.CompanyListingName + " | " + c.CustomerCode;

                });
            }
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

        [HttpGet]
        [Route("GetCompanyListingForSWDistributor")]
        public List<CompanyListingModel> GetCompanyListingForSWDistributor()
        {
            var businessCategoryIds = _context.ApplicationMasterDetail.Where(a => a.Value.ToLower().Trim() == "sw distributor").Select(a => a.ApplicationMasterDetailId).ToList();
            var companyListingIds = _context.CompanyListingLine.Where(s => businessCategoryIds.Contains(s.BusinessCategoryId.Value)).Select(s => s.CompanyListingId).Distinct().ToList();
            var companyListing = _context.CompanyListing.Where(s => (companyListingIds.Contains(s.CompanyListingId)) && s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingModel> companyListingModel = new List<CompanyListingModel>();
            companyListing.ForEach(s =>
            {
                CompanyListingModel companyListingModels = new CompanyListingModel
                {
                    CompanyListingID = s.CompanyListingId,
                    CompanyListingName = s.CompanyName,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ProfileID = s.ProfileId,
                    CustomerCodeId = s.CustomerCodeId,
                    IsNonTransaction = s.IsNonTransaction,
                    LinkNonTransactionCompanyId = s.LinkNonTransactionCompanyId,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    No = s.No,
                };
                companyListingModel.Add(companyListingModels);
            });
            return companyListingModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }

    }
}