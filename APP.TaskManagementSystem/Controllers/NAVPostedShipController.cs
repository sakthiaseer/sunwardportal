﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NAV;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NAVPostedShipController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<NAVPostedShipController> _logger;
        private readonly IConfiguration _config;
        public NAVPostedShipController(CRT_TMSContext context, IMapper mapper, ILogger<NAVPostedShipController> logger, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
            _config = config;
        }
        [HttpPost]
        [Route("GetShipment")]
        public async Task<List<NavpostedShipment>> GetShipment(PostedShipSearchParam searchModel)
        {
            var company = _context.Plant.FirstOrDefault(f => f.PlantId == searchModel.CompanyId.GetValueOrDefault(0));
            if (company == null)
                return null;

            searchModel.Company = company.NavCompanyName;
            var customers = _context.Navcustomer.Where(f => f.CompanyId == searchModel.CompanyId.GetValueOrDefault(0)).ToList();
            var context = new DataService(_config, searchModel.Company);
            int pageSize = 1000;
            int page = 0;
            while (true)
            {

                var nquery = context.Context.PostedSalesShipmentLines.Where(p => p.Posting_Date <= searchModel.ToPostingDate && p.Posting_Date >= searchModel.FromPostingDate && p.Type == "Item").Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<PostedSalesShipmentLines> query = (DataServiceQuery<PostedSalesShipmentLines>)nquery;
                TaskFactory<IEnumerable<PostedSalesShipmentLines>> taskFactory = new TaskFactory<IEnumerable<PostedSalesShipmentLines>>();
                IEnumerable<PostedSalesShipmentLines> salesResults = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                var salesList = salesResults.ToList();
                salesList.ToList().ForEach(s =>
                {
                    var cust = customers.FirstOrDefault(f => f.Code == s.Sell_to_Customer_No);
                    var existingDO = _context.NavpostedShipment.FirstOrDefault(d => d.DeliveryOrderNo == s.Document_No && d.ItemNo == s.No);
                    if (existingDO == null)
                    {
                        //if (s.No == "FP-PP-SUSP-009")
                        //{
                        //    var qty = s.Quantity;
                        //}

                        var doitem = new NavpostedShipment
                        {
                            CompanyId = searchModel.CompanyId,
                            Company = company.PlantCode,
                            CustomerId = cust?.CustomerId,
                            Customer = cust != null ? cust.Name : string.Empty,
                            CustomerNo = s.Sell_to_Customer_No,
                            Description = s.Description + "|" + s.Description_2,
                            DeliveryOrderNo = s.Document_No,
                            DolineNo = s.Line_No,
                            DoQty = s.Quantity ,
                            StockBalanceMonth = searchModel.StockMonth,
                            ItemNo = s.No,
                            PostingDate = s.Posting_Date,
                            AddedByUserId = searchModel.UserId,
                            StatusCodeId = 1,
                            IsRecived = true,
                            AddedDate = DateTime.Now,
                        };
                        _context.NavpostedShipment.Add(doitem);
                    }
                    else
                    {
                        //if (s.No == "FP-PP-SUSP-009")
                        //{
                        //    var qty = s.Quantity;
                        //}
                        existingDO.PostingDate = s.Posting_Date;
                        existingDO.CustomerId = cust?.CustomerId;
                        existingDO.Customer = cust != null ? cust.Name : string.Empty;
                        existingDO.CustomerNo = s.Sell_to_Customer_No;
                        existingDO.DoQty = s.Quantity;
                        // existingDO.IsRecived = true;                        
                    }
                });
                _context.SaveChanges();

                if (salesList.Count < 1000)
                    break;
                page++;
            }

            var userGroup = new List<NavpostedShipment>();
            var searchquery = _context.NavpostedShipment.Where(p => p.PostingDate <= searchModel.ToPostingDate && p.PostingDate >= searchModel.FromPostingDate).AsQueryable();
            if (!string.IsNullOrEmpty(searchModel.CustomerId))
            {
                searchquery = searchquery.Where(d => d.CustomerNo == searchModel.CustomerId);
            }
            if (!string.IsNullOrEmpty(searchModel.Search))
            {
                searchquery = searchquery.Where(d => d.DeliveryOrderNo.Contains(searchModel.Search));
            }
            return searchquery.ToList();
        }

        [HttpPut]
        [Route("UpdateShipment")]
        public NavpostedShipment Put(NavpostedShipment value)
        {
            var ship = _context.NavpostedShipment.SingleOrDefault(p => p.ShipmentId == value.ShipmentId);
            ship.IsRecived = value.IsRecived;
            ship.ModifiedByUserId = value.AddedByUserId;
            ship.AddedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
    }
}