﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BmrmovementController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public BmrmovementController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #region Bmrmovement
        // GET: api/Project
        [HttpGet]
        [Route("GetBmrmovement")]
        public List<BmrmovementModel> Get()
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var codeMaster = _context.CodeMaster.Select(s => new
            {
                s.CodeId,
                s.CodeValue,
            }).AsNoTracking().ToList();
            var department = _context.Department.Select(s => new
            {
                s.DepartmentId,
                s.Name,
                s.Description,
            }).AsNoTracking().ToList();
            var documents = _context.Bmrmovement.AsNoTracking().ToList();
            var applicationMasterIds = _context.ApplicationMaster.Where(f => f.ApplicationMasterCodeId == 318).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var bmrIds = documents.Select(b => b.BmrmovementId).ToList();
            var includedBmrIds = _context.BmrmovementLine.Where(l => bmrIds.Contains(l.BmrmovementId.Value)).Select(s => s.BmrmovementId).Distinct().ToList();
            List<BmrmovementModel> bmrmovementModel = new List<BmrmovementModel>();
            documents.Where(s => includedBmrIds.Contains(s.BmrmovementId)).ToList().ForEach(s =>
              {
                  BmrmovementModel bmrmovementModels = new BmrmovementModel
                  {
                      BmrmovementId = s.BmrmovementId,
                      ShipReceivedStatus = s.ShipReceivedStatus,
                      BmrstatusId = s.BmrstatusId,
                      ShipToId = s.ShipToId,
                      ModifiedByUserID = s.ModifiedByUserId,
                      AddedByUserID = s.AddedByUserId,
                      AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                      ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                      StatusCodeID = s.StatusCodeId,
                      ShippedBy = s.ShipReceivedStatus == true ? "Ship" : "Received",
                      StatusCode = s.StatusCodeId != null ? codeMaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : "",
                      Bmrstatus = s.BmrstatusId != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BmrstatusId)?.Value : "",
                      ShipTo = s.ShipToId != null ? department.FirstOrDefault(f => f.DepartmentId == s.ShipToId)?.Name : "",
                      AddedDate = s.AddedDate,
                      ModifiedDate = s.ModifiedDate,

                  };
                  bmrmovementModel.Add(bmrmovementModels);
              });
            return bmrmovementModel.OrderByDescending(a => a.BmrmovementId).ToList();
        }
        [HttpPost]
        [Route("InsertBmrmovement")]
        public BmrmovementModel Post(BmrmovementModel value)
        {
            Bmrmovement bmrmovement = null;
            if (value.BmrmovementId > 0)
            {
                bmrmovement = _context.Bmrmovement.FirstOrDefault(b => b.BmrmovementId == value.BmrmovementId);
                bmrmovement.ShipReceivedStatus = value.ShipReceivedStatus;
                bmrmovement.ShipToId = value.ShipToId;
                bmrmovement.BmrstatusId = value.BmrstatusId;
                bmrmovement.ModifiedByUserId = value.ModifiedByUserID;
                bmrmovement.ModifiedDate = DateTime.Now;

                var bmrMovementLines = _context.BmrmovementLine.Where(b => b.BmrmovementId == value.BmrmovementId).ToList();
                if (bmrMovementLines.Any())
                {
                    _context.BmrmovementLine.RemoveRange(bmrMovementLines);
                    _context.SaveChanges();
                }
                if (value.BmrmovementLines.Any())
                {
                    value.BmrmovementLines.ForEach(b =>
                    {
                        BmrmovementLine bmrmovementLine = new BmrmovementLine
                        {
                            BmrmovementId = value.BmrmovementId,
                            Bmrname = b.ItemNo,
                            Description1 = b.Description1,
                            Description2 = b.Description2,
                            BatchNo = b.BatchNo,
                            LocationCode = b.LocationCode,
                            Quantity = b.Qty,
                            ProductionOrderNo = b.ProductionOrderNo,
                            AddedByUserId = b.AddedByUserID,
                            AddedDate = b.AddedDate,
                            StatusCodeId = b.StatusCodeID,
                            LineNo = b.LineNo
                        };
                        _context.BmrmovementLine.Add(bmrmovementLine);
                    });
                }

            }
            else
            {
                bmrmovement = new Bmrmovement
                {
                    ShipReceivedStatus = value.ShipReceivedStatus,
                    ShipToId = value.ShipToId,
                    BmrstatusId = value.BmrstatusId,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value

                };
                _context.Bmrmovement.Add(bmrmovement);
            }
            _context.SaveChanges();
            value.BmrmovementId = bmrmovement.BmrmovementId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBmrmovement")]
        public BmrmovementModel Put(BmrmovementModel value)
        {
            var bmrmovement = _context.Bmrmovement.SingleOrDefault(p => p.BmrmovementId == value.BmrmovementId);

            bmrmovement.ShipReceivedStatus = value.ShipReceivedStatus;
            bmrmovement.ShipToId = value.ShipToId;
            bmrmovement.BmrstatusId = value.BmrstatusId;
            bmrmovement.ModifiedByUserId = value.ModifiedByUserID;
            bmrmovement.ModifiedDate = DateTime.Now;
            bmrmovement.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBmrmovement")]
        public void Delete(int id)
        {
            var bmrmovement = _context.Bmrmovement.SingleOrDefault(p => p.BmrmovementId == id);
            if (bmrmovement != null)
            {
                var bmrmovementLine = _context.BmrmovementLine.Where(p => p.BmrmovementId == id).ToList();
                if (bmrmovementLine != null)
                {
                    _context.BmrmovementLine.RemoveRange(bmrmovementLine);
                    _context.SaveChanges();
                }
                _context.Bmrmovement.Remove(bmrmovement);
                _context.SaveChanges();
            }
        }
        #endregion
        #region BmrmovementLine
        [HttpGet]
        [Route("GetBmrmovementLine")]
        public List<BmrmovementLineModel> GetBmrmovementLine(long? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var codeMaster = _context.CodeMaster.Select(s => new
            {
                s.CodeId,
                s.CodeValue,
            }).AsNoTracking().ToList();
            var bmrmovementLines = _context.BmrmovementLine.Where(w => w.BmrmovementId == id).AsNoTracking().ToList();
            var itemNos = bmrmovementLines.Select(n => n.Bmrname).ToList();
            var navItems = _context.Navitems.Where(n => itemNos.Contains(n.No)).ToList();
            List<BmrmovementLineModel> bmrmovementLineModel = new List<BmrmovementLineModel>();
            bmrmovementLines.ForEach(s =>
            {
                var item = navItems.FirstOrDefault(n => n.No == s.Bmrname);
                BmrmovementLineModel bmrmovementLineModels = new BmrmovementLineModel
                {
                    BmrmovementId = s.BmrmovementId,
                    BmrmovementLineId = s.BmrmovementLineId,
                    Bmrname = s.Bmrname,
                    LineNo = s.LineNo,
                    ItemNo = s.Bmrname,
                    FPDescription = item?.Description,
                    UOM = item?.BaseUnitofMeasure,
                    ProductionOrderNo = s.ProductionOrderNo,
                    LocationCode = s.LocationCode,
                    Qty = s.Quantity,
                    BatchNo = s.BatchNo,
                    Description1 = s.Description1,
                    Description2 = s.Description2,
                    FPItemNo = s.Bmrname,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCodeId != null ? codeMaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                bmrmovementLineModel.Add(bmrmovementLineModels);
            });
            return bmrmovementLineModel.OrderByDescending(a => a.BmrmovementLineId).ToList();
        }
        [Route("InsertBmrmovementLine")]
        public BmrmovementLineModel Post(BmrmovementLineModel value)
        {
            var bmrmovementLine = new BmrmovementLine
            {
                Bmrname = value.Bmrname,
                BmrmovementId = value.BmrmovementId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.BmrmovementLine.Add(bmrmovementLine);
            _context.SaveChanges();
            value.BmrmovementLineId = bmrmovementLine.BmrmovementLineId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBmrmovementLine")]
        public BmrmovementLineModel UpdateBmrmovementLine(BmrmovementLineModel value)
        {
            var bmrmovementLine = _context.BmrmovementLine.SingleOrDefault(p => p.BmrmovementLineId == value.BmrmovementLineId);
            bmrmovementLine.Bmrname = value.Bmrname;
            bmrmovementLine.BmrmovementId = value.BmrmovementId;
            bmrmovementLine.ModifiedByUserId = value.ModifiedByUserID;
            bmrmovementLine.ModifiedDate = DateTime.Now;
            bmrmovementLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBmrmovementLine")]
        public void DeleteBmrmovementLine(int id)
        {
            var bmrmovementLine = _context.BmrmovementLine.SingleOrDefault(p => p.BmrmovementLineId == id);
            if (bmrmovementLine != null)
            {

                _context.BmrmovementLine.Remove(bmrmovementLine);
                _context.SaveChanges();
            }
        }
        #endregion
    }
}
