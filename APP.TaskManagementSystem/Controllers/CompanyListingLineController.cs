﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyListingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CompanyListingLineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetCompanyListingsByID")]
        public List<CompanyListingLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var companyListingLine = _context.CompanyListingLine.
                Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CompanyListingLineModel> companyListingLineModel = new List<CompanyListingLineModel>();
            companyListingLine.ForEach(s =>
            {
                CompanyListingLineModel companyListingLineModels = new CompanyListingLineModel
                {
                    CompanyListingLineID = s.CompanyListingLineId,
                    CompanyListingID = s.CompanyListingId,
                    BusinessActivityID = s.BusinessActivityId,
                    BusinessCategoryID = s.BusinessCategoryId,
                    BusinessActivity = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessActivityId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessActivityId).Value : "",
                    BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId).Value : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                companyListingLineModel.Add(companyListingLineModels);
            });
            return companyListingLineModel.Where(c => c.CompanyListingID == id).OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetDistributorCompanyListing")]
        public List<CompanyListingLineModel> GetDistributorCompanyListing(string type)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var companyListingLine = _context.CompanyListingLine.Include(c=>c.CompanyListing).Where(s=>s.StatusCodeId==1).AsNoTracking().ToList();
            List<CompanyListingLineModel> companyListingLineModel = new List<CompanyListingLineModel>();
            companyListingLine.ForEach(s =>
            {
                CompanyListingLineModel companyListingLineModels = new CompanyListingLineModel
                {
                    CompanyListingLineID = s.CompanyListingLineId,
                    CompanyListingID = s.CompanyListingId,
                    BusinessActivityID = s.BusinessActivityId,
                    BusinessCategoryID = s.BusinessCategoryId,
                    BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId).Value : "",
                    CompanyListingName=s.CompanyListing!=null?s.CompanyListing.CompanyName:null,
                };
                companyListingLineModel.Add(companyListingLineModels);
            });
            return companyListingLineModel.Where(c => c.BusinessCategory.ToLower() == type).OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetDistributorCompanyListingByID")]
        public List<CompanyListingLineModel> GetDistributorCompanyListingByID(int? id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var companyListingLine = _context.CompanyListingLine.Include(c => c.CompanyListing).Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingLineModel> companyListingLineModel = new List<CompanyListingLineModel>();
            companyListingLine.ForEach(s =>
            {
                CompanyListingLineModel companyListingLineModels = new CompanyListingLineModel
                {
                    CompanyListingLineID = s.CompanyListingLineId,
                    CompanyListingID = s.CompanyListingId,
                    BusinessActivityID = s.BusinessActivityId,
                    BusinessCategoryID = s.BusinessCategoryId,
                    BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId).Value : "",
                    CompanyListingName = s.CompanyListing != null ? s.CompanyListing.CompanyName : null,
                };
                companyListingLineModel.Add(companyListingLineModels);
            });
            return companyListingLineModel.Where(c => c.BusinessCategoryID==id).OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetDistributorCompanyListingByName")]
        public List<CompanyListingLineModel> GetDistributorCompanyListingByName()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var companyListingLine = _context.CompanyListingLine.Include(c => c.CompanyListing).Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            List<CompanyListingLineModel> companyListingLineModel = new List<CompanyListingLineModel>();
            companyListingLine.ForEach(s =>
            {
                CompanyListingLineModel companyListingLineModels = new CompanyListingLineModel
                {
                    CompanyListingLineID = s.CompanyListingLineId,
                    CompanyListingID = s.CompanyListingId,
                    BusinessActivityID = s.BusinessActivityId,
                    BusinessCategoryID = s.BusinessCategoryId,
                    BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.BusinessCategoryId).Value : "",
                    CompanyListingName = s.CompanyListing != null ? s.CompanyListing.CompanyName : null,
                };
                companyListingLineModel.Add(companyListingLineModels);
            });
            return companyListingLineModel.Where(c => c.BusinessCategory.ToLower() == "buyer -  distributor" || c.BusinessCategory.ToLower()== "sw distributor").OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpGet]
        [Route("GetCompanyListingsByRefNo")]
        public List<CompanyListingLineModel> GetCompanyListingsByRefNo(string refNo)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companyListingLine = _context.CompanyListingLine.
                Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CompanyListingLineModel> companyListingLineModel = new List<CompanyListingLineModel>();
            companyListingLine.ForEach(s =>
            {
                CompanyListingLineModel companyListingLineModels = new CompanyListingLineModel
                {
                    CompanyListingID = s.CompanyListingId,

                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                companyListingLineModel.Add(companyListingLineModels);
            });
            return companyListingLineModel.OrderByDescending(a => a.CompanyListingID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyListingLineModel> GetData(SearchModel searchModel)
        {
            var companyListingLine = new CompanyListingLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                    case "Last":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Next":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Previous":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).FirstOrDefault();
                        break;
                    case "Last":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).LastOrDefault();
                        break;
                    case "Next":
                        companyListingLine = _context.CompanyListingLine.OrderBy(o => o.CompanyListingId).FirstOrDefault(s => s.CompanyListingId > searchModel.Id);
                        break;
                    case "Previous":
                        companyListingLine = _context.CompanyListingLine.OrderByDescending(o => o.CompanyListingId).FirstOrDefault(s => s.CompanyListingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyListingLineModel>(companyListingLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyListingLine")]
        public CompanyListingLineModel Post(CompanyListingLineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companyListingLine = new CompanyListingLine
            {
                CompanyListingId = value.CompanyListingID,
                BusinessActivityId = value.BusinessActivityID,
                BusinessCategoryId = value.BusinessCategoryID,

                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,

            };
            _context.CompanyListingLine.Add(companyListingLine);
            _context.SaveChanges();
            value.BusinessActivity = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessActivityID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessActivityID).Value : "";
            value.BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessCategoryID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessCategoryID).Value : "";
            value.CompanyListingLineID = companyListingLine.CompanyListingLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyListingLine")]
        public CompanyListingLineModel Put(CompanyListingLineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var companyListingLine = _context.CompanyListingLine.SingleOrDefault(p => p.CompanyListingLineId == value.CompanyListingLineID);
            companyListingLine.CompanyListingId = value.CompanyListingID;
            companyListingLine.BusinessActivityId = value.BusinessActivityID;
            companyListingLine.BusinessCategoryId = value.BusinessCategoryID;
            companyListingLine.ModifiedByUserId = value.ModifiedByUserID;
            companyListingLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.BusinessActivity = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessActivityID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessActivityID).Value : "";
            value.BusinessCategory = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessCategoryID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.BusinessCategoryID).Value : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyListingLine")]
        public void Delete(int id)
        {
            var companyListingLine = _context.CompanyListingLine.SingleOrDefault(p => p.CompanyListingLineId == id);
            if (companyListingLine != null)
            {
                _context.CompanyListingLine.Remove(companyListingLine);
                _context.SaveChanges();
            }
        }
    }
}