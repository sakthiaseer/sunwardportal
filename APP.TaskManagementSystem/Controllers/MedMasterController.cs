﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MedMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public MedMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        #region MedMaster
        [HttpGet]
        [Route("GetMedMaster")]
        public List<MedMasterModel> Get()
        {
            List<MedMasterModel> medMasterModels = new List<MedMasterModel>();
            var medMaster = _context.MedMaster
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(p => p.Company)
                                           .Include(a => a.MetCatalog)
                                           .Include(a => a.Units)
                                           .Include(m => m.ModifiedByUser)
                                           .OrderByDescending(o => o.MedMasterId).AsNoTracking()
                                           .ToList();

            if (medMaster != null && medMaster.Count > 0)
            {
                medMaster.ForEach(s =>
                {
                    MedMasterModel medMasterModel = new MedMasterModel
                    {
                        MedMasterId = s.MedMasterId,
                        CompanyId = s.CompanyId,
                        CompanyName = s.Company?.PlantCode,
                        MetCatalogName = s.MetCatalog?.CategoryNo,
                        MetCatalogId = s.MetCatalogId,
                        UnitsId = s.UnitsId,
                        Units = s.Units?.Value,
                        ModelNo = s.ModelNo,
                        IsCalibrationRequire = s.IsCalibrationRequire,
                        TotalNumber = s.TotalNumber,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Description = s.Description,
                        SessionId = s.SessionId,
                        Name = s.Name,
                        IsCalibrationRequireFlag = s.IsCalibrationRequire == true ? "Yes" : "No",
                    };
                    medMasterModels.Add(medMasterModel);
                });

            }
            return medMasterModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MedMasterModel> GetData(SearchModel searchModel)
        {
            var medMaster = new MedMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).LastOrDefault();
                        break;
                    case "Next":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).LastOrDefault();
                        break;
                    case "Next":
                        medMaster = _context.MedMaster.OrderBy(o => o.MedMasterId).FirstOrDefault(s => s.MedMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        medMaster = _context.MedMaster.OrderByDescending(o => o.MedMasterId).FirstOrDefault(s => s.MedMasterId < searchModel.Id);
                        break;
                }
            }


            var result = _mapper.Map<MedMasterModel>(medMaster);
            return result;
        }
        [HttpPost]
        [Route("InsertMedMaster")]
        public MedMasterModel Post(MedMasterModel value)
        {
            var sessionId = Guid.NewGuid();
            var medMaster = new MedMaster
            {
                CompanyId = value.CompanyId,
                MetCatalogId = value.MetCatalogId,
                UnitsId = value.UnitsId,
                ModelNo = value.ModelNo,
                IsCalibrationRequire = value.IsCalibrationRequireFlag == "Yes" ? true : false,
                TotalNumber = value.TotalNumber,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                Name = value.Name,
                Description = value.Description,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
            };
            _context.MedMaster.Add(medMaster);
            _context.SaveChanges();
            if (value.TotalNumber != null && value.TotalNumber > 0)
            {
                var plantDetails = _context.Plant.Where(w => w.PlantId == value.CompanyId).FirstOrDefault();
                var catalogNo = _context.MedCatalogClassification.Include(a => a.Category).Where(w => w.MedCatalogId == value.MetCatalogId).FirstOrDefault();
                for (var i = 1; i <= value.TotalNumber; i++)
                {
                    var sessionIds = Guid.NewGuid();
                    var nos = (i <= 9) ? ("000" + i) : ("00" + i);
                    var no = catalogNo?.CategoryNo + "/" + nos;
                    var navItems = new Navitems();
                    var exist = _context.Navitems.Where(n => n.No == no)?.FirstOrDefault();
                    if (exist == null)
                    {
                        navItems = new Navitems
                        {
                            No = no,
                            Description = no + "-" + catalogNo.Category?.Description,
                            ItemCategoryCode = catalogNo.Category?.Value,
                            CompanyId = value.CompanyId,
                            StatusCodeId = value.StatusCodeID.Value,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            InternalRef = plantDetails?.NavCompanyName,
                            Company = plantDetails?.NavCompanyName,
                        };
                        _context.Navitems.Add(navItems);
                        _context.SaveChanges();
                        if (navItems.ItemId > 0)
                        {
                            var medMasterLine = new MedMasterLine
                            {
                                MedMasterId = medMaster.MedMasterId,
                                SessionId = sessionIds,
                                StatusCodeId = value.StatusCodeID.Value,
                                AddedByUserId = value.AddedByUserID.Value,
                                AddedDate = DateTime.Now,
                                ItemId = navItems.ItemId,
                            };
                            _context.MedMasterLine.Add(medMasterLine);
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (exist.ItemId > 0)
                        {
                            var medMasterLine = new MedMasterLine
                            {
                                MedMasterId = medMaster.MedMasterId,
                                SessionId = sessionIds,
                                StatusCodeId = value.StatusCodeID.Value,
                                AddedByUserId = value.AddedByUserID.Value,
                                AddedDate = DateTime.Now,
                                ItemId = exist.ItemId,
                            };
                            _context.MedMasterLine.Add(medMasterLine);
                            _context.SaveChanges();
                        }
                    }
                    
                }
                _context.SaveChanges();
            }
            value.MedMasterId = medMaster.MedMasterId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateMedMaster")]
        public MedMasterModel Put(MedMasterModel value)
        {
            var medMaster = _context.MedMaster.SingleOrDefault(p => p.MedMasterId == value.MedMasterId);
            medMaster.CompanyId = value.CompanyId;
            medMaster.MetCatalogId = value.MetCatalogId;
            medMaster.UnitsId = value.UnitsId;
            medMaster.ModelNo = value.ModelNo;
            medMaster.IsCalibrationRequire = value.IsCalibrationRequireFlag == "Yes" ? true : false;
            medMaster.TotalNumber = value.TotalNumber;
            medMaster.Name = value.Name;
            medMaster.Description = value.Description;
            medMaster.StatusCodeId = value.StatusCodeID.Value;
            medMaster.ModifiedByUserId = value.ModifiedByUserID.Value;
            medMaster.ModifiedDate = DateTime.Now;
            
            if (value.TotalNumber != null && value.TotalNumber > 0)
            {
                var existingMEDMasterLines = _context.MedMasterLine.Where(m => m.MedMasterId == value.MedMasterId).ToList().Count;
                if (value.TotalNumber > existingMEDMasterLines)
                {
                    int? newTotalnumber = value.TotalNumber - existingMEDMasterLines;
                    var plantDetails = _context.Plant.Where(w => w.PlantId == value.CompanyId).FirstOrDefault();
                    var catalogNo = _context.MedCatalogClassification.Include(a => a.Category).Where(w => w.MedCatalogId == value.MetCatalogId).FirstOrDefault();
                    if (newTotalnumber > 0)
                    {
                        for (var i = existingMEDMasterLines+1; i <= value.TotalNumber; i++)
                        {
                            var nos = (i <= 9) ? ("000" + i) : ("00" + i);
                            var no = catalogNo?.CategoryNo + "/" + nos;
                            var navItems = new Navitems();
                            var exist = _context.Navitems.Where(n => n.No == no)?.FirstOrDefault();
                            if (exist == null)
                            {
                                navItems = new Navitems
                                {
                                    No = no,
                                    Description = no + "-" + catalogNo.Category?.Description,
                                    ItemCategoryCode = catalogNo.Category?.Value,
                                    CompanyId = value.CompanyId,
                                    StatusCodeId = value.StatusCodeID.Value,
                                    AddedByUserId = value.AddedByUserID.Value,
                                    AddedDate = DateTime.Now,
                                    InternalRef = plantDetails?.NavCompanyName,
                                    Company = plantDetails?.NavCompanyName,
                                };
                                _context.Navitems.Add(navItems);
                                _context.SaveChanges();
                            }
                          
                            var sessionIds = Guid.NewGuid();
                            if (navItems.ItemId > 0)
                            {
                                var medMasterLine = new MedMasterLine
                                {
                                    MedMasterId = medMaster.MedMasterId,
                                    SessionId = sessionIds,
                                    StatusCodeId = value.StatusCodeID.Value,
                                    AddedByUserId = value.AddedByUserID.Value,
                                    AddedDate = DateTime.Now,
                                    ItemId = navItems.ItemId,
                                };
                                _context.MedMasterLine.Add(medMasterLine);
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                _context.SaveChanges();
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteMedMaster")]
        public void Delete(int id)
        {
            try
            {


                var medMaster = _context.MedMaster.SingleOrDefault(p => p.MedMasterId == id);
                if (medMaster != null)
                {

                    _context.MedMaster.Remove(medMaster);
                    _context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("MED Master Refers to others Cannot Delete");
            }
        }
        #endregion
        #region MedMasterLine
        [HttpGet]
        [Route("GetMedMasterLine")]
        public List<MedMasterLineModel> GetMedMasterLine(long? id)
        {
            List<MedMasterLineModel> medMasterLineModels = new List<MedMasterLineModel>();
            var medMasterLine = _context.MedMasterLine
                                           .Include(a => a.AddedByUser)
                                           .Include(s => s.StatusCode)
                                           .Include(p => p.DedicatedUsage)
                                           .Include(a => a.InventoryType)
                                           .Include(a => a.Item)
                                           .Include(a => a.Location)
                                           .Include(m => m.ModifiedByUser)
                                           .Include(a => a.Area)
                                           .Include(b => b.SpecificArea)
                                           .Include(c => c.DedicatedUsageLocation)
                                           .Include(d => d.DedicatedUsagespecificArea)
                                           .Include(e => e.DedicatedUsageArea)
                                           .Include(f => f.ManufacturingProcess)
                                           .Include(g => g.MedMasterLineMachine)
                                           .Where(w => w.MedMasterId == id)
                                           .OrderByDescending(o => o.MedMasterLineId).AsNoTracking()
                                           .ToList();

            if (medMasterLine != null && medMasterLine.Count > 0)
            {
                medMasterLine.ForEach(s =>
                {
                    MedMasterLineModel medMasterLineModel = new MedMasterLineModel
                    {
                        MedMasterLineId = s.MedMasterLineId,
                        MedMasterId = s.MedMasterId,
                        ItemId = s.ItemId,
                        SerialNo = s.SerialNo,
                        InventoryTypeId = s.InventoryTypeId,
                        IsFixPartMedFlag = s.IsFixPartMed == true ? "Yes" : "No",
                        IsDedicatedUsageFlag = s.IsDedicatedUsage == true ? "Yes" : "No",
                        LocationId = s.LocationId,
                        DedicatedUsageId = s.DedicatedUsageId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Item = s.Item?.No,
                        InventoryType = s.InventoryType?.Name,
                        Location = s.Location?.Name,
                        DedicatedUsage = s.DedicatedUsage?.CodeValue,
                        AreaId = s.AreaId,
                        SpecificAreaId = s.SpecificAreaId,
                        DedicatedUsageAreaId = s.DedicatedUsageAreaId,
                        DedicatedUsageLocationId = s.DedicatedUsageLocationId,
                        DedicatedUsagespecificAreaId = s.DedicatedUsagespecificAreaId,
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        Area = s.Area?.Name,
                        SpecificArea = s.SpecificArea?.Name,
                        DedicatedUsageLocation = s.DedicatedUsageLocation?.Name,
                        DedicatedUsageArea = s.DedicatedUsageArea?.Name,
                        DedicatedUsagespecificArea = s.DedicatedUsagespecificArea?.Name,
                        ManufacturingProcess = s.ManufacturingProcess?.Value,
                        MachineIds = s.MedMasterLineMachine != null ? s.MedMasterLineMachine.Where(c => c.MedMasterLineId == s.MedMasterLineId).Select(c => c.MachineId).ToList() : new List<long?>(),
                        SessionId = s.SessionId,
                    };
                    medMasterLineModels.Add(medMasterLineModel);
                });

            }
            return medMasterLineModels;
        }
        [HttpPost]
        [Route("InsertMedMasterLine")]
        public MedMasterLineModel Post(MedMasterLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var medMasterLine = new MedMasterLine
            {
                MedMasterId = value.MedMasterId,
                ItemId = value.ItemId,
                SerialNo = value.SerialNo,
                InventoryTypeId = value.InventoryTypeId,
                IsFixPartMed = value.IsFixPartMedFlag == "Yes" ? true : false,
                IsDedicatedUsage = value.IsDedicatedUsageFlag == "Yes" ? true : false,
                LocationId = value.LocationId,
                DedicatedUsageId = value.DedicatedUsageId,
                AddedByUserId = value.AddedByUserID.Value,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                SessionId = sessionId,
                AreaId = value.AreaId,
                SpecificAreaId = value.SpecificAreaId,
                DedicatedUsageAreaId = value.DedicatedUsageAreaId,
                DedicatedUsageLocationId = value.DedicatedUsageLocationId,
                DedicatedUsagespecificAreaId = value.DedicatedUsagespecificAreaId,
                ManufacturingProcessId = value.ManufacturingProcessId,
            };
            _context.MedMasterLine.Add(medMasterLine);
            _context.SaveChanges();
            value.MedMasterLineId = medMasterLine.MedMasterLineId;
            value.SessionId = sessionId;
            if (value.MachineIds != null && value.MachineIds.Count > 0)
            {
                value.MachineIds.ForEach(f =>
                {
                    var machine = new MedMasterLineMachine
                    {
                        MedMasterLineId = value.MedMasterLineId,
                        MachineId = f,

                    };
                    _context.MedMasterLineMachine.Add(machine);
                });
                _context.SaveChanges();
            }

            return value;
        }
        [HttpPut]
        [Route("UpdateMedMasterLine")]
        public MedMasterLineModel UpdateMedMasterLine(MedMasterLineModel value)
        {
            var medMasterLine = _context.MedMasterLine.SingleOrDefault(p => p.MedMasterLineId == value.MedMasterLineId);
            medMasterLine.MedMasterId = value.MedMasterId;
            medMasterLine.ItemId = value.ItemId;
            medMasterLine.SerialNo = value.SerialNo;
            medMasterLine.InventoryTypeId = value.InventoryTypeId;
            medMasterLine.IsFixPartMed = value.IsFixPartMedFlag == "Yes" ? true : false;
            medMasterLine.IsDedicatedUsage = value.IsDedicatedUsageFlag == "Yes" ? true : false;
            medMasterLine.LocationId = value.LocationId;
            medMasterLine.DedicatedUsageId = value.DedicatedUsageId;
            medMasterLine.StatusCodeId = value.StatusCodeID.Value;
            medMasterLine.ModifiedByUserId = value.ModifiedByUserID.Value;
            medMasterLine.ModifiedDate = DateTime.Now;
            medMasterLine.AreaId = value.AreaId;
            medMasterLine.SpecificAreaId = value.SpecificAreaId;
            medMasterLine.DedicatedUsageAreaId = value.DedicatedUsageAreaId;
            medMasterLine.DedicatedUsageLocationId = value.DedicatedUsageLocationId;
            medMasterLine.DedicatedUsagespecificAreaId = value.DedicatedUsagespecificAreaId;
            medMasterLine.ManufacturingProcessId = value.ManufacturingProcessId;
            var removeMachine = _context.MedMasterLineMachine.Where(t => t.MedMasterLineId == value.MedMasterLineId).ToList();
            if (removeMachine != null)
            {
                _context.MedMasterLineMachine.RemoveRange(removeMachine);
                _context.SaveChanges();
            }
            if (value.MachineIds != null && value.MachineIds.Count > 0)
            {
                value.MachineIds.ForEach(f =>
                {
                    var machine = new MedMasterLineMachine
                    {
                        MedMasterLineId = value.MedMasterLineId,
                        MachineId = f,

                    };
                    _context.MedMasterLineMachine.Add(machine);
                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteMedMasterLine")]
        public void DeleteMedMasterLine(int id)
        {
            var medMasterLine = _context.MedMasterLine.SingleOrDefault(p => p.MedMasterLineId == id);
            if (medMasterLine != null)
            {
                var removeMachine = _context.MedMasterLineMachine.Where(t => t.MedMasterLineId == medMasterLine.MedMasterLineId).ToList();
                if (removeMachine != null)
                {
                    _context.MedMasterLineMachine.RemoveRange(removeMachine);
                    _context.SaveChanges();
                }
                _context.MedMasterLine.Remove(medMasterLine);
                _context.SaveChanges();
            }
        }
        #endregion
    }
}
