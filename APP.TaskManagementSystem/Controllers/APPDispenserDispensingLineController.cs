﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class APPDispenserDispensingLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        public APPDispenserDispensingLineController(CRT_TMSContext context, IHostingEnvironment host, IConfiguration config, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
            _config = config;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPDispenserDispensingLines")]
        public List<APPDispenserDispensingLineModel> Get()
        {
            var appDispenserDispensingLine = _context.AppdispenserDispensingLine.Include("ModifiedByUser").Select(s => new APPDispenserDispensingLineModel
            {

                DispenserDispensingLineId = s.DispenserDispensingLineId,
                DispenserDispensingId = s.DispenserDispensingId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                SubLotNo = s.SubLotNo,
                JobNo = s.JobNo,
                BagNo = s.BagNo,
                BeforeTareWeight = s.BeforeTareWeight,
                ImageBeforeTare = s.ImageBeforeTare,
                AfterTareWeight = s.AfterTareWeight,
                ImageAfterTare = s.ImageAfterTare,
                ItemNo = s.ItemNo,
                LotNo = s.LotNo,
                QCRefNo = s.QcrefNo,
                Weight = s.Weight,
                WeighingPhoto = s.WeighingPhoto,
                PrintLabel = s.PrintLabel,
                PostedtoNAV = s.PostedtoNav,



            }).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPDispenserDispensingLineModel>>(APPDispenserDispensingLine);
            return appDispenserDispensingLine;
        }

        [HttpGet]
        [Route("GetAPPDispenserDispensingLineItemByID")]
        public APPDispenserDispensingLineModel GetAPPDispenserDispensingLineItemByID(long Id)
        {
            var appDispenserDispensingLine = _context.AppdispenserDispensingLine.Select(s => new APPDispenserDispensingLineModel
            {

                DispenserDispensingLineId = s.DispenserDispensingLineId,
                DispenserDispensingId = s.DispenserDispensingId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                SubLotNo = s.SubLotNo,
                JobNo = s.JobNo,
                BagNo = s.BagNo,
                BeforeTareWeight = s.BeforeTareWeight,
                ImageBeforeTare = s.ImageBeforeTare,
                AfterTareWeight = s.AfterTareWeight,
                ImageAfterTare = s.ImageAfterTare,
                ItemNo = s.ItemNo,
                LotNo = s.LotNo,
                QCRefNo = s.QcrefNo,
                Weight = s.Weight,
                WeighingPhoto = s.WeighingPhoto,
                PrintLabel = s.PrintLabel,
                PostedtoNAV = s.PostedtoNav,
            }).OrderByDescending(o => o.DispenserDispensingLineId).FirstOrDefault(t => t.DispenserDispensingLineId == Id);


            var drumDetails = _context.AppdispenserDispensingDrumDetails
                                .Where(d => d.DispenserDispensingLineId == appDispenserDispensingLine.DispenserDispensingLineId)
                                .AsNoTracking().ToList();

            if (drumDetails != null)
            {
                drumDetails.ForEach(dd =>
                {
                    appDispenserDispensingLine.DispenserDispensingDrumDetailModels.Add(new AppDispenserDispensingDrumDetailModel
                    {
                        DispenserDispensingDrumDetailsId = dd.DispenserDispensingDrumDetailsId,
                        DispenserDispensingLineId = dd.DispenserDispensingLineId,
                        MaterialNo = dd.MaterialNo,
                        LotNo = dd.LotNo,
                        QcrefNo = dd.QcrefNo,
                        Weight = dd.Weight,
                        WeighingPhoto = dd.WeighingPhoto
                    });
                });
            }


            return appDispenserDispensingLine;
        }

        [HttpGet]
        [Route("GetAPPDispenserDispensingLineByID")]
        public List<APPDispenserDispensingLineModel> GetDispensingByWorkOrder(long Id)
        {
            var appDispenserDispensingLine = _context.AppdispenserDispensingLine.Select(s => new APPDispenserDispensingLineModel
            {

                DispenserDispensingLineId = s.DispenserDispensingLineId,
                DispenserDispensingId = s.DispenserDispensingId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                SubLotNo = s.SubLotNo,
                JobNo = s.JobNo,
                BagNo = s.BagNo,
                BeforeTareWeight = s.BeforeTareWeight,
                ImageBeforeTare = s.ImageBeforeTare,
                AfterTareWeight = s.AfterTareWeight,
                ImageAfterTare = s.ImageAfterTare,
                ItemNo = s.ItemNo,
                LotNo = s.LotNo,
                QCRefNo = s.QcrefNo,
                Weight = s.Weight,
                WeighingPhoto = s.WeighingPhoto,
                PrintLabel = s.PrintLabel,
                PostedtoNAV = s.PostedtoNav,
            }).OrderByDescending(o => o.DispenserDispensingId).Where(t => t.DispenserDispensingId == Id).AsNoTracking().ToList();

            return appDispenserDispensingLine;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPDispenserDispensingLineModel> GetData(SearchModel searchModel)
        {
            var appDispenserDispensingLine = new AppdispenserDispensingLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).LastOrDefault();
                        break;
                    case "Next":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).LastOrDefault();
                        break;
                    case "Previous":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).FirstOrDefault();
                        break;
                    case "Last":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).LastOrDefault();
                        break;
                    case "Next":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderBy(o => o.DispenserDispensingLineId).FirstOrDefault(s => s.DispenserDispensingLineId > searchModel.Id);
                        break;
                    case "Previous":
                        appDispenserDispensingLine = _context.AppdispenserDispensingLine.OrderByDescending(o => o.DispenserDispensingLineId).FirstOrDefault(s => s.DispenserDispensingLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPDispenserDispensingLineModel>(appDispenserDispensingLine);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertAPPDispenserDispensingLine")]
        public APPDispenserDispensingLineModel Post(APPDispenserDispensingLineModel value)
        {
            if (value.DispenserDispensingLineId > 0)
            {
                return Update(value);
            }
            List<AppdispenserDispensingDrumDetails> appdispenserDispensingDrumDetails = new List<AppdispenserDispensingDrumDetails>();

            if (value.DispenserDispensingDrumDetailModels.Any())
            {
                foreach (var drumDetail in value.DispenserDispensingDrumDetailModels)
                {
                    var appDispenserDispensingDrumDetail = new AppdispenserDispensingDrumDetails
                    {
                        MaterialNo = drumDetail.MaterialNo,
                        LotNo = drumDetail.LotNo,
                        QcrefNo = drumDetail.QcrefNo,
                        Weight = drumDetail.Weight,
                        WeighingPhoto = drumDetail.WeighingPhoto
                    };

                    if (drumDetail.WeighingPhotoStream != null && drumDetail.WeighingPhotoStream.Length > 0)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + drumDetail.WeighingPhoto;
                        System.IO.File.WriteAllBytes(serverPath, drumDetail.WeighingPhotoStream);
                    }

                    appdispenserDispensingDrumDetails.Add(appDispenserDispensingDrumDetail);

                }

            }

            var appDispenserDispensingLine = new AppdispenserDispensingLine
            {
                DispenserDispensingId = value.DispenserDispensingId,
                ProdOrderNo = value.ProdOrderNo,
                ProdLineNo = value.ProdLineNo,
                SubLotNo = value.SubLotNo,
                JobNo = value.JobNo,
                BagNo = value.BagNo,
                BeforeTareWeight = value.BeforeTareWeight,
                ImageBeforeTare = value.ImageBeforeTare,
                AfterTareWeight = value.AfterTareWeight,
                ImageAfterTare = value.ImageAfterTare,
                ItemNo = value.ItemNo,
                LotNo = value.LotNo,
                QcrefNo = value.QCRefNo,
                Weight = value.Weight,
                WeighingPhoto = value.WeighingPhoto,
                PrintLabel = value.PrintLabel,
                PostedtoNav = value.PostedtoNAV,
            };

            if (value.AfterTarePhoto != null && value.AfterTarePhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.ImageAfterTare;
                System.IO.File.WriteAllBytes(serverPath, value.AfterTarePhoto);
            }

            if (value.BeforeTarePhoto != null && value.BeforeTarePhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.ImageBeforeTare;
                System.IO.File.WriteAllBytes(serverPath, value.BeforeTarePhoto);
            }

            if (value.WeighingPhotoStream != null && value.WeighingPhotoStream.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.WeighingPhoto;
                System.IO.File.WriteAllBytes(serverPath, value.WeighingPhotoStream);
            }

            appdispenserDispensingDrumDetails.ForEach(d =>
            {
                appDispenserDispensingLine.AppdispenserDispensingDrumDetails.Add(d);

            });

            _context.AppdispenserDispensingLine.Add(appDispenserDispensingLine);

            _context.SaveChanges();



            value.DispenserDispensingLineId = appDispenserDispensingLine.DispenserDispensingLineId;
            return value;
        }


        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAPPDispenserDispensingLine")]
        public APPDispenserDispensingLineModel Put(APPDispenserDispensingLineModel value)
        {
            return Update(value);
        }


        private APPDispenserDispensingLineModel Update(APPDispenserDispensingLineModel value)
        {
            var appDispenserDrumDetails = _context.AppdispenserDispensingDrumDetails.Where(p => p.DispenserDispensingLineId == value.DispenserDispensingLineId);
            if (appDispenserDrumDetails.Any())
            {
                _context.AppdispenserDispensingDrumDetails.RemoveRange(appDispenserDrumDetails);
            }
            var appDispenserDispensingLine = _context.AppdispenserDispensingLine.SingleOrDefault(p => p.DispenserDispensingId == value.DispenserDispensingId);
            appDispenserDispensingLine.DispenserDispensingId = value.DispenserDispensingId;
            appDispenserDispensingLine.ProdOrderNo = value.ProdOrderNo;
            appDispenserDispensingLine.ProdLineNo = value.ProdLineNo;
            appDispenserDispensingLine.SubLotNo = value.SubLotNo;
            appDispenserDispensingLine.JobNo = value.JobNo;
            appDispenserDispensingLine.BagNo = value.BagNo;
            appDispenserDispensingLine.BeforeTareWeight = value.BeforeTareWeight;
            appDispenserDispensingLine.ImageBeforeTare = value.ImageBeforeTare;
            appDispenserDispensingLine.AfterTareWeight = value.AfterTareWeight;
            appDispenserDispensingLine.ImageAfterTare = value.ImageAfterTare;
            appDispenserDispensingLine.ItemNo = value.ItemNo;
            appDispenserDispensingLine.LotNo = value.LotNo;
            appDispenserDispensingLine.QcrefNo = value.QCRefNo;
            appDispenserDispensingLine.Weight = value.Weight;
            appDispenserDispensingLine.WeighingPhoto = value.WeighingPhoto;
            appDispenserDispensingLine.PrintLabel = value.PrintLabel;
            appDispenserDispensingLine.PostedtoNav = value.PostedtoNAV;

            List<AppdispenserDispensingDrumDetails> appdispenserDispensingDrumDetails = new List<AppdispenserDispensingDrumDetails>();

            if (value.DispenserDispensingDrumDetailModels.Any())
            {
                foreach (var drumDetail in value.DispenserDispensingDrumDetailModels)
                {
                    var appDispenserDispensingDrumDetail = new AppdispenserDispensingDrumDetails
                    {
                        MaterialNo = drumDetail.MaterialNo,
                        LotNo = drumDetail.LotNo,
                        QcrefNo = drumDetail.QcrefNo,
                        Weight = drumDetail.Weight,
                        WeighingPhoto = drumDetail.WeighingPhoto
                    };

                    if (drumDetail.WeighingPhotoStream != null && drumDetail.WeighingPhotoStream.Length > 0)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + drumDetail.WeighingPhoto;
                        System.IO.File.WriteAllBytes(serverPath, drumDetail.WeighingPhotoStream);
                    }

                    appdispenserDispensingDrumDetails.Add(appDispenserDispensingDrumDetail);

                }

            }

            if (appdispenserDispensingDrumDetails.Any())
            {
                appdispenserDispensingDrumDetails.ForEach(a =>
                {
                    appDispenserDispensingLine.AppdispenserDispensingDrumDetails.Add(a);
                });
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDispensingEntryLine")]
        public void Delete(int id)
        {
            var appDispenserDrumDetails = _context.AppdispenserDispensingDrumDetails.Where(p => p.DispenserDispensingLineId == id);
            var appDispenserDispensingLine = _context.AppdispenserDispensingLine.SingleOrDefault(p => p.DispenserDispensingLineId == id);
            var errorMessage = "";
            try
            {
                if (appDispenserDispensingLine != null)
                {
                    _context.AppdispenserDispensingDrumDetails.RemoveRange(appDispenserDrumDetails);
                    _context.AppdispenserDispensingLine.Remove(appDispenserDispensingLine);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}