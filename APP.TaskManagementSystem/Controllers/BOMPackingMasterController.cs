using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BOMPackingMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public BOMPackingMasterController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;



        }



        [HttpGet]
        [Route("GetBompackingMaster")]
        public List<BompackingMasterModel> GetBompackingMaster()
        {
            List<BompackingMasterModel> bompackingMasterModels = new List<BompackingMasterModel>();
            var bompackingLanguageMultiple = _context.BompackingLanguageMultiple.ToList();

            var bompackingMasterlist = _context.BompackingMaster.Include("DosageForm").Include("AddedByUser")
                                        .Include("ModifiedByUser").OrderByDescending(o => o.BompackingId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel

            if (bompackingMasterlist != null && bompackingMasterlist.Count > 0)
            {
                bompackingMasterlist.ForEach(s =>
                {
                    BompackingMasterModel bompackingMasterModel = new BompackingMasterModel
                    {
                        BompackingId = s.BompackingId,
                        ProfileNo = s.ProfileNo,
                        ProfileId = s.ProfileId,
                        DosageFormId = s.DosageFormId,
                        DosageFormName = s.DosageForm != null ? s.DosageForm.CodeValue : "",
                        PackQty = s.PackQty,
                        PerPackId = s.PerPackId,
                        PackQtyunitId = s.PackQtyunitId,
                        SalesPerPackId = s.SalesPerPackId,
                        BottleTypeId = s.BottleTypeId,
                        BompackingName = s.BompackingName,
                        Description = s.Description,
                        VesionNo = s.VesionNo,
                        BlisterStripSizeLength = s.BlisterStripSizeLength,
                        BlisterStripSizeWidth = s.BlisterStripSizeWidth,
                        BsalesPackQty = s.BsalesPackQty,
                        BsalesPackUnitId = s.BsalesPackUnitId,
                        CcapsuleSize = s.CcapsuleSize,
                        TbtabletSizeLength = s.TbtabletSizeLength,
                        TbtableSizeWidth = s.TbtableSizeWidth,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        CountryId = bompackingLanguageMultiple != null ? bompackingLanguageMultiple.Where(a => a.BompackingId == s.BompackingId).Select(l => l.LanguageId).ToList() : new List<long?>(),

                    };
                    bompackingMasterModels.Add(bompackingMasterModel);
                });

            }


            return bompackingMasterModels;
        }

        [HttpGet]
        [Route("GetBompackingMasterLineID")]
        public List<BompackingMasterLineModel> GetBompackingMasterLineID(int id)
        {
            List<BompackingMasterLineModel> bompackingMasterLineModels = new List<BompackingMasterLineModel>();

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();

            var bompackingMasterLinelist = _context.BompackingMasterLine.Include("StatusCode")
                                            .Include("CalculationMethod").Include("AddedByUser").Include("ModifiedByUser")
                                            .OrderByDescending(o => o.BompackingMasterLineId).Where(t => t.BompackingMasterId == id)
                                            .AsNoTracking().ToList();//.Select(s => new BompackingMasterLineModel
            if (bompackingMasterLinelist != null && bompackingMasterLinelist.Count > 0)
            {
                bompackingMasterLinelist.ForEach(s =>
                {
                    BompackingMasterLineModel bompackingMasterLineModel = new BompackingMasterLineModel
                    {
                        BompackingMasterLineId = s.BompackingMasterLineId,
                        BompackingMasterId = s.BompackingMasterId,
                        SetTypeId = s.SetTypeId,
                        SetTypeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SetTypeId).Select(a => a.Value).SingleOrDefault() : "",
                        No = s.No,
                        ItemName = s.ItemName,
                        CalculationMethodId = s.CalculationMethodId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                        CalulationName = s.CalculationMethod != null ? s.CalculationMethod.CodeValue : "",
                        Qty = s.Qty,
                        Uomid = s.Uomid,
                        Bominformation = s.Bominformation,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,

                    };
                    bompackingMasterLineModels.Add(bompackingMasterLineModel);
                });

            }

         
           




            return bompackingMasterLineModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BompackingMasterModel> GetData(SearchModel searchModel)
        {


            var bompackingMaster = new BompackingMaster();
            var bompackingMasterModel = new BompackingMasterModel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).FirstOrDefault();
                        break;
                    case "Last":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).LastOrDefault();
                        break;
                    case "Next":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).LastOrDefault();
                        break;
                    case "Previous":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).FirstOrDefault();
                        break;
                    case "Last":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).LastOrDefault();
                        break;
                    case "Next":
                        bompackingMaster = _context.BompackingMaster.OrderBy(o => o.BompackingId).FirstOrDefault(s => s.BompackingId > searchModel.Id);
                        break;
                    case "Previous":
                        bompackingMaster = _context.BompackingMaster.OrderByDescending(o => o.BompackingId).FirstOrDefault(s => s.BompackingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BompackingMasterModel>(bompackingMaster);
            if (result != null)
            {

                var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
                var bompackingMasterLinelist = _context.BompackingMasterLine.Include("AddedByUser").Include("CalculationMethod").Include("ModifiedByUser").OrderByDescending(o => o.BompackingMasterLineId).Where(t => t.BompackingMasterId == result.BompackingId).ToList();//.Select(s => new BompackingMasterLineModel


                if (bompackingMasterLinelist != null && bompackingMasterLinelist.Count > 0)
                {
                    bompackingMasterLinelist.ForEach(s =>
                    {
                        BompackingMasterLineModel bompackingMasterLineModel = new BompackingMasterLineModel
                        {
                            BompackingMasterLineId = s.BompackingMasterLineId,
                            BompackingMasterId = s.BompackingMasterId,
                            SetTypeId = s.SetTypeId,
                            SetTypeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SetTypeId).Select(a => a.Value).SingleOrDefault() : "",
                            No = s.No,
                            ItemName = s.ItemName,
                            CalculationMethodId = s.CalculationMethodId,
                            CalulationName = s.CalculationMethod != null ? s.CalculationMethod.CodeValue : "",
                            Qty = s.Qty,
                            Uomid = s.Uomid,
                            Bominformation = s.Bominformation,

                            StatusCodeID = s.StatusCodeId,
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,

                        };
                        bompackingMasterModel.BompackingMasterLine.Add(bompackingMasterLineModel);

                    });

                }
            }

         

         
            if (bompackingMasterModel.BompackingMasterLine.Count > 0)
            {
                result.BompackingMasterLine = bompackingMasterModel.BompackingMasterLine;
            }


            return result;
        }




        // POST: api/User
        [HttpPost]
        [Route("InsertBompackingMaster")]
        public BompackingMasterModel Post(BompackingMasterModel value)
        {
            var profileNo = "";
            if (value.ProfileId > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.BompackingName });
            }
            var query = new BompackingMaster
            {
                // BompackingId=value.BompackingId,
                // ProfileID=value.ProfileID,
                ProfileId = value.ProfileId,
                ProfileNo = profileNo,
                DosageFormId = value.DosageFormId,
                PackQty = value.PackQty,
                PackQtyunitId = value.PackQtyunitId,
                PerPackId = value.PerPackId,
                SalesPerPackId = value.SalesPerPackId,
                BottleTypeId = value.BottleTypeId,
                BompackingName = value.BompackingName,
                Description = value.Description,
                VesionNo = value.VesionNo,
                BlisterStripSizeLength = value.BlisterStripSizeLength,
                BlisterStripSizeWidth = value.BlisterStripSizeWidth,
                BsalesPackQty = value.BsalesPackQty,
                BsalesPackUnitId = value.BsalesPackUnitId,
                CcapsuleSize = value.CcapsuleSize,
                TbtabletSizeLength = value.TbtabletSizeLength,
                TbtableSizeWidth = value.TbtableSizeWidth,

                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };

            value.CountryId.ForEach(l =>
            {
                BompackingLanguageMultiple bompackingLanguageMultiple = new BompackingLanguageMultiple
                {
                    LanguageId = l,

                };
                query.BompackingLanguageMultiple.Add(bompackingLanguageMultiple);

            });

            _context.BompackingMaster.Add(query);
            _context.SaveChanges();


            value.BompackingId = query.BompackingId;
            value.ProfileNo = query.ProfileNo;
            return value;


        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBompackingMaster")]
        public BompackingMasterModel Put(BompackingMasterModel value)
        {
            var query = _context.BompackingMaster.SingleOrDefault(p => p.BompackingId == value.BompackingId);

            query.ProfileNo = value.ProfileNo;
            query.DosageFormId = value.DosageFormId;
            query.PackQty = value.PackQty;
            query.PackQtyunitId = value.PackQtyunitId;
            query.PerPackId = value.PerPackId;
            query.SalesPerPackId = value.SalesPerPackId;
            query.BottleTypeId = value.BottleTypeId;
            query.BompackingName = value.BompackingName;
            query.Description = value.Description;
            query.VesionNo = value.VesionNo;
            query.BlisterStripSizeLength = value.BlisterStripSizeLength;
            query.BlisterStripSizeWidth = value.BlisterStripSizeWidth;
            query.BsalesPackQty = value.BsalesPackQty;
            query.BsalesPackUnitId = value.BsalesPackUnitId;
            query.CcapsuleSize = value.CcapsuleSize;
            query.TbtabletSizeLength = value.TbtabletSizeLength;
            query.TbtableSizeWidth = value.TbtableSizeWidth;
            query.ModifiedByUserId = value.ModifiedByUserID;
            query.ModifiedDate = DateTime.Now;
            query.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();


            var bompackingLanguageList = _context.BompackingLanguageMultiple.Where(s => s.BompackingId == value.BompackingId).ToList();
            if (bompackingLanguageList != null)
            {
                _context.BompackingLanguageMultiple.RemoveRange(bompackingLanguageList);
                _context.SaveChanges();
            }
            value.CountryId.ForEach(l =>
          {
              BompackingLanguageMultiple bompackingLanguageMultiple = new BompackingLanguageMultiple
              {
                  LanguageId = l,

              };
              query.BompackingLanguageMultiple.Add(bompackingLanguageMultiple);
              _context.SaveChanges();

          });




            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBompackingMaster")]
        public void DeleteBompackingMaster(int id)
        {

            var bompackingMaster = _context.BompackingMaster.SingleOrDefault(p => p.BompackingId == id);
            if (bompackingMaster != null)
            {
                var bompackingMasterLine = _context.BompackingMasterLine.Where(prc => prc.BompackingMasterId == bompackingMaster.BompackingId).ToList();
                if (bompackingMasterLine != null)
                {

                    _context.BompackingMasterLine.RemoveRange(bompackingMasterLine);
                    _context.SaveChanges();
                }

                var bompackingLanguageList = _context.BompackingLanguageMultiple.Where(s => s.BompackingId == bompackingMaster.BompackingId).ToList();
                if (bompackingLanguageList != null)
                {
                    _context.BompackingLanguageMultiple.RemoveRange(bompackingLanguageList);
                    _context.SaveChanges();
                }



                _context.BompackingMaster.Remove(bompackingMaster);
                _context.SaveChanges();
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBompackingMasterLine")]
        public void DeleteBompackingMasterLine(int id)
        {

            var query = _context.BompackingMasterLine.SingleOrDefault(p => p.BompackingMasterLineId == id);
            if (query != null)
            {
                _context.BompackingMasterLine.Remove(query);
                _context.SaveChanges();

            }
        }






        // POST: api/User
        [HttpPost]
        [Route("InsertBompackingMasterLine")]
        public BompackingMasterLineModel PostBompackingMasterLine(BompackingMasterLineModel value)
        {

            var query = new BompackingMasterLine
            {
                //ExchangeRateLineId
                BompackingMasterId = value.BompackingMasterId,
                SetTypeId = value.SetTypeId,
                No = value.No,
                ItemName = value.ItemName,
                CalculationMethodId = value.CalculationMethodId,

                Qty = value.Qty,
                Uomid = value.Uomid,
                Bominformation = value.Bominformation,

                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,

            };

            _context.BompackingMasterLine.Add(query);
            _context.SaveChanges();
            value.BompackingMasterLineId = query.BompackingMasterLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBompackingMasterLine")]
        public BompackingMasterLineModel PutBompackingMasterLine(BompackingMasterLineModel value)
        {
            var bompackingMasterLine = _context.BompackingMasterLine.SingleOrDefault(p => p.BompackingMasterLineId == value.BompackingMasterLineId);
            bompackingMasterLine.BompackingMasterId = value.BompackingMasterId;
            bompackingMasterLine.SetTypeId = value.SetTypeId;

            bompackingMasterLine.No = value.No;
            bompackingMasterLine.ItemName = value.ItemName;
            bompackingMasterLine.CalculationMethodId = value.CalculationMethodId;
            bompackingMasterLine.Qty = value.Qty;
            bompackingMasterLine.Uomid = value.Uomid;
            bompackingMasterLine.Bominformation = value.Bominformation;

            bompackingMasterLine.ModifiedByUserId = value.ModifiedByUserID;
            bompackingMasterLine.ModifiedDate = DateTime.Now;
            bompackingMasterLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }












    }
}