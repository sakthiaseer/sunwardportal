﻿using System;
using System.Collections.Generic;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionMachineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionMachineController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionMachines")]
        public List<ProductionMachineModel> Get()
        {
            List<ProductionMachineModel> productionMachineModels = new List<ProductionMachineModel>();
            var productionMachine = _context.ProductionMachine.Include("AddedByUser").Include("ModifiedByUser").Include(s=>s.StatusCode).OrderByDescending(o => o.ProductionMachineId).AsNoTracking().ToList();
            if(productionMachine!=null && productionMachine.Count>0)
            {
                List<long?> masterIds = productionMachine.Where(w => w.ManufacturerId != null).Select(a => a.ManufacturerId).Distinct().ToList();
                masterIds.AddRange(productionMachine.Where(w => w.ItemNoId != null).Select(a => a.ItemNoId).Distinct().ToList());
                masterIds.AddRange(productionMachine.Where(w => w.PurposeOfProcessId != null).Select(a => a.PurposeOfProcessId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionMachine.ForEach(s =>
                {
                    ProductionMachineModel productionMachineModel = new ProductionMachineModel
                    {
                        ProductionMachineID = s.ProductionMachineId,
                        ItemNoID = s.ItemNoId,
                        ItemNo = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNoId).Select(a => a.Value).SingleOrDefault() : "",
                        ManufacturerID = s.ManufacturerId,
                        Manufacturer = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturerId).Select(a => a.Value).SingleOrDefault() : "",
                        PurposeOfProcessID = s.PurposeOfProcessId,
                        PurposeOfProcess = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeOfProcessId).Select(a => a.Value).SingleOrDefault() : "",
                        BasicModelNo = s.BasicModelNo,
                        OperationProcedureIDs = s.OperationProcedureMultiple.Where(t => t.ProductionMachineId == s.ProductionMachineId).Select(t => t.OperationProcedureId).ToList(),
                        SerialNo = s.SerialNo,
                        NameOfTheMachine = s.NameOfTheMachine,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionMachineModels.Add(productionMachineModel);
                });
            }           
            return productionMachineModels;
        }


        [HttpPost]
        [Route("GetProductionMachinesByRefNo")]
        public List<ProductionMachineModel> GetProductionMachinesByRefNo(RefSearchModel refSearchModel)
        {
            List<ProductionMachineModel> productionMachineModels = new List<ProductionMachineModel>();
            List<ProductionMachine> productionMachine = new List<ProductionMachine>();
            if (refSearchModel.IsHeader)
            {
                productionMachine = _context.ProductionMachine.Include("AddedByUser").Include("ModifiedByUser").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionMachineId).AsNoTracking().ToList();
            }
            else
            {
                productionMachine = _context.ProductionMachine.Include("AddedByUser").Include("ModifiedByUser").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionMachineId).AsNoTracking().ToList();
            }

            if(productionMachine!=null && productionMachine.Count > 0)
            {
                List<long?> masterIds = productionMachine.Where(w => w.ManufacturerId != null).Select(a => a.ManufacturerId).Distinct().ToList();
                masterIds.AddRange(productionMachine.Where(w => w.ItemNoId != null).Select(a => a.ItemNoId).Distinct().ToList());
                masterIds.AddRange(productionMachine.Where(w => w.PurposeOfProcessId != null).Select(a => a.PurposeOfProcessId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionMachine.ForEach(s =>
                {
                    ProductionMachineModel productionMachineModel = new ProductionMachineModel
                    {
                        ProductionMachineID = s.ProductionMachineId,
                        ItemNoID = s.ItemNoId,
                        ItemNo = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNoId).Select(a => a.Value).SingleOrDefault() : "",
                        ManufacturerID = s.ManufacturerId,
                        Manufacturer = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ManufacturerId).Select(a => a.Value).SingleOrDefault() : "",
                        PurposeOfProcessID = s.PurposeOfProcessId,
                        PurposeOfProcess = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeOfProcessId).Select(a => a.Value).SingleOrDefault() : "",
                        BasicModelNo = s.BasicModelNo,
                        OperationProcedureIDs = s.OperationProcedureMultiple.Where(t => t.ProductionMachineId == s.ProductionMachineId).Select(t => t.OperationProcedureId).ToList(),
                        SerialNo = s.SerialNo,
                        NameOfTheMachine = s.NameOfTheMachine,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    productionMachineModels.Add(productionMachineModel);
                });
            }
                           
            return productionMachineModels;
        }




        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionMachineModel> GetData(SearchModel searchModel)
        {
            var productionMachine = new ProductionMachine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).LastOrDefault();
                        break;
                    case "Next":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).LastOrDefault();
                        break;
                    case "Previous":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).LastOrDefault();
                        break;
                    case "Next":
                        productionMachine = _context.ProductionMachine.OrderBy(o => o.ProductionMachineId).FirstOrDefault(s => s.ProductionMachineId > searchModel.Id);
                        break;
                    case "Previous":
                        productionMachine = _context.ProductionMachine.OrderByDescending(o => o.ProductionMachineId).FirstOrDefault(s => s.ProductionMachineId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<ProductionMachineModel>(productionMachine);
            if (result != null)
            {
                if (result.ProductionMachineID > 0)
                {
                    result.OperationProcedureIDs = _context.OperationProcedureMultiple.Where(t => t.ProductionMachineId == result.ProductionMachineID).AsNoTracking().Select(t => t.OperationProcedureId).ToList();
                }
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionMachine")]
        public ProductionMachineModel Post(ProductionMachineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710 , Title = value.NameOfTheMachine});
            var productionMachine = new ProductionMachine
            {
                ItemNoId = value.ItemNoID,
                PurposeOfProcessId = value.PurposeOfProcessID,
                SerialNo = value.SerialNo,
                ManufacturerId = value.ManufacturerID,
                NameOfTheMachine = value.NameOfTheMachine,
                BasicModelNo = value.BasicModelNo,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                ProfileReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.ProductionMachine.Add(productionMachine);
            _context.SaveChanges();
            value.ProductionMachineID = productionMachine.ProductionMachineId;
            if (value.OperationProcedureIDs != null && value.OperationProcedureIDs.Any())
            {
                value.OperationProcedureIDs.ForEach(o =>
                {
                    var operationProcedureMultiple = new OperationProcedureMultiple
                    {
                        ProductionMachineId = value.ProductionMachineID,
                        OperationProcedureId = o,
                    };
                    _context.OperationProcedureMultiple.Add(operationProcedureMultiple);
                });
                _context.SaveChanges();
            }
            value.MasterProfileReferenceNo = productionMachine.MasterProfileReferenceNo;
            value.Manufacturer = value.ManufacturerID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturerID).Select(m => m.Value).FirstOrDefault() : "";
            value.PurposeOfProcess = value.PurposeOfProcessID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.PurposeOfProcessID).Select(m => m.Value).FirstOrDefault() : "";
            return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionMachine")]
        public ProductionMachineModel Put(ProductionMachineModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionMachine = _context.ProductionMachine.SingleOrDefault(p => p.ProductionMachineId == value.ProductionMachineID);
            productionMachine.ItemNoId = value.ItemNoID;
            productionMachine.PurposeOfProcessId = value.PurposeOfProcessID;
            productionMachine.ManufacturerId = value.ManufacturerID;
            productionMachine.ProductionMachineId = value.ProductionMachineID;
            productionMachine.SerialNo = value.SerialNo;
            productionMachine.NameOfTheMachine = value.NameOfTheMachine;
            productionMachine.BasicModelNo = value.BasicModelNo;
            productionMachine.ModifiedByUserId = value.ModifiedByUserID;
            productionMachine.ModifiedDate = DateTime.Now;
            var OperationProcedureMultiple = _context.OperationProcedureMultiple.Where(m => m.ProductionMachineId == value.ProductionMachineID).AsNoTracking().ToList();
            if (OperationProcedureMultiple.Count() > 0)
            {
                _context.OperationProcedureMultiple.RemoveRange(OperationProcedureMultiple);
                _context.SaveChanges();
            }
            if (value.OperationProcedureIDs.Any() && value.OperationProcedureIDs != null)
            {
                value.OperationProcedureIDs.ForEach(o =>
                {
                    var exist = _context.OperationProcedureMultiple.Where(opmodel => opmodel.OperationProcedureId == o).FirstOrDefault();
                    if (exist == null)
                    {
                        var operationProcedureMultiple = new OperationProcedureMultiple
                        {
                            ProductionMachineId = value.ProductionMachineID,
                            OperationProcedureId = o,
                        };
                        _context.OperationProcedureMultiple.Add(operationProcedureMultiple);
                    }
                });
                _context.SaveChanges();
            }
            _context.SaveChanges();
            value.Manufacturer = value.ManufacturerID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ManufacturerID).Select(m => m.Value).FirstOrDefault() : "";
            value.PurposeOfProcess = value.PurposeOfProcessID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.PurposeOfProcessID).Select(m => m.Value).FirstOrDefault() : "";
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionMachine")]
        public void Delete(int id)
        {
            var productionMachine = _context.ProductionMachine.SingleOrDefault(p => p.ProductionMachineId == id);
            if (productionMachine != null)
            {
                var OperationProcedureMultiple = _context.OperationProcedureMultiple.Where(m => m.ProductionMachineId == id).AsNoTracking().ToList();
                if (OperationProcedureMultiple.Count() > 0)
                {
                    _context.OperationProcedureMultiple.RemoveRange(OperationProcedureMultiple);
                    _context.SaveChanges();
                }
                _context.ProductionMachine.Remove(productionMachine);
                _context.SaveChanges();
            }
        }
    }
}