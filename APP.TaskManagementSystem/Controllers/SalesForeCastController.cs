﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesForeCastController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SalesForeCastController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetSaleForeCast")]
        public List<ForecastPivotModel> GetSaleForeCast()
        {
            var companyIds = new List<long> { 1 };
            var month = DateTime.Now.Month;
            var year = DateTime.Now.Year;
            var salesforecast = new List<ForecastPivotModel>();
            var acEntries = _context.Acentry.Include("AcentryLines.Item").Where(ac => ac.ToDate.Value.Month >= month && ac.FromDate.Value.Month <= month && ac.ToDate.Value.Year == year && companyIds.Contains(ac.CompanyId.Value)).AsNoTracking().ToList();
            var acModel = new List<ACItemsModel>();

            acEntries.ForEach(ac =>
            {
                ac.AcentryLines.ToList().ForEach(al =>
                {
                    var acentry = new ACItemsModel
                    {
                        DistId = ac.CustomerId.Value,
                        ACMonth = ac.ToDate.Value,
                        ItemNo = al.Item.No,
                        ACQty = al.Quantity.GetValueOrDefault(0),
                        SWItemId = al.ItemId,
                        ItemDesc = al.Item.Description,
                        CustomerId = ac.CustomerId,
                    };
                    acModel.Add(acentry);
                });

            });
            var navPostedships = _context.NavpostedShipment.Where(f => f.PostingDate >= DateTime.Now && f.PostingDate <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            var itemsalePrice = _context.ItemSalesPrice.Where(f => f.StartDate >= DateTime.Now && f.EndDate <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            var productionForecasts = _context.ProductionForecast.Where(f => f.ProductionMonth >= DateTime.Now && f.ProductionMonth <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            var navtrans = _context.Navinpreport.Where(f => f.TableName == "Production Order" || f.TableName =="Sales Order").AsNoTracking().ToList();



            return salesforecast;
        }
        [HttpGet]
        [Route("GetForeCastPivot")]
        public List<ForecastPivotModel> GetForeCastPivot()
        {
            string sqlQuery = string.Empty;
            sqlQuery = "Select distinct * from view_ForecastPivot ";
            var viewForecastPivot = _context.Set<view_ForecastPivot>().FromSqlRaw(sqlQuery).AsNoTracking().ToList();
            var saleForecasts = new List<ForecastPivotModel>();
            var itemsalePrice = _context.ItemSalesPrice.Where(f => f.StartDate >= DateTime.Now && f.EndDate <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            var productionForecasts = _context.ProductionForecast.Where(f => f.ProductionMonth >= DateTime.Now && f.ProductionMonth <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            var navPostedships = _context.NavpostedShipment.Where(f => f.PostingDate >= DateTime.Now && f.PostingDate <= DateTime.Now.AddMonths(4)).AsNoTracking().ToList();
            viewForecastPivot.ForEach(f =>
            {
                if (!saleForecasts.Any(a => a.ItemId == f.ItemId))
                {
                    var postedqty = navPostedships.Where(s => s.ItemNo == f.No).Sum(s => s.DoQty);
                    var packQuantity = productionForecasts.Where(p => p.ItemId == f.ItemId).Sum(s => s.PackQuantity);
                    saleForecasts.Add(new ForecastPivotModel
                    {
                       // MethodCodeID = f.MethodCodeID,
                        MethodDescription = f.MethodDescription,
                        MethodName = f.MethodName,
                        ItemId = f.ItemId,
                        No = f.No,
                        Description = f.Description,
                        Description2 = f.Description2,
                        Type = f.TableName == "Production Order" ? "Prod" : "Sales",
                        //CustomerId = f.CustomerId,
                        //ACQty = f.ACQty,
                        ACProd = packQuantity,
                        ACSales = f.ACQty,
                        NAVSales = f.TableName == "Production Order" ? 0 : f.Quantity,
                        NAVProd = f.TableName == "Production Order" ? f.Quantity : 0,
                        PostedProd = 0,
                        PostedSales = postedqty,
                        FromDate = f.FromDate,
                        ToDate = f.ToDate,
                        ACFromDate = f.ACFromDate,
                        ACToDate = f.ACToDate,
                        //ACEntryLineId= f.ACEntryLineId,
                    });
                }
            });
            return saleForecasts;
        }

        [HttpGet]
        [Route("GetAcByDistributors")]
        public List<ACDistributorModel> GetAcByDistributors()
        {
            string sqlQuery = string.Empty;
            sqlQuery = "Select distinct * from view_ACByDistributor ";
            var acByDistributors = _context.Set<view_AcByDistributor>().FromSqlRaw(sqlQuery).AsNoTracking().ToList();
            return PopulaterQuaterlyDistributors(acByDistributors);
        }

        private List<ACDistributorModel> PopulaterQuaterlyDistributors(List<view_AcByDistributor> acByDistributors)
        {
            DateTime currentDate = DateTime.Now;
            DateTime quaterlyDate = DateTime.Now.AddMonths(3);

            var acCurrentDate = new DateTime(currentDate.Year, currentDate.Month, 1);
            var acCurrentEndDate = new DateTime(quaterlyDate.Year, quaterlyDate.Month, DateTime.DaysInMonth(quaterlyDate.Year, quaterlyDate.Month));

            acByDistributors = acByDistributors.Where(c => (c.StartDate != null && c.EndDate != null)
                                                    && acCurrentDate <= new DateTime(c.StartDate.Value.Year, c.StartDate.Value.Month, 1)
                                                    && acCurrentEndDate >= new DateTime(c.EndDate.Value.Year, c.EndDate.Value.Month, 1)).ToList();
            List<ACDistributorModel> newacByDistributors = new List<ACDistributorModel>();
            acByDistributors.ForEach(ac =>
            {
                if (!newacByDistributors.Any(n => n.ItemId == ac.ItemId && n.CustomerId == ac.CustomerId))
                {
                    ACDistributorModel acByDistributor = new ACDistributorModel();
                    acByDistributor.ACEntryLineId = ac.ACEntryLineId;
                    acByDistributor.FromDate = ac.FromDate.Value;
                    acByDistributor.ACFromDate = acByDistributor.FromDate.Value.ToString("MMM");
                    acByDistributor.ToDate = ac.ToDate.Value;
                    acByDistributor.ACTODate = acByDistributor.ToDate.Value.ToString("MMM"); ;
                    acByDistributor.CustomerId = ac.CustomerId;
                    acByDistributor.ItemId = ac.ItemId;
                    acByDistributor.ACQty = ac.ACQty;
                    acByDistributor.No = ac.No;
                    acByDistributor.Description = ac.Description;
                    acByDistributor.Description2 = ac.Description2;
                    acByDistributor.MethodCodeID = ac.MethodCodeID;
                    acByDistributor.MethodName = ac.MethodName;
                    acByDistributor.MethodDescription = ac.MethodDescription;
                    acByDistributor.SellingPrice = ac.SellingPrice;
                    acByDistributor.StartDate = ac.StartDate;
                    acByDistributor.EndDate = ac.EndDate;
                    acByDistributor.SaleAmount = ac.SaleAmount;
                    acByDistributor.Code = ac.Code;
                    acByDistributor.Name = ac.Name;

                    var acMonthCurrentDate = new DateTime(currentDate.Year, currentDate.Month, 1);
                    var acMonthCurrentEndDate = new DateTime(currentDate.Year, currentDate.Month, DateTime.DaysInMonth(currentDate.Year, currentDate.Month));


                    var acMonth1Item = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.StartDate != null && c.EndDate != null)
                                                                        && (acMonthCurrentDate <= new DateTime(c.StartDate.Value.Year, c.StartDate.Value.Month, 1))
                                                                        && (acMonthCurrentEndDate >= new DateTime(c.EndDate.Value.Year, c.EndDate.Value.Month, DateTime.DaysInMonth(c.EndDate.Value.Year, c.EndDate.Value.Month))));

                    var acMonth2CurrentDate = new DateTime(currentDate.AddMonths(1).Year, currentDate.AddMonths(1).Month, 1);
                    var acMonth2CurrentEndDate = new DateTime(currentDate.AddMonths(1).Year, currentDate.AddMonths(1).Month, DateTime.DaysInMonth(currentDate.AddMonths(1).Year, currentDate.AddMonths(1).Month));

                    var acMonth2Item = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.StartDate != null && c.EndDate != null)
                                                                        && (acMonth2CurrentDate <= new DateTime(c.StartDate.Value.Year, c.StartDate.Value.Month, 1))
                                                                        && (acMonth2CurrentEndDate >= new DateTime(c.EndDate.Value.Year, c.EndDate.Value.Month, DateTime.DaysInMonth(c.EndDate.Value.Year, c.EndDate.Value.Month))));

                    var acMonth3CurrentDate = new DateTime(currentDate.AddMonths(2).Year, currentDate.AddMonths(2).Month, 1);
                    var acMonth3CurrentEndDate = new DateTime(currentDate.AddMonths(2).Year, currentDate.AddMonths(2).Month, DateTime.DaysInMonth(currentDate.AddMonths(2).Year, currentDate.AddMonths(2).Month));

                    var acMonth3Item = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.StartDate != null && c.EndDate != null)
                                                                        && (acMonth3CurrentDate <= new DateTime(c.StartDate.Value.Year, c.StartDate.Value.Month, 1))
                                                                        && (acMonth3CurrentEndDate >= new DateTime(c.EndDate.Value.Year, c.EndDate.Value.Month, DateTime.DaysInMonth(c.EndDate.Value.Year, c.EndDate.Value.Month))));

                    var acMonth4CurrentDate = new DateTime(currentDate.AddMonths(3).Year, currentDate.AddMonths(3).Month, 1);
                    var acMonth4CurrentEndDate = new DateTime(currentDate.AddMonths(3).Year, currentDate.AddMonths(3).Month, DateTime.DaysInMonth(currentDate.AddMonths(3).Year, currentDate.AddMonths(3).Month));


                    var acMonth4Item = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.StartDate != null && c.EndDate != null)
                                                                        && (acMonth4CurrentDate <= new DateTime(c.StartDate.Value.Year, c.StartDate.Value.Month, 1))
                                                                        && (acMonth4CurrentEndDate >= new DateTime(c.EndDate.Value.Year, c.EndDate.Value.Month, DateTime.DaysInMonth(c.EndDate.Value.Year, c.EndDate.Value.Month))));

                    acByDistributor.Month1SaleAmount = acMonth1Item != null ? acMonth1Item.SaleAmount : 0;
                    acByDistributor.Month2SaleAmount = acMonth2Item != null ? acMonth2Item.SaleAmount : 0;
                    acByDistributor.Month3SaleAmount = acMonth3Item != null ? acMonth3Item.SaleAmount : 0;
                    acByDistributor.Month4SaleAmount = acMonth4Item != null ? acMonth4Item.SaleAmount : 0;




                    var acMonth1QTY = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.FromDate != null && c.ToDate != null)
                                                                        && (acMonthCurrentDate >= new DateTime(c.FromDate.Value.Year, c.FromDate.Value.Month, 1))
                                                                        && (acMonthCurrentEndDate <= new DateTime(c.ToDate.Value.Year, c.ToDate.Value.Month, DateTime.DaysInMonth(c.ToDate.Value.Year, c.ToDate.Value.Month))));


                    var acMonth2QTY = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.FromDate != null && c.ToDate != null)
                                                                        && (acMonth2CurrentDate >= new DateTime(c.FromDate.Value.Year, c.FromDate.Value.Month, 1))
                                                                        && (acMonth2CurrentEndDate <= new DateTime(c.ToDate.Value.Year, c.ToDate.Value.Month, DateTime.DaysInMonth(c.ToDate.Value.Year, c.ToDate.Value.Month))));


                    var acMonth3QTY = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.FromDate != null && c.ToDate != null)
                                                                        && (acMonth3CurrentDate >= new DateTime(c.FromDate.Value.Year, c.FromDate.Value.Month, 1))
                                                                        && (acMonth3CurrentEndDate <= new DateTime(c.ToDate.Value.Year, c.ToDate.Value.Month, DateTime.DaysInMonth(c.ToDate.Value.Year, c.ToDate.Value.Month))));

                    var acMonth4QTY = acByDistributors.FirstOrDefault(c => (c.ItemId == ac.ItemId && c.CustomerId == ac.CustomerId)
                                                                        && (c.FromDate != null && c.ToDate != null)
                                                                        && (acMonth4CurrentDate >= new DateTime(c.FromDate.Value.Year, c.FromDate.Value.Month, 1))
                                                                        && (acMonth4CurrentEndDate <= new DateTime(c.ToDate.Value.Year, c.ToDate.Value.Month, DateTime.DaysInMonth(c.ToDate.Value.Year, c.ToDate.Value.Month))));

                    acByDistributor.Month1SaleAmount = acMonth1Item != null ? acMonth1Item.SaleAmount : 0;
                    acByDistributor.Month2SaleAmount = acMonth2Item != null ? acMonth2Item.SaleAmount : 0;
                    acByDistributor.Month3SaleAmount = acMonth3Item != null ? acMonth3Item.SaleAmount : 0;
                    acByDistributor.Month4SaleAmount = acMonth4Item != null ? acMonth4Item.SaleAmount : 0;

                    acByDistributor.Month1Qty = acMonth1QTY != null ? acMonth1QTY.ACQty : 0;
                    acByDistributor.Month2Qty = acMonth2QTY != null ? acMonth2QTY.ACQty : 0;
                    acByDistributor.Month3Qty = acMonth3QTY != null ? acMonth3QTY.ACQty : 0;
                    acByDistributor.Month4Qty = acMonth4QTY != null ? acMonth4QTY.ACQty : 0;

                    newacByDistributors.Add(acByDistributor);
                }
            });
            return newacByDistributors;
        }
    }
}