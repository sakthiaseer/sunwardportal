﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class EmployeeICTInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public EmployeeICTInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetEmployeeICTInformationByID")]
        public List<EmployeeICTInformationModel> GetEmployeeICTInformationByID(int id)
        {

            EmployeeICTInformationModel employeeICTInformationRole = new EmployeeICTInformationModel();
            var userid = _context.Employee.Where(s => s.EmployeeId == id).Select(e=>e.UserId).FirstOrDefault();
            //var roleList = _context.ApplicationUserRole.Where(s=>s.UserId==userid).AsNoTracking().Select(s=>s.RoleId).ToList();
           
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var employeeIctinformation = _context.EmployeeIctinformation.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(s=>s.EmployeeIctrole).AsNoTracking().ToList();

            List<EmployeeICTInformationModel> employeeICTInformationModel = new List<EmployeeICTInformationModel>();
            employeeIctinformation.ForEach(s =>
            {
                EmployeeICTInformationModel employeeICTInformationModels = new EmployeeICTInformationModel();

                employeeICTInformationModels.EmployeeICTInformationID = s.EmployeeIctinformationId;
                employeeICTInformationModels.EmployeeID = s.EmployeeId;
                employeeICTInformationModels.SoftwareID = s.SoftwareId;
                employeeICTInformationModels.LoginID = s.LoginId;
                employeeICTInformationModels.Password = s.Password;
                employeeICTInformationModels.RoleID = s.RoleId;
                employeeICTInformationModels.IsPortal = s.IsPortal;
                employeeICTInformationModels.StatusCodeID = s.StatusCodeId;
                employeeICTInformationModels.AddedByUserID = s.AddedByUserId;
                employeeICTInformationModels.ModifiedByUserID = s.ModifiedByUserId;
                employeeICTInformationModels.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                employeeICTInformationModels.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                employeeICTInformationModels.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                employeeICTInformationModels.AddedDate = s.AddedDate;
                employeeICTInformationModels.ModifiedDate = s.ModifiedDate;              
              
                employeeICTInformationModels.RoleIDs = s.EmployeeIctrole?.Select(e => e.RoleId).ToList();
                
                employeeICTInformationModels.Software = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.SoftwareId).Select(a => a.Value).FirstOrDefault();
                employeeICTInformationModels.Role = s.Role?.RoleName;
                
                employeeICTInformationModel.Add(employeeICTInformationModels);
            });
            return employeeICTInformationModel.OrderByDescending(a => a.EmployeeICTInformationID).Where(w => w.EmployeeID == id).ToList();
        }



        [HttpPost()]
        [Route("GetData")]
        public ActionResult<EmployeeICTInformationModel> GetData(SearchModel searchModel)
        {
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var EmployeeIctinformation = new EmployeeIctinformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).FirstOrDefault();
                        break;
                    case "Last":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).LastOrDefault();
                        break;
                    case "Next":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).LastOrDefault();
                        break;
                    case "Previous":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).FirstOrDefault();
                        break;
                    case "Last":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).LastOrDefault();
                        break;
                    case "Next":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderBy(o => o.EmployeeIctinformationId).FirstOrDefault(s => s.EmployeeIctinformationId > searchModel.Id);
                        break;
                    case "Previous":
                        EmployeeIctinformation = _context.EmployeeIctinformation.OrderByDescending(o => o.EmployeeIctinformationId).FirstOrDefault(s => s.EmployeeIctinformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<EmployeeICTInformationModel>(EmployeeIctinformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertEmployeeICTInformation")]
        public EmployeeICTInformationModel Post(EmployeeICTInformationModel value)
        {
            var applicationdetail = _context.ApplicationMasterDetail.ToList();
            //var existingIctPortal = _context.EmployeeIctinformation.Where(e => e.EmployeeId == value.EmployeeID && e.IsPortal == true).ToList();
          
            var EmployeeIctinformation = new EmployeeIctinformation
            {
                EmployeeId = value.EmployeeID,
                SoftwareId = value.SoftwareID,
                LoginId = value.LoginID,
                Password = value.Password,
                RoleId = value.RoleID,
                IsPortal = value.IsPortal,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.EmployeeIctinformation.Add(EmployeeIctinformation);
            _context.SaveChanges();
            value.EmployeeICTInformationID = EmployeeIctinformation.EmployeeIctinformationId;        

           
                //var userID = _context.Employee.Where(e => e.EmployeeId == value.EmployeeID).Select(e => e.UserId).FirstOrDefault();
                //var applicationUser = _context.ApplicationUser.Where(a => a.UserId == userID).FirstOrDefault();
                //applicationUser.LoginId = value.LoginID;
                //applicationUser.LoginPassword = value.Password;
                //var existinguser = _context.ApplicationUser.Where(a => a.LoginId == value.LoginID).FirstOrDefault();
                //if (existinguser == null)
                //{
                //    applicationUser.LoginId = value.LoginID;
                //    if (value.Password != null)
                //    {
                //        var password = EncryptDecryptPassword.Encrypt(value.Password);
                //        applicationUser.LoginPassword = password;
                //    }
                //}
                //else
                //{
                //    throw new AppException("Already existing loginID in the Current Portal! Please change the employee loginID", null);
                //}
                //if (value.RoleIDs != null && value.RoleIDs.Count > 0)
                //{
                //    value.RoleIDs.ForEach(r =>
                //    {


                //        var existing = _context.ApplicationUserRole.Where(a => a.RoleId == r && a.UserId == userID).FirstOrDefault();
                //        if (existing == null)
                //        {
                //            var userRole = new ApplicationUserRole
                //            {

                //                UserId = userID.Value,
                //                RoleId = r.Value,

                //            };
                //            _context.ApplicationUserRole.Add(userRole);
                //        }
                //    });
                //}
           
                if (value.RoleIDs != null && value.RoleIDs.Count > 0)
                {
                    value.RoleIDs.ForEach(r =>
                    {


                        var existing = _context.EmployeeIctrole.Where(a => a.RoleId == r && a.EmployeeIctinformationId == value.EmployeeICTInformationID).FirstOrDefault();
                        if (existing == null)
                        {
                            var employeeIctrole = new EmployeeIctrole
                            {

                                EmployeeIctinformationId = value.EmployeeICTInformationID,
                                RoleId = r.Value,

                            };
                            _context.EmployeeIctrole.Add(employeeIctrole);
                        }
                    });
                }
            
            _context.SaveChanges();
            value.Software = applicationdetail.Where(a => a.ApplicationMasterDetailId == value.SoftwareID).Select(a => a.Value).FirstOrDefault();
            value.Role = _context.ApplicationRole.Where(a => a.RoleId == value.RoleID).Select(a => a.RoleName).FirstOrDefault();

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateEmployeeICTInformation")]
        public EmployeeICTInformationModel Put(EmployeeICTInformationModel value)
        {
            var applicationdetail = _context.ApplicationMasterDetail.ToList();
            //var existingIctPortal = _context.EmployeeIctinformation.Where(e => e.EmployeeId == value.EmployeeID && e.IsPortal == true).ToList();
            //if (value.IsPortal == true)
            //{
            //    if (existingIctPortal != null && existingIctPortal.Count > 0)
            //    {


            //        existingIctPortal.ForEach(e =>
            //        {
            //            e.IsPortal = false;
            //        });
            //        _context.SaveChanges();
            //    }
            //}
            var EmployeeIctinformation = _context.EmployeeIctinformation.SingleOrDefault(p => p.EmployeeIctinformationId == value.EmployeeICTInformationID);

            EmployeeIctinformation.EmployeeId = value.EmployeeID;
            EmployeeIctinformation.SoftwareId = value.SoftwareID;
            EmployeeIctinformation.RoleId = value.RoleID;
            EmployeeIctinformation.LoginId = value.LoginID;
            EmployeeIctinformation.IsPortal = value.IsPortal;
            EmployeeIctinformation.Password = value.Password;
            EmployeeIctinformation.ModifiedByUserId = value.ModifiedByUserID;
            EmployeeIctinformation.ModifiedDate = DateTime.Now;
            EmployeeIctinformation.StatusCodeId = value.StatusCodeID.Value;
            value.Software = applicationdetail.Where(a => a.ApplicationMasterDetailId == value.SoftwareID).Select(a => a.Value).FirstOrDefault();
            value.Role = _context.ApplicationRole.Where(a => a.RoleId == value.RoleID).Select(a => a.RoleName).FirstOrDefault();
            if (value.RoleIDs != null && value.RoleIDs.Count > 0)
            {
                value.RoleIDs.ForEach(r =>
                {


                    var existing = _context.EmployeeIctrole.Where(a => a.RoleId == r && a.EmployeeIctinformationId == value.EmployeeICTInformationID).FirstOrDefault();
                    if (existing == null)
                    {
                        var employeeIctrole = new EmployeeIctrole
                        {

                            EmployeeIctinformationId = value.EmployeeICTInformationID,
                            RoleId = r.Value,

                        };
                        _context.EmployeeIctrole.Add(employeeIctrole);
                    }
                });
            }
            //if (value.IsPortal == true)
            //{
            //    var userID = _context.Employee.Where(e => e.EmployeeId == value.EmployeeID).Select(e => e.UserId).FirstOrDefault();
            //    var applicationUser = _context.ApplicationUser.Where(a => a.UserId == userID).FirstOrDefault();
            //    var existinguser = _context.ApplicationUser.Where(a => a.LoginId == value.LoginID).FirstOrDefault();
            //    if (existinguser == null)
            //    {
            //        applicationUser.LoginId = value.LoginID;
            //        if (value.Password != null)
            //        {
            //            var password = EncryptDecryptPassword.Encrypt(value.Password);
            //            applicationUser.LoginPassword = password;
            //        }

            //    }
            //    else
            //    {
            //        throw new AppException("Already existing loginID in the Current Portal! Please change the employee loginID", null);
            //    }
            //    if (value.RoleIDs != null && value.RoleIDs.Count > 0)
            //    {
            //        value.RoleIDs.ForEach(r =>
            //        {


            //            var existing = _context.ApplicationUserRole.Where(a => a.RoleId == r && a.UserId == userID).FirstOrDefault();
            //            if (existing == null)
            //            {
            //                var userRole = new ApplicationUserRole
            //                {

            //                    UserId = userID.Value,
            //                    RoleId = r.Value,

            //                };
            //                _context.ApplicationUserRole.Add(userRole);
            //            }
            //        });
            //    }
            //}
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteEmployeeICTInformation")]
        public void Delete(int id)
        {
            var EmployeeIctinformation = _context.EmployeeIctinformation.SingleOrDefault(p => p.EmployeeIctinformationId == id);
            if (EmployeeIctinformation != null)
            {
                var employeeictRole = _context.EmployeeIctrole.Where(s => s.EmployeeIctinformationId == id).AsNoTracking().ToList();
                if(employeeictRole!=null)
                {
                    _context.EmployeeIctrole.RemoveRange(employeeictRole);
                    _context.SaveChanges();
                }
                _context.EmployeeIctinformation.Remove(EmployeeIctinformation);
                _context.SaveChanges();
            }
        }
    }
}