﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesBorrowHeaderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;


        public SalesBorrowHeaderController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetSalesBorrowHeader")]
        public List<SalesBorrowHeaderModel> Get()
        {
            var SalesBorrowHeader = _context.SalesBorrowHeader.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Include(s=>s.OnBehalf).Include(s=>s.BorrowFrom).Include(s=>s.CustomerToSend).AsNoTracking().ToList();
          
            List<SalesBorrowHeaderModel> SalesBorrowHeaderModel = new List<SalesBorrowHeaderModel>();
            SalesBorrowHeader.ForEach(s =>
            {
                SalesBorrowHeaderModel SalesBorrowHeaderModels = new SalesBorrowHeaderModel
                {
                    SalesBorrowHeaderId = s.SalesBorrowHeaderId,
                    OnBehalfId = s.OnBehalfId,
                    OnBehalf = s.OnBehalf?.UserName,
                    BorrowFrom = s.BorrowFrom?.CompanyName,
                    BorrowFromId = s.BorrowFromId,
                    CustomerToSendId = s.CustomerToSendId,
                    CustomerToSend = s.CustomerToSend?.CompanyName,
                    PurposeOfBorrow = s.PurposeOfBorrow,
                    IsReturn = s.IsReturn,
                    SessionId = s.SessionId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                SalesBorrowHeaderModel.Add(SalesBorrowHeaderModels);
            });
            return SalesBorrowHeaderModel.OrderByDescending(a => a.SalesBorrowHeaderId).ToList();
        }

        [HttpGet]
        [Route("GetSalesBorrowLinesByID")]
        public List<SalesBorrowLineModel> GetSalesBorrowLinesByID(long? id)
        {
            var salesBorrowLine = _context.SalesBorrowLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).
                Include(s => s.StatusCode).Include(i=>i.Item).Where(s=>s.SalesBorrowId == id).AsNoTracking().ToList();
           
            List<SalesBorrowLineModel> salesBorrowLineModels = new List<SalesBorrowLineModel>();
            salesBorrowLine.ForEach(s =>
            {
                SalesBorrowLineModel salesBorrowLineModel = new SalesBorrowLineModel
                {
                    
                    SalesBorrowLineId = s.SalesBorrowLineId,
                    SalesBorrowId = s.SalesBorrowId,
                    ItemId = s.ItemId,
                    Item = s.Item?.Code,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                salesBorrowLineModels.Add(salesBorrowLineModel);
            });
            return salesBorrowLineModels.OrderByDescending(a => a.SalesBorrowLineId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SalesBorrowHeaderModel> GetData(SearchModel searchModel)
        {
            var SalesBorrowHeader = new SalesBorrowHeader();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).LastOrDefault();
                        break;
                    case "Next":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).LastOrDefault();
                        break;
                    case "Previous":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).LastOrDefault();
                        break;
                    case "Next":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderBy(o => o.SalesBorrowHeaderId).FirstOrDefault(s => s.SalesBorrowHeaderId > searchModel.Id);
                        break;
                    case "Previous":
                        SalesBorrowHeader = _context.SalesBorrowHeader.OrderByDescending(o => o.SalesBorrowHeaderId).FirstOrDefault(s => s.SalesBorrowHeaderId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SalesBorrowHeaderModel>(SalesBorrowHeader);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSalesBorrowHeader")]
        public SalesBorrowHeaderModel Post(SalesBorrowHeaderModel value)
        {
            var SessionId = Guid.NewGuid();
            var SalesBorrowHeader = new SalesBorrowHeader
            {

                OnBehalfId = value.OnBehalfId,
                BorrowFromId = value.BorrowFromId,
                CustomerToSendId = value.CustomerToSendId,
                PurposeOfBorrow = value.PurposeOfBorrow,
                IsReturn = value.IsReturn,
                
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.SalesBorrowHeader.Add(SalesBorrowHeader);
            _context.SaveChanges();
            value.SalesBorrowHeaderId = SalesBorrowHeader.SalesBorrowHeaderId;
            value.SessionId = SalesBorrowHeader.SessionId;
            if (value.OnBehalfId != null)
            {
                value.OnBehalf = _context.ApplicationUser.Where(s => s.UserId == value.OnBehalfId).Select(s => s.UserName).FirstOrDefault();
            }
            if(value.BorrowFromId!=null)
            {
                value.BorrowFrom = _context.CompanyListing.Where(c => c.CompanyListingId == value.BorrowFromId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.CustomerToSendId != null)
            {
                value.CustomerToSend = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerToSendId).Select(s => s.CompanyName).FirstOrDefault();
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesBorrowHeader")]
        public SalesBorrowHeaderModel Put(SalesBorrowHeaderModel value)
        {
            var SessionId = Guid.NewGuid();


            var salesBorrowHeader = _context.SalesBorrowHeader.SingleOrDefault(p => p.SalesBorrowHeaderId == value.SalesBorrowHeaderId);
            salesBorrowHeader.OnBehalfId = value.OnBehalfId;
            salesBorrowHeader.BorrowFromId = value.BorrowFromId;
            salesBorrowHeader.CustomerToSendId = value.CustomerToSendId;
            salesBorrowHeader.PurposeOfBorrow = value.PurposeOfBorrow;
            salesBorrowHeader.IsReturn = value.IsReturn;
            if (value.SessionId != null)
            {
                salesBorrowHeader.SessionId = value.SessionId;


            }
            else
            {
                salesBorrowHeader.SessionId = SessionId;

            }
            salesBorrowHeader.ModifiedByUserId = value.ModifiedByUserID;
            salesBorrowHeader.ModifiedDate = DateTime.Now;
            salesBorrowHeader.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            if (value.OnBehalfId != null)
            {
                value.OnBehalf = _context.ApplicationUser.Where(s => s.UserId == value.OnBehalfId).Select(s => s.UserName).FirstOrDefault();
            }
            if (value.BorrowFromId != null)
            {
                value.BorrowFrom = _context.CompanyListing.Where(c => c.CompanyListingId == value.BorrowFromId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.CustomerToSendId != null)
            {
                value.CustomerToSend = _context.CompanyListing.Where(c => c.CompanyListingId == value.CustomerToSendId).Select(s => s.CompanyName).FirstOrDefault();
            }
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSalesBorrowHeader")]
        public void Delete(int id)
        {
            try
            {
                var SalesBorrowHeader = _context.SalesBorrowHeader.SingleOrDefault(p => p.SalesBorrowHeaderId == id);
                if (SalesBorrowHeader != null)
                {
                    _context.SalesBorrowHeader.Remove(SalesBorrowHeader);
                    _context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("This Sales Borrow Cannot be Delete Reference to others", ex);
            }
        }


        [HttpPost]
        [Route("InsertSalesBorrowLine")]
        public SalesBorrowLineModel InsertSalesBorrowLine(SalesBorrowLineModel value)
        {
           
            var salesBorrowLine = new SalesBorrowLine
            {


                SalesBorrowId = value.SalesBorrowId,
                ItemId = value.ItemId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                AddedByUserId = value.AddedByUserID.Value,
            };
            _context.SalesBorrowLine.Add(salesBorrowLine);
            _context.SaveChanges();
            value.SalesBorrowLineId = salesBorrowLine.SalesBorrowLineId;
            //if (value.ItemId > 0)
            //{
            //    value.Item = _context.GenericCodes.Where(g => g.GenericCodeId == value.ItemId).FirstOrDefault().Code;
            //}
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesBorrowLine")]
        public SalesBorrowLineModel UpdateSalesBorrowLine(SalesBorrowLineModel value)
        {
            var SessionId = Guid.NewGuid();


            var salesBorrowLine = _context.SalesBorrowLine.SingleOrDefault(p => p.SalesBorrowLineId == value.SalesBorrowLineId);

            salesBorrowLine.SalesBorrowId = value.SalesBorrowId;
            salesBorrowLine.ItemId = value.ItemId;
            salesBorrowLine.ModifiedByUserId = value.ModifiedByUserID;
            salesBorrowLine.ModifiedDate = DateTime.Now;
            salesBorrowLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSalesBorrowLine")]
        public void DeleteSalesBorrowLine(int id)
        {
            var salesBorrowLine = _context.SalesBorrowLine.SingleOrDefault(p => p.SalesBorrowLineId == id);
            if (salesBorrowLine != null)
            {
                _context.SalesBorrowLine.Remove(salesBorrowLine);
                _context.SaveChanges();
            }
        }

    }
}