﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class GroupPlanningController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IHubContext<ChatHub> _hub;

        private readonly IBackgroundJobClient _backgroundJobClient;
        public GroupPlanningController(CRT_TMSContext context, IMapper mapper,
            IConfiguration config, IHubContext<ChatHub> hub,
            IBackgroundJobClient backgroundJobClient)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
            _hub = hub;

            _backgroundJobClient = backgroundJobClient;
        }
        [HttpGet]
        [Route("GetGroupPlanning")]
        public List<GroupPlanningModel> Get()
        {
           
            List<GroupPlanningModel> groupPlanningModels = new List<GroupPlanningModel>();
            var groupPlanning = _context.GroupPlanning
               .OrderByDescending(o => o.GroupPlanningId).AsNoTracking().ToList();
            if (groupPlanning != null && groupPlanning.Count > 0)
            {
                groupPlanning.ForEach(s =>
                {
                    GroupPlanningModel groupplanningModel = new GroupPlanningModel
                    {
                        GroupPlanningId = s.GroupPlanningId,
                        CompanyId = s.CompanyId,
                        ProductGroupCode = s.ProductGroupCode,
                        StartDate = s.StartDate,
                        ItemNo = s.ItemNo,
                        ProductDescription = s.ProductDescription,
                        ItemDescription = s.ItemDescription,
                        ItemDescription1 = s.ItemDescription1,
                        RecipeNo = s.RecipeNo,
                        BatchSize = s.BatchSize,
                        Quantity = s.Quantity,
                        Uom = s.Uom,
                        NoOfTicket = s.NoOfTicket,
                        OrderCreated = s.OrderCreated,
                        ItemDescriptions = s.ItemDescription + " | " + s.ItemDescription1,


                    };
                    groupPlanningModels.Add(groupplanningModel);

                });

            }

            return groupPlanningModels;
        }

        [HttpPost()]
        [Route("GetSearchData")]
        public ActionResult<GroupPlanningModel> GetData(SearchModel searchModel)
        {
            var GroupPlanning = new GroupPlanning();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).FirstOrDefault();
                        break;
                    case "Last":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).LastOrDefault();
                        break;
                    case "Next":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).LastOrDefault();
                        break;
                    case "Previous":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).FirstOrDefault();
                        break;
                    case "Last":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).LastOrDefault();
                        break;
                    case "Next":
                        GroupPlanning = _context.GroupPlanning.OrderBy(o => o.GroupPlanningId).FirstOrDefault(s => s.GroupPlanningId > searchModel.Id);
                        break;
                    case "Previous":
                        GroupPlanning = _context.GroupPlanning.OrderByDescending(o => o.GroupPlanningId).FirstOrDefault(s => s.GroupPlanningId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<GroupPlanningModel>(GroupPlanning);
            return result;
        }
        [HttpPut]
        [Route("UpdateGroupPlanning")]
        public GroupPlanningModel Put(GroupPlanningModel value)
        {
            var groupPlanning = _context.GroupPlanning.SingleOrDefault(p => p.GroupPlanningId == value.GroupPlanningId);
         
            groupPlanning.Quantity = value.Quantity;          
            groupPlanning.NoOfTicket = value.NoOfTicket;
           

            _context.SaveChanges();
            return value;
        }

        [HttpDelete]
        [Route("DeleteGroupPlanning")]
        public ActionResult<string> DeleteGroupPlanning(int id)
        {
            try
            {
                var deleteGroupPlanning = _context.GroupPlanning.Where(p => p.GroupPlanningId == id).FirstOrDefault();
                if (deleteGroupPlanning != null)
                {
                    _context.GroupPlanning.Remove(deleteGroupPlanning);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
