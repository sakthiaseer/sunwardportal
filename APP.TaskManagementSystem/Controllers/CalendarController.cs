﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CalendarController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CalendarController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCalendars")]
        public List<CalendarModel> Get()
        {
            var calendars = _context.CompanyEvents.ToList();
            List<CalendarModel> calendarModel = new List<CalendarModel>();
            calendars.ForEach(s =>
            {
                CalendarModel calendarModels = new CalendarModel
                {
                    Id = s.CompanyEventId,
                    Title = s.Name,
                    Start = s.StartDate,
                    End = s.StartDate,
                    SourceId = s.CompanyEventId,
                    Type = "CompanyEnvent",
                    cssClass = "blue",
                };
                calendarModel.Add(calendarModels);
            });
            var holidays = _context.CompanyHolidayDetail.Include(h => h.Holiday).AsNoTracking().ToList();
            List<CalendarModel> holidaysModel = new List<CalendarModel>();
            holidays.ForEach(s =>
            {
                CalendarModel holidaysModels = new CalendarModel
                {
                    Id = s.CompanyHolidayDetailId,
                    Title = s.Holiday.Name,
                    Start = s.HolidayDate,
                    End = s.HolidayDate,
                    SourceId = s.CompanyHolidayDetailId,
                    Type = "Holidays",
                    cssClass = "green"

                };
                holidaysModel.Add(holidaysModels);
            });
            if (holidaysModel.Count > 0)
                calendarModel.AddRange(holidaysModel);

            return calendarModel;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetCalendars/{id:int}")]
        public ActionResult<CalendarModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var calendar = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == id.Value);
            var result = _mapper.Map<CalendarModel>(calendar);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CalendarModel> GetData(SearchModel searchModel)
        {
            var calendar = new CompanyEvents();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                    case "Last":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Next":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Previous":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                    case "Last":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Next":
                        calendar = _context.CompanyEvents.OrderBy(o => o.CompanyEventId).FirstOrDefault(s => s.CompanyEventId > searchModel.Id);
                        break;
                    case "Previous":
                        calendar = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault(s => s.CompanyEventId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CalendarModel>(calendar);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCalendar")]
        public CalendarModel Post(CalendarModel value)
        {
            var calendar = new CompanyEvents
            {
                Name = value.Type,
                StartDate = value.Start,
                TaskDueDate = value.End,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.CompanyEvents.Add(calendar);
            _context.SaveChanges();
            value.Id = calendar.CompanyEventId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCalendar")]
        public CalendarModel Put(CalendarModel value)
        {
            var calendar = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == value.Id);
            //Country.CountryId = value.CountryID;
            calendar.ModifiedByUserId = value.ModifiedByUserID;
            calendar.ModifiedDate = DateTime.Now;
            calendar.Name = value.Type;
            calendar.StartDate = value.Start;
            calendar.TaskDueDate = value.End;
            calendar.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCalendar")]
        public void Delete(int id)
        {
            var calendar = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == id);
            if (calendar != null)
            {
                _context.CompanyEvents.Remove(calendar);
                _context.SaveChanges();
            }
        }
    }
}