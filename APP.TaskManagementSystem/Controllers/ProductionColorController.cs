﻿using System;
using System.Collections.Generic;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionColorController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionColorController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionColors")]
        public List<ProductionColorModel> Get()
        {
            List<ProductionColorModel> productionColorModels = new List<ProductionColorModel>();
            var productionColor = _context.ProductionColor.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").OrderByDescending(o => o.ProductionColorId).AsNoTracking().ToList();
            if(productionColor!=null && productionColor.Count>0)
            {
                List<long?> masterIds = productionColor.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList();
                masterIds.AddRange(productionColor.Where(w => w.ColorIndexNumberId != null).Select(a => a.ColorIndexNumberId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.GenericName != null).Select(a => a.GenericName).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionColor.ForEach(s =>
                {
                    ProductionColorModel productionColorModel = new ProductionColorModel
                    {
                        ProductionColorID = s.ProductionColorId,
                        ColorID = s.ColorId,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        ColorIndexNumberID = s.ColorIndexNumberId,
                        ColorIndexNumber = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorIndexNumberId).Select(a => a.Value).SingleOrDefault() : "",
                        DyeContent = s.DyeContent,
                        ItemNo = s.ItemNo,
                        //Item = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ItemNo).Select(a => a.Value).SingleOrDefault() : "",
                        GenericName = s.GenericName,
                        GenericMasterName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.GenericName).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec!=null? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",

                    };
                    productionColorModels.Add(productionColorModel);
                });
            }           
            return productionColorModels;
        }

        [HttpPost]
        [Route("GetProductionColorsByRefNo")]
        public List<ProductionColorModel> GetProductionColorsByRefNo(RefSearchModel refSearchModel)
        {
            List<ProductionColorModel> productionColorModels = new List<ProductionColorModel>();
            List<ProductionColor> productionColor = new List<ProductionColor>();
            if (refSearchModel.IsHeader)
            {
                productionColor = _context.ProductionColor.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionColorId).AsNoTracking().ToList();
            }
            else
            {
                productionColor = _context.ProductionColor.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionColorId).AsNoTracking().ToList();
            }
            if(productionColor!=null && productionColor.Count>0)
            {
                List<long?> masterIds = productionColor.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList();
                masterIds.AddRange(productionColor.Where(w => w.ColorIndexNumberId != null).Select(a => a.ColorIndexNumberId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.GenericName != null).Select(a => a.GenericName).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionColor.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();

                productionColor.ForEach(s =>
                {
                    ProductionColorModel productionColorModel = new ProductionColorModel
                    {
                        ProductionColorID = s.ProductionColorId,
                        ColorID = s.ColorId,
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        ColorIndexNumberID = s.ColorIndexNumberId,
                        ColorIndexNumber = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorIndexNumberId).Select(a => a.Value).SingleOrDefault() : "",
                        DyeContent = s.DyeContent,
                        ItemNo = s.ItemNo,
                        GenericName = s.GenericName,
                        GenericMasterName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.GenericName).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNoID = s.ProductionColorId,
                        SpecNo = s.SpecNo,
                        SpecNos = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()) + " " + "|" + " " + s.MaterialName,
                        SpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName =  s.ProcessUseSpec!=null? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",

                    };
                    productionColorModels.Add(productionColorModel);
                });
            }          
            
            return productionColorModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionColorModel> GetData(SearchModel searchModel)
        {
            var productionColor = new ProductionColor();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).FirstOrDefault();
                        break;
                    case "Last":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).LastOrDefault();
                        break;
                    case "Next":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).LastOrDefault();
                        break;
                    case "Previous":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).FirstOrDefault();
                        break;
                    case "Last":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).LastOrDefault();
                        break;
                    case "Next":
                        productionColor = _context.ProductionColor.OrderBy(o => o.ProductionColorId).FirstOrDefault(s => s.ProductionColorId > searchModel.Id);
                        break;
                    case "Previous":
                        productionColor = _context.ProductionColor.OrderByDescending(o => o.ProductionColorId).FirstOrDefault(s => s.ProductionColorId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionColorModel>(productionColor);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionColor")]
        public ProductionColorModel Post(ProductionColorModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MaterialName });
            var ProductionColorCount = _context.ProductionColor.Where(p => p.LinkProfileReferenceNo == value.LinkProfileReferenceNo).Count();

            var productionColor = new ProductionColor
            {
                ColorId = value.ColorID,
                ItemNo = value.ItemNo,
                GenericName = value.GenericName,
                ColorIndexNumberId = value.ColorIndexNumberID,
                DyeContent = value.DyeContent,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                ProfileReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                IsSameAsMaster = value.IsSameAsMaster,
                MaterialName = value.MaterialName,
                SpecNo = ProductionColorCount + 1,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                PurposeId = value.PurposeId,
                StudyLink = value.StudyLink,
                ProcessUseSpecId = value.ProcessUseSpecId,
                RndpurposeId = value.RndpurposeId,
            };
            _context.ProductionColor.Add(productionColor);
            _context.SaveChanges();
            value.ProductionColorID = productionColor.ProductionColorId;
            value.MasterProfileReferenceNo = productionColor.MasterProfileReferenceNo;
            value.Color = value.ColorID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ColorID).Select(m => m.Value).FirstOrDefault() : "";
            value.Item = value.ItemNo != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ItemNo).Select(m => m.Value).FirstOrDefault() : "";
            value.GenericMasterName = value.GenericName != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.GenericName).Select(m => m.Value).FirstOrDefault() : "";
            value.SpecificationNo = "Spec" + " " + ((productionColor.SpecNo > 10) ? productionColor.SpecNo.ToString() : "0" + productionColor.SpecNo.ToString());
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionColor")]
        public ProductionColorModel Put(ProductionColorModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionColor = _context.ProductionColor.SingleOrDefault(p => p.ProductionColorId == value.ProductionColorID);
            productionColor.ItemNo = value.ItemNo;
            productionColor.ColorId = value.ColorID;
            productionColor.ColorIndexNumberId = value.ColorIndexNumberID;
            productionColor.ProductionColorId = value.ProductionColorID;
            productionColor.DyeContent = value.DyeContent;
            productionColor.GenericName = value.GenericName;
            productionColor.ModifiedByUserId = value.ModifiedByUserID;
            productionColor.ModifiedDate = DateTime.Now;
            productionColor.IsSameAsMaster = value.IsSameAsMaster;
            productionColor.MaterialName = value.MaterialName;
            productionColor.PurposeId = value.PurposeId;
            productionColor.StudyLink = value.StudyLink;
            productionColor.ProcessUseSpecId = value.ProcessUseSpecId;
            productionColor.RndpurposeId = value.RndpurposeId;
            productionColor.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            value.Color = value.ColorID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ColorID).Select(m => m.Value).FirstOrDefault() : "";
            value.Item = value.ItemNo != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.ItemNo).Select(m => m.Value).FirstOrDefault() : "";
            value.GenericMasterName = value.GenericName != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.GenericName).Select(m => m.Value).FirstOrDefault() : "";
            value.SpecificationNo = "Spec" + " " + ((productionColor.SpecNo > 10) ? productionColor.SpecNo.ToString() : "0" + productionColor.SpecNo.ToString());
            return value;

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionColor")]
        public void Delete(int id)
        {
            try
            {
                var productionColor = _context.ProductionColor.SingleOrDefault(p => p.ProductionColorId == id);
            if (productionColor != null)
            {
                var ProductionColorLine = _context.ProductionColorLine.Where(m => m.ProductionColorId == id).AsNoTracking().ToList();
                if (ProductionColorLine.Count() > 0)
                {
                    _context.ProductionColorLine.RemoveRange(ProductionColorLine);
                    _context.SaveChanges();
                }
                _context.ProductionColor.Remove(productionColor);
                _context.SaveChanges();
                /*var ProductionColorList = _context.ProductionColor.Where(p => p.LinkProfileReferenceNo == productionColor.LinkProfileReferenceNo).OrderBy(a => a.ProductionColorId).AsNoTracking().ToList();
                int inc = 1;
                if (ProductionColorList != null)
                {
                    ProductionColorList.ForEach(h =>
                    {
                        var ProductionColorListItems = _context.ProductionColor.SingleOrDefault(p => p.ProductionColorId == h.ProductionColorId);
                        ProductionColorListItems.SpecNo = inc;
                        _context.SaveChanges();
                        inc++;
                    });
                }*/
            }
               // return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new AppException("You’re not allowed to delete this record", ex);
            }
        }
    }
}