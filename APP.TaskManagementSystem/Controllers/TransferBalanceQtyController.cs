﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.Utilities;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TransferBalanceQtyController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public TransferBalanceQtyController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        [Route("GetTransferBalanceQty")]
        public List<TransferBalanceQtyModel> Get()
        {
            var transferBalanceQty = _context.TransferBalanceQty.Include(t => t.TransferTime).Include(p => p.Product).Include(c => c.Customer).Include(t => t.TransferToContract).Include(c => c.Contract).Include(p => p.Product).Include(ss => ss.Product.SobyCustomers).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().ToList();

            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<TransferBalanceQtyModel> transferBalanceQtyModel = new List<TransferBalanceQtyModel>();
            if (transferBalanceQty != null && transferBalanceQty.Count > 0)
            {
                var masterDetailIds = transferBalanceQty.Where(s => s.Product?.PerUomId != null).Select(s => s.Product?.PerUomId).ToList();
                if (masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                transferBalanceQty.ForEach(s =>
                {
                    TransferBalanceQtyModel transferBalanceQtyModels = new TransferBalanceQtyModel
                    {
                        TransferBalanceQtyId = s.TransferBalanceQtyId,
                        CustomerId = s.CustomerId,
                        CustomerName = s.Customer?.CompanyName,
                        ContractId = s.ContractId,
                        ContractNo = s.Contract?.OrderNo,
                        ProductId = s.ProductId,
                        TransferToContractId = s.TransferToContractId,
                        TransferToContractNo = s.TransferToContract?.OrderNo,
                        TransferTimeId = s.TransferTimeId,
                        TransferTimeName = s.TransferTime != null ? s.TransferTime.CodeValue : null,
                        SpecificMonth = s.SpecificMonth,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProductName = s.Product?.CustomerReferenceNo,
                        Uom = s.Product != null && s.Product?.PerUomId != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        SessionId = s.SessionId.Value,
                    };
                    transferBalanceQtyModel.Add(transferBalanceQtyModels);
                });
            }
            return transferBalanceQtyModel.OrderByDescending(a => a.TransferBalanceQtyId).ToList();
        }

        [Route("GetTransferBalanceQtyByContractId")]
        public List<TransferBalanceQtyModel> GetTransferBalanceQtyByContractId(int id)
        {
            var transferBalanceQty = _context.TransferBalanceQty.Include(t => t.TransferTime).Include(p => p.Product).Include(c => c.Customer).Include(t => t.TransferToContract).Include(c => c.Contract).Include(p => p.Product).Include(ss => ss.Product.SobyCustomers).Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.ContractId == id).AsNoTracking().ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<TransferBalanceQtyModel> transferBalanceQtyModel = new List<TransferBalanceQtyModel>();
            if (transferBalanceQty != null && transferBalanceQty.Count > 0)
            {
                var masterDetailIds = transferBalanceQty.Where(s => s.Product?.PerUomId != null).Select(s => s.Product?.PerUomId).ToList();
                if (masterDetailIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                transferBalanceQty.ForEach(s =>
                {

                    TransferBalanceQtyModel transferBalanceQtyModels = new TransferBalanceQtyModel
                    {
                        TransferBalanceQtyId = s.TransferBalanceQtyId,
                        CustomerId = s.CustomerId,
                        CustomerName = s.Customer?.CompanyName,
                        ContractId = s.ContractId,
                        ContractNo = s.Contract?.OrderNo,
                        ProductId = s.ProductId,
                        TransferToContractId = s.TransferToContractId,
                        TransferToContractNo = s.TransferToContract?.OrderNo,
                        TransferTimeId = s.TransferTimeId,
                        TransferTimeName = s.TransferTime != null ? s.TransferTime.CodeValue : null,
                        SpecificMonth = s.SpecificMonth,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProductName = s.Product?.CustomerReferenceNo,
                        Uom = s.Product != null && s.Product?.PerUomId != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.Product?.PerUomId).Select(a => a.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        SessionId = s.SessionId.Value,
                    };
                    transferBalanceQtyModel.Add(transferBalanceQtyModels);
                });
            }
            return transferBalanceQtyModel.OrderByDescending(a => a.TransferBalanceQtyId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TransferBalanceQtyModel> GetData(SearchModel searchModel)
        {
            var transferBalanceQty = new TransferBalanceQty();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).FirstOrDefault();
                        break;
                    case "Last":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).LastOrDefault();
                        break;
                    case "Next":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).LastOrDefault();
                        break;
                    case "Previous":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).FirstOrDefault();
                        break;
                    case "Last":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).LastOrDefault();
                        break;
                    case "Next":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderBy(o => o.TransferBalanceQtyId).FirstOrDefault(s => s.TransferBalanceQtyId > searchModel.Id);
                        break;
                    case "Previous":
                        transferBalanceQty = _context.TransferBalanceQty.Include(c => c.Contract).OrderByDescending(o => o.TransferBalanceQtyId).FirstOrDefault(s => s.TransferBalanceQtyId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TransferBalanceQtyModel>(transferBalanceQty);
            if (result != null)
            {
                result.ContractNo = transferBalanceQty.Contract?.OrderNo;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertTransferBalanceQty")]
        public TransferBalanceQtyModel Post(TransferBalanceQtyModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.TransferTimeId == 1581)
            {
                value.SpecificMonth = null;
            }
            var transferBalanceQty = new TransferBalanceQty
            {
                CustomerId = value.CustomerId,
                ContractId = value.ContractId,
                ProductId = value.ProductId,
                TransferToContractId = value.TransferToContractId,
                TransferTimeId = value.TransferTimeId,
                SpecificMonth = value.SpecificMonth,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                SessionId = SessionId,
            };
            _context.TransferBalanceQty.Add(transferBalanceQty);
            _context.SaveChanges();
            value.SessionId = SessionId;
            if (value.ProductId > 0)
            {
                value.ProductName = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.ProductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            if (value.TransferToContractId > 0)
            {
                value.TransferToContractNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.TransferToContractId).Select(s => s.OrderNo).FirstOrDefault();
            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.ContractId > 0)
            {
                value.ContractNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.ContractId).Select(s => s.OrderNo).FirstOrDefault();
            }
            value.TransferBalanceQtyId = transferBalanceQty.TransferBalanceQtyId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTransferBalanceQty")]
        public TransferBalanceQtyModel Put(TransferBalanceQtyModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionId == null)
            {
                value.SessionId = SessionId;
            }
            if (value.TransferTimeId == 1581)
            {
                value.SpecificMonth = null;
            }
            var transferBalanceQty = _context.TransferBalanceQty.SingleOrDefault(p => p.TransferBalanceQtyId == value.TransferBalanceQtyId);
            transferBalanceQty.CustomerId = value.CustomerId;
            transferBalanceQty.ContractId = value.ContractId;
            transferBalanceQty.ProductId = value.ProductId;
            transferBalanceQty.TransferToContractId = value.TransferToContractId;
            transferBalanceQty.SpecificMonth = value.SpecificMonth;
            transferBalanceQty.TransferTimeId = value.TransferTimeId;
            transferBalanceQty.ModifiedDate = DateTime.Now;
            transferBalanceQty.ModifiedByUserId = value.ModifiedByUserID;
            transferBalanceQty.StatusCodeId = value.StatusCodeID.Value;
            transferBalanceQty.SessionId = value.SessionId;
            _context.SaveChanges();
            if (value.ProductId > 0)
            {
                value.ProductName = _context.SocustomersItemCrossReference.Where(s => s.SocustomersItemCrossReferenceId == value.ProductId).Select(s => s.CustomerReferenceNo).FirstOrDefault();
            }
            if (value.TransferToContractId > 0)
            {
                value.TransferToContractNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.TransferToContractId).Select(s => s.OrderNo).FirstOrDefault();
            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.ContractId > 0)
            {
                value.ContractNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.ContractId).Select(s => s.OrderNo).FirstOrDefault();
            }
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTransferBalanceQty")]
        public void Delete(int id)
        {
            var transferBalanceQty = _context.TransferBalanceQty.SingleOrDefault(p => p.TransferBalanceQtyId == id);
            if (transferBalanceQty != null)
            {

                var query = string.Format("delete from TransferBalanceQtyDocument Where SessionId='{0}'", transferBalanceQty.SessionId);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
                _context.TransferBalanceQty.Remove(transferBalanceQty);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("UploadTransferBalanceQtyDocument")]
        public IActionResult Upload(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new TransferBalanceQtyDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.TransferBalanceQtyDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetTransferBalanceQtyDocument")]
        public List<TransferBalanceQtyDocumentModel> GetTransferBalanceQtyDocument(Guid? SessionId)
        {
            var query = _context.TransferBalanceQtyDocument.Select(s => new TransferBalanceQtyDocumentModel
            {
                SessionId = s.SessionId,
                TransferBalanceQtyDocumentId = s.TransferBalanceQtyDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
            }).Where(l => l.SessionId == SessionId).OrderByDescending(o => o.TransferBalanceQtyDocumentId).ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadTransferBalanceQtyDocument")]
        public IActionResult DownLoadMachineDocument(long id)
        {
            var document = _context.TransferBalanceQtyDocument.SingleOrDefault(t => t.TransferBalanceQtyDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteTransferBalanceQtyDocument")]
        public void DeleteTransferBalanceQtyDocument(long? id)
        {
            var query = string.Format("delete from TransferBalanceQtyDocument Where TransferBalanceQtyDocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }

        [HttpGet]
        [Route("GetTransferBalanceQtyAttachment")]
        public List<TransferBalanceQtyDocumentModel> GetBlanketOrderAttachment(int? id)
        {
            var BlanketOrder = _context.TransferBalanceQty.Where(w => w.TransferBalanceQtyId == id).Select(s => s.SessionId).FirstOrDefault();
            var query = _context.TransferBalanceQtyDocument.Select(s => new TransferBalanceQtyDocumentModel
            {
                SessionId = s.SessionId,
                TransferBalanceQtyDocumentId = s.TransferBalanceQtyDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == BlanketOrder).OrderByDescending(o => o.TransferBalanceQtyDocumentId).AsNoTracking().ToList();
            return query;
        }

        [HttpPost]
        [Route("UploadTransferBalanceQtyAttachments")]
        public IActionResult UploadTransferBalanceQtyAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new TransferBalanceQtyDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId
                };

                _context.TransferBalanceQtyDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetTransferBalanceQtyAttachmentBySessionID")]
        public List<DocumentsModel> GetTransferBalanceQtyAttachmentBySessionID(Guid? sessionId, int userId)
        {//
            var query = _context.TransferBalanceQtyDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.TransferBalanceQtyDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadTransferBalanceQtyAttachment")]
        public IActionResult DownLoadTransferBalanceQtyAttachment(long id)
        {
            var document = _context.TransferBalanceQtyDocument.SingleOrDefault(t => t.TransferBalanceQtyDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteTransferBalanceQtyAttachment")]
        public void DeleteTransferBalanceQtyAttachment(int id)
        {
            var query = string.Format("delete from TransferBalanceQtyDocument Where TransferBalanceQtyDocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }


    }
}