﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavStockBalanceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        public NavStockBalanceController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpPost]
        [Route("GetDistMonthlyStockBalance")]
        public List<NavStockBalanceModel> GetDistMonthlyStockBalance(StockBalanceSearch searchModel)
        {
            List<NavStockBalanceModel> navStockBalanceModels = new List<NavStockBalanceModel>();
            //var month = searchModel.StkMonth.Month == 1 ? 12 : searchModel.StkMonth.Month - 1;
            //var year = searchModel.StkMonth.Month == 1 ? searchModel.StkMonth.Year - 1 : searchModel.StkMonth.Year;
            var month = searchModel.StkMonth.Month;
            var year = searchModel.StkMonth.Year;
            var weekofMonth = GetWeekNumberOfMonth(searchModel.StkMonth);
            var distItems = _context.DistStockBalance.Where(d => d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && (d.StockBalWeek == weekofMonth || d.StockBalWeek == null)).AsNoTracking().ToList();
            var distItemsID = distItems.Select(s => s.DistItemId).ToList();
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavStockBalanceModel> navStockBalances = new List<NavStockBalanceModel>();
            var navStockBalance = _context.Acitems.OrderByDescending(o => o.DistAcid).Where(b => distItemsID.Contains(b.DistAcid) && b.StatusCodeId == 1 && b.CompanyId == searchModel.CompanyId).AsNoTracking().ToList();
            navStockBalance.ForEach(s =>
            {

                NavStockBalanceModel navStockBalanceModel = new NavStockBalanceModel
                {
                    AvnavStockBalID = s.DistAcid,
                    StockBalMonth = distItems.FirstOrDefault(f => f.DistItemId == s.DistAcid).StockBalMonth,
                    ItemDecs = s.ItemDesc,
                    RemainingQty = distItems.FirstOrDefault(f => f.DistItemId == s.DistAcid).Quantity,
                    Dist = s.DistName,
                    ItemCode = s.ItemNo,
                    StatusCodeID = s.StatusCodeId,
                    CompanyId = s.CompanyId,
                };
                navStockBalances.Add(navStockBalanceModel);
            });

            var acItemIDs = navStockBalances.Select(c => c.AvnavStockBalID).ToList();
            var navItemCList = _context.NavItemCitemList.Where(c => acItemIDs.Contains(c.NavItemCustomerItemId.Value)).AsNoTracking().ToList();
            var navItems = _context.Navitems.Include("GenericCode").Where(i => navItemCList.Select(c => c.NavItemId).Distinct().Contains(i.ItemId)).AsNoTracking().ToList();

            navStockBalances.ForEach(a =>
            {
                var navCItem = navItemCList.FirstOrDefault(c => c.NavItemCustomerItemId == a.AvnavStockBalID);
                if (navCItem != null)
                {

                    var navItem = navItems.FirstOrDefault(n => n.ItemId == navCItem.NavItemId);
                    var groupName = navItem != null ? navItem.CategoryId.GetValueOrDefault(0) : 0;
                    a.PackSize = navItem != null ? navItem.PackSize.ToString() : string.Empty;
                    a.Packuom = navItem != null ? navItem.PackUom : string.Empty;
                    a.Uom = navItem != null ? navItem.BaseUnitofMeasure : string.Empty;
                    //a.GenericCode = navItem != null ? navItem.GenericCode?.Code : string.Empty;
                    a.SWItemNo = navItem != null ? navItem.No : string.Empty;
                    a.SWDesc = navItem != null ? navItem.Description : string.Empty;
                    a.SWDesc2 = navItem != null ? navItem.Description2 : string.Empty;
                    a.InternalRefNo = navItem != null ? navItem.InternalRef : "";
                    a.ItemGroup = navItem != null ? navItem.ItemCategoryCode : ""; // groupName > 0 ? categoryList[int.Parse(groupName.ToString()) - 1].ToString() : "";
                }
            });


            return navStockBalances;
        }
        // GET: api/Project
        [HttpPost]
        [Route("GetNavMonthlyStockBalance")]
        public List<NavStockBalanceModel> GetNavMonthlyStockBalance(StockBalanceSearch searchModel)
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            //var month = searchModel.StkMonth.Month == 1 ? 12 : searchModel.StkMonth.Month - 1;
            //var year = searchModel.StkMonth.Month == 1 ? searchModel.StkMonth.Year - 1 : searchModel.StkMonth.Year;
            var month = searchModel.StkMonth.Month;
            var year = searchModel.StkMonth.Year;
            var weekofMonth = GetWeekNumberOfMonth(searchModel.StkMonth);

            var distItems = _context.NavitemStockBalance.Where(d => d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && (
            d.StockBalWeek == weekofMonth || d.StockBalWeek == null)).AsNoTracking().ToList();
            var distItemsID = distItems.Select(s => s.ItemId).ToList();
            List<NavStockBalanceModel> navStockBalances = new List<NavStockBalanceModel>();
            var navStockBalance = _context.Navitems.OrderByDescending(o => o.ItemId).Where(i => i.No.StartsWith("FP-") && i.StatusCodeId == 1 && distItemsID.Contains(i.ItemId) && i.CompanyId == searchModel.CompanyId).AsNoTracking().ToList();
            navStockBalance.ForEach(s =>
            {

                NavStockBalanceModel navStockBalanceModel = new NavStockBalanceModel
                {
                    AvnavStockBalID = s.ItemId,
                    StockBalMonth = distItems.FirstOrDefault(f => f.ItemId == s.ItemId).StockBalMonth,
                    ItemDecs = s.Description,
                    SWDesc2 = s.Description2,
                    SWDesc = s.Description,
                    Uom = s.BaseUnitofMeasure,
                    RemainingQty = distItems.FirstOrDefault(f => f.ItemId == s.ItemId).Quantity * s.PackQty,
                    ItemCode = s.No,
                    PackSize = s.PackSize.HasValue ? s.PackSize.ToString() : string.Empty,
                    Packuom = s.PackUom,
                    StatusCodeID = s.StatusCodeId,
                    CompanyId = s.CompanyId,
                    InternalRefNo = s.InternalRef,
                    ItemGroup = s.ItemCategoryCode,

                };
                navStockBalances.Add(navStockBalanceModel);
            });

            return navStockBalances;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", StockBalMonth = "Get NavstockBalanace")]
        [HttpGet("GetDistMonthlyStockBalance/{id:int}")]
        public ActionResult<NavStockBalanceModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var navStockBalance = _context.NavstockBalanace.SingleOrDefault(p => p.AvnavStockBalId == id.Value);
            var result = _mapper.Map<NavStockBalanceModel>(navStockBalance);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NavStockBalanceModel> GetData(SearchModel searchModel)
        {
            var navStockBalance = new NavstockBalanace();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).FirstOrDefault();
                        break;
                    case "Last":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).LastOrDefault();
                        break;
                    case "Next":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).LastOrDefault();
                        break;
                    case "Previous":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).FirstOrDefault();
                        break;
                    case "Last":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).LastOrDefault();
                        break;
                    case "Next":
                        navStockBalance = _context.NavstockBalanace.OrderBy(o => o.AvnavStockBalId).Where(i => i.IsNav == false).FirstOrDefault(s => s.AvnavStockBalId > searchModel.Id);
                        break;
                    case "Previous":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == false).FirstOrDefault(s => s.AvnavStockBalId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NavStockBalanceModel>(navStockBalance);
            return result;
        }

        [HttpPost()]
        [Route("GetDataNavMonthlyStockBalance")]
        public ActionResult<NavStockBalanceModel> GetDataNavMonthlyStockBalance(SearchModel searchModel)
        {
            var navStockBalance = new NavstockBalanace();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).FirstOrDefault();
                        break;
                    case "Last":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).LastOrDefault();
                        break;
                    case "Next":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).LastOrDefault();
                        break;
                    case "Previous":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).FirstOrDefault();
                        break;
                    case "Last":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).LastOrDefault();
                        break;
                    case "Next":
                        navStockBalance = _context.NavstockBalanace.OrderBy(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).FirstOrDefault(s => s.AvnavStockBalId > searchModel.Id);
                        break;
                    case "Previous":
                        navStockBalance = _context.NavstockBalanace.OrderByDescending(o => o.AvnavStockBalId).Where(i => i.IsNav == true || i.IsNav == null).FirstOrDefault(s => s.AvnavStockBalId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NavStockBalanceModel>(navStockBalance);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDistMonthlyStockBalance")]
        public NavStockBalanceModel Post(NavStockBalanceModel value)
        {
            var navStockBalance = new NavstockBalanace
            {
                //AvnavStockBalId = value.AvnavStockBalID,
                StockBalMonth = value.StockBalMonth,
                ItemDecs = value.ItemDecs,
                Uom = value.Uom,
                RemainingQty = value.RemainingQty,
                IsNav = value.IsNav,
                PackSize = int.Parse(value.PackSize),
                Ac = value.Ac,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                //StatusCode=value.StatusCode.Select()
                //StatusCode = value.Status

            };
            _context.NavstockBalanace.Add(navStockBalance);
            _context.SaveChanges();
            value.AvnavStockBalID = navStockBalance.AvnavStockBalId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDistMonthlyStockBalance")]
        public NavStockBalanceModel Put(NavStockBalanceModel value)
        {
            var navStockBalance = _context.NavstockBalanace.SingleOrDefault(p => p.AvnavStockBalId == value.AvnavStockBalID);
            //NavstockBalanace.AvnavStockBalId = value.AvnavStockBalID;

            navStockBalance.StockBalMonth = value.StockBalMonth;
            navStockBalance.ItemDecs = value.ItemDecs;
            navStockBalance.Uom = value.Uom;
            navStockBalance.RemainingQty = value.RemainingQty;
            navStockBalance.IsNav = value.IsNav;
            navStockBalance.PackSize = int.Parse(value.PackSize);
            navStockBalance.Ac = value.Ac;

            navStockBalance.StatusCodeId = value.StatusCodeID.Value;
            navStockBalance.ModifiedByUserId = value.ModifiedByUserID;
            navStockBalance.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavMonthlyStockBalance")]
        public NavStockBalanceModel PutNavMonthlyStockBalance(NavStockBalanceModel value)
        {
            var stkBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
            var navStockBalance = _context.NavstockBalanace.SingleOrDefault(p => p.AvnavStockBalId == value.AvnavStockBalID);
            //NavstockBalanace.AvnavStockBalId = value.AvnavStockBalID;

            navStockBalance.StockBalMonth = value.StockBalMonth;
            navStockBalance.ItemDecs = value.ItemDecs;
            navStockBalance.Uom = value.Uom;
            navStockBalance.RemainingQty = value.RemainingQty;
            navStockBalance.IsNav = value.IsNav;

            navStockBalance.StatusCodeId = value.StatusCodeID.Value;
            navStockBalance.ModifiedByUserId = value.ModifiedByUserID;
            navStockBalance.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDistMonthlyStockBalance")]
        public void Delete(int id)
        {
            var navStockBalance = _context.NavstockBalanace.SingleOrDefault(p => p.AvnavStockBalId == id);
            if (navStockBalance != null)
            {
                _context.NavstockBalanace.Remove(navStockBalance);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("ImportuploadDocuments")]
        public List<HeaderList> ImportuploadDocuments(IFormCollection files)
        {
            var navCustomerIds= files["NavCustomerId"].ToString();
            var navCustomerId = !string.IsNullOrEmpty(navCustomerIds) ? long.Parse(navCustomerIds) : -1;
            var type = files["Type"].ToString();
            var SessionId = Guid.NewGuid();
            var plant = files["plantId"].ToString();
            var user = files["userId"].ToString();
            var stkMonth = files["stkMonth"].ToString();
            long userId = !string.IsNullOrEmpty(user) ? long.Parse(user) : -1;
            var companyId = !string.IsNullOrEmpty(plant) ? long.Parse(plant) : -1;
            List<JsonList> JsonList = new List<JsonList>();
            if (type == "Insert")
            {
                if (!string.IsNullOrWhiteSpace(files["Items"].ToString()))
                {
                    dynamic ItemsList = JsonConvert.DeserializeObject(files["Items"], typeof(List<JsonList>));
                    foreach (var item in ItemsList)
                    {
                        JsonList Json = new JsonList();
                        Json.EntityName = item.EntityName;
                        Json.ColumnId = item.ColumnId;
                        JsonList.Add(Json);
                    }
                }
            }
            List<HeaderList> headerList = new List<HeaderList>();
            ACImportExcelModel ACImportExcelModel = new ACImportExcelModel();
            List<HeaderList> columnPropertyList = new List<HeaderList>
                {
                    new HeaderList{Text="Dist Name",Value="distName"},
                    new HeaderList{Text="Item No",Value="itemNo"},
                    new HeaderList{Text="Description",Value="description"},
                    new HeaderList{Text="Qty On Hand",Value="qtyOnHand"},
                    new HeaderList{Text="Location",Value="location" },
                    new HeaderList{Text="Month",Value="month"},
                    new HeaderList{Text="Stock Bal Date",Value="stockBalDate"},
                };
            var acItems = new List<ACImportModel>();
            var count = 0;
            files.Files.ToList().ForEach(f =>
            {
                if (count == 0)
                {
                    var file = f;
                    var extension = Path.GetExtension(file.FileName);
                    if (extension == ".xlsx")
                    {
                        var fs = file.OpenReadStream();
                        using (var package = new ExcelPackage(fs))
                        {
                            var worksheets = package.Workbook.Worksheets.Select(x => x.Name).ToList();
                            var count = worksheets.Count();
                            if (count > 0)
                            {
                                var oSheet = package.Workbook.Worksheets[worksheets[0]];
                                if (oSheet != null)
                                {
                                    ExcelWorksheet worksheet = oSheet;
                                    var rowCount = worksheet.Dimension.Rows;
                                    for (int i = 1; i <= worksheet.Dimension.End.Column; i++)
                                    {
                                        headerList.Add(new HeaderList
                                        {
                                            Text = worksheet.Cells[1, i].Value.ToString(),
                                            ID = i
                                        });
                                    }
                                    if (JsonList.Count > 0)
                                    {
                                        for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                                        {
                                            ACImportModel ACImportModel = new ACImportModel();
                                            JsonList.ForEach(j =>
                                            {
                                                if (j.EntityName == "distName")
                                                {
                                                    ACImportModel.DistName = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "itemNo")
                                                {
                                                    ACImportModel.ItemNo = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "description")
                                                {
                                                    ACImportModel.Description = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "qtyOnHand")
                                                {
                                                    ACImportModel.QtyOnHand = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "location")
                                                {
                                                    ACImportModel.Location = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "month")
                                                {
                                                    ACImportModel.Month = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                                else if (j.EntityName == "stockBalDate")
                                                {
                                                    ACImportModel.StockBalDate = worksheet.Cells[row, j.ColumnId.Value].Value.ToString().Trim();
                                                }
                                            });
                                            acItems.Add(ACImportModel);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                            }
                        }
                    }
                    else
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!.It  will support only (.xlsx) Extension", null);
                    }
                }
                count++;
            });
            columnPropertyList.ForEach(s =>
            {
                List<HeaderList> headerLists = new List<HeaderList>();
                headerList.ForEach(h =>
                {
                    HeaderList HeaderList = new HeaderList();
                    HeaderList.Text = h.Text;
                    HeaderList.ID = h.ID;
                    HeaderList.Value = s.Value;
                    headerLists.Add(HeaderList);
                });
                s.ColumnList = headerLists;
            });
            //var acItemss = acItems;
            if(type=="Insert")
            {
               // var customers = _context.Navcustomer.AsNoTracking().ToList();
                acItems.ForEach(ac =>
                {
                     long? custId = null;
                    /*var cust = customers.FirstOrDefault(cu => cu.Name == ac.DistName);
                    if (cust != null)
                    {
                        custId = cust.CustomerId;
                    }
                    else
                    {

                    }*/
                    if (navCustomerId > 0)
                    {
                        custId = navCustomerId;
                    }
                     var acMonth = DateTime.Parse(stkMonth);
                    var qty = acItems.Where(q => q.ItemNo == ac.ItemNo && q.DistName == ac.DistName && ac.QtyOnHand!=null).Sum(s => decimal.Parse(s.QtyOnHand));

                    var exist = _context.Acitems.FirstOrDefault(d => d.CustomerId == custId && d.CompanyId == companyId && d.ItemNo == ac.ItemNo);
                    if (exist == null)
                    {
                        var acitem = new Acitems
                        {
                            DistName = ac.DistName,
                            Acqty = qty,
                            ItemDesc = ac.Description,
                            ItemNo = ac.ItemNo,
                            Acmonth = acMonth,
                            StatusCodeId = 2,
                            CompanyId = companyId,
                            CustomerId = custId,
                            AddedDate = DateTime.Now,
                            AddedByUserId = userId,
                            DistStockBalance = new List<DistStockBalance>()
                        };
                        var stockBal = new DistStockBalance
                        {
                            Quantity = qty,
                            StockBalMonth = acMonth,
                            AddedDate = DateTime.Now,
                            AddedByUserId = userId,
                        };
                        stockBal.StockBalWeek = GetWeekNumberOfMonth(stockBal.StockBalMonth.Value);
                        acitem.DistStockBalance.Add(stockBal);
                        _context.Acitems.Add(acitem);
                    }
                    else
                    {
                        //exist.CompanyId = companyId;
                        //exist.CustomerId = custId;
                        //exist.StatusCodeId = 1;
                        exist.ModifiedByUserId = userId;
                        exist.ModifiedDate = DateTime.Now;
                        var stockBalWeek = GetWeekNumberOfMonth(acMonth);
                        var stkbalances = _context.DistStockBalance.Where(fd => fd.DistItemId == exist.DistAcid && fd.StockBalMonth.Value.Month == acMonth.Month && fd.StockBalMonth.Value.Year == acMonth.Year);
                        DistStockBalance stkbalance = null;
                        if (stkbalances.Any(c => c.StockBalWeek == stockBalWeek))
                        {
                            stkbalance = stkbalances.FirstOrDefault(s => s.StockBalWeek == stockBalWeek);
                        }
                        if (stkbalance == null)
                        {
                            exist.DistStockBalance = new List<DistStockBalance>();
                            var stockBal = new DistStockBalance
                            {
                                DistItemId = exist.DistAcid,
                                Quantity = qty,
                                StockBalMonth = acMonth,
                                AddedDate = DateTime.Now,
                                AddedByUserId = userId,
                            };
                            stockBal.StockBalWeek = stockBalWeek;
                            exist.DistStockBalance.Add(stockBal);
                        }
                        else
                        {
                            stkbalance.Quantity = qty;
                            stkbalance.ModifiedDate = DateTime.Now;
                            stkbalance.ModifiedByUserId = userId;
                        }
                    }
                    _context.SaveChanges();
                });
            }
            return columnPropertyList;
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult Upload(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            var plant = files["plantId"].ToString();
            var user = files["userId"].ToString();
            var stkMonth = files["stkMonth"].ToString();
            long userId = !string.IsNullOrEmpty(user) ? long.Parse(user) : -1;
            var companyId = !string.IsNullOrEmpty(plant) ? long.Parse(plant) : -1;
            files.Files.ToList().ForEach(f =>
            {

                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);
                string sheetName = "ACImport";
                var fileName = file.FileName;

                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + fileName;
                System.IO.File.WriteAllBytes(serverPath, document);
                var fileInfo = new FileInfo(serverPath);
                var acItems = new List<ACImportModel>();
                using (var package = new ExcelPackage(fs))
                {
                    var oSheet = package.Workbook.Worksheets[sheetName];
                    if (oSheet != null)
                    {
                        ExcelWorksheet worksheet = oSheet;
                        var rowCount = worksheet.Dimension.Rows;

                        for (int row = 2; row <= rowCount; row++)
                        {
                            if (worksheet.Cells[row, 1].Value != null && worksheet.Cells[row, 2].Value != null && worksheet.Cells[row, 6].Value != null)
                            {
                                acItems.Add(new ACImportModel
                                {
                                    DistName = worksheet.Cells[row, 1].Value.ToString().Trim(),
                                    ItemNo = worksheet.Cells[row, 2].Value.ToString().Trim(),
                                    Description = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                    QtyOnHand = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                    Location = worksheet.Cells[row, 5].Value?.ToString().Trim(),
                                    Month = worksheet.Cells[row, 6].Value.ToString().Trim(),
                                });
                            }
                        }
                    }
                    else
                    {
                        throw new AppException("Please Check Sheet Name or File format/Version!. Excel Export will not support lower versions(.xls).", null);
                    }
                }
                var customers = _context.Navcustomer.AsNoTracking().ToList();
                acItems.ForEach(ac =>
                {
                    long? custId = null;
                    var cust = customers.FirstOrDefault(cu => cu.Name == ac.DistName);
                    if (cust != null)
                    {
                        custId = cust.CustomerId;
                    }
                    else
                    {

                    }
                    var acMonth = DateTime.Parse(stkMonth);
                    var qty = acItems.Where(q => q.ItemNo == ac.ItemNo && q.DistName == ac.DistName).Sum(s => decimal.Parse(s.QtyOnHand));

                    var exist = _context.Acitems.FirstOrDefault(d => d.CustomerId == custId && d.CompanyId == companyId && d.ItemNo == ac.ItemNo);
                    if (exist == null)
                    {
                        var acitem = new Acitems
                        {
                            DistName = ac.DistName,
                            Acqty = qty,
                            ItemDesc = ac.Description,
                            ItemNo = ac.ItemNo,
                            Acmonth = acMonth,
                            StatusCodeId = 2,
                            CompanyId = companyId,
                            CustomerId = custId,
                            AddedDate = DateTime.Now,
                            AddedByUserId = userId,
                            DistStockBalance = new List<DistStockBalance>()
                        };
                        var stockBal = new DistStockBalance
                        {
                            Quantity = qty,
                            StockBalMonth = acMonth,
                            AddedDate = DateTime.Now,
                            AddedByUserId = userId,
                        };
                        stockBal.StockBalWeek = GetWeekNumberOfMonth(stockBal.StockBalMonth.Value);
                        acitem.DistStockBalance.Add(stockBal);
                        _context.Acitems.Add(acitem);
                    }
                    else
                    {
                        //exist.CompanyId = companyId;
                        //exist.CustomerId = custId;
                        //exist.StatusCodeId = 1;
                        exist.ModifiedByUserId = userId;
                        exist.ModifiedDate = DateTime.Now;
                        var stockBalWeek = GetWeekNumberOfMonth(acMonth);
                        var stkbalances = _context.DistStockBalance.Where(fd => fd.DistItemId == exist.DistAcid && fd.StockBalMonth.Value.Month == acMonth.Month && fd.StockBalMonth.Value.Year == acMonth.Year);
                        DistStockBalance stkbalance = null;
                        if (stkbalances.Any(c => c.StockBalWeek == stockBalWeek))
                        {
                            stkbalance = stkbalances.FirstOrDefault(s => s.StockBalWeek == stockBalWeek);
                        }
                        if (stkbalance == null)
                        {
                            exist.DistStockBalance = new List<DistStockBalance>();
                            var stockBal = new DistStockBalance
                            {
                                DistItemId = exist.DistAcid,
                                Quantity = qty,
                                StockBalMonth = acMonth,
                                AddedDate = DateTime.Now,
                                AddedByUserId = userId,
                            };
                            stockBal.StockBalWeek = stockBalWeek;
                            exist.DistStockBalance.Add(stockBal);
                        }
                        else
                        {
                            stkbalance.Quantity = qty;
                            stkbalance.ModifiedDate = DateTime.Now;
                            stkbalance.ModifiedByUserId = userId;
                        }
                    }
                    _context.SaveChanges();
                });

            });
            //_context.SaveChanges();
            return Content(SessionId.ToString());
        }

        [HttpGet]
        [Route("GetNavItemStockBalanceById")]
        public List<NavItemStockBalanceModel> GetNavItemStockBalanceById(int id)
        {
            var navItemStockBalances = _context.NavitemStockBalance.Include(i => i.Item).Select(s => new NavItemStockBalanceModel
            {
                NavStockBalanceId = s.NavStockBalanceId,
                ItemId = s.ItemId,
                ItemName = s.Item != null ? s.Item.No : string.Empty,
                Quantity = s.Quantity,
                RejectQuantity = s.RejectQuantity,
                StockBalMonth = s.StockBalMonth,
                ReworkQty = s.ReworkQty,
                GlobalQty = s.GlobalQty,
                Wipqty = s.Wipqty,
                StockBalWeek = s.StockBalWeek

            }).Where(d => d.ItemId == id).AsNoTracking().ToList();
            return navItemStockBalances.OrderByDescending(o => o.NavStockBalanceId).ToList();
        }

        [HttpPost]
        [Route("SaveNavItemStockBalance")]
        public NavItemStockBalanceModel SaveNavItemStockBalance(NavItemStockBalanceModel value)
        {
            var stkBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
            NavitemStockBalance itemStockBalance = null;
            var navitemStockBalance = _context.NavitemStockBalance.FirstOrDefault(d => d.ItemId == value.ItemId && d.StockBalMonth.Value.Month == value.StockBalMonth.Value.Month && d.StockBalMonth.Value.Year == value.StockBalMonth.Value.Year && d.StockBalWeek == stkBalWeek);
            if (navitemStockBalance == null)
            {
                itemStockBalance = new NavitemStockBalance
                {
                    ItemId = value.ItemId,
                    StockBalMonth = value.StockBalMonth,
                    StockBalWeek = value.StockBalWeek,
                    Quantity = value.Quantity,
                    RejectQuantity = value.RejectQuantity,
                    AddedDate = DateTime.Now,
                    AddedByUserId = value.AddedByUserID,
                    StatusCodeId = 1,
                };
            }
            else
            {
                navitemStockBalance.RejectQuantity = value.RejectQuantity;
                navitemStockBalance.Quantity = value.Quantity;
                navitemStockBalance.StockBalMonth = value.StockBalMonth;
                navitemStockBalance.StockBalWeek = stkBalWeek;
                navitemStockBalance.ModifiedDate = DateTime.Now;
                navitemStockBalance.ModifiedByUserId = value.ModifiedByUserID;
                navitemStockBalance.StatusCodeId = 1;
            }

            _context.NavitemStockBalance.Add(itemStockBalance);
            _context.SaveChanges();
            var navItem = _context.Navitems.FirstOrDefault(a => a.ItemId == itemStockBalance.ItemId);
            value.ItemName = navItem.No;
            value.NavStockBalanceId = itemStockBalance.NavStockBalanceId;
            return value;
        }

        [HttpPut]
        [Route("UpdateNavItemStockBalance")]
        public NavItemStockBalanceModel UpdateNavItemStockBalance(NavItemStockBalanceModel value)
        {
            var stkBalWeek = GetWeekNumberOfMonth(value.StockBalMonth.Value);
            var navitemStockBalance = _context.NavitemStockBalance.FirstOrDefault(d => d.ItemId == value.ItemId && d.StockBalMonth.Value.Month == value.StockBalMonth.Value.Month && d.StockBalMonth.Value.Year == value.StockBalMonth.Value.Year && d.StockBalWeek == stkBalWeek);
            if (navitemStockBalance != null)
            {
                navitemStockBalance.RejectQuantity = value.RejectQuantity;
                navitemStockBalance.Quantity = value.Quantity;
                navitemStockBalance.StockBalMonth = value.StockBalMonth;
                navitemStockBalance.StockBalWeek = stkBalWeek;
                navitemStockBalance.ModifiedDate = DateTime.Now;
                navitemStockBalance.ModifiedByUserId = value.ModifiedByUserID;
                navitemStockBalance.StatusCodeId = 1;
            }
            else
            {
                NavitemStockBalance navItemStockBalance = new NavitemStockBalance
                {
                    ItemId = value.ItemId,
                    RejectQuantity = value.RejectQuantity,
                    Quantity = value.Quantity,
                    StockBalMonth = value.StockBalMonth,
                    AddedDate = DateTime.Now,
                    AddedByUserId = value.AddedByUserID,
                    StatusCodeId = 1,
                };
                navItemStockBalance.StockBalWeek = stkBalWeek;
                _context.NavitemStockBalance.Add(navItemStockBalance);

            }
            _context.SaveChanges();
            value.StockBalWeek = stkBalWeek;
            return value;
        }

        [HttpDelete]
        [Route("DeleteNavItemStockBalance")]
        public void DeleteNavItemStockBalance(int id)
        {
            var navitemStockBalance = _context.NavitemStockBalance.FirstOrDefault(f => f.NavStockBalanceId == id);
            _context.NavitemStockBalance.Remove(navitemStockBalance);
            _context.SaveChanges();
        }

        private int GetWeekNumberOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDay = (int)firstDayOfMonth.DayOfWeek;
            if (firstDay == 0)
            {
                firstDay = 7;
            }
            double d = (firstDay + date.Day - 1) / 7.0;
            return d > 5 ? (int)Math.Floor(d) : (int)Math.Ceiling(d);
        }
    }
}