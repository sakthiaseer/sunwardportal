﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductGroupingNavController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductGroupingNavController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetProductGroupingNavItems")]
        public List<ProductGroupingNavModel> GetProductGroupingNavItems(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProductGroupingNavModel> productGroupingNavModels = new List<ProductGroupingNavModel>();
            var productGroupingNavs = _context.ProductGroupingNav.Include("AddedByUser").Include(p => p.GenericCodeSupplyToMultiple).Include(i => i.Item).Include("ModifiedByUser").Where(p => p.ProductGroupingManufactureId == id).OrderByDescending(o => o.ProductGroupingNavId).AsNoTracking().ToList();
            if (productGroupingNavs != null && productGroupingNavs.Count > 0)
            {
                productGroupingNavs.ForEach(s =>
                {
                    ProductGroupingNavModel productGroupingNavModel = new ProductGroupingNavModel();

                    productGroupingNavModel.ProductGroupingManufactureId = s.ProductGroupingManufactureId;
                    productGroupingNavModel.ProductGroupingNavId = s.ProductGroupingNavId;
                    productGroupingNavModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                    productGroupingNavModel.GenericCodeSupplyToMultiple = s.GenericCodeSupplyToMultiple?.GenericCodeSupplyDescription;
                    productGroupingNavModel.ItemId = s.ItemId;
                    productGroupingNavModel.NavNo = s.Item?.No;
                    productGroupingNavModel.VarianceNo = s.VarianceNo;
                    productGroupingNavModel.ManufactureBy = s.Item?.ItemCategoryCode.Replace("FP", "").Trim();
                    productGroupingNavModel.ReplenishmentMethod = s.Item?.VendorNo;
                    productGroupingNavModel.Description = s.Item?.Description;
                    productGroupingNavModel.ManufactureFor = s.Item?.InternalRef;
                    productGroupingNavModel.UOM = s.Item?.BaseUnitofMeasure;
                    productGroupingNavModel.AddedByUserID = s.AddedByUserId;
                    productGroupingNavModel.ModifiedByUserID = s.ModifiedByUserId;
                    productGroupingNavModel.StatusCodeID = s.StatusCodeId;
                    productGroupingNavModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    productGroupingNavModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    productGroupingNavModel.AddedDate = s.AddedDate;
                    productGroupingNavModel.ModifiedDate = s.ModifiedDate;
                    productGroupingNavModels.Add(productGroupingNavModel);


                });
            }

            return productGroupingNavModels;
        }


        [HttpGet]
        [Route("GetProductGroupingNavItemsByGenericID")]
        public List<ProductGroupingNavModel> GetProductGroupingNavItemsByGenericID(int id)
        {
            List<ProductGroupingNavModel> productGroupingNavModels = new List<ProductGroupingNavModel>();
            var productGroupingManufactureIds = _context.ProductGroupingManufacture.Where(d => d.ProductGroupingId == id).Select(m => m.ProductGroupingManufactureId).ToList();
            if (productGroupingManufactureIds.Any())
            {
                var productGroupingNavs = _context.ProductGroupingNav.Include(i=>i.Item).Where(p => productGroupingManufactureIds.Contains(p.ProductGroupingManufactureId.Value) && p.Item.VendorNo=="Purchase").OrderByDescending(o => o.ProductGroupingNavId).AsNoTracking().ToList();
                if (productGroupingNavs != null && productGroupingNavs.Count > 0)
                {
                    productGroupingNavs.ForEach(s =>
                    {
                        ProductGroupingNavModel productGroupingNavModel = new ProductGroupingNavModel();
                        productGroupingNavModel.ProductGroupingManufactureId = s.ProductGroupingManufactureId;
                        productGroupingNavModel.ProductGroupingNavId = s.ProductGroupingNavId;
                        productGroupingNavModel.ItemId = s.ItemId;
                        productGroupingNavModel.NavNo = s.Item?.No;
                        productGroupingNavModel.Description = s.Item?.Description;
                        productGroupingNavModel.Description = s.Item?.Description2;
                        productGroupingNavModel.InternalRef = s.Item?.InternalRef;
                        productGroupingNavModel.UOM = s.Item?.BaseUnitofMeasure;
                        productGroupingNavModel.GenericCodeSupplyToMultipleId = s.GenericCodeSupplyToMultipleId;
                        productGroupingNavModels.Add(productGroupingNavModel);
                    });
                }
            }

            return productGroupingNavModels;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertProductGroupingNav")]
        public ProductGroupingNavModel Post(ProductGroupingNavModel value)
        {
            var productGroupingNav = new ProductGroupingNav
            {
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ProductGroupingManufactureId = value.ProductGroupingManufactureId,
                ItemId = value.ItemId,
                VarianceNo = value.VarianceNo,
                ReplenishmentMethodId = value.ReplenishmentMethodId,
                GenericCodeSupplyToMultipleId = value.GenericCodeSupplyToMultipleId
            };
            _context.ProductGroupingNav.Add(productGroupingNav);
            _context.SaveChanges();
            value.ProductGroupingNavId = productGroupingNav.ProductGroupingNavId;
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateProductGroupingNav")]
        public ProductGroupingNavModel Put(ProductGroupingNavModel value)
        {
            var productGroupingNav = _context.ProductGroupingNav.SingleOrDefault(p => p.ProductGroupingNavId == value.ProductGroupingNavId);
            productGroupingNav.ModifiedByUserId = value.ModifiedByUserID;
            productGroupingNav.ModifiedDate = DateTime.Now;
            productGroupingNav.StatusCodeId = value.StatusCodeID.Value;
            productGroupingNav.ProductGroupingManufactureId = value.ProductGroupingManufactureId;
            productGroupingNav.ItemId = value.ItemId;
            productGroupingNav.VarianceNo = value.VarianceNo;
            productGroupingNav.ReplenishmentMethodId = value.ReplenishmentMethodId;
            productGroupingNav.GenericCodeSupplyToMultipleId = value.GenericCodeSupplyToMultipleId;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteProductGroupingNav")]
        public void Delete(int id)
        {
            try
            {
                var ProductGroupingNav = _context.ProductGroupingNav.SingleOrDefault(p => p.ProductGroupingNavId == id);
                if (ProductGroupingNav != null)
                {
                    _context.ProductGroupingNav.Remove(ProductGroupingNav);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Record Cannot be delete! Reference To Others", ex);
            }
        }
    }
}
