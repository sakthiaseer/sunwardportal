﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskInviteUserController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly MailService _mailService;

        public TaskInviteUserController(CRT_TMSContext context, IMapper mapper, MailService mailService)
        {
            _context = context;
            _mapper = mapper;
            _mailService = mailService;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskInviteUser")]
        public ActionResult<TaskInviteUserModel> Get(int id, int userId)
        {
            TaskInviteUserModel taskInviteUserModel = new TaskInviteUserModel();
            taskInviteUserModel.InviteUserIds = _context.TaskInviteUser.Where(t => t.TaskId == id).Select(s => s.InviteUserId).ToList();
            taskInviteUserModel.TaskId = id;
            var invite = _context.TaskInviteUser.Where(t => t.TaskId == id && t.InviteUserId == userId).FirstOrDefault();
            if(invite!=null)
            {
                taskInviteUserModel.TaskInviteUserId = invite.TaskInviteUserId;
            }
            return taskInviteUserModel;


        }
        [HttpGet]
        [Route("CheckTaskInviteUser")]
        public bool CheckTaskInviteUser(int id, long userId)
        {
            bool _result = false;
            try
            {
                var inviteuser = _context.TaskInviteUser.SingleOrDefault(p => p.TaskId == id && p.InviteUserId == userId);
                if (inviteuser != null)
                {                   
                        _result = true;           
                  
                }
                else
                {
                    _result = false;
                }

                return _result;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }



        // POST: api/User
        [HttpPost]
        [Route("InsertTaskInviteUser")]
        public TaskInviteUserModel Post(TaskInviteUserModel value)
        {
            if (value.InviteUserIds.Count > 0)
            {
                var inviteuser = _context.TaskInviteUser.Where(w => w.TaskId == value.TaskId).ToList();
                if (inviteuser.Count>0)
                {
                    _context.TaskInviteUser.RemoveRange(inviteuser);
                    _context.SaveChanges();
                }
                value.InviteUserIds.ForEach(i =>
                {
               
                var taskInviteUser = _context.TaskInviteUser.Where(p => p.TaskId == value.TaskId && p.InviteUserId == i).FirstOrDefault();
                if (taskInviteUser == null)
                {
                    var taskInvite = new TaskInviteUser
                    {

                        TaskId = value.TaskId,
                        InviteUserId = i,
                        DueDate = value.DueDate,
                        InvitedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeId,
                        IsRead = value.IsRead,

                    };
                    _context.TaskInviteUser.Add(taskInvite);
                    _context.SaveChanges();
                   
                }
                });
                TaskCommentModel taskCommentModel = new TaskCommentModel();
                taskCommentModel.TaskCommentID = value.TaskCommentID.Value;
                taskCommentModel.AddedByUser = value.AddedByUser;
                taskCommentModel.TaskId = value.TaskId;
                taskCommentModel.TaskMasterID = value.TaskId;
                taskCommentModel.Baseurl = value.Baseurl;
                var taskComment = _context.TaskComment.FirstOrDefault(f => f.TaskMasterId == value.TaskId);
                taskCommentModel.ParentCommentId = taskComment?.ParentCommentId;
                taskCommentModel.TaskSubject= taskComment?.TaskSubject;
                taskCommentModel.MessageAssignedToIds = value.InviteUserIds;
                //SendEmailInviteMessage(taskCommentModel);
            }
            return value;

        }
        private void SendEmailInviteMessage(TaskCommentModel value)
        {
            var TaskComment = _context.TaskComment.Where(w => w.TaskCommentId == value.ParentCommentId).FirstOrDefault();
            var tasMaster = _context.TaskMaster.Where(w => w.TaskId == value.TaskMasterID).FirstOrDefault();
            List<long> users = new List<long>();
            users = users.Distinct().ToList();
            var sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", value.MessageAssignedToIds.Distinct().ToList()) + ")";
            var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            var employee = result.Select(s => new
            {
                s.Status,
                s.EmployeeID,
                s.FirstName,
                s.LastName,
                s.NickName,
                s.Gender,
                s.DepartmentName,
                s.PlantID,
                UserId = s.UserID,
                s.Email
            }).Where(w => w.Email != null).ToList();
            MailServiceModel mailServiceModel = new MailServiceModel();
            var title = "Task - [" + value.TaskSubject + "/" + tasMaster?.Title + "] ";
            if (value.ParentCommentId == null)
            {

            }
            else
            {
                title = "Task - [" + TaskComment?.TaskSubject + "/" + tasMaster?.Title + "] ";
            }
            var message = "";
            message += "Please be informed that you have received a unread subject commented by " + value.AddedByUser + " in portal: <br><br>";
            message += "<table>";
            message += "<tr><td>Task name:</td><td>" + tasMaster?.Title + "</td></tr>";
            var url = value.Baseurl + "unReadSubject?id=" + tasMaster?.MainTaskId + "&taskid=" + tasMaster?.TaskId + "&taskCommentID=" + value.TaskCommentID + "&type=Dashboard&goBackframe=back";
            if (value.ParentCommentId == null)
            {
                message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + value.TaskSubject + "</a></td></tr>";
            }
            else
            {
                if (TaskComment != null)
                {
                    message += "<tr><td>Task subject:</td><td><a href=" + url + ">" + TaskComment.TaskSubject + "</a></td></tr>";
                }
            }


            message += "<tr><td>Subject sent date & time: </td><td>" + DateTime.Now.ToString("dd/MMM/yyyy hh:mm tt") + "</td></tr>";
            if (tasMaster.IsNoDueDate == true)
            {
                message += "<tr><td>Subject due date:</td><td></td></tr>";
            }
            else
            {
                message += "<tr><td>Subject due date:</td><td>" + (value.DueDate != null ? value.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
            }

            message += "<tr><td>Message:</td><td>" + value.Comment;
            message += "</table>";
            mailServiceModel.Subject = title;
            mailServiceModel.Body = message;
            List<MailHeaderModel> mailHeaderModels = new List<MailHeaderModel>();
            mailHeaderModels.AddRange(employee.Where(w => value.MessageAssignedToIds.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
            { Email = s.Email, Name = s.FirstName }).ToList());
            mailServiceModel.ToMail = mailHeaderModels;
            _mailService.SendEmailService(mailServiceModel);
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskInviteUser")]
        public TaskInviteUserModel Put(TaskInviteUserModel value)
        {
            var TaskInviteUser = _context.TaskInviteUser.SingleOrDefault(p => p.TaskInviteUserId == value.TaskInviteUserId);           
            TaskInviteUser.TaskId = value.TaskId;
            TaskInviteUser.InviteUserId = value.InviteUserId;
            TaskInviteUser.DueDate = value.DueDate;
            TaskInviteUser.InvitedDate = DateTime.Now;
            TaskInviteUser.IsRead = value.IsRead;          
            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("DeleteTaskInviteUser")]
        public void DeleteTaskInviteUser(TaskInviteUserModel value)
        {
            var inviteuser = _context.TaskInviteUser.Where(w => w.TaskId == value.TaskId && w.InviteUserId == value.InviteUserId)?.FirstOrDefault();
            if (inviteuser!=null)
            {
                _context.TaskInviteUser.Remove(inviteuser);
                _context.SaveChanges();
            }

        }
    }
}
