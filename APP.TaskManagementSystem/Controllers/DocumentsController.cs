﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using DinkToPdf;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.IO.Packaging;
using System.Linq;
using System.Xml.Linq;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using MsgReader.Outlook;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public DocumentsController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDocuments")]
        public List<DocumentsModel> Get()
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var documents = _context.Documents.Include(s => s.StatusCode).Select(s => new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.AddedDate,
                s.DepartmentId,
                s.WikiId,
                s.Extension,
                s.CategoryId,
                s.DocumentType,
                s.DisplayName,
                s.LinkId,
                s.IsSpecialFile,
                s.IsTemp,
                s.ReferenceNumber,
                s.Description,
                s.StatusCodeId,
                s.IsCompressed,
                s.IsVideoFile
            }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.DocumentId,
                    DepartmentID = s.DepartmentId,
                    WikiID = s.WikiId,
                    FileName = s.FileName,
                    Extension = s.Extension,
                    ContentType = s.ContentType,
                    CategoryID = s.CategoryId,
                    DocumentType = s.DocumentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    DisplayName = s.DisplayName,
                    SessionID = s.SessionId,
                    LinkID = s.LinkId,
                    IsSpecialFile = s.IsSpecialFile,
                    IsTemp = s.IsTemp,
                    ReferenceNumber = s.ReferenceNumber,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = "Active",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    IsCompressed = s.IsCompressed,
                    IsVideoFile = s.IsVideoFile,
                };
                documentsModel.Add(documentsModels);
            });
            return documentsModel.OrderByDescending(a => a.DocumentID).Where(w => w.IsTemp == false).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetDocumentsFileSize")]
        public List<DocumentsModel> GetDocumentsFileSize()
        {
            var documents = _context.Documents.Select(s => new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.FolderId,
            }).Where(w => (w.FileSize == null || w.FileSize <= 0) || w.FileName.Contains(".ai") && !w.ContentType.Contains("video")).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
            var documentFolders = _context.DocumentFolder.Select(s => new { s.FolderId, s.DocumentId }).Where(w => w.FolderId > 0).ToList();
            var foldersListItems = _context.Folders.AsNoTracking().ToList();
            documents.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel();
                documentsModels.DocumentID = s.DocumentId;
                documentsModels.FileName = s.FileName;
                documentsModels.FileSize = s.FileSize;
                documentsModels.ContentType = s.ContentType;
                documentsModels.FilterProfileTypeId = s.FilterProfileTypeId;
                documentsModels.FolderId = s.FolderId;
                documentsModels.SessionId = s.SessionId;
                List<string> FolderPathLists = new List<string>();
                if (s.FilterProfileTypeId != null)
                {
                    var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                    if (fileProfileTypeData != null)
                    {
                        FolderPathLists = GetAllFileProfileTypeName(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                        FolderPathLists.Add(fileProfileTypeData.Name);
                        documentsModels.Type = "File Profile Type";
                        documentsModels.FileProfileTypeName = string.Join(" / ", FolderPathLists);
                    }
                }
                if (s.FolderId != null)
                {
                    //var folderId = documentFolders.Where(w => w.DocumentId == s.DocumentId).FirstOrDefault()?.FolderId;
                    var folderData = foldersListItems.Where(w => w.FolderId == s.FolderId).FirstOrDefault();
                    if (folderData != null)
                    {
                        FolderPathLists = GetAllFolderName(foldersListItems, folderData, FolderPathLists);
                        FolderPathLists.Add(folderData.Name);
                        documentsModels.Type = "Public Folder";
                        documentsModels.FileProfileTypeName = string.Join(" / ", FolderPathLists);
                    }
                }
                documentsModel.Add(documentsModels);
            });
            return documentsModel.OrderByDescending(a => a.DocumentID).ToList();
        }
        private List<string> GetAllFolderName(List<Folders> foldersListItems, Folders s, List<string> foldersModel)
        {
            if (s.ParentFolderId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderId == s.ParentFolderId);
                GetAllFolderName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        private List<string> GetAllFileProfileTypeName(List<FileProfileType> foldersListItems, FileProfileType s, List<string> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        [HttpPost]
        [Route("UploadDocumentsSize")]
        public IActionResult UploadDocumentsSize(IFormCollection files)
        {
            var documentId = Convert.ToInt32(files["documentId"].ToString());
            var userId = Convert.ToInt32(files["userID"].ToString());
            var documents = _context.Documents.SingleOrDefault(p => p.DocumentId == documentId);
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
                //var sessionId = files["sessionId"].ToString();
                files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + documents.SessionId;

                if (!System.IO.Directory.Exists(serverPaths))
                {
                    System.IO.Directory.CreateDirectory(serverPaths);
                }
                var ext = "";
                ext = f.FileName;
                int i = ext.LastIndexOf('.');
                var isVideoFiles = false;
                var contentType = f.ContentType.Split("/");
                if (contentType[0] == "video")
                {
                    isVideoFiles = true;
                }
                string[] split = f.FileName.Split('.');
                var serverPath = serverPaths + @"\" + documents.SessionId + '.' + split.Last();
                var filePath = getNextFileName(serverPath);
                var newFile = filePath.Replace(serverPaths + @"\", "");
                using (var targetStream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(targetStream);
                    targetStream.Flush();
                }
                documents.FileSize = file.Length;
                documents.FileName = file.FileName;
                documents.ContentType = file.ContentType;
                documents.FileData = null;
                documents.ModifiedByUserId = userId;
                documents.ModifiedDate = DateTime.Now;
                documents.IsVideoFile = isVideoFiles;
                documents.FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", "");
                _context.SaveChanges();
            });
            
            return Content(files["documentId"].ToString());

        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPublicFolders")]
        public List<DocumentsModel> GetPublicFolders(int? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var documents = _context.Documents.Include(s => s.StatusCode)
                .Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.IsCompressed,
                    s.IsVideoFile,
                }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.DocumentId,
                    DepartmentID = s.DepartmentId,
                    WikiID = s.WikiId,
                    FileName = s.FileName,
                    Extension = s.Extension,
                    ContentType = s.ContentType,
                    CategoryID = s.CategoryId,
                    DocumentType = s.DocumentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    DisplayName = s.DisplayName,
                    SessionID = s.SessionId,
                    LinkID = s.LinkId,
                    IsSpecialFile = s.IsSpecialFile,
                    IsTemp = s.IsTemp,
                    IsCompressed = s.IsCompressed,
                    //Changes Done by Aravinth
                    ReferenceNumber = s.ReferenceNumber,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = "Active",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsVideoFile = s.IsVideoFile
                };
                documentsModel.Add(documentsModels);
            });
            return documentsModel.OrderByDescending(a => a.DocumentID).Where(w => w.AddedByUserID == id).ToList();
        }


        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Documents")]
        [HttpGet("GetDocuments/{id:int}")]
        public ActionResult<DocumentsModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var documents = _context.Documents.SingleOrDefault(p => p.DocumentId == id.Value);
            var result = _mapper.Map<DocumentsModel>(documents);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpGet("AttachmentDownload")]
        public ActionResult<DocumentsModel> AttachmentDownload(long? id, long? userId)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var documents = _context.Documents.Include(f => f.FilterProfileType).Include(d => d.FilterProfileType.DocumentUserRole).Include(s => s.StatusCode).Where(w => w.DocumentId == id)
                .Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.IsCompressed,
                    FilterProfileType = s.FilterProfileType,
                    s.IsVideoFile,
                    s.FilePath
                }).AsNoTracking().SingleOrDefault();
            DocumentsModel documentsModel = new DocumentsModel();
            if (documents != null)
            {
                var s = documents;
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.DocumentId,
                    DepartmentID = s.DepartmentId,
                    WikiID = s.WikiId,
                    FileName = s.FileName,
                    Extension = s.Extension,
                    ContentType = s.ContentType,
                    CategoryID = s.CategoryId,
                    DocumentType = s.DocumentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    DisplayName = s.DisplayName,
                    SessionID = s.SessionId,
                    LinkID = s.LinkId,
                    IsSpecialFile = s.IsSpecialFile,
                    IsTemp = s.IsTemp,
                    ReferenceNumber = s.ReferenceNumber,
                    Description = s.Description,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = "Active",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    IsCompressed = s.IsCompressed,
                    FilePath = s.FilePath,
                    Type = "Document",
                    CanDownload = s.AddedByUserId == userId ? true : (s.FilterProfileType != null && s.FilterProfileType.DocumentUserRole != null ? (s.FilterProfileType.DocumentUserRole.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId && w.UserId == userId).Count() > 0 ? true : false) : false),
                    IsVideoFile = s.IsVideoFile
                };
                if (s.SessionId != null)
                {
                    var appwiki = _context.ApplicationWiki.Where(w => w.SessionId == s.SessionId).FirstOrDefault();
                    if (appwiki != null)
                    {
                        documentsModels.WikiTitle = appwiki.Title;
                        documentsModels.WikiType = appwiki.WikiTypeId != null ? _context.ApplicationMasterDetail.FirstOrDefault(w => w.ApplicationMasterDetailId == appwiki.WikiTypeId).Value : string.Empty;
                        documentsModels.WikiOwner = appwiki.WikiOwnerId != null ? _context.ApplicationMasterDetail.FirstOrDefault(w => w.ApplicationMasterDetailId == appwiki.WikiOwnerId).Value : string.Empty;
                    }
                }
                documentsModel = documentsModels;
            }
            return documentsModel;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DocumentsModel> GetData(SearchModel searchModel)
        {
            var documents = new Documents();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).FirstOrDefault();
                        break;
                    case "Last":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).LastOrDefault();
                        break;
                    case "Next":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).LastOrDefault();
                        break;
                    case "Previous":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).FirstOrDefault();
                        break;
                    case "Last":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).LastOrDefault();
                        break;
                    case "Next":
                        documents = _context.Documents.OrderBy(o => o.DocumentId).FirstOrDefault(s => s.DocumentId > searchModel.Id);
                        break;
                    case "Previous":
                        documents = _context.Documents.OrderByDescending(o => o.DocumentId).FirstOrDefault(s => s.DocumentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DocumentsModel>(documents);
            return result;
        }

        [HttpGet]
        [Route("GetSubPublicFolderId")]
        public List<DocumentsModel> GetSubPublicFolderId(int? id, int userId)
        {

            var userdocumentId = _context.Documents.Where(t => t.AddedByUserId == userId || t.ModifiedByUserId == userId).AsNoTracking().Select(s => s.DocumentId).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var documents = _context.Documents.Include(s => s.StatusCode)
                .Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.IsCompressed,
                    s.IsVideoFile
                }).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.DocumentId,
                    DepartmentID = s.DepartmentId,
                    WikiID = s.WikiId,
                    CategoryID = s.CategoryId,
                    FileName = s.FileName,
                    DisplayName = s.DisplayName,
                    Extension = s.Extension,
                    ContentType = s.ContentType,
                    DocumentType = s.DocumentType,
                    //FileData=value.file
                    //FileData = value.FileData,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = DateTime.Now,
                    SessionID = s.SessionId,
                    LinkID = s.LinkId,
                    IsSpecialFile = s.IsSpecialFile,
                    IsTemp = false,
                    IsCompressed = s.IsCompressed,
                    //Changes Done by Aravinth
                    ReferenceNumber = s.ReferenceNumber,
                    Description = s.Description,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : "",
                    ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    Children = new List<DocumentsModel>(),
                    IsLocked = s.IsLocked,
                    IsVideoFile = s.IsVideoFile
                };
                documentsModel.Add(documentsModels);
            });
            return documentsModel.OrderByDescending(a => a.DocumentID).Where(w => w.DocumentID == id).ToList();
        }

        [HttpGet]
        [Route("GetPublicFolderId")]
        public List<DocumentsModel> GetPublicFolderId(int? id, int userId)
        {
            var documenttree = _context.Documents
             .OrderByDescending(o => o.DocumentId).Where(i => i.DocumentId == id)
             .Select(s => new
             {
                 s.DepartmentId,
                 s.DocumentId,
                 s.WikiId,
                 s.CategoryId,
                 s.FileName,
                 s.DisplayName,
                 s.Extension,
                 s.ContentType,
                 s.DocumentType,
                 s.FileSize,
                 s.SessionId,
                 s.LinkId,
                 s.IsSpecialFile,
                 s.Description,
                 s.ReferenceNumber,
                 s.IsCompressed,
                 s.IsVideoFile
             }).AsNoTracking().ToList();

            //var documenttree = _context.Documents
            //.OrderByDescending(o => o.TaskId).Where(i => i.MainTaskId == id).ToList();

            var lookup = documenttree.ToLookup(x => x.DepartmentId);

            Func<long?, List<DocumentsModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new DocumentsModel()
                    {
                        DocumentID = x.DocumentId,
                        DepartmentID = x.DocumentId,
                        WikiID = x.WikiId,
                        CategoryID = x.CategoryId,
                        FileName = x.FileName,
                        DisplayName = x.DisplayName,
                        Extension = x.Extension,
                        ContentType = x.ContentType,
                        DocumentType = x.DocumentType,
                        FileSize = (long)Math.Round(Convert.ToDouble(x.FileSize / 1024)),
                        UploadDate = DateTime.Now,
                        SessionID = x.SessionId,
                        LinkID = x.LinkId,
                        IsSpecialFile = x.IsSpecialFile,
                        IsTemp = false,
                        ReferenceNumber = x.ReferenceNumber,
                        Description = x.Description,
                        Children = build(x.DocumentId),
                        IsCompressed = x.IsCompressed,
                        IsVideoFile = x.IsVideoFile
                    })
                    .ToList();
            var nestedlist = build(null).ToList();



            //List<TaskMasterModel> allPageComponents = nestedlist.SelectMany(wps => wps.Children).ToList();

            return nestedlist;
        }

        // POST: api/Documents
        [HttpPost]
        [Route("InsertDocuments")]
        public DocumentsModel Post(DocumentsModel value)
        {
            var documents = new Documents
            {
                //DocumentId = value.DocumentID,
                DepartmentId = value.DepartmentID,
                WikiId = value.WikiID,
                CategoryId = value.CategoryID,
                FileName = value.FileName,
                DisplayName = value.DisplayName,
                Extension = value.Extension,
                ContentType = value.ContentType,
                DocumentType = value.DocumentType,
                //FileData=value.file
                //FileData = value.FileData,
                FileSize = value.FileSize,
                UploadDate = DateTime.Now,
                SessionId = value.SessionID,
                LinkId = value.LinkID,
                IsSpecialFile = value.IsSpecialFile,
                IsTemp = false,
                IsCompressed = true,
                //Changes Done by Aravinth
                ReferenceNumber = value.ReferenceNumber,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                IsVideoFile = value.IsVideoFile

            };
            //var file = value.UploadedFile.Files.GetFile("File");
            //var fs = file.OpenReadStream();
            //var br = new BinaryReader(fs);
            //Byte[] document = br.ReadBytes((Int32)fs.Length);
            //documents.FileData = document;

            _context.Documents.Add(documents);
            _context.SaveChanges();
            value.DocumentID = documents.DocumentId;
            if (value.UserID.Count() > 0)
            {

                value.UserID.ForEach(u =>
                {
                    var access = new DocumentRights
                    {
                        DocumentId = value.DocumentID,
                        UserId = u
                    };
                    _context.DocumentRights.Add(access);

                });
                _context.SaveChanges();
            }
            return value;
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("UpdateDocuments")]
        public DocumentsModel Put(DocumentsModel value)
        {
            var documents = _context.Documents.SingleOrDefault(p => p.SessionId == value.SessionID);
            documents.DepartmentId = value.DepartmentID;
            documents.WikiId = value.WikiID;
            documents.CategoryId = value.CategoryID;
            documents.FileName = value.FileName;
            documents.DisplayName = value.DisplayName;
            documents.Extension = value.Extension;
            documents.ContentType = value.ContentType;
            documents.DocumentType = value.DocumentType;
            documents.FileSize = value.FileSize;
            documents.UploadDate = DateTime.Now;
            documents.LinkId = value.LinkID;
            documents.SessionId = value.SessionID;
            documents.IsSpecialFile = value.IsSpecialFile;
            documents.IsTemp = true;
            documents.ExpiryDate = value.ExpiryDate;

            //Changes Done by Aravinth
            documents.ReferenceNumber = value.ReferenceNumber;
            documents.Description = value.Description;
            documents.AddedByUserId = value.AddedByUserID;
            documents.AddedDate = DateTime.Now;
            documents.ModifiedByUserId = value.ModifiedByUserID;
            documents.StatusCodeId = value.StatusCodeID.Value;
            documents.ModifiedDate = DateTime.Now;
            documents.IsVideoFile = value.IsVideoFile;

            if (value.UserID.Count() > 0)
            {
                var accessDoc = _context.DocumentRights.Where(d => d.DocumentId == documents.DocumentId).ToList();
                if (accessDoc.Count > 0)
                {
                    _context.DocumentRights.RemoveRange(accessDoc);
                    _context.SaveChanges();
                }
                value.UserID.ForEach(u =>
                {
                    var access = new DocumentRights
                    {
                        DocumentId = documents.DocumentId,
                        UserId = u
                    };
                    _context.DocumentRights.Add(access);

                });
            }
            _context.SaveChanges();
            value.DocumentID = documents.DocumentId;
            return value;
        }

        // DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        [Route("DeleteDocuments")]
        public void Delete(int id)
        {
            try
            {
                var docAccess = _context.DocumentRights.Where(d => d.DocumentId == id).ToList();
                _context.DocumentRights.RemoveRange(docAccess);

                var docper = _context.DocumentPermission.Where(d => d.DocumentId == id).ToList();
                _context.DocumentPermission.RemoveRange(docper);

                var taskatt = _context.TaskAttachment.Where(d => d.DocumentId == id).ToList();
                _context.TaskAttachment.RemoveRange(taskatt);

                var commentAttachments = _context.CommentAttachment.Where(d => d.DocumentId == id).ToList();
                if (commentAttachments.Count > 0)
                {
                    _context.CommentAttachment.RemoveRange(commentAttachments);
                    _context.SaveChanges();
                }

                // commented due to live error  by Arun

                var docUserRolefolder = _context.DocumentUserRole.Where(d => d.DocumentId == id).ToList();
                if (docUserRolefolder.Count > 0)
                {
                    _context.DocumentUserRole.RemoveRange(docUserRolefolder);
                    _context.SaveChanges();
                }

                // 
                var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DocumentId == id);
                if (folderDiscussion != null)
                {
                    _context.FolderDiscussion.Remove(folderDiscussion);
                    _context.SaveChanges();
                }
                var documents = _context.Documents.Where(d => d.DocumentId == id)?.FirstOrDefault()?.TaskId;
                if (documents == null)
                {
                    var docfolder = _context.DocumentFolder.Where(d => d.DocumentId == id).ToList();
                    if (docfolder.Count > 0)
                    {
                        _context.DocumentFolder.RemoveRange(docfolder);
                        _context.SaveChanges();
                    }
                }
                else if (documents != null)
                {

                    throw new AppException("Task Created this file Cannot Delete");

                }
                //_context.SaveChanges();
                //var documents = _context.Documents.SingleOrDefault(p => p.DocumentId == id);
                //if (documents != null)
                //{
                //    _context.Documents.Remove(documents);
                //    _context.SaveChanges();
                //}

                var query = string.Format("delete from Documents Where DocumentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }

                //
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }

        [HttpDelete]
        [Route("DeletePortfolioDocument")]
        public void DeletePortfolioDocument(int id)
        {
            try
            {


                var portfolioAttachment = _context.PortfolioAttachment.SingleOrDefault(p => p.DocumentId == id);
                if (portfolioAttachment != null)
                {
                    _context.PortfolioAttachment.Remove(portfolioAttachment);
                    _context.SaveChanges();
                }

                var documents = _context.Documents.Where(d => d.DocumentId == id)?.FirstOrDefault();

                var query = string.Format("delete from Documents Where DocumentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }

                //
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPut]
        [Route("DeleteWikiDocuments")]
        public void DeleteWikiDocuments(DocumentsModel value)
        {
            try
            {
                var documents = _context.Documents.Where(d => d.DocumentId == value.DocumentID)?.FirstOrDefault();
                if (documents != null)
                {

                    documents.IsWikiDraftDelete = true;
                    documents.IsLatest = false;
                    _context.SaveChanges();

                }
                var linkDocuments = _context.LinkFileProfileTypeDocument.Where(d => d.TransactionSessionId == value.SessionID && d.DocumentId == value.DocumentID)?.FirstOrDefault();
                if (linkDocuments != null)
                {
                    _context.LinkFileProfileTypeDocument.Remove(linkDocuments);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }

        [HttpPut]
        [Route("DeleteTaskDocuments")]
        public void DeleteTaskDocuments(DocumentsModel value)
        {
            try
            {
                var docAccess = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (docAccess != null)
                {
                    _context.DocumentRights.RemoveRange(docAccess);
                    _context.SaveChanges();
                }
                var docper = _context.DocumentPermission.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (docper != null)
                {
                    _context.DocumentPermission.RemoveRange(docper);
                    _context.SaveChanges();
                }


                var commentAttachments = _context.CommentAttachment.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (commentAttachments.Count > 0)
                {
                    _context.CommentAttachment.RemoveRange(commentAttachments);
                    _context.SaveChanges();
                }

                // commented due to live error  by Arun

                var docUserRolefolder = _context.DocumentUserRole.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (docUserRolefolder.Count > 0)
                {
                    _context.DocumentUserRole.RemoveRange(docUserRolefolder);
                    _context.SaveChanges();
                }

                // 
                var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DocumentId == value.DocumentID);
                if (folderDiscussion != null)
                {
                    _context.FolderDiscussion.Remove(folderDiscussion);
                    _context.SaveChanges();
                }

                /*  var docfolder = _context.DocumentFolder.Where(d => d.DocumentId == value.DocumentID).ToList();
                  if (docfolder.Count > 0)
                  {
                      _context.DocumentFolder.RemoveRange(docfolder);
                      _context.SaveChanges();
                  }*/
                LinkFileProfileTypeDocument linkFileProfileTypeDocument = new LinkFileProfileTypeDocument();
                bool? islinkDoc = false;
                var tasksessionId = _context.TaskMaster.FirstOrDefault(t => t.TaskId == value.LinkID)?.SessionId;
                if (tasksessionId != null)
                {
                    linkFileProfileTypeDocument = _context.LinkFileProfileTypeDocument.FirstOrDefault(l => l.DocumentId == value.DocumentID && l.TransactionSessionId == tasksessionId);
                    if (linkFileProfileTypeDocument != null)
                    {
                        _context.LinkFileProfileTypeDocument.Remove(linkFileProfileTypeDocument);
                        _context.SaveChanges();
                    }

                }
                var taskatt = _context.TaskAttachment.Where(d => d.DocumentId == value.DocumentID).ToList();
                if (taskatt != null)
                {
                    var taskId = taskatt.FirstOrDefault().TaskMasterId;
                    var task = _context.TaskMaster.FirstOrDefault(f => f.SourceId == value.DocumentID);
                    if (task == null)
                    {
                        _context.TaskAttachment.RemoveRange(taskatt);
                        _context.SaveChanges();
                        var mainSourceID = _context.TaskMaster.FirstOrDefault(f => f.TaskId == taskId)?.SourceId;
                        var doclink = _context.DocumentLink.FirstOrDefault(l => l.LinkDocumentId == value.DocumentID && l.DocumentId == mainSourceID);
                        if (doclink != null)
                        {
                            _context.DocumentLink.Remove(doclink);
                            _context.SaveChanges();
                            islinkDoc = true;
                        }

                    }
                    else if (task != null)
                    {
                        throw new Exception("Task Main Document Cannot Delete");
                    }
                }
                if (linkFileProfileTypeDocument == null && islinkDoc == false)
                {
                    var query = string.Format("delete from Documents Where DocumentId={0}", value.DocumentID);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to delete document");
                    }
                }

                //
            }
            catch (Exception ex)
            {
                throw new AppException("File in use.,You’re not allowed to delete the file", ex);
            }
        }
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult Upload(IFormCollection files)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var SessionId = Guid.NewGuid();
            var videoFiles = files["isVideoFile"].ToString().Split(",");
            var folderId = files["folderId"].ToString();
            int idx = 0;
            long? folderid = new long?();
            bool? isPublicFolder = false;
            if (folderId != "" && folderId != "undefined" && folderId != null)
            {
                folderid = Convert.ToInt64(folderId);
                isPublicFolder = true;
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fileName1 = SessionId + "." + f.FileName.Split(".").Last(); ;
                if (videoFiles.Length > 0 && videoFiles != null)
                {
                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);

                    if (isVideoFile)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                        using (var targetStream = System.IO.File.Create(serverPath))
                        {
                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = null,
                            FileSize = file.Length,
                            //Description = files.desc
                            UploadDate = DateTime.Now,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                            SessionId = SessionId,
                            IsTemp = true,
                            IsCompressed = true,
                            IsVideoFile = true,
                            FolderId = folderid,
                            IsPublichFolder = isPublicFolder,
                        };
                        _context.Documents.Add(documents);
                    }
                    else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = compressedData,
                            FileSize = fs.Length,
                            //Description = files.desc
                            UploadDate = DateTime.Now,
                            SessionId = SessionId,
                            IsTemp = true,
                            IsCompressed = true,
                            FolderId = folderid,
                            IsPublichFolder = isPublicFolder,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                        };
                        _context.Documents.Add(documents);
                    }
                }



                idx++;

            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }

        [HttpPost]
        [Route("UploadCalandarLinkAllDocuments")]
        public IActionResult UploadCalandarLinkAllDocuments(IFormCollection files, Guid SessionId, long? userId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fileName1 = SessionId + "." + f.FileName.Split(".").Last(); ;
                var isvideoFiles = f.ContentType.Split('/');
                if (isvideoFiles[0] == "video")
                {
                    var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                    using (var targetStream = System.IO.File.Create(serverPath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        IsVideoFile = true,
                    };
                    _context.Documents.Add(documents);
                }
                else
                {
                    var fs = file.OpenReadStream();
                    var br = new BinaryReader(fs);
                    Byte[] document = br.ReadBytes((Int32)fs.Length);
                    var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = compressedData,
                        FileSize = fs.Length,
                        UploadDate = DateTime.Now,
                        SessionId = SessionId,
                        IsTemp = true,
                        IsCompressed = true,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                    };
                    _context.Documents.Add(documents);
                }
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpPost]
        [Route("UploadCalandarLinkDocuments")]
        public IActionResult UploadCalandarLinkDocuments(IFormCollection files)
        {
            var userId = new long?();
            var user = files["userID"].ToString();
            if (user != "" && user != "undefined" && user != null)
            {
                userId = Convert.ToInt32(files["userID"].ToString());
            }
            var SessionId = new Guid(files["sessionId"].ToString());
            var videoFiles = files["isVideoFile"].ToString().Split(",");
            var folderId = files["folderId"].ToString();
            int idx = 0;
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fileName1 = SessionId + "." + f.FileName.Split(".").Last(); 
                if (videoFiles.Length > 0 && videoFiles != null)
                {
                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);

                    if (isVideoFile)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                        using (var targetStream = System.IO.File.Create(serverPath))
                        {
                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = null,
                            FileSize = file.Length,
                            UploadDate = DateTime.Now,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                            SessionId = SessionId,
                            IsTemp = true,
                            IsCompressed = true,
                            IsVideoFile = true,
                        };
                        _context.Documents.Add(documents);
                    }
                    else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = compressedData,
                            FileSize = fs.Length,
                            UploadDate = DateTime.Now,
                            SessionId = SessionId,
                            IsTemp = true,
                            IsCompressed = true,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                        };
                        _context.Documents.Add(documents);
                    }
                }
                idx++;
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpPost]
        [Route("UploadAudio")]
        public IActionResult UploadAudio(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            //files.Files.ToList().ForEach(f =>
            //{
            //    var file = f;
            //    var fs = file.OpenReadStream();
            //    var br = new BinaryReader(fs);
            //    Byte[] document = br.ReadBytes((Int32)fs.Length);

            //    var documents = new Documents
            //    {
            //        FileName = file.FileName,
            //        ContentType = file.ContentType,
            //        FileData = document,
            //        FileSize = fs.Length,
            //        UploadDate = DateTime.Now,
            //        SessionId = SessionId,
            //        IsTemp = true,
            //    };
            //    _context.Documents.Add(documents);
            //});
            // _context.SaveChanges();
            return Content(SessionId.ToString());

        }

        private static byte[] Compress(Stream input)
        {
            using (var compressStream = new MemoryStream())
            using (var compressor = new DeflateStream(compressStream, CompressionMode.Compress))
            {
                input.CopyTo(compressor);
                compressor.Close();
                return compressStream.ToArray();
            }
        }

        private static byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var gzip = new GZipStream(output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return output.ToArray();
        }
        private static byte[] Decompress(byte[] data)
        {
            var output = new MemoryStream();
            var input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;

            using (var gzip = new GZipStream(input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);

                while (read > 0)
                {
                    output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }

                gzip.Close();
            }
            return output.ToArray();
        }
        [HttpGet]
        [Route("GetDocumentsBySessionTopicForum")]
        public List<DocumentsModel> GetDocumentsBySessionTopicForum(Guid? sessionId, int? ActionUrl, string ActionType)
        {
            var userId = _context.ApplicationUser.SingleOrDefault(s => s.SessionId == sessionId)?.UserId;
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var exits = 0;
            var realseWikiCount = 0;
            var setAccess = _context.DocumentUserRole.Where(u => u.UserId == userId).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            if (!string.IsNullOrEmpty(ActionType))
            {
                var querys = _context.Documents.Include(c => c.FilterProfileType).
               Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.FileSize,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   s.DocumentParentId,
                   s.TableName,
                   s.ExpiryDate,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   IsLocked = s.IsLocked,
                   LockedByUserId = s.LockedByUserId,
                   LockedDate = s.LockedDate,
                   s.AddedDate,
                   s.DepartmentId,
                   s.WikiId,
                   s.Extension,
                   s.CategoryId,
                   s.DocumentType,
                   s.DisplayName,
                   s.LinkId,
                   s.IsSpecialFile,
                   s.IsTemp,
                   s.ReferenceNumber,
                   s.Description,
                   s.StatusCodeId,
                   s.ScreenId,
                   FilterProfileType = s.FilterProfileType,
                   s.ProfileNo,
                   s.IsLatest,
                   s.IsWikiDraft,
                   s.IsMobileUpload,
                   s.IsWikiDraftDelete,
                   s.IsCompressed,
                   s.IsVideoFile,
                   s.CloseDocumentId,
                   s.IsPrint,
                   s.IsWiki,
                   s.FileIndex,
                   s.SubjectName

               }).Where(w => w.DocumentId > 0);
                var TotalDocuments = 0;
                if (ActionType == "Case")
                {
                    exits = 1;
                    var templateTestCaseId = _context.TemplateTestCaseForm.Select(s => new { s.TemplateTestCaseId, s.SessionId, s.TemplateTestCaseFormId }).FirstOrDefault(f => f.TemplateTestCaseFormId == ActionUrl);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    if (templateTestCaseId != null)
                    {
                        temSessionIdss.Add(templateTestCaseId.SessionId);
                        var temSessionIds = _context.TemplateTestCaseCheckList.Where(w => (w.TemplateTestCaseFormId == templateTestCaseId.TemplateTestCaseFormId) && w.TemplateTestCaseId == templateTestCaseId.TemplateTestCaseId && w.SessionId != null).Select(s => s.SessionId).ToList();
                        if (temSessionIds != null && temSessionIds.Count > 0)
                        {
                            temSessionIdss.AddRange(temSessionIds);
                        }
                        List<long> linkDocumentIds = new List<long>();
                        var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == templateTestCaseId.SessionId).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                        linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                    }
                }
                if (ActionType == "CheckList")
                {
                    exits = 1;
                    var templateTestCaseId = _context.TemplateTestCaseCheckList.Select(s => new { s.TemplateTestCaseId, s.SessionId, s.TemplateTestCaseFormId, s.TemplateTestCaseCheckListId }).FirstOrDefault(f => f.TemplateTestCaseCheckListId == ActionUrl);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    if (templateTestCaseId != null)
                    {
                        temSessionIdss.Add(templateTestCaseId.SessionId);
                        List<long> linkDocumentIds = new List<long>();
                        var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == templateTestCaseId.SessionId).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                        linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                    }
                }
                if (ActionType == "Ipir")
                {
                    exits = 1;
                    var templateTestCaseId = _context.IpirReport.FirstOrDefault(f => f.IpirReportId == ActionUrl);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    if (templateTestCaseId != null)
                    {
                        temSessionIdss.Add(templateTestCaseId.SessionId);
                        List<long> linkDocumentIds = new List<long>();
                        var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == templateTestCaseId.SessionId).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                        linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                    }
                }
                if (ActionType == "Production Activity")
                {
                    exits = 1;
                    var templateTestCaseId = _context.ProductionActivityAppLine.FirstOrDefault(f => f.ProductionActivityAppLineId == ActionUrl);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    if (templateTestCaseId != null)
                    {
                        temSessionIdss.Add(templateTestCaseId.SessionId);
                        List<long> linkDocumentIds = new List<long>();
                        var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == templateTestCaseId.SessionId).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                        linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                    }
                }
                if (ActionType == "Production Routine")
                {
                    exits = 1;
                    var templateTestCaseId = _context.ProductionActivityRoutineAppLine.FirstOrDefault(f => f.ProductionActivityRoutineAppLineId == ActionUrl);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    if (templateTestCaseId != null)
                    {
                        temSessionIdss.Add(templateTestCaseId.SessionId);
                        List<long> linkDocumentIds = new List<long>();
                        var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == templateTestCaseId.SessionId).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                        linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                        linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                        querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                    }
                }
                if (exits == 1)
                {
                    var query = querys.AsNoTracking().ToList();
                    var documentsIds = query.Select(s => s.DocumentId).ToList();
                    var companyCalandarDocumentPermissions = _context.CompanyCalandarDocumentPermission.Where(w => documentsIds.Contains(w.DocumentId.Value)).ToList();
                    if (query != null && query.Count > 0)
                    {
                        var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                        query.ForEach(s =>
                        {
                            var documentcount = query?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();

                            var name = s.FileName != null ? s.FileName?.Substring(s.FileName.LastIndexOf(".")) : "";
                            var fileName = s.FileName?.Split(name);
                            DocumentsModel documentsModel = new DocumentsModel();
                            documentsModel.SessionID = s.SessionId;
                            documentsModel.DocumentID = s.DocumentId;
                            documentsModel.FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                            documentsModel.ContentType = s.ContentType;
                            documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                            documentsModel.ScreenID = s.ScreenId;
                            documentsModel.Uploaded = true;
                            documentsModel.UploadDate = s.UploadDate;
                            documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                            documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                            documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                            documentsModel.ProfileNo = s.ProfileNo;
                            documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                            documentsModel.DocumentParentId = s.DocumentParentId;
                            documentsModel.IsLatest = s.IsLatest;
                            documentsModel.TotalDocument = TotalDocuments + 1;
                            documentsModel.ExpiryDate = s.ExpiryDate;
                            documentsModel.SubjectName = s.SubjectName;
                            documentsModel.LockedByUser = appUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName;

                            documentsModel.IsLocked = realseWikiCount > 0 ? false : s.IsLocked;
                            documentsModel.Type = "Document";
                            documentsModel.AddedByUserID = s.AddedByUserId;
                            documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                            documentsModel.IsWikiDraft = s.IsWikiDraft;
                            documentsModel.IsMobileUpload = s.IsMobileUpload;
                            documentsModel.IsCompressed = s.IsCompressed;
                            documentsModel.IsVideoFile = s.IsVideoFile;
                            documentsModel.CloseDocumentId = s.CloseDocumentId;
                            documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                            documentsModel.IsPrint = s.IsPrint;
                            documentsModel.Description = s.Description;
                            documentsModel.LockedByUserId = s.LockedByUserId;
                            if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                            {
                                documentsModel.ExpiryDate = null;
                            }
                            var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                            List<FileProfileType> FolderPathLists = new List<FileProfileType>();
                            if (fileProfileTypeData != null)
                            {
                                FolderPathLists = GetAllFileProfileTypeNameAll(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                                FolderPathLists.Add(fileProfileTypeData);
                                documentsModel.FileProfileTypeParentId = FolderPathLists.FirstOrDefault()?.FileProfileTypeId;
                                documentsModel.FileProfileTypeName = string.Join(" / ", FolderPathLists.Select(s => s.Name).ToList());
                            }
                            if (setAccess.Count > 0)
                            {
                                var roleDocItem = setAccess.FirstOrDefault(u => u.DocumentId == s.DocumentId);
                                if (roleDocItem != null)
                                {
                                    var roleItem = setAccess.FirstOrDefault(u => u.UserId == userId && u.DocumentId == s.DocumentId);
                                    if (roleItem != null)
                                    {
                                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                        var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                                        documentsModel.DocumentPermissionData = permissionData;
                                    }
                                    else
                                    {
                                        documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = true, IsRename = false, IsCopy = false, IsCreateFolder = false, IsCloseDocument = false, IsEdit = false, IsMove = false, IsShare = false, IsFileDelete = false };
                                    }
                                }
                                else
                                {

                                    var filprofilepermission = setAccess.FirstOrDefault(u => u.FileProfileTypeId == s.FilterProfileTypeId && u.DocumentId == null && u.UserId == userId);
                                    if (filprofilepermission != null)
                                    {
                                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                        var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)filprofilepermission.RoleId);
                                        documentsModel.DocumentPermissionData = permissionData;
                                    }
                                    else
                                    {
                                        documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = true, IsCopy = true, IsCreateFolder = true, IsCloseDocument = true, IsEdit = true, IsMove = true, IsShare = true, IsFileDelete = true };
                                    }
                                }
                            }
                            else
                            {
                                documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
                            }
                            if (s.AddedByUserId != userId)
                            {
                                var companyCalandarDocumentPermissionsdata = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId);
                                if (companyCalandarDocumentPermissionsdata != null)
                                {
                                    var companyCalandarDocumentPermissionUser = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId && f.UserId == userId);
                                    documentsModel.CalandarPermissionFlag = companyCalandarDocumentPermissionUser != null ? true : false;
                                }
                                else
                                {
                                    documentsModel.CalandarPermissionFlag = false;
                                }
                            }
                            documentsModels.Add(documentsModel);

                        });

                    }
                }
                List<long?> filterProfileTypeIds = documentsModels.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            }
            return documentsModels;
        }

        [HttpGet]
        [Route("GetDocumentsBySessionID")]
        public List<DocumentsModel> GetDocumentsBySessionID(Guid? sessionId, int? userId, string Type)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var TotalDocuments = _context.Documents.Where(s => s.SessionId == sessionId).Count();
            List<long> linkDocumentIds = new List<long>();
            var linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == sessionId).ToList();
            linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
            var realseWikiCount = 0;
            var setAccess = _context.DocumentUserRole.Where(u => u.UserId == userId).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            if (!string.IsNullOrEmpty(Type))
            {
                var querys = _context.Documents.Include(c => c.FilterProfileType).
               Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.FileSize,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   s.DocumentParentId,
                   s.TableName,
                   s.ExpiryDate,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   IsLocked = s.IsLocked,
                   LockedByUserId = s.LockedByUserId,
                   LockedDate = s.LockedDate,
                   s.AddedDate,
                   s.DepartmentId,
                   s.WikiId,
                   s.Extension,
                   s.CategoryId,
                   s.DocumentType,
                   s.DisplayName,
                   s.LinkId,
                   s.IsSpecialFile,
                   s.IsTemp,
                   s.ReferenceNumber,
                   s.Description,
                   s.StatusCodeId,
                   s.ScreenId,
                   FilterProfileType = s.FilterProfileType,
                   s.ProfileNo,
                   s.IsLatest,
                   s.IsWikiDraft,
                   s.IsMobileUpload,
                   s.IsWikiDraftDelete,
                   s.IsCompressed,
                   s.IsVideoFile,
                   s.CloseDocumentId,
                   s.IsPrint,
                   s.IsWiki,
                   s.FileIndex,
                   s.SubjectName,
                   s.FilePath,
               }).Where(w => w.DocumentId > 0);
                if (Type == "Wiki")
                {
                    var applicationWikis = _context.AppWikiReleaseDoc.Include(a => a.ApplicationWiki).Where(w => w.ApplicationWiki.SessionId == sessionId && w.ApplicationWiki.StatusCodeId == 841).Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
                    realseWikiCount = applicationWikis.Count();
                    if (realseWikiCount > 0)
                    {
                        querys = querys.Where(l => applicationWikis.Count > 0 && applicationWikis.Contains(l.DocumentId));
                    }
                    else
                    {
                        querys = querys.Where(l => l.SessionId == sessionId && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId)));
                    }
                }
                var applicationWikisAll = _context.AppWikiDraftDoc.Include(a => a.ApplicationWiki).Where(w => w.ApplicationWiki.SessionId == sessionId && w.ApplicationWiki.StatusCodeId == 840).Select(s => new { documentID = s.DocumentId.GetValueOrDefault(0) }).ToList();

                if (Type == "Draft")
                {
                    var applicationWikis = _context.AppWikiDraftDoc.Include(a => a.ApplicationWiki).Where(w => w.ApplicationWiki.SessionId == sessionId && w.ApplicationWiki.StatusCodeId == 840).Select(s => s.DocumentId.GetValueOrDefault(0)).ToList();
                    if (applicationWikis != null && applicationWikis.Count > 0)
                    {
                        //realseWikiCount = applicationWikis.Count();
                        querys = querys.Where(l => applicationWikis.Count > 0 && applicationWikis.Contains(l.DocumentId));
                    }
                    else
                    {
                        querys = querys.Where(l => l.SessionId == sessionId && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId)));

                    }
                }
                if (Type == "TemplateCaseForm")
                {
                    var templateTestCaseId = _context.TemplateTestCaseForm.Select(s => new { s.TemplateTestCaseId, s.SessionId, s.TemplateTestCaseFormId }).FirstOrDefault(f => f.SessionId == sessionId);
                    List<Guid?> temSessionIdss = new List<Guid?>();
                    temSessionIdss.Add(sessionId);
                    if (templateTestCaseId != null)
                    {
                        var temSessionIds = _context.TemplateTestCaseCheckList.Where(w => (w.TemplateTestCaseFormId == templateTestCaseId.TemplateTestCaseFormId) && w.TemplateTestCaseId == templateTestCaseId.TemplateTestCaseId && w.SessionId != null).Select(s => s.SessionId).ToList();
                        if (temSessionIds != null && temSessionIds.Count > 0)
                        {
                            temSessionIdss.AddRange(temSessionIds);
                        }
                    }
                    TotalDocuments = _context.Documents.Where(s => temSessionIdss.Contains(s.SessionId)).Count();
                    linkDocuments = _context.LinkFileProfileTypeDocument.Where(s => temSessionIdss.Contains(s.TransactionSessionId)).ToList();
                    linkDocumentIds = linkDocuments.Select(d => d.DocumentId.GetValueOrDefault(0)).ToList();
                    querys = querys.Where(l => temSessionIdss.Contains(l.SessionId) && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId);
                }
                var query = querys.AsNoTracking().ToList();
                if (Type == "Wiki")
                {
                    query = query.Where(w => w.IsWikiDraft != true && w.IsWikiDraftDelete != true).ToList();
                }
                if (Type == "Draft")
                {
                    //query = query.Where(w => w.IsWikiDraft == true).ToList();
                }
                var documentsIds = query.Select(s => s.DocumentId).ToList();
                var companyCalandarDocumentPermissions = _context.CompanyCalandarDocumentPermission.Where(w => documentsIds.Contains(w.DocumentId.Value)).ToList();
                if (query != null && query.Count > 0)
                {
                    var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                    query.ForEach(s =>
                    {
                        var documentcount = query?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();

                        var name = s.FileName != null ? s.FileName?.Substring(s.FileName.LastIndexOf(".")) : "";
                        var fileName = s.FileName?.Split(name);
                        DocumentsModel documentsModel = new DocumentsModel();
                        documentsModel.SessionID = s.SessionId;
                        documentsModel.DocumentID = s.DocumentId;
                        documentsModel.FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                        documentsModel.ContentType = s.ContentType;
                        documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                        documentsModel.ScreenID = s.ScreenId;
                        documentsModel.Uploaded = true;
                        documentsModel.UploadDate = s.UploadDate;
                        documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                        documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                        documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                        documentsModel.ProfileNo = s.ProfileNo;
                        documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                        documentsModel.DocumentParentId = s.DocumentParentId;
                        documentsModel.IsLatest = s.IsLatest;
                        documentsModel.TotalDocument = TotalDocuments + 1;
                        documentsModel.ExpiryDate = s.ExpiryDate;
                        documentsModel.SubjectName = s.SubjectName;
                        documentsModel.LockedByUser = appUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName;
                        documentsModel.FilePath = s.FilePath;
                        documentsModel.IsLocked = realseWikiCount > 0 ? false : s.IsLocked;
                        if (Type == "Draft")
                        {
                            var applicationWikisIs = applicationWikisAll.Where(w => w.documentID == s.DocumentId).Count();
                            documentsModel.IsLocked = applicationWikisIs > 0 ? false : s.IsLocked;
                            if (s.IsLocked == true && s.IsLatest == true)
                            {
                                documentsModel.IsLocked = s.IsLocked;
                            }
                        }
                        documentsModel.Type = "Document";
                        documentsModel.AddedByUserID = s.AddedByUserId;
                        documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                        documentsModel.IsWikiDraft = s.IsWikiDraft;
                        documentsModel.IsMobileUpload = s.IsMobileUpload;
                        documentsModel.IsCompressed = s.IsCompressed;
                        documentsModel.IsVideoFile = s.IsVideoFile;
                        documentsModel.CloseDocumentId = s.CloseDocumentId;
                        documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                        documentsModel.IsPrint = s.IsPrint;
                        documentsModel.Description = s.Description;
                        documentsModel.LockedByUserId = s.LockedByUserId;
                        if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                        {
                            documentsModel.ExpiryDate = null;
                        }
                        var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                        List<FileProfileType> FolderPathLists = new List<FileProfileType>();
                        if (fileProfileTypeData != null)
                        {
                            FolderPathLists = GetAllFileProfileTypeNameAll(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                            FolderPathLists.Add(fileProfileTypeData);
                            documentsModel.FileProfileTypeParentId = FolderPathLists.FirstOrDefault()?.FileProfileTypeId;
                            documentsModel.FileProfileTypeName = string.Join(" / ", FolderPathLists.Select(s => s.Name).ToList());
                        }
                        if (setAccess.Count > 0)
                        {
                            var roleDocItem = setAccess.FirstOrDefault(u => u.DocumentId == s.DocumentId);
                            if (roleDocItem != null)
                            {
                                var roleItem = setAccess.FirstOrDefault(u => u.UserId == userId && u.DocumentId == s.DocumentId);
                                if (roleItem != null)
                                {
                                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                                    documentsModel.DocumentPermissionData = permissionData;
                                }
                                else
                                {
                                    documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = true, IsRename = false, IsCopy = false, IsCreateFolder = false, IsCloseDocument = false, IsEdit = false, IsMove = false, IsShare = false, IsFileDelete = false };
                                }
                            }
                            else
                            {

                                var filprofilepermission = setAccess.FirstOrDefault(u => u.FileProfileTypeId == s.FilterProfileTypeId && u.DocumentId == null && u.UserId == userId);
                                if (filprofilepermission != null)
                                {
                                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)filprofilepermission.RoleId);
                                    documentsModel.DocumentPermissionData = permissionData;
                                }
                                else
                                {
                                    documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = true, IsCopy = true, IsCreateFolder = true, IsCloseDocument = true, IsEdit = true, IsMove = true, IsShare = true, IsFileDelete = true };
                                }
                            }
                        }
                        else
                        {
                            documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
                        }
                        if (s.AddedByUserId != userId)
                        {
                            var companyCalandarDocumentPermissionsdata = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId);
                            if (companyCalandarDocumentPermissionsdata != null)
                            {
                                var companyCalandarDocumentPermissionUser = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId && f.UserId == userId);
                                documentsModel.CalandarPermissionFlag = companyCalandarDocumentPermissionUser != null ? true : false;
                            }
                            else
                            {
                                documentsModel.CalandarPermissionFlag = false;
                            }
                        }
                        documentsModels.Add(documentsModel);

                    });

                }
            }
            else
            {
                var query = _context.Documents.Include(c => c.FilterProfileType).
               Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.FileSize,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   s.DocumentParentId,
                   s.TableName,
                   s.ExpiryDate,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   IsLocked = s.IsLocked,
                   LockedByUserId = s.LockedByUserId,
                   LockedDate = s.LockedDate,
                   s.AddedDate,
                   s.DepartmentId,
                   s.WikiId,
                   s.Extension,
                   s.CategoryId,
                   s.DocumentType,
                   s.DisplayName,
                   s.LinkId,
                   s.IsSpecialFile,
                   s.IsTemp,
                   s.ReferenceNumber,
                   s.Description,
                   s.StatusCodeId,
                   s.ScreenId,
                   FilterProfileType = s.FilterProfileType,
                   s.ProfileNo,
                   s.IsLatest,
                   s.IsWikiDraft,
                   s.IsMobileUpload,
                   s.IsWikiDraftDelete,
                   s.IsCompressed,
                   s.IsVideoFile,
                   s.CloseDocumentId,
                   s.IsPrint,
                   s.IsWiki,
                   s.FileIndex,
                   s.SubjectName,
                   s.FilePath,
               }).Where(l => l.SessionId == sessionId && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId).AsNoTracking().ToList();
                var documentsIds = query.Select(s => s.DocumentId).ToList();
                var companyCalandarDocumentPermissions = _context.CompanyCalandarDocumentPermission.Where(w => documentsIds.Contains(w.DocumentId.Value)).ToList();
                if (query != null && query.Count > 0)
                {
                    var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                    query.ForEach(s =>
                    {
                        var documentcount = query?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();
                        var name = s.FileName != null ? s.FileName?.Substring(s.FileName.LastIndexOf(".")) : "";
                        var fileName = s.FileName?.Split(name);
                        DocumentsModel documentsModel = new DocumentsModel();
                        documentsModel.SessionID = s.SessionId;
                        documentsModel.DocumentID = s.DocumentId;
                        documentsModel.FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                        documentsModel.ContentType = s.ContentType;
                        documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                        documentsModel.ScreenID = s.ScreenId;
                        documentsModel.Uploaded = true;
                        documentsModel.UploadDate = s.UploadDate;
                        documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                        documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                        documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                        documentsModel.ProfileNo = s.ProfileNo;
                        documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                        documentsModel.DocumentParentId = s.DocumentParentId;
                        documentsModel.IsLatest = s.IsLatest;
                        documentsModel.TotalDocument = TotalDocuments + 1;
                        documentsModel.ExpiryDate = s.ExpiryDate;
                        documentsModel.IsLocked = realseWikiCount > 0 ? false : s.IsLocked;
                        documentsModel.Type = "Document";
                        documentsModel.AddedByUserID = s.AddedByUserId;
                        documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                        documentsModel.IsWikiDraft = s.IsWikiDraft;
                        documentsModel.IsMobileUpload = s.IsMobileUpload;
                        documentsModel.IsCompressed = s.IsCompressed;
                        documentsModel.IsVideoFile = s.IsVideoFile;
                        documentsModel.CloseDocumentId = s.CloseDocumentId;
                        documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                        documentsModel.IsPrint = s.IsPrint;
                        documentsModel.IsWiki = s.IsWiki;
                        documentsModel.Description = s.Description;
                        documentsModel.IsWikiDraftDelete = s.IsWikiDraftDelete;
                        documentsModel.SubjectName = s.SubjectName;
                        documentsModel.LockedByUserId = s.LockedByUserId;
                        documentsModel.LockedByUser = appUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName;
                        documentsModel.FilePath = s.FilePath;
                        if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                        {
                            documentsModel.ExpiryDate = null;
                        }
                        var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                        List<FileProfileType> FolderPathLists = new List<FileProfileType>();
                        if (fileProfileTypeData != null)
                        {
                            FolderPathLists = GetAllFileProfileTypeNameAll(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                            FolderPathLists.Add(fileProfileTypeData);
                            documentsModel.FileProfileTypeParentId = FolderPathLists.FirstOrDefault()?.FileProfileTypeId;
                            documentsModel.FileProfileTypeName = string.Join(" / ", FolderPathLists.Select(s => s.Name).ToList());
                        }
                        if (setAccess.Count > 0)
                        {
                            var roleDocItem = setAccess.FirstOrDefault(u => u.DocumentId == s.DocumentId);
                            if (roleDocItem != null)
                            {
                                var roleItem = setAccess.FirstOrDefault(u => u.UserId == userId && u.DocumentId == s.DocumentId);
                                if (roleItem != null)
                                {
                                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                                    documentsModel.DocumentPermissionData = permissionData;
                                }
                                else
                                {
                                    documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = true, IsRename = false, IsCopy = false, IsCreateFolder = false, IsCloseDocument = false, IsEdit = false, IsMove = false, IsShare = false, IsFileDelete = false };
                                }
                            }
                            else
                            {

                                var filprofilepermission = setAccess.FirstOrDefault(u => u.FileProfileTypeId == s.FilterProfileTypeId && u.DocumentId == null && u.UserId == userId && s.FilterProfileTypeId != null);
                                if (filprofilepermission != null)
                                {
                                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)filprofilepermission.RoleId);
                                    documentsModel.DocumentPermissionData = permissionData;
                                }
                                else
                                {
                                    documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = true, IsCopy = true, IsCreateFolder = true, IsCloseDocument = true, IsEdit = true, IsMove = true, IsShare = true, IsFileDelete = true };
                                }
                            }
                        }
                        else
                        {
                            documentsModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
                        }
                        if (s.AddedByUserId != userId)
                        {
                            var companyCalandarDocumentPermissionsdata = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId);
                            if (companyCalandarDocumentPermissionsdata != null)
                            {
                                var companyCalandarDocumentPermissionUser = companyCalandarDocumentPermissions.FirstOrDefault(f => f.DocumentId == s.DocumentId && f.UserId == userId);
                                documentsModel.CalandarPermissionFlag = companyCalandarDocumentPermissionUser != null ? true : false;
                            }
                            else
                            {
                                documentsModel.CalandarPermissionFlag = false;
                            }
                        }
                        documentsModels.Add(documentsModel);

                    });

                }
            }
            List<long?> filterProfileTypeIds = documentsModels.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            /* var roleItems = _context.DocumentUserRole.AsNoTracking().Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
             documentsModels.ForEach(q =>
             {
                 if (q.AddedByUserID == userId)
                 {
                     q.DocumentPermissionData = new DocumentPermissionModel
                     {
                         IsUpdateDocument = true,
                         IsCreateDocument = true,
                         IsRead = true,
                         IsDelete = true,
                         IsDocumentAccess = true,

                     };
                 }
                 else
                 {
                     var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();

                     q.DocumentPermissionData = new DocumentPermissionModel();
                     if (roles.Count > 0)
                     {
                         q.DocumentPermissionData = new DocumentPermissionModel();
                         var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                         if (roleItem != null)
                         {
                             DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                             q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                         }
                         else
                         {
                             q.DocumentPermissionData = new DocumentPermissionModel
                             {
                                 IsUpdateDocument = false,
                                 IsCreateDocument = false,
                                 IsRead = false,
                                 IsDelete = false,
                                 IsDocumentAccess = false,

                             };
                         }
                     }
                     else
                     {
                         q.DocumentPermissionData = new DocumentPermissionModel
                         {
                             IsUpdateDocument = true,
                             IsCreateDocument = true,
                             IsRead = true,
                             IsDelete = true,
                             IsDocumentAccess = true,

                         };
                     }
                 }
             });*/
            return documentsModels;
        }

        [HttpGet]
        [Route("GetDocumentsByCalandarLink")]
        public List<DocumentsModel> GetDocumentsByCalandarLink(Guid? sessionId, int? userId)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var querys = _context.Documents.Include(c => c.FilterProfileType).
           Select(s => new
           {
               s.SessionId,
               s.DocumentId,
               s.FileName,
               s.ContentType,
               s.FileSize,
               s.UploadDate,
               s.FilterProfileTypeId,
               s.DocumentParentId,
               s.TableName,
               s.ExpiryDate,
               s.AddedByUserId,
               s.ModifiedByUserId,
               s.ModifiedDate,
               IsLocked = s.IsLocked,
               LockedByUserId = s.LockedByUserId,
               LockedDate = s.LockedDate,
               s.AddedDate,
               s.DepartmentId,
               s.WikiId,
               s.Extension,
               s.CategoryId,
               s.DocumentType,
               s.DisplayName,
               s.LinkId,
               s.IsSpecialFile,
               s.IsTemp,
               s.ReferenceNumber,
               s.Description,
               s.StatusCodeId,
               s.ScreenId,
               FilterProfileType = s.FilterProfileType,
               s.ProfileNo,
               s.IsLatest,
               s.IsWikiDraft,
               s.IsMobileUpload,
               s.IsWikiDraftDelete,
               s.IsCompressed,
               s.IsVideoFile,
               s.CloseDocumentId,
               s.IsPrint,
               s.IsWiki,
           }).Where(w => w.SessionId == sessionId);
            var query = querys.AsNoTracking().ToList();
            if (query != null && query.Count > 0)
            {
                query.ForEach(s =>
                {
                    DocumentsModel documentsModel = new DocumentsModel();
                    documentsModel.SessionID = s.SessionId;
                    documentsModel.DocumentID = s.DocumentId;
                    documentsModel.FileName = s.FileName;
                    documentsModel.ContentType = s.ContentType;
                    documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                    documentsModel.ScreenID = s.ScreenId;
                    documentsModel.Uploaded = true;
                    documentsModel.UploadDate = s.UploadDate;
                    documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                    documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                    documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                    documentsModel.ProfileNo = s.ProfileNo;
                    documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                    documentsModel.DocumentParentId = s.DocumentParentId;
                    documentsModel.IsLatest = s.IsLatest;
                    documentsModel.ExpiryDate = s.ExpiryDate;
                    documentsModel.Type = "Document";
                    documentsModel.AddedByUserID = s.AddedByUserId;
                    documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                    documentsModel.IsWikiDraft = s.IsWikiDraft;
                    documentsModel.IsMobileUpload = s.IsMobileUpload;
                    documentsModel.IsCompressed = s.IsCompressed;
                    documentsModel.IsVideoFile = s.IsVideoFile;
                    documentsModel.CloseDocumentId = s.CloseDocumentId;
                    documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                    documentsModel.IsPrint = s.IsPrint;
                    documentsModel.Description = s.Description;
                    documentsModels.Add(documentsModel);
                });
            }
            return documentsModels;
        }
        private List<FileProfileType> GetAllFileProfileTypeNameAll(List<FileProfileType> foldersListItems, FileProfileType s, List<FileProfileType> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeNameAll(foldersListItems, items, foldersModel);
                foldersModel.Add(items);
            }
            return foldersModel;
        }
        [HttpGet]
        [Route("GetTaskAttachmentBySessionID")]
        public List<DocumentsModel> GetTaskAttachmentBySessionID(Guid? sessionId, int? userId, int? type)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var TotalDocuments = _context.Documents.Where(s => s.SessionId == sessionId).Count();
            List<long?> linkDocumentIds = new List<long?>();
            List<long?> taskAttachmentIds = new List<long?>();
            taskAttachmentIds = _context.TaskAttachment.Where(t => t.TaskMasterId == type && t.IsLatest == true).Select(s => s.DocumentId).ToList();
            // var finishProductInfoDocuments = _context.FinishProductGeneralInfoDocument.Where(s => s.SessionId == sessionId).AsNoTracking().ToList();
            linkDocumentIds = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == sessionId).Select(d => d.DocumentId).ToList();
            var query = _context.Documents.Include(c => c.FilterProfileType).
                Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.ScreenId,
                    FilterProfileType = s.FilterProfileType,
                    s.ProfileNo,
                    s.IsLatest,
                    s.IsWikiDraft,
                    s.IsMobileUpload,
                    s.IsWikiDraftDelete,
                    s.IsCompressed,
                    s.IsVideoFile,
                    s.CloseDocumentId,
                    s.FileIndex,
                    s.IsMainTask
                }).Where(l => l.SessionId == sessionId && l.IsLatest == true || (linkDocumentIds.Count > 0 && linkDocumentIds.Contains(l.DocumentId)) || (taskAttachmentIds.Count > 0 && taskAttachmentIds.Contains(l.DocumentId))).OrderByDescending(o => o.DocumentId).AsNoTracking().Distinct().ToList();

            if (query != null && query.Count > 0)
            {
                query.ForEach(s =>
                {
                    var documents = query.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                    var fileName = documents?.FileName.Split('.');
                    DocumentsModel documentsModel = new DocumentsModel();
                    documentsModel.SessionID = s.SessionId;
                    documentsModel.DocumentID = s.DocumentId;
                    documentsModel.FileName = documents?.FileIndex > 0 ? fileName[0] + "_V0" + documents?.FileIndex + "." + fileName[1] : documents?.FileName;
                    documentsModel.ContentType = s.ContentType;
                    documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                    documentsModel.ScreenID = s.ScreenId;
                    documentsModel.Uploaded = true;
                    documentsModel.UploadDate = s.UploadDate;
                    documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                    documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                    documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                    documentsModel.ProfileNo = s.ProfileNo;
                    documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                    documentsModel.DocumentParentId = s.DocumentParentId;
                    documentsModel.IsLatest = s.IsLatest;
                    documentsModel.TotalDocument = TotalDocuments + 1;
                    documentsModel.ExpiryDate = s.ExpiryDate;
                    documentsModel.IsLocked = s.IsLocked;
                    documentsModel.Type = "Document";
                    documentsModel.AddedByUserID = s.AddedByUserId;
                    documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                    documentsModel.IsWikiDraft = s.IsWikiDraft;
                    documentsModel.IsMobileUpload = s.IsMobileUpload;
                    documentsModel.IsCompressed = s.IsCompressed;
                    documentsModel.IsVideoFile = s.IsVideoFile;
                    documentsModel.CloseDocumentId = s.CloseDocumentId;
                    documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                    documentsModel.IsMainTask = s.IsMainTask;
                    if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                    {
                        documentsModel.ExpiryDate = null;
                    }
                    documentsModels.Add(documentsModel);

                });

            }

            List<long?> filterProfileTypeIds = documentsModels.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.AsNoTracking().Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            documentsModels.ForEach(q =>
            {
                if (q.AddedByUserID == userId)
                {
                    q.DocumentPermissionData = new DocumentPermissionModel
                    {
                        IsUpdateDocument = true,
                        IsCreateDocument = true,
                        IsRead = true,
                        IsDelete = true,
                        IsDocumentAccess = true,

                    };
                }
                else
                {
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();

                    q.DocumentPermissionData = new DocumentPermissionModel();
                    if (roles.Count > 0)
                    {
                        q.DocumentPermissionData = new DocumentPermissionModel();
                        var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData = new DocumentPermissionModel
                            {
                                IsUpdateDocument = false,
                                IsCreateDocument = false,
                                IsRead = false,
                                IsDelete = false,
                                IsDocumentAccess = false,

                            };
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData = new DocumentPermissionModel
                        {
                            IsUpdateDocument = true,
                            IsCreateDocument = true,
                            IsRead = true,
                            IsDelete = true,
                            IsDocumentAccess = true,

                        };
                    }
                }
            });
            return documentsModels;
        }

        [HttpPost]
        [Route("UploadFromTaskDocument")]
        public IActionResult UploadFromTaskDocument(IFormCollection files)
        {
            long? addedByUserId = null;
            long? TaskID = null;
            List<long?> reaWriteUserIds = new List<long?>();
            List<long?> reaOnlyUserIds = new List<long?>();
            List<long?> noAccessIds = new List<long?>();
            DateTime? expiryDate = new DateTime?();
            bool? isFromcommentAttachment = new bool?();
            isFromcommentAttachment = false;
            var sessionID = new Guid(files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            if (userId != null)
            {
                addedByUserId = Convert.ToInt64(userId);
            }
            var taskId = files["taskId"].ToString();
            if (taskId != "null")
            {
                TaskID = Convert.ToInt64(taskId);
            }
            var taskMaster = _context.TaskMaster.FirstOrDefault(w => w.TaskId == TaskID.GetValueOrDefault(0));
            var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
            var videoFiles = files["isVideoFile"].ToString().Split(",");

            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var screenID = files["screeenID"].ToString();
            var description = files["description"].ToString();
            var readWriteUserID = files["readWriteUserID"].ToString();
            var isFromComment = files["isFromComment"].ToString();
            var link = files["link"].ToString();
            if (isFromComment != null && isFromComment != "" && isFromComment != "undefined")
            {
                isFromcommentAttachment = Convert.ToBoolean(isFromComment);
            }
            var readonlyUserID = files["readonlyUserID"].ToString();
            var noAccessID = files["noAccessIDs"].ToString();
            if (noAccessID != null && noAccessID != "" && noAccessID != "undefined")
            {
                var rea = noAccessID.Split(',').ToArray();
                for (int i = 0; i < rea.Length; i++)
                {
                    noAccessIds.Add(Convert.ToInt64(rea[i]));
                }

            }
            if (readonlyUserID != null && readonlyUserID != "" && readonlyUserID != "undefined")
            {
                var reaOnly = readonlyUserID.Split(',').ToArray();
                for (int i = 0; i < reaOnly.Length; i++)
                {
                    reaOnlyUserIds.Add(Convert.ToInt64(reaOnly[i]));
                }
            }
            if (readWriteUserID != null && readWriteUserID != "" && readWriteUserID != "undefined")
            {
                var readWrite = readWriteUserID.Split(',').ToArray();
                for (int i = 0; i < readWrite.Length; i++)
                {
                    reaWriteUserIds.Add(Convert.ToInt64(readWrite[i]));
                }
            }
            var expiry = files["expiryDate"].ToString();
            if (expiry != "null" && expiry != null)
            {
                expiryDate = Convert.ToDateTime(files["expiryDate"].ToString());
            }
            else if (expiry == "null")
            {
                expiryDate = null;
            }
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                if (addedByUserId != null)
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, AddedByUserID = addedByUserId, StatusCodeID = 710, });

                }
                else
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, StatusCodeID = 710, });
                }
            }

            int idx = 0;
            var documentlinkDoc = new long?();
            if (TaskID != null)
            {
                documentlinkDoc = _context.Documents.Where(d => d.TaskId != null && d.TaskId > 0 && d.TaskId == TaskID)?.FirstOrDefault()?.DocumentId;

            }
            files.Files.ToList().ForEach(f =>
            {
                var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                var file = f;
                var fileName1 = sessionID + "." + f.FileName.Split(".").Last(); 
                if (isVideoFile)
                {
                    var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                    using (var targetStream = System.IO.File.Create(serverPath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        ExpiryDate = expiryDate,
                        Description = description,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        IsVideoFile = true,
                        AddedDate = DateTime.Now,

                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    if (documentlinkDoc != null)
                    {
                        var DocumentLink = new DocumentLink
                        {
                            DocumentId = documentlinkDoc,
                            AddedByUserId = addedByUserId,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            LinkDocumentId = documents.DocumentId,
                            FileProfieTypeId = fileProfileTypeId,
                            DocumentPath = "Link From Task",

                        };
                        _context.DocumentLink.Add(DocumentLink);
                        _context.SaveChanges();
                    }

                    if (TaskID != null)
                    {
                        var checktaskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.TaskMasterId == TaskID);
                        if (checktaskAttachment != null)
                        {
                            var createtask = _context.Documents.FirstOrDefault(f => f.DocumentId == checktaskAttachment.DocumentId)?.TaskId;
                            if (createtask != null)
                            {
                                var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                                if (doccheck != null)
                                {
                                    //doccheck.TaskId = TaskID;
                                    doccheck.IsMainTask = false;
                                }

                            }
                        }
                        else
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = null;
                            }
                        }

                        if (isFromcommentAttachment == true)
                        {
                            var taskAttachment = new TaskAttachment
                            {
                                DocumentId = documents.DocumentId,
                                IsDiscussionNotes = false,
                                IsLatest = true,
                                IsLocked = false,
                                IsComment = true,

                                IsMajorChange = false,
                                IsMeetingNotes = false,
                                TaskMasterId = TaskID,
                                UploadedByUserId = addedByUserId,
                                UploadedDate = DateTime.Now,
                                VersionNo = "1.0",

                            };
                            _context.TaskAttachment.Add(taskAttachment);
                        }
                        var notes = new Notes
                        {
                            Notes1 = taskMaster?.Title,
                            Link = link + "id=" + taskMaster?.MainTaskId + "&taskid=" + taskId,
                            DocumentId = documents.DocumentId,
                            SessionId = documents.SessionId,
                            AddedByUserId = addedByUserId.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();
                    }
                }
                else
                {
                    var fs = file.OpenReadStream();
                    var br = new BinaryReader(fs);
                    Byte[] document = br.ReadBytes((Int32)fs.Length);
                    var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = compressedData,
                        FileSize = fs.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        ExpiryDate = expiryDate,
                        Description = description,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        AddedDate = DateTime.Now,

                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    if (TaskID != null)
                    {
                        var notes = new Notes
                        {
                            Notes1 = taskMaster?.Title,
                            Link = link + "id=" + taskMaster?.MainTaskId + "&taskid=" + taskId,
                            DocumentId = documents.DocumentId,
                            SessionId = documents.SessionId,
                            AddedByUserId = addedByUserId.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();
                        if (documentlinkDoc != null)
                        {
                            var DocumentLink = new DocumentLink
                            {
                                DocumentId = documentlinkDoc,
                                AddedByUserId = addedByUserId,
                                AddedDate = DateTime.Now,
                                StatusCodeId = 1,
                                LinkDocumentId = documents.DocumentId,
                                FileProfieTypeId = fileProfileTypeId,
                                DocumentPath = "Link From Task",

                            };
                            _context.DocumentLink.Add(DocumentLink);
                            _context.SaveChanges();
                        }
                        var checktaskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.TaskMasterId == TaskID);
                        if (checktaskAttachment != null)
                        {
                            var createtask = _context.Documents.FirstOrDefault(f => f.DocumentId == checktaskAttachment.DocumentId)?.TaskId;
                            if (createtask != null)
                            {
                                var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                                if (doccheck != null)
                                {
                                    //doccheck.TaskId = TaskID;
                                    doccheck.IsMainTask = false;
                                }

                            }
                            else
                            {
                                var docIds = _context.TaskAttachment.Where(d => d.TaskMasterId == TaskID).Select(t => t.DocumentId).ToList();
                                if (docIds != null)
                                {
                                    var existlinkdocu = _context.Documents.FirstOrDefault(d => docIds.Contains(d.DocumentId) && d.TaskId != null);
                                    if (existlinkdocu != null)
                                    {
                                        var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                                        if (doccheck != null)
                                        {
                                            //doccheck.TaskId = TaskID;
                                            doccheck.IsMainTask = false;
                                        }
                                    }
                                    else
                                    {
                                        var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                                        if (doccheck != null)
                                        {
                                            //doccheck.TaskId = TaskID;
                                            doccheck.IsMainTask = null;
                                        }
                                    }


                                }

                            }
                        }
                        else
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = null;
                            }
                        }
                        if (isFromcommentAttachment == true)
                        {
                            var existtaskAttachment = _context.TaskAttachment.FirstOrDefault(t => t.TaskMasterId == TaskID && t.DocumentId == documents.DocumentId);
                            if (existtaskAttachment == null)
                            {
                                var taskAttachment = new TaskAttachment
                                {
                                    DocumentId = documents.DocumentId,
                                    IsDiscussionNotes = false,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsComment = true,

                                    IsMajorChange = false,
                                    IsMeetingNotes = false,
                                    TaskMasterId = TaskID,
                                    UploadedByUserId = addedByUserId,
                                    UploadedDate = DateTime.Now,
                                    VersionNo = "1.0",
                                    IsLatestCommentAttachment = true,

                                };
                                _context.TaskAttachment.Add(taskAttachment);
                                _context.SaveChanges();
                            }

                        }
                    }
                    if (reaOnlyUserIds != null && reaOnlyUserIds.Count > 0)
                    {
                        reaOnlyUserIds.ForEach(id =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = true,
                                IsReadWrite = false,
                                UserId = id,
                                DocumentId = documents.DocumentId,
                            };
                            _context.DocumentRights.Add(docAccess);
                        });
                    }

                    if (reaWriteUserIds != null && reaWriteUserIds.Count > 0)
                    {
                        reaWriteUserIds.ForEach(id =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = true,
                                UserId = id,
                                DocumentId = documents.DocumentId,
                            };
                            _context.DocumentRights.Add(docAccess);
                        });
                    }
                    if (noAccessIds != null && noAccessIds.Count > 0)
                    {
                        noAccessIds.ForEach(id =>
                        {
                            var docAccess = new DocumentRights
                            {
                                IsRead = false,
                                IsReadWrite = false,
                                UserId = id,
                                DocumentId = documents.DocumentId,
                            };
                            _context.DocumentRights.Add(docAccess);
                        });
                    }
                }

                idx++;
            });

            if (sessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == sessionID).ToList();
                if (TaskID != null)
                {
                    docu.ForEach(file =>
                    {
                        file.IsTemp = false;
                        if (docu != null)
                        {
                            var existtaskAttachment = _context.TaskAttachment.FirstOrDefault(t => t.TaskMasterId == TaskID && t.DocumentId == file.DocumentId);
                            if (existtaskAttachment == null)
                            {
                                var taskAttach = new TaskAttachment
                                {
                                    DocumentId = file.DocumentId,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = addedByUserId,
                                    VersionNo = "1",
                                    TaskMasterId = TaskID,
                                    CheckInDescription = description,

                                    //TaskMasterId = value.TaskID
                                };
                                _context.TaskAttachment.Add(taskAttach);
                                var exist = _context.DocumentLink.Where(l => l.DocumentId == documentlinkDoc && l.LinkDocumentId == file.DocumentId)?.FirstOrDefault();
                                if (exist == null)
                                {
                                    var DocumentLink = new DocumentLink
                                    {
                                        DocumentId = documentlinkDoc,
                                        AddedByUserId = addedByUserId,
                                        AddedDate = DateTime.Now,
                                        StatusCodeId = 1,
                                        LinkDocumentId = file.DocumentId,
                                        FileProfieTypeId = fileProfileTypeId,
                                        DocumentPath = "Link From Task",

                                    };
                                    _context.DocumentLink.Add(DocumentLink);
                                    _context.SaveChanges();
                                }
                            }
                        }




                    });
                }
            }

            _context.SaveChanges();
            return Content(sessionID.ToString());

        }
        [HttpPost]
        [Route("UploadDocument")]
        public IActionResult UploadDocument(IFormCollection files)
        {
            long? addedByUserId = null;
            long? fileProfileTypeId = null;
            DateTime? expiryDate = new DateTime?();
            var sessionID = new Guid(files["sessionID"].ToString());
            //var userSession = new Guid(files["userSession"].ToString());
            //var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            //if (userExits > 0)
            //{
                var userId = files["userId"].ToString();
                if (userId != null)
                {
                    addedByUserId = Convert.ToInt64(userId);
                }
                var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var wikiStatusType = files["wikiStatusType"].ToString();
                var description = files["description"].ToString();
                var subjectName = files["subjectName"].ToString();
                var fileProfileType = files["fileProfileTypeId"].ToString();
                if (fileProfileType != null)
                {
                    fileProfileTypeId = Convert.ToInt64(fileProfileType);
                }
                fileProfileTypeId = fileProfileTypeId > 0 ? fileProfileTypeId : null;
                var screenID = files["screeenID"].ToString();
                var expiry = files["expiryDate"].ToString();
                if (expiry != "null" && expiry != null)
                {
                    expiryDate = Convert.ToDateTime(files["expiryDate"].ToString());
                }
                else if (expiry == "null")
                {
                    expiryDate = null;
                }
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                string profileNo = "";
                if (profile != null)
                {
                    if (addedByUserId != null)
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, AddedByUserID = addedByUserId, StatusCodeID = 710, });

                    }
                    else
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, StatusCodeID = 710, });
                    }
                }
                bool? wikiStatusTypes = null;
                bool? isWikiFlag = null;
                if (wikiStatusType == "Draft")
                {
                    wikiStatusTypes = true;
                }
                if (wikiStatusType == "Wiki")
                {
                    isWikiFlag = true;
                }
                int idx = 0;
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    //var sessionID = Guid.NewGuid();
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        ExpiryDate = expiryDate,
                        IsWikiDraft = wikiStatusTypes,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        IsVideoFile = isVideoFiles,
                        IsMainTask = false,
                        AddedDate = DateTime.Now,
                        Description = description,
                        IsWiki = isWikiFlag,
                        SubjectName = subjectName,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    if (wikiStatusType == "Draft")
                    {
                        var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == sessionID && f.StatusCodeId == 840)?.ApplicationWikiId;
                        if (appWikId > 0)
                        {
                            var appWikiDraftDoc = new AppWikiDraftDoc
                            {
                                DocumentId = documents.DocumentId,
                                ApplicationWikiId = appWikId,
                            };
                            _context.AppWikiDraftDoc.Add(appWikiDraftDoc);
                            _context.SaveChanges();
                        }
                    }


                    idx++;
                });
            
            return Content(sessionID.ToString());

        }
        private long InsertFileProfileType(FileProfileTypeModel value)
        {
            var fileProfileType = new FileProfileType
            {
                ParentId = value.ParentId,
                Name = value.Name,
                Description = value.Description,
                ProfileId = value.ProfileId,
                IsExpiryDate = value.IsExpiryDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsAllowMobileUpload = value.IsAllowMobileUpload,
                IsDocumentAccess = value.IsDocumentAccess,
                ShelfLifeDurationId = value.ShelfLifeDurationId,
                ShelfLifeDuration = value.ShelfLifeDuration,
                Hints = value.Hints,
                IsEnableCreateTask = value.IsEnableCreateTask,
                IsCreateByMonth = value.IsCreateByMonth,
                IsCreateByYear = value.IsCreateByYear,
                IsHidden = value.IsHidden,
                ProfileInfo = value.ProfileInfo,
            };
            _context.FileProfileType.Add(fileProfileType);
            _context.SaveChanges();
            return fileProfileType.FileProfileTypeId;
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [HttpPost]
        [Route("UploadCaseDocument")]
        public IActionResult UploadCaseDocument(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            if (userExits > 0)
            {
                long? fileProfileTypeId = null;
                long? fileProfileTypeParentId = null;
                long? addedByUserId = null;
                var userId = files["userId"].ToString();
                var caseNo = files["caseNo"].ToString();
                if (userId != null)
                {
                    addedByUserId = Convert.ToInt64(userId);
                }
                FileProfileTypeModel ParentProfile = new FileProfileTypeModel();
                ParentProfile.Name = "Template Test Case Form";
                ParentProfile.ProfileId = _context.DocumentNoSeries.FirstOrDefault().ProfileId.Value;
                ParentProfile.StatusCodeID = 1;
                ParentProfile.AddedByUserID = addedByUserId;
                var fileProfileTypePar = _context.FileProfileType.FirstOrDefault(f => f.Name.ToLower() == ParentProfile.Name.ToLower());
                if (fileProfileTypePar == null)
                {
                    fileProfileTypeParentId = InsertFileProfileType(ParentProfile);
                }
                else
                {
                    fileProfileTypeParentId = fileProfileTypePar.FileProfileTypeId;
                }
                FileProfileTypeModel ChildProfile = new FileProfileTypeModel();
                ChildProfile.Name = caseNo;
                ChildProfile.ProfileId = ParentProfile.ProfileId;
                ChildProfile.StatusCodeID = 1;
                ChildProfile.ParentId = fileProfileTypeParentId;
                ChildProfile.AddedByUserID = addedByUserId;
                var fileProfileTypeChild = _context.FileProfileType.FirstOrDefault(f => f.Name.ToLower() == ChildProfile.Name.ToLower() && f.ParentId == fileProfileTypeParentId);
                if (fileProfileTypeChild == null)
                {
                    fileProfileTypeId = InsertFileProfileType(ChildProfile);
                }
                else
                {
                    fileProfileTypeId = fileProfileTypeChild.FileProfileTypeId;
                }
                DateTime? expiryDate = new DateTime?();


                var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var description = files["description"].ToString();
                var subjectName = files["subjectName"].ToString();
                var screenID = files["screeenID"].ToString();
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                string profileNo = "";
                if (profile != null)
                {
                    if (addedByUserId != null)
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, AddedByUserID = addedByUserId, StatusCodeID = 710, });

                    }
                    else
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, StatusCodeID = 710, });
                    }
                }
                bool? wikiStatusTypes = null;
                bool? isWikiFlag = null;
                int idx = 0;
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        ExpiryDate = expiryDate,
                        IsWikiDraft = wikiStatusTypes,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        IsVideoFile = isVideoFiles,
                        IsMainTask = false,
                        AddedDate = DateTime.Now,
                        Description = description,
                        IsWiki = isWikiFlag,
                        SubjectName = subjectName,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();


                    idx++;
                });
            }
            return Content(sessionID.ToString());

        }


        [HttpGet]
        [Route("DocumentCheckIn")]
        public bool DocumentCheckIn(long id, long userId)
        {
            var documents = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            documents.IsLocked = true;
            documents.LockedByUserId = userId;
            _context.SaveChanges();
            return documents.IsLocked.Value;
        }

        [HttpPut]
        [Route("FileProfileCheckOut")]
        public DocumentsModel DocumentCheckOut(DocumentsModel model)
        {
            DocumentsModel documentsModel = new DocumentsModel();

            var taskAttachment = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == model.DocumentID && t.TaskMasterId != null);
            if (taskAttachment != null)
            {
                var isFromProfileDocument = _context.TaskMaster.FirstOrDefault(f => f.TaskId == taskAttachment.TaskMasterId)?.IsFromProfileDocument;
                if (isFromProfileDocument != null && isFromProfileDocument == true)
                {
                    taskAttachment.IsLocked = true;
                    taskAttachment.LockedDate = DateTime.Now;
                    taskAttachment.LockedByUserId = model.LockedByUserId;
                }
            }
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == model.DocumentID);
            if (document != null)
            {
                document.IsLocked = true;
                document.LockedDate = DateTime.Now;
                document.LockedByUserId = model.LockedByUserId;
                documentsModel.IsLocked = document.IsLocked;
                documentsModel.LockedDate = DateTime.Now;
                documentsModel.LockedByUserId = document.LockedByUserId;
                documentsModel.IsCompressed = document.IsCompressed;
            }

            _context.SaveChanges();
            return documentsModel;
        }

        [HttpPut]
        [Route("FileProfileDocumentCheckIn")]
        public DocumentsModel FileProfileDocumentCheckIn(DocumentsModel model)
        {
            DocumentsModel documentsModel = new DocumentsModel();
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == model.DocumentID);
            if (document != null)
            {
                document.IsLocked = false;
                document.IsLatest = true;

            }

            _context.SaveChanges();
            return documentsModel;
        }

        [HttpGet]
        [Route("DownLoadDocument")]
        public IActionResult DownLoadDocument(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {
                if (document.FilePath != null)
                {
                    string serverPath = _hostingEnvironment.ContentRootPath + @"\" + document.FilePath;
                    FileStream stream = System.IO.File.OpenRead(serverPath);
                    var br = new BinaryReader(stream);
                    Byte[] documents = br.ReadBytes((Int32)stream.Length);
                    Stream streams = new MemoryStream(documents);
                    if (streams == null)
                        return NotFound();
                    return Ok(streams);
                }
                else
                {
                    if (document.IsCompressed == true && !document.ContentType.Contains("video/mp4"))
                    {
                        Stream stream = new MemoryStream(DocumentZipUnZip.Unzip(document.FileData));
                        //Stream stream = new MemoryStream(Decompress(document.FileData));
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    else
                    {
                        Stream stream = new MemoryStream(document.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                }
            }
            return NotFound();
        }

        [HttpGet]
        [Route("DownLoadFinishProductGeneralInfoDocument")]
        public IActionResult DownLoadFinishProductGeneralInfoDocument(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {
                if (document.IsCompressed == true && !document.ContentType.Contains("video/mp4"))
                {
                    Stream stream = new MemoryStream(DocumentZipUnZip.Unzip(document.FileData));
                    //Stream stream = new MemoryStream(Decompress(document.FileData));
                    if (stream == null)
                        return NotFound();

                    return Ok(stream);
                }
                else
                {
                    Stream stream = new MemoryStream(document.FileData);
                    if (stream == null)
                        return NotFound();

                    return Ok(stream);
                }

            }
            else if (document == null)
            {
                var finishProductdocument = _context.FinishProductGeneralInfoDocument.SingleOrDefault(t => t.FinishProductGeneralInfoDocumentId == id);
                if (finishProductdocument != null)
                {
                    Stream stream = new MemoryStream(finishProductdocument.FileData);

                    if (stream == null)
                        return NotFound();

                    return Ok(stream);
                }
            }
            return NotFound();
        }
        [HttpGet]
        [Route("DownLoadHeaderDocument")]
        public DocumentsModel DownLoadHeaderDocument(string sessionID)
        {
            var sessionId = new Guid(sessionID);
            DocumentsModel documentsModel = new DocumentsModel();
            var document = _context.Documents.FirstOrDefault(t => t.SessionId == sessionId && t.IsHeaderImage == true);
            if (document != null)
            {
                if (document.IsCompressed == true)
                {
                    documentsModel.DocumentID = document.DocumentId;
                    documentsModel.ImageData = DocumentZipUnZip.Unzip(document.FileData);
                    documentsModel.ContentType = document.ContentType;
                    documentsModel.FileName = document.FileName;
                }

            }
            return documentsModel;
        }


        [HttpGet]
        [Route("GetDocumentBySessionID")]
        public DocumentsModel GetDocumentsBySessionID(string sessionID)
        {
            DocumentsModel documentsModel = new DocumentsModel();
            var sessionId = new Guid(sessionID);
            var query = _context.Documents.
                Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.DepartmentId,
                    s.WikiId,
                    s.Extension,
                    s.CategoryId,
                    s.DocumentType,
                    s.DisplayName,
                    s.LinkId,
                    s.IsSpecialFile,
                    s.IsTemp,
                    s.ReferenceNumber,
                    s.Description,
                    s.StatusCodeId,
                    s.ScreenId,
                    FilterProfileType = s.FilterProfileType,
                    s.ProfileNo,
                    s.IsLatest,
                    s.IsWikiDraft,
                    s.IsMobileUpload,
                    s.IsWikiDraftDelete,
                    s.IsCompressed,
                    s.FileIndex,
                    s.IsVideoFile,
                    s.IsHeaderImage,
                    s.FilePath,
                }).Where(s => s.SessionId == sessionId && s.IsHeaderImage == true).OrderByDescending(o => o.DocumentId).AsNoTracking().FirstOrDefault();
            if (query != null)
            {
                var fileName = query.FileName.Split('.');
                documentsModel.SessionID = query.SessionId;
                documentsModel.DocumentID = query.DocumentId;
                if (query.FileIndex > 0)
                {
                    documentsModel.FileName = fileName[0] + "_V0" + query.FileIndex + "." + fileName[1];
                }
                else
                {
                    documentsModel.FileName = query.FileName;
                }
                documentsModel.ContentType = query.ContentType;
                documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(query.FileSize / 1024)); ;
                documentsModel.ScreenID = query.ScreenId;
                documentsModel.Uploaded = true;
                documentsModel.UploadDate = query.UploadDate;
                documentsModel.IsImage = query.ContentType.ToLower().Contains("image") ? true : false;
                documentsModel.FilterProfileTypeId = query.FilterProfileTypeId;
                documentsModel.FileProfileTypeName = query.FilterProfileType != null ? query.FilterProfileType.Name : string.Empty;
                documentsModel.ProfileNo = query.ProfileNo;
                documentsModel.ProfileID = query.FilterProfileType != null ? query.FilterProfileType.ProfileId : -1;
                documentsModel.DocumentParentId = query.DocumentParentId;
                documentsModel.IsLatest = query.IsLatest;
                documentsModel.ExpiryDate = query.ExpiryDate;
                documentsModel.IsLocked = query.IsLocked;
                documentsModel.Type = "Document";
                documentsModel.AddedByUserID = query.AddedByUserId;
                documentsModel.IsExpiryDate = query.FilterProfileType?.IsExpiryDate;
                documentsModel.IsWikiDraft = query.IsWikiDraft;
                documentsModel.IsMobileUpload = query.IsMobileUpload;
                documentsModel.IsCompressed = query.IsCompressed;
                documentsModel.FilePath = query.FilePath;
                documentsModel.IsVideoFile = query.IsVideoFile;
                if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                {
                    documentsModel.ExpiryDate = null;
                }
            }
            return documentsModel;
        }

        [HttpDelete]
        [Route("DeleteDocument")]
        public void DeleteDocument(int id)
        {
            bool? isLinkDocument = false;
            var document = _context.Documents.SingleOrDefault(p => p.DocumentId == id);
            if (document != null)
            {
                var linkdoc = _context.LinkFileProfileTypeDocument.Where(l => l.DocumentId == id)?.FirstOrDefault();
                if (linkdoc != null)
                {
                    _context.LinkFileProfileTypeDocument.Remove(linkdoc);
                    _context.SaveChanges();
                    isLinkDocument = true;
                }
            }
            if (isLinkDocument == false)
            {
                var query = string.Format("delete from Documents Where DocumentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
            }
        }
        [HttpGet]
        [Route("UpdateDocument")]
        public void UpdateDocument(int id)
        {
            //var document = _context.Documents.SingleOrDefault(p => p.DocumentId == id);
            //if (document != null)
            //{
            //    document.IsWikiDraftDelete = true;
            //    _context.SaveChanges();
            //}


            var query = string.Format("Update Documents Set IsWikiDraftDelete = 1 Where DocumentId={0}", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }
        }
        [HttpPost]
        [Route("ViewDocument")]
        public string ViewDocument(DocumentsModel DocumentsModel)
        {
            string base64String = "";
            Documents document = new Documents();
            if (DocumentsModel.Type == "Document")
            {
                var documents = _context.Documents.SingleOrDefault(t => t.DocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    if (documents.FilePath != null)
                    {
                        string serverPath = _hostingEnvironment.ContentRootPath + @"\" + documents.FilePath;
                        document.FileData = System.IO.File.ReadAllBytes(serverPath);
                    }
                    else
                    {
                        document.FileData = documents.IsCompressed.GetValueOrDefault(false) && documents.FileData != null ? DocumentZipUnZip.Unzip(documents.FileData) : documents.FileData;
                    }
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.IsMobileUpload = documents.IsMobileUpload;
                    document.DocumentId = documents.DocumentId;
                }
            }
            if (DocumentsModel.Type == "BlanketOrderAttachment")
            {
                var documents = _context.BlanketOrderAttachment.SingleOrDefault(t => t.BlanketOrderAttachmentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.BlanketOrderAttachmentId;
                }
            }
            if (DocumentsModel.Type == "ContractDistributionAttachment")
            {
                var documents = _context.ContractDistributionAttachment.SingleOrDefault(t => t.ContractDistributionAttachmentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.ContractDistributionAttachmentId;
                }
            }
            if (DocumentsModel.Type == "SalesAdhocNovateAttachment")
            {
                var documents = _context.SalesAdhocNovateAttachment.SingleOrDefault(t => t.SalesAdhocNovateAttachmentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.SalesAdhocNovateAttachmentId;
                }
            }
            if (DocumentsModel.Type == "CommonFieldsProductionMachineDocument")
            {
                var documents = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(t => t.MachineDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.MachineDocumentId;
                }
            }
            if (DocumentsModel.Type == "TransferBalanceQtyDocument")
            {
                var documents = _context.TransferBalanceQtyDocument.SingleOrDefault(t => t.TransferBalanceQtyDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.TransferBalanceQtyDocumentId;
                }
            }
            if (DocumentsModel.Type == "QuotationHistoryDocument")
            {
                var documents = _context.QuotationHistoryDocument.SingleOrDefault(t => t.QuotationHistoryDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.QuotationHistoryDocumentId;
                }
            }
            if (DocumentsModel.Type == "PurchaseOrderDocument")
            {
                var documents = _context.PurchaseOrderDocument.SingleOrDefault(t => t.PurchaseOrderDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.PurchaseOrderDocumentId;
                }
            }
            if (DocumentsModel.Type == "ProductRecipeDocument")
            {
                var documents = _context.ProductRecipeDocument.SingleOrDefault(t => t.ProductRecipeDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.ProductRecipeDocumentId;
                }
            }
            if (DocumentsModel.Type == "PackagingHistoryItemLineDocument")
            {
                var documents = _context.PackagingHistoryItemLineDocument.SingleOrDefault(t => t.PackagingHistoryItemLineDocumentId == DocumentsModel.DocumentID);

                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.PackagingHistoryItemLineDocumentId;
                }
            }
            if (DocumentsModel.Type == "IpirlineDocument")
            {
                var documents = _context.IpirlineDocument.SingleOrDefault(t => t.IpirlineDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.IpirlineDocumentId;
                }
            }
            if (DocumentsModel.Type == "FinishProductGeneralInfoDocument")
            {
                var documents = _context.FinishProductGeneralInfoDocument.SingleOrDefault(t => t.FinishProductGeneralInfoDocumentId == DocumentsModel.DocumentID);
                if (documents != null)
                {
                    document.FileData = documents.FileData;
                    document.ContentType = documents.ContentType;
                    document.FileName = documents.FileName;
                    document.DocumentId = documents.FinishProductGeneralInfoDocumentId;
                }
            }
            var plainTextBytes = "The file is either older version or program not able to read. please download  & view in your local computer.";
            if (document != null)
            {
                if (document.IsMobileUpload == true)
                {
                    plainTextBytes = document.FileName;
                }
                else
                {
                    string[] subs = document.FileName.Split('.');

                    if (subs[1] == "msg")
                    {
                        return OutLookMailDocuments(document, plainTextBytes);
                    }
                    else
                    {
                        if (document.ContentType == "application/msword" || document.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                        {
                            return ParseDOCX(document);
                            //return ConvertDocumentWordToPdf(document);
                        }
                        if (document.FileData != null)
                        {
                            if (document.ContentType == "application/pdf" || document.ContentType.Contains("image"))
                            {
                                base64String = Convert.ToBase64String(document.FileData, 0, document.FileData.Length);
                                var filebaseString = "data:" + document.ContentType + ";base64," + base64String;
                                return filebaseString;
                            }
                            else if (document.ContentType == "text/html" || document.ContentType == "text/plain")
                            {
                                var filebaseString = System.Text.Encoding.UTF8.GetString(document.FileData);
                                return filebaseString;
                            }
                        }
                    }
                }
            }
            // base64String =System.Convert.ToBase64String(plainTextBytes);
            return plainTextBytes;
        }
        private string OutLookMailDocuments(Documents doc, string plainTextBytes)
        {
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "OutLookMsg";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var filePath = serverPath + @"\" + doc.DocumentId;
            var compressedData = doc.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(doc.FileData) : doc.FileData;
            System.IO.File.WriteAllBytes((string)filePath, compressedData);
            using (var msg = new Storage.Message(filePath))
            {
                plainTextBytes = msg.BodyHtml;
                var attachments = msg.Attachments;
                if (attachments != null && attachments.Count > 0)
                {
                    attachments.ForEach(a =>
                    {
                        var attachment = a as Storage.Message.Attachment;
                        if (attachment.ContentId != null && attachment.IsInline == true)
                        {
                            var contentId = "cid:" + attachment.ContentId;
                            if (plainTextBytes.Contains(contentId))
                            {
                                var contentType = "image/" + attachment.FileName.Split('.').Last();
                                var base64String = Convert.ToBase64String(attachment.Data, 0, attachment.Data.Length);
                                var filebaseString = "data:" + contentType + ";base64," + base64String;
                                plainTextBytes = plainTextBytes.Replace(contentId, filebaseString);
                            }
                        }
                    });
                }
            }
            System.IO.File.Delete(filePath);
            return plainTextBytes;
        }
        [HttpPost]
        [Route("ConvertDocumentWordToPdf")]
        public IActionResult ConvertDocumentWordToPdf(DocumentsModel documents)
        {
            try
            {
                var document = _context.Documents.FirstOrDefault(w => w.DocumentId == documents.DocumentID);
                Word2Pdf objWorPdf = new Word2Pdf();
                string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
                string newFolderName = "ConvertWordToPdf";
                string serverPath = System.IO.Path.Combine(folderName, newFolderName);
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                object FromLocation = folderName + @"\" + newFolderName + @"\" + document.FileName;
                var compressedData = document.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(document.FileData) : document.FileData;
                System.IO.File.WriteAllBytes((string)FromLocation, compressedData);


                string FileExtension = Path.GetExtension(document.FileName);
                string ChangeExtension = document.FileName.Replace(FileExtension, ".pdf");
                string ToLocation = folderName + @"\" + newFolderName + @"\" + ChangeExtension;
                if (FileExtension == ".doc" || FileExtension == ".docx")
                {

                    objWorPdf.InputLocation = FromLocation;
                    objWorPdf.OutputLocation = ToLocation;



                    objWorPdf.Word2PdfCOnversion();
                }
                System.IO.File.Delete((string)FromLocation);
                FileStream stream = System.IO.File.OpenRead(ToLocation);
                var br = new BinaryReader(stream);
                Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
                Stream streams = new MemoryStream(documentStream);
                stream.Close();
                System.IO.File.Delete((string)ToLocation);
                return Ok(streams);
            }
            catch (Exception ex)
            {
                throw new AppException("File Error", ex);
            }
        }
        [HttpPost]
        [Route("ScanByteToPDF")]
        public ScanDocumentModel ScanByteToPDF(ScanDocumentModel scanDocumentModel)
        {

            var SessionId = scanDocumentModel.SessionId ?? Guid.NewGuid();
            string fileName = scanDocumentModel.DocumentName == string.Empty ? SessionId + ".pdf" : scanDocumentModel.DocumentName + ".pdf";
            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + fileName;
            var folderpath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + scanDocumentModel.FolderName;
            PdfDocument pdfDocument = new PdfDocument(new PdfWriter(serverPath));
            Document document = new Document(pdfDocument);
            int i = 0;

            foreach (string filename in Directory.GetFiles(folderpath).OrderBy(o => o))
            {
                var imageFile = System.IO.File.ReadAllBytes(filename);

                var image = new Image(ImageDataFactory.Create(imageFile));
                pdfDocument.AddNewPage(new iText.Kernel.Geom.PageSize(image.GetImageWidth(), image.GetImageHeight()));
                image.SetFixedPosition(i + 1, 0, 0);
                document.Add(image);
                i++;

            }


            //foreach (var Scanimage in scanDocumentModel.Items)
            //{
            //    //ImageData imageData = ImageDataFactory.Create(Scanimage.ImageByte);
            //    //Image image = new Image(imageData);

            //    var image = new Image(ImageDataFactory.Create(Scanimage.ImageByte));
            //    pdfDocument.AddNewPage(new iText.Kernel.Geom.PageSize(image.GetImageWidth(), image.GetImageHeight()));
            //    image.SetFixedPosition(i + 1, 0, 0);
            //    document.Add(image);
            //    i++;
            //    //image.SetBackgroundColor(iText.Kernel.Colors.Color.ConvertCmykToRgb(iText.Kernel.Colors.DeviceCmyk.MakeLighter));
            //    //image.SetWidth(pdfDocument.GetDefaultPageSize().GetWidth() - 50);
            //    //image.SetAutoScaleHeight(true);
            //    //document.Add(image);
            //    //if (scanDocumentModel.Items.Count - 1 != scanDocumentModel.Items.IndexOf(Scanimage))
            //    //{
            //    //    pdfDocument.AddNewPage();
            //    //}
            //}

            pdfDocument.Close();

            var pdfFile = System.IO.File.ReadAllBytes(serverPath);

            if (scanDocumentModel.IsProductionDocument)
            {
                var documentItem = _context.Documents.FirstOrDefault(d => d.SessionId == SessionId);

                var productionDocumentItem = _context.ProductionDocument.FirstOrDefault(d => d.SessionId == SessionId);
                if (documentItem != null)
                {
                    _context.Documents.Remove(documentItem);
                    _context.ProductionDocument.Remove(productionDocumentItem);
                }
                ProductionDocument productionDocument = new ProductionDocument
                {
                    DocumentName = fileName,
                    SessionId = SessionId,
                    FileProfileTypeId = scanDocumentModel.FileProfileTypeID,
                    StatusCodeId = 1,
                    AddedByUserId = scanDocumentModel.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.ProductionDocument.Add(productionDocument);
                _context.SaveChanges();
            }

            if (scanDocumentModel.RetakeID > 0)
            {
                var dItem = _context.Documents.FirstOrDefault(d => d.DocumentId == scanDocumentModel.RetakeID);
                _context.Documents.Remove(dItem);
            }

            var documents = new Documents
            {
                FileName = fileName,
                DisplayName = fileName,
                ContentType = "application/pdf",
                FileData = DocumentZipUnZip.Zip(pdfFile),
                FileSize = pdfFile.Length,
                //Description = files.desc
                UploadDate = DateTime.Now,
                SessionId = SessionId,
                IsMobileUpload = true,
                IsLatest = true,
                IsTemp = true,
                IsCompressed = true,
                FilterProfileTypeId = scanDocumentModel.FileProfileTypeID,
                AddedByUserId = scanDocumentModel.AddedByUserID,
                AddedDate = DateTime.Now,
            };

            if (scanDocumentModel.FileProfileTypeID > 0)
            {
                string profileNo = "";
                if (scanDocumentModel.IsFileProfileSelect)
                {
                    var company = _context.Company.FirstOrDefault(d => d.CompanyId == scanDocumentModel.CompanyId);
                    var department = _context.Department.FirstOrDefault(d => d.DepartmentId == scanDocumentModel.DepartmentId);
                    var section = _context.Section.FirstOrDefault(d => d.SectionId == scanDocumentModel.SectionId);
                    var subSection = _context.SubSection.FirstOrDefault(d => d.SubSectionId == scanDocumentModel.SubSectionId);
                    profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
                    {
                        ProfileID = scanDocumentModel.ProfileId,
                        AddedByUserID = scanDocumentModel.AddedByUserID,
                        StatusCodeID = 710,
                        DepartmentName = department?.Name,
                        CompanyCode = company?.CompanyCode,
                        SectionName = section?.Name,
                        SubSectionName = subSection?.Name,
                        DepartmentId = scanDocumentModel.DepartmentId,
                        PlantID = scanDocumentModel.CompanyId,
                        SectionId = scanDocumentModel.SectionId,
                        SubSectionId = scanDocumentModel.SubSectionId,
                        ScreenAutoNumberId = scanDocumentModel.FileProfileTypeID,
                    });
                }
                else
                {
                    var fileProfileType = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == scanDocumentModel.FileProfileTypeID);

                    if (fileProfileType != null)
                    {
                        if (scanDocumentModel.AddedByUserID != null)
                        {
                            profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, AddedByUserID = scanDocumentModel.AddedByUserID, StatusCodeID = 710, });

                        }
                        else
                        {
                            profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, StatusCodeID = 710, });
                        }
                    }

                }
                documents.ProfileNo = profileNo;
            }
            _context.Documents.Add(documents);
            _context.SaveChanges();
            scanDocumentModel.SessionId = documents.SessionId;
            scanDocumentModel.DocumentID = documents.DocumentId;
            scanDocumentModel.Items = new List<ItemModel>();
            return scanDocumentModel;
        }

        [HttpGet]
        [Route("GetDocumentById")]
        public DocumentsModel GetDocumentById(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            DocumentsModel documentsModel = new DocumentsModel();
            if (document != null)
            {
                documentsModel.DocumentID = document.DocumentId;
                documentsModel.SessionID = document.SessionId;
                documentsModel.UniqueSessionId = document.UniqueSessionId;
                documentsModel.IsLatest = document.IsLatest;
                documentsModel.AddedByUserID = document.AddedByUserId;
                documentsModel.FilePath = document.FilePath;
              
            }
            return documentsModel;
        }
        [HttpPost]
        [Route("ScanImages")]
        public ItemModel ScanImages(ItemModel scanDocumentModel)
        {
            var path = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + scanDocumentModel.FolderName;
            if (!(Directory.Exists(path)))
            {
                Directory.CreateDirectory(path);
            }
            var fileName = path + @"\" + scanDocumentModel.FileName;
            System.IO.File.WriteAllBytes(fileName, scanDocumentModel.ImageByte);

            return scanDocumentModel;
        }

        [HttpGet]
        [Route("GetMobileDocuments")]
        public List<ScanDocumentModel> GetMobileDocuments(int id)
        {
            List<ScanDocumentModel> documentsModels = new List<ScanDocumentModel>();
            var documents = _context.Documents.Include(f => f.FilterProfileType).Select(s => new { s.DocumentId, s.FileName, s.FilterProfileType, s.SessionId, s.AddedByUserId, s.IsMobileUpload }).Where(i => i.AddedByUserId == id && i.IsMobileUpload == true).ToList();

            foreach (var item in documents)
            {
                ScanDocumentModel scanDocumentModel = new ScanDocumentModel();
                scanDocumentModel.DocumentID = item.DocumentId;
                scanDocumentModel.DocumentName = item.FileName;
                scanDocumentModel.ProfileTypeName = item.FilterProfileType != null ? item.FilterProfileType.Name : "";
                scanDocumentModel.DocumentPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + item.FileName + ".pdf";
                scanDocumentModel.SessionId = item.SessionId != null ? item.SessionId.Value : Guid.NewGuid();
                documentsModels.Add(scanDocumentModel);
            }
            return documentsModels;
        }

        public static string ParseDOCX(Documents fileInfo)
        {
            try
            {
                byte[] byteArray = fileInfo.FileData;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    using (WordprocessingDocument wDoc =
                                                WordprocessingDocument.Open(memoryStream, true))
                    {
                        int imageCounter = 0;
                        var pageTitle = fileInfo.FileName;
                        var part = wDoc.CoreFilePropertiesPart;
                        if (part != null)
                            pageTitle = (string)part.GetXDocument()
                                                    .Descendants(DC.title)
                                                    .FirstOrDefault() ?? fileInfo.FileName;
                        MainDocumentPart documentPart = wDoc.MainDocumentPart;
                        var headerParts = wDoc.MainDocumentPart.HeaderParts.ToList().FirstOrDefault()?.Header.OuterXml;
                        //var footerParts = wDoc.MainDocumentPart.FooterParts.ToList().FirstOrDefault()?.Footer.InnerText;

                        WmlToHtmlConverterSettings settings = new WmlToHtmlConverterSettings()
                        {
                            // AdditionalCss = "body { margin: 1cm auto; max-width: 20cm; padding: 0; }",
                            PageTitle = pageTitle,
                            CssClassPrefix = "pt-",
                            FabricateCssClasses = true,
                            RestrictToSupportedLanguages = false,
                            RestrictToSupportedNumberingFormats = false,
                            ImageHandler = imageInfo =>
                            {
                                ++imageCounter;
                                string extension = imageInfo.ContentType.Split('/')[1].ToLower();
                                ImageFormat imageFormat = null;
                                if (extension == "png") imageFormat = ImageFormat.Png;
                                else if (extension == "gif") imageFormat = ImageFormat.Gif;
                                else if (extension == "bmp") imageFormat = ImageFormat.Bmp;
                                else if (extension == "jpeg") imageFormat = ImageFormat.Jpeg;
                                else if (extension == "tiff")
                                {
                                    extension = "gif";
                                    imageFormat = ImageFormat.Gif;
                                }
                                else if (extension == "x-wmf")
                                {
                                    extension = "wmf";
                                    imageFormat = ImageFormat.Wmf;
                                }

                                if (imageFormat == null) return null;

                                string base64 = null;
                                try
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        imageInfo.Bitmap.Save(ms, imageFormat);
                                        var ba = ms.ToArray();
                                        base64 = System.Convert.ToBase64String(ba);
                                    }
                                }
                                catch (System.Runtime.InteropServices.ExternalException)
                                { return null; }

                                ImageFormat format = imageInfo.Bitmap.RawFormat;
                                ImageCodecInfo codec = ImageCodecInfo.GetImageDecoders()
                                                            .First(c => c.FormatID == format.Guid);
                                string mimeType = codec.MimeType;

                                string imageSource =
                                        string.Format("data:{0};base64,{1}", mimeType, base64);

                                XElement img = new XElement(Xhtml.img,
                                        new XAttribute(NoNamespace.src, imageSource),
                                        imageInfo.ImgStyleAttribute,
                                        imageInfo.AltText != null ?
                                            new XAttribute(NoNamespace.alt, imageInfo.AltText) : null);
                                return img;
                            }
                        };
                        XElement htmlElement = WmlToHtmlConverter.ConvertToHtml(wDoc, settings);
                        var html = new XDocument(new XDocumentType("html", null, null, null), htmlElement);
                        var htmlString = html.ToString(SaveOptions.DisableFormatting);
                        return headerParts + htmlString;
                    }
                }
            }
            catch (Exception ex)
            {
                return "The file is either older version or program not able to read. please download  & view in your local computer.";
            }
        }
        [DisableRequestSizeLimit]
        [HttpPost]
        [Route("UploadVideo")]
        public IActionResult UploadVideo(IFormCollection files)
        {
            var SessionId = Guid.NewGuid();
            files.Files.ToList().ForEach(f =>
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + f.FileName;
                var fileName = GetNextFileName(serverPath);
                using FileStream fileStream = new FileStream(fileName, FileMode.Create);

                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                // var compressStream = CompressStream(document);
                //  var de_compressStream = DecompressStream(compressStream);
                var compressedData = DocumentZipUnZip.Zip(document);// Compress(document);
                                                                    //  var de_compress = Decompress(compressedData);
                fs.CopyTo(fileStream);

            });

            return Content(SessionId.ToString());

        }
        public static byte[] CompressStream(byte[] data)
        {
            byte[] compressArray = null;
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (DeflateStream deflateStream = new DeflateStream(memoryStream, CompressionMode.Compress))
                    {
                        deflateStream.Write(data, 0, data.Length);
                    }
                    compressArray = memoryStream.ToArray();
                }
            }
            catch (Exception exception)
            {
                // do something !
            }
            return compressArray;
        }

        public static byte[] DecompressStream(byte[] data)
        {
            byte[] decompressedArray = null;
            try
            {
                using (MemoryStream decompressedStream = new MemoryStream())
                {
                    using (MemoryStream compressStream = new MemoryStream(data))
                    {
                        using (DeflateStream deflateStream = new DeflateStream(compressStream, CompressionMode.Decompress))
                        {
                            deflateStream.CopyTo(decompressedStream);
                        }
                    }
                    decompressedArray = decompressedStream.ToArray();
                }
            }
            catch (Exception exception)
            {
                // do something !
            }

            return decompressedArray;
        }
        private string GetNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(fileName);
            string fileNameOnly = Path.Combine(pathName, Path.GetFileNameWithoutExtension(fileName));
            int i = 0;
            // If the file exists, keep trying until it doesn't
            while (System.IO.File.Exists(fileName))
            {
                i += 1;
                fileName = string.Format("{0}({1}){2}", fileNameOnly, i, extension);
            }
            return fileName;
        }
        [HttpGet]
        [Route("GetVideoFiles")]
        public List<MediaModel> GetVideoFiles()
        {
            List<MediaModel> mediaModels = new List<MediaModel>();
            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\";
            var s = 1;
            foreach (var files in Directory.GetFiles(serverPath))
            {
                MediaModel MediaModel = new MediaModel();
                FileInfo info = new FileInfo(files);
                MediaModel.VideoId = s;
                MediaModel.Caption = info.Name;
                MediaModel.Type = "video";
                MediaModel.Width = 800;
                MediaModel.Height = 600;
                /* DocumentsModel.FileName = Path.GetFileName(info.FullName);
                 DocumentsModel.ContentType = MimeKit.MimeTypes.GetMimeType(info.Name);
                 DocumentsModel.Extension = info.Extension;*/
                MediaModel.Src = Path.GetFileName(info.FullName);
                MediaModel.SrcType = MimeKit.MimeTypes.GetMimeType(info.Name);
                mediaModels.Add(MediaModel);
                s++;
            }
            return mediaModels;
        }
        [HttpPut]
        [Route("DeleteUploadVideo")]
        public MediaModel DeleteUploadVideo(MediaModel value)
        {
            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + value.Src;
            if (System.IO.File.Exists(serverPath))
            {
                System.IO.File.Delete(serverPath);
            }
            return value;
        }


        [HttpPost]
        [Route("InsertUpdateFileProfileTypeDocument")]
        public DocumentListModel InsertFileProfileTypeDocument(DocumentListModel lists)
        {

            if (lists.DocumentList.Count > 0)
            {
                lists.DocumentList.ForEach(value =>
                {
                    var document = _context.Documents.FirstOrDefault(d => d.DocumentId == value.DocumentID);

                    if (document.FilterProfileTypeId != null && document.FilterProfileTypeId != value.FilterProfileTypeId)
                    {
                        var fileProfileType = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == value.FilterProfileTypeId);
                        string profileNo = "";
                        if (fileProfileType != null)
                        {
                            if (value.AddedByUserID != null)
                            {
                                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, });

                            }
                            else
                            {
                                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, StatusCodeID = 710, });
                            }
                            value.FileProfileTypeName = fileProfileType.Name;
                        }
                        var documents = new Documents
                        {
                            FileName = document.FileName,
                            ContentType = document.ContentType,
                            FileData = document.FileData,
                            FileSize = document.FileSize,
                            UploadDate = DateTime.Now,
                            SessionId = document.SessionId,
                            FilterProfileTypeId = value.FilterProfileTypeId,
                            ScreenId = document.ScreenId,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedByUserId = value.AddedByUserID,
                            ExpiryDate = document.ExpiryDate,
                            IsWikiDraft = document.IsWikiDraft,
                            IsCompressed = document.IsCompressed,
                            IsHeaderImage = document.IsHeaderImage,
                            FileIndex = 0,
                            AddedDate = DateTime.Now,
                            Description = lists.Description,
                        };

                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                        value.DocumentID = documents.DocumentId;

                    }
                    else
                    {
                        if (document.FilterProfileTypeId == null)
                        {
                            var fileProfileType = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == lists.FilterProfileTypeId);
                            string profileNo = "";
                            if (fileProfileType != null)
                            {
                                if (lists.AddedByUserID != null)
                                {
                                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, AddedByUserID = lists.AddedByUserID, StatusCodeID = 710, });

                                }
                                else
                                {
                                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = fileProfileType.ProfileId, Title = fileProfileType.Name, StatusCodeID = 710, });
                                }
                                value.FileProfileTypeName = fileProfileType.Name;
                            }
                            document.ProfileNo = profileNo;



                        }
                        var linkdocument = _context.LinkFileProfileTypeDocument.FirstOrDefault(c => c.TransactionSessionId == document.SessionId && c.DocumentId == document.DocumentId && c.FileProfileTypeId == lists.FilterProfileTypeId);
                        try
                        {
                            if (linkdocument == null)
                            {
                                LinkFileProfileTypeDocument linkFileProfileTypeDocument = new LinkFileProfileTypeDocument
                                {
                                    TransactionSessionId = document.SessionId,
                                    DocumentId = value.DocumentID,
                                    FileProfileTypeId = lists.FilterProfileTypeId,
                                    Description = lists.Description,
                                };
                                _context.LinkFileProfileTypeDocument.Add(linkFileProfileTypeDocument);
                                _context.SaveChanges();

                            }
                        }
                        catch (Exception ex)
                        {
                            throw new AppException("Document is already linked.", ex);
                        }
                        document.IsLatest = true;
                        document.FilterProfileTypeId = value.FilterProfileTypeId;
                        document.AddedByUserId = value.AddedByUserID;
                        document.AddedDate = value.AddedDate;
                        _context.SaveChanges();
                    }
                });
            }
            return lists;
        }
        [HttpPost]
        [Route("InsertLinkFileProfileType")]
        public LinkFileProfileTypeDocumentModel InsertLinkFileProfileType(LinkFileProfileTypeDocumentModel value)
        {
            var document = _context.LinkFileProfileTypeDocument.FirstOrDefault(c => c.TransactionSessionId == value.TransactionSessionId && c.DocumentId == value.DocumentId);
            var selectedDocument = _context.Documents.FirstOrDefault(f => f.DocumentId == value.DocumentId);
            var errormessage = "Document Already Linked";
            bool? wikiStatusTypes = null;
            bool? isWikiFlag = null;
            if (value.WikiStatusType == "Draft")
            {
                wikiStatusTypes = true;
            }
            if (value.WikiStatusType == "Wiki")
            {
                isWikiFlag = true;
            }
            try
            {
                if (document == null)
                {
                    if (value.TaskID == null && value.PortfolioLineId == null)
                    {
                        LinkFileProfileTypeDocument linkFileProfileTypeDocument = new LinkFileProfileTypeDocument
                        {
                            TransactionSessionId = value.TransactionSessionId,
                            DocumentId = value.DocumentId,
                            AddedDate = DateTime.Now,
                            AddedByUserId = value.AddedByUserID,
                            FileProfileTypeId = value.FileProfileTypeId,
                            FolderId = value.FolderId,
                            Description = value.Description,
                            IsWikiDraft = wikiStatusTypes,
                            IsWiki = isWikiFlag,
                        };
                        _context.LinkFileProfileTypeDocument.Add(linkFileProfileTypeDocument);
                        _context.SaveChanges();
                        value.LinkFileProfileTypeDocumentId = linkFileProfileTypeDocument.LinkFileProfileTypeDocumentId;

                    }

                    if (value.PortfolioLineId > 0 && value.IsPortfolioAttachment == true)
                    {
                        var portfolio = _context.PortfolioLine.Where(f => f.PortfolioLineId == value.PortfolioLineId).FirstOrDefault();
                        var sageNo = "";
                        var companyName = "";
                        var typeofportfolio = "";
                        var versionNo = "";
                        if (portfolio != null)
                        {
                            if (portfolio.PortfolioId != null)
                            {
                                var portfoliodata = _context.Portfolio.Include(f => f.Company).Include(e => e.OnBehalf).Include(e => e.OnBehalf).Include(t => t.PortfolioType).FirstOrDefault(f => f.PortfolioId == portfolio.PortfolioId);
                                if (portfoliodata != null)
                                {
                                    sageNo = _context.Employee.FirstOrDefault(f => f.UserId == portfoliodata.OnBehalfId)?.SageId;

                                    companyName = portfoliodata?.Company?.PlantCode;
                                    typeofportfolio = portfoliodata?.PortfolioType?.Value;
                                    versionNo = portfolio?.VersionNo;
                                }
                            }
                        }
                        var existportfolioAttachment = _context.PortfolioAttachment.FirstOrDefault(t => t.PortfolioLineId == value.PortfolioLineId && t.DocumentId == value.DocumentId);
                        if (existportfolioAttachment == null)
                        {
                            var portfolioAttachment = new PortfolioAttachment
                            {
                                DocumentId = value.DocumentId,
                                IsLatest = true,
                                IsLocked = false,

                                UploadedDate = DateTime.Now,
                                UploadedByUserId = value.AddedByUserID,
                                VersionNo = "1",
                                PortfolioLineId = value.PortfolioLineId,

                                FileName = typeofportfolio + " " + companyName + " " + sageNo + " " + DateTime.Now.Date + " " + versionNo,
                                //TaskMasterId = value.TaskID
                            };
                            _context.PortfolioAttachment.Add(portfolioAttachment);
                            _context.SaveChanges();
                        }
                    }

                    var task = _context.TaskMaster.FirstOrDefault(s => s.TaskId == value.TaskID);
                    List<long?> readonlyUserIds = new List<long?>();
                    List<long?> readWriteUserIds = new List<long?>();
                    if (value.TaskID != null)
                    {
                        bool? isFromProfileDocument = false;
                        if (selectedDocument.IsMainTask != true)
                        {
                            var existattach = _context.TaskAttachment.Where(t => t.TaskMasterId == value.TaskID && t.DocumentId == value.DocumentId)?.FirstOrDefault();
                            if (existattach == null)
                            {
                                var taskAttach = new TaskAttachment
                                {
                                    DocumentId = value.DocumentId,
                                    CheckInDescription = value.Description,
                                    TaskMasterId = value.TaskID,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = value.AddedByUserID,
                                    VersionNo = "1",
                                    FolderId = value.FolderId,
                                    FileProfieTypeId = value.FileProfileTypeId,
                                    IsLatestCommentAttachment = value.IsLatestCommentAttachment,

                                };
                                _context.TaskAttachment.Add(taskAttach);
                                _context.SaveChanges();
                                var notes = new Notes
                                {
                                    Notes1 = task?.Title,
                                    Link = value.Link + "id=" + task?.TaskId + "&taskid=" + value.TaskID,
                                    DocumentId = value.DocumentId,
                                    SessionId = task?.SessionId,
                                    AddedByUserId = value.AddedByUserID.Value,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = 1,
                                };
                                _context.Notes.Add(notes);
                                _context.SaveChanges();
                            }
                            else
                            {
                                if (value.IsLatestCommentAttachment == true)
                                {
                                    existattach.IsLatestCommentAttachment = value.IsLatestCommentAttachment;
                                    _context.SaveChanges();
                                }
                            }
                        }
                        else if (selectedDocument.IsMainTask == true)
                        {
                            errormessage = "This is Main Document, So Cannot link";
                            throw new Exception("This is Main Document, So Cannot link");

                        }
                        if (value.ReadOnlyUserID != null)
                        {
                            readonlyUserIds = value.ReadOnlyUserID;
                        }
                        if (value.ReadWriteUserID != null)
                        {
                            readWriteUserIds = value.ReadWriteUserID;
                        }
                        //readWriteUserIds = _context.TaskAssigned.Where(t => t.TaskId == value.TaskID).Select(s => s.TaskOwnerId).ToList();
                        //readonlyUserIds = _context.TaskMembers.Where(t => t.TaskId == value.TaskID).Select(s => s.AssignedCc).ToList();
                        //readWriteUserIds.Add(task.OwnerId);

                        if (value.DocumentId != null)
                        {
                            var rights = _context.DocumentRights.Where(d => d.DocumentId == value.DocumentId).ToList();

                            if (rights.Count() > 0)
                            {
                                _context.DocumentRights.RemoveRange(rights);
                                _context.SaveChanges();
                            }
                            //var file = _context.Documents.Select(s => new { s.DocumentId, s.DocumentRights }).FirstOrDefault(d => d.DocumentId == value.DocumentId);

                            if (readonlyUserIds != null)
                            {
                                readonlyUserIds.ForEach(i =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = true,
                                        IsReadWrite = false,
                                        UserId = i,
                                        DocumentId = value.DocumentId,
                                    };
                                    _context.DocumentRights.Add(docAccess);
                                });
                            }
                            if (readWriteUserIds != null)
                            {
                                readWriteUserIds.ForEach(i =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = false,
                                        IsReadWrite = true,
                                        UserId = i,
                                        DocumentId = value.DocumentId,
                                    };
                                    _context.DocumentRights.Add(docAccess);
                                });
                            }
                            _context.SaveChanges();
                        }

                        var taskexist = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskID);
                        if (taskexist != null)
                        {
                            isFromProfileDocument = task.IsFromProfileDocument;
                            if (isFromProfileDocument != null && isFromProfileDocument == true)
                            {

                                var docIds = _context.TaskAttachment.Where(d => d.TaskMasterId == value.TaskID).Select(t => t.DocumentId).ToList();
                                if (docIds != null)
                                {
                                    var existlinkdocu = _context.Documents.FirstOrDefault(d => docIds.Contains(d.DocumentId) && d.TaskId != null);
                                    if (existlinkdocu != null)
                                    {
                                        var DocumentLink = new DocumentLink
                                        {
                                            DocumentId = existlinkdocu.DocumentId,
                                            AddedByUserId = value.AddedByUserID,
                                            AddedDate = DateTime.Now,
                                            StatusCodeId = 1,
                                            LinkDocumentId = value.DocumentId,
                                            FolderId = value.FolderId,
                                            FileProfieTypeId = value.FileProfileTypeId,
                                            DocumentPath = value.FolderId != null ? "Link From Folder" : "Link From DMS Profile Type",

                                        };
                                        _context.DocumentLink.Add(DocumentLink);
                                        _context.SaveChanges();
                                    }


                                }
                                //var existlinkedDocuments = _context.DocumentLink.Where(d => d.DocumentId == value.DocumentId).Select(d => d.LinkDocumentId).ToList();
                                //if (existlinkedDocuments != null && existlinkedDocuments.Count > 0)
                                //{
                                //    existlinkedDocuments.ForEach(f =>
                                //    {

                                //        var existattach = _context.TaskAttachment.Where(t => t.TaskMasterId == value.TaskID && t.DocumentId == f)?.FirstOrDefault();
                                //        if (existattach == null)
                                //        {
                                //            var taskAttached = new TaskAttachment
                                //            {
                                //                DocumentId = f,
                                //                CheckInDescription = value.Description,
                                //                TaskMasterId = value.TaskID,
                                //                IsLatest = true,
                                //                IsLocked = false,
                                //                IsMajorChange = false,
                                //                UploadedDate = DateTime.Now,
                                //                UploadedByUserId = value.AddedByUserID,
                                //                VersionNo = "1",
                                //                IsLatestCommentAttachment=value.IsLatestCommentAttachment

                                //            };
                                //            _context.TaskAttachment.Add(taskAttached);
                                //            _context.SaveChanges();
                                //        }
                                //        else
                                //        {
                                //            if (value.IsLatestCommentAttachment == true)
                                //            {
                                //                existattach.IsLatestCommentAttachment = value.IsLatestCommentAttachment;
                                //                _context.SaveChanges();
                                //            }
                                //        }
                                //    });
                                //}

                                if (selectedDocument != null)
                                {
                                    selectedDocument.IsMainTask = false;
                                    _context.SaveChanges();
                                }
                            }
                        }
                    }
                    if (value.WikiStatusType == "Draft")
                    {
                        var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == value.TransactionSessionId && f.StatusCodeId == 840)?.ApplicationWikiId;
                        if (appWikId > 0)
                        {
                            var appWikiDraftDoc = new AppWikiDraftDoc
                            {
                                DocumentId = value.DocumentId,
                                ApplicationWikiId = appWikId,
                            };
                            _context.AppWikiDraftDoc.Add(appWikiDraftDoc);
                            _context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AppException(errormessage, ex);
            }
            return value;

        }
        [HttpGet]
        [Route("UpdateDocumentsArchive")]
        public void UpdateDocumentsArchive()
        {
            var fileprofileType = _context.FileProfileType.Where(w => w.ShelfLifeDuration != null && w.ShelfLifeDurationId != null).Select(s => new { s.FileProfileTypeId, s.ShelfLifeDuration, s.ShelfLifeDurationId, ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation.CodeValue }).ToList();
            if (fileprofileType != null)
            {
                fileprofileType.ForEach(s =>
                {
                    DateTime? date = null;
                    if (s.ShelfLifeDurationStatus == "Day")
                    {
                        var day = "-" + s.ShelfLifeDuration;
                        date = DateTime.Now.AddDays(Convert.ToInt32(day));
                    }
                    if (s.ShelfLifeDurationStatus == "Month")
                    {
                        var month = "-" + s.ShelfLifeDuration;
                        date = DateTime.Now.AddMonths(Convert.ToInt32(month));
                    }
                    if (s.ShelfLifeDurationStatus == "Year")
                    {
                        var year = "-" + s.ShelfLifeDuration;
                        date = DateTime.Now.AddYears(Convert.ToInt32(year));
                    }
                    if (s.ShelfLifeDurationStatus == "Week")
                    {
                        var day = "-" + (s.ShelfLifeDuration * 7);
                        date = DateTime.Now.AddDays(Convert.ToInt32(day));
                    }
                    var documentIds = _context.Documents.Select(s => new { s.DocumentId, s.ArchiveStatusId, s.FilterProfileTypeId, s.ModifiedDate, s.AddedDate }).Where(w => w.ArchiveStatusId == null && w.FilterProfileTypeId == s.FileProfileTypeId && w.ModifiedDate == null && w.AddedDate.Value.Date <= date.Value.Date).ToList();
                    var documentIds1 = _context.Documents.Select(s => new { s.DocumentId, s.ArchiveStatusId, s.FilterProfileTypeId, s.ModifiedDate, s.AddedDate }).Where(w => w.ArchiveStatusId == null && w.FilterProfileTypeId == s.FileProfileTypeId && w.ModifiedDate != null && w.ModifiedDate.Value.Date <= date.Value.Date).ToList();
                    var lists = documentIds.Concat(documentIds);
                    if (lists != null && lists.ToList().Count > 0)
                    {
                        var query = string.Format("Update Documents Set ArchiveStatusId = 2562 Where DocumentId in" + '(' + "{0}" + ')', string.Join(',', lists.Select(s => s.DocumentId)));
                        var rowaffected = _context.Database.ExecuteSqlRaw(query);
                        if (rowaffected <= 0)
                        {
                            throw new Exception("Failed to update document");
                        }

                        //lists.ToList().ForEach(d =>
                        //{
                        //    d.ArchiveStatusId = 2562;
                        //});
                        //_context.SaveChanges();
                    }
                });
            }

        }

        [HttpGet]
        [Route("GetDocumentTypes")]
        public List<string> GetDocumentTypes()
        {
            List<string> documentTypes = new List<string>();
            documentTypes = _context.Documents.Select(d => d.ContentType).Distinct().ToList();
            return documentTypes;
        }

        [HttpGet]
        [Route("GetPortfolioAttachmentsBySessionID")]
        public List<DocumentsModel> GetPortfolioAttachmentsBySessionID(Guid? sessionId, int? userId, int? type)
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var TotalDocuments = _context.Documents.Where(s => s.SessionId == sessionId).Count();

            var docIds = _context.PortfolioAttachment.Where(p => p.PortfolioLineId == type && p.IsLatest == true).Select(p => p.DocumentId).ToList();

            var linkdocIds = _context.LinkFileProfileTypeDocument.Where(s => s.TransactionSessionId == sessionId).Select(s => s.DocumentId).ToList();
            if (linkdocIds != null && linkdocIds.Count > 0)
            {
                docIds.AddRange(linkdocIds);
            }


            var query = _context.Documents.Include(c => c.FilterProfileType).
           Select(s => new
           {
               s.SessionId,
               s.DocumentId,
               s.FileName,
               s.ContentType,
               s.FileSize,
               s.UploadDate,
               s.FilterProfileTypeId,
               s.DocumentParentId,
               s.TableName,
               s.ExpiryDate,
               s.AddedByUserId,
               s.ModifiedByUserId,
               s.ModifiedDate,
               IsLocked = s.IsLocked,
               LockedByUserId = s.LockedByUserId,
               LockedDate = s.LockedDate,
               s.AddedDate,
               s.DepartmentId,
               s.WikiId,
               s.Extension,
               s.CategoryId,
               s.DocumentType,
               s.DisplayName,
               s.LinkId,
               s.IsSpecialFile,
               s.IsTemp,
               s.ReferenceNumber,
               s.Description,
               s.StatusCodeId,
               s.ScreenId,
               FilterProfileType = s.FilterProfileType,
               s.ProfileNo,
               s.IsLatest,
               s.IsWikiDraft,
               s.IsMobileUpload,
               s.IsWikiDraftDelete,
               s.IsCompressed,
               s.IsVideoFile,
               s.CloseDocumentId,
               s.IsPrint,
           }).Where(l => docIds.Contains(l.DocumentId)).OrderByDescending(o => o.DocumentId).AsNoTracking().ToList();
            if (query != null && query.Count > 0)
            {
                var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                query.ForEach(s =>
                {
                    DocumentsModel documentsModel = new DocumentsModel();
                    documentsModel.SessionID = s.SessionId;
                    documentsModel.DocumentID = s.DocumentId;
                    documentsModel.FileName = s.FileName;
                    documentsModel.ContentType = s.ContentType;
                    documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                    documentsModel.ScreenID = s.ScreenId;
                    documentsModel.Uploaded = true;
                    documentsModel.UploadDate = s.UploadDate;
                    documentsModel.IsImage = s.ContentType.ToLower().Contains("image") ? true : false;
                    documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                    documentsModel.FileProfileTypeName = s.FilterProfileType != null ? s.FilterProfileType.Name : string.Empty;
                    documentsModel.ProfileNo = s.ProfileNo;
                    documentsModel.ProfileID = s.FilterProfileType != null ? s.FilterProfileType.ProfileId : -1;
                    documentsModel.DocumentParentId = s.DocumentParentId;
                    documentsModel.IsLatest = s.IsLatest;
                    documentsModel.TotalDocument = TotalDocuments + 1;
                    documentsModel.ExpiryDate = s.ExpiryDate;
                    documentsModel.IsLocked = s.IsLocked;
                    documentsModel.Type = "Document";
                    documentsModel.AddedByUserID = s.AddedByUserId;
                    documentsModel.IsExpiryDate = s.FilterProfileType?.IsExpiryDate;
                    documentsModel.IsWikiDraft = s.IsWikiDraft;
                    documentsModel.IsMobileUpload = s.IsMobileUpload;
                    documentsModel.IsCompressed = s.IsCompressed;
                    documentsModel.IsVideoFile = s.IsVideoFile;
                    documentsModel.CloseDocumentId = s.CloseDocumentId;
                    documentsModel.isDocumentAccess = s.FilterProfileType?.IsDocumentAccess;
                    documentsModel.IsPrint = s.IsPrint;
                    if (documentsModel.IsExpiryDate == false || documentsModel.IsExpiryDate == null)
                    {
                        documentsModel.ExpiryDate = null;
                    }
                    var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                    List<FileProfileType> FolderPathLists = new List<FileProfileType>();
                    if (fileProfileTypeData != null)
                    {
                        FolderPathLists = GetAllFileProfileTypeNameAll(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                        FolderPathLists.Add(fileProfileTypeData);
                        documentsModel.FileProfileTypeParentId = FolderPathLists.FirstOrDefault()?.FileProfileTypeId;
                        documentsModel.FileProfileTypeName = string.Join(" / ", FolderPathLists.Select(s => s.Name).ToList());
                    }
                    documentsModels.Add(documentsModel);

                });

            }

            List<long?> filterProfileTypeIds = documentsModels.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.AsNoTracking().Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            documentsModels.ForEach(q =>
            {
                if (q.AddedByUserID == userId)
                {
                    q.DocumentPermissionData = new DocumentPermissionModel
                    {
                        IsUpdateDocument = true,
                        IsCreateDocument = true,
                        IsRead = true,
                        IsDelete = true,
                        IsDocumentAccess = true,

                    };
                }
                else
                {
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();

                    q.DocumentPermissionData = new DocumentPermissionModel();
                    if (roles.Count > 0)
                    {
                        q.DocumentPermissionData = new DocumentPermissionModel();
                        var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData = new DocumentPermissionModel
                            {
                                IsUpdateDocument = false,
                                IsCreateDocument = false,
                                IsRead = false,
                                IsDelete = false,
                                IsDocumentAccess = false,

                            };
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData = new DocumentPermissionModel
                        {
                            IsUpdateDocument = true,
                            IsCreateDocument = true,
                            IsRead = true,
                            IsDelete = true,
                            IsDocumentAccess = true,

                        };
                    }
                }
            });
            return documentsModels;
        }
        [HttpPost]
        [Route("UploadDocumentFromProfileReservedNumber")]
        public IActionResult UploadDocumentFromProfileReservedNumber(IFormCollection files)
        {
            long? addedByUserId = null;
            long? fileProfileTypeId = null;
            DateTime? expiryDate = new DateTime?();
            var sessionID = new Guid(files["sessionID"].ToString());
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            if (userExits > 0)
            {
                var userId = files["userId"].ToString();
                if (userId != null)
                {
                    addedByUserId = Convert.ToInt64(userId);
                }
                var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var wikiStatusType = files["wikiStatusType"].ToString();
                var description = files["description"].ToString();
                //var fileProfileType = Convert.ToInt32(files["fileProfileTypeId"].ToString());
                var fileProfileType = files["fileProfileTypeId"].ToString();
                if (fileProfileType != null)
                {
                    fileProfileTypeId = Convert.ToInt64(fileProfileType);
                }
                var screenID = files["screeenID"].ToString();
                var expiry = files["expiryDate"].ToString();
                if (expiry != "null" && expiry != null)
                {
                    expiryDate = Convert.ToDateTime(files["expiryDate"].ToString());
                }
                else if (expiry == "null")
                {
                    expiryDate = null;
                }
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                string profileNo = files["profileNo"].ToString();

                bool? wikiStatusTypes = null;
                bool? isWikiFlag = null;
                if (wikiStatusType == "Draft")
                {
                    wikiStatusTypes = true;
                }
                if (wikiStatusType == "Wiki")
                {
                    isWikiFlag = true;
                }
                int idx = 0;
                files.Files.ToList().ForEach(f =>
                {
                    var file = f;
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        ExpiryDate = expiryDate,
                        IsWikiDraft = wikiStatusTypes,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        IsVideoFile = isVideoFiles,
                        IsMainTask = false,
                        AddedDate = DateTime.Now,
                        Description = description,
                        IsWiki = isWikiFlag,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    if (wikiStatusType == "Draft")
                    {
                        var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == sessionID && f.StatusCodeId == 840)?.ApplicationWikiId;
                        if (appWikId > 0)
                        {
                            var appWikiDraftDoc = new AppWikiDraftDoc
                            {
                                DocumentId = documents.DocumentId,
                                ApplicationWikiId = appWikId,
                            };
                            _context.AppWikiDraftDoc.Add(appWikiDraftDoc);
                            _context.SaveChanges();
                        }
                    }

                    idx++;
                });
            }
            return Content(sessionID.ToString());

        }

        [HttpPost]
        [Route("UploadPortfolioDocument")]
        public IActionResult UploadPortfolioDocument(IFormCollection files)
        {
            long? addedByUserId = null;
            long? portfolioId = null;

            DateTime? expiryDate = new DateTime?();
            bool? isFromcommentAttachment = new bool?();
            isFromcommentAttachment = false;
            var sessionID = new Guid(files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            if (userId != null)
            {
                addedByUserId = Convert.ToInt64(userId);
            }
            var portfolioLineId = files["portfolioLineId"].ToString();
            if (portfolioLineId != "null")
            {
                portfolioId = Convert.ToInt64(portfolioLineId);
            }
            //var taskMaster = _context.TaskMaster.FirstOrDefault(w => w.TaskId == TaskID.GetValueOrDefault(0));
            // var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
            var videoFiles = files["isVideoFile"].ToString().Split(",");

            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var screenID = files["screeenID"].ToString();
            var description = files["description"].ToString();
            //var readWriteUserID = files["readWriteUserID"].ToString();
            //var isFromComment = files["isFromComment"].ToString();
            //var link = files["link"].ToString();

            var expiry = files["expiryDate"].ToString();
            if (expiry != "null" && expiry != null)
            {
                expiryDate = Convert.ToDateTime(files["expiryDate"].ToString());
            }
            else if (expiry == "null")
            {
                expiryDate = null;
            }
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                if (addedByUserId != null)
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, AddedByUserID = addedByUserId, StatusCodeID = 710, });

                }
                else
                {
                    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, StatusCodeID = 710, });
                }
            }

            int idx = 0;
            var documentlinkDoc = new long?();
            var portfolio = _context.PortfolioLine.Where(f => f.PortfolioLineId == portfolioId).FirstOrDefault();
            var sageNo = "";
            var companyName = "";
            var typeofportfolio = "";
            var versionNo = "";
            if (portfolio != null)
            {
                if (portfolio.PortfolioId != null)
                {
                    var portfoliodata = _context.Portfolio.Include(f => f.Company).Include(e => e.OnBehalf).Include(e => e.OnBehalf).Include(t => t.PortfolioType).FirstOrDefault(f => f.PortfolioId == portfolio.PortfolioId);
                    if (portfoliodata != null)
                    {
                        sageNo = _context.Employee.FirstOrDefault(f => f.UserId == portfoliodata.OnBehalfId)?.SageId;

                        companyName = portfoliodata?.Company?.PlantCode;
                        typeofportfolio = portfoliodata?.PortfolioType?.Value;
                        versionNo = portfolio?.VersionNo;
                    }
                }
            }
            files.Files.ToList().ForEach(f =>
            {
                var FileName = "[" + typeofportfolio + "]/" + " [ " + companyName + "] [" + sageNo + "] [" + DateTime.Now.Date.ToString("dd/MM/yyyy") + "] [" + versionNo + "]" + f.FileName;
                var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                var file = f;
                var fileName1 = sessionID + "." + FileName;
                if (isVideoFile)
                {
                    var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                    using (var targetStream = System.IO.File.Create(serverPath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var existdocument = _context.Documents.FirstOrDefault(f => f.FileName == FileName);
                    if (existdocument == null)
                    {
                        var documents = new Documents
                        {
                            FileName = FileName,
                            ContentType = file.ContentType,
                            FileData = null,
                            FileSize = file.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionID,
                            FilterProfileTypeId = fileProfileTypeId,
                            ScreenId = screenID,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedByUserId = addedByUserId,
                            ExpiryDate = expiryDate,
                            Description = description,
                            IsCompressed = true,

                            FileIndex = 0,
                            IsVideoFile = true,
                            AddedDate = DateTime.Now,

                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("This Format Document Already Exist, Please Change The version");
                    }

                }
                else
                {
                    var fs = file.OpenReadStream();
                    var br = new BinaryReader(fs);
                    Byte[] document = br.ReadBytes((Int32)fs.Length);
                    var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                    var existdocument = _context.Documents.FirstOrDefault(f => f.FileName == FileName);
                    if (existdocument == null)
                    {
                        var documents = new Documents
                        {
                            FileName = FileName,
                            ContentType = file.ContentType,
                            FileData = compressedData,
                            FileSize = fs.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionID,
                            FilterProfileTypeId = fileProfileTypeId,
                            ScreenId = screenID,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedByUserId = addedByUserId,
                            ExpiryDate = expiryDate,
                            Description = description,
                            IsCompressed = true,

                            FileIndex = 0,
                            AddedDate = DateTime.Now,

                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("This Format Document Already Exist, Please Change The version");
                    }



                }

                idx++;
            });

            if (sessionID != null)
            {
                var docu = _context.Documents.Select(s => new
                Documents
                { SessionId = s.SessionId, DocumentId = s.DocumentId, IsTemp = false, FileName = s.FileName, DocumentRights = s.DocumentRights })
                    .Where(d => d.SessionId == sessionID).ToList();
                if (portfolioId != null)
                {
                    docu.ForEach(file =>
                    {
                        file.IsTemp = false;
                        if (docu != null)
                        {
                            var existportfolioAttachment = _context.PortfolioAttachment.FirstOrDefault(t => t.PortfolioLineId == portfolioId && t.DocumentId == file.DocumentId);
                            if (existportfolioAttachment == null)
                            {
                                var portfolio = new PortfolioAttachment
                                {
                                    DocumentId = file.DocumentId,
                                    IsLatest = true,
                                    IsLocked = false,

                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = addedByUserId,
                                    VersionNo = "1",
                                    PortfolioLineId = portfolioId,

                                    FileName = "[" + typeofportfolio + "]/" + " [ " + companyName + "] [" + sageNo + "] [" + DateTime.Now.Date.ToString("dd/MM/yyyy") + "] [" + versionNo + "]" + file.FileName,
                                    //TaskMasterId = value.TaskID
                                };
                                _context.PortfolioAttachment.Add(portfolio);
                                _context.SaveChanges();
                            }
                        }




                    });
                }
            }

            _context.SaveChanges();
            return Content(sessionID.ToString());

        }
    }
}