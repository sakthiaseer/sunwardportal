﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskTagController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskTagController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskTag")]
        public List<TaskTagModel> Get()
        {
            var taskTag = _context.TaskTag.Select(s=>new TaskTagModel
            {
                TaskTagID = s.TaskTagId,
                TaskMasterID = s.TaskMasterId,
                TagID = s.TagId,
            }).OrderByDescending(o => o.TaskTagID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<TaskTagModel>>(taskTag);
            return taskTag;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskTag/{id:int}")]
        public ActionResult<TaskTagModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskTag = _context.TaskTag.SingleOrDefault(p => p.TaskTagId == id.Value);
            var result = _mapper.Map<TaskTagModel>(taskTag);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskTagModel> GetData(SearchModel searchModel)
        {
            var taskTag = new TaskTag();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).FirstOrDefault();
                        break;
                    case "Last":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).LastOrDefault();
                        break;
                    case "Next":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).LastOrDefault();
                        break;
                    case "Previous":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).FirstOrDefault();
                        break;
                    case "Last":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).LastOrDefault();
                        break;
                    case "Next":
                        taskTag = _context.TaskTag.OrderBy(o => o.TaskTagId).FirstOrDefault(s => s.TaskTagId > searchModel.Id);
                        break;
                    case "Previous":
                        taskTag = _context.TaskTag.OrderByDescending(o => o.TaskTagId).FirstOrDefault(s => s.TaskTagId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskTagModel>(taskTag);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskTag")]
        public TaskTagModel Post(TaskTagModel value)
        {
            var taskTag = new TaskTag
            {
                TaskTagId=value.TaskTagID,
                TaskMasterId=value.TaskMasterID,
                TagId=value.TagID
                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID,
                //Name = value.Name,
                //Description = value.Description
            };
            _context.TaskTag.Add(taskTag);
            _context.SaveChanges();
            value.TaskTagID = taskTag.TaskTagId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskTag")]
        public TaskTagModel Put(TaskTagModel value)
        {
            var taskTag = _context.TaskTag.SingleOrDefault(p => p.TaskTagId ==value.TaskTagID);
            taskTag.TaskMasterId = value.TaskMasterID;
            taskTag.TagId = value.TagID;
            //taskTag.ModifiedByUserId = value.ModifiedByUserID;
            //project.ModifiedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            //project.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskTag")]
        public void Delete(int id)
        {
            var taskTag = _context.TaskTag.SingleOrDefault(p => p.TagId == id);
            if (taskTag != null)
            {
                _context.TaskTag.Remove(taskTag);
                _context.SaveChanges();
            }
        }
    }
}