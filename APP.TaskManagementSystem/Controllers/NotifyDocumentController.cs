﻿using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NotifyDocumentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private Microsoft.Extensions.Configuration.IConfiguration _config;
        private readonly MailService _mailService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public NotifyDocumentController(CRT_TMSContext context, IMapper mapper, MailService mailService, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _mailService = mailService;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNotifyDocuments")]
        public List<NotifyDocumentModel> Get()
        {
            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            var notifyDocuments = _context.NotifyDocument
              .Include(a => a.AddedByUser)
              .Include(b => b.ModifiedByUser)
              .Include(c => c.StatusCode)
              .Include(d => d.Document)
              .Include(e => e.User)
              .OrderByDescending(o => o.NotifyDocumentId).AsNoTracking().ToList();
            notifyDocuments.ForEach(s =>
            {
                NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel
                {
                    NotifyDocumentId = s.NotifyDocumentId,
                    UserId = s.UserId,
                    UserName = s.User?.NickName,
                    Remarks = s.Remarks,
                    DocumentName = s.Document?.FileName,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,
                    IsUrgent = s.IsUrgent,
                    IsQuote = s.IsQuote,
                    QuoteNotifyId = s.QuoteNotifyId,
                    IsSenderClose = s.IsSenderClose,
                    DueDate = s.DueDate,
                };
                notifyDocumentModels.Add(notifyDocumentModel);

            });
            return notifyDocumentModels;
        }
        [HttpGet]
        [Route("GetLongNotesByNotifyId")]
        public NotifyLongNotesModel GetLongNotesByNotifyId(long? id)
        {
            NotifyLongNotesModel notifyLongNotesModel = new NotifyLongNotesModel();
            var notifylongNote = _context.NotifyLongNotes.Where(f => f.NotifyDocumentId == id).OrderByDescending(s => s.NotifyLongNotesId).FirstOrDefault();
            if (notifylongNote != null)
            {
                notifyLongNotesModel.NotifyDocumentId = notifylongNote.NotifyDocumentId;
                notifyLongNotesModel.DetailNotes = notifylongNote.LongNotes;
                notifyLongNotesModel.NotifyLongNotesId = notifylongNote.NotifyLongNotesId;
            }
            return notifyLongNotesModel;
        }
        [HttpGet]
        [Route("GetLatestNotifyDocumentById")]
        public NotifyDocumentModel GetLatestNotifyDocumentById(long? id)
        {
            NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel();
            var notifyDocuments = _context.NotifyDocument
             .Include(c => c.QuoteNotify)
             .Where(t => t.DocumentId == id)
             .OrderByDescending(o => o.NotifyDocumentId).AsNoTracking().ToList();
            var notifydocumentsids = notifyDocuments.Select(s => s.NotifyDocumentId).ToList();
            var notifyDocumentUser = _context.NotifyDocumentUserGroup.Where(u => notifydocumentsids.Contains(u.NotifyDocumentId.Value)).Include(a => a.User).Include(a => a.UserGroup).AsNoTracking().ToList();
            var latestNotifydocument = notifyDocuments.OrderByDescending(d => d.NotifyDocumentId).FirstOrDefault();
            if (latestNotifydocument != null)
            {
                notifyDocumentModel.AddedByUserID = latestNotifydocument.AddedByUserId;
                notifyDocumentModel.UserIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == latestNotifydocument.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null)).Select(m => m.UserId).ToList();
                notifyDocumentModel.UserGroupIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == latestNotifydocument.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null) && m.UserGroupId != null).Select(m => m.UserGroupId).ToList();
                notifyDocumentModel.CcuserIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == latestNotifydocument.NotifyDocumentId) && (m.IsCcuser == true)).Select(m => m.UserId).ToList();
                notifyDocumentModel.CcuserGroupIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == latestNotifydocument.NotifyDocumentId) && (m.IsCcuser == true) && m.UserGroupId != null).Select(m => m.UserGroupId).ToList();
            }
            return notifyDocumentModel;
        }
        [HttpGet]
        [Route("GetNotifyDocumentById")]
        public List<NotifyDocumentModel> GetNotifyDocumentById(long? id, string type, string ipirReportType)
        {
            // var sessionId = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).FirstOrDefault(d => d.DocumentId == id)?.SessionId;
            //var documentIs = _context.Documents.Select(s => new { s.DocumentId, s.SessionId }).Where(d => d.DocumentId == id).Select(a => a.DocumentId).ToList();


            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            var notifyDocumentss = _context.NotifyDocument
              .Include(c => c.QuoteNotify)
              .Where(t => t.NotifyDocumentId > 0);
            if (type != null && type.ToLower() == "NoDocument".ToLower())
            {
                if (ipirReportType != null && ipirReportType == "IPIR")
                {
                    notifyDocumentss = notifyDocumentss.Where(w => w.IpirReportId == id);
                }
                else if (ipirReportType != null && ipirReportType == "IpirReportAssignment")
                {
                    notifyDocumentss = notifyDocumentss.Where(w => w.IpirReportAssignmentId == id);
                }
                else
                {
                    notifyDocumentss = notifyDocumentss.Where(w => w.ProductionActivityAppLineId == id);
                }
            }
            else
            {
                notifyDocumentss = notifyDocumentss.Where(w => w.DocumentId == id);
            }
            var notifyDocuments = notifyDocumentss.OrderByDescending(o => o.NotifyDocumentId).AsNoTracking().ToList();
            var notifydocumentsids = notifyDocuments.Select(s => s.NotifyDocumentId).ToList();
            var quotenotifyIds = notifyDocuments.Where(d => d.QuoteNotifyId != null).Select(s => s.QuoteNotifyId).ToList();
            var notifyDocumentUser = _context.NotifyDocumentUserGroup.Where(u => notifydocumentsids.Contains(u.NotifyDocumentId.Value)).Include(a => a.User).Include(a => a.UserGroup).AsNoTracking().ToList();
            var longnotesList = _context.NotifyLongNotes.Where(n => notifydocumentsids.Contains(n.NotifyDocumentId.Value)).Select(s => s.NotifyDocumentId).ToList();
            var quotelongnotesList = _context.NotifyLongNotes.Where(n => quotenotifyIds.Contains(n.NotifyDocumentId.Value)).Select(s => s.NotifyDocumentId).ToList();
            if (notifyDocuments != null && notifyDocuments.Count > 0)
            {
                var userIds = notifyDocuments.Select(s => s.AddedByUserId).ToList();

                userIds.AddRange(notifyDocuments.Select(s => s.ModifiedByUserId).ToList());
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(u => userIds.Contains(u.UserId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).AsNoTracking().ToList();
                var documentIds = notifyDocuments.Select(d => d.DocumentId).ToList();
                var documentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, SessionId = e.SessionId }).Where(n => documentIds.Contains(n.DocumentId)).ToList();
                notifyDocuments.ForEach(s =>
                {
                    List<string> userGroupNames = new List<string>();
                    List<string> userNames = new List<string>();
                    List<string> ccuserNames = new List<string>();
                    List<string> ccuserGroupNames = new List<string>();
                    NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel();
                    var documentItem = documentItems.FirstOrDefault(s => s.DocumentId == s.DocumentId);
                    notifyDocumentModel.NotifyDocumentId = s.NotifyDocumentId;
                    notifyDocumentModel.UserId = s.UserId;
                    // notifyDocumentModel.UserName = notifyDocumentUser != null ? string.Join(",", notifyDocumentUser.Where(m => m.NotifyDocumentId == s.NotifyDocumentId).Select(m => m.User?.UserName).ToList()) : "";
                    if (notifyDocumentUser != null)
                    {
                        userNames = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null)).Select(m => m.User?.UserName).ToList();
                        userGroupNames = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null)).Select(m => m.UserGroup?.Name).ToList();
                        ccuserNames = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == true)).Select(m => m.User?.UserName).ToList();
                        ccuserGroupNames = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == true)).Select(m => m.UserGroup?.Name).ToList();

                        notifyDocumentModel.UserIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null)).Select(m => m.UserId).ToList();
                        notifyDocumentModel.UserGroupIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == false || m.IsCcuser == null)).Select(m => m.UserGroupId).ToList();
                        notifyDocumentModel.CcuserIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == true)).Select(m => m.UserId).ToList();
                        notifyDocumentModel.CcuserGroupIDs = notifyDocumentUser.Where(m => (m.NotifyDocumentId == s.NotifyDocumentId) && (m.IsCcuser == true)).Select(m => m.UserGroupId).ToList();
                    }
                    notifyDocumentModel.UserName = userNames.All(u => u == string.Empty || u == null) ? string.Empty : string.Join(",", userNames.Where(u => u != string.Empty || u != null));
                    notifyDocumentModel.UserGroupName = userGroupNames.All(i => i == string.Empty || i == null) ? string.Empty : string.Join(",", userGroupNames.Where(u => u != string.Empty || u != null));
                    notifyDocumentModel.CcuserName = ccuserNames.All(u => u == string.Empty || u == null) ? string.Empty : string.Join(",", ccuserNames.Where(u => u != string.Empty || u != null));
                    notifyDocumentModel.Remarks = s.Remarks;
                    notifyDocumentModel.DocumentName = documentItem?.FileName;
                    notifyDocumentModel.StatusCodeID = s.StatusCodeId;
                    notifyDocumentModel.AddedByUserID = s.AddedByUserId;
                    notifyDocumentModel.ModifiedByUserID = s.ModifiedByUserId;
                    notifyDocumentModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(c => c.UserId == s.AddedByUserId)?.UserName : string.Empty;
                    notifyDocumentModel.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(c => c.UserId == s.ModifiedByUserId)?.UserName : string.Empty;
                    notifyDocumentModel.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : string.Empty;
                    notifyDocumentModel.AddedDate = s.AddedDate;
                    notifyDocumentModel.ModifiedDate = s.ModifiedDate;
                    notifyDocumentModel.SessionId = s.SessionId;
                    notifyDocumentModel.IsUrgent = s.IsUrgent;
                    notifyDocumentModel.DocumentId = s.DocumentId;
                    notifyDocumentModel.IsQuote = s.IsQuote;
                    notifyDocumentModel.QuoteNotifyId = s.QuoteNotifyId;
                    notifyDocumentModel.IsSenderClose = s.IsSenderClose;
                    notifyDocumentModel.DueDate = s.DueDate;
                    // var notifylongNote = longnotesList.FirstOrDefault(f => f.NotifyDocumentId == s.NotifyDocumentId);
                    if (longnotesList.Contains(s.NotifyDocumentId))
                    {
                        notifyDocumentModel.IsDetailNotes = true;
                    }
                    if (quotelongnotesList.Contains(s.QuoteNotifyId))
                    {
                        notifyDocumentModel.IsQuoteDetailNotes = true;
                    }
                    notifyDocumentModel.QuoteMessage = s.QuoteNotify?.Remarks + "  " + s.QuoteNotify?.AddedByUser?.UserName + " " + s.QuoteNotify?.AddedDate.ToString("dd/MMM/yyyy HH:mm tt");
                    notifyDocumentModels.Add(notifyDocumentModel);

                });
            }
            return notifyDocumentModels.OrderByDescending(o => o.AddedDate).ToList();
        }
        private List<FileProfileTypeModel> GetMainProfileId(List<FileProfileTypeModel> fileprofile, long? mainProfileId)
        {
            var fileprofileList = fileprofile;
            if (mainProfileId != null)
            {
                fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList().ForEach(s =>
                {
                    if (s.ParentId != null)
                    {
                        fileprofileList = GetMainProfileId(fileprofileList, s.ParentId);
                    }
                    else
                    {
                        fileprofileList = fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList();
                    }
                });
            }
            return fileprofileList;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetNotifyDocumentByUser")]
        public List<NotifyDocumentModel> GetNotifyDocumentByUser(int Id, string type)
        {
            List<NotifyDocumentModel> latestDocumentModels = new List<NotifyDocumentModel>();
            // Settings.  
            SqlParameter userParam = new SqlParameter("@userId", Id);

            // Processing.  
            string sqlQuery = "EXEC [dbo].[GetNotifyDocumentByUser] @userId";

            var notifyDocumentsLinq = _context.Set<SpNotifyDocumentModel>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();
            notifyDocumentsLinq = notifyDocumentsLinq.OrderByDescending(d => d.NotifyDocumentID).ToList();
            List<NotifyDocumentModel> notifyDocumentModelResults = new List<NotifyDocumentModel>();
            foreach (var item in notifyDocumentsLinq)
            {
                NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel
                {
                    NotifyDocumentId = item.NotifyDocumentID,
                    UserId = item.UserId,
                    Remarks = item.Remarks,
                    DocumentID = item.DocumentID,
                    DocumentId = item.DocumentID,
                    StatusCodeID = item.StatusCodeID,
                    AddedByUserID = item.AddedByUserID,
                    AddedDate = item.AddedDate,
                    ModifiedByUserID = item.ModifiedByUserID,
                    ModifiedDate = item.ModifiedDate,
                    SessionId = item.SessionID,
                    IsUrgent = item.IsUrgent,
                    IsQuote = item.IsQuote,
                    QuoteNotifyId = item.QuoteNotifyID,
                    IsSenderClose = item.IsSenderClose,
                    DueDate = item.DueDate,
                    NotifyDocumentUserGroupId = item.NotifyDocumentUserGroupID == null ? 0 : item.NotifyDocumentUserGroupID.Value,
                    GUserId = item.GUserId,
                    GUserGroupId = item.GUserGroupId,
                    GIsClosed = item.GIsClosed,
                    GIsCcuser = item.GIsCcuser,
                    GIsRead = item.GIsRead,
                    NotifyUserGroupUserId = item.NotifyUserGroupUserID == null ? 0 : item.NotifyUserGroupUserID.Value,
                    UUserId = item.UUserId,
                    UUserGroupId = item.UUserGroupId,
                    UIsClosed = item.UIsClosed,
                    UIsCcuser = item.UIsCcuser,
                    UIsRead = item.UIsRead,
                    IpirReportAssignmentId = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == item.NotifyDocumentID)?.IpirReportAssignmentId,
                    NotifyType = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == item.NotifyDocumentID)?.NotifyType,
                    ProductionActivityAppLineId = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == item.NotifyDocumentID)?.ProductionActivityAppLineId,
                };
                notifyDocumentModelResults.Add(notifyDocumentModel);
            }
            var list = new List<string>()
                    {
                        "ProductionApp",
                        "IPIR",
                        "IpirReportAssignment"
                    };
            if (type == "ProductionApp")
            {
                notifyDocumentModelResults = notifyDocumentModelResults.Where(w => list.Contains(w.NotifyType)).ToList();
            }
            else
            {
                notifyDocumentModelResults = notifyDocumentModelResults.Where(w => !list.Contains(w.NotifyType)).ToList();
            }
            if (notifyDocumentModelResults != null && notifyDocumentModelResults.Count > 0)
            {
                var userIds = notifyDocumentModelResults.Select(s => s.AddedByUserID.GetValueOrDefault(0)).ToList();
                userIds.AddRange(notifyDocumentModelResults.Select(s => s.ModifiedByUserID.GetValueOrDefault(0)).ToList());
                var codeIds = notifyDocumentModelResults.Select(s => s.StatusCodeID).ToList();
                var notifyDocumentId = notifyDocumentModelResults.Select(n => n.DocumentID).ToList();
                var notifyDocumentItems = _context.Documents.Select(e => new { e.DocumentId, e.FileName, e.ContentType, FileProfileTypeId = e.FilterProfileTypeId, e.SessionId, e.IsLatest }).Where(n => notifyDocumentId.Contains(n.DocumentId)).ToList();
                var docSessionIds = notifyDocumentItems.Select(s => s.SessionId).ToList();
                var fileProfileTypeIds = notifyDocumentItems.Select(s => s.FileProfileTypeId).ToList();
                var fileProfileType = _context.FileProfileType.Select(f => new FileProfileTypeModel { ParentId = f.ParentId, IsHidden = f.IsHidden, FileProfileTypeId = f.FileProfileTypeId, Name = f.Name }).AsNoTracking().ToList();
                var notifyDocumentIds = notifyDocumentModelResults.Select(s => s.NotifyDocumentId).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var longnotesList = _context.NotifyLongNotes.Where(n => notifyDocumentIds.Contains(n.NotifyDocumentId.Value)).Select(s => s.NotifyDocumentId).ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
                notifyDocumentModelResults.ForEach(s =>
                {
                    var document = notifyDocumentItems.FirstOrDefault(d => d.DocumentId == s.DocumentID);
                    if (document != null)
                    {
                        var mainprofileId = GetMainProfileId(fileProfileType, document.FileProfileTypeId);
                        s.DocumentName = notifyDocumentItems?.FirstOrDefault(r => r.DocumentId == s.DocumentID)?.FileName;
                        s.ContentType = document.ContentType;
                        s.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserID)?.UserName : "";
                        s.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserID)?.UserName : "";
                        s.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.StatusCodeID)?.CodeValue : "";
                        s.SessionId = document.SessionId;
                        s.Type = "Document";
                        s.FileProfileTypeId = document.FileProfileTypeId;
                        s.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == document.FileProfileTypeId)?.Select(s => s.Name).FirstOrDefault();
                        s.ParentId = mainprofileId.FirstOrDefault()?.ParentId != null ? mainprofileId.FirstOrDefault()?.ParentId : mainprofileId.FirstOrDefault()?.FileProfileTypeId;
                        var isHidden = fileProfileType.Where(t => t.FileProfileTypeId == document.FileProfileTypeId)?.Select(s => s.IsHidden).FirstOrDefault();
                        bool? isHiddens = true;
                        if (isHidden != null && isHidden == true && Id != 47)
                        {
                            isHiddens = false;
                        }
                        s.IsHidden = isHiddens;

                        if (s.GUserGroupId == null && s.GUserId == Id)
                        {
                            s.CssClass = s.GIsRead == true ? "true" : "false";
                            if (s.AddedByUserID == Id)
                            {
                                s.CssClass = "true";
                            }
                        }
                        if (s.UUserId == Id)
                        {
                            s.CssClass = s.UIsRead == true ? "true" : "false";
                            if (s.AddedByUserID == Id)
                            {
                                s.CssClass = "true";
                            }
                        }
                        if (longnotesList.Contains(s.NotifyDocumentId))
                        {
                            s.IsDetailNotes = true;
                        }
                    }
                });
                var documentGroups = notifyDocumentModelResults.GroupBy(s => s.DocumentID).ToList();
                documentGroups.ForEach(d =>
                            {
                                var latestItem = d.ToList().OrderByDescending(d => d.NotifyDocumentId).FirstOrDefault();
                                if (latestItem != null)
                                {
                                    var latestDocument = notifyDocumentItems.FirstOrDefault(s => s.DocumentId == latestItem.DocumentID && s.FileProfileTypeId == latestItem.FileProfileTypeId && s.IsLatest == true);
                                    if (latestDocument != null)
                                    {
                                        latestItem.DocumentName = latestDocument.FileName;
                                    }
                                    latestDocumentModels.Add(latestItem);
                                }
                            });
            }
            return latestDocumentModels;
        }
        [HttpGet]
        [Route("UpdateNotify")]
        public void UpdateNotify()
        {

            var notifydocument = _context.NotifyDocument.Select(n => n.DocumentId).ToList();
            //var totalDocuments = _context.Documents.Where(d => notifydocument.Contains(d.DocumentId)).Select(d => new { d.DocumentId, d.IsLatest, d.FilterProfileTypeId }).ToList();
            var doc = 136963;
            var latestdocument = _context.Documents.Where(d => d.DocumentId == doc && d.IsLatest == true).Select(d => new GroupItem { DocumentId = d.DocumentId, DocumentGroupId = d.DocumentParentId.Value }).ToList();
            var latestgroup = latestdocument.GroupBy(l => l.DocumentGroupId).ToList();
            var documentperentids = latestgroup.Select(s => s.Key);
            var documentids = _context.Documents.Where(d => documentperentids.Contains(d.DocumentParentId)).Select(d => new GroupItem { DocumentId = d.DocumentId, DocumentGroupId = d.DocumentParentId.Value }).ToList();
            var documentgroupby = documentids.GroupBy(d => d.DocumentGroupId).ToList();
            if (documentgroupby.Count > 0)
            {
                Dictionary<long?, List<GroupItem>> latedtGroups = new Dictionary<long?, List<GroupItem>>();
                List<GroupItem> parentdoc = new List<GroupItem>();
                foreach (var d in documentgroupby)
                {
                    var latestdocumentid = latestgroup.FirstOrDefault(l => l.Key == d.Key).Select(d => d.DocumentId).First();
                    if (latestdocumentid > 0)
                    {
                        latedtGroups.Add(latestdocumentid, d.ToList());

                    }
                }
                if (latedtGroups.Count > 0)
                {
                    var listDocumentIds = latedtGroups.Values.ToList();
                    List<long?> docu = new List<long?>();
                    listDocumentIds[0].ForEach(d =>
                    {
                        docu.Add(d.DocumentId);
                    });
                    var latesDoc = latedtGroups.Keys.First();
                    List<NotifyDocument> notifyDocument = BulkUpdateNotifyDocument(docu, latesDoc);
                    //_context.BulkUpdate(notifyDocument);

                    if (notifyDocument.Count > 0)
                    {
                        SqlBulkUpload objBulkInsert = new SqlBulkUpload(_context, _config);
                        objBulkInsert.BulkUpdateData(notifyDocument, "NotifyDocumentId", new List<string>() { "AddedByUser", "Document", "ModifiedByUser", "QuoteNotify", "StatusCode", "User",
                        "InverseQuoteNotify", "NotifyDocumentUserGroup", "NotifyLongNotes"}, "NotifyDocument");
                    }

                }
            }
        }

        [HttpGet]
        [Route("UpdateNotifyStatus")]
        public void UpdateNotifyStatus(int id, int userId)
        {
            var notifyDocument = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == id);
            var notifyDocumentUserGroup = _context.NotifyDocumentUserGroup.Where(g => g.NotifyDocumentId == notifyDocument.NotifyDocumentId).ToList();
            if (notifyDocumentUserGroup.Count > 0)
            {
                var notifyDocumentUsers = notifyDocumentUserGroup.Where(g => g.UserId > 0 && g.UserGroupId == null);
                foreach (var u in notifyDocumentUsers)
                {
                    if (u.UserId == userId)
                    {
                        u.IsRead = true;
                    }
                }
                var notifyDocumentUserGroups = notifyDocumentUserGroup.Where(g => g.UserGroupId > 0);
                var notifyDocumentUserGroupIds = notifyDocumentUserGroups.Select(g => g.NotifyDocumentUserGroupId).ToList();
                var notifyDocumentUserGroupUser = _context.NotifyUserGroupUser.Where(g => notifyDocumentUserGroupIds.Contains(g.NotifyDocumentUserGroupId.Value) && g.UserId == userId);
                foreach (var u in notifyDocumentUserGroupUser)
                {
                    if (u.UserId == userId)
                    {
                        u.IsRead = true;
                    }
                }

                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetMailNotifyById")]
        public List<NotifyLongNotesModel> GetMailNotifyById(long? id)
        {
            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            List<NotifyLongNotesModel> notifyLongNotesModels = new List<NotifyLongNotesModel>();
            var notifyDocuments = _context.NotifyDocument.Include(a => a.AddedByUser)
              .Where(t => t.DocumentId == id).Select(n => new NotifyDocumentModel { NotifyDocumentId = n.NotifyDocumentId, Remarks = n.Remarks, AddedByUser = n.AddedByUser.UserName, AddedDate = n.AddedDate }).ToList();
            var documentItem = _context.Documents.Select(e => new { e.DocumentId, e.FileName, e.ContentType, FileProfileTypeId = e.FilterProfileTypeId, e.SessionId, e.IsLatest }).Where(n => n.DocumentId == id).FirstOrDefault();
            var notifyDocumentIds = notifyDocuments.Select(n => n.NotifyDocumentId).ToList();

            var notifyLongNotes = _context.NotifyLongNotes.Include(n => n.NotifyDocument).Where(n => notifyDocumentIds.Contains(n.NotifyDocumentId.Value)).AsEnumerable().GroupBy(s => s.NotifyDocumentId).ToList();
            foreach (var item in notifyDocuments)
            {
                item.DocumentName = documentItem.FileName;
                var longNote = notifyLongNotes.FirstOrDefault(n => n.Key == item.NotifyDocumentId);
                if (longNote != null)
                {
                    foreach (var noteItem in longNote.ToList())
                    {
                        NotifyLongNotesModel notifyLongNotesModel = new NotifyLongNotesModel
                        {
                            DetailNotes = noteItem.LongNotes,
                            Remarks = item.Remarks,
                            DocumentName = item.DocumentName,
                            NotifyLongNotesId = noteItem.NotifyLongNotesId,
                            ParentId = noteItem.ParentId,
                            AddedByUser = item.AddedByUser,
                            AddedDate = item.AddedDate
                        };
                        notifyLongNotesModels.Add(notifyLongNotesModel);
                    }
                }
            }
            return notifyLongNotesModels;
        }

        [HttpGet]
        [Route("GetNotifyDocumentByCCUser")]
        public List<NotifyDocumentModel> GetNotifyDocumentByCCUser(int Id)
        {
            List<NotifyDocumentModel> latestDocumentModels = new List<NotifyDocumentModel>();
            // Settings.  
            SqlParameter userParam = new SqlParameter("@userId", Id);

            // Processing.  
            string sqlQuery = "EXEC [dbo].[GetNotifyDocumentByCCUser] @userId";

            var notifyDocumentsLinq = _context.Set<SpNotifyDocumentModel>().FromSqlRaw(sqlQuery, userParam).AsNoTracking().ToList();
            List<NotifyDocumentModel> notifyDocumentModelResults = new List<NotifyDocumentModel>();
            foreach (var item in notifyDocumentsLinq)
            {
                NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel
                {
                    NotifyDocumentId = item.NotifyDocumentID,
                    UserId = item.UserId,
                    Remarks = item.Remarks,
                    DocumentID = item.DocumentID,
                    DocumentId = item.DocumentID,
                    StatusCodeID = item.StatusCodeID,
                    AddedByUserID = item.AddedByUserID,
                    AddedDate = item.AddedDate,
                    ModifiedByUserID = item.ModifiedByUserID,
                    ModifiedDate = item.ModifiedDate,
                    SessionId = item.SessionID,
                    IsUrgent = item.IsUrgent,
                    IsQuote = item.IsQuote,
                    QuoteNotifyId = item.QuoteNotifyID,
                    IsSenderClose = item.IsSenderClose,
                    DueDate = item.DueDate,
                    NotifyDocumentUserGroupId = item.NotifyDocumentUserGroupID == null ? 0 : item.NotifyDocumentUserGroupID.Value,
                    GUserId = item.GUserId,
                    GUserGroupId = item.GUserGroupId,
                    GIsClosed = item.GIsClosed,
                    GIsCcuser = item.GIsCcuser,
                    GIsRead = item.GIsRead,
                    NotifyUserGroupUserId = item.NotifyUserGroupUserID == null ? 0 : item.NotifyUserGroupUserID.Value,
                    UUserId = item.UUserId,
                    UUserGroupId = item.UUserGroupId,
                    UIsClosed = item.UIsClosed,
                    UIsCcuser = item.UIsCcuser,
                    UIsRead = item.UIsRead,
                };
                notifyDocumentModelResults.Add(notifyDocumentModel);
            }
            if (notifyDocumentModelResults != null && notifyDocumentModelResults.Count > 0)
            {
                var userIds = notifyDocumentModelResults.Select(s => s.AddedByUserID.GetValueOrDefault(0)).ToList();
                userIds.AddRange(notifyDocumentModelResults.Select(s => s.ModifiedByUserID.GetValueOrDefault(0)).ToList());
                var codeIds = notifyDocumentModelResults.Select(s => s.StatusCodeID).ToList();
                var notifyDocumentId = notifyDocumentModelResults.Select(n => n.DocumentID).ToList();
                var notifyDocumentItems = _context.Documents.Select(e => new { e.DocumentId, e.FileName, e.ContentType, FileProfileTypeId = e.FilterProfileTypeId, e.SessionId, e.IsLatest }).Where(n => notifyDocumentId.Contains(n.DocumentId)).ToList();
                var docSessionIds = notifyDocumentItems.Select(s => s.SessionId).ToList();
                var fileProfileTypeIds = notifyDocumentItems.Select(s => s.FileProfileTypeId).ToList();
                var fileProfileType = _context.FileProfileType.Select(f => new FileProfileTypeModel { ParentId = f.ParentId, IsHidden = f.IsHidden, FileProfileTypeId = f.FileProfileTypeId, Name = f.Name }).AsNoTracking().ToList();
                var notifyDocumentIds = notifyDocumentModelResults.Select(s => s.NotifyDocumentId).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var longnotesList = _context.NotifyLongNotes.Where(n => notifyDocumentIds.Contains(n.NotifyDocumentId.Value)).Select(s => s.NotifyDocumentId).ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
                notifyDocumentModelResults.ForEach(s =>
                {
                    var document = notifyDocumentItems.FirstOrDefault(d => d.DocumentId == s.DocumentID);
                    if (document != null)
                    {
                        var mainprofileId = GetMainProfileId(fileProfileType, document.FileProfileTypeId);
                        s.DocumentName = notifyDocumentItems?.FirstOrDefault(r => r.DocumentId == s.DocumentID)?.FileName;
                        s.ContentType = document.ContentType;
                        s.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserID)?.UserName : "";
                        s.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserID)?.UserName : "";
                        s.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.StatusCodeID)?.CodeValue : "";
                        s.SessionId = document.SessionId;
                        s.Type = "Document";
                        s.FileProfileTypeId = document.FileProfileTypeId;
                        s.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == document.FileProfileTypeId)?.Select(s => s.Name).FirstOrDefault();
                        s.ParentId = mainprofileId.FirstOrDefault()?.ParentId != null ? mainprofileId.FirstOrDefault()?.ParentId : mainprofileId.FirstOrDefault()?.FileProfileTypeId;
                        var isHidden = fileProfileType.Where(t => t.FileProfileTypeId == document.FileProfileTypeId)?.Select(s => s.IsHidden).FirstOrDefault();
                        bool? isHiddens = true;
                        if (isHidden != null && isHidden == true && Id != 47)
                        {
                            isHiddens = false;
                        }
                        s.IsHidden = isHiddens;
                        if (s.GUserGroupId == null && s.GUserId == Id)
                        {
                            s.CssClass = s.GIsRead == true ? "true" : "false";
                            if (s.AddedByUserID == Id)
                            {
                                s.CssClass = "true";
                            }
                        }
                        if (s.UUserId == Id)
                        {
                            s.CssClass = s.UIsRead == true ? "true" : "false";
                            if (s.AddedByUserID == Id)
                            {
                                s.CssClass = "true";
                            }
                        }
                        if (longnotesList.Contains(s.NotifyDocumentId))
                        {
                            s.IsDetailNotes = true;
                        }
                    }
                });
                var documentGroups = notifyDocumentModelResults.GroupBy(s => s.DocumentID).ToList();
                documentGroups.ForEach(d =>
                {
                    var latestItem = d.ToList().OrderByDescending(d => d.NotifyDocumentId).FirstOrDefault();
                    if (latestItem != null)
                    {
                        var latestDocument = notifyDocumentItems.FirstOrDefault(s => s.DocumentId == latestItem.DocumentID && s.FileProfileTypeId == latestItem.FileProfileTypeId && s.IsLatest == true);
                        if (latestDocument != null)
                        {
                            latestItem.DocumentName = latestDocument.FileName;
                        }
                        latestDocumentModels.Add(latestItem);
                    }
                });
            }
            return latestDocumentModels;

        }
        [HttpGet]
        [Route("GetNotifyDocumentBySendUser")]
        public List<NotifyDocumentModel> GetNotifyDocumentBySendUser(int Id, string type)
        {
            var userGroups = _context.UserGroupUser.Where(e => e.UserGroupId > 0 && e.UserId == Id).Select(ug => ug.UserGroupId).ToList();
            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            List<NotifyDocumentModel> latestDocumentModels = new List<NotifyDocumentModel>();
            var notifyDocuments = _context.NotifyDocument
              .Where(n => n.AddedByUserId == Id && n.StatusCodeId == 1 && (n.IsSenderClose == false || n.IsSenderClose == null))
              .OrderByDescending(o => o.NotifyDocumentId).AsNoTracking().ToList();
            var list = new List<string>()
                    {
                        "ProductionApp",
                        "IPIR",
                        "IpirReportAssignment"
                    };
            if (type == "ProductionApp")
            {
                notifyDocuments = notifyDocuments.Where(w => list.Contains(w.NotifyType)).ToList();
            }
            else
            {
                notifyDocuments = notifyDocuments.Where(w => !list.Contains(w.NotifyType)).ToList();
            }
            if (notifyDocuments != null && notifyDocuments.Count > 0)
            {

                var userIds = notifyDocuments.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
                userIds.AddRange(notifyDocuments.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
                var codeIds = notifyDocuments.Select(s => s.StatusCodeId).ToList();
                var notifyDocumentId = notifyDocuments.Select(n => n.DocumentId).ToList();
                var notifyDocumentItems = _context.Documents.Select(e => new { e.DocumentId, e.FileName, e.ContentType, FileProfileTypeId = e.FilterProfileTypeId, e.SessionId }).Where(n => notifyDocumentId.Contains(n.DocumentId)).ToList();
                var docSessionIds = notifyDocumentItems.Select(s => s.SessionId).ToList();
                var fileProfileTypeIds = notifyDocumentItems.Select(s => s.FileProfileTypeId).ToList();
                var latestDocumentNames = _context.Documents.Select(e => new { DocumentId = e.DocumentId, e.FileName, e.SessionId, e.IsLatest, e.FilterProfileTypeId }).Where(n => docSessionIds.Contains(n.SessionId) && n.IsLatest == true).ToList();
                var fileProfileType = _context.FileProfileType.Select(f => new FileProfileTypeModel { ParentId = f.ParentId, IsHidden = f.IsHidden, FileProfileTypeId = f.FileProfileTypeId, Name = f.Name }).AsNoTracking().ToList();
                var notifyDocumentIds = notifyDocuments.Select(s => s.NotifyDocumentId).ToList();
                var longnotesList = _context.NotifyLongNotes.Where(n => notifyDocumentIds.Contains(n.NotifyDocumentId.Value)).Select(s => s.NotifyDocumentId).ToList();

                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
                notifyDocuments.ForEach(s =>
                {
                    var document = notifyDocumentItems.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                    // if (document != null)
                    // {
                    var mainprofileId = GetMainProfileId(fileProfileType, document?.FileProfileTypeId);
                    NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel();
                    notifyDocumentModel.NotifyDocumentId = s.NotifyDocumentId;
                    notifyDocumentModel.Remarks = s.Remarks;
                    notifyDocumentModel.DocumentName = document?.FileName;
                    notifyDocumentModel.ContentType = document?.ContentType;
                    notifyDocumentModel.DocumentID = s.DocumentId;
                    notifyDocumentModel.DocumentId = s.DocumentId;
                    notifyDocumentModel.StatusCodeID = s.StatusCodeId;
                    notifyDocumentModel.AddedByUserID = s.AddedByUserId;
                    notifyDocumentModel.ModifiedByUserID = s.ModifiedByUserId;
                    notifyDocumentModel.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "";
                    notifyDocumentModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "";
                    notifyDocumentModel.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.StatusCodeId)?.CodeValue : "";
                    notifyDocumentModel.AddedDate = s.AddedDate;
                    notifyDocumentModel.ModifiedDate = s.ModifiedDate;
                    notifyDocumentModel.SessionId = document?.SessionId;
                    notifyDocumentModel.IsUrgent = s.IsUrgent;
                    notifyDocumentModel.Type = "Document";
                    notifyDocumentModel.FileProfileTypeId = document?.FileProfileTypeId;
                    var isHidden = fileProfileType.Where(t => t.FileProfileTypeId == document?.FileProfileTypeId)?.Select(s => s.IsHidden).FirstOrDefault();
                    notifyDocumentModel.IsHidden = true;
                    if (isHidden != null && isHidden == true && Id != 47)
                    {
                        notifyDocumentModel.IsHidden = false;
                    }
                    notifyDocumentModel.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == document?.FileProfileTypeId)?.Select(s => s.Name).FirstOrDefault();
                    notifyDocumentModel.ParentId = mainprofileId.FirstOrDefault()?.FileProfileTypeId;
                    notifyDocumentModel.IsQuote = s.IsQuote;
                    notifyDocumentModel.QuoteNotifyId = s.QuoteNotifyId;
                    notifyDocumentModel.IsSenderClose = s.IsSenderClose;
                    notifyDocumentModel.DueDate = s.DueDate;
                    notifyDocumentModel.NotifyType = s.NotifyType;
                    notifyDocumentModel.ProductionActivityAppLineId = s.ProductionActivityAppLineId;
                    notifyDocumentModel.IpirReportId = s.IpirReportId;
                    notifyDocumentModel.IpirReportAssignmentId = s.IpirReportAssignmentId;
                    //var notifylongNote = longnotesList.FirstOrDefault(f => f.NotifyDocumentId == s.NotifyDocumentId);
                    if (longnotesList.Contains(s.NotifyDocumentId))
                    {
                        notifyDocumentModel.IsDetailNotes = true;
                    }
                    notifyDocumentModels.Add(notifyDocumentModel);
                    //}
                });
                // var documentGroups = notifyDocumentModels.GroupBy(s => s.SessionId).ToList();
                var documentGroups = notifyDocumentModels.GroupBy(s => s.DocumentID).ToList();
                documentGroups.ForEach(d =>
                {
                    var latestItem = d.ToList().OrderByDescending(d => d.AddedDate).FirstOrDefault();
                    if (latestItem != null)
                    {
                        var latestDocument = latestDocumentNames.FirstOrDefault(s => s.DocumentId == latestItem.DocumentID && s.FilterProfileTypeId == latestItem.FileProfileTypeId);
                        if (latestDocument != null)
                        {
                            latestItem.DocumentName = latestDocument.FileName;
                        }
                        latestDocumentModels.Add(latestItem);
                    }
                });
            }
            return latestDocumentModels;
        }
        [HttpPost]
        [Route("InsertNotifyDocument")]
        public NotifyDocumentModel Post(NotifyDocumentModel value)
        {

            value.SessionId ??= Guid.NewGuid();
            var notifyDocument = new NotifyDocument
            {
                DocumentId = value.DocumentId,
                UserId = value.UserId,
                Remarks = value.Remarks,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = 1,
                SessionId = value.SessionId,
                IsUrgent = value.IsUrgent,
                IsQuote = value.IsQuote,
                QuoteNotifyId = value.QuoteNotifyId,
                IsSenderClose = false,
                DueDate = value.DueDate,
                NotifyType = value.NotifyType,
                ProductionActivityAppLineId = value.ProductionActivityAppLineId,
                IpirReportId = value.IpirReportId,
                IpirReportAssignmentId = value.IpirReportAssignmentId,
            };
            _context.NotifyDocument.Add(notifyDocument);
            var userGroups = _context.UserGroupUser.Where(u => u.UserGroupId != null).AsNoTracking().ToList();
            if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(g =>
                {
                    if (g != null)
                    {
                        NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
                        //notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
                        notifyDocumentUserGroup.UserGroupId = g;
                        notifyDocumentUserGroup.IsClosed = false;
                        //_context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                        var userids = userGroups.Where(s => s.UserGroupId == g).Select(s => s.UserId).ToList();

                        if (userids.Count > 0 && userids != null)
                        {
                            List<NotifyUserGroupUser> notifyUserGroupUsers = new List<NotifyUserGroupUser>();
                            userids.ForEach(u =>
                            {
                                NotifyUserGroupUser notifyUserGroupUser = new NotifyUserGroupUser();
                                notifyUserGroupUser.UserGroupId = g;
                                notifyUserGroupUser.UserId = u;
                                notifyUserGroupUser.IsClosed = false;
                                notifyDocumentUserGroup.NotifyUserGroupUser.Add(notifyUserGroupUser);

                            });
                            notifyDocument.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                        }
                    }
                });

            }
            if (value.UserIDs != null && value.UserIDs.Count > 0)
            {
                value.UserIDs.ForEach(u =>
                {
                    NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
                    //notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
                    notifyDocumentUserGroup.UserId = u;
                    notifyDocumentUserGroup.IsClosed = false;
                    notifyDocumentUserGroup.IsCcuser = false;
                    notifyDocument.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                    //_context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                });
            }
            if (value.CcuserIDs != null && value.CcuserIDs.Count > 0)
            {
                value.CcuserIDs.ForEach(u =>
                {
                    NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
                    //notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
                    notifyDocumentUserGroup.UserId = u;
                    notifyDocumentUserGroup.IsClosed = false;
                    notifyDocumentUserGroup.IsCcuser = true;
                    notifyDocument.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                    //_context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                });
            }
            if (value.CcuserGroupIDs != null && value.CcuserGroupIDs.Count > 0)
            {
                value.CcuserGroupIDs.ForEach(g =>
                {
                    if (g != null)
                    {
                        NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
                        //notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
                        notifyDocumentUserGroup.UserGroupId = g;
                        notifyDocumentUserGroup.IsClosed = false;
                        notifyDocumentUserGroup.IsCcuser = true;
                        //_context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                        notifyDocument.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                        var userids = userGroups.Where(s => s.UserGroupId == g).Select(s => s.UserId).ToList();

                        if (userids.Count > 0 && userids != null)
                        {
                            userids.ForEach(u =>
                            {
                                NotifyUserGroupUser notifyUserGroupUser = new NotifyUserGroupUser();
                                //notifyUserGroupUser.NotifyDocumentUserGroupId = notifyDocumentUserGroup.NotifyDocumentUserGroupId;
                                notifyUserGroupUser.UserGroupId = g;
                                notifyUserGroupUser.UserId = u;
                                notifyUserGroupUser.IsClosed = false;
                                notifyUserGroupUser.IsCcuser = true;
                                notifyDocumentUserGroup.NotifyUserGroupUser.Add(notifyUserGroupUser);

                            });
                            notifyDocument.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
                        }
                    }

                });

            }
            _context.SaveChanges();

            var notifyDocumentUser = _context.NotifyDocumentUserGroup.Include(a => a.User).Include(a => a.UserGroup).Where(n => n.NotifyDocumentId == value.NotifyDocumentId).Select(s => new { IsCcuser = s.IsCcuser, UserName = s.User.UserName, GroupName = s.UserGroup.Name }).AsNoTracking().ToList();
            List<string> userGroupNames = new List<string>();
            List<string> userNames = new List<string>();
            List<string> ccuserNames = new List<string>();
            List<string> ccuserGroupNames = new List<string>();

            value.NotifyDocumentId = notifyDocument.NotifyDocumentId;
            if (value.UserIDs != null && value.UserIDs.Count > 0)
            {
                userNames = notifyDocumentUser.Where(m => m.IsCcuser == null || m.IsCcuser == false).Select(m => m.UserName).ToList();
                value.UserName = userNames.All(u => u == string.Empty || u == null) ? string.Empty : string.Join(",", userNames.Where(u => u != string.Empty || u != null));


            }
            if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                userGroupNames = notifyDocumentUser.Where(m => m.IsCcuser == null || m.IsCcuser == false).Select(m => m.GroupName).ToList();
                value.UserGroupName = userGroupNames.All(i => i == string.Empty || i == null) ? string.Empty : string.Join(",", userGroupNames.Where(u => u != string.Empty || u != null));
            }
            if (value.CcuserIDs != null && value.CcuserIDs.Count > 0)
            {
                ccuserNames = notifyDocumentUser.Where(m => m.IsCcuser == true).Select(m => m.UserName).ToList();
                value.CcuserName = ccuserNames.All(u => u == string.Empty || u == null) ? string.Empty : string.Join(",", ccuserNames.Where(u => u != string.Empty || u != null));
            }
            if (value.CcuserGroupIDs != null && value.CcuserGroupIDs.Count > 0)
            {
                ccuserGroupNames = notifyDocumentUser.Where(m => m.IsCcuser == true).Select(m => m.GroupName).ToList();
                value.UserGroupName = ccuserGroupNames.All(i => i == string.Empty || i == null) ? string.Empty : string.Join(",", ccuserGroupNames.Where(u => u != string.Empty || u != null));
            }
            value.SessionId = notifyDocument.SessionId;
            if (value.IsDetailNotes != true)
            {
                SendEmailMessage(value);
            }
            return value;
        }
        private void SendEmailMessage(NotifyDocumentModel value)
        {
            var productActivityApp = _context.ProductionActivityAppLine
               .Include(a => a.AddedByUser)
               .Include(a => a.ModifiedByUser)
               .Include(a => a.ManufacturingProcessChild)
               .Include(a => a.ProdActivityCategoryChild)
               .Include(a => a.ProdActivityActionChildDNavigation)
               .Include(a => a.ProdActivityResult)
               .Include(a => a.ProductionActivityApp)
               .Include(a => a.ProductionActivityApp.Company)
               .Include(a => a.StatusCode)
                .Include(a => a.NavprodOrderLine)
               .Include(a => a.ProductActivityCaseLine).FirstOrDefault(f => f.ProductionActivityAppLineId == value.ProductionActivityAppLineId);
            var ipirReport = _context.IpirReport.Include(s => s.AddedByUser).Include(a => a.Company).Include(s => s.ModifiedByUser).Include(s => s.ManufacturingProcess).OrderByDescending(o => o.IpirReportId).FirstOrDefault(w => w.IpirReportId == value.IpirReportId);
            var ipirAssignment = _context.IpirReportAssignment.Include(a => a.IpirReport).Include(a => a.StatusCode).FirstOrDefault(f => f.IpirReportAssignmentId == value.IpirReportAssignmentId);
            var notifyDoc = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == value.NotifyDocumentId);

            var notifyDocumentUser = _context.NotifyDocumentUserGroup.Where(n => n.NotifyDocumentId == value.NotifyDocumentId).Select(s => new { s.NotifyDocumentId, s.UserId, s.UserGroupId, s.IsCcuser }).AsNoTracking().ToList();
            List<long?> users = new List<long?>();
            List<long?> usersTo = new List<long?>();
            List<long?> usersCC = new List<long?>();
            MailServiceModel mailServiceModel = new MailServiceModel();
            if (notifyDocumentUser != null)
            {
                var userGroup = notifyDocumentUser.Where(w => w.UserId == null).Select(s => s.UserGroupId.GetValueOrDefault(0)).ToList();
                var userGroupUserIds = _context.UserGroupUser.Where(w => w.UserId != null && userGroup.Contains(w.UserGroupId.Value)).Select(s => new { s.UserId, s.UserGroupId }).ToList();
                var usersToIds = notifyDocumentUser.Where(w => w.IsCcuser != true && w.UserId != null).Select(s => s.UserId).ToList();
                if (usersToIds != null)
                {
                    usersTo.AddRange(usersToIds);
                }
                var usersToGroupIds = notifyDocumentUser.Where(w => w.IsCcuser != true && w.UserGroupId != null).Select(s => s.UserGroupId).ToList();
                if (usersToGroupIds != null)
                {
                    usersTo.AddRange(userGroupUserIds.Where(w => usersToGroupIds.Contains(w.UserGroupId) && w.UserId != null).Select(s => s.UserId).ToList());
                }
                var usersCCIds = notifyDocumentUser.Where(w => w.IsCcuser == true && w.UserId != null).Select(s => s.UserId).ToList();
                if (usersCCIds != null)
                {
                    usersCC.AddRange(usersCCIds);
                }
                var usersCCGroupIds = notifyDocumentUser.Where(w => w.IsCcuser == true && w.UserGroupId != null).Select(s => s.UserGroupId).ToList();
                if (usersCCGroupIds != null)
                {
                    usersCC.AddRange(userGroupUserIds.Where(w => usersCCGroupIds.Contains(w.UserGroupId) && w.UserId != null).Select(s => s.UserId).ToList());
                }
                users.AddRange(usersTo);
                users.AddRange(usersCC);
                users = users.Distinct().ToList();
                var sqlQuery = "Select  * from view_GetEmployee where Status='Active' and UserID in(" + string.Join(",", users.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.Status,
                    s.EmployeeID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.Gender,
                    s.DepartmentName,
                    s.PlantID,
                    UserId = s.UserID,
                    s.Email
                }).Where(w => w.Email != null).ToList();
                var fileProfileTypeItems = _context.FileProfileType.Select(f => new FileProfileTypeModel { ParentId = f.ParentId, IsHidden = f.IsHidden, FileProfileTypeId = f.FileProfileTypeId, Name = f.Name }).AsNoTracking().ToList();
                DocumentsModel documentsModels = new DocumentsModel();
                documentsModels = _context.Documents.Select(e => new DocumentsModel { DocumentID = e.DocumentId, IsCompressed = e.IsCompressed, FileName = e.FileName, ContentType = e.ContentType, SubjectName = e.SubjectName, FilterProfileTypeId = e.FilterProfileTypeId, SessionId = e.SessionId, IsLatest = e.IsLatest }).Where(n => n.DocumentID == notifyDoc.DocumentId).FirstOrDefault();
                var fileProfileType = fileProfileTypeItems.FirstOrDefault(f => f.FileProfileTypeId == documentsModels?.FilterProfileTypeId);
                List<string> FolderPathLists = new List<string>();
                var DocumentPath = "";
                if (fileProfileType != null)
                {
                    FolderPathLists = GetAllFileProfileTypeName(fileProfileTypeItems, fileProfileType, FolderPathLists);
                    FolderPathLists.Add(fileProfileType.Name);
                    DocumentPath = string.Join(" / ", FolderPathLists);
                }
                var subject = "";
                if (documentsModels != null)
                {
                    var name = documentsModels.FileName != null ? documentsModels.FileName?.Substring(documentsModels.FileName.LastIndexOf(".")) : "";
                    var fileName = documentsModels.FileName?.Split(name);
                    subject = (documentsModels.FileIndex > 0 ? fileName[0] + "_V0" + documentsModels.FileIndex + "." + fileName[1] : documentsModels.FileName);
                }
                mailServiceModel.ToMail = employee.Where(w => usersTo.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                { Email = s.Email, Name = s.FirstName }).ToList();
                mailServiceModel.CcMail = employee.Where(w => usersCC.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                { Email = s.Email, Name = s.FirstName }).ToList();
                var message = "";
                if (value.NotifyType == "IpirReportAssignment")
                {
                    message += "<table>";
                    mailServiceModel.Subject = "Notify -IPIR No:" + ipirAssignment?.IpirReportAssignmentId + "" + ipirAssignment?.IpirReport?.ProdOrder;
                    var userNameId = value.AddedByUserID != null ? value.AddedByUserID : value.ModifiedByUserID;
                    var username = _context.ApplicationUser.FirstOrDefault(f => f.UserId == userNameId)?.UserName;
                    message += "<tr><td>Document name:</td><td>" + subject + "</td></tr>";
                    message += "<tr><td>Urgency:</td><td>" + (ipirAssignment?.Urgent == true ? "Yes" : "No") + "</td></tr>";
                    message += "<tr><td>Message:</td><td>" + ipirAssignment?.Description + "</td></tr>";
                    message += "<tr><td>Status:</td><td>" + (ipirAssignment?.StatusCode?.CodeValue) + "</td></tr>";
                    message += "</table>";
                    mailServiceModel.FromName = username;
                    mailServiceModel.Body = message;
                }
                else if (value.NotifyType == "CheckList")
                {
                    var userNameId = value.AddedByUserID != null ? value.AddedByUserID : value.ModifiedByUserID;
                    var username = _context.ApplicationUser.FirstOrDefault(f => f.UserId == userNameId)?.UserName;
                    message += "Please be informed that you have received notify from " + username + " in portal:";
                    var templateTestCaseCheckList = _context.TemplateTestCaseCheckList.Include(a => a.TemplateTestCase).Include(a => a.TemplateTestCaseForm).FirstOrDefault(f => f.SessionId == value.SessionId);
                    message += "<table>";
                    mailServiceModel.Subject = "Notify -Case No: " + templateTestCaseCheckList?.TemplateTestCaseForm?.CaseNo + " and " + templateTestCaseCheckList?.TemplateTestCaseForm.SubjectName;

                    message += "<tr><td>Check List:</td><td>" + subject + "</td></tr>";
                    message += "<tr><td>Notify sent date&time:</td><td>" + (DateTime.Now.ToString("dd/MMM/yyyy hh:mm tt")) + "</td></tr>";
                    message += "<tr><td>Message:</td><td>" + templateTestCaseCheckList?.Description + "</td></tr>";
                    message += "</table>";
                    mailServiceModel.FromName = username;
                    mailServiceModel.Body = message;
                }
                else
                {
                    mailServiceModel.Subject = "Notify -" + subject;
                    if (subject == "" && documentsModels == null)
                    {
                        var ticketNo = productActivityApp?.ProductionActivityApp?.ProdOrderNo;
                        if (value.NotifyType == "IPIR")
                        {
                            ticketNo = ipirReport?.ProdOrder;
                            mailServiceModel.Subject = "Notify - IPIR(" + ticketNo + ")";
                        }
                        else
                        {
                            mailServiceModel.Subject = "Notify - Production Activity(" + ticketNo + ")";
                        }
                    }

                    var mainprofileId = GetMainProfileId(fileProfileTypeItems, documentsModels?.FilterProfileTypeId);

                    var userNameId = value.AddedByUserID != null ? value.AddedByUserID : value.ModifiedByUserID;
                    var username = _context.ApplicationUser.FirstOrDefault(f => f.UserId == userNameId)?.UserName;
                    var url = Uri.EscapeUriString(value.Baseurl + "DMS/fileProfileTypeDocTree?fileProfileTypeId=" + documentsModels?.FilterProfileTypeId + "&mainfileProfileTypeId=" + (mainprofileId?.FirstOrDefault()?.FileProfileTypeId) + "&documentName=" + subject.Split(".")[0]);
                    if (value.NotifyType == "ProductionApp")
                    {
                        message += "Please be informed that you have received notify from " + username + " in Production Activity:";
                    }
                    else if (value.NotifyType == "IPIR")
                    {
                        message += "Please be informed that you have received notify from " + username + " in IPIR:";
                    }
                    else
                    {
                        message += "Please be informed that you have received notify from " + username + " in portal:";
                    }
                    message += "<table>";
                    if (DocumentPath != "")
                    {
                        message += "<tr><td>Location of file:</td><td>" + DocumentPath + " </td></tr>";

                        if (documentsModels != null && documentsModels.FilterProfileTypeId != null)
                        {
                            message += "<tr><td>Document name:</td><td><a href=" + url + ">" + subject + "</a></td></tr>";
                        }
                        else
                        {
                            message += "<tr><td>Document name:</td><td>" + subject + "</td></tr>";
                        }
                    }
                    else
                    {
                        if (subject != "")
                        {
                            message += "<tr><td>Document name:</td><td>" + subject + "</td></tr>";
                        }
                    }
                    if (value.NotifyType == "ProductionApp")
                    {
                        message += "<tr><td>Manufacturing Process:</td><td>" + productActivityApp?.ManufacturingProcessChild?.Value + "</td></tr>";
                        message += "<tr><td>Category:</td><td>" + productActivityApp?.ProdActivityCategoryChild?.Value + "</td></tr>";
                        message += "<tr><td>Action List:</td><td>" + productActivityApp?.ProdActivityActionChildDNavigation?.Value + "</td></tr>";
                        message += "<tr><td>Result:</td><td>" + productActivityApp?.ProdActivityResult?.Value + "</td></tr>";
                    }
                    if (value.NotifyType == "IPIR")
                    {
                        message += "<tr><td>Manufacturing Process:</td><td>" + ipirReport?.ManufacturingProcess?.Value + "</td></tr>";
                    }

                    message += "<tr><td>Notify sent date&time:</td><td>" + (DateTime.Now.ToString("dd/MMM/yyyy hh:mm tt")) + "</td></tr>";
                    message += "<tr><td>Due date:</td><td>" + (value.DueDate != null ? value.DueDate.Value.ToString("dd/MMM/yyyy") : "") + "</td></tr>";
                    message += "<tr><td>Message:</td><td>" + value.Remarks + "</td></tr>";
                    message += "</table>";
                    mailServiceModel.FromName = username;
                    mailServiceModel.Body = message;
                }
                if (documentsModels != null)
                {
                    mailServiceModel.Attachments = addAttachment(documentsModels);
                }
                _mailService.SendEmailService(mailServiceModel);
            }
        }
        private AttachmentsPathModel addAttachment(DocumentsModel docs)
        {
            AttachmentsPathModel attachmentsPathModel = new AttachmentsPathModel();
            if (docs != null && docs.DocumentID > 0)
            {
                List<string> pathList = new List<string>();
                string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
                string newFolderName = "Attachment";
                string serverPath = System.IO.Path.Combine(folderName, newFolderName);
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }

                var sessionId = Guid.NewGuid();
                string FromLocation = folderName + @"\" + newFolderName;
                serverPath = System.IO.Path.Combine(FromLocation, sessionId.ToString());
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                string newPath = folderName + @"\" + newFolderName + @"\" + sessionId.ToString();
                var doc = _context.Documents.FirstOrDefault(f => f.DocumentId == docs.DocumentID)?.FileData;
                string FromLocations = newPath + @"\" + docs.FileName;
                var compressedData = docs.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(doc) : doc;
                System.IO.File.WriteAllBytes((string)FromLocations, compressedData);
                pathList.Add(FromLocations);

                attachmentsPathModel.PathName = newPath;
                attachmentsPathModel.Attachments = pathList;
            }
            return attachmentsPathModel;
        }
        private List<string> GetAllFileProfileTypeName(List<FileProfileTypeModel> foldersListItems, FileProfileTypeModel s, List<string> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNotifyDocument")]
        public NotifyDocumentModel Put(NotifyDocumentModel value)
        {
            var notifyDocument = _context.NotifyDocument.SingleOrDefault(p => p.NotifyDocumentId == value.NotifyDocumentId);
            value.SessionId = notifyDocument.SessionId;
            // notifyDocument.DocumentId = value.DocumentId;
            //notifyDocument.Remarks = value.Remarks;
            //notifyDocument.UserId = value.UserId;
            // notifyDocument.ModifiedByUserId = value.ModifiedByUserID;
            notifyDocument.ModifiedDate = DateTime.Now;
            //  notifyDocument.StatusCodeId = value.StatusCodeID.Value;
            notifyDocument.IsQuote = value.IsQuote;
            notifyDocument.NotifyDocumentId = value.NotifyDocumentId;
            notifyDocument.IsSenderClose = value.IsSenderClose;
            notifyDocument.NotifyType = value.NotifyType;
            notifyDocument.ProductionActivityAppLineId = value.ProductionActivityAppLineId;
            // notifyDocument.IsUrgent = value.IsUrgent;
            //var notifyDocumentUserGroups = _context.NotifyDocumentUserGroup.Where(n => n.NotifyDocumentId == value.NotifyDocumentId).ToList();
            //if (notifyDocumentUserGroups != null)
            //{
            //    _context.NotifyDocumentUserGroup.RemoveRange(notifyDocumentUserGroups);
            //    _context.SaveChanges();
            //}
            //if (value.UserGroupIDs != null)
            //{
            //    value.UserGroupIDs.ForEach(g =>
            //    {
            //        NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
            //        notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
            //        notifyDocumentUserGroup.UserGroupId = g;
            //        _context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
            //    });
            //}
            //if (value.UserIDs != null)
            //{
            //    value.UserIDs.ForEach(u =>
            //    {
            //        NotifyDocumentUserGroup notifyDocumentUserGroup = new NotifyDocumentUserGroup();
            //        notifyDocumentUserGroup.NotifyDocumentId = notifyDocument.NotifyDocumentId;
            //        notifyDocumentUserGroup.UserId = u;
            //        _context.NotifyDocumentUserGroup.Add(notifyDocumentUserGroup);
            //    });
            //}
            _context.SaveChanges();
            SendEmailMessage(value);
            return value;
        }
        [HttpPut]
        [Route("UpdateSendNotifyDueDate")]
        public NotifyDueDateModel UpdateSendNotifyDueDate(NotifyDueDateModel value)
        {
            var notifyDocument = _context.NotifyDocument.SingleOrDefault(p => p.NotifyDocumentId == value.NotifyDocumentId);
            if (notifyDocument != null)
            {
                notifyDocument.DueDate = value.DueDate;
                notifyDocument.ModifiedByUserId = value.ModifiedByUserID;
                notifyDocument.ModifiedDate = DateTime.Now;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("CreateNotifylongNotes")]
        public NotifyLongNotesModel CreateNotifylongNotes(NotifyLongNotesModel value)
        {
            var existLongnotes = _context.NotifyLongNotes.FirstOrDefault(f => f.NotifyDocumentId == value.NotifyDocumentId);
            if (existLongnotes == null && value.NotifyDocumentId > 0)
            {
                var notifylongnotes = new NotifyLongNotes
                {
                    NotifyDocumentId = value.NotifyDocumentId,
                    LongNotes = value.DetailNotes,

                };
                _context.NotifyLongNotes.Add(notifylongnotes);
                _context.SaveChanges();
                value.NotifyLongNotesId = notifylongnotes.NotifyLongNotesId;
            }
            else if (existLongnotes != null)
            {
                var notifylongnotes = new NotifyLongNotes
                {
                    NotifyDocumentId = value.NotifyDocumentId,
                    LongNotes = value.DetailNotes,
                    ParentId = existLongnotes.NotifyLongNotesId
                };
                _context.NotifyLongNotes.Add(notifylongnotes);
                _context.SaveChanges();
                value.DetailNotes = notifylongnotes.LongNotes;
                value.NotifyLongNotesId = notifylongnotes.NotifyLongNotesId;
            }
            var notifyDoc = _context.NotifyDocument.Where(f => f.NotifyDocumentId == value.NotifyDocumentId).Select(s => new { s.NotifyDocumentId, s.Remarks, s.QuoteNotifyId }).FirstOrDefault();
            NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel();
            if (notifyDoc != null && notifyDoc.QuoteNotifyId != null)
            {
                notifyDocumentModel.DueDate = _context.NotifyDocument.Where(f => f.NotifyDocumentId == notifyDoc.QuoteNotifyId).FirstOrDefault()?.DueDate;
                notifyDocumentModel.SessionId = _context.NotifyDocument.Where(f => f.NotifyDocumentId == notifyDoc.QuoteNotifyId).FirstOrDefault()?.SessionId;
            }

            notifyDocumentModel.NotifyDocumentId = value.NotifyDocumentId.Value;
            notifyDocumentModel.Remarks = notifyDoc?.Remarks + "<br>" + value.DetailNotes;
            notifyDocumentModel.AddedByUserID = value.AddedByUserID;
            notifyDocumentModel.Baseurl = value.Baseurl;
            SendEmailMessage(notifyDocumentModel);
            return value;
        }
        [HttpPut]
        [Route("CloseSenderNotifyDocument")]
        public NotifyDocumentModel CloseSenderNotifyDocument(NotifyDocumentModel value)
        {
            //var userGroupIds = _context.UserGroupUser.Where(s => s.UserId == value.UserId).AsNoTracking().Select(s => s.UserGroupId).ToList();
            var notifyDocument = _context.NotifyDocument.FirstOrDefault(p => p.NotifyDocumentId == value.NotifyDocumentId && p.AddedByUserId == value.UserId);
            if (notifyDocument != null)
            {
                notifyDocument.IsSenderClose = true;

                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("CloseNotifyDocument")]
        public NotifyDocumentModel CloseNotifyDocument(NotifyDocumentModel value)
        {
            var userGroupIds = _context.UserGroupUser.Where(s => s.UserId == value.UserId).AsNoTracking().Select(s => s.UserGroupId).ToList();
            var notifyDocumentUserGroup = _context.NotifyDocumentUserGroup.FirstOrDefault(p => p.NotifyDocumentId == value.NotifyDocumentId && p.UserGroupId == null && p.UserId == value.UserId);
            if (notifyDocumentUserGroup != null)
            {
                if (notifyDocumentUserGroup.UserId != null && notifyDocumentUserGroup.UserId == value.UserId)
                {
                    notifyDocumentUserGroup.IsClosed = true;
                }
                _context.SaveChanges();
            }
            else
            {
                var notifyDocumentUserGroups = _context.NotifyDocumentUserGroup.FirstOrDefault(p => p.NotifyDocumentId == value.NotifyDocumentId && p.UserId == null);
                if (notifyDocumentUserGroups != null)
                {
                    if (notifyDocumentUserGroups.UserGroupId != null)
                    {
                        var notifyUserGroupUser = _context.NotifyUserGroupUser.FirstOrDefault(p => p.NotifyDocumentUserGroupId == notifyDocumentUserGroups.NotifyDocumentUserGroupId && p.UserId == value.UserId && (userGroupIds.Contains(p.UserGroupId)));

                        if (notifyUserGroupUser != null)
                        {
                            notifyUserGroupUser.IsClosed = true;
                        }

                    }
                    _context.SaveChanges();
                }
            }





            return value;
        }

        [HttpPut]
        [Route("CloseBothNotifyDocument")]
        public NotifyDocumentModel CloseBothNotifyDocument(NotifyDocumentModel value)
        {

            var userGroupIds = _context.UserGroupUser.Where(s => s.UserId == value.UserId).AsNoTracking().Select(s => s.UserGroupId).ToList();
            var notifyDocumentIds = _context.NotifyDocument.Where(n => n.DocumentId == value.DocumentID).Select(n => n.NotifyDocumentId).ToList();
            var notifyDocumentUserGroup = _context.NotifyDocumentUserGroup.Where(p => notifyDocumentIds.Contains(p.NotifyDocumentId.Value) && p.UserGroupId == null && p.UserId == value.UserId).ToList();
            if (notifyDocumentUserGroup != null && notifyDocumentUserGroup.Count > 0)
            {
                notifyDocumentUserGroup.ForEach(n =>
                {


                    if (n.UserId != null && n.UserId == value.UserId)
                    {
                        var notify = _context.NotifyDocumentUserGroup.FirstOrDefault(f => f.NotifyDocumentUserGroupId == n.NotifyDocumentUserGroupId);
                        if (notify != null)
                        {
                            notify.IsClosed = true;
                            _context.SaveChanges();
                        }
                    }

                });
            }
            else
            {
                var notifyDocumentUserGroups = _context.NotifyDocumentUserGroup.FirstOrDefault(p => notifyDocumentIds.Contains(p.NotifyDocumentId.Value) && p.UserId == null && userGroupIds.Contains(p.UserGroupId));
                if (notifyDocumentUserGroups != null)
                {
                    if (notifyDocumentUserGroups.UserGroupId != null)
                    {
                        var notifyUserGroupUser = _context.NotifyUserGroupUser.Where(p => p.NotifyDocumentUserGroupId == notifyDocumentUserGroups.NotifyDocumentUserGroupId && p.UserId == value.UserId).ToList();


                        if (notifyUserGroupUser != null && notifyUserGroupUser.Count > 0)
                        {
                            notifyUserGroupUser.ForEach(f =>
                            {
                                var closeGroupUser = _context.NotifyUserGroupUser.FirstOrDefault(p => p.NotifyDocumentUserGroupId == f.NotifyDocumentUserGroupId && p.UserId == value.UserId);
                                if (closeGroupUser != null)
                                {
                                    closeGroupUser.IsClosed = true;
                                    _context.SaveChanges();
                                }

                            });

                        }

                    }
                    _context.SaveChanges();
                }
            }
            var notifyDocument = _context.NotifyDocument.Where(p => notifyDocumentIds.Contains(p.NotifyDocumentId)).ToList();
            if (notifyDocument != null && notifyDocument.Count > 0)
            {
                notifyDocument.ForEach(n =>
                {
                    var sendoc = _context.NotifyDocument.FirstOrDefault(f => f.NotifyDocumentId == n.NotifyDocumentId);
                    if (sendoc != null)
                    {

                        sendoc.IsSenderClose = true;

                        _context.SaveChanges();
                    }
                });
            }




            return value;
        }
        [HttpGet]
        [Route("GetNotifyHistory")]
        public List<NotifyDocumentModel> GetNotifyHistory(int id)
        {
            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            var notifyDocument = _context.NotifyDocument.FirstOrDefault(n => n.NotifyDocumentId == id);
            var notifyDocuments = _context.NotifyDocument
              .Include(a => a.AddedByUser)
              .Include(b => b.ModifiedByUser)
              .Include(c => c.StatusCode)
              .Include(g => g.NotifyDocumentUserGroup)
              .OrderByDescending(o => o.NotifyDocumentId).Where(d => d.DocumentId == notifyDocument.DocumentId && d.NotifyDocumentId != id).AsNoTracking().ToList();
            var notifyDocumentId = notifyDocuments.Select(n => n.DocumentId).ToList();
            var notifyDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, ContentType = e.ContentType }).Where(n => notifyDocumentId.Contains(n.DocumentId)).ToList();
            notifyDocuments.ForEach(s =>
             {
                 var document = notifyDocumentItems.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                 NotifyDocumentModel notifyDocumentModel = new NotifyDocumentModel
                 {
                     NotifyDocumentId = s.NotifyDocumentId,
                     UserId = s.UserId,
                     UserName = s.User?.NickName,
                     Remarks = s.Remarks,
                     DocumentName = document?.FileName,
                     StatusCodeID = s.StatusCodeId,
                     StatusCode = s.StatusCode?.CodeValue,
                     AddedByUserID = s.AddedByUserId,
                     ModifiedByUserID = s.ModifiedByUserId,
                     AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                     ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                     AddedDate = s.AddedDate,
                     ModifiedDate = s.ModifiedDate,
                     SessionId = s.SessionId,
                     DocumentID = s.DocumentId,
                     IsUrgent = s.IsUrgent,
                     IsQuote = s.IsQuote,
                     QuoteNotifyId = s.QuoteNotifyId,
                 };
                 notifyDocumentModels.Add(notifyDocumentModel);
             });

            return notifyDocumentModels;
        }

        private List<NotifyDocument> BulkUpdateNotifyDocument(List<long?> selectedDocumentIds, long? latestDocumentId)
        {
            List<NotifyDocument> notifyDocuments = new List<NotifyDocument>();
            selectedDocumentIds.ForEach(e =>
            {
                var notify = _context.NotifyDocument.FirstOrDefault(i => i.DocumentId == e);
                if (notify != null)
                {
                    notify.DocumentId = latestDocumentId;
                    notifyDocuments.Add(notify);
                }
            });
            return notifyDocuments;
        }
        [HttpGet]
        [Route("GetFolderDocumentPath")]
        public List<FileProfileTypeModel> GetFolderDocumentPath(long? id)
        {
            List<FileProfileTypeModel> fileProfilelistModels = new List<FileProfileTypeModel>();
            List<FileProfileTypeModel> fileProfileModels = new List<FileProfileTypeModel>();

            var fileProfilelist = _context.FileProfileType.AsNoTracking().ToList();
            var folderIds = fileProfilelist.Select(s => s.FileProfileTypeId).ToList();
            int idx = 0;
            fileProfilelist.OrderBy(f => f.FileProfileTypeId).ToList().ForEach(s =>
            {
                FileProfileTypeModel fileProfileModel = new FileProfileTypeModel();
                fileProfileModel.Id = ++idx;
                if (fileProfileModels.Any(a => a.FileProfileTypeId == s.ParentId))
                {
                    var folder = fileProfileModels.FirstOrDefault(g => g.FileProfileTypeId == s.ParentId);
                    //fileProfileModel.ParentId = folder.Id;
                }
                fileProfileModel.FileProfileTypeId = s.FileProfileTypeId;
                fileProfileModel.ParentId = s.ParentId;
                fileProfileModel.Name = s.Name;
                fileProfileModel.Description = s.Description;
                fileProfileModel.ModifiedByUserID = s.ModifiedByUserId;
                fileProfileModel.AddedByUserID = s.AddedByUserId;
                fileProfileModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                fileProfileModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                fileProfileModel.StatusCodeID = s.StatusCodeId;
                fileProfileModel.AddedDate = s.AddedDate;
                fileProfileModel.ModifiedDate = s.ModifiedDate;
                fileProfileModels.Add(fileProfileModel);
            });
            fileProfilelistModels = GenerateDocumentPath(fileProfileModels, id);

            return fileProfilelistModels;
        }
        private List<FileProfileTypeModel> GenerateDocumentPath(List<FileProfileTypeModel> folderslist, long? id)
        {
            List<FileProfileTypeModel> folderLocation = new List<FileProfileTypeModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in folderslist)
            {
                FileProfileTypeModel f = new FileProfileTypeModel();

                if (fn.ParentId > 0)
                {
                    f.FileProfileTypeId = fn.FileProfileTypeId;
                    f.Name = fn.Name;
                    f.ParentId = fn.ParentId;
                    if (f.ParentId != id)
                    {
                        GenerateLocation(f, fn, folderslist, emptyFolderIds);
                        folderPath += folderPath != "" ? " / " + f.Name : f.Name;
                    }
                    f.FileProfilePath = folderPath;
                    folderLocation.Add(f);

                }
                else if (fn.ParentId == null)
                {
                    folderPath = fn.Name;
                    f.FileProfilePath = folderPath;
                    f.FileProfileTypeId = fn.FileProfileTypeId;
                    f.Name = fn.Name;
                    f.ParentId = fn.ParentId;
                    f.FileProfilePath = folderPath;
                    folderLocation.Add(f);
                }
            }
            return folderLocation;
        }

        private void GenerateLocation(FileProfileTypeModel fileProfileModel, FileProfileTypeModel parentFolderModel, List<FileProfileTypeModel> fileProfileModels, List<long> emptyIds)
        {
            if (!emptyIds.Contains(parentFolderModel.FileProfileTypeId))
            {
                emptyIds.Add(parentFolderModel.FileProfileTypeId);
                parentFolderModel = fileProfileModels.FirstOrDefault(s => s.FileProfileTypeId == parentFolderModel.ParentId);

                if (parentFolderModel != null)
                {
                    fileProfileModel.FileProfilePaths.Add(parentFolderModel.Name);
                    if (parentFolderModel.ParentId != null)
                    {
                        GenerateLocation(fileProfileModel, parentFolderModel, fileProfileModels, emptyIds);
                    }
                }
            }

        }


        private long GetMainFolderDocumentID(long? id)
        {
            long mainparentId = 0;
            List<FileProfileTypeModel> fileProfileModels = new List<FileProfileTypeModel>();

            var fileProfilelist = _context.FileProfileType.AsNoTracking().ToList();
            var folderIds = fileProfilelist.Select(s => s.FileProfileTypeId).ToList();
            int idx = 0;
            fileProfilelist.OrderBy(f => f.FileProfileTypeId).ToList().ForEach(s =>
            {
                FileProfileTypeModel fileProfileModel = new FileProfileTypeModel();
                fileProfileModel.Id = ++idx;
                if (fileProfileModels.Any(a => a.FileProfileTypeId == s.ParentId))
                {
                    var folder = fileProfileModels.FirstOrDefault(g => g.FileProfileTypeId == s.ParentId);
                    //fileProfileModel.ParentId = folder.Id;
                }
                fileProfileModel.FileProfileTypeId = s.FileProfileTypeId;
                fileProfileModel.ParentId = s.ParentId;
                fileProfileModel.Name = s.Name;
                fileProfileModel.Description = s.Description;
                fileProfileModel.ModifiedByUserID = s.ModifiedByUserId;
                fileProfileModel.AddedByUserID = s.AddedByUserId;
                fileProfileModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                fileProfileModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                fileProfileModel.StatusCodeID = s.StatusCodeId;
                fileProfileModel.AddedDate = s.AddedDate;
                fileProfileModel.ModifiedDate = s.ModifiedDate;
                fileProfileModels.Add(fileProfileModel);
            });
            var mainParent = fileProfileModels.FirstOrDefault(f => f.FileProfileTypeId == id && f.ParentId == null);
            if (mainParent != null)
            {
                mainparentId = mainParent.FileProfileTypeId;
            }
            else
            {
                var filterlist = fileProfileModels.Where(f => f.FileProfileTypeId == id || f.ParentId == id).ToList();
                List<long> fProfileTypeIds = new List<long>();
                mainparentId = GenerateMainFolderID(fileProfileModels, filterlist, id, fProfileTypeIds);
            }

            return mainparentId;
        }
        private long GenerateMainFolderID(List<FileProfileTypeModel> folderslist, List<FileProfileTypeModel> filterlist, long? id, List<long> fProfileTypeIds)
        {
            long mainFolderId = 0;
            foreach (var fn in filterlist)
            {
                FileProfileTypeModel f = new FileProfileTypeModel();

                if (fn.ParentId > 0)
                {
                    filterlist = folderslist.Where(f => f.FileProfileTypeId == fn.ParentId || f.ParentId == fn.ParentId).ToList();
                    GenerateMainFolderID(folderslist, filterlist, fn.ParentId, fProfileTypeIds);
                }
                else if (fn.ParentId == null)
                {
                    mainFolderId = fn.FileProfileTypeId;
                }

                fProfileTypeIds.Add(fn.FileProfileTypeId);
            }
            return mainFolderId;
        }
    }
}

class GroupItem
{
    public long? DocumentId { get; set; }
    public long? DocumentGroupId { get; set; }
}
