﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SunwardGroupCompanyController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public SunwardGroupCompanyController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetSunwardGroupCompanys")]
        public List<SunwardGroupCompanyModel> Get()
        {
            List<SunwardGroupCompanyModel> sunwardGroupCompanyModels = new List<SunwardGroupCompanyModel>();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
          
            var sunwardGroupCompany = _context.SunwardGroupCompany.Include("AddedByUser")
                                        .Include("ModifiedByUser")
                                        .Include("StatusCode").OrderByDescending(o => o.SunwardGroupCompanyId).AsNoTracking().ToList();
            if (sunwardGroupCompany != null && sunwardGroupCompany.Count > 0)
            {
                var masterDetailIds = sunwardGroupCompany.Where(s => s.RegisteredCountryId != null).Select(s => s.RegisteredCountryId).ToList();
                if(masterDetailIds.Count>0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a=> masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
                sunwardGroupCompany.ForEach(s =>
                {
                    SunwardGroupCompanyModel sunwardGroupCompanyModel = new SunwardGroupCompanyModel
                    {
                        SunwardGroupCompanyID = s.SunwardGroupCompanyId,
                        SunwardGroupCompanyName = s.CompanyName,
                        CompanyCode = s.CompanyCode,
                        RegisteredCountryID = s.RegisteredCountryId,
                        CompanyRegistrationNo = s.CompanyRegistrationNo,
                        CompanyEstablishedDate = s.CompanyEstablishedDate,
                        GSTNo = s.Gstno,
                        RegisteredCountry = s.RegisteredCountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.RegisteredCountryId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode.CodeValue,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser.UserName,
                        ModifiedByUser = s.ModifiedByUser.UserName,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,

                    };
                    sunwardGroupCompanyModels.Add(sunwardGroupCompanyModel);
                });
            }
            return sunwardGroupCompanyModels;
        }




        [HttpPost()]
        [Route("GetData")]
        public ActionResult<SunwardGroupCompanyModel> GetData(SearchModel searchModel)
        {
            var sunwardGroupCompany = new SunwardGroupCompany();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).FirstOrDefault();
                        break;
                    case "Last":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).LastOrDefault();
                        break;
                    case "Next":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).LastOrDefault();
                        break;
                    case "Previous":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).FirstOrDefault();
                        break;
                    case "Last":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).LastOrDefault();
                        break;
                    case "Next":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderBy(o => o.SunwardGroupCompanyId).FirstOrDefault(s => s.SunwardGroupCompanyId > searchModel.Id);
                        break;
                    case "Previous":
                        sunwardGroupCompany = _context.SunwardGroupCompany.OrderByDescending(o => o.SunwardGroupCompanyId).FirstOrDefault(s => s.SunwardGroupCompanyId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SunwardGroupCompanyModel>(sunwardGroupCompany);
            if (result != null && result.SunwardGroupCompanyID > 0)
            {
                result.SunwardGroupCompanyName = result.CompanyName;
            }

            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertSunwardGroupCompany")]
        public SunwardGroupCompanyModel Post(SunwardGroupCompanyModel value)
        {
           
            var sunwardGroupCompany = new SunwardGroupCompany
            {
                CompanyName = value.SunwardGroupCompanyName,
                CompanyCode = value.CompanyCode,
                RegisteredCountryId = value.RegisteredCountryID,
                CompanyRegistrationNo = value.CompanyRegistrationNo,
                CompanyEstablishedDate = value.CompanyEstablishedDate,
                Gstno = value.GSTNo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,



            };
            _context.SunwardGroupCompany.Add(sunwardGroupCompany);
            _context.SaveChanges();
            value.SunwardGroupCompanyID = sunwardGroupCompany.SunwardGroupCompanyId;
            if (value.RegisteredCountryID > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s=>s.ApplicationMasterDetailId == value.RegisteredCountryID).AsNoTracking().ToList();
                value.RegisteredCountry = value.RegisteredCountryID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisteredCountryID).Select(m => m.Value).FirstOrDefault() : "";
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateSunwardGroupCompany")]
        public SunwardGroupCompanyModel Put(SunwardGroupCompanyModel value)
        {
           
            var sunwardGroupCompany = _context.SunwardGroupCompany.SingleOrDefault(p => p.SunwardGroupCompanyId == value.SunwardGroupCompanyID);
            sunwardGroupCompany.CompanyName = value.SunwardGroupCompanyName;
            sunwardGroupCompany.CompanyCode = value.CompanyCode;
            sunwardGroupCompany.RegisteredCountryId = value.RegisteredCountryID;
            sunwardGroupCompany.CompanyRegistrationNo = value.CompanyRegistrationNo;
            sunwardGroupCompany.CompanyEstablishedDate = value.CompanyEstablishedDate;
            sunwardGroupCompany.Gstno = value.GSTNo;
            sunwardGroupCompany.ModifiedByUserId = value.ModifiedByUserID;
            sunwardGroupCompany.ModifiedDate = DateTime.Now;
            sunwardGroupCompany.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            if (value.RegisteredCountryID > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(s => s.ApplicationMasterDetailId == value.RegisteredCountryID).AsNoTracking().ToList();
                value.RegisteredCountry = value.RegisteredCountryID != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.RegisteredCountryID).Select(m => m.Value).FirstOrDefault() : "";
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteSunwardGroupCompany")]
        public void Delete(int id)
        {
            var sunwardGroupCompany = _context.SunwardGroupCompany.SingleOrDefault(p => p.SunwardGroupCompanyId == id);
            if (sunwardGroupCompany != null)
            {
                _context.SunwardGroupCompany.Remove(sunwardGroupCompany);
                _context.SaveChanges();
            }
        }

    }
}