﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AuditLogController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AuditLogController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpPost()]
        [Route("GetAuditLogs")]
        public List<AuditLogModel> GetAuditLogs(AuditSearchModel searchModel)
        {
            var tableNames = searchModel.TableName.Split(",").ToList();
            var auditTrails = new List<AuditLogModel>();
            var primaryKeys = new List<long>() { searchModel.PrimaryKeyValue.GetValueOrDefault(0) };
            var auditLog = _context.AuditLog.Where(f => tableNames.Contains(f.TableName) && primaryKeys.Contains(f.PrimaryKeyValue.GetValueOrDefault(0))).ToList();

            var userList = _context.ApplicationUser.Select(s => new
            {
                s.UserId,
                s.UserName,
                s.LoginId,

            });

            auditLog.ForEach(f =>
            {
                if (!f.IsPrimaryKey.GetValueOrDefault(false) && !f.IsForeignKey.GetValueOrDefault(false))
                {
                    if (searchModel.DisplayColumns.Contains(f.ColumnName))
                    {
                        var userName = string.Empty;
                        if (f.AuditByUserId > 0)
                        {
                            userName = userList.FirstOrDefault(u => u.UserId == f.AuditByUserId).UserName;
                        }

                        auditTrails.Add(new AuditLogModel
                        {
                            AuditDate = f.AuditDate,
                            AuditLogId = f.AuditLogId,
                            AuditType = f.AuditType,
                            ColumnName = f.ColumnName,
                            ForeignKeyName = f.ForeignKeyName,
                            IsForeignKey = f.IsForeignKey,
                            IsModified = f.IsModified,
                            IsPrimaryKey = f.IsPrimaryKey,
                            NewValue = f.NewValue,
                            OldValue = f.OldValue,
                            PrimaryKeyName = f.PrimaryKeyName,
                            PrimaryKeyValue = f.PrimaryKeyValue,
                            TableName = f.TableName,
                            AuditByUser = userName,
                        });
                    }
                }
            });

            return auditTrails;
        }
    }
}