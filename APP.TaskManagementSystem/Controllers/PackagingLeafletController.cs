﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingLeafletController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingLeafletController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingLeaflets")]
        public List<PackageLeafletModel> Get()
        {
            List<PackageLeafletModel> packageLeafletModels = new List<PackageLeafletModel>();
            var packagingLeaflet = _context.PackagingLeaflet.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.PrintedOn).OrderByDescending(o => o.LeafletId).AsNoTracking().ToList();
            if (packagingLeaflet != null && packagingLeaflet.Count > 0)
            {
                List<long?> masterIds = packagingLeaflet.Where(w => w.PackingMaterialId != null).Select(a => a.PackingMaterialId).Distinct().ToList();
                masterIds.AddRange(packagingLeaflet.Where(w => w.PackingItemId != null).Select(a => a.PackingItemId).Distinct().ToList());
                masterIds.AddRange(packagingLeaflet.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingLeaflet.ForEach(s =>
                {
                    PackageLeafletModel packageLeafletModel = new PackageLeafletModel
                    {
                        PackingItemId = s.PackingItemId,
                        PackingMaterialId = s.PackingMaterialId,
                        LeafletId = s.LeafletId,
                        LengthSize = s.LengthSize,
                        PackingUnitId = s.PackingUnitId,
                        WidthSize = s.WidthSize,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        PrintedOnId = s.PrintedOnId,
                        PrintedOnType = s.PrintedOn != null ? s.PrintedOn.CodeValue : "",
                        LeafletTypeID = s.LeafletTypeId,
                        PackingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        PackingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo,
                        IsVersion = s.IsVersion,

                    };
                    packageLeafletModels.Add(packageLeafletModel);

                });
            }
            return packageLeafletModels;
        }

        [HttpPost]
        [Route("GetPackagingLeafletsByRefNo")]
        public List<PackageLeafletModel> GetPackagingLeafletsByRefNo(RefSearchModel refSearchModel)
        {
            List<PackageLeafletModel> packageLeafletModels = new List<PackageLeafletModel>();
            List<PackagingLeaflet> packageLeaflet = new List<PackagingLeaflet>();
            if (refSearchModel.IsHeader)
            {
                packageLeaflet = _context.PackagingLeaflet.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.PrintedOn).Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.LeafletTypeId == refSearchModel.TypeID).OrderByDescending(o => o.LeafletId).OrderByDescending(l => l.LeafletId).AsNoTracking().ToList();
            }
            else
            {
                packageLeaflet = _context.PackagingLeaflet.Include(a => a.AddedByUser).Include(b => b.ModifiedByUser).Include(c => c.PrintedOn).Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.LeafletTypeId == refSearchModel.TypeID).OrderByDescending(o => o.LeafletId).OrderByDescending(l => l.LeafletId).AsNoTracking().ToList();
            }
            if (packageLeaflet != null && packageLeaflet.Count > 0)
            {
                List<long?> masterIds = packageLeaflet.Where(w => w.PackingMaterialId != null).Select(a => a.PackingMaterialId).Distinct().ToList();
                masterIds.AddRange(packageLeaflet.Where(w => w.PackingItemId != null).Select(a => a.PackingItemId).Distinct().ToList());
                masterIds.AddRange(packageLeaflet.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packageLeaflet.ForEach(s =>
                {
                    PackageLeafletModel packageLeafletModel = new PackageLeafletModel
                    {
                        PackingItemId = s.PackingItemId,
                        PackingMaterialId = s.PackingMaterialId,
                        LeafletId = s.LeafletId,
                        LengthSize = s.LengthSize,
                        PackingUnitId = s.PackingUnitId,
                        WidthSize = s.WidthSize,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        PrintedOnId = s.PrintedOnId,
                        PrintedOnType = s.PrintedOn != null ? s.PrintedOn.CodeValue : "",
                        LeafletTypeID = s.LeafletTypeId,
                        PackingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        PackingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo,
                        IsVersion = s.IsVersion,

                    };
                    packageLeafletModels.Add(packageLeafletModel);

                });
            }

            return packageLeafletModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetPackagingLeaflet/{id:int}")]
        public ActionResult<PackageLeafletModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var packagingLeaflet = _context.PackagingLeaflet.SingleOrDefault(p => p.LeafletId == id.Value);
            var result = _mapper.Map<PackageLeafletModel>(packagingLeaflet);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackageLeafletModel> GetData(SearchModel searchModel)
        {
            var packagingLeaflet = new PackagingLeaflet();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).LastOrDefault();
                        break;
                    case "Next":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).LastOrDefault();
                        break;
                    case "Next":
                        packagingLeaflet = _context.PackagingLeaflet.OrderBy(o => o.LeafletId).FirstOrDefault(s => s.LeafletId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingLeaflet = _context.PackagingLeaflet.OrderByDescending(o => o.LeafletId).FirstOrDefault(s => s.LeafletId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackageLeafletModel>(packagingLeaflet);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingLeaflet")]
        public PackageLeafletModel Post(PackageLeafletModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "PackagingLeaflet" });
            var packagingLeaflet = new PackagingLeaflet
            {

                LengthSize = value.LengthSize,
                PackingItemId = value.PackingItemId,
                PackingMaterialId = value.PackingMaterialId,
                PackingUnitId = value.PackingUnitId,
                WidthSize = value.WidthSize,
                PrintedOnId = value.PrintedOnId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                LeafletTypeId = value.LeafletTypeID,
                IsVersion = value.IsVersion,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.PackagingLeaflet.Add(packagingLeaflet);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = packagingLeaflet.MasterProfileReferenceNo;
            value.LeafletId = packagingLeaflet.LeafletId;
            value.PackingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackageLeafletModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LeafletTypeID == 1083)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        if (value.PrintedOnId > 0)
                        {
                            value.PrintedOnType = value.PrintedOnId == 2001 ? "One Side Print" : "Both Side Print";
                            itemName = value.PrintedOnType + " ";
                        }

                        if (value.LengthSize != null)
                        {
                            itemName = itemName + value.LengthSize + "mm(L)" + " ";
                        }
                        if (value.WidthSize != null)
                        {
                            itemName = itemName + " X " + value.WidthSize + "mm(W) " + " ";
                        }
                        if (!string.IsNullOrEmpty(value.PackingMaterial))
                        {
                            itemName = itemName + value.PackingMaterial + " ";
                        }
                        itemName = itemName + "Leaflet";
                        //itemName = value.PrintedOnType + " " + value.LengthSize + "mm(L)" + " X " + value.WidthSize + "mm(W) " + value.PackingMaterial + " Leaflet";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            else if (value.LeafletTypeID == 1082)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        if (value.LengthSize != null)
                        {
                            itemName = value.LengthSize + " " + "mm" + " " + "X" + " ";
                        }
                        if (value.WidthSize != null)
                        {
                            itemName = itemName + value.WidthSize + " " + "mm" + " ";
                        }
                        itemName = itemName + "Leaflet";
                        //itemName = value.LengthSize + " " + "mm" + " " + "X" + " " + value.WidthSize + " " + "mm" + " " + "Leaflet";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingLeaflet")]
        public PackageLeafletModel Put(PackageLeafletModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingLeaflet = _context.PackagingLeaflet.SingleOrDefault(p => p.LeafletId == value.LeafletId);
            packagingLeaflet.LengthSize = value.LengthSize;
            packagingLeaflet.PackingItemId = value.PackingItemId;
            packagingLeaflet.PackingMaterialId = value.PackingMaterialId;
            packagingLeaflet.PackingUnitId = value.PackingUnitId;
            packagingLeaflet.PrintedOnId = value.PrintedOnId;
            packagingLeaflet.WidthSize = value.WidthSize;
            packagingLeaflet.ModifiedByUserId = value.ModifiedByUserID;
            packagingLeaflet.ModifiedDate = DateTime.Now;
            packagingLeaflet.StatusCodeId = value.StatusCodeID.Value;
            packagingLeaflet.LeafletTypeId = value.LeafletTypeID;
            packagingLeaflet.IsVersion = value.IsVersion;
            _context.SaveChanges();
            value.PackingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingLeaflet")]
        public void Delete(int id)
        {
            var packagingLeaflet = _context.PackagingLeaflet.SingleOrDefault(p => p.LeafletId == id);
            if (packagingLeaflet != null)
            {
                _context.PackagingLeaflet.Remove(packagingLeaflet);
                _context.SaveChanges();
            }
        }
    }
}