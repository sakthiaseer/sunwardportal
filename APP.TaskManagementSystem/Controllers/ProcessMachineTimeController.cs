﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProcessMachineTimeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProcessMachineTimeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetProcessMachineTimesAll")]
        public List<ProcessMachineTimeModel> GetProcessMachineTimesAll()
        {
            var ProcessMachineTime = _context.ProcessMachineTime.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include("StatusCode").AsNoTracking().ToList();
            List<ProcessMachineTimeModel> ProcessMachineTimeModels = new List<ProcessMachineTimeModel>();
            if (ProcessMachineTime != null)
            {
                var ProcessMachineTimeIds = ProcessMachineTime.Select(s => s.ProcessMachineTimeId).ToList();             
              
               
                ProcessMachineTime.ForEach(s =>
                {                  
                  
                    ProcessMachineTimeModel ProcessMachineTimeModel = new ProcessMachineTimeModel
                    {
                        ProcessMachineTimeId = s.ProcessMachineTimeId,
                        ProductionAreaId = s.ProductionAreaId,
                        IsRunSteroidCompaign = s.IsRunSteroidCompaign,
                        IsInterCompanyProduction = s.IsInterCompanyProduction,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        NavBatchSizeId = s.NavBatchSizeId,
                        NavMethodCodeId = s.NavMethodCodeId,
                        NavItemCategoryId = s.NavItemCategoryId,
                        NavProductCodeId = s.NavProductCodeId,

                    };
                    ProcessMachineTimeModels.Add(ProcessMachineTimeModel);
                });
            }
            return ProcessMachineTimeModels.OrderByDescending(a => a.ProcessMachineTimeId).ToList();
        }
       
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProcessMachineTimeModel> GetData(SearchModel searchModel)
        {
            var ProcessMachineTime = new ProcessMachineTime();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).FirstOrDefault();
                        break;
                    case "Last":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).LastOrDefault();
                        break;
                    case "Next":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).LastOrDefault();
                        break;
                    case "Previous":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).FirstOrDefault();
                        break;
                    case "Last":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).LastOrDefault();
                        break;
                    case "Next":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderBy(o => o.ProcessMachineTimeId).FirstOrDefault(s => s.ProcessMachineTimeId > searchModel.Id);
                        break;
                    case "Previous":
                        ProcessMachineTime = _context.ProcessMachineTime.OrderByDescending(o => o.ProcessMachineTimeId).FirstOrDefault(s => s.ProcessMachineTimeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProcessMachineTimeModel>(ProcessMachineTime);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProcessMachineTime")]
        public ProcessMachineTimeModel Post(ProcessMachineTimeModel value)
        {
            var ProcessMachineTime = new ProcessMachineTime
            {
                CompanyId = value.CompanyId,
                NavMethodCodeId = value.NavMethodCodeId,
                NavBatchSizeId = value.NavBatchSizeId,
                NavItemCategoryId = value.NavItemCategoryId,
                NavProductCodeId = value.NavProductCodeId,
                ProductionAreaId = value.ProductionAreaId,
                IsInterCompanyProduction = value.IsInterCompanyProduction,
                IsRunSteroidCompaign = value.IsRunSteroidCompaign,
                ItemId = value.ItemId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                

            };
            _context.ProcessMachineTime.Add(ProcessMachineTime);
            _context.SaveChanges();
            List<Navitems> navitems = new List<Navitems>();
            if (value.ItemId > 0)
            {
                navitems = _context.Navitems.Where(t => t.ItemId == value.ItemId).ToList();
                value.IsFPItemStrip = navitems.Where(a => a.ItemId == value.ItemId && a.No.Contains("FP") && a.BaseUnitofMeasure.ToLower().Trim().Contains("strip")).ToList().Count > 0 ? true : false;
                value.IsFPItemBox = navitems.Where(a => a.ItemId == value.ItemId && a.No.Contains("FP") && (a.BaseUnitofMeasure.ToLower().Trim().Contains("bottle") || (a.BaseUnitofMeasure.ToLower().Trim().Contains("box")))).ToList().Count > 0 ? true : false;
            }
            value.ProcessMachineTimeId = ProcessMachineTime.ProcessMachineTimeId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProcessMachineTime")]
        public ProcessMachineTimeModel Put(ProcessMachineTimeModel value)
        {
            var ProcessMachineTime = _context.ProcessMachineTime.SingleOrDefault(p => p.ProcessMachineTimeId == value.ProcessMachineTimeId);
            ProcessMachineTime.IsInterCompanyProduction = value.IsInterCompanyProduction;
            ProcessMachineTime.IsRunSteroidCompaign = value.IsRunSteroidCompaign;
            ProcessMachineTime.NavProductCodeId = value.NavProductCodeId;
            ProcessMachineTime.ModifiedByUserId = value.ModifiedByUserID;
            ProcessMachineTime.ModifiedDate = DateTime.Now;
            ProcessMachineTime.StatusCodeId = value.StatusCodeID.Value;
            ProcessMachineTime.NavBatchSizeId = value.NavBatchSizeId;
            ProcessMachineTime.NavMethodCodeId = value.NavMethodCodeId;
            ProcessMachineTime.NavItemCategoryId = value.NavItemCategoryId;
            ProcessMachineTime.ProductionAreaId = value.ProductionAreaId;
           
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProcessMachineTime")]
        public void Delete(int id)
        {
            try
            {
                var division = _context.ProcessMachineTime.SingleOrDefault(p => p.ProcessMachineTimeId == id);
                if (division != null)
                {
                    
                    _context.ProcessMachineTime.Remove(division);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("ProcessMachineTime Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
