﻿using APP.DataAccess.Models;
using APP.Common;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationFormController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationFormController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetApplicationForm")]
        public List<ApplicationFormModel> Get()
        {
            var ApplicationFormSub = _context.ApplicationFormSub.ToList();
            var ApplicationForm = _context.ApplicationForm.Include(a => a.AddedByUser)
                                    .Include(a => a.ClassificationType).Include(m => m.ModifiedByUser)
                                    .Include(s => s.StatusCode).Include(p => p.Profile).AsNoTracking().ToList();
            List<ApplicationFormModel> applicationFormModels = new List<ApplicationFormModel>();
            ApplicationForm.ForEach(s =>
            {
                ApplicationFormModel applicationFormModel = new ApplicationFormModel
                {
                    FormID = s.FormId,
                    ModuleName = s.ModuleName,
                    PathURL = s.PathUrl,
                    Name = s.Name,
                    FormType = s.FormType,
                    TableName = s.TableName,
                    ProfileID = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : string.Empty,
                    ApplicationFormSubIDs = ApplicationFormSub.Where(l => l.FormId == s.FormId) != null ? ApplicationFormSub.Where(l => l.FormId == s.FormId).Select(a => a.ApplicationFormId.Value).ToList() : new List<long>(),
                    StatusCodeID = s.StatusCodeId,
                    ClassificationTypeId = s.ClassificationTypeId,
                    ClassificationType = s.ClassificationType != null ? s.ClassificationType.CodeValue : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    ScreenId = s.ScreenId,
                    PersonInchargeId = s.PersonInchargeId,
                };
                applicationFormModels.Add(applicationFormModel);
            });
            return applicationFormModels.OrderByDescending(a => a.FormID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationFormModel> GetData(SearchModel searchModel)
        {
            var ApplicationForm = new ApplicationForm();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).LastOrDefault();
                        break;
                    case "Previous":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).FirstOrDefault();
                        break;
                    case "Last":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).LastOrDefault();
                        break;
                    case "Next":
                        ApplicationForm = _context.ApplicationForm.OrderBy(o => o.FormId).FirstOrDefault(s => s.FormId > searchModel.Id);
                        break;
                    case "Previous":
                        ApplicationForm = _context.ApplicationForm.OrderByDescending(o => o.FormId).FirstOrDefault(s => s.FormId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationFormModel>(ApplicationForm);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationForm")]
        public ApplicationFormModel Post(ApplicationFormModel value)
        {
            try
            {
                var ApplicationForm = new ApplicationForm
                {
                    ModuleName = value.ModuleName,
                    PathUrl = value.PathURL,
                    Name = value.Name,
                    FormType = value.FormType,
                    TableName = value.TableName,
                    ProfileId = value.ProfileID,
                    ClassificationTypeId = value.ClassificationTypeId,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    ScreenId = value.ScreenId,
                    PersonInchargeId = value.PersonInchargeId,
                };
                _context.ApplicationForm.Add(ApplicationForm);
                if (value.ApplicationFormSubIDs != null)
                {
                    value.ApplicationFormSubIDs.ForEach(c =>
                    {
                        var ApplicationFormSub = new ApplicationFormSub
                        {
                            ApplicationFormId = c,
                        };
                        ApplicationForm.ApplicationFormSubForm.Add(ApplicationFormSub);
                    });
                }
                _context.SaveChanges();
                value.FormID = ApplicationForm.FormId;
            }
            catch (Exception e)
            {
                throw new AppException("Screen ID Already Exits.", e);
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationForm")]
        public ApplicationFormModel Put(ApplicationFormModel value)
        {
            try
            {
                var ApplicationForm = _context.ApplicationForm.SingleOrDefault(p => p.FormId == value.FormID);
                ApplicationForm.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationForm.ModifiedDate = DateTime.Now;
                ApplicationForm.ModuleName = value.ModuleName;
                ApplicationForm.Name = value.Name;
                ApplicationForm.PathUrl = value.PathURL;
                ApplicationForm.FormType = value.FormType;
                ApplicationForm.TableName = value.TableName;
                ApplicationForm.ProfileId = value.ProfileID;
                ApplicationForm.ClassificationTypeId = value.ClassificationTypeId;
                ApplicationForm.StatusCodeId = value.StatusCodeID;
                ApplicationForm.PersonInchargeId = value.PersonInchargeId;
                ApplicationForm.ScreenId = value.ScreenId;
                _context.SaveChanges();
                var ApplicationFormSubItems = _context.ApplicationFormSub.Where(l => l.FormId == value.FormID).ToList();
                if (ApplicationFormSubItems.Count > 0)
                {
                    _context.ApplicationFormSub.RemoveRange(ApplicationFormSubItems);
                    _context.SaveChanges();
                }
                if (value.ApplicationFormSubIDs != null)
                {
                    value.ApplicationFormSubIDs.ForEach(c =>
                    {
                        var ApplicationFormSub = new ApplicationFormSub
                        {
                            ApplicationFormId = c,
                        };
                        ApplicationForm.ApplicationFormSubForm.Add(ApplicationFormSub);
                    });
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new AppException("Screen ID Already Exits.", e);
            }
            return value;
        }
        [HttpGet]
        [Route("GetApplicationFormAccess")]
        public List<ItemClassificationAccessModel> GetItemClassificationAccess(int id)
        {

            var itemClassificationAccess = _context.ItemClassificationAccess.Include("User")
                                            .Include("Role").Include("UserGroup").Where(f => f.FormId == id)
                                            .AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            List<long> userIds = itemClassificationAccess.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee.Include("Department").Include("Designation").Where(e => e.UserId != null && userIds.Distinct().ToList().Contains(e.UserId.Value)).AsNoTracking().ToList();
            List<ItemClassificationAccessModel> itemClassificationAccessModels = new List<ItemClassificationAccessModel>();
            userIds.ForEach(u =>
            {
                if (!itemClassificationAccessModels.Any(r => r.UserID == u))
                {
                    var employee = employees.FirstOrDefault(e => e.UserId == u);
                    var userUserRole = itemClassificationAccess.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                    var userGroupRole = itemClassificationAccess.FirstOrDefault(ug => ug.FormId == userUserRole.FormId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                    //List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u).Select(s => s.UserGroupId.Value).ToList();
                    ItemClassificationAccessModel itemClassificationAccessModel = new ItemClassificationAccessModel();
                    itemClassificationAccessModel.ItemClassificationAccessID = userUserRole.ItemClassificationAccessId;
                    itemClassificationAccessModel.UserID = userUserRole.UserId;
                    itemClassificationAccessModel.RoleID = userUserRole.RoleId;
                    itemClassificationAccessModel.FormId = userUserRole.FormId;
                    itemClassificationAccessModel.RoleName = userUserRole.Role.DocumentRoleName;
                    itemClassificationAccessModel.UserName = userUserRole.User.UserName;
                    itemClassificationAccessModel.NickName = employee.NickName;
                    itemClassificationAccessModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                    itemClassificationAccessModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                    itemClassificationAccessModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                    itemClassificationAccessModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                    itemClassificationAccessModels.Add(itemClassificationAccessModel);
                }
            });

            itemClassificationAccessModels = itemClassificationAccessModels.OrderByDescending(o => o.FormId).Where(s => s.FormId == id).ToList();
            return itemClassificationAccessModels;

        }
        [HttpPost]
        [Route("InsertApplicationAccess")]
        public ItemClassificationAccessModel PostRole(ItemClassificationAccessModel value)
        {
            CreateItemClassificationAccess(value);
            return value;
        }
        private void CreateItemClassificationAccess(ItemClassificationAccessModel value)
        {
            var itemClassificationAccess = new ItemClassificationAccess();
            var existingRole = _context.ItemClassificationAccess.Where(d => d.FormId == value.FormId).ToList();
            var userExistingRole = existingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = existingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();
            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            if (value.FormId > 0)
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    if (userExistingRole == null)
                    {
                        if ((userList != null) && (userList.Count > 0))
                        {
                            userList.ForEach(u =>
                            {
                                var userExisting = _context.ItemClassificationAccess.Where(d => d.UserId == u && d.FormId == value.FormId).FirstOrDefault();
                                if (userExisting == null)
                                {
                                    itemClassificationAccess = new ItemClassificationAccess
                                    {
                                        UserId = u,
                                        RoleId = value.RoleID,
                                        UserGroupId = value.UserGroupID,
                                        FormId = value.FormId,
                                    };
                                    _context.ItemClassificationAccess.Add(itemClassificationAccess);
                                    _context.SaveChanges();
                                }
                            });

                        }
                    }
                    else
                    {
                        throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                    }
                }
                else if ((userGroupList != null) && (userGroupList.Count > 0))
                {
                    if (userGroupExistingRole == null)
                    {
                        userGroupList.ForEach(ug =>
                        {
                            List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                            currentGroupUsers.ForEach(c =>
                            {
                                var userExist = _context.ItemClassificationAccess.Where(du => du.FormId == value.FormId && du.UserId == c.UserId).ToList();
                                if (userExist.Count == 0)
                                {
                                    itemClassificationAccess = new ItemClassificationAccess
                                    {
                                        UserId = c.UserId,
                                        RoleId = value.RoleID,
                                        UserGroupId = ug,
                                        FormId = value.FormId,
                                    };
                                    _context.ItemClassificationAccess.Add(itemClassificationAccess);
                                    _context.SaveChanges();
                                }
                            });
                        });

                    }
                    else
                    {
                        throw new Exception("User Permission Already Exist! If you want to change, delete the existing permission and add the new. ");
                    }
                }

            }

        }
        [HttpPut]
        [Route("UpdateApplicationFormAccess")]
        public ItemClassificationAccessModel UpdateItemClassificationAccess(ItemClassificationAccessModel value)
        {
            var itemAccess = _context.ItemClassificationAccess.Where(i => i.ItemClassificationAccessId == value.ItemClassificationAccessID).FirstOrDefault();
            if (itemAccess != null)
            {
                itemAccess.FormId = value.FormId;
                itemAccess.RoleId = value.RoleID;
                _context.SaveChanges();
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteApplicationForm")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var ApplicationForm = _context.ApplicationForm.SingleOrDefault(p => p.FormId == id);
                if (ApplicationForm != null)
                {
                    var ApplicationFormSubItems = _context.ApplicationFormSub.Where(l => l.FormId == id).ToList();
                    if (ApplicationFormSubItems.Count > 0)
                    {
                        _context.ApplicationFormSub.RemoveRange(ApplicationFormSubItems);
                        _context.SaveChanges();
                    }
                    var ItemClassificationAccess = _context.ItemClassificationAccess.Where(s => s.FormId == id).ToList();
                    if (ItemClassificationAccess.Count() > 0)
                    {
                        _context.ItemClassificationAccess.RemoveRange(ItemClassificationAccess);
                        _context.SaveChanges();
                    }
                    _context.ApplicationForm.Remove(ApplicationForm);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new AppException("Application Form Mapped Someother Items", null);
            }
        }
        [HttpPut]
        [Route("DeleteApplicationFormAccess")]
        public ItemClassificationAccessModel DeleteItemClassificationAccess(ItemClassificationAccessModel value)
        {

            var selectItemClassificationAccessIDs = value.ItemClassificationAccessIDs;
            if (selectItemClassificationAccessIDs.Count > 0)
            {
                selectItemClassificationAccessIDs.ForEach(sel =>
                {
                    var itemAccess = _context.ItemClassificationAccess.Where(p => p.ItemClassificationAccessId == sel).FirstOrDefault();
                    if (itemAccess != null)
                    {

                        var documentExistingUser = _context.ItemClassificationAccess.Where(p => p.UserId == itemAccess.UserId && p.FormId == itemAccess.FormId).ToList();
                        if (documentExistingUser.Count > 0 && documentExistingUser != null)
                        {
                            _context.ItemClassificationAccess.RemoveRange(documentExistingUser);
                        }
                        _context.ItemClassificationAccess.Remove(itemAccess);
                        _context.SaveChanges();
                    }
                });

                _context.SaveChanges();
            }


            return value;
        }

        private List<ApplicationFormSearchModel> GetApplicationSearch(int? id, int? userId)
        {
            var applicationForms = _context.ApplicationForm.AsNoTracking().ToList();
            var applicationFormsselect = applicationForms.Where(s => s.FormId == id && s.FormType == "Search").ToList();
            var formIds = new List<ApplicationFormSearchModel>();
            var TableColumnItems = new List<ApplicationFormSearchModel>();
            if (applicationFormsselect.Count > 0)
            {
                var applicationSubForms = _context.ApplicationFormSub.AsNoTracking().ToList();
                var currentSubFormFilters = applicationSubForms.Where(sf => sf.FormId == id).ToList();
                var ApplicationFormSearchList = _context.ApplicationFormSearch.Where(s => s.MainFormId == id).ToList();
                currentSubFormFilters.ToList().ForEach(s =>
                {
                    if (!formIds.Any(a => a.ApplicationFormId == s.FormId))
                    {
                        var formIdlist = new ApplicationFormSearchModel
                        {
                            ApplicationFormId = s.FormId,
                            ParentId = 0,
                        };
                        formIds.Add(formIdlist);
                    }
                    if (applicationSubForms.Any(a => a.FormId == s.ApplicationFormId))
                    {
                        LoadSubFormIds(applicationSubForms, formIds, s.ApplicationFormId, s.FormId);
                    }
                    else
                    {
                        var formIdlists = new ApplicationFormSearchModel
                        {
                            ApplicationFormId = s.ApplicationFormId,
                            ParentId = s.FormId,
                        };
                        formIds.Add(formIdlists);
                    }

                });
                if (formIds.Count > 0)
                {
                    var aForms = applicationForms.Where(s => formIds.Select(f => f.ApplicationFormId).ToList().Contains(s.FormId)).ToList();

                    if (aForms.Count > 0)
                    {
                        int i = 1;
                        aForms.ForEach(h =>
                        {
                            if (h.TableName != null)
                            {
                                var wikiType = _context.Model.FindEntityType("APP.DataAccess.Models." + h.TableName);
                                if (wikiType != null)
                                {
                                    wikiType.GetProperties().ToList().ForEach(p =>
                                    {
                                        var ApplicationFormSearchItem = ApplicationFormSearchList.FirstOrDefault(s => s.ApplicationFormId == h.FormId && s.ColumnName == p.Name);
                                        var TableColumnList = new ApplicationFormSearchModel
                                        {
                                            ApplicationSearchFormId = (ApplicationFormSearchItem == null) ? 0 : ApplicationFormSearchItem.ApplicationSearchFormId,
                                            UniqueId = i,
                                            ColumnName = p.Name,
                                            DisplayName = (ApplicationFormSearchItem == null) ? p.Name : ApplicationFormSearchItem.DisplayName,
                                            IsReadOnly = (ApplicationFormSearchItem == null) ? false : ApplicationFormSearchItem.IsReadOnly,
                                            IsVisible = (ApplicationFormSearchItem == null) ? false : ApplicationFormSearchItem.IsVisible,
                                            MainFormId = id,
                                            ApplicationFormId = h.FormId,
                                            TableName = h.TableName,
                                            ParentId = formIds.Where(s => s.ApplicationFormId == h.FormId).Select(s => s.ParentId.Value).FirstOrDefault(),
                                            AddedByUserID = userId,
                                            ModifiedByUserID = userId,
                                            StatusCodeID = 1,

                                        };
                                        i++;
                                        TableColumnItems.Add(TableColumnList);
                                    });
                                }
                            }
                        });

                    }
                }
            }
            return TableColumnItems;
        }
        private void LoadSubFormIds(List<ApplicationFormSub> applicationSubForms, List<ApplicationFormSearchModel> formIds, long? id, long? parentId)
        {
            applicationSubForms.ForEach(s =>
            {
                if (s.FormId == id)
                {
                    var formIdlist = new ApplicationFormSearchModel
                    {
                        ApplicationFormId = s.FormId,
                        ParentId = parentId,
                    };
                    formIds.Add(formIdlist);
                    if (applicationSubForms.Any(a => a.FormId == s.ApplicationFormId))
                    {
                        LoadSubFormIds(applicationSubForms, formIds, s.ApplicationFormId, s.FormId);
                    }
                    else
                    {
                        var formIdlists = new ApplicationFormSearchModel
                        {
                            ApplicationFormId = s.ApplicationFormId,
                            ParentId = s.FormId,
                        };
                        formIds.Add(formIdlists);
                    }
                }
            });
        }

        [HttpGet]
        [Route("GetApplicationFormSearch")]
        public List<ApplicationFormSearchModel> GetApplicationFormSearchGet(int? id, int? userId)
        {
            return GetApplicationSearch(id, userId);
        }
        [HttpPost]
        [Route("InsertApplicationFormSearch")]
        public void ApplicationFormSearch(ApplicationFormSearchListModel value)
        {
            value.ApplicationFormSearchList.ForEach(s =>
            {
                if (s.ApplicationSearchFormId > 0)
                {
                    var ApplicationForm = _context.ApplicationFormSearch.SingleOrDefault(p => p.ApplicationSearchFormId == s.ApplicationSearchFormId);
                    ApplicationForm.MainFormId = s.MainFormId;
                    ApplicationForm.ApplicationFormId = s.ApplicationFormId;
                    ApplicationForm.ParentId = (s.ParentId == 0) ? null : s.ParentId;
                    ApplicationForm.ColumnName = s.ColumnName;
                    ApplicationForm.DisplayName = s.DisplayName;
                    ApplicationForm.IsReadOnly = s.IsReadOnly;
                    ApplicationForm.IsVisible = s.IsVisible;
                    ApplicationForm.StatusCodeId = s.StatusCodeID;
                    ApplicationForm.ModifiedByUserId = s.ModifiedByUserID;
                    ApplicationForm.ModifiedDate = s.ModifiedDate;
                    _context.SaveChanges();
                }
                else
                {
                    _context.ApplicationFormSearch.Add(new ApplicationFormSearch
                    {
                        MainFormId = s.MainFormId,
                        ApplicationFormId = s.ApplicationFormId,
                        ParentId = (s.ParentId == 0) ? null : s.ParentId,
                        ColumnName = s.ColumnName,
                        DisplayName = s.DisplayName,
                        IsReadOnly = s.IsReadOnly,
                        IsVisible = s.IsVisible,
                        StatusCodeId = s.StatusCodeID,
                        AddedByUserId = s.AddedByUserID,
                        AddedDate = DateTime.Now,
                    });
                }
                _context.SaveChanges();
            });
        }

        [HttpGet]
        [Route("GetApplicationFormByScreenID")]
        public ApplicationFormModel GetApplicationFormByScreenID(string screenId)
        {
            var ApplicationFormSub = _context.ApplicationFormSub.ToList();
            var applicationForm = _context.ApplicationForm.Include(a => a.AddedByUser)
                                    .Include(a => a.ClassificationType).Include(m => m.ModifiedByUser)
                                    .Include(s => s.StatusCode).Include(p => p.Profile).Where(s => s.ScreenId == screenId).FirstOrDefault();
            ApplicationFormModel applicationFormModel = new ApplicationFormModel();
            if (applicationForm != null)
            {
                applicationFormModel.FormID = applicationForm.FormId;
                applicationFormModel.ModuleName = applicationForm.ModuleName;
                applicationFormModel.PathURL = applicationForm.PathUrl;
                applicationFormModel.Name = applicationForm.Name;
                applicationFormModel.FormType = applicationForm.FormType;
                applicationFormModel.TableName = applicationForm.TableName;
                applicationFormModel.ProfileID = applicationForm.ProfileId;
                applicationFormModel.ProfileName = applicationForm.Profile != null ? applicationForm.Profile.Name : string.Empty;
                applicationFormModel.ApplicationFormSubIDs = ApplicationFormSub.Where(l => l.FormId == applicationForm.FormId) != null ? ApplicationFormSub.Where(l => l.FormId == applicationForm.FormId).Select(a => a.ApplicationFormId.Value).ToList() : new List<long>();
                applicationFormModel.StatusCodeID = applicationForm.StatusCodeId;
                applicationFormModel.ClassificationTypeId = applicationForm.ClassificationTypeId;
                applicationFormModel.ClassificationType = applicationForm.ClassificationType != null ? applicationForm.ClassificationType.CodeValue : "";
                applicationFormModel.StatusCode = applicationForm.StatusCode != null ? applicationForm.StatusCode.CodeValue : "";
                applicationFormModel.AddedByUserID = applicationForm.AddedByUserId;
                applicationFormModel.ModifiedByUserID = applicationForm.ModifiedByUserId;
                applicationFormModel.AddedByUser = applicationForm.AddedByUser != null ? applicationForm.AddedByUser.UserName : "";
                applicationFormModel.ModifiedByUser = applicationForm.ModifiedByUser != null ? applicationForm.ModifiedByUser.UserName : "";
                applicationFormModel.AddedDate = applicationForm.AddedDate;
                applicationFormModel.ModifiedDate = applicationForm.ModifiedDate;
                applicationFormModel.ScreenId = applicationForm.ScreenId;
                applicationFormModel.PersonInchargeId = applicationForm.PersonInchargeId;
            }
            return applicationFormModel;
        }

        [HttpGet]
        [Route("GetMobileApplicationForm")]
        public ApplicationFormModel GetProfileIdByScreen(string screenId)
        {
            var applicationForm = _context.ApplicationForm.Where(s => s.ScreenId == screenId)?.FirstOrDefault();
            var fileProfileType = _context.FileProfileType.FirstOrDefault(p => p.ProfileId == applicationForm.ProfileId);
            ApplicationFormModel applicationFormModel = new ApplicationFormModel();
            if (applicationForm != null)
            {
                applicationFormModel.FormID = applicationForm.FormId;
                applicationFormModel.ProfileID = applicationForm.ProfileId;
                applicationFormModel.FileProfileTypeID = fileProfileType.FileProfileTypeId;
                applicationFormModel.ScreenId = applicationForm.ScreenId;
            }
            return applicationFormModel;
        }
    }
}