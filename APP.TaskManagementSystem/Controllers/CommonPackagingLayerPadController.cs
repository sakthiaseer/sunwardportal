﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingLayerPadController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingLayerPadController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingLayerPad")]
        public List<CommonPackagingLayerPadModel> Get(string refNo)
        {
            var commonPackagingLayerPad = _context.CommonPackagingLayerPad
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(b => b.BottleCapUseForItems)
                .AsNoTracking().ToList();
            List<CommonPackagingLayerPadModel> commonPackagingLayerPadModel = new List<CommonPackagingLayerPadModel>();
            if (commonPackagingLayerPad.Count > 0)
            {
                List<long?> masterIds = commonPackagingLayerPad.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingLayerPad.ForEach(s =>
                {
                    CommonPackagingLayerPadModel commonPackagingLayerPadModels = new CommonPackagingLayerPadModel
                    {
                        LayerPadSpecificationId = s.LayerPadSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.LayerPadSpecificationId == s.LayerPadSpecificationId).Select(b => b.UseForItemId).ToList(),
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                    };
                    commonPackagingLayerPadModel.Add(commonPackagingLayerPadModels);
                });
            }
            return commonPackagingLayerPadModel.Where(w => w.LinkProfileReferenceNo == refNo).OrderByDescending(a => a.LayerPadSpecificationId).ToList();
        }
        [HttpPost]
        [Route("GetCommonPackagingLayerPadByRefNo")]
        public List<CommonPackagingLayerPadModel> GetCommonPackagingLayerPadByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingLayerPad = _context.CommonPackagingLayerPad
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(b => b.BottleCapUseForItems).Where(w => w.LayerPadSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingLayerPad = commonPackagingLayerPad.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingLayerPad = commonPackagingLayerPad.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingLayerPads = commonPackagingLayerPad.AsNoTracking().ToList();
            List<CommonPackagingLayerPadModel> commonPackagingLayerPadModel = new List<CommonPackagingLayerPadModel>();
            if (commonPackagingLayerPads.Count > 0)
            {
                List<long?> masterIds = commonPackagingLayerPads.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList();
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingLayerPads.ForEach(s =>
                {
                    CommonPackagingLayerPadModel commonPackagingLayerPadModels = new CommonPackagingLayerPadModel
                    {
                        LayerPadSpecificationId = s.LayerPadSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        Set = s.Set,
                        MeasurementLength = s.MeasurementLength,
                        MeasurementWidth = s.MeasurementWidth,
                        StatusCodeID = s.StatusCodeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.LayerPadSpecificationId == s.LayerPadSpecificationId).Select(b => b.UseForItemId).ToList(),
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo
                    };
                    commonPackagingLayerPadModel.Add(commonPackagingLayerPadModels);
                });
            }
            return commonPackagingLayerPadModel.OrderByDescending(o => o.LayerPadSpecificationId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingLayerPadModel> GetData(SearchModel searchModel)
        {
            var commonPackagingLayerPad = new CommonPackagingLayerPad();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderBy(o => o.LayerPadSpecificationId).FirstOrDefault(s => s.LayerPadSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingLayerPad = _context.CommonPackagingLayerPad.OrderByDescending(o => o.LayerPadSpecificationId).FirstOrDefault(s => s.LayerPadSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingLayerPadModel>(commonPackagingLayerPad);
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingLayerPad")]
        public CommonPackagingLayerPadModel Post(CommonPackagingLayerPadModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "CommonPackagingLayerPad" });

            var commonPackagingLayerPad = new CommonPackagingLayerPad
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                MeasurementWidth = value.MeasurementWidth,
                MeasurementLength = value.MeasurementLength,
                ProfileLinkReferenceNo = profileNo,
                VersionControl = value.VersionControl,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingLayerPad.Add(commonPackagingLayerPad);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = commonPackagingLayerPad.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingLayerPad.ProfileLinkReferenceNo;
            value.LayerPadSpecificationId = commonPackagingLayerPad.LayerPadSpecificationId;
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.LayerPadSpecificationId == value.LayerPadSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        LayerPadSpecificationId = value.LayerPadSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(CommonPackagingLayerPadModel value)
        {
            value.PackagingItemName = "";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                var itemName = "";
                if (itemHeader != null)
                {
                    if (value.MeasurementLength != null && value.MeasurementLength > 0)
                    {
                        itemName += value.MeasurementLength + "mm" + "(L)" + " ";
                    }
                    if (value.MeasurementWidth != null && value.MeasurementWidth > 0)
                    {
                        itemName += "X" + " " + value.MeasurementWidth + "mm" + "(W)" + " ";
                    }
                    itemName += "Layer pad";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingLayerPad")]
        public CommonPackagingLayerPadModel Put(CommonPackagingLayerPadModel value)
        {
            var commonPackagingLayerPad = _context.CommonPackagingLayerPad.SingleOrDefault(p => p.LayerPadSpecificationId == value.LayerPadSpecificationId);
            commonPackagingLayerPad.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingLayerPad.Set = value.Set;
            commonPackagingLayerPad.MeasurementWidth = value.MeasurementWidth;
            commonPackagingLayerPad.MeasurementLength = value.MeasurementLength;
            commonPackagingLayerPad.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingLayerPad.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingLayerPad.VersionControl = value.VersionControl;
            commonPackagingLayerPad.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingLayerPad.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingLayerPad.ModifiedDate = DateTime.Now;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.LayerPadSpecificationId == value.LayerPadSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.LayerPadSpecificationId == value.LayerPadSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        LayerPadSpecificationId = value.LayerPadSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingLayerPad")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingLayerPad = _context.CommonPackagingLayerPad.Where(p => p.LayerPadSpecificationId == id).FirstOrDefault();
                if (commonPackagingLayerPad != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.LayerPadSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingLayerPad.Remove(commonPackagingLayerPad);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}