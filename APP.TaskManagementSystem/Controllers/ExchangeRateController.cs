using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ExchangeRateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        public ExchangeRateController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]
        [Route("GetExchangeVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ExchangeRateModel>>> GetExchangeVersion(string sessionID)
        {
            return await _repository.GetList<ExchangeRateModel>(sessionID);
        }
        [HttpGet]
        [Route("GetExchangeLineVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ExchangeRateLineModel>>> GetExchangeLineVersion(string sessionID)
        {
            return await _repository.GetList<ExchangeRateLineModel>(sessionID);
        }
        [HttpGet]
        [Route("GetExchangeRate")]
        public List<ExchangeRateModel> GetExchangeRate()
        {

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var exchangeRate = _context.ExchangeRate.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();

            List<ExchangeRateModel> exchangeRateModel = new List<ExchangeRateModel>();
            exchangeRate.ForEach(s =>
            {
                ExchangeRateModel exchangeRateModels = new ExchangeRateModel
                {
                    ExchangeRateId = s.ExchangeRateId,
                    PurposeId = s.PurposeId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                    PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                    Description = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Description).SingleOrDefault() : "",
                    ValidPeriodFrom = s.ValidPeriodFrom,
                    ValidPeriodTo = s.ValidPeriodTo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,
                };
                exchangeRateModel.Add(exchangeRateModels);
            });
            return exchangeRateModel.OrderByDescending(a => a.ExchangeRateId).ToList();

        }

        [HttpGet]
        [Route("GetExchangeRateLineID")]
        public List<ExchangeRateLineModel> GetExchangeRateLineID(long id)
        {

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var exchangeRateLine = _context.ExchangeRateLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
            List<ExchangeRateLineModel> exchangeRateLineModel = new List<ExchangeRateLineModel>();
            exchangeRateLine.ForEach(s =>
            {
                ExchangeRateLineModel exchangeRateLineModels = new ExchangeRateLineModel
                {
                    ExchangeRateLineId = s.ExchangeRateLineId,
                    ExchangeRateId = s.ExchangeRateId,
                    Units = s.Units,
                    CurrencyId = s.CurrencyId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                    CurrencyName1 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                    EquivalentUnitId = s.EquivalentUnitId,
                    EquivalebtCurrencyId = s.EquivalebtCurrencyId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                    CurrencyName2 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.EquivalebtCurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                exchangeRateLineModel.Add(exchangeRateLineModels);
            });
            return exchangeRateLineModel.Where(w => w.ExchangeRateId == id).OrderByDescending(a => a.ExchangeRateLineId).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ExchangeRateModel> GetData(SearchModel searchModel)
        {


            var exchangeRate = new ExchangeRate();
            var exchangeRateModel = new ExchangeRateModel();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).FirstOrDefault();
                        break;
                    case "Last":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).LastOrDefault();
                        break;
                    case "Next":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).LastOrDefault();
                        break;
                    case "Previous":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).FirstOrDefault();
                        break;
                    case "Last":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).LastOrDefault();
                        break;
                    case "Next":
                        exchangeRate = _context.ExchangeRate.OrderBy(o => o.ExchangeRateId).FirstOrDefault(s => s.ExchangeRateId > searchModel.Id);
                        break;
                    case "Previous":
                        exchangeRate = _context.ExchangeRate.OrderByDescending(o => o.ExchangeRateId).FirstOrDefault(s => s.ExchangeRateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ExchangeRateModel>(exchangeRate);


            if (result != null)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                result.Description = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == result.PurposeId).Select(a => a.Description).SingleOrDefault() : "";
                var exchangeRateLine = _context.ExchangeRateLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).AsNoTracking().ToList();
                List<ExchangeRateLineModel> exchangeRateLineModel = new List<ExchangeRateLineModel>();
                exchangeRateLine.ForEach(s =>
                {
                    ExchangeRateLineModel exchangeRateLineModels = new ExchangeRateLineModel
                    {
                        ExchangeRateLineId = s.ExchangeRateLineId,
                        ExchangeRateId = s.ExchangeRateId,
                        Units = s.Units,
                        CurrencyId = s.CurrencyId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                        CurrencyName1 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                        EquivalentUnitId = s.EquivalentUnitId,
                        EquivalebtCurrencyId = s.EquivalebtCurrencyId,//applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Units).Select(a => a.Value).SingleOrDefault() : "",   
                        CurrencyName2 = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.EquivalebtCurrencyId).Select(a => a.Value).SingleOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    exchangeRateLineModel.Add(exchangeRateLineModels);
                });
                exchangeRateLineModel.Where(w => w.ExchangeRateId == result.ExchangeRateId).OrderByDescending(a => a.ExchangeRateLineId).ToList();
                if (exchangeRateLineModel.Count > 0)
                {
                    result.ExchangeRateLine = exchangeRateLineModel;
                }

            }

            return result;
        }



        [HttpGet]
        [Route("GetApplicationMasterPurpose")]
        public List<PurposeModel> GetApplicationMasterPurpose()
        {
            var codeMasterDetails = new List<PurposeModel>();
            var applicationmasterIds = new List<long?>() { 215 };
            var applicationMasters = _context.ApplicationMaster.Where(a => applicationmasterIds.Contains(a.ApplicationMasterCodeId)).AsNoTracking().Select(m => m.ApplicationMasterId).ToList();
            if (applicationMasters != null)
            {
                var codeMasterDetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                codeMasterDetail.ForEach(s =>
                {
                    PurposeModel purposeModels = new PurposeModel
                    {
                        PurposeId = s.ApplicationMasterDetailId,
                        ApplicationMasterID = s.ApplicationMasterId,
                        PurposeName = s.Value,
                        Description = s.Description,
                    };
                    codeMasterDetails.Add(purposeModels);
                });
                codeMasterDetails.Where(d => applicationMasters.Contains(d.ApplicationMasterID)).OrderBy(o => o.PurposeName).ToList();
            }
            return codeMasterDetails;
        }



        [HttpGet]
        [Route("GetApplicationMasterCurrency")]
        public List<CurrencyModel> GetApplicationMasterCurrency()
        {
            var codeMasterDetails = new List<CurrencyModel>();
            var applicationmasterIds = new List<long?>() { 216 };
            var applicationMasters = _context.ApplicationMaster.Where(a => applicationmasterIds.Contains(a.ApplicationMasterCodeId)).AsNoTracking().Select(m => m.ApplicationMasterId).ToList();
            if (applicationMasters != null)
            {
                var codeMasterDetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                codeMasterDetail.ForEach(s =>
                {
                    CurrencyModel currencyModels = new CurrencyModel
                    {
                        CurrencyId = s.ApplicationMasterDetailId,
                        ApplicationMasterID = s.ApplicationMasterId,
                        CurrencyName = s.Value,
                    };
                    codeMasterDetails.Add(currencyModels);
                });
                codeMasterDetails.Where(d => applicationMasters.Contains(d.ApplicationMasterID)).OrderBy(o => o.CurrencyName).ToList();
            }
            return codeMasterDetails;

        }



        // POST: api/User
        [HttpPost]
        [Route("InsertExchangeRate")]
        public ExchangeRateModel Post(ExchangeRateModel value)
        {
            var SessionId = Guid.NewGuid();
            var exchangeRate = new ExchangeRate
            {
                // ExchangeRateId=value.ExchangeRateId,
                PurposeId = value.PurposeId,
                //Description = value.Description,
                ValidPeriodFrom = value.ValidPeriodFrom,
                ValidPeriodTo = value.ValidPeriodTo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                SessionId = SessionId,
            };

            _context.ExchangeRate.Add(exchangeRate);
            _context.SaveChanges();
            value.ExchangeRateId = exchangeRate.ExchangeRateId;
            value.SessionId = SessionId;
            return value;


        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateExchangeRate")]
        public async Task<ExchangeRateModel> Put(ExchangeRateModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var exchangeRate = _context.ExchangeRate.SingleOrDefault(p => p.ExchangeRateId == value.ExchangeRateId);
            if (exchangeRate != null)
            {
                exchangeRate.PurposeId = value.PurposeId;
                exchangeRate.Description = value.Description;
                exchangeRate.ValidPeriodFrom = value.ValidPeriodFrom;
                exchangeRate.ValidPeriodTo = value.ValidPeriodTo;
                exchangeRate.ModifiedByUserId = value.ModifiedByUserID;
                exchangeRate.ModifiedDate = DateTime.Now;
                exchangeRate.StatusCodeId = value.StatusCodeID.Value;
                exchangeRate.SessionId = value.SessionId;
                _context.SaveChanges();
            }
            return value;
        }


        [HttpPut]
        [Route("ApplyVersion")]
        public ExchangeRateModel PutVersion(ExchangeRateModel value)
        {
            var exchangeRate = _context.ExchangeRate.SingleOrDefault(p => p.ExchangeRateId == value.ExchangeRateId);
            if (exchangeRate != null)
            {
                exchangeRate.PurposeId = value.PurposeId;
                exchangeRate.Description = value.Description;
                exchangeRate.ValidPeriodFrom = value.ValidPeriodFrom;
                exchangeRate.ValidPeriodTo = value.ValidPeriodTo;
                exchangeRate.ModifiedByUserId = value.ModifiedByUserID;
                exchangeRate.ModifiedDate = DateTime.Now;
                exchangeRate.StatusCodeId = value.StatusCodeID.Value;
                exchangeRate.SessionId = value.SessionId;
                _context.SaveChanges();
            }
            var exchangeRateLine = _context.ExchangeRateLine.Where(prc => prc.ExchangeRateId == exchangeRate.ExchangeRateId).ToList();
            if (exchangeRateLine != null)
            {
                _context.ExchangeRateLine.RemoveRange(exchangeRateLine);
                _context.SaveChanges();
            }
            if (value.ExchangeRateLine != null)
            {
                value.ExchangeRateLine.ForEach(ec =>
                {
                    var exchangeRateLine = new ExchangeRateLine
                    {
                        ExchangeRateId = ec.ExchangeRateId,
                        Units = ec.Units,
                        CurrencyId = ec.CurrencyId,
                        EquivalentUnitId = ec.EquivalentUnitId,
                        EquivalebtCurrencyId = ec.EquivalebtCurrencyId,
                        StatusCodeId = ec.StatusCodeID.Value,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                    };
                    _context.ExchangeRateLine.Add(exchangeRateLine);


                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<ExchangeRateModel> CreateVersion(ExchangeRateModel value)
        {
            value.SessionId = value.SessionId.Value;
            var ExchangeRate = _context.ExchangeRate.SingleOrDefault(p => p.ExchangeRateId == value.ExchangeRateId);
            if (ExchangeRate != null)
            {
                var verObject = _mapper.Map<ExchangeRateModel>(ExchangeRate);
                verObject.ExchangeRateLine = GetExchangeRateLineID(value.ExchangeRateId);
                var versionInfo = new TableDataVersionInfoModel<ExchangeRateModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ExchangeRate",
                    PrimaryKey = value.ExchangeRateId,
                    ReferenceInfo = value.ReferenceInfo
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(ExchangeRateModel value)
        {
            value.SessionId = value.SessionId.Value;
            var ExchangeRate = _context.ExchangeRate.SingleOrDefault(p => p.ExchangeRateId == value.ExchangeRateId);

            if (ExchangeRate != null)
            {
                var verObject = _mapper.Map<ExchangeRateModel>(ExchangeRate);
                verObject.ExchangeRateLine = GetExchangeRateLineID(value.ExchangeRateId);
                var versionInfo = new TableDataVersionInfoModel<ExchangeRateModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ExchangeRate",
                    PrimaryKey = value.ExchangeRateId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<ExchangeRateModel> UndoVersion(int Id)
        {

            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<ExchangeRateModel>(tableData.JsonData);

                    var exchangeRate = _context.ExchangeRate.Include(m => m.ExchangeRateLine).SingleOrDefault(p => p.ExchangeRateId == verObject.ExchangeRateId);


                    if (verObject != null && exchangeRate != null)
                    {
                        exchangeRate.PurposeId = verObject.PurposeId;
                        exchangeRate.Description = verObject.Description;
                        exchangeRate.ValidPeriodFrom = verObject.ValidPeriodFrom;
                        exchangeRate.ValidPeriodTo = verObject.ValidPeriodTo;
                        exchangeRate.ModifiedByUserId = verObject.ModifiedByUserID;
                        exchangeRate.ModifiedDate = DateTime.Now;
                        exchangeRate.StatusCodeId = verObject.StatusCodeID.Value;
                        exchangeRate.SessionId = verObject.SessionId;
                        _context.SaveChanges();
                        if (verObject.ExchangeRateLine != null)
                        {
                            _context.ExchangeRateLine.RemoveRange(exchangeRate.ExchangeRateLine);
                            _context.SaveChanges();
                        }
                        if (verObject.ExchangeRateLine != null)
                        {
                            verObject.ExchangeRateLine.ForEach(ec =>
                            {
                                var exchangeRateLine = new ExchangeRateLine
                                {
                                    ExchangeRateId = ec.ExchangeRateId,
                                    Units = ec.Units,
                                    CurrencyId = ec.CurrencyId,
                                    EquivalentUnitId = ec.EquivalentUnitId,
                                    EquivalebtCurrencyId = ec.EquivalebtCurrencyId,
                                    StatusCodeId = ec.StatusCodeID.Value,
                                    AddedByUserId = ec.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                };
                                _context.ExchangeRateLine.Add(exchangeRateLine);
                            });
                            _context.SaveChanges();
                        }
                    }
                    return verObject;
                }
                return new ExchangeRateModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteExchangeRate")]
        public void Delete(int id)
        {

            var exchangeRate = _context.ExchangeRate.Include(l => l.ExchangeRateLine).SingleOrDefault(p => p.ExchangeRateId == id);
            if (exchangeRate != null)
            {
                if (exchangeRate.SessionId != null)
                {
                    var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == exchangeRate.SessionId).ToList();
                    _context.TableDataVersionInfo.RemoveRange(versions);
                }

                _context.ExchangeRate.Remove(exchangeRate);
                _context.SaveChanges();
            }
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteExchangeRateLine")]
        public void DeleteExchangeRateLine(int id)
        {

            var exchangeRateLine = _context.ExchangeRateLine.SingleOrDefault(p => p.ExchangeRateLineId == id);
            if (exchangeRateLine != null)
            {
                _context.ExchangeRateLine.Remove(exchangeRateLine);
                _context.SaveChanges();

            }
        }






        // POST: api/User
        [HttpPost]
        [Route("InsertExchangeRateLine")]
        public ExchangeRateLineModel PostExchangeRateLine(ExchangeRateLineModel value)
        {

            var exchangeRateLine = new ExchangeRateLine
            {
                //ExchangeRateLineId
                ExchangeRateId = value.ExchangeRateId,
                Units = value.Units,
                CurrencyId = value.CurrencyId,
                EquivalentUnitId = value.EquivalentUnitId,
                EquivalebtCurrencyId = value.EquivalebtCurrencyId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,


            };

            _context.ExchangeRateLine.Add(exchangeRateLine);
            _context.SaveChanges();
            value.ExchangeRateLineId = exchangeRateLine.ExchangeRateLineId;

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateExchangeRateLine")]
        public async Task<ExchangeRateLineModel> PutExchangeRateLine(ExchangeRateLineModel value)
        {
            var exchangeRateLine = _context.ExchangeRateLine.SingleOrDefault(p => p.ExchangeRateLineId == value.ExchangeRateLineId);

            exchangeRateLine.ExchangeRateId = value.ExchangeRateId;
            exchangeRateLine.Units = value.Units;

            exchangeRateLine.CurrencyId = value.CurrencyId;
            exchangeRateLine.EquivalentUnitId = value.EquivalentUnitId;
            exchangeRateLine.EquivalebtCurrencyId = value.EquivalebtCurrencyId;
            exchangeRateLine.ModifiedByUserId = value.ModifiedByUserID;
            exchangeRateLine.ModifiedDate = DateTime.Now;
            exchangeRateLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }
    }
}