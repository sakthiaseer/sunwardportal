﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ManpowerInformationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ManpowerInformationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetManpowerInformationLines")]
        public List<ManpowerInformationLineModel> Get()
        {
            var ManpowerInformationLineModels = _context.ManpowerInformationLine.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.StatusCode).Select(s => new ManpowerInformationLineModel
            {
                ManpowerInformationID = s.ManpowerInformationId,
                ManpowerInformationLineID = s.ManpowerInformationLineId,
                OperationNo = s.OperationNo,
                IsOperatorSkilled = s.IsOperatorSkilled,
                IsQCApproval = s.IsQcapproval,
                IsPICInPerson = s.IsPicinPerson,
                OperationFromNo = s.OperationFromNo,
                OperationToNo = s.OperationToNo,
                AddedByHuman = s.AddedByHuman,
                MaxNumberOfHuman = s.MaxNumberOfHuman,               
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ManpowerInformationLineID).AsNoTracking().ToList();
            return ManpowerInformationLineModels;
        }

        [HttpGet]
        [Route("GetManpowerInformationLinesByID")]
        public List<ManpowerInformationLineModel> GetManpowerInformationLinesByID(int id)
        {
            var ManpowerInformationLineModels = _context.ManpowerInformationLine.Include("AddedByUser").Include("ModifiedByUser").Include(s => s.StatusCode).Select(s => new ManpowerInformationLineModel
            {
                ManpowerInformationID = s.ManpowerInformationId,
                ManpowerInformationLineID = s.ManpowerInformationLineId,
                OperationNo = s.OperationNo,
                IsOperatorSkilled = s.IsOperatorSkilled,
                IsQCApproval = s.IsQcapproval,
                IsPICInPerson = s.IsPicinPerson,
                OperationFromNo = s.OperationFromNo,
                OperationToNo = s.OperationToNo,
                AddedByHuman = s.AddedByHuman,
                MaxNumberOfHuman = s.MaxNumberOfHuman,
               
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).Where(m=>m.ManpowerInformationID == id).OrderByDescending(o => o.ManpowerInformationLineID).AsNoTracking().ToList();
            return ManpowerInformationLineModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ManpowerInformationLineModel> GetData(SearchModel searchModel)
        {
            var ManpowerInformationLine = new ManpowerInformationLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).LastOrDefault();
                        break;
                    case "Next":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).LastOrDefault();
                        break;
                    case "Previous":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).LastOrDefault();
                        break;
                    case "Next":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderBy(o => o.ManpowerInformationLineId).FirstOrDefault(s => s.ManpowerInformationLineId > searchModel.Id);
                        break;
                    case "Previous":
                        ManpowerInformationLine = _context.ManpowerInformationLine.OrderByDescending(o => o.ManpowerInformationLineId).FirstOrDefault(s => s.ManpowerInformationLineId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<ManpowerInformationLineModel>(ManpowerInformationLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertManpowerInformationLine")]
        public ManpowerInformationLineModel Post(ManpowerInformationLineModel value)
        {
            var manpowerInformationLine = new ManpowerInformationLine
            {
                ManpowerInformationId = value.ManpowerInformationID,
                IsOperatorSkilled = value.IsOperatorSkilled,
                IsPicinPerson = value.IsPICInPerson,
                IsQcapproval = value.IsQCApproval,
                OperationNo = value.OperationNo,
                OperationFromNo = value.OperationFromNo,
                OperationToNo = value.OperationToNo,
                AddedByHuman = value.AddedByHuman,
                MaxNumberOfHuman = value.MaxNumberOfHuman,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.ManpowerInformationLine.Add(manpowerInformationLine);
            _context.SaveChanges();
            value.ManpowerInformationLineID = manpowerInformationLine.ManpowerInformationLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateManpowerInformationLine")]
        public ManpowerInformationLineModel Put(ManpowerInformationLineModel value)
        {
            var manpowerInformationLine = _context.ManpowerInformationLine.SingleOrDefault(p => p.ManpowerInformationLineId == value.ManpowerInformationLineID);
            manpowerInformationLine.ManpowerInformationId = value.ManpowerInformationID;
            manpowerInformationLine.OperationNo = value.OperationNo;
            manpowerInformationLine.IsQcapproval = value.IsQCApproval;
            manpowerInformationLine.IsOperatorSkilled = value.IsOperatorSkilled;
            manpowerInformationLine.IsPicinPerson = value.IsPICInPerson;
            manpowerInformationLine.OperationFromNo = value.OperationFromNo;
            manpowerInformationLine.OperationToNo = value.OperationToNo;
            manpowerInformationLine.AddedByHuman = value.AddedByHuman;
            manpowerInformationLine.MaxNumberOfHuman = value.MaxNumberOfHuman;
            manpowerInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            manpowerInformationLine.ModifiedDate = DateTime.Now;
            manpowerInformationLine.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteManpowerInformationLine")]
        public void Delete(int id)
        {
            var ManpowerInformationLine = _context.ManpowerInformationLine.SingleOrDefault(p => p.ManpowerInformationLineId == id);
            if (ManpowerInformationLine != null)
            {
                _context.ManpowerInformationLine.Remove(ManpowerInformationLine);
                _context.SaveChanges();
            }
        }
    }
}