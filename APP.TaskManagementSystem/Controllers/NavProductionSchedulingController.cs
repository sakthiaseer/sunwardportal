﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModel.SearchParam;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavProductionSchedulingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public NavProductionSchedulingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavINPItems")]
        public List<INPModel> Get()
        {
            var inpItems = _context.NavinpitemReport.Where(q => q.Quantity <= 0).Select(s => new INPModel
            {
                //Changes Done by Aravinth Start
                InpreportId = s.InpreportId,
                //Changes Done by Aravinth End
                ItemNo = s.ItemNo,
                CompletionDate = s.CompletionDate,
                Description = s.Description,
                Quantity = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                ShipmentDate = s.ShipmentDate,
                AvailableQty = Math.Abs(s.Inventory.GetValueOrDefault(0)),
                SaftyStock = Math.Abs(s.SaftyStock.GetValueOrDefault(0)),
                ItemCategory = s.ItemCategory,
                Uom = s.Uom,
                Company = s.Company,
                Supply = Math.Abs(s.SupplyQuantity.GetValueOrDefault(0)),
                Demand = Math.Abs(s.DemandQuantity.GetValueOrDefault(0)),
            }).OrderByDescending(o => o.ItemNo).AsNoTracking().ToList();
            return inpItems;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavINPItemDetails")]
        public List<INPItemModel> GetINPItemDetails(string itemNo)
        {
            if (string.IsNullOrEmpty(itemNo))
                return new List<INPItemModel>();

            var inventory = _context.Navitems.FirstOrDefault(i => i.No == itemNo);

            var inpDetailItems = _context.Navinpreport.Where(q => q.ItemNo == itemNo).Select(s => new INPItemModel
            {
                ItemNo = s.ItemNo,
                Quantity = s.Quantity,
                QuantityBase = s.QuantityBase,
                DocumentNo = s.DocumentNo,
                Description = s.Description,
                CompletionDate = s.CompletionDate,
                Balance = s.Balance,
                Demand = s.Demand,
                LocationCode = s.LocationCode,
                Supply = s.Supply,
                Rpobalance = s.Rpobalance,
                Rpono = s.Rpono,
                Safetylead = s.SaftyLeadTime,
                Shipmentdate = s.ShipmentDate,
                Sourcename = s.Company,
                BatchNo = s.BatchNo,
                DocumentType = s.TableName,
                FilterType = s.FilterType
            }).OrderBy(o => o.CompletionDate).AsNoTracking().ToList();

            var supply = inpDetailItems.Where(w => w.FilterType == "Supply").Sum(s => s.Supply);
            var qty = inventory != null ? inventory.Inventory + supply : supply;
            inpDetailItems.Where(d => d.FilterType == "Demand").ToList().ForEach(f =>
              {
                  qty -= f.Demand.GetValueOrDefault(0);
                  f.Balance = qty;
              });
            qty = inventory != null ? inventory.Inventory : 0;
            inpDetailItems.Where(d => d.FilterType == "Supply").ToList().ForEach(f =>
            {
                qty += f.Supply.GetValueOrDefault(0);
                f.Balance = qty;
            });


            return inpDetailItems;
        }

        [HttpGet]
        [Route("GetINPItemsByDemand")]
        public List<INPItemModel> GeINPItemsByDemand(string itemNo)
        {
            if (string.IsNullOrEmpty(itemNo))
                return new List<INPItemModel>();

            var inpDetailItems = _context.Navinpreport.Where(s => s.ItemNo == itemNo && s.FilterType == "Demand").Select(s => new INPItemModel
            {
                ItemNo = s.ItemNo,
                Quantity = s.Quantity,
                QuantityBase = s.QuantityBase,
                DocumentNo = s.DocumentNo,
                Description = s.Description,
                CompletionDate = s.CompletionDate,
                Balance = s.Balance,
                Demand = s.Demand,
                LocationCode = s.LocationCode,
                Supply = s.Supply,
                Rpobalance = s.Rpobalance,
                Rpono = s.Rpono,
                Safetylead = s.SaftyLeadTime,
                Shipmentdate = s.ShipmentDate,
                Sourcename = s.Company,
                BatchNo = s.BatchNo,
                DocumentType = s.TableName,
            }).OrderBy(o => o.CompletionDate).AsNoTracking().ToList();
            return inpDetailItems;
        }

        [HttpGet]
        [Route("GetINPItemsBySupply")]
        public List<INPItemModel> GetINPItemsBySupply(string itemNo)
        {
            if (string.IsNullOrEmpty(itemNo))
                return new List<INPItemModel>();

            var inpDetailItems = _context.Navinpreport.Where(s => s.ItemNo == itemNo && s.FilterType == "Supply").Select(s => new INPItemModel
            {
                ItemNo = s.ItemNo,
                Quantity = s.Quantity,
                QuantityBase = s.QuantityBase,
                DocumentNo = s.DocumentNo,
                Description = s.Description,
                CompletionDate = s.CompletionDate,
                Balance = s.Balance,
                Demand = s.Demand,
                LocationCode = s.LocationCode,
                Supply = s.Supply,
                Rpobalance = s.Rpobalance,
                Rpono = s.Rpono,
                Safetylead = s.SaftyLeadTime,
                Shipmentdate = s.ShipmentDate,
                Sourcename = s.Company,
                BatchNo = s.BatchNo,
                DocumentType = s.TableName,

            }).OrderBy(o => o.CompletionDate).AsNoTracking().ToList();
            return inpDetailItems;
        }

        [HttpPost]
        [Route("GetINPCalendarView")]
        public INPCalendarView GetINPCalendarView(DateRangeModel dateRangeModel)
        {

            INPCalendarView iNPCalendarView = new INPCalendarView();
            iNPCalendarView.Headers = GenerateMethodCodeHeaders();
            iNPCalendarView.ChildHeaders = GenerateMethodCodeChildHeaders(iNPCalendarView);
            if (string.IsNullOrEmpty(dateRangeModel.ItemNo))
            {
                iNPCalendarView.Calendars = GetMethodCodeCalendar();
            }
            else
            {
                iNPCalendarView.Calendars = GetMethodCodeCalendar(dateRangeModel.ToDate).Where(s => s.ChildCalendarItems.Any(i => i.ItemNo == dateRangeModel.ItemNo)).ToList();
            }

            if (iNPCalendarView.Calendars.Any())
            {
                GetMethodCodeCompletionBefore(iNPCalendarView.Calendars);
            }
            return iNPCalendarView;
        }

        private List<INPCalendarHeader> GenerateMethodCodeHeaders()
        {
            List<INPCalendarHeader> calendarHeaders = new List<INPCalendarHeader>();
            calendarHeaders.Add(new INPCalendarHeader { Text = "", Value = "", Align = "left" });
            calendarHeaders.Add(new INPCalendarHeader { Text = "MethodCode", Value = "methodCode", Align = "left" });
            return calendarHeaders;
        }


        private List<INPCalendarHeader> GenerateMethodCodeChildHeaders(INPCalendarView iNPCalendarView)
        {
            List<INPCalendarHeader> calendarHeaders = new List<INPCalendarHeader>();
            var nextTwelveMonths = Enumerable.Range(0, 12).Select(i => DateTime.Now.AddMonths(i).ToString("MMMM"));
            int idx = 1;
            calendarHeaders.Add(new INPCalendarHeader { Text = "ItemNo", Value = "itemNo", Align = "left" });
            calendarHeaders.Add(new INPCalendarHeader { Text = "Description", Value = "description", Align = "left" });
            foreach (var month in nextTwelveMonths)
            {
                INPCalendarHeader iNPCalendarHeader = new INPCalendarHeader
                {
                    Text = month,
                    Value = "month" + idx,
                    Align = "left"
                };

                if (nameof(iNPCalendarView.Month1).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month1 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month2).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month2 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month3).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month3 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month4).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month4 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month5).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month5 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month6).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month6 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month7).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month7 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month8).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month8 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month9).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month9 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month10).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month10 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month11).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month11 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month12).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month12 = iNPCalendarHeader.Text;
                }
                idx++;
                calendarHeaders.Add(iNPCalendarHeader);
            }

            return calendarHeaders;
        }


        private List<INPCalendarModel> GetMethodCodeCalendar(DateTime? endDate = null)
        {
            var MethodCodeList = new List<INPCalendarModel>();

            var mCodeList = _context.NavMethodCode.Include("NavMethodCodeLines.Item").AsNoTracking().ToList();

            mCodeList.ForEach(mc =>
            {
                mc.NavMethodCodeLines.ToList().ForEach(l =>
                {
                    if (l.Item != null)
                    {
                        MethodCodeList.Add(new INPCalendarModel
                        {
                            MethodCode = mc.MethodName,
                            ItemNo = l.Item.No,
                            Description = l.Item.Description,
                            UOM = l.Item.BaseUnitofMeasure
                        });
                    }
                });
            });

            List<string> methodCodeItemNos = MethodCodeList.Select(s => s.ItemNo).ToList();
            var navItemReports = endDate == null ? _context.NavinpitemReport.Where(r => methodCodeItemNos.Contains(r.ItemNo)).ToList() : _context.NavinpitemReport.Where(r => methodCodeItemNos.Contains(r.ItemNo) && r.CompletionDate <= endDate).AsNoTracking().ToList();

            navItemReports.ForEach(n =>
            {
                var methodCodeItem = MethodCodeList.FirstOrDefault(m => m.ItemNo == n.ItemNo);
                methodCodeItem.CompletionDate = n.CompletionDate != null ? n.CompletionDate : DateTime.Now;
                methodCodeItem.IntMonth = methodCodeItem.CompletionDate.Value.Month;
                methodCodeItem.Quantity = n.Quantity.GetValueOrDefault(0);
            });

            var groupItems = MethodCodeList.GroupBy(g => new { g.MethodCode, g.IntMonth }).ToList();
            var methodCalendarItems = new List<INPCalendarModel>();
            var nextTwelve = Enumerable.Range(0, 12).Select(i => DateTime.Now.AddMonths(i).Month).ToList();
            groupItems.ForEach(g =>
            {
                var itemGroup = g.GroupBy(cg => cg.MethodCode).ToList();
                INPCalendarModel parentCalendarItem = new INPCalendarModel();
                var existingParentCalendarItem = methodCalendarItems.FirstOrDefault(c => c.MethodCode == g.Key.MethodCode);
                if (existingParentCalendarItem != null)
                {
                    parentCalendarItem = existingParentCalendarItem;
                }
                else
                {
                    parentCalendarItem.MethodCode = g.Key.MethodCode;
                }
                var childCalendarItems = new List<INPCalendarModel>();
                itemGroup[0].ToList().ForEach(item =>
                {
                    var methodCode = g.First();
                    var qty = item.Quantity;
                    var month = g.Key.IntMonth;


                    if (parentCalendarItem.ChildCalendarItems.Any())
                    {
                        var existingChildCalendarItem = parentCalendarItem.ChildCalendarItems.FirstOrDefault(i => i.ItemNo == item.ItemNo);

                        if (existingChildCalendarItem != null)
                        {
                            existingChildCalendarItem.Month1 = month == nextTwelve[0] ? existingChildCalendarItem.Month1 + qty : existingChildCalendarItem.Month1;
                            existingChildCalendarItem.Month2 = month == nextTwelve[1] ? existingChildCalendarItem.Month2 + qty : existingChildCalendarItem.Month2;
                            existingChildCalendarItem.Month3 = month == nextTwelve[2] ? existingChildCalendarItem.Month3 + qty : existingChildCalendarItem.Month3;
                            existingChildCalendarItem.Month4 = month == nextTwelve[3] ? existingChildCalendarItem.Month4 + qty : existingChildCalendarItem.Month4;
                            existingChildCalendarItem.Month5 = month == nextTwelve[4] ? existingChildCalendarItem.Month5 + qty : existingChildCalendarItem.Month5;
                            existingChildCalendarItem.Month6 = month == nextTwelve[5] ? existingChildCalendarItem.Month6 + qty : existingChildCalendarItem.Month6;
                            existingChildCalendarItem.Month7 = month == nextTwelve[6] ? existingChildCalendarItem.Month7 + qty : existingChildCalendarItem.Month7;
                            existingChildCalendarItem.Month8 = month == nextTwelve[7] ? existingChildCalendarItem.Month8 + qty : existingChildCalendarItem.Month8;
                            existingChildCalendarItem.Month9 = month == nextTwelve[8] ? existingChildCalendarItem.Month9 + qty : existingChildCalendarItem.Month9;
                            existingChildCalendarItem.Month10 = month == nextTwelve[9] ? existingChildCalendarItem.Month10 + qty : existingChildCalendarItem.Month10;
                            existingChildCalendarItem.Month11 = month == nextTwelve[10] ? existingChildCalendarItem.Month11 + qty : existingChildCalendarItem.Month11;
                            existingChildCalendarItem.Month12 = month == nextTwelve[11] ? existingChildCalendarItem.Month12 + qty : existingChildCalendarItem.Month12;

                            existingChildCalendarItem.Month1CompletionDate = month == nextTwelve[0] ? existingChildCalendarItem.Month1CompletionDate : null;
                            existingChildCalendarItem.Month2CompletionDate = month == nextTwelve[1] ? existingChildCalendarItem.Month2CompletionDate : null;
                            existingChildCalendarItem.Month3CompletionDate = month == nextTwelve[2] ? existingChildCalendarItem.Month3CompletionDate : null;
                            existingChildCalendarItem.Month4CompletionDate = month == nextTwelve[3] ? existingChildCalendarItem.Month4CompletionDate : null;
                            existingChildCalendarItem.Month5CompletionDate = month == nextTwelve[4] ? existingChildCalendarItem.Month5CompletionDate : null;
                            existingChildCalendarItem.Month6CompletionDate = month == nextTwelve[5] ? existingChildCalendarItem.Month6CompletionDate : null;
                            existingChildCalendarItem.Month7CompletionDate = month == nextTwelve[6] ? existingChildCalendarItem.Month7CompletionDate : null;
                            existingChildCalendarItem.Month8CompletionDate = month == nextTwelve[7] ? existingChildCalendarItem.Month8CompletionDate : null;
                            existingChildCalendarItem.Month9CompletionDate = month == nextTwelve[8] ? existingChildCalendarItem.Month9CompletionDate : null;
                            existingChildCalendarItem.Month10CompletionDate = month == nextTwelve[9] ? existingChildCalendarItem.Month10CompletionDate : null;
                            existingChildCalendarItem.Month11CompletionDate = month == nextTwelve[10] ? existingChildCalendarItem.Month11CompletionDate : null;
                            existingChildCalendarItem.Month12CompletionDate = month == nextTwelve[11] ? existingChildCalendarItem.Month12CompletionDate : null;
                        }
                        else
                        {
                            INPCalendarModel iNPCalendarModel = new INPCalendarModel();
                            iNPCalendarModel.ItemNo = item.ItemNo;
                            iNPCalendarModel.MethodCode = g.Key.MethodCode;
                            iNPCalendarModel.Description = item.Description;
                            iNPCalendarModel.UOM = item.UOM;
                            iNPCalendarModel.Quantity = qty;
                            iNPCalendarModel.IntMonth = g.Key.IntMonth;
                            iNPCalendarModel.CompletionDate = item.CompletionDate;
                            iNPCalendarModel.Month1 = month == nextTwelve[0] ? qty : 0;
                            iNPCalendarModel.Month2 = month == nextTwelve[1] ? qty : 0;
                            iNPCalendarModel.Month3 = month == nextTwelve[2] ? qty : 0;
                            iNPCalendarModel.Month4 = month == nextTwelve[3] ? qty : 0;
                            iNPCalendarModel.Month5 = month == nextTwelve[4] ? qty : 0;
                            iNPCalendarModel.Month6 = month == nextTwelve[5] ? qty : 0;
                            iNPCalendarModel.Month7 = month == nextTwelve[6] ? qty : 0;
                            iNPCalendarModel.Month8 = month == nextTwelve[7] ? qty : 0;
                            iNPCalendarModel.Month9 = month == nextTwelve[8] ? qty : 0;
                            iNPCalendarModel.Month10 = month == nextTwelve[9] ? qty : 0;
                            iNPCalendarModel.Month11 = month == nextTwelve[10] ? qty : 0;
                            iNPCalendarModel.Month12 = month == nextTwelve[11] ? qty : 0;

                            iNPCalendarModel.Month1CompletionDate = month == nextTwelve[0] ? item.CompletionDate : null;
                            iNPCalendarModel.Month2CompletionDate = month == nextTwelve[1] ? item.CompletionDate : null;
                            iNPCalendarModel.Month3CompletionDate = month == nextTwelve[2] ? item.CompletionDate : null;
                            iNPCalendarModel.Month4CompletionDate = month == nextTwelve[3] ? item.CompletionDate : null;
                            iNPCalendarModel.Month5CompletionDate = month == nextTwelve[4] ? item.CompletionDate : null;
                            iNPCalendarModel.Month6CompletionDate = month == nextTwelve[5] ? item.CompletionDate : null;
                            iNPCalendarModel.Month7CompletionDate = month == nextTwelve[6] ? item.CompletionDate : null;
                            iNPCalendarModel.Month8CompletionDate = month == nextTwelve[7] ? item.CompletionDate : null;
                            iNPCalendarModel.Month9CompletionDate = month == nextTwelve[8] ? item.CompletionDate : null;
                            iNPCalendarModel.Month10CompletionDate = month == nextTwelve[9] ? item.CompletionDate : null;
                            iNPCalendarModel.Month11CompletionDate = month == nextTwelve[10] ? item.CompletionDate : null;
                            iNPCalendarModel.Month12CompletionDate = month == nextTwelve[11] ? item.CompletionDate : null;
                            childCalendarItems.Add(iNPCalendarModel);

                        }
                    }
                    else
                    {
                        INPCalendarModel iNPCalendarModel = new INPCalendarModel();
                        iNPCalendarModel.ItemNo = item.ItemNo;
                        iNPCalendarModel.MethodCode = g.Key.MethodCode;
                        iNPCalendarModel.Description = item.Description;
                        iNPCalendarModel.UOM = item.UOM;
                        iNPCalendarModel.Quantity = qty;
                        iNPCalendarModel.IntMonth = g.Key.IntMonth;
                        iNPCalendarModel.CompletionDate = item.CompletionDate;
                        iNPCalendarModel.Month1 = month == nextTwelve[0] ? qty : 0;
                        iNPCalendarModel.Month2 = month == nextTwelve[1] ? qty : 0;
                        iNPCalendarModel.Month3 = month == nextTwelve[2] ? qty : 0;
                        iNPCalendarModel.Month4 = month == nextTwelve[3] ? qty : 0;
                        iNPCalendarModel.Month5 = month == nextTwelve[4] ? qty : 0;
                        iNPCalendarModel.Month6 = month == nextTwelve[5] ? qty : 0;
                        iNPCalendarModel.Month7 = month == nextTwelve[6] ? qty : 0;
                        iNPCalendarModel.Month8 = month == nextTwelve[7] ? qty : 0;
                        iNPCalendarModel.Month9 = month == nextTwelve[8] ? qty : 0;
                        iNPCalendarModel.Month10 = month == nextTwelve[9] ? qty : 0;
                        iNPCalendarModel.Month11 = month == nextTwelve[10] ? qty : 0;
                        iNPCalendarModel.Month12 = month == nextTwelve[11] ? qty : 0;

                        iNPCalendarModel.Month1CompletionDate = month == nextTwelve[0] ? item.CompletionDate : null;
                        iNPCalendarModel.Month2CompletionDate = month == nextTwelve[1] ? item.CompletionDate : null;
                        iNPCalendarModel.Month3CompletionDate = month == nextTwelve[2] ? item.CompletionDate : null;
                        iNPCalendarModel.Month4CompletionDate = month == nextTwelve[3] ? item.CompletionDate : null;
                        iNPCalendarModel.Month5CompletionDate = month == nextTwelve[4] ? item.CompletionDate : null;
                        iNPCalendarModel.Month6CompletionDate = month == nextTwelve[5] ? item.CompletionDate : null;
                        iNPCalendarModel.Month7CompletionDate = month == nextTwelve[6] ? item.CompletionDate : null;
                        iNPCalendarModel.Month8CompletionDate = month == nextTwelve[7] ? item.CompletionDate : null;
                        iNPCalendarModel.Month9CompletionDate = month == nextTwelve[8] ? item.CompletionDate : null;
                        iNPCalendarModel.Month10CompletionDate = month == nextTwelve[9] ? item.CompletionDate : null;
                        iNPCalendarModel.Month11CompletionDate = month == nextTwelve[10] ? item.CompletionDate : null;
                        iNPCalendarModel.Month12CompletionDate = month == nextTwelve[11] ? item.CompletionDate : null;
                        childCalendarItems.Add(iNPCalendarModel);
                    }
                });

                parentCalendarItem.ChildCalendarItems.AddRange(childCalendarItems);
                if (!methodCalendarItems.Any(ac => ac.MethodCode == parentCalendarItem.MethodCode))
                {
                    methodCalendarItems.Add(parentCalendarItem);
                }
            });


            foreach (var mc in methodCalendarItems)
            {
                mc.Month1Total = mc.ChildCalendarItems.Sum(c => c.Month1);
                mc.Month2Total = mc.ChildCalendarItems.Sum(c => c.Month2);
                mc.Month3Total = mc.ChildCalendarItems.Sum(c => c.Month3);
                mc.Month4Total = mc.ChildCalendarItems.Sum(c => c.Month4);
                mc.Month5Total = mc.ChildCalendarItems.Sum(c => c.Month5);
                mc.Month6Total = mc.ChildCalendarItems.Sum(c => c.Month6);
                mc.Month7Total = mc.ChildCalendarItems.Sum(c => c.Month7);
                mc.Month8Total = mc.ChildCalendarItems.Sum(c => c.Month8);
                mc.Month9Total = mc.ChildCalendarItems.Sum(c => c.Month9);
                mc.Month10Total = mc.ChildCalendarItems.Sum(c => c.Month10);
                mc.Month11Total = mc.ChildCalendarItems.Sum(c => c.Month11);
                mc.Month12Total = mc.ChildCalendarItems.Sum(c => c.Month12);

                mc.EarlistShipment1 = mc.ChildCalendarItems.Min(c => c.Month1CompletionDate);
                mc.EarlistShipment2 = mc.ChildCalendarItems.Min(c => c.Month2CompletionDate);
                mc.EarlistShipment3 = mc.ChildCalendarItems.Min(c => c.Month3CompletionDate);
                mc.EarlistShipment4 = mc.ChildCalendarItems.Min(c => c.Month4CompletionDate);
                mc.EarlistShipment5 = mc.ChildCalendarItems.Min(c => c.Month5CompletionDate);
                mc.EarlistShipment6 = mc.ChildCalendarItems.Min(c => c.Month6CompletionDate);
                mc.EarlistShipment7 = mc.ChildCalendarItems.Min(c => c.Month7CompletionDate);
                mc.EarlistShipment8 = mc.ChildCalendarItems.Min(c => c.Month8CompletionDate);
                mc.EarlistShipment9 = mc.ChildCalendarItems.Min(c => c.Month9CompletionDate);
                mc.EarlistShipment10 = mc.ChildCalendarItems.Min(c => c.Month10CompletionDate);
                mc.EarlistShipment11 = mc.ChildCalendarItems.Min(c => c.Month11CompletionDate);
                mc.EarlistShipment12 = mc.ChildCalendarItems.Min(c => c.Month12CompletionDate);
            }



            return methodCalendarItems;

        }

        private void GetMethodCodeCompletionBefore(List<INPCalendarModel> iNPCalendarModels)
        {
            DateTime completionBefore = DateTime.Now;


            iNPCalendarModels.ForEach(mc =>
            {
                List<DateTime?> completionDates = new List<DateTime?>();
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month1CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month2CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month3CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month4CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month5CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month6CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month7CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month8CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month9CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month10CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month11CompletionDate));
                completionDates.Add(mc.ChildCalendarItems.Min(c => c.Month12CompletionDate));

                mc.CompletionBefore = completionDates.Max(c => c) ?? null;

            });
        }

        [HttpPost]
        [Route("GetINPCalendarPivotView")]
        public List<INPCalendarPivotModel> GetINPCalendarPivotView(DateRangeModel dateRangeModel)
        {
            if (string.IsNullOrEmpty(dateRangeModel.ItemNo))
            {
                return GetMethodCodePivotData();
            }
            else
            {
                return GetMethodCodePivotData(dateRangeModel.ToDate).Where(s => s.ItemNo == dateRangeModel.ItemNo).ToList();
            }
        }

        private List<INPCalendarPivotModel> GetMethodCodePivotData(DateTime? endDate = null)
        {
            var MethodCodeList = new List<INPCalendarPivotModel>();

            var mCodeList = _context.NavMethodCode.Include("NavMethodCodeLines.Item").AsNoTracking().ToList();

            mCodeList.ForEach(mc =>
            {
                mc.NavMethodCodeLines.ToList().ForEach(l =>
                {
                    if (l.Item != null)
                    {
                        MethodCodeList.Add(new INPCalendarPivotModel
                        {
                            MethodCode = mc.MethodName,
                            ItemNo = l.Item!=null?l.Item.No : string.Empty,
                            Description = l.Item != null ? l.Item.Description : string.Empty,
                            UOM = l.Item != null ? l.Item.BaseUnitofMeasure : string.Empty,
                        });
                    }
                });
            });

            List<string> methodCodeItemNos = MethodCodeList.Select(s => s.ItemNo).ToList();
            var navItemReports = endDate == null ? _context.NavinpitemReport.Where(r => methodCodeItemNos.Contains(r.ItemNo)).ToList() : _context.NavinpitemReport.Where(r => methodCodeItemNos.Contains(r.ItemNo) && r.CompletionDate <= endDate).AsNoTracking().ToList();

            navItemReports.ForEach(n =>
            {
                var methodCodeItem = MethodCodeList.FirstOrDefault(m => m.ItemNo == n.ItemNo);
                methodCodeItem.Month = n.CompletionDate != null ? n.CompletionDate.Value.ToString("MMMM") : DateTime.Now.ToString("MMMM");
                methodCodeItem.Quantity = n.Quantity.GetValueOrDefault(0);
            });

            return MethodCodeList;

        }

        [HttpPost()]
        [Route("GetINPACInfoView")]
        public INPCalendarView GetINPACInfoView(DateRangeModel value)
        {
            INPCalendarView iNPCalendarView = new INPCalendarView();
            iNPCalendarView.Headers = GenerateACInfoHeaders(iNPCalendarView);
            iNPCalendarView.ChildHeaders = GenerateACInfoChildHeaders(iNPCalendarView);
            iNPCalendarView.Calendars = GetCustomerAC(value);
            return iNPCalendarView;
        }

        private List<INPCalendarHeader> GenerateACInfoHeaders(INPCalendarView iNPCalendarView)
        {
            List<INPCalendarHeader> calendarHeaders = new List<INPCalendarHeader>();
            calendarHeaders.Add(new INPCalendarHeader { Text = "", Value = "", Align = "left" });
            calendarHeaders.Add(new INPCalendarHeader { Text = "ItemNo", Value = "itemNo", Align = "left" });
            calendarHeaders.Add(new INPCalendarHeader { Text = "Description", Value = "decription", Align = "left" });
            calendarHeaders.Add(new INPCalendarHeader { Text = "UOM", Value = "uom", Align = "left" });

            return calendarHeaders;
        }

        private List<INPCalendarHeader> GenerateACInfoChildHeaders(INPCalendarView iNPCalendarView)
        {
            List<INPCalendarHeader> calendarHeaders = new List<INPCalendarHeader>();
            var lastTwelveMonths = Enumerable.Range(0, 12).Select(i => DateTime.Now.AddMonths(i).ToString("MMMM"));
            int idx = 1;
            calendarHeaders.Add(new INPCalendarHeader { Text = "CustomerName", Value = "customerName", Align = "left" });
            foreach (var month in lastTwelveMonths)
            {
                INPCalendarHeader iNPCalendarHeader = new INPCalendarHeader
                {
                    Text = month,
                    Value = "month" + idx,
                    Align = "left"
                };

                if (nameof(iNPCalendarView.Month1).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month1 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month2).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month2 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month3).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month3 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month4).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month4 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month5).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month5 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month6).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month6 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month7).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month7 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month8).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month8 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month9).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month9 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month10).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month10 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month11).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month11 = iNPCalendarHeader.Text;
                }
                if (nameof(iNPCalendarView.Month12).ToLower() == iNPCalendarHeader.Value)
                {
                    iNPCalendarView.Month12 = iNPCalendarHeader.Text;
                }
                idx++;
                calendarHeaders.Add(iNPCalendarHeader);
            }

            return calendarHeaders;
        }
        [HttpPost()]
        [Route("getCustomerAC")]
        public List<INPCalendarModel> GetCustomerAC(DateRangeModel value)
        {
            var ACList = new List<INPCalendarModel>();

            var acEntryList = _context.Acentry.Include("AcentryLines.Item").Include("Customer").Where(a => a.FromDate >= value.FromDate && a.ToDate <= value.ToDate).AsNoTracking().ToList();

            acEntryList.ForEach(ac =>
            {
                ac.AcentryLines.ToList().ForEach(l =>
                {
                    ACList.Add(new INPCalendarModel
                    {
                        CustomerName = ac.Customer!=null? ac.Customer.Name : string.Empty,
                        ItemNo = l.Item != null ? l.Item.No : string.Empty,
                        Description = l.Item != null ? l.Item.Description : string.Empty,
                        UOM = l.Item != null ? l.Item.BaseUnitofMeasure : string.Empty,
                        Quantity = l.Quantity.GetValueOrDefault(0),
                        FromDate = ac.FromDate,
                        ToDate = ac.ToDate,
                        IntMonth = ac.ToDate.Value.Month,
                    });
                });
            });
            var groupItems = ACList.GroupBy(g => new { g.ItemNo, g.IntMonth }).ToList();
            var acCalendarItems = new List<INPCalendarModel>();
            var nextTwelve = Enumerable.Range(0, 12).Select(i => DateTime.Now.AddMonths(i).Month).ToList();
            groupItems.ForEach(g =>
            {
                var custgroup = g.GroupBy(cg => cg.CustomerName).ToList();
                custgroup[0].ToList().ForEach(cust =>
                {
                    var acEntry = g.First();
                    var qty = cust.Quantity;
                    var month = g.Key.IntMonth;

                    acCalendarItems.Add(new INPCalendarModel
                    {
                        CustomerName = cust.CustomerName,
                        ItemNo = g.Key.ItemNo,
                        Description = acEntry.Description,
                        UOM = acEntry.UOM,
                        Quantity = acEntry.Quantity,
                        FromDate = acEntry.FromDate,
                        ToDate = acEntry.ToDate,
                        IntMonth = g.Key.IntMonth,
                        Month1 = month == nextTwelve[0] ? qty : 0,
                        Month2 = month == nextTwelve[1] ? qty : 0,
                        Month3 = month == nextTwelve[2] ? qty : 0,
                        Month4 = month == nextTwelve[3] ? qty : 0,
                        Month5 = month == nextTwelve[4] ? qty : 0,
                        Month6 = month == nextTwelve[5] ? qty : 0,
                        Month7 = month == nextTwelve[6] ? qty : 0,
                        Month8 = month == nextTwelve[7] ? qty : 0,
                        Month9 = month == nextTwelve[8] ? qty : 0,
                        Month10 = month == nextTwelve[9] ? qty : 0,
                        Month11 = month == nextTwelve[10] ? qty : 0,
                        Month12 = month == nextTwelve[11] ? qty : 0,

                        Month1Total = month == nextTwelve[0] ? qty : 0,
                        Month2Total = month == nextTwelve[1] ? qty : 0,
                        Month3Total = month == nextTwelve[2] ? qty : 0,
                        Month4Total = month == nextTwelve[3] ? qty : 0,
                        Month5Total = month == nextTwelve[4] ? qty : 0,
                        Month6Total = month == nextTwelve[5] ? qty : 0,
                        Month7Total = month == nextTwelve[6] ? qty : 0,
                        Month8Total = month == nextTwelve[7] ? qty : 0,
                        Month9Total = month == nextTwelve[8] ? qty : 0,
                        Month10Total = month == nextTwelve[9] ? qty : 0,
                        Month11Total = month == nextTwelve[10] ? qty : 0,
                        Month12Total = month == nextTwelve[11] ? qty : 0,

                    });
                });
            });

            var Calendardata = new List<INPCalendarModel>();
            var itemgroups = acCalendarItems.GroupBy(g => g.ItemNo).ToList();
            itemgroups.ForEach(g =>
            {
                var acEntry = g.First();
                var qty = g.Sum(s => s.Quantity);
                var month = acEntry.IntMonth;
                var item = new INPCalendarModel();
                item.ItemNo = g.Key;
                item.Description = acEntry.Description;
                item.UOM = acEntry.UOM;
                item.Quantity = qty;
                item.FromDate = acEntry.FromDate;
                item.ToDate = acEntry.ToDate;
                item.Month1Total = month == nextTwelve[0] ? qty : 0;
                item.Month2Total = month == nextTwelve[1] ? qty : 0;
                item.Month3Total = month == nextTwelve[2] ? qty : 0;
                item.Month4Total = month == nextTwelve[3] ? qty : 0;
                item.Month5Total = month == nextTwelve[4] ? qty : 0;
                item.Month6Total = month == nextTwelve[5] ? qty : 0;
                item.Month7Total = month == nextTwelve[6] ? qty : 0;
                item.Month8Total = month == nextTwelve[7] ? qty : 0;
                item.Month9Total = month == nextTwelve[8] ? qty : 0;
                item.Month10Total = month == nextTwelve[9] ? qty : 0;
                item.Month11Total = month == nextTwelve[10] ? qty : 0;
                item.Month12Total = month == nextTwelve[11] ? qty : 0;
                item.ChildCalendarItems = new List<INPCalendarModel>();
                g.ToList().ForEach(l =>
                {
                    qty = l.Quantity;
                    var cusItem = item.ChildCalendarItems.FirstOrDefault(c => c.ItemNo == g.Key && c.CustomerName == l.CustomerName);
                    if (cusItem == null)
                    {
                        item.ChildCalendarItems.Add(new INPCalendarModel
                        {
                            CustomerName = l.CustomerName,
                            IntMonth = l.IntMonth,
                            Month1 = l.IntMonth == nextTwelve[0] ? qty : 0,
                            Month2 = l.IntMonth == nextTwelve[1] ? qty : 0,
                            Month3 = l.IntMonth == nextTwelve[2] ? qty : 0,
                            Month4 = l.IntMonth == nextTwelve[3] ? qty : 0,
                            Month5 = l.IntMonth == nextTwelve[4] ? qty : 0,
                            Month6 = l.IntMonth == nextTwelve[5] ? qty : 0,
                            Month7 = l.IntMonth == nextTwelve[6] ? qty : 0,
                            Month8 = l.IntMonth == nextTwelve[7] ? qty : 0,
                            Month9 = l.IntMonth == nextTwelve[8] ? qty : 0,
                            Month10 = l.IntMonth == nextTwelve[9] ? qty : 0,
                            Month11 = l.IntMonth == nextTwelve[10] ? qty : 0,
                            Month12 = l.IntMonth == nextTwelve[11] ? qty : 0,
                        });
                    }
                    else
                    {
                        cusItem.Month1 = l.IntMonth == nextTwelve[0] ? qty : cusItem.Month1;
                        cusItem.Month2 = l.IntMonth == nextTwelve[1] ? qty : cusItem.Month2;
                        cusItem.Month3 = l.IntMonth == nextTwelve[2] ? qty : cusItem.Month3;
                        cusItem.Month4 = l.IntMonth == nextTwelve[3] ? qty : cusItem.Month4;
                        cusItem.Month5 = l.IntMonth == nextTwelve[4] ? qty : cusItem.Month5;
                        cusItem.Month6 = l.IntMonth == nextTwelve[5] ? qty : cusItem.Month6;
                        cusItem.Month7 = l.IntMonth == nextTwelve[6] ? qty : cusItem.Month7;
                        cusItem.Month8 = l.IntMonth == nextTwelve[7] ? qty : cusItem.Month8;
                        cusItem.Month9 = l.IntMonth == nextTwelve[8] ? qty : cusItem.Month9;
                        cusItem.Month10 = l.IntMonth == nextTwelve[9] ? qty : cusItem.Month10;
                        cusItem.Month11 = l.IntMonth == nextTwelve[10] ? qty : cusItem.Month11;
                        cusItem.Month12 = l.IntMonth == nextTwelve[11] ? qty : cusItem.Month12;
                    }
                });
                Calendardata.Add(item);
            });
            foreach (var mc in Calendardata)
            {
                mc.Month1Total = mc.ChildCalendarItems.Sum(c => c.Month1);
                mc.Month2Total = mc.ChildCalendarItems.Sum(c => c.Month2);
                mc.Month3Total = mc.ChildCalendarItems.Sum(c => c.Month3);
                mc.Month4Total = mc.ChildCalendarItems.Sum(c => c.Month4);
                mc.Month5Total = mc.ChildCalendarItems.Sum(c => c.Month5);
                mc.Month6Total = mc.ChildCalendarItems.Sum(c => c.Month6);
                mc.Month7Total = mc.ChildCalendarItems.Sum(c => c.Month7);
                mc.Month8Total = mc.ChildCalendarItems.Sum(c => c.Month8);
                mc.Month9Total = mc.ChildCalendarItems.Sum(c => c.Month9);
                mc.Month10Total = mc.ChildCalendarItems.Sum(c => c.Month10);
                mc.Month11Total = mc.ChildCalendarItems.Sum(c => c.Month11);
                mc.Month12Total = mc.ChildCalendarItems.Sum(c => c.Month12);

            }
            return Calendardata;
        }


        // GET: api/Project
        [HttpGet]
        [Route("GetParInfoItems")]
        public List<ParInfoModel> GetParInfoItems(string itemNo)
        {
            var parInfoItems = _context.Navinpreport.Select(s => new ParInfoModel
            {
                ItemNo = s.ItemNo,
                Quantity = s.Quantity,
                DocumentNo = s.DocumentNo,
                CompletionDate = s.CompletionDate,
                Balance = s.Balance,
                Demand = s.Demand,
                LocationCode = s.LocationCode,
                Supply = s.Supply,
                SourceName = s.SourceName,
                BatchNo = s.BatchNo,
                ShipmentDate = s.ShipmentDate,
                ShippingAdvice = "sunward"
            }).OrderByDescending(o => o.ItemNo).Where(q => q.ItemNo == itemNo).ToList();
            return parInfoItems;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetPlannerComments")]
        public List<PlannerCommentModel> GetPlannerComments()
        {
            var plannerComments = _context.NavinpitemReport.Where(q => q.Quantity <= 0).Select(s => new PlannerCommentModel
            {
                InpreportId = s.InpreportId,
                ItemNo = s.ItemNo,
                Description = s.Description,
                Quantity = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                ItemCategory = s.ItemCategory,
                Uom = s.Uom,
                Comments = s.PlannerComment

            }).OrderByDescending(o => o.ItemNo).ToList();
            return plannerComments;
        }

        [HttpPut]
        [Route("UpdatePlannerComment")]
        public void Put(PlannerCommentModel value)
        {
            var navinpitemReport = _context.NavinpitemReport.SingleOrDefault(p => p.InpreportId == value.InpreportId);
            navinpitemReport.PlannerComment = value.Comments;
            _context.Update(navinpitemReport);
            _context.SaveChanges();
        }

        [HttpPost]
        [Route("GetPlannerCommentsFilter")]
        public List<PlannerCommentModel> GetPlannerFilter(SearchProductionSchedulingModel value)
        {
            List<PlannerCommentModel> filteredItems = new List<PlannerCommentModel>();

            var inpItems = _context.NavinpitemReport.Where(q => q.Quantity <= 0).Select(s => new PlannerCommentModel
            {
                InpreportId = s.InpreportId,
                ItemNo = s.ItemNo,
                Description = s.Description,
                Quantity = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                CompletionDate = s.CompletionDate,
                Company = s.Company,
                ItemCategory = s.ItemCategory,
                Uom = s.Uom,
                Comments = s.PlannerComment
            }).OrderByDescending(o => o.ItemNo).AsNoTracking().ToList();

            if (IsParamSelected(value))
            {

                if ((value.EndDate != DateTime.Now.Date))
                {
                    var dateFilter = inpItems.Where(p => (p.CompletionDate != null && p.CompletionDate.Value.Date <= value.EndDate.Value.Date)).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }

                if (!String.IsNullOrEmpty(value.Company))
                {
                    var dateFilter = inpItems.Where(p => p.Company.Trim().ToLower() == value.Company.Trim().ToLower()).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }

                if (!String.IsNullOrEmpty(value.Search))
                {
                    var dateFilter = inpItems.Where(p => p.ItemNo.Trim().ToLower().Contains(value.Search) || p.Description.Trim().ToLower().Contains(value.Search)).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }
            }
            else
            {
                filteredItems = inpItems;
            }

            return filteredItems;
        }


        // GET: api/Project
        [HttpGet]
        [Route("GetMCRCheckingItems")]
        public List<MCRCheckingModel> GetMCRCheckingItems(string itemNo)
        {
            var mcrCheckingItems = _context.Navinpreport.Select(s => new MCRCheckingModel
            {
                ItemNo = s.ItemNo,
                Quantity = s.Quantity,
                DocumentNo = s.DocumentNo,
                Balance = s.Balance,
                Demand = s.Demand,
                LocationCode = s.LocationCode,
                Supply = s.Supply,
                SourceName = s.SourceName,
                PoNo = s.Pono,
                PoBalance = s.Pobalance,
                RefPurchaseOrderNo = s.Pono,
                ReplanRefNo = s.ReplanRefNo,
                SafetyLeadTime = s.SaftyLeadTime,
                OrderDate = s.OrderDate
            }).OrderByDescending(o => o.ItemNo).Where(q => q.ItemNo == itemNo).AsNoTracking().ToList();
            return mcrCheckingItems;
        }


        [HttpPost]
        [Route("GetProductionSchedulingFilter")]
        public List<INPModel> GetFilter(SearchProductionSchedulingModel value)
        {
            List<INPModel> filteredItems = new List<INPModel>();

            var inpItems = _context.NavinpitemReport.Where(q => q.Quantity <= 0).Select(s => new INPModel
            {
                ItemNo = s.ItemNo,
                CompletionDate = s.CompletionDate,
                Description = s.Description,
                Company = s.Company,
                Quantity = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                ShipmentDate = s.ShipmentDate,
                AvailableQty = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                SaftyStock = Math.Abs(s.Quantity.GetValueOrDefault(0)),
                ItemCategory = s.ItemCategory,
                Uom = s.Uom,
                Supply = Math.Abs(s.SupplyQuantity.GetValueOrDefault(0)),
                Demand = Math.Abs(s.DemandQuantity.GetValueOrDefault(0)),
            }).OrderByDescending(o => o.ItemNo).AsNoTracking().ToList();

            if (IsParamSelected(value))
            {

                if ((value.EndDate != DateTime.Now.Date))
                {
                    var dateFilter = inpItems.Where(p => (p.CompletionDate != null && p.CompletionDate.Value.Date <= value.EndDate.Value.Date)).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }

                if (!String.IsNullOrEmpty(value.Company))
                {
                    var dateFilter = inpItems.Where(p => p.Company.Trim().ToLower() == value.Company.Trim().ToLower()).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }

                if (!String.IsNullOrEmpty(value.Search))
                {
                    var dateFilter = inpItems.Where(p => p.ItemNo.Trim().ToLower().Contains(value.Search) || p.Description.Trim().ToLower().Contains(value.Search)).ToList();
                    dateFilter.ForEach(d =>
                    {
                        if (!filteredItems.Any(f => f.ItemNo == d.ItemNo))
                        {
                            filteredItems.Add(d);
                        }
                    });
                }
            }
            else
            {
                filteredItems = inpItems;
            }

            return filteredItems;
        }

        private bool IsParamSelected(SearchProductionSchedulingModel value)
        {
            bool paramSelected = false;
            if (!String.IsNullOrEmpty(value.Search) && !string.IsNullOrEmpty(value.Company) && value.EndDate != DateTime.Now.Date)
            {
                paramSelected = true;
            }
            return paramSelected;
        }


        [HttpGet]
        [Route("GetProductionBatchSize")]
        public List<NAVRecipesModel> GetProductionBatchSize(long Id)
        {
            var nAVRecipesModels = _context.Navrecipes.Where(f=>f.CompanyId ==Id).Select(s => new NAVRecipesModel
            {
                ItemNo = s.ItemNo,
                RecipeNo = s.RecipeNo,
                Description = s.Description,
                BatchSize = s.BatchSize,
                Remark = s.Remark,
                ProductionTime = 120,
                UnitQTY = 900000,
                UOM = "Tab",
                ParentItemNo = s.ItemNo != s.RecipeNo ? s.ItemNo : null,
                CompanyId = s.CompanyId,
                
            }).OrderByDescending(o => o.ItemNo).AsNoTracking().ToList();
            List<NAVRecipesModel> navProdBomTreeModels = new List<NAVRecipesModel>();
            var lookup = nAVRecipesModels.ToLookup(x => x.ParentItemNo);
            Func<string, List<NAVRecipesModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new NAVRecipesModel()
                    {
                        ItemNo = x.ItemNo,
                        RecipeNo = x.RecipeNo,
                        Description = x.Description,
                        BatchSize = x.BatchSize,
                        Remark = x.Remark,
                        ProductionTime = 0,
                        UnitQTY = 0,
                        UOM = string.Empty,
                        Children = build(x.RecipeNo),
                        CompanyId = x.CompanyId,
                    })
                    .ToList();
            var nestedlist = build(null).ToList();

            return nestedlist.OrderBy(n=>n.Description).ToList();
        }


    }
}