﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavSaleCategoryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavSaleCategoryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNavCategory")]
        public List<NavSaleCategoryModel> Get()
        {
            List<NavSaleCategoryModel> navSaleCategoryModels = new List<NavSaleCategoryModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var navsalecategory = _context.NavSaleCategory.Include("AddedByUser")
              .Include("ModifiedByUser")
              .Include("StatusCode").OrderByDescending(o => o.SaleCategoryId).AsNoTracking().ToList();
            if(navsalecategory!=null && navsalecategory.Count>0)
            {
                navsalecategory.ForEach(s =>
                {
                    NavSaleCategoryModel navSaleCategoryModel = new NavSaleCategoryModel
                    {
                        SaleCategoryId = s.SaleCategoryId,
                        Code = s.Code,
                        Description = s.Description,
                        NoSeries = s.NoSeries,
                        LocationID = s.LocationId,
                        Location = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.LocationId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.LocationId).Value : "",
                        SGNoSeries = s.SgnoSeries,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedByUserID = s.AddedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedByUserID = s.ModifiedByUserId,
                        ModifiedDate = s.ModifiedDate,
                    };
                    navSaleCategoryModels.Add(navSaleCategoryModel);
                });
            }
           
            return navSaleCategoryModels;
        }
        [HttpGet]
        [Route("GetNavSaleCategoryByID")]
        public NavSaleCategoryModel GetByID(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var navsalecategory = _context.NavSaleCategory.Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Select(s => new NavSaleCategoryModel
                {
                    SaleCategoryId = s.SaleCategoryId,
                    Code = s.Code,
                    Description = s.Description,
                    NoSeries = s.NoSeries,
                    LocationID = s.LocationId,
                    Location = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.LocationId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.LocationId).Value : "",
                    SGNoSeries = s.SgnoSeries,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,                   
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                }).Where(t=>t.SaleCategoryId == id).OrderByDescending(o => o.SaleCategoryId).FirstOrDefault();
            return navsalecategory;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NavSaleCategoryModel> GetData(SearchModel searchModel)
        {
            var navSaleCategory = new NavSaleCategory();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).FirstOrDefault();
                        break;
                    case "Last":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).LastOrDefault();
                        break;
                    case "Next":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).LastOrDefault();
                        break;
                    case "Previous":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).FirstOrDefault();
                        break;
                    case "Last":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).LastOrDefault();
                        break;
                    case "Next":
                        navSaleCategory = _context.NavSaleCategory.OrderBy(o => o.SaleCategoryId).FirstOrDefault(s => s.SaleCategoryId > searchModel.Id);
                        break;
                    case "Previous":
                        navSaleCategory = _context.NavSaleCategory.OrderByDescending(o => o.SaleCategoryId).FirstOrDefault(s => s.SaleCategoryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NavSaleCategoryModel>(navSaleCategory);
            if(result.LocationID>0)
            {
                result.Location = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == result.LocationID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == result.LocationID).Value : "";
            }
            return result;
        }
        [HttpPut]
        [Route("UpdateNavSaleCategory")]
        public NavSaleCategoryModel Put(NavSaleCategoryModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var navSaleCategory = _context.NavSaleCategory.SingleOrDefault(p => p.SaleCategoryId == value.SaleCategoryId);
            navSaleCategory.Code = value.Code;
            navSaleCategory.Description = value.Description;
            navSaleCategory.NoSeries = value.NoSeries;
            navSaleCategory.LocationId = value.LocationID;
            navSaleCategory.SgnoSeries = value.SGNoSeries;
            navSaleCategory.StatusCodeId = value.StatusCodeID.Value;
            navSaleCategory.ModifiedByUserId = value.ModifiedByUserID;
            navSaleCategory.ModifiedDate = DateTime.Now;
            value.Location = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.LocationID) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == value.LocationID).Value : "";
            _context.SaveChanges();
            return value;
        }
    }
}