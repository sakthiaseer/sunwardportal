﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FPProductNavisionLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FPProductNavisionLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetFPProductNavisionLines")]
        public List<FPProductNavisionLineModel> Get()
        {
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var FPProductNavisionLine = _context.FpproductNavisionLine.Include(a => a.AddedByUser).Include(m => m.StatusCode).Include(m => m.ModifiedByUser).Include(f => f.Navision).AsNoTracking().ToList();
            List<FPProductNavisionLineModel> FPProductNavisionLineModel = new List<FPProductNavisionLineModel>();
            FPProductNavisionLine.ForEach(s =>
            {
                FPProductNavisionLineModel FPProductNavisionLineModels = new FPProductNavisionLineModel
                {
                    FPProductNavisionLineID = s.FpproductNavisionLineId,
                    DatabaseID = s.DatabaseId,
                    NavisionID = s.NavisionId,
                    ProfileRefNo = s.ProfileRefNo,
                    LinkProfileRefNo = s.LinkProfileRefNo,
                    MasterProfileRefNo = s.MasterProfileRefNo,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUserID = s.AddedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    Database = applicationMasterDetails != null && applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId) != null ? applicationMasterDetails.SingleOrDefault(a => a.ApplicationMasterDetailId == s.DatabaseId).Value : string.Empty,
                    Navision = s.Navision != null ? (s.Navision.No + '|' + s.Navision.Description) : "",
                };
                FPProductNavisionLineModel.Add(FPProductNavisionLineModels);
            });
            return FPProductNavisionLineModel.OrderByDescending(o => o.FPProductNavisionLineID).ToList();
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FPProductNavisionLineModel> GetData(SearchModel searchModel)
        {
            var FPProductNavisionLine = new FpproductNavisionLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).LastOrDefault();
                        break;
                    case "Next":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).LastOrDefault();
                        break;
                    case "Previous":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).FirstOrDefault();
                        break;
                    case "Last":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).LastOrDefault();
                        break;
                    case "Next":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderBy(o => o.FpproductNavisionLineId).FirstOrDefault(s => s.FpproductNavisionLineId > searchModel.Id);
                        break;
                    case "Previous":
                        FPProductNavisionLine = _context.FpproductNavisionLine.OrderByDescending(o => o.FpproductNavisionLineId).FirstOrDefault(s => s.FpproductNavisionLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FPProductNavisionLineModel>(FPProductNavisionLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFPProductNavisionLine")]
        public FPProductNavisionLineModel Post(FPProductNavisionLineModel value)
        {
            var FPProductNavisionLine = new FpproductNavisionLine
            {

                DatabaseId = value.DatabaseID,
                NavisionId = value.NavisionID,
                LinkProfileRefNo = value.LinkProfileRefNo,
                MasterProfileRefNo = value.MasterProfileRefNo,
                ProfileRefNo = value.ProfileRefNo,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID,



            };
            _context.FpproductNavisionLine.Add(FPProductNavisionLine);
            _context.SaveChanges();
            value.FPProductNavisionLineID = FPProductNavisionLine.FpproductNavisionLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFPProductNavisionLine")]
        public FPProductNavisionLineModel Put(FPProductNavisionLineModel value)
        {
            var fproduct = _context.FpproductNavisionLine.SingleOrDefault(p => p.FpproductNavisionLineId == value.FPProductNavisionLineID);
            fproduct.DatabaseId = value.DatabaseID;
            fproduct.NavisionId = value.NavisionID;
            fproduct.LinkProfileRefNo = value.LinkProfileRefNo;
            fproduct.MasterProfileRefNo = value.MasterProfileRefNo;
            fproduct.ProfileRefNo = value.ProfileRefNo;
            fproduct.StatusCodeId = value.StatusCodeID;
            fproduct.ModifiedByUserId = value.ModifiedByUserID;
            fproduct.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFPProductNavisionLine")]
        public void Delete(int id)
        {
            var FPProductNavisionLine = _context.FpproductNavisionLine.SingleOrDefault(p => p.FpproductNavisionLineId == id);
            if (FPProductNavisionLine != null)
            {
                _context.FpproductNavisionLine.Remove(FPProductNavisionLine);
                _context.SaveChanges();
            }
        }
    }
}