﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CustomerAcceptanceConfirmationController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CustomerAcceptanceConfirmationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

       
        [HttpGet("GetCustomerAcceptanceConfirmationByID")]
        public List<CustomerAcceptanceConfirmationModel> Get(int id)
        {
            var CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.Include(s=>s.AddedByUser).Include(s=>s.ModifiedByUser).Include(s=>s.StatusCode).Include(s=>s.BlanketOrder).Include(s=>s.BlanketOrder.Contract).Where(s=>s.BlanketOrderId == id).Select(s => new CustomerAcceptanceConfirmationModel
            {
                CustomerAcceptanceConfirmationID = s.CustomerAcceptanceConfirmationId,
                IsCustomerName = s.IsCustomerName,
                IsContractNo = s.IsContractNo,
                IsNoOfLot = s.IsNoOfLot,
                IsProductName = s.IsProductName,
                IsQuotationNo = s.IsQuotationNo,
                IsTenderExtensionFrom = s.IsTenderExtensionFrom,
                IsTenderExtensionTo = s.IsTenderExtensionTo,
                IsUom = s.IsUom,
                ConfirmationPONo = s.ConfirmationPono,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                AddedByUser = s.AddedByUser!=null? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser!=null? s.ModifiedByUser.UserName : "",
                StatusCode = s.StatusCode!=null? s.StatusCode.CodeValue : "",
                BlanketOrderId = s.BlanketOrderId,
                

            }).OrderByDescending(o => o.CustomerAcceptanceConfirmationID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<CustomerAcceptanceConfirmationModel>>(CustomerAcceptanceConfirmation);
            return CustomerAcceptanceConfirmation;
        }
        [HttpGet("GetCustomerAcceptanceQtyOrderByID")]
        public List<CustomerAcceptanceQtyOrderModel> GetCustomerAcceptanceQtyOrderByID(int id)
        {
            List<CustomerAcceptanceQtyOrderModel> customerAcceptanceQtyOrderModels = new List<CustomerAcceptanceQtyOrderModel>();
            var customerAcceptanceQtyOrder = _context.CustomerAcceptanceQtyOrder.Where(s => s.CustomerAcceptanceConfirmationId == id).ToList();
            if(customerAcceptanceQtyOrder!=null && customerAcceptanceQtyOrder.Count>0)
            {
                customerAcceptanceQtyOrder.ForEach(s =>
                {
                    CustomerAcceptanceQtyOrderModel customerAcceptanceQtyOrderModel = new CustomerAcceptanceQtyOrderModel
                    {
                        Qty = s.Qty,
                        CustomerAcceptanceConfirmationID = s.CustomerAcceptanceConfirmationId,
                        CustomerAcceptanceQtyOrderID = s.CustomerAcceptanceQtyOrderId,
                    };
                    customerAcceptanceQtyOrderModels.Add(customerAcceptanceQtyOrderModel);

                });
            }
            return customerAcceptanceQtyOrderModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CustomerAcceptanceConfirmationModel> GetData(SearchModel searchModel)
        {
            var CustomerAcceptanceConfirmation = new CustomerAcceptanceConfirmation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).FirstOrDefault();
                        break;
                    case "Last":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).LastOrDefault();
                        break;
                    case "Next":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).LastOrDefault();
                        break;
                    case "Previous":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).FirstOrDefault();
                        break;
                    case "Last":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).LastOrDefault();
                        break;
                    case "Next":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderBy(o => o.CustomerAcceptanceConfirmationId).FirstOrDefault(s => s.CustomerAcceptanceConfirmationId > searchModel.Id);
                        break;
                    case "Previous":
                        CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.OrderByDescending(o => o.CustomerAcceptanceConfirmationId).FirstOrDefault(s => s.CustomerAcceptanceConfirmationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CustomerAcceptanceConfirmationModel>(CustomerAcceptanceConfirmation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCustomerAcceptanceConfirmation")]
        public CustomerAcceptanceConfirmationModel Post(CustomerAcceptanceConfirmationModel value)
        {
            var CustomerAcceptanceConfirmation = new CustomerAcceptanceConfirmation
            {
                IsCustomerName = value.IsCustomerName,
                IsProductName = value.IsProductName,
                IsContractNo = value.IsContractNo,
                IsNoOfLot = value.IsNoOfLot,
                IsQuotationNo = value.IsQuotationNo,
                IsUom = value.IsUom,
                IsTenderExtensionFrom = value.IsTenderExtensionFrom,
                IsTenderExtensionTo = value.IsTenderExtensionTo,
                ConfirmationPono = value.ConfirmationPONo,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID,
                AddedDate = value.AddedDate,
                BlanketOrderId = value.BlanketOrderId,


            };
            _context.CustomerAcceptanceConfirmation.Add(CustomerAcceptanceConfirmation);
            _context.SaveChanges();
            value.CustomerAcceptanceConfirmationID = CustomerAcceptanceConfirmation.CustomerAcceptanceConfirmationId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCustomerAcceptanceConfirmation")]
        public CustomerAcceptanceConfirmationModel Put(CustomerAcceptanceConfirmationModel value)
        {
            var customerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.SingleOrDefault(p => p.CustomerAcceptanceConfirmationId == value.CustomerAcceptanceConfirmationID);
            customerAcceptanceConfirmation.IsCustomerName = value.IsCustomerName;
            customerAcceptanceConfirmation.IsProductName = value.IsProductName;
            customerAcceptanceConfirmation.IsContractNo = value.IsContractNo;
            customerAcceptanceConfirmation.IsNoOfLot = value.IsNoOfLot;
            customerAcceptanceConfirmation.IsQuotationNo = value.IsQuotationNo;
            customerAcceptanceConfirmation.IsUom = value.IsUom;
            customerAcceptanceConfirmation.IsTenderExtensionFrom = value.IsTenderExtensionFrom;
            customerAcceptanceConfirmation.IsTenderExtensionTo = value.IsTenderExtensionTo;
            customerAcceptanceConfirmation.BlanketOrderId = value.BlanketOrderId;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCustomerAcceptanceConfirmation")]
        public void Delete(int id)
        {
            var CustomerAcceptanceConfirmation = _context.CustomerAcceptanceConfirmation.SingleOrDefault(p => p.CustomerAcceptanceConfirmationId == id);
            if (CustomerAcceptanceConfirmation != null)
            {
                _context.CustomerAcceptanceConfirmation.Remove(CustomerAcceptanceConfirmation);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("InsertCustomerAcceptanceQtyOrder")]
        public CustomerAcceptanceQtyOrderModel InsertCustomerAcceptanceQtyOrder(CustomerAcceptanceQtyOrderModel value)
        {
            var customerAcceptanceQtyOrder = new CustomerAcceptanceQtyOrder
            {
                Qty = value.Qty,
                CustomerAcceptanceConfirmationId = value.CustomerAcceptanceConfirmationID,


            };
            _context.CustomerAcceptanceQtyOrder.Add(customerAcceptanceQtyOrder);
            _context.SaveChanges();
            value.CustomerAcceptanceQtyOrderID = customerAcceptanceQtyOrder.CustomerAcceptanceQtyOrderId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCustomerAcceptanceQtyOrder")]
        public CustomerAcceptanceQtyOrderModel UpdateCustomerAcceptanceQtyOrder(CustomerAcceptanceQtyOrderModel value)
        {
            var customerAcceptanceQtyOrder = _context.CustomerAcceptanceQtyOrder.SingleOrDefault(p => p.CustomerAcceptanceConfirmationId == value.CustomerAcceptanceConfirmationID);
            customerAcceptanceQtyOrder.Qty = value.Qty;
            customerAcceptanceQtyOrder.CustomerAcceptanceConfirmationId = value.CustomerAcceptanceConfirmationID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCustomerAcceptanceQtyOrder")]
        public void DeleteCustomerAcceptanceQtyOrder(int id)
        {
            var customerAcceptanceQtyOrder = _context.CustomerAcceptanceQtyOrder.SingleOrDefault(p => p.CustomerAcceptanceQtyOrderId == id);
            if (customerAcceptanceQtyOrder != null)
            {
                _context.CustomerAcceptanceQtyOrder.Remove(customerAcceptanceQtyOrder);
                _context.SaveChanges();
            }
        }

    }
}