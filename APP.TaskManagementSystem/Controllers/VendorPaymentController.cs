﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class VendorPaymentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public VendorPaymentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetVendorPayment")]
        public List<VendorPaymentModel> Get()
        {
            var vendorpayment = _context.VendorPayment.Select(s => new VendorPaymentModel
            {
                PaymentID = s.PaymentId,
                Description = s.Description,
                PaymentCode = s.PaymentCode,
                
                VendorListID = s.VendorListId.Value,


            }).OrderByDescending(o => o.PaymentID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<VendorPaymentModel>>(VendorPayment);
            return vendorpayment;
        }
        [HttpGet]
        [Route("GetActiveVendorPayment")]
        public List<VendorPaymentModel> GetActiveVendorPayment()
        {
            var vendorPayment = _context.VendorPayment.Select(s => new VendorPaymentModel
            {
                PaymentID = s.PaymentId,
                Description = s.Description,
                PaymentCode = s.PaymentCode,

                VendorListID = s.VendorListId.Value,

            }).OrderByDescending(o => o.PaymentID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<VendorPaymentModel>>(VendorPayment);
            return vendorPayment;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<VendorPaymentModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var vendorPayment = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id.Value);
            var result = _mapper.Map<VendorPaymentModel>(vendorPayment);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<VendorPaymentModel> GetData(SearchModel searchModel)
        {
            var vendorPayment = new VendorPayment();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).LastOrDefault();
                        break;
                    case "Next":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).LastOrDefault();
                        break;
                    case "Previous":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).LastOrDefault();
                        break;
                    case "Next":
                        vendorPayment = _context.VendorPayment.OrderBy(o => o.PaymentId).FirstOrDefault(s => s.PaymentId > searchModel.Id);
                        break;
                    case "Previous":
                        vendorPayment = _context.VendorPayment.OrderByDescending(o => o.PaymentId).FirstOrDefault(s => s.PaymentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<VendorPaymentModel>(vendorPayment);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertVendorPayment")]
        public VendorPaymentModel Post(VendorPaymentModel value)
        {
            var vendorPayment = new VendorPayment
            {

                PaymentCode = value.PaymentCode,
                Description = value.Description,                
                VendorListId = value.VendorListID,


            };
            _context.VendorPayment.Add(vendorPayment);
            _context.SaveChanges();
            value.PaymentID = vendorPayment.PaymentId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateVendorPayment")]
        public VendorPaymentModel Put(VendorPaymentModel value)
        {
            var vendorPayment = _context.VendorPayment.SingleOrDefault(p => p.PaymentId == value.PaymentID);
            vendorPayment.PaymentCode = value.PaymentCode;
            vendorPayment.Description = value.Description;
            vendorPayment.VendorListId = value.VendorListID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteVendorPayment")]
        public void Delete(int id)
        {
            var vendorPayment = _context.VendorPayment.SingleOrDefault(p => p.PaymentId == id);
            if (vendorPayment != null)
            {
                _context.VendorPayment.Remove(vendorPayment);
                _context.SaveChanges();
            }
        }
    }
}