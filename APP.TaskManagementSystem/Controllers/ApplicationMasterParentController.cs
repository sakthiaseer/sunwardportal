﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationMasterParentController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public ApplicationMasterParentController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetApplicationMasterParentByParent")]
        public List<ApplicationMasterParentModel> GetApplicationMasterParentByParent()
        {
            List<ApplicationMasterParentModel> applicationMasterParentModels = new List<ApplicationMasterParentModel>();
            var applicationmasterParent = _context.ApplicationMasterParent.Include(a => a.Parent).Where(w => w.ParentId == null).AsNoTracking().ToList();
            if (applicationmasterParent != null)
            {
                applicationmasterParent.ForEach(s =>
                {
                    ApplicationMasterParentModel applicationMasterParentModel = new ApplicationMasterParentModel();
                    applicationMasterParentModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterParentModel.ApplicationMasterParentCodeId = s.ApplicationMasterParentCodeId;
                    applicationMasterParentModel.ApplicationMasterName = s.ApplicationMasterName;
                    applicationMasterParentModel.Description = s.Description;
                    applicationMasterParentModel.ParentId = s.ParentId;
                    applicationMasterParentModel.IsDisplay = s.IsDisplay;
                    applicationMasterParentModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterParentModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterParentModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterParentModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterParentModel.ParentName = s.Parent?.ApplicationMasterName;
                    applicationMasterParentModels.Add(applicationMasterParentModel);
                });
            }
            return applicationMasterParentModels.OrderByDescending(o => o.ApplicationMasterParentCodeId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterParent")]
        public List<ApplicationMasterParentModel> Get()
        {
            List<ApplicationMasterParentModel> applicationMasterParentModels = new List<ApplicationMasterParentModel>();
            var applicationmasterParent = _context.ApplicationMasterParent.Include(a => a.Parent).AsNoTracking().ToList();
            if (applicationmasterParent != null)
            {
                applicationmasterParent.ForEach(s =>
                {
                    ApplicationMasterParentModel applicationMasterParentModel = new ApplicationMasterParentModel();
                    applicationMasterParentModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterParentModel.ApplicationMasterParentCodeId = s.ApplicationMasterParentCodeId;
                    applicationMasterParentModel.ApplicationMasterName = s.ApplicationMasterName;
                    applicationMasterParentModel.Description = s.Description;
                    applicationMasterParentModel.ParentId = s.ParentId;
                    applicationMasterParentModel.IsDisplay = s.IsDisplay;
                    applicationMasterParentModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterParentModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterParentModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterParentModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterParentModel.ParentName = s.Parent?.ApplicationMasterName;
                    applicationMasterParentModels.Add(applicationMasterParentModel);
                });
            }
            return applicationMasterParentModels.OrderByDescending(o => o.ApplicationMasterParentCodeId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterParentDropDownList")]
        public List<ApplicationMasterParentModel> GetApplicationMasterParentDropDownList(long? id)
        {
            List<ApplicationMasterParentModel> applicationMasterParentModels = new List<ApplicationMasterParentModel>();
            var applicationmasterParent = _context.ApplicationMasterParent.AsNoTracking().Where(w => w.ApplicationMasterParentCodeId == id).ToList();
            if (applicationmasterParent != null)
            {
                applicationmasterParent.ForEach(s =>
                {
                    ApplicationMasterParentModel applicationMasterParentModel = new ApplicationMasterParentModel();
                    applicationMasterParentModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterParentModel.ApplicationMasterParentCodeId = s.ApplicationMasterParentCodeId;
                    applicationMasterParentModel.ApplicationMasterName = s.ApplicationMasterName;
                    applicationMasterParentModel.Description = s.Description;
                    applicationMasterParentModel.ParentId = s.ParentId;
                    applicationMasterParentModel.IsDisplay = s.IsDisplay;
                    applicationMasterParentModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterParentModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterParentModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterParentModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterParentModels.Add(applicationMasterParentModel);
                });
            }
            return applicationMasterParentModels.OrderByDescending(o => o.ApplicationMasterParentCodeId).ToList();
        }
        [HttpGet]
        [Route("GetApplicationMasterParentDropDown")]
        public List<ApplicationMasterParentModel> GetApplicationMasterParentDropDown(long? id)
        {
            List<ApplicationMasterParentModel> applicationMasterParentModels = new List<ApplicationMasterParentModel>();
            var applicationmasterParent = _context.ApplicationMasterParent.AsNoTracking().Where(w => w.ApplicationMasterParentCodeId != id).ToList();
            if (applicationmasterParent != null)
            {
                applicationmasterParent.ForEach(s =>
                {
                    ApplicationMasterParentModel applicationMasterParentModel = new ApplicationMasterParentModel();
                    applicationMasterParentModel.ApplicationMasterParentId = s.ApplicationMasterParentId;
                    applicationMasterParentModel.ApplicationMasterParentCodeId = s.ApplicationMasterParentCodeId;
                    applicationMasterParentModel.ApplicationMasterName = s.ApplicationMasterName;
                    applicationMasterParentModel.Description = s.Description;
                    applicationMasterParentModel.ParentId = s.ParentId;
                    applicationMasterParentModel.IsDisplay = s.IsDisplay;
                    applicationMasterParentModel.StatusCodeID = s.StatusCodeId;
                    applicationMasterParentModel.AddedByUserID = s.AddedByUserId;
                    applicationMasterParentModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationMasterParentModel.ModifiedDate = s.ModifiedDate;
                    applicationMasterParentModels.Add(applicationMasterParentModel);
                });
            }
            return applicationMasterParentModels.OrderByDescending(o => o.ApplicationMasterParentCodeId).ToList();
        }
        [HttpPost]
        [Route("GetData")]
        public ActionResult<ApplicationMasterParentModel> GetData(SearchModel searchModel)
        {
            var applicationmasterParent = new ApplicationMasterParent();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderBy(o => o.ApplicationMasterParentId).FirstOrDefault(s => s.ApplicationMasterParentId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationmasterParent = _context.ApplicationMasterParent.OrderByDescending(o => o.ApplicationMasterParentId).FirstOrDefault(s => s.ApplicationMasterParentId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationMasterParentModel>(applicationmasterParent);
            return result;
        }
        [HttpPost]
        [Route("InsertApplicationMasterParent")]
        public ApplicationMasterParentModel Post(ApplicationMasterParentModel value)
        {
            try
            {
                var applicationmasterParent = new ApplicationMasterParent
                {
                    ApplicationMasterParentCodeId = value.ApplicationMasterParentCodeId,
                    ApplicationMasterName = value.ApplicationMasterName,
                    Description = value.Description,
                    ParentId = value.ParentId,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,

                };
                _context.ApplicationMasterParent.Add(applicationmasterParent);
                _context.SaveChanges();
                value.ApplicationMasterParentId = applicationmasterParent.ApplicationMasterParentId;

            }
            catch (Exception ex)
            {
                var errorMessage = "Cannot insert duplicate ApplicationMaster Code";
                throw new AppException(errorMessage, ex);
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateApplicationMasterParent")]
        public ApplicationMasterParentModel Put(ApplicationMasterParentModel value)
        {
            try
            {
                var applicationmasterParent = _context.ApplicationMasterParent.SingleOrDefault(p => p.ApplicationMasterParentId == value.ApplicationMasterParentId);
                applicationmasterParent.ApplicationMasterParentCodeId = value.ApplicationMasterParentCodeId;
                applicationmasterParent.ApplicationMasterName = value.ApplicationMasterName;
                applicationmasterParent.Description = value.Description;
                applicationmasterParent.ParentId = value.ParentId;
                applicationmasterParent.ModifiedByUserId = value.ModifiedByUserID;
                applicationmasterParent.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var errorMessage = "Cannot insert duplicate ApplicationMaster Code";
                throw new AppException(errorMessage, ex);
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteApplicationMasterParent")]
        public void Delete(int id)
        {
            try
            {
                var applicationmasterParent = _context.ApplicationMasterParent.SingleOrDefault(p => p.ApplicationMasterParentCodeId == id);
                if (applicationmasterParent != null)
                {
                    var applicationmasterChild = _context.ApplicationMasterChild.Where(p => p.ApplicationMasterParentId == id).ToList();
                    if (applicationmasterChild != null)
                    {
                        _context.ApplicationMasterChild.RemoveRange(applicationmasterChild);
                        _context.SaveChanges();
                    }
                    _context.ApplicationMasterParent.Remove(applicationmasterParent);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "This record cannot be deleted! Reference to others!";
                throw new AppException(errorMessage, ex);
            }
        }
    }
}
