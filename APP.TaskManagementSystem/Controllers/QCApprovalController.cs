﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class QCApprovalController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public QCApprovalController(CRT_TMSContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetQCApprovals")]
        public List<QcApprovalModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<QcApprovalModel> qcApprovalModels = new List<QcApprovalModel>();
            var qcApprovals = _context.Qcapproval.Include(q => q.QcapprovalLine).Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.QcapprovalId).AsNoTracking().ToList();

            qcApprovals.ForEach(s =>
            {
                var qcApprovalModel = new QcApprovalModel
                {
                    QcapprovalId = s.QcapprovalId,
                    InspectionNo = s.InspectionNo,
                    IsRetest = s.IsRetest,
                    IsRetestFlag = s.IsRetest == true ? "Yes" : "No",
                    TestNameId = s.TestNameId,
                    TestName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.TestNameId).Select(a => a.Value).SingleOrDefault() : "",
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : s.AddedByUser.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,

                };
                if (s.QcapprovalLine != null)
                {
                    s.QcapprovalLine.ToList().ForEach(q =>
                    {
                        QcApprovalLineModel qcApprovalLineModel = new QcApprovalLineModel
                        {
                            QcapprovalId = q.QcapprovalId,
                            QcapprovalLineId = q.QcapprovalLineId,
                            ProductionOrderNo = q.ProductionOrderNo,
                            SubLotNo = q.SubLotNo,
                            DrumNo = q.DrumNo,
                            MoisturePercent = q.MoisturePercent,
                        };
                        qcApprovalModel.QcApprovalLines.Add(qcApprovalLineModel);
                    });
                }
                qcApprovalModels.Add(qcApprovalModel);
            });
            return qcApprovalModels;
        }
        [HttpGet]
        [Route("GetQCApprovalsLine")]
        public List<QcApprovalLineModel> GetQCApprovalsLine(long id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<QcApprovalLineModel> qcApprovalModels = new List<QcApprovalLineModel>();
            var qcApprovals = _context.QcapprovalLine.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.QcapprovalLineId).Where(w => w.QcapprovalId == id).AsNoTracking().ToList();
            qcApprovals.ForEach(s =>
            {
                QcApprovalLineModel qcApprovalLineModel = new QcApprovalLineModel
                {
                    QcapprovalId = s.QcapprovalId,
                    QcapprovalLineId = s.QcapprovalLineId,
                    ProductionOrderNo = s.ProductionOrderNo,
                    SubLotNo = s.SubLotNo,
                    DrumNo = s.DrumNo,
                    ModifiedByUserID = s.AddedByUserId,
                    AddedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : s.AddedByUser.UserName,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    MoisturePercent = s.MoisturePercent,
                };
                qcApprovalModels.Add(qcApprovalLineModel);
            });
            return qcApprovalModels.OrderByDescending(o => o.QcapprovalLineId).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<QcApprovalModel> GetData(SearchModel searchModel)
        {
            var qcapproval = new Qcapproval();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).FirstOrDefault();
                        break;
                    case "Last":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).LastOrDefault();
                        break;
                    case "Next":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).LastOrDefault();
                        break;
                    case "Previous":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).FirstOrDefault();
                        break;

                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).FirstOrDefault();
                        break;
                    case "Last":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).LastOrDefault();
                        break;
                    case "Next":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderBy(o => o.QcapprovalId).FirstOrDefault(s => s.QcapprovalId > searchModel.Id);
                        break;
                    case "Previous":
                        qcapproval = _context.Qcapproval.Include(q => q.QcapprovalLine).OrderByDescending(o => o.QcapprovalId).FirstOrDefault(s => s.QcapprovalId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<QcApprovalModel>(qcapproval);
            /*            if (qcapproval.QcapprovalLine != null)
                        {
                            qcapproval.QcapprovalLine.ToList().ForEach(q =>
                            {
                                QcApprovalLineModel qcApprovalLineModel = new QcApprovalLineModel
                                {
                                    QcapprovalId = q.QcapprovalId,
                                    QcapprovalLineId = q.QcapprovalLineId,
                                    ProductionOrderNo = q.ProductionOrderNo,
                                    SubLotNo = q.SubLotNo,
                                    DrumNo = q.DrumNo
                                };
                                result.QcApprovalLines.Add(qcApprovalLineModel);
                            });
                        }*/

            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertQcApproval")]
        public async Task<QcApprovalModel> Post(QcApprovalModel value)
        {
            if (!value.IsSkipValidation.GetValueOrDefault(false))
            {
                var anyExist = _context.Qcapproval.Any(a => a.InspectionNo == value.InspectionNo);
                if (anyExist)
                {
                    var qcapproval = new QcApprovalModel
                    {
                        IsError = true,
                        Errormessage = "Production order QC Completed.Please scan another Production Order!"

                    };
                    return qcapproval;
                }
            }

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            try
            {
                var sessionId = value.SessionId ?? Guid.NewGuid();
                var qcapproval = new Qcapproval
                {
                    InspectionNo = value.InspectionNo,
                    IsRetest = value.IsRetest,
                    TestNameId = value.TestNameId,
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID,
                    SessionId = sessionId
                };

                List<ProductionOutput> productionOutputs = _context.ProductionOutput.Where(c => c.ProductionOrderNo == value.ProductionOrderNo).ToList();

                productionOutputs.ForEach(p =>
                {
                    qcapproval.QcapprovalLine.Add(new QcapprovalLine
                    {
                        ProductionOrderNo = p.ProductionOrderNo,
                        SubLotNo = p.SubLotNo,
                        DrumNo = p.DrumNo,
                        AddedByUserId = value.AddedByUserID.Value,
                        AddedDate = DateTime.Now,
                        StatusCodeId = value.StatusCodeID,
                    });
                });

                _context.Qcapproval.Add(qcapproval);
                _context.SaveChanges();
                value.IsError = false;
                value.Errormessage = string.Empty;

                value.QcapprovalId = qcapproval.QcapprovalId;
                if (value.TestNameId > 0)
                {
                    value.TestName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.TestNameId).Select(a => a.Value).SingleOrDefault() : "";
                }
                if (qcapproval.QcapprovalLine != null)
                {

                    value.QcApprovalLines = new List<QcApprovalLineModel>();
                    qcapproval.QcapprovalLine.ToList().ForEach(l =>
                    {
                        value.QcApprovalLines.Add(new QcApprovalLineModel
                        {
                            ProductionOrderNo = l.ProductionOrderNo,
                            SubLotNo = l.SubLotNo,
                            DrumNo = l.DrumNo,
                            MoisturePercent = l.MoisturePercent,
                        });
                    });
                }
                value.QcapprovalId = qcapproval.QcapprovalId;
                //var navstatus = await PostQCNAV(value);
                //if (!navstatus)
                //{
                //    throw new Exception(errorMessage);
                //}
                return value;
            }

            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }

        }
        string errorMessage = string.Empty;
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateQcApproval")]
        public QcApprovalModel Put(QcApprovalModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var qcapproval = _context.Qcapproval.SingleOrDefault(p => p.QcapprovalId == value.QcapprovalId);
            var sessionId = value.SessionId ?? Guid.NewGuid();
            qcapproval.InspectionNo = value.InspectionNo;
            qcapproval.IsRetest = value.IsRetest;
            qcapproval.TestNameId = value.TestNameId;
            qcapproval.ModifiedByUserId = value.ModifiedByUserID.Value;
            qcapproval.ModifiedDate = DateTime.Now;
            qcapproval.StatusCodeId = value.StatusCodeID.Value;
            qcapproval.IsRetest = value.IsRetestFlag == "Yes" ? true : false;
            qcapproval.SessionId = sessionId;
            _context.SaveChanges();

            return value;
        }


        [HttpPost]
        [Route("PostQCNAV")]
        public async Task<QcApprovalModel> PostQCNAV(QcApprovalModel value)
        {
            try
            {
                var qcApproval = _context.Qcapproval.FirstOrDefault(q => q.QcapprovalId == value.QcapprovalId);
                qcApproval.StatusCodeId = value.StatusCodeID;
                _context.SaveChanges();
                var productionOutputs = _context.ProductionOutput.Where(c => c.ProductionOrderNo == value.ProductionOrderNo).ToList();

                var groupedProdOrders = productionOutputs.GroupBy(g => g.ProductionOrderNo).ToList();

                if (value.StatusCodeID != 2211)
                {
                    var context = new DataService(_config, value.CompanyName);
                    var Company = value.CompanyName;
                    foreach (var prod in groupedProdOrders)
                    {
                        var p = prod.FirstOrDefault() as ProductionOutput;
                        p.OutputQty = prod.Sum(s => s.OutputQty);


                        var nquery = context.Context.CPS_ProdOrderLine.Where(i => i.Prod_Order_No == p.ProductionOrderNo);
                        DataServiceQuery<NAV.CPS_ProdOrderLine> query = (DataServiceQuery<NAV.CPS_ProdOrderLine>)nquery;
                        TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>> taskFactory1 = new TaskFactory<IEnumerable<NAV.CPS_ProdOrderLine>>();
                        var prodLine = await taskFactory1.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                        var prodList = prodLine.FirstOrDefault();

                        int entryNo = (int)HiResDateTime.UtcNowTicks;
                        var QcData = new SWDWebIntegrationEntry
                        {
                            Posting_Date = DateTime.Now,
                            Entry_Type = "Post QC",
                            Item_No = p.ItemNo,
                            Approve_Quantity = value.StatusCodeID == 2210 ? p.OutputQty : 0,
                            Rework_Quantity = value.StatusCodeID == 2212 ? p.OutputQty : 0,
                            Batch_No = p.BatchNo,
                            Created_on = DateTime.Now,
                            Lot_No = p.SubLotNo,
                            Location_Code = prodList.Location_Code,
                            Entry_No = entryNo,
                            Inspection_Datasheet_No = value.InspectionNo,
                            Quality_Control_Type = "In-stock",
                            Document_No = p.ProductionOrderNo,
                            //Document_Line_No = p.SubLotNo
                        };

                        context.Context.AddToSWDWebIntegrationEntry(QcData);

                        TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                        var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));


                        var post = new StockAdjustmentService.SWDWebIntegration_PortClient();

                        post.Endpoint.Address =
                   new EndpointAddress(new Uri(_config[Company + ":SoapUrl"] + "/" + _config[Company + ":Company"] + "/Codeunit/SWDWebIntegration"),
                   new DnsEndpointIdentity(""));

                        post.ClientCredentials.UserName.UserName = _config[Company + ":UserName"];
                        post.ClientCredentials.UserName.Password = _config[Company + ":Password"];
                        post.ClientCredentials.Windows.ClientCredential.UserName = _config[Company + ":UserName"]; ;
                        post.ClientCredentials.Windows.ClientCredential.Password = _config[Company + ":Password"];
                        post.ClientCredentials.Windows.ClientCredential.Domain = _config[Company + ":Domain"];
                        post.ClientCredentials.Windows.AllowedImpersonationLevel =
                        System.Security.Principal.TokenImpersonationLevel.Impersonation;

                        var result = await post.FnPostQCInspectionAsync(entryNo);
                    }
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "Production Lot is not Completed";
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;
                value.Errormessage = errorMessage;
                return value;
            }
            return value;
        }

        [HttpPost]
        [Route("GetApprovalLineByOrderNo")]
        public async Task<QcApprovalModel> GetApprovalLineByOrderNo(QcApprovalModel value)
        {
            List<ProductionOutput> productionOutputs = _context.ProductionOutput.Where(c => c.ProductionOrderNo == value.ProductionOrderNo).ToList();
            var lotGroups = productionOutputs.GroupBy(s => s.SubLotNo).ToList();
            foreach (var item in lotGroups)
            {
                var lotList = item.ToList();
                if (lotList.Any(s => s.IsLotComplete == true))
                {
                    continue;
                }
                else
                {
                    var qcapproval = new QcApprovalModel
                    {
                        IsError = true,
                        Errormessage = "Procduction order Lot is not Completed!",
                    };
                    return qcapproval;
                }
            }

            if (productionOutputs.Any(g => g.IsProdutionOrderComplete == true))
            {
                var result = await Post(value);

                if (result.IsError)
                {
                    return result;
                }
                result.QcApprovalLines = new List<QcApprovalLineModel>();
                List<QcApprovalLineModel> qcApprovalLineModels = new List<QcApprovalLineModel>();
                List<QcapprovalLine> qcapprovalLines = _context.QcapprovalLine.Where(c => c.ProductionOrderNo == value.ProductionOrderNo).ToList();

                qcapprovalLines.ForEach(p =>
                {
                    result.QcApprovalLines.Add(new QcApprovalLineModel
                    {
                        ProductionOrderNo = p.ProductionOrderNo,
                        SubLotNo = p.SubLotNo,
                        DrumNo = p.DrumNo,
                        QcapprovalLineId = p.QcapprovalLineId,
                        QcapprovalId = p.QcapprovalId
                    });
                });

                return result;
            }
            else
            {
                var qcapproval = new QcApprovalModel
                {
                    IsError = true,
                    Errormessage = "Procduction order is not Completed!",
                };
                return qcapproval;
            }
        }


        [HttpPost]
        [Route("UpdateQcApprovalLine")]
        public QcApprovalLineModel UpdateQcApprovalLine(QcApprovalLineModel value)
        {
            var qcapprovalLine = _context.QcapprovalLine.SingleOrDefault(p => p.QcapprovalLineId == value.QcapprovalLineId);
            qcapprovalLine.StatusCodeId = value.StatusCodeID;
            qcapprovalLine.MoisturePercent = value.MoisturePercent;
            qcapprovalLine.MoistureStatus = value.MoistureStatus;
            _context.SaveChanges();
            var qcapprovalLineAll = _context.QcapprovalLine.Where(p => p.QcapprovalId == qcapprovalLine.QcapprovalId).Count();
            var qcapprovalLines = _context.QcapprovalLine.Where(p => p.QcapprovalId == qcapprovalLine.QcapprovalId && p.StatusCodeId == 2210).Count();
            if (qcapprovalLineAll > 0 && qcapprovalLineAll == qcapprovalLines)
            {
                var qcapproval = _context.Qcapproval.SingleOrDefault(p => p.QcapprovalId == qcapprovalLine.QcapprovalId);
                qcapproval.StatusCodeId = value.StatusCodeID;
                _context.SaveChanges();
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteQcApproval")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var qcapproval = _context.Qcapproval.Include(c => c.QcapprovalLine).SingleOrDefault(p => p.QcapprovalId == id);
                if (qcapproval != null)
                {
                    _context.Qcapproval.Remove(qcapproval);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteQcApprovalLine")]
        public ActionResult<string> DeleteQcApprovalLine(int id)
        {
            try
            {
                var qcapprovalLine = _context.QcapprovalLine.SingleOrDefault(p => p.QcapprovalLineId == id);
                if (qcapprovalLine != null)
                {
                    _context.QcapprovalLine.Remove(qcapprovalLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
