﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CommonPackagingBottleCapController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public CommonPackagingBottleCapController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetCommonPackagingBottleCap")]
        public List<CommonPackagingBottleCapModel> Get()
        {
            var commonPackagingBottleCap = _context.CommonPackagingBottleCap
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(b => b.BottleCapUseForItems)
                .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<CommonPackagingBottleCapModel> commonPackagingBottleCapModel = new List<CommonPackagingBottleCapModel>();
            if (commonPackagingBottleCap.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottleCap.Where(w => w.PackagingMaterialColorId != null).Select(a => a.PackagingMaterialColorId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottleCap.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleCap.Where(w => w.PackagingTypeId != null).Select(a => a.PackagingTypeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleCap.Where(w => w.CapTypeId != null).Select(a => a.CapTypeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottleCap.ForEach(s =>
                {
                    CommonPackagingBottleCapModel commonPackagingBottleCapModels = new CommonPackagingBottleCapModel
                    {
                        BottleCapSpecificationId = s.BottleCapSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        CapMeasurement = s.CapMeasurement,
                        Code = s.Code,
                        PackagingMaterialColorId = s.PackagingMaterialColorId,
                        CapTypeId = s.CapTypeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == s.BottleCapSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackagingTypeId = s.PackagingTypeId,
                        PackagingMaterialColorName = s.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingTypeName = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        CapTypeName = s.CapTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CapTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingBottleCapModel.Add(commonPackagingBottleCapModels);
                });
            }
            return commonPackagingBottleCapModel.OrderByDescending(a => a.BottleCapSpecificationId).ToList();
        }

        [HttpPost]
        [Route("GetCommonPackagingBottleCapByRefNo")]
        public List<CommonPackagingBottleCapModel> GetCommonPackagingBottleCapByRefNo(RefSearchModel refSearchModel)
        {
            var commonPackagingBottleCap = _context.CommonPackagingBottleCap
               .Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(b => b.BottleCapUseForItems)
               .Include(s => s.StatusCode).Where(w => w.BottleCapSpecificationId > 0);
            if (refSearchModel.IsHeader)
            {
                commonPackagingBottleCap = commonPackagingBottleCap.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            else
            {
                commonPackagingBottleCap = commonPackagingBottleCap.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo);
            }
            var commonPackagingBottleCaps = commonPackagingBottleCap.AsNoTracking().ToList();
            List<CommonPackagingBottleCapModel> commonPackagingBottleCapModel = new List<CommonPackagingBottleCapModel>();
            if (commonPackagingBottleCaps.Count > 0)
            {
                List<long?> masterIds = commonPackagingBottleCaps.Where(w => w.PackagingMaterialColorId != null).Select(a => a.PackagingMaterialColorId).Distinct().ToList();
                masterIds.AddRange(commonPackagingBottleCaps.Where(w => w.PackagingItemSetInfoId != null).Select(a => a.PackagingItemSetInfoId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleCaps.Where(w => w.PackagingTypeId != null).Select(a => a.PackagingTypeId).Distinct().ToList());
                masterIds.AddRange(commonPackagingBottleCaps.Where(w => w.CapTypeId != null).Select(a => a.CapTypeId).Distinct().ToList());
                var masterDetailList = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                commonPackagingBottleCaps.ForEach(s =>
                {
                    CommonPackagingBottleCapModel commonPackagingBottleCapModels = new CommonPackagingBottleCapModel
                    {
                        BottleCapSpecificationId = s.BottleCapSpecificationId,
                        PackagingItemSetInfoId = s.PackagingItemSetInfoId,
                        Set = s.Set,
                        CapMeasurement = s.CapMeasurement,
                        Code = s.Code,
                        PackagingMaterialColorId = s.PackagingMaterialColorId,
                        CapTypeId = s.CapTypeId,
                        PackagingTypeId = s.PackagingTypeId,
                        VersionControl = s.VersionControl,
                        UseforItemIds = s.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == s.BottleCapSpecificationId).Select(b => b.UseForItemId).ToList(),
                        PackagingMaterialColorName = s.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingItemSetInfoName = s.PackagingItemSetInfoId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingItemSetInfoId).Select(m => m.Value).FirstOrDefault() : "",
                        PackagingTypeName = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        CapTypeName = s.CapTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CapTypeId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                    };
                    commonPackagingBottleCapModel.Add(commonPackagingBottleCapModels);
                });
            }
            return commonPackagingBottleCapModel.OrderByDescending(o => o.BottleCapSpecificationId).ToList();
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CommonPackagingBottleCapModel> GetData(SearchModel searchModel)
        {
            var commonPackagingBottleCap = new CommonPackagingBottleCap();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).LastOrDefault();
                        break;
                    case "Previous":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).FirstOrDefault();
                        break;
                    case "Last":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).LastOrDefault();
                        break;
                    case "Next":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderBy(o => o.BottleCapSpecificationId).FirstOrDefault(s => s.BottleCapSpecificationId > searchModel.Id);
                        break;
                    case "Previous":
                        commonPackagingBottleCap = _context.CommonPackagingBottleCap.OrderByDescending(o => o.BottleCapSpecificationId).FirstOrDefault(s => s.BottleCapSpecificationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CommonPackagingBottleCapModel>(commonPackagingBottleCap);
            if (result != null && result.BottleCapSpecificationId > 0)
            {

                result.UseforItemIds = _context.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == result.BottleCapSpecificationId).Select(b => b.UseForItemId).ToList();
            }
            return result;
        }
        [HttpPost]
        [Route("InsertCommonPackagingBottleCap")]
        public CommonPackagingBottleCapModel Post(CommonPackagingBottleCapModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.Code });
            var commonPackagingBottleCap = new CommonPackagingBottleCap
            {
                PackagingItemSetInfoId = value.PackagingItemSetInfoId,
                Set = value.Set,
                CapMeasurement = value.CapMeasurement,
                Code = value.Code,
                CapTypeId = value.CapTypeId,
                PackagingMaterialColorId = value.PackagingMaterialColorId,
                PackagingTypeId = value.PackagingTypeId,
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                VersionControl = value.VersionControl,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.CommonPackagingBottleCap.Add(commonPackagingBottleCap);
            _context.SaveChanges();

            value.MasterProfileReferenceNo = commonPackagingBottleCap.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = commonPackagingBottleCap.ProfileLinkReferenceNo;
            value.BottleCapSpecificationId = commonPackagingBottleCap.BottleCapSpecificationId;
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == value.BottleCapSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        BottleCapSpecificationId = value.BottleCapSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            value.PackagingItemName = UpdateCommonPackage(value);

            return value;
        }
        private string UpdateCommonPackage(CommonPackagingBottleCapModel value)
        {
            value.PackagingItemName = "";
            var itemName = " ";
            //if (value.FormTab == true)
            //{
            if (value.LinkProfileReferenceNo != null)
            {
                var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                if (itemHeader != null)
                {
                    var masterDetailList = _context.ApplicationMasterDetail.ToList();
                    var PackagingMaterialColorName = value.PackagingMaterialColorId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialColorId).Select(m => m.Value).FirstOrDefault() : "";
                    var PackagingTypeName = value.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "";
                    var CapTypeName = value.CapTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.CapTypeId).Select(m => m.Value).FirstOrDefault() : "";
                    if (value.CapMeasurement != null && value.CapMeasurement > 0)
                    {
                        itemName = itemName + value.CapMeasurement + " " + "mm" + " ";
                    }
                    if (!string.IsNullOrEmpty(value.Code))
                    {
                        itemName = itemName + value.Code + " ";
                    }
                    if (!string.IsNullOrEmpty(PackagingTypeName))
                    {
                        itemName = itemName + PackagingTypeName + " ";
                    }
                    if (!string.IsNullOrEmpty(PackagingMaterialColorName))
                    {
                        itemName = itemName + PackagingMaterialColorName + " ";
                    }
                    if (!string.IsNullOrEmpty(CapTypeName))
                    {
                        itemName = itemName + CapTypeName + " ";
                    }
                    itemName = itemName + "Cap";
                    //itemName = value.CapMeasurement + " " + "mm" + " " + value.Code + " " + PackagingTypeName + " " + PackagingMaterialColorName + " " + CapTypeName + " " + "Cap";
                    itemHeader.PackagingItemName = itemName;
                    value.PackagingItemName = itemName;
                    _context.SaveChanges();
                }
            }
            //}
            return value.PackagingItemName;
        }
        [HttpPut]
        [Route("UpdateCommonPackagingBottleCap")]
        public CommonPackagingBottleCapModel Put(CommonPackagingBottleCapModel value)
        {
            var commonPackagingBottleCap = _context.CommonPackagingBottleCap.SingleOrDefault(p => p.BottleCapSpecificationId == value.BottleCapSpecificationId);
            commonPackagingBottleCap.PackagingItemSetInfoId = value.PackagingItemSetInfoId;
            commonPackagingBottleCap.Set = value.Set;
            commonPackagingBottleCap.CapMeasurement = value.CapMeasurement;
            commonPackagingBottleCap.Code = value.Code;
            commonPackagingBottleCap.CapTypeId = value.CapTypeId;
            commonPackagingBottleCap.PackagingMaterialColorId = value.PackagingMaterialColorId;
            commonPackagingBottleCap.PackagingTypeId = value.PackagingTypeId;
            commonPackagingBottleCap.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            commonPackagingBottleCap.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            commonPackagingBottleCap.StatusCodeId = value.StatusCodeID.Value;
            commonPackagingBottleCap.ModifiedByUserId = value.ModifiedByUserID;
            commonPackagingBottleCap.ModifiedDate = DateTime.Now;
            commonPackagingBottleCap.VersionControl = value.VersionControl;
            var UseforItemItems = _context.BottleCapUseForItems.Where(w => w.BottleCapSpecificationId == value.BottleCapSpecificationId).ToList();
            if (UseforItemItems != null)
            {
                _context.BottleCapUseForItems.RemoveRange(UseforItemItems);
                _context.SaveChanges();
            }
            if (value.UseforItemIds != null && value.UseforItemIds.Count > 0)
            {
                value.UseforItemIds.ForEach(u =>
                {
                    //var exist = _context.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == value.BottleCapSpecificationId && b.UseForItemId == u).FirstOrDefault();
                    //if (exist == null)
                    //{
                    var bottlecapUseForItems = new BottleCapUseForItems()
                    {
                        BottleCapSpecificationId = value.BottleCapSpecificationId,
                        UseForItemId = u,
                    };
                    _context.BottleCapUseForItems.Add(bottlecapUseForItems);
                    _context.SaveChanges();
                    //}
                });
            }
            _context.SaveChanges();
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        [HttpDelete]
        [Route("DeleteCommonPackagingBottleCap")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var commonPackagingBottleCap = _context.CommonPackagingBottleCap.Where(p => p.BottleCapSpecificationId == id).FirstOrDefault();
                if (commonPackagingBottleCap != null)
                {
                    var bottlecapuse = _context.BottleCapUseForItems.Where(b => b.BottleCapSpecificationId == id).ToList();
                    if (bottlecapuse != null && bottlecapuse.Count > 0)
                    {
                        _context.BottleCapUseForItems.RemoveRange(bottlecapuse);
                        _context.SaveChanges();
                    }
                    _context.CommonPackagingBottleCap.Remove(commonPackagingBottleCap);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}