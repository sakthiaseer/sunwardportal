﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NovateInformationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NovateInformationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNovateInformationLines")]
        public List<NovateInformationLineModel> Get(int id)
        {
            var novateInformationLines = _context.NovateInformationLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.NovateFrom)
                .Include(s => s.NovateTo)
                .Where(s => s.TenderInformationId == id)
                .AsNoTracking()
                .ToList();
            List<NovateInformationLineModel> novateInformationLineModels = new List<NovateInformationLineModel>();
            novateInformationLines.ForEach(s =>
            {
                NovateInformationLineModel novateInformationLineModel = new NovateInformationLineModel
                {
                    NovateInformationLineId = s.NovateInformationLineId,
                    NovateFromId = s.NovateFromId,
                    NovateToId = s.NovateToId,
                    NovateFrom = s.NovateFrom?.CompanyName,
                    NovateTo = s.NovateTo?.CompanyName,
                    Quantity = s.Quantity,
                    TenderInformationId = s.TenderInformationId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                novateInformationLineModels.Add(novateInformationLineModel);
            });
            return novateInformationLineModels.OrderByDescending(a => a.NovateInformationLineId).ToList();
        }


        [HttpPost]
        [Route("InsertNovateInformationLine")]
        public NovateInformationLineModel Post(NovateInformationLineModel value)
        {
            var novateInformationLine = new NovateInformationLine
            {
                NovateFromId = value.NovateFromId,
                NovateToId = value.NovateToId,
                Quantity = value.Quantity,
                TenderInformationId = value.TenderInformationId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.NovateInformationLine.Add(novateInformationLine);
            _context.SaveChanges();
            value.NovateInformationLineId = novateInformationLine.NovateInformationLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNovateInformationLine")]
        public NovateInformationLineModel Put(NovateInformationLineModel value)
        {
            var novateInformationLine = _context.NovateInformationLine.SingleOrDefault(p => p.NovateInformationLineId == value.NovateInformationLineId);
            novateInformationLine.NovateInformationLineId = value.NovateInformationLineId;
            novateInformationLine.Quantity = value.Quantity;
            novateInformationLine.NovateFromId = value.NovateFromId;
            novateInformationLine.NovateToId = value.NovateToId;
            novateInformationLine.TenderInformationId = value.TenderInformationId;
            novateInformationLine.StatusCodeId = value.StatusCodeID.Value;
            novateInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            novateInformationLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteNovateInformation")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var novateInformationLine = _context.NovateInformationLine.Include(s => s.TenderInformation).Where(p => p.NovateInformationLineId == id).FirstOrDefault();
                if (novateInformationLine != null)
                {
                    var extensionInformationLine = _context.ExtensionInformationLine.Where(p => p.ExtensionInformationLineId == id).ToList();
                    if (extensionInformationLine != null)
                    {
                        _context.ExtensionInformationLine.RemoveRange(extensionInformationLine);
                        _context.SaveChanges();
                    }
                    _context.NovateInformationLine.Remove(novateInformationLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
