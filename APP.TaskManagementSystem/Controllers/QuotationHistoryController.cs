﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APP.TaskManagementSystem.Helper;
using APP.Common;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class QuotationHistoryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public QuotationHistoryController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetQualificationHistoryItems")]
        public List<QuotationHistoryModel> Get()
        {

            //var quotationHistories = _context.QuotationHistory
            //    .Include(a => a.AddedByUser)
            //    .Include(m => m.ModifiedByUser)
            //    .Include(s => s.StatusCode)
            //    .Include(l => l.Company).OrderByDescending(a => a.QuotationHistoryId).AsNoTracking()
            //    .ToList();
            //List<QuotationHistoryModel> quotationHistoryModels = new List<QuotationHistoryModel>();
            //quotationHistories.ForEach(s =>
            //{
            //    QuotationHistoryModel quotationHistoryModel = new QuotationHistoryModel
            //    {
            //        QuotationHistoryId = s.QuotationHistoryId,
            //        CompanyId = s.CompanyId,
            //        CompanyName = s.Company != null ? s.Company.PlantCode : "",
            //        SwreferenceNo = s.SwreferenceNo,
            //        Date = s.Date,
            //        CustomerId = s.CustomerId,
            //        CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
            //        CustomerRefNo = s.CustomerRefNo,
            //        SessionId = s.SessionId,
            //        StatusCodeID = s.StatusCodeId,
            //        AddedByUserID = s.AddedByUserId,
            //        ModifiedByUserID = s.ModifiedByUserId,
            //        AddedDate = s.AddedDate,
            //        ModifiedDate = s.ModifiedDate,
            //        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
            //        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
            //        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
            //    };
            //    quotationHistoryModels.Add(quotationHistoryModel);
            //});




            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            var quotationHistories = _context.QuotationHistory
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.QuotationHistoryLine)
                .OrderByDescending(a => a.QuotationHistoryId).AsNoTracking()
                .ToList();
            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();
            var quotationHistoryIds = quotationHistories.Select(q => q.QuotationHistoryId).ToList();
            var producIds = _context.QuotationHistoryLine.Where(q => quotationHistoryIds.Contains(q.QutationHistoryId.Value)).AsNoTracking().Select(q => q.ProductId).ToList();
            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            if (producIds != null && producIds.Count > 0)
            {
                genericCodes = _context.GenericCodes.Where(g => producIds.Contains(g.ProductNameId)).AsNoTracking().ToList();
            }
            List<QuotationHistoryModel> quotationHistoryModels = new List<QuotationHistoryModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    QuotationHistoryModel quotationHistoryModel = new QuotationHistoryModel();
                    quotationHistoryModel = new QuotationHistoryModel
                    {
                        QuotationHistoryId = s.QuotationHistoryId,
                        CompanyId = s.CompanyId,
                        Company = s.Company != null ? s.Company.PlantCode : "",
                        SwreferenceNo = s.SwreferenceNo,
                        Date = s.Date,
                        CustomerId = s.CustomerId,
                        CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                        CustomerRefNo = s.CustomerRefNo,
                        SessionId = s.SessionId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                    };
                    quotationHistoryModels.Add(quotationHistoryModel);







                });
            }
            return quotationHistoryModels;
        }

        [HttpPost]
        [Route("GetQuotationHistoryBySearch")]
        public List<QuotationHistoryModel> GetQuotationHistoryBySearch(QuotationHistorySearchModel searchModel)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            var quotationHistories = _context.QuotationHistory
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.QuotationHistoryLine)
                .Include("QuotationHistoryLine.Product")
                .OrderByDescending(a => a.QuotationHistoryId).AsNoTracking()
                .ToList();
            if (searchModel.CompanyId != null && searchModel.CompanyId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CompanyId == searchModel.CompanyId).ToList();
            }
            if (searchModel.CustomerId != null && searchModel.CustomerId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CustomerId == searchModel.CustomerId).ToList();
            }

            if (searchModel.SwreferenceNo != null && searchModel.SwreferenceNo != "")
            {
                quotationHistories = quotationHistories.Where(s => s.SwreferenceNo.ToLower().Contains(searchModel.SwreferenceNo.ToLower())).ToList();
            }
            if (searchModel.FromMonth != null && searchModel.ToMonth != null)
            {
                quotationHistories = quotationHistories.Where(s => s.Date >= searchModel.FromMonth && s.Date <= searchModel.ToMonth.Value.AddMonths(1).AddDays(-1)).ToList();
            }

            if (searchModel.ProductId != null && searchModel.ProductId > 0)
            {
                if (quotationHistories != null && quotationHistories.Count > 0)
                {
                    var quotationHistoryIds = _context.QuotationHistoryLine.Where(q => q.ProductId == searchModel.ProductId).AsNoTracking().Select(s => s.QutationHistoryId).ToList();
                    if (quotationHistoryIds.Count > 0)
                    {
                        quotationHistories = quotationHistories.Where(s => quotationHistoryIds.Contains(s.QuotationHistoryId)).ToList();
                    }
                }
            }
            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();

            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            List<QuotationHistoryModel> quotationHistoryModels = new List<QuotationHistoryModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    QuotationHistoryModel quotationHistoryModel = new QuotationHistoryModel();

                    quotationHistoryModel = new QuotationHistoryModel
                    {
                        QuotationHistoryId = s.QuotationHistoryId,
                        CompanyId = s.CompanyId,
                        Company = s.Company != null ? s.Company.PlantCode : "",
                        SwreferenceNo = s.SwreferenceNo,
                        Date = s.Date,
                        CustomerId = s.CustomerId,

                        CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                        CustomerRefNo = s.CustomerRefNo,
                        SessionId = s.SessionId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                    };
                    quotationHistoryModels.Add(quotationHistoryModel);



                });
            }

            return quotationHistoryModels;
        }

        [HttpPost]
        [Route("GetQuotationHistoryReportBySearch")]
        public List<QuotationHistoryReportModel> GetQuotationHistoryReportBySearch(QuotationHistorySearchModel searchModel)
        {
            List<QuotationHistoryReportModel> quotationHistoryReportModels = new List<QuotationHistoryReportModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            var quotationHistories = _context.QuotationHistory
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.QuotationHistoryLine)
                 .Include("QuotationHistoryLine.Product")
                .OrderByDescending(a => a.QuotationHistoryId).AsNoTracking()
                .ToList();
            if (searchModel.CompanyId != null && searchModel.CompanyId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CompanyId == searchModel.CompanyId).ToList();
            }
            if (searchModel.CustomerId != null && searchModel.CustomerId > 0)
            {
                quotationHistories = quotationHistories.Where(s => s.CustomerId == searchModel.CustomerId).ToList();
            }

            if (searchModel.SwreferenceNo != null && searchModel.SwreferenceNo != "")
            {
                quotationHistories = quotationHistories.Where(s => s.SwreferenceNo.ToLower().Contains(searchModel.SwreferenceNo.ToLower())).ToList();
            }
            if (searchModel.FromMonth != null && searchModel.ToMonth != null)
            {
                quotationHistories = quotationHistories.Where(s => s.Date >= searchModel.FromMonth && s.Date <= searchModel.ToMonth.Value.AddMonths(1).AddDays(-1)).ToList();
            }


            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();

            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            List<QuotationHistoryModel> quotationHistoryModels = new List<QuotationHistoryModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    QuotationHistoryReportModel quotationHistoryReportModel = new QuotationHistoryReportModel();
                    if (!s.QuotationHistoryLine.Any())
                    {
                        quotationHistoryReportModel = new QuotationHistoryReportModel
                        {
                            QuotationHistoryId = s.QuotationHistoryId,
                            Company = s.Company != null ? s.Company.PlantCode : "",
                            SwreferenceNo = s.SwreferenceNo,
                            CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                            DateOfOffer = s.Date,
                            SessionId = s.SessionId,
                            CustomerRefNo = s.CustomerRefNo,


                        };
                        quotationHistoryReportModels.Add(quotationHistoryReportModel);
                    }
                    else if (s.QuotationHistoryLine.ToList().Count > 0)
                    {
                        s.QuotationHistoryLine.ToList().ForEach(l =>
                        {
                            quotationHistoryReportModel = new QuotationHistoryReportModel
                            {
                                QuotationHistoryId = s.QuotationHistoryId,
                                Company = s.Company != null ? s.Company.PlantCode : "",
                                SwreferenceNo = s.SwreferenceNo,
                                CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                                QuotationHistoryLineId = l.QuotationHistoryLineId,
                                ProductName = l.Product?.Code,
                                Qty = l.Quantity,
                                UOM = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == (l.Product?.Uom))?.Value,
                                ProductId = l.ProductId,
                                OfferCurrency = masterDetailList != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == l.OfferCurrencyId)?.Value : "",
                                OfferPrice = l.OfferPrice,
                                DateOfOffer = s.Date,
                                IsAwarded = l.IsAwarded,
                                SessionId = s.SessionId,
                                CustomerRefNo = s.CustomerRefNo,

                            };
                            quotationHistoryReportModels.Add(quotationHistoryReportModel);
                        });
                    }


                });
            }
            if (searchModel.ProductId != null && searchModel.ProductId > 0)
            {
                if (quotationHistoryReportModels != null && quotationHistoryReportModels.Count > 0)
                {
                    //var quotationHistoryIds = _context.QuotationHistoryLine.Where(q => q.ProductId == searchModel.ProductId).Select(s => s.QutationHistoryId).ToList();
                    quotationHistoryReportModels = quotationHistoryReportModels.Where(s => s.ProductId == searchModel.ProductId).ToList();
                }
            }
            return quotationHistoryReportModels;
        }
        [HttpPost()]
        [Route("GetQuotationHistoryData")]
        public ActionResult<QuotationHistoryModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var quotationHistory = new QuotationHistory();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).LastOrDefault();
                        break;
                    case "Next":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).LastOrDefault();
                        break;
                    case "Previous":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).LastOrDefault();
                        break;
                    case "Next":
                        quotationHistory = _context.QuotationHistory.OrderBy(o => o.QuotationHistoryId).FirstOrDefault(s => s.QuotationHistoryId > searchModel.Id);
                        break;
                    case "Previous":
                        quotationHistory = _context.QuotationHistory.OrderByDescending(o => o.QuotationHistoryId).FirstOrDefault(s => s.QuotationHistoryId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<QuotationHistoryModel>(quotationHistory);
            return result;
        }

        [HttpPost]
        [Route("InsertQuotationHistory")]
        public QuotationHistoryModel Post(QuotationHistoryModel value)
        {
            var SessionId = Guid.NewGuid();
            var quotationHistory = new QuotationHistory
            {
                CompanyId = value.CompanyId,
                SwreferenceNo = value.SwreferenceNo,
                Date = value.Date,
                CustomerId = value.CustomerId,
                CustomerRefNo = value.CustomerRefNo,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.QuotationHistory.Add(quotationHistory);
            _context.SaveChanges();
            value.SessionId = SessionId;
            if (value.CompanyId > 0)
            {
                value.Company = _context.Plant.Where(p => p.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();

            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.FirstOrDefault(s => s.UserId == value.AddedByUserID)?.UserName;
            }
            value.QuotationHistoryId = quotationHistory.QuotationHistoryId;
            return value;
        }
        [HttpPut]
        [Route("UpdateQuotationHistory")]
        public QuotationHistoryModel Put(QuotationHistoryModel value)
        {
            if (value.SessionId == null)
            {
                var SessionId = Guid.NewGuid();
                value.SessionId = SessionId;
            }
            var quotationHistory = _context.QuotationHistory.SingleOrDefault(p => p.QuotationHistoryId == value.QuotationHistoryId);
            quotationHistory.CompanyId = value.CompanyId;
            quotationHistory.SwreferenceNo = value.SwreferenceNo;
            quotationHistory.Date = value.Date;
            quotationHistory.CustomerId = value.CustomerId;
            quotationHistory.CustomerRefNo = value.CustomerRefNo;
            quotationHistory.SessionId = value.SessionId;
            quotationHistory.StatusCodeId = value.StatusCodeID.Value;
            quotationHistory.ModifiedByUserId = value.ModifiedByUserID;
            quotationHistory.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (value.CompanyId > 0)
            {
                value.Company = _context.Plant.Where(p => p.PlantId == value.CompanyId).Select(s => s.PlantCode).FirstOrDefault();

            }
            if (value.CustomerId > 0)
            {
                value.CustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.CustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.FirstOrDefault(s => s.UserId == value.AddedByUserID)?.UserName;
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteQuotationHistory")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var quotationHistory = _context.QuotationHistory.Where(p => p.QuotationHistoryId == id).FirstOrDefault();
                if (quotationHistory != null)
                {
                    var quotationHistoryLines = _context.QuotationHistoryLine.Where(p => p.QutationHistoryId == id).AsNoTracking().ToList();
                    if (quotationHistoryLines != null)
                    {
                        quotationHistoryLines.ForEach(h =>
                        {
                            var query = string.Format("delete from QuotationHistoryDocument Where SessionId='{0}'", h.SessionId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            if (rowaffected <= 0)
                            {
                                throw new Exception("Failed to delete document");
                            }
          
                        });
                        _context.QuotationHistoryLine.RemoveRange(quotationHistoryLines);
                        _context.SaveChanges();
                    }
                    _context.QuotationHistory.Remove(quotationHistory);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("GetQuotationHistoryLine")]
        public List<QuotationHistoryLineModel> GetQuotationHistoryLine(int? id)
        {
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var quotationHistoryLines = _context.QuotationHistoryLine.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(l => l.Product).Where(w => w.QutationHistoryId == id).OrderByDescending(a => a.QuotationHistoryLineId).AsNoTracking().ToList();
            var productGroupings = _context.GenericCodes.AsNoTracking().ToList();
            List<QuotationHistoryLineModel> quotationHistoryLineModels = new List<QuotationHistoryLineModel>();
            if (quotationHistoryLines != null)
            {
                quotationHistoryLines.ForEach(s =>
                {
                    QuotationHistoryLineModel quotationHistoryLineModel = new QuotationHistoryLineModel
                    {
                        QuotationHistoryLineId = s.QuotationHistoryLineId,
                        QutationHistoryId = s.QutationHistoryId,
                        ProductId = s.ProductId,

                        ProductName = s.Product?.Code,
                        UOM = applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Product?.Uom)?.Value,
                        Productdescription = s.Product?.Description,
                        Source = s.Source,
                        Quantity = s.Quantity,
                        Uomid = s.Uomid,
                        PackingId = s.PackingId,
                        Packing = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PackingId)?.Value : "",
                        OfferCurrencyId = s.OfferCurrencyId,
                        OfferCurrency = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OfferCurrencyId)?.Value : "",
                        OfferPrice = s.OfferPrice,
                        OfferUomid = s.OfferUomid,
                        OfferUOM = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.OfferUomid)?.Value : "",
                        Focqty = s.Focqty,
                        ShippingTermsId = s.ShippingTermsId,
                        ShippingTerms = applicationmasterdetail != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ShippingTermsId)?.Value : "",
                        IsTenderExceed = s.IsTenderExceed,
                        IsAwarded = s.IsAwarded,
                        SessionId = s.SessionId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        Remarks = s.Remarks,
                    };
                    quotationHistoryLineModels.Add(quotationHistoryLineModel);
                });
            }
            return quotationHistoryLineModels;
        }
        [HttpPost]
        [Route("GetQuotationAwardReference")]
        public QuotationAwardReferenceModel GetQuotationAwardReference(QuotationHistorySearchModel quotationHistorySearchModel)
        {
            SellingPriceInformation sellingPriceInformation = new SellingPriceInformation();
            QuotationAwardReferenceModel quotationAwardReferenceModel = new QuotationAwardReferenceModel();
            var genericcode = _context.GenericCodes.Where(s => s.ProductNameId == quotationHistorySearchModel.ProductId && s.SupplyToId == quotationHistorySearchModel.CustomerId).FirstOrDefault();
            var sellingCatelogueQty = _context.SellingPriceInformation.Where(s => s.ProductId == quotationHistorySearchModel.ProductId).ToList();
            if (quotationHistorySearchModel.Qty != null)
            {
                sellingPriceInformation = _context.SellingPriceInformation.Where(s => s.ProductId == quotationHistorySearchModel.ProductId && s.MinQty < quotationAwardReferenceModel.MinQty).OrderByDescending(s => s.MinQty).FirstOrDefault();
            }
            else
            {
                sellingPriceInformation = _context.SellingPriceInformation.Where(s => s.ProductId == quotationHistorySearchModel.ProductId).OrderByDescending(s => s.MinQty).FirstOrDefault();
            }
            if (sellingPriceInformation != null)
            {
                quotationAwardReferenceModel = new QuotationAwardReferenceModel();
                quotationAwardReferenceModel.ProductGroupCode = genericcode?.Code;
                quotationAwardReferenceModel.MinQty = sellingPriceInformation.MinQty;
                quotationAwardReferenceModel.PricePerUnit = sellingPriceInformation.MinPrice;

            }
            return quotationAwardReferenceModel;
        }
        [HttpGet]
        [Route("GetQuotationHistoryLineProducts")]
        public List<GenericCodesModel> GetQuotationHistoryLineProducts(int? id)
        {
            List<GenericCodesModel> genericCodesModels = new List<GenericCodesModel>();
            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            applicationmasterdetail = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();
            var productIds = _context.QuotationHistoryLine.Where(s => s.QutationHistoryId == id).Select(s => s.ProductId).ToList();
            if (productIds != null && productIds.Count > 0)
            {
                var genericCodesProducts = _context.GenericCodes.Where(a => productIds.Contains(a.GenericCodeId)).ToList();
                if (genericCodesProducts != null && genericCodesProducts.Count > 0)
                {
                    genericCodesProducts.ForEach(a =>
                    {


                        var genericCodesModel = new GenericCodesModel
                        {                            
                            Code = a.Code,
                            Description2 = a.Description2,
                            GenericCodeID = a.GenericCodeId,
                            Description = a.Description,
                            Uom = a.Uom,
                            UomName = applicationmasterdetail.FirstOrDefault(t => t.ApplicationMasterDetailId == a.Uom)?.Value,
                        };
                        genericCodesModels.Add(genericCodesModel);
                    });
                }
            }
            if(genericCodesModels!=null && genericCodesModels.Count>0)
            {
                genericCodesModels.ForEach(g =>
                {
                    g.Name = g.Code +" | "+ g.Description2;

                });
            }
            return genericCodesModels;
        }


        [HttpGet("GetApplicationMasterDetailById/{id:int}")]
        public List<ApplicationMasterDetailModel> GetApplicationMasterDetailById(int? id)
        {
            var applicationmaster = _context.ApplicationMasterDetail.Include("ApplicationMaster").Select(s => new ApplicationMasterDetailModel
            {
                ApplicationMasterDetailId = s.ApplicationMasterDetailId,
                ApplicationMasterId = s.ApplicationMasterId,
                Description = s.Description,
                ApplicationMaster = s.ApplicationMaster.ApplicationMasterName,
                Value = s.Value
            }).Where(s => s.ApplicationMasterDetailId == id).OrderByDescending(o => o.ApplicationMasterDetailId).AsNoTracking().ToList();
            return applicationmaster;
        }

        [HttpPost()]
        [Route("GetQuotationHistoryLineData")]
        public ActionResult<QuotationHistoryLineModel> GetQuotationHistoryLineData(SearchModel searchModel)
        {
            var quotationHistoryLine = new QuotationHistoryLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).LastOrDefault();
                        break;
                    case "Next":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).LastOrDefault();
                        break;
                    case "Previous":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).FirstOrDefault();
                        break;
                    case "Last":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).LastOrDefault();
                        break;
                    case "Next":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderBy(o => o.QuotationHistoryLineId).FirstOrDefault(s => s.QuotationHistoryLineId > searchModel.Id);
                        break;
                    case "Previous":
                        quotationHistoryLine = _context.QuotationHistoryLine.OrderByDescending(o => o.QuotationHistoryLineId).FirstOrDefault(s => s.QuotationHistoryLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<QuotationHistoryLineModel>(quotationHistoryLine);
            return result;
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertQuotationHistoryLine")]
        public QuotationHistoryLineModel InsertQuotationHistoryLine(QuotationHistoryLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var quotationHistoryLine = new QuotationHistoryLine
            {
                QutationHistoryId = value.QutationHistoryId,
                ProductId = value.ProductId,

                Source = value.Source,
                Quantity = value.Quantity,
                Uomid = value.Uomid,
                PackingId = value.PackingId,
                OfferCurrencyId = value.OfferCurrencyId,
                OfferPrice = value.OfferPrice,
                OfferUomid = value.OfferUomid,
                Focqty = value.Focqty,
                ShippingTermsId = value.ShippingTermsId,
                IsTenderExceed = value.IsTenderExceed,
                SessionId = SessionId,
                IsAwarded = value.IsAwarded,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Remarks = value.Remarks,

            };
            _context.QuotationHistoryLine.Add(quotationHistoryLine);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.QuotationHistoryLineId = quotationHistoryLine.QuotationHistoryLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateQuotationHistoryLine")]
        public QuotationHistoryLineModel UpdateQuotationHistoryLine(QuotationHistoryLineModel value)
        {
            var quotationHistoryLine = _context.QuotationHistoryLine.SingleOrDefault(p => p.QuotationHistoryLineId == value.QuotationHistoryLineId);
            quotationHistoryLine.ProductId = value.ProductId;
            quotationHistoryLine.Source = value.Source;
            quotationHistoryLine.Quantity = value.Quantity;
            quotationHistoryLine.Uomid = value.Uomid;
            quotationHistoryLine.PackingId = value.PackingId;
            quotationHistoryLine.OfferCurrencyId = value.OfferCurrencyId;
            quotationHistoryLine.OfferPrice = value.OfferPrice;
            quotationHistoryLine.OfferUomid = value.OfferUomid;
            quotationHistoryLine.Focqty = value.Focqty;
            quotationHistoryLine.ShippingTermsId = value.ShippingTermsId;
            quotationHistoryLine.IsTenderExceed = value.IsTenderExceed;
            quotationHistoryLine.SessionId = value.SessionId;
            quotationHistoryLine.IsAwarded = value.IsAwarded;
            quotationHistoryLine.ModifiedByUserId = value.ModifiedByUserID;
            quotationHistoryLine.ModifiedDate = DateTime.Now;
            quotationHistoryLine.StatusCodeId = value.StatusCodeID.Value;
            quotationHistoryLine.Remarks = value.Remarks;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteQuotationHistoryLine")]
        public void DeleteQuotationHistoryLine(int id)
        {
            var quotationHistoryLine = _context.QuotationHistoryLine.SingleOrDefault(p => p.QuotationHistoryLineId == id);
            if (quotationHistoryLine != null)
            {
                // var quotationHistoryDocuments = _context.QuotationHistoryDocument.Where(p => p.SessionId == quotationHistoryLine.SessionId).AsNoTracking().ToList();
                // if (quotationHistoryDocuments != null)
                // {
                //     _context.QuotationHistoryDocument.RemoveRange(quotationHistoryDocuments);
                //     _context.SaveChanges();
                // }
                _context.QuotationHistoryLine.Remove(quotationHistoryLine);
                _context.SaveChanges();
            }
        }
        [HttpPost]
        [Route("UploadQuotationHistoryDocuments")]
        public IActionResult UploadQuotationHistoryDocuments(IFormCollection files, Guid SessionId)
        {
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new QuotationHistoryDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = SessionId,
                };
                _context.QuotationHistoryDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(SessionId.ToString());

        }
        [HttpGet]
        [Route("GetQuotationHistoryDocument")]
        public List<QuotationHistoryDocumentModel> GetQuotationHistoryDocument(int? id)
        {
            var quotationHistoryLine = _context.QuotationHistory.Where(w => w.QuotationHistoryId == id).Select(s => s.SessionId).FirstOrDefault();
            var query = _context.QuotationHistoryDocument.Select(s => new QuotationHistoryDocumentModel
            {
                SessionId = s.SessionId,
                QuotationHistoryDocumentId = s.QuotationHistoryDocumentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == quotationHistoryLine).OrderByDescending(o => o.QuotationHistoryDocumentId).AsNoTracking().ToList();
            return query;
        }
        [HttpGet]
        [Route("DownLoadQuotationHistoryDocument")]
        public IActionResult DownLoadQuotationHistoryDocument(long id)
        {
            var document = _context.QuotationHistoryDocument.SingleOrDefault(t => t.QuotationHistoryDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteQuotationHistoryDocument")]
        public void DeleteQuotationHistoryDocument(int id)
        {
            var query = string.Format("delete from QuotationHistoryDocument Where QuotationHistoryDocumentId='{0}'", id);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }



        [HttpPost]
        [Route("UploadQuotationHistoryAttachments")]
        public IActionResult UploadQuotationHistoryAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            //sessionID = Guid.Parse( files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var screenId = files["screenID"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710, Title = profile.Name });
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new QuotationHistoryDocument
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId

                };

                _context.QuotationHistoryDocument.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }

        [HttpGet]
        [Route("GetQuotationHistoryAttachmentBySessionID")]
        public List<DocumentsModel> GetQuotationHistoryAttachmentBySessionID(Guid? sessionId, int? userId)
        {
            var query = _context.QuotationHistoryDocument.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.QuotationHistoryDocumentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();

            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });

            return query;
        }
        [HttpGet]
        [Route("DownLoadQuotationHistoryAttachment")]
        public IActionResult DownLoadQuotationHistoryAttachment(long id)
        {
            var document = _context.QuotationHistoryDocument.SingleOrDefault(t => t.QuotationHistoryDocumentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteQuotationHistoryAttachment")]
        public void DeleteQuotationHistoryAttachment(int id)
        {
            var query = string.Format("delete from QuotationHistoryDocument Where QuotationHistoryDocumentId ="  + "{0}" ,id);

            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }

        [HttpGet]
        [Route("GetQuotationHistoryReports")]
        public List<QuotationHistoryReportModel> GetQuotationHistoryReports()
        {

            List<long?> applicationMasterCodeIds = new List<long?> { 103, 229, 234, 235, 236 };
            var applicationMasterIds = _context.ApplicationMaster.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterCodeId)).AsNoTracking().Select(s => s.ApplicationMasterId).ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            masterDetailList = _context.ApplicationMasterDetail.Where(f => applicationMasterIds.Contains(f.ApplicationMasterId)).AsNoTracking().ToList();

            List<CompanyListing> companyListing = new List<CompanyListing>();
            List<GenericCodes> genericCodes = new List<GenericCodes>();

            //var genericCodes = _context.GenericCodes.AsNoTracking().ToList();
            //var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            var quotationHistories = _context.QuotationHistory
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(q => q.QuotationHistoryLine)
                .Include("QuotationHistoryLine.Product")
                .OrderByDescending(a => a.QuotationHistoryId).AsNoTracking()
                .ToList();
            var customerIds = quotationHistories?.Select(q => q.CustomerId).ToList();
            var quotationHistoryIds = quotationHistories.Select(q => q.QuotationHistoryId).ToList();
            var producIds = _context.QuotationHistoryLine.Where(q => quotationHistoryIds.Contains(q.QutationHistoryId.Value)).AsNoTracking().Select(q => q.ProductId).ToList();
            if (customerIds != null && customerIds.Count > 0)
            {
                companyListing = _context.CompanyListing.Where(c => customerIds.Contains(c.CompanyListingId)).AsNoTracking().ToList();
            }
            if (producIds != null && producIds.Count > 0)
            {
                genericCodes = _context.GenericCodes.Where(g => producIds.Contains(g.ProductNameId)).AsNoTracking().ToList();
            }
            List<QuotationHistoryReportModel> quotationHistoryReportModels = new List<QuotationHistoryReportModel>();
            if (quotationHistories != null && quotationHistories.Count > 0)
            {
                quotationHistories.ForEach(s =>
                {
                    QuotationHistoryReportModel quotationHistoryReportModel = new QuotationHistoryReportModel();
                    if (!s.QuotationHistoryLine.Any())
                    {
                        quotationHistoryReportModel = new QuotationHistoryReportModel
                        {
                            QuotationHistoryId = s.QuotationHistoryId,
                            Company = s.Company != null ? s.Company.PlantCode : "",
                            SwreferenceNo = s.SwreferenceNo,
                            CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                            DateOfOffer = s.Date,
                            SessionId = s.SessionId,
                            CustomerRefNo = s.CustomerRefNo,

                        };
                        quotationHistoryReportModels.Add(quotationHistoryReportModel);
                    }
                    else if (s.QuotationHistoryLine.Any())
                    {
                        s.QuotationHistoryLine.ToList().ForEach(l =>
                        {
                            quotationHistoryReportModel = new QuotationHistoryReportModel
                            {
                                QuotationHistoryId = s.QuotationHistoryId,
                                Company = s.Company != null ? s.Company.PlantCode : "",
                                SwreferenceNo = s.SwreferenceNo,
                                CustomerName = companyListing != null ? companyListing.FirstOrDefault(a => a.CompanyListingId == s.CustomerId)?.CompanyName : "",
                                QuotationHistoryLineId = l.QuotationHistoryLineId,
                                ProductName = l.Product?.Code,
                                Qty = l.Quantity,
                                UOM = masterDetailList.FirstOrDefault(s => s.ApplicationMasterDetailId == l.Product?.Uom)?.Value,

                                OfferCurrency = masterDetailList != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == l.OfferCurrencyId)?.Value : "",
                                OfferPrice = l.OfferPrice,
                                DateOfOffer = s.Date,
                                IsAwarded = l.IsAwarded,
                                SessionId = s.SessionId,
                                CustomerRefNo = s.CustomerRefNo,


                            };
                            quotationHistoryReportModels.Add(quotationHistoryReportModel);
                        });
                    }


                });
            }
            return quotationHistoryReportModels;
        }

    }
}