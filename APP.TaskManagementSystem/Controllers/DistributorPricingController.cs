﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DistributorPricingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DistributorPricingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetDistributorPricings")]
        public List<DistributorPricingModel> Get()
        {
            var distributorPricings = _context.DistributorPricing.Include(i => i.AddedByUser).Include(c => c.Customer).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).AsNoTracking().OrderByDescending(o => o.DistributorPricingId).AsNoTracking().ToList();
            var applicationMasterDetails = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<DistributorPricingModel> distributorPricingModels = new List<DistributorPricingModel>();
            if (distributorPricings != null && distributorPricings.Count > 0)
            {
                distributorPricings.ForEach(s =>
                {
                    DistributorPricingModel distributorPricingModel = new DistributorPricingModel();
                    distributorPricingModel.DistributorPricingID = s.DistributorPricingId;
                    distributorPricingModel.BusinessCategoryID = s.BusinessCategoryId;
                    var businessCategory = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == s.BusinessCategoryId);
                    distributorPricingModel.BusinessCategoryID = s.BusinessCategoryId;
                    distributorPricingModel.BusinessCategory = businessCategory?.Value;
                    distributorPricingModel.CustomerID = s.CustomerId;
                    distributorPricingModel.Customer = s.Customer?.CompanyName;
                    distributorPricingModel.MarginInformation = "";
                    distributorPricingModel.GenericName = "";
                    distributorPricingModel.NavNo = "";
                    distributorPricingModel.Description = "";
                    distributorPricingModel.Description2 = "";
                    distributorPricingModel.UOM = "";
                    distributorPricingModel.Currency = "";
                    distributorPricingModel.DistributorPrice = s.DistributorPrice;
                    distributorPricingModel.AddedByUserID = s.AddedByUserId;
                    distributorPricingModel.StatusCodeID = s.StatusCodeId;
                    distributorPricingModel.AddedDate = s.AddedDate;
                    distributorPricingModel.ModifiedByUserID = s.ModifiedByUserId;
                    distributorPricingModel.ModifiedDate = s.ModifiedDate;
                    distributorPricingModel.AddedByUser = s.AddedByUser?.UserName;
                    distributorPricingModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    distributorPricingModel.StatusCode = s.StatusCode?.CodeValue;
                    distributorPricingModels.Add(distributorPricingModel);
                });
            }

            return distributorPricingModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DistributorPricingModel> GetData(SearchModel searchModel)
        {
            var distributorPricing = new DistributorPricing();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).LastOrDefault();
                        break;
                    case "Next":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).LastOrDefault();
                        break;
                    case "Previous":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).LastOrDefault();
                        break;
                    case "Next":
                        distributorPricing = _context.DistributorPricing.OrderBy(o => o.DistributorPricingId).FirstOrDefault(s => s.DistributorPricingId > searchModel.Id);
                        break;
                    case "Previous":
                        distributorPricing = _context.DistributorPricing.OrderByDescending(o => o.DistributorPricingId).FirstOrDefault(s => s.DistributorPricingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DistributorPricingModel>(distributorPricing);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDistributorPricing")]
        public DistributorPricingModel Post(DistributorPricingModel value)
        {
            var distributorPricing = new DistributorPricing
            {
                BusinessCategoryId = value.BusinessCategoryID,
                CustomerId = value.CustomerID,
                DistributorPrice = value.DistributorPrice,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.DistributorPricing.Add(distributorPricing);
            _context.SaveChanges();
            value.DistributorPricingID = distributorPricing.DistributorPricingId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDistributorPricing")]
        public DistributorPricingModel Put(DistributorPricingModel value)
        {
            var distributorPricing = _context.DistributorPricing.SingleOrDefault(p => p.DistributorPricingId == value.DistributorPricingID);
            if (distributorPricing != null)
            {
                distributorPricing.BusinessCategoryId = value.BusinessCategoryID;
                distributorPricing.CustomerId = value.CustomerID;
                distributorPricing.DistributorPrice = value.DistributorPrice;
                distributorPricing.CustomerId = value.CustomerID;
                distributorPricing.ModifiedByUserId = value.ModifiedByUserID;
                distributorPricing.ModifiedDate = DateTime.Now;
                distributorPricing.StatusCodeId = value.StatusCodeID.Value;

                _context.SaveChanges();
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDistributorPricing")]
        public void Delete(int id)
        {
            var distributorPricing = _context.DistributorPricing.SingleOrDefault(p => p.DistributorPricingId == id);
            if (distributorPricing != null)
            {
                _context.DistributorPricing.Remove(distributorPricing);
                _context.SaveChanges();
            }
        }
    }
}
