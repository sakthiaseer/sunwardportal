﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IPIRMobileActionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _config;

        public IPIRMobileActionController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment hostingEnvironment, GenerateDocumentNoSeries generateDocumentNoSeries, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _config = config;
        }

        [HttpGet]
        [Route("GetIPIRMobileActionItems")]
        public List<IPIRMobileActionModel> GetIPIRMobileActionItems()
        {
            List<IPIRMobileActionModel> IPIRMobileActionModels = new List<IPIRMobileActionModel>();
            var IPIRMobileActionItems = _context.IpirmobileAction
                                    .Include(s => s.IntendedAction)
                                    .Include(s => s.StatusCode)
                                    .Include(s => s.AddedByUser)
                                    .Include(s => s.ModifiedByUser)
                                    .Include(s => s.AssignmentToNavigation)
                                    .Include(s => s.AssignmentStatus)
                                    .OrderByDescending(o => o.IpirmobileActionId).AsNoTracking().ToList();
            IPIRMobileActionItems.ForEach(s =>
            {
                IPIRMobileActionModel IPIRMobileActionModel = new IPIRMobileActionModel
                {
                    IpirmobileActionId = s.IpirmobileActionId,
                    IpirmobileId = s.IpirmobileId,
                    IntendedActionId = s.IntendedActionId,
                    IsConfirmIpir = s.IsConfirmIpir,
                    IntendedAction = s.IntendedAction?.CodeValue,
                    AssignmentTo = s.AssignmentTo,
                    AssignmentStatusId = s.AssignmentStatusId,
                    AssignmentStatus = s.AssignmentStatus?.CodeValue,
                    AssignmentToName = s.AssignmentToNavigation?.UserName,
                    IssueDescription = s.IssueDescription,
                    Photo = s.Photo,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,
                };
                IPIRMobileActionModels.Add(IPIRMobileActionModel);

            });
            return new List<IPIRMobileActionModel>();
        }


        [HttpPost]
        [Route("GetIPIRMobileAction")]
        public IPIRMobilePost GetIPIRMobileAction(IPIRMobilePost value)
        {
            try
            {
                var ipirmobileActionItems = _context.IpirmobileAction
                                                .Include(i => i.IpirissueRelatedMultiple)
                                                .Include(i => i.IpirissueTitleMultiple)
                                                .Where(i => i.IpirmobileId == value.IpirmobileId).AsNoTracking().ToList();

                if (ipirmobileActionItems.Any())
                {
                    ipirmobileActionItems.ForEach(v =>
                    {
                        var IPIRMobileAction = new IPIRMobileActionModel
                        {
                            IpirmobileActionId = v.IpirmobileActionId,
                            IpirmobileId = v.IpirmobileId,
                            IntendedActionId = v.IntendedActionId,
                            IsConfirmIpir = v.IsConfirmIpir,
                            AssignmentTo = v.AssignmentTo,
                            AssignmentStatusId = v.AssignmentStatusId,
                            IssueDescription = v.IssueDescription,
                            Photo = v.Photo,
                            AddedDate = v.AddedDate,
                            StatusCodeID = v.StatusCodeId,
                            SessionId = v.SessionId,
                        };
                        value.MobileActionItems.Add(IPIRMobileAction);
                        if (v.IpirissueRelatedMultiple.Count > 0)
                        {
                            v.IpirissueRelatedMultiple.ToList().ForEach(i =>
                            {
                                IPIRMobileAction.IssueRelatedIds.Add(i.IssueRelatedId);
                            });
                        }
                        if (v.IpirissueTitleMultiple.Count > 0)
                        {
                            v.IpirissueTitleMultiple.ToList().ForEach(i =>
                            {
                                IPIRMobileAction.IssueTitleIds.Add(i.IssueTitleId);
                            });
                        }
                    });
                }

                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }


        // POST: api/User
        [HttpPost]
        [Route("InsertIPIRMobileAction")]
        public IPIRMobilePost Post(IPIRMobilePost value)
        {
            try
            {
                var ipirmobile = _context.Ipirmobile.Include(c => c.IpirmobileAction)
                                .Include("IpirmobileAction.IpirissueRelatedMultiple")
                                .Include("IpirmobileAction.IpirissueTitleMultiple")
                                .FirstOrDefault(i => i.IpirmobileId == value.IpirmobileId);
                ipirmobile.IntendedActionId = value.IntendedActionId;
                if (ipirmobile.IpirmobileAction.Any())
                {
                    ipirmobile.IpirmobileAction.ToList().ForEach(a =>
                    {
                        _context.IpirissueRelatedMultiple.RemoveRange(a.IpirissueRelatedMultiple);
                        _context.IpirissueTitleMultiple.RemoveRange(a.IpirissueTitleMultiple);
                    });
                    _context.IpirmobileAction.RemoveRange(ipirmobile.IpirmobileAction);
                    _context.SaveChanges();
                }
                var sessionId = Guid.NewGuid();
                if (value.MobileActionItems.Any())
                {
                    value.MobileActionItems.ForEach(v =>
                    {
                        var IPIRMobileAction = new IpirmobileAction
                        {
                            IpirmobileActionId = v.IpirmobileActionId,
                            IpirmobileId = v.IpirmobileId,
                            IntendedActionId = v.IntendedActionId,
                            IsConfirmIpir = v.IsConfirmIpir,
                            AssignmentTo = v.AssignmentTo,
                            AssignmentStatusId = v.AssignmentStatusId,
                            IssueDescription = v.IssueDescription,
                            Photo = v.Photo,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,
                            SessionId = sessionId,
                        };
                        _context.IpirmobileAction.Add(IPIRMobileAction);

                        _context.SaveChanges();
                        v.IpirmobileActionId = IPIRMobileAction.IpirmobileActionId;
                        if (v.IssueRelatedIds != null && v.IssueRelatedIds.Count > 0)
                        {
                            v.IssueRelatedIds.ForEach(i =>
                            {
                                var issueRelated = new IpirissueRelatedMultiple
                                {
                                    IpirissueMobileActionId = v.IpirmobileActionId,
                                    IssueRelatedId = i,

                                };
                                _context.IpirissueRelatedMultiple.Add(issueRelated);
                                _context.SaveChanges();
                            });
                        }

                        if (v.IssueTitleIds != null && v.IssueTitleIds.Count > 0)
                        {
                            v.IssueTitleIds.ForEach(i =>
                            {
                                var ipirissueTitleMultiple = new IpirissueTitleMultiple
                                {
                                    IpirissueMobileActionId = v.IpirmobileActionId,
                                    IssueTitleId = i,

                                };
                                _context.IpirissueTitleMultiple.Add(ipirissueTitleMultiple);
                                _context.SaveChanges();
                            });
                        }
                    });
                }

                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateIPIRMobileAction")]
        public IPIRMobileActionModel Put(IPIRMobileActionModel value)
        {
            var ipirItem = _context.IpirmobileAction.FirstOrDefault(f => f.IpirmobileActionId == value.IpirmobileActionId);
            ipirItem.IntendedActionId = value.IntendedActionId;

            ipirItem.IpirmobileId = value.IpirmobileId;
            ipirItem.IntendedActionId = value.IntendedActionId;
            ipirItem.IsConfirmIpir = value.IsConfirmIpir;
            ipirItem.AssignmentTo = value.AssignmentTo;
            ipirItem.AssignmentStatusId = value.AssignmentStatusId;
            ipirItem.IssueDescription = value.IssueDescription;
            ipirItem.Photo = value.Photo;
            ipirItem.IssueDescription = value.IssueDescription;
            ipirItem.Photo = value.Photo;
            ipirItem.ModifiedDate = DateTime.Now;
            ipirItem.StatusCodeId = value.StatusCodeID.Value;
            ipirItem.SessionId = value.SessionId ?? Guid.NewGuid();
            var ipirissueRelatedMultiple = _context.IpirissueRelatedMultiple.Where(p => p.IpirissueMobileActionId == value.IpirmobileActionId).ToList();
            if (ipirissueRelatedMultiple != null)
            {
                _context.IpirissueRelatedMultiple.RemoveRange(ipirissueRelatedMultiple);
                _context.SaveChanges();
            }
            if (value.IssueRelatedIds.Count > 0)
            {
                value.IssueRelatedIds.ForEach(i =>
                {


                    var issueRelated = new IpirissueRelatedMultiple
                    {
                        IpirissueMobileActionId = value.IpirmobileActionId,
                        IssueRelatedId = i,

                    };
                    _context.IpirissueRelatedMultiple.Add(issueRelated);
                    _context.SaveChanges();
                });
            }

            var ipirissueTitleMultiple = _context.IpirissueTitleMultiple.Where(p => p.IpirissueMobileActionId == value.IpirmobileActionId).ToList();
            if (ipirissueTitleMultiple != null)
            {
                _context.IpirissueTitleMultiple.RemoveRange(ipirissueTitleMultiple);
                _context.SaveChanges();
            }
            if (value.IssueTitleIds.Count > 0)
            {
                value.IssueTitleIds.ForEach(i =>
                {


                    var ipirissueTitleMultiple = new IpirissueTitleMultiple
                    {
                        IpirissueMobileActionId = value.IpirmobileActionId,
                        IssueTitleId = i,

                    };
                    _context.IpirissueTitleMultiple.Add(ipirissueTitleMultiple);
                    _context.SaveChanges();
                });
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteIPIRMobileAction")]
        public void Delete(int id)
        {
            try
            {
                var IPIRMobileAction = _context.IpirmobileAction.SingleOrDefault(p => p.IpirmobileActionId == id);
                if (IPIRMobileAction != null)
                {
                    _context.IpirmobileAction.Remove(IPIRMobileAction);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("IPIR Mobile Action Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
