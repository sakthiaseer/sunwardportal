﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;
using APP.TaskManagementSystem.Helper;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavItemController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavItemController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavItems")]
        public List<NavItemModel> Get()
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navitems = _context.Navitems.Include(n => n.LastSyncByNavigation).Include("AddedByUser")
                .Include("GenericCode").Include("NavMethodCodeLines.MethodCode").Include("ModifiedByUser")
                .Include("CompanyNavigation").Include(c => c.StatusCode).OrderByDescending(o => o.Description).Where(i => (i.No.StartsWith("FP-") || i.No.StartsWith("SF-")) && i.StatusCodeId == 1 && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description,
                        RelatedItemNo = s.RelatedItemNo,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        Inventory = s.Inventory,
                        InternalRef = s.InternalRef,
                        ItemRegistration = s.ItemRegistration,
                        ExpirationCalculation = s.ExpirationCalculation,
                        BatchNos = s.BatchNos,
                        ProductionRecipeNo = s.ProductionRecipeNo,
                        Qcenabled = s.Qcenabled,
                        SafetyLeadTime = s.SafetyLeadTime,
                        ProductionBomno = s.ProductionBomno,
                        RoutingNo = s.RoutingNo,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        UnitCost = s.UnitCost,
                        UnitPrice = s.UnitPrice,
                        VendorNo = s.VendorNo,
                        VendorItemNo = s.VendorItemNo,
                        ItemCategoryCode = s.ItemCategoryCode,
                        ItemTrackingCode = s.ItemTrackingCode,
                        Qclocation = s.Qclocation,
                        Company = s.Company,
                        LastSyncDate = s.LastSyncDate,
                        LastSyncBy = s.LastSyncBy,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        NavItemCustomerItemID = s.NavItemCitemList != null ? s.NavItemCitemList.Select(t => t.NavItemCustomerItemId.Value).ToList() : new List<long>(),
                        CategoryID = s.CategoryId,
                        Steroid = s.Steroid,
                        SteroidText = s.Steroid.HasValue ? s.Steroid.Value == true ? "Steroid" : "Non-steroid" : string.Empty,
                        ShelfLife = s.ShelfLife,
                        Quota = s.Quota,
                        PackSize = s.PackSize,
                        PackUom = s.PackUom,
                        GenericCodeId = s.GenericCodeId,
                        GenericCode = s.GenericCode != null ? s.GenericCode.Code : string.Empty,
                        MethodCode = s.NavMethodCodeLines.FirstOrDefault() != null ? s.NavMethodCodeLines.FirstOrDefault().MethodCode.MethodName : "",
                        MethodCodeId = s.NavMethodCodeLines.FirstOrDefault() != null ? s.NavMethodCodeLines.FirstOrDefault().MethodCode.MethodCodeId : -1,
                        ItemGroup = s.ItemCategoryCode,// s.CategoryId.GetValueOrDefault(0) > 0 ? categoryList[int.Parse((s.CategoryId.GetValueOrDefault(0) -1).ToString() )].ToString() : "",
                        CompanyId = s.CompanyId,
                        CompanyName = s.CompanyNavigation != null ? s.CompanyNavigation.PlantCode : "",
                        IsPortal = s.IsPortal.GetValueOrDefault(false),
                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }
        [HttpGet]
        [Route("GetNavItemsList")]
        public List<NavItemModel> GetNavItemsList()
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navitems = _context.Navitems.OrderBy(o => o.Description).Where(i => i.StatusCodeId == 1 && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.ItemCategoryCode + '|' + s.Description + '|' + s.BaseUnitofMeasure + '|' + s.InternalRef,
                        Description = s.Description,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        ItemNoDetailList = s.No + '|' + s.BaseUnitofMeasure,
                        Description2 = s.Description2,
                        ShelfLife = s.ShelfLife,
                        InternalRef = s.InternalRef,
                        PackQty = s.PackQty.Value,
                        PackSize = s.PackSize,
                        PurchaseUOM = s.PurchaseUom,
                        PackUom = s.PackUom,
                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }
        //dr
        [HttpGet]
        [Route("GetRelatedItemNo")]
        public List<NavItemModel> GetRelatedItemNo()
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var relatedNavItems = _context.Navitems.Where(r => r.No.StartsWith("PKG-") && r.RelatedItemNo != null).AsNoTracking().Select(s => s.RelatedItemNo).Distinct().ToList();
            var navitems = _context.Navitems.Where(r => relatedNavItems.Contains(r.No) && r.RelatedItemNo != "" && (r.IsPortal == false || r.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description,
                        RelatedItemNo = s.RelatedItemNo,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        InternalRef = s.InternalRef,
                        CategoryID = s.CategoryId,
                        StatusCodeID = s.StatusCodeId,
                        CompanyId = s.CompanyId,

                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }


        [HttpGet]
        [Route("GetNavItemsListByMethodCode")]
        public List<NavItemModel> GetNavItemsListByMethodCode(long? id)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navItemIds = _context.NavMethodCodeLines.Where(f => f.MethodCodeId == id).Select(s => s.ItemId).ToList();
            var navitems = _context.Navitems.Include(i => i.ProductGroupingNav)
                .Where(s => s.StatusCodeId == 1 && navItemIds.Contains(s.ItemId) && (s.IsPortal == false || s.IsPortal == null) && s.VendorNo == "Prod. Order" && s.StatusCodeId == 1).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description + '|' + s.InternalRef + '|' + s.BaseUnitofMeasure,
                        RelatedItemNo = s.RelatedItemNo,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        InternalRef = s.InternalRef,
                        CategoryID = s.CategoryId,
                        StatusCodeID = s.StatusCodeId,
                        CompanyId = s.CompanyId,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        ShelfLife = s.ShelfLife,

                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }
        [HttpGet]
        [Route("GetNavItemBasedRelatedItemsNos")]
        public List<NavItemModel> GetNavItemBasedRelatedItemsNos(long Id)
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var nav = _context.Navitems.Include(c => c.StatusCode).OrderByDescending(o => o.Description).Where(i => i.ItemId == Id && i.StatusCodeId == 1 && (i.IsPortal == false || i.IsPortal == null)).Select(s => s.RelatedItemNo).FirstOrDefault();

            var navitems = _context.Navitems.OrderByDescending(o => o.Description).Where(i => i.RelatedItemNo == nav && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();//i.No.StartsWith("PKG-")

            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description + '|' + s.Description2 + '|' + s.InternalRef,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        InternalRef = s.InternalRef,
                        CategoryID = s.CategoryId,
                        StatusCodeID = s.StatusCodeId,
                        CompanyId = s.CompanyId,


                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }






        //dr





        // GET: api/Project

        [HttpGet]
        [Route("GetNavItemForSampleRequestform")]
        public List<NavItemModel> GetNavItemForSampleRequestform(long? id)
        {

            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var openingStockId = _context.InventoryTypeOpeningStockBalance.Where(s => s.InventoryTypeId == id).Select(o => o.OpeningBalanceId).ToList();
            if (openingStockId.Count > 0)
            {
                var inventoryLineItemIds = _context.InventoryTypeOpeningStockBalanceLine.Where(s => openingStockId.Contains(s.OpeningStockId.Value)).Select(s => s.ItemId).ToList();
                if (inventoryLineItemIds.Count > 0)
                {
                    var navitems = _context.Navitems.Where(n => inventoryLineItemIds.Contains(n.ItemId)).OrderByDescending(o => o.Description).AsNoTracking().ToList();

                    if (navitems != null && navitems.Count > 0)
                    {
                        navitems.ForEach(s =>
                        {
                            NavItemModel navItemModel = new NavItemModel
                            {
                                ItemId = s.ItemId,
                                No = s.No,
                                ItemNoDetail = s.No + '|' + s.Description,
                                Description = s.Description,
                                Description2 = s.Description2,
                                ItemType = s.ItemType,
                                InternalRef = s.InternalRef,
                                CategoryID = s.CategoryId,
                                StatusCodeID = s.StatusCodeId,
                                CompanyId = s.CompanyId,
                                BaseUnitofMeasure = s.BaseUnitofMeasure,
                                PackUom = s.PackUom,


                            };
                            navItemModels.Add(navItemModel);
                        });
                    }
                }
            }
            return navItemModels;
        }

        [HttpGet]
        [Route("GetNavItemForProcessMachineTime")]
        public List<NavItemModel> GetNavItemForProcessMachineTime(long? id)
        {

            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var itemIds = _context.NavMethodCodeLines.Where(m => m.MethodCodeId == id && m.ItemId!=null).Select(s => s.ItemId).ToList();
            var navitems = _context.Navitems.Include(s=>s.AddedByUser).Where(n=>itemIds.Contains(n.ItemId)).OrderByDescending(o => o.Description).AsNoTracking().ToList();

            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.Description + '|' + s.InternalRef,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,                        
                        CategoryID = s.CategoryId,
                        StatusCodeID = s.StatusCodeId,
                        CompanyId = s.CompanyId,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        PackUom = s.PackUom,
                        ItemCategoryCode = s.ItemCategoryCode,
                        AddedDate = s.AddedDate,
                       
                        AddedByUser = s.AddedByUser?.UserName,
                    };
                    navItemModels.Add(navItemModel);
                });
            }

            return navItemModels;
        }

        [HttpGet]
        [Route("GetNavBatchSizeForProcessMachineTime")]
        public List<NavMethodCodeBatchModel> GetNavBatchSizeForProcessMachineTime(long? id)
        {

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<NavMethodCodeBatchModel> navMethodCodeBatchModels = new List<NavMethodCodeBatchModel>();

            var navmethodCodeBatch = _context.NavmethodCodeBatch.OrderByDescending(o => o.MethodCodeBatchId).Where(t => t.NavMethodCodeId == id).AsNoTracking().ToList();
            if (navmethodCodeBatch != null && navmethodCodeBatch.Count > 0)
            {
                navmethodCodeBatch.ForEach(s =>
                {
                    NavMethodCodeBatchModel navMethodCodeBatchModel = new NavMethodCodeBatchModel
                    {
                        MethodCodeBatchId = s.MethodCodeBatchId,
                        NavMethodCodeId = s.NavMethodCodeId,
                        BatchSize = s.BatchSize,
                        DefaultBatchSize = s.DefaultBatchSize,
                        BatchUnitSize = s.BatchUnitSize,
                        BatchSizeValue = applicationmasterdetail.FirstOrDefault(f=>f.ApplicationMasterDetailId == s.BatchSize)?.Value,
                    };
                    navMethodCodeBatchModels.Add(navMethodCodeBatchModel);
                });

            }
            return navMethodCodeBatchModels;
        }
        [HttpGet]
        [Route("GetNavItemForSampleRequestDoctors")]
        public List<NavItemModel> GetNavItemForSampleRequestDoctors(long? id)
        {

            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var ItemIds = new List<long?>();
            var samplelinesIds = _context.SampleRequestForm.Where(s => s.InventoryTypeId == id).Select(s => s.SampleRequestFormId).ToList();
            if (samplelinesIds.Count > 0)
            {
                var sampleItemIds = _context.SampleRequestFormLine.Where(t => samplelinesIds.Contains(t.SampleRequestFormId.Value)).Select(s => s.ItemId).ToList();
                ItemIds.AddRange(sampleItemIds);
            }
            var openingStockId = _context.InventoryTypeOpeningStockBalance.Where(s => s.InventoryTypeId == id).FirstOrDefault()?.OpeningBalanceId;
            if (openingStockId > 0)
            {
                var openingItemIds = _context.InventoryTypeOpeningStockBalanceLine.Where(s => s.OpeningStockId == openingStockId).Select(s => s.ItemId).ToList();
                ItemIds.AddRange(openingItemIds);
            }
            if (ItemIds.Count > 0)
            {

                var navitems = _context.Navitems.Where(n => ItemIds.Contains(n.ItemId)).OrderByDescending(o => o.Description).AsNoTracking().Distinct().ToList();

                if (navitems != null && navitems.Count > 0)
                {
                    navitems.ForEach(s =>
                    {
                        NavItemModel navItemModel = new NavItemModel
                        {
                            ItemId = s.ItemId,
                            No = s.No,
                            ItemNoDetail = s.No + '|' + s.Description + '|' + s.BaseUnitofMeasure,
                            Description = s.Description,
                            Description2 = s.Description2,
                            ItemType = s.ItemType,
                            InternalRef = s.InternalRef,
                            CategoryID = s.CategoryId,
                            StatusCodeID = s.StatusCodeId,
                            CompanyId = s.CompanyId,
                            BaseUnitofMeasure = s.BaseUnitofMeasure,
                            PackUom = s.PackUom,


                        };
                        navItemModels.Add(navItemModel);
                    });
                }
            }

            return navItemModels;
        }
        [HttpGet]
        [Route("GetNavItemspkg")]
        public List<NavItemModel> GetNavItemspkg()
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navitems = _context.Navitems.Include(c => c.StatusCode).OrderByDescending(o => o.Description).Where(i => i.No.StartsWith("PKG-") && i.StatusCodeId == 1 && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description + '|' + s.Description2 + '|' + s.InternalRef,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        InternalRef = s.InternalRef,
                        CategoryID = s.CategoryId,
                        StatusCodeID = s.StatusCodeId,
                        CompanyId = s.CompanyId,


                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }



        //
        // GET: api/Project
        [HttpGet]
        [Route("GetNavItemsMat")]
        public List<NavItemModel> GetNavItemsMat()
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1).Include("CompanyNavigation").Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = s.No + "  |  " + s.Description,
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                ItemType = s.ItemType,
                IsPortal = s.IsPortal
            }).OrderBy(o => o.Description).Where(w => w.No.ToLower().Contains("mat") && (w.IsPortal == false || w.IsPortal == null)).AsNoTracking().ToList();
            return items;
        }
        [HttpGet]
        [Route("GetNavItemsbyCountry")]
        public List<NavItemModel> GetNavItemsbyCountry(long Id)
        {
            var categoryList = new List<string>
            {
                "CAP",
                "CREAM",
                "DD",
                "SYRUP",
                "TABLET",
                "VET",
                "POWDER",
                "INJ"
            };
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var navitems = _context.Navitems.Include(n => n.LastSyncByNavigation).Include("AddedByUser")
               .Include("GenericCode").Include("NavMethodCodeLines.MethodCode").Include("ModifiedByUser")
               .Include("CompanyNavigation").Include(c => c.StatusCode).Include(n => n.NavItemCitemList).Where(f => f.CompanyId == Id && (f.IsPortal == false || f.IsPortal == null)).OrderByDescending(o => o.Description).Where(i => i.StatusCodeId == 1).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                if (Id != 6)
                {
                    navitems = navitems.Where(n => n.No.StartsWith("FP-")).ToList();
                }
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        RelatedItemNo = s.RelatedItemNo,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        Inventory = s.Inventory,
                        InternalRef = s.InternalRef,
                        ItemRegistration = s.ItemRegistration,
                        ExpirationCalculation = s.ExpirationCalculation,
                        BatchNos = s.BatchNos,
                        ProductionRecipeNo = s.ProductionRecipeNo,
                        Qcenabled = s.Qcenabled,
                        SafetyLeadTime = s.SafetyLeadTime,
                        ProductionBomno = s.ProductionBomno,
                        RoutingNo = s.RoutingNo,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        UnitCost = s.UnitCost,
                        UnitPrice = s.UnitPrice,
                        VendorNo = s.VendorNo,
                        VendorItemNo = s.VendorItemNo,
                        ItemCategoryCode = s.ItemCategoryCode,
                        ItemTrackingCode = s.ItemTrackingCode,
                        Qclocation = s.Qclocation,
                        Company = s.Company,
                        LastSyncDate = s.LastSyncDate,
                        LastSyncBy = s.LastSyncBy,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        NavItemCustomerItemID = s.NavItemCitemList != null ? s.NavItemCitemList.Select(t => t.NavItemCustomerItemId.Value).ToList() : new List<long>(),
                        CategoryID = s.CategoryId,
                        Steroid = s.Steroid,
                        SteroidText = s.Steroid.HasValue ? s.Steroid.Value == true ? "Steroid" : "Non-steroid" : string.Empty,
                        ShelfLife = s.ShelfLife,
                        Quota = s.Quota,
                        PackSize = s.PackSize,
                        PackUom = s.PackUom,
                        GenericCodeId = s.GenericCodeId,
                        GenericCode = s.GenericCode != null ? s.GenericCode.Code : string.Empty,
                        MethodCode = s.NavMethodCodeLines.FirstOrDefault() != null ? s.NavMethodCodeLines.FirstOrDefault().MethodCode.MethodName : "",
                        MethodCodeId = s.NavMethodCodeLines.FirstOrDefault() != null ? s.NavMethodCodeLines.FirstOrDefault().MethodCode.MethodCodeId : -1,
                        ItemGroup = s.ItemCategoryCode,// s.CategoryId.GetValueOrDefault(0) > 0 ? categoryList[int.Parse((s.CategoryId.GetValueOrDefault(0) -1).ToString() )].ToString() : "",
                        CompanyId = s.CompanyId,
                        CompanyName = s.CompanyNavigation != null ? s.CompanyNavigation.PlantCode : "",
                        IsDifferentAcuom = s.IsDifferentAcuom.GetValueOrDefault(false),
                        PackQty = s.PackQty.GetValueOrDefault(0),

                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }
        [HttpGet]
        [Route("GetItems")]
        public List<NavItemModel> GetItems()
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1).Include("CompanyNavigation").Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = s.CompanyNavigation != null ? s.No + "  |  " + s.Description + "  |  " + s.Description2 + "  |  " + s.CompanyNavigation.PlantCode : s.No + "  |  " + s.Description + "  |  " + s.Description2,
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                IsPortal = s.IsPortal

            }).OrderByDescending(o => o.ItemId).Where(s => (s.IsPortal == false || s.IsPortal == null)).AsNoTracking().ToList();

            //items.ForEach(f =>
            //{
            //    f.ItemNoDetail = f.No + "  |  " + f.Description + " | " + f.Description2 + " | " + f.ItemCategoryCode + " | " + f.Company;
            //});
            return items;
        }

        [HttpPost]
        [Route("GetNavItemsForInventoryLine")]
        public List<NavItemModel> GetNavItemsForInventoryLine(SearchModel searchModel)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CompanyId == searchModel.Id && f.ItemCategoryCode == searchModel.NavType && (f.IsPortal == false || f.IsPortal == null)).Include("CompanyNavigation").Include(n => n.LastSyncByNavigation).Include(c => c.StatusCode).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = (s.No) + "  |  " + (s.Description) + "  |  " + (s.Description2 == null ? "" : s.Description2),
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                BatchNos = s.BatchNos,

            }).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();

            //items.ForEach(f =>
            //{
            //    f.ItemNoDetail = f.No + "  |  " + f.Description + " | " + f.Description2 + " | " + f.ItemCategoryCode + " | " + f.Company;
            //});
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionByCountryItems")]
        public List<NavItemModel> GetDescriptionByCountryItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = s.No + "  |  " + s.Description + " | " + s.Description2,
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                ItemType = s.ItemType,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                CompanyId = s.CompanyId,
                InternalRef = s.InternalRef,

            }).Where(f => f.CompanyId == id).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }

        [HttpGet]
        [Route("GetNavItemsItemCategory")]
        public List<NavItemModel> GetNavItemsItemCategory(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = (s.No) + "  |  " + (s.Description) + " | " + (s.Description2),
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                ItemType = s.ItemType,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                CompanyId = s.CompanyId,
                InternalRef = s.InternalRef,
                ItemCategoryCode = s.ItemCategoryCode,


            }).Where(f => f.CompanyId == id).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionByCountryMATItems")]
        public List<NavItemModel> GetDescriptionByCountryMATItems(long id, int type)
        {
            string typeFrom = "";
            if (type == 1040)
            {
                typeFrom = "MAT-";
            }
            if (type == 1041)
            {
                typeFrom = "PKG";
            }
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = s.No + "  |  " + s.Description + " | " + s.Description2,
                No = s.No,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                ItemType = s.ItemType,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                CompanyId = s.CompanyId,
                InternalRef = s.InternalRef,
                StatusCodeID = s.StatusCodeId,
                StatusCode = s.StatusCode.CodeValue,
            }).Where(f => f.CompanyId == id).OrderByDescending(o => o.ItemId).Where(i => i.No.StartsWith(typeFrom)).AsNoTracking().ToList();
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionPUOMByCountryItems")]
        public List<NavItemModel> GetDescriptionPUOMByCountryItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description + " | " + s.Description2 + " | " + s.BaseUnitofMeasure,
                InternalRef = s.No + "  |  " + s.Description,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId

            }).Where(f => f.CompanyId == id && f.No.StartsWith("FP") && f.VendorNo.Contains("Prod. Order") && f.BaseUnitofMeasure.ToUpper() != "STRIP" && f.StatusCodeID == 1).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }

        [HttpGet]
        [Route("GetDescriptionFPandPRItems")]
        public List<NavItemModel> GetDescriptionFPandPRItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description,
                InternalRef = s.No + " | " + s.Description + " | " + s.Description2,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,

            }).Where(f => f.CompanyId == id && f.No.StartsWith("FP") || f.No.StartsWith("PR") && f.StatusCodeID == 1).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionFPItems")]
        public List<NavItemModel> GetDescriptionFPItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description,
                InternalRef = s.No + " | " + s.Description + " | " + s.Description2,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                Company = s.Company,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,

            }).Where(f => f.CompanyId == id && f.No.StartsWith("FP") && f.StatusCodeID == 1).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionFPItemsDetails")]
        public List<NavItemModel> GetDescriptionFPItemsDetails(long id)
        {
            var items = _context.Navitems.Where(f => !f.Description.Contains("1X10") && f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = (s.No) + "  |  " + (s.ItemCategoryCode) + "  |  " + (s.InternalRef) + " | " + (s.Description) + " " + (s.Description2),
                InternalRef = s.ItemCategoryCode + " For " + s.InternalRef,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description + " " + s.Description2,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                Company = s.Company,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,

            }).Where(f => f.CompanyId == id && f.No.StartsWith("FP")).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }

        [HttpPost]
        [Route("GetNavItemById")]
        public NavItemModel GetNavItemById(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.ItemId == id && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = (s.No) + "  |  " + (s.ItemCategoryCode) + "  |  " + (s.InternalRef) + " | " + (s.Description) + " " + (s.Description2),
                InternalRef = s.ItemCategoryCode + " For " + s.InternalRef,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description + " " + s.Description2,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                Company = s.Company,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,

            }).OrderByDescending(o => o.ItemId).AsNoTracking().FirstOrDefault();
            return items;
        }
        [HttpGet]
        [Route("GetDescriptionExceptFPandPRItems")]
        public List<NavItemModel> GetDescriptionExceptFPandPRItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CompanyId == id && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description,
                InternalRef = s.No + " | " + s.Description + " | " + s.Description2,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                CompanyId = s.CompanyId,
                VendorNo = s.VendorNo,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,

            }).Where(f => !f.No.StartsWith("FP") || !f.No.StartsWith("PR")).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }


        //

        [HttpGet]
        [Route("GetDescriptionCategory")]
        public List<NavItemModel> GetDescriptionCategory()
        {

            var navSaleCategory = _context.NavSaleCategory.AsNoTracking().ToList();
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CategoryId != null && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description,
                // related item no get category code
                // = navSaleCategory != null ? navSaleCategory.Where(a => a.SaleCategoryId == s.CategoryId).Select(a => a.Code).SingleOrDefault() : "",
                InternalRef = s.No + " | " + s.Description + " | " + s.Description2,//+ " | " //+ navSaleCategory != null ? navSaleCategory.Where(a => a.SaleCategoryId == s.CategoryId).Select(a => a.Code).SingleOrDefault() : "", 
                // RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,
                ItemCategoryCode = s.ItemCategoryCode,


                PurchaseUOM = s.PurchaseUom,

            }).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;//.Where(f => !f.No.StartsWith("FP") || !f.No.StartsWith("PR"))
        }




        // dr

        [HttpGet]
        [Route("GetDescriptionExceptItems")]
        public List<NavItemModel> GetDescriptionExceptItems(long id)
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CompanyId == id && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + " | " + s.Description + " | " + s.Description2 + " | " + s.InternalRef,
                InternalRef = s.InternalRef,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                CompanyId = s.CompanyId,
                StatusCodeID = s.StatusCodeId,
                PurchaseUOM = s.PurchaseUom,
                Qclocation = s.Qclocation

            }).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;//.Where(f => !f.No.StartsWith("FP") || !f.No.StartsWith("PR"))
        }

        [HttpGet]
        [Route("GetNavItemsForSalesEntry")]
        public List<NavItemModel> GetNavItemsForSalesEntry(long id)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.No.StartsWith("FP") || f.No.StartsWith("SF")) && f.CompanyId == id && (f.IsPortal == false || f.IsPortal == null)).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            if (items != null && items.Count > 0)
            {
                items.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel();
                    navItemModel.ItemId = s.ItemId;
                    navItemModel.No = s.No;

                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.BaseUnitofMeasure = s.BaseUnitofMeasure;
                    navItemModel.RelatedItemNo = s.RelatedItemNo;
                    navItemModel.Description = s.Description;
                    navItemModel.Description2 = s.Description2;
                    navItemModel.PackUom = s.PackUom;
                    navItemModel.BatchNos = s.BatchNos;
                    navItemModel.ItemCategoryCode = s.ItemCategoryCode?.Replace("FP", "");
                    navItemModel.CompanyId = s.CompanyId;
                    navItemModel.StatusCodeID = s.StatusCodeId;
                    navItemModel.PurchaseUOM = s.PurchaseUom;
                    navItemModel.Qclocation = s.Qclocation;
                    navItemModel.ItemNoDetail = s.No + " | " + s.ItemCategoryCode + " | " + s.Description + " | " + s.Description2 + " | " + s.InternalRef;
                    navItemModels.Add(navItemModel);
                });
            }

            return navItemModels;//.Where(f => !f.No.StartsWith("FP") || !f.No.StartsWith("PR"))
        }

        [HttpGet]
        [Route("GetNavItemByDescription")]
        public NavItemModel GetNavItemByDescription(string description)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var item = _context.Navitems.Where(f => f.Description == description && (f.IsPortal == false || f.IsPortal == null)).FirstOrDefault();
            NavItemModel navItemModel = new NavItemModel();
            if (item != null)
            {

                navItemModel = new NavItemModel();
                navItemModel.ItemId = item.ItemId;
                navItemModel.No = item.No;

                navItemModel.InternalRef = item.InternalRef;
                navItemModel.BaseUnitofMeasure = item.BaseUnitofMeasure;
                navItemModel.Description = item.Description;
                navItemModel.Description2 = item.Description2;
                navItemModel.PackUom = item.PackUom;
                navItemModel.BatchNos = item.BatchNos;
                navItemModel.ItemCategoryCode = item.ItemCategoryCode;


                navItemModels.Add(navItemModel);

            }

            return navItemModel;
        }
        //dr
        [HttpGet]
        [Route("GetSWItems")]
        public List<NavItemModel> GetSWItems()
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && (f.IsPortal == false || f.IsPortal == null)).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                ItemNoDetail = s.No + " | " + s.Description + " | " + s.PackSize.GetValueOrDefault(0),
                No = s.No,
                CompanyId = s.CompanyId,
            }).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get navItems")]
        [HttpGet("GetNavItems/{id:int}")]
        public ActionResult<NavItemModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var navItems = _context.Navitems.SingleOrDefault(p => p.ItemId == id.Value && (p.IsPortal == false || p.IsPortal == null));
            var result = _mapper.Map<NavItemModel>(navItems);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        //dr

        //dr
        // GET: api/Project
        [HttpGet]
        [Route("GetNavDistributedItems")]
        public List<NavItemCitemListModel> GetNavDistributedItems()
        {
            var allitems = _context.NavItemCitemList.Select(s => new NavItemCitemListModel
            {
                NavItemCitemID = s.NavItemCitemId,
                NavItemID = s.NavItemId,
                NavItemCustomerItemID = s.NavItemCustomerItemId,

            }).OrderByDescending(o => o.NavItemCitemID).AsNoTracking().ToList();
            return allitems;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavItem")]
        public NavItemModel Put(NavItemModel value)
        {
            var genericCodes = _context.GenericCodes.AsNoTracking().ToList();
            var navItems = _context.Navitems.SingleOrDefault(p => p.ItemId == value.ItemId);
            //navItems.StateId = value.StateID;
            var deleteNavCItemList = _context.NavItemCitemList.Where(c => c.NavItemId.Value == value.ItemId).AsNoTracking().ToList();
            _context.NavItemCitemList.RemoveRange(deleteNavCItemList);
            _context.SaveChanges();
            if (value.NavItemCustomerItemID.Count > 0)
            {
                value.NavItemCustomerItemID.ForEach(id =>
                {
                    var navItemCitemList = new NavItemCitemList
                    {
                        NavItemId = value.ItemId,
                        NavItemCustomerItemId = id,
                    };
                    _context.NavItemCitemList.Add(navItemCitemList);
                    _context.SaveChanges();
                });
            }
            navItems.ModifiedByUserId = value.ModifiedByUserID;
            navItems.ModifiedDate = DateTime.Now;
            navItems.Steroid = value.Steroid;
            navItems.CategoryId = value.CategoryID;
            navItems.ShelfLife = value.ShelfLife;
            navItems.Quota = value.Quota;
            navItems.PackSize = value.PackSize;
            navItems.PackUom = value.PackUom;
            navItems.PackQty = value.PackQty;
            navItems.IsDifferentAcuom = value.IsDifferentAcuom;
            navItems.GenericCodeId = value.GenericCodeId;
            navItems.StatusCodeId = value.StatusCodeID;

            if (value.MethodCodeId > 0)
            {
                var methodCode = _context.NavMethodCodeLines.Any(f => f.MethodCodeId == value.MethodCodeId && f.ItemId == value.ItemId);
                if (!methodCode)
                {
                    var NavMethodCode = new NavMethodCodeLines
                    {
                        ItemId = value.ItemId,
                        MethodCodeId = value.MethodCodeId,
                    };
                    _context.NavMethodCodeLines.Add(NavMethodCode);
                }
            }


            _context.SaveChanges();
            value.GenericCode = navItems.GenericCodeId > 0 ? genericCodes.FirstOrDefault(g => g.GenericCodeId == navItems.GenericCodeId).Code : string.Empty;
            value.MethodCode = value.MethodCodeId > 0 ? _context.NavMethodCode.FirstOrDefault(g => g.MethodCodeId == value.MethodCodeId).MethodName : value.MethodCode;
            value.SteroidText = navItems.Steroid.HasValue ? navItems.Steroid.Value == true ? "Steroid" : "Non-steroid" : string.Empty;
            return value;
        }

        [HttpGet]
        [Route("GetGenericCodes")]
        public List<GenericCodeModel> GetGenericCodes()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var genericCodes = _context.GenericCodes.AsNoTracking().ToList();
            List<GenericCodeModel> GenericCodesModel = new List<GenericCodeModel>();
            genericCodes.ForEach(s =>
            {
                GenericCodeModel GenericCodesModels = new GenericCodeModel
                {
                    GenericCodeId = s.GenericCodeId,
                    Code = s.Code,
                    Description = s.Description,
                    CodeUOM = s.Code + "|" + (applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null),
                };
                GenericCodesModel.Add(GenericCodesModels);
            });
            return GenericCodesModel.OrderByDescending(o => o.Code).ToList();
        }

        [HttpGet]
        [Route("GetGenericCodeByCountry")]
        public List<GenericCodeModel> GetGenericCodeByCountry(int id)
        {
            string internalRef = string.Empty;
            var applicationMasterDetails = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(a => a.ApplicationMaster.ApplicationMasterCodeId == 109).ToList();
            var countryItem = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == id);
            if (countryItem.Value == "Malaysia")
            {
                internalRef = "SWJB";
            }
            if (countryItem.Value == "Singapore")
            {
                internalRef = "SWSIN";
            }
            if (!string.IsNullOrEmpty(internalRef))
            {
                var genericodeIds = _context.Navitems.Where(s => s.InternalRef.Contains(internalRef)).Select(g => g.GenericCodeId).ToList();
                var items = _context.GenericCodes.Select(s => new GenericCodeModel
                {
                    GenericCodeId = s.GenericCodeId,
                    Code = s.Code
                }).Where(c => genericodeIds.Contains(c.GenericCodeId)).OrderByDescending(o => o.Code).AsNoTracking().ToList();
                return items;
            }
            return new List<GenericCodeModel>();
        }
        [HttpGet]
        [Route("GetGenericCodeByPlantID")]
        public List<GenericCodeModel> GetGenericCodeByPlantID(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            string internalRef = string.Empty;
            var plant = _context.Plant.FirstOrDefault(a => a.PlantId == id);
            List<GenericCodeModel> GenericCodesModel = new List<GenericCodeModel>();

            var items = _context.GenericCodes.Include(g => g.GenericCodeCountry).ToList();
            items.ForEach(s =>
            {
                if (s.GenericCodeCountry.Count > 0 && s.GenericCodeCountry.Select(t => t.CountryId).Contains(plant.PlantId))
                {
                    GenericCodeModel GenericCodesModels = new GenericCodeModel
                    {
                        GenericCodeId = s.GenericCodeId,
                        Code = s.Code,
                        Description = s.Description,
                        UOM = (applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom) != null ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.Uom).Value : null),
                    };
                    GenericCodesModel.Add(GenericCodesModels);
                }

            });
            return GenericCodesModel.OrderBy(o => o.Code).ToList();
        }
        [HttpGet]
        [Route("GetGenericCodeByPlant")]
        public List<GenericCodeModel> GetGenericCodeByPlant(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            string internalRef = string.Empty;
            var plant = _context.Plant.FirstOrDefault(a => a.PlantId == id);
            if (plant.PlantCode == "SWMY")
            {
                internalRef = "SWJB";
            }
            if (plant.PlantCode == "SWSG")
            {
                internalRef = "SWSIN";
            }
            var navItems = _context.Navitems.Include("GenericCode").Where(s => s.InternalRef.Contains(internalRef)).ToList();
            List<GenericCodeModel> genericCodeModels = new List<GenericCodeModel>();
            navItems.ForEach(n =>
            {
                if (n.GenericCode != null)
                {
                    GenericCodeModel genericCodeModel = new GenericCodeModel
                    {
                        GenericCodeId = n.GenericCode.GenericCodeId,
                        Code = n.GenericCode.Code,
                        //UOM = n.BaseUnitofMeasure,
                        UOM = masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == n.GenericCode.Uom).Select(m => m.Value).FirstOrDefault() : null,


                    };
                    genericCodeModels.Add(genericCodeModel);
                }
            });
            return genericCodeModels;
        }


        [HttpGet]
        [Route("GetNavItemByGenericID")]
        public List<NavItemModel> GetNavItemByGenericID(int id, int countryId)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            string internalRef = string.Empty;
            var applicationMasterDetails = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(a => a.ApplicationMaster.ApplicationMasterCodeId == 109).ToList();
            var countryItem = applicationMasterDetails.FirstOrDefault(a => a.ApplicationMasterDetailId == countryId);
            if (countryItem.Value == "Malaysia")
            {
                internalRef = "SWJB";
            }
            if (countryItem.Value == "Singapore")
            {
                internalRef = "SWSIN";
            }
            var navitems = _context.Navitems
            .Include("GenericCode").OrderByDescending(o => o.Description).Where(i => i.GenericCodeId == id && i.StatusCodeId == 1 && i.InternalRef.Contains(internalRef) && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        ProductionBomno = s.ProductionBomno,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        PackUom = s.PackUom,
                        GenericCodeId = s.GenericCodeId,
                        GenericCode = s.GenericCode != null ? s.GenericCode.Code : string.Empty,
                        InternalRef = s.InternalRef

                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }

        [HttpGet]
        [Route("GetNavItemsByGenericID")]
        public List<NavItemModel> GetNavItemsByGenericID(int id)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            string internalRef = string.Empty;
            var navitems = _context.Navitems
            .Include("GenericCode").OrderByDescending(o => o.Description).Where(i => i.GenericCodeId == id && i.StatusCodeId == 1 && (i.IsPortal == false || i.IsPortal == null)).AsNoTracking().ToList();
            if (navitems != null && navitems.Count > 0)
            {
                navitems.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel
                    {
                        ItemId = s.ItemId,
                        No = s.No,
                        ItemNoDetail = s.No + '|' + s.Description,
                        Description = s.Description,
                        Description2 = s.Description2,
                        ItemType = s.ItemType,
                        ProductionBomno = s.ProductionBomno,
                        BaseUnitofMeasure = s.BaseUnitofMeasure,
                        PackUom = s.PackUom,
                        GenericCodeId = s.GenericCodeId,
                        GenericCode = s.GenericCode != null ? s.GenericCode.Code : string.Empty,
                        InternalRef = s.InternalRef,
                        ItemNoDetailList = s.No + '|' + s.ItemCategoryCode + '|' + s.Description + '|' + s.InternalRef + '|' + s.BaseUnitofMeasure,

                    };
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels;
        }

        [HttpGet]
        [Route("GetNAVItemForProductGrouping")]
        public List<NavItemModel> GetNAVItemForProductGrouping()
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var items = _context.Navitems.Include(s => s.StatusCode).Where(f => f.No.StartsWith("FP") || f.No.StartsWith("SF") && (f.IsPortal == false || f.IsPortal == null)).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            if (items != null && items.Count > 0)
            {
                items.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel();
                    navItemModel.ItemId = s.ItemId;
                    navItemModel.No = s.No;
                    navItemModel.ItemCategoryCode = s.ItemCategoryCode != null ? s.ItemCategoryCode.Replace("FP", "").Trim() : "";
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.RelatedItemNo = s.RelatedItemNo;
                    navItemModel.Description = s.Description;
                    navItemModel.Description2 = s.Description2;
                    navItemModel.PackUom = s.PackUom;
                    navItemModel.BaseUnitofMeasure = s.BaseUnitofMeasure;
                    navItemModel.ItemType = s.ItemType;
                    navItemModel.CompanyId = s.CompanyId;
                    navItemModel.VendorNo = s.VendorNo;
                    navItemModel.StatusCodeID = s.StatusCodeId;
                    navItemModel.StatusCode = s.StatusCode?.CodeValue;
                    navItemModel.PurchaseUOM = s.PurchaseUom;
                    navItemModel.ReplenishmentMethod = s.VendorNo;

                    navItemModels.Add(navItemModel);
                });


            }
            if (navItemModels != null && navItemModels.Count > 0)
            {
                navItemModels.ForEach(s =>
                {
                    s.ItemNoDetail = s.No + "  |  " + s.ItemCategoryCode + " | " + s.Description + " | " + s.InternalRef + " | " + s.PackUom;
                });
            }
            return navItemModels;
        }


        [HttpPost]
        [Route("GetNAVItemsByCompanies")]
        public List<NavItemModel> GetNAVItemsByCompanies(List<long> ids)
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var items = _context.Navitems.Include(s => s.StatusCode).Where(f => (f.No.StartsWith("FP") || f.No.StartsWith("SF") && (f.IsPortal == false || f.IsPortal == null)) && (f.CompanyId != null && ids.Contains(f.CompanyId.Value))).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            if (items != null && items.Count > 0)
            {
                items.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel();
                    navItemModel.ItemId = s.ItemId;
                    navItemModel.No = s.No;
                    navItemModel.ItemCategoryCode = s.ItemCategoryCode != "" && s.ItemCategoryCode != null ? s.ItemCategoryCode.Replace("FP", "").Trim() : "";
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.RelatedItemNo = s.RelatedItemNo;
                    navItemModel.Description = s.Description;
                    navItemModel.Description2 = s.Description2;
                    navItemModel.PackUom = s.PackUom;
                    navItemModel.BaseUnitofMeasure = s.BaseUnitofMeasure;
                    navItemModel.ItemType = s.ItemType;
                    navItemModel.CompanyId = s.CompanyId;
                    navItemModel.VendorNo = s.VendorNo;
                    navItemModel.StatusCodeID = s.StatusCodeId;
                    navItemModel.StatusCode = s.StatusCode?.CodeValue;
                    navItemModel.PurchaseUOM = s.PurchaseUom;
                    navItemModel.ReplenishmentMethod = s.VendorNo;

                    navItemModels.Add(navItemModel);
                });


            }
            if (navItemModels != null && navItemModels.Count > 0)
            {
                navItemModels.ForEach(s =>
                {
                    s.ItemNoDetail = s.No + "  |  " + s.ItemCategoryCode + " | " + s.Description + " | " + s.InternalRef + " | " + s.BaseUnitofMeasure;
                });
            }
            return navItemModels;
        }

        [HttpPost]
        [Route("DeleteNavItems")]
        public void DeleteNavItems(List<NavItemMasterModel> navItemMasterModels)
        {

            List<long> ids = navItemMasterModels.Select(i => i.ItemId).ToList();


            var query = _context.Navitems.Where(f => ids.Contains(f.ItemId)).AsQueryable();

            foreach (var property in _context.Model.FindEntityType(typeof(Navitems)).GetNavigations())
                query = query.Include(property.Name);

            var items = query.ToList();

            Navitems exceptionItem = null;
            try
            {
                if (items != null)
                {

                    items.ForEach(s =>
                    {
                        exceptionItem = s;
                        RemoveNavigationProperties(s);
                        _context.Navitems.Remove(s);
                        _context.SaveChanges();
                    });


                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception Item." + exceptionItem.ItemId + "," + exceptionItem.No, ex);
            }

        }

        private void RemoveNavigationProperties(Navitems s)
        {
            _context.AcentryLines.RemoveRange(s.AcentryLines);
            // _context.SaveChanges();
            _context.ClassificationBmr.RemoveRange(s.ClassificationBmr);
            // _context.SaveChanges();
            _context.FinishProductExcipientLine.RemoveRange(s.FinishProductExcipientLine);
            //_context.SaveChanges();
            _context.FpproductLine.RemoveRange(s.FpproductLine);
            // _context.SaveChanges();
            _context.FpproductNavisionLine.RemoveRange(s.FpproductNavisionLine);
            // _context.SaveChanges();
            _context.GeneralEquivalaentNavision.RemoveRange(s.GeneralEquivalaentNavision);
            // _context.SaveChanges();
            _context.InterCompanyPurchaseOrderLine.RemoveRange(s.InterCompanyPurchaseOrderLine);
            // _context.SaveChanges();
            _context.Ipir.RemoveRange(s.Ipir);
            //  _context.SaveChanges();
            _context.ItemCost.RemoveRange(s.ItemCost);
            //  _context.SaveChanges();
            _context.ItemCostLine.RemoveRange(s.ItemCostLine);
            //  _context.SaveChanges();
            _context.ItemSalesPrice.RemoveRange(s.ItemSalesPrice);
            //  _context.SaveChanges();
            _context.ItemStockInfo.RemoveRange(s.ItemStockInfo);
            //  _context.SaveChanges();
            _context.NavItemBatchNo.RemoveRange(s.NavItemBatchNo);
            //  _context.SaveChanges();
            _context.NavItemCitemList.RemoveRange(s.NavItemCitemList);
            //   _context.SaveChanges();
            _context.NavManufacturingProcess.RemoveRange(s.NavManufacturingProcess);
            //   _context.SaveChanges();
            _context.NavMethodCodeLines.RemoveRange(s.NavMethodCodeLines);
            //   _context.SaveChanges();
            _context.NavPackingMethod.RemoveRange(s.NavPackingMethod);
            //   _context.SaveChanges();
            _context.NavcustomerItems.RemoveRange(s.NavcustomerItems);
            //  _context.SaveChanges();
            _context.NavisionSpecification.RemoveRange(s.NavisionSpecification);
            //  _context.SaveChanges();
            _context.NavitemLinks.RemoveRange(s.NavitemLinksMyItem);
            // _context.SaveChanges();
            _context.NavitemStockBalance.RemoveRange(s.NavitemStockBalance);
            //   _context.SaveChanges();
            _context.NavplannedProdOrder.RemoveRange(s.NavplannedProdOrder);
            //   _context.SaveChanges();
            _context.OrderRequirement.RemoveRange(s.OrderRequirement);
            //   _context.SaveChanges();
            _context.OrderRequirementLine.RemoveRange(s.OrderRequirementLine);
            //   _context.SaveChanges();
            _context.OrderRequirementLineSplit.RemoveRange(s.OrderRequirementLineSplit);
            //   _context.SaveChanges();
            _context.PackagingItemHistory.RemoveRange(s.PackagingItemHistory);
            //   _context.SaveChanges();
            _context.PackagingItemHistoryLine.RemoveRange(s.PackagingItemHistoryLine);
            //   _context.SaveChanges();
            _context.PkgapprovalInformation.RemoveRange(s.PkgapprovalInformation);
            //    _context.SaveChanges();
            _context.ProductGroupingNav.RemoveRange(s.ProductGroupingNav);
            //   _context.SaveChanges();
            _context.ProductionBatchInformationLine.RemoveRange(s.ProductionBatchInformationLine);
            //   _context.SaveChanges();
            _context.ProductionForecast.RemoveRange(s.ProductionForecast);
            //   _context.SaveChanges();
            _context.ProductionOrder.RemoveRange(s.ProductionOrder);
            ///   _context.SaveChanges();
            _context.ProductionOutput.RemoveRange(s.ProductionOutput);
            //   _context.SaveChanges();
            _context.ProductionSimulation.RemoveRange(s.ProductionSimulation);
            //    _context.SaveChanges();
            _context.PsbreportCommentDetail.RemoveRange(s.PsbreportCommentDetail);
            //   _context.SaveChanges();
            _context.PurchaseItemSalesEntryLine.RemoveRange(s.PurchaseItemSalesEntryLine);
            //   _context.SaveChanges();
            _context.PurchaseOrderLine.RemoveRange(s.PurchaseOrderLine);
            //   _context.SaveChanges();

            //   _context.SaveChanges();
            _context.RequestAcline.RemoveRange(s.RequestAcline);
            //    _context.SaveChanges();
            _context.SalesOrderEntry.RemoveRange(s.SalesOrderEntry);
            //    _context.SaveChanges();
            _context.SampleRequestForDoctor.RemoveRange(s.SampleRequestForDoctor);
            //  _context.SaveChanges();
            _context.SampleRequestFormLine.RemoveRange(s.SampleRequestFormLineInventoryItem);
            //  _context.SaveChanges();
            _context.SimulationAddhoc.RemoveRange(s.SimulationAddhoc);
            //  _context.SaveChanges();
            _context.TempSalesPackInformationFactor.RemoveRange(s.TempSalesPackInformationFactor);
            //  _context.SaveChanges();

        }

        [HttpGet]
        [Route("GetNavItemsByCountryDataBase")]
        public List<NavItemModel> GetNavItemsByCountryDataBase(int id)
        {
            string internalRef = string.Empty;
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            NavItemModel navItemModel = new NavItemModel();

            var navItems = _context.Navitems.Where(s => s.CompanyId == id && s.No.StartsWith("FP") && (s.IsPortal == false || s.IsPortal == null)).ToList();
            if (navItems != null && navItems.Count > 0)
            {
                navItems.ForEach(s =>
                {
                    navItemModel = new NavItemModel();
                    navItemModel.ItemId = s.ItemId;
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.No = s.No;
                    navItemModel.ItemCategoryCode = s.ItemCategoryCode?.Replace("FP", "").Trim();
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.RelatedItemNo = s.RelatedItemNo;
                    navItemModel.Description = s.Description;
                    navItemModel.Description2 = s.Description2;
                    navItemModel.PackUom = s.PackUom;
                    navItemModel.BaseUnitofMeasure = s.BaseUnitofMeasure;
                    navItemModel.ItemType = s.ItemType;
                    navItemModel.CompanyId = s.CompanyId;
                    navItemModel.VendorNo = s.VendorNo;
                    navItemModel.StatusCodeID = s.StatusCodeId;
                    navItemModel.StatusCode = s.StatusCode?.CodeValue;
                    navItemModel.PurchaseUOM = s.PurchaseUom;
                    navItemModel.ReplenishmentMethod = s.VendorNo;
                    navItemModels.Add(navItemModel);
                });

            }
            // return items;

            if (navItemModels != null && navItemModels.Count > 0)
            {
                navItemModels.ForEach(s =>
                {
                    s.ItemNoDetail = s.No + " | " + s.ItemCategoryCode + "  |  " + s.Description + " | " + s.BaseUnitofMeasure + " | " + s.InternalRef;
                });
            }
            return navItemModels;
        }


        [HttpGet]
        [Route("GetItemsByCountryDataBase")]
        public List<NavItemModel> GetItemsByCountryDataBase(int id)
        {
            string internalRef = string.Empty;
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            NavItemModel navItemModel = new NavItemModel();

            var navItems = _context.Navitems.Where(s => s.CompanyId == id && (s.IsPortal == false || s.IsPortal == null)).ToList();
            if (navItems != null && navItems.Count > 0)
            {
                navItems.ForEach(s =>
                {
                    navItemModel = new NavItemModel();
                    navItemModel.ItemId = s.ItemId;
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.No = s.No;
                    navItemModel.ItemCategoryCode = s.ItemCategoryCode?.Replace("FP", "").Trim();
                    navItemModel.InternalRef = s.InternalRef;
                    navItemModel.RelatedItemNo = s.RelatedItemNo;
                    navItemModel.Description = s.Description;
                    navItemModel.Description2 = s.Description2;
                    navItemModel.PackUom = s.PackUom;
                    navItemModel.BaseUnitofMeasure = s.BaseUnitofMeasure;
                    navItemModel.ItemType = s.ItemType;
                    navItemModel.CompanyId = s.CompanyId;
                    navItemModel.VendorNo = s.VendorNo;
                    navItemModel.StatusCodeID = s.StatusCodeId;
                    navItemModel.StatusCode = s.StatusCode?.CodeValue;
                    navItemModel.PurchaseUOM = s.PurchaseUom;
                    navItemModel.ReplenishmentMethod = s.VendorNo;
                    navItemModels.Add(navItemModel);
                });

            }
            // return items;

            if (navItemModels != null && navItemModels.Count > 0)
            {
                navItemModels.ForEach(s =>
                {
                    s.ItemNoDetail = s.No + " | " + s.ItemCategoryCode + "  |  " + s.Description + " | " + s.BaseUnitofMeasure + " | " + s.InternalRef;
                });
            }
            return navItemModels;
        }

    }
}