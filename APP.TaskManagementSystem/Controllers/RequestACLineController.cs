﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RequestACLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHubContext<ChatHub> _hub;
        private readonly IHostingEnvironment _hostingEnvironment;

        public RequestACLineController(CRT_TMSContext context, IMapper mapper, IHubContext<ChatHub> hub, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _hub = hub;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetRequestACLines")]
        public List<RequestACLineModel> Get()
        {
            var RequestAcline = _context.RequestAcline.Include("AddedByUser").Include("ModifiedByUser").Include("NavItem").Include(r => r.RequestAc).Select(s => new RequestACLineModel
            {
                RequestAcid = s.RequestAcid,
                NavItemId = s.NavItemId,
                DistributorItemId = s.DistributorItemId,
                ShelfLife = s.ShelfLife,
                Avg12Months = s.Avg12Months,
                Avg6Months = s.Avg6Months,
                Avg3Months = s.Avg3Months,
                CurrentAc = s.CurrentAc,
                ProposedNewAc = s.ProposedNewAc,
                DifferenceInAc = s.DifferenceInAc,
                PercentDifference = s.PercentDifference,
                Comments = s.Comments,
                ItemName = s.NavItem.No,
                CompanyId = s.RequestAc.CompanyId,
                CustomerId = s.RequestAc.CustomerId,
                CompanyName = s.RequestAc.Company != null ? s.RequestAc.Company.PlantCode : "",
                CustomerName = s.RequestAc.Customer != null ? s.RequestAc.Customer.CompanyName : "",


                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,


            }).OrderByDescending(o => o.RequestAcid).AsNoTracking().ToList();
            //var result = _mapper.Map<List<RequestACLineModel>>(RequestAcline);
            return RequestAcline;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<RequestACLineModel> GetData(SearchModel searchModel)
        {
            var RequestAcline = new RequestAcline();
            List<RequestAcline> RequestAClist = new List<RequestAcline>();
            RequestACLineModel RequestACLineModel = new RequestACLineModel();
            //RequestACLinesModel entryLinesModel = new RequestACLinesModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                    case "Last":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Next":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Previous":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).FirstOrDefault();
                        break;
                    case "Last":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).LastOrDefault();
                        break;
                    case "Next":
                        RequestAcline = _context.RequestAcline.OrderBy(o => o.RequestAcid).FirstOrDefault(s => s.RequestAcid > searchModel.Id);
                        break;
                    case "Previous":
                        RequestAcline = _context.RequestAcline.OrderByDescending(o => o.RequestAcid).FirstOrDefault(s => s.RequestAcid < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<RequestACLineModel>(RequestAcline);
            return result;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertRequestACLine")]
        public RequestACLineModel Post(RequestACLineModel value)
        {

            var RequestAcline = new RequestAcline
            {
                RequestAcid = value.RequestAcid,
                NavItemId = value.NavItemId,
                DistributorItemId = value.DistributorItemId,
                ShelfLife = value.ShelfLife,
                Avg12Months = value.Avg12Months,
                Avg6Months = value.Avg6Months,
                Avg3Months = value.Avg3Months,
                CurrentAc = value.CurrentAc,
                DifferenceInAc = value.DifferenceInAc,
                ProposedNewAc = value.ProposedNewAc,
                PercentDifference = value.PercentDifference,
                Comments = value.Comments,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.RequestAcline.Add(RequestAcline);
            _context.SaveChanges();
            value.RequestAcid = RequestAcline.RequestAcid;



            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateRequestACLine")]
        public async Task<RequestACLineModel> Put(RequestACLineModel value)
        {

            var RequestAcline = _context.RequestAcline.SingleOrDefault(p => p.RequestAclineId == value.RequestAclineId);

            RequestAcline.ModifiedDate = DateTime.Now;
            RequestAcline.RequestAcid = value.RequestAcid;
            RequestAcline.DistributorItemId = value.DistributorItemId;
            RequestAcline.NavItemId = value.NavItemId;
            RequestAcline.ShelfLife = value.ShelfLife;
            RequestAcline.Avg12Months = value.Avg12Months;
            RequestAcline.Avg6Months = value.Avg6Months;
            RequestAcline.Avg3Months = value.Avg3Months;
            RequestAcline.CurrentAc = value.CurrentAc;
            RequestAcline.DifferenceInAc = value.DifferenceInAc;
            RequestAcline.ProposedNewAc = value.ProposedNewAc;
            RequestAcline.PercentDifference = value.PercentDifference;
            RequestAcline.ModifiedByUserId = value.ModifiedByUserID;
            RequestAcline.StatusCodeId = value.StatusCodeID.Value;
            RequestAcline.Comments = value.Comments;

            _context.SaveChanges();

            var requestAcLineItems = _context.RequestAcline.Where(p => p.RequestAcid == value.RequestAcid).ToList();

            if (requestAcLineItems.All(r => r.StatusCodeId == 701))
            {
                var requestAcItem = _context.RequestAcline.FirstOrDefault(p => p.RequestAcid == value.RequestAcid);
                _context.SaveChanges();
                if (value.StatusCodeID == 701)
                {
                    await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Approved");
                }
                else if (value.StatusCodeID == 702)
                {
                    await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Rejected");
                }
            }
            else
            {
                if (value.StatusCodeID == 701)
                {
                    await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Approved");
                }
                else if (value.StatusCodeID == 702)
                {
                    await _hub.Clients.Group(value.PicId.ToString()).SendAsync("requestACNotification", "Rejected");
                }
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteRequestAC")]
        public void Delete(int id)
        {
            var RequestAcline = _context.RequestAcline.SingleOrDefault(p => p.RequestAcid == id);
            if (RequestAcline != null)
            {

                _context.RequestAcline.Remove(RequestAcline);
                _context.SaveChanges();
            }
        }

        [HttpGet]
        [Route("GetIncreaseDecreaseLines")]
        public RequestACResponse GetIncreaseDecreaseLines(int id)
        {
            var requestACLineModels = _context.RequestAcline.Include("AddedByUser").Include("ModifiedByUser").Include("NavItem").Include(n => n.RequestAc).Select(s => new RequestACLineModel
            {
                RequestAcid = s.RequestAcid,
                RequestAclineId = s.RequestAclineId,
                NavItemId = s.NavItemId,
                DistributorItemId = s.DistributorItemId,
                ShelfLife = s.ShelfLife,
                Avg12Months = s.Avg12Months,
                Avg6Months = s.Avg6Months,
                Avg3Months = s.Avg3Months,
                CurrentAc = s.CurrentAc,
                ProposedNewAc = s.ProposedNewAc,
                DifferenceInAc = s.DifferenceInAc,
                PercentDifference = s.PercentDifference,
                Comments = s.Comments,
                ItemName = s.NavItem.No,
                PackSize = s.NavItem.PackSize,
                CompanyId = s.RequestAc.CompanyId,
                CustomerId = s.RequestAc.CustomerId,
                CompanyName = s.RequestAc.Company != null ? s.RequestAc.Company.PlantCode : "",
                CustomerName = s.RequestAc.Customer != null ? s.RequestAc.Customer.CompanyName : "",

                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,


            }).OrderByDescending(o => o.RequestAcid).AsNoTracking().Where(r => r.RequestAcid == id).ToList();
            return new RequestACResponse { IncreaseACItems = requestACLineModels.Where(i => i.DifferenceInAc >= 0).ToList(), DecreaseACItems = requestACLineModels.Where(i => i.DifferenceInAc < 0).ToList() };
        }
    }
}