﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApptransferRelcassEntryController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApptransferRelcassEntryController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetApptransferReclassEntrys")]
        public List<ApptransferRelcassEntryModel> Get()
        {
            var apptransferrelcassentry = _context.ApptransferRelcassEntry.Include("ModifiedByUser").Include("TransferFromLocation").Include("TransferToLocation").Select(s => new ApptransferRelcassEntryModel
            {
                //Changes Done by Aravinth 18JUL2019 10.15 AM Start

                TransferReclassID = s.TransferReclassId,
                TransferFromLocationID = s.TransferFromLocationId,
                TransferFromAreaID = s.TransferFromAreaId,
                TransferFromSpecificAreaID = s.TransferFromSpecificAreaId,
                TransferToLocationID = s.TransferToLocationId,
                TransferToAreaID = s.TransferToAreaId,
                TransferToSpecificAreaID = s.TransferToSpecificAreaId,
                TransferTypeID = s.TransferTypeId,
                TransferFromLocationName = s.TransferFromLocation.Name,
                TransferToLocationName = s.TransferToLocation.Name,

                //Changes Done by Aravinth 18JUL2019 10.45 AM End

                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.TransferReclassID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<ApptransferRelcassEntryModel>>(ApptransferRelcassEntry);
            return apptransferrelcassentry;
        }

        //Changes Done by Aravinth Start 18JUL2019

        [HttpPost]
        [Route("GetApptransferRelcassEntryFilter")]
        public List<ApptransferRelcassEntryModel> GetFilter(ApptransferRelcassEntrySearchModel value)
        {
            var appTransferReclassEntry = _context.ApptransferRelcassEntry.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new ApptransferRelcassEntryModel
            {
                TransferReclassID = s.TransferReclassId,
                TransferFromLocationName = s.TransferFromLocation.Name,
                TransferToLocationName = s.TransferToLocation.Name,

            }).OrderByDescending(o => o.TransferReclassID).AsNoTracking().ToList();

            List<ApptransferRelcassEntryModel> filteredItems = new List<ApptransferRelcassEntryModel>();
            if (value.TransferFromLocationName.Any())
            {
                var itemTransferFromLocationNameFilters = appTransferReclassEntry.Where(p => p.TransferFromLocationName != null ? (p.TransferFromLocationName.ToLower() == (value.TransferFromLocationName.ToLower())) : false).ToList();
                itemTransferFromLocationNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.TransferReclassID == i.TransferReclassID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.TransferToLocationName.Any())
            {
                var itemTransferToLocationNameFilters = appTransferReclassEntry.Where(p => p.TransferToLocationName != null ? (p.TransferToLocationName.ToLower() == (value.TransferToLocationName.ToLower())) : false).ToList();
                itemTransferToLocationNameFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.TransferReclassID == i.TransferReclassID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }

            return filteredItems;
        }

        [HttpGet]
        [Route("GetApptransferRelcassEntryItemByID")]
        public ApptransferRelcassEntryModel GetApptransferRelcassEntryItemByID(long Id)
        {
            var appTransferReclassEntry = _context.ApptransferRelcassEntry.Select(s => new ApptransferRelcassEntryModel
            {

                TransferReclassID = s.TransferReclassId,
                TransferFromLocationID = s.TransferFromLocationId,
                TransferFromAreaID = s.TransferFromAreaId,
                TransferFromSpecificAreaID = s.TransferFromSpecificAreaId,
                TransferToLocationID = s.TransferToLocationId,
                TransferToAreaID = s.TransferToAreaId,
                TransferToSpecificAreaID = s.TransferToSpecificAreaId,
                TransferTypeID = s.TransferTypeId,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.TransferReclassID).FirstOrDefault(t => t.TransferReclassID == Id);


            var transferRelcassLineDetails = _context.ApptransferRelcassLines.Where(d => d.TransferRelcassId == appTransferReclassEntry.TransferReclassID).AsNoTracking().ToList();

            if (transferRelcassLineDetails != null)
            {
                transferRelcassLineDetails.ForEach(dd =>
                {
                    appTransferReclassEntry.ApptransferRelcassLinesDetailModels.Add(new ApptransferRelcassLinesModel
                    {
                        ApptransferRelcassLineID = dd.ApptransferRelcassLineId,
                        TransferRelcassID = dd.TransferRelcassId,
                        DrumNo = dd.DrumNo,
                        ProdOrderNo = dd.ProdOrderNo,
                        ProdLineNo = dd.ProdLineNo,
                        ItemNo = dd.ItemNo,
                        Description = dd.Description,
                        LotNo = dd.LotNo,
                        QcrefNo = dd.QcrefNo,
                        BatchNo = dd.BatchNo,
                        Quantity = dd.Quantity.GetValueOrDefault(0)
                    });
                });
            }


            return appTransferReclassEntry;
        }

        //Changes Done by Aravinth End 18JUL2019

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApptransferRelcassEntryModel> GetData(SearchModel searchModel)
        {
            var apptransferrelcassentry = new ApptransferRelcassEntry();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).LastOrDefault();
                        break;
                    case "Previous":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).FirstOrDefault();
                        break;
                    case "Last":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).LastOrDefault();
                        break;
                    case "Next":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderBy(o => o.TransferReclassId).FirstOrDefault(s => s.TransferReclassId > searchModel.Id);
                        break;
                    case "Previous":
                        apptransferrelcassentry = _context.ApptransferRelcassEntry.OrderByDescending(o => o.TransferReclassId).FirstOrDefault(s => s.TransferReclassId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApptransferRelcassEntryModel>(apptransferrelcassentry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApptransferReclassEntry")]
        public ApptransferRelcassEntryModel Post(ApptransferRelcassEntryModel value)
        {
            try
            {
                value.IsError = false;
                value.Errormessage = "";
                var ICtLocation = _context.Ictmaster.FirstOrDefault(l => l.Name == value.FromLocation);
                if (ICtLocation != null)
                {
                    var apptransferrelcassentry = _context.ApptransferRelcassEntry.FirstOrDefault(p => p.TransferFromLocationId == ICtLocation.IctmasterId && p.AddedByUserId == value.AddedByUserID);
                    if (apptransferrelcassentry == null)
                    {
                        apptransferrelcassentry = new ApptransferRelcassEntry
                        {
                            TransferFromLocationId = ICtLocation.IctmasterId,
                            //TransferFromSpecificAreaId = value.TransferFromSpecificAreaID,
                            //TransferToLocationId = value.TransferToLocationID,                       
                            TransferTypeId = 651,
                            StatusCodeId = 1,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.ApptransferRelcassEntry.Add(apptransferrelcassentry);
                    }
                    else
                    {
                        apptransferrelcassentry.TransferFromLocationId = ICtLocation.IctmasterId;
                        //apptransferrelcassentry.TransferToLocationId = value.TransferToLocationID;                 
                        apptransferrelcassentry.TransferTypeId = 651;
                        apptransferrelcassentry.ModifiedByUserId = value.AddedByUserID;
                        apptransferrelcassentry.ModifiedDate = value.ModifiedDate;
                        apptransferrelcassentry.StatusCodeId = 1;
                    }
                    _context.SaveChanges();

                    value.TransferReclassID = apptransferrelcassentry.TransferReclassId;
                    return value;
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "From location is not valid. It is not exist in ICT Master.";
                    return value;
                }
            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApptransferReclassEntry")]
        public ApptransferRelcassEntryModel Put(ApptransferRelcassEntryModel value)
        {
            var apptransferrelcassentry = _context.ApptransferRelcassEntry.SingleOrDefault(p => p.TransferReclassId == value.TransferReclassID);

            apptransferrelcassentry.TransferFromLocationId = value.TransferFromLocationID;
            apptransferrelcassentry.TransferFromAreaId = value.TransferFromAreaID;
            apptransferrelcassentry.TransferFromSpecificAreaId = value.TransferFromSpecificAreaID;
            apptransferrelcassentry.TransferToLocationId = value.TransferToLocationID;
            apptransferrelcassentry.TransferToAreaId = value.TransferToAreaID;
            apptransferrelcassentry.TransferToSpecificAreaId = value.TransferToSpecificAreaID;
            apptransferrelcassentry.TransferTypeId = value.TransferTypeID;

            apptransferrelcassentry.ModifiedByUserId = value.ModifiedByUserID;
            apptransferrelcassentry.ModifiedDate = value.ModifiedDate;
            apptransferrelcassentry.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();

            //Changes Done by Aravinth Start 18JUL2019

            if (value.ApptransferRelcassLinesDetailModels.Count != 0)
            {
                var apptransferRelcassLinesDetailModels = value.ApptransferRelcassLinesDetailModels;
                apptransferRelcassLinesDetailModels.ForEach(apptransferRelcassLines =>
                {
                    if (apptransferRelcassLinesDetailModels.Count > 0)
                    {
                        var apptransferRelcassLineEntry = _context.ApptransferRelcassLines.SingleOrDefault(p => p.ApptransferRelcassLineId == apptransferRelcassLines.ApptransferRelcassLineID);
                        apptransferRelcassLineEntry.TransferRelcassId = value.TransferReclassID;
                        apptransferRelcassLineEntry.DrumNo = apptransferRelcassLines.DrumNo;
                        apptransferRelcassLineEntry.ProdOrderNo = apptransferRelcassLines.ProdOrderNo;
                        apptransferRelcassLineEntry.ProdLineNo = apptransferRelcassLines.ProdLineNo;
                        apptransferRelcassLineEntry.ItemNo = apptransferRelcassLines.ItemNo;
                        apptransferRelcassLineEntry.Description = apptransferRelcassLines.Description;
                        apptransferRelcassLineEntry.LotNo = apptransferRelcassLines.LotNo;
                        apptransferRelcassLineEntry.QcrefNo = apptransferRelcassLines.QcrefNo;
                        apptransferRelcassLineEntry.BatchNo = apptransferRelcassLines.BatchNo;
                        apptransferRelcassLineEntry.Quantity = apptransferRelcassLines.Quantity;

                        _context.SaveChanges();
                    }
                });
            }

            return value;

            //Changes Done by Aravinth End 18JUL2019
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApptransferReclassEntry")]
        public void Delete(int id)
        {
            var appTransferRelcassEntryLines = _context.ApptransferRelcassLines.Where(p => p.TransferRelcassId == id);
            var apptransferrelcassentry = _context.ApptransferRelcassEntry.SingleOrDefault(p => p.TransferReclassId == id);
            var errorMessage = "";
            try
            {
                if (apptransferrelcassentry != null)
                {
                    _context.ApptransferRelcassLines.RemoveRange(appTransferRelcassEntryLines);
                    _context.ApptransferRelcassEntry.Remove(apptransferrelcassentry);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        [HttpPost]
        [Route("GetTransferOrderExist")]
        public ApptransferRelcassEntryModel GetTransferOrderExist(ApptransferRelcassEntryModel value)
        {
            try
            {
                value.IsError = false;
                value.Errormessage = "";
                var apptransferRelcassEntry = _context.ApptransferRelcassEntry.FirstOrDefault(l => l.TransferOrderNo == value.TransferOrderNo);
                if (apptransferRelcassEntry != null)
                {
                    value.TransferReclassID = apptransferRelcassEntry.TransferReclassId;
                    return value;
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "Invalid Transfer Order";
                    return value;
                }
            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return value;
            }
        }
    }
}