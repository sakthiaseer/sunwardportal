﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DocumentLinkController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public DocumentLinkController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hostingEnvironment = host;
        }

        [HttpPost()]
        [Route("GetDocumentDirectory")]
        public List<DocumentsModel> GetDocumentDirectory(SearchModel searchModel)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();

            var documentsModel = _context.Documents
                                                        .Select(s =>
            new
            {
                s.SessionId,
                s.DocumentId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.ProfileNo,
                //FileProfileTypeName = s.FilterProfileType.Name,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                s.AddedByUser,
                s.ModifiedByUser,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.DepartmentId,
                s.WikiId,
                s.Extension,
                s.CategoryId,
                s.DocumentType,
                s.DisplayName,
                s.LinkId,
                s.IsSpecialFile,
                s.IsTemp,
                s.ReferenceNumber,
                s.Description,
                s.StatusCodeId,
                s.IsCompressed,
                s.IsVideoFile,
                s.CloseDocumentId,
                s.IsLatest,
                s.TaskId
            }).Where(s => s.IsLatest == true || s.IsLatest == null).ToList();
            var documentFolder = _context.DocumentFolder.Select(s => new { s.FolderId, s.DocumentId, s.IsLatest }).Where(w => w.FolderId > 0 && w.IsLatest == true).ToList();
            var folderids = documentFolder?.Select(s => s.FolderId).Distinct().ToList();
            List<long?> existedfolderIds = new List<long?>();
            //var folderlist = _context.Folders.AsNoTracking().ToList();
            if (searchModel.Action == "Public Folder" && searchModel.FolderId != null)
            {

                if (searchModel.FolderId != null)
                {
                    documentFolder = documentFolder?.Where(w => w.FolderId == searchModel.FolderId).ToList();
                }
                var documentIds = documentFolder?.Select(s => s.DocumentId).Distinct().ToList();
                documentsModel = documentsModel.Where(w => documentIds.Contains(w.DocumentId)).Distinct().ToList();
            }
            else if (searchModel.Action == "Public Folder" && searchModel.FolderId == null)
            {
                //var documentFolder = _context.DocumentFolder.Select(s => new { s.FolderId, s.DocumentId }).Where(w => w.FolderId > 0);

                var documentIds = documentFolder?.Select(s => s.DocumentId).Distinct().ToList();
                documentsModel = documentsModel.Where(w => documentIds.Contains(w.DocumentId)).Distinct().ToList();
            }
            if (searchModel.Action == "File Profile" && searchModel.FileProfileTypeId != null)
            {
                documentsModel = documentsModel.Where(f => f.IsLatest == true).Distinct().ToList();
                if (searchModel.FileProfileTypeId != null)
                {
                    documentsModel = documentsModel.Where(f => f.FilterProfileTypeId == searchModel.FileProfileTypeId).Distinct().ToList();
                }
            }
            if (searchModel.Action == "File Profile" && searchModel.FileProfileTypeId == null)
            {
                documentsModel = documentsModel.Where(f => f.FilterProfileTypeId != null).Distinct().ToList();
                //documentsModel = documentsModel.Where(f => f.FilterProfileTypeId == searchModel.FileProfileTypeId);
            }
            if (searchModel.FileName != null && searchModel.FileName != "")
            {
                documentsModel = documentsModel.Where(f => f.FileName.Contains(searchModel.FileName)).Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(searchModel.ContentType))
            {
                documentsModel = documentsModel.Where(f => f.ContentType == searchModel.ContentType).Distinct().ToList();
            }

            if (searchModel.FromMonth != null && searchModel.ToMonth != null)
            {
                documentsModel = documentsModel.Where(f => f.AddedDate > searchModel.FromMonth && f.AddedDate <= searchModel.ToMonth).Distinct().ToList();
            }

            // var query = await documentsModel.GetPaged(searchModel.PageCount, searchModel.PageSize, searchModel.SortCol, searchModel.SortBy, searchModel.FilterFields, searchModel.SearchString);

            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            documentsModel.ForEach(s =>
            {
                DocumentsModel documentsModel = new DocumentsModel();

                documentsModel.DocumentID = s.DocumentId;
                documentsModel.DepartmentID = s.DepartmentId;
                documentsModel.WikiID = s.WikiId;
                documentsModel.FileName = s.FileName;
                documentsModel.Extension = s.Extension;
                documentsModel.ContentType = s.ContentType;
                documentsModel.CategoryID = s.CategoryId;
                documentsModel.DocumentType = s.DocumentType;
                documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                documentsModel.UploadDate = s.UploadDate;
                documentsModel.DisplayName = s.DisplayName;
                documentsModel.SessionID = s.SessionId;
                documentsModel.LinkID = s.LinkId;
                documentsModel.IsSpecialFile = s.IsSpecialFile;
                documentsModel.IsTemp = s.IsTemp;
                documentsModel.IsCompressed = s.IsCompressed;
                //Changes Done by Aravinth
                documentsModel.ReferenceNumber = s.ReferenceNumber;
                documentsModel.Description = s.Description;
                documentsModel.ModifiedByUserID = s.ModifiedByUserId;
                documentsModel.AddedByUserID = s.AddedByUserId;
                documentsModel.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "";
                documentsModel.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "";
                documentsModel.StatusCodeID = s.StatusCodeId;
                documentsModel.StatusCode = "Active";
                documentsModel.AddedDate = s.UploadDate;
                documentsModel.ModifiedDate = s.ModifiedDate;
                documentsModel.IsVideoFile = s.IsVideoFile;
                documentsModel.ProfileNo = s.ProfileNo;
                //documentsModel.FileProfileTypeName = s.FileProfileTypeName;
                //documentsModel.CloseStatus = s.CloseStatus;
                documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                documentsModel.TaskId = s.TaskId;
                documentsModel.Type = "Document";
                var folderid = documentFolder?.Where(f => f.DocumentId == s.DocumentId).Select(t => t.FolderId).FirstOrDefault();


                List<FoldersModel> folderModels = new List<FoldersModel>();

                if (folderid != null)
                {

                    folderModels = GetPublicFolderMainPath(folderid);
                    if (folderModels != null)
                    {
                        documentsModel.DocumentPath = folderModels.FirstOrDefault(d => d.FolderID == folderid)?.FolderPath;
                    }

                }

                else if (s.FilterProfileTypeId != null)
                {
                    List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
                    fileProfileTypeModels = GetFileProfileTypePath(s.FilterProfileTypeId);
                    if (fileProfileTypeModels != null)
                    {
                        documentsModel.DocumentPath = fileProfileTypeModels.FirstOrDefault(d => d.FileProfileTypeId == s.FilterProfileTypeId).FileProfilePath;
                    }
                }



                documentsModels.Add(documentsModel);

            });
            return documentsModels.OrderByDescending(d => d.AddedDate).Distinct().ToList();
        }

        [HttpGet]
        [Route("GetDocumentLinkByDocumentId")]
        public List<DocumentLinkModel> GetDocumentLinkByDocumentId(int id, string wikiStatus)
        {

            List<DocumentLinkModel> DocumentLinkModels = new List<DocumentLinkModel>();
            DocumentLinkModel DocumentLinkModel = new DocumentLinkModel();
            var fileProfileList = _context.FileProfileType.AsNoTracking().ToList();
            var DocumentLink = _context.DocumentLink.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.DocumentId == id).AsNoTracking().ToList();
            var DocumentLinkDocumentId = DocumentLink.Select(n => n.DocumentId).ToList();
            var linkdocIds = DocumentLink.Select(n => n.LinkDocumentId).ToList();
            if (linkdocIds != null && linkdocIds.Count > 0)
            {
                DocumentLinkDocumentId.AddRange(linkdocIds);
            }
            var documentLinkDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FilePath = e.FilePath, FileName = e.FileName, ContentType = e.ContentType, Type = e.TableName, FileSize = e.FileSize, FilterProfileTypeId = e.FilterProfileTypeId, FolderId = e.FolderId, SessionID = e.SessionId }).Where(n => DocumentLinkDocumentId.Contains(n.DocumentId)).ToList();
            var appWikiLinkDraftDoc = _context.AppWikiLinkDraftDoc.Where(w => w.Type == wikiStatus).Select(s => s.DocumentLinkId).ToList();
            if (DocumentLink != null && DocumentLink.Count > 0)
            {
                DocumentLink.ForEach(s =>
                {
                    bool statuss = true;
                    if (wikiStatus == "Draft" || wikiStatus == "Wiki")
                    {
                        statuss = appWikiLinkDraftDoc.Any(cus => cus == s.DocumentLinkId);
                    }
                    if (statuss == true)
                    {
                        //var document = DocumentLinkDocumentItems.FirstOrDefault(n => n.DocumentId == s.DocumentId);
                        DocumentLinkModel = new DocumentLinkModel();

                        DocumentLinkModel.DocumentLinkId = s.DocumentLinkId;
                        DocumentLinkModel.DocumentID = s.LinkDocumentId;
                        DocumentLinkModel.LinkDocumentId = s.LinkDocumentId;
                        DocumentLinkModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                        DocumentLinkModel.StatusCode = s.StatusCode?.CodeValue;
                        DocumentLinkModel.AddedByUserID = s.AddedByUserId;
                        DocumentLinkModel.ModifiedByUserID = s.ModifiedByUserId;
                        DocumentLinkModel.StatusCodeID = s.StatusCodeId;
                        DocumentLinkModel.AddedDate = s.AddedDate;
                        DocumentLinkModel.ModifiedDate = s.ModifiedDate;
                        DocumentLinkModel.DocumentId = s.DocumentId;
                        DocumentLinkModel.AddedByUser = s.AddedByUser?.UserName;
                        DocumentLinkModel.FileProfieTypeId = s.FileProfieTypeId;
                        DocumentLinkModel.FolderId = s.FolderId;
                        DocumentLinkModel.DocumentPath = s.DocumentPath;
                        DocumentLinkModel.SessionId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.SessionID;
                        var filesize = (long)Math.Round(Convert.ToDouble(documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileSize / 1024));
                        DocumentLinkModel.FileSize = filesize;
                        DocumentLinkModel.Title = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileName;
                        DocumentLinkModel.Type = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.Type;
                        DocumentLinkModel.ContentType = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.ContentType;
                        DocumentLinkModel.DocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FileName;
                        DocumentLinkModel.LinkDocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileName;
                        var FileProfileTypeId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FilterProfileTypeId;
                        DocumentLinkModel.FilePath = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FilePath;

                        var FolderId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FolderId;
                        if (FileProfileTypeId != null)
                        {
                            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
                            fileProfileTypeModels = GetFileProfileTypePath(FileProfileTypeId);
                            if (fileProfileTypeModels != null)
                            {
                                DocumentLinkModel.FullPath = fileProfileTypeModels.FirstOrDefault(d => d.FileProfileTypeId == FileProfileTypeId).FileProfilePath;
                                DocumentLinkModel.FileProfileTypeParentId = fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId != null ? fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId : FileProfileTypeId;
                                DocumentLinkModel.PathFileProfieTypeId = FileProfileTypeId;
                            }
                        }
                        if (FolderId != null)
                        {

                            List<FoldersModel> folderModels = new List<FoldersModel>();

                            folderModels = GetPublicFolderMainPath(s.FolderId);
                            if (folderModels != null)
                            {
                                DocumentLinkModel.FullPath = folderModels.FirstOrDefault(d => d.FolderID == FolderId)?.FolderPath;
                            }
                        }
                        DocumentLinkModels.Add(DocumentLinkModel);
                    }
                });

            }

            return DocumentLinkModels.OrderByDescending(d => d.AddedDate).ToList();
        }

        [HttpGet]
        [Route("GetParentDocumentsByLinkDocumentId")]
        public List<DocumentLinkModel> GetParentDocumentsByLinkDocumentId(int id)
        {

            List<DocumentLinkModel> DocumentLinkModels = new List<DocumentLinkModel>();
            DocumentLinkModel DocumentLinkModel = new DocumentLinkModel();
            var DocumentLink = _context.DocumentLink.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.LinkDocumentId == id).AsNoTracking().ToList();
            var DocumentLinkDocumentId = DocumentLink.Select(n => n.DocumentId).ToList();

            var linkdocIds = DocumentLink.Select(n => n.LinkDocumentId).ToList();
            if (linkdocIds != null && linkdocIds.Count > 0)
            {
                DocumentLinkDocumentId.AddRange(linkdocIds);
            }
            var documentLinkDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FilePath = e.FilePath, FileName = e.FileName, ContentType = e.ContentType, Type = e.TableName, FileSize = e.FileSize, FilterProfileTypeId = e.FilterProfileTypeId, FolderId = e.FolderId, SessionID = e.SessionId }).Where(n => DocumentLinkDocumentId.Contains(n.DocumentId)).ToList();

            if (DocumentLink != null && DocumentLink.Count > 0)
            {
                DocumentLink.ForEach(s =>
                {
                    //var document = DocumentLinkDocumentItems.FirstOrDefault(n => n.DocumentId == s.DocumentId);
                    DocumentLinkModel = new DocumentLinkModel();

                    DocumentLinkModel.DocumentLinkId = s.DocumentLinkId;
                    DocumentLinkModel.LinkDocumentId = s.LinkDocumentId;
                    DocumentLinkModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    DocumentLinkModel.StatusCode = s.StatusCode?.CodeValue;
                    DocumentLinkModel.AddedByUserID = s.AddedByUserId;
                    DocumentLinkModel.ModifiedByUserID = s.ModifiedByUserId;
                    DocumentLinkModel.StatusCodeID = s.StatusCodeId;
                    DocumentLinkModel.AddedDate = s.AddedDate;
                    DocumentLinkModel.ModifiedDate = s.ModifiedDate;
                    DocumentLinkModel.DocumentID = s.DocumentId;
                    DocumentLinkModel.DocumentId = s.DocumentId;
                    DocumentLinkModel.AddedByUser = s.AddedByUser?.UserName;
                    DocumentLinkModel.FileProfieTypeId = s.FileProfieTypeId;
                    DocumentLinkModel.FolderId = s.FolderId;
                    DocumentLinkModel.DocumentPath = s.DocumentPath;
                    DocumentLinkModel.SessionId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.SessionID;
                    var filesize = (long)Math.Round(Convert.ToDouble(documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileSize / 1024));
                    DocumentLinkModel.FileSize = filesize;
                    DocumentLinkModel.Title = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileName;
                    DocumentLinkModel.Type = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.Type;
                    DocumentLinkModel.ContentType = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.ContentType;
                    DocumentLinkModel.DocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FileName;
                    DocumentLinkModel.LinkDocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.LinkDocumentId)?.FileName;
                    var FileProfileTypeId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FilterProfileTypeId;
                    var FolderId = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FolderId;
                    DocumentLinkModel.FilePath = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == s.DocumentId)?.FilePath;
                    if (FileProfileTypeId != null)
                    {
                        List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
                        fileProfileTypeModels = GetFileProfileTypePath(FileProfileTypeId);
                        if (fileProfileTypeModels != null)
                        {
                            DocumentLinkModel.FullPath = fileProfileTypeModels.FirstOrDefault(d => d.FileProfileTypeId == FileProfileTypeId).FileProfilePath;
                            DocumentLinkModel.FileProfileTypeParentId = fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId != null ? fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId : FileProfileTypeId;
                            DocumentLinkModel.PathFileProfieTypeId = FileProfileTypeId;
                        }
                    }
                    if (FolderId != null)
                    {

                        List<FoldersModel> folderModels = new List<FoldersModel>();

                        folderModels = GetPublicFolderMainPath(s.FolderId);
                        if (folderModels != null)
                        {
                            DocumentLinkModel.FullPath = folderModels.FirstOrDefault(d => d.FolderID == FolderId)?.FolderPath;
                        }
                    }
                    DocumentLinkModels.Add(DocumentLinkModel);
                });

            }

            return DocumentLinkModels.OrderByDescending(d => d.AddedDate).ToList();
        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        [Route("UploadFromDocumentLink")]
        public IActionResult UploadFromDocumentLink(IFormCollection files)
        {
            long? addedByUserId = null;
            long? fileProfileTypeId = null;
            long? folderId = null;
            DateTime? expiryDate = new DateTime?();
            var sessionID = new Guid(files["sessionID"].ToString());
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            if (userExits > 0)
            {
                var userId = files["userId"].ToString();
                var documentPath = files["documentPath"].ToString();
                var wikiStatusType = files["wikiStatusType"].ToString();
                if (documentPath == null || documentPath == "")
                {
                    documentPath = "Compound Link Document";
                }
                if (userId != null)
                {
                    addedByUserId = Convert.ToInt64(userId);
                }
                var isHeaderImage = Convert.ToBoolean(files["isHeaderImage"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var fileProfile = files["fileProfileTypeId"].ToString();
                var link = files["link"].ToString();
                var expiry = files["expiryDate"].ToString();
                if (expiry != "null" && expiry != null && expiry != "")
                {
                    expiryDate = Convert.ToDateTime(files["expiryDate"].ToString());
                }
                else if (expiry == "null")
                {
                    expiryDate = null;
                }
                if (fileProfile != null && fileProfile != "null" && fileProfile != "" && fileProfile != "undefined")
                {
                    fileProfileTypeId = Convert.ToInt64(fileProfile);
                }
                var profileSelect = files["ProfileSelect"].ToString();
                var plantID = Convert.ToInt64(files["plantID"].ToString());
                long? departmentId = null;
                var departmentIds = Convert.ToInt64(files["departmentId"].ToString());
                if (departmentIds > 0)
                {
                    departmentId = departmentIds;
                }
                long? sectionId = null;
                var sectionIds = Convert.ToInt64(files["sectionId"].ToString());
                if (sectionIds > 0)
                {
                    sectionId = sectionIds;
                }
                long? subSectionId = null;
                var subSectionIds = Convert.ToInt64(files["subSectionId"].ToString());
                if (subSectionIds > 0)
                {
                    subSectionId = subSectionIds;
                }
                long? divisionId = null;
                var divisionIds = Convert.ToInt64(files["divisionId"].ToString());
                if (divisionIds > 0)
                {
                    divisionId = divisionIds;
                }
                var folder = 0;
                try
                {
                    folder = Convert.ToInt32(files["folderID"].ToString());
                }
                catch (System.FormatException)
                {
                    folder = 0; // or other default value as appropriate in context.
                }
                if (folder > 0)
                {
                    folderId = folder;
                }
                var screenID = files["screeenID"].ToString();
                var mainDocumentLinkID = Convert.ToInt32(files["documentID"].ToString());
                var description = files["description"].ToString();
                var mainDocumentTaskId = _context.Documents.Where(d => d.TaskId != null && d.TaskId > 0 && d.DocumentId == mainDocumentLinkID)?.FirstOrDefault()?.TaskId;
                List<long?> readWriteUserIds = new List<long?>();
                List<long?> readonlyUserIds = new List<long?>();
                if (mainDocumentTaskId == null)
                {
                    var taskAttach = _context.TaskAttachment.FirstOrDefault(d => d.DocumentId == mainDocumentLinkID)?.TaskMasterId;
                    if (taskAttach != null)
                    {
                        var task = _context.TaskMaster.FirstOrDefault(t => t.TaskId == taskAttach);
                        if (task != null)
                        {
                            readWriteUserIds = _context.TaskAssigned.Where(t => t.TaskId == task.TaskId).Select(s => s.UserId).ToList();
                            readonlyUserIds = _context.TaskMembers.Where(t => t.TaskId == task.TaskId).Select(s => s.AssignedCc).ToList();
                            readWriteUserIds.Add(task.OwnerId);
                            if (task.IsFromProfileDocument == true)
                            {
                                //mainDocumentTaskId = task?.TaskId;
                            }
                        }
                    }
                }

                if (mainDocumentTaskId != null)
                {
                    readWriteUserIds = _context.TaskAssigned.Where(t => t.TaskId == mainDocumentTaskId).Select(s => s.UserId).ToList();
                    readonlyUserIds = _context.TaskMembers.Where(t => t.TaskId == mainDocumentTaskId).Select(s => s.AssignedCc).ToList();
                    //readWriteUserIds.Add(task.OwnerId);
                }
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                string profileNo = "";
                if (profile != null)
                {
                    if (profileSelect == "ProfileSelect")
                    {
                        profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
                        {
                            ProfileID = profile.ProfileId,
                            AddedByUserID = addedByUserId,
                            StatusCodeID = 710,
                            DepartmentName = files["departmentName"].ToString(),
                            CompanyCode = files["companyCode"].ToString(),
                            SectionName = files["sectionName"].ToString(),
                            SubSectionName = files["subSectionName"].ToString(),
                            DepartmentId = departmentId,
                            PlantID = plantID,
                            SectionId = sectionId,
                            SubSectionId = subSectionId,
                            ScreenAutoNumberId = profile.FileProfileTypeId,
                        });
                    }
                    else
                    {
                        if (addedByUserId != null)
                        {
                            profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, AddedByUserID = addedByUserId, StatusCodeID = 710, });

                        }
                        else
                        {
                            profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name, StatusCodeID = 710, });
                        }
                    }
                }

                int idx = 0;
                files.Files.ToList().ForEach(f =>
                {
                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                    var file = f;
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    var documents = new Documents
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        ScreenId = screenID,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = addedByUserId,
                        AddedDate = DateTime.Now,
                        IsCompressed = true,
                        IsHeaderImage = isHeaderImage,
                        FileIndex = 0,
                        IsVideoFile = isVideoFiles,
                        IsMainTask = false,
                        Description = description,
                        ExpiryDate = expiryDate,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),

                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    var linkDocumentId = documents.DocumentId;
                    if (mainDocumentTaskId != null)
                    {
                        var taskAttach = new TaskAttachment
                        {
                            DocumentId = documents.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            IsMajorChange = false,
                            UploadedDate = DateTime.Now,
                            UploadedByUserId = addedByUserId,
                            VersionNo = "1",
                            TaskMasterId = mainDocumentTaskId,

                            //TaskMasterId = value.TaskID
                        };
                        _context.TaskAttachment.Add(taskAttach);
                        var notes = new Notes
                        {
                            Notes1 = file.FileName,
                            Link = link + "id=" + mainDocumentTaskId + "&taskid=" + mainDocumentTaskId,
                            DocumentId = documents.DocumentId,
                            SessionId = documents.SessionId,
                            AddedByUserId = addedByUserId.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 1,


                        };
                        _context.Notes.Add(notes);
                        _context.SaveChanges();
                        if (readonlyUserIds != null)
                        {
                            readonlyUserIds.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = true,
                                    IsReadWrite = false,
                                    UserId = i,
                                    DocumentId = documents.DocumentId,
                                };
                                _context.DocumentRights.Add(docAccess);
                            });
                        }
                        if (readWriteUserIds != null)
                        {
                            readWriteUserIds.ForEach(i =>
                            {
                                var docAccess = new DocumentRights
                                {
                                    IsRead = false,
                                    IsReadWrite = true,
                                    UserId = i,
                                    DocumentId = documents.DocumentId,

                                };
                                _context.DocumentRights.Add(docAccess);
                            });
                        }
                        _context.SaveChanges();
                    }

                    var DocumentLink = new DocumentLink
                    {
                        DocumentId = mainDocumentLinkID,
                        AddedByUserId = addedByUserId,
                        AddedDate = DateTime.Now,

                        LinkDocumentId = linkDocumentId,
                        FolderId = folderId,
                        FileProfieTypeId = fileProfileTypeId,
                        DocumentPath = documentPath,
                    };
                    _context.DocumentLink.Add(DocumentLink);
                    _context.SaveChanges();
                    if (wikiStatusType == "Draft" || wikiStatusType == "Wiki")
                    {
                        var mainSessionId = new Guid(files["mainSessionId"].ToString());
                        long? applicationWikiDraftId = null;
                        long? applicationWikiId = null;
                        if (wikiStatusType == "Draft")
                        {
                            applicationWikiDraftId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == mainSessionId)?.ApplicationWikiId;
                        }
                        if (wikiStatusType == "Wiki")
                        {
                            applicationWikiId = _context.ApplicationWiki.FirstOrDefault(f => f.SessionId == mainSessionId)?.ApplicationWikiId;
                        }
                        var AppWikiLinkDraftDoc = new AppWikiLinkDraftDoc
                        {
                            ApplicationWikiDraftId = applicationWikiDraftId,
                            ApplicationWikiId = applicationWikiId,
                            Type = wikiStatusType,
                            DocumentLinkId = DocumentLink.DocumentLinkId,
                        };
                        _context.AppWikiLinkDraftDoc.Add(AppWikiLinkDraftDoc);
                        _context.SaveChanges();
                    }
                    // }
                    /*else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        var compressedData = DocumentZipUnZip.Zip(document);//Compress(document);
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = compressedData,
                            FileSize = fs.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionID,
                            FilterProfileTypeId = fileProfileTypeId,
                            ScreenId = screenID,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedDate = DateTime.Now,
                            AddedByUserId = addedByUserId,
                            Description = description,
                            ExpiryDate = expiryDate,

                            IsCompressed = true,
                            IsHeaderImage = isHeaderImage,
                            FileIndex = 0,
                            IsMainTask = false,
                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                        var linkDocumentId = documents.DocumentId;

                        var DocumentLink = new DocumentLink
                        {
                            DocumentId = mainDocumentLinkID,
                            AddedByUserId = addedByUserId,
                            AddedDate = DateTime.Now,

                            LinkDocumentId = linkDocumentId,
                            FolderId = folderId,
                            FileProfieTypeId = fileProfileTypeId,
                            DocumentPath = documentPath,

                        };
                        _context.DocumentLink.Add(DocumentLink);
                        _context.SaveChanges();
                        if (wikiStatusType == "Draft" || wikiStatusType == "Wiki")
                        {
                            var mainSessionId = new Guid(files["mainSessionId"].ToString());
                            long? applicationWikiDraftId = null;
                            long? applicationWikiId = null;
                            if (wikiStatusType == "Draft")
                            {
                                applicationWikiDraftId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == mainSessionId)?.ApplicationWikiId;
                            }
                            if (wikiStatusType == "Wiki")
                            {
                                applicationWikiId = _context.ApplicationWiki.FirstOrDefault(f => f.SessionId == mainSessionId)?.ApplicationWikiId;
                            }
                            var AppWikiLinkDraftDoc = new AppWikiLinkDraftDoc
                            {
                                ApplicationWikiDraftId = applicationWikiDraftId,
                                ApplicationWikiId = applicationWikiId,
                                Type = wikiStatusType,
                                DocumentLinkId = DocumentLink.DocumentLinkId,
                            };
                            _context.AppWikiLinkDraftDoc.Add(AppWikiLinkDraftDoc);
                            _context.SaveChanges();
                        }
                        if (mainDocumentTaskId != null)
                        {
                            var taskAttach = new TaskAttachment
                            {
                                DocumentId = documents.DocumentId,
                                IsLatest = true,
                                IsLocked = false,
                                IsMajorChange = false,
                                UploadedDate = DateTime.Now,
                                UploadedByUserId = addedByUserId,
                                VersionNo = "1",
                                TaskMasterId = mainDocumentTaskId,
                                //TaskMasterId = value.TaskID
                            };
                            _context.TaskAttachment.Add(taskAttach);
                            var notes = new Notes
                            {
                                Notes1 = file.FileName,
                                Link = link + "id=" + mainDocumentTaskId + "&taskid=" + mainDocumentTaskId,
                                DocumentId = documents.DocumentId,
                                SessionId = documents.SessionId,
                                AddedByUserId = addedByUserId.Value,
                                AddedDate = DateTime.Now,
                                StatusCodeId = 1,


                            };
                            _context.Notes.Add(notes);
                            _context.SaveChanges();

                            if (readonlyUserIds != null)
                            {
                                readonlyUserIds.ForEach(i =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = true,
                                        IsReadWrite = false,
                                        UserId = i,
                                        DocumentId = documents.DocumentId,
                                    };
                                    _context.DocumentRights.Add(docAccess);
                                });
                            }
                            if (readWriteUserIds != null)
                            {
                                readWriteUserIds.ForEach(i =>
                                {
                                    var docAccess = new DocumentRights
                                    {
                                        IsRead = false,
                                        IsReadWrite = true,
                                        UserId = i,
                                        DocumentId = documents.DocumentId,

                                    };
                                    _context.DocumentRights.Add(docAccess);
                                });
                            }
                            _context.SaveChanges();

                        }
                        //if (folderId != null)
                        //{
                        //    var Documentfolder = new DocumentFolder
                        //    {
                        //        DocumentId = linkDocumentId,
                        //        UploadedByUserId = addedByUserId,
                        //        IsLatest = true,
                        //        FolderId = folderId,
                        //        PreviousDocumentId = mainDocumentLinkID,
                        //        UploadedDate = DateTime.Now,
                        //        IsLocked = false,
                        //        IsNoChange = false,
                        //        IsReleaseVersion = false,
                        //    };
                        //    _context.DocumentFolder.Add(Documentfolder);
                        //    _context.SaveChanges();
                        //}
                    }*/
                    idx++;
                });
                _context.SaveChanges();
            }
            return Content(sessionID.ToString());

        }
        [HttpPost()]
        [Route("GetFileProfilePathByDocumentNo")]
        public ActionResult<DocumentLinkModel> GetFileProfilePathByDocumentNo(DocumentNoSeriesModel documentNoSeriesModel)
        {
            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
            DocumentLinkModel DocumentLinkModel = new DocumentLinkModel();
            var documentNogeneration = _context.Documents.Where(d => d.ProfileNo.Contains(documentNoSeriesModel.DocumentNo)).Select(s => s.FilterProfileTypeId).ToList();
            if (documentNogeneration.Count > 0)
            {
                var fileProfileTypeId = _context.FileProfileType.Where(f => documentNogeneration.Contains(f.FileProfileTypeId) && f.ProfileId == documentNoSeriesModel.ProfileID).Select(f => f.FileProfileTypeId).FirstOrDefault();
                if (fileProfileTypeId > 0)
                {
                    fileProfileTypeModels = GetFileProfileTypePath(fileProfileTypeId);
                    if (fileProfileTypeModels != null)
                    {
                        DocumentLinkModel.FullPath = fileProfileTypeModels.FirstOrDefault(d => d.FileProfileTypeId == fileProfileTypeId).FileProfilePath;
                        DocumentLinkModel.FileProfileTypeParentId = fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId != null ? fileProfileTypeModels.FirstOrDefault()?.FileProfileTypeParentId : fileProfileTypeId;
                        DocumentLinkModel.PathFileProfieTypeId = fileProfileTypeId;
                    }
                }
            }
            return DocumentLinkModel;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertDocumentLink")]
        public DocumentLinkModel Post(DocumentLinkModel value)
        {
            try
            {
                var exist = _context.DocumentLink.FirstOrDefault(d => d.DocumentId == value.DocumentId && d.LinkDocumentId != null && d.LinkDocumentId == value.LinkDocumentId);
                if (exist == null)
                {
                    if (value.DocumentId != value.LinkDocumentId)
                    {
                        var DocumentLink = new DocumentLink
                        {
                            DocumentId = value.DocumentId,
                            AddedByUserId = value.AddedByUserID.Value,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            LinkDocumentId = value.LinkDocumentId,
                            FolderId = value.FolderId,
                            FileProfieTypeId = value.FileProfieTypeId,
                            DocumentPath = value.DocumentPath,

                        };
                        _context.DocumentLink.Add(DocumentLink);
                        _context.SaveChanges();
                        if (value.DocumentId > 0 || value.LinkDocumentId > 0)
                        {
                            var documentLinkDocumentItems = _context.Documents.Select(e => new { DocumentId = e.DocumentId, FileName = e.FileName, ContentType = e.ContentType }).Where(n => n.DocumentId == value.DocumentId || n.DocumentId == value.LinkDocumentId).ToList();
                            if (documentLinkDocumentItems != null)
                            {
                                value.DocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == value.DocumentId)?.FileName;
                                value.LinkDocumentName = documentLinkDocumentItems?.FirstOrDefault(d => d.DocumentId == value.LinkDocumentId)?.FileName;
                            }
                        }
                        value.AddedDate = DocumentLink.AddedDate;
                        value.DocumentLinkId = DocumentLink.DocumentLinkId;
                        if (value.WikiStatusType == "Draft" || value.WikiStatusType == "Wiki")
                        {
                            long? applicationWikiDraftId = null;
                            long? applicationWikiId = null;
                            if (value.WikiStatusType == "Draft")
                            {
                                applicationWikiDraftId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == value.MainSessionId)?.ApplicationWikiId;
                            }
                            if (value.WikiStatusType == "Wiki")
                            {
                                applicationWikiId = _context.ApplicationWiki.FirstOrDefault(f => f.SessionId == value.MainSessionId)?.ApplicationWikiId;
                            }
                            var AppWikiLinkDraftDoc = new AppWikiLinkDraftDoc
                            {
                                ApplicationWikiDraftId = applicationWikiDraftId,
                                ApplicationWikiId = applicationWikiId,
                                Type = value.WikiStatusType,
                                DocumentLinkId = value.DocumentLinkId,
                            };
                            _context.AppWikiLinkDraftDoc.Add(AppWikiLinkDraftDoc);
                            _context.SaveChanges();
                        }
                        if (value.AddedByUserID > 0)
                        {
                            value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
                        }
                        var tasksource = _context.TaskMaster.FirstOrDefault(t => t.SourceId != null && t.SourceId == value.DocumentId);
                        var documents = _context.Documents.FirstOrDefault(t => t.DocumentId == value.LinkDocumentId);
                        if (documents != null)
                        {
                            documents.IsMainTask = false;
                            _context.SaveChanges();
                        }
                        if (tasksource != null)
                        {

                            var taskAttachExist = _context.TaskAttachment.Where(t => t.TaskMasterId == tasksource.TaskId && t.DocumentId == value.LinkDocumentId)?.FirstOrDefault();
                            if (taskAttachExist == null)
                            {
                                var taskAttach = new TaskAttachment
                                {
                                    DocumentId = value.LinkDocumentId,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = value.AddedByUserID,
                                    VersionNo = "1",
                                    TaskMasterId = tasksource.TaskId,

                                    //TaskMasterId = value.TaskID
                                };
                                _context.TaskAttachment.Add(taskAttach);
                            }
                        }
                        else
                        {

                        }
                        _context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Same Document Cannot be link");

                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("This Document already linked", ex);
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDocumentLink")]
        public DocumentLinkModel Put(DocumentLinkModel value)
        {


            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDocumentLink")]
        public void Delete(int id)
        {
            var DocumentLink = _context.DocumentLink.SingleOrDefault(p => p.DocumentLinkId == id);
            if (DocumentLink != null)
            {

                var taskId = _context.TaskMaster.FirstOrDefault(d => d.SourceId == DocumentLink.DocumentId)?.TaskId;
                if (taskId != null)
                {
                    var taskatt = _context.TaskAttachment.FirstOrDefault(d => d.DocumentId == DocumentLink.LinkDocumentId && d.TaskMasterId == taskId);
                    if (taskatt != null)
                    {
                        _context.TaskAttachment.Remove(taskatt);
                        _context.SaveChanges();

                    }
                }
                _context.DocumentLink.Remove(DocumentLink);
                _context.SaveChanges();


            }
        }
        [HttpGet]
        [Route("GetPublicFolderMainPath")]
        public List<FoldersModel> GetPublicFolderMainPath(long? id)
        {
            var foldersListItem = _context.Folders.AsNoTracking().ToList();
            var foldersListItems = foldersListItem?.FirstOrDefault(t => t.FolderId == id);
            List<FoldersModel> foldersList = new List<FoldersModel>();
            if (foldersListItems != null)
            {

                List<string> FolderPathLists = new List<string>();
                FoldersModel foldersModel = new FoldersModel();
                foldersModel.FolderID = foldersListItems.FolderId;
                foldersModel.ParentFolderID = foldersListItems.ParentFolderId;
                foldersModel.MainFolderID = foldersListItems.MainFolderId;
                foldersModel.Name = foldersListItems.Name;
                foldersModel.Description = foldersListItems.Description;
                foldersModel.StatusCodeID = foldersListItems.StatusCodeId;
                foldersModel.AddedByUserID = foldersListItems.AddedByUserId;
                FolderPathLists = GetAllFolderName(foldersListItem, foldersListItems, FolderPathLists);
                FolderPathLists.Add(foldersListItems.Name);
                foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                foldersList.Add(foldersModel);

            }
            return foldersList;
        }
        private List<string> GetAllFolderName(List<Folders> foldersListItems, Folders s, List<string> foldersModel)
        {
            if (s.ParentFolderId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderId == s.ParentFolderId);
                GetAllFolderName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }

        [HttpGet]
        [Route("GetFileProfileTypePath")]
        public List<FileProfileTypeModel> GetFileProfileTypePath(long? id)
        {
            var foldersListItem = _context.FileProfileType.AsNoTracking().ToList();
            var foldersListItems = foldersListItem.Where(t => t.FileProfileTypeId == id).ToList();
            FileProfileTypeModel fileprofile = new FileProfileTypeModel();
            List<FileProfileTypeModel> foldersList = new List<FileProfileTypeModel>();
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                foldersListItems.ForEach(s =>
                {
                    List<string> FolderPathLists = new List<string>();
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel();
                    fileProfileTypeModel.FileProfileTypeId = s.FileProfileTypeId;
                    // fileProfileTypeModel.ParentId = s.ParentId;

                    fileProfileTypeModel.Name = s.Name;

                    fileProfileTypeModel.AddedByUserID = s.AddedByUserId;

                    fileprofile = GetProfileTypeName(foldersListItem, s, FolderPathLists, fileprofile);
                    FolderPathLists.Add(s.Name);
                    fileProfileTypeModel.FileProfileTypeParentId = fileprofile.ParentId;
                    fileProfileTypeModel.FileProfilePath = string.Join(" / ", FolderPathLists);
                    foldersList.Add(fileProfileTypeModel);
                });
            }
            return foldersList;
        }
        private FileProfileTypeModel GetProfileTypeName(List<FileProfileType> foldersListItems, FileProfileType s, List<string> foldersModel, FileProfileTypeModel fileProfileTypeModel)
        {

            if (s.ParentId != null)
            {

                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetProfileTypeName(foldersListItems, items, foldersModel, fileProfileTypeModel);
                foldersModel.Add(items.Name);
            }
            else if (s.ParentId == null)
            {
                fileProfileTypeModel.ParentId = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.FileProfileTypeId;
            }
            return fileProfileTypeModel;
        }
    }
}
