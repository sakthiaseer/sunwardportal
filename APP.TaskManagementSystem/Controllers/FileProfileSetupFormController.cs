﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FileProfileSetupFormController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public FileProfileSetupFormController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFileProfileSetupFormsById")]
        public List<FileProfileSetupFormModel> Get(long? id)
        {
            List<FileProfileSetupFormModel> FileProfileSetupFormModels = new List<FileProfileSetupFormModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var FileProfileSetupFormlist = _context.FileProfileSetupForm
                .Include(s => s.AddedByUser).Include(s => s.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.ControlType).Where(d => d.FileProfileTypeId == id).OrderBy(o => o.FileProfileSetupFormId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel
            var dropDownvalus = _context.FileProfileSetUpDropDown.ToList();
            if (FileProfileSetupFormlist != null && FileProfileSetupFormlist.Count > 0)
            {
                FileProfileSetupFormlist.ForEach(s =>
                {

                    FileProfileSetupFormModel FileProfileSetupFormModel = new FileProfileSetupFormModel
                    {
                        FileProfileSetupFormId = s.FileProfileSetupFormId,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ControlTypeId = s.ControlTypeId,
                        ControlTypeValue = s.ControlTypeValue,
                        IsDefault = s.IsDefault,
                        DefaultValue = s.DefaultValue,
                        Date = s.Date,
                        ControlType = s.ControlType?.CodeValue,
                        StatusCode = s.StatusCode?.CodeValue,
                        FileProfileTypeId = s.FileProfileTypeId,
                        DropDownValues = dropDownvalus.Where(d => d.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(d => d.DisplayValue).ToList(),
                        IsMultiple = s.IsMultiple,
                        IsRequired = s.IsRequired,
                        Placeholder = s.Placeholder,
                        RequiredMessage = s.RequiredMessage,
                        DropDownTypeId = s.DropDownTypeId,
                        DataSourceId = s.DataSourceId,
                        PropertyName = s.ControlType?.CodeValue.ToLower() + '_' + s.FileProfileSetupFormId,
                        ProductActivityCaseId = s.ProductActivityCaseId,
                        DropDownItems = dropDownvalus.Where(w => w.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(tn => new DropDownNameItems { Value = tn.DisplayValue, Text = tn.DisplayValue }).ToList(),
                    };
                    FileProfileSetupFormModels.Add(FileProfileSetupFormModel);
                });
            }



            return FileProfileSetupFormModels;
        }

        [HttpGet]
        [Route("GetProductActivityCaseSetupFormsById")]
        public List<FileProfileSetupFormModel> GetProductActivityCaseSetupFormsById(long? id)
        {
            List<FileProfileSetupFormModel> FileProfileSetupFormModels = new List<FileProfileSetupFormModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var FileProfileSetupFormlist = _context.FileProfileSetupForm
                .Include(s => s.AddedByUser).Include(s => s.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.ControlType).Where(d => d.ProductActivityCaseId == id).OrderBy(o => o.FileProfileSetupFormId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel
            var dropDownvalus = _context.FileProfileSetUpDropDown.ToList();
            if (FileProfileSetupFormlist != null && FileProfileSetupFormlist.Count > 0)
            {
                FileProfileSetupFormlist.ForEach(s =>
                {

                    FileProfileSetupFormModel FileProfileSetupFormModel = new FileProfileSetupFormModel
                    {
                        FileProfileSetupFormId = s.FileProfileSetupFormId,
                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ControlTypeId = s.ControlTypeId,
                        ControlTypeValue = s.ControlTypeValue,
                        IsDefault = s.IsDefault,
                        DefaultValue = s.DefaultValue,
                        Date = s.Date,
                        ControlType = s.ControlType?.CodeValue,
                        StatusCode = s.StatusCode?.CodeValue,
                        FileProfileTypeId = s.FileProfileTypeId,
                        DropDownValues = dropDownvalus.Where(d => d.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(d => d.DisplayValue).ToList(),
                        IsMultiple = s.IsMultiple,
                        IsRequired = s.IsRequired,
                        Placeholder = s.Placeholder,
                        RequiredMessage = s.RequiredMessage,
                        DropDownTypeId = s.DropDownTypeId,
                        DataSourceId = s.DataSourceId,
                        PropertyName = s.ControlType?.CodeValue.ToLower() + '_' + s.FileProfileSetupFormId,
                        ProductActivityCaseId = s.ProductActivityCaseId,
                        DropDownItems = dropDownvalus.Where(w => w.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(tn => new DropDownNameItems { Value = tn.DisplayValue, Text = tn.DisplayValue }).ToList(),
                    };
                    FileProfileSetupFormModels.Add(FileProfileSetupFormModel);
                });
            }



            return FileProfileSetupFormModels;
        }
        [HttpGet]
        [Route("GetValidFileProfileIds")]
        public FileProfileType GetValidFileProfileIds(long? id)
        {
            var fileProfileType = _context.FileProfileType.Where(w => w.FileProfileTypeId == id).FirstOrDefault();
            return fileProfileType;
        }
        [HttpGet]
        [Route("GetValidFileProfileSetUpFromIds")]
        public bool GetValidFileProfileSetUpFromIds(long? id)
        {
            if (id == null)
                return false;
            var fileProfileType = _context.FileProfileSetupForm.Where(w => w.FileProfileTypeId == id).Count();
            return fileProfileType > 0 ? true : false;
        }

        [HttpGet]
        [Route("GetValidProductActivtySetUpFromIds")]
        public bool GetValidProductActivtySetUpFromIds(long? id)
        {
            if (id == null)
                return false;
            var productActivityCase = _context.FileProfileSetupForm.Where(w => w.ProductActivityCaseId == id).Count();
            return productActivityCase > 0 ? true : false;
        }
        [HttpGet]
        [Route("GetFileProfileSetupFormsByIds")]
        public List<FileProfileSetupFormModel> GetFileProfileSetupFormsByIds(long? id)
        {
            List<FileProfileSetupFormModel> FileProfileSetupFormModels = new List<FileProfileSetupFormModel>();
            // var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();

            var FileProfileSetupFormlist = _context.FileProfileSetupForm
                .Include(s => s.AddedByUser).Include(s => s.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.ControlType).Where(d => d.FileProfileTypeId == id).OrderBy(o => o.FileProfileSetupFormId).AsNoTracking().ToList();//.Select(s => new BompackingMasterModel
            var dropDownvalus = _context.FileProfileSetUpDropDown.ToList();
            if (FileProfileSetupFormlist != null && FileProfileSetupFormlist.Count > 0)
            {
                string sqlQuery = string.Empty;
                sqlQuery = "Select  * from view_GetEmployee where Status='active'";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.Gender,
                    s.DepartmentName,
                    s.PlantID,
                    EmployeeName = s.FirstName + " " + (!string.IsNullOrEmpty(s.LastName) ? ("|" + s.LastName) : "") + (!string.IsNullOrEmpty(s.NickName) ? ("|" + s.NickName) : "")
                }).ToList();
                var navItems = _context.Navitems.Where(w => w.StatusCodeId == 1).Select(s => new { s.ItemId, s.No, s.Description }).AsNoTracking().ToList();
                var vendor = _context.VendorsList.Where(w => w.StatusCodeId == 1).Select(s => new { s.VendorsListId, s.VendorNo, s.Name }).ToList();
                var customer = _context.Navcustomer.Where(w => w.StatusCodeId == 1).Select(s => new { s.CustomerId, s.Code, s.Name }).ToList();
                var navProductCode = _context.NavproductCode.Where(w => w.StatusCodeId == 1).Select(s => new { s.NavproductCodeId, s.ProductCode, s.ProductCodeName }).ToList();
                var company = _context.Plant.Where(w => w.StatusCodeId == 1).Select(s=>new { s.PlantId,s.PlantCode,s.Description}).ToList();
                var department = _context.Department.Where(w => w.StatusCodeId == 1).Select(s => new { s.DepartmentId, s.Name, s.Description }).ToList();
                FileProfileSetupFormlist.ForEach(s =>
                {
                    FileProfileSetupFormModel FileProfileSetupFormModel = new FileProfileSetupFormModel();
                    FileProfileSetupFormModel.FileProfileSetupFormId = s.FileProfileSetupFormId;
                    FileProfileSetupFormModel.ModifiedByUserID = s.AddedByUserId;
                    FileProfileSetupFormModel.AddedByUserID = s.ModifiedByUserId;
                    FileProfileSetupFormModel.StatusCodeID = s.StatusCodeId;
                    FileProfileSetupFormModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    FileProfileSetupFormModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    FileProfileSetupFormModel.AddedDate = s.AddedDate;
                    FileProfileSetupFormModel.ModifiedDate = s.ModifiedDate;
                    FileProfileSetupFormModel.ControlTypeId = s.ControlTypeId;
                    FileProfileSetupFormModel.ControlTypeValue = s.ControlTypeValue;
                    FileProfileSetupFormModel.IsDefault = s.IsDefault;
                    FileProfileSetupFormModel.DefaultValue = s.DefaultValue;
                    FileProfileSetupFormModel.Date = s.Date;
                    FileProfileSetupFormModel.ControlType = s.ControlType?.CodeValue;
                    FileProfileSetupFormModel.StatusCode = s.StatusCode?.CodeValue;
                    FileProfileSetupFormModel.FileProfileTypeId = s.FileProfileTypeId;
                    FileProfileSetupFormModel.IsMultiple = s.IsMultiple;
                    FileProfileSetupFormModel.IsRequired = s.IsRequired;
                    FileProfileSetupFormModel.Placeholder = s.Placeholder;
                    FileProfileSetupFormModel.RequiredMessage = s.RequiredMessage;
                    FileProfileSetupFormModel.DataSourceId = s.DataSourceId;
                    FileProfileSetupFormModel.DropDownTypeId = s.DropDownTypeId;
                    FileProfileSetupFormModel.PropertyName = s.ControlType?.CodeValue.ToLower() + '_' + s.FileProfileSetupFormId;
                    if (s.DropDownTypeId == "InBuild")
                    {
                        FileProfileSetupFormModel.DropDownValues = dropDownvalus.Where(d => d.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(d => d.DisplayValue).ToList();
                        FileProfileSetupFormModel.DropDownItems = dropDownvalus.Where(w => w.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(tn => new DropDownNameItems { Value = tn.DisplayValue, Text = tn.DisplayValue }).ToList();
                    }
                    if (s.DropDownTypeId == null)
                    {
                        FileProfileSetupFormModel.DropDownValues = dropDownvalus.Where(d => d.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(d => d.DisplayValue).ToList();
                        FileProfileSetupFormModel.DropDownItems = dropDownvalus.Where(w => w.FileProfileSetupFormId == s.FileProfileSetupFormId).Select(tn => new DropDownNameItems { Value = tn.DisplayValue, Text = tn.DisplayValue }).ToList();
                    }
                    if (s.DropDownTypeId == "Data Source")
                    {
                        if (s.DataSourceId == "Employee")
                        {
                            FileProfileSetupFormModel.DropDownItems = employee.Select(tn => new DropDownNameItems { Value = tn.EmployeeID.ToString() + "_" + tn.EmployeeName, Text = tn.EmployeeName }).ToList();
                        }
                        if (s.DataSourceId == "Customer")
                        {
                            FileProfileSetupFormModel.DropDownItems = customer.Select(tn => new DropDownNameItems { Value = tn.CustomerId.ToString() + "_" + tn.Code + "|" + tn.Name, Text = tn.Code + "||" + tn.Name }).ToList();
                        }
                        if (s.DataSourceId == "Vendor")
                        {
                            FileProfileSetupFormModel.DropDownItems = vendor.Select(tn => new DropDownNameItems { Value = tn.VendorsListId.ToString() + "_" + tn.VendorNo + "|" + tn.Name, Text = tn.VendorNo + " || " + tn.Name }).ToList();
                        }
                        if (s.DataSourceId == "Nav Items")
                        {
                            FileProfileSetupFormModel.DropDownItems = navItems.Select(tn => new DropDownNameItems { Value = tn.ItemId.ToString() + "_" + tn.No + " || " + tn.Description, Text = tn.No + " || " + tn.Description }).ToList();
                        }
                        if (s.DataSourceId == "Nav Product Code")
                        {
                            FileProfileSetupFormModel.DropDownItems = navProductCode.Select(tn => new DropDownNameItems { Value = tn.NavproductCodeId.ToString() + "_" + tn.ProductCode + "|" + tn.ProductCodeName, Text = tn.ProductCode + " || " + tn.ProductCodeName }).ToList();
                        }
                        if (s.DataSourceId == "Company")
                        {
                            FileProfileSetupFormModel.DropDownItems = company.Select(tn => new DropDownNameItems { Value = tn.PlantId.ToString() + "_" + tn.PlantCode + "||" + tn.Description, Text = tn.PlantCode + " || " + tn.Description }).ToList();
                        }
                        if (s.DataSourceId == "Department")
                        {
                            FileProfileSetupFormModel.DropDownItems = department.Select(tn => new DropDownNameItems { Value = tn.DepartmentId.ToString() + "_" + tn.Name + "||" + tn.Description, Text = tn.Name + " || " + tn.Description }).ToList();
                        }
                    }
                    FileProfileSetupFormModels.Add(FileProfileSetupFormModel);
                });
            }



            return FileProfileSetupFormModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FileProfileSetupFormModel> GetData(SearchModel searchModel)
        {
            var FileProfileSetupForm = new FileProfileSetupForm();
            var FileProfileSetupFormModel = new FileProfileSetupFormModel();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).FirstOrDefault();
                        break;
                    case "Last":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).LastOrDefault();
                        break;
                    case "Next":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).LastOrDefault();
                        break;
                    case "Previous":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).FirstOrDefault();
                        break;
                    case "Last":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).LastOrDefault();
                        break;
                    case "Next":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderBy(o => o.FileProfileSetupFormId).FirstOrDefault(s => s.FileProfileSetupFormId > searchModel.Id);
                        break;
                    case "Previous":
                        FileProfileSetupForm = _context.FileProfileSetupForm.OrderByDescending(o => o.FileProfileSetupFormId).FirstOrDefault(s => s.FileProfileSetupFormId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FileProfileSetupFormModel>(FileProfileSetupForm);


            return null;
        }

        [HttpPost]
        [Route("InsertFileProfileSetupForm")]
        public FileProfileSetupFormModel Post(FileProfileSetupFormModel value)
        {
            var FileProfileSetupForm = new FileProfileSetupForm
            {

                ControlTypeId = value.ControlTypeId,
                ControlTypeValue = value.ControlTypeValue,
                IsDefault = value.IsDefault,
                DefaultValue = value.DefaultValue,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Date = value.Date,
                FileProfileTypeId = value.FileProfileTypeId,
                IsMultiple = value.IsMultiple,
                IsRequired = value.IsRequired,
                Placeholder = value.Placeholder,
                RequiredMessage = value.RequiredMessage,
                PropertyName = value.PropertyName,
                DropDownTypeId = value.DropDownTypeId,
                DataSourceId = value.DataSourceId,
                ProductActivityCaseId = value.ProductActivityCaseId,

            };


            _context.FileProfileSetupForm.Add(FileProfileSetupForm);
            _context.SaveChanges();
            value.FileProfileSetupFormId = FileProfileSetupForm.FileProfileSetupFormId;
            if (value.DropDownValues.Count > 0)
            {
                value.DropDownValues.ForEach(d =>
                {
                    FileProfileSetUpDropDown fileProfileSetUpDropDown = new FileProfileSetUpDropDown()
                    {
                        FileProfileSetupFormId = value.FileProfileSetupFormId,
                        DisplayValue = d,

                    };
                    _context.FileProfileSetUpDropDown.Add(fileProfileSetUpDropDown);
                    _context.SaveChanges();
                });
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFileProfileSetupForm")]
        public FileProfileSetupFormModel Put(FileProfileSetupFormModel value)
        {
            var FileProfileSetupForm = _context.FileProfileSetupForm.SingleOrDefault(p => p.FileProfileSetupFormId == value.FileProfileSetupFormId);
            FileProfileSetupForm.ModifiedByUserId = value.ModifiedByUserID;
            FileProfileSetupForm.ModifiedDate = DateTime.Now;
            FileProfileSetupForm.ControlTypeId = value.ControlTypeId;
            FileProfileSetupForm.ControlTypeValue = value.ControlTypeValue;
            FileProfileSetupForm.DefaultValue = value.DefaultValue;
            FileProfileSetupForm.IsDefault = value.IsDefault;
            FileProfileSetupForm.StatusCodeId = value.StatusCodeID.Value;
            FileProfileSetupForm.Date = value.Date;
            FileProfileSetupForm.DropDownTypeId = value.DropDownTypeId;
            FileProfileSetupForm.DataSourceId = value.DataSourceId;
            var filesetupdropdown = _context.FileProfileSetUpDropDown.Where(f => f.FileProfileSetupFormId == value.FileProfileSetupFormId).ToList();
            if (filesetupdropdown != null)
            {
                _context.FileProfileSetUpDropDown.RemoveRange(filesetupdropdown);
                _context.SaveChanges();
            }
            if (value.DropDownValues.Count > 0)
            {
                value.DropDownValues.ForEach(d =>
                {
                    FileProfileSetUpDropDown fileProfileSetUpDropDown = new FileProfileSetUpDropDown()
                    {
                        FileProfileSetupFormId = value.FileProfileSetupFormId,
                        DisplayValue = d,

                    };
                    _context.FileProfileSetUpDropDown.Add(fileProfileSetUpDropDown);
                    _context.SaveChanges();
                });
            }
            FileProfileSetupForm.IsMultiple = value.IsMultiple;
            FileProfileSetupForm.IsRequired = value.IsRequired;
            FileProfileSetupForm.Placeholder = value.Placeholder;
            FileProfileSetupForm.RequiredMessage = value.RequiredMessage;
            FileProfileSetupForm.PropertyName = value.PropertyName;
            FileProfileSetupForm.ProductActivityCaseId = value.ProductActivityCaseId;
            _context.SaveChanges();

            return value;
        }


        [HttpDelete]
        [Route("DeleteFileProfileSetupForm")]
        public void Delete(int id)
        {
            var FileProfileSetupForm = _context.FileProfileSetupForm.SingleOrDefault(p => p.FileProfileSetupFormId == id);
            if (FileProfileSetupForm != null)
            {
                var filesetupdropdown = _context.FileProfileSetUpDropDown.Where(f => f.FileProfileSetupFormId == id).ToList();
                if (filesetupdropdown != null)
                {
                    _context.FileProfileSetUpDropDown.RemoveRange(filesetupdropdown);
                    _context.SaveChanges();
                }
                _context.FileProfileSetupForm.Remove(FileProfileSetupForm);
                _context.SaveChanges();
            }
        }
    }
}
