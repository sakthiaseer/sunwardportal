﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using System.Dynamic;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FileProfileTypeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IHubContext<ChatHub> _hub;
        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public FileProfileTypeController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries, IHubContext<ChatHub> hub, IBackgroundJobClient backgroundJobClient, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _hub = hub;
            _backgroundJobClient = backgroundJobClient;
            _hostingEnvironment = host;
        }
        private List<FileProfileTypeModel> FileProfileTypeIds(long? id)
        {
            List<FileProfileTypeModel> ids = new List<FileProfileTypeModel>();
            var fileFrofileType = _context.FileProfileType.Select(s => new FileProfileTypeModel { Name = s.Name, FileProfileTypeId = s.FileProfileTypeId, ParentId = s.ParentId }).ToList();
            var fileProfileName = fileFrofileType.FirstOrDefault(f => f.FileProfileTypeId == id);
            if (fileProfileName! != null)
            {
                ids.Add(fileProfileName);
                ids = GetAllFileProfileTypeIds(fileFrofileType, fileProfileName, ids);
            }
            return ids;
        }
        private List<FileProfileTypeModel> GetAllFileProfileTypeIds(List<FileProfileTypeModel> foldersListItems, FileProfileTypeModel s, List<FileProfileTypeModel> foldersModel)
        {
            var items = foldersListItems.Where(f => f.ParentId == s.FileProfileTypeId).ToList();
            if (items != null)
            {
                items.ForEach(s =>
                {
                    GetAllFileProfileTypeIds(foldersListItems, s, foldersModel);
                    foldersModel.Add(s);
                });
            }
            return foldersModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeIds")]
        public List<long> GetFileProfileTypeIds(long? id)
        {
            var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
            var fileProfileType = fileProfileTypeItems.FirstOrDefault(w => w.FileProfileTypeId == id);
            List<long> FolderPathLists = new List<long>();
            FolderPathLists = GetAllFileProfileTypeId(fileProfileTypeItems, fileProfileType, FolderPathLists);
            return FolderPathLists;
        }
        private List<long> GetAllFileProfileTypeId(List<FileProfileType> foldersListItems, FileProfileType s, List<long> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeId(foldersListItems, items, foldersModel);
                foldersModel.Add(items.FileProfileTypeId);
            }
            return foldersModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeDynamicFormBy")]
        public dynamic GetFileProfileTypeDynamicFormBy(long? id)
        {
            dynamic myobject = new ExpandoObject();
            var fileProfileSetupForm = _context.FileProfileSetupForm.Include(a => a.ControlType).AsNoTracking().Where(w => w.FileProfileTypeId == 1).ToList();
            var fileProfileTypeItems = _context.FileProfileTypeDynamicForm.AsNoTracking().FirstOrDefault(f => f.DocumentId == id);
            if (fileProfileTypeItems != null)
            {
                IDictionary<string, object> myUnderlyingObject = myobject;

                dynamic data = JObject.Parse(fileProfileTypeItems.DynamicFormData);
                myobject = data;
                /*fileProfileSetupForm.ForEach(s =>
                {
                    var key = s.ControlType?.CodeValue.ToLower() + "_" + s.FileProfileSetupFormId;
                    var value = data[key];
                    myUnderlyingObject.Add(key, value); 
                });
               */
            }
            return myobject;
        }
        [HttpPost]
        [Route("GetExcel")]
        public IActionResult GetExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("File Name");
            headers.Add("Profile Type Name");
            headers.Add("Profile No");
            headers.Add("Size(KB)");
            headers.Add("Description");
            headers.Add("Expiry Date");
            headers.Add("Modify By");
            headers.Add("Modify Date");

            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();

            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string newFolderName = "ConvertExcel";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            string FromLocation = folderName + @"\" + newFolderName + @"\" + searchModel.UserID + ".xlsx";
            var fileProfileTypes = FileProfileTypeIds(searchModel.FileProfileTypeId).ToList();
            var fileProfileTypeIds = fileProfileTypes.Select(s => s.FileProfileTypeId).ToList();
            var documents = _context.Documents.Select(s => new
            {
                s.FileName,
                s.FileIndex,
                s.FileSize,
                s.ProfileNo,
                s.UploadDate,
                s.DocumentId,
                s.FilterProfileTypeId,
                s.ArchiveStatusId,
                s.IsLatest,
                s.AddedByUserId,
                s.ModifiedDate,
                s.ModifiedByUserId,
                s.Description,
                s.ExpiryDate
            }).Where(w => w.IsLatest == true && w.ArchiveStatusId != 2562 && fileProfileTypeIds.Contains(w.FilterProfileTypeId.Value))
            .AsNoTracking().ToList();
            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                int j = 1;
                fileProfileTypes.ForEach(i =>
                {
                    List<string> headersBy = new List<string>();
                    headersBy.AddRange(headers);
                    var fileProfileSetupForm = _context.FileProfileSetupForm.Include(a => a.ControlType).Where(s => s.FileProfileTypeId == i.FileProfileTypeId && s.ControlTypeId != 3225).ToList();
                    if (fileProfileSetupForm != null && fileProfileSetupForm.Count > 0)
                    {
                        fileProfileSetupForm.ForEach(s =>
                        {
                            headersBy.Add(s.ControlTypeValue);
                        });
                    }
                    var linkfileProfileTypeDocumentids = _context.LinkFileProfileTypeDocument.Where(l => l.FileProfileTypeId == i.FileProfileTypeId).Select(s => s.DocumentId).Distinct().ToList();
                    var document = documents.Where(w => w.FilterProfileTypeId == i.FileProfileTypeId || linkfileProfileTypeDocumentids.Contains(w.DocumentId)).ToList();

                    var documentsIds = documents.Select(s => s.DocumentId).Distinct().ToList();
                    if (linkfileProfileTypeDocumentids.Count > 0)
                    {
                        linkfileProfileTypeDocumentids.ForEach(l =>
                        {
                            documentsIds.Add(l.Value);
                        });

                    }
                    var setAccess = _context.FileProfileTypeSetAccess.Where(s => documentsIds.Contains(s.DocumentId.Value)).Distinct().ToList();

                    WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                    var sheetName = j.ToString() + ") " + (i.Name.Length > 25 ? i.Name.Substring(0, 25) : i.Name);
                    Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = sheetName };
                    sheets.Append(sheet);
                    SheetData sheetData = new SheetData();
                    wSheetPart.Worksheet = new Worksheet(sheetData);

                    Row headerRow = new Row();
                    foreach (string column in headersBy)
                    {
                        /*Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(column);
                        headerRow.AppendChild(cell);*/
                        Cell cellHeader = new Cell();
                        cellHeader.DataType = CellValues.InlineString;
                        Run run1 = new Run();
                        run1.Append(new Text(column));

                        RunProperties run1Properties = new RunProperties();
                        run1Properties.Append(new Bold());
                        run1.RunProperties = run1Properties;
                        InlineString inlineString = new InlineString();
                        inlineString.Append(run1);
                        cellHeader.Append(inlineString);
                        headerRow.AppendChild(cellHeader);
                    }
                    sheetData.AppendChild(headerRow);
                    /*  document = document.Where(w => w.DocumentId == 20460).ToList();*/
                    if (document != null && document.Count > 0)
                    {

                        document.ForEach(s =>
                        {
                            var name = s.FileName?.Substring(s.FileName.LastIndexOf("."));
                            var fileName = s.FileName?.Split(name);
                            var setAccessdocExits = setAccess.Where(w => w.DocumentId == s.DocumentId).Count();
                            var setAccessExits = true;
                            if (s.AddedByUserId != searchModel.UserID)
                            {
                                if (setAccessdocExits > 0)
                                {
                                    setAccessExits = false;
                                    var userExits = setAccess.Where(w => w.UserId == searchModel.UserID && w.DocumentId == s.DocumentId).Count();
                                    if (userExits > 0)
                                    {
                                        setAccessExits = true;
                                    }
                                }
                            }
                            if (setAccessExits == true)
                            {

                                var addedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate;
                                var addedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName;

                                var fileNames = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                                Row row = new Row();
                                Cell fileNamecell = new Cell();
                                fileNamecell.DataType = CellValues.String;
                                fileNamecell.CellValue = new CellValue(fileNames);
                                row.AppendChild(fileNamecell);

                                Cell profileTypeNamecell = new Cell();
                                profileTypeNamecell.DataType = CellValues.String;
                                profileTypeNamecell.CellValue = new CellValue(i.Name);
                                row.AppendChild(profileTypeNamecell);

                                Cell profileNocell = new Cell();
                                profileNocell.DataType = CellValues.String;
                                profileNocell.CellValue = new CellValue(s.ProfileNo?.ToString());
                                row.AppendChild(profileNocell);

                                Cell sizeCell = new Cell();
                                sizeCell.DataType = CellValues.Number;
                                sizeCell.CellValue = new CellValue(Math.Round(Convert.ToDouble(s.FileSize / 1024)));
                                row.AppendChild(sizeCell);

                                Cell descriptioncell = new Cell();
                                descriptioncell.DataType = CellValues.String;
                                descriptioncell.CellValue = new CellValue(s.Description?.ToString());
                                row.AppendChild(descriptioncell);
                                Cell expiryDateCell = new Cell();
                                expiryDateCell.DataType = CellValues.String;
                                expiryDateCell.CellValue = new CellValue(s.ExpiryDate?.ToString("dd-MMM-yyyy hh:mm tt"));
                                row.AppendChild(expiryDateCell);

                                Cell addedByUserCell = new Cell();
                                addedByUserCell.DataType = CellValues.String;
                                addedByUserCell.CellValue = new CellValue(addedByUser?.ToString());
                                row.AppendChild(addedByUserCell);
                                Cell uploadDateCell = new Cell();
                                uploadDateCell.DataType = CellValues.String;
                                uploadDateCell.CellValue = new CellValue(addedDate?.ToString("dd-MMM-yyyy hh:mm tt"));
                                row.AppendChild(uploadDateCell);


                                var Url = searchModel.BaseUrl + "?id=" + s.DocumentId + "&&type=latest";

                                var fileProfileTypeDynamicForm = _context.FileProfileTypeDynamicForm.AsNoTracking().FirstOrDefault(f => f.DocumentId == s.DocumentId);
                                if (fileProfileTypeDynamicForm != null)
                                {

                                    dynamic data = JObject.Parse(fileProfileTypeDynamicForm.DynamicFormData);
                                    fileProfileSetupForm.ForEach(s =>
                                    {
                                        var key = s.ControlType?.CodeValue.ToLower() + "_" + s.FileProfileSetupFormId;
                                        var type = data[key]?.GetType()?.ToString();
                                        var value = "";
                                        if (type != null)
                                        {

                                            if (type == "Newtonsoft.Json.Linq.JValue")
                                            {
                                                value = data.GetValue(key)?.ToString();
                                            }
                                            if (type == "Newtonsoft.Json.Linq.JArray")
                                            {
                                                var array = data[key] as JArray;
                                                value = string.Join(",", array.ToObject<List<string>>());
                                            }
                                        }
                                        Cell profileTypeNamecells = new Cell();
                                        profileTypeNamecells.DataType = CellValues.String;
                                        profileTypeNamecells.CellValue = new CellValue(value);
                                        row.AppendChild(profileTypeNamecells);
                                    });
                                }
                                else
                                {
                                    if (fileProfileSetupForm != null && fileProfileSetupForm.Count > 0)
                                    {
                                        for (int k = 0; k < fileProfileSetupForm.Count; k++)
                                        {
                                            Cell fileNamecella = new Cell();
                                            fileNamecella.DataType = CellValues.String;
                                            fileNamecella.CellValue = new CellValue(string.Empty);
                                            row.AppendChild(fileNamecella);
                                        }
                                    }
                                }


                                Cell cellHeader = new Cell();
                                cellHeader.DataType = CellValues.InlineString;
                                CellFormula cellFormula1 = new CellFormula() { Space = SpaceProcessingModeValues.Preserve };
                                cellFormula1.Text = @"HYPERLINK(""" + Url + @""", ""Click Here"")";
                                cellHeader.CellFormula = cellFormula1;
                                Run run1 = new Run();
                                run1.Append(new Text("Click Here"));

                                RunProperties run1Properties = new RunProperties();
                                run1Properties.Append(new Bold());
                                run1.RunProperties = run1Properties;
                                InlineString inlineString = new InlineString();
                                inlineString.Append(run1);
                                cellHeader.Append(inlineString);
                                row.AppendChild(cellHeader);


                                sheetData.AppendChild(row);
                            }
                        });
                    }
                    j++;
                    sheetId++;
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        [HttpGet]
        [Route("GetFileProfileTypeListDropdownOneItems")]
        public List<FileProfileTypeListModel> GetFileProfileTypeListDropdownOneItems(long? id, long? userId)
        {
            List<FileProfileTypeListModel> fileProfileTypeModels = new List<FileProfileTypeListModel>();
            var fileprofileTypes = _context.FileProfileType.AsNoTracking().ToList();
            var fileProfileType = fileprofileTypes.Where(w => w.FileProfileTypeId == id).OrderByDescending(o => o.FileProfileTypeId).ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeListModel fileProfileTypeModel = new FileProfileTypeListModel();
                    List<string> FolderPathLists = new List<string>();
                    fileProfileTypeModel.FileProfileTypeId = s.FileProfileTypeId;
                    fileProfileTypeModel.Label = s.Name;
                    fileProfileTypeModel.ProfileId = s.ProfileId;
                    fileProfileTypeModel.Id = s.FileProfileTypeId;
                    fileProfileTypeModel.IsExpiryDate = s.IsExpiryDate;
                    fileProfileTypeModel.AddedByUserID = s.AddedByUserId;
                    fileProfileTypeModel.SessionId = s.SessionId;
                    FolderPathLists = GetAllFileProfileTypeName(fileprofileTypes, s, FolderPathLists);
                    FolderPathLists.Add(s.Name);
                    fileProfileTypeModel.Name = string.Join(" / ", FolderPathLists);
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
                List<long> fileProfileTypeIds = fileProfileTypeModels.Select(f => f.FileProfileTypeId).ToList();
                var roleItems = _context.DocumentUserRole.Where(f => f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)).AsNoTracking().ToList();
                fileProfileTypeModels.ForEach(q =>
                {
                    q.DocumentPermissionData = new DocumentPermissionModel();
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FileProfileTypeId).ToList();
                    if (roles.Count > 0)
                    {
                        var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData.IsCreateDocument = false;
                            q.DocumentPermissionData.IsRead = false;
                            q.DocumentPermissionData.IsDelete = false;
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = true;
                        q.DocumentPermissionData.IsRead = true;
                        q.DocumentPermissionData.IsDelete = true;
                    }
                });
            }
            return fileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        [HttpGet]
        [Route("GetFileProfileTypeListDropdownItems")]
        public List<FileProfileTypeListModel> GetFileProfileTypeListDropdownItems(long? id, long? userId)
        {
            List<FileProfileTypeListModel> fileProfileTypeModels = new List<FileProfileTypeListModel>();
            var fileprofileTypes = _context.FileProfileType.AsNoTracking().ToList();
            var fileProfileType = fileprofileTypes.Where(w => w.ParentId == id).OrderByDescending(o => o.FileProfileTypeId).ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeListModel fileProfileTypeModel = new FileProfileTypeListModel();
                    List<string> FolderPathLists = new List<string>();
                    fileProfileTypeModel.FileProfileTypeId = s.FileProfileTypeId;
                    fileProfileTypeModel.Label = s.Name;
                    fileProfileTypeModel.Id = s.FileProfileTypeId;
                    fileProfileTypeModel.IsExpiryDate = s.IsExpiryDate;
                    fileProfileTypeModel.AddedByUserID = s.AddedByUserId;
                    fileProfileTypeModel.SessionId = s.SessionId;
                    FolderPathLists = GetAllFileProfileTypeName(fileprofileTypes, s, FolderPathLists);
                    FolderPathLists.Add(s.Name);
                    fileProfileTypeModel.Name = string.Join(" / ", FolderPathLists);
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
                List<long> fileProfileTypeIds = fileProfileTypeModels.Select(f => f.FileProfileTypeId).ToList();
                var roleItems = _context.DocumentUserRole.Where(f => f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)).AsNoTracking().ToList();
                fileProfileTypeModels.ForEach(q =>
                {
                    q.DocumentPermissionData = new DocumentPermissionModel();
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FileProfileTypeId).ToList();
                    if (roles.Count > 0)
                    {
                        var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData.IsCreateDocument = false;
                            q.DocumentPermissionData.IsRead = false;
                            q.DocumentPermissionData.IsDelete = false;
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = true;
                        q.DocumentPermissionData.IsRead = true;
                        q.DocumentPermissionData.IsDelete = true;
                    }
                });
            }
            return fileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetFileProfileTypeListItems")]
        public List<FileProfileTypeModel> GetFileProfileTypeListItems()
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
            var fileProfileType = _context.FileProfileType
                .Include(s => s.ShelfLifeDurationNavigation)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode)
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = s.Name,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        SessionId=s.SessionId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                        ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsAllowMobileUpload = s.IsAllowMobileUpload,
                        IsDocumentAccess = s.IsDocumentAccess,
                        ShelfLifeDuration = s.ShelfLifeDuration,
                        ShelfLifeDurationId = s.ShelfLifeDurationId,
                        ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation?.CodeValue,
                        Hints = s.Hints,
                        IsEnableCreateTask = s.IsEnableCreateTask,
                        ProfileTypeInfo = s.ProfileTypeInfo,
                        IsCreateByMonth = s.IsCreateByMonth,
                        IsCreateByYear = s.IsCreateByYear,
                        IsHidden = s.IsHidden,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
            }
            return fileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        [HttpGet]
        [Route("GetFileProfileTypeParent")]
        public List<FileProfileTypeModel> GetFileProfileTypeParent()
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
            var fileProfileType = _context.FileProfileType
                .Include(s => s.ShelfLifeDurationNavigation)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode)
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = s.Name,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        SessionId=s.SessionId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                        ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsAllowMobileUpload = s.IsAllowMobileUpload,
                        IsDocumentAccess = s.IsDocumentAccess,
                        ShelfLifeDuration = s.ShelfLifeDuration,
                        ShelfLifeDurationId = s.ShelfLifeDurationId,
                        ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation?.CodeValue,
                        Hints = s.Hints,
                        IsEnableCreateTask = s.IsEnableCreateTask,
                        ProfileTypeInfo = s.ProfileTypeInfo,
                        IsCreateByMonth = s.IsCreateByMonth,
                        IsCreateByYear = s.IsCreateByYear,
                        IsHidden = s.IsHidden,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
            }
            return fileProfileTypeModels.Where(w => w.ParentId == null).OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        [HttpGet]
        [Route("GetFileProfileTypeInfoById")]
        public FileProfileTypeModel GetFileProfileTypeInfoById(int id)
        {
            var fileProfileType = _context.FileProfileType.Where(t => t.FileProfileTypeId == id)
             .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().FirstOrDefault();
            FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
            {
                FileProfileTypeId = fileProfileType.FileProfileTypeId,
                ParentId = fileProfileType.ParentId,
                Name = fileProfileType.Name,
                ProfileId = fileProfileType.ProfileId,
                IsExpiryDate = fileProfileType.IsExpiryDate,
                Description = fileProfileType.Description,
                StatusCodeID = fileProfileType.StatusCodeId,
                AddedByUserID = fileProfileType.AddedByUserId,
                ModifiedByUserID = fileProfileType.ModifiedByUserId,
                ProfileInfo = fileProfileType.ProfileInfo,
                ProfileTypeInfo = fileProfileType.ProfileTypeInfo,
                IsTemplateCaseNo = fileProfileType.IsTemplateCaseNo,
                TemplateTestCaseId = fileProfileType.TemplateTestCaseId,
                SessionId = fileProfileType.SessionId,
            };
            return fileProfileTypeModel;
        }

        [HttpGet]
        [Route("GetFileProfileTypeById")]
        public FileProfileTypeModel GetFileProfileTypeById(int id, long? userid)
        {

            var fileProfileType = _context.FileProfileType.Where(t => t.FileProfileTypeId == id)
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().FirstOrDefault();
            FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
            {
                FileProfileTypeId = fileProfileType.FileProfileTypeId,
                ParentId = fileProfileType.ParentId,
                Name = fileProfileType.Name,
                ProfileId = fileProfileType.ProfileId,
                ProfileInfo = fileProfileType.ProfileInfo,
                Description = fileProfileType.Description,
                StatusCodeID = fileProfileType.StatusCodeId,
                AddedByUserID = fileProfileType.AddedByUserId,
                ModifiedByUserID = fileProfileType.ModifiedByUserId,
                IsTemplateCaseNo = fileProfileType.IsTemplateCaseNo,
                TemplateTestCaseId = fileProfileType.TemplateTestCaseId,
                ProfileTypeInfo = fileProfileType.ProfileTypeInfo,
                SessionId = fileProfileType.SessionId,
            };

            var roleItem = _context.DocumentUserRole.FirstOrDefault(f => f.FileProfileTypeId != null && f.FileProfileTypeId == id && f.UserId == userid);

            fileProfileTypeModel.DocumentPermissionData = new DocumentPermissionModel();
            if (roleItem != null)
            {
                DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                fileProfileTypeModel.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
            }
            else
            {
                fileProfileTypeModel.DocumentPermissionData.IsCreateDocument = false;
                fileProfileTypeModel.DocumentPermissionData.IsRead = false;
                fileProfileTypeModel.DocumentPermissionData.IsDelete = false;
                fileProfileTypeModel.DocumentPermissionData.IsEnableProfileTypeInfo = true;
            }
            return fileProfileTypeModel;
        }

        [HttpGet]
        [Route("GetDocumentIdPermission")]
        public List<PermissionListViewModel> GetDocumentIdPermission(int id)
        {
            List<DocumentUserRoleModel> documentUserRoleModels = new List<DocumentUserRoleModel>();
            List<PermissionListViewModel> permissionListViewModels = new List<PermissionListViewModel>();
            var applicationroleItems = _context.DocumentRole.AsNoTracking().ToList();
            var documentUserRoleList = _context.DocumentUserRole.AsNoTracking().ToList();
            var applicationUserList = _context.ApplicationUser.AsNoTracking().ToList();
            var permissionList = _context.DocumentPermission.ToList();
            var document = _context.Documents.FirstOrDefault(f => f.DocumentId == id);
            if (document != null)
            {
                var fileProfileType = _context.FileProfileType.FirstOrDefault(d => d.FileProfileTypeId == document.FilterProfileTypeId)?.Name;
                int fileProfileId = Convert.ToInt32(document.FilterProfileTypeId);
                documentUserRoleModels = GetFileProfileTypeAccess(fileProfileId);


                documentUserRoleModels.ForEach(r =>
                {


                    // var selectRole = applicationroleItems.FirstOrDefault(a => a.DocumentRoleId == r)?.DocumentRoleName;
                    PermissionListViewModel permissionListViewModel = new PermissionListViewModel();
                    permissionListViewModel.RoleName = r.RoleName;
                    permissionListViewModel.FileProfileType = fileProfileType;
                    permissionListViewModel.UserName = r.UserName;
                    permissionListViewModel.DocumentUserRoleID = r.DocumentUserRoleID;
                    permissionListViewModel.DocumentId = id;
                    var permissionlist = permissionList.FirstOrDefault(d => d.DocumentRoleId == r.RoleID);
                    if (permissionlist != null)
                    {
                        permissionListViewModel.PermissionList = new List<string>();
                        if (permissionlist.IsRead == true)
                        {
                            permissionListViewModel.PermissionList.Add("Read");
                        }
                        if (permissionlist.IsEdit == true)
                        {
                            permissionListViewModel.PermissionList.Add("Edit");
                        }
                        if (permissionlist.IsDelete == true)
                        {
                            permissionListViewModel.PermissionList.Add("Delete");
                        }
                        if (permissionlist.IsUpdateDocument == true)
                        {
                            permissionListViewModel.PermissionList.Add("Update Document");
                        }
                        if (permissionlist.IsMove == true)
                        {
                            permissionListViewModel.PermissionList.Add("Move");
                        }
                        if (permissionlist.IsRename == true)
                        {
                            permissionListViewModel.PermissionList.Add("Rename");
                        }
                        if (permissionlist.IsCreateFolder == true)
                        {
                            permissionListViewModel.PermissionList.Add("Create Folder");
                        }
                        if (permissionlist.IsCreateDocument == true)
                        {
                            permissionListViewModel.PermissionList.Add("Create Document");
                        }
                        if (permissionlist.IsShare == true)
                        {
                            permissionListViewModel.PermissionList.Add("Share");
                        }
                        if (permissionlist.IsFileDelete == true)
                        {
                            permissionListViewModel.PermissionList.Add("File Delete");
                        }
                        if (permissionListViewModel.PermissionList != null && permissionListViewModel.PermissionList.Count > 0)
                        {
                            permissionListViewModel.Permission = string.Join(",  ", permissionListViewModel.PermissionList);
                        }
                        else if (permissionListViewModel.PermissionList.Count == 0)
                        {
                            permissionListViewModel.Permission = "No Permission";
                        }
                    }
                    permissionListViewModels.Add(permissionListViewModel);

                });
                var setAccess = _context.FileProfileTypeSetAccess.Where(s => s.DocumentId == id).ToList();
                if (setAccess != null && setAccess.Count > 0)
                {
                    setAccess.ForEach(su =>
                    {
                        PermissionListViewModel permissionListViewModel = new PermissionListViewModel();
                        permissionListViewModel.PermissionList = new List<string>();
                        permissionListViewModel.PermissionList.Add("Read");
                        permissionListViewModel.PermissionList.Add("Edit");
                        permissionListViewModel.PermissionList.Add("Delete");
                        permissionListViewModel.PermissionList.Add("Move");
                        permissionListViewModel.PermissionList.Add("Rename");
                        permissionListViewModel.PermissionList.Add("Create Document");
                        permissionListViewModel.PermissionList.Add("Share");
                        permissionListViewModel.PermissionList.Add("File Delete");
                        if (permissionListViewModel.PermissionList != null && permissionListViewModel.PermissionList.Count > 0)
                        {
                            permissionListViewModel.Permission = string.Join(",  ", permissionListViewModel.PermissionList);
                        }
                        permissionListViewModel.FileProfileTypeSetAccessId = su.FileProfileTypeSetAccessId;
                        //permissionListViewModel.Permission = "Document Level Access";
                        permissionListViewModel.RoleName = "Document Set Access";
                        permissionListViewModel.UserName = applicationUserList.FirstOrDefault(f => f.UserId == su.UserId)?.UserName;
                        permissionListViewModel.FileProfileType = fileProfileType;
                        permissionListViewModel.DocumentId = id;
                        permissionListViewModels.Add(permissionListViewModel);
                    });
                }


            }
            return permissionListViewModels;
        }
        [HttpGet]
        [Route("GetFileProfileTypeItems")]
        public List<FileProfileTypeModel> GetFileProfileTypeItems(int id)
        {

            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
            var fileProfileType = _context.FileProfileType
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                var ProfileIds = fileProfileType.Select(s => s.ProfileId).ToList();
                var UserIds = fileProfileType.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
                UserIds.AddRange(fileProfileType.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
                var statusCodeId = fileProfileType.Select(s => s.StatusCodeId).ToList();
                statusCodeId.AddRange(fileProfileType.Select(s => s.ShelfLifeDurationId.GetValueOrDefault(0)).ToList());
                var appUsers = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(w => UserIds.Contains(w.UserId)).AsNoTracking().ToList();
                var codeMaster = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(w => statusCodeId.Contains(w.CodeId)).AsNoTracking().ToList();
                var Profile = _context.DocumentProfileNoSeries.Select(s => new
                {
                    s.ProfileId,
                    s.Name,
                }).Where(w => ProfileIds.Contains(w.ProfileId)).AsNoTracking().ToList();
                fileProfileType.ForEach(s =>
                {
                    List<string> FolderPathLists = new List<string>();
                    FolderPathLists = GetAllFileProfileTypeName(fileProfileType, s, FolderPathLists);
                    FolderPathLists.Add(s.Name);
                    string DocumentPath = string.Join(" / ", FolderPathLists);
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = DocumentPath,
                        ProfileId = s.ProfileId,
                        SessionId=s.SessionId,
                        ProfileName = Profile.FirstOrDefault(f => f.ProfileId == s.ProfileId)?.Name,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = codeMaster.FirstOrDefault(f => f.CodeId == s.StatusCodeId)?.CodeValue,
                        AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                        ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsExpiryDate = s.IsExpiryDate,
                        IsAllowMobileUpload = s.IsAllowMobileUpload,
                        IsDocumentAccess = s.IsDocumentAccess,
                        ShelfLifeDuration = s.ShelfLifeDuration,
                        ShelfLifeDurationId = s.ShelfLifeDurationId,
                        ShelfLifeDurationStatus = s.ShelfLifeDurationId != null ? codeMaster.FirstOrDefault(f => f.CodeId == s.ShelfLifeDurationId)?.CodeValue : "",
                        Hints = s.Hints,
                        IsEnableCreateTask = s.IsEnableCreateTask,
                        ProfileTypeInfo = s.ProfileTypeInfo,
                        IsCreateByMonth = s.IsCreateByMonth,
                        IsCreateByYear = s.IsCreateByYear,
                        IsHidden = s.IsHidden,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
                List<long> fileProfileTypeIds = fileProfileTypeModels.Select(f => f.FileProfileTypeId).ToList();
                var roleItems = _context.DocumentUserRole.Where(f => f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)).AsNoTracking().ToList();
                fileProfileTypeModels.ForEach(q =>
                {
                    q.DocumentPermissionData = new DocumentPermissionModel();
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FileProfileTypeId).ToList();
                    if (roles.Count > 0)
                    {
                        var roleItem = roles.FirstOrDefault(r => r.UserId == id);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData.IsCreateDocument = false;
                            q.DocumentPermissionData.IsRead = false;
                            q.DocumentPermissionData.IsDelete = false;
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = true;
                        q.DocumentPermissionData.IsRead = true;
                        q.DocumentPermissionData.IsDelete = true;
                    }
                });
            }
            return fileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        private List<string> GetAllFileProfileTypeName(List<FileProfileType> foldersListItems, FileProfileType s, List<string> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }
        private List<string> GetAllFileProfileTypeModelName(List<FileProfileTypeModel> foldersListItems, FileProfileTypeModel s, List<string> foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllFileProfileTypeModelName(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Label);
            }
            return foldersModel;
        }
        [HttpGet]
        [Route("GetDocumentPermissionData")]
        public DocumentPermissionModel GetDocumentPermissionData(long? id, long? userid)
        {
            DocumentPermissionModel DocumentPermissionData = new DocumentPermissionModel();
            var roles = _context.DocumentUserRole.Where(f => f.FileProfileTypeId == id).ToList();
            if (roles.Count > 0)
            {
                var roleItem = roles.FirstOrDefault(r => r.UserId == userid);
                if (roleItem != null)
                {
                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                    DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                }
                else
                {
                    DocumentPermissionData.IsCreateDocument = false;
                    DocumentPermissionData.IsRead = false;
                    DocumentPermissionData.IsDelete = false;
                }
            }
            else
            {
                DocumentPermissionData.IsCreateDocument = true;
                DocumentPermissionData.IsRead = true;
                DocumentPermissionData.IsDelete = true;
            }
            return DocumentPermissionData;
        }
        [HttpGet]
        [Route("GetFileProfileTypeTreeDropDown")]
        public List<FileProfileTypeModel> GetFileProfileTypeTreeDropDown(long? id)
        {
            var classificationTree = _context.FileProfileType
                .Select(s => new FileProfileTypeModel
                {
                    FileProfileTypeId = s.FileProfileTypeId,
                    ParentId = s.ParentId,
                    Label = s.Name,
                    Name = s.Name,
                    Description = s.Description,
                    ProfileId = s.ProfileId,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    IsExpiryDate = s.IsExpiryDate,
                    IsAllowMobileUpload = s.IsAllowMobileUpload,
                    IsDocumentAccess = s.IsDocumentAccess,
                    ShelfLifeDurationId = s.ShelfLifeDurationId,
                    ShelfLifeDuration = s.ShelfLifeDuration,
                    Hints = s.Hints,
                    IsEnableCreateTask = s.IsEnableCreateTask,
                    IsCreateByMonth = s.IsCreateByMonth,
                    IsCreateByYear = s.IsCreateByYear,
                    SessionId=s.SessionId,
                    IsHidden = s.IsHidden,
                    ProfileInfo = s.ProfileInfo,
                    IsTemplateCaseNo = s.IsTemplateCaseNo,
                    TemplateTestCaseId = s.TemplateTestCaseId,
                }).AsNoTracking().ToList();
            List<long> fileProfileTypeIds = classificationTree.Select(f => f.FileProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)).AsNoTracking().ToList();
            classificationTree.ForEach(s =>
            {
                List<string> FolderPathLists = new List<string>();
                FolderPathLists = GetAllFileProfileTypeModelName(classificationTree, s, FolderPathLists);
                FolderPathLists.Add(s.Label);
                s.Name = string.Join(" / ", FolderPathLists);
                s.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == s.FileProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == id);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        s.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        s.DocumentPermissionData.IsCreateDocument = false;
                        s.DocumentPermissionData.IsRead = false;
                        s.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    s.DocumentPermissionData.IsCreateDocument = true;
                    s.DocumentPermissionData.IsRead = true;
                    s.DocumentPermissionData.IsDelete = true;
                }
            });
            var lookup = classificationTree.ToLookup(x => x.ParentId);

            Func<long?, List<FileProfileTypeModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new FileProfileTypeModel()
                    {
                        FileProfileTypeId = x.FileProfileTypeId,
                        Id = x.FileProfileTypeId,
                        ParentId = x.ParentId,
                        Label = x.Label,
                        Name = x.Name,
                        // Description = x.Description,
                        ProfileId = x.ProfileId,
                        AddedByUserID = x.AddedByUserID,
                        ModifiedByUserID = x.ModifiedByUserID,
                        StatusCodeID = x.StatusCodeID,
                        IsExpiryDate = x.IsExpiryDate,
                        IsDocumentAccess = x.IsDocumentAccess,
                        ShelfLifeDuration = x.ShelfLifeDuration,
                        ShelfLifeDurationId = x.ShelfLifeDurationId,
                        IsHidden = x.IsHidden,
                        ProfileInfo = x.ProfileInfo,
                        IsTemplateCaseNo = x.IsTemplateCaseNo,
                        TemplateTestCaseId = x.TemplateTestCaseId,
                        Children = build(x.FileProfileTypeId).Count > 0 ? build(x.FileProfileTypeId) : new List<FileProfileTypeModel>(),
                    })
                    .ToList();
            var nestedlist = build(null).ToList();
            return nestedlist.ToList();
        }
        [HttpGet]
        [Route("GetFileProfileTypeDetailsDropDown")]
        public List<FileProfileTypeModel> GetFileProfileTypeDetailsDropDown(int id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<FileProfileTypeModel> fileProfileTypeModels = new List<FileProfileTypeModel>();
            List<FileProfileTypeModel> folderLocation = new List<FileProfileTypeModel>();
            var fileProfileType = _context.FileProfileType
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = s.Name,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        SessionId=s.SessionId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        IsExpiryDate = s.IsExpiryDate,
                        IsAllowMobileUpload = s.IsAllowMobileUpload,
                        IsDocumentAccess = s.IsDocumentAccess,
                        ShelfLifeDuration = s.ShelfLifeDuration,
                        ShelfLifeDurationId = s.ShelfLifeDurationId,
                        ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation?.CodeValue,
                        Hints = s.Hints,
                        IsEnableCreateTask = s.IsEnableCreateTask,
                        ProfileTypeInfo = s.ProfileTypeInfo,
                        IsCreateByMonth = s.IsCreateByMonth,
                        IsCreateByYear = s.IsCreateByYear,
                        IsHidden = s.IsHidden,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });

                var profileIds = new List<long>();
                string folderPath = "";
                foreach (var fn in fileProfileTypeModels)
                {
                    FileProfileTypeModel f = new FileProfileTypeModel();

                    if (fn.ParentId > 0)
                    {
                        f.FileProfileTypeId = fn.FileProfileTypeId;
                        f.Name = fn.Name;
                        f.ParentId = fn.ParentId;
                        f.FileProfilePaths.Add(fn.Name);
                        GenerateLocation(f, fn, fileProfileTypeModels, profileIds);
                        f.FileProfilePaths.Reverse();
                        foreach (var item in f.FileProfilePaths)
                        {
                            if (f.FileProfilePaths[f.FileProfilePaths.Count - 1] != item)
                            {
                                f.FileProfilePath += item + " / ";
                            }
                            else
                            {
                                f.FileProfilePath += item;
                            }
                        }
                        f.Description = fn.Description;
                        f.StatusCodeID = fn.StatusCodeID;
                        f.AddedByUserID = fn.AddedByUserID;
                        f.ModifiedByUserID = fn.ModifiedByUserID;
                        f.IsExpiryDate = fn.IsExpiryDate;
                        f.IsAllowMobileUpload = fn.IsAllowMobileUpload;
                        f.IsDocumentAccess = fn.IsDocumentAccess;
                        f.ShelfLifeDuration = fn.ShelfLifeDuration;
                        f.ShelfLifeDurationId = fn.ShelfLifeDurationId;
                        f.Hints = fn.Hints;
                        f.IsEnableCreateTask = fn.IsEnableCreateTask;
                        f.ProfileTypeInfo = fn.ProfileTypeInfo;
                        f.IsCreateByMonth = fn.IsCreateByMonth;
                        f.IsCreateByYear = fn.IsCreateByYear;
                        f.IsHidden = fn.IsHidden;
                        f.ProfileInfo = fn.ProfileInfo;
                        f.IsTemplateCaseNo = fn.IsTemplateCaseNo;
                        f.TemplateTestCaseId = fn.TemplateTestCaseId;
                        folderLocation.Add(f);

                    }
                    else if (f.ParentId == null)
                    {
                        f.FileProfilePath = fn.Name;
                        f.FileProfileTypeId = fn.FileProfileTypeId;
                        f.Name = fn.Name;
                        f.ParentId = fn.ParentId;
                        f.Description = fn.Description;
                        f.StatusCodeID = fn.StatusCodeID;
                        f.AddedByUserID = fn.AddedByUserID;
                        f.ModifiedByUserID = fn.ModifiedByUserID;
                        f.IsExpiryDate = fn.IsExpiryDate;
                        f.IsAllowMobileUpload = fn.IsAllowMobileUpload;
                        f.IsDocumentAccess = fn.IsDocumentAccess;
                        f.ShelfLifeDuration = fn.ShelfLifeDuration;
                        f.ShelfLifeDurationId = fn.ShelfLifeDurationId;
                        f.Hints = fn.Hints;
                        f.IsEnableCreateTask = fn.IsEnableCreateTask;
                        f.ProfileTypeInfo = fn.ProfileTypeInfo;
                        f.IsCreateByMonth = fn.IsCreateByMonth;
                        f.IsCreateByYear = fn.IsCreateByYear;
                        f.IsHidden = fn.IsHidden;
                        f.ProfileInfo = fn.ProfileInfo;
                        f.IsTemplateCaseNo = fn.IsTemplateCaseNo;
                        f.TemplateTestCaseId = fn.TemplateTestCaseId;
                        folderLocation.Add(f);
                    }
                }

                List<long> fileProfileTypeIds = fileProfileTypeModels.Select(f => f.FileProfileTypeId).ToList();
                var roleItems = _context.DocumentUserRole.Where(f => f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)).AsNoTracking().ToList();
                folderLocation.ForEach(q =>
                {
                    q.DocumentPermissionData = new DocumentPermissionModel();
                    var roles = roleItems.Where(f => f.FileProfileTypeId == q.FileProfileTypeId).ToList();
                    if (roles.Count > 0)
                    {
                        var roleItem = roles.FirstOrDefault(r => r.UserId == id);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                        }
                        else
                        {
                            q.DocumentPermissionData.IsCreateDocument = false;
                            q.DocumentPermissionData.IsRead = false;
                            q.DocumentPermissionData.IsDelete = false;
                        }
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = true;
                        q.DocumentPermissionData.IsRead = true;
                        q.DocumentPermissionData.IsDelete = true;
                    }
                });
            }
            return folderLocation.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }

        [HttpGet]
        [Route("GetFileProfileTypeMobileItems")]
        public List<FileProfileTypeMobile> GetFileProfileTypeMobileItems()
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<FileProfileTypeMobile> fileProfileTypeModels = new List<FileProfileTypeMobile>();
            var fileProfileType = _context.FileProfileType
                .Include(s => s.ShelfLifeDurationNavigation)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode)
                .Where(s => s.IsAllowMobileUpload == true)
                .OrderByDescending(o => o.FileProfileTypeId).AsNoTracking().ToList();
            if (fileProfileType != null && fileProfileType.Count > 0)
            {
                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeMobile fileProfileTypeModel = new FileProfileTypeMobile
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = s.Name,
                        Hint = s.Hints,
                        SessionId=s.SessionId,
                        ProfileId = s.ProfileId,
                        ProfileName = s.Profile != null ? s.Profile.Name : null,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                        ModifiedByUser = s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsExpiryDate = s.IsExpiryDate,
                        IsAllowMobileUpload = s.IsAllowMobileUpload,
                        IsDocumentAccess = s.IsDocumentAccess,
                        ShelfLifeDuration = s.ShelfLifeDuration,
                        ShelfLifeDurationId = s.ShelfLifeDurationId,
                        ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation?.CodeValue,
                        Hints = s.Hints,
                        IsEnableCreateTask = s.IsEnableCreateTask,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    fileProfileTypeModels.Add(fileProfileTypeModel);
                });
            }
            return fileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }

        [HttpGet]
        [Route("GetFileProfileTypeParentId")]
        public FileProfileTypeModel GetFileProfileTypeParentId(int? id, int userId)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var fileProfileType = _context.FileProfileType
                .Include(h => h.ShelfLifeDurationNavigation)
                .Include(p => p.Profile)
                .Include(s => s.StatusCode).ToList();
            List<FileProfileTypeModel> FileProfileTypeModels = new List<FileProfileTypeModel>();
            fileProfileType.ForEach(s =>
            {
                FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                {
                    FileProfileTypeId = s.FileProfileTypeId,
                    ParentId = s.ParentId,
                    Name = s.Name,
                    SessionId=s.SessionId,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : null,
                    ShelfLifeDurationStatus = s.ShelfLifeDurationNavigation != null ? s.ShelfLifeDurationNavigation.CodeValue : null,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                    ModifiedByUser = appUsers != null && s.ModifiedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    IsExpiryDate = s.IsExpiryDate,
                    IsAllowMobileUpload = s.IsAllowMobileUpload,
                    IsDocumentAccess = s.IsDocumentAccess,
                    ShelfLifeDuration = s.ShelfLifeDuration,
                    ShelfLifeDurationId = s.ShelfLifeDurationId,
                    Hints = s.Hints,
                    IsEnableCreateTask = s.IsEnableCreateTask,
                    ProfileTypeInfo = s.ProfileTypeInfo,
                    IsCreateByMonth = s.IsCreateByMonth,
                    IsCreateByYear = s.IsCreateByYear,
                    IsHidden = s.IsHidden,
                    ProfileInfo = s.ProfileInfo,
                    IsTemplateCaseNo = s.IsTemplateCaseNo,
                    TemplateTestCaseId = s.TemplateTestCaseId,
                };
                FileProfileTypeModels.Add(fileProfileTypeModel);
            });
            return FileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).Where(i => i.FileProfileTypeId == id).FirstOrDefault();
        }

        [HttpGet]
        [Route("GetChildFileProfileType")]
        public List<FileProfileTypeModel> GetChildFileProfileType(long? id)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            List<FileProfileTypeModel> FileProfileTypeModels = new List<FileProfileTypeModel>();
            var parentids = _context.FileProfileType.Where(s => s.ParentId == id).Select(s => s.FileProfileTypeId).ToList();
            if (parentids.Count > 0)
            {
                var fileProfileType = _context.FileProfileType
                    .Where(w => parentids.Contains(w.FileProfileTypeId)).ToList();

                fileProfileType.ForEach(s =>
                {
                    FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel
                    {
                        FileProfileTypeId = s.FileProfileTypeId,
                        ParentId = s.ParentId,
                        Name = s.Name,
                        SessionId=s.SessionId,
                        ProfileId = s.ProfileId,
                        Description = s.Description,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsCreateByMonth = s.IsCreateByMonth,
                        IsCreateByYear = s.IsCreateByYear,
                        IsHidden = s.IsHidden,
                        ProfileInfo = s.ProfileInfo,
                        IsTemplateCaseNo = s.IsTemplateCaseNo,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                    };
                    FileProfileTypeModels.Add(fileProfileTypeModel);
                });
            }
            return FileProfileTypeModels.OrderByDescending(o => o.FileProfileTypeId).ToList();
        }
        [HttpGet]
        [Route("GetFileProfileTypeTreeItems")]
        public ApplicationPermissionTreeModel GetFileProfileTypeTreeItems(long id, int userId)
        {
            var itemClassificationTree = _context.FileProfileType.FirstOrDefault(t => t.FileProfileTypeId == id);
            if (itemClassificationTree == null)
            {
                return null;
            }

            var classificationTree = _context.FileProfileType

                .AsNoTracking()
                .Select(s => new
                {
                    s.FileProfileTypeId,
                    s.ParentId,
                    s.Name,
                    s.Description,
                    s.ProfileId,
                    s.StatusCodeId,
                    s.AddedByUserId,
                    s.AddedDate,
                    s.SessionId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    s.IsExpiryDate,
                    s.IsAllowMobileUpload,
                    s.IsDocumentAccess,
                    s.ShelfLifeDuration,
                    s.ShelfLifeDurationId,
                    s.Hints,
                    s.IsEnableCreateTask,
                    s.IsCreateByYear,
                    s.IsCreateByMonth,
                    s.IsHidden,
                    s.IsTemplateCaseNo,
                    s.TemplateTestCaseId,
                })
             .ToList();

            var lookup = classificationTree.ToLookup(x => x.ParentId);

            Func<long?, List<ApplicationPermissionModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new ApplicationPermissionModel()
                    {
                        Id = x.FileProfileTypeId,
                        Name = x.Name,
                        Children = build(x.FileProfileTypeId),

                    })
                    .ToList();
            var nestedlist = build(null).ToList();
            ApplicationPermissionModel ApplicationPermissionModel = new ApplicationPermissionModel();
            ApplicationPermissionModel.Children = nestedlist.ToList();
            var allDescendantNames = GetAllChildren(ApplicationPermissionModel).Select(child => child.Children).ToList();
            List<long?> itemClassificationModelsLists = new List<long?>();
            List<ApplicationPermissionModel> FlatItems = new List<ApplicationPermissionModel>();
            ApplicationPermissionModel FlatItemData = new ApplicationPermissionModel();
            if (nestedlist != null && nestedlist.Count > 0)
            {
                nestedlist.ForEach(n =>
                {
                    ApplicationPermissionModel FlatItem = new ApplicationPermissionModel();
                    FlatItem.Id = n.Id;
                    FlatItem.Name = n.Name;
                    FlatItems.Add(FlatItem);
                    itemClassificationModelsLists.Add(n.Id);
                });
            }
            allDescendantNames.ForEach(h =>
            {
                if (h.Count > 0)
                {
                    h.ForEach(a =>
                    {
                        ApplicationPermissionModel FlatItem = new ApplicationPermissionModel();
                        FlatItem.Id = a.Id;
                        FlatItem.Name = a.Name;
                        FlatItems.Add(FlatItem);
                        itemClassificationModelsLists.Add(a.Id);
                    });
                }
            });
            ApplicationPermissionTreeModel ApplicationPermissionTreeModel = new ApplicationPermissionTreeModel();
            ApplicationPermissionTreeModel.TreeItems = ApplicationPermissionModel.Children;
            ApplicationPermissionTreeModel.Ids = itemClassificationModelsLists;
            ApplicationPermissionTreeModel.FlatItems = FlatItems;
            return ApplicationPermissionTreeModel;
        }
        public IEnumerable<ApplicationPermissionModel> GetAllChildren(ApplicationPermissionModel item)
        {
            return item.Children.Concat(item.Children.SelectMany(GetAllChildren));
        }
        [HttpGet]
        [Route("GetFileProfileTypeFlatItems")]
        public FileProfileTypeModel GetFileProfileTypeFlatItems(int? id)
        {
            FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel();
            var x = _context.FileProfileType.Select(s => new
            {
                s.FileProfileTypeId,
                s.ParentId,
                s.Name,
                s.Description,
                s.ProfileId,
                s.StatusCodeId,
                s.AddedByUserId,
                s.AddedDate,
                s.ModifiedByUserId,
                s.ModifiedDate,
                s.IsExpiryDate,
                s.IsAllowMobileUpload,
                s.IsDocumentAccess,
                s.ShelfLifeDurationId,
                s.ShelfLifeDuration,
                s.Hints,
                s.IsEnableCreateTask,
                s.IsCreateByMonth,
                s.IsCreateByYear,
                s.IsHidden,
                s.ProfileInfo,
                s.IsTemplateCaseNo,
                s.TemplateTestCaseId,
                s.SessionId,
            }).FirstOrDefault(t => t.FileProfileTypeId == id);
            if (x != null)
            {
                if (x.ParentId != null)
                {
                    var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                    var fileProfileType = fileProfileTypeItems.FirstOrDefault(w => w.FileProfileTypeId == id);
                    List<long> FolderPathLists = new List<long>();
                    FolderPathLists = GetAllFileProfileTypeId(fileProfileTypeItems, fileProfileType, FolderPathLists);
                    fileProfileTypeModel.FileProfileTypeMainId = FolderPathLists.FirstOrDefault();
                }
                else
                {
                    fileProfileTypeModel.FileProfileTypeMainId = x.FileProfileTypeId;
                }
                fileProfileTypeModel.FileProfileTypeId = x.FileProfileTypeId;
                fileProfileTypeModel.Id = x.FileProfileTypeId;
                fileProfileTypeModel.ParentId = x.ParentId;
                fileProfileTypeModel.Name = x.Name;
                fileProfileTypeModel.Description = x.Description;
                fileProfileTypeModel.ProfileId = x.ProfileId;
                fileProfileTypeModel.AddedByUserID = x.AddedByUserId;
                fileProfileTypeModel.SessionId = x.SessionId;
                fileProfileTypeModel.ModifiedByUserID = x.ModifiedByUserId;
                fileProfileTypeModel.StatusCodeID = x.StatusCodeId;
                fileProfileTypeModel.IsExpiryDate = x.IsExpiryDate;
                fileProfileTypeModel.IsDocumentAccess = x.IsDocumentAccess;
                fileProfileTypeModel.ShelfLifeDuration = x.ShelfLifeDuration;
                fileProfileTypeModel.ShelfLifeDurationId = x.ShelfLifeDurationId;
                fileProfileTypeModel.IsHidden = x.IsHidden;
                fileProfileTypeModel.ProfileInfo = x.ProfileInfo;
                fileProfileTypeModel.IsTemplateCaseNo = x.IsTemplateCaseNo;
                fileProfileTypeModel.TemplateTestCaseId = x.TemplateTestCaseId;
            }
            return fileProfileTypeModel;
        }

        [HttpGet]
        [Route("GetFileProfileTypeDetailsByProfileId")]
        public FileProfileTypeDetailModel GetFileProfileTypeDetailsByProfileId(int? id)
        {
            FileProfileTypeDetailModel fileProfileTypeModel = new FileProfileTypeDetailModel();
            var documentProfile = _context.DocumentProfileNoSeries.Select(s => new
            {
                s.Abbreviation1,
                s.Name,
                s.Description,
                s.ProfileId,
            }).Where(t => t.ProfileId == id).FirstOrDefault();
            if (documentProfile != null)
            {
                List<NumberSeriesCodeModel> numberSeriesCodes = new List<NumberSeriesCodeModel>();
                if (!string.IsNullOrEmpty(documentProfile.Abbreviation1))
                {
                    numberSeriesCodes = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(documentProfile.Abbreviation1).ToList());
                }
                fileProfileTypeModel.ProfileId = id;
                fileProfileTypeModel.IsCompany = numberSeriesCodes.Any(n => n.Index == 1);
                fileProfileTypeModel.IsDepartment = numberSeriesCodes.Any(n => n.Index == 2);
                fileProfileTypeModel.IsSection = numberSeriesCodes.Any(n => n.Index == 3);
                fileProfileTypeModel.IsSubSection = numberSeriesCodes.Any(n => n.Index == 4);

                if (fileProfileTypeModel.IsCompany.GetValueOrDefault(false))
                {
                    fileProfileTypeModel.Companies = _context.Plant.Select(p => new CodeMasterModel { Id = p.PlantId, CodeValue = p.PlantCode, CodeDescription = p.Description }).ToList();
                }
                if (fileProfileTypeModel.IsDivision.GetValueOrDefault(false))
                {
                    var plantIds = fileProfileTypeModel.Companies.Select(s => s.Id).ToList();
                    fileProfileTypeModel.Divisions = _context.Division.Where(d => plantIds.Contains(d.CompanyId.Value)).Select(p => new CodeMasterModel { Id = p.DivisionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
                }
                if (fileProfileTypeModel.IsDepartment.GetValueOrDefault(false))
                {
                    var divisionIds = fileProfileTypeModel.Divisions.Select(d => d.Id).ToList();
                    fileProfileTypeModel.Departments = _context.Department.Where(d => divisionIds.Contains(d.DivisionId.Value)).Select(p => new CodeMasterModel { Id = p.DepartmentId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
                }
                if (fileProfileTypeModel.IsSection.GetValueOrDefault(false))
                {
                    var departmentIds = fileProfileTypeModel.Departments.Select(d => d.Id).ToList();
                    fileProfileTypeModel.Sections = _context.Section.Where(d => departmentIds.Contains(d.DepartmentId.Value)).Select(p => new CodeMasterModel { Id = p.SectionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
                }
                if (fileProfileTypeModel.IsSubSection.GetValueOrDefault(false))
                {
                    var sectionIds = fileProfileTypeModel.Sections.Select(d => d.Id).ToList();
                    fileProfileTypeModel.SubSections = _context.SubSection.Where(d => sectionIds.Contains(d.SectionId.Value)).Select(p => new CodeMasterModel { Id = p.SubSectionId, CodeValue = p.Name, CodeDescription = p.Description }).ToList();
                }
            }

            return fileProfileTypeModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeTree")]
        public List<FileProfileTypeModel> GetFileProfileTypeTree(int? id, int userId, bool isHidden)
        {
            var itemClassificationTree = _context.FileProfileType.FirstOrDefault(t => t.FileProfileTypeId == id);
            if (itemClassificationTree == null)
            {
                return null;
            }

            var classificationTree = _context.FileProfileType


                .AsNoTracking().Select(s => new
                {
                    s.FileProfileTypeId,
                    s.ParentId,
                    s.Name,
                    s.Description,
                    s.ProfileId,
                    s.StatusCodeId,
                    s.AddedByUserId,
                    s.AddedDate,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    s.IsExpiryDate,
                    s.IsAllowMobileUpload,
                    s.IsDocumentAccess,
                    s.ShelfLifeDurationId,
                    s.ShelfLifeDuration,
                    s.Hints,
                    s.IsEnableCreateTask,
                    s.IsCreateByMonth,
                    s.IsCreateByYear,
                    s.IsHidden,
                    s.ProfileInfo,
                    s.IsTemplateCaseNo,
                    s.TemplateTestCaseId,
                    s.SessionId,
                })
             .ToList();
            if (isHidden == false)
            {
                classificationTree = classificationTree.Where(w => w.IsHidden != true).ToList();
            }

            var lookup = classificationTree.ToLookup(x => x.ParentId);

            Func<long?, List<FileProfileTypeModel>> build = null;
            build = pid =>
                lookup[pid]
                    .Select(x => new FileProfileTypeModel()
                    {
                        FileProfileTypeId = x.FileProfileTypeId,
                        Id = x.FileProfileTypeId,
                        ParentId = x.ParentId,
                        Name = x.Name,
                        Description = x.Description,
                        ProfileId = x.ProfileId,
                        AddedByUserID = x.AddedByUserId,
                        ModifiedByUserID = x.ModifiedByUserId,
                        StatusCodeID = x.StatusCodeId,
                        IsExpiryDate = x.IsExpiryDate,
                        IsDocumentAccess = x.IsDocumentAccess,
                        ShelfLifeDuration = x.ShelfLifeDuration,
                        ShelfLifeDurationId = x.ShelfLifeDurationId,
                        IsHidden = x.IsHidden,
                        ProfileInfo = x.ProfileInfo,
                        IsTemplateCaseNo = x.IsTemplateCaseNo,
                        TemplateTestCaseId = x.TemplateTestCaseId,
                        SessionId=x.SessionId,
                        Children = build(x.FileProfileTypeId),

                    })
                    .ToList();
            var nestedlist = build(null).ToList();

            return nestedlist.Where(w => w.FileProfileTypeId == id).ToList();
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFileProfileType")]
        public FileProfileTypeModel Post(FileProfileTypeModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var fileProfileType = new FileProfileType
            {
                ParentId = value.ParentId,
                Name = value.Name,
                Description = value.Description,
                ProfileId = value.ProfileId,
                IsExpiryDate = value.IsExpiryDate,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                IsAllowMobileUpload = value.IsAllowMobileUpload,
                IsDocumentAccess = value.IsDocumentAccess,
                ShelfLifeDurationId = value.ShelfLifeDurationId,
                ShelfLifeDuration = value.ShelfLifeDuration,
                Hints = value.Hints,
                IsEnableCreateTask = value.IsEnableCreateTask,
                IsCreateByMonth = value.IsCreateByMonth,
                IsCreateByYear = value.IsCreateByYear,
                IsHidden = value.IsHidden,
                ProfileInfo = value.ProfileInfo,
                IsTemplateCaseNo = value.IsTemplateCaseNo,
                TemplateTestCaseId = value.TemplateTestCaseId,
                SessionId=value.SessionId,
            };
            _context.FileProfileType.Add(fileProfileType);
            _context.SaveChanges();
            if (fileProfileType.ParentId != null)
            {
                var documentUserRoles = _context.DocumentUserRole.Where(d => d.FileProfileTypeId == fileProfileType.ParentId).AsNoTracking().ToList();
                documentUserRoles.ForEach(d =>
                {
                    DocumentUserRole userRole = new DocumentUserRole
                    {
                        UserId = d.UserId,
                        RoleId = d.RoleId,
                        FileProfileTypeId = fileProfileType.FileProfileTypeId,
                        UserGroupId = d.UserGroupId
                    };
                    _context.DocumentUserRole.Add(userRole);
                });
                _context.SaveChanges();
                FileProfileSetupFormController fileProfileSetupForm = new FileProfileSetupFormController(_context, _mapper);
                var fileProfileSetupForms = fileProfileSetupForm.Get(value.ParentId).ToList();
                if (fileProfileSetupForms != null && fileProfileSetupForms.Count > 0)
                {
                    fileProfileSetupForms.ForEach(a =>
                    {
                        a.FileProfileTypeId = fileProfileType.FileProfileTypeId;
                        a.AddedByUserID = value.AddedByUserID;
                        fileProfileSetupForm.Post(a);
                    });

                }
            }
            value.FileProfileTypeId = fileProfileType.FileProfileTypeId;
            return value;
        }
        [HttpPut]
        [Route("UpdateFileProfileType")]
        public FileProfileTypeModel Put(FileProfileTypeModel value)
        {
            var fileProfileType = _context.FileProfileType.SingleOrDefault(p => p.FileProfileTypeId == value.FileProfileTypeId);
            value.SessionId ??= Guid.NewGuid();
            fileProfileType.ModifiedByUserId = value.ModifiedByUserID;
            fileProfileType.ProfileId = value.ProfileId;
            fileProfileType.ModifiedDate = DateTime.Now;
            fileProfileType.ParentId = value.ParentId;
            fileProfileType.IsExpiryDate = value.IsExpiryDate;
            fileProfileType.Name = value.Name;
            fileProfileType.Description = value.Description;
            fileProfileType.StatusCodeId = value.StatusCodeID.Value;
            fileProfileType.IsAllowMobileUpload = value.IsAllowMobileUpload;
            fileProfileType.IsDocumentAccess = value.IsDocumentAccess;
            fileProfileType.ShelfLifeDuration = value.ShelfLifeDuration;
            fileProfileType.ShelfLifeDurationId = value.ShelfLifeDurationId;
            fileProfileType.Hints = value.Hints;
            fileProfileType.IsEnableCreateTask = value.IsEnableCreateTask;
            fileProfileType.ProfileTypeInfo = value.ProfileTypeInfo;
            fileProfileType.IsCreateByYear = value.IsCreateByYear;
            fileProfileType.IsCreateByMonth = value.IsCreateByMonth;
            fileProfileType.IsHidden = value.IsHidden;
            fileProfileType.ProfileInfo = value.ProfileInfo;
            fileProfileType.IsTemplateCaseNo = value.IsTemplateCaseNo;
            fileProfileType.TemplateTestCaseId = value.TemplateTestCaseId;
            fileProfileType.SessionId = value.SessionId;
            _context.SaveChanges();
            var FileProfileSetupFormCount = _context.FileProfileSetupForm.Where(w => w.FileProfileTypeId == fileProfileType.FileProfileTypeId).Count();
            if (FileProfileSetupFormCount == 0)
            {
                FileProfileSetupFormController fileProfileSetupForm = new FileProfileSetupFormController(_context, _mapper);
                var fileProfileSetupForms = fileProfileSetupForm.Get(value.ParentId).ToList();
                if (fileProfileSetupForms != null && fileProfileSetupForms.Count > 0)
                {
                    fileProfileSetupForms.ForEach(a =>
                    {
                        a.FileProfileTypeId = fileProfileType.FileProfileTypeId;
                        a.AddedByUserID = value.AddedByUserID;
                        fileProfileSetupForm.Post(a);
                    });

                }
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateFileProfileTypeInfo")]
        public FileProfileTypeModel UpdateFileProfileTypeInfo(ProfileTypeInfoModel value)
        {
            var fileProfileType = _context.FileProfileType.SingleOrDefault(p => p.FileProfileTypeId == value.FileProfileTypeId);
            fileProfileType.ModifiedByUserId = value.ModifiedByUserID;
            fileProfileType.ProfileTypeInfo = value.ProfileTypeInfo;
            fileProfileType.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            FileProfileTypeModel fileProfileTypeModel = new FileProfileTypeModel();
            fileProfileTypeModel.ModifiedByUserID = fileProfileType.ModifiedByUserId.Value;
            fileProfileTypeModel.ProfileTypeInfo = fileProfileType.ProfileTypeInfo;
            fileProfileTypeModel.FileProfileTypeId = fileProfileType.FileProfileTypeId;
            fileProfileTypeModel.ModifiedDate = fileProfileType.ModifiedDate;
            fileProfileTypeModel.SessionId = fileProfileType.SessionId;
            return fileProfileTypeModel;
        }
        public IEnumerable<FileProfileTypeModel> GetAllChildren(FileProfileTypeModel item)
        {
            return item.Children.Concat(item.Children.SelectMany(GetAllChildren));
        }
        // DELETE: api/ApiWithActions/5
        [HttpPost]
        [Route("DeleteFileProfileType")]
        public void Delete(SearchModel searchModel)
        {
            FileProfileTypeModel itemClassificationModel = new FileProfileTypeModel();
            List<FileProfileTypeModel> itemClassificationModels = new List<FileProfileTypeModel>();
            itemClassificationModel.Children = GetFileProfileTypeTree(searchModel.Id, 1, false);
            itemClassificationModels.Add(itemClassificationModel);
            var allDescendantNames = GetAllChildren(itemClassificationModel).Select(child => child.Children).ToList();
            List<FileProfileTypeModel> itemClassificationModelsLists = new List<FileProfileTypeModel>();
            allDescendantNames.ForEach(h =>
            {
                FileProfileTypeModel itemClassificationModelList = new FileProfileTypeModel();
                if (h.Count > 0)
                {
                    h.ForEach(a =>
                    {
                        FileProfileTypeModel itemClassificationModelList = new FileProfileTypeModel();
                        itemClassificationModelList.FileProfileTypeId = a.FileProfileTypeId;
                        itemClassificationModelList.ParentId = a.ParentId;
                        itemClassificationModelsLists.Add(itemClassificationModelList);
                    });
                }
            });
            FileProfileTypeModel itemClassificationModelList = new FileProfileTypeModel();

            itemClassificationModelList.FileProfileTypeId = searchModel.MasterTypeID.Value;
            itemClassificationModelList.ParentId = searchModel.MasterTypeID.Value;
            itemClassificationModelsLists.Add(itemClassificationModelList);
            var lists = itemClassificationModelsLists.Where(p => p.ParentId >= searchModel.MasterTypeID).OrderByDescending(p => p.FileProfileTypeId).ToList();
            List<long> fileProfileTypeIds = new List<long>();
            List<FileProfileType> ItemClassificationForms = new List<FileProfileType>();
            if (lists.Count > 0)
            {
                var errorMessages = "";
                try
                {
                    lists.ToList().ForEach(b =>
                    {
                        fileProfileTypeIds.Add(b.FileProfileTypeId);
                        ItemClassificationForms.AddRange(_context.FileProfileType.Where(w => w.FileProfileTypeId == b.FileProfileTypeId).ToList());
                        if (ItemClassificationForms != null)
                        {
                            ItemClassificationForms.ForEach(i =>
                            {
                                fileProfileTypeIds.Add(i.FileProfileTypeId);
                            });
                        }
                    });
                    var documentIdList = _context.Documents.Select(s => new { s.DocumentId, s.FilterProfileTypeId }).Where(w => fileProfileTypeIds.Contains(w.FilterProfileTypeId.Value)).ToList();
                    var documentIds = documentIdList.Select(s => s.DocumentId).ToList();
                    if (documentIds.Count > 0)
                    {
                        var queryNotes = string.Format("delete from Notes Where DocumentId in" + '(' + "{0}" + ')', string.Join(',', documentIds));
                        if (queryNotes != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryNotes);
                        }
                    }
                    if (fileProfileTypeIds.Count > 0)
                    {

                        var query = string.Format("delete from Documents Where FilterProfileTypeID in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (query != null)
                        {
                            _context.Database.ExecuteSqlRaw(query);
                        }
                        var queryBlank = string.Format("delete from ProductionDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryBlank != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryBlank);
                        }
                        var queryProduction = string.Format("delete from BlanketOrderAttachment Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryProduction != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryProduction);
                        }
                        var querySales = string.Format("delete from SalesAdhocNovateAttachment Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (querySales != null)
                        {
                            _context.Database.ExecuteSqlRaw(querySales);
                        }
                        var queryContract = string.Format("delete from ContractDistributionAttachment Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryContract != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryContract);
                        }
                        var queryOrder = string.Format("delete from SalesOrderAttachment Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryOrder != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryOrder);
                        }
                        var queryProductionMachine = string.Format("delete from CommonFieldsProductionMachineDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryProductionMachine != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryProductionMachine);
                        }
                        var queryTransfer = string.Format("delete from TransferBalanceQtyDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryTransfer != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryTransfer);
                        }
                        var queryQuotation = string.Format("delete from QuotationHistoryDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryQuotation != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryQuotation);
                        }
                        var queryPurchase = string.Format("delete from PurchaseOrderDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryPurchase != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryPurchase);
                        }
                        var queryRecipe = string.Format("delete from ProductRecipeDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryRecipe != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryRecipe);
                        }
                        var queryPack = string.Format("delete from PackagingHistoryItemLineDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryPack != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryPack);
                        }
                        var queryIpir = string.Format("delete from IpirlineDocument Where FileProfileTypeId in" + '(' + "{0}" + ')', string.Join(',', fileProfileTypeIds));
                        if (queryIpir != null)
                        {
                            _context.Database.ExecuteSqlRaw(queryIpir);
                        }
                    }

                    var DocumentUserRole = _context.DocumentUserRole.Where(w => fileProfileTypeIds.Contains(w.FileProfileTypeId.Value)).ToList();
                    if (DocumentUserRole != null)
                    {
                        _context.DocumentUserRole.RemoveRange(DocumentUserRole);
                        _context.SaveChanges();
                    }
                    _context.FileProfileType.RemoveRange(ItemClassificationForms);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (string.IsNullOrEmpty(errorMessages))
                    {
                        errorMessages = "There is a transaction record for this company record cannot be deleted!";
                    }
                    throw new AppException(errorMessages, ex);
                }
            }
        }
        [HttpPost]
        [Route("GetSubFileProfileTypeDocuments")]
        public List<DocumentsModel> GetSubFileProfileTypeDocuments(FileProfileSearchModel fileProfileSearchModel)
        {
            DocumentTypeModel documentTypeModel = new DocumentTypeModel();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            var fileProfileTypeIds = _context.FileProfileType.Where(f => f.ParentId == fileProfileSearchModel.FileProfileTypeId).Select(s => s.FileProfileTypeId).ToList();
            if (fileProfileTypeIds.Count > 0)
            {
                fileProfileTypeIds.Add(fileProfileSearchModel.FileProfileTypeId.Value);
                var subfileProfileTypeIds = _context.FileProfileType.Where(f => fileProfileTypeIds.Contains(f.ParentId.Value)).Select(s => s.FileProfileTypeId).ToList();
                if (subfileProfileTypeIds.Count > 0)
                {
                    fileProfileTypeIds.AddRange(subfileProfileTypeIds);
                }
            }
            var fileProfileType = _context.FileProfileType.Select(s => new
            {
                s.FileProfileTypeId,
                s.Name,
                s.IsDocumentAccess,
                s.IsEnableCreateTask,
                s.IsExpiryDate,
                s.IsHidden,
                s.AddedByUserId,
                s.SessionId,
            }).AsNoTracking().Where(s => s.IsHidden == null || s.IsHidden == false).ToList();
            // var roleItemsList = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id && w.UserId == userId).ToList();

            var documents = _context.Documents
               .Include(f => f.FilterProfileType)
               .Include(a => a.FileProfileTypeDynamicForm)
               .Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.FileSize,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   s.CloseDocumentId,
                   s.DocumentParentId,
                   s.IsMobileUpload,
                   s.TableName,
                   s.ExpiryDate,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   s.IsLocked,
                   s.LockedByUserId,
                   s.LockedDate,
                   s.AddedDate,
                   s.IsCompressed,
                   s.FileIndex,
                   s.ProfileNo,
                   s.IsLatest,
                   s.ArchiveStatusId,
                   s.Description,
                   s.IsWikiDraft,
                   s.IsWiki,
                   s.FilePath,
                   DynamicFormDatas = s.FileProfileTypeDynamicForm.FirstOrDefault(f => f.DocumentId == s.DocumentId).DynamicFormData,
               }).OrderByDescending(o => o.DocumentId)
               .Where(s => fileProfileTypeIds.Contains(s.FilterProfileTypeId.Value) && (s.FileName.Contains(fileProfileSearchModel.FileName)|| s.DynamicFormDatas.Contains(fileProfileSearchModel.FileName)) && s.IsLatest == true && s.ArchiveStatusId != 2562)
               .AsNoTracking().ToList();

            DocumentTypeModel DocumentTypeModel = new DocumentTypeModel();
            var documentsIds = documents.Select(s => s.DocumentId).Distinct().ToList();


            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {

                var documentcount = documents?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();

                var name = s.FileName != null ? s.FileName?.Substring(s.FileName.LastIndexOf(".")) : "";
                var fileName = s.FileName?.Split(name);



                DocumentsModel documentsModels = new DocumentsModel();

                documentsModels.SessionId = s.SessionId;
                documentsModels.DocumentID = s.DocumentId;
                documentsModels.FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                documentsModels.ContentType = s.ContentType;
                documentsModels.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                documentsModels.UploadDate = s.UploadDate;
                documentsModels.SessionID = s.SessionId;
                documentsModels.FilterProfileTypeId = s.FilterProfileTypeId;
                documentsModels.FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.Name;
                documentsModels.DocumentParentId = s.DocumentParentId;
                documentsModels.TableName = s.TableName;
                documentsModels.IsMobileUpload = s.IsMobileUpload;
                documentsModels.Type = "Document";
                documentsModels.ExpiryDate = s.ExpiryDate;
                documentsModels.FileIndex = s.FileIndex;
                documentsModels.TotalDocument = documentcount == 1 ? 1 : (documentcount + 1);
                documentsModels.UploadedByUserId = s.AddedByUserId;
                documentsModels.ModifiedByUserID = s.ModifiedByUserId;
                documentsModels.AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate;
                documentsModels.AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName;
                documentsModels.IsLocked = s.IsLocked;
                documentsModels.LockedByUserId = s.LockedByUserId;
                documentsModels.LockedDate = s.LockedDate;
                documentsModels.AddedByUserID = s.AddedByUserId;
                documentsModels.IsCompressed = s.IsCompressed;
                documentsModels.LockedByUser = appUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName;
                documentsModels.isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.IsDocumentAccess;
                documentsModels.IsEnableCreateTask = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.IsEnableCreateTask;
                documentsModels.CloseDocumentId = s.CloseDocumentId;
                documentsModels.CssClass = s.CloseDocumentId != null && s.CloseDocumentId == 2561 ? "blue-grey lighten - 3" : "transparent";
                documentsModels.ProfileNo = s.ProfileNo;
                documentsModels.FilePath = s.FilePath;
                documentsModels.FileProfileTypeAddedByUserId = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.AddedByUserId;
                documentsModel.Add(documentsModels);

            });
            //DocumentTypeModel.DocumentsData = documentsModel.OrderByDescending(a => a.DocumentID).ToList();
            return documentsModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeDocument")]
        public DocumentTypeModel GetFileProfileTypeDocument(long? id, long? userId)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();

            var fileProfileType = _context.FileProfileType.Select(s => new
            {
                s.FileProfileTypeId,
                s.Name,
                s.IsDocumentAccess,
                s.IsEnableCreateTask,
                s.IsExpiryDate,
                s.IsHidden,
                s.AddedByUserId
            }).AsNoTracking().Where(s => s.IsHidden == null || s.IsHidden == false).ToList();
            var linkfileProfileTypes = _context.LinkFileProfileTypeDocument.Where(l => l.FileProfileTypeId == id).ToList();
            var roleItemsList = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id && w.UserId == userId).ToList();
            var linkfileProfileTypeDocumentids = _context.LinkFileProfileTypeDocument.Where(l => l.FileProfileTypeId == id).Select(s => s.DocumentId).Distinct().ToList();
            var documents = _context.Documents
                .Include(f => f.FilterProfileType)
                .Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    //FilterProfileType = s.FilterProfileType.Name,
                    s.CloseDocumentId,
                    //isDocumentAccess = s.FilterProfileType.IsDocumentAccess,
                    s.DocumentParentId,
                    s.IsMobileUpload,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    s.IsLocked,
                    s.LockedByUserId,
                    s.LockedDate,
                    s.AddedDate,
                    s.IsCompressed,
                    s.FileIndex,
                    s.ProfileNo,
                    s.IsLatest,
                    s.ArchiveStatusId,
                    s.Description,
                    s.IsWikiDraft,
                    s.IsWiki,
                    s.FilePath,
                    s.IsDelete
                }).OrderByDescending(o => o.DocumentId)
                .Where(s => s.FilterProfileTypeId == id && s.IsLatest == true && (s.IsDelete == null || s.IsDelete == false) && s.ArchiveStatusId != 2562 || (linkfileProfileTypeDocumentids.Contains(s.DocumentId) && s.IsLatest == true) )
                .AsNoTracking().ToList();


            DocumentTypeModel DocumentTypeModel = new DocumentTypeModel();
            var documentsIds = documents.Select(s => s.DocumentId).Distinct().ToList();
            var notes = _context.Notes.Select(s => new { s.NotesId, s.DocumentId }).Where(s => documentsIds.Contains(s.DocumentId.Value)).AsNoTracking().ToList();
            var taskMasternotes = _context.TaskMaster.Select(s => new { s.TaskId, s.SourceId }).Where(s => documentsIds.Contains(s.SourceId.Value)).AsNoTracking().ToList();
            if (linkfileProfileTypeDocumentids.Count > 0)
            {
                linkfileProfileTypeDocumentids.ForEach(l =>
                {
                    documentsIds.Add(l.Value);
                });

            }
            // var setAccess = _context.DocumentUserRole.Where(s => documentsIds.Contains(s.DocumentId.Value)).Distinct().ToList();
            var setAccess = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id || documentsIds.Contains(w.DocumentId.Value)).ToList();

            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                // var documentExits = documentsModel.Where(w => w.DocumentParentId == s.DocumentId && w.TableName == "Document").Count();
                var documentcount = documents?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();

                var name = s.FileName != null ? s.FileName?.Substring(s.FileName.LastIndexOf(".")) : "";
                var fileName = s.FileName?.Split(name);

                //var testname = 
                //var setAccessdocExits = setAccess.Where(w => w.DocumentId == s.DocumentId).Count();
                //var setAccessExits = true;
                //bool? isSetAccess = false;
                //if (s.AddedByUserId != userId)
                //{
                //    if (setAccessdocExits > 0)
                //    {
                //        setAccessExits = false;

                //        var userExits = setAccess.Where(w => w.UserId == userId && w.DocumentId == s.DocumentId).FirstOrDefault();
                //        if (userExits != null)
                //        {

                //            setAccessExits = true;
                //            isSetAccess = true;
                //        }
                //        else
                //        {
                //            var withsetAccess = setAccess.Where(w => w.UserId != userId && w.DocumentId == s.DocumentId).Count();
                //            if (withsetAccess > 0)
                //            {
                //                setAccessExits = false;

                //            }
                //        }
                //    }
                //}


                DocumentsModel documentsModels = new DocumentsModel();
                var setAccessFlag = roleItemsList.Where(a => a.UserId == userId && a.DocumentId == s.DocumentId).Count();
                documentsModels.NotesCount = notes.Where(a => a.DocumentId == s.DocumentId).Count();
                documentsModels.NotesColor = "";
                var taskNotesCount = taskMasternotes.Where(a => a.SourceId == s.DocumentId).Count();
                if (taskNotesCount > 0)
                {
                    documentsModels.NotesColor = "green";
                }
                else
                {
                    if (documentsModels.NotesCount > 0)
                    {
                        documentsModels.NotesColor = "blue";
                    }
                }
                documentsModels.SetAccessFlag = setAccessFlag > 0 ? true : false;
                documentsModels.SessionId = s.SessionId;
                documentsModels.DocumentID = s.DocumentId;
                documentsModels.FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + name : s.FileName;
                documentsModels.ContentType = s.ContentType;
                documentsModels.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                documentsModels.UploadDate = s.UploadDate;
                documentsModels.SessionID = s.SessionId;
                documentsModels.FilterProfileTypeId = s.FilterProfileTypeId;
                documentsModels.FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.Name;
                documentsModels.DocumentParentId = s.DocumentParentId;
                documentsModels.TableName = s.TableName;
                documentsModels.IsMobileUpload = s.IsMobileUpload;
                documentsModels.Type = "Document";
                documentsModels.ExpiryDate = s.ExpiryDate;
                documentsModels.FileIndex = s.FileIndex;
                documentsModels.TotalDocument = documentcount == 1 ? 1 : (documentcount + 1);
                documentsModels.UploadedByUserId = s.AddedByUserId;
                documentsModels.ModifiedByUserID = s.ModifiedByUserId;
                documentsModels.AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate;
                documentsModels.AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName;
                documentsModels.IsLocked = s.IsLocked;
                documentsModels.LockedByUserId = s.LockedByUserId;
                documentsModels.LockedDate = s.LockedDate;
                documentsModels.AddedByUserID = s.AddedByUserId;
                documentsModels.IsCompressed = s.IsCompressed;
                documentsModels.LockedByUser = appUsers.FirstOrDefault(f => f.UserId == s.LockedByUserId)?.UserName;
                documentsModels.isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.IsDocumentAccess;
                documentsModels.IsEnableCreateTask = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.IsEnableCreateTask;
                documentsModels.CloseDocumentId = s.CloseDocumentId;
                documentsModels.CssClass = s.CloseDocumentId != null && s.CloseDocumentId == 2561 ? "blue-grey lighten - 3" : "transparent";
                documentsModels.ProfileNo = s.ProfileNo;
                documentsModels.FilePath = s.FilePath;
                documentsModels.FileProfileTypeAddedByUserId = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.AddedByUserId;
                if (documentsModels.isDocumentAccess == true && (documentsModels.IsEnableCreateTask == false || documentsModels.IsEnableCreateTask == null))
                {
                    documentsModels.ItemsAllFlag = true;
                }
                if (documentsModels.isDocumentAccess != true && (documentsModels.IsEnableCreateTask == false || documentsModels.IsEnableCreateTask == null))
                {
                    documentsModels.ItemsFlag = true;
                }
                if (documentsModels.isDocumentAccess != true && documentsModels.IsEnableCreateTask == true)
                {
                    documentsModels.ItemsWithCreateTask = true;
                }
                if (documentsModels.isDocumentAccess == true && documentsModels.IsEnableCreateTask == true)
                {
                    documentsModels.ItemsAllWithCreateTask = true;
                }
                //documentsModels.isSetAccess = isSetAccess;
                if (setAccess.Count > 0)
                {
                    var roleDocItem = setAccess.FirstOrDefault(u => u.DocumentId == s.DocumentId);
                    if (roleDocItem != null)
                    {
                        var roleItem = setAccess.FirstOrDefault(u => u.UserId == userId && u.DocumentId == s.DocumentId);
                        if (roleItem != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                            documentsModels.DocumentPermissionData = permissionData;
                        }
                        else
                        {
                            documentsModels.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = true, IsRename = false, IsCopy = false, IsCreateFolder = false, IsEdit = false, IsMove = false, IsShare = false, IsFileDelete = false };
                        }
                    }
                    else
                    {

                        var filprofilepermission = setAccess.FirstOrDefault(u => u.FileProfileTypeId == s.FilterProfileTypeId && u.DocumentId == null && u.UserId == userId);
                        if (filprofilepermission != null)
                        {
                            DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                            var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)filprofilepermission.RoleId);
                            documentsModels.DocumentPermissionData = permissionData;
                        }
                        else
                        {
                            documentsModels.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = true, IsCopy = true, IsCreateFolder = true, IsEdit = true, IsMove = true, IsShare = true, IsFileDelete = true };
                        }
                    }
                }
                else
                {
                    documentsModels.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
                }
                documentsModels.IsExpiryDate = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FilterProfileTypeId)?.IsExpiryDate;
                var description = linkfileProfileTypes?.Where(f => f.FileProfileTypeId == id && f.TransactionSessionId == s.SessionId && f.DocumentId == s.DocumentId).FirstOrDefault()?.Description;
                if (description != null)
                {
                    documentsModels.Description = description;
                }
                else
                {
                    documentsModels.Description = s.Description;
                }
                if (documentsModels.DocumentPermissionData != null)
                {
                    if (documentsModels.DocumentPermissionData.IsRead == true)
                    {
                        documentsModel.Add(documentsModels);
                    }
                    else if (documentsModels.DocumentPermissionData.IsRead == false)
                    {

                    }
                }
                else
                {
                    documentsModel.Add(documentsModels);
                }


            });
            var blanketOrderAttachment = _context.BlanketOrderAttachment
                .Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.BlanketOrderAttachmentId,
                    s.BlanketOrderId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    //AddedByUserId = s.BlanketOrder.AddedByUserId,
                    //ModifiedByUserId = s.BlanketOrder.ModifiedByUserId,
                    //ModifiedDate = s.BlanketOrder.ModifiedDate,
                    //ModifiedByUser = s.BlanketOrder.ModifiedByUser.UserName,
                    //AddedByUser = s.BlanketOrder.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                })
            .AsNoTracking().ToList();

            var blanketIds = blanketOrderAttachment.Select(s => s.BlanketOrderId).ToList();
            var blanketOrders = _context.BlanketOrder.Select(s => new { s.AddedByUserId, s.ModifiedByUserId, s.BlanketOrderId, s.ModifiedDate, s.AddedDate }).Where(f => blanketIds.Contains(f.BlanketOrderId)).ToList();

            if (blanketOrderAttachment != null && blanketOrderAttachment.Count > 0)
            {
                blanketOrderAttachment.ForEach(s =>
                {
                    var blanketOrder = blanketOrders.FirstOrDefault(b => b.BlanketOrderId == s.BlanketOrderId);
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.BlanketOrderAttachmentId && w.TableName == "BlanketOrderAttachment").Count();
                    var documentcount = blanketOrderAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.BlanketOrderAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            Type = "BlanketOrderAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = blanketOrder.AddedByUserId,
                            ModifiedByUserID = blanketOrder.ModifiedByUserId,
                            AddedDate = blanketOrder.ModifiedDate == null ? s.UploadDate : blanketOrder.ModifiedDate,
                            AddedByUser = blanketOrder.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == blanketOrder.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == blanketOrder.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var contractDistributionAttachment = _context.ContractDistributionAttachment

                .Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.ContractDistributionAttachmentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (contractDistributionAttachment != null && contractDistributionAttachment.Count > 0)
            {
                contractDistributionAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.ContractDistributionAttachmentId && w.TableName == "ContractDistributionAttachment").Count();
                    var documentcount = contractDistributionAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.ContractDistributionAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "ContractDistributionAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUserID = s.AddedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var salesAdhocNovateAttachment = _context.SalesAdhocNovateAttachment

                .Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.SalesAdhocNovateAttachmentId,
                    s.SessionId,
                    //FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (salesAdhocNovateAttachment != null && salesAdhocNovateAttachment.Count > 0)
            {
                salesAdhocNovateAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.SalesAdhocNovateAttachmentId && w.TableName == "SalesAdhocNovateAttachment").Count();
                    var documentcount = salesAdhocNovateAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.SalesAdhocNovateAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            Type = "SalesAdhocNovateAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var salesOrderAttachment = _context.SalesOrderAttachment

                .Where(w => w.FileProfileTypeId == id).ToList();
            if (salesOrderAttachment != null && salesOrderAttachment.Count > 0)
            {
                salesOrderAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.SalesOrderAttachmentId && w.TableName == "SalesOrderAttachment").Count();
                    if (documentExits == 0)
                    {
                        var documentcount = salesOrderAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.SalesOrderAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "SalesOrderAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var commonFieldsProductionMachineDocument = _context.CommonFieldsProductionMachineDocument

                .Where(w => w.FileProfileTypeId == id).Select(s => new
                {
                    s.MachineDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    // AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (commonFieldsProductionMachineDocument != null && commonFieldsProductionMachineDocument.Count > 0)
            {
                commonFieldsProductionMachineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.MachineDocumentId && w.TableName == "CommonFieldsProductionMachineDocument").Count();
                    var documentcount = commonFieldsProductionMachineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.MachineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "CommonFieldsProductionMachineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var transferBalanceQtyDocument = _context.TransferBalanceQtyDocument

                .Where(w => w.FileProfileTypeId == id).Select(s => new
                {
                    s.TransferBalanceQtyDocumentId,
                    s.SessionId,
                    //FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (transferBalanceQtyDocument != null && transferBalanceQtyDocument.Count > 0)
            {
                transferBalanceQtyDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.TransferBalanceQtyDocumentId && w.TableName == "TransferBalanceQtyDocument").Count();
                    var documentcount = transferBalanceQtyDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.TransferBalanceQtyDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "TransferBalanceQtyDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var quotationHistoryDocument = _context.QuotationHistoryDocument

                .Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.QuotationHistoryDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (quotationHistoryDocument != null && quotationHistoryDocument.Count > 0)
            {
                quotationHistoryDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.QuotationHistoryDocumentId && w.TableName == "QuotationHistoryDocument").Count();
                    var documentcount = quotationHistoryDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.QuotationHistoryDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "QuotationHistoryDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var purchaseOrderDocument = _context.PurchaseOrderDocument

                .Where(w => w.FileProfileTypeId == id)
                .Select(s => new
                {
                    s.PurchaseOrderDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (purchaseOrderDocument != null && purchaseOrderDocument.Count > 0)
            {
                purchaseOrderDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.PurchaseOrderDocumentId && w.TableName == "PurchaseOrderDocument").Count();
                    var documentcount = purchaseOrderDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.PurchaseOrderDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "PurchaseOrderDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var productRecipeDocument = _context.ProductRecipeDocument

                .Where(w => w.FileProfileTypeId == id)
                .Select(s => new
                {
                    s.ProductRecipeDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (productRecipeDocument != null && productRecipeDocument.Count > 0)
            {
                productRecipeDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.ProductRecipeDocumentId && w.TableName == "ProductRecipeDocument").Count();
                    var documentcount = productRecipeDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.ProductRecipeDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "ProductRecipeDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument

                .Where(w => w.FileProfileTypeId == id).Select(s => new
                {
                    s.PackagingHistoryItemLineDocumentId,
                    s.SessionId,
                    //  FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (packagingHistoryItemLineDocument != null && packagingHistoryItemLineDocument.Count > 0)
            {
                packagingHistoryItemLineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.PackagingHistoryItemLineDocumentId && w.TableName == "PackagingHistoryItemLineDocument").Count();
                    var documentcount = packagingHistoryItemLineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.PackagingHistoryItemLineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "PackagingHistoryItemLineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var ipirlineDocument = _context.IpirlineDocument

                .Where(w => w.FileProfileTypeId == id).Select(s => new
                {
                    s.IpirlineDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    //isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (ipirlineDocument != null && ipirlineDocument.Count > 0)
            {
                ipirlineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.IpirlineDocumentId && w.TableName == "IpirlineDocument").Count();
                    var documentcount = ipirlineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.IpirlineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                            Type = "IpirlineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                            isDocumentAccess = fileProfileType.FirstOrDefault(p => p.FileProfileTypeId == s.FileProfileTypeId)?.IsDocumentAccess,

                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }


            DocumentTypeModel.DocumentsData = documentsModel.OrderByDescending(a => a.DocumentID).ToList();
            var closedocumentPermission = _context.CloseDocumentPermission.Where(f => f.FileProfileTypeId == id).ToList();
            var roleItems = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id).ToList();
            if (roleItems.Count > 0)
            {
                var roleItem = roleItems.FirstOrDefault(u => u.UserId == userId);
                if (roleItem != null)
                {
                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    DocumentTypeModel.DocumentPermissionData = permissionData;
                    if (closedocumentPermission.Count > 0)
                    {
                        var userpermission = closedocumentPermission.FirstOrDefault(f => f.UserId == userId);
                        if (userpermission != null)
                        {
                            DocumentTypeModel.DocumentPermissionData.IsCloseDocument = true;
                        }
                        else
                        {
                            DocumentTypeModel.DocumentPermissionData.IsCloseDocument = false;
                        }
                    }
                    else
                    {
                        DocumentTypeModel.DocumentPermissionData.IsCloseDocument = true;
                    }
                }
                else
                {
                    if (DocumentTypeModel.DocumentsData.Count > 0)
                    {
                        DocumentTypeModel.DocumentsData.ForEach(p =>
                        {
                            p.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = false, IsRename = false };
                            if (closedocumentPermission.Count > 0)
                            {
                                var userpermission = closedocumentPermission.FirstOrDefault(f => f.UserId == userId);
                                if (userpermission != null)
                                {
                                    p.DocumentPermissionData.IsCloseDocument = true;
                                }
                                else
                                {
                                    p.DocumentPermissionData.IsCloseDocument = false;
                                }
                            }
                            else
                            {
                                p.DocumentPermissionData.IsCloseDocument = true;
                            }
                        });
                    }
                }
            }
            else
            {

                DocumentTypeModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
                if (closedocumentPermission.Count > 0)
                {
                    var userpermission = closedocumentPermission.FirstOrDefault(f => f.UserId == userId);
                    if (userpermission != null)
                    {
                        DocumentTypeModel.DocumentPermissionData.IsCloseDocument = true;
                    }
                    else
                    {
                        DocumentTypeModel.DocumentPermissionData.IsCloseDocument = false;
                    }
                }
                else
                {
                    DocumentTypeModel.DocumentPermissionData.IsCloseDocument = true;
                }
            }
            DocumentTypeModel.OpenDocument = DocumentTypeModel.DocumentsData.Where(d => d.CloseDocumentId == null || d.CloseDocumentId < 0).ToList().Count();
            if (DocumentTypeModel != null)
            {
                DocumentTypeModel.IsExpiryDate = DocumentTypeModel.DocumentsData.FirstOrDefault()?.IsExpiryDate;
                DocumentTypeModel.TotalDocument = DocumentTypeModel.DocumentsData.ToList().Count();
                DocumentTypeModel.OpenDocument = DocumentTypeModel.DocumentsData.Where(d => d.CloseDocumentId == null || d.CloseDocumentId < 0).ToList().Count();
            }
            return DocumentTypeModel;
            //return documentsModel.OrderByDescending(a => a.DocumentID).ToList();
        }
        [HttpPut]
        [Route("CreateSubFileProfileTypes")]
        public MassWayFileProfileTypeModel CreateSubFileProfileTypes(MassWayFileProfileTypeModel value)
        {
            MassWayFileProfileTypeModel massWayFileProfileTypeModel = new MassWayFileProfileTypeModel();
            if (value.MassWayType.ToLower().Trim() == "create")
            {
                if (value.FileProfileTypeId != null && value.FileProfileTypeId > 0)
                {
                    var childProfileTypes = _context.FileProfileType.Where(d => d.ParentId == value.FileProfileTypeId).ToList();
                    var profileName = childProfileTypes.Select(s => s.Name).ToList();
                    var childProfileTypeIds = childProfileTypes.Select(s => s.FileProfileTypeId).ToList();
                    if (profileName != null && profileName.Contains(value.MainProfileName))
                    {
                        var parentProfile = childProfileTypes.Where(c => c.Name == value.MainProfileName).ToList();
                        if (parentProfile != null && parentProfile.Count > 0)
                        {
                            parentProfile.ForEach(p =>
                            {
                                var fileProfileType = new FileProfileType
                                {
                                    ParentId = p.FileProfileTypeId,
                                    Name = value.CreateProfileName,
                                    ProfileId = p.ProfileId,
                                    StatusCodeId = value.StatusCodeID.Value,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,

                                };
                                _context.FileProfileType.Add(fileProfileType);
                                _context.SaveChanges();

                            });
                        }
                    }
                    else

                    {
                        var secondlevelchildfolders = _context.FileProfileType.Where(f => childProfileTypeIds.Contains(f.ParentId.Value)).ToList();

                        if (secondlevelchildfolders != null && secondlevelchildfolders.Count > 0)
                        {
                            var secondlevelprofileName = secondlevelchildfolders.Select(s => s.Name).ToList();
                            if (secondlevelprofileName != null && secondlevelprofileName.Contains(value.MainProfileName))
                            {
                                var parentProfile = secondlevelchildfolders.Where(c => c.Name == value.MainProfileName).ToList();
                                if (parentProfile != null && parentProfile.Count > 0)
                                {
                                    parentProfile.ForEach(p =>
                                    {
                                        var fileProfileType = new FileProfileType
                                        {
                                            ParentId = p.FileProfileTypeId,
                                            Name = value.CreateProfileName,
                                            ProfileId = p.ProfileId,
                                            StatusCodeId = value.StatusCodeID.Value,
                                            AddedByUserId = value.AddedByUserID,
                                            AddedDate = DateTime.Now,

                                        };
                                        _context.FileProfileType.Add(fileProfileType);
                                        _context.SaveChanges();

                                    });
                                }
                            }
                        }
                    }

                }
            }
            else if (value.MassWayType.ToLower().Trim() == "rename")
            {

                if (value.FileProfileTypeId != null && value.FileProfileTypeId > 0)
                {
                    var childProfileTypes = _context.FileProfileType.Where(d => d.ParentId == value.FileProfileTypeId).ToList();
                    var profileName = childProfileTypes.Select(s => s.Name).ToList();
                    var childProfileTypeIds = childProfileTypes.Select(s => s.FileProfileTypeId).ToList();
                    if (profileName != null && profileName.Contains(value.MainProfileName))
                    {
                        var parentProfile = childProfileTypes.Where(c => c.Name == value.MainProfileName).ToList();
                        if (parentProfile != null && parentProfile.Count > 0)
                        {
                            parentProfile.ForEach(p =>
                            {
                                p.Name = value.CreateProfileName;
                                p.ModifiedByUserId = value.AddedByUserID;
                                p.ModifiedDate = DateTime.Now;
                                _context.SaveChanges();

                            });
                        }
                    }
                    else

                    {
                        var secondlevelchildfolders = _context.FileProfileType.Where(f => childProfileTypeIds.Contains(f.ParentId.Value)).ToList();

                        if (secondlevelchildfolders != null && secondlevelchildfolders.Count > 0)
                        {
                            var secondlevelprofileName = secondlevelchildfolders.Select(s => s.Name).ToList();
                            if (secondlevelprofileName != null && secondlevelprofileName.Contains(value.MainProfileName))
                            {
                                var parentProfile = secondlevelchildfolders.Where(c => c.Name == value.MainProfileName).ToList();
                                if (parentProfile != null && parentProfile.Count > 0)
                                {
                                    parentProfile.ForEach(p =>
                                    {
                                        p.Name = value.CreateProfileName;
                                        p.ModifiedByUserId = value.AddedByUserID;
                                        p.ModifiedDate = DateTime.Now;
                                        _context.SaveChanges();

                                    });
                                }
                            }
                        }
                    }

                }
            }
            return massWayFileProfileTypeModel;

        }
        [HttpGet]
        [Route("GetFileProfileTypeByDocumentList")]
        public List<DocumentsModel> GetFileProfileTypeByDocumentList(long? id)
        {
            var documents = _context.Documents.Include(a => a.AddedByUser).Include(f => f.FilterProfileType).Include(a => a.LockedByUser).Include(m => m.ModifiedByUser).Where(s => s.FilterProfileTypeId == id).
                Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    FilterProfileType = s.FilterProfileType.Name,
                    s.CloseDocumentId,
                    isDocumentAccess = s.FilterProfileType.IsDocumentAccess,
                    s.DocumentParentId,
                    s.IsMobileUpload,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUser = s.AddedByUser.UserName,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.IsCompressed,
                    s.FileIndex,
                    LockedByUser = s.LockedByUser.UserName,
                    ProfileNo = s.ProfileNo,
                    IsEnableCreateTask = s.FilterProfileType.IsEnableCreateTask,
                }).OrderByDescending(o => o.DocumentId).AsNoTracking().ToList();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                // var documentExits = documentsModel.Where(w => w.DocumentParentId == s.DocumentId && w.TableName == "Document").Count();
                var documentcount = documents?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();
                var fileName = s.FileName.Split('.');
                DocumentsModel documentsModels = new DocumentsModel
                {
                    SessionId = s.SessionId,
                    DocumentID = s.DocumentId,
                    FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FilterProfileTypeId,
                    FileProfileTypeName = s.FilterProfileType,
                    DocumentParentId = s.DocumentParentId,
                    TableName = s.TableName,
                    IsMobileUpload = s.IsMobileUpload,
                    Type = "Document",
                    ExpiryDate = s.ExpiryDate,
                    TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                    UploadedByUserId = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                    AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    AddedByUserID = s.AddedByUserId,
                    IsCompressed = s.IsCompressed,
                    LockedByUser = s.LockedByUser,
                    isDocumentAccess = s.isDocumentAccess,
                    CloseDocumentId = s.CloseDocumentId,
                    ProfileNo = s.ProfileNo,
                    IsEnableCreateTask = s.IsEnableCreateTask,

                };
                documentsModel.Add(documentsModels);
            });
            return documentsModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypeArchiveDocument")]
        public DocumentTypeModel GetFileProfileTypeArchiveDocument(long? id, long? userId)
        {
            var documents = _context.Documents.Include(a => a.AddedByUser).Include(f => f.FilterProfileType).Include(a => a.LockedByUser).Include(m => m.ModifiedByUser).Where(s => s.FilterProfileTypeId == id && s.IsLatest == true && s.ArchiveStatusId == 2562).
                Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    FilterProfileType = s.FilterProfileType.Name,
                    s.CloseDocumentId,
                    isDocumentAccess = s.FilterProfileType.IsDocumentAccess,
                    s.DocumentParentId,
                    s.IsMobileUpload,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUser = s.AddedByUser.UserName,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.IsCompressed,
                    s.FileIndex,
                    LockedByUser = s.LockedByUser.UserName,
                    ProfileNo = s.ProfileNo,
                    IsEnableCreateTask = s.FilterProfileType.IsEnableCreateTask,
                }).OrderByDescending(o => o.DocumentId).AsNoTracking().ToList();
            DocumentTypeModel DocumentTypeModel = new DocumentTypeModel();
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            documents.ForEach(s =>
            {
                // var documentExits = documentsModel.Where(w => w.DocumentParentId == s.DocumentId && w.TableName == "Document").Count();
                var documentcount = documents?.Where(w => w.DocumentParentId == s.DocumentParentId).Count();
                var fileName = s.FileName.Split('.');
                DocumentsModel documentsModels = new DocumentsModel
                {
                    SessionId = s.SessionId,
                    DocumentID = s.DocumentId,
                    FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FilterProfileTypeId,
                    FileProfileTypeName = s.FilterProfileType,
                    DocumentParentId = s.DocumentParentId,
                    TableName = s.TableName,
                    IsMobileUpload = s.IsMobileUpload,
                    Type = "Document",
                    ExpiryDate = s.ExpiryDate,
                    TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                    UploadedByUserId = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                    AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    AddedByUserID = s.AddedByUserId,
                    IsCompressed = s.IsCompressed,
                    LockedByUser = s.LockedByUser,
                    isDocumentAccess = s.isDocumentAccess,
                    CloseDocumentId = s.CloseDocumentId,
                    ProfileNo = s.ProfileNo,
                    IsEnableCreateTask = s.IsEnableCreateTask,

                };
                documentsModel.Add(documentsModels);

            });
            var blanketOrderAttachment = _context.BlanketOrderAttachment.Include(f => f.BlanketOrder).Include(f => f.BlanketOrder.AddedByUser).Include(f => f.BlanketOrder.ModifiedByUser).Include(f => f.FileProfileType).Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.BlanketOrderAttachmentId,
                    s.BlanketOrderId,
                    s.SessionId,
                    FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.BlanketOrder.AddedByUserId,
                    ModifiedByUserId = s.BlanketOrder.ModifiedByUserId,
                    ModifiedDate = s.BlanketOrder.ModifiedDate,
                    ModifiedByUser = s.BlanketOrder.ModifiedByUser.UserName,
                    AddedByUser = s.BlanketOrder.AddedByUser.UserName,
                    isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                })
            .AsNoTracking().ToList();
            if (blanketOrderAttachment != null && blanketOrderAttachment.Count > 0)
            {
                blanketOrderAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.BlanketOrderAttachmentId && w.TableName == "BlanketOrderAttachment").Count();
                    var documentcount = blanketOrderAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.BlanketOrderAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FileProfileTypeName = s.FileProfileType,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            Type = "BlanketOrderAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var contractDistributionAttachment = _context.ContractDistributionAttachment.Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Include(c => c.FileProfileType).Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.ContractDistributionAttachmentId,
                    s.SessionId,
                    FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUser = s.AddedByUser.UserName,
                    isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (contractDistributionAttachment != null && contractDistributionAttachment.Count > 0)
            {
                contractDistributionAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.ContractDistributionAttachmentId && w.TableName == "ContractDistributionAttachment").Count();
                    var documentcount = contractDistributionAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.ContractDistributionAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "ContractDistributionAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedByUserID = s.AddedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var salesAdhocNovateAttachment = _context.SalesAdhocNovateAttachment.Include(i => i.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.SalesAdhocNovateAttachmentId,
                    s.SessionId,
                    FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUser = s.AddedByUser.UserName,
                    isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (salesAdhocNovateAttachment != null && salesAdhocNovateAttachment.Count > 0)
            {
                salesAdhocNovateAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.SalesAdhocNovateAttachmentId && w.TableName == "SalesAdhocNovateAttachment").Count();
                    var documentcount = salesAdhocNovateAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.SalesAdhocNovateAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FileProfileTypeName = s.FileProfileType,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            Type = "SalesAdhocNovateAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var salesOrderAttachment = _context.SalesOrderAttachment.Include(f => f.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).ToList();
            if (salesOrderAttachment != null && salesOrderAttachment.Count > 0)
            {
                salesOrderAttachment.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.SalesOrderAttachmentId && w.TableName == "SalesOrderAttachment").Count();
                    if (documentExits == 0)
                    {
                        var documentcount = salesOrderAttachment?.Where(w => w.SessionId == s.SessionId).Count();
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.SalesOrderAttachmentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType?.Name,
                            Type = "SalesOrderAttachment",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser?.UserName == null ? s.AddedByUser?.UserName : s.ModifiedByUser?.UserName,
                            isDocumentAccess = s.FileProfileType?.IsDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var commonFieldsProductionMachineDocument = _context.CommonFieldsProductionMachineDocument.Include(i => i.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.MachineDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (commonFieldsProductionMachineDocument != null && commonFieldsProductionMachineDocument.Count > 0)
            {
                commonFieldsProductionMachineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.MachineDocumentId && w.TableName == "CommonFieldsProductionMachineDocument").Count();
                    var documentcount = commonFieldsProductionMachineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.MachineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "CommonFieldsProductionMachineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var transferBalanceQtyDocument = _context.TransferBalanceQtyDocument.Include(d => d.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.TransferBalanceQtyDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (transferBalanceQtyDocument != null && transferBalanceQtyDocument.Count > 0)
            {
                transferBalanceQtyDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.TransferBalanceQtyDocumentId && w.TableName == "TransferBalanceQtyDocument").Count();
                    var documentcount = transferBalanceQtyDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.TransferBalanceQtyDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "TransferBalanceQtyDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var quotationHistoryDocument = _context.QuotationHistoryDocument.Include(p => p.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).
                Select(s => new
                {
                    s.QuotationHistoryDocumentId,
                    s.SessionId,
                    FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    AddedByUser = s.AddedByUser.UserName,
                    isDocumentAccess = s.FileProfileType.IsDocumentAccess,
                }).AsNoTracking().ToList();
            if (quotationHistoryDocument != null && quotationHistoryDocument.Count > 0)
            {
                quotationHistoryDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.QuotationHistoryDocumentId && w.TableName == "QuotationHistoryDocument").Count();
                    var documentcount = quotationHistoryDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.QuotationHistoryDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "QuotationHistoryDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var purchaseOrderDocument = _context.PurchaseOrderDocument.Include(p => p.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.PurchaseOrderDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (purchaseOrderDocument != null && purchaseOrderDocument.Count > 0)
            {
                purchaseOrderDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.PurchaseOrderDocumentId && w.TableName == "PurchaseOrderDocument").Count();
                    var documentcount = purchaseOrderDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.PurchaseOrderDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "PurchaseOrderDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var productRecipeDocument = _context.ProductRecipeDocument.Include(n => n.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.ProductRecipeDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (productRecipeDocument != null && productRecipeDocument.Count > 0)
            {
                productRecipeDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.ProductRecipeDocumentId && w.TableName == "ProductRecipeDocument").Count();
                    var documentcount = productRecipeDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.ProductRecipeDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "ProductRecipeDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument.Include(f => f.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.PackagingHistoryItemLineDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (packagingHistoryItemLineDocument != null && packagingHistoryItemLineDocument.Count > 0)
            {
                packagingHistoryItemLineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.PackagingHistoryItemLineDocumentId && w.TableName == "PackagingHistoryItemLineDocument").Count();
                    var documentcount = packagingHistoryItemLineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.PackagingHistoryItemLineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "PackagingHistoryItemLineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,
                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }
            var ipirlineDocument = _context.IpirlineDocument.Include(f => f.FileProfileType).Include(s => s.AddedByUser).Include(s => s.ModifiedByUser).Where(w => w.FileProfileTypeId == id).Select(s => new
            {
                s.IpirlineDocumentId,
                s.SessionId,
                FileProfileType = s.FileProfileType.Name,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FileProfileTypeId,
                AddedByUserId = s.AddedByUserId,
                ModifiedByUserId = s.ModifiedByUserId,
                ModifiedDate = s.ModifiedDate,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedByUser = s.AddedByUser.UserName,
                isDocumentAccess = s.FileProfileType.IsDocumentAccess,
            }).AsNoTracking().ToList();
            if (ipirlineDocument != null && ipirlineDocument.Count > 0)
            {
                ipirlineDocument.ForEach(s =>
                {
                    var documentExits = documentsModel.Where(w => w.DocumentParentId == s.IpirlineDocumentId && w.TableName == "IpirlineDocument").Count();
                    var documentcount = ipirlineDocument?.Where(w => w.SessionId == s.SessionId).Count();
                    if (documentExits == 0)
                    {
                        DocumentsModel documentsModels = new DocumentsModel
                        {
                            DocumentID = s.IpirlineDocumentId,
                            FileName = s.FileName,
                            ContentType = s.ContentType,
                            FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                            UploadDate = s.UploadDate,
                            SessionID = s.SessionId,
                            FilterProfileTypeId = s.FileProfileTypeId,
                            FileProfileTypeName = s.FileProfileType,
                            Type = "IpirlineDocument",
                            TotalDocument = documentcount == 1 ? 1 : (documentcount + 1),
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                            AddedByUser = s.ModifiedByUser == null ? s.AddedByUser : s.ModifiedByUser,
                            isDocumentAccess = s.isDocumentAccess,

                        };
                        documentsModel.Add(documentsModels);
                    }
                });
            }


            DocumentTypeModel.DocumentsData = documentsModel.OrderByDescending(a => a.DocumentID).ToList();
            var roleItems = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id).ToList();
            if (roleItems.Count > 0)
            {
                var roleItem = roleItems.FirstOrDefault(u => u.UserId == userId);
                if (roleItem != null)
                {
                    DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                    var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    DocumentTypeModel.DocumentPermissionData = permissionData;
                }
                else
                {
                    DocumentTypeModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = false, IsRename = false };
                }
            }
            else
            {
                DocumentTypeModel.DocumentPermissionData = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsRename = false };
            }
            return DocumentTypeModel;
            //return documentsModel.OrderByDescending(a => a.DocumentID).ToList();
        }
        [HttpPost]
        [Route("GetFileProfileTypeMoveDocument")]
        public SearchModel GetFileProfileTypeMoveDocument(SearchModel SearchModel)
        {
            if (SearchModel.Ids.Count > 0)
            {
                SearchModel.Ids.ForEach(s =>
                {
                    //var docid = _context.Documents.Where(w => w.DocumentId == s).Select(s => s.DocumentId).FirstOrDefault();
                    var sessionId = _context.Documents.Where(w => w.DocumentId == s).Select(s => s.SessionId).FirstOrDefault();
                    var documents = _context.Documents.Select(d => new { d.DocumentId, d.SessionId, d.IsLatest }).Where(w => (w.SessionId == sessionId && w.IsLatest == false) || (w.DocumentId == s)).ToList();
                    var query = string.Format("Update Documents Set FilterProfileTypeId = {1} Where DocumentId in" + '(' + "{0}" + ')', string.Join(',', documents.Select(s => s.DocumentId)), SearchModel.ParentID);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to update document");
                    }
                    //documents.ForEach(d =>
                    //{
                    //    d.FilterProfileTypeId = SearchModel.ParentID;
                    //});

                    var linkfileprofiledocument = _context.LinkFileProfileTypeDocument.Where(d => d.TransactionSessionId == sessionId && d.DocumentId == s).FirstOrDefault();
                    if (linkfileprofiledocument != null)
                    {
                        linkfileprofiledocument.FileProfileTypeId = SearchModel.ParentID;
                    }
                    _context.SaveChanges();
                });

            }
            return SearchModel;
        }
        [HttpPost]
        [Route("UpdatecloseDocuments")]
        public DocumentsModel UpdatecloseDocuments(DocumentsModel DocumentsModel)
        {
            if (DocumentsModel.Type == "Document")
            {
                var query = string.Format("Update Documents Set CloseDocumentId = 2561 Where DocumentId = {0}", DocumentsModel.DocumentID);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to close document");
                }
                //var document = _context.Documents.SingleOrDefault(w => w.DocumentId == DocumentsModel.DocumentID);
                //document.CloseDocumentId = 2561;
                //_context.SaveChanges();
            }
            return DocumentsModel;
        }
        [HttpGet]
        [Route("GetFileProfileTypePermission")]
        public DocumentPermissionModel GetFileProfileTypePermission(long? id, long? userId)
        {
            DocumentPermissionModel documentPermissionModel = new DocumentPermissionModel();
            var roleItems = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == id).ToList();
            var FileProfileTypeItmes = _context.FileProfileType.Where(w => w.FileProfileTypeId == id && w.AddedByUserId == userId).FirstOrDefault();
            if (FileProfileTypeItmes == null)
            {
                if (roleItems.Count > 0)
                {
                    var roleItem = roleItems.FirstOrDefault(u => u.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        documentPermissionModel = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = false, IsMove = false };
                    }
                }
                else
                {
                    documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true, IsMove = false };
                }
                //}
                //else
                //{
                //    documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true };
                //}
            }
            else
            {
                documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true };
            }
            if (documentPermissionModel != null)
            {
                documentPermissionModel.FolderName = FileProfileTypeItmes?.Name;
            }
            return documentPermissionModel;
        }
        [HttpPost]
        [Route("UpdateReleaseForWiki")]
        public SearchModel UpdateReleaseForWiki(SearchModel value)
        {
            var appWikiReleaseDoc = _context.AppWikiReleaseDoc.ToList();
            var documents = GetFileProfileTypeDocumentByHistory(value).DocumentsData.ToList();
            if (documents != null && documents.Count > 0)
            {
                documents.ForEach(s =>
                {
                    var appWikiReleaseDocs = appWikiReleaseDoc.Where(d => d.DocumentId == s.DocumentID).ToList();
                    if (appWikiReleaseDocs.Count > 0)
                    {
                        var query = string.Format("Update AppWikiReleaseDoc Set DocumentId='{0}' Where DocumentId= {1}", value.Id, s.DocumentID);
                        _context.Database.ExecuteSqlRaw(query);
                    }
                });
            }
            return value;
        }
        [HttpPost]
        [Route("UpdateAppReleaseForWiki")]
        public SearchModel UpdateAppReleaseForWiki(SearchModel value)
        {
            var query = string.Format("Update AppWikiReleaseDoc Set DocumentId='{1}' Where AppWikiReleaseDocId= {0}", value.Id, value.ParentID);
            _context.Database.ExecuteSqlRaw(query);
            return value;
        }
        [HttpGet]
        [Route("ReleaseForWikiDashboard")]
        public List<DocumentsModel> ReleaseForWikiDashboard()
        {
            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var appWikiReleaseDoc = _context.AppWikiReleaseDoc.Include(a => a.ApplicationWiki).Include(d => d.Document).Where(w => w.ApplicationWiki.StatusCodeId == 841 && w.Document.FolderId == null).Select(s => new { s.DocumentId, s.ApplicationWiki.Title, s.Document.SessionId, s.Document.FileName, s.AppWikiReleaseDocId }).ToList();
            if (appWikiReleaseDoc != null && appWikiReleaseDoc.Count > 0)
            {
                var sessionids = appWikiReleaseDoc.Select(s => s.SessionId).Distinct().ToList();
                var documentsdata = _context.Documents.Include(a => a.FilterProfileType).Where(w => sessionids.Contains(w.SessionId) && w.IsLatest == true && w.FolderId == null).Select(s => new { s.DocumentId, s.SessionId, s.FileName, s.ContentType, s.FileSize, s.AddedByUserId, s.UploadDate, s.DocumentParentId, s.FileIndex, s.FilterProfileTypeId, FilterProfileType = s.FilterProfileType.Name }).ToList();
                appWikiReleaseDoc.ForEach(s =>
                {
                    var documentdata = documentsdata.FirstOrDefault(f => f.DocumentParentId == s.DocumentId);
                    if (documentdata != null && documentdata.DocumentId != s.DocumentId)
                    {
                        DocumentsModel documentsModel = new DocumentsModel();
                        var name = documentdata.FileName != null ? documentdata.FileName?.Substring(documentdata.FileName.LastIndexOf(".")) : "";
                        var fileName = s.FileName?.Split(name);
                        documentsModel.WikiTitle = s.Title;
                        documentsModel.DocumentID = s.DocumentId.Value;
                        documentsModel.SessionID = s.SessionId;
                        documentsModel.SessionId = s.SessionId;
                        documentsModel.AppWikiReleaseDocId = s.AppWikiReleaseDocId;
                        documentsModel.AddedByUserID = documentdata?.AddedByUserId;
                        documentsModel.UploadDate = documentdata?.UploadDate;
                        documentsModel.FileName = s.FileName;
                        documentsModel.DocumentParentId = documentdata?.DocumentId;
                        documentsModel.FilterProfileTypeId = documentdata?.FilterProfileTypeId;
                        documentsModel.FileProfileTypeName = documentdata?.FilterProfileType;
                        documentsModel.ContentType = documentdata?.ContentType;
                        documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(documentdata.FileSize / 1024));
                        documentsModel.DocumentName = documentdata.FileIndex > 0 ? fileName[0] + "_V0" + documentdata.FileIndex + name : documentdata.FileName;
                        documentsModels.Add(documentsModel);
                    }
                });
            }
            return documentsModels;
        }
        [HttpPost]
        [Route("GetFileProfileTypeDocumentByHistory")]
        public DocumentTypeModel GetFileProfileTypeDocumentByHistory(SearchModel searchModel)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();

            var fileProfileType = _context.FileProfileType.Select(s => new
            {
                s.FileProfileTypeId,
                s.Name,
                s.IsDocumentAccess
            }).AsNoTracking().ToList();
            List<long?> documentFolderDocIds = new List<long?>();
            if (searchModel.Id != null)
            {
                var document = _context.Documents.FirstOrDefault(s => s.DocumentId == searchModel.Id && s.IsPublichFolder == true);
                if (document != null)
                {
                    var previousId = _context.DocumentFolder.FirstOrDefault(d => d.DocumentId == searchModel.Id && d.FolderId == document.FolderId && d.IsLatest == true)?.DocumentFolderId;
                    if (previousId > 0)
                    {
                        documentFolderDocIds = _context.DocumentFolder.Where(d => d.PreviousDocumentId == previousId && d.FolderId == document.FolderId && d.IsLatest == false).Select(s => s.DocumentId).ToList();
                    }
                }
            }
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            if (searchModel.SearchString == "Document")
            {
                var documetsparent = _context.Documents
                    .Include(s => s.StatusCode)
                    .Where(s => s.SessionId == searchModel.SessionID.Value && s.FilterProfileTypeId == searchModel.MasterTypeID && s.DocumentId == searchModel.ParentID && s.IsLatest == false).Select(s => new
                    {
                        s.SessionId,
                        s.DocumentId,
                        s.FileName,
                        s.ContentType,
                        s.FileSize,
                        s.UploadDate,
                        s.FilterProfileTypeId,
                        // FilterProfileType = s.FilterProfileType.Name,
                        s.DocumentParentId,
                        s.TableName,
                        s.ExpiryDate,
                        s.AddedByUserId,
                        s.ModifiedByUserId,
                        s.ModifiedDate,
                        //ModifiedByUser = s.ModifiedByUser.UserName,
                        //AddedByUser = s.AddedByUser.UserName,
                        IsLocked = s.IsLocked,
                        LockedByUserId = s.LockedByUserId,
                        LockedDate = s.LockedDate,
                        s.AddedDate,
                        s.IsCompressed,
                        s.FileIndex,
                        s.ProfileNo,
                        s.Description,
                        s.FilePath,
                    }).AsNoTracking().FirstOrDefault();


                if (documetsparent != null)
                {


                    var fileName = documetsparent.FileName.Split('.');
                    DocumentsModel documentsModelss = new DocumentsModel
                    {
                        DocumentID = documetsparent.DocumentId,
                        FileName = documetsparent.FileIndex > 0 ? fileName[0] + "_V0" + documetsparent.FileIndex + "." + fileName[1] : documetsparent.FileName,
                        ContentType = documetsparent.ContentType,
                        FileSize = (long)Math.Round(Convert.ToDouble(documetsparent.FileSize / 1024)),
                        UploadDate = documetsparent.UploadDate,
                        SessionID = documetsparent.SessionId,
                        FilterProfileTypeId = documetsparent.FilterProfileTypeId,
                        FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == documetsparent.FilterProfileTypeId)?.Name,
                        DocumentParentId = documetsparent.DocumentParentId,
                        TableName = documetsparent.TableName,
                        Type = "Document",
                        AddedDate = documetsparent.AddedDate,
                        AddedByUser = appUsers.FirstOrDefault(f => f.UserId == documetsparent.AddedByUserId)?.UserName,
                        IsCompressed = documetsparent.IsCompressed,
                        FileIndex = documetsparent.FileIndex,
                        ProfileNo = documetsparent.ProfileNo,
                        Description = documetsparent.Description,
                        FilePath = documetsparent.FilePath,
                    };
                    documentsModel.Add(documentsModelss);
                }

                var documents = _context.Documents

                       .Include(s => s.StatusCode)
                      .Select(s => new
                      {
                          s.SessionId,
                          s.DocumentId,
                          s.FileName,
                          s.ContentType,
                          s.FileSize,
                          s.UploadDate,
                          s.FilterProfileTypeId,
                          // FilterProfileType = s.FilterProfileType.Name,
                          s.DocumentParentId,
                          s.TableName,
                          s.ExpiryDate,
                          s.AddedByUserId,
                          s.ModifiedByUserId,
                          s.ModifiedDate,
                          s.AddedDate,
                          //ModifiedByUser = s.ModifiedByUser.UserName,
                          //AddedByUser = s.AddedByUser.UserName,
                          IsLocked = s.IsLocked,
                          LockedByUserId = s.LockedByUserId,
                          LockedDate = s.LockedDate,
                          s.IsCompressed,
                          s.FileIndex,
                          s.ProfileNo,
                          s.IsLatest,
                          s.Description,
                          s.FilePath
                      }).AsNoTracking().ToList();
                var documentlist = documents?.Where(s => s.SessionId == searchModel.SessionID.Value && s.FilterProfileTypeId == searchModel.MasterTypeID && s.DocumentParentId == searchModel.ParentID && s.IsLatest == false).ToList();
                if (searchModel.ParentID != null)
                {
                    if (documentlist.Count > 0)
                    {
                        documentlist.ForEach(s =>
                        {
                            var fileName = s.FileName.Split('.');
                            DocumentsModel documentsModels = new DocumentsModel
                            {
                                DocumentID = s.DocumentId,
                                FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName,
                                ContentType = s.ContentType,
                                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                                UploadDate = s.UploadDate,
                                SessionID = s.SessionId,
                                FilterProfileTypeId = s.FilterProfileTypeId,
                                FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FilterProfileTypeId)?.Name,
                                DocumentParentId = s.DocumentParentId,
                                TableName = s.TableName,
                                Type = "Document",
                                AddedDate = s.AddedDate,
                                AddedByUser = appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName,
                                IsCompressed = s.IsCompressed,
                                ProfileNo = s.ProfileNo,
                                Description = s.Description,
                                FilePath = s.FilePath
                            };
                            documentsModel.Add(documentsModels);
                        });
                    }
                }
                if (documentFolderDocIds != null && documentFolderDocIds.Count > 0)
                {
                    var folderdocumentlist = documents?.Where(s => documentFolderDocIds.Contains(s.DocumentId)).ToList();
                    if (folderdocumentlist != null && folderdocumentlist.Count > 0)
                    {
                        folderdocumentlist.ForEach(s =>
                        {
                            var fileName = s.FileName.Split('.');
                            DocumentsModel documentsModels = new DocumentsModel
                            {
                                DocumentID = s.DocumentId,
                                FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName,
                                ContentType = s.ContentType,
                                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                                UploadDate = s.UploadDate,
                                SessionID = s.SessionId,
                                FilterProfileTypeId = s.FilterProfileTypeId,
                                FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FilterProfileTypeId)?.Name,
                                DocumentParentId = s.DocumentParentId,
                                TableName = s.TableName,
                                Type = "Document",
                                AddedDate = s.AddedDate,
                                AddedByUser = appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName,
                                IsCompressed = s.IsCompressed,
                                ProfileNo = s.ProfileNo,
                                Description = s.Description,
                                FilePath = s.FilePath
                            };
                            documentsModel.Add(documentsModels);
                        });
                    }
                }

            }
            //if (searchModel.SearchString == "BlanketOrderAttachment")
            // {
            var blanketOrderAttachment = _context.BlanketOrderAttachment
                .Include(f => f.FileProfileType)
                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.BlanketOrderAttachmentId != searchModel.Id).Select(s => new
                {
                    s.BlanketOrderAttachmentId,
                    s.SessionId,
                    FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId
                }).AsNoTracking().ToList();
            blanketOrderAttachment.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.BlanketOrderAttachmentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FileProfileTypeName = s.FileProfileType,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    Type = "BlanketOrderAttachment",
                    AddedDate = s.UploadDate,
                };
                documentsModel.Add(documentsModels);
            });
            //}
            // if (searchModel.SearchString == "ContractDistributionAttachment")
            // {
            var contractDistributionAttachment = _context.ContractDistributionAttachment

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.ContractDistributionAttachmentId != searchModel.Id).
                Select(s => new
                {
                    s.ContractDistributionAttachmentId,
                    s.SessionId,
                    //FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    s.AddedDate,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName
                }).AsNoTracking().ToList();
            contractDistributionAttachment.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.ContractDistributionAttachmentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "ContractDistributionAttachment",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            // }
            //if (searchModel.SearchString == "SalesAdhocNovateAttachment")
            //{
            var salesAdhocNovateAttachment = _context.SalesAdhocNovateAttachment

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.SalesAdhocNovateAttachmentId != searchModel.Id).
                Select(s => new
                {
                    s.SalesAdhocNovateAttachmentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            salesAdhocNovateAttachment.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.SalesAdhocNovateAttachmentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    Type = "SalesAdhocNovateAttachment",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /* }
             if (searchModel.SearchString == "SalesOrderAttachment")
             {*/
            var salesOrderAttachment = _context.SalesOrderAttachment

.Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.SalesOrderAttachmentId != searchModel.Id).
Select(s => new
{
    s.SalesOrderAttachmentId,
    s.SessionId,
    //FileProfileType = s.FileProfileType.Name,
    s.FileName,
    s.ContentType,
    s.FileSize,
    s.UploadDate,
    s.FileProfileTypeId,
    AddedByUserId = s.AddedByUserId,
    ModifiedByUserId = s.ModifiedByUserId,
    ModifiedDate = s.ModifiedDate,
    //ModifiedByUser = s.ModifiedByUser.UserName,
    // AddedByUser = s.AddedByUser.UserName,
    s.AddedDate,
}).AsNoTracking().ToList();
            salesOrderAttachment.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.SalesOrderAttachmentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "SalesOrderAttachment",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);

            });
            /* }
             if (searchModel.SearchString == "CommonFieldsProductionMachineDocument")
             {*/
            var commonFieldsProductionMachineDocument = _context.CommonFieldsProductionMachineDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.MachineDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.MachineDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            commonFieldsProductionMachineDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.MachineDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "CommonFieldsProductionMachineDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);

            });
            /*}
            if (searchModel.SearchString == "TransferBalanceQtyDocument")
            {*/
            var transferBalanceQtyDocument = _context.TransferBalanceQtyDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.TransferBalanceQtyDocumentId != searchModel.Id).Select(s => new
                {
                    s.TransferBalanceQtyDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    // AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            transferBalanceQtyDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.TransferBalanceQtyDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "TransferBalanceQtyDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /*}
            if (searchModel.SearchString == "QuotationHistoryDocument")
            {*/
            var quotationHistoryDocument = _context.QuotationHistoryDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.QuotationHistoryDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.QuotationHistoryDocumentId,
                    s.SessionId,
                    //FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate,
                }).AsNoTracking().ToList();
            quotationHistoryDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.QuotationHistoryDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "QuotationHistoryDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /*}
            if (searchModel.SearchString == "PurchaseOrderDocument")
            {*/
            var purchaseOrderDocument = _context.PurchaseOrderDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.PurchaseOrderDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.PurchaseOrderDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            purchaseOrderDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.PurchaseOrderDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "PurchaseOrderDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /* }
             if (searchModel.SearchString == "ProductRecipeDocument")
             {*/
            var productRecipeDocument = _context.ProductRecipeDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.ProductRecipeDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.ProductRecipeDocumentId,
                    s.SessionId,
                    //FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    // AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            productRecipeDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.ProductRecipeDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "ProductRecipeDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /*}
            if (searchModel.SearchString == "PackagingHistoryItemLineDocument")
            {*/
            var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument

                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.PackagingHistoryItemLineDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.PackagingHistoryItemLineDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    // AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            packagingHistoryItemLineDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.PackagingHistoryItemLineDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "PackagingHistoryItemLineDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            /*}
            if (searchModel.SearchString == "IpirlineDocument")
            {*/
            var ipirlineDocument = _context.IpirlineDocument
                .Where(s => s.SessionId == searchModel.SessionID.Value && s.FileProfileTypeId == searchModel.MasterTypeID && s.IpirlineDocumentId != searchModel.Id).
                Select(s => new
                {
                    s.IpirlineDocumentId,
                    s.SessionId,
                    // FileProfileType = s.FileProfileType.Name,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FileProfileTypeId,
                    AddedByUserId = s.AddedByUserId,
                    ModifiedByUserId = s.ModifiedByUserId,
                    ModifiedDate = s.ModifiedDate,
                    // ModifiedByUser = s.ModifiedByUser.UserName,
                    // AddedByUser = s.AddedByUser.UserName,
                    s.AddedDate
                }).AsNoTracking().ToList();
            ipirlineDocument.ForEach(s =>
            {
                DocumentsModel documentsModels = new DocumentsModel
                {
                    DocumentID = s.IpirlineDocumentId,
                    FileName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.Name,
                    Type = "IpirlineDocument",
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUserId != null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : null,
                };
                documentsModel.Add(documentsModels);
            });
            // }

            DocumentTypeModel documentTypeModel = new DocumentTypeModel();
            documentTypeModel.DocumentsData = documentsModel.OrderByDescending(a => a.DocumentID).ToList();
            var roleid = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == searchModel.MasterTypeID).Select(s => s.RoleId).FirstOrDefault();
            if (roleid != null)
            {
                DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                var permissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleid);
                documentTypeModel.DocumentPermissionData = permissionData;
            }
            else
            {
                documentTypeModel.DocumentPermissionData = new DocumentPermissionModel { IsUpdateDocument = true, IsRead = true, IsShare = true, IsEdit = true, IsMove = true };
            }
            return documentTypeModel;
        }

        [HttpGet]
        [Route("GetlatestDocumentById")]
        public DocumentTypeModel GetlatestDocumentById(long? id)
        {
            DocumentTypeModel documentTypeModel = new DocumentTypeModel();
            var document = _context.Documents.FirstOrDefault(d => d.DocumentId == id && d.IsLatest == true);
            if (document != null)
            {
                documentTypeModel.DocumentID = id;
            }
            else
            {
                var selectdocument = _context.Documents.FirstOrDefault(d => d.DocumentId == id && d.IsLatest == false);
                if (selectdocument != null)
                {
                    var latestdocument = new Documents();
                    if (selectdocument.DocumentParentId != null)
                    {
                        latestdocument = _context.Documents.FirstOrDefault(d => d.DocumentParentId == selectdocument.DocumentParentId && d.IsLatest == true);
                    }
                    else
                    {
                        latestdocument = _context.Documents.FirstOrDefault(d => d.DocumentParentId == selectdocument.DocumentId && d.IsLatest == true);
                    }
                    if (latestdocument != null)
                    {
                        documentTypeModel = new DocumentTypeModel();
                        documentTypeModel.DocumentID = latestdocument.DocumentId;

                    }
                }
            }
            return documentTypeModel;
        }
        [HttpGet]
        [Route("GetDocumentInfo")]
        public DocumentTypeModel GetDocumentInfo(long? id, long? userId)
        {

            DocumentTypeModel documentTypeModel = new DocumentTypeModel();
            var s = _context.Documents

                .Include(s => s.StatusCode).Where(s => s.DocumentId == id).Select(s => new
                {
                    s.SessionId,
                    s.DocumentId,
                    s.FileName,
                    s.ContentType,
                    s.FileSize,
                    s.UploadDate,
                    s.FilterProfileTypeId,
                    // FilterProfileType = s.FilterProfileType.Name,
                    s.DocumentParentId,
                    s.TableName,
                    s.ExpiryDate,
                    s.AddedByUserId,
                    s.ModifiedByUserId,
                    s.ModifiedDate,
                    //ModifiedByUser = s.ModifiedByUser.UserName,
                    //AddedByUser = s.AddedByUser.UserName,
                    IsLocked = s.IsLocked,
                    LockedByUserId = s.LockedByUserId,
                    LockedDate = s.LockedDate,
                    s.AddedDate,
                    s.IsCompressed,
                    s.FileIndex,
                    s.IsMobileUpload,
                    s.IsVideoFile,
                    s.FilePath,
                }).AsNoTracking().FirstOrDefault();
            var fileName = s.FileName.Split('.');
            var userIds = new List<long?> { s.AddedByUserId, s.ModifiedByUserId };
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().Where(f => userIds.Contains(f.UserId)).ToList();

            var fileProfileType = _context.FileProfileType
            .AsNoTracking().FirstOrDefault(f => f.FileProfileTypeId == s.FilterProfileTypeId);
            //var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();


            var result = new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.DocumentId,
                FileName = s.FileIndex > 0 ? fileName[0] + "_V0" + s.FileIndex + "." + fileName[1] : s.FileName,
                ContentType = s.ContentType,
                FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                UploadDate = s.UploadDate,
                SessionID = s.SessionId,
                FilterProfileTypeId = s.FilterProfileTypeId,
                FileProfileTypeName = fileProfileType != null ? fileProfileType.Name : "",
                FileProfileTypeParentId = fileProfileType != null ? fileProfileType.ParentId != null ? fileProfileType.ParentId : null : null,
                DocumentParentId = s.DocumentParentId,
                TableName = s.TableName,
                IsMobileUpload = s.IsMobileUpload,
                Type = "Document",
                ExpiryDate = s.ExpiryDate,
                UploadedByUserId = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedDate = s.ModifiedDate == null ? s.UploadDate : s.ModifiedDate,
                AddedByUser = s.ModifiedByUserId == null ? appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName : appUsers.FirstOrDefault(f => f.UserId == s.ModifiedByUserId)?.UserName,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                AddedByUserID = s.AddedByUserId,
                IsCompressed = s.IsCompressed,
                IsVideoFile = s.IsVideoFile,
                FilePath = s.FilePath,

            };
            if (result != null)
            {
                if (result.FilterProfileTypeId != null)
                {
                    if (fileProfileType != null)
                    {
                        var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
                        List<string> FolderPathLists = new List<string>();
                        FolderPathLists = GetAllFileProfileTypeName(fileProfileTypeItems, fileProfileType, FolderPathLists);
                        FolderPathLists.Add(fileProfileType.Name);
                        result.DocumentPath = string.Join(" / ", FolderPathLists);
                    }
                }
            }
            documentTypeModel.DocumentsData.Add(result);

            DocumentPermissionModel documentPermissionModel = new DocumentPermissionModel();
            var roleItems = _context.DocumentUserRole.Where(w => w.FileProfileTypeId == result.FilterProfileTypeId).ToList();
            var FileProfileTypeItmes = _context.FileProfileType.Where(w => w.FileProfileTypeId == id && w.AddedByUserId == userId).FirstOrDefault();
            if (FileProfileTypeItmes == null)
            {
                if (roleItems.Count > 0)
                {
                    var roleItem = roleItems.FirstOrDefault(u => u.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        documentPermissionModel = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = false, IsDelete = false, IsUpdateDocument = false, IsRead = false };
                    }
                }
                else
                {
                    documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true };
                }
            }
            else
            {
                documentPermissionModel = new DocumentPermissionModel { IsCreateDocument = true, IsDelete = true, IsUpdateDocument = true, IsRead = true };
            }

            documentTypeModel.DocumentPermissionData = documentPermissionModel;

            return documentTypeModel;
        }



        [HttpPost]
        [Route("DeleteDocument")]
        public void DeleteDocument(SearchModel searchModel)
        {
            var id = searchModel.Id;
            var type = searchModel.SearchString;
            switch (type)
            {
                case "Document":
                    try
                    {
                        var documents = _context.Documents.Select(s => new { s.DocumentId, s.DocumentParentId, s.FileName, s.FilterProfileTypeId, s.IsVideoFile }).FirstOrDefault(f => f.DocumentId == id);
                        var linkFiledocument = _context.LinkFileProfileTypeDocument.SingleOrDefault(p => p.DocumentId == id && p.FileProfileTypeId != null && p.FileProfileTypeId == searchModel.FileProfileTypeId);
                        if (linkFiledocument != null)
                        {
                            _context.LinkFileProfileTypeDocument.Remove(linkFiledocument);
                            _context.SaveChanges();
                        }
                        var fileProfileTypeDynamicForm = _context.FileProfileTypeDynamicForm.SingleOrDefault(p => p.DocumentId == id && p.FileProfileTypeId != null && p.FileProfileTypeId == searchModel.FileProfileTypeId);
                        if (fileProfileTypeDynamicForm != null)
                        {
                            _context.FileProfileTypeDynamicForm.Remove(fileProfileTypeDynamicForm);
                            _context.SaveChanges();
                        }
                        if (documents != null && documents.DocumentParentId == null)
                        {
                            var isVideoFile = documents.IsVideoFile.GetValueOrDefault(false);
                            if (isVideoFile)
                            {
                                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + documents.FileName;
                                System.IO.File.Delete(serverPath);
                            }
                            var query = string.Format("Delete from Documents  Where  DocumentId='{0}' ", documents.DocumentId);
                            _context.Database.ExecuteSqlRaw(query);
                        }
                        if (documents != null && documents.DocumentParentId != null)
                        {
                            if (linkFiledocument == null)
                            {
                                var document = _context.Documents.Where(p => p.SessionId == searchModel.SessionID).ToList();
                                if (document != null)
                                {
                                    document.ForEach(a =>
                                    {
                                        var isVideoFile = a.IsVideoFile.GetValueOrDefault(false);
                                        _context.Documents.Remove(a);
                                        if (isVideoFile)
                                        {
                                            var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + a.FileName;
                                            System.IO.File.Delete(serverPath);
                                        }
                                        _context.SaveChanges();
                                    });
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new AppException("File in use.,You’re not allowed to delete the file", ex);
                    }
                    break;
                case "BlanketOrderAttachment":
                    try
                    {
                        var blanketOrderAttachment = _context.BlanketOrderAttachment.SingleOrDefault(p => p.BlanketOrderAttachmentId == id);
                        if (blanketOrderAttachment != null)
                        {
                            _context.BlanketOrderAttachment.Remove(blanketOrderAttachment);
                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new AppException("File in use.,You’re not allowed to delete the file", ex);
                    }
                    break;
                case "ContractDistributionAttachment":
                    try
                    {
                        var contractDistributionAttachment = _context.ContractDistributionAttachment.SingleOrDefault(p => p.ContractDistributionAttachmentId == id);
                        if (contractDistributionAttachment != null)
                        {
                            _context.ContractDistributionAttachment.Remove(contractDistributionAttachment);
                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new AppException("File in use.,You’re not allowed to delete the file", ex);
                    }
                    break;
                case "SalesAdhocNovateAttachment":
                    var salesAdhocNovateAttachment = _context.SalesAdhocNovateAttachment.SingleOrDefault(p => p.SalesAdhocNovateAttachmentId == id);
                    if (salesAdhocNovateAttachment != null)
                    {
                        _context.SalesAdhocNovateAttachment.Remove(salesAdhocNovateAttachment);
                        _context.SaveChanges();
                    }
                    break;
                case "SalesOrderAttachment":
                    var salesOrderAttachment = _context.SalesOrderAttachment.SingleOrDefault(p => p.SalesOrderAttachmentId == id);
                    if (salesOrderAttachment != null)
                    {
                        _context.SalesOrderAttachment.Remove(salesOrderAttachment);
                        _context.SaveChanges();
                    }
                    break;
                case "SalesOrderAtCommonFieldsProductionMachineDocumenttachment":
                    var commonFieldsProductionMachineDocument = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == id);
                    if (commonFieldsProductionMachineDocument != null)
                    {
                        _context.CommonFieldsProductionMachineDocument.Remove(commonFieldsProductionMachineDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "TransferBalanceQtyDocument":
                    var transferBalanceQtyDocument = _context.TransferBalanceQtyDocument.SingleOrDefault(p => p.TransferBalanceQtyDocumentId == id);
                    if (transferBalanceQtyDocument != null)
                    {
                        _context.TransferBalanceQtyDocument.Remove(transferBalanceQtyDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "QuotationHistoryDocument":
                    var quotationHistoryDocument = _context.QuotationHistoryDocument.SingleOrDefault(p => p.QuotationHistoryDocumentId == id);
                    if (quotationHistoryDocument != null)
                    {
                        _context.QuotationHistoryDocument.Remove(quotationHistoryDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "PurchaseOrderDocument":
                    var purchaseOrderDocument = _context.PurchaseOrderDocument.SingleOrDefault(p => p.PurchaseOrderDocumentId == id);
                    if (purchaseOrderDocument != null)
                    {
                        _context.PurchaseOrderDocument.Remove(purchaseOrderDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "ProductRecipeDocument":
                    var productRecipeDocument = _context.ProductRecipeDocument.SingleOrDefault(p => p.ProductRecipeDocumentId == id);
                    if (productRecipeDocument != null)
                    {
                        _context.ProductRecipeDocument.Remove(productRecipeDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "PackagingHistoryItemLineDocument":
                    var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument.SingleOrDefault(p => p.PackagingHistoryItemLineDocumentId == id);
                    if (packagingHistoryItemLineDocument != null)
                    {
                        _context.PackagingHistoryItemLineDocument.Remove(packagingHistoryItemLineDocument);
                        _context.SaveChanges();
                    }
                    break;
                case "IpirlineDocument":
                    var ipirlineDocument = _context.IpirlineDocument.SingleOrDefault(p => p.IpirlineDocumentId == id);
                    if (ipirlineDocument != null)
                    {
                        _context.IpirlineDocument.Remove(ipirlineDocument);
                        _context.SaveChanges();
                    }
                    break;
            };

        }

        [HttpPost]
        [Route("DeletePermissionDocument")]
        public void DeletePermissionDocument(PermissionListViewModel searchModel)
        {
            if (searchModel.DocumentUserRoleID != null)
            {
                var documentuser = _context.DocumentUserRole.FirstOrDefault(f => f.DocumentUserRoleId == searchModel.DocumentUserRoleID);
                if (documentuser != null)
                {
                    _context.DocumentUserRole.Remove(documentuser);
                    _context.SaveChanges();
                }
            }
            if (searchModel.FileProfileTypeSetAccessId != null)
            {
                var fileProfileTypeSetAccess = _context.FileProfileTypeSetAccess.FirstOrDefault(f => f.FileProfileTypeSetAccessId == searchModel.FileProfileTypeSetAccessId);
                if (fileProfileTypeSetAccess != null)
                {
                    _context.FileProfileTypeSetAccess.Remove(fileProfileTypeSetAccess);
                    _context.SaveChanges();
                }
            }

        }

        [HttpPost]
        [Route("DownLoadSelfTestDocument")]
        public IActionResult DownLoadSelfTestDocument(SearchModel searchModel)
        {
            var id = searchModel.Id;
            var type = searchModel.SearchString;
            switch (type)
            {
                case "Document":
                    var document = _context.Documents.SingleOrDefault(p => p.DocumentId == id);
                    if (document != null)
                    {
                        if (document.ContentType.Contains("image"))
                        {
                            // string[] subs = document.FileName.Split('.');
                            int lastIndex = document.FileName.LastIndexOf('.');
                            var ext = document.FileName.Substring(lastIndex + 1);
                            string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\SelfTest\" + document.SessionId + "." + ext.ToString();
                            FileStream stream = System.IO.File.OpenRead(serverPath);
                            var br = new BinaryReader(stream);
                            Byte[] documents = br.ReadBytes((Int32)stream.Length);
                            Stream streams = new MemoryStream(documents);
                            if (streams == null)
                                return NotFound();

                            return Ok(streams);
                        }

                    }
                    break;
            };
            return NotFound();
        }
        [HttpPost]
        [Route("DownLoadDocument")]
        public IActionResult DownLoadDocument(SearchModel searchModel)
        {
            var id = searchModel.Id;
            var type = searchModel.SearchString;
            switch (type)
            {
                case "Document":
                    var document = _context.Documents.SingleOrDefault(p => p.DocumentId == id);
                    if (document != null)
                    {
                        if (document.ContentType == "video/mp4")
                        {
                            string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + document.SessionId + ".html";
                            if (document.FilePath != null)
                            {
                                serverPath = _hostingEnvironment.ContentRootPath + @"\" + document.FilePath;
                            }
                            FileStream stream = System.IO.File.OpenRead(serverPath);
                            var br = new BinaryReader(stream);
                            Byte[] documents = br.ReadBytes((Int32)stream.Length);
                            Stream streams = new MemoryStream(documents);
                            if (streams == null)
                                return NotFound();

                            return Ok(streams);
                        }
                        else
                        {
                            if (document.FilePath != null)
                            {

                                string serverPath = _hostingEnvironment.ContentRootPath + @"\" + document.FilePath;
                                FileStream stream = System.IO.File.OpenRead(serverPath);
                                var br = new BinaryReader(stream);
                                Byte[] documents = br.ReadBytes((Int32)stream.Length);
                                Stream streams = new MemoryStream(documents);
                                if (streams == null)
                                    return NotFound();

                                return Ok(streams);
                            }
                            else
                            {
                                if (document.IsCompressed == true)
                                {
                                    //Stream stream = new MemoryStream(Decompress(document.FileData));

                                    Stream stream = new MemoryStream(DocumentZipUnZip.Unzip(document.FileData));
                                    if (stream == null)
                                        return NotFound();

                                    return Ok(stream);
                                }
                                else
                                {
                                    Stream stream = new MemoryStream(document.FileData);
                                    if (stream == null)
                                        return NotFound();

                                    return Ok(stream);
                                }
                            }
                        }

                    }
                    break;
                case "BlanketOrderAttachment":
                    var blanketOrderAttachment = _context.BlanketOrderAttachment.SingleOrDefault(p => p.BlanketOrderAttachmentId == id);
                    if (blanketOrderAttachment != null)
                    {
                        Stream stream = new MemoryStream(blanketOrderAttachment.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "ContractDistributionAttachment":
                    var contractDistributionAttachment = _context.ContractDistributionAttachment.SingleOrDefault(p => p.ContractDistributionAttachmentId == id);
                    if (contractDistributionAttachment != null)
                    {
                        Stream stream = new MemoryStream(contractDistributionAttachment.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "SalesAdhocNovateAttachment":
                    var salesAdhocNovateAttachment = _context.SalesAdhocNovateAttachment.SingleOrDefault(p => p.SalesAdhocNovateAttachmentId == id);
                    if (salesAdhocNovateAttachment != null)
                    {
                        Stream stream = new MemoryStream(salesAdhocNovateAttachment.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "SalesOrderAttachment":
                    var salesOrderAttachment = _context.SalesOrderAttachment.SingleOrDefault(p => p.SalesOrderAttachmentId == id);
                    if (salesOrderAttachment != null)
                    {
                        Stream stream = new MemoryStream(salesOrderAttachment.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "SalesOrderAtCommonFieldsProductionMachineDocumenttachment":
                    var commonFieldsProductionMachineDocument = _context.CommonFieldsProductionMachineDocument.SingleOrDefault(p => p.MachineDocumentId == id);
                    if (commonFieldsProductionMachineDocument != null)
                    {
                        Stream stream = new MemoryStream(commonFieldsProductionMachineDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "TransferBalanceQtyDocument":
                    var transferBalanceQtyDocument = _context.TransferBalanceQtyDocument.SingleOrDefault(p => p.TransferBalanceQtyDocumentId == id);
                    if (transferBalanceQtyDocument != null)
                    {
                        Stream stream = new MemoryStream(transferBalanceQtyDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "QuotationHistoryDocument":
                    var quotationHistoryDocument = _context.QuotationHistoryDocument.SingleOrDefault(p => p.QuotationHistoryDocumentId == id);
                    if (quotationHistoryDocument != null)
                    {
                        Stream stream = new MemoryStream(quotationHistoryDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "PurchaseOrderDocument":
                    var purchaseOrderDocument = _context.PurchaseOrderDocument.SingleOrDefault(p => p.PurchaseOrderDocumentId == id);
                    if (purchaseOrderDocument != null)
                    {
                        Stream stream = new MemoryStream(purchaseOrderDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "ProductRecipeDocument":
                    var productRecipeDocument = _context.ProductRecipeDocument.SingleOrDefault(p => p.ProductRecipeDocumentId == id);
                    if (productRecipeDocument != null)
                    {
                        Stream stream = new MemoryStream(productRecipeDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "PackagingHistoryItemLineDocument":
                    var packagingHistoryItemLineDocument = _context.PackagingHistoryItemLineDocument.SingleOrDefault(p => p.PackagingHistoryItemLineDocumentId == id);
                    if (packagingHistoryItemLineDocument != null)
                    {
                        Stream stream = new MemoryStream(packagingHistoryItemLineDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
                case "IpirlineDocument":
                    var ipirlineDocument = _context.IpirlineDocument.SingleOrDefault(p => p.IpirlineDocumentId == id);
                    if (ipirlineDocument != null)
                    {
                        Stream stream = new MemoryStream(ipirlineDocument.FileData);
                        if (stream == null)
                            return NotFound();

                        return Ok(stream);
                    }
                    break;
            };
            return NotFound();
        }

        //[HttpPost]
        //[Route("CreateDocument")]
        //public async Task<IActionResult> CreateDocument()
        //{
        //    var userId = "1";
        //    var progressInfo = string.Empty;
        //    progressInfo = DateTime.Now.ToLongTimeString() + "- File upload starts.";
        //   await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

        //    var boundary = HeaderUtilities.RemoveQuotes(
        //        MediaTypeHeaderValue.Parse(Request.ContentType).Boundary
        //    ).Value;

        //    var reader = new MultipartReader(boundary, Request.Body);
        //    progressInfo = DateTime.Now.ToLongTimeString() + "- ReadNextSectionAsync";
        //    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
        //    var section = await reader.ReadNextSectionAsync();
        //    var formAccumelator = new KeyValueAccumulator();
        //    byte[] dataArray = Array.Empty<byte>();
        //    while (section != null)
        //    {
        //        var hasContentDisposition = ContentDispositionHeaderValue.TryParse(
        //            section.ContentDisposition, out var contentDisposition
        //        );
        //        progressInfo = DateTime.Now.ToLongTimeString() + "- hasContentDisposition";
        //        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
        //        if (hasContentDisposition)
        //        {
        //            if (contentDisposition.DispositionType.Equals("form-data") &&
        //            (!string.IsNullOrEmpty(contentDisposition.FileName.Value) ||
        //            !string.IsNullOrEmpty(contentDisposition.FileNameStar.Value)))
        //            {
        //                progressInfo = DateTime.Now.ToLongTimeString() + "- uploaded files form fileds";
        //                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
        //                // uploaded files form fileds
        //                byte[] fileByteArray;
        //                using (var memoryStream = new BinaryReader(section.Body))
        //                {
        //                    progressInfo = DateTime.Now.ToLongTimeString() + "- CopyToAsync";
        //                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
        //                    // await section.Body.CopyToAsync(memoryStream);
        //                    dataArray =  memoryStream.ReadBytes((int)section.Body.Length);
        //                    progressInfo = DateTime.Now.ToLongTimeString() + "- memoryStream.ToArray";
        //                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
        //                    //dataArray = memoryStream.ToArray();
        //                }

        //            }
        //            else
        //            {
        //                var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name).Value;
        //                using (var streamReader = new StreamReader(section.Body,
        //                encoding: Encoding.UTF8,
        //                detectEncodingFromByteOrderMarks: true,
        //                bufferSize: 1024,
        //                leaveOpen: true))
        //                {
        //                    var value = await streamReader.ReadToEndAsync();
        //                    if (string.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        value = string.Empty;
        //                    }
        //                    formAccumelator.Append(key, value);
        //                }
        //            }
        //        }
        //        section = await reader.ReadNextSectionAsync();
        //    }

        //    progressInfo = DateTime.Now.ToLongTimeString() + "- Uploaded successfully";
        //    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

        //    return Content("Uploaded successfully");
        //}


        [HttpPut]
        [Route("UpdateDescriptionField")]
        public DocumentsModel UpdateDescriptionField(DocumentsModel value)
        {

            var documents = _context.Documents.Where(d => d.DocumentId == value.DocumentID && d.FilterProfileTypeId == value.FilterProfileTypeId).FirstOrDefault();
            if (documents != null)
            {
                documents.Description = value.Description;
                documents.ModifiedByUserId = value.ModifiedByUserID;
                documents.ModifiedDate = DateTime.Now;
            }
            var linkfileprofiledoc = _context.LinkFileProfileTypeDocument.Where(l => l.DocumentId == value.DocumentID && l.TransactionSessionId == documents.SessionId).FirstOrDefault();
            if (linkfileprofiledoc != null)
            {
                linkfileprofiledoc.Description = value.Description;
            }
            _context.SaveChanges();
            return value;
        }

        [HttpPut]
        [Route("UpdateExpiryDateField")]
        public DocumentsModel UpdateExpiryDateField(DocumentsModel value)
        {

            var documents = _context.Documents.Where(d => d.DocumentId == value.DocumentID && d.FilterProfileTypeId == value.FilterProfileTypeId).FirstOrDefault();
            if (documents != null)
            {
                documents.ExpiryDate = value.ExpiryDate;
                documents.ModifiedByUserId = value.ModifiedByUserID;
                documents.ModifiedDate = DateTime.Now;
            }

            _context.SaveChanges();
            return value;
        }

        [HttpPost]
        [Route("CreateDocument")]
        public async Task<IActionResult> CreateDocument(IFormCollection files)
        {
            var userId = -1;
            var sessionId = Guid.NewGuid();
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            if (userExits > 0)
            {
                var progressInfo = string.Empty;
                int idx = 0;
                var profileSelect = files["ProfileSelect"].ToString();
                long? plantID = null;
                var plantIDs = Convert.ToInt64(files["plantID"].ToString());
                if (plantIDs > 0)
                {
                    plantID = plantIDs;
                }
                long? departmentId = null;
                var departmentIds = Convert.ToInt64(files["departmentId"].ToString());
                if (departmentIds > 0)
                {
                    departmentId = departmentIds;
                }
                long? sectionId = null;
                var sectionIds = Convert.ToInt64(files["sectionId"].ToString());
                if (sectionIds > 0)
                {
                    sectionId = sectionIds;
                }
                long? subSectionId = null;
                var subSectionIds = Convert.ToInt64(files["subSectionId"].ToString());
                if (subSectionIds > 0)
                {
                    subSectionId = subSectionIds;
                }
                long? divisionId = null;
                var divisionIds = Convert.ToInt64(files["divisionId"].ToString());
                if (divisionIds > 0)
                {
                    divisionId = divisionIds;
                }
                foreach (var f in files.Files)
                {
                    DateTime? expiryDate = new DateTime?();
                    var userIds = files["userID"].ToString().Split(',').ToList();
                    sessionId = Guid.NewGuid();
                    userId = Convert.ToInt32(userIds.FirstOrDefault().ToString());
                    progressInfo = DateTime.Now.ToLongTimeString() + "- File upload starts.";
                    var description = files["description"].ToString();
                    var expiryDateString = files["expiryDate"].ToString();
                    if (expiryDateString != null && expiryDateString != "" && expiryDateString != "undefined" && expiryDateString != "null")
                    {
                        expiryDate = Convert.ToDateTime(expiryDateString);
                    }
                    else
                    {
                        expiryDate = null;
                    }
                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                    var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString().Split(',')[0].ToString());
                    var tableName = files["type"].ToString().Split(',')[0].ToString();
                    var videoFiles = files["isVideoFile"].ToString().Split(",");

                    var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                    string profileNo = "";
                    progressInfo = DateTime.Now.ToLongTimeString() + "- Creating file profile no.";

                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                    progressInfo = DateTime.Now.ToLongTimeString() + "- Profile no. assigned to uploaded document.";

                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());



                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                    var fileName = sessionId + "." + f.FileName.Split(".").Last();
                    var file = f;
                    progressInfo = DateTime.Now.ToLongTimeString() + "- Reading Open File Stream.";
                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                    byte[] dataArray = new byte[0];
                    //var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName;
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionId;

                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    var isVideoFiles = false;
                    var contentType = f.ContentType.Split("/");
                    if (contentType[0] == "video")
                    {
                        isVideoFiles = true;
                    }
                    string lhs = i < 0 ? ext : ext.Substring(0, i), rhs = i < 0 ? "" : ext.Substring(i + 1);
                    var serverPath = serverPaths + @"\" + fileName;
                    var filePath = getNextFileName(serverPath);
                    var newFile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        progressInfo = DateTime.Now.ToLongTimeString() + "- Reading file.CopyTo.";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    progressInfo = DateTime.Now.ToLongTimeString() + "- Save Data into Database.";
                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                    var existing = _context.Documents.Where(d => d.FileName == file.FileName && d.ContentType == file.ContentType && d.FilterProfileTypeId == fileProfileTypeId).FirstOrDefault();

                    if (existing == null)
                    {
                        if (profile != null)
                        {
                            long? SectionId = new long?();
                            long? SubSectionId = new long?();
                            if (profile.ProfileId != null)
                            {
                                var docu = _context.ProfileAutoNumber.FirstOrDefault(f => f.ProfileId == profile.ProfileId);
                                if (docu != null)
                                {
                                    SectionId = docu.SectionId;
                                    SubSectionId = docu.SubSectionId;
                                }
                            }
                            if (profileSelect == "ProfileSelect")
                            {
                                profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
                                {
                                    ProfileID = profile.ProfileId,
                                    AddedByUserID = userId,
                                    StatusCodeID = 710,
                                    DepartmentName = files["departmentName"].ToString(),
                                    CompanyCode = files["companyCode"].ToString(),
                                    SectionName = files["sectionName"].ToString(),
                                    SubSectionName = files["subSectionName"].ToString(),
                                    DepartmentId = departmentId,
                                    PlantID = plantID,
                                    SectionId = sectionId,
                                    SubSectionId = subSectionId,
                                    ScreenAutoNumberId = profile.FileProfileTypeId,
                                });
                            }
                            else
                            {
                                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profile.Name, SectionId = SectionId, SubSectionId = SubSectionId });
                            }
                        }
                        _context.SaveChanges();
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = null,
                            FileSize = file.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionId,
                            FilterProfileTypeId = fileProfileTypeId,
                            TableName = tableName,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                            IsVideoFile = isVideoFiles,
                            Description = description,
                            ExpiryDate = expiryDate,
                            FileIndex = 0,
                            FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                            SourceFrom = "FileProfile",
                        };
                        _context.Documents.Add(documents);
                    }
                    else
                    {
                        progressInfo = "";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        try
                        {
                            throw new Exception("File Already existing this profile type or history");
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("File Already existing this profile type or history", ex);

                        }
                        //progressInfo = "File Already existing this profile type";
                        //await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        //return new Exception(error,"File Already existing this profile type");
                        //return error;
                    }
                    // }
                    /* else
                     {
                         using var fileStream = file.OpenReadStream();
                         byte[] bytes = new byte[file.Length];
                         fileStream.Read(bytes, 0, (int)file.Length);

                         progressInfo = DateTime.Now.ToLongTimeString() + "- Reading ReadAllBytes.";
                         await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                         Byte[] document = bytes;// System.IO.File.ReadAllBytes(serverPath);

                         // br.ReadBytes((Int32)fs.Length);
                         progressInfo = DateTime.Now.ToLongTimeString() + "- Compressing file before upload.";

                         await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                         progressInfo = DateTime.Now.ToLongTimeString() + "- Compressing file process started.";

                         await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                         var compressedData = DocumentZipUnZip.Zip(document);// Compress(document);
                         progressInfo = DateTime.Now.ToLongTimeString() + "- File compression completed.";

                         await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                         var existing = _context.Documents.Where(d => d.FileName == file.FileName && d.ContentType == file.ContentType && d.FilterProfileTypeId == fileProfileTypeId).FirstOrDefault();
                         if (existing == null)
                         {
                             if (profile != null)
                             {
                                 long? SectionId = new long?();
                                 long? SubSectionId = new long?();
                                 if (profile.ProfileId != null)
                                 {
                                     var docu = _context.ProfileAutoNumber.FirstOrDefault(f => f.ProfileId == profile.ProfileId);
                                     if (docu != null)
                                     {
                                         SectionId = docu.SectionId;
                                         SubSectionId = docu.SubSectionId;
                                     }
                                 }
                                 if (profileSelect == "ProfileSelect")
                                 {
                                     profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
                                     {
                                         ProfileID = profile.ProfileId,
                                         AddedByUserID = userId,
                                         StatusCodeID = 710,
                                         DepartmentName = files["departmentName"].ToString(),
                                         CompanyCode = files["companyCode"].ToString(),
                                         SectionName = files["sectionName"].ToString(),
                                         SubSectionName = files["subSectionName"].ToString(),
                                         DepartmentId = departmentId,
                                         PlantID = plantID,
                                         SectionId = sectionId,
                                         SubSectionId = subSectionId,
                                         ScreenAutoNumberId = profile.FileProfileTypeId,
                                     });
                                 }
                                 else
                                 {
                                     profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, AddedByUserID = userId, StatusCodeID = 710, Title = profile.Name, SectionId = SectionId, SubSectionId = SubSectionId });
                                 }
                             }
                             _context.SaveChanges();
                             var documents = new Documents
                             {
                                 FileName = file.FileName,
                                 ContentType = file.ContentType,
                                 FileData = compressedData,
                                 FileSize = compressedData.Length,
                                 UploadDate = DateTime.Now,
                                 SessionId = sessionId,
                                 FilterProfileTypeId = fileProfileTypeId,
                                 TableName = tableName,
                                 ProfileNo = profileNo,
                                 IsLatest = true,
                                 AddedByUserId = userId,
                                 AddedDate = DateTime.Now,
                                 IsCompressed = true,
                                 Description = description,
                                 ExpiryDate = expiryDate,
                                 FileIndex = 0,
                             };
                             _context.Documents.Add(documents);


                         }

                         else
                         {
                             progressInfo = "";
                             await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                             try
                             {
                                 throw new Exception("File Already existing to this profile type or history");
                             }
                             catch (Exception ex)
                             {
                                 throw new Exception("File Already existing to this profile type or history", ex);
                             }
                             //progressInfo = "File Already existing this profile type";
                             //await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                             //return new Exception(error,"File Already existing this profile type");
                             //return error;
                         }

                         //  System.IO.File.Delete(serverPath);
                     }*/
                    idx++;

                    progressInfo = DateTime.Now.ToLongTimeString() + "- File upload started. It will take sometime to complete.";

                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                    _context.SaveChanges();
                }
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload completed.";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                progressInfo = DateTime.Now.ToLongTimeString() + "- Completed";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
            }
            return Content(sessionId.ToString());

        }
        private byte[] CombineJunk(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        public static IEnumerable<byte[]> ReadChunks(string path)
        {
            var lengthBytes = new byte[sizeof(int)];

            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                int n = fs.Read(lengthBytes, 0, sizeof(int));  // Read block size.

                if (n == 0)      // End of file.
                    yield break;

                if (n != sizeof(int))
                    throw new InvalidOperationException("Invalid header");

                int blockLength = BitConverter.ToInt32(lengthBytes, 0);
                var buffer = new byte[blockLength];
                n = fs.Read(buffer, 0, blockLength);

                if (n != blockLength)
                    throw new InvalidOperationException("Missing data");

                yield return buffer;
            }
        }
        public byte[] ReadAllBytes(BinaryReader reader)
        {
            const int bufferSize = 4096;
            using (var ms = new MemoryStream())
            {
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                    ms.Write(buffer, 0, count);
                return ms.ToArray();
            }

        }
        [HttpPost]
        [Route("UploadDocument")]
        public async Task<IActionResult> UploadDocument(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            var filePath = files["filePath"].ToString();
            if (filePath == "filePath")
            {
                UploadFileAsByPath(files);
            }
            else
            {
                var progressInfo = string.Empty;
                long? fileProfileTypeId = new long?();
                DateTime? expiryDate = new DateTime?();

                var wikiStatusType = files["wikiStatusType"].ToString();
                var screenID = files["screenID"].ToString();
                var expiryDateString = files["expiryDate"].ToString();
                var subjectName = files["subjectName"].ToString();
                if (expiryDateString != null && expiryDateString != "" && expiryDateString != "undefined" && expiryDateString != "null")
                {
                    expiryDate = Convert.ToDateTime(expiryDateString);
                }
                else
                {
                    expiryDate = null;
                }

                var userId = Convert.ToInt32(files["userID"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var description = "";
                if (files["description"].ToString() != "null")
                {
                    description = files["description"].ToString();
                }
                var link = files["link"].ToString();
                int idx = 0;
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload starts.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                bool? wikiStatusTypes = null;
                bool? isWikiFlag = null;
                if (wikiStatusType == "Draft")
                {
                    wikiStatusTypes = true;
                }
                if (wikiStatusType == "Wiki")
                {
                    isWikiFlag = true;
                }
                var fileProfileType = files["fileProfileTypeId"].ToString();
                if (fileProfileType != "null" && fileProfileType != null && fileProfileType != "")
                {
                    fileProfileTypeId = Convert.ToInt32(fileProfileType);
                }
                var tableName = files["type"].ToString();
                var documentId = Convert.ToInt32(files["documentID"].ToString());
                progressInfo = DateTime.Now.ToLongTimeString() + "- Get existing document to create history data.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                var documentItem = _context.Documents.FirstOrDefault(d => d.DocumentId == documentId);
                var documentItemIsLatest = documentItem;
                var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == documentId && t.TaskMasterId != null);
                string profileNo = "";
                bool? isFromProfileDocument = false;
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                if (documentItem != null)
                {
                    profileNo = documentItem.ProfileNo;

                }
                var task = new TaskMaster();
                if (taskattach != null)
                {
                    task = _context.TaskMaster.FirstOrDefault(d => d.TaskId == taskattach.TaskMasterId);
                    if (task != null)
                    {
                        isFromProfileDocument = task.IsFromProfileDocument;
                    }
                }
                var isLocked = files["isLocked"].ToString();
                progressInfo = DateTime.Now.ToLongTimeString() + "- Document history created & Version updated.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                progressInfo = DateTime.Now.ToLongTimeString() + "-Create New profile.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                var fileIndex = documentItem.FileIndex == null ? 0 : documentItem.FileIndex;
                //if (profile != null)
                //{

                //    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name });
                //}
                progressInfo = DateTime.Now.ToLongTimeString() + "-Assigned profile no.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                //if (tableName == "Document")
                // {
                //documentItem.IsLocked = isLocked=="" || Convert.ToBoolean(isLocked) == true ? false : true;

                var newCheckinDocId = new long?();
                //_context.SaveChanges();
                // }
                /* if (documentItem != null)
                 {
                     documentItem.IsLatest = false;
                     _context.SaveChanges();
                 }*/
                foreach (var f in files.Files)
                {
                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                    var file = f;
                    var fileName1 = sessionID + "." + f.FileName.Split(".").Last();
                    if (isVideoFile)
                    {
                        var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + fileName1;
                        using (var targetStream = System.IO.File.Create(serverPath))
                        {
                            progressInfo = DateTime.Now.ToLongTimeString() + "- Reading file.CopyTo.";
                            await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                            file.CopyTo(targetStream);
                            targetStream.Flush();
                        }
                        progressInfo = DateTime.Now.ToLongTimeString() + "- Save Data into Database.";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        var documents = new Documents
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            FileData = null,
                            FileSize = file.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionID,
                            FilterProfileTypeId = fileProfileTypeId,
                            TableName = tableName,
                            ProfileNo = profileNo,
                            IsLatest = true,
                            AddedByUserId = userId,
                            AddedDate = DateTime.Now,
                            IsVideoFile = true,
                            Description = description,
                            DocumentParentId = documentItem.DocumentParentId > 0 ? documentItem.DocumentParentId : documentItem.DocumentId,
                            //TaskId = documentItem?.TaskId!=null? documentItem?.TaskId: null,
                            //IsMainTask = false,
                            ExpiryDate = expiryDate,
                            IsWikiDraft = wikiStatusTypes,
                            IsWiki = isWikiFlag,
                            SubjectName = subjectName,
                            SourceFrom = "FileProfile",

                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                        if (wikiStatusType == "Draft")
                        {
                            var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == sessionID && f.StatusCodeId == 840)?.ApplicationWikiId;
                            if (appWikId > 0)
                            {
                                var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{0}'  Where ApplicationWikiId= {1} and DocumentId='{2}' ", documents.DocumentId, appWikId, documentId);
                                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            }
                        }
                        if (wikiStatusType == null || wikiStatusType == "")
                        {
                            if (documentItemIsLatest != null && documentItemIsLatest.IsLatest != true)
                            {
                                //From Upload DMS
                                var appWikiReleaseDocIds = _context.AppWikiReleaseDoc.Where(w => w.DocumentId == documentId).ToList();
                                if (appWikiReleaseDocIds != null && appWikiReleaseDocIds.Count > 0)
                                {
                                    var appWikiReleaseDocquerys = string.Format("Update AppWikiReleaseDoc Set DocumentId='{1}' Where AppWikiReleaseDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiReleaseDocIds.Select(s => s.AppWikiReleaseDocId)), documents.DocumentId);
                                    _context.Database.ExecuteSqlRaw(appWikiReleaseDocquerys);
                                }

                                var appWikiDraftDocIds = _context.AppWikiDraftDoc.Where(w => w.DocumentId == documentId).ToList();
                                if (appWikiDraftDocIds != null && appWikiDraftDocIds.Count > 0)
                                {
                                    var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{1}' Where AppWikiDraftDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiDraftDocIds.Select(s => s.AppWikiDraftDocId)), documents.DocumentId);
                                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                                }
                            }

                        }
                        documentItem.FileIndex = fileIndex;
                        documentItem.IsLatest = false;
                        _context.SaveChanges();
                        newCheckinDocId = documents.DocumentId;
                        if (isFromProfileDocument != null && isFromProfileDocument == true)
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = false;
                            }
                            _context.SaveChanges();

                        }
                        else
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = null;
                            }
                        }
                        var fileProfileTypeDynamicForm = _context.FileProfileTypeDynamicForm.Where(w => w.DocumentId == documentId).Count();
                        if (fileProfileTypeDynamicForm > 0)
                        {
                            var queryForm = string.Format("Update FileProfileTypeDynamicForm Set DocumentId='{0}' Where DocumentId='{1}'", documents.DocumentId, documentId);
                            _context.Database.ExecuteSqlRaw(queryForm);
                        }


                    }
                    else
                    {
                        var fs = file.OpenReadStream();
                        var br = new BinaryReader(fs);
                        Byte[] document = br.ReadBytes((Int32)fs.Length);
                        progressInfo = DateTime.Now.ToLongTimeString() + "- Compress file before upload.";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        var compressedData = DocumentZipUnZip.Zip(document);// Compress(document);

                        progressInfo = DateTime.Now.ToLongTimeString() + "- Compress file process completed.";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        var fileName = file.FileName.Split("_V0")[0];
                        var index_point = documentItem.FileName.IndexOf(".") + 1;
                        var extension = '.' + documentItem.FileName.Substring(index_point);

                        fileName = fileName.Contains(extension) ? fileName : fileName + extension;
                        var documents = new Documents
                        {
                            FileName = fileName == documentItem.FileName ? fileName : file.FileName,
                            ContentType = file.ContentType,
                            FileData = compressedData,
                            FileSize = fs.Length,
                            UploadDate = DateTime.Now,
                            SessionId = sessionID,
                            ScreenId = screenID,
                            FilterProfileTypeId = fileProfileTypeId,
                            TableName = tableName,
                            ProfileNo = profileNo,
                            DocumentParentId = documentItem.DocumentParentId > 0 ? documentItem.DocumentParentId : documentItem.DocumentId,
                            IsLatest = true,
                            IsLocked = false,
                            AddedDate = DateTime.Now,
                            AddedByUserId = userId,
                            IsCompressed = true,
                            Description = description,
                            FileIndex = fileName == documentItem.FileName && documentItem.FileIndex != null ? documentItem.FileIndex + 1 : 0,
                            ExpiryDate = expiryDate,
                            IsWikiDraft = wikiStatusTypes,
                            IsWiki = isWikiFlag,
                            SubjectName = subjectName,
                            SourceFrom = "FileProfile",
                            //TaskId = documentItem?.TaskId != null ? documentItem?.TaskId : null,
                            //IsMainTask= false,
                        };
                        _context.Documents.Add(documents);
                        _context.SaveChanges();
                        if (wikiStatusType == "Draft")
                        {
                            var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == sessionID && f.StatusCodeId == 840)?.ApplicationWikiId;
                            if (appWikId > 0)
                            {
                                var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{0}'  Where ApplicationWikiId= {1} and DocumentId='{2}' ", documents.DocumentId, appWikId, documentId);
                                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            }
                        }
                        if (wikiStatusType == null || wikiStatusType == "")
                        {
                            if (documentItemIsLatest != null && documentItemIsLatest.IsLatest != true)
                            {
                                //From Upload DMS
                                var appWikiReleaseDocIds = _context.AppWikiReleaseDoc.Where(w => w.DocumentId == documentId).ToList();
                                if (appWikiReleaseDocIds != null && appWikiReleaseDocIds.Count > 0)
                                {
                                    var appWikiReleaseDocquerys = string.Format("Update AppWikiReleaseDoc Set DocumentId='{1}' Where AppWikiReleaseDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiReleaseDocIds.Select(s => s.AppWikiReleaseDocId)), documents.DocumentId);
                                    _context.Database.ExecuteSqlRaw(appWikiReleaseDocquerys);
                                }
                                var appWikiDraftDocIds = _context.AppWikiDraftDoc.Where(w => w.DocumentId == documentId).ToList();
                                if (appWikiDraftDocIds != null && appWikiDraftDocIds.Count > 0)
                                {
                                    var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{1}' Where AppWikiDraftDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiDraftDocIds.Select(s => s.AppWikiDraftDocId)), documents.DocumentId);
                                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                                }
                            }
                        }
                        //documentItem.FileIndex = fileIndex;
                        documentItem.IsLatest = false;
                        _context.SaveChanges();
                        newCheckinDocId = documents.DocumentId;
                        // isFromProfileDocument = _context.TaskMaster.FirstOrDefault(d => d.TaskId == documentItem.TaskId)?.IsFromProfileDocument;
                        if (isFromProfileDocument != null && isFromProfileDocument == true)
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = false;
                            }
                            _context.SaveChanges();

                        }
                        else
                        {
                            var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                            if (doccheck != null)
                            {
                                doccheck.IsMainTask = null;
                            }
                        }
                        var fileProfileTypeDynamicForm = _context.FileProfileTypeDynamicForm.Where(w => w.DocumentId == documentId).Count();
                        if (fileProfileTypeDynamicForm > 0)
                        {

                            var queryForm = string.Format("Update FileProfileTypeDynamicForm Set DocumentId='{0}' Where DocumentId='{1}'", documents.DocumentId, documentId);
                            _context.Database.ExecuteSqlRaw(queryForm);
                        }

                    }
                    idx++;
                }
                if (documentId != null && newCheckinDocId != null)
                {

                    if (isFromProfileDocument != null && isFromProfileDocument == true)
                    {

                        if (task.TaskId != null)
                        {
                            var source = _context.TaskMaster.FirstOrDefault(t => t.TaskId == task.TaskId);
                            if (source != null)
                            {
                                if (source.SourceId == documentId)
                                {
                                    source.SourceId = newCheckinDocId;
                                    var documentlink = _context.DocumentLink.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                                    if (documentlink != null && documentlink.Count > 0)
                                    {
                                        //documentlink.ForEach(d =>
                                        //{
                                        var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                                        var rowaffected = _context.Database.ExecuteSqlRaw(query);


                                    }
                                    var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documentId);
                                    long? taskId = new long?();
                                    if (doccheck != null)
                                    {
                                        doccheck.IsLatest = false;
                                        doccheck.TaskId = null;
                                        doccheck.IsMainTask = false;
                                    }
                                    var newdoc = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                    if (newdoc != null)
                                    {
                                        newdoc.IsMainTask = true;
                                        newdoc.TaskId = taskId;
                                    }
                                    _context.SaveChanges();

                                }
                                else if (source.SourceId != documentId)
                                {
                                    var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                    if (doccheck != null)
                                    {
                                        doccheck.IsLatest = true;
                                        doccheck.IsMainTask = false;
                                    }
                                }
                                var documentlinklist = _context.DocumentLink.Where(d => d.LinkDocumentId == documentId).AsNoTracking().ToList();
                                var checkin = "Link From Task CheckIn";

                                if (documentlinklist != null && documentlinklist.Count > 0)
                                {
                                    documentlinklist.ForEach(d =>
                                    {
                                        var doclinkexist = _context.DocumentLink.Where(r => r.LinkDocumentId == d.LinkDocumentId)?.FirstOrDefault();
                                        if (doclinkexist != null)
                                        {
                                            var linkparentdocId = doclinkexist.DocumentId;
                                            _context.DocumentLink.Remove(doclinkexist);
                                            _context.SaveChanges();

                                            if (linkparentdocId != newCheckinDocId)
                                            {
                                                var DocumentLink = new DocumentLink
                                                {
                                                    DocumentId = linkparentdocId,
                                                    AddedByUserId = userId,
                                                    AddedDate = DateTime.Now,
                                                    StatusCodeId = 1,
                                                    LinkDocumentId = newCheckinDocId,

                                                    FileProfieTypeId = documentItem.FilterProfileTypeId,
                                                    DocumentPath = "Document Link From Task checkin",

                                                };
                                                _context.DocumentLink.Add(DocumentLink);
                                                _context.SaveChanges();
                                            }
                                        }

                                    });

                                }
                            }
                            var taskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.TaskMasterId == task.TaskId && d.DocumentId == documentId);
                            if (taskAttachment != null)
                            {
                                taskAttachment.IsLatest = false;
                                taskAttachment.IsLocked = true;
                                taskAttachment.LockedDate = DateTime.Now;
                                taskAttachment.LockedByUserId = userId;
                                _context.SaveChanges();

                                var taskAttach = new TaskAttachment
                                {
                                    DocumentId = newCheckinDocId,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = userId,
                                    VersionNo = "1",
                                    TaskMasterId = task.TaskId,
                                    CheckInDescription = description,
                                    PreviousDocumentId = taskAttachment.PreviousDocumentId == null ? taskAttachment.TaskAttachmentId : taskAttachment.PreviousDocumentId,

                                };
                                _context.TaskAttachment.Add(taskAttach);
                                _context.SaveChanges();
                                var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                var notes = new Notes
                                {
                                    Notes1 = task.Title,
                                    Link = link + "id=" + task.TaskId + "&taskid=" + task.TaskId,
                                    DocumentId = newCheckinDocId,
                                    SessionId = doccheck?.SessionId,
                                    AddedByUserId = userId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = 1,


                                };
                                _context.Notes.Add(notes);
                                _context.SaveChanges();
                            }
                        }




                    }
                    else
                    {
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            //documentlink.ForEach(d =>
                            //{
                            var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);


                        }
                        var documentlinkedDoc = _context.DocumentLink.Where(d => d.LinkDocumentId == documentId).AsNoTracking().ToList();
                        if (documentlinkedDoc != null && documentlinkedDoc.Count > 0)
                        {
                            var query = string.Format("Update DocumentLink Set LinkDocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where LinkDocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);

                        }

                        var notes = _context.Notes.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                        if (notes != null && notes.Count > 0)
                        {
                            var query = string.Format("Update Notes Set DocumentId='{1}'  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);

                        }
                    }

                    var linkFileProfileType = _context.LinkFileProfileTypeDocument.Where(l => l.DocumentId == documentId).FirstOrDefault();
                    if (linkFileProfileType != null)
                    {
                        linkFileProfileType.DocumentId = newCheckinDocId;
                        _context.SaveChanges();
                    }

                }
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload started. It will take sometime to complete.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                if (documentId != null && newCheckinDocId != null)
                {

                    var query = string.Format("Update NotifyDocument Set DocumentId='{1}'  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);

                }
                if (documentItem.CloseDocumentId != null && documentItem.CloseDocumentId == 2561)
                {
                    documentItem.CloseDocumentId = null;
                }
                _context.SaveChanges();
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload completed.";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                progressInfo = DateTime.Now.ToLongTimeString() + "- Completed";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
            }
            return Content(sessionID.ToString());

        }
        private string getNextFileName(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            int i = 0;
            while (System.IO.File.Exists(fileName))
            {
                if (i == 0)
                    fileName = fileName.Replace(extension, "(" + ++i + ")" + extension);
                else
                    fileName = fileName.Replace("(" + i + ")" + extension, "(" + ++i + ")" + extension);
            }

            return fileName;
        }
        private async Task<IActionResult> UploadFileAsByPath(IFormCollection files)
        {
            var progressInfo = string.Empty;
            long? fileProfileTypeId = new long?();
            DateTime? expiryDate = new DateTime?();
            var sessionID = new Guid(files["sessionID"].ToString());
            var userSession = new Guid(files["userSession"].ToString());
            var userExits = _context.ApplicationUser.Where(a => a.SessionId == userSession).Count();
            if (userExits > 0)
            {
                var wikiStatusType = files["wikiStatusType"].ToString();
                var screenID = files["screenID"].ToString();
                var expiryDateString = files["expiryDate"].ToString();
                var subjectName = files["subjectName"].ToString();
                if (expiryDateString != null && expiryDateString != "" && expiryDateString != "undefined" && expiryDateString != "null")
                {
                    expiryDate = Convert.ToDateTime(expiryDateString);
                }
                else
                {
                    expiryDate = null;
                }

                var userId = Convert.ToInt32(files["userID"].ToString());
                var videoFiles = files["isVideoFile"].ToString().Split(",");
                var description = "";
                if (files["description"].ToString() != "null")
                {
                    description = files["description"].ToString();
                }
                var link = files["link"].ToString();
                int idx = 0;
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload starts.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                bool? wikiStatusTypes = null;
                bool? isWikiFlag = null;
                if (wikiStatusType == "Draft")
                {
                    wikiStatusTypes = true;
                }
                if (wikiStatusType == "Wiki")
                {
                    isWikiFlag = true;
                }
                var fileProfileType = files["fileProfileTypeId"].ToString();
                if (fileProfileType != "null" && fileProfileType != null && fileProfileType != "")
                {
                    fileProfileTypeId = Convert.ToInt32(fileProfileType);
                }
                var tableName = files["type"].ToString();
                var documentId = Convert.ToInt32(files["documentID"].ToString());
                progressInfo = DateTime.Now.ToLongTimeString() + "- Get existing document to create history data.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                var documentItem = _context.Documents.FirstOrDefault(d => d.DocumentId == documentId);
                var documentItemIsLatest = documentItem;
                var taskattach = _context.TaskAttachment.FirstOrDefault(t => t.DocumentId == documentId && t.TaskMasterId != null);
                string profileNo = "";
                bool? isFromProfileDocument = false;
                var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
                if (documentItem != null)
                {
                    profileNo = documentItem.ProfileNo;

                }
                var task = new TaskMaster();
                if (taskattach != null)
                {
                    task = _context.TaskMaster.FirstOrDefault(d => d.TaskId == taskattach.TaskMasterId);
                    if (task != null)
                    {
                        isFromProfileDocument = task.IsFromProfileDocument;
                    }
                }
                var isLocked = files["isLocked"].ToString();
                progressInfo = DateTime.Now.ToLongTimeString() + "- Document history created & Version updated.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());

                progressInfo = DateTime.Now.ToLongTimeString() + "-Create New profile.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                var fileIndex = documentItem.FileIndex == null ? 0 : documentItem.FileIndex;
                //if (profile != null)
                //{

                //    profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, Title = profile.Name });
                //}
                progressInfo = DateTime.Now.ToLongTimeString() + "-Assigned profile no.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                //if (tableName == "Document")
                // {
                //documentItem.IsLocked = isLocked=="" || Convert.ToBoolean(isLocked) == true ? false : true;

                var newCheckinDocId = new long?();
                foreach (var f in files.Files)
                {
                    var isVideoFile = Convert.ToBoolean(videoFiles[idx]);
                    var file = f;
                    var serverPaths = _hostingEnvironment.ContentRootPath + @"\AppUpload\Files\" + sessionID;
                    if (!System.IO.Directory.Exists(serverPaths))
                    {
                        System.IO.Directory.CreateDirectory(serverPaths);
                    }
                    var ext = "";
                    ext = f.FileName;
                    int i = ext.LastIndexOf('.');
                    string[] split = f.FileName.Split('.');
                    var serverPath = serverPaths + @"\" + sessionID + '.' + split.Last();
                    var filePath = getNextFileName(serverPath);
                    var newfile = filePath.Replace(serverPaths + @"\", "");
                    using (var targetStream = System.IO.File.Create(filePath))
                    {
                        progressInfo = DateTime.Now.ToLongTimeString() + "- Reading file.CopyTo.";
                        await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                        file.CopyTo(targetStream);
                        targetStream.Flush();
                    }
                    progressInfo = DateTime.Now.ToLongTimeString() + "- Save Data into Database.";
                    await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                    var documents = new Documents
                    {
                        FileName = f.FileName,
                        //FileName = newfile,
                        ContentType = file.ContentType,
                        FileData = null,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        SessionId = sessionID,
                        FilterProfileTypeId = fileProfileTypeId,
                        TableName = tableName,
                        ProfileNo = profileNo,
                        IsLatest = true,
                        AddedByUserId = userId,
                        AddedDate = DateTime.Now,
                        IsVideoFile = true,
                        Description = description,
                        DocumentParentId = documentItem.DocumentParentId > 0 ? documentItem.DocumentParentId : documentItem.DocumentId,
                        //TaskId = documentItem?.TaskId!=null? documentItem?.TaskId: null,
                        //IsMainTask = false,
                        ExpiryDate = expiryDate,
                        IsWikiDraft = wikiStatusTypes,
                        IsWiki = isWikiFlag,
                        SubjectName = subjectName,
                        FilePath = filePath.Replace(_hostingEnvironment.ContentRootPath + @"\", ""),
                        SourceFrom = "FileProfile",
                    };
                    _context.Documents.Add(documents);
                    _context.SaveChanges();
                    if (wikiStatusType == "Draft")
                    {
                        var appWikId = _context.DraftApplicationWiki.FirstOrDefault(f => f.SessionId == sessionID && f.StatusCodeId == 840)?.ApplicationWikiId;
                        if (appWikId > 0)
                        {
                            var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{0}'  Where ApplicationWikiId= {1} and DocumentId='{2}' ", documents.DocumentId, appWikId, documentId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);
                        }
                    }
                    if (wikiStatusType == null || wikiStatusType == "")
                    {
                        if (documentItemIsLatest != null && documentItemIsLatest.IsLatest != true)
                        {
                            //From Upload DMS
                            var appWikiReleaseDocIds = _context.AppWikiReleaseDoc.Where(w => w.DocumentId == documentId).ToList();
                            if (appWikiReleaseDocIds != null && appWikiReleaseDocIds.Count > 0)
                            {
                                var appWikiReleaseDocquerys = string.Format("Update AppWikiReleaseDoc Set DocumentId='{1}' Where AppWikiReleaseDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiReleaseDocIds.Select(s => s.AppWikiReleaseDocId)), documents.DocumentId);
                                _context.Database.ExecuteSqlRaw(appWikiReleaseDocquerys);
                            }

                            var appWikiDraftDocIds = _context.AppWikiDraftDoc.Where(w => w.DocumentId == documentId).ToList();
                            if (appWikiDraftDocIds != null && appWikiDraftDocIds.Count > 0)
                            {
                                var query = string.Format("Update AppWikiDraftDoc Set DocumentId='{1}' Where AppWikiDraftDocId in" + '(' + "{0}" + ')', string.Join(',', appWikiDraftDocIds.Select(s => s.AppWikiDraftDocId)), documents.DocumentId);
                                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                            }
                        }

                    }
                    documentItem.FileIndex = fileIndex;
                    documentItem.IsLatest = false;
                    _context.SaveChanges();
                    newCheckinDocId = documents.DocumentId;
                    if (isFromProfileDocument != null && isFromProfileDocument == true)
                    {
                        var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                        if (doccheck != null)
                        {
                            doccheck.IsMainTask = false;
                        }
                        _context.SaveChanges();

                    }
                    else
                    {
                        var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documents.DocumentId);
                        if (doccheck != null)
                        {
                            doccheck.IsMainTask = null;
                        }
                    }
                    var fileProfileTypeDynamicForm = _context.FileProfileTypeDynamicForm.Where(w => w.DocumentId == documentId).Count();
                    if (fileProfileTypeDynamicForm > 0)
                    {
                        var queryForm = string.Format("Update FileProfileTypeDynamicForm Set DocumentId='{0}' Where DocumentId='{1}'", documents.DocumentId, documentId);
                        _context.Database.ExecuteSqlRaw(queryForm);
                    }
                    idx++;
                }
                if (documentId != null && newCheckinDocId != null)
                {

                    if (isFromProfileDocument != null && isFromProfileDocument == true)
                    {

                        if (task.TaskId != null)
                        {
                            var source = _context.TaskMaster.FirstOrDefault(t => t.TaskId == task.TaskId);
                            if (source != null)
                            {
                                if (source.SourceId == documentId)
                                {
                                    source.SourceId = newCheckinDocId;
                                    var documentlink = _context.DocumentLink.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                                    if (documentlink != null && documentlink.Count > 0)
                                    {
                                        //documentlink.ForEach(d =>
                                        //{
                                        var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                                        var rowaffected = _context.Database.ExecuteSqlRaw(query);


                                    }
                                    var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == documentId);
                                    long? taskId = new long?();
                                    if (doccheck != null)
                                    {
                                        doccheck.IsLatest = false;
                                        doccheck.TaskId = null;
                                        doccheck.IsMainTask = false;
                                    }
                                    var newdoc = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                    if (newdoc != null)
                                    {
                                        newdoc.IsMainTask = true;
                                        newdoc.TaskId = taskId;
                                    }
                                    _context.SaveChanges();

                                }
                                else if (source.SourceId != documentId)
                                {
                                    var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                    if (doccheck != null)
                                    {
                                        doccheck.IsLatest = true;
                                        doccheck.IsMainTask = false;
                                    }
                                }
                                var documentlinklist = _context.DocumentLink.Where(d => d.LinkDocumentId == documentId).AsNoTracking().ToList();
                                var checkin = "Link From Task CheckIn";

                                if (documentlinklist != null && documentlinklist.Count > 0)
                                {
                                    documentlinklist.ForEach(d =>
                                    {
                                        var doclinkexist = _context.DocumentLink.Where(r => r.LinkDocumentId == d.LinkDocumentId)?.FirstOrDefault();
                                        if (doclinkexist != null)
                                        {
                                            var linkparentdocId = doclinkexist.DocumentId;
                                            _context.DocumentLink.Remove(doclinkexist);
                                            _context.SaveChanges();

                                            if (linkparentdocId != newCheckinDocId)
                                            {
                                                var DocumentLink = new DocumentLink
                                                {
                                                    DocumentId = linkparentdocId,
                                                    AddedByUserId = userId,
                                                    AddedDate = DateTime.Now,
                                                    StatusCodeId = 1,
                                                    LinkDocumentId = newCheckinDocId,

                                                    FileProfieTypeId = documentItem.FilterProfileTypeId,
                                                    DocumentPath = "Document Link From Task checkin",

                                                };
                                                _context.DocumentLink.Add(DocumentLink);
                                                _context.SaveChanges();
                                            }
                                        }

                                    });

                                }
                            }
                            var taskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.TaskMasterId == task.TaskId && d.DocumentId == documentId);
                            if (taskAttachment != null)
                            {
                                taskAttachment.IsLatest = false;
                                taskAttachment.IsLocked = true;
                                taskAttachment.LockedDate = DateTime.Now;
                                taskAttachment.LockedByUserId = userId;
                                _context.SaveChanges();

                                var taskAttach = new TaskAttachment
                                {
                                    DocumentId = newCheckinDocId,
                                    IsLatest = true,
                                    IsLocked = false,
                                    IsMajorChange = false,
                                    UploadedDate = DateTime.Now,
                                    UploadedByUserId = userId,
                                    VersionNo = "1",
                                    TaskMasterId = task.TaskId,
                                    CheckInDescription = description,
                                    PreviousDocumentId = taskAttachment.PreviousDocumentId == null ? taskAttachment.TaskAttachmentId : taskAttachment.PreviousDocumentId,

                                };
                                _context.TaskAttachment.Add(taskAttach);
                                _context.SaveChanges();
                                var doccheck = _context.Documents.FirstOrDefault(f => f.DocumentId == newCheckinDocId);
                                var notes = new Notes
                                {
                                    Notes1 = task.Title,
                                    Link = link + "id=" + task.TaskId + "&taskid=" + task.TaskId,
                                    DocumentId = newCheckinDocId,
                                    SessionId = doccheck?.SessionId,
                                    AddedByUserId = userId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = 1,


                                };
                                _context.Notes.Add(notes);
                                _context.SaveChanges();
                            }
                        }




                    }
                    else
                    {
                        var documentlink = _context.DocumentLink.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                        if (documentlink != null && documentlink.Count > 0)
                        {
                            //documentlink.ForEach(d =>
                            //{
                            var query = string.Format("Update DocumentLink Set DocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);


                        }
                        var documentlinkedDoc = _context.DocumentLink.Where(d => d.LinkDocumentId == documentId).AsNoTracking().ToList();
                        if (documentlinkedDoc != null && documentlinkedDoc.Count > 0)
                        {
                            var query = string.Format("Update DocumentLink Set LinkDocumentId='{1}', ModifiedByUserId={3}, ModifiedDate={2:d}  Where LinkDocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);

                        }

                        var notes = _context.Notes.Where(d => d.DocumentId == documentId).AsNoTracking().ToList();
                        if (notes != null && notes.Count > 0)
                        {
                            var query = string.Format("Update Notes Set DocumentId='{1}'  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                            var rowaffected = _context.Database.ExecuteSqlRaw(query);

                        }
                    }

                    var linkFileProfileType = _context.LinkFileProfileTypeDocument.Where(l => l.DocumentId == documentId).FirstOrDefault();
                    if (linkFileProfileType != null)
                    {
                        linkFileProfileType.DocumentId = newCheckinDocId;
                        _context.SaveChanges();
                    }

                }
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload started. It will take sometime to complete.";
                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                if (documentId != null && newCheckinDocId != null)
                {

                    var query = string.Format("Update NotifyDocument Set DocumentId='{1}'  Where DocumentId= {0}", documentId, newCheckinDocId, DateTime.Now, userId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);

                }
                if (documentItem.CloseDocumentId != null && documentItem.CloseDocumentId == 2561)
                {
                    documentItem.CloseDocumentId = null;
                }
                _context.SaveChanges();
                progressInfo = DateTime.Now.ToLongTimeString() + "- File upload completed.";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
                progressInfo = DateTime.Now.ToLongTimeString() + "- Completed";

                await _hub.Clients.Group(userId.ToString()).SendAsync("progress", progressInfo.ToString());
            }
            return Content(sessionID.ToString());
        }
        [HttpPost]
        [Route("InsertFileProfileTypeAccess")]
        public DocumentUserRoleModel PostRole(DocumentUserRoleModel value)
        {
            var userRoles = new DocumentUserRole();
            var existingRole = _context.DocumentUserRole.Where(d => d.FileProfileTypeId == value.FileProfileTypeId && d.DocumentId == null).ToList();
            var userExistingRole = existingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = existingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();
            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            if (value.FileProfileTypeId > 0)
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    if ((userList != null) && (userList.Count > 0))
                    {
                        userList.ForEach(u =>
                        {
                            var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == value.FileProfileTypeId && d.DocumentId == null).FirstOrDefault();
                            if (userExisting == null)
                            {
                                userRoles = new DocumentUserRole
                                {
                                    UserId = u,
                                    RoleId = value.RoleID,
                                    UserGroupId = value.UserGroupID,
                                    FileProfileTypeId = value.FileProfileTypeId,
                                };
                                _context.DocumentUserRole.Add(userRoles);
                                _context.SaveChanges();
                            }

                            if (value.FileProfileTypeIds.Count > 0)
                            {
                                value.FileProfileTypeIds.ForEach(f =>
                                {
                                    var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f && d.DocumentId == null).FirstOrDefault();
                                    if (userExisting == null)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            UserId = u,
                                            RoleId = value.RoleID,
                                            UserGroupId = value.UserGroupID,
                                            FileProfileTypeId = f,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        });
                    }
                }
                else if ((userGroupList != null) && (userGroupList.Count > 0))
                {
                    userGroupList.ForEach(ug =>
                    {
                        List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                        currentGroupUsers.ForEach(c =>
                        {
                            var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == c.UserId).ToList();
                            if (userExist.Count == 0)
                            {
                                userRoles = new DocumentUserRole
                                {
                                    UserId = c.UserId,
                                    RoleId = value.RoleID,
                                    UserGroupId = ug,
                                    FileProfileTypeId = value.FileProfileTypeId,
                                };
                                _context.DocumentUserRole.Add(userRoles);
                                _context.SaveChanges();
                            }
                            if (value.FileProfileTypeIds.Count > 0)
                            {
                                value.FileProfileTypeIds.ForEach(f =>
                                {
                                    var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == f && du.UserId == c.UserId).ToList();
                                    if (userExist.Count == 0)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            UserId = c.UserId,
                                            RoleId = value.RoleID,
                                            UserGroupId = ug,
                                            FileProfileTypeId = f,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }

                                });
                            }
                        });
                    });
                }
                else if ((value.LevelID != null) && (value.LevelID > 0))
                {
                    List<long?> userIds = new List<long?>();
                    var designationList = _context.Designation.Where(d => d.LevelId == value.LevelID).Select(e => e.DesignationId).ToList();
                    userIds = _context.Employee.Where(e => designationList.Contains(e.DesignationId.Value)).Select(e => e.UserId).ToList();

                    if (userIds != null && userIds.Count > 0)
                    {
                        userIds.ForEach(u =>
                        {

                            var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == u && du.DocumentId == null).ToList();
                            if (userExist.Count == 0)
                            {
                                userRoles = new DocumentUserRole
                                {
                                    RoleId = value.RoleID,
                                    LevelId = value.LevelID,
                                    FileProfileTypeId = value.FileProfileTypeId,
                                    IsFolderLevel = value.IsFolderLevel,
                                    UserId = u,
                                };
                                _context.DocumentUserRole.Add(userRoles);
                                _context.SaveChanges();
                            }
                            if (value.FileProfileTypeIds.Count > 0)
                            {
                                value.FileProfileTypeIds.ForEach(f =>
                                {
                                    var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f).FirstOrDefault();
                                    if (userExisting == null)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            UserId = u,
                                            RoleId = value.RoleID,
                                            UserGroupId = value.UserGroupID,
                                            FileProfileTypeId = f,
                                            LevelId = value.LevelID,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        });
                    }

                }
            }
            return value;
        }


        [HttpPost]
        [Route("InsertFileProfileTypeDocumentAccess")]
        public DocumentUserRoleModel InsertFileProfileTypeDocumentAccess(DocumentUserRoleModel value)
        {
            var userRoles = new DocumentUserRole();
            var existingRole = _context.DocumentUserRole.Where(d => d.FileProfileTypeId == value.FileProfileTypeId).ToList();
            var userExistingRole = existingRole.Where(d => value.UserIDs.Contains(d.UserId)).FirstOrDefault();
            var userGroupExistingRole = existingRole.Where(d => value.UserGroupIDs.Contains(d.UserGroupId)).FirstOrDefault();
            var userList = value.UserIDs.Distinct().ToList();
            var docuIds = value.DocumentIds.Distinct().ToList();
            var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
            var userGroupList = value.UserGroupIDs.Distinct().ToList();
            if (value.FileProfileTypeId > 0)
            {
                if ((userList != null) && (userList.Count > 0))
                {
                    if ((userList != null) && (userList.Count > 0))
                    {
                        userList.ForEach(u =>
                        {
                            if (docuIds != null && docuIds.Count > 0)
                            {
                                docuIds.ForEach(t =>
                                {
                                    var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == value.FileProfileTypeId && d.DocumentId == t).FirstOrDefault();
                                    if (userExisting == null)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            UserId = u,
                                            RoleId = value.RoleID,
                                            UserGroupId = value.UserGroupID,
                                            FileProfileTypeId = value.FileProfileTypeId,
                                            DocumentId = t,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }

                                    if (value.FileProfileTypeIds.Count > 0)
                                    {
                                        value.FileProfileTypeIds.ForEach(f =>
                                        {
                                            var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f && d.DocumentId == t).FirstOrDefault();
                                            if (userExisting == null)
                                            {
                                                userRoles = new DocumentUserRole
                                                {
                                                    UserId = u,
                                                    RoleId = value.RoleID,
                                                    UserGroupId = value.UserGroupID,
                                                    FileProfileTypeId = f,
                                                    DocumentId = t,
                                                };
                                                _context.DocumentUserRole.Add(userRoles);
                                                _context.SaveChanges();
                                            }
                                        });
                                    }
                                });
                            }
                            else
                            {
                                var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == value.FileProfileTypeId && d.DocumentId == null).FirstOrDefault();
                                if (userExisting == null)
                                {
                                    userRoles = new DocumentUserRole
                                    {
                                        UserId = u,
                                        RoleId = value.RoleID,
                                        UserGroupId = value.UserGroupID,
                                        FileProfileTypeId = value.FileProfileTypeId,
                                        DocumentId = null,
                                    };
                                    _context.DocumentUserRole.Add(userRoles);
                                    _context.SaveChanges();
                                }

                                if (value.FileProfileTypeIds.Count > 0)
                                {
                                    value.FileProfileTypeIds.ForEach(f =>
                                    {
                                        var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f && d.DocumentId == null).FirstOrDefault();
                                        if (userExisting == null)
                                        {
                                            userRoles = new DocumentUserRole
                                            {
                                                UserId = u,
                                                RoleId = value.RoleID,
                                                UserGroupId = value.UserGroupID,
                                                FileProfileTypeId = f,
                                                DocumentId = null,
                                            };
                                            _context.DocumentUserRole.Add(userRoles);
                                            _context.SaveChanges();
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
                else if ((userGroupList != null) && (userGroupList.Count > 0))
                {
                    userGroupList.ForEach(ug =>
                    {
                        List<UserGroupUser> currentGroupUsers = userGroupUserList.Where(u => u.UserGroupId == ug).ToList();
                        currentGroupUsers.ForEach(c =>
                        {
                            if (docuIds != null && docuIds.Count > 0)
                            {
                                docuIds.ForEach(t =>
                                {
                                    var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == c.UserId && du.DocumentId == t).ToList();
                                    if (userExist.Count == 0)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            UserId = c.UserId,
                                            RoleId = value.RoleID,
                                            UserGroupId = ug,
                                            FileProfileTypeId = value.FileProfileTypeId,
                                            DocumentId = t,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }
                                    if (value.FileProfileTypeIds.Count > 0)
                                    {
                                        value.FileProfileTypeIds.ForEach(f =>
                                        {
                                            var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == f && du.UserId == c.UserId && du.DocumentId == t).ToList();
                                            if (userExist.Count == 0)
                                            {
                                                userRoles = new DocumentUserRole
                                                {
                                                    UserId = c.UserId,
                                                    RoleId = value.RoleID,
                                                    UserGroupId = ug,
                                                    FileProfileTypeId = f,
                                                    DocumentId = t,
                                                };
                                                _context.DocumentUserRole.Add(userRoles);
                                                _context.SaveChanges();
                                            }

                                        });
                                    }
                                });
                            }
                            else
                            {
                                var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                if (userExist.Count == 0)
                                {
                                    userRoles = new DocumentUserRole
                                    {
                                        UserId = c.UserId,
                                        RoleId = value.RoleID,
                                        UserGroupId = ug,
                                        FileProfileTypeId = value.FileProfileTypeId,
                                        DocumentId = null,
                                    };
                                    _context.DocumentUserRole.Add(userRoles);
                                    _context.SaveChanges();
                                }
                                if (value.FileProfileTypeIds.Count > 0)
                                {
                                    value.FileProfileTypeIds.ForEach(f =>
                                    {
                                        var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == f && du.UserId == c.UserId && du.DocumentId == null).ToList();
                                        if (userExist.Count == 0)
                                        {
                                            userRoles = new DocumentUserRole
                                            {
                                                UserId = c.UserId,
                                                RoleId = value.RoleID,
                                                UserGroupId = ug,
                                                FileProfileTypeId = f,
                                                DocumentId = null,
                                            };
                                            _context.DocumentUserRole.Add(userRoles);
                                            _context.SaveChanges();
                                        }

                                    });
                                }
                            }
                        });
                    });
                }
                else if ((value.LevelID != null) && (value.LevelID > 0))
                {
                    List<long?> userIds = new List<long?>();
                    var designationList = _context.Designation.Where(d => d.LevelId == value.LevelID).Select(e => e.DesignationId).ToList();
                    userIds = _context.Employee.Where(e => designationList.Contains(e.DesignationId.Value)).Select(e => e.UserId).ToList();

                    if (userIds != null && userIds.Count > 0)
                    {
                        userIds.ForEach(u =>
                        {
                            if (docuIds != null && docuIds.Count > 0)
                            {
                                docuIds.ForEach(t =>
                                {
                                    var userExist = _context.DocumentUserRole.Where(du => du.FileProfileTypeId == value.FileProfileTypeId && du.UserId == u && du.DocumentId == t).ToList();
                                    if (userExist.Count == 0)
                                    {
                                        userRoles = new DocumentUserRole
                                        {
                                            RoleId = value.RoleID,
                                            LevelId = value.LevelID,
                                            FileProfileTypeId = value.FileProfileTypeId,
                                            IsFolderLevel = value.IsFolderLevel,
                                            UserId = u,
                                            DocumentId = t,
                                        };
                                        _context.DocumentUserRole.Add(userRoles);
                                        _context.SaveChanges();
                                    }
                                    if (value.FileProfileTypeIds.Count > 0)
                                    {
                                        value.FileProfileTypeIds.ForEach(f =>
                                        {
                                            var userExisting = _context.DocumentUserRole.Where(d => d.UserId == u && d.FileProfileTypeId == f && d.DocumentId == t).FirstOrDefault();
                                            if (userExisting == null)
                                            {
                                                userRoles = new DocumentUserRole
                                                {
                                                    UserId = u,
                                                    RoleId = value.RoleID,
                                                    UserGroupId = value.UserGroupID,
                                                    FileProfileTypeId = f,
                                                    LevelId = value.LevelID,
                                                    DocumentId = t,
                                                };
                                                _context.DocumentUserRole.Add(userRoles);
                                                _context.SaveChanges();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                }
            }
            return value;
        }

        [HttpGet]
        [Route("GetFileProfileTypeDetails")]
        public async Task<List<FPModel>> GetFileProfileTypeDetails()
        {
            List<FPModel> fPModels = new List<FPModel>();
            var fileProfileTypes = await _context.FileProfileType.Where(m => m.IsAllowMobileUpload == true).ToListAsync();
            foreach (var item in fileProfileTypes)
            {
                if (item.ParentId == null)
                {
                    fPModels.Add(new FPModel { Id = item.FileProfileTypeId, ProfileId = item.ProfileId, Description = item.Name, Hint = item.Hints });
                }
                else if (item.ParentId > 0)
                {
                    FPModel fPModel = new FPModel();
                    fPModel.Id = item.FileProfileTypeId;
                    fPModel.Description = item.Name;
                    fPModel.Hint = item.Hints;
                    fPModel.ProfileId = item.ProfileId;
                    var parentItem = fileProfileTypes.FirstOrDefault(f => f.FileProfileTypeId == item.ParentId);
                    if (parentItem != null)
                    {
                        BuildFileProfileTypeDetail(fPModel, parentItem, fileProfileTypes);
                        fPModels.Add(fPModel);
                    }
                    else
                    {
                        fPModels.Add(fPModel);
                    }
                }
            }
            return fPModels;
        }

        private void BuildFileProfileTypeDetail(FPModel fPModel, FileProfileType item, List<FileProfileType> fileProfileTypes)
        {
            if (item.ParentId == null)
            {
                fPModel.Description = item.Name + "/" + fPModel.Description;
            }
            else if (item.ParentId > 0)
            {
                fPModel.Description = item.Name + "/" + fPModel.Description;
                var parentItem = fileProfileTypes.FirstOrDefault(f => f.FileProfileTypeId == item.ParentId);
                if (parentItem != null)
                {
                    BuildFileProfileTypeDetail(fPModel, parentItem, fileProfileTypes);
                }
            }
        }

        private void FileProfileTypePath(FileProfileTypePathModel fPModel, FileProfileType item, List<FileProfileType> fileProfileTypes)
        {
            if (item.ParentId == null)
            {
                fPModel.Name = item.Name;
            }
            else if (item.ParentId > 0)
            {
                fPModel.Name = fPModel.Name + "/" + item.Name;
                var parentItem = fileProfileTypes.FirstOrDefault(f => f.FileProfileTypeId == item.ParentId);
                FileProfileTypePath(fPModel, parentItem, fileProfileTypes);
            }
        }
        [HttpGet]
        [Route("GetFileProfileTypeAccess")]
        public List<DocumentUserRoleModel> GetFileProfileTypeAccess(int id)
        {

            var documentUserRoles = _context.DocumentUserRole
                .Include("User")
                .Include("Role")
                .Include("UserGroup")
                .Where(f => f.FileProfileTypeId == id && f.DocumentId == null).AsNoTracking().ToList();
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            List<long> userIds = documentUserRoles.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee
                .Include(d => d.Department)
                .Include(d => d.Designation)
                .Include(d => d.Designation.SubSectionT)
                .Include(d => d.Designation.SubSectionT.SubSecton)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section.Department)
                .Where(e => e.UserId != null && userIds.Distinct().ToList().Contains(e.UserId.Value)).AsNoTracking().ToList();

            List<DocumentUserRoleModel> documentUserRolesModels = new List<DocumentUserRoleModel>();
            userIds.ForEach(u =>
            {
                if (!documentUserRolesModels.Any(r => r.UserID == u))
                {
                    var employee = employees.FirstOrDefault(e => e.UserId == u);
                    var userUserRole = documentUserRoles.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                    var userGroupRole = documentUserRoles.FirstOrDefault(ug => ug.FileProfileTypeId == userUserRole.FileProfileTypeId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                    List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u && ug.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
                    DocumentUserRoleModel documentUserRolesModel = new DocumentUserRoleModel();
                    documentUserRolesModel.DocumentUserRoleID = userUserRole.DocumentUserRoleId;
                    documentUserRolesModel.FileProfileTypeId = userUserRole.FileProfileTypeId;
                    documentUserRolesModel.UserID = userUserRole.UserId;
                    documentUserRolesModel.RoleID = userUserRole.RoleId;
                    documentUserRolesModel.RoleName = userUserRole.Role.DocumentRoleName;
                    documentUserRolesModel.UserName = userUserRole.User.UserName;
                    documentUserRolesModel.NickName = employee.NickName;
                    documentUserRolesModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                    documentUserRolesModel.DepartmentName = employee?.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                    //documentUserRolesModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                    documentUserRolesModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                    documentUserRolesModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                    documentUserRolesModels.Add(documentUserRolesModel);
                }
            });
            documentUserRolesModels = documentUserRolesModels.OrderByDescending(o => o.FileProfileTypeId).Where(s => s.FileProfileTypeId == id).ToList();
            return documentUserRolesModels;

        }

        [HttpPost]
        [Route("GetFileProfileTypeDocumentAccess")]
        public List<DocumentUserRoleModel> GetFileProfileTypeDocumentAccess(DocumentUserRoleModel value)
        {
            List<DocumentUserRole> documentUserRoles = new List<DocumentUserRole>();
            if (value.DocumentIds.Count > 0)
            {
                documentUserRoles = _context.DocumentUserRole
                   .Include("User")
                   .Include("Role")
                   .Include("UserGroup")

                   .Where(f => ((f.FileProfileTypeId == value.FileProfileTypeId && f.DocumentId == null) || (f.FileProfileTypeId == value.FileProfileTypeId && value.DocumentIds.Contains(f.DocumentId)))).AsNoTracking().ToList();
            }
            else if (value.DocumentIds.Count == 0 || value.DocumentIds == null)
            {
                documentUserRoles = _context.DocumentUserRole
                 .Include("User")
                 .Include("Role")
                 .Include("UserGroup")

                 .Where(f => f.FileProfileTypeId == value.FileProfileTypeId && f.DocumentId == null).AsNoTracking().ToList();
            }
            List<UserGroupUser> userGroupUserItems = _context.UserGroupUser.AsNoTracking().ToList();
            List<UserGroup> userGroupItems = _context.UserGroup.AsNoTracking().ToList();
            var documents = _context.Documents.Select(s => new
            {
                DocumentId = s.DocumentId,
                FileName = s.FileName,

            }).ToList();
            List<long> fileProfileUserIds = documentUserRoles.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            List<long> userIds = documentUserRoles.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
            var employees = _context.Employee
                .Include(d => d.Department)
                .Include(d => d.Designation)
                .Include(d => d.Designation.SubSectionT)
                .Include(d => d.Designation.SubSectionT.SubSecton)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section)
                .Include(d => d.Designation.SubSectionT.SubSecton.Section.Department)
                .Where(e => e.UserId != null && (userIds.Distinct().ToList().Contains(e.UserId.Value) || fileProfileUserIds.Distinct().ToList().Contains(e.UserId.Value))).AsNoTracking().ToList();

            List<DocumentUserRoleModel> documentUserRolesModels = new List<DocumentUserRoleModel>();
            if (userIds != null && userIds.Count > 0)
            {
                userIds.ForEach(u =>
                {
                    if (!documentUserRolesModels.Any(r => r.UserID == u))
                    {
                        var employee = employees.FirstOrDefault(e => e.UserId == u);
                        var userUserRole = documentUserRoles.Where(s => s.UserId != null && s.DocumentId != null).FirstOrDefault(r => r.UserId == u);
                        if (userUserRole == null)
                        {
                            userUserRole = documentUserRoles.Where(s => s.UserId != null && s.DocumentId == null).FirstOrDefault(r => r.UserId == u);
                        }
                        var userGroupRole = documentUserRoles.FirstOrDefault(ug => ug.FileProfileTypeId == userUserRole.FileProfileTypeId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                        List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u && ug.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
                        DocumentUserRoleModel documentUserRolesModel = new DocumentUserRoleModel();
                        documentUserRolesModel.DocumentUserRoleID = userUserRole.DocumentUserRoleId;
                        documentUserRolesModel.FileProfileTypeId = userUserRole.FileProfileTypeId;
                        documentUserRolesModel.UserID = userUserRole.UserId;
                        documentUserRolesModel.RoleID = userUserRole.RoleId;
                        documentUserRolesModel.RoleName = userUserRole.Role.DocumentRoleName;
                        documentUserRolesModel.UserName = userUserRole.User.UserName;
                        documentUserRolesModel.NickName = employee.NickName;
                        documentUserRolesModel.DocumentID = userUserRole.DocumentId;
                        documentUserRolesModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                        documentUserRolesModel.DepartmentName = employee?.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                        //documentUserRolesModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                        documentUserRolesModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                        documentUserRolesModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                        if (userUserRole.DocumentId != null)
                        {
                            documentUserRolesModel.FileName = documents?.FirstOrDefault(f => f.DocumentId == userUserRole.DocumentId)?.FileName;
                        }
                        documentUserRolesModels.Add(documentUserRolesModel);
                    }
                });
            }
            else
            {
                userIds = documentUserRoles.Where(u => u.UserId != null).Select(s => s.UserId.Value).ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    userIds.ForEach(u =>
                    {
                        if (!documentUserRolesModels.Any(r => r.UserID == u))
                        {
                            var employee = employees.FirstOrDefault(e => e.UserId == u);
                            var userUserRole = documentUserRoles.Where(s => s.UserId != null).FirstOrDefault(r => r.UserId == u);
                            var userGroupRole = documentUserRoles.FirstOrDefault(ug => ug.FileProfileTypeId == userUserRole.FileProfileTypeId && ug.RoleId == userUserRole.RoleId && ug.UserGroupId != null);
                            List<long> currentUserGroupIds = userGroupUserItems.Where(ug => ug.UserId == u && ug.UserGroupId != null).Select(s => s.UserGroupId.Value).ToList();
                            DocumentUserRoleModel documentUserRolesModel = new DocumentUserRoleModel();
                            documentUserRolesModel.DocumentUserRoleID = userUserRole.DocumentUserRoleId;
                            documentUserRolesModel.FileProfileTypeId = userUserRole.FileProfileTypeId;
                            documentUserRolesModel.UserID = userUserRole.UserId;
                            documentUserRolesModel.RoleID = userUserRole.RoleId;
                            documentUserRolesModel.RoleName = userUserRole.Role.DocumentRoleName;
                            documentUserRolesModel.UserName = userUserRole.User.UserName;
                            documentUserRolesModel.NickName = employee.NickName;
                            documentUserRolesModel.DocumentID = userUserRole.DocumentId;
                            documentUserRolesModel.UserGroupName = userUserRole != null && userUserRole.UserGroupId.HasValue && userUserRole.UserGroupId != null && userGroupItems.Count > 0 ? userGroupItems.FirstOrDefault(s => s.UserGroupId == userUserRole.UserGroupId).Name : string.Empty;
                            documentUserRolesModel.DepartmentName = employee?.Designation?.SubSectionT?.SubSecton?.Section?.Department?.Name;
                            //documentUserRolesModel.DepartmentName = (employee != null && employee.Department != null) ? string.Join(',', employee.Department.Select(d => d.Name)) : string.Empty;
                            documentUserRolesModel.Designation = (employee != null && employee.Designation != null) ? employee.Designation.Name : string.Empty;
                            documentUserRolesModel.UserGroupIDs = new List<long?> { userUserRole.UserId };
                            if (userUserRole.DocumentId != null)
                            {
                                documentUserRolesModel.FileName = documents?.FirstOrDefault(f => f.DocumentId == userUserRole.DocumentId)?.FileName;
                            }
                            documentUserRolesModels.Add(documentUserRolesModel);
                        }
                    });
                }
            }
            documentUserRolesModels = documentUserRolesModels.OrderByDescending(o => o.FileProfileTypeId).Where(s => s.FileProfileTypeId == value.FileProfileTypeId).ToList();
            return documentUserRolesModels;

        }
        [HttpPut]
        [Route("UpdateDocumentUserRoleAccess")]
        public DocumentUserRoleModel UpdateDocumentUserRoleAccess(DocumentUserRoleModel value)
        {
            var itemAccess = _context.DocumentUserRole.Where(i => i.DocumentUserRoleId == value.DocumentUserRoleID).FirstOrDefault();
            if (itemAccess != null)
            {
                itemAccess.DocumentUserRoleId = value.DocumentUserRoleID;
                itemAccess.RoleId = value.RoleID;
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("DeleteDocumentUserRoleAccess")]
        public ItemClassificationAccessModel DeleteDocumentUserRoleAccess(ItemClassificationAccessModel value)
        {

            var selectItemClassificationAccessIDs = value.ItemClassificationAccessIDs;
            if (selectItemClassificationAccessIDs.Count > 0)
            {
                selectItemClassificationAccessIDs.ForEach(sel =>
                {
                    var itemAccess = _context.DocumentUserRole.Where(p => p.DocumentUserRoleId == sel).FirstOrDefault();
                    if (itemAccess != null)
                    {
                        var documentExistingUser = _context.DocumentUserRole.Where(p => p.UserId == itemAccess.UserId && p.DocumentUserRoleId == itemAccess.DocumentUserRoleId).ToList();
                        if (documentExistingUser.Count > 0 && documentExistingUser != null)
                        {
                            _context.DocumentUserRole.RemoveRange(documentExistingUser);
                        }
                    }
                });
                _context.SaveChanges();
            }


            return value;
        }
        [HttpGet]
        [Route("GetlatestDocumentByFileProfileTypeId")]
        public DocumentsModel GetlatestDocumentByFileProfileTypeId(int id)
        {
            DocumentsModel documentsModel = new DocumentsModel();
            var documents = _context.Documents

                .Where(s => s.FilterProfileTypeId == id && s.IsLatest == true).
               Select(s => new
               {
                   s.SessionId,
                   s.DocumentId,
                   s.FileName,
                   s.ContentType,
                   s.UploadDate,
                   s.FilterProfileTypeId,
                   //FilterProfileType = s.FilterProfileType.Name,
                   s.DocumentParentId,
                   s.IsMobileUpload,
                   s.AddedByUserId,
                   s.ModifiedByUserId,
                   s.ModifiedDate,
                   s.IsCompressed,
                   s.AddedDate,

               }).OrderByDescending(o => o.UploadDate).FirstOrDefault();
            if (documents != null)
            {
                documentsModel.DocumentID = documents.DocumentId;
                documentsModel.FilterProfileTypeId = documents.FilterProfileTypeId;
                documentsModel.ModifiedDate = documents.ModifiedDate;
                documentsModel.IsCompressed = documents.IsCompressed;
                documentsModel.ContentType = documents.ContentType;

            }

            return documentsModel;
        }
        [HttpGet]
        [Route("GetFolderDocumentPath")]
        public List<FileProfileTypeModel> GetFolderDocumentPath(long? id)
        {
            List<FileProfileTypeModel> fileProfilelistModels = new List<FileProfileTypeModel>();
            List<FileProfileTypeModel> fileProfileModels = new List<FileProfileTypeModel>();

            var fileProfilelist = _context.FileProfileType.AsNoTracking().ToList();
            var folderIds = fileProfilelist.Select(s => s.FileProfileTypeId).ToList();
            int idx = 0;
            fileProfilelist.OrderBy(f => f.FileProfileTypeId).ToList().ForEach(s =>
            {
                FileProfileTypeModel fileProfileModel = new FileProfileTypeModel();
                fileProfileModel.Id = ++idx;
                if (fileProfileModels.Any(a => a.FileProfileTypeId == s.ParentId))
                {
                    var folder = fileProfileModels.FirstOrDefault(g => g.FileProfileTypeId == s.ParentId);
                    //fileProfileModel.ParentId = folder.Id;
                }
                fileProfileModel.FileProfileTypeId = s.FileProfileTypeId;
                fileProfileModel.ParentId = s.ParentId;
                fileProfileModel.Name = s.Name;
                fileProfileModel.Description = s.Description;
                fileProfileModel.ModifiedByUserID = s.ModifiedByUserId;
                fileProfileModel.AddedByUserID = s.AddedByUserId;
                fileProfileModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                fileProfileModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                fileProfileModel.StatusCodeID = s.StatusCodeId;
                fileProfileModel.AddedDate = s.AddedDate;
                fileProfileModel.ModifiedDate = s.ModifiedDate;
                fileProfileModels.Add(fileProfileModel);
            });
            fileProfilelistModels = GenerateDocumentPath(fileProfileModels, id);

            return fileProfilelistModels;
        }
        private List<FileProfileTypeModel> GenerateDocumentPath(List<FileProfileTypeModel> folderslist, long? id)
        {
            List<FileProfileTypeModel> folderLocation = new List<FileProfileTypeModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in folderslist)
            {
                FileProfileTypeModel f = new FileProfileTypeModel();

                if (fn.ParentId > 0)
                {
                    f.FileProfileTypeId = fn.FileProfileTypeId;
                    f.Name = fn.Name;
                    f.ParentId = fn.ParentId;
                    if (f.ParentId != id)
                    {
                        GenerateLocation(f, fn, folderslist, emptyFolderIds);
                        folderPath += folderPath != "" ? " / " + f.Name : f.Name;
                    }
                    f.FileProfilePath = folderPath;
                    folderLocation.Add(f);

                }
                else if (fn.ParentId == null)
                {
                    folderPath = fn.Name;
                    f.FileProfilePath = folderPath;
                    f.FileProfileTypeId = fn.FileProfileTypeId;
                    f.Name = fn.Name;
                    f.ParentId = fn.ParentId;
                    f.FileProfilePath = folderPath;
                    folderLocation.Add(f);
                }
            }
            return folderLocation;
        }

        private void GenerateLocation(FileProfileTypeModel fileProfileModel, FileProfileTypeModel parentFolderModel, List<FileProfileTypeModel> fileProfileModels, List<long> emptyIds)
        {
            if (!emptyIds.Contains(parentFolderModel.FileProfileTypeId))
            {
                emptyIds.Add(parentFolderModel.FileProfileTypeId);
                parentFolderModel = fileProfileModels.FirstOrDefault(s => s.FileProfileTypeId == parentFolderModel.ParentId);

                if (parentFolderModel != null)
                {
                    fileProfileModel.FileProfilePaths.Add(parentFolderModel.Name);
                    if (parentFolderModel.ParentId != null)
                    {
                        GenerateLocation(fileProfileModel, parentFolderModel, fileProfileModels, emptyIds);
                    }
                }
            }

        }
        [HttpGet]
        [Route("UpdateArchiveDocument")]
        public void UpdateDocument(int id)
        {
            var SessionId = _context.Documents.SingleOrDefault(p => p.DocumentId == id)?.SessionId;
            if (SessionId != null)
            {
                //var documentList = _context.Documents.Where(w => w.SessionId == SessionId).ToList();
                //if (documentList != null)
                //{
                //    documentList.ForEach(s =>
                //    {
                //        s.ArchiveStatusId = null;
                //    });
                //    _context.SaveChanges();
                //}
                var query = string.Format("Update Documents Set ArchiveStatusId = null Where SessionId =" + "'" + "{0}" + "'", SessionId);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to update document");
                }
            }
        }

        [HttpPut]
        [Route("UpdateDocumentRename")]
        public DocumentsModel UpdateDocumentRename(DocumentsModel value)
        {
            var query = string.Format("Update Documents Set FileName = '{1}' Where  DocumentId={0}", value.DocumentID, value.FileName);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateDocumentIsPrint")]
        public DocumentsModel UpdateDocumentIsPrint(DocumentsModel value)
        {
            var query = string.Format("Update Documents Set IsPrint = '{1}' Where  DocumentId={0}", value.DocumentID, value.IsPrint);
            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to update document");
            }
            return value;
        }
        [HttpGet]
        [Route("GetFileProfileTypeMoveDocumentUpdate")]
        public void GetFileProfileTypeMoveDocumentUpdate()
        {

            var fileprofileType = _context.FileProfileType.Select(s => s.FileProfileTypeId).ToList();
            if (fileprofileType != null)
            {
                var documents = _context.Documents.Select(s => new { s.IsLatest, s.DocumentId, s.SessionId, s.FilterProfileTypeId }).Where(w => w.IsLatest == true && w.FilterProfileTypeId != null && fileprofileType.Contains(w.FilterProfileTypeId.Value)).ToList();
                documents.ForEach(d =>
                {
                    //var d1 = _context.Documents.Where(a => a.SessionId == d.SessionId && a.IsLatest != true).ToList();
                    //if (d1 != null)
                    //{
                    //    d1.ForEach(u =>
                    //    {
                    //        u.FilterProfileTypeId = d.FilterProfileTypeId;
                    //    });
                    //}

                    var query = string.Format("Update Documents Set FilterProfileTypeId = {1} Where SessionId =" + "'" + "{0}" + "'and IsLatest != 1", d.SessionId, d.FilterProfileTypeId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to update document");
                    }
                });
                _context.SaveChanges();
            }

        }
        [HttpPost]
        [Route("GetShareDocument")]
        public DocumentShareModel GetShareDocument(DocumentShareModel value)
        {
            var DocumentShare = _context.DocumentShare.FirstOrDefault(w => w.DocumentId == value.DocumentId);
            if (DocumentShare != null)
            {
                if (DocumentShare.IsShareAnyOne == true)
                {
                    value.IsShareAnyOne = DocumentShare.IsShareAnyOne;
                    value.UserIDs = new List<long?>();
                }
                else
                {
                    var userlists = _context.DocumentShareUser.Where(w => w.DocumentShareId == DocumentShare.DocumentShareId && w.UserId != null).Select(s => s.UserId).ToList();
                    value.IsShareAnyOne = false;
                    value.UserIDs = userlists.Count > 0 ? userlists : new List<long?>();
                }
                var DocumentShareDocuments = _context.DocumentShareDocuments.Select(d => new { d.DocumentId, d.DocumentShareId }).Where(w => w.DocumentShareId == DocumentShare.DocumentShareId).ToList();
                var documentIds = DocumentShareDocuments.Select(s => s.DocumentId).ToList();
                var documentsList = _context.Documents.Select(s => new { s.DocumentId, s.FilePath, s.IsLatest, s.SessionId, s.DocumentParentId }).Where(w => documentIds.Contains(w.DocumentId)).ToList();
                List<DocumentsModel> SelectedShareItems = new List<DocumentsModel>();
                DocumentShareDocuments.ForEach(a =>
                {
                    var documentsLists = documentsList.FirstOrDefault(w => w.DocumentId == a.DocumentId);
                    DocumentsModel DocumentsModel = new DocumentsModel();
                    DocumentsModel.DocumentID = documentsLists.DocumentId;
                    DocumentsModel.IsLatest = documentsLists?.IsLatest;
                    DocumentsModel.FilePath = documentsLists?.FilePath;
                    SelectedShareItems.Add(DocumentsModel);
                });
                value.SelectedShareItems = SelectedShareItems;
            }
            return value;
        }
        [HttpGet]
        [Route("GetSharedViewDocumentsByUser")]
        public List<DocumentsModel> GetSharedViewDocumentsByUser(long? id)
        {
            List<DocumentsModel> documentsModel = new List<DocumentsModel>();
            var documentsData = _context.DocumentShareDocuments.Where(w => w.DocumentShareId == id).Select(s => s.DocumentId).ToList();
            var documetsparent = _context.Documents
                    .Where(s => documentsData.Contains(s.DocumentId)).Select(s => new
                    {
                        s.SessionId,
                        s.DocumentId,
                        s.FileName,
                        s.ContentType,
                        s.FileSize,
                        s.UploadDate,
                        s.FilterProfileTypeId,
                        // FilterProfileType = s.FilterProfileType.Name,
                        s.DocumentParentId,
                        s.TableName,
                        s.ExpiryDate,
                        s.AddedByUserId,
                        s.ModifiedByUserId,
                        s.ModifiedDate,
                        //ModifiedByUser = s.ModifiedByUser.UserName,
                        //AddedByUser = s.AddedByUser.UserName,
                        IsLocked = s.IsLocked,
                        LockedByUserId = s.LockedByUserId,
                        LockedDate = s.LockedDate,
                        s.AddedDate,
                        s.IsCompressed,
                        s.FileIndex,
                        s.ProfileNo,
                    }).AsNoTracking().ToList();
            var userIds = documetsparent.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            var fileProfileTypeIds = documetsparent.Select(s => s.FilterProfileTypeId.GetValueOrDefault(0)).ToList();
            var fileProfileType = _context.FileProfileType.Where(f => fileProfileTypeIds.Contains(f.FileProfileTypeId)).AsNoTracking().ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
            documetsparent.ForEach(s =>
            {
                DocumentsModel documentsModelss = new DocumentsModel
                {
                    DocumentID = s.DocumentId,

                    FileName = s.FileName,
                    DocumentName = s.FileName,
                    ContentType = s.ContentType,
                    FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024)),
                    UploadDate = s.UploadDate,
                    SessionID = s.SessionId,
                    FilterProfileTypeId = s.FilterProfileTypeId,
                    FileProfileTypeName = fileProfileType.FirstOrDefault(f => f.FileProfileTypeId == s.FilterProfileTypeId)?.Name,
                    DocumentParentId = s.DocumentParentId,
                    TableName = s.TableName,
                    Type = "Document",
                    AddedDate = s.AddedDate,
                    AddedByUser = appUsers.FirstOrDefault(f => f.UserId == s.AddedByUserId)?.UserName,
                    IsCompressed = s.IsCompressed,
                    FileIndex = s.FileIndex,
                    ProfileNo = s.ProfileNo,
                };
                documentsModel.Add(documentsModelss);
            });
            return documentsModel;
        }
        [HttpGet]
        [Route("GetSharedDocumentsByUser")]
        public List<NotifyDocumentModel> GetSharedDocumentsByUser(long? id)
        {
            List<NotifyDocumentModel> notifyDocumentModels = new List<NotifyDocumentModel>();
            var SharedDocument = _context.DocumentShare.ToList();
            var SharedDocumentData = SharedDocument.ToList();
            var SharedDocumentIds = SharedDocumentData.Select(s => s.DocumentShareId).ToList();
            var DocumentShareUsers = _context.DocumentShareUser.Where(w => SharedDocumentIds.Contains(w.DocumentShareId.Value)).ToList();
            var documentIds = SharedDocumentData.Select(s => s.DocumentId).ToList();
            var documentItems = _context.Documents.Select(e => new
            {
                e.DocumentId,
                e.FileName,
                e.ContentType,
                FileProfileTypeId = e.FilterProfileTypeId,
                e.SessionId,
                e.AddedByUserId,
                e.StatusCodeId,
                e.ModifiedByUserId,
                e.IsLatest
            }).Where(n => documentIds.Contains(n.DocumentId)).ToList();
            var latestDoc = _context.Documents.Select(e => new
            {
                e.DocumentId,
                e.FileName,
                e.ContentType,
                FileProfileTypeId = e.FilterProfileTypeId,
                e.SessionId,
                e.AddedByUserId,
                e.StatusCodeId,
                e.ModifiedByUserId,
                e.IsLatest,
                e.DocumentParentId,
                e.FileIndex,
            }).Where(n => documentIds.Contains(n.DocumentParentId) && n.IsLatest == true).ToList();
            var userIds = SharedDocumentData.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            userIds.AddRange(SharedDocumentData.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var codeIds = SharedDocumentData.Select(s => s.StatusCodeId).ToList();
            var fileProfileTypeIds = documentItems.Select(s => s.FileProfileTypeId.GetValueOrDefault(0)).ToList();
            var fileProfileType = _context.FileProfileType.AsNoTracking().ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();
            SharedDocument.ToList().ForEach(s =>
            {
                var counts = 0;
                if (s.IsShareAnyOne == true)
                {
                    counts = 1;
                }
                else
                {
                    counts = DocumentShareUsers.Where(w => w.UserId == id && w.DocumentShareId == s.DocumentShareId).Count();

                }
                if (counts > 0)
                {
                    NotifyDocumentModel documentModel = new NotifyDocumentModel();
                    var document = documentItems.FirstOrDefault(d => d.DocumentId == s.DocumentId);
                    if (document != null && document.IsLatest == false)
                    {
                        var documents = latestDoc.FirstOrDefault(d => d.DocumentParentId == s.DocumentId);
                        var fileName = documents?.FileName.Split('.');
                        documentModel.DocumentParentId = document?.DocumentId;
                        documentModel.DocumentID = documents?.DocumentId;
                        documentModel.DocumentId = documents?.DocumentId;
                        documentModel.DocumentShareId = s.DocumentShareId;
                        documentModel.DocumentName = documents?.FileIndex > 0 ? fileName[0] + "_V0" + documents?.FileIndex + "." + fileName[1] : documents?.FileName;
                        documentModel.ContentType = documents?.ContentType;
                        documentModel.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == documents?.FileProfileTypeId)?.FirstOrDefault()?.Name;
                        var mainprofileId = GetMainProfileId(fileProfileType, documents?.FileProfileTypeId);
                        documentModel.FileProfileTypeId = documents?.FileProfileTypeId;
                        documentModel.ParentId = mainprofileId?.FirstOrDefault()?.FileProfileTypeId;
                        documentModel.SessionId = documents?.SessionId;
                        documentModel.Type = "Document";
                        documentModel.ModifiedByUser = appUsers != null && documents?.ModifiedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == documents?.ModifiedByUserId)?.UserName : "";
                        documentModel.AddedByUser = appUsers != null && documents?.AddedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == documents?.AddedByUserId)?.UserName : "";
                        documentModel.StatusCode = codeMasters != null && documents?.StatusCodeId != null ? codeMasters.FirstOrDefault(a => a.CodeId == documents?.StatusCodeId)?.CodeValue : "";
                        var isHidden = fileProfileType.Where(t => t.FileProfileTypeId == documents?.FileProfileTypeId)?.Select(s => s.IsHidden).FirstOrDefault();
                        documentModel.IsHidden = true;
                        if (isHidden != null && isHidden == true && id != 47)
                        {
                            documentModel.IsHidden = false;
                        }
                        notifyDocumentModels.Add(documentModel);
                    }
                    else
                    {
                        documentModel.DocumentParentId = document?.DocumentId;
                        documentModel.DocumentID = document?.DocumentId;
                        documentModel.DocumentId = document?.DocumentId;
                        documentModel.DocumentShareId = s.DocumentShareId;
                        documentModel.DocumentName = document?.FileName;
                        documentModel.ContentType = document?.ContentType;
                        documentModel.DocumentPath = fileProfileType.Where(t => t.FileProfileTypeId == document?.FileProfileTypeId)?.FirstOrDefault()?.Name;
                        var mainprofileId = GetMainProfileId(fileProfileType, document?.FileProfileTypeId);
                        documentModel.FileProfileTypeId = document?.FileProfileTypeId;
                        documentModel.ParentId = mainprofileId?.FirstOrDefault()?.FileProfileTypeId;
                        documentModel.SessionId = document?.SessionId;
                        documentModel.Type = "Document";
                        documentModel.ModifiedByUser = appUsers != null && document?.ModifiedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == document?.ModifiedByUserId)?.UserName : "";
                        documentModel.AddedByUser = appUsers != null && document?.AddedByUserId != null ? appUsers.FirstOrDefault(a => a.UserId == document?.AddedByUserId)?.UserName : "";
                        documentModel.StatusCode = codeMasters != null && document?.StatusCodeId != null ? codeMasters.FirstOrDefault(a => a.CodeId == document?.StatusCodeId)?.CodeValue : "";
                        var isHidden = fileProfileType.Where(t => t.FileProfileTypeId == document?.FileProfileTypeId)?.Select(s => s.IsHidden).FirstOrDefault();
                        documentModel.IsHidden = true;
                        if (isHidden != null && isHidden == true && id != 47)
                        {
                            documentModel.IsHidden = false;
                        }
                        notifyDocumentModels.Add(documentModel);
                    }
                }
            });
            return notifyDocumentModels.Where(w => w.DocumentId > 0).ToList();
        }
        private List<FileProfileType> GetMainProfileId(List<FileProfileType> fileprofile, long? mainProfileId)
        {
            var fileprofileList = fileprofile;
            fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList().ForEach(s =>
            {
                if (s.ParentId != null)
                {
                    fileprofileList = GetMainProfileId(fileprofileList, s.ParentId);
                }
                else
                {
                    fileprofileList = fileprofile.Where(w => w.FileProfileTypeId == mainProfileId).ToList();
                }
            });
            return fileprofileList;
        }
        [HttpPost]
        [Route("SaveShareDocument")]
        public DocumentShareModel SaveShareDocument(DocumentShareModel value)
        {

            if (value.DocumentId != null)
            {
                var DocumentShares = _context.DocumentShare.FirstOrDefault(w => w.DocumentId == value.DocumentId);
                if (DocumentShares != null)
                {
                    var DocumentShareDocumentquery = string.Format("Delete from DocumentShareDocuments Where DocumentShareId = " + DocumentShares.DocumentShareId);
                    _context.Database.ExecuteSqlRaw(DocumentShareDocumentquery);
                    var DocumentShareUserquery = string.Format("Delete from DocumentShareUser Where DocumentShareId = " + DocumentShares.DocumentShareId);
                    _context.Database.ExecuteSqlRaw(DocumentShareUserquery);
                    var query = string.Format("Delete from DocumentShare Where DocumentShareId = '" + DocumentShares.DocumentShareId + "'");
                    _context.Database.ExecuteSqlRaw(query);
                }
            }
            if (value.UserIDs.Count > 0 || value.IsShareAnyOne == true)
            {
                var DocumentShare = new DocumentShare
                {
                    IsShareAnyOne = value.UserIDs.Count > 0 ? false : true,
                    StatusCodeId = value.StatusCodeID,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    DocumentId = value.DocumentId,
                };
                _context.DocumentShare.Add(DocumentShare);
                _context.SaveChanges();
                if (value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(s =>
                    {
                        var DocumentShareUser = new DocumentShareUser
                        {
                            UserId = s,
                            DocumentShareId = DocumentShare.DocumentShareId,
                        };
                        _context.DocumentShareUser.Add(DocumentShareUser);
                    });
                }
                if (value.SelectedShareItems != null && value.SelectedShareItems.Count > 0)
                {
                    value.SelectedShareItems.ForEach(a =>
                    {
                        var DocumentShareDocuments = new DocumentShareDocuments
                        {
                            DocumentId = a.DocumentID,
                            DocumentShareId = DocumentShare.DocumentShareId,
                        };
                        _context.DocumentShareDocuments.Add(DocumentShareDocuments);
                    });
                }
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPost]
        [Route("GetSetAccessByUser")]
        public FileProfileTypeSetAccessModel GetShareGetSetAccessByUserDocument(FileProfileTypeSetAccessModel value)
        {
            FileProfileTypeSetAccessModel FileProfileTypeSetAccessModel = new FileProfileTypeSetAccessModel();
            var setAccessData = _context.FileProfileTypeSetAccess.Where(w => w.DocumentId == value.DocumentId && w.AddedByUserId == value.AddedByUserId).ToList();
            if (setAccessData != null)
            {
                value.UserIDs = setAccessData.Select(s => s.UserId).ToList();
            }
            return value;
        }
        [HttpPost]
        [Route("InsertSetAccessByUser")]
        public FileProfileTypeSetAccessModel InsertSetAccessByUser(FileProfileTypeSetAccessModel value)
        {

            if (value.DocumentId != null)
            {
                var Query = string.Format("Delete from FileProfileTypeSetAccess Where DocumentId = " + value.DocumentId + " and AddedByUserID=" + value.AddedByUserId);
                _context.Database.ExecuteSqlRaw(Query);
            }
            if (value.UserIDs.Count > 0)
            {
                if (value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(s =>
                    {
                        var inserData = new FileProfileTypeSetAccess
                        {
                            UserId = s,
                            AddedByUserId = value.AddedByUserId,
                            AddedDate = DateTime.Now,
                            DocumentId = value.DocumentId
                        };
                        _context.FileProfileTypeSetAccess.Add(inserData);
                    });
                }
                _context.SaveChanges();
            }
            return value;
        }

        [HttpGet]
        [Route("GetFileProfilePathAll")]
        public List<FileProfileTypeModel> getFileProfilePathAll()
        {

            var foldersListItems = _context.FileProfileType.Select(s => new FileProfileType
            {
                FileProfileTypeId = s.FileProfileTypeId,
                ParentId = s.ParentId,
                Name = s.Name,
                Description = s.Description,
                AddedByUserId = s.AddedByUserId,
                StatusCodeId = s.StatusCodeId,
                ProfileId = s.ProfileId,

            }).AsNoTracking().ToList();
            List<FileProfileTypeModel> foldersList = new List<FileProfileTypeModel>();
            List<FileProfileTypeModel> folderLocation = new List<FileProfileTypeModel>();
            var emptyFolderIds = new List<long>();
            string folderPath = "";
            foreach (var fn in foldersListItems)
            {
                FileProfileTypeModel f = new FileProfileTypeModel();


                f.FileProfileTypeId = fn.FileProfileTypeId;
                f.Name = fn.Name;
                f.ParentId = fn.ParentId;
                f.ProfileId = fn.ProfileId;
                List<string> FolderPathLists = new List<string>();
                FolderPathLists = GetAllProfileName(foldersListItems, fn, FolderPathLists);
                //f.ParentName = foldersListItems.FirstOrDefault(t => t.FileProfileTypeId == fn.ParentId)?.Name + "/" + f.Name;
                FolderPathLists.Add(fn.Name);
                f.FileProfilePath = string.Join(" / ", FolderPathLists);
                //folderPath += folderPath != "" ? "/" + f.Name : f.Name;
                //f.FileProfilePath = folderPath;

                folderLocation.Add(f);



            }
            if (foldersListItems != null && foldersListItems.Count > 0)
            {
                //var folderIds = foldersListItems.Select(s => s.FileProfileTypeId).ToList();
                //var documentUserRole = _context.DocumentUserRole.Where(w => w.UserId == id && folderIds.Contains(w.FolderId.Value)).ToList();
                //var roleIds = documentUserRole.Select(s => s.RoleId.GetValueOrDefault(0)).ToList();
                //var documentPermission = _context.DocumentPermission.Select(s => new { s.DocumentRoleId, s.IsRead }).Where(w => roleIds.Contains(w.DocumentRoleId.Value)).ToList();
                foldersListItems.ForEach(s =>
                {
                    //bool? isRead = true;
                    //var documentUserRoleId = documentUserRole.FirstOrDefault(w => w.UserId == id && w.FolderId == s.FileProfileTypeId)?.RoleId;
                    //var isReads = documentPermission.FirstOrDefault(f => f.DocumentRoleId == documentUserRoleId)?.IsRead;
                    //if (s.AddedByUserId != id)
                    //{
                    //    isRead = false;
                    //}
                    //if (isReads == true)
                    //{
                    //    isRead = true;
                    //}
                    //if (isRead == true)
                    //{
                    List<string> FolderPathLists = new List<string>();
                    FileProfileTypeModel foldersModel = new FileProfileTypeModel();
                    foldersModel.FileProfileTypeId = s.FileProfileTypeId;
                    foldersModel.ParentId = s.ParentId;

                    foldersModel.Name = s.Name;
                    foldersModel.Description = s.Description;
                    foldersModel.StatusCodeID = s.StatusCodeId;
                    foldersModel.AddedByUserID = s.AddedByUserId;
                    foldersModel.FileProfilePath = folderLocation?.FirstOrDefault(f => f.FileProfileTypeId == s.FileProfileTypeId)?.FileProfilePath;
                    foldersModel.ProfileId = s.ProfileId;
                    //FolderPathLists.Add(s.Name);
                    //foldersModel.FolderPath = string.Join(" / ", FolderPathLists);
                    foldersList.Add(foldersModel);
                    //}
                });
            }
            return foldersList;
        }
        private List<string> GetAllProfileName(List<FileProfileType> foldersListItems, FileProfileType s, List<string> path)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                GetAllProfileName(foldersListItems, items, path);
                path.Add(items.Name);
            }
            return path;
        }
        [HttpPost]
        [Route("GetFileProfileTypeUpdateArchiveDocument")]
        public SearchModel GetFileProfileTypeUpdateArchiveDocument(SearchModel SearchModel)
        {
            if (SearchModel.Ids.Count > 0)
            {
                var query = string.Format("Update Documents Set ArchiveStatusId = {1} Where DocumentId in" + '(' + "{0}" + ')', string.Join(',', SearchModel.Ids), 2562);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to update document");
                }
                _context.SaveChanges();
            }
            return SearchModel;
        }
        [HttpPost]
        [Route("GetDocumentDirectoryExcel")]
        public async Task<List<DocumentsModel>> GetDocumentDirectory(SearchModel searchModel)
        {

            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_DocumentDirectory";
            var result = _context.Set<ViewDocumentDirectory>().FromSqlRaw(sqlQuery).AsQueryable();


            List<long> folderdocIds = new List<long>();
            List<ViewDocumentDirectory> qresult = new List<ViewDocumentDirectory>();
            var documentFolder = new List<DocumentFolder>();
            List<long> selectdocIds = new List<long>();
            if (searchModel.Action == "Public Folder")
            {
                selectdocIds = _context.DocumentFolder.Where(w => w.FolderId > 0 && w.IsLatest == true).Select(s => s.DocumentId.Value).ToList();
                List<long?> existedfolderIds = new List<long?>();
                if (searchModel.FolderId != null)
                {

                    if (searchModel.FolderId != null)
                    {
                        selectdocIds = new List<long>();
                        selectdocIds = _context.DocumentFolder.Where(w => w.FolderId == searchModel.FolderId).Select(s => s.DocumentId.Value).Distinct().ToList();
                    }
                }
                if (searchModel.FileName != null && searchModel.FileName != "")
                {

                    selectdocIds = result.Where(f => f.FileName.Contains(searchModel.FileName) && selectdocIds.Contains(f.DocumentId)).Select(s => s.DocumentId).ToList();
                }
                if (!string.IsNullOrEmpty(searchModel.ContentType))
                {
                    selectdocIds = result.Where(f => f.ContentType == searchModel.ContentType && selectdocIds.Contains(f.DocumentId)).Select(s => s.DocumentId).ToList();
                }

                if (searchModel.FromMonth != null && searchModel.ToMonth != null)
                {

                    selectdocIds = result.Where(f => (f.UploadDate.Value.Month >= searchModel.FromMonth.Value.Month && f.UploadDate.Value.Year >= searchModel.FromMonth.Value.Year) && (f.UploadDate.Value.Month <= searchModel.ToMonth.Value.Month && f.UploadDate.Value.Year <= searchModel.ToMonth.Value.Year) && selectdocIds.Contains(f.DocumentId)).Select(s => s.DocumentId).ToList();
                }

                if (qresult != null && qresult.Count > 0)
                {
                    selectdocIds = qresult.Select(s => s.DocumentId).ToList();
                }
                folderdocIds = selectdocIds;

            }
            if (searchModel.Action == "File Profile")
            {


                result = result.Where(f => f.IsLatest == true && f.FilterProfileTypeId != null);
                if (searchModel.FileProfileTypeId != null)
                {
                    result = result.Where(f => f.FilterProfileTypeId == searchModel.FileProfileTypeId);
                }
                if (searchModel.FileName != null && searchModel.FileName != "")
                {
                    result = result.Where(f => f.FileName.Contains(searchModel.FileName));
                }
                if (!string.IsNullOrEmpty(searchModel.ContentType))
                {
                    result = result.Where(f => f.ContentType == searchModel.ContentType);
                }

                if (searchModel.FromMonth != null && searchModel.ToMonth != null)
                {
                    result = result.Where(f => (f.UploadDate.Value.Month >= searchModel.FromMonth.Value.Month && f.UploadDate.Value.Year >= searchModel.FromMonth.Value.Year) && (f.UploadDate.Value.Month <= searchModel.ToMonth.Value.Month && f.UploadDate.Value.Year <= searchModel.ToMonth.Value.Year));

                }
                folderdocIds = result.Select(d => d.DocumentId).ToList();
            }

            var setAccess = _context.FileProfileTypeSetAccess.Select(s => new { s.DocumentId, s.UserId }).Where(w => w.UserId != searchModel.UserID).ToList();
            var setAccessDocIds = setAccess.Select(s => s.DocumentId.GetValueOrDefault(0)).Distinct().ToList();

            if (setAccessDocIds.Count > 0)
            {
                var documentsModelData = result.Select(s => new { s.DocumentId, s.AddedByUserId }).Where(r => folderdocIds.Contains(r.DocumentId)).ToList();
                setAccessDocIds.ForEach(d =>
                {
                    var exits = documentsModelData.Where(w => w.DocumentId == d && (w.AddedByUserId == searchModel.UserID || w.AddedByUserId == null)).Count();
                    if (exits > 0)
                    {
                        setAccessDocIds = setAccessDocIds.Where(w => w != d).ToList();
                    }
                });
                if (setAccessDocIds != null && setAccessDocIds.Count > 0)
                {
                    setAccessDocIds.ForEach(f =>
                    {
                        folderdocIds.Remove(f);
                    });
                }

            }
            int totalCount = 0;
            var whereCondition = "";
            List<string> filterFields = new List<string>();
            filterFields = searchModel.FilterFields;
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });



            /*if (folderdocIds != null && folderdocIds.Count > 0 && (searchModel.SearchString == null || searchModel.SearchString == ""))
            {

                totalCount = folderdocIds.Count;
                var skip = (searchModel.PageCount - 1) * searchModel.PageSize;

                folderdocIds = folderdocIds.Skip(skip).Take(searchModel.PageSize).ToList();
                result = result.Where(r => folderdocIds.Contains(r.DocumentId));
                int page = folderdocIds.Count / searchModel.PageSize;
                if (page <= 1)
                {
                    searchModel.PageCount = 1;
                }

            }*/

            var query = await result
                 .Select(s =>
            new
            {

                s.DocumentId,
                s.SessionId,
                s.FileName,
                s.ContentType,
                s.FileSize,
                s.UploadDate,
                s.FilterProfileTypeId,
                s.ProfileNo,
                s.DocumentParentId,
                s.TableName,
                s.ExpiryDate,
                s.AddedDate,
                s.AddedByUserId,
                s.ModifiedByUserId,
                s.ModifiedDate,
                s.AddedByUser,
                s.ModifiedByUser,
                IsLocked = s.IsLocked,
                LockedByUserId = s.LockedByUserId,
                LockedDate = s.LockedDate,
                s.DepartmentId,
                s.WikiId,
                s.Extension,
                s.CategoryId,
                s.DocumentType,
                s.DisplayName,
                s.LinkId,
                s.IsSpecialFile,
                s.IsTemp,
                s.ReferenceNumber,
                s.Description,
                s.StatusCodeId,
                s.IsCompressed,
                s.IsVideoFile,
                s.CloseDocumentId,
                s.IsLatest,
                s.IsPublichFolder,
                s.FolderId,
                s.CloseStatus,
                s.FolderDocumentUser,
            }).ToListAsync();


            List<DocumentsModel> documentsModels = new List<DocumentsModel>();
            var foldersListItems = _context.Folders.Where(s => s.StatusCodeId == 1).AsNoTracking().ToList();
            var fileProfileTypeItems = _context.FileProfileType.AsNoTracking().ToList();
            documentFolder = _context.DocumentFolder.Where(w => w.FolderId > 0 && w.IsLatest == true).ToList();
            var UserIds = query.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            UserIds.AddRange(query.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => UserIds.Contains(w.UserId)).AsNoTracking().ToList();
            var fileProfileTypeIds = query.Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => (f.FileProfileTypeId != null && fileProfileTypeIds.Contains(f.FileProfileTypeId.Value)) || f.FolderId != null).AsNoTracking().ToList();
            query.ToList().ForEach(s =>
            {
                List<string> FolderPathLists = new List<string>();
                string DocumentPath = "";
                if (searchModel.Action == "Public Folder")
                {
                    var folderId = documentFolder.Where(w => w.DocumentId == s.DocumentId && w.FolderId > 0).FirstOrDefault()?.FolderId;
                    var folderData = foldersListItems.Where(w => w.FolderId == folderId).FirstOrDefault();

                    if (folderData != null)
                    {
                        var selectFolders = foldersListItems;
                        if (folderData.ParentFolderId != null)
                        {
                            selectFolders = foldersListItems.Where(d => d.FolderId == folderId || d.MainFolderId == folderData.MainFolderId || d.FolderId == folderData.ParentFolderId || d.ParentFolderId == folderData.ParentFolderId).ToList();
                        }
                        FolderPathLists = GetAllFolderName(selectFolders, folderData, FolderPathLists);
                        FolderPathLists.Add(folderData.Name);
                        DocumentPath = string.Join(" / ", FolderPathLists);
                    }
                }
                if (searchModel.Action == "File Profile")
                {
                    var fileProfileTypeData = fileProfileTypeItems.Where(w => w.FileProfileTypeId == s.FilterProfileTypeId).FirstOrDefault();
                    if (fileProfileTypeData != null)
                    {
                        FolderPathLists = GetAllFileProfileTypeName(fileProfileTypeItems, fileProfileTypeData, FolderPathLists);
                        FolderPathLists.Add(fileProfileTypeData.Name);
                        DocumentPath = string.Join(" / ", FolderPathLists);
                    }
                }
                DocumentsModel documentsModel = new DocumentsModel();
                documentsModel.DocumentPermissionData = new DocumentPermissionModel();
                documentsModel.DocumentID = s.DocumentId;

                documentsModel.FileName = s.FileName;

                documentsModel.ContentType = s.ContentType;

                documentsModel.DocumentType = s.DocumentType;
                documentsModel.FileSize = (long)Math.Round(Convert.ToDouble(s.FileSize / 1024));
                documentsModel.UploadDate = s.UploadDate;
                documentsModel.DisplayName = s.DisplayName;

                documentsModel.IsCompressed = s.IsCompressed;

                documentsModel.Description = s.Description;
                documentsModel.ModifiedByUserID = s.ModifiedByUserId;
                documentsModel.AddedByUserID = s.AddedByUserId;
                documentsModel.FilterProfileTypeId = s.FilterProfileTypeId;
                documentsModel.DocumentParentId = s.DocumentParentId;
                documentsModel.AddedByUser = s.AddedByUser;
                documentsModel.ModifiedByUser = s.ModifiedByUser;
                documentsModel.AddedDate = s.AddedDate;
                if (s.IsPublichFolder == true && s.FolderId > 0)
                {
                    documentsModel.AddedByUser = s.FolderDocumentUser;
                }
                documentsModel.StatusCodeID = s.StatusCodeId;
                documentsModel.StatusCode = "Active";
                documentsModel.ModifiedDate = s.ModifiedDate;
                documentsModel.IsVideoFile = s.IsVideoFile;
                documentsModel.ProfileNo = s.ProfileNo;
                documentsModel.FileProfileTypeName = DocumentPath;
                documentsModel.Type = "Document";
                documentsModel.IsPublichFolder = s.IsPublichFolder;
                documentsModel.FolderId = s.FolderId;
                documentsModel.CloseDocumentId = s.CloseDocumentId;
                documentsModel.IsLocked = s.IsLocked;
                documentsModel.CloseStatus = s.CloseStatus;
                /* long? roleId = 0;
                 if (searchModel.Action == "File Profile")
                 {
                     roleId = roleItems.FirstOrDefault(f => f.FileProfileTypeId == s.FilterProfileTypeId && f.UserId == searchModel.UserID)?.RoleId;
                 }
                 if (searchModel.Action == "Public Folder")
                 {
                     roleId = roleItems.FirstOrDefault(f => f.FolderId != null && f.FolderId == s.FolderId && f.UserId == searchModel.UserID)?.RoleId;
                 }
                 if (roleId != null)
                 {
                     var documentPermission = _context.DocumentPermission.FirstOrDefault(r => r.DocumentRoleId == roleId);
                     if (documentPermission != null)
                     {
                         documentsModel.DocumentPermissionData.IsRead = documentPermission.IsRead;
                         documentsModel.DocumentPermissionData.IsCreateFolder = documentPermission.IsCreateFolder;
                         documentsModel.DocumentPermissionData.IsCreateDocument = documentPermission.IsCreateDocument.GetValueOrDefault(false);
                         documentsModel.DocumentPermissionData.IsSetAlert = documentPermission.IsSetAlert;
                         documentsModel.DocumentPermissionData.IsEditIndex = documentPermission.IsEditIndex;
                         documentsModel.DocumentPermissionData.IsRename = documentPermission.IsRename;
                         documentsModel.DocumentPermissionData.IsUpdateDocument = documentPermission.IsUpdateDocument;
                         documentsModel.DocumentPermissionData.IsCopy = documentPermission.IsCopy;
                         documentsModel.DocumentPermissionData.IsMove = documentPermission.IsMove;
                         documentsModel.DocumentPermissionData.IsDelete = documentPermission.IsDelete;
                         documentsModel.DocumentPermissionData.IsRelationship = documentPermission.IsRelationship;
                         documentsModel.DocumentPermissionData.IsListVersion = documentPermission.IsListVersion;
                         documentsModel.DocumentPermissionData.IsInvitation = documentPermission.IsInvitation;
                         documentsModel.DocumentPermissionData.IsSendEmail = documentPermission.IsSendEmail;
                         documentsModel.DocumentPermissionData.IsDiscussion = documentPermission.IsDiscussion;
                         documentsModel.DocumentPermissionData.IsAccessControl = documentPermission.IsAccessControl;
                         documentsModel.DocumentPermissionData.IsAuditTrail = documentPermission.IsAuditTrail;
                         documentsModel.DocumentPermissionData.IsRequired = documentPermission.IsRequired;
                         documentsModel.DocumentPermissionData.IsEdit = documentPermission.IsEdit;
                         documentsModel.DocumentPermissionData.IsFileDelete = documentPermission.IsFileDelete;
                         documentsModel.DocumentPermissionData.IsGrantAdminPermission = documentPermission.IsGrantAdminPermission;
                         documentsModel.DocumentPermissionData.IsDocumentAccess = documentPermission.IsDocumentAccess;
                         documentsModel.DocumentPermissionData.IsCreateTask = documentPermission.IsCreateTask;
                         documentsModel.DocumentPermissionData.IsEnableProfileTypeInfo = documentPermission.IsEnableProfileTypeInfo;

                     }
                     else
                     {
                         documentsModel.DocumentPermissionData.IsCreateDocument = false;
                         documentsModel.DocumentPermissionData.IsDocumentAccess = false;
                         documentsModel.DocumentPermissionData.IsRead = false;
                         documentsModel.DocumentPermissionData.IsDelete = false;
                         documentsModel.DocumentPermissionData.IsUpdateDocument = false;
                     }
                 }
                 else
                 {
                     documentsModel.DocumentPermissionData.IsCreateDocument = false;
                     documentsModel.DocumentPermissionData.IsDocumentAccess = false;
                     documentsModel.DocumentPermissionData.IsRead = true;
                     documentsModel.DocumentPermissionData.IsDelete = true;
                     documentsModel.DocumentPermissionData.IsUpdateDocument = true;
                 }*/
                documentsModel.SessionId = s.SessionId;
                documentsModel.SessionID = s.SessionId;
                documentsModels.Add(documentsModel);

            });
            return documentsModels;

        }
        private List<string> GetAllFolderName(List<Folders> foldersListItems, Folders s, List<string> foldersModel)
        {
            if (s.ParentFolderId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderId == s.ParentFolderId);
                if (items != null)
                {
                    GetAllFolderName(foldersListItems, items, foldersModel);
                    foldersModel.Add(items.Name);
                }
            }
            return foldersModel;
        }
        private FileProfileTypeBaseModel GetAllFolderNameAll(List<FileProfileTypeBaseModel> foldersListItems, FileProfileTypeBaseModel s, FileProfileTypeBaseModel foldersModel)
        {
            if (s.ParentId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FileProfileTypeId == s.ParentId);
                if (items != null)
                {
                    GetAllFolderNameAll(foldersListItems, items, foldersModel);
                    foldersModel = items;
                }
            }
            return foldersModel;
        }
    }
}