﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionFlavourController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ProductionFlavourController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetProductionFlavours")]
        public List<ProductionFlavourModel> Get()
        {
            List<ProductionFlavourModel> productionFlavourModels = new List<ProductionFlavourModel>();
            var productionFlavour = _context.ProductionFlavour.Include("AddedByUser").Include("ModifiedByUser").Include("ProcessUseSpec").Include("StatusCode").OrderByDescending(o => o.ProductionFlavourId).AsNoTracking().ToList();
            if (productionFlavour != null && productionFlavour.Count > 0)
            {
                List<long?> masterIds = productionFlavour.Where(w => w.FlavourId != null).Select(a => a.FlavourId).Distinct().ToList();
                masterIds.AddRange(productionFlavour.Where(w => w.SupplierId != null).Select(a => a.SupplierId).Distinct().ToList());
                masterIds.AddRange(productionFlavour.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionFlavour.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionFlavour.ForEach(s =>
                {
                    ProductionFlavourModel productionFlavourModel = new ProductionFlavourModel
                    {
                        ProductionFlavourID = s.ProductionFlavourId,
                        FlavourID = s.FlavourId,
                        Flavour = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.FlavourId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.FlavourId).Value : "",
                        ItemNoID = s.ItemNoId,
                        SupplierID = s.SupplierId,
                        Supplier = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SupplierId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SupplierId).Value : "",
                        SupplierCode = s.SupplierCode,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec != null ? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",


                    };
                    productionFlavourModels.Add(productionFlavourModel);
                });
            }
            return productionFlavourModels;
        }

        [HttpPost]
        [Route("GetProductionFlavoursByRefNo")]
        public List<ProductionFlavourModel> GetProductionFlavoursByRefNo(RefSearchModel refSearchModel)
        {
            List<ProductionFlavourModel> productionFlavourModels = new List<ProductionFlavourModel>();
            List<ProductionFlavour> productionFlavour = new List<ProductionFlavour>();
            if (refSearchModel.IsHeader)
            {
                productionFlavour = _context.ProductionFlavour.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").Include("ProcessUseSpec").Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionFlavourId).AsNoTracking().ToList();
            }
            else
            {
                productionFlavour = _context.ProductionFlavour.Include("AddedByUser").Include("ModifiedByUser").Include("StatusCode").Include("ProcessUseSpec").Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.ProductionFlavourId).AsNoTracking().ToList();

            }
            if (productionFlavour != null && productionFlavour.Count > 0)
            {

                List<long?> masterIds = productionFlavour.Where(w => w.FlavourId != null).Select(a => a.FlavourId).Distinct().ToList();
                masterIds.AddRange(productionFlavour.Where(w => w.SupplierId != null).Select(a => a.SupplierId).Distinct().ToList());
                masterIds.AddRange(productionFlavour.Where(w => w.PurposeId != null).Select(a => a.PurposeId).Distinct().ToList());
                masterIds.AddRange(productionFlavour.Where(w => w.RndpurposeId != null).Select(a => a.RndpurposeId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                productionFlavour.ForEach(s =>
                {
                    ProductionFlavourModel productionFlavourModel = new ProductionFlavourModel
                    {
                        ProductionFlavourID = s.ProductionFlavourId,
                        FlavourID = s.FlavourId,
                        Flavour = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.FlavourId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.FlavourId).Value : "",
                        ItemNoID = s.ItemNoId,
                        SupplierID = s.SupplierId,
                        Supplier = applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SupplierId) != null ? applicationmasterdetail.FirstOrDefault(f => f.ApplicationMasterDetailId == s.SupplierId).Value : "",
                        SupplierCode = s.SupplierCode,
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileReferenceNo = s.ProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkProfileReferenceNo,
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        IsSameAsMaster = s.IsSameAsMaster,
                        MaterialName = s.MaterialName,
                        SpecNo = s.SpecNo,
                        SpecNoID = s.ProductionFlavourId,
                        SpecificationNo = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()),
                        SpecNos = "Spec" + " " + ((s.SpecNo > 10) ? s.SpecNo.ToString() : "0" + s.SpecNo.ToString()) + " " + "|" + " " + s.MaterialName,
                        PurposeId = s.PurposeId,
                        ProcessUseSpecId = s.ProcessUseSpecId,
                        RndpurposeId = s.RndpurposeId,
                        PurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        RndpurposeName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.RndpurposeId).Select(a => a.Value).SingleOrDefault() : "",
                        StudyLink = s.StudyLink,
                        ProcessUseSpecName = s.ProcessUseSpec != null ? "Spec" + " " + ((s.ProcessUseSpec.SpecNo > 10) ? s.ProcessUseSpec.SpecNo.ToString() : "0" + s.ProcessUseSpec.SpecNo.ToString()) + " " + "|" + " " + s.ProcessUseSpec.MaterialName : "",
                    };
                    productionFlavourModels.Add(productionFlavourModel);
                });
            }

            return productionFlavourModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionFlavourModel> GetData(SearchModel searchModel)
        {
            var productionFlavour = new ProductionFlavour();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).FirstOrDefault();
                        break;
                    case "Last":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).LastOrDefault();
                        break;
                    case "Next":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).LastOrDefault();
                        break;
                    case "Previous":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).FirstOrDefault();
                        break;
                    case "Last":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).LastOrDefault();
                        break;
                    case "Next":
                        productionFlavour = _context.ProductionFlavour.OrderBy(o => o.ProductionFlavourId).FirstOrDefault(s => s.ProductionFlavourId > searchModel.Id);
                        break;
                    case "Previous":
                        productionFlavour = _context.ProductionFlavour.OrderByDescending(o => o.ProductionFlavourId).FirstOrDefault(s => s.ProductionFlavourId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionFlavourModel>(productionFlavour);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionFlavour")]
        public ProductionFlavourModel Post(ProductionFlavourModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.MaterialName });
            var productionFlavourCount = _context.ProductionFlavour.Where(p => p.LinkProfileReferenceNo == value.LinkProfileReferenceNo).Count();

            var productionFlavour = new ProductionFlavour
            {
                FlavourId = value.FlavourID,
                ItemNoId = value.ItemNoID,
                SupplierId = value.SupplierID,
                SupplierCode = value.SupplierCode,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                IsSameAsMaster = value.IsSameAsMaster,
                MaterialName = value.MaterialName,
                SpecNo = productionFlavourCount + 1,
                PurposeId = value.PurposeId,
                StudyLink = value.StudyLink,
                ProcessUseSpecId = value.ProcessUseSpecId,
                RndpurposeId = value.RndpurposeId,
            };
            _context.ProductionFlavour.Add(productionFlavour);
            _context.SaveChanges();
            value.ProductionFlavourID = productionFlavour.ProductionFlavourId;
            value.MasterProfileReferenceNo = productionFlavour.MasterProfileReferenceNo;
            value.SpecificationNo = "Spec" + " " + ((productionFlavour.SpecNo > 10) ? productionFlavour.SpecNo.ToString() : "0" + productionFlavour.SpecNo.ToString());

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionFlavour")]
        public ProductionFlavourModel Put(ProductionFlavourModel value)
        {
            var productionFlavour = _context.ProductionFlavour.SingleOrDefault(p => p.ProductionFlavourId == value.ProductionFlavourID);
            productionFlavour.ItemNoId = value.ItemNoID;
            productionFlavour.SupplierId = value.SupplierID;
            productionFlavour.SupplierCode = value.SupplierCode;
            productionFlavour.FlavourId = value.FlavourID;
            productionFlavour.ModifiedByUserId = value.ModifiedByUserID;
            productionFlavour.ModifiedDate = DateTime.Now;
            productionFlavour.IsSameAsMaster = value.IsSameAsMaster;
            productionFlavour.MaterialName = value.MaterialName;
            productionFlavour.PurposeId = value.PurposeId;
            productionFlavour.StudyLink = value.StudyLink;
            productionFlavour.ProcessUseSpecId = value.ProcessUseSpecId;
            productionFlavour.RndpurposeId = value.RndpurposeId;
            productionFlavour.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            value.SpecificationNo = "Spec" + " " + ((productionFlavour.SpecNo > 10) ? productionFlavour.SpecNo.ToString() : "0" + productionFlavour.SpecNo.ToString());
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionFlavour")]
        public void Delete(int id)
        {
            try
            {
                var productionFlavour = _context.ProductionFlavour.SingleOrDefault(p => p.ProductionFlavourId == id);
                if (productionFlavour != null)
                {
                    _context.ProductionFlavour.Remove(productionFlavour);
                    _context.SaveChanges();
                    /*var productionFlavourList = _context.ProductionFlavour.Where(p => p.LinkProfileReferenceNo == productionFlavour.LinkProfileReferenceNo).OrderBy(a => a.ProductionFlavourId).AsNoTracking().ToList();
                    int inc = 1;
                    if (productionFlavourList != null)
                    {
                        productionFlavourList.ForEach(h =>
                        {
                            var productionFlavourListItems = _context.ProductionFlavour.SingleOrDefault(p => p.ProductionFlavourId == h.ProductionFlavourId);
                            productionFlavourListItems.SpecNo = inc;
                            _context.SaveChanges();
                            inc++;
                        });
                    }*/
                    //return Ok("Record deleted.");
                }
            }
            catch (Exception ex)
            {
                throw new AppException("You’re not allowed to delete this record", ex);
            }
        }
    }
}