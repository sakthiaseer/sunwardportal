﻿using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MarginInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public MarginInformationController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet]
        [Route("GetMarginInformationVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<MarginInformationModel>>> GetMarginInformationVersion(string sessionID)
        {
            return await _repository.GetList<MarginInformationModel>(sessionID);
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetMarginInformation")]
        public List<MarginInformationModel> Get()
        {
            var marginInformation = _context.MarginInformation.Include(i => i.AddedByUser).Include(m => m.ModifiedByUser).Include(b => b.Distirbutor).Include(d => d.DistributeFor).Include(s => s.StatusCode).AsNoTracking().OrderByDescending(o => o.MarginInformationId).ToList();
            var companylisting = _context.CompanyListing.AsNoTracking().OrderByDescending(o => o.CompanyListingId).ToList();

            List<MarginInformationModel> marginInformationModellist = new List<MarginInformationModel>();
            if (marginInformation != null && marginInformation.Count > 0)
            {
                marginInformation.ForEach(s =>
                {
                    MarginInformationModel marginInformationModel = new MarginInformationModel();
                    marginInformationModel.MarginInformationID = s.MarginInformationId;
                    marginInformationModel.ValidFrom = s.ValidFrom;
                    marginInformationModel.ValidTo = s.ValidTo;
                    marginInformationModel.NeedToKeepTrackVersion = s.NeedToKeepTrackVersion;
                    marginInformationModel.CompanyID = s.CompanyId;
                    marginInformationModel.DistributeForId = s.DistributeForId;
                    marginInformationModel.DistributeFor = s.DistributeFor?.PlantCode;
                    marginInformationModel.VersionSessionId = s.VersionSessionId;
                    marginInformationModel.SessionId = s.VersionSessionId;
                    //marginInformationModel.CompanyName=
                    marginInformationModel.CompanyName = companylisting != null ? companylisting.Where(a => a.CompanyListingId == s.CompanyId).Select(a => a.CompanyName).SingleOrDefault() : "";
                    marginInformationModel.MarginPercentage = s.MarginPercentage;
                    marginInformationModel.DistirbutorID = s.DistirbutorId;
                    marginInformationModel.Distirbutor = s.Distirbutor?.CompanyName;
                    marginInformationModel.AddedByUserID = s.AddedByUserId;
                    marginInformationModel.StatusCodeID = s.StatusCodeId;
                    marginInformationModel.AddedDate = s.AddedDate;
                    marginInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                    marginInformationModel.ModifiedDate = s.ModifiedDate;
                    marginInformationModel.AddedByUser = s.AddedByUser?.UserName;
                    marginInformationModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    marginInformationModel.StatusCode = s.StatusCode?.CodeValue;
                    marginInformationModellist.Add(marginInformationModel);
                });
            }

            return marginInformationModellist;
        }

        [HttpGet]
        [Route("GetMarginInformationLineByID")]
        public List<MarginInformationLineModel> GetMarginInformationLineByID(long id)
        {
            var marginInformationLine = _context.MarginInformationLine.Include(g => g.Item).Include(i => i.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(s => s.MarginInformationId == id).AsNoTracking().OrderByDescending(o => o.MarginInformationLineId).ToList();
            var naveitems = _context.GenericCodes.AsNoTracking().OrderByDescending(o => o.GenericCodeId).ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            List<MarginInformationLineModel> marginInformationLineModellist = new List<MarginInformationLineModel>();
            if (marginInformationLine != null && marginInformationLine.Count > 0)
            {
                marginInformationLine.ForEach(s =>
                {
                    MarginInformationLineModel marginInformationLineModel = new MarginInformationLineModel();
                    marginInformationLineModel.MarginInformationID = s.MarginInformationId;
                    marginInformationLineModel.MarginInformationLineID = s.MarginInformationLineId;
                    marginInformationLineModel.SpecialMarginPercentage = s.SpecialMarginPercentage;
                    marginInformationLineModel.ItemId = s.ItemId;
                    marginInformationLineModel.Item = s.Item != null ? s.Item.Code : null;
                    marginInformationLineModel.Buom = s.Item != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.Item.Uom).Select(m => m.Value).FirstOrDefault() : null;
                    marginInformationLineModel.AddedByUserID = s.AddedByUserId;
                    marginInformationLineModel.StatusCodeID = s.StatusCodeId;
                    marginInformationLineModel.AddedDate = s.AddedDate;
                    marginInformationLineModel.ModifiedByUserID = s.ModifiedByUserId;
                    marginInformationLineModel.ModifiedDate = s.ModifiedDate;
                    marginInformationLineModel.AddedByUser = s.AddedByUser?.UserName;
                    marginInformationLineModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    marginInformationLineModel.StatusCode = s.StatusCode?.CodeValue;
                    marginInformationLineModellist.Add(marginInformationLineModel);
                });
            }

            return marginInformationLineModellist;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<MarginInformationModel> GetData(SearchModel searchModel)
        {
            var marginInformation = new MarginInformation();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).LastOrDefault();
                        break;
                    case "Next":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).LastOrDefault();
                        break;
                    case "Previous":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).FirstOrDefault();
                        break;
                    case "Last":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).LastOrDefault();
                        break;
                    case "Next":
                        marginInformation = _context.MarginInformation.OrderBy(o => o.MarginInformationId).FirstOrDefault(s => s.MarginInformationId > searchModel.Id);
                        break;
                    case "Previous":
                        marginInformation = _context.MarginInformation.OrderByDescending(o => o.MarginInformationId).FirstOrDefault(s => s.MarginInformationId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MarginInformationModel>(marginInformation);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertMarginInformation")]
        public MarginInformationModel Post(MarginInformationModel value)
        {
            var SessionId = Guid.NewGuid();
            var marginInformation = new MarginInformation
            {
                DistirbutorId = value.DistirbutorID,
                CompanyId = value.CompanyID,
                DistributeForId = value.DistributeForId,
                ValidFrom = value.ValidFrom,
                ValidTo = value.ValidTo,
                NeedToKeepTrackVersion = value.NeedToKeepTrackVersion,
                MarginPercentage = value.MarginPercentage,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                VersionSessionId = SessionId,
            };
            _context.MarginInformation.Add(marginInformation);
            _context.SaveChanges();
            value.MarginInformationID = marginInformation.MarginInformationId;
            value.VersionSessionId = marginInformation.VersionSessionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMarginInformation")]
        public async Task<MarginInformationModel> UpdateMarginInformation(MarginInformationModel value)
        {
            value.VersionSessionId ??= Guid.NewGuid();
            var marginInformation = _context.MarginInformation.SingleOrDefault(p => p.MarginInformationId == value.MarginInformationID);
            if (marginInformation != null)
            {
                marginInformation.DistirbutorId = value.DistirbutorID;
                marginInformation.DistributeForId = value.DistributeForId;
                marginInformation.CompanyId = value.CompanyID;
                marginInformation.ValidFrom = value.ValidFrom;
                marginInformation.ValidTo = value.ValidTo;
                marginInformation.NeedToKeepTrackVersion = value.NeedToKeepTrackVersion;
                marginInformation.MarginPercentage = value.MarginPercentage;
                marginInformation.ModifiedByUserId = value.ModifiedByUserID;
                marginInformation.ModifiedDate = DateTime.Now;
                marginInformation.StatusCodeId = value.StatusCodeID.Value;
                marginInformation.VersionSessionId = value.VersionSessionId;
                _context.SaveChanges();
            }
            return value;
        }

        [HttpPut]
        [Route("CreateVersion")]
        public async Task<MarginInformationModel> CreateVersion(MarginInformationModel value)
        {
            value.VersionSessionId = value.VersionSessionId.Value;
            var marginInformation = _context.MarginInformation.SingleOrDefault(p => p.MarginInformationId == value.MarginInformationID);
            if (marginInformation != null)
            {
                var verObject = _mapper.Map<MarginInformationModel>(marginInformation);
                verObject.MarginInformationLineModels = GetMarginInformationLineByID(value.MarginInformationID);
                var versionInfo = new TableDataVersionInfoModel<MarginInformationModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "MarginInformation",
                    PrimaryKey = value.MarginInformationID,
                    ReferenceInfo = value.ReferenceInfo,
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("ApplyVersion")]
        public MarginInformationModel ApplyVersion(MarginInformationModel value)
        {
            var marginInformation = _context.MarginInformation.Include(u => u.MarginInformationLine).SingleOrDefault(p => p.MarginInformationId == value.MarginInformationID);
            if (marginInformation != null)
            {
                marginInformation.DistirbutorId = value.DistirbutorID;
                marginInformation.DistributeForId = value.DistributeForId;
                marginInformation.CompanyId = value.CompanyID;
                marginInformation.ValidFrom = value.ValidFrom;
                marginInformation.ValidTo = value.ValidTo;
                marginInformation.NeedToKeepTrackVersion = value.NeedToKeepTrackVersion;
                marginInformation.MarginPercentage = value.MarginPercentage;
                marginInformation.ModifiedByUserId = value.ModifiedByUserID;
                marginInformation.ModifiedDate = DateTime.Now;
                marginInformation.StatusCodeId = value.StatusCodeID.Value;
                marginInformation.VersionSessionId = value.VersionSessionId;
                _context.SaveChanges();

                if (marginInformation.MarginInformationLine != null)
                {
                    _context.MarginInformationLine.RemoveRange(marginInformation.MarginInformationLine);
                    _context.SaveChanges();
                }
                if (value.MarginInformationLineModels != null)
                {
                    foreach (var lineModel in value.MarginInformationLineModels)
                    {
                        var marginInformationline = new MarginInformationLine
                        {
                            MarginInformationId = lineModel.MarginInformationID,
                            ManufacturingSiteId = lineModel.ManufacturingSiteID,
                            ItemId = lineModel.ItemId,
                            SpecialMarginPercentage = lineModel.SpecialMarginPercentage,
                            AddedByUserId = lineModel.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = lineModel.StatusCodeID.Value,
                        };
                        _context.MarginInformationLine.Add(marginInformationline);
                    }
                }
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(MarginInformationModel value)
        {
            value.VersionSessionId = value.VersionSessionId.Value;
            var distributorReplenishment = _context.MarginInformation.SingleOrDefault(p => p.MarginInformationId == value.MarginInformationID);

            if (distributorReplenishment != null)
            {
                var verObject = _mapper.Map<MarginInformationModel>(distributorReplenishment);
                verObject.MarginInformationLineModels = GetMarginInformationLineByID(value.MarginInformationID);
                var versionInfo = new TableDataVersionInfoModel<MarginInformationModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "MarginInformation",
                    PrimaryKey = value.MarginInformationID,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<MarginInformationModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<MarginInformationModel>(tableData.JsonData);

                    var marginInformation = _context.MarginInformation.Include(m => m.MarginInformationLine).SingleOrDefault(p => p.MarginInformationId == verObject.MarginInformationID);


                    if (verObject != null && marginInformation != null)
                    {
                        marginInformation.DistirbutorId = verObject.DistirbutorID;
                        marginInformation.DistributeForId = verObject.DistributeForId;
                        marginInformation.CompanyId = verObject.CompanyID;
                        marginInformation.ValidFrom = verObject.ValidFrom;
                        marginInformation.ValidTo = verObject.ValidTo;
                        marginInformation.NeedToKeepTrackVersion = verObject.NeedToKeepTrackVersion;
                        marginInformation.MarginPercentage = verObject.MarginPercentage;
                        marginInformation.ModifiedByUserId = verObject.ModifiedByUserID;
                        marginInformation.ModifiedDate = DateTime.Now;
                        marginInformation.StatusCodeId = verObject.StatusCodeID.Value;
                        _context.SaveChanges();
                        if (marginInformation.MarginInformationLine != null)
                        {
                            _context.MarginInformationLine.RemoveRange(marginInformation.MarginInformationLine);
                            _context.SaveChanges();
                        }
                        if (verObject.MarginInformationLineModels != null)
                        {
                            foreach (var lineModel in verObject.MarginInformationLineModels)
                            {
                                var marginInformationline = new MarginInformationLine
                                {
                                    MarginInformationId = lineModel.MarginInformationID,
                                    ManufacturingSiteId = lineModel.ManufacturingSiteID,
                                    ItemId = lineModel.ItemId,
                                    SpecialMarginPercentage = lineModel.SpecialMarginPercentage,
                                    AddedByUserId = lineModel.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = lineModel.StatusCodeID.Value,
                                };
                                _context.MarginInformationLine.Add(marginInformationline);
                            }
                            _context.SaveChanges();
                        }
                    }
                    return verObject;
                }
                return new MarginInformationModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMarginInformation")]
        public void Delete(int id)
        {
            var MarginInformation = _context.MarginInformation.Include(l => l.MarginInformationLine).SingleOrDefault(p => p.MarginInformationId == id);
            if (MarginInformation != null)
            {
                if (MarginInformation.VersionSessionId != null)
                {
                    var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == MarginInformation.VersionSessionId).ToList();
                    _context.TableDataVersionInfo.RemoveRange(versions);

                }
                _context.MarginInformation.Remove(MarginInformation);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("InsertMarginInformationLine")]
        public MarginInformationLineModel InsertMarginInformationLine(MarginInformationLineModel value)
        {
            var marginInformationline = new MarginInformationLine
            {
                MarginInformationId = value.MarginInformationID,
                ManufacturingSiteId = value.ManufacturingSiteID,
                ItemId = value.ItemId,
                SpecialMarginPercentage = value.SpecialMarginPercentage,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.MarginInformationLine.Add(marginInformationline);
            _context.SaveChanges();
            value.MarginInformationLineID = marginInformationline.MarginInformationLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateMarginInformationLine")]
        public MarginInformationLineModel UpdateMarginInformationLine(MarginInformationLineModel value)
        {
            var marginInformationline = _context.MarginInformationLine.SingleOrDefault(p => p.MarginInformationLineId == value.MarginInformationLineID);
            if (marginInformationline != null)
            {
                marginInformationline.MarginInformationId = value.MarginInformationID;
                marginInformationline.ManufacturingSiteId = value.ManufacturingSiteID;
                marginInformationline.ItemId = value.ItemId;
                marginInformationline.SpecialMarginPercentage = value.SpecialMarginPercentage;
                marginInformationline.ModifiedByUserId = value.ModifiedByUserID;
                marginInformationline.ModifiedDate = DateTime.Now;
                marginInformationline.StatusCodeId = value.StatusCodeID.Value;
                _context.SaveChanges();
            }
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteMarginInformationLine")]
        public void DeleteMarginInformationLine(int id)
        {
            var marginInformationline = _context.MarginInformationLine.SingleOrDefault(p => p.MarginInformationLineId == id);
            if (marginInformationline != null)
            {
                _context.MarginInformationLine.Remove(marginInformationline);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetProductwithName")]
        public List<SellingPriceInformationModel> GetProductwithName(int id)
        {
            var sellingCatalogueid = _context.SellingCatalogue.Where(s => s.CompanyId == id).Select(s => s.SellingCatalogueId).FirstOrDefault();
            var sellingPriceInformation = _context.SellingPriceInformation.Where(s => s.SellingCatalogueId == sellingCatalogueid).AsNoTracking().ToList();

            var navitems = _context.Navitems.AsNoTracking().OrderByDescending(o => o.ItemId).ToList();
            List<SellingPriceInformationModel> sellingPriceInformationModelList = new List<SellingPriceInformationModel>();
            if (sellingPriceInformation != null && sellingPriceInformation.Count > 0)
            {
                sellingPriceInformation.ForEach(s =>
                {

                    SellingPriceInformationModel sellingCatalogueModel = new SellingPriceInformationModel();
                    sellingCatalogueModel.ProductID = s.ProductId;
                    sellingCatalogueModel.ProductName = navitems != null ? navitems.Where(a => a.ItemId == s.ProductId).Select(a => a.Description).SingleOrDefault() : "";

                    sellingPriceInformationModelList.Add(sellingCatalogueModel);
                });
            }

            return sellingPriceInformationModelList;
        }

        [HttpGet]
        [Route("GetProductUomID")]
        public MarginInformationLineModel GetProductUomID(int id)
        {
            var sellingCatalogueid = _context.Navitems.Where(s => s.ItemId == id).Select(s => s.BaseUnitofMeasure).FirstOrDefault();
            // var sellingPriceInformation = _context.SellingPriceInformation.Where(s => s.SellingCatalogueId == sellingCatalogueid).AsNoTracking().ToList();
            MarginInformationLineModel marginInformationLineModel = new MarginInformationLineModel();
            if (sellingCatalogueid != null)
            {
                marginInformationLineModel.Buom = sellingCatalogueid;
            }

            return marginInformationLineModel;
        }
        [HttpPost]
        [Route("GetMarginInformationByCompany")]
        public MarginInformationModel GetMarginInformationByCompany(MarginInformationSearchModel searchModel)
        {
            var companyListing = _context.CompanyListingLine.Include(c => c.CompanyListing).Where(s => s.CompanyListingId == searchModel.CompanyId && s.BusinessCategoryId == searchModel.BusinessCategoryId).FirstOrDefault();
            MarginInformationModel marginInformationModel = new MarginInformationModel();
            if (companyListing != null)
            {
                var marginInformation = _context.MarginInformation.Where(c => c.CompanyId == companyListing.CompanyListingId).FirstOrDefault();
                if (marginInformation != null)
                {
                    marginInformationModel.MarginInformationID = marginInformation.MarginInformationId;
                    marginInformationModel.MarginPercentage = marginInformation.MarginPercentage;
                    marginInformationModel.DistirbutorID = marginInformation.DistirbutorId;
                    marginInformationModel.CompanyID = marginInformation.CompanyId;
                }
            }

            return marginInformationModel;
        }









    }
}