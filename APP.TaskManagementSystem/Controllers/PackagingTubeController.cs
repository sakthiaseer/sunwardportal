﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingTubeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public PackagingTubeController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingTubes")]
        public List<PackagingTubeModel> Get()
        {
            List<PackagingTubeModel> packagingTubeModels = new List<PackagingTubeModel>();
            var packagingTube = _context.PackagingTube.Include(a=>a.AddedByUser).Include(b=>b.ModifiedByUser).OrderByDescending(o => o.TubeId).AsNoTracking().ToList();
            if (packagingTube != null && packagingTube.Count > 0)
            {           
                List<long?> masterIds = packagingTube.Where(w => w.CoatingMaterialId != null).Select(a => a.CoatingMaterialId).Distinct().ToList();
                masterIds.AddRange(packagingTube.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingTube.ForEach(s =>
                {
                    PackagingTubeModel packagingTubeModel = new PackagingTubeModel
                    {
                        ColorId = s.ColorId,
                        Diameter = s.Diameter,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Non-Printed",
                        Length = s.Length,
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PantoneColorId = s.PantoneColorId,
                        TubeId = s.TubeId,
                        Width = s.Width,
                        PackingUnitId = s.PackingUnitId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        CoatingMaterialId = s.CoatingMaterialId,
                        TubeTypeID = s.TubeTypeId,
                        IsVersion = s.IsVersion,
                        CoatingMaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CoatingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkeProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo,
                    };
                    packagingTubeModels.Add(packagingTubeModel);

                });
            }
            return packagingTubeModels;
        }

        [HttpPost]
        [Route("GetPackagingTubesByRefNo")]
        public List<PackagingTubeModel> GetPackagingTubesByRefNo(RefSearchModel refSearchModel)
        {
            List<PackagingTubeModel> packagingTubeModels = new List<PackagingTubeModel>();
            List<PackagingTube> packagingTube = new List<PackagingTube>();
            if (refSearchModel.IsHeader)
            {
                packagingTube = _context.PackagingTube.Include(a=>a.AddedByUser).Include(b=>b.ModifiedByUser).Where(r => r.LinkeProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.TubeTypeId == refSearchModel.TypeID).OrderByDescending(o => o.TubeId).AsNoTracking().ToList();
            }
            else
            {
                packagingTube = _context.PackagingTube.Include(a=>a.AddedByUser).Include(b=>b.ModifiedByUser).Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo && r.TubeTypeId == refSearchModel.TypeID).OrderByDescending(o => o.TubeId).AsNoTracking().ToList();

            }
            if (packagingTube != null && packagingTube.Count > 0)
            {
                List<long?> masterIds = packagingTube.Where(w => w.CoatingMaterialId != null).Select(a => a.CoatingMaterialId).Distinct().ToList();
                masterIds.AddRange(packagingTube.Where(w => w.ColorId != null).Select(a => a.ColorId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackagingItemId != null).Select(a => a.PackagingItemId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackagingMaterialId != null).Select(a => a.PackagingMaterialId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PantoneColorId != null).Select(a => a.PantoneColorId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.PackingUnitId != null).Select(a => a.PackingUnitId).Distinct().ToList());
                masterIds.AddRange(packagingTube.Where(w => w.LetteringId != null).Select(a => a.LetteringId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Distinct().Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                packagingTube.ForEach(s =>
                {
                    PackagingTubeModel packagingTubeModel = new PackagingTubeModel
                    {
                        ColorId = s.ColorId,
                        Diameter = s.Diameter,
                        IsPrinted = s.IsPrinted,
                        Printed = s.IsPrinted == true ? "Printed" : "Non-Printed",
                        Length = s.Length,
                        LetteringId = s.LetteringId,
                        NoOfColor = s.NoOfColor,
                        PackagingItemId = s.PackagingItemId,
                        PackagingMaterialId = s.PackagingMaterialId,
                        PantoneColorId = s.PantoneColorId,
                        TubeId = s.TubeId,
                        Width = s.Width,
                        PackingUnitId = s.PackingUnitId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        CoatingMaterialId = s.CoatingMaterialId,
                        TubeTypeID = s.TubeTypeId,
                        IsVersion = s.IsVersion,
                        CoatingMaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.CoatingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "",
                        PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "",
                        PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "",
                        Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.LetteringId).Select(a => a.Value).SingleOrDefault() : "",
                        MasterProfileReferenceNo = s.MasterProfileReferenceNo,
                        LinkProfileReferenceNo = s.LinkeProfileReferenceNo,
                        ProfileLinkReferenceNo = s.ProfileReferenceNo,
                    };
                    packagingTubeModels.Add(packagingTubeModel);

                });
            }

            return packagingTubeModels;

        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackagingTubeModel> GetData(SearchModel searchModel)
        {
            var packagingTube = new PackagingTube();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).LastOrDefault();
                        break;
                    case "Next":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).LastOrDefault();
                        break;
                    case "Next":
                        packagingTube = _context.PackagingTube.OrderBy(o => o.TubeId).FirstOrDefault(s => s.TubeId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingTube = _context.PackagingTube.OrderByDescending(o => o.TubeId).FirstOrDefault(s => s.TubeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackagingTubeModel>(packagingTube);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingTube")]
        public PackagingTubeModel Post(PackagingTubeModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title= "PackagingTube" });
            var packagingTube = new PackagingTube
            {
                ColorId = value.ColorId,
                Diameter = value.Diameter,
                IsPrinted = value.IsPrinted,
                Length = value.Length,
                LetteringId = value.LetteringId,
                NoOfColor = value.NoOfColor,
                PackagingItemId = value.PackagingItemId,
                PackagingMaterialId = value.PackagingMaterialId,
                PantoneColorId = value.PantoneColorId,
                Width = value.Width,
                PackingUnitId = value.PackingUnitId,
                IsVersion = value.IsVersion,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                TubeTypeId = value.TubeTypeID,
                LinkeProfileReferenceNo = value.LinkProfileReferenceNo,
                CoatingMaterialId = value.CoatingMaterialId,
                ProfileReferenceNo = profileNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
            };
            _context.PackagingTube.Add(packagingTube);
            _context.SaveChanges();
            value.MasterProfileReferenceNo = packagingTube.MasterProfileReferenceNo;
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";
            value.CoatingMaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.CoatingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.TubeId = packagingTube.TubeId;
            value.Printed = packagingTube.IsPrinted == true ? "Printed" : "Not-Printed";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }
        private string UpdateCommonPackage(PackagingTubeModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.PackagingItemName = "";
            var itemName = "";
            //if (value.FormTab == true)
            //{
            if (value.TubeTypeID == 1084)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                    if (itemHeader != null)
                    {
                        //[No of color] Color[Diameter] mm X[Size(W)] mm X[Size(L)] mm[Packaging Material] Tube
                        if (value.IsPrinted == true)
                        {
                            itemName = "Printed" + " ";
                        }
                        if (value.IsPrinted == false)
                        {
                            itemName = "Non- Printed" + " ";
                        }
                        var PackagingMaterial = value.PackagingMaterialId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == value.PackagingMaterialId).Select(m => m.Value).FirstOrDefault() : "";
                        if (value.NoOfColor != null)
                        {
                            itemName = itemName + value.NoOfColor + " " + "Color" + " ";
                        }
                        if (value.Diameter != null)
                        {
                            itemName = itemName + value.Diameter + " " + "mm" + " ";
                        }
                        if (value.Width != null)
                        {
                            itemName = itemName + "X" + " " + value.Width + " " + "mm" + " ";
                        }
                        if (value.Length != null)
                        {
                            itemName = itemName + "X" + " " + value.Length + " " + "mm" + " ";
                        }
                        if (!string.IsNullOrEmpty(PackagingMaterial))
                        {
                            itemName = itemName + PackagingMaterial + " ";
                        }
                        itemName = itemName + "Tube";
                        //itemName = value.NoOfColor + " " + "Color" + " " + value.Diameter + " " + "mm" + " " + "X" + " " + value.Width + " " + "mm" + " " + "X" + " " + value.Length + " " + "mm" + " " + PackagingMaterial + " " + "Tube";
                        itemHeader.PackagingItemName = itemName;
                        value.PackagingItemName = itemName;
                        _context.SaveChanges();
                    }
                }
            }
            else if (value.TubeTypeID == 1085)
            {
                if (value.LinkProfileReferenceNo != null)
                {
                    if (value.IsPrinted.GetValueOrDefault(false))
                    {
                        var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                        if (itemHeader != null)
                        {
                            //[No of color] Color[Diameter] mm X[Size(W)] mm X[Size(L)] mm[Packaging Material] Tube
                            if (value.IsPrinted == true)
                            {
                                itemName = "Printed" + " ";
                            }
                            if (value.IsPrinted == false)
                            {
                                itemName = "Non- Printed" + " ";
                            }
                            var coatingMaterial = value.CoatingMaterialId > 0 && masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == value.CoatingMaterialId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == value.CoatingMaterialId).Value : "";
                            if (value.NoOfColor != null)
                            {
                                itemName = itemName + value.NoOfColor + " " + "Color" + " ";
                            }
                            if (value.Diameter != null)
                            {
                                itemName = itemName + value.Diameter + " " + "mm(Dia)" + " " + "X" + " ";
                            }
                            if (value.Width != null)
                            {
                                itemName = itemName + value.Width + " " + "mm(W)" + " " + "X" + " ";
                            }
                            if (value.Length != null)
                            {
                                itemName = itemName + value.Length + " " + "mm(L)" + " ";
                            }
                            if (coatingMaterial != null)
                            {
                                itemName = itemName + coatingMaterial + " ";
                            }
                            itemName = itemName + "Tube";
                            //itemName = value.NoOfColor + " " + "Color" + " " + value.Diameter + " " + "mm(Dia)" + " " + "X" + " " + value.Width + " " + "mm(W)" + " " + "X" + " " + value.Length + " " + "mm(L)" + " " + coatingMaterial + " " + "Tube";
                            itemHeader.PackagingItemName = itemName;
                            value.PackagingItemName = itemName;
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        var itemHeader = _context.CommonPackagingItemHeader.SingleOrDefault(p => p.ProfileReferenceNo == value.LinkProfileReferenceNo);
                        if (itemHeader != null)
                        {
                            //[No of color] Color[Diameter] mm X[Size(W)] mm X[Size(L)] mm[Packaging Material] Tube
                            if (value.IsPrinted == true)
                            {
                                itemName = "Printed" + " ";
                            }
                            if (value.IsPrinted == false)
                            {
                                itemName = "Non- Printed" + " ";
                            }
                            var coatingMaterial = value.CoatingMaterialId > 0 && masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == value.CoatingMaterialId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == value.CoatingMaterialId).Value : "";
                            if (value.Diameter != null)
                            {
                                itemName = itemName + value.Diameter + " " + "mm(Dia)" + " " + "X" + " ";
                            }
                            if (value.Width != null)
                            {
                                itemName = itemName + value.Width + " " + "mm(W)" + " " + "X" + " ";
                            }
                            if (value.Length != null)
                            {
                                itemName = itemName + value.Length + " " + "mm(L)" + " ";
                            }
                            if (coatingMaterial != null)
                            {
                                itemName = itemName + coatingMaterial + " ";
                            }
                            itemName = itemName + "Tube";
                            itemHeader.PackagingItemName = itemName;
                            value.PackagingItemName = itemName;
                            _context.SaveChanges();
                        }
                    }
                }
            }
            //}
            return value.PackagingItemName;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingTube")]
        public PackagingTubeModel Put(PackagingTubeModel value)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var packagingTube = _context.PackagingTube.SingleOrDefault(p => p.TubeId == value.TubeId);
            packagingTube.ColorId = value.ColorId;
            packagingTube.Diameter = value.Diameter;
            packagingTube.IsPrinted = value.IsPrinted;
            packagingTube.Length = value.Length;
            packagingTube.LetteringId = value.LetteringId;
            packagingTube.NoOfColor = value.NoOfColor;
            packagingTube.PackagingItemId = value.PackagingItemId;
            packagingTube.PackagingMaterialId = value.PackagingMaterialId;
            packagingTube.PantoneColorId = value.PantoneColorId;
            packagingTube.Width = value.Width;
            packagingTube.IsVersion = value.IsVersion;
            packagingTube.PackingUnitId = value.PackingUnitId;
            packagingTube.ModifiedByUserId = value.ModifiedByUserID;
            packagingTube.ModifiedDate = DateTime.Now;
            packagingTube.StatusCodeId = value.StatusCodeID.Value;
            packagingTube.TubeTypeId = value.TubeTypeID;
            packagingTube.CoatingMaterialId = value.CoatingMaterialId;

            _context.SaveChanges();
            value.Color = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.ColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingItem = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingItemId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingMaterial = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackagingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.PantoneColor = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PantoneColorId).Select(a => a.Value).SingleOrDefault() : "";
            value.PackagingUnit = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.PackingUnitId).Select(a => a.Value).SingleOrDefault() : "";
            value.Lettering = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.LetteringId).Select(a => a.Value).SingleOrDefault() : "";
            value.CoatingMaterialName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == value.CoatingMaterialId).Select(a => a.Value).SingleOrDefault() : "";
            value.Printed = packagingTube.IsPrinted == true ? "Printed" : "Not-Printed";
            value.PackagingItemName = UpdateCommonPackage(value);
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingTube")]
        public void Delete(int id)
        {
            var packagingTube = _context.PackagingTube.SingleOrDefault(p => p.TubeId == id);
            if (packagingTube != null)
            {
                _context.PackagingTube.Remove(packagingTube);
                _context.SaveChanges();
            }
        }
    }
}