﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionSimulationGroupingTicketController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionSimulationGroupingTicketController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [Route("GetProductionSimulationGroupingTicke")]
        public List<ProductionSimulationGroupingTicketModel> GetProductionSimulationGroupingTicke(long id)
        {
            var productionSimulationGrouping = _context.ProductionSimulationGroupingTicket.Where(w => w.MethodCodeId == id).ToList();
            var codeMasterIds = productionSimulationGrouping.Select(s => s.StatusCodeId.GetValueOrDefault(0)).ToList();
            var userIds = productionSimulationGrouping.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            userIds.AddRange(productionSimulationGrouping.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var itemIds = productionSimulationGrouping.Select(s => s.ItemId.GetValueOrDefault(0)).ToList();
            var methodIds = productionSimulationGrouping.Select(s => s.MethodCodeId.GetValueOrDefault(0)).ToList();
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(u => userIds.Contains(u.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(c => codeMasterIds.Contains(c.CodeId)).AsNoTracking().ToList();
            var methodLists = _context.NavMethodCode.Select(s => new
            {
                s.MethodCodeId,
                s.MethodDescription,
            }).Where(c => methodIds.Contains(c.MethodCodeId)).AsNoTracking().ToList();
            var navitems = _context.Navitems.Select(s => new
            {
                s.ItemId,
                s.No,
                s.Description,
                s.Description2,
                s.BaseUnitofMeasure,
                s.InternalRef
            }).Where(c => itemIds.Contains(c.ItemId)).AsNoTracking().ToList();
            List<ProductionSimulationGroupingTicketModel> productionSimulationGroupingTicketModels = new List<ProductionSimulationGroupingTicketModel>();
            productionSimulationGrouping.ForEach(s =>
            {
                ProductionSimulationGroupingTicketModel productionSimulationGroupingTicketModel = new ProductionSimulationGroupingTicketModel
                {
                    ProductionSimulationGroupingTicketId = s.ProductionSimulationGroupingTicketId,
                    ExistingTicket = s.ExistingTicket,
                    FullTicket = s.FullTicket,
                    Qty = s.Qty,
                    SplitTicket = s.SplitTicket,
                    ItemId = s.ItemId,
                    MethodCodeId = s.MethodCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    WeekOfDate = s.WeekOfDate,
                    WeekNo = s.WeekNo,
                    MethodCodeName = methodLists != null ? methodLists.FirstOrDefault(u => u.MethodCodeId == s.MethodCodeId)?.MethodDescription : "",
                    ItemName = navitems != null ? navitems.FirstOrDefault(u => u.ItemId == s.ItemId)?.Description : "",
                    AddedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.AddedByUserId)?.UserName : "",
                    StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : "",
                    ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(u => u.UserId == s.ModifiedByUserId)?.UserName : "",
                };
                productionSimulationGroupingTicketModels.Add(productionSimulationGroupingTicketModel);
            });

            return productionSimulationGroupingTicketModels;
        }
        [HttpPost]
        [Route("InsertProductionSimulationGroupingTicket")]
        public ProductionSimulationGroupingTicketModel Post(ProductionSimulationGroupingTicketModel value)
        {
            var extisting = _context.ProductionSimulationGroupingTicket.SingleOrDefault(p => p.MethodCodeId == value.MethodCodeId && p.ItemId == value.ItemId);
            if (extisting == null)
            {
                var packSize = _context.Navitems.FirstOrDefault(s => s.ItemId == value.ItemId)?.PackSize;
                var ProductionSimulationGrouping = new ProductionSimulationGroupingTicket
                {
                    ExistingTicket = value.ExistingTicket,
                    FullTicket = value.FullTicket,
                    SplitTicket = value.SplitTicket,
                    Qty = packSize != null ? (value.Qty * packSize) : value.Qty,
                    MethodCodeId = value.MethodCodeId,
                    ItemId = value.ItemId,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID,
                    WeekOfDate = DateTime.Now,
                    WeekNo = GetWeekNumberOfMonth(DateTime.Now),

                };
                _context.ProductionSimulationGroupingTicket.Add(ProductionSimulationGrouping);
                _context.SaveChanges();
                value.ProductionSimulationGroupingTicketId = ProductionSimulationGrouping.ProductionSimulationGroupingTicketId;
            }
            else
            {
                value = UpdateProductionSimulationGroupingTicket(value);
            }
            return value;
        }
        private int GetWeekNumberOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDay = (int)firstDayOfMonth.DayOfWeek;
            if (firstDay == 0)
            {
                firstDay = 7;
            }
            double d = (firstDay + date.Day - 1) / 7.0;
            return d > 5 ? (int)Math.Floor(d) : (int)Math.Ceiling(d);
        }
        [HttpPut]
        [Route("UpdateProductionSimulationGroupingTicket")]
        public ProductionSimulationGroupingTicketModel UpdateProductionSimulationGroupingTicket(ProductionSimulationGroupingTicketModel value)
        {
            var productionSimulationGrouping = _context.ProductionSimulationGroupingTicket.SingleOrDefault(p => p.ProductionSimulationGroupingTicketId == value.ProductionSimulationGroupingTicketId);
            var packSize = _context.Navitems.FirstOrDefault(s => s.ItemId == value.ItemId)?.PackSize;
            productionSimulationGrouping.MethodCodeId = value.MethodCodeId;
            productionSimulationGrouping.ItemId = value.ItemId;
            productionSimulationGrouping.ExistingTicket = value.ExistingTicket;
            productionSimulationGrouping.FullTicket = value.FullTicket;
            productionSimulationGrouping.Qty = packSize != null ? (value.Qty * packSize) : value.Qty;
            productionSimulationGrouping.SplitTicket = value.SplitTicket;
            productionSimulationGrouping.ModifiedDate = DateTime.Now;
            productionSimulationGrouping.StatusCodeId = value.StatusCodeID;
            productionSimulationGrouping.ModifiedByUserId = value.ModifiedByUserID;
            productionSimulationGrouping.WeekNo = GetWeekNumberOfMonth(DateTime.Now);
            _context.SaveChanges();
            return value;
        }
    }
}
