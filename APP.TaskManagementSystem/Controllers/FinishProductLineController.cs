﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FinishProductLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FinishProductLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetFinishProductLine")]
        public List<FinishProductLineModel> Get(int? id)
        {
            var applicationdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var finishProductline = _context.FinishProductLine.AsNoTracking().ToList();
            List<FinishProductLineModel> finishProductLineModel = new List<FinishProductLineModel>();
            finishProductline.ForEach(s =>
            {
                FinishProductLineModel finishProductLineModels = new FinishProductLineModel
                {
                    FinishProductLineId = s.FinishProductLineId,
                    FinishProductId = s.FinishProductId,
                    Uom = s.Uom,
                    DosageForm = s.DosageForm,
                    Overage = s.Overage,
                    MaterialId = s.MaterialId,
                    DosageUnits = s.DosageUnits,
                    OverageUnits = s.OverageUnits,
                    PerDosage = s.PerDosage,
                    PerDosageName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.PerDosage).Select(a => a.Value).FirstOrDefault(),
                    MaterialName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.MaterialId).Select(a => a.Value).FirstOrDefault(),
                    UnitName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.Uom).Select(a => a.Value).FirstOrDefault(),
                    DosageUnitsName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.DosageUnits).Select(a => a.Value).FirstOrDefault(),
                    OverageUnitsName = applicationdetail.Where(a => a.ApplicationMasterDetailId == s.OverageUnits).Select(a => a.Value).FirstOrDefault(),
                };
                finishProductLineModel.Add(finishProductLineModels);
            });
            return finishProductLineModel.Where(w => w.FinishProductId == id).OrderByDescending(a => a.FinishProductLineId).ToList();
        }
        [HttpPost]
        [Route("InsertFinishProductLine")]
        public FinishProductLineModel Post(FinishProductLineModel value)
        {
            var finishproductline = new FinishProductLine
            {
                FinishProductId = value.FinishProductId,
                MaterialId = value.MaterialId,
                DosageForm = value.DosageForm,
                Uom = value.Uom,
                Overage = value.Overage,
                OverageUnits = value.OverageUnits,
                DosageUnits = value.DosageUnits,
                PerDosage = value.PerDosage,
            };
            _context.FinishProductLine.Add(finishproductline);
            _context.SaveChanges();
            value.FinishProductLineId = finishproductline.FinishProductLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateFinishProductLine")]
        public FinishProductLineModel Put(FinishProductLineModel value)
        {
            var finishproductline = _context.FinishProductLine.SingleOrDefault(p => p.FinishProductLineId == value.FinishProductLineId);
            finishproductline.FinishProductId = value.FinishProductId;
            finishproductline.Overage = value.Overage;
            finishproductline.Uom = value.Uom;
            finishproductline.MaterialId = value.MaterialId;
            finishproductline.DosageForm = value.DosageForm;
            finishproductline.DosageUnits = value.DosageUnits;
            finishproductline.OverageUnits = value.OverageUnits;
            finishproductline.PerDosage = value.PerDosage;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteFinishProductLine")]
        public void Delete(int id)
        {
            var finishproductline = _context.FinishProductLine.SingleOrDefault(p => p.FinishProductLineId == id);
            if (finishproductline != null)
            {
                _context.FinishProductLine.Remove(finishproductline);
                _context.SaveChanges();
            }
        }
    }
}