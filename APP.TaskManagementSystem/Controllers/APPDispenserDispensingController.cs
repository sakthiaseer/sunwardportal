﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class APPDispenserDispensingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public APPDispenserDispensingController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPDispenserDispensings")]
        public List<APPDispenserDispensingModel> Get()
        {
            var appDispenserDispensingEntry = _context.AppdispenserDispensing.Include("ModifiedByUser").Select(s => new APPDispenserDispensingModel
            {

                // DispenserDispensingId = s.Key.
                DispenserDispensingId = s.DispenserDispensingId,
                WorkOrderNo = s.WorkOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                TotalItem = s.TotalItem,
                WeighingMaterial = s.WeighingMaterial,

            }).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPDispenserDispensingModel>>(APPDispenserDispensing);
            return appDispenserDispensingEntry;
        }

        //Changes Done by Aravinth Start

        [HttpPost]
        [Route("GetAPPDispenserDispensingEntryFilter")]
        public List<APPDispenserDispensingModel> GetFilter(APPDispenserDispensingSearchModel value)
        {
            var appDispenserDispensingEntry = _context.AppdispenserDispensing.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new APPDispenserDispensingModel
            {
                DispenserDispensingId = s.DispenserDispensingId,
                WorkOrderNo = s.WorkOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                TotalItem = s.TotalItem,
                WeighingMaterial = s.WeighingMaterial

            }).OrderByDescending(o => o.DispenserDispensingId).AsNoTracking().ToList();

            List<APPDispenserDispensingModel> filteredItems = new List<APPDispenserDispensingModel>();
            if (value.WorkOrderNo.Any())
            {
                var itemWorkOrderNoFilters = appDispenserDispensingEntry.Where(p => p.WorkOrderNo != null ? (p.WorkOrderNo.ToLower() == (value.WorkOrderNo.ToLower())) : false).ToList();
                itemWorkOrderNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.DispenserDispensingId == i.DispenserDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ItemNo.Any())
            {
                var itemItemNoFilters = appDispenserDispensingEntry.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (value.ItemNo.ToLower())) : false).ToList();
                itemItemNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.DispenserDispensingId == i.DispenserDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.Description.Any())
            {
                var itemDescriptionFilters = appDispenserDispensingEntry.Where(p => p.Description != null ? (p.Description.ToLower() == (value.Description.ToLower())) : false).ToList();
                itemDescriptionFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.DispenserDispensingId == i.DispenserDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.TotalItem.ToString().Any())
            {
                var itemTotalItemFilters = appDispenserDispensingEntry.Where(p => p.TotalItem.ToString() != null ? (p.TotalItem.ToString().ToLower() == (value.TotalItem.ToString().ToLower())) : false).ToList();
                itemTotalItemFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.DispenserDispensingId == i.DispenserDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.WeighingMaterial.ToString().Any())
            {
                var itemWeighingMaterialFilters = appDispenserDispensingEntry.Where(p => p.WeighingMaterial.ToString() != null ? (p.WeighingMaterial.ToString().ToLower() == (value.WeighingMaterial.ToString().ToLower())) : false).ToList();
                itemWeighingMaterialFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.DispenserDispensingId == i.DispenserDispensingId))
                    {
                        filteredItems.Add(i);
                    }

                });
            }

            return filteredItems;
        }

        //Changes Done by Aravinth End

        [HttpGet]
        [Route("GetAPPDispenserDispensingByID")]
        public List<APPDispenserDispensingModel> GetDispensingByWorkOrder(string Id)
        {
            var appDispenserDispensingEntry = _context.AppdispenserDispensing.Select(s => new APPDispenserDispensingModel
            {

                DispenserDispensingId = s.DispenserDispensingId,
                WorkOrderNo = s.WorkOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                TotalItem = s.TotalItem,
                WeighingMaterial = s.WeighingMaterial,



            }).OrderByDescending(o => o.DispenserDispensingId).Where(t => t.WorkOrderNo == Id).AsNoTracking().ToList();

            return appDispenserDispensingEntry;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPDispenserDispensingModel> GetData(SearchModel searchModel)
        {
            var appDispenserDispensingEntry = new AppdispenserDispensing();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).FirstOrDefault();
                        break;
                    case "Last":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).LastOrDefault();
                        break;
                    case "Next":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).LastOrDefault();
                        break;
                    case "Previous":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).FirstOrDefault();
                        break;
                    case "Last":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).LastOrDefault();
                        break;
                    case "Next":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderBy(o => o.DispenserDispensingId).FirstOrDefault(s => s.DispenserDispensingId > searchModel.Id);
                        break;
                    case "Previous":
                        appDispenserDispensingEntry = _context.AppdispenserDispensing.OrderByDescending(o => o.DispenserDispensingId).FirstOrDefault(s => s.DispenserDispensingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPDispenserDispensingModel>(appDispenserDispensingEntry);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAPPDispenserDispensing")]
        public APPDispenserDispensingModel Post(APPDispenserDispensingModel value)
        {


            var appDispenserDispensingEntry = new AppdispenserDispensing
            {

               
                WorkOrderNo = value.WorkOrderNo,
                ItemNo = value.ItemNo,
                Description = value.Description,
                TotalItem = value.TotalItem,
                WeighingMaterial = value.WeighingMaterial,
                StatusCodeId = 1,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,


            };
            _context.AppdispenserDispensing.Add(appDispenserDispensingEntry);

            _context.SaveChanges();

            value.DispenserDispensingId = appDispenserDispensingEntry.DispenserDispensingId;
            return value;
        }
        [HttpPost]
        [Route("PostDispensing")]
        public APPDispenserDispensingModel PostDispensing(APPDispenserDispensingModel value)
        {
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAPPDispenserDispensing")]
        public APPDispenserDispensingModel Put(APPDispenserDispensingModel value)
        {
            var appDispenserDispensing = _context.AppdispenserDispensing.SingleOrDefault(p => p.DispenserDispensingId == value.DispenserDispensingId);
            appDispenserDispensing.WorkOrderNo = value.WorkOrderNo;
            appDispenserDispensing.TotalItem = value.TotalItem;
            appDispenserDispensing.ItemNo = value.ItemNo;
            appDispenserDispensing.Description = value.Description;
            appDispenserDispensing.WeighingMaterial = value.WeighingMaterial;
            //APPDispenserDispensing.APPDispenserDispensingId = value.APPDispenserDispensingID;
            appDispenserDispensing.ModifiedByUserId = value.ModifiedByUserID;
            appDispenserDispensing.ModifiedDate = DateTime.Now;
            appDispenserDispensing.ModifiedByUserId = value.ModifiedByUserID;
            appDispenserDispensing.ModifiedDate = value.ModifiedDate;
            appDispenserDispensing.StatusCodeId = value.StatusCodeID;
          
            _context.SaveChanges();
            if (value.DispensingLines.Count != 0)
            {
                var DespensingLines = value.DispensingLines;
                DespensingLines.ForEach(despensing =>
                {
                    if (DespensingLines.Count > 0)
                    {
                        var appDispenserDispensingLine = _context.AppdispenserDispensingLine.SingleOrDefault(p => p.DispenserDispensingLineId == despensing.DispenserDispensingLineId);
                        appDispenserDispensingLine.DispenserDispensingId = value.DispenserDispensingId;
                        appDispenserDispensingLine.ProdOrderNo = despensing.ProdOrderNo;
                        appDispenserDispensingLine.ProdLineNo = despensing.ProdLineNo;
                        appDispenserDispensingLine.SubLotNo = despensing.SubLotNo;
                        appDispenserDispensingLine.JobNo = despensing.JobNo;
                        appDispenserDispensingLine.BagNo = despensing.BagNo;
                        appDispenserDispensingLine.BeforeTareWeight = despensing.BeforeTareWeight;
                        appDispenserDispensingLine.ImageBeforeTare = despensing.ImageBeforeTare;
                        appDispenserDispensingLine.AfterTareWeight = despensing.AfterTareWeight;
                        appDispenserDispensingLine.ImageAfterTare = despensing.ImageAfterTare;
                        appDispenserDispensingLine.ItemNo = despensing.ItemNo;
                        appDispenserDispensingLine.LotNo = despensing.LotNo;
                        appDispenserDispensingLine.QcrefNo = despensing.QCRefNo;
                        appDispenserDispensingLine.Weight = despensing.Weight;
                        appDispenserDispensingLine.WeighingPhoto = despensing.WeighingPhoto;
                        appDispenserDispensingLine.PrintLabel = despensing.PrintLabel;
                        appDispenserDispensingLine.PostedtoNav = despensing.PostedtoNAV;
                        _context.SaveChanges();
                    }

                });
            }

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDispensingEntry")]
        public void Delete(int id)
        {
            var appDispenserDispensingEntry = _context.AppdispenserDispensing.SingleOrDefault(p => p.DispenserDispensingId == id);
            var errorMessage = "";
            try
            {
                if (appDispenserDispensingEntry != null)
                {
                    _context.AppdispenserDispensing.Remove(appDispenserDispensingEntry);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}