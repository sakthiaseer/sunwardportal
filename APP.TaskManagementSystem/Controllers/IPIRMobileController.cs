﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class IPIRMobileController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _config;

        public IPIRMobileController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment hostingEnvironment, GenerateDocumentNoSeries generateDocumentNoSeries, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _config = config;
        }

        [HttpGet]
        [Route("GetIPIRMobileItems")]
        public List<IPIRMobileModel> GetIPIRMobileItems()
        {
            List<IPIRMobileModel> iPIRMobileModels = new List<IPIRMobileModel>();
            var ipirMobileItems = _context.Ipirmobile
                                    .Include(s => s.IntendedAction)
                                    .Include(s => s.Company)
                                    .Include(s => s.StatusCode)
                                    .Include(s => s.AddedByUser)
                                    .Include(s => s.ModifiedByUser)
                                    .OrderByDescending(o => o.IpirmobileId).AsNoTracking().ToList();
            ipirMobileItems.ForEach(s =>
            {
                IPIRMobileModel iPIRMobileModel = new IPIRMobileModel
                {
                    IpirmobileId = s.IpirmobileId,
                    CompanyName = s.Company?.PlantCode,
                    IntendedAction = s.IntendedAction?.CodeValue,
                    IpirreportNo = s.IpirreportNo,
                    Location = s.Location,
                    ProductionOrderNo = s.ProductionOrderNo,
                    ProductName = s.ProductName,
                    BatchNo = s.BatchNo,
                    IssueDescription = s.IssueDescription,
                    Photo = s.Photo,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode?.CodeValue,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SessionId = s.SessionId,
                };
                iPIRMobileModels.Add(iPIRMobileModel);

            });
            return iPIRMobileModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<IPIRMobileModel> GetData(SearchModel searchModel)
        {
            var ipirmobile = new Ipirmobile();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).FirstOrDefault();
                        break;
                    case "Last":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).LastOrDefault();
                        break;
                    case "Next":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).LastOrDefault();
                        break;
                    case "Previous":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).FirstOrDefault();
                        break;
                    case "Last":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).LastOrDefault();
                        break;
                    case "Next":
                        ipirmobile = _context.Ipirmobile.OrderBy(o => o.IpirmobileId).FirstOrDefault(s => s.IpirmobileId > searchModel.Id);
                        break;
                    case "Previous":
                        ipirmobile = _context.Ipirmobile.OrderByDescending(o => o.IpirmobileId).FirstOrDefault(s => s.IpirmobileId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<IPIRMobileModel>(ipirmobile);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertIpirMobile")]
        public IPIRMobileModel Post(IPIRMobileModel value)
        {
            try
            {
                value.ProfileId = _context.ApplicationForm.Where(s => s.ScreenId == "IPIR001")?.FirstOrDefault()?.ProfileId;
                var plant = _context.Plant.FirstOrDefault(p => p.NavCompanyName == value.CompanyName);
                var reportNo = "";
                if (value.ProfileId > 0)
                {
                    reportNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "IPIR Mobile" });
                }
                var sessionId = value.SessionId ?? Guid.NewGuid();
                var ipirmobile = new Ipirmobile
                {
                    IntendedActionId = value.IntendedActionId,
                    IpirreportNo = reportNo,
                    Location = value.Location,
                    ProductionOrderNo = value.ProductionOrderNo,
                    ProductName = value.ProductName,
                    BatchNo = value.BatchNo,
                    IssueDescription = value.IssueDescription,
                    Photo = value.Photo,
                    AddedDate = DateTime.Now,
                    CompanyId = plant.PlantId,
                    StatusCodeId = 1,
                    SessionId = sessionId,
                };

                if (value.PhotoSource != null && value.PhotoSource.Length > 0)
                {
                    var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.Photo;
                    System.IO.File.WriteAllBytes(serverPath, value.PhotoSource);
                }

                _context.Ipirmobile.Add(ipirmobile);
                _context.SaveChanges();
                value.IpirmobileId = ipirmobile.IpirmobileId;
                return value;

            }
            catch (Exception ex)
            {
                value.IsError = true;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return value;
            }
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateIpirMobile")]
        public IPIRMobileModel Put(IPIRMobileModel value)
        {
            var ipirItem = _context.Ipirmobile.FirstOrDefault(f => f.IpirmobileId == value.IpirmobileId);
            ipirItem.IntendedActionId = value.IntendedActionId;
            ipirItem.IpirreportNo = value.IpirreportNo;
            ipirItem.Location = value.Location;
            ipirItem.ProductionOrderNo = value.ProductionOrderNo;
            ipirItem.ProductName = value.ProductName;
            ipirItem.BatchNo = value.BatchNo;
            ipirItem.IssueDescription = value.IssueDescription;
            ipirItem.Photo = value.Photo;
            ipirItem.ModifiedDate = DateTime.Now;
            ipirItem.StatusCodeId = value.StatusCodeID.Value;
            ipirItem.SessionId = value.SessionId ?? Guid.NewGuid();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteIpirMobile")]
        public void Delete(int id)
        {
            try
            {
                var ipirmobile = _context.Ipirmobile.SingleOrDefault(p => p.IpirmobileId == id);
                if (ipirmobile != null)
                {
                    _context.Ipirmobile.Remove(ipirmobile);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Notice Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}

