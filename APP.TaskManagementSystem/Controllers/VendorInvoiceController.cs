﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class VendorInvoiceController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public VendorInvoiceController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project

        [HttpGet]
        [Route("GetVendorInvoice")]
        public List<VendorInvoiceModel> Get()
        {
            var vendorInvoice = _context.VendorInvoice.Select(s => new VendorInvoiceModel
            {
               InvoiceID = s.InvoiceId,
               GstRegistrationNo = s.GstRegistrationNo,
               VendorListID = s.VendorListId.Value,


            }).OrderByDescending(o => o.VendorListID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<VendorInvoiceModel>>(VendorInvoice);
            return vendorInvoice;
        }
        [HttpGet]
        [Route("GetActiveVendorInvoice")]
        public List<VendorInvoiceModel> GetActiveVendorInvoice()
        {
            var vendorInvoice = _context.VendorInvoice.Select(s => new VendorInvoiceModel
            {
                InvoiceID = s.InvoiceId,
                GstRegistrationNo = s.GstRegistrationNo,
                VendorListID = s.VendorListId.Value,

            }).OrderByDescending(o => o.InvoiceID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<VendorInvoiceModel>>(VendorInvoice);
            return vendorInvoice;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskMaster/{id:int}")]
        public ActionResult<VendorInvoiceModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var vendorInvoice = _context.VendorsList.SingleOrDefault(p => p.VendorsListId == id.Value);
            var result = _mapper.Map<VendorInvoiceModel>(vendorInvoice);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<VendorInvoiceModel> GetData(SearchModel searchModel)
        {
            var vendorInvoice = new VendorInvoice();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).LastOrDefault();
                        break;
                    case "Next":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).LastOrDefault();
                        break;
                    case "Previous":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).FirstOrDefault();
                        break;
                    case "Last":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).LastOrDefault();
                        break;
                    case "Next":
                        vendorInvoice = _context.VendorInvoice.OrderBy(o => o.InvoiceId).FirstOrDefault(s => s.InvoiceId > searchModel.Id);
                        break;
                    case "Previous":
                        vendorInvoice = _context.VendorInvoice.OrderByDescending(o => o.InvoiceId).FirstOrDefault(s => s.InvoiceId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<VendorInvoiceModel>(vendorInvoice);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertVendorInvoice")]
        public VendorInvoiceModel Post(VendorInvoiceModel value)
        {
            var vendorInvoice = new VendorInvoice
            {

                GstRegistrationNo = value.GstRegistrationNo,
                VendorListId = value.VendorListID,
               

            };
            _context.VendorInvoice.Add(vendorInvoice);
            _context.SaveChanges();
            value.InvoiceID = vendorInvoice.InvoiceId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateVendorInvoice")]
        public VendorInvoiceModel Put(VendorInvoiceModel value)
        {
            var vendorInvoice = _context.VendorInvoice.SingleOrDefault(p => p.InvoiceId == value.InvoiceID);
            vendorInvoice.GstRegistrationNo = value.GstRegistrationNo;
            vendorInvoice.VendorListId = value.VendorListID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteVendorInvoice")]
        public void Delete(int id)
        {
            var vendorInvoice = _context.VendorInvoice.SingleOrDefault(p => p.InvoiceId == id);
            if (vendorInvoice != null)
            {
                _context.VendorInvoice.Remove(vendorInvoice);
                _context.SaveChanges();
            }
        }
    }
}