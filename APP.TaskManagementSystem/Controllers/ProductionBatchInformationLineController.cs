﻿using System;
using System.Collections.Generic;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionBatchInformationLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ProductionBatchInformationLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavProductCode")]
        public List<NavproductCode> GetNavProductCode()
        {
            List<NavproductCode> navproductCodes = new List<NavproductCode>();
            var productCode = _context.NavproductCode.OrderByDescending(o => o.NavproductCodeId).AsNoTracking().ToList();
            if (productCode != null && productCode.Count > 0)
            {
                productCode.ForEach(s =>
                {
                    NavproductCode navproductCode = new NavproductCode
                    {
                        NavproductCodeId = s.NavproductCodeId,
                        ProductCode = s.ProductCode,
                        ProductCodeName = s.ProductCodeName,
                        CompanyId = s.CompanyId
                    };
                    navproductCodes.Add(navproductCode);

                });
            }

            return navproductCodes;
        }
        [HttpGet]
        [Route("GetProductionBatchInformationLinesByID")]
        public List<ProductionBatchInformationLineModel> Get(int id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<ProductionBatchInformationLineModel> productionBatchInformationLineModels = new List<ProductionBatchInformationLineModel>();
            var productionBatchInformationLine = _context.ProductionBatchInformationLine.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.Item).Include(i => i.ProductCode).Include(i => i.StatusCode).Where(p => p.ProductionBatchInformationId == id).OrderByDescending(o => o.ProductionBatchInformationLineId).AsNoTracking().ToList();
            if (productionBatchInformationLine != null && productionBatchInformationLine.Count > 0)
            {
                productionBatchInformationLine.ForEach(s =>
                {
                    ProductionBatchInformationLineModel productionBatchInformationLineModel = new ProductionBatchInformationLineModel
                    {
                        ProductionBatchInformationLineID = s.ProductionBatchInformationLineId,
                        ProductionBatchInformationID = s.ProductionBatchInformationId,
                        ProductCodeID = s.ProductCodeId,
                        ProductCode = s.ProductCode != null ? s.ProductCode.ProductCode : "",
                        ProductCodeName = s.ProductCode != null ? s.ProductCode.ProductCodeName : "",
                        ItemId = s.ItemId,
                        Product = s.Item != null ? s.Item.No : "",
                        ProductUOM = s.Item != null ? s.Item.BaseUnitofMeasure : "",
                        BatchNo = s.BatchNo,
                        QtyPack = s.QtyPack,
                        QtyPackUnitsID = s.QtyPackUnitsId,
                        ExpiryDate = s.ExpiryDate,
                        PackingUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.QtyPackUnitsId).Select(a => a.Value).SingleOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    productionBatchInformationLineModels.Add(productionBatchInformationLineModel);
                });
            }

            return productionBatchInformationLineModels;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionBatchInformationLineModel> GetData(SearchModel searchModel)
        {
            var productionBatchInformationLine = new ProductionBatchInformationLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).LastOrDefault();
                        break;
                    case "Previous":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).FirstOrDefault();
                        break;
                    case "Last":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).LastOrDefault();
                        break;
                    case "Next":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderBy(o => o.ProductionBatchInformationLineId).FirstOrDefault(s => s.ProductionBatchInformationLineId > searchModel.Id);
                        break;
                    case "Previous":
                        productionBatchInformationLine = _context.ProductionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).FirstOrDefault(s => s.ProductionBatchInformationLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionBatchInformationLineModel>(productionBatchInformationLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionBatchInformationLine")]
        public ProductionBatchInformationLineModel Post(ProductionBatchInformationLineModel value)
        {

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionBatchInformationLine = new ProductionBatchInformationLine
            {
                ItemId = value.ItemId,
                ProductionBatchInformationId = value.ProductionBatchInformationID,
                ProductCodeId = value.ProductCodeID,
                BatchNo = value.BatchNo,
                QtyPack = value.QtyPack,
                QtyPackUnitsId = value.QtyPackUnitsID,
                ExpiryDate = value.ExpiryDate,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,

            };
            _context.ProductionBatchInformationLine.Add(productionBatchInformationLine);
            _context.SaveChanges();
            value.ProductionBatchInformationLineID = productionBatchInformationLine.ProductionBatchInformationLineId;
            value.Product = _context.Navitems.Where(n => n.ItemId == value.ItemId).Select(t => t.No).FirstOrDefault();
            value.PackingUnits = value.QtyPackUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.QtyPackUnitsID).Select(m => m.Value).FirstOrDefault() : "";

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionBatchInformationLine")]
        public ProductionBatchInformationLineModel Put(ProductionBatchInformationLineModel value)
        {

            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var productionBatchInformationLine = _context.ProductionBatchInformationLine.SingleOrDefault(p => p.ProductionBatchInformationLineId == value.ProductionBatchInformationLineID);
            productionBatchInformationLine.ProductionBatchInformationId = value.ProductionBatchInformationID;
            productionBatchInformationLine.BatchNo = value.BatchNo;
            productionBatchInformationLine.ProductCodeId = value.ProductCodeID;
            productionBatchInformationLine.ItemId = value.ItemId;
            productionBatchInformationLine.QtyPack = value.QtyPack;
            productionBatchInformationLine.QtyPackUnitsId = value.QtyPackUnitsID;
            productionBatchInformationLine.ExpiryDate = value.ExpiryDate;
            productionBatchInformationLine.ModifiedByUserId = value.ModifiedByUserID;
            productionBatchInformationLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            value.Product = _context.Navitems.Where(n => n.ItemId == value.ItemId).Select(t => t.No).FirstOrDefault();
            value.PackingUnits = value.QtyPackUnitsID != 0 && applicationmasterdetail.Count > 0 ? applicationmasterdetail.Where(m => m.ApplicationMasterDetailId == value.QtyPackUnitsID).Select(m => m.Value).FirstOrDefault() : "";

            return value;

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionBatchInformationLine")]
        public void Delete(int id)
        {
            var ProductionBatchInformationLine = _context.ProductionBatchInformationLine.SingleOrDefault(p => p.ProductionBatchInformationLineId == id);
            if (ProductionBatchInformationLine != null)
            {
                _context.ProductionBatchInformationLine.Remove(ProductionBatchInformationLine);
                _context.SaveChanges();
            }
        }
        [HttpGet]
        [Route("GetProductionBatchNos")]
        public List<ProductionBatchInformationLineModel> GetProductionBatchNos()
        {
            List<ProductionBatchInformationLineModel> productionBatchInformationLineModels = new List<ProductionBatchInformationLineModel>();
            var productionBatchInformationLine = _context.ProductionBatchInformationLine.Where(l => l.BatchNo != null).OrderByDescending(o => o.ProductionBatchInformationLineId).AsNoTracking().Select(s => s.BatchNo).Distinct().ToList();
            if (productionBatchInformationLine != null && productionBatchInformationLine.Count > 0)
            {
                productionBatchInformationLine.ForEach(s =>
                {
                    ProductionBatchInformationLineModel productionBatchInformationLineModel = new ProductionBatchInformationLineModel();
                    productionBatchInformationLineModel.BatchNo = s;
                    productionBatchInformationLineModels.Add(productionBatchInformationLineModel);
                });
            }
            return productionBatchInformationLineModels;
        }
        [HttpPost]
        [Route("GetProductionBatchQRCodeReport")]
        public List<ProductionBatchInformationReportModel> GetProductionBatchQRCodeReport(ProductionBatchParam productionBatchParam)
        {
            List<ProductionBatchInformationReportModel> productionBatchInformationReportModels = new List<ProductionBatchInformationReportModel>();

            var productionBatchInformationLine = _context.ProductionBatchInformationLine.Include(i => i.ProductCode).Include(i => i.Item).Include(p => p.ProductionBatchInformation).Where(s => s.BatchNo != "" && s.ExpiryDate != null && s.ProductionBatchInformation.ManufacturingStartDate != null && s.ProductCodeId > 0 && s.Item.Description != "").GroupBy(x => new { x.BatchNo, x.ProductCodeId, x.ExpiryDate, x.ProductionBatchInformation.ManufacturingStartDate, x.Item.Description, x.ItemId, x.ProductCode.ProductCode }).Select(s => new { s.Key.BatchNo, s.Key.ExpiryDate, s.Key.ProductCodeId, s.Key.ManufacturingStartDate, s.Key.Description, s.Key.ProductCode, s.Key.ItemId });
            if (productionBatchParam.TillDate != null)
            {
                productionBatchInformationLine = productionBatchInformationLine.Where(p => p.ManufacturingStartDate <= productionBatchParam.TillDate);
            }
            if (productionBatchParam.BatchNos.Count > 0)
            {
                productionBatchInformationLine = productionBatchInformationLine.Where(p => productionBatchParam.BatchNos.Contains(p.BatchNo));
            }
            var productionBatchInformationLines = productionBatchInformationLine.AsNoTracking().ToList();

            if (productionBatchInformationLines != null && productionBatchInformationLines.Count > 0)
            {
                int sno = 1;
                productionBatchInformationLines.ForEach(s =>
                {
                    ProductionBatchInformationReportModel productionBatchInformationLineModel = new ProductionBatchInformationReportModel();
                    productionBatchInformationLineModel.BatchNo = s.BatchNo;
                    productionBatchInformationLineModel.ManufacturingDate = s.ManufacturingStartDate;
                    productionBatchInformationLineModel.ExpiryDate = s.ExpiryDate;
                    productionBatchInformationLineModel.ProductDescription = s.Description;
                    productionBatchInformationLineModel.DisposalDate = s.ExpiryDate?.AddYears(1);
                    productionBatchInformationLineModel.ProductionBatchInformationLineID = sno;
                    productionBatchInformationLineModel.ItemId = s.ItemId;
                    productionBatchInformationLineModel.ProductCode = s.ProductCode;
                    productionBatchInformationReportModels.Add(productionBatchInformationLineModel);
                    sno++;
                });
            }
            return productionBatchInformationReportModels;
        }
        [HttpPost]
        [Route("GetProductionBatchInformationReport")]
        public List<ProductionBatchInformationReportModel> GetProductionBatchInformationReport(ProductionBatchParam productionBatchParam)
        {
            List<ProductionBatchInformationReportModel> productionBatchInformationReportModels = new List<ProductionBatchInformationReportModel>();

            var productionBatchInformationLine = _context.ProductionBatchInformationLine.Include(i => i.Item).Include(p => p.ProductionBatchInformation).Include(b => b.ProductionBatchInformation.Plant).Where(s => s.ProductionBatchInformationLineId > 0);
            if (productionBatchParam.TillDate != null)
            {
                productionBatchInformationLine = productionBatchInformationLine.Where(p => p.ProductionBatchInformation.ManufacturingStartDate <= productionBatchParam.TillDate);
            }
            if (productionBatchParam.BatchNos.Count > 0)
            {
                productionBatchInformationLine = productionBatchInformationLine.Where(p => productionBatchParam.BatchNos.Contains(p.BatchNo));
            }
            var productionBatchInformationLines = productionBatchInformationLine.OrderByDescending(o => o.ProductionBatchInformationLineId).AsNoTracking().ToList();
            if (productionBatchInformationLines != null && productionBatchInformationLines.Count > 0)
            {
                var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                productionBatchInformationLines.ForEach(s =>
                {
                    ProductionBatchInformationReportModel productionBatchInformationLineModel = new ProductionBatchInformationReportModel();

                    productionBatchInformationLineModel.ProductionBatchInformationLineID = s.ProductionBatchInformationLineId;
                    productionBatchInformationLineModel.ProductionBatchInformationID = s.ProductionBatchInformationId != null ? s.ProductionBatchInformationId.Value : 0;
                    productionBatchInformationLineModel.BatchNo = s.BatchNo;
                    productionBatchInformationLineModel.ManufacturingDate = s.ProductionBatchInformation?.ManufacturingStartDate;
                    productionBatchInformationLineModel.ExpiryDate = s.ExpiryDate;
                    productionBatchInformationLineModel.ProductDescription = s.Item?.Description;
                    productionBatchInformationLineModel.DisposalDate = s.ExpiryDate?.AddYears(1);
                    productionBatchInformationLineModel.QtyPackUnits = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.QtyPackUnitsId).Select(a => a.Value).SingleOrDefault() : "";
                    productionBatchInformationLineModel.QtyPack = s.QtyPack;
                    productionBatchInformationLineModel.CompanyName = s.ProductionBatchInformation != null && s.ProductionBatchInformation.Plant != null ? s.ProductionBatchInformation.Plant.PlantCode : "";
                    productionBatchInformationLineModel.TicketNo = s.ProductionBatchInformation?.TicketNo;
                    productionBatchInformationLineModel.ProductionOrderNo = s.ProductionBatchInformation?.ProductionOrderNo;
                    productionBatchInformationLineModel.UnitsofBatchSize = s.ProductionBatchInformation != null && applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.ProductionBatchInformation.UnitsofBatchSize).Select(a => a.Value).SingleOrDefault() : "";
                    productionBatchInformationReportModels.Add(productionBatchInformationLineModel);
                });

                /*productionBatchInformationReportModels.ForEach(p =>
                {
                    p.QRCodeData += p.ProductionBatchInformationLineID.ToString();
                    if (!string.IsNullOrEmpty(p.ProductDescription))
                    {
                        p.QRCodeData += '|' + p.ProductDescription;
                    }
                    if (!string.IsNullOrEmpty(p.BatchNo))
                    {
                        p.QRCodeData += '|' + p.BatchNo;
                    }
                    if (p.ManufacturingDate != null)
                    {
                        p.QRCodeData += '|' + p.ManufacturingDate.Value.ToString("dd-MMM-yyyy");
                    }
                    if (p.ExpiryDate != null)
                    {
                        p.QRCodeData += '|' + p.ExpiryDate.Value.ToString("dd-MMM-yyyy");
                    }
                    if (p.DisposalDate != null)
                    {
                        p.QRCodeData += '|' + p.DisposalDate.Value.ToString("dd-MMM-yyyy");
                    }
                });*/
            }

            return productionBatchInformationReportModels;
        }
    }
}