﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DivisionController : ControllerBase
    {

        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DivisionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDivisions")]
        public List<DivisionModel> Get()
        {
            var division = _context.Division.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(c => c.Company).Include("StatusCode").AsNoTracking().ToList();
            List<DivisionModel> divisionModel = new List<DivisionModel>();
            division.ForEach(s =>
            {
                DivisionModel divisionModels = new DivisionModel
                {
                    DivisionID = s.DivisionId,
                    CompanyID = s.CompanyId,
                    CompanyName = s.Company != null ? s.Company.Description : "",
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    DivisionCompanyName = (s.Company != null ? s.Company.Description : "") + '|' + s.Name,
                };
                divisionModel.Add(divisionModels);
            });
            return divisionModel.OrderByDescending(a => a.DivisionID).ToList();
        }
        [HttpGet]
        [Route("GetDivisionsByCompany")]
        public List<DivisionModel> GetDivisionsByCompany(int id)
        {
            var division = _context.Division.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(c => c.Company).Include("StatusCode").AsNoTracking().Where(d=>d.CompanyId == id).ToList();
            List<DivisionModel> divisionModel = new List<DivisionModel>();
            division.ForEach(s =>
            {
                DivisionModel divisionModels = new DivisionModel
                {
                    DivisionID = s.DivisionId,
                    CompanyID = s.CompanyId,
                    CompanyName = s.Company != null ? s.Company.Description : "",
                    Name = s.Name,
                    Code = s.Code,
                    Description = s.Description,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                };
                divisionModel.Add(divisionModels);
            });
            return divisionModel.OrderByDescending(a => a.DivisionID).ToList();
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<DivisionModel> GetData(SearchModel searchModel)
        {
            var division = new Division();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).FirstOrDefault();
                        break;
                    case "Last":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).LastOrDefault();
                        break;
                    case "Next":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).LastOrDefault();
                        break;
                    case "Previous":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).FirstOrDefault();
                        break;
                    case "Last":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).LastOrDefault();
                        break;
                    case "Next":
                        division = _context.Division.OrderBy(o => o.DivisionId).FirstOrDefault(s => s.DivisionId > searchModel.Id);
                        break;
                    case "Previous":
                        division = _context.Division.OrderByDescending(o => o.DivisionId).FirstOrDefault(s => s.DivisionId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<DivisionModel>(division);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertDivision")]
        public DivisionModel Post(DivisionModel value)
        {
            var division = new Division
            {
                CompanyId = value.CompanyID,
                Name = value.Name,
                Code = value.Code,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.Division.Add(division);
            _context.SaveChanges();
            value.DivisionID = division.DivisionId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDivision")]
        public DivisionModel Put(DivisionModel value)
        {
            var division = _context.Division.SingleOrDefault(p => p.DivisionId == value.DivisionID);

            division.CompanyId = value.CompanyID;
            division.Code = value.Code;
            division.Description = value.Description;
            division.Name = value.Name;
            division.ModifiedByUserId = value.ModifiedByUserID;
            division.ModifiedDate = DateTime.Now;
            division.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteDivision")]
        public void Delete(int id)
        {
            try
            {
                var division = _context.Division.SingleOrDefault(p => p.DivisionId == id);
                if (division != null)
                {
                    _context.Division.Remove(division);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Division Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}