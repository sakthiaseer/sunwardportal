﻿using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using APP.TaskManagementSystem.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TemplateTestCaseController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IHubContext<ChatHub> _hub;
        private readonly MailService _mailService;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public TemplateTestCaseController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository, GenerateDocumentNoSeries generate, IHubContext<ChatHub> hub, MailService mailService, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
            _hub = hub;
            _generateDocumentNoSeries = generate;
            _mailService = mailService;
            _hostingEnvironment = host;
        }
        #region TemplateTestCase
        [HttpGet]
        [Route("GetTemplateTestCase")]
        public List<TemplateTestCaseModel> GetTemplateTestCase()
        {
            var templateTestCase = _context.TemplateTestCase
                                .Include(r => r.Company)
                                .Include(m => m.Department)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(s => s.Profile)
                                .Include(a => a.TemplateTestCaseDepartment)
                                .AsNoTracking().OrderByDescending(o => o.TemplateTestCaseId).ToList();
            List<TemplateTestCaseModel> templateTestCaseModels = new List<TemplateTestCaseModel>();
            if (templateTestCase != null && templateTestCase.Count > 0)
            {
                var templateTestCaseIds = templateTestCase.Select(s => s.TemplateTestCaseId).ToList();
                var templateTestCaseLink = _context.TemplateTestCaseLink.Select(n => new TemplateTestCaseLinkModel { DocumentNo = n.DocumentNo, TemplateTestCaseLinkId = n.TemplateTestCaseLinkId, TemplateTestCaseId = n.TemplateTestCaseId, NameOfTemplate = n.NameOfTemplate, Subject = n.Subject, Link = n.Link, LocationName = n.LocationName, Naming = n.Naming, IsAutoNumbering = n.IsAutoNumbering, TemplateProfileId = n.TemplateProfileId, TemplateProfileName = n.TemplateProfile.Name, AutoNumbering = n.IsAutoNumbering == true ? "Yes" : "No", LocationToSaveId = n.LocationToSaveId, TemplateCaseFormId = n.TemplateCaseFormId }).Where(w => templateTestCaseIds.Contains(w.TemplateTestCaseId.Value) && w.TemplateCaseFormId == null).ToList();

                templateTestCase.ForEach(s =>
                {
                    TemplateTestCaseModel templateTestCaseModel = new TemplateTestCaseModel();
                    templateTestCaseModel.TemplateTestCaseId = s.TemplateTestCaseId;
                    templateTestCaseModel.CompanyId = s.CompanyId;
                    templateTestCaseModel.ProfileId = s.ProfileId;
                    templateTestCaseModel.ProfileName = s.Profile?.Name;
                    templateTestCaseModel.TemplateCode = s.TemplateCode;
                    templateTestCaseModel.TemplateName = s.TemplateName;
                    templateTestCaseModel.CompanyName = s.Company?.Description;
                    templateTestCaseModel.DepartmentId = s.DepartmentId;
                    templateTestCaseModel.DepartmentName = s.Department?.Name;
                    templateTestCaseModel.Instruction = s.Instruction;
                    templateTestCaseModel.ModifiedByUserID = s.AddedByUserId;
                    templateTestCaseModel.AddedByUserID = s.ModifiedByUserId;
                    templateTestCaseModel.StatusCodeID = s.StatusCodeId;
                    templateTestCaseModel.AddedByUser = s.AddedByUser.UserName;
                    templateTestCaseModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    templateTestCaseModel.AddedDate = s.AddedDate;
                    templateTestCaseModel.ModifiedDate = s.ModifiedDate;
                    templateTestCaseModel.StatusCode = s.StatusCode.CodeValue;
                    templateTestCaseModel.SessionId = s.SessionId;
                    templateTestCaseModel.VersionNo = s.VersionNo;
                    templateTestCaseModel.SpecificCase = s.SpecificCase;
                    templateTestCaseModel.DepartmentIds = s.TemplateTestCaseDepartment != null ? s.TemplateTestCaseDepartment.Select(d => d.DepartmentId).ToList() : new List<long?>();
                    templateTestCaseModel.templateTestCaseLinkModels = templateTestCaseLink.Where(t => t.TemplateTestCaseId == s.TemplateTestCaseId).ToList();

                    templateTestCaseModels.Add(templateTestCaseModel);
                });
            }
            return templateTestCaseModels;
        }
        [HttpPost]
        [Route("TemplateTestChecListInherit")]
        public SearchModel TemplateTestChecListInherit(SearchModel searchModel)
        {
            var templateTestCaseCheckListResponseId = _context.TemplateTestCaseCheckListResponse.Select(s => new { s.TemplateTestCaseCheckListId, s.TemplateTestCaseCheckListResponseId }).Where(w => searchModel.Ids.Contains(w.TemplateTestCaseCheckListId)).Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
            if (templateTestCaseCheckListResponseId != null && templateTestCaseCheckListResponseId.Count > 0)
            {
                var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseId.Contains(w.TemplateTestCaseCheckListResponseId.Value)).Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.CompanyId }).ToList();
                var companyIds = templateTestCaseCheckListResponseDuty.Where(w => w.CompanyId > 0).Select(s => s.CompanyId).Distinct().ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.UserGroupId, s.UserId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
                var templateTestCaseCheckListResponseIdExit = _context.TemplateTestCaseCheckListResponse.FirstOrDefault(f => f.TemplateTestCaseCheckListId == searchModel.Id)?.TemplateTestCaseCheckListResponseId;
                if (templateTestCaseCheckListResponseIdExit == null)
                {
                    var templateTestCaseCheckListResponseInsert = new TemplateTestCaseCheckListResponse
                    {
                        TemplateTestCaseCheckListId = searchModel.Id,
                        AddedByUserId = searchModel.UserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = 1,
                        ModifiedByUserId = searchModel.UserID,
                        ModifiedDate = DateTime.Now,
                        DueDate = DateTime.Now,
                        SessionId = Guid.NewGuid(),
                    };
                    _context.TemplateTestCaseCheckListResponse.Add(templateTestCaseCheckListResponseInsert);
                    _context.SaveChanges();
                    templateTestCaseCheckListResponseIdExit = templateTestCaseCheckListResponseInsert.TemplateTestCaseCheckListResponseId;
                }
                var count = 0;
                if (companyIds != null && companyIds.Count > 0)
                {
                    companyIds.ForEach(a =>
                    {

                        var templateTestCaseCheckListResponseDutyIs = templateTestCaseCheckListResponseDuty.Where(w => w.CompanyId == a).ToList();
                        if (templateTestCaseCheckListResponseDutyIs != null && templateTestCaseCheckListResponseDutyIs.Count > 0)
                        {
                            templateTestCaseCheckListResponseDutyIs.ForEach(b =>
                            {
                                var checktemplateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Where(f => f.CompanyId == a && f.TemplateTestCaseCheckListResponseId == templateTestCaseCheckListResponseIdExit).FirstOrDefault()?.TemplateTestCaseCheckListResponseDutyId;
                                if (checktemplateTestCaseCheckListResponseDuty == null)
                                {
                                    var templateTestCaseCheckListResponseDuty = new TemplateTestCaseCheckListResponseDuty
                                    {
                                        TemplateTestCaseCheckListResponseId = templateTestCaseCheckListResponseIdExit,
                                        CompanyId = a,
                                        StatusCodeId = 1,
                                        AddedByUserId = searchModel.UserID,
                                        ModifiedByUserId = searchModel.UserID,
                                        AddedDate = DateTime.Now,
                                        ModifiedDate = DateTime.Now,
                                        Type = "Responsibility",
                                    };
                                    _context.TemplateTestCaseCheckListResponseDuty.Add(templateTestCaseCheckListResponseDuty);
                                    _context.SaveChanges();
                                    checktemplateTestCaseCheckListResponseDuty = templateTestCaseCheckListResponseDuty.TemplateTestCaseCheckListResponseDutyId;
                                }
                                if (count == 0)
                                {
                                    searchModel.MasterTypeID = a;
                                    searchModel.ParentID = checktemplateTestCaseCheckListResponseDuty;
                                }
                                var responsibleUser = templateTestCaseCheckListResponseResponsible.Where(c => c.TemplateTestCaseCheckListResponseDutyId == b.TemplateTestCaseCheckListResponseDutyId).ToList();
                                if (responsibleUser != null && responsibleUser.Count > 0)
                                {
                                    responsibleUser.ForEach(r =>
                                    {
                                        var check = _context.TemplateTestCaseCheckListResponseResponsible.Select(z => new { z.EmployeeId, z.TemplateTestCaseCheckListResponseDutyId }).Where(d => d.EmployeeId == r.EmployeeId && d.TemplateTestCaseCheckListResponseDutyId == checktemplateTestCaseCheckListResponseDuty).Count();
                                        if (check == 0)
                                        {
                                            var templateTestCaseCheckListResponseResponsibleAdd = new TemplateTestCaseCheckListResponseResponsible
                                            {
                                                TemplateTestCaseCheckListResponseDutyId = checktemplateTestCaseCheckListResponseDuty,
                                                UserId = r.UserId,
                                                EmployeeId = r.EmployeeId,
                                                UserGroupId = r.UserGroupId,
                                            };
                                            _context.TemplateTestCaseCheckListResponseResponsible.Add(templateTestCaseCheckListResponseResponsibleAdd);
                                            _context.SaveChanges();
                                        }
                                    });
                                }
                            });
                        }
                        count++;
                    });
                }
            }
            return searchModel;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseProposalUsers")]
        public TemplateTestCaseProposalModel GetTemplateTestCaseProposalUsers(long? id)
        {
            var companyId = _context.TemplateTestCaseForm.FirstOrDefault(f => f.TemplateTestCaseFormId == id)?.CompanyId;
            TemplateTestCaseProposalModel templateTestCaseLinkModels = new TemplateTestCaseProposalModel();
            List<string> userTypeGroup = new List<string>() { "Group Companies", "Super User" };
            var templateTestCaseProposal = _context.TemplateTestCaseProposal.Where(w => w.TemplateCaseFormId == id && userTypeGroup.Contains(w.UserType)).ToList();
            List<long?> proposalIds = new List<long?>();
            if (templateTestCaseProposal != null)
            {
                proposalIds.AddRange(templateTestCaseProposal.Where(w => w.UserId != null).Select(s => s.UserId).ToList());
                proposalIds.AddRange(templateTestCaseProposal.Where(w => w.ReassignUserId != null).Select(s => s.ReassignUserId).ToList());
            }
            List<string> userTypeSpecific = new List<string>() { "Specific Company", "Proposal" };
            var templateTestCaseProposalByPropsal = _context.TemplateTestCaseProposal.Where(w => w.TemplateCaseFormId == id && userTypeSpecific.Contains(w.UserType)).ToList();
            List<long?> proposalIdsPro = new List<long?>();
            if (templateTestCaseProposalByPropsal != null)
            {
                proposalIdsPro.AddRange(templateTestCaseProposalByPropsal.Where(w => w.UserId != null).Select(s => s.UserId).ToList());
                proposalIdsPro.AddRange(templateTestCaseProposalByPropsal.Where(w => w.ReassignUserId != null).Select(s => s.ReassignUserId).ToList());
                var employee = _context.Employee.Select(s => new { s.UserId, s.PlantId }).Where(a => proposalIdsPro.Contains(a.UserId) && a.PlantId == companyId).Select(s => s.UserId).ToList();
                if (employee.Count > 0 && employee != null)
                {
                    proposalIds.AddRange(employee);
                }
            }
            templateTestCaseLinkModels.TemplateTestCaseProposalId = 0;
            templateTestCaseLinkModels.UserIds = proposalIds.Distinct().ToList();
            var appUsers = _context.ApplicationUser.Where(w => templateTestCaseLinkModels.UserIds.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList();
            templateTestCaseLinkModels.ParticipantUsers = string.Join(",", appUsers);
            return templateTestCaseLinkModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseByCompanyProposalUsers")]
        public TemplateTestCaseProposalModel GetTemplateTestCaseByCompanyProposalUsers(long? id, long? caseFormId)
        {
            TemplateTestCaseProposalModel templateTestCaseLinkModels = new TemplateTestCaseProposalModel();
            var companyId = _context.TemplateTestCaseForm.FirstOrDefault(f => f.TemplateTestCaseFormId == caseFormId)?.CompanyId;
            List<string> userTypeSpecific = new List<string>() { "Specific Company", "Proposal" };
            var templateTestCaseProposal = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == id && w.TemplateCaseFormId == caseFormId && userTypeSpecific.Contains(w.UserType)).ToList();
            List<long?> proposalIds = new List<long?>();
            if (templateTestCaseProposal != null)
            {
                proposalIds.AddRange(templateTestCaseProposal.Where(w => w.UserId != null).Select(s => s.UserId).ToList());
                proposalIds.AddRange(templateTestCaseProposal.Where(w => w.ReassignUserId != null).Select(s => s.ReassignUserId).ToList());
                var employee = _context.Employee.Select(s => new { s.UserId, s.PlantId }).Where(a => proposalIds.Contains(a.UserId) && a.PlantId == companyId).Select(s => s.UserId).ToList();
                if (employee.Count > 0 && employee != null)
                {
                    proposalIds = employee;
                }
            }
            templateTestCaseLinkModels.TemplateTestCaseProposalId = 0;
            templateTestCaseLinkModels.UserIds = proposalIds.Distinct().ToList();
            var appUsers = _context.ApplicationUser.Where(w => templateTestCaseLinkModels.UserIds.Contains(w.UserId) && w.SessionId != null).Select(s => s.SessionId).ToList();
            templateTestCaseLinkModels.ParticipantUsers = string.Join(",", appUsers);
            return templateTestCaseLinkModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseLink")]
        public List<TemplateTestCaseLinkModel> GetTemplateTestCaseLink(long? id, string type, long typeId)
        {
            List<TemplateTestCaseLinkModel> templateTestCaseLinkModels = new List<TemplateTestCaseLinkModel>();
            var templateTestCaseCheckLists = _context.TemplateTestCaseLink.Include(a => a.TemplateProfile).Include(a => a.LocationToSave).Where(w => w.TemplateTestCaseId == id);
            if (type == "TemplateCaseForm")
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateCaseFormId == typeId);
            }
            else
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateCaseFormId == null);
            }
            var templateTestCaseLink = templateTestCaseCheckLists.AsNoTracking().ToList();
            templateTestCaseLink.ForEach(s =>
            {
                TemplateTestCaseLinkModel templateTestCaseModel = new TemplateTestCaseLinkModel();
                templateTestCaseModel.TemplateTestCaseId = s.TemplateTestCaseId;
                templateTestCaseModel.TemplateTestCaseLinkId = s.TemplateTestCaseLinkId;
                templateTestCaseModel.NameOfTemplate = s.NameOfTemplate;
                templateTestCaseModel.Subject = s.Subject;
                templateTestCaseModel.Link = s.Link;
                templateTestCaseModel.LocationName = s.LocationToSave?.Name;
                templateTestCaseModel.Naming = s.Naming;
                templateTestCaseModel.IsAutoNumbering = s.IsAutoNumbering;
                templateTestCaseModel.AutoNumbering = s.IsAutoNumbering == true ? "Yes" : "No";
                templateTestCaseModel.TemplateProfileId = s.TemplateProfileId;
                templateTestCaseModel.TemplateProfileName = s.TemplateProfile?.Name;
                templateTestCaseModel.DocumentNo = s.DocumentNo;
                templateTestCaseModel.LocationToSaveId = s.LocationToSaveId;
                templateTestCaseLinkModels.Add(templateTestCaseModel);
            });
            return templateTestCaseLinkModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseLinkDocNo")]
        public List<TemplateTestCaseLinkDocModel> GetTemplateTestCaseLinkDocNo(long? id)
        {
            List<TemplateTestCaseLinkDocModel> templateTestCaseLinkModels = new List<TemplateTestCaseLinkDocModel>();
            var templateTestCaseLink = _context.TemplateTestCaseLinkDoc.Include(a => a.AddedByUser).Include(a => a.ModifiedByUser).Where(w => w.TemplateTestCaseLinkId == id && w.SessionId != null).ToList();
            if (templateTestCaseLink != null && templateTestCaseLink.Count > 0)
            {
                var sessionids = templateTestCaseLink.Select(s => s.SessionId).ToList();
                var documents = _context.Documents.Select(s => new { s.SessionId, s.DocumentId }).Where(w => sessionids.Contains(w.SessionId)).ToList();
                templateTestCaseLink.ForEach(s =>
                {
                    var sessionExits = documents.Where(w => w.SessionId == s.SessionId).FirstOrDefault()?.DocumentId;
                    if (sessionExits == null)
                    {
                        TemplateTestCaseLinkDocModel templateTestCaseModel = new TemplateTestCaseLinkDocModel();
                        templateTestCaseModel.TemplateTestCaseLinkDocId = s.TemplateTestCaseLinkDocId;
                        templateTestCaseModel.TemplateTestCaseLinkId = s.TemplateTestCaseLinkId;
                        templateTestCaseModel.DocumentNo = s.DocumentNo;
                        templateTestCaseModel.SessionId = s.SessionId;
                        templateTestCaseModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                        templateTestCaseModel.ModifiedDate = s.ModifiedDate;
                        templateTestCaseLinkModels.Add(templateTestCaseModel);
                    }
                });
            }
            return templateTestCaseLinkModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TemplateTestCaseModel> GetData(SearchModel searchModel)
        {
            var templateTestCase = new TemplateTestCase();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).FirstOrDefault();
                        break;
                    case "Last":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).LastOrDefault();
                        break;
                    case "Next":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).LastOrDefault();
                        break;
                    case "Previous":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).FirstOrDefault();
                        break;
                    case "Last":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).LastOrDefault();
                        break;
                    case "Next":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderBy(o => o.TemplateTestCaseId).FirstOrDefault(s => s.TemplateTestCaseId > searchModel.Id);
                        break;
                    case "Previous":
                        templateTestCase = _context.TemplateTestCase.Include(a => a.TemplateTestCaseDepartment).OrderByDescending(o => o.TemplateTestCaseId).FirstOrDefault(s => s.TemplateTestCaseId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TemplateTestCaseModel>(templateTestCase);
            if (result != null)
            {
                var templateTestCaseLink = _context.TemplateTestCaseLink.Select(n => new TemplateTestCaseLinkModel { DocumentNo = n.DocumentNo, TemplateTestCaseLinkId = n.TemplateTestCaseLinkId, TemplateTestCaseId = n.TemplateTestCaseId, NameOfTemplate = n.NameOfTemplate, Subject = n.Subject, Link = n.Link, LocationName = n.LocationName, Naming = n.Naming, IsAutoNumbering = n.IsAutoNumbering, TemplateProfileId = n.TemplateProfileId, TemplateProfileName = n.TemplateProfile.Name, AutoNumbering = n.IsAutoNumbering == true ? "Yes" : "No", TemplateCaseFormId = n.TemplateCaseFormId }).Where(t => t.TemplateTestCaseId == templateTestCase.TemplateTestCaseId && t.TemplateCaseFormId == null).ToList();
                result.templateTestCaseLinkModels = templateTestCaseLink.Where(t => t.TemplateTestCaseId == templateTestCase.TemplateTestCaseId).ToList();
                result.DepartmentIds = templateTestCase.TemplateTestCaseDepartment != null ? templateTestCase.TemplateTestCaseDepartment.Select(d => d.DepartmentId).ToList() : new List<long?>();

            }
            return result;
        }
        [HttpPost]
        [Route("GenerateTemplateTestCaseDocNo")]
        public TemplateTestCaseLinkDocNoModel GenerateTemplateTestCaseDocNo(TemplateTestCaseLinkDocNoModel value)
        {
            var sessionId = Guid.NewGuid();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = value.StatusCodeID, Title = value.Title });
            value.DocumentNo = profileNo;
            value.SessionId = sessionId;
            var templateCaseNo = new TemplateTestCaseLinkDoc
            {
                TemplateTestCaseLinkId = value.TemplateTestCaseLinkId,
                DocumentNo = value.DocumentNo,
                SessionId = sessionId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
            };
            _context.TemplateTestCaseLinkDoc.Add(templateCaseNo);
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertTemplateTestCase")]
        public TemplateTestCaseModel Post(TemplateTestCaseModel value)
        {
            var sessionId = Guid.NewGuid();
            var templateTestCase = new TemplateTestCase
            {
                TemplateName = value.TemplateName,
                TemplateCode = value.TemplateCode,
                CompanyId = value.CompanyId,
                DepartmentId = value.DepartmentId,
                Instruction = value.Instruction,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = sessionId,
                ProfileId = value.ProfileId,
                VersionNo = 1,
                SpecificCase = value.SpecificCase,
            };

            if (value.DepartmentIds != null && value.DepartmentIds.Count > 0)
            {
                value.DepartmentIds.ForEach(h =>
                {
                    TemplateTestCaseDepartment templateTestCaseDepartment = new TemplateTestCaseDepartment
                    {
                        DepartmentId = h,
                    };
                    templateTestCase.TemplateTestCaseDepartment.Add(templateTestCaseDepartment);
                });
                _context.SaveChanges();
            }
            _context.TemplateTestCase.Add(templateTestCase);
            _context.SaveChanges();
            value.TemplateTestCaseId = templateTestCase.TemplateTestCaseId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseInherit")]
        public TemplateTestCaseModel InsertTemplateTestCaseInherit(TemplateTestCaseModel value)
        {
            var templateNameExits = _context.TemplateTestCase.FirstOrDefault(f => f.TemplateName.ToLower() == value.TemplateName.ToLower());
            value.IsTemplateExits = true;
            if (templateNameExits == null)
            {
                var preTemplateTestCaseId = value.TemplateTestCaseId;
                value.IsTemplateExits = false;
                var sessionId = Guid.NewGuid();
                var templateTestCaseForm = new TemplateTestCase
                {
                    TemplateName = value.TemplateName,
                    TemplateCode = value.TemplateCode,
                    CompanyId = value.CompanyId,
                    DepartmentId = value.DepartmentId,
                    Instruction = value.Instruction,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    ModifiedByUserId = value.AddedByUserID,
                    ModifiedDate = DateTime.Now,
                    SessionId = sessionId,
                    ProfileId = value.ProfileId,
                    VersionNo = 1,
                    SpecificCase = value.SpecificCase,
                };
                _context.TemplateTestCase.Add(templateTestCaseForm);
                _context.SaveChanges();
                value.TemplateTestCaseId = templateTestCaseForm.TemplateTestCaseId;
                value.SessionId = sessionId;
                value.VersionNo = templateTestCaseForm.VersionNo;
                value.AddedByUser = _context.ApplicationUser.FirstOrDefault(f => f.UserId == value.AddedByUserID)?.UserName;
                CreateTemplateTestCaseLink(value, preTemplateTestCaseId);
                CreateTemplateCaseProposalInherit(value, preTemplateTestCaseId);
                CreateNewTempalteCheckListInherit(value, preTemplateTestCaseId);
            }
            return value;
        }
        public void CreateTemplateTestCaseLink(TemplateTestCaseModel value, long? preTemplateTestCaseId)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseLink.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == null).Count();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseProposal = _context.TemplateTestCaseLink.Where(w => w.TemplateTestCaseId == preTemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                if (templateTestCaseProposal != null && templateTestCaseProposal.Count > 0)
                {
                    templateTestCaseProposal.ForEach(s =>
                    {
                        var templateTestCaseLink = new TemplateTestCaseLink
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            Link = s.Link,
                            Subject = s.Subject,
                            NameOfTemplate = s.NameOfTemplate,
                            IsAutoNumbering = s.IsAutoNumbering,
                            TemplateProfileId = s.TemplateProfileId,
                            LocationToSaveId = s.LocationToSaveId,
                            Naming = s.Naming,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                        };
                        _context.TemplateTestCaseLink.Add(templateTestCaseLink);
                    });
                    _context.SaveChanges();
                }
            }
        }
        public void CreateTemplateCaseProposalInherit(TemplateTestCaseModel value, long? preTemplateTestCaseId)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == null).Count();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseProposal = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == preTemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                if (templateTestCaseProposal != null && templateTestCaseProposal.Count > 0)
                {
                    templateTestCaseProposal.ForEach(s =>
                    {
                        var templateTestCaseLink = new TemplateTestCaseProposal
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            UserId = s.UserId,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            UserType = s.UserType,
                            IsByCaseCompany = s.IsByCaseCompany,
                        };
                        _context.TemplateTestCaseProposal.Add(templateTestCaseLink);
                    });
                    _context.SaveChanges();
                }
            }
        }
        public void CreateNewTempalteCheckListInherit(TemplateTestCaseModel value, long? preTemplateTestCaseId)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == null).Count();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseCheckList = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == preTemplateTestCaseId && w.TemplateTestCaseFormId == null).ToList();
                if (templateTestCaseCheckList != null && templateTestCaseCheckList.Count > 0)
                {
                    var templateTestCaseCheckListIds = templateTestCaseCheckList.Select(s => s.TemplateTestCaseCheckListId).ToList();
                    var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Where(w => templateTestCaseCheckListIds.Contains(w.TemplateTestCaseCheckListId.Value)).ToList();
                    var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                    var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Include(a => a.TemplateTestCaseCheckListResponseResponsible).Where(w => templateTestCaseCheckListResponseIds.Contains(w.TemplateTestCaseCheckListResponseId.Value)).ToList();
                    templateTestCaseCheckList.ForEach(s =>
                    {
                        var templateTestCaseCheckListResponseBy = templateTestCaseCheckListResponse.Where(a => a.TemplateTestCaseCheckListId == s.TemplateTestCaseCheckListId).ToList();
                        var sessionId = Guid.NewGuid();
                        var templateTestCaseLink = new TemplateTestCaseCheckList
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            Description = s.Description,
                            Instruction = s.Instruction,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            SessionId = sessionId,
                            No = s.No,
                            SeqNo = s.SeqNo,
                            IsMasterTemplate = true,
                        };
                        _context.TemplateTestCaseCheckList.Add(templateTestCaseLink);
                        if (templateTestCaseCheckListResponseBy != null && templateTestCaseCheckListResponseBy.Count > 0)
                        {
                            templateTestCaseCheckListResponseBy.ForEach(a =>
                            {
                                var templateTestCaseCheckListResponseadd = new TemplateTestCaseCheckListResponse
                                {
                                    DutyId = a.DutyId,
                                    Responsibility = a.Responsibility,
                                    FunctionLink = a.FunctionLink,
                                    PageLink = a.PageLink,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    ModifiedByUserId = value.AddedByUserID,
                                    ModifiedDate = DateTime.Now,
                                    StatusCodeId = value.StatusCodeID.Value,
                                    NotificationAdvice = a.NotificationAdvice,
                                    NotificationAdviceTypeId = a.NotificationAdviceTypeId,
                                    RepeatId = a.RepeatId,
                                    CustomId = a.CustomId,
                                    DueDate = a.DueDate == null ? DateTime.Now : a.DueDate,
                                    Monthly = a.Monthly,
                                    Yearly = a.Yearly,
                                    EventDescription = a.EventDescription,
                                    DaysOfWeek = a.DaysOfWeek,
                                    SessionId = Guid.NewGuid(),
                                    NotificationStatusId = a.NotificationStatusId,
                                    Title = a.Title,
                                    Message = a.Message,
                                    ScreenId = a.ScreenId,
                                    NotifyEndDate = a.NotifyEndDate,
                                    IsAllowDocAccess = a.IsAllowDocAccess,
                                };
                                templateTestCaseLink.TemplateTestCaseCheckListResponse.Add(templateTestCaseCheckListResponseadd);
                                var templateTestCaseCheckListResponseDutyBy = templateTestCaseCheckListResponseDuty.Where(w => w.TemplateTestCaseCheckListResponseId == a.TemplateTestCaseCheckListResponseId).ToList();
                                if (templateTestCaseCheckListResponseDutyBy != null && templateTestCaseCheckListResponseDutyBy.Count > 0)
                                {
                                    templateTestCaseCheckListResponseDutyBy.ForEach(c =>
                                    {
                                        var TemplateTestCaseCheckListResponseDutyAdd = new TemplateTestCaseCheckListResponseDuty
                                        {
                                            DepartmantId = c.DepartmantId,
                                            DesignationId = c.DesignationId,
                                            EmployeeId = c.EmployeeId,
                                            CompanyId = c.CompanyId,
                                            DesignationNumber = c.DesignationNumber,
                                            DutyNo = c.DutyNo,
                                            AddedByUserId = value.AddedByUserID,
                                            AddedDate = DateTime.Now,
                                            ModifiedByUserId = value.AddedByUserID,
                                            ModifiedDate = DateTime.Now,
                                            StatusCodeId = c.StatusCodeId,
                                            Type = c.Type,
                                            Description = c.Description,
                                        };
                                        if (c.TemplateTestCaseCheckListResponseResponsible != null)
                                        {
                                            c.TemplateTestCaseCheckListResponseResponsible.ToList().ForEach(d =>
                                            {
                                                var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                                                {
                                                    EmployeeId = d.EmployeeId,
                                                    UserGroupId = d.UserGroupId,
                                                    UserId = d.UserId,
                                                };
                                                TemplateTestCaseCheckListResponseDutyAdd.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                                            });
                                        }
                                        templateTestCaseCheckListResponseadd.TemplateTestCaseCheckListResponseDuty.Add(TemplateTestCaseCheckListResponseDutyAdd);
                                    });
                                }
                            });
                        }
                        _context.SaveChanges();
                    });

                }
            }
        }
        [HttpPut]
        [Route("UpdateTemplateTestCase")]
        public TemplateTestCaseModel UpdateTemplateTestCase(TemplateTestCaseModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.TemplateTestCase.SingleOrDefault(p => p.TemplateTestCaseId == value.TemplateTestCaseId);
            templateTestCase.TemplateName = value.TemplateName;
            templateTestCase.TemplateCode = value.TemplateCode;
            templateTestCase.CompanyId = value.CompanyId;
            templateTestCase.DepartmentId = value.DepartmentId;
            templateTestCase.Instruction = value.Instruction;
            templateTestCase.StatusCodeId = value.StatusCodeID.Value;
            templateTestCase.ModifiedByUserId = value.AddedByUserID;
            templateTestCase.ModifiedDate = DateTime.Now;
            templateTestCase.SessionId = value.SessionId;
            templateTestCase.ProfileId = value.ProfileId;
            templateTestCase.SpecificCase = value.SpecificCase;
            templateTestCase.VersionNo = value.VersionNo + 1;
            _context.SaveChanges();

            var templateTestCaseDepartmentRemove = _context.TemplateTestCaseDepartment.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId).ToList();
            if (templateTestCaseDepartmentRemove != null)
            {
                _context.TemplateTestCaseDepartment.RemoveRange(templateTestCaseDepartmentRemove);
                _context.SaveChanges();
            }
            if (value.DepartmentIds != null && value.DepartmentIds.Count > 0)
            {
                value.DepartmentIds.ForEach(h =>
                {
                    TemplateTestCaseDepartment templateTestCaseDepartment = new TemplateTestCaseDepartment
                    {
                        TemplateTestCaseId = value.TemplateTestCaseId,
                        DepartmentId = h,
                    };
                    _context.TemplateTestCaseDepartment.Add(templateTestCaseDepartment);
                });
                _context.SaveChanges();
            }
            value.VersionNo = templateTestCase.VersionNo;
            return value;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseLink")]
        public TemplateTestCaseLinkModel InsertTemplateTestCaseLink(TemplateTestCaseLinkModel value)
        {
            var templateTestCaseLink = new TemplateTestCaseLink
            {
                TemplateTestCaseId = value.TemplateTestCaseId,
                Link = value.Link,
                Subject = value.Subject,
                NameOfTemplate = value.NameOfTemplate,
                IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false,
                TemplateProfileId = value.TemplateProfileId,
                LocationToSaveId = value.LocationToSaveId,
                Naming = value.Naming,
                TemplateCaseFormId = value.TemplateCaseFormId,
            };
            _context.TemplateTestCaseLink.Add(templateTestCaseLink);
            _context.SaveChanges();
            value.TemplateTestCaseLinkId = templateTestCaseLink.TemplateTestCaseLinkId;
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseLink")]
        public TemplateTestCaseLinkModel UpdateTemplateTestCaseLink(TemplateTestCaseLinkModel value)
        {
            var templateTestCase = _context.TemplateTestCaseLink.SingleOrDefault(p => p.TemplateTestCaseLinkId == value.TemplateTestCaseLinkId);

            templateTestCase.TemplateTestCaseId = value.TemplateTestCaseId;
            templateTestCase.Link = value.Link;
            templateTestCase.Subject = value.Subject;
            templateTestCase.NameOfTemplate = value.NameOfTemplate;
            templateTestCase.IsAutoNumbering = value.AutoNumbering == "Yes" ? true : false;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.TemplateProfileId = value.TemplateProfileId;
            templateTestCase.LocationToSaveId = value.LocationToSaveId;
            templateTestCase.Naming = value.Naming;
            templateTestCase.TemplateCaseFormId = value.TemplateCaseFormId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteTemplateTestCase")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var templateTestCase = _context.TemplateTestCase.SingleOrDefault(p => p.TemplateTestCaseId == id);
                if (templateTestCase != null)
                {
                    var templateTestCaseProposal = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == templateTestCase.TemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                    if (templateTestCaseProposal != null)
                    {
                        _context.TemplateTestCaseProposal.RemoveRange(templateTestCaseProposal);
                        _context.SaveChanges();
                    }
                    var templateTestCaseLink = _context.TemplateTestCaseLink.Where(w => w.TemplateTestCaseId == templateTestCase.TemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                    if (templateTestCaseLink != null)
                    {
                        _context.TemplateTestCaseLink.RemoveRange(templateTestCaseLink);
                        _context.SaveChanges();
                    }
                    var templateTestCaseDepartment = _context.TemplateTestCaseDepartment.Where(w => w.TemplateTestCaseId == templateTestCase.TemplateTestCaseId).ToList();
                    if (templateTestCaseDepartment != null)
                    {
                        _context.TemplateTestCaseDepartment.RemoveRange(templateTestCaseDepartment);
                        _context.SaveChanges();
                    }
                    _context.TemplateTestCase.Remove(templateTestCase);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("TemplateTestCase Cannot be delete! Reference to others");
            }
        }
        [HttpDelete]
        [Route("DeleteTemplateTestCaseProposal")]
        public ActionResult<string> DeDeleteTemplateTestCaseProposallete(int id)
        {
            try
            {
                var templateTestCase = _context.TemplateTestCaseProposal.SingleOrDefault(p => p.TemplateTestCaseProposalId == id);
                if (templateTestCase != null)
                {
                    _context.TemplateTestCaseProposal.Remove(templateTestCase);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("TemplateTestCase Cannot be delete! Reference to others");
            }
        }
        #endregion
        #region TemplateTestCaseForm
        [HttpGet]
        [Route("GetTemplateTestCaseForm")]
        public List<TemplateTestCaseFormModel> GetTemplateTestCaseForm()
        {
            var templateTestCaseForm = _context.TemplateTestCaseForm
                                .Include(r => r.Company)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(a => a.TemplateTestCase)
                                .Include(a => a.FileProfileType)
                                .AsNoTracking().OrderByDescending(o => o.TemplateTestCaseFormId).ToList();
            List<TemplateTestCaseFormModel> templateTestCaseFormModels = new List<TemplateTestCaseFormModel>();
            if (templateTestCaseForm != null && templateTestCaseForm.Count > 0)
            {
                var templateTestCaseIds = templateTestCaseForm.Select(s => s.TemplateTestCaseId.GetValueOrDefault(0)).ToList();
                var templateTestCaseLink = _context.TemplateTestCaseLink.Select(n => new TemplateTestCaseLinkModel { TemplateTestCaseLinkId = n.TemplateTestCaseLinkId, TemplateTestCaseId = n.TemplateTestCaseId, NameOfTemplate = n.NameOfTemplate, Subject = n.Subject, Link = n.Link, LocationName = n.LocationName, Naming = n.Naming, IsAutoNumbering = n.IsAutoNumbering, TemplateProfileId = n.TemplateProfileId, TemplateProfileName = n.TemplateProfile.Name, AutoNumbering = n.IsAutoNumbering == true ? "Yes" : "No", TemplateCaseFormId = n.TemplateCaseFormId }).Where(w => templateTestCaseIds.Contains(w.TemplateTestCaseId.Value) && w.TemplateCaseFormId != null).ToList();
                templateTestCaseForm.ForEach(s =>
                {
                    TemplateTestCaseFormModel templateTestCaseFormModel = new TemplateTestCaseFormModel();
                    templateTestCaseFormModel.TemplateTestCaseFormId = s.TemplateTestCaseFormId;
                    templateTestCaseFormModel.TemplateTestCaseId = s.TemplateTestCaseId;
                    templateTestCaseFormModel.TemplateName = s.TemplateTestCase?.TemplateName;
                    templateTestCaseFormModel.TemplateInstruction = s.TemplateTestCase?.Instruction;
                    templateTestCaseFormModel.CompanyId = s.CompanyId;
                    templateTestCaseFormModel.SubjectName = s.SubjectName;
                    templateTestCaseFormModel.CompanyName = s.Company?.Description;
                    templateTestCaseFormModel.VersionNo = s.VersionNo;
                    templateTestCaseFormModel.CaseNo = s.CaseNo;
                    templateTestCaseFormModel.Instruction = s.Instruction;
                    templateTestCaseFormModel.ModifiedByUserID = s.AddedByUserId;
                    templateTestCaseFormModel.AddedByUserID = s.ModifiedByUserId;
                    templateTestCaseFormModel.StatusCodeID = s.StatusCodeId;
                    templateTestCaseFormModel.AddedByUser = s.AddedByUser.UserName;
                    templateTestCaseFormModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    templateTestCaseFormModel.AddedDate = s.AddedDate;
                    templateTestCaseFormModel.ModifiedDate = s.ModifiedDate;
                    templateTestCaseFormModel.StatusCode = s.StatusCode.CodeValue;
                    templateTestCaseFormModel.SessionId = s.SessionId;
                    templateTestCaseFormModel.FileProfileTypeId = s.FileProfileTypeId;
                    templateTestCaseFormModel.FileProfileType = s.FileProfileType?.Name;
                    templateTestCaseFormModel.TopicId = s.TopicId;
                    templateTestCaseFormModel.templateTestCaseLinkModels = templateTestCaseLink.Where(t => t.TemplateTestCaseId == s.TemplateTestCaseId && t.TemplateCaseFormId == s.TemplateTestCaseFormId).ToList();
                    templateTestCaseFormModels.Add(templateTestCaseFormModel);
                });
            }
            return templateTestCaseFormModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseFormByTopic")]
        public List<TemplateTestCaseFormModel> GetTemplateTestCaseFormByTopic(long? id)
        {
            var templateTestCaseForm = _context.TemplateTestCaseForm
                                .Include(r => r.Company)
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(a => a.TemplateTestCase)
                                .Include(a => a.FileProfileType)
                                .Where(w => w.TemplateTestCaseFormId == id)
                                .AsNoTracking().OrderByDescending(o => o.TemplateTestCaseFormId).ToList();
            List<TemplateTestCaseFormModel> templateTestCaseFormModels = new List<TemplateTestCaseFormModel>();
            if (templateTestCaseForm != null && templateTestCaseForm.Count > 0)
            {
                templateTestCaseForm.ForEach(s =>
                {
                    TemplateTestCaseFormModel templateTestCaseFormModel = new TemplateTestCaseFormModel();
                    templateTestCaseFormModel.TemplateTestCaseFormId = s.TemplateTestCaseFormId;
                    templateTestCaseFormModel.TemplateTestCaseId = s.TemplateTestCaseId;
                    templateTestCaseFormModel.TemplateName = s.TemplateTestCase?.TemplateName;
                    templateTestCaseFormModel.TemplateInstruction = s.TemplateTestCase?.Instruction;
                    templateTestCaseFormModel.CompanyId = s.CompanyId;
                    templateTestCaseFormModel.SubjectName = s.SubjectName;
                    templateTestCaseFormModel.CompanyName = s.Company?.Description;
                    templateTestCaseFormModel.VersionNo = s.VersionNo;
                    templateTestCaseFormModel.CaseNo = s.CaseNo;
                    templateTestCaseFormModel.Instruction = s.Instruction;
                    templateTestCaseFormModel.ModifiedByUserID = s.AddedByUserId;
                    templateTestCaseFormModel.AddedByUserID = s.ModifiedByUserId;
                    templateTestCaseFormModel.StatusCodeID = s.StatusCodeId;
                    templateTestCaseFormModel.AddedByUser = s.AddedByUser.UserName;
                    templateTestCaseFormModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    templateTestCaseFormModel.AddedDate = s.AddedDate;
                    templateTestCaseFormModel.ModifiedDate = s.ModifiedDate;
                    templateTestCaseFormModel.StatusCode = s.StatusCode.CodeValue;
                    templateTestCaseFormModel.SessionId = s.SessionId;
                    templateTestCaseFormModel.FileProfileTypeId = s.FileProfileTypeId;
                    templateTestCaseFormModel.FileProfileType = s.FileProfileType?.Name;
                    templateTestCaseFormModel.TopicId = s.TopicId;
                    templateTestCaseFormModel.templateTestCaseLinkModels = GetTemplateTestCaseLink(s.TemplateTestCaseId, "TemplateCaseForm", s.TemplateTestCaseFormId);
                    templateTestCaseFormModel.TemplateTestCaseProposalModels = GetTemplateTestCaseProposal(s.TemplateTestCaseId, "TemplateCaseForm", s.TemplateTestCaseFormId);
                    templateTestCaseFormModel.TemplateTestCaseCheckListModels = GetTemplateTestCaseCheckList(s.TemplateTestCaseId, "templateTestCaseForm", s.TemplateTestCaseFormId, "Topic");
                    templateTestCaseFormModels.Add(templateTestCaseFormModel);
                });
            }
            return templateTestCaseFormModels;
        }
        [HttpPost()]
        [Route("GetTemplateTestCaseFormData")]
        public ActionResult<TemplateTestCaseFormModel> GetTemplateTestCaseFormData(SearchModel searchModel)
        {
            var templateTestCase = new TemplateTestCaseForm();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).FirstOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Last":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).LastOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Next":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).LastOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Previous":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).FirstOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).FirstOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Last":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).LastOrDefault(f => f.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Next":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderBy(o => o.TemplateTestCaseFormId).FirstOrDefault(s => s.TemplateTestCaseFormId > searchModel.Id && s.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                    case "Previous":
                        templateTestCase = _context.TemplateTestCaseForm.Include(a => a.TemplateTestCase).OrderByDescending(o => o.TemplateTestCaseFormId).FirstOrDefault(s => s.TemplateTestCaseFormId < searchModel.Id && s.FileProfileTypeId == searchModel.FileProfileTypeId);
                        break;
                }
            }
            var result = _mapper.Map<TemplateTestCaseFormModel>(templateTestCase);
            if (result != null)
            {
                result.TemplateName = templateTestCase?.TemplateTestCase?.TemplateName;
            }
            return result;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseForm")]
        public TemplateTestCaseFormModel InsertTemplateTestCaseForm(TemplateTestCaseFormModel value)
        {
            var sessionId = Guid.NewGuid();
            var versionNo = 1;
            var templateTestCaseForm = new TemplateTestCaseForm
            {
                TemplateTestCaseId = value.TemplateTestCaseId,
                CaseNo = GenerteCaseNo(value),
                CompanyId = value.CompanyId,
                SubjectName = value.SubjectName,
                VersionNo = versionNo.ToString(),
                Instruction = value.Instruction,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = sessionId,
                FileProfileTypeId = value.FileProfileTypeId,
            };
            _context.TemplateTestCaseForm.Add(templateTestCaseForm);
            _context.SaveChanges();
            value.CaseNo = templateTestCaseForm.CaseNo;
            value.TemplateTestCaseFormId = templateTestCaseForm.TemplateTestCaseFormId;
            if (value.TemplateTestCaseId != null)
            {
                value.TemplateName = _context.TemplateTestCase.FirstOrDefault(f => f.TemplateTestCaseId == value.TemplateTestCaseId)?.TemplateName;
            }
            value.SessionId = sessionId;
            value.VersionNo = templateTestCaseForm.VersionNo;
            var caseLinkUpdate = CreateTemplateTestCaseLinkForm(value);
            CreateTemplateCaseProposal(value);
            CreateNewTempalteCheckList(value);
            if (caseLinkUpdate != null && caseLinkUpdate.Count > 0)
            {
                var templateTestCaseProposalExites = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId && w.TemplateTestCaseLinkId != null).ToList();

                caseLinkUpdate.ForEach(a =>
                {
                    var query = string.Format("Update TemplateTestCaseCheckList Set TemplateTestCaseLinkId = {1} Where TemplateTestCaseLinkId={0} and TemplateTestCaseFormId={2}", a.PreTemplateTestCaseLinkId, a.NewTemplateTestCaseLinkId, value.TemplateTestCaseFormId);
                    _context.Database.ExecuteSqlRaw(query);
                });
            }
            return value;
        }
        public List<TemplateTestCaseLinkSetModel> CreateTemplateTestCaseLinkForm(TemplateTestCaseFormModel value)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseLink.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == value.TemplateTestCaseFormId).Count();
            List<TemplateTestCaseLinkSetModel> templateTestCaseLinkSetModels = new List<TemplateTestCaseLinkSetModel>();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseProposal = _context.TemplateTestCaseLink.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                if (templateTestCaseProposal != null && templateTestCaseProposal.Count > 0)
                {
                    templateTestCaseProposal.ForEach(s =>
                    {
                        var templateTestCaseLink = new TemplateTestCaseLink
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            Link = s.Link,
                            Subject = s.Subject,
                            NameOfTemplate = s.NameOfTemplate,
                            IsAutoNumbering = s.IsAutoNumbering,
                            TemplateProfileId = s.TemplateProfileId,
                            LocationToSaveId = s.LocationToSaveId,
                            Naming = s.Naming,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            TemplateCaseFormId = value.TemplateTestCaseFormId,
                        };
                        _context.TemplateTestCaseLink.Add(templateTestCaseLink);
                        _context.SaveChanges();
                        TemplateTestCaseLinkSetModel templateTestCaseLinkSetModel = new TemplateTestCaseLinkSetModel();
                        templateTestCaseLinkSetModel.PreTemplateTestCaseLinkId = s.TemplateTestCaseLinkId;
                        templateTestCaseLinkSetModel.NewTemplateTestCaseLinkId = templateTestCaseLink.TemplateTestCaseLinkId;
                        templateTestCaseLinkSetModels.Add(templateTestCaseLinkSetModel);
                    });

                }
            }
            return templateTestCaseLinkSetModels;
        }
        public void CreateTemplateCaseProposal(TemplateTestCaseFormModel value)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == value.TemplateTestCaseFormId).Count();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseProposal = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateCaseFormId == null).ToList();
                if (templateTestCaseProposal != null && templateTestCaseProposal.Count > 0)
                {
                    templateTestCaseProposal.ForEach(s =>
                    {
                        var templateTestCaseLink = new TemplateTestCaseProposal
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            UserId = s.UserId,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            UserType = s.UserType,
                            IsByCaseCompany = s.IsByCaseCompany,
                            TemplateCaseFormId = value.TemplateTestCaseFormId
                        };
                        _context.TemplateTestCaseProposal.Add(templateTestCaseLink);
                    });
                    _context.SaveChanges();
                }
            }
        }
        public void CreateNewTempalteCheckList(TemplateTestCaseFormModel value)
        {
            var templateTestCaseProposalExites = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId).Count();
            if (templateTestCaseProposalExites == 0)
            {
                var templateTestCaseCheckList = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == null).ToList();
                if (templateTestCaseCheckList != null && templateTestCaseCheckList.Count > 0)
                {
                    var templateTestCaseCheckListIds = templateTestCaseCheckList.Select(s => s.TemplateTestCaseCheckListId).ToList();
                    var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Where(w => templateTestCaseCheckListIds.Contains(w.TemplateTestCaseCheckListId.Value)).ToList();
                    var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                    var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Include(a => a.TemplateTestCaseCheckListResponseResponsible).Where(w => templateTestCaseCheckListResponseIds.Contains(w.TemplateTestCaseCheckListResponseId.Value)).ToList();
                    templateTestCaseCheckList.ForEach(s =>
                    {
                        var templateTestCaseCheckListResponseBy = templateTestCaseCheckListResponse.Where(a => a.TemplateTestCaseCheckListId == s.TemplateTestCaseCheckListId).ToList();
                        var sessionId = Guid.NewGuid();
                        var templateTestCaseLink = new TemplateTestCaseCheckList
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            Description = s.Description,
                            Instruction = s.Instruction,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            SessionId = sessionId,
                            No = s.No,
                            SeqNo = s.SeqNo,
                            TemplateTestCaseFormId = value.TemplateTestCaseFormId,
                            IsMasterTemplate = true,
                            TemplateTestCaseLinkId = s.TemplateTestCaseLinkId,
                        };
                        _context.TemplateTestCaseCheckList.Add(templateTestCaseLink);
                        if (templateTestCaseCheckListResponseBy != null && templateTestCaseCheckListResponseBy.Count > 0)
                        {
                            templateTestCaseCheckListResponseBy.ForEach(a =>
                            {
                                var templateTestCaseCheckListResponseadd = new TemplateTestCaseCheckListResponse
                                {
                                    DutyId = a.DutyId,
                                    Responsibility = a.Responsibility,
                                    FunctionLink = a.FunctionLink,
                                    PageLink = a.PageLink,
                                    AddedByUserId = value.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    ModifiedByUserId = value.AddedByUserID,
                                    ModifiedDate = DateTime.Now,
                                    StatusCodeId = value.StatusCodeID.Value,
                                    NotificationAdvice = a.NotificationAdvice,
                                    NotificationAdviceTypeId = a.NotificationAdviceTypeId,
                                    RepeatId = a.RepeatId,
                                    CustomId = a.CustomId,
                                    DueDate = a.DueDate == null ? DateTime.Now : a.DueDate,
                                    Monthly = a.Monthly,
                                    Yearly = a.Yearly,
                                    EventDescription = a.EventDescription,
                                    DaysOfWeek = a.DaysOfWeek,
                                    SessionId = Guid.NewGuid(),
                                    NotificationStatusId = a.NotificationStatusId,
                                    Title = a.Title,
                                    Message = a.Message,
                                    ScreenId = a.ScreenId,
                                    NotifyEndDate = a.NotifyEndDate,
                                    IsAllowDocAccess = a.IsAllowDocAccess,
                                };
                                templateTestCaseLink.TemplateTestCaseCheckListResponse.Add(templateTestCaseCheckListResponseadd);
                                var templateTestCaseCheckListResponseDutyBy = templateTestCaseCheckListResponseDuty.Where(w => w.TemplateTestCaseCheckListResponseId == a.TemplateTestCaseCheckListResponseId).ToList();
                                if (templateTestCaseCheckListResponseDutyBy != null && templateTestCaseCheckListResponseDutyBy.Count > 0)
                                {
                                    templateTestCaseCheckListResponseDutyBy.ForEach(c =>
                                    {
                                        var TemplateTestCaseCheckListResponseDutyAdd = new TemplateTestCaseCheckListResponseDuty
                                        {
                                            DepartmantId = c.DepartmantId,
                                            DesignationId = c.DesignationId,
                                            EmployeeId = c.EmployeeId,
                                            CompanyId = c.CompanyId,
                                            DesignationNumber = c.DesignationNumber,
                                            DutyNo = c.DutyNo,
                                            AddedByUserId = value.AddedByUserID,
                                            AddedDate = DateTime.Now,
                                            ModifiedByUserId = value.AddedByUserID,
                                            ModifiedDate = DateTime.Now,
                                            StatusCodeId = c.StatusCodeId,
                                            Type = c.Type,
                                            Description = c.Description,
                                        };
                                        if (c.TemplateTestCaseCheckListResponseResponsible != null)
                                        {
                                            c.TemplateTestCaseCheckListResponseResponsible.ToList().ForEach(d =>
                                            {
                                                var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                                                {
                                                    EmployeeId = d.EmployeeId,
                                                    UserGroupId = d.UserGroupId,
                                                    UserId = d.UserId,
                                                };
                                                TemplateTestCaseCheckListResponseDutyAdd.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                                            });
                                        }
                                        templateTestCaseCheckListResponseadd.TemplateTestCaseCheckListResponseDuty.Add(TemplateTestCaseCheckListResponseDutyAdd);
                                    });
                                }
                            });
                        }
                        _context.SaveChanges();
                        //var templateTestCaseCheckListId = templateTestCaseLink.TemplateTestCaseCheckListId;
                        // InsertTemplateTestCaseCheckListResponseByCreateCase(s.TemplateTestCaseCheckListId, templateTestCaseLink.TemplateTestCaseCheckListId);
                    });

                }
            }
        }
        public void InsertTemplateTestCaseCheckListResponseByCreateCase(long oldId, long? newId)
        {
            var templateTestCaseCheckListResponseOld = GetTemplateTestCaseCheckListResponse(oldId).ToList();
            if (templateTestCaseCheckListResponseOld != null && templateTestCaseCheckListResponseOld.Count > 0)
            {
                templateTestCaseCheckListResponseOld.ForEach(s =>
                {
                    var SessionId = Guid.NewGuid();
                    var ApplicationWikiLine = new TemplateTestCaseCheckListResponse
                    {
                        TemplateTestCaseCheckListId = newId,
                        DutyId = s.DutyId,
                        Responsibility = s.Responsibility,
                        FunctionLink = s.FunctionLink,
                        PageLink = s.PageLink,
                        AddedByUserId = s.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = s.StatusCodeID.Value,
                        NotificationAdvice = s.NotificationAdviceFlag == "Yes" ? true : false,
                        NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                        RepeatId = s.RepeatId,
                        CustomId = s.CustomId,
                        DueDate = s.DueDate == null ? DateTime.Now : s.DueDate,
                        Monthly = s.Monthly,
                        Yearly = s.Yearly,
                        EventDescription = s.EventDescription,
                        DaysOfWeek = s.DaysOfWeek,
                        SessionId = SessionId,
                        NotificationStatusId = s.NotificationStatusId,
                        Title = s.Title,
                        Message = s.Message,
                        ScreenId = s.ScreenId,
                        NotifyEndDate = s.NotifyEndDate,
                        IsAllowDocAccess = s.IsAllowDocAccess,
                    };
                    _context.TemplateTestCaseCheckListResponse.Add(ApplicationWikiLine);
                    _context.SaveChanges();
                    if (s.WeeklyIds != null && s.WeeklyIds.Count > 0)
                    {
                        s.WeeklyIds.ForEach(u =>
                        {
                            var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                            {
                                TemplateTestCaseCheckListResponseId = ApplicationWikiLine.TemplateTestCaseCheckListResponseId,
                                WeeklyId = u,
                                CustomType = "Weekly",
                            };
                            _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                            _context.SaveChanges();
                        });
                    }
                    if (s.DaysOfWeek == true && s.DaysOfWeekIds != null && s.DaysOfWeekIds.Count > 0)
                    {
                        s.DaysOfWeekIds.ForEach(u =>
                        {
                            var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                            {
                                TemplateTestCaseCheckListResponseId = ApplicationWikiLine.TemplateTestCaseCheckListResponseId,
                                WeeklyId = u,
                                CustomType = "Yearly",
                            };
                            _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                            _context.SaveChanges();
                        });
                    }
                    InsertApplicationWikiLineDutyByCase(s.TemplateTestCaseCheckListResponseDutyModels, ApplicationWikiLine.TemplateTestCaseCheckListResponseId);
                });
            }
        }
        public void InsertApplicationWikiLineDutyByCase(List<TemplateTestCaseCheckListResponseDutyModel> templateTestCaseCheckListResponseDutyModels, long? newId)
        {
            if (templateTestCaseCheckListResponseDutyModels != null && templateTestCaseCheckListResponseDutyModels.Count > 0)
            {
                templateTestCaseCheckListResponseDutyModels.ForEach(a =>
                {
                    var ApplicationWikiLineDuty = new TemplateTestCaseCheckListResponseDuty
                    {
                        TemplateTestCaseCheckListResponseId = newId,
                        DepartmantId = a.DepartmentId,
                        DesignationId = a.DesignationId,
                        EmployeeId = a.EmployeeId,
                        CompanyId = a.CompanyId,
                        DesignationNumber = a.DesignationNumber,
                        DutyNo = a.DutyNo,
                        AddedByUserId = a.AddedByUserID,
                        AddedDate = DateTime.Now,
                        StatusCodeId = a.StatusCodeID.Value,
                        Type = a.Type,
                        Description = a.Description,
                    };
                    _context.TemplateTestCaseCheckListResponseDuty.Add(ApplicationWikiLineDuty);
                    _context.SaveChanges();
                    if (a.WikiResponsibilityModels != null && a.WikiResponsibilityModels.Count > 0)
                    {
                        a.WikiResponsibilityModels.ForEach(s =>
                        {
                            var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                            {
                                EmployeeId = s.EmployeeID,
                                TemplateTestCaseCheckListResponseDutyId = ApplicationWikiLineDuty.TemplateTestCaseCheckListResponseDutyId,
                            };
                            _context.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                            _context.SaveChanges();
                        });
                    }
                });
            }
        }
        private string GenerteCaseNo(TemplateTestCaseFormModel value)
        {
            var caseNo = string.Empty;
            var templateTestCaseForm = _context.TemplateTestCaseForm.Where(w => w.CompanyId == value.CompanyId).Count() + 1;
            var templatCode = _context.TemplateTestCase.FirstOrDefault(f => f.TemplateTestCaseId == value.TemplateTestCaseId)?.TemplateCode;
            var plantCode = _context.Plant.FirstOrDefault(w => w.PlantId == value.CompanyId)?.PlantCode;
            if (!string.IsNullOrEmpty(plantCode))
            {
                caseNo += plantCode.Substring(2, plantCode.Length - 2) + "-";
            }
            if (!string.IsNullOrEmpty(caseNo) && !string.IsNullOrEmpty(templatCode))
            {
                caseNo += templatCode + "-";
            }
            if (!string.IsNullOrEmpty(caseNo))
            {
                caseNo += DateTime.Now.ToString("yy") + "-";
            }
            if (!string.IsNullOrEmpty(caseNo))
            {
                caseNo += string.Format("{0:0000}", templateTestCaseForm);
            }
            return caseNo;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseForm")]
        public TemplateTestCaseFormModel UpdateTemplateTestCaseForm(TemplateTestCaseFormModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCaseForm = _context.TemplateTestCaseForm.SingleOrDefault(p => p.TemplateTestCaseFormId == value.TemplateTestCaseFormId);
            templateTestCaseForm.TemplateTestCaseId = value.TemplateTestCaseId;
            templateTestCaseForm.SubjectName = value.SubjectName;
            //templateTestCaseForm.CaseNo = value.CaseNo;
            templateTestCaseForm.CompanyId = value.CompanyId;
            string versionNo = string.IsNullOrEmpty(templateTestCaseForm.VersionNo) ? "0" : templateTestCaseForm.VersionNo;
            templateTestCaseForm.VersionNo = (Int16.Parse(versionNo) + 1).ToString();
            templateTestCaseForm.Instruction = value.Instruction;
            templateTestCaseForm.StatusCodeId = value.StatusCodeID.Value;
            templateTestCaseForm.ModifiedByUserId = value.AddedByUserID;
            templateTestCaseForm.ModifiedDate = DateTime.Now;
            templateTestCaseForm.SessionId = value.SessionId;
            templateTestCaseForm.FileProfileTypeId = value.FileProfileTypeId;
            _context.SaveChanges();
            if (value.TemplateTestCaseId != null)
            {
                value.TemplateName = _context.TemplateTestCase.FirstOrDefault(f => f.TemplateTestCaseId == value.TemplateTestCaseId)?.TemplateName;
            }
            value.VersionNo = templateTestCaseForm.VersionNo;
            CreateTemplateTestCaseLinkForm(value);
            CreateTemplateCaseProposal(value);
            CreateNewTempalteCheckList(value);
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseFormTopic")]
        public TemplateTestCaseFormModel UpdateTemplateTestCaseFormTopic(TemplateTestCaseFormModel value)
        {
            var templateTestCaseForm = _context.TemplateTestCaseForm.SingleOrDefault(p => p.TemplateTestCaseFormId == value.TemplateTestCaseFormId);
            templateTestCaseForm.TopicId = value.TopicId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteTemplateTestCaseForm")]
        public ActionResult<string> DeleteTemplateTestCaseForm(int id)
        {
            try
            {
                var templateTestCaseForm = _context.TemplateTestCaseForm.SingleOrDefault(p => p.TemplateTestCaseFormId == id);
                if (templateTestCaseForm != null)
                {
                    _context.TemplateTestCaseForm.Remove(templateTestCaseForm);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("TemplateTestCaseForm Cannot be delete! Reference to others");
            }
        }
        #endregion
        #region Notes
        [HttpGet]
        [Route("GetNotesByDocumentId")]
        public List<TemplateCaseFormNotesModel> GetNotesByDocumentId(int id)
        {

            List<TemplateCaseFormNotesModel> notesModels = new List<TemplateCaseFormNotesModel>();
            TemplateCaseFormNotesModel notesModel = new TemplateCaseFormNotesModel();
            var notes = _context.TemplateCaseFormNotes.Include(i => i.AddedByUser).Include(i => i.ModifiedByUser).Include(i => i.StatusCode).Where(s => s.TemplateTestCaseFormId == id).AsNoTracking().ToList();
            if (notes != null && notes.Count > 0)
            {
                notes.ForEach(s =>
                {
                    notesModel = new TemplateCaseFormNotesModel();
                    notesModel.SessionId = s.SessionId;
                    notesModel.Notes = s.Notes;
                    notesModel.Link = s.Link;
                    notesModel.TemplateTestCaseFormId = s.TemplateTestCaseFormId;
                    notesModel.TemplateCaseFormNotesId = s.TemplateCaseFormNotesId;
                    notesModel.ModifiedByUser = s.ModifiedByUser?.UserName;
                    notesModel.StatusCode = s.StatusCode?.CodeValue;
                    notesModel.AddedByUserID = s.AddedByUserId;
                    notesModel.ModifiedByUserID = s.ModifiedByUserId;
                    notesModel.StatusCodeID = s.StatusCodeId;
                    notesModel.AddedDate = s.AddedDate;
                    notesModel.ModifiedDate = s.ModifiedDate;
                    notesModel.AddedByUser = s.AddedByUser?.UserName;
                    notesModels.Add(notesModel);
                });
            }
            return notesModels;
        }
        [HttpPost]
        [Route("InsertNotes")]
        public TemplateCaseFormNotesModel InsertNotes(TemplateCaseFormNotesModel value)
        {

            var notes = new TemplateCaseFormNotes
            {
                Notes = value.Notes,
                Link = value.Link,
                TemplateTestCaseFormId = value.TemplateTestCaseFormId,
                SessionId = value.SessionId,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.TemplateCaseFormNotes.Add(notes);
            value.AddedDate = notes.AddedDate;
            if (value.AddedByUserID > 0)
            {
                value.AddedByUser = _context.ApplicationUser.Where(s => s.UserId == value.AddedByUserID)?.FirstOrDefault().UserName;
            }
            _context.SaveChanges();
            value.TemplateCaseFormNotesId = notes.TemplateCaseFormNotesId;
            return value;
        }
        [HttpPut]
        [Route("UpdateNotes")]
        public TemplateCaseFormNotesModel UpdateNotes(TemplateCaseFormNotesModel value)
        {

            var notes = _context.TemplateCaseFormNotes.SingleOrDefault(p => p.TemplateCaseFormNotesId == value.TemplateCaseFormNotesId);
            if (notes != null)
            {
                notes.TemplateTestCaseFormId = value.TemplateTestCaseFormId;
                notes.Link = value.Link;
                notes.ModifiedByUserId = value.ModifiedByUserID;
                notes.ModifiedDate = DateTime.Now;
                notes.StatusCodeId = value.StatusCodeID.Value;
                notes.Notes = value.Notes;
                notes.StatusCodeId = value.StatusCodeID.Value;
            }
            _context.SaveChanges();
            value.ModifiedDate = notes.ModifiedDate;
            if (value.ModifiedByUserID > 0)
            {
                value.ModifiedByUser = _context.ApplicationUser.Where(s => s.UserId == value.ModifiedByUserID)?.FirstOrDefault().UserName;
            }
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNotes")]
        public void DeleteNotes(int id)
        {
            var notes = _context.TemplateCaseFormNotes.SingleOrDefault(p => p.TemplateCaseFormNotesId == id);
            if (notes != null)
            {
                _context.TemplateCaseFormNotes.Remove(notes);
                _context.SaveChanges();
            }
        }
        [HttpDelete]
        [Route("DeleteTemplateTestCaseLink")]
        public void DeleteTemplateTestCaseLink(int id)
        {
            var notes = _context.TemplateTestCaseLink.SingleOrDefault(p => p.TemplateTestCaseLinkId == id);
            if (notes != null)
            {
                _context.TemplateTestCaseLink.Remove(notes);
                _context.SaveChanges();
            }
        }

        #endregion
        #region TemplateTestCaseCheckList
        [HttpGet]
        [Route("GetTemplateTestCaseProposal")]
        public List<TemplateTestCaseProposalModel> GetTemplateTestCaseProposal(long? id, string type, long? typeId)
        {
            List<TemplateTestCaseProposalModel> templateTestCaseCheckListModels = new List<TemplateTestCaseProposalModel>();
            var templateTestCaseCheckLists = _context.TemplateTestCaseProposal.Include(a => a.User).Include(a => a.ReassignUser).Include(a => a.StatusCode).Include(a => a.ModifiedByUser).Include(a => a.AddedByUser).Where(w => w.TemplateTestCaseId == id);
            if (type == "TemplateCaseForm")
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateCaseFormId == typeId);
            }
            else
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateCaseFormId == null);
            }
            var templateTestCaseCheckList = templateTestCaseCheckLists.AsNoTracking().ToList();
            var plant = _context.Plant.ToList();
            var employees = _context.Employee.Select(s => new { s.PlantId, s.UserId, s.EmployeeId }).ToList();
            List<string> userTypeGroup = new List<string>() { "Group Companies", "Super User" };
            templateTestCaseCheckList.ForEach(s =>
            {
                var plantId = employees.FirstOrDefault(f => f.UserId == s.UserId)?.PlantId;
                var companyName = plant.FirstOrDefault(f => f.PlantId == plantId)?.PlantCode;
                var userType = userTypeGroup.Contains(s.UserType) ? "Group Companies" : "Specific Company";
                var templateTestCaseLink = new TemplateTestCaseProposalModel
                {
                    TemplateTestCaseProposalId = s.TemplateTestCaseProposalId,
                    TemplateTestCaseId = s.TemplateTestCaseId,
                    UserId = s.UserId,
                    CompanyName = companyName,
                    CompanyId = plantId,
                    UserName = s.User?.UserName,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedDate = DateTime.Now,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    IsByCaseCompany = s.IsByCaseCompany,
                    UserType = userType,
                    IsByCaseCompanyFlag = s.IsByCaseCompany == true ? "Yes" : "No",
                    TemplateCaseFormId = s.TemplateCaseFormId,
                    ReassignUserId = s.ReassignUserId,
                    ReassignUser = s.ReassignUser?.UserName,
                };
                templateTestCaseCheckListModels.Add(templateTestCaseLink);
            });
            return templateTestCaseCheckListModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseCheckListByTopic")]
        public List<TemplateTestCaseCheckListModel> GetTemplateTestCaseCheckListByTopic(long? id)
        {
            List<TemplateTestCaseCheckListModel> templateTestCaseCheckListModels = new List<TemplateTestCaseCheckListModel>();
            var templateTestCaseCheckLists = _context.TemplateTestCaseCheckList.Include(a=>a.TemplateTestCaseLink).Include(a => a.StatusCode).Include(a => a.ModifiedByUser).Include(a => a.TemplateTestCaseForm).Include(a => a.TemplateTestCaseForm.TemplateTestCaseCheckListForm).Include(a => a.AddedByUser).Where(w => w.TemplateTestCaseCheckListId == id);
            var templateTestCaseCheckList = templateTestCaseCheckLists.AsNoTracking().ToList();
            if (templateTestCaseCheckList != null && templateTestCaseCheckList.Count > 0)
            {
                var templateTestCaseCheckListIds = templateTestCaseCheckList.Select(s => s.TemplateTestCaseCheckListId).ToList();
                var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Select(s => new { s.TemplateTestCaseCheckListResponseId, s.TemplateTestCaseCheckListId }).Where(w => templateTestCaseCheckListIds.Contains(w.TemplateTestCaseCheckListId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.TemplateTestCaseCheckListResponseId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.TemplateTestCaseCheckListResponseId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIds = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIds.Contains(w.UserId) && w.SessionId != null).ToList();
                templateTestCaseCheckList.ForEach(s =>
                {
                    var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.TemplateTestCaseCheckListId == s.TemplateTestCaseCheckListId).Select(s => new { s.TemplateTestCaseCheckListResponseId, s.TemplateTestCaseCheckListId }).ToList();
                    var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                    var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.TemplateTestCaseCheckListResponseId.Value)).Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.TemplateTestCaseCheckListResponseId }).ToList();
                    var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
                    var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
                    var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                    var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                    var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();
                    var participantUsers = string.Empty;
                    if (appUserss != null && appUserss.Count > 0)
                    {
                        var users = appUserss.Select(s => s.SessionId).ToList();
                        participantUsers = string.Join(",", users);
                    }
                    var templateTestCaseLink = new TemplateTestCaseCheckListModel
                    {
                        ParticipantUsers = participantUsers,
                        CaseNo = s.TemplateTestCaseForm?.CaseNo,
                        TemplateTestCaseCheckListId = s.TemplateTestCaseCheckListId,
                        TemplateTestCaseId = s.TemplateTestCaseId,
                        Description = s.Description,
                        Instruction = s.Instruction,
                        AddedByUserId = s.AddedByUserId,
                        AddedDate = s.AddedDate,
                        StatusCodeId = s.StatusCodeId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        ModifiedDate = DateTime.Now,
                        SessionId = s.SessionId,
                        AddedByUser = s.AddedByUser?.UserName,
                        ModifiedByUser = s.ModifiedByUser?.UserName,
                        StatusCode = s.StatusCode?.CodeValue,
                        No = s.No,
                        Nos = s.TemplateTestCaseFormId > 0 && s.IsMasterTemplate != true ? ("S" + s.No) : ("T" + s.No),
                        SeqNo = s.SeqNo,
                        IsResponsibility = s.IsResponsibility,
                        IsResponsibilityFlag = s.IsResponsibility == true ? "Yes" : "No",
                        IsSkipTest = s.IsSkipTest,
                        IsSkipTestFlag = s.IsSkipTest == true ? "Yes" : "No",
                        TopicId = s.TopicId,
                        TemplateTestCaseFormId = s.TemplateTestCaseFormId,
                        IsMasterTemplate = s.IsMasterTemplate,
                        TemplateTestCaseLinkId = s.TemplateTestCaseLinkId,
                        NameOfTemplate = s.TemplateTestCaseLink?.NameOfTemplate,
                        LocationToSaveId= s.TemplateTestCaseLink?.LocationToSaveId,
                        TemplateTestCaseCheckListResponseModels = GetTemplateTestCaseCheckListResponse(s.TemplateTestCaseCheckListId),
                    };
                    templateTestCaseCheckListModels.Add(templateTestCaseLink);
                });
            }
            return templateTestCaseCheckListModels;
        }
        [HttpGet]
        [Route("GetTemplateTestCaseCheckList")]
        public List<TemplateTestCaseCheckListModel> GetTemplateTestCaseCheckList(long? id, string type, long typeId, string isTopic)
        {
            List<TemplateTestCaseCheckListModel> templateTestCaseCheckListModels = new List<TemplateTestCaseCheckListModel>();
            var templateTestCaseCheckLists = _context.TemplateTestCaseCheckList.Include(a => a.TemplateTestCaseLink).Where(w => w.TemplateTestCaseId == id);
            if (type == "templateTestCaseForm")
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateTestCaseFormId == typeId);
            }
            else
            {
                templateTestCaseCheckLists = templateTestCaseCheckLists.Where(w => w.TemplateTestCaseFormId == null);
            }
            var templateTestCaseCheckList = templateTestCaseCheckLists.AsNoTracking().ToList();
            if (templateTestCaseCheckList != null && templateTestCaseCheckList.Count > 0)
            {
                var appUsersIds = templateTestCaseCheckList.Select(s => s.AddedByUserId.GetValueOrDefault(-1)).ToList();
                var codeMasterIds = templateTestCaseCheckList.Select(s => s.StatusCodeId.GetValueOrDefault(-1)).ToList();
                appUsersIds.AddRange(templateTestCaseCheckList.Select(s => s.ModifiedByUserId.GetValueOrDefault(-1)).ToList());
                var appUsersList = _context.ApplicationUser.Select(s => new
                {
                    s.UserName,
                    s.UserId,
                }).Where(u => appUsersIds.Contains(u.UserId)).AsNoTracking().ToList();
                var codeMasters = _context.CodeMaster.Select(s => new
                {
                    s.CodeValue,
                    s.CodeId,
                }).Where(c => codeMasterIds.Contains(c.CodeId)).AsNoTracking().ToList();
                var plantItems = _context.Plant.Select(s => new
                {
                    s.PlantCode,
                    s.Description,
                    s.PlantId,
                }).AsNoTracking().ToList();
                var templateTestCaseFormIds = templateTestCaseCheckLists.Where(w => w.TemplateTestCaseFormId != null).Select(s => s.TemplateTestCaseFormId.GetValueOrDefault(0)).ToList();
                var templateTestCaseForm = _context.TemplateTestCaseForm.Select(s => new { s.TemplateTestCaseFormId, s.CaseNo }).Where(a => templateTestCaseFormIds.Contains(a.TemplateTestCaseFormId)).ToList();
                var templateTestCaseCheckListIds = templateTestCaseCheckList.Select(s => s.TemplateTestCaseCheckListId).ToList();
                var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Select(s => new { s.TemplateTestCaseCheckListResponseId, s.TemplateTestCaseCheckListId }).Where(w => templateTestCaseCheckListIds.Contains(w.TemplateTestCaseCheckListId.Value)).ToList();
                var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.TemplateTestCaseCheckListResponseId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.TemplateTestCaseCheckListResponseId.Value)).ToList();
                var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
                var templateTestCaseCheckListResponseResponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
                var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
                var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                var userIds = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIds.Contains(w.UserId) && w.SessionId != null).ToList();
                templateTestCaseCheckList.ForEach(s =>
                {
                    var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.TemplateTestCaseCheckListId == s.TemplateTestCaseCheckListId).Select(s => new { s.TemplateTestCaseCheckListResponseId, s.TemplateTestCaseCheckListId }).ToList();
                    var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
                    var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.TemplateTestCaseCheckListResponseId.Value)).Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.TemplateTestCaseCheckListResponseId }).ToList();
                    var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
                    var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
                    var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
                    var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                    var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();
                    var participantUsers = string.Empty;
                    List<long> responsibilityUsers = new List<long>();
                    if (appUserss != null && appUserss.Count > 0)
                    {
                        var users = appUserss.Select(s => s.SessionId).ToList();
                        var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
                        participantUsers = string.Join(",", users);
                        if (usersIdsBy != null && usersIdsBy.Count > 0)
                        {
                            responsibilityUsers.AddRange(usersIdsBy);
                        }
                    }
                    TemplateTestCaseCheckListModel templateTestCaseLink = new TemplateTestCaseCheckListModel();

                    templateTestCaseLink.ResponsibilityUsers = responsibilityUsers;
                    templateTestCaseLink.ParticipantUsers = participantUsers;
                    templateTestCaseLink.CaseNo = s.TemplateTestCaseFormId != null && templateTestCaseForm != null ? templateTestCaseForm.FirstOrDefault(f => f.TemplateTestCaseFormId == s.TemplateTestCaseFormId)?.CaseNo : "";
                    templateTestCaseLink.TemplateTestCaseCheckListId = s.TemplateTestCaseCheckListId;
                    templateTestCaseLink.TemplateTestCaseId = s.TemplateTestCaseId;
                    templateTestCaseLink.Description = s.Description;
                    templateTestCaseLink.Instruction = s.Instruction;
                    templateTestCaseLink.AddedByUserId = s.AddedByUserId;
                    templateTestCaseLink.AddedDate = s.AddedDate;
                    templateTestCaseLink.StatusCodeId = s.StatusCodeId;
                    templateTestCaseLink.ModifiedByUserID = s.ModifiedByUserId;
                    templateTestCaseLink.ModifiedDate = DateTime.Now;
                    templateTestCaseLink.SessionId = s.SessionId;
                    templateTestCaseLink.AddedByUser = s.AddedByUserId != null && appUsersList != null ? appUsersList.FirstOrDefault(u => u.UserId == s.AddedByUserId)?.UserName : "";
                    templateTestCaseLink.ModifiedByUser = s.ModifiedByUserId != null && appUsersList != null ? appUsersList.FirstOrDefault(u => u.UserId == s.ModifiedByUserId)?.UserName : "";
                    templateTestCaseLink.StatusCode = s.StatusCodeId != null && codeMasters != null ? codeMasters.FirstOrDefault(c => c.CodeId == s.StatusCodeId)?.CodeValue : "";
                    templateTestCaseLink.No = s.No;
                    templateTestCaseLink.Nos = s.TemplateTestCaseFormId > 0 && s.IsMasterTemplate != true ? ("S" + s.No) : ("T" + s.No);
                    templateTestCaseLink.SeqNo = s.SeqNo;
                    templateTestCaseLink.IsResponsibility = s.IsResponsibility;
                    templateTestCaseLink.IsResponsibilityFlag = s.IsResponsibility == true ? "Yes" : "No";
                    templateTestCaseLink.IsSkipTest = s.IsSkipTest;
                    templateTestCaseLink.IsSkipTestFlag = s.IsSkipTest == true ? "Yes" : "No";
                    templateTestCaseLink.TopicId = s.TopicId;
                    templateTestCaseLink.TemplateTestCaseFormId = s.TemplateTestCaseFormId;
                    templateTestCaseLink.TemplateTestCaseLinkId = s.TemplateTestCaseLinkId;
                    templateTestCaseLink.LocationToSaveId = s.TemplateTestCaseLink?.LocationToSaveId;
                    templateTestCaseLink.NameOfTemplate = s.TemplateTestCaseLink?.NameOfTemplate;
                    if (isTopic == "Topic")
                    {
                        templateTestCaseLink.TemplateTestCaseCheckListResponseModels = GetTemplateTestCaseCheckListResponse(s.TemplateTestCaseCheckListId);
                        templateTestCaseLink.TemplateTestCaseCheckListResponseDutyModels = ApplicationWikiLineDuty(s.TemplateTestCaseCheckListId);
                    }
                    templateTestCaseLink.IsMasterTemplate = s.IsMasterTemplate;
                    templateTestCaseCheckListModels.Add(templateTestCaseLink);
                });
            }
            return templateTestCaseCheckListModels;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseCheckList")]
        public TemplateTestCaseCheckListModel InsertTemplateTestCaseCheckList(TemplateTestCaseCheckListModel value)
        {
            int? no = null;
            int? seqNos = null;
            if (value.TemplateTestCaseFormId != null)
            {
                no = _context.TemplateTestCaseCheckList.OrderByDescending(a => a.No).Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId && w.IsMasterTemplate != true).Count();

                seqNos = _context.TemplateTestCaseCheckList.OrderByDescending(a => a.SeqNo).Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId).Count();
            }
            else
            {
                no = _context.TemplateTestCaseCheckList.OrderByDescending(a => a.No).Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId && w.IsMasterTemplate != true).Count();

                seqNos = _context.TemplateTestCaseCheckList.OrderByDescending(a => a.SeqNo).Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId && w.TemplateTestCaseFormId == value.TemplateTestCaseFormId && w.IsMasterTemplate != true).Count();
            }
            var sessionId = Guid.NewGuid();
            var templateTestCaseLink = new TemplateTestCaseCheckList
            {
                TemplateTestCaseId = value.TemplateTestCaseId,
                Description = value.Description,
                Instruction = value.Instruction,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                ModifiedByUserId = value.AddedByUserID,
                ModifiedDate = DateTime.Now,
                SessionId = sessionId,
                No = (no == 0 ? 1 : (no + 1)).ToString(),
                SeqNo = (seqNos == 0 ? 1 : (seqNos + 1)).ToString(),
                //IsMasterTemplate = value.TemplateTestCaseFormId > 0 ? true : false,
                TemplateTestCaseLinkId = value.TemplateTestCaseLinkId,
                TemplateTestCaseFormId = value.TemplateTestCaseFormId > 0 ? value.TemplateTestCaseFormId : null,
            };
            _context.TemplateTestCaseCheckList.Add(templateTestCaseLink);
            _context.SaveChanges();
            value.No = templateTestCaseLink.No;
            value.SeqNo = templateTestCaseLink.SeqNo;
            value.TemplateTestCaseCheckListId = templateTestCaseLink.TemplateTestCaseCheckListId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseCheckListSeqNo")]
        public TemplateTestCaseCheckListModel UpdateTemplateTestCaseCheckListSeqNo(TemplateTestCaseCheckListModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.TemplateTestCaseCheckList.SingleOrDefault(p => p.TemplateTestCaseCheckListId == value.TemplateTestCaseCheckListId);
            templateTestCase.SeqNo = value.SeqNo;
            templateTestCase.No = value.No;
            if (templateTestCase.IsMasterTemplate == true)
            {
                // templateTestCase.IsMasterTemplate = false;
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseCheckList")]
        public TemplateTestCaseCheckListModel UpdateTemplateTestCaseCheckList(TemplateTestCaseCheckListModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var templateTestCase = _context.TemplateTestCaseCheckList.SingleOrDefault(p => p.TemplateTestCaseCheckListId == value.TemplateTestCaseCheckListId);
            var oldNo = value.No;
            //var currentSeqNo = templateTestCase.SeqNo;
            //var nos = templateTestCase.No;
            /*var templateTestCaseSeq = _context.TemplateTestCaseCheckList.SingleOrDefault(p => p.TemplateTestCaseId == templateTestCase.TemplateTestCaseId && p.SeqNo == value.SeqNo);
            if (templateTestCaseSeq != null)
            {
                oldNo = templateTestCaseSeq.No;
                templateTestCaseSeq.SeqNo = currentSeqNo;
                templateTestCaseSeq.No = nos;
                _context.SaveChanges();
            }*/
            templateTestCase.TemplateTestCaseId = value.TemplateTestCaseId;
            templateTestCase.Description = value.Description;
            templateTestCase.Instruction = value.Instruction;
            templateTestCase.ModifiedDate = DateTime.Now;
            templateTestCase.ModifiedByUserId = value.ModifiedByUserID;
            templateTestCase.SessionId = value.SessionId;
            //templateTestCase.SeqNo = value.SeqNo;
            //templateTestCase.No = oldNo;
            templateTestCase.IsResponsibility = value.IsResponsibility;
            templateTestCase.IsSkipTest = value.IsSkipTest;
            templateTestCase.TemplateTestCaseLinkId = value.TemplateTestCaseLinkId;
            if (templateTestCase.TemplateTestCaseFormId != null)
            {
                templateTestCase.TemplateTestCaseFormId = value.TemplateTestCaseFormId > 0 ? value.TemplateTestCaseFormId : null;
            }
            //value.No = oldNo;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseProposal")]
        public TemplateTestCaseProposalModel InsertTemplateTestCaseProposal(TemplateTestCaseProposalModel value)
        {
            var templateTestCaseProposalIds = _context.TemplateTestCaseProposal.Where(w => w.TemplateTestCaseId == value.TemplateTestCaseId).Select(s => s.UserId).ToList();

            if (value.UserIds != null && value.UserIds.Count > 0)
            {
                value.UserIds.ForEach(s =>
                {
                    if (!templateTestCaseProposalIds.Contains(s))
                    {
                        var templateTestCaseLink = new TemplateTestCaseProposal
                        {
                            TemplateTestCaseId = value.TemplateTestCaseId,
                            UserId = s,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = value.StatusCodeID.Value,
                            ModifiedByUserId = value.AddedByUserID,
                            ModifiedDate = DateTime.Now,
                            UserType = value.UserType,
                            IsByCaseCompany = value.IsByCaseCompany,
                        };
                        _context.TemplateTestCaseProposal.Add(templateTestCaseLink);
                        _context.SaveChanges();
                    }
                });
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseProposal")]
        public TemplateTestCaseProposalModel UpdateTemplateTestCaseProposal(TemplateTestCaseProposalModel value)
        {
            var templateTestCase = _context.TemplateTestCaseProposal.SingleOrDefault(p => p.TemplateTestCaseProposalId == value.TemplateTestCaseProposalId);
            templateTestCase.UserId = value.UserId;
            templateTestCase.ModifiedDate = DateTime.Now;
            templateTestCase.ModifiedByUserId = value.AddedByUserID;
            templateTestCase.UserType = value.UserType;
            templateTestCase.ReassignUserId = value.ReassignUserId;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseCheckListTopic")]
        public TemplateTestCaseCheckListModel UpdateTemplateTestCaseCheckListTopic(TemplateTestCaseCheckListModel value)
        {
            var templateTestCase = _context.TemplateTestCaseCheckList.SingleOrDefault(p => p.TemplateTestCaseCheckListId == value.TemplateTestCaseCheckListId);
            templateTestCase.TopicId = value.TopicId;
            if (templateTestCase != null)
            {
                SendMailTemplateCheckListForum(templateTestCase);
            }
            _context.SaveChanges();
            return value;
        }
        private void SendMailTemplateCheckListForum(TemplateTestCaseCheckList value)
        {
            var templateTestCase = _context.TemplateTestCase.FirstOrDefault(f => f.TemplateTestCaseId == value.TemplateTestCaseId);
            var templateTestCaseForm = _context.TemplateTestCaseForm.FirstOrDefault(f => f.TemplateTestCaseFormId == value.TemplateTestCaseFormId);
            var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Select(s => new { s.TemplateTestCaseCheckListResponseId, s.TemplateTestCaseCheckListId }).Where(w => w.TemplateTestCaseCheckListId == value.TemplateTestCaseCheckListId).ToList();
            var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
            var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.Select(s => new { s.TemplateTestCaseCheckListResponseDutyId, s.TemplateTestCaseCheckListResponseId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.TemplateTestCaseCheckListResponseId.Value)).ToList();
            var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
            var templateTestCaseCheckListResponseResponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).Select(s => new { s.EmployeeId, s.TemplateTestCaseCheckListResponseDutyId }).ToList();
            var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
            MailServiceModel mailServiceModel = new MailServiceModel();
            if (empIds != null && empIds.Count > 0)
            {
                var sqlQuery = "Select  * from view_GetEmployee where Status='Active' and EmployeeID in(" + string.Join(",", empIds.Distinct().ToList()) + ")";
                var result = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
                var employee = result.Select(s => new
                {
                    s.EmployeeID,
                    s.Status,
                    s.FirstName,
                    s.LastName,
                    s.NickName,
                    s.Gender,
                    s.DepartmentName,
                    s.PlantID,
                    UserId = s.UserID,
                    s.Email
                }).Where(w => w.Email != null).ToList();
                if (employee != null && employee.Count > 0)
                {
                    var message = "";
                    mailServiceModel.CcMail = employee.Where(w => empIds.Contains(w.UserId.Value)).Select(s => new MailHeaderModel
                    { Email = s.Email, Name = s.FirstName }).ToList();
                    var userNameId = value.AddedByUserId != null ? value.AddedByUserId : value.ModifiedByUserId;
                    var username = _context.ApplicationUser.FirstOrDefault(f => f.UserId == userNameId)?.UserName;
                    message += "<table>";
                    mailServiceModel.Subject = templateTestCaseForm?.CaseNo + "/" + templateTestCaseForm?.SubjectName + "/" + value?.SeqNo;
                    message += "<tr><td>Description:</td><td>" + value?.Description + "</td></tr>";
                    message += "<tr><td>Instruction:</td><td>" + value?.Instruction + "</td></tr>";
                    message += "</table>";
                    mailServiceModel.FromName = username;
                    mailServiceModel.Body = message;
                    List<DocumentsModel> documentsModels = new List<DocumentsModel>();
                    documentsModels = _context.Documents.Select(e => new DocumentsModel { DocumentID = e.DocumentId, IsCompressed = e.IsCompressed, FileName = e.FileName, ContentType = e.ContentType, SubjectName = e.SubjectName, FilterProfileTypeId = e.FilterProfileTypeId, SessionId = e.SessionId, IsLatest = e.IsLatest }).Where(n => n.SessionId == value.SessionId && n.IsLatest == true).ToList();
                    mailServiceModel.Attachments = addAttachment(documentsModels);
                    _mailService.SendEmailService(mailServiceModel);
                }
            }
        }
        private AttachmentsPathModel addAttachment(List<DocumentsModel> documentsModels)
        {
            AttachmentsPathModel attachmentsPathModel = new AttachmentsPathModel();
            if (documentsModels != null)
            {
                List<string> pathList = new List<string>();
                string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
                string newFolderName = "Attachment";
                string serverPath = System.IO.Path.Combine(folderName, newFolderName);
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                var sessionId = Guid.NewGuid();
                string FromLocation = folderName + @"\" + newFolderName;
                serverPath = System.IO.Path.Combine(FromLocation, sessionId.ToString());
                if (!System.IO.Directory.Exists(serverPath))
                {
                    System.IO.Directory.CreateDirectory(serverPath);
                }
                string newPath = folderName + @"\" + newFolderName + @"\" + sessionId.ToString();
                documentsModels.ForEach(s =>
                {
                    var doc = _context.Documents.FirstOrDefault(f => f.DocumentId == s.DocumentID)?.FileData;
                    string FromLocations = newPath + @"\" + s.FileName;
                    var compressedData = s.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(doc) : doc;
                    System.IO.File.WriteAllBytes((string)FromLocations, compressedData);
                    pathList.Add(FromLocations);
                });
                attachmentsPathModel.PathName = newPath;
                attachmentsPathModel.Attachments = pathList;
            }
            return attachmentsPathModel;
        }
        [HttpDelete]
        [Route("DeleteTemplateTestCaseCheckList")]
        public ActionResult<string> DeleteTemplateTestCaseCheckList(int id)
        {
            /*try
            {*/
            var templateTestCaseCheckList = _context.TemplateTestCaseCheckList.SingleOrDefault(p => p.TemplateTestCaseCheckListId == id);
            if (templateTestCaseCheckList != null)
            {
                var productionActivityMaster = _context.TemplateTestCaseCheckListResponse.Where(p => p.TemplateTestCaseCheckListId == id).ToList();
                if (productionActivityMaster != null)
                {
                    productionActivityMaster.ForEach(s =>
                    {
                        var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId);
                        if (templateTestCaseCheckListResponse != null)
                        {
                            var responeDuty = _context.TemplateTestCaseCheckListResponseDuty.Where(w => w.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId).ToList();
                            if (responeDuty != null)
                            {
                                responeDuty.ForEach(a =>
                                {
                                    var ApplicationWikiLineDuty = _context.TemplateTestCaseCheckListResponseDuty.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseDutyId == a.TemplateTestCaseCheckListResponseDutyId);
                                    var wikiresponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(p => p.TemplateTestCaseCheckListResponseDutyId == a.TemplateTestCaseCheckListResponseDutyId).ToList();
                                    if (wikiresponsible != null)
                                    {
                                        _context.TemplateTestCaseCheckListResponseResponsible.RemoveRange(wikiresponsible);
                                        _context.SaveChanges();
                                    }
                                    _context.TemplateTestCaseCheckListResponseDuty.Remove(ApplicationWikiLineDuty);
                                    _context.SaveChanges();
                                });
                            }
                            var applicationWikiWeeklyRemove = _context.TemplateTestCaseCheckListResponseWeekly.Where(p => p.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId).ToList();
                            if (applicationWikiWeeklyRemove != null)
                            {
                                _context.TemplateTestCaseCheckListResponseWeekly.RemoveRange(applicationWikiWeeklyRemove);
                                _context.SaveChanges();
                            }
                            var TemplateTestCaseCheckListResponseRecurrenceRemove = _context.TemplateTestCaseCheckListResponseRecurrence.Where(p => p.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId).ToList();
                            if (TemplateTestCaseCheckListResponseRecurrenceRemove != null)
                            {
                                _context.TemplateTestCaseCheckListResponseRecurrence.RemoveRange(TemplateTestCaseCheckListResponseRecurrenceRemove);
                                _context.SaveChanges();
                            }
                            _context.TemplateTestCaseCheckListResponse.Remove(templateTestCaseCheckListResponse);
                            _context.SaveChanges();
                        }
                    });
                    _context.TemplateTestCaseCheckList.Remove(templateTestCaseCheckList);
                    _context.SaveChanges();
                    //updateNoAndSeqNo(templateTestCaseCheckList);
                }
            }
            return Ok("Record deleted.");
            /*}
            catch (Exception ex)
            {
                throw new Exception("TemplateTestCase CheckList Cannot be delete! Reference to others");
            }*/
        }
        private void updateNoAndSeqNo(TemplateTestCaseCheckList templateTestCaseCheckList)
        {
            var temCaseForm = _context.TemplateTestCaseCheckList.Where(w => w.TemplateTestCaseId == templateTestCaseCheckList.TemplateTestCaseId && w.TemplateTestCaseFormId == templateTestCaseCheckList.TemplateTestCaseFormId).ToList();
            var no = temCaseForm.OrderBy(a => a.TemplateTestCaseCheckListId).Where(w => w.TemplateTestCaseId == templateTestCaseCheckList.TemplateTestCaseId && w.TemplateTestCaseFormId == templateTestCaseCheckList.TemplateTestCaseFormId && w.IsMasterTemplate != true).ToList();
            var seqNo = temCaseForm.OrderBy(a => a.TemplateTestCaseCheckListId).Where(w => w.TemplateTestCaseId == templateTestCaseCheckList.TemplateTestCaseId && w.TemplateTestCaseFormId == templateTestCaseCheckList.TemplateTestCaseFormId).ToList();
            if (seqNo != null)
            {
                int sno = 1;
                seqNo.ForEach(s =>
                {
                    var query = string.Format("Update TemplateTestCaseCheckList Set SeqNo='{0}'  Where TemplateTestCaseCheckListId= {1} ", sno, s.TemplateTestCaseCheckListId);
                    _context.Database.ExecuteSqlRaw(query);
                    sno++;

                });
            }
            if (no != null)
            {
                int snoo = 1;
                no.ForEach(s =>
                {
                    var query = string.Format("Update TemplateTestCaseCheckList Set No='{0}'  Where TemplateTestCaseCheckListId= {1} ", snoo, s.TemplateTestCaseCheckListId);
                    _context.Database.ExecuteSqlRaw(query);
                    snoo++;

                });
            }
        }
        [HttpGet]
        [Route("GetTemplateTestCaseCheckListResponse")]
        public List<TemplateTestCaseCheckListResponseModel> GetTemplateTestCaseCheckListResponse(long id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.TemplateTestCaseCheckListResponse
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("FunctionLinkNavigation")
                                .Include("PageLinkNavigation")
                                .Include(c => c.NotificationAdviceType)
                                .Include(a => a.TemplateTestCaseCheckListResponseWeekly)
                                .Where(w => w.TemplateTestCaseCheckListId == id).OrderByDescending(o => o.TemplateTestCaseCheckListResponseId).AsNoTracking().ToList();
            List<TemplateTestCaseCheckListResponseModel> applicationWikiLineModels = new List<TemplateTestCaseCheckListResponseModel>();
            applicationWikis.ForEach(s =>
            {
                TemplateTestCaseCheckListResponseModel applicationWikiLineModel = new TemplateTestCaseCheckListResponseModel
                {
                    TemplateTestCaseCheckListResponseId = s.TemplateTestCaseCheckListResponseId,
                    TemplateTestCaseCheckListId = s.TemplateTestCaseCheckListId,
                    DutyId = s.DutyId,
                    DutyName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId).Value : string.Empty,
                    Responsibility = s.Responsibility,
                    PageLink = s.PageLink,
                    PageLinkName = s.PageLinkNavigation?.PermissionName,
                    FunctionLink = s.FunctionLink,
                    FunctionLinkName = s.FunctionLinkNavigation?.PermissionName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    NotificationAdvice = s.NotificationAdvice,
                    NotificationAdviceFlag = s.NotificationAdvice == true ? "Yes" : "No",
                    NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                    CustomId = s.CustomId,
                    RepeatId = s.RepeatId,
                    DueDate = s.DueDate,
                    Monthly = s.Monthly,
                    Yearly = s.Yearly,
                    EventDescription = s.EventDescription,
                    DaysOfWeek = s.DaysOfWeek,
                    SessionId = s.SessionId,
                    NotificationStatusId = s.NotificationStatusId,
                    ScreenId = s.ScreenId,
                    Title = s.Title,
                    Message = s.Message,
                    NotifyEndDate = s.NotifyEndDate,
                    NotificationAdviceType = s.NotificationAdviceType?.CodeValue,
                    WeeklyIds = s.TemplateTestCaseCheckListResponseWeekly != null ? s.TemplateTestCaseCheckListResponseWeekly.Where(c => c.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    DaysOfWeekIds = s.TemplateTestCaseCheckListResponseWeekly != null ? s.TemplateTestCaseCheckListResponseWeekly.Where(c => c.TemplateTestCaseCheckListResponseId == s.TemplateTestCaseCheckListResponseId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    IsAllowDocAccess = s.IsAllowDocAccess,
                    TemplateTestCaseCheckListResponseDutyModels = GetApplicationWikiLineDutyList(s.TemplateTestCaseCheckListResponseId),
                };
                applicationWikiLineModels.Add(applicationWikiLineModel);
            });

            return applicationWikiLineModels;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseCheckListResponse")]
        public TemplateTestCaseCheckListResponseModel InsertTemplateTestCaseCheckListResponse(TemplateTestCaseCheckListResponseModel value)
        {
            var SessionId = Guid.NewGuid();
            var ApplicationWikiLine = new TemplateTestCaseCheckListResponse
            {
                TemplateTestCaseCheckListId = value.TemplateTestCaseCheckListId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = value.ScreenId,
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess = value.IsAllowDocAccess,
            };
            _context.TemplateTestCaseCheckListResponse.Add(ApplicationWikiLine);
            _context.SaveChanges();
            value.TemplateTestCaseCheckListResponseId = ApplicationWikiLine.TemplateTestCaseCheckListResponseId;
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                    {
                        TemplateTestCaseCheckListResponseId = ApplicationWikiLine.TemplateTestCaseCheckListResponseId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                    {
                        TemplateTestCaseCheckListResponseId = ApplicationWikiLine.TemplateTestCaseCheckListResponseId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            value.SessionId = SessionId;
            value.NotificationAdvice = ApplicationWikiLine.NotificationAdvice;
            //NotificationHandeler(value);
            return value;
        }
        [HttpPut]
        [Route("UpdateTemplateTestCaseCheckListResponse")]
        public TemplateTestCaseCheckListResponseModel updateTemplateTestCaseCheckListResponse(TemplateTestCaseCheckListResponseModel value)
        {
            var productionActivityMaster = _context.TemplateTestCaseCheckListResponse.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseId == value.TemplateTestCaseCheckListResponseId);

            productionActivityMaster.TemplateTestCaseCheckListId = value.TemplateTestCaseCheckListId;
            productionActivityMaster.DutyId = value.DutyId;
            productionActivityMaster.Responsibility = value.Responsibility;
            productionActivityMaster.FunctionLink = value.FunctionLink;
            productionActivityMaster.PageLink = value.PageLink;
            productionActivityMaster.ModifiedByUserId = value.AddedByUserID;
            productionActivityMaster.ModifiedDate = DateTime.Now;
            productionActivityMaster.StatusCodeId = value.StatusCodeID.Value;
            productionActivityMaster.NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false;
            productionActivityMaster.NotificationAdviceTypeId = value.NotificationAdviceTypeId;
            productionActivityMaster.RepeatId = value.RepeatId;
            productionActivityMaster.CustomId = value.CustomId;
            productionActivityMaster.DueDate = value.DueDate == null ? DateTime.Now : value.DueDate;
            productionActivityMaster.Monthly = value.Monthly;
            productionActivityMaster.Yearly = value.Yearly;
            productionActivityMaster.EventDescription = value.EventDescription;
            productionActivityMaster.DaysOfWeek = value.DaysOfWeek;
            // SessionId = SessionId,
            productionActivityMaster.NotificationStatusId = value.NotificationStatusId;
            productionActivityMaster.Title = value.Title;
            productionActivityMaster.Message = value.Message;
            productionActivityMaster.ScreenId = value.ScreenId;
            productionActivityMaster.NotifyEndDate = value.NotifyEndDate;
            productionActivityMaster.IsAllowDocAccess = value.IsAllowDocAccess;
            _context.SaveChanges();
            var applicationWikiWeeklyRemove = _context.TemplateTestCaseCheckListResponseWeekly.Where(p => p.TemplateTestCaseCheckListResponseId == value.TemplateTestCaseCheckListResponseId).ToList();
            if (applicationWikiWeeklyRemove != null)
            {
                _context.TemplateTestCaseCheckListResponseWeekly.RemoveRange(applicationWikiWeeklyRemove);
                _context.SaveChanges();
            }
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                    {
                        TemplateTestCaseCheckListResponseId = productionActivityMaster.TemplateTestCaseCheckListResponseId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new TemplateTestCaseCheckListResponseWeekly()
                    {
                        TemplateTestCaseCheckListResponseId = productionActivityMaster.TemplateTestCaseCheckListResponseId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.TemplateTestCaseCheckListResponseWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            value.NotificationAdvice = productionActivityMaster.NotificationAdvice;
            value.TemplateTestCaseCheckListResponseId = productionActivityMaster.TemplateTestCaseCheckListResponseId;
            return value;
        }
        [HttpPost]
        [Route("InsertTemplateTestCaseCheckListResponseRecurrence")]
        public TemplateTestCaseCheckListResponseRecurrenceModel InsertApplicationWikiRecurrence(TemplateTestCaseCheckListResponseRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.TemplateTestCaseCheckListResponseRecurrence.Where(p => p.TemplateTestCaseCheckListResponseId == value.TemplateTestCaseCheckListResponseId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.TemplateTestCaseCheckListResponseRecurrence.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseId == value.TemplateTestCaseCheckListResponseId);
                ApplicationWikiRecurrences.TemplateTestCaseCheckListResponseId = value.TemplateTestCaseCheckListResponseId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new TemplateTestCaseCheckListResponseRecurrence
                {
                    TemplateTestCaseCheckListResponseId = value.TemplateTestCaseCheckListResponseId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.TemplateTestCaseCheckListResponseRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.TemplateTestCaseCheckListResponseRecurrenceId = ApplicationWikiRecurrence.TemplateTestCaseCheckListResponseRecurrenceId;
                return value;
            }
        }

        [HttpDelete]
        [Route("DeleteTemplateTestCaseCheckListResponse")]
        public ActionResult<string> deleteTemplateTestCaseCheckListResponse(int id)
        {
            try
            {
                var productionActivityMaster = _context.TemplateTestCaseCheckListResponse.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseId == id);
                if (productionActivityMaster != null)
                {
                    var applicationWikiWeeklyRemove = _context.TemplateTestCaseCheckListResponseWeekly.Where(p => p.TemplateTestCaseCheckListResponseId == id).ToList();
                    if (applicationWikiWeeklyRemove != null)
                    {
                        _context.TemplateTestCaseCheckListResponseWeekly.RemoveRange(applicationWikiWeeklyRemove);
                        _context.SaveChanges();
                    }
                    var TemplateTestCaseCheckListResponseRecurrenceRemove = _context.TemplateTestCaseCheckListResponseRecurrence.Where(p => p.TemplateTestCaseCheckListResponseId == id).ToList();
                    if (TemplateTestCaseCheckListResponseRecurrenceRemove != null)
                    {
                        _context.TemplateTestCaseCheckListResponseRecurrence.RemoveRange(TemplateTestCaseCheckListResponseRecurrenceRemove);
                        _context.SaveChanges();
                    }
                    var responeDuty = _context.TemplateTestCaseCheckListResponseDuty.Where(w => w.TemplateTestCaseCheckListResponseId == id).ToList();
                    if (responeDuty != null)
                    {
                        responeDuty.ForEach(a =>
                        {
                            var ApplicationWikiLineDuty = _context.TemplateTestCaseCheckListResponseDuty.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseDutyId == a.TemplateTestCaseCheckListResponseDutyId);
                            var wikiresponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(p => p.TemplateTestCaseCheckListResponseDutyId == a.TemplateTestCaseCheckListResponseDutyId).ToList();
                            if (wikiresponsible != null)
                            {
                                _context.TemplateTestCaseCheckListResponseResponsible.RemoveRange(wikiresponsible);
                                _context.SaveChanges();
                            }
                            _context.TemplateTestCaseCheckListResponseDuty.Remove(ApplicationWikiLineDuty);
                            _context.SaveChanges();
                        });
                    }
                    _context.TemplateTestCaseCheckListResponse.Remove(productionActivityMaster);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("ProductionActivityMasterRespons Cannot be delete! Reference to others");
            }
        }
        [HttpGet]
        [Route("GetTemplateTestCaseCheckListResponseRecurrence")]
        public List<TemplateTestCaseCheckListResponseRecurrenceModel> GetTemplateTestCaseCheckListResponseRecurrence(long id)
        {
            List<TemplateTestCaseCheckListResponseRecurrenceModel> productionActivityMasterResponsRecurrenceModels = new List<TemplateTestCaseCheckListResponseRecurrenceModel>();
            var recurrence = _context.TemplateTestCaseCheckListResponseRecurrence
                 .Include(a => a.AddedByUser)
                .Include(a => a.ModifiedByUser)
                .Include(b => b.StatusCode)
                .Include(c => c.Type)
                .Include(d => d.OccurenceOption)
                .AsNoTracking().Where(w => w.TemplateTestCaseCheckListResponseId == id).OrderByDescending(o => o.TemplateTestCaseCheckListResponseRecurrenceId).AsNoTracking().ToList();
            recurrence.ForEach(s =>
            {
                TemplateTestCaseCheckListResponseRecurrenceModel productionActivityMasterResponsRecurrenceModel = new TemplateTestCaseCheckListResponseRecurrenceModel
                {
                    TemplateTestCaseCheckListResponseRecurrenceId = s.TemplateTestCaseCheckListResponseRecurrenceId,
                    TemplateTestCaseCheckListResponseId = s.TemplateTestCaseCheckListResponseId,
                    TypeId = s.TypeId,
                    RepeatNos = s.RepeatNos,
                    OccurenceOptionId = s.OccurenceOptionId,
                    NoOfOccurences = s.NoOfOccurences,
                    Sunday = s.Sunday,
                    Monday = s.Monday,
                    Tuesday = s.Tuesday,
                    Wednesday = s.Wednesday,
                    Thursday = s.Thursday,
                    Friday = s.Friday,
                    Saturyday = s.Saturyday,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode?.CodeValue,
                    TypeName = s.Type.CodeValue,
                    OccurenceOptionName = s.OccurenceOption?.CodeValue,
                };
                productionActivityMasterResponsRecurrenceModels.Add(productionActivityMasterResponsRecurrenceModel);
            });
            return productionActivityMasterResponsRecurrenceModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiLineDutyList")]
        public List<TemplateTestCaseCheckListResponseDutyModel> GetApplicationWikiLineDutyList(long id)
        {
            List<TemplateTestCaseCheckListResponseDutyModel> applicationWikiLineDutyModels = new List<TemplateTestCaseCheckListResponseDutyModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.TemplateTestCaseCheckListResponseDuty
                                .Include("Departmant")
                                .Include("Designation")
                                .Include("Company")
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("Employee")
                                .Where(w => w.TemplateTestCaseCheckListResponseId == id).OrderByDescending(o => o.TemplateTestCaseCheckListResponseDutyId).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                TemplateTestCaseCheckListResponseDutyModel applicationWikiLineDutyModel = new TemplateTestCaseCheckListResponseDutyModel
                {
                    TemplateTestCaseCheckListResponseId = s.TemplateTestCaseCheckListResponseId,
                    TemplateTestCaseCheckListResponseDutyId = s.TemplateTestCaseCheckListResponseDutyId,
                    DepartmentId = s.DepartmantId,
                    EmployeeId = s.EmployeeId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Departmant?.Name,
                    DesignationNumber = s.DesignationNumber,
                    CompanyId = s.CompanyId,
                    PlantCompanyName = s.Company?.Description,
                    DesignationId = s.DesignationId,
                    DesignationName = s.Designation?.Name,
                    DutyNo = s.DutyNo,
                    DutyNoName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    Type = s.Type,
                    Description = s.Description,
                    WikiResponsibilityModels = GetWikiResponsibleByID((int)s.TemplateTestCaseCheckListResponseDutyId)
                    //EmployeeIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.EmployeeId).ToList(),
                    //UserGroupIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.UserGroupId).Distinct().ToList(),
                };
                applicationWikiLineDutyModels.Add(applicationWikiLineDutyModel);
            });

            return applicationWikiLineDutyModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiLineDuty")]
        public List<TemplateTestCaseCheckListResponseDutyModel> ApplicationWikiLineDuty(long id)
        {
            var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Where(w => w.TemplateTestCaseCheckListId == id).Select(s => s.TemplateTestCaseCheckListResponseId).ToList();
            List<TemplateTestCaseCheckListResponseDutyModel> applicationWikiLineDutyModels = new List<TemplateTestCaseCheckListResponseDutyModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.TemplateTestCaseCheckListResponseDuty
                                .Include("Departmant")
                                .Include("Designation")
                                .Include("Company")
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("Employee")
                                .Where(w => templateTestCaseCheckListResponse.Contains(w.TemplateTestCaseCheckListResponseId.Value)).OrderByDescending(o => o.TemplateTestCaseCheckListResponseDutyId).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                TemplateTestCaseCheckListResponseDutyModel applicationWikiLineDutyModel = new TemplateTestCaseCheckListResponseDutyModel
                {
                    TemplateTestCaseCheckListResponseId = s.TemplateTestCaseCheckListResponseId,
                    TemplateTestCaseCheckListResponseDutyId = s.TemplateTestCaseCheckListResponseDutyId,
                    DepartmentId = s.DepartmantId,
                    EmployeeId = s.EmployeeId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Departmant?.Name,
                    DesignationNumber = s.DesignationNumber,
                    CompanyId = s.CompanyId,
                    PlantCompanyName = s.Company?.Description,
                    DesignationId = s.DesignationId,
                    DesignationName = s.Designation?.Name,
                    DutyNo = s.DutyNo,
                    DutyNoName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    Type = s.Type,
                    Description = s.Description,
                    WikiResponsibilityModels = GetWikiResponsibleByID((int)s.TemplateTestCaseCheckListResponseDutyId)
                    //EmployeeIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.EmployeeId).ToList(),
                    //UserGroupIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.UserGroupId).Distinct().ToList(),
                };
                applicationWikiLineDutyModels.Add(applicationWikiLineDutyModel);
            });

            return applicationWikiLineDutyModels;
        }
        [HttpGet]
        [Route("GetWikiResponsibleByID")]
        public List<TemplateTestCaseCheckListResponseResponsibleModel> GetWikiResponsibleByID(int? id)
        {
            List<TemplateTestCaseCheckListResponseResponsibleModel> wikiResponsibilityModels = new List<TemplateTestCaseCheckListResponseResponsibleModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var usergroup = _context.UserGroupUser.Include(u => u.UserGroup).ToList();
            var wikiResponsible = _context.TemplateTestCaseCheckListResponseResponsible

                                .Include(o => o.Employee)
                                .Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.UserGroup)
                                .Where(s => s.TemplateTestCaseCheckListResponseDutyId == id.Value).AsNoTracking().ToList();
            wikiResponsible.ForEach(s =>
            {
                TemplateTestCaseCheckListResponseResponsibleModel wikiResponsibilityModel = new TemplateTestCaseCheckListResponseResponsibleModel
                {
                    WikiResponsibilityID = s.WikiResponsibilityId,
                    TemplateTestCaseCheckListResponseDutyId = s.TemplateTestCaseCheckListResponseDutyId,
                    EmployeeID = s.EmployeeId,
                    UserGroupID = s.UserGroupId,
                    UserID = s.UserId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Employee?.DepartmentNavigation?.Name,
                    DesignationName = s.Employee?.Designation?.Name,
                    HeadCount = s.Employee?.HeadCount,
                    UserGroupName = usergroup != null && s.Employee != null ? (usergroup.Where(w => w.UserId == s.Employee.UserId && w.UserGroupId == s.UserGroupId).Select(g => g.UserGroup?.Name).FirstOrDefault()) : "",
                };
                wikiResponsibilityModels.Add(wikiResponsibilityModel);
            });

            return wikiResponsibilityModels;
        }
        [HttpGet]
        [Route("GetWikiResponsiblesByExits")]
        public WikiResponsibilityModel GetWikiResponsiblesByExits(long? id, long? caseFormId)
        {
            WikiResponsibilityModel wikiResponsibilityModel = new WikiResponsibilityModel();
            var templateTestCaseCheckListResponse = _context.TemplateTestCaseCheckListResponse.Where(w => w.TemplateTestCaseCheckListId == caseFormId).Select(s => s.TemplateTestCaseCheckListResponseId).ToList();

            var templateTestCaseCheckListResponseDutyIds = _context.TemplateTestCaseCheckListResponseDuty.Where(w => w.CompanyId == id && templateTestCaseCheckListResponse.Contains(w.TemplateTestCaseCheckListResponseId.Value)).Select(s => s.TemplateTestCaseCheckListResponseDutyId).ToList();
            var templateTestCaseCheckListResponseResponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.TemplateTestCaseCheckListResponseDutyId.Value)).ToList();
            wikiResponsibilityModel.CompanyId = id;
            wikiResponsibilityModel.WikiResponsibilityID = 0;
            var userGroupIds = templateTestCaseCheckListResponseResponsible.Where(w => w.UserGroupId != null).Select(u => u.UserGroupId).Distinct().ToList();
            wikiResponsibilityModel.UserGroupIDs = userGroupIds != null && userGroupIds.Count > 0 ? userGroupIds : new List<long?>();
            var userIds = templateTestCaseCheckListResponseResponsible.Where(w => w.EmployeeId != null).Select(u => u.EmployeeId).Distinct().ToList();
            wikiResponsibilityModel.UserIDs = userIds != null && userIds.Count > 0 ? userIds : new List<long?>();
            return wikiResponsibilityModel;
        }
        [HttpPost]
        [Route("InsertApplicationWikiLineDuty")]
        public TemplateTestCaseCheckListResponseDutyModel InsertApplicationWikiLineDuty(TemplateTestCaseCheckListResponseDutyModel value)
        {
            TemplateTestCaseCheckListResponseModel templateTestCaseCheckListResponseModel = new TemplateTestCaseCheckListResponseModel();
            templateTestCaseCheckListResponseModel.TemplateTestCaseCheckListId = value.TemplateTestCaseCheckListResponseId;
            templateTestCaseCheckListResponseModel.AddedByUserId = value.AddedByUserID;
            templateTestCaseCheckListResponseModel.AddedByUserID = value.AddedByUserID;
            templateTestCaseCheckListResponseModel.StatusCodeID = value.StatusCodeID;
            templateTestCaseCheckListResponseModel.StatusCodeId = value.StatusCodeID;
            var templateTestCaseCheckListResponseExit = _context.TemplateTestCaseCheckListResponse.FirstOrDefault(f => f.TemplateTestCaseCheckListId == value.TemplateTestCaseCheckListResponseId)?.TemplateTestCaseCheckListResponseId;
            if (templateTestCaseCheckListResponseExit == null)
            {
                var values = InsertTemplateTestCaseCheckListResponse(templateTestCaseCheckListResponseModel);
                templateTestCaseCheckListResponseExit = values.TemplateTestCaseCheckListResponseId;
            }
            var templateTestCaseCheckListResponseDutyExit = _context.TemplateTestCaseCheckListResponseDuty.FirstOrDefault(f => f.TemplateTestCaseCheckListResponseId == templateTestCaseCheckListResponseExit && f.CompanyId == value.CompanyId)?.TemplateTestCaseCheckListResponseDutyId;
            if (templateTestCaseCheckListResponseDutyExit == null)
            {
                var ApplicationWikiLineDuty = new TemplateTestCaseCheckListResponseDuty
                {
                    TemplateTestCaseCheckListResponseId = templateTestCaseCheckListResponseExit,
                    DepartmantId = value.DepartmentId,
                    DesignationId = value.DesignationId,
                    EmployeeId = value.EmployeeId,
                    CompanyId = value.CompanyId,
                    DesignationNumber = value.DesignationNumber,
                    DutyNo = value.DutyNo,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Type = value.Type,
                    Description = value.Description,
                };
                _context.TemplateTestCaseCheckListResponseDuty.Add(ApplicationWikiLineDuty);
                _context.SaveChanges();
                value.TemplateTestCaseCheckListResponseDutyId = ApplicationWikiLineDuty.TemplateTestCaseCheckListResponseDutyId;
            }
            else
            {
                value.TemplateTestCaseCheckListResponseDutyId = templateTestCaseCheckListResponseDutyExit.Value;
            }
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {
                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.TemplateTestCaseCheckListResponseResponsible.Where(e => e.EmployeeId == s && e.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i=>i.UserGroupId).FirstOrDefault() : null,
                            TemplateTestCaseCheckListResponseDutyId = value.TemplateTestCaseCheckListResponseDutyId,
                        };
                        _context.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                    if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                    {
                        var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                        if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                        {
                            userGroupEmpIds.ForEach(emp =>
                            {
                                var existing = _context.TemplateTestCaseCheckListResponseResponsible.Where(e => e.EmployeeId == emp && e.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId).FirstOrDefault();
                                if (existing == null)
                                {
                                    var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                                    {
                                        EmployeeId = emp,
                                        UserGroupId = s,
                                        TemplateTestCaseCheckListResponseDutyId = value.TemplateTestCaseCheckListResponseDutyId,
                                    };
                                    _context.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                                    _context.SaveChanges();
                                }
                            });
                        }
                    }
                });
            }
            ModifiedCheckListByTemplateTestCaseCheckListResponseDuty(value, value.AddedByUserID.Value);
            return value;
        }
        private void ModifiedCheckListByTemplateTestCaseCheckListResponseDuty(TemplateTestCaseCheckListResponseDutyModel value, long userId)
        {
            var templateTestCaseCheckListResponseDuty = _context.TemplateTestCaseCheckListResponseDuty.FirstOrDefault(f => f.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId);
            if (templateTestCaseCheckListResponseDuty != null && templateTestCaseCheckListResponseDuty.TemplateTestCaseCheckListResponseId > 0)
            {
                var templateTestCaseCheckListId = _context.TemplateTestCaseCheckListResponse.FirstOrDefault(a => a.TemplateTestCaseCheckListResponseId == templateTestCaseCheckListResponseDuty.TemplateTestCaseCheckListResponseId)?.TemplateTestCaseCheckListId;
                if (templateTestCaseCheckListId != null && templateTestCaseCheckListId > 0)
                {
                    var templateTestCaseCheckList = _context.TemplateTestCaseCheckList.SingleOrDefault(s => s.TemplateTestCaseCheckListId == templateTestCaseCheckListId);
                    templateTestCaseCheckList.ModifiedByUserId = userId;
                    templateTestCaseCheckList.ModifiedDate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
        }
        [HttpPut]
        [Route("UpdateApplicationWikiLineDuty")]
        public TemplateTestCaseCheckListResponseDutyModel UpdateApplicationWikiLineDuty(TemplateTestCaseCheckListResponseDutyModel value)
        {
            var ApplicationWikiLineDuty = _context.TemplateTestCaseCheckListResponseDuty.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId);
            // ApplicationWikiLineDuty.TemplateTestCaseCheckListResponseId = value.TemplateTestCaseCheckListResponseId;
            ApplicationWikiLineDuty.DepartmantId = value.DepartmentId;
            ApplicationWikiLineDuty.DesignationId = value.DesignationId;
            ApplicationWikiLineDuty.CompanyId = value.CompanyId;
            ApplicationWikiLineDuty.EmployeeId = value.EmployeeId;
            ApplicationWikiLineDuty.DesignationNumber = value.DesignationNumber;
            ApplicationWikiLineDuty.DutyNo = value.DutyNo;
            ApplicationWikiLineDuty.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLineDuty.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLineDuty.ModifiedDate = DateTime.Now;
            ApplicationWikiLineDuty.Type = value.Type;
            ApplicationWikiLineDuty.Description = value.Description;
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.TemplateTestCaseCheckListResponseResponsible.Where(e => e.EmployeeId == s && e.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i => i.UserGroupId).FirstOrDefault() : null,
                            TemplateTestCaseCheckListResponseDutyId = value.TemplateTestCaseCheckListResponseDutyId,
                        };
                        _context.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.TemplateTestCaseCheckListResponseResponsible.Where(e => e.EmployeeId == emp && e.TemplateTestCaseCheckListResponseDutyId == value.TemplateTestCaseCheckListResponseDutyId).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new TemplateTestCaseCheckListResponseResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            TemplateTestCaseCheckListResponseDutyId = value.TemplateTestCaseCheckListResponseDutyId,
                                        };
                                        _context.TemplateTestCaseCheckListResponseResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            _context.SaveChanges();
            ModifiedCheckListByTemplateTestCaseCheckListResponseDuty(value, value.ModifiedByUserID.Value);
            return value;

        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLineDuty")]
        public void DeleteApplicationWikiLineDuty(int id)
        {
            var ApplicationWikiLineDuty = _context.TemplateTestCaseCheckListResponseDuty.SingleOrDefault(p => p.TemplateTestCaseCheckListResponseDutyId == id);
            var wikiresponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(p => p.TemplateTestCaseCheckListResponseDutyId == id).ToList();
            if (wikiresponsible != null)
            {
                _context.TemplateTestCaseCheckListResponseResponsible.RemoveRange(wikiresponsible);
                _context.SaveChanges();
            }
            _context.TemplateTestCaseCheckListResponseDuty.Remove(ApplicationWikiLineDuty);
            _context.SaveChanges();
        }
        [HttpDelete]
        [Route("DeleteWikiResponsible")]
        public void DeleteWikiResponsible(int id)
        {
            var wikiresponsible = _context.TemplateTestCaseCheckListResponseResponsible.SingleOrDefault(p => p.WikiResponsibilityId == id);
            if (wikiresponsible != null)
            {
                _context.TemplateTestCaseCheckListResponseResponsible.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }
        [HttpPut]
        [Route("DeleteWikiResponsibles")]
        public WikiResponsibilityModel DeleteWikiResponsibles(WikiResponsibilityModel value)
        {

            // var selectDocumentRoleIds = value.WikiResponsibilityIDs;
            if (value.WikiResponsibilityIDs.Count > 0)
            {
                value.WikiResponsibilityIDs.ForEach(sel =>
                {
                    var wikiresponsible = _context.TemplateTestCaseCheckListResponseResponsible.Where(p => p.WikiResponsibilityId == sel).FirstOrDefault();
                    if (wikiresponsible != null)
                    {


                        _context.TemplateTestCaseCheckListResponseResponsible.Remove(wikiresponsible);
                        _context.SaveChanges();

                    }
                });

                _context.SaveChanges();
            }


            return value;
        }

        #endregion
    }
}
